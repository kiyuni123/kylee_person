﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using log4net;
using log4net.Config;

namespace GTIFramework.Common.Log
{
    public class Logs
    {
        public static string strLogin_ID = "admin";
        public static string strLogin_Edit = "Y";
        public static string strLogin_DESC = "관리자";

        public static void ErrLogging(Exception e)
        {
            Console.WriteLine(e);

            XmlConfigurator.Configure(new FileInfo(Properties.Resources.RES_LOG_CONF));

            if (Properties.Resources.RES_LOG_ERR_YN.ToUpper().Equals("Y"))
            {
                ILog errLog = LogManager.GetLogger("errLogger");

                errLog.Debug("=Start=========================================================================================================");
                errLog.Debug("");
                errLog.Debug(e);
                errLog.Debug("");
                errLog.Debug("=End===========================================================================================================");
            }
        }
    }
}
