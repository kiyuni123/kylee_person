﻿using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using DevExpress.XtraPrinting;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace GTIFramework.Common.Utils.Converters
{
    public class ExcelUtil
    {
        /// <summary>
        /// DataTable을 그대로 보여주기
        /// data : DataTable
        /// strcol : data컬럼명
        /// strFileName : 저장 파일명
        /// </summary>
        /// <param name="data"></param>
        /// <param name="strcol"></param>
        /// <param name="strFileName"></param>
        public static void ExcelWYSIWYG(DataTable data, List<string> strcol, string strFileName)
        {
            if (data == null) return;
            if (strcol.Count == 0) return;

            try
            {
                SaveFileDialog savefile = new SaveFileDialog();
                savefile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                savefile.Title = "엑셀 다운로드";
                savefile.FileName = strFileName + ".xlsx";
                savefile.Filter = "All xlsx Files | *.xlsx";

                if (savefile.ShowDialog() == DialogResult.OK)
                {
                    Excel.Application excel = new Excel.Application();
                    excel.DisplayAlerts = false;

                    Excel.Workbook workbook = excel.Workbooks.Add(Type.Missing);

                    Excel.Worksheet worksheet = (Excel.Worksheet)workbook.ActiveSheet;
                    worksheet.Name = strFileName;

                    Excel.Range range = null;

                    long Rcnt = data.Rows.Count;
                    int Ccnt = data.Columns.Count;

                    object[,] datas = new object[Rcnt + 1, Ccnt];

                    //헤더 정보 + 데이터 정보
                    for (int i = 0; i < Ccnt; i++)
                    {
                        datas[0, i] = strcol[i]; //헤더 정보

                        for (int j = 0; j < Rcnt; j++)
                        {
                            datas[j + 1, i] = data.Rows[j][i].ToString(); //데이터 정보
                        }
                    }

                    //range 설정
                    Excel.Range sPoint = worksheet.Cells[1, 1];
                    Excel.Range ePoint = worksheet.Cells[Rcnt + 1, Ccnt];

                    range = worksheet.get_Range(sPoint, ePoint);
                    range.Value2 = datas;
                    range.EntireColumn.AutoFit();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    range.Borders.Weight = Excel.XlBorderWeight.xlThin;

                    //헤더 범위
                    Excel.Range HsPoint = worksheet.Cells[1, 1];
                    Excel.Range HePoint = worksheet.Cells[1, Ccnt];
                    range = worksheet.get_Range(HsPoint, HePoint);
                    range.Interior.Color = ColorTranslator.ToOle(Color.GreenYellow);

                    workbook.SaveAs(savefile.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                        Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    //프로세스 Kill 후 재실행
                    int hwnd;
                    GetWindowThreadProcessId(excel.Hwnd, out hwnd);

                    Process p = Process.GetProcessById(hwnd);
                    p.Kill();

                    Process process = new Process();
                    process.StartInfo.FileName = savefile.FileName;
                    process.Start();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //데브 WYSIWYG
        public static void DevExcelWYSIWYG(TableView[] view, string strFileName)
        {
            try
            {
                SaveFileDialog savefile = new SaveFileDialog();
                savefile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                savefile.Title = "엑셀 다운로드";
                savefile.FileName = strFileName;
                savefile.Filter = "All xlsx Files | *.xlsx";

                int pageW = 0, pageH = 400;

                if (savefile.ShowDialog() == DialogResult.OK)
                {
                    List<TemplatedLink> links = new List<TemplatedLink>();

                    foreach (TableView tableview in view)
                    {
                        tableview.PrintAutoWidth = false;
                        PrintableControlLink print = new PrintableControlLink(tableview);
                        links.Add(print);

                        if (((DataTable)((GridControl)tableview.Parent).ItemsSource).Columns.Count * 1000 > pageW)
                        {
                            pageW = ((DataTable)((GridControl)tableview.Parent).ItemsSource).Columns.Count * 1000;
                        }
                        if (((DataTable)((GridControl)tableview.Parent).ItemsSource).Rows.Count * 100 + 400 > pageH)
                        {
                            pageH = ((DataTable)((GridControl)tableview.Parent).ItemsSource).Rows.Count * 100 + 400;
                        }
                    }

                    CompositeLink compositeLink = new CompositeLink(links);
                    compositeLink.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                    compositeLink.CustomPaperSize = new Size(pageW, pageH);
                    compositeLink.CreateDocument(false);
                    compositeLink.CreatePageForEachLink();

                    XlsxExportOptionsEx option = new XlsxExportOptionsEx();
                    option.ExportMode = XlsxExportMode.SingleFilePageByPage;
                    option.ExportType = DevExpress.Export.ExportType.WYSIWYG;

                    compositeLink.ExportToXlsx(savefile.FileName, option);
                    Messages.ShowOkMsgBox();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// DataTable(밴드)을 그대로 보여주기
        /// data : DataTable
        /// strBand : data밴드명(같은값 데이터일 경우 Merge)
        /// strcol : data컬럼명
        /// strFileName : 저장 파일명
        /// </summary>
        /// <param name="data"></param>
        /// <param name="strcol"></param>
        /// <param name="strFileName"></param>
        public static void ExcelWYSIWYG(DataTable data, List<string> strBand, List<string> strcol, string strFileName)
        {
            if (data == null) return;
            if (strcol.Count == 0) return;

            try
            {
                SaveFileDialog savefile = new SaveFileDialog();
                savefile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                savefile.Title = "엑셀 다운로드";
                savefile.FileName = strFileName + ".xlsx";
                savefile.Filter = "All xlsx Files | *.xlsx";

                if (savefile.ShowDialog() == DialogResult.OK)
                {
                    Excel.Application excel = new Excel.Application();
                    excel.DisplayAlerts = false;

                    Excel.Workbook workbook = excel.Workbooks.Add(Type.Missing);

                    Excel.Worksheet worksheet = (Excel.Worksheet)workbook.ActiveSheet;
                    worksheet.Name = strFileName;

                    Excel.Range range = null;

                    long Rcnt = data.Rows.Count;
                    int Ccnt = data.Columns.Count;

                    object[,] datas = new object[Rcnt + 2, Ccnt];

                    //밴드 정보 + 데이터 정보
                    for (int i = 0; i < Ccnt; i++)
                    {
                        datas[0, i] = strBand[i]; //헤더 정보
                    }

                    //헤더 정보 + 데이터 정보
                    for (int i = 0; i < Ccnt; i++)
                    {
                        datas[1, i] = strcol[i]; //헤더 정보

                        for (int j = 0; j < Rcnt; j++)
                        {
                            datas[j + 2, i] = data.Rows[j][i].ToString(); //데이터 정보
                        }
                    }

                    int Fcell = 0;
                    int Lcell = 0;

                    for (int i = 0; i < Ccnt - 1; i++)
                    {
                        if (!datas[0, i].Equals(datas[0, i + 1]))
                        {
                            Lcell = i;
                            Excel.Range sp = worksheet.Cells[1, Lcell + 1];
                            Excel.Range ep = worksheet.Cells[1, Fcell + 1];
                            range = worksheet.get_Range(sp, ep);
                            range.Merge(true);
                            Fcell = i + 1;
                        }
                        if (i == Ccnt - 2)
                        {
                            Lcell = i+1;
                            Excel.Range sp = worksheet.Cells[1, Lcell + 1];
                            Excel.Range ep = worksheet.Cells[1, Fcell + 1];
                            range = worksheet.get_Range(sp, ep);
                            range.Merge(true);
                        }
                        if (datas[0, i].Equals(datas[1,i]))
                        {
                            Excel.Range sp = worksheet.Cells[1, i+1];
                            Excel.Range ep = worksheet.Cells[2, i+1];
                            range = worksheet.get_Range(sp, ep);
                            range.Merge(false);
                        }
                    }

                    //range 설정
                    Excel.Range sPoint = worksheet.Cells[1, 1];
                    Excel.Range ePoint = worksheet.Cells[Rcnt + 1, Ccnt];

                    range = worksheet.get_Range(sPoint, ePoint);
                    range.Value2 = datas;
                    range.EntireColumn.AutoFit();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    range.Borders.Weight = Excel.XlBorderWeight.xlThin;

                    //헤더 범위
                    Excel.Range HsPoint = worksheet.Cells[1, 1];
                    Excel.Range HePoint = worksheet.Cells[2, Ccnt];
                    range = worksheet.get_Range(HsPoint, HePoint);
                    range.Interior.Color = ColorTranslator.ToOle(Color.GreenYellow);

                    workbook.SaveAs(savefile.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                        Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    //프로세스 Kill 후 재실행
                    int hwnd;
                    GetWindowThreadProcessId(excel.Hwnd, out hwnd);

                    Process p = Process.GetProcessById(hwnd);
                    p.Kill();

                    Process process = new Process();
                    process.StartInfo.FileName = savefile.FileName;
                    process.Start();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// DataTable을 그대로 보여주기
        /// data : DataTable
        /// strcol : data 컬럼명
        /// strHide : data Hiden 컬럼명 strcol명과 같아야 한다.
        /// strFileName : 저장 파일명
        /// </summary>
        /// <param name="data"></param>
        /// <param name="strcol"></param>
        /// <param name="strHide"></param>
        /// <param name="strFileName"></param>
        public static void ExcelColumnHide(DataTable data, List<string> strcol, List<string> strHide, string strFileName)
        {
            if (data == null) return;
            if (strcol.Count == 0) return;

            try
            {
                SaveFileDialog savefile = new SaveFileDialog();
                savefile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                savefile.Title = "엑셀 다운로드";
                savefile.FileName = strFileName + ".xlsx";
                savefile.Filter = "All xlsx Files | *.xlsx";

                if (savefile.ShowDialog() == DialogResult.OK)
                {
                    Excel.Application excel = new Excel.Application();
                    excel.DisplayAlerts = false;

                    Excel.Workbook workbook = excel.Workbooks.Add(Type.Missing);

                    Excel.Worksheet worksheet = (Excel.Worksheet)workbook.ActiveSheet;
                    worksheet.Name = strFileName;

                    Excel.Range range = null;

                    long Rcnt = data.Rows.Count;
                    int Ccnt = data.Columns.Count;

                    object[,] datas = new object[Rcnt + 1, Ccnt];
                    List<int> inthide = new List<int>();

                    //헤더 정보 + 데이터 정보
                    for (int i = 0; i < Ccnt; i++)
                    {
                        datas[0, i] = strcol[i]; //헤더 정보

                        if (strHide.Contains(strcol[i]))
                        {
                            inthide.Add(i);
                        }

                        for (int j = 0; j < Rcnt; j++)
                        {
                            datas[j + 1, i] = data.Rows[j][i].ToString(); //데이터 정보
                        }
                    }

                    //range 설정
                    Excel.Range sPoint = worksheet.Cells[1, 1];
                    Excel.Range ePoint = worksheet.Cells[Rcnt + 1, Ccnt];

                    range = worksheet.get_Range(sPoint, ePoint);
                    range.Value2 = datas;
                    range.EntireColumn.AutoFit();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    range.Borders.Weight = Excel.XlBorderWeight.xlThin;

                    //헤더 범위
                    Excel.Range HsPoint = worksheet.Cells[1, 1];
                    Excel.Range HePoint = worksheet.Cells[1, Ccnt];
                    range = worksheet.get_Range(HsPoint, HePoint);
                    range.Interior.Color = ColorTranslator.ToOle(Color.GreenYellow);

                    //컬럼 숨기기
                    foreach (int i in inthide)
                    {
                        range = (Excel.Range)worksheet.Cells[1, i + 1];
                        range.EntireColumn.Hidden = true;
                    }

                    workbook.SaveAs(savefile.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                        Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    //프로세스 Kill 후 재실행
                    int hwnd;
                    GetWindowThreadProcessId(excel.Hwnd, out hwnd);

                    Process p = Process.GetProcessById(hwnd);
                    p.Kill();

                    Process process = new Process();
                    process.StartInfo.FileName = savefile.FileName;
                    process.Start();
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("을(를) 사용할 수 없습니다") != -1)
                {
                    Messages.ShowErrMsgBox("엑셀을 종료하세요.");
                }
                else
                {
                    throw ex;
                }

            }
        }

        /// <summary>
        /// Excel데이터 DataTable 리턴
        /// </summary>
        /// <returns></returns>
        public static DataTable ExcelImport(string FilePath)
        {
            DataTable dtresult = new DataTable();

            try
            {
                Excel.Application excel = new Excel.Application();

                Excel.Workbook workbook = excel.Workbooks.Open(FilePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                Excel.Worksheet worksheet = (Excel.Worksheet)workbook.Sheets.get_Item(1);

                Excel.Range range = worksheet.UsedRange;

                object[,] valueArray = (object[,])range.get_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault);

                dtresult = ProcessObjects(valueArray);

                int hwnd;
                GetWindowThreadProcessId(excel.Hwnd, out hwnd);

                Process p = Process.GetProcessById(hwnd);
                p.Kill();

                return dtresult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region 삽질
        //public static void ExcelColumnHide(string strFileName, DataSet DS, List<string> strCol)
        //{
        //    SaveFileDialog savefile = new SaveFileDialog();
        //    savefile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        //    savefile.Title = "엑셀 다운로드";
        //    savefile.FileName = strFileName + ".xlsx";
        //    savefile.Filter = "All xlsx Files | *.xlsx";

        //    if (savefile.ShowDialog() == DialogResult.OK)
        //    {
        //        //같은이름 File 삭제
        //        if (File.Exists(savefile.FileName))
        //        {
        //            File.Delete(savefile.FileName);
        //        }

        //        string TempFile = savefile.FileName;

        //        OleDbConnection OleDBConn = null;

        //        try
        //        {
        //            OleDbCommand Cmd = null;

        //            string ConnStr = 
        //                string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"{0}\";Mode=ReadWrite|Share Deny None;Extended Properties='Excel 12.0;HDR=YES';Persist Security Info=False", savefile.FileName);

        //            OleDBConn = new OleDbConnection(ConnStr);
        //            OleDBConn.Open();

        //            // Create Table(s).. : 테이블 단위 처리
        //            foreach (DataTable DT in DS.Tables)
        //            {
        //                String TableName = DT.TableName;

        //                StringBuilder FldsInfo = new StringBuilder();
        //                StringBuilder Flds = new StringBuilder();

        //                // Create Field(s) String : 현재 테이블의 Field 명 생성
        //                for (int i = 0; i < strCol.Count; i++)
        //                {
        //                    if (FldsInfo.Length > 0)
        //                    {
        //                        FldsInfo.Append(",");
        //                        Flds.Append(",");
        //                    }

        //                    FldsInfo.Append("[" + strCol[i].Replace("'", "''") + "] CHAR(255)");
        //                    Flds.Append(strCol[i].Replace("'", "''"));
        //                }

        //                // 테이블 생성
        //                Cmd = new OleDbCommand("CREATE TABLE ["+ TableName +"](" + FldsInfo.ToString() + ")", OleDBConn);
        //                Cmd.ExecuteNonQuery();

        //                // 데이터 바인딩
        //                foreach (DataRow DR in DT.Rows)
        //                {
        //                    StringBuilder Values = new StringBuilder();
        //                    foreach (DataColumn Column in DT.Columns)
        //                    {
        //                        if (Values.Length > 0) Values.Append(",");
        //                        Values.Append("'" + DR[Column.ColumnName].ToString().Replace("'", "''") + "'");
        //                    }

        //                    Cmd = new OleDbCommand(
        //                        "INSERT INTO [" + TableName + "]" +
        //                        "(" + Flds.ToString() + ") " +
        //                        "VALUES (" + Values.ToString() + ")",
        //                        OleDBConn);
        //                    Cmd.ExecuteNonQuery();
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //        finally
        //        {
        //            if (OleDBConn != null) OleDBConn.Close();
        //        }
        //    }
        //}




        //public static void ExcelWYSIWYG(DataTable data, List<string> strcol, string strFileName)
        //{
        //    if (data == null) return;
        //    if (strcol.Count == 0) return;

        //    try
        //    {
        //        SaveFileDialog savefile = new SaveFileDialog();
        //        savefile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        //        savefile.Title = "엑셀 다운로드";
        //        savefile.FileName = strFileName + ".xlsx";
        //        savefile.Filter = "All xlsx Files | *.xlsx";

        //        if (savefile.ShowDialog() == DialogResult.OK)
        //        {
        //            Excel.Application excel = new Excel.Application();
        //            excel.DisplayAlerts = false;
        //            Excel.Workbook workbook = excel.Workbooks.Add(Type.Missing);
        //            Excel.Worksheet worksheet = (Excel.Worksheet)workbook.Worksheets[1];
        //            Excel.Range range = null;

        //            worksheet.Name = strFileName;

        //            //데이터 바인딩
        //            for (int i = 0; i < strcol.Count; i++)
        //            {
        //                //헤더 바인딩
        //                range = (Excel.Range)worksheet.Cells[1, i + 1];
        //                range.Cells.NumberFormat = "@";
        //                worksheet.Cells[1, i + 1] = strcol[i].ToString();
        //                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //                range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
        //                range.Borders.Weight = Excel.XlBorderWeight.xlThin;
        //                range.Interior.Color = ColorTranslator.ToOle(Color.GreenYellow);
        //                range.EntireColumn.AutoFit();

        //                //내용 바인딩
        //                for (int j = 0; j < data.Rows.Count; j++)
        //                {
        //                    range = (Excel.Range)worksheet.Cells[j + 2, i + 1];
        //                    range.Cells.NumberFormat = "@";
        //                    worksheet.Cells[j + 2, i + 1] = data.Rows[j][i].ToString();
        //                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //                    range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
        //                    range.Borders.Weight = Excel.XlBorderWeight.xlThin;

        //                    range.EntireColumn.AutoFit();
        //                }
        //            }

        //            excel.Visible = true;

        //            workbook.SaveAs(savefile.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing
        //                , Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion

        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

        /// <summary>
        /// object[,]를 DataTable로
        /// </summary>
        /// <param name="valueArray"></param>
        /// <returns></returns>
        private static DataTable ProcessObjects(object[,] valueArray)
        {
            DataTable dt = new DataTable();

            try
            {
                //컬럼이름 생성
                for (int k = 1; k <= valueArray.GetLength(1); k++)
                {
                    dt.Columns.Add((string)valueArray[1, k]);
                }

                //데이터 바인딩
                object[] singleDValue = new object[valueArray.GetLength(1)];

                for (int i = 2; i <= valueArray.GetLength(0); i++)
                {
                    for (int j = 0; j < valueArray.GetLength(1); j++)
                    {
                        if (valueArray[i, j + 1] != null)
                        {
                            singleDValue[j] = valueArray[i, j + 1].ToString();
                        }
                        else
                        {
                            singleDValue[j] = valueArray[i, j + 1];
                        }
                    }
                    dt.LoadDataRow(singleDValue, LoadOption.PreserveChanges);
                }

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #region 커스텀
        /// <summary>
        /// DataTable(밴드)을 그대로 보여주기
        /// data : DataTable
        /// strBand : data밴드명(같은값 데이터일 경우 Merge)
        /// strcol : data컬럼명
        /// strFileName : 저장 파일명
        /// </summary>
        /// <param name="data"></param>
        /// <param name="strcol"></param>
        /// <param name="strFileName"></param>
        public static void ExcelWQStatistics(Hashtable htTable, Hashtable htBand, Hashtable htcol, string strFileName)
        {
            if (htTable.Count == 0 || htBand.Count == 0 || htcol.Count == 0 || strFileName == "") return;

            try
            {
                SaveFileDialog savefile = new SaveFileDialog();
                savefile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                savefile.Title = "엑셀 다운로드";
                savefile.FileName = strFileName + ".xlsx";
                savefile.Filter = "All xlsx Files | *.xlsx";

                if (savefile.ShowDialog() == DialogResult.OK)
                {
                    Excel.Application excel = new Excel.Application();
                    excel.DisplayAlerts = false;

                    Excel.Workbook workbook = excel.Workbooks.Add(Type.Missing);

                    Excel.Worksheet worksheet = (Excel.Worksheet)workbook.ActiveSheet;
                    worksheet.Name = strFileName;

                    Excel.Range range = null;

                    long Rcnt = 0;
                    int Ccnt = 0;

                    for (int i = 1; i < htTable.Count + 1; i++)
                    {
                        DataTable temp = (DataTable)htTable[i.ToString()];
                        Rcnt = Rcnt + temp.Rows.Count + 2;

                        if (Ccnt < temp.Columns.Count) Ccnt = temp.Columns.Count;
                    }

                    object[,] datas = new object[Rcnt, Ccnt];

                    int Bandcnt = 0;
                    List<int> inthead = new List<int>();

                    //밴드 정보 + 데이터 정보
                    for (int i = 1; i < htBand.Count + 1; i++)
                    {
                        List<string> strband = (List<string>)htBand[i.ToString()];
                        List<string> strcol = (List<string>)htcol[i.ToString()];

                        DataTable data = (DataTable)htTable[i.ToString()];

                        for (int j = 0; j < strband.Count; j++)
                        {
                            datas[Bandcnt, j] = strband[j]; //밴드 정보
                            inthead.Add(Bandcnt);
                        }
                        Bandcnt++;

                        for (int j = 0; j < strcol.Count; j++)
                        {
                            datas[Bandcnt, j] = strcol[j]; //헤더 정보
                            inthead.Add(Bandcnt);
                        }
                        Bandcnt++;

                        for (int j = 0; j < data.Rows.Count; j++)
                        {
                            for (int k = 0; k < data.Columns.Count; k++)
                            {
                                datas[Bandcnt, k] = data.Rows[j][k].ToString(); //데이터 정보
                            }
                            Bandcnt++;
                        }
                    }

                    //range 설정
                    Excel.Range sPoint = worksheet.Cells[1, 1];
                    Excel.Range ePoint = worksheet.Cells[Rcnt, Ccnt];
                    range = worksheet.get_Range(sPoint, ePoint);
                    range.Value2 = datas;
                    range.EntireColumn.AutoFit();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    range.Borders.Weight = Excel.XlBorderWeight.xlThin;

                    //헤더 범위
                    foreach (int inth in inthead)
                    {
                        Excel.Range HsPoint = worksheet.Cells[inth + 1, 1];
                        Excel.Range HePoint = worksheet.Cells[inth + 1, Ccnt];
                        range = worksheet.get_Range(HsPoint, HePoint);
                        range.Interior.Color = ColorTranslator.ToOle(Color.GreenYellow);

                        int Fcell = 0;
                        int Lcell = 0;

                        for (int i = 0; i < Ccnt-1; i++)
                        {
                            if (!datas[inth, i].Equals(datas[inth, i + 1]))
                            {
                                Lcell = i;
                                Excel.Range ep = worksheet.Cells[inth + 1, Fcell + 1];
                                Excel.Range sp = worksheet.Cells[inth + 1, Lcell + 1];
                                range = worksheet.get_Range(sp, ep);
                                range.Merge(true);
                                Fcell = i + 1;
                            }
                        }
                    }

                    workbook.SaveAs(savefile.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                        Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    //프로세스 Kill 후 재실행
                    int hwnd;
                    GetWindowThreadProcessId(excel.Hwnd, out hwnd);

                    Process p = Process.GetProcessById(hwnd);
                    p.Kill();

                    Process process = new Process();
                    process.StartInfo.FileName = savefile.FileName;
                    process.Start();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
