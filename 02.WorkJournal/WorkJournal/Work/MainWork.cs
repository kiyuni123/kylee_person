﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkJournal.Dao;

namespace WorkJournal.Work
{
    class MainWork
    {
        MainDao dao = new MainDao();

        /// <summary>
        /// 조 로그인 정보 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_LoginInfo(Hashtable conditions)
        {
            return dao.Select_LoginInfo(conditions);
        }
    }
}
