﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkJournal.Dao;

namespace WorkJournal.Work
{
    class TeamManageWork
    {
        TeamManageDao dao = new TeamManageDao();

        /// <summary>
        /// 조 리스트 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_LoginInfo(Hashtable conditions)
        {
            return dao.Select_TeamInfoList(conditions);
        }

        /// <summary>
        /// 근무자 리스트 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_UserInfoList(Hashtable conditions)
        {
            return dao.Select_UserInfoList(conditions);
        }

        /// <summary>
        /// 조 유무 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_GRPIDChk(Hashtable conditions)
        {
            return dao.Select_GRPIDChk(conditions);
        }

        /// <summary>
        /// 조 추가 INSERT
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public void Insert_GRPMSTINFO(Hashtable conditions)
        {
            dao.Insert_GRPMSTINFO(conditions);
        }

        /// <summary>
        /// 조 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_GRPMSTINFO(Hashtable conditions)
        {
            dao.Delete_GRPMSTINFO(conditions);
        }

        /// <summary>
        /// 조 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_GRPMSTINFO(Hashtable conditions)
        {
            dao.Update_GRPMSTINFO(conditions);
        }

        /// <summary>
        /// 근무자 추가 INSERT
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public void Insert_GRPDTLINFO(Hashtable conditions)
        {
            dao.Insert_GRPDTLINFO(conditions);
        }

        /// <summary>
        /// 근무자 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_GRPDTLINFO(Hashtable conditions)
        {
            dao.Delete_GRPDTLINFO(conditions);
        }

        /// <summary>
        /// 근무자 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_GRPDTLINFO(Hashtable conditions)
        {
            dao.Update_GRPDTLINFO(conditions);
        }
    }
}
