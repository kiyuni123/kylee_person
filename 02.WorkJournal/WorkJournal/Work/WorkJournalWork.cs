﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkJournal.Dao;

namespace WorkJournal.Work
{
    class WorkJournalWork
    {
        WorkJournalDao dao = new WorkJournalDao();

        /// <summary>
        /// 조 리스트 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_GRPList(Hashtable conditions)
        {
            return dao.Select_GRPList(conditions);
        }

        /// <summary>
        /// 업무 리스트
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_WorkRptList(Hashtable conditions)
        {
            return dao.Select_WorkRptList(conditions);
        }

        /// <summary>
        /// 등록 버튼시 순번 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_WorkORD(Hashtable conditions)
        {
            return dao.Select_WorkORD(conditions);
        }

        /// <summary>
        /// 저장버튼(추가)
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_WorkRpt(Hashtable conditions)
        {
            dao.Insert_WorkRpt(conditions);
        }

        /// <summary>
        /// 업무일지 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_WorkRpt(Hashtable conditions)
        {
            dao.Delete_WorkRpt(conditions);
        }

        /// <summary>
        /// 업무일지 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_WorkRpt(Hashtable conditions)
        {
            dao.Update_WorkRpt(conditions);
        }
    }
}
