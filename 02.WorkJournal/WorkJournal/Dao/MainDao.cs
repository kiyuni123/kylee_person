﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkJournal.Dao
{
    class MainDao
    {
        /// <summary>
        /// 조 로그인 정보 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_LoginInfo(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_LoginInfo", conditions);
        }
    }
}
