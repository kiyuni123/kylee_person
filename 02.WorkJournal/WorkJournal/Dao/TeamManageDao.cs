﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkJournal.Dao
{
    class TeamManageDao
    {
        /// <summary>
        /// 조 리스트 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_TeamInfoList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_TeamInfoList", conditions);
        }

        /// <summary>
        /// 근무자 리스트 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_UserInfoList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_UserInfoList", conditions);
        }

        /// <summary>
        /// 조 유무 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_GRPIDChk(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_GRPIDChk", conditions);
        }

        /// <summary>
        /// 조 추가 INSERT
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public void Insert_GRPMSTINFO(Hashtable conditions)
        {
            DBManager.QueryForInsert("Insert_GRPMSTINFO", conditions);
        }

        /// <summary>
        /// 조 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_GRPMSTINFO(Hashtable conditions)
        {
            DBManager.QueryForUpdate("Delete_GRPMSTINFO", conditions);
        }

        /// <summary>
        /// 조 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_GRPMSTINFO(Hashtable conditions)
        {
            DBManager.QueryForUpdate("Update_GRPMSTINFO", conditions);
        }

        /// <summary>
        /// 근무자 추가 INSERT
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public void Insert_GRPDTLINFO(Hashtable conditions)
        {
            DBManager.QueryForInsert("Insert_GRPDTLINFO", conditions);
        }

        /// <summary>
        /// 근무자 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_GRPDTLINFO(Hashtable conditions)
        {
            DBManager.QueryForUpdate("Delete_GRPDTLINFO", conditions);
        }

        /// <summary>
        /// 근무자 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_GRPDTLINFO(Hashtable conditions)
        {
            DBManager.QueryForUpdate("Update_GRPDTLINFO", conditions);
        }
    }
}
