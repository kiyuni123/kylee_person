﻿using DevExpress.Xpf.Accordion;
using GTIFramework.Common.Log;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WorkJournal.Form.uc;

namespace WorkJournal.Form
{   
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {                    
        //선언부
        ucWorkJournal ucworkJournal = new ucWorkJournal();
        ucTeamManage ucteamManage = new ucTeamManage();

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        /// <summary>
        /// 로드이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ThemeApply.Themeapply(this);
            InitializePage();
            InitializeEvent();

            if(!Logs.strLogin_ID.Equals(Properties.Settings.Default.adminID))
            {
                btnworkjournal.Visibility = Visibility.Collapsed;
                btnteammanage.Visibility = Visibility.Collapsed;
            }
        }

        #region 이벤트
        /// <summary>
        /// 업무관리 Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Acc_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AccordionItem item = sender as AccordionItem;

            UIHidden();

            switch (item.Header.ToString())
            {
                case "업무일지":
                    ucworkJournal.Visibility = Visibility.Visible;
                    break;
                case "조관리":
                    ucteamManage.Visibility = Visibility.Visible;
                    break;
            }
        }
        #endregion

        #region 함수
        /// <summary>
        /// 페이지 바인딩
        /// </summary>
        private void InitializePage()
        {
            contentduck.Children.Add(ucworkJournal);
            contentduck.Children.Add(ucteamManage);
        }

        /// <summary>
        /// 이벤트 선언부
        /// </summary>
        private void InitializeEvent()
        {
            accworkjournal.PreviewMouseLeftButtonDown += Acc_PreviewMouseLeftButtonDown;
            accteammanage.PreviewMouseLeftButtonDown += Acc_PreviewMouseLeftButtonDown;

            btnteammanage.Click += Btn_Click;
            btnworkjournal.Click += Btn_Click;

            this.Closed += MainWindow_Closed;
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            Button item = sender as Button;

            UIHidden();

            switch(item.Content.ToString())
            {
                case "업무일지":
                    ucworkJournal.Visibility = Visibility.Visible;
                    break;
                case "조관리":
                    ucteamManage.Visibility = Visibility.Visible;
                    break;
            }
        }

        /// <summary>
        /// 화면 Form Hidden
        /// </summary>
        private void UIHidden()
        {
            foreach (Control element in contentduck.Children)
            {
                element.Visibility = Visibility.Collapsed;
            }
        }
        #endregion

    }
}
