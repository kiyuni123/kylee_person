﻿using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WorkJournal.Work;

namespace WorkJournal.Form
{
    /// <summary>
    /// Login.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Login : Window
    {
        //추후 로그인 기능 개발
        bool blogin = false;
        MainWork work = new MainWork();
        MainWindow main = new MainWindow();

        public Login()
        {
            InitializeComponent();
            Loaded += Login_Loaded;
        }

        /// <summary>
        /// 로드 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Login_Loaded(object sender, RoutedEventArgs e)
        {
            ThemeApply.Themeapply(this);
            InitializeEvent();
        }

        #region 이벤트
        /// <summary>
        /// 로그인 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btnlogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                loginRun();

                if (blogin)
                {          
                    main.Show();
                    Close();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 함수
        /// <summary>
        /// 이벤트 초기화
        /// </summary>
        private void InitializeEvent()
        {
            btnlogin.Click += Btnlogin_Click;
            txtpw.KeyDown += Txtpw_KeyDown;
            btnclose.Click += Btnclose_Click;
        }

        private void Btnclose_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Txtpw_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key.Equals(Key.Enter))
            {
                Btnlogin_Click(null, null);
            }
        }

        /// <summary>
        /// 로그인 실행 (blogin 값이 성공시 true, 실패시 false)
        /// </summary>
        private void loginRun()
        {
            string keyinid = string.Empty;
            string keyinpwd = string.Empty;

            try
            {
                keyinid = txtid.Text;
                keyinpwd = txtpw.Text;

                DataTable dtUserList = new DataTable();
                Hashtable conditions = new Hashtable();
                conditions.Add("GRP_ID", keyinid);
                dtUserList = work.Select_LoginInfo(conditions);

                if(txtid.Text.Length == 0)
                {
                    Messages.ShowInfoMsgBox("아이디를 입력해주세요.");
                    txtid.Focus();
                    return;
                }
                else if(txtpw.Text.Length == 0)
                {
                    Messages.ShowInfoMsgBox("비밀번호를 입력해주세요.");
                    txtpw.Focus();
                    return;
                }

                //사용자 유무 확인
                if(dtUserList.Rows.Count == 0)
                {
                    if(txtid.Text.ToLower().Equals(Properties.Settings.Default.adminID.ToLower()))
                    {
                        if(txtpw.Text.ToLower().Equals(Properties.Settings.Default.adminPW.ToLower()))
                        {
                            //Messages.ShowInfoMsgBox("관리자로 로그인하셨습니다.");
                            blogin = true;
                        }
                        else if(!txtpw.Text.ToLower().Equals(Properties.Settings.Default.adminPW.ToLower()))
                        {
                            Messages.ShowInfoMsgBox("비밀번호가 틀립니다.");
                        }
                    }
                    else
                    {
                        Messages.ShowInfoMsgBox("등록된 아이디가 없습니다.");
                    }
                }
                else
                {
                    //아이디 삭제여부
                    if(dtUserList.Rows[0]["DEL_YN"].ToString().Equals("Y"))
                    {
                        Messages.ShowInfoMsgBox("아이디가 삭제되었습니다.");
                    }
                    else
                    {    
                        //패스워드 검증
                        if(dtUserList.Rows[0]["GRP_PWD"].ToString().Equals(keyinpwd))
                        {
                            Logs.strLogin_ID = keyinid;
                            Logs.strLogin_DESC = dtUserList.Rows[0]["GRP_DESC"].ToString();
                            Logs.strLogin_Edit = dtUserList.Rows[0]["EDT_YN"].ToString();
                            blogin = true;
                        }
                        //패스워드 다를경우 MessageBox
                        else
                        {
                            Messages.ShowInfoMsgBox("비밀번호가 틀립니다.");
                        }
                    }
                }                 
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
