﻿using DevExpress.Xpf.Grid;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WorkJournal.Form.Popup;
using WorkJournal.Work;

namespace WorkJournal.Form.uc
{
    /// <summary>
    /// ucTeamManage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucTeamManage : UserControl
    {
        TeamManageWork work = new TeamManageWork();

        public ucTeamManage()
        {
            InitializeComponent();
            Loaded += UcTeamManage_Loaded;
        }

        private void UcTeamManage_Loaded(object sender, RoutedEventArgs e)
        {
            InitializePage();
            InitializeEvent();
        }

        #region 함수
        /// <summary>
        /// 이벤트 선언부 new
        /// </summary>
        private void InitializeEvent()
        {
            btnMemberRegister.Click += BtnMemberRegister_Click;
            btnTeamRegister.Click += BtnTeamRegister_Click;
            gdTeamList.MouseDown += GdTeamList_MouseDown;
            gdTeamList.Loaded += GdTeamList_Loaded;
            ((TableView)gdTeamList.View).RowDoubleClick += UcTeamManage_RowDoubleClick;
            ((TableView)gdMemberList.View).RowDoubleClick += UcUserManage_RowDoubleClick;
        }

        private void UcUserManage_RowDoubleClick(object sender, RowDoubleClickEventArgs e)
        {
            try
            {
                IList selectitem = e.Source.SelectedRows;
                DataRowView drv = selectitem[0] as DataRowView;

                PopupUserMng popup = new PopupUserMng(drv, this);
                popup.ShowDialog();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void UcTeamManage_RowDoubleClick(object sender, RowDoubleClickEventArgs e)
        {
            try
            {
                IList selectitem = e.Source.SelectedRows;
                DataRowView drv = selectitem[0] as DataRowView;

                PopupTeamMng popup = new PopupTeamMng(drv, this);
                popup.ShowDialog();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 페이지 바인딩 new
        /// </summary>
        public void InitializePage()
        {
            try
            {
                //조리스트 조회
                gdTeamList.ItemsSource = work.Select_LoginInfo(null);
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 팀 로우 선택 액션
        /// </summary>
        public void TeamRowSelect()
        {
            try
            {
                IList selectitem = ((TableView)gdTeamList.View).SelectedRows;

                if(selectitem.Count == 1)
                {
                    DataRowView drv = selectitem[0] as DataRowView;

                    Hashtable conditions = new Hashtable();
                    conditions.Add("GRP_CD", drv["GRP_CD"].ToString());
                    gdMemberList.ItemsSource = work.Select_UserInfoList(conditions);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 이벤트
        /// <summary>
        /// 조 등록
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnTeamRegister_Click(object sender, RoutedEventArgs e)
        {
            PopupTeamMng popup = new PopupTeamMng(this);
            popup.ShowDialog();
        }

        /// <summary>
        /// 근무자 등록
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMemberRegister_Click(object sender, RoutedEventArgs e)
        {
            IList selectitem = gdTeamList.View.SelectedRows;
            

            if(selectitem.Count == 0)
            {
                PopupUserMng popup = new PopupUserMng(this, string.Empty);
                popup.ShowDialog();
            }
            else
            {
                DataRowView drv = selectitem[0] as DataRowView;
                PopupUserMng popup = new PopupUserMng(this, drv["GRP_CD"].ToString());
                popup.ShowDialog();
            }

            
        }

        /// <summary>
        /// 조 Load 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GdTeamList_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                TeamRowSelect();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 조 Row 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GdTeamList_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TeamRowSelect();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion
    }
}
