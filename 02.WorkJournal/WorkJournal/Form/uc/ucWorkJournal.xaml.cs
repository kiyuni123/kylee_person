﻿using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using DevExpress.XtraPrinting;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WorkJournal.Form.Popup;
using WorkJournal.Work;

namespace WorkJournal.Form.uc
{
    /// <summary>
    /// ucWorkJournal.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucWorkJournal : UserControl
    {
        WorkJournalWork work = new WorkJournalWork();

        public ucWorkJournal()
        {
            InitializeComponent();
            Loaded += UcWorkJournal_Loaded;
        }

        private void UcWorkJournal_Loaded(object sender, RoutedEventArgs e)
        {
            InitializePage();
            InitializeEvent();
        }

        #region 함수
        /// <summary>
        /// 페이지 바인딩
        /// </summary>
        private void InitializePage()
        {
            try
            {
                DateTime timenow = DateTime.Now;

                SDate.DateTime = new DateTime(timenow.Year, timenow.Month, timenow.AddDays(-1).Day, 07, 00, 00);
                EDate.DateTime = new DateTime(timenow.Year, timenow.Month, timenow.Day, timenow.Hour, timenow.Minute, 00);

                DataTable temp = new DataTable();
                temp = work.Select_GRPList(null);
                DataRow r = temp.NewRow();
                r["GRP_CD"] = "*";
                r["GRP_DESC"] = "전체";
                temp.Rows.InsertAt(r, 0);

                cbteam.ItemsSource = temp;

                BtnSearch_Click(null, null);
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }     
        }

        /// <summary>
        /// 이벤트 선언부
        /// </summary>
        private void InitializeEvent()
        {
            btnSearch.Click += BtnSearch_Click;
            btnExcel.Click += BtnExcel_Click;
            btnRegister.Click += BtnRegister_Click;
            btnInit.Click += BtnInit_Click;
            ((TableView)gdList.View).RowDoubleClick += UcWorkJournal_RowDoubleClick;
        }

        private void BtnInit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime timenow = DateTime.Now;

                SDate.DateTime = new DateTime(timenow.Year, timenow.Month, timenow.AddDays(-1).Day , 07, 00, 00);
                EDate.DateTime = new DateTime(timenow.Year, timenow.Month, timenow.Day, timenow.Hour, timenow.Minute, 00);
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 이벤트
        /// <summary>
        /// 그리드 row 더블클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcWorkJournal_RowDoubleClick(object sender, RowDoubleClickEventArgs e)
        {
            try
            {
                IList selectitem = e.Source.SelectedRows;
                DataRowView drv = selectitem[0] as DataRowView;

                PopupWorkMng popup = new PopupWorkMng(drv, this);
                popup.ShowDialog();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 등록버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            PopupWorkMng popup = new PopupWorkMng(this);
            popup.ShowDialog();
        }

        /// <summary>
        /// 엑셀버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnExcel_Click(object sender, RoutedEventArgs e)
        {
            try
            {   
                if(((DataTable)gdList.ItemsSource).Rows.Count == 0)
                {
                    Messages.ShowInfoMsgBox("엑셀로 저장할 데이터가 없습니다.");
                }
                else
                {
                    TableView[] view = { (TableView)gdList.View };
                    string filename = cbteam.DisplayText + " 업무일지 (" + SDate.DateTime.ToString("yyyy년MM월dd일HH시mm분") + "~" + EDate.DateTime.ToString("yyyy년MM월dd일HH시mm분") + ")";

                    System.Windows.Forms.SaveFileDialog savefile = new System.Windows.Forms.SaveFileDialog();

                    string strDir = @"C:\업무일지\" + DateTime.Now.ToString("yyyy") + @"년\" + DateTime.Now.ToString("MM") + @"월";
                    System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(strDir);
                    if(di.Exists == false)
                    {
                        di.Create();
                    }

                    savefile.InitialDirectory = @"C:\업무일지\" + DateTime.Now.ToString("yyyy") + @"년\" + DateTime.Now.ToString("MM") + @"월";
                    savefile.Title = "엑셀 다운로드";
                    savefile.FileName = filename;
                    savefile.Filter = "All xlsx Files | *.xlsx";

                    int pageW = 0, pageH = 400;

                    if(savefile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        List<TemplatedLink> links = new List<TemplatedLink>();

                        foreach(TableView tableview in view)
                        {
                            tableview.PrintAutoWidth = false;
                            PrintableControlLink print = new PrintableControlLink(tableview);
                            links.Add(print);

                            if(((DataTable)((GridControl)tableview.Parent).ItemsSource).Columns.Count * 1000 > pageW)
                            {
                                pageW = ((DataTable)((GridControl)tableview.Parent).ItemsSource).Columns.Count * 1000;
                            }
                            if(((DataTable)((GridControl)tableview.Parent).ItemsSource).Rows.Count * 100 + 400 > pageH)
                            {
                                pageH = ((DataTable)((GridControl)tableview.Parent).ItemsSource).Rows.Count * 100 + 400;
                            }
                        }

                        CompositeLink compositeLink = new CompositeLink(links);
                        compositeLink.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                        compositeLink.CustomPaperSize = new System.Drawing.Size(pageW, pageH);
                        compositeLink.CreateDocument(false);
                        compositeLink.CreatePageForEachLink();

                        XlsxExportOptionsEx option = new XlsxExportOptionsEx();
                        option.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        option.ExportType = DevExpress.Export.ExportType.WYSIWYG;

                        compositeLink.ExportToXlsx(savefile.FileName, option);
                        Messages.ShowOkMsgBox();
                    }                                   
                }   
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }                                 
        }

        /// <summary>
        /// 조회버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                searchclick();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        public void searchclick()
        {
            try
            {
                if(SDate.DateTime >= EDate.DateTime)
                {
                    Messages.ShowInfoMsgBox("시작기간이 종료기간보다 크거나 같을수 없습니다.");
                }
                else
                {
                    Hashtable conditions = new Hashtable();
                    conditions.Add("SDT", SDate.DateTime.ToString("yyyyMMddHHmm"));
                    conditions.Add("EDT", EDate.DateTime.ToString("yyyyMMddHHmm"));
                    conditions.Add("GRP_CD", cbteam.EditValue);

                    gdList.ItemsSource = work.Select_WorkRptList(conditions);
                }
            }
            catch(Exception ex)
            {     
                throw ex;
            }
        }
        #endregion
    }
}
