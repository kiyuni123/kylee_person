﻿using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WorkJournal.Form.uc;
using WorkJournal.Work;

namespace WorkJournal.Form.Popup
{
    /// <summary>
    /// PopupWorkMng.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupWorkMng:Window
    {
        WorkJournalWork work = new WorkJournalWork();
        DataRowView selectdrv;
        ucWorkJournal uc;

        public PopupWorkMng(object pform)
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            Loaded += PopupWorkMng_NewLoaded;
            uc = (ucWorkJournal)pform;
        }

        public PopupWorkMng(DataRowView drv, object pform)
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            selectdrv = drv;
            Loaded += PopupWorkMng_RowLoaded;
            uc = (ucWorkJournal)pform;

        }

        private void PopupWorkMng_RowLoaded(object sender, RoutedEventArgs e)
        {
            InitializeRowPage();
            InitializeRowEvent();
        }

        private void PopupWorkMng_NewLoaded(object sender, RoutedEventArgs e)
        {
            InitializeNewPage();
            InitializeNewEvent();
        }

        #region 함수
        /// <summary>
        /// 이벤트 선언부 new
        /// </summary>
        private void InitializeNewEvent()
        {
            btnSave.Click += BtnSave_NewClick;
            btnClose.Click += BtnClose_NewClick;
            this.Closed += PopupWorkMng_Closed;
        }

        private void PopupWorkMng_Closed(object sender, EventArgs e)
        {
            uc.EDate.DateTime = DateTime.Now;
            uc.searchclick();
        }

        /// <summary>
        /// 페이지 바인딩 new
        /// </summary>
        private void InitializeNewPage()
        {
            try
            {
                //팀바인딩
                cbteam.ItemsSource = work.Select_GRPList(null);
                cbteam.Text = Logs.strLogin_DESC;

                //No 바인딩
                DataTable temp = new DataTable();
                temp = work.Select_WorkORD(null);
                spinNo.Text = temp.Rows[0]["ORD"].ToString();

                //일시 바인딩
                Date.DateTime = DateTime.Now;

                btnDel.Visibility = Visibility.Collapsed;
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 이벤트 선언부 row
        /// </summary>
        private void InitializeRowEvent()
        {
            btnSave.Click += BtnSave_RowClick;
            btnClose.Click += BtnClose_RowClick;
            btnDel.Click += BtnDel_RowClick;
            this.Closed += PopupWorkMng_Closed;
        }

        /// <summary>
        /// 페이지 바인딩 row
        /// </summary>
        private void InitializeRowPage()
        {
            try
            {   
                //팀바인딩
                cbteam.ItemsSource = work.Select_GRPList(null);
                cbteam.Text = selectdrv["GRP_DESC"].ToString();

                //No 바인딩
                DataTable temp = new DataTable();
                spinNo.Text = selectdrv["ORD"].ToString();

                //일시 바인딩
                CultureInfo provider = CultureInfo.InvariantCulture;
                Date.DateTime = Convert.ToDateTime(selectdrv["WORK_DT"].ToString());

                txtContents.Text = selectdrv["CONTENTS"].ToString();
                txtETC.Text = selectdrv["ETC"].ToString();

                btnDel.Visibility = Visibility.Visible;
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 이벤트
        /// <summary>
        /// 닫기버튼 new ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_NewClick(object sender, RoutedEventArgs e)
        {
            this.Close();            
        }

        /// <summary>
        /// 저장버튼 new ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_NewClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if(cbteam.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조는 입력 필수 항목입니다.");
                    return;
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("GRP_CD", cbteam.EditValue);
                conditions.Add("ORD", spinNo.Text);
                conditions.Add("DT", Date.DateTime.ToString("yyyyMMddHHmm"));
                conditions.Add("CONTENTS", txtContents.Text);
                conditions.Add("ETC", txtETC.Text);
                conditions.Add("REG_ID", Logs.strLogin_ID);
                work.Insert_WorkRpt(conditions);

                Messages.ShowOkMsgBox();

                this.Close();
                
                //uc.searchclick();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 닫기버튼 row ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_RowClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 삭제버튼 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDel_RowClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if(Messages.ShowYesNoMsgBox("해당 업무를 삭제하시겠습니까?") == MessageBoxResult.Yes)
                {
                    Hashtable conditions = new Hashtable();
                    conditions.Add("REG_ID", Logs.strLogin_ID);
                    conditions.Add("SEQ", selectdrv["SEQ"].ToString());
                    work.Delete_WorkRpt(conditions);

                    this.Close();
                }
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 저장버튼 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_RowClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if(cbteam.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조는 입력 필수 항목입니다.");
                    return;
                }

                Hashtable condition = new Hashtable();
                condition.Add("ORD", spinNo.Text);
                condition.Add("DT", Date.DateTime.ToString("yyyyMMddHHmm"));
                condition.Add("CONTENTS", txtContents.Text);
                condition.Add("ETC", txtETC.Text);
                condition.Add("REG_ID", Logs.strLogin_ID);
                condition.Add("SEQ", selectdrv["SEQ"].ToString());
                work.Update_WorkRpt(condition);

                Messages.ShowOkMsgBox();

                this.Close();

                //uc.searchclick();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }

        }
        #endregion
    }
}
