﻿using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WorkJournal.Form.uc;
using WorkJournal.Work;

namespace WorkJournal.Form.Popup
{
    /// <summary>
    /// PopupUserMng.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupUserMng:Window
    {
        TeamManageWork work = new TeamManageWork();
        WorkJournalWork cwork = new WorkJournalWork();
        DataRowView selectdrv;
        string strTeam;

        ucTeamManage uc;

        public PopupUserMng(object pform, string StrTeam)
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            strTeam = StrTeam;
            Loaded += PopupUserMng_NewLoaded;
            uc = (ucTeamManage)pform;
        }

        public PopupUserMng(DataRowView drv, object pform)
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            selectdrv = drv;
            Loaded += PopupUserMng_RowLoaded;
            uc = (ucTeamManage)pform;
        }
        #region New 등록

        #region 이벤트
        private void PopupUserMng_NewLoaded(object sender, RoutedEventArgs e)
        {
            InitializeNewPage();
            InitializeNewEvent();
        }

        private void BtnSave_NewClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if(cbteam.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조는 입력 필수 항목입니다.");
                    return;
                }
                if(txtDEP.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("부서는 입력 필수 항목입니다.");
                    txtDEP.Focus();
                    return;
                }
                if(txtNM.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("이름은 입력 필수 항목입니다.");
                    txtNM.Focus();
                    return;
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("GRP_CD", cbteam.EditValue);
                conditions.Add("DEPT_NM", txtDEP.Text);
                conditions.Add("NM", txtNM.Text);
                conditions.Add("ETC", txtETC.Text);
                conditions.Add("REG_ID", Logs.strLogin_ID);

                work.Insert_GRPDTLINFO(conditions);

                Messages.ShowOkMsgBox();

                this.Close();

                uc.TeamRowSelect();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void BtnClose_NewClick(object sender, RoutedEventArgs e)
        {
            try
            {
                uc.TeamRowSelect();
                this.Close();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 함수
        /// <summary>
        /// 페이지 초기화
        /// </summary>
        private void InitializeNewPage()
        {
            try
            {
                cbteam.ItemsSource = cwork.Select_GRPList(null);

                btnDel.Visibility = Visibility.Collapsed;

                if(!strTeam.Equals(string.Empty))
                {
                    cbteam.EditValue = strTeam;
                }
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 이벤트 선언
        /// </summary>
        private void InitializeNewEvent()
        {
            try
            {
                btnClose.Click += BtnClose_NewClick;
                btnSave.Click += BtnSave_NewClick;
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #endregion

        #region Row 수정

        #region 이벤트

        /// <summary>
        /// 로드 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopupUserMng_RowLoaded(object sender, RoutedEventArgs e)
        {
            InitializeRowPage();
            InitializeRowEvent();
        }

        /// <summary>
        /// 저장버튼 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_RowClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if(cbteam.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조는 입력 필수 항목입니다.");
                    return;
                }
                if(txtDEP.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("부서는 입력 필수 항목입니다.");
                    txtDEP.Focus();
                    return;
                }
                if(txtNM.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("이름은 입력 필수 항목입니다.");
                    txtNM.Focus();
                    return;
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("GRP_CD", cbteam.EditValue);
                conditions.Add("DEPT_NM", txtDEP.Text);
                conditions.Add("NM", txtNM.Text);
                conditions.Add("ETC", txtETC.Text);
                conditions.Add("REG_ID", Logs.strLogin_ID);
                conditions.Add("DTL_CD", selectdrv["DTL_CD"].ToString());

                work.Update_GRPDTLINFO(conditions);

                Messages.ShowOkMsgBox();
                uc.TeamRowSelect();
                this.Close();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 닫기버튼 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_RowClick(object sender, RoutedEventArgs e)
        {
            try
            {
                uc.TeamRowSelect();
                this.Close();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 삭제버튼 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDel_RowClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("DTL_CD", selectdrv["DTL_CD"].ToString());
                conditions.Add("REG_ID", Logs.strLogin_ID);
                work.Delete_GRPDTLINFO(conditions);

                Messages.ShowOkMsgBox();
                uc.TeamRowSelect();
                this.Close();
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 함수
        /// <summary>
        /// 페이지 초기화
        /// </summary>
        private void InitializeRowPage()
        {
            try
            {   
                cbteam.ItemsSource = cwork.Select_GRPList(null);

                cbteam.EditValue = selectdrv["GRP_CD"].ToString();
                txtDEP.Text = selectdrv["DEPT_NM"].ToString();
                txtETC.Text = selectdrv["ETC"].ToString();
                txtNM.Text = selectdrv["NM"].ToString();

                btnDel.Visibility = Visibility.Visible;
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 이벤트 선언
        /// </summary>
        private void InitializeRowEvent()
        {
            try
            {
                btnClose.Click += BtnClose_RowClick;
                btnDel.Click += BtnDel_RowClick;
                btnSave.Click += BtnSave_RowClick;
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #endregion
        
    }
}
