﻿using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WorkJournal.Form.uc;
using WorkJournal.Work;

namespace WorkJournal.Form.Popup
{
    /// <summary>
    /// PopupTeamMng.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupTeamMng:Window
    {
        TeamManageWork work = new TeamManageWork();
        DataRowView selectdrv;
        ucTeamManage uc;

        public PopupTeamMng(object pform)
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            Loaded += PopupTeamMng_NewLoaded;
            uc = (ucTeamManage)pform;
        }
                 
        public PopupTeamMng(DataRowView drv, object pform)
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            selectdrv = drv;
            Loaded += PopupTeamMng_RowLoaded;
            uc = (ucTeamManage)pform;
        }

        #region New 등록

        #region 이벤트
        private void PopupTeamMng_NewLoaded(object sender, RoutedEventArgs e)
        {
            InitializeNewPage();
            InitializeNewEvent();
        }

        private void BtnSave_NewClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if(txtTeamID.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조 아이디는 입력 필수 항목입니다.");
                    txtTeamID.Focus();
                    return;
                }
                if(txtTeamPWD.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조 비밀번호는 입력 필수 항목입니다.");
                    txtTeamPWD.Focus();
                    return;
                }
                if(txtTeamDESC.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조 설명은 입력 필수 항목입니다.");
                    txtTeamDESC.Focus();
                    return;
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("GRP_ID", txtTeamID.Text);
                conditions.Add("GRP_PWD", txtTeamPWD.Text);
                conditions.Add("GRP_DESC", txtTeamDESC.Text);
                conditions.Add("REG_ID", Logs.strLogin_ID);

                DataTable dtChk = new DataTable();
                dtChk = work.Select_GRPIDChk(conditions);

                if(dtChk.Rows.Count == 0)
                {
                    work.Insert_GRPMSTINFO(conditions);

                    Messages.ShowOkMsgBox();

                    this.Close();

                    uc.InitializePage();
                }
                else
                {
                    Messages.ShowInfoMsgBox("해당 그룹 아이디가 존재합니다.");
                }
            }
            catch(Exception ex)
            {     
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void BtnClose_NewClick(object sender, RoutedEventArgs e)
        {
            try
            {
                uc.InitializePage();
                this.Close();
            }
            catch(Exception ex)
            {   
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 함수
        /// <summary>
        /// 페이지 초기화
        /// </summary>
        private void InitializeNewPage()
        {
            try
            {
                btnDel.Visibility = Visibility.Collapsed;
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 이벤트 선언
        /// </summary>
        private void InitializeNewEvent()
        {
            try
            {
                btnClose.Click += BtnClose_NewClick;
                btnSave.Click += BtnSave_NewClick;
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #endregion
                    
        #region Row 수정

        #region 이벤트
        /// <summary>
        /// 로드 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopupTeamMng_RowLoaded(object sender, RoutedEventArgs e)
        {
            InitializeRowPage();
            InitializeRowEvent();
        }

        /// <summary>
        /// 저장버튼 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_RowClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if(txtTeamID.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조 아이디는 입력 필수 항목입니다.");
                    txtTeamID.Focus();
                    return;
                }
                if(txtTeamPWD.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조 비밀번호는 입력 필수 항목입니다.");
                    txtTeamPWD.Focus();
                    return;
                }
                if(txtTeamDESC.Text.Equals(""))
                {
                    Messages.ShowInfoMsgBox("조 설명은 입력 필수 항목입니다.");
                    txtTeamDESC.Focus();
                    return;
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("GRP_ID", txtTeamID.Text);
                conditions.Add("GRP_PWD", txtTeamPWD.Text);
                conditions.Add("GRP_DESC", txtTeamDESC.Text);
                conditions.Add("REG_ID", Logs.strLogin_ID);
                conditions.Add("GRP_CD", selectdrv["GRP_CD"].ToString());

                //기존 아이디와 동일한 변경
                if(selectdrv["GRP_ID"].ToString().Equals(txtTeamID.Text))
                { 
                    work.Update_GRPMSTINFO(conditions);

                    Messages.ShowOkMsgBox();
                    uc.InitializePage();
                    this.Close();
                }
                //아이디 변경
                else
                {
                    DataTable dtChk = new DataTable();
                    dtChk = work.Select_GRPIDChk(conditions);

                    if(dtChk.Rows.Count == 0)
                    {
                        work.Update_GRPMSTINFO(conditions);

                        Messages.ShowOkMsgBox();
                        uc.InitializePage();
                        this.Close();
                    }
                    else
                    {
                        Messages.ShowInfoMsgBox("해당 그룹 아이디가 존재합니다.");
                    }
                }
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 닫기버튼 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_RowClick(object sender, RoutedEventArgs e)
        {
            try
            {
                uc.InitializePage();
                this.Close();
            }
            catch(Exception ex)
            {   
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 삭제버튼 row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDel_RowClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("GRP_CD", selectdrv["GRP_CD"].ToString());
                conditions.Add("REG_ID", Logs.strLogin_ID);
                work.Delete_GRPMSTINFO(conditions);

                Messages.ShowOkMsgBox();
                uc.InitializePage();
                this.Close();
            }
            catch(Exception ex)
            {                                 
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 함수
        /// <summary>
        /// 페이지 초기화
        /// </summary>
        private void InitializeRowPage()
        {
            try
            {
                txtTeamID.Text = selectdrv["GRP_ID"].ToString();
                txtTeamPWD.Text = selectdrv["GRP_PWD"].ToString();
                txtTeamDESC.Text = selectdrv["GRP_DESC"].ToString();

                btnDel.Visibility = Visibility.Visible;
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 이벤트 선언
        /// </summary>
        private void InitializeRowEvent()
        {
            try
            {
                btnClose.Click += BtnClose_RowClick;
                btnDel.Click += BtnDel_RowClick;
                btnSave.Click += BtnSave_RowClick;
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #endregion
    }
}
