﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace HMIServer
{
    public class HMIPacket
    {
        public string strpacket { get; set; } = string.Empty;   //패킷원문
        public DateTime? dttime { get; set; }                   //계측날짜
        public DateTime? dtRX { get; set; }                     //수신날짜 //수신받을때 HMISocket에서 작성예정?
        public decimal? dfl { get; set; }                       //유량값
        public decimal? dprs { get; set; }                      //수압값
        public decimal? dlev { get; set; }                      //수위값
        public decimal? detc { get; set; }                      //기타값
        public int intRN { get; set; } = 0;                     //최근데이터 판별을 위한 Number
        public bool bpacketStat { get; set; }                   //패킷상태 (정상 : true, 비정상 : false)

        public HMIPacket(string _strpacket)
        {
            strpacket = _strpacket;
            packetparsing();
        }

        string strdateFormat = "yyyyMMddHHmmss";
        CultureInfo koKR = new CultureInfo("ko-KR");

        public void packetparsing()
        {
            try
            {
                if(strpacket.Length == 68)
                {
                    //길이대로 자르기 우선
                    //               00000000001111 1111112222 2222223333 3333334444 4444445555 55555566666666 십자리
                    //               01234567890123 4567890123 4567890123 4567890123 4567890123 45678901234567 일자리
                    //추측 packet -> yyyyMMddHHmmss 0000000000 1111111111 2222222222 3333333333 yyyyMMddHHmmss
                    //               시간14          유량10    수압10     수위10     기타10     수집시간14
                    //길이 54

                    //계측날짜
                    {
                        if (DateTime.TryParseExact(strpacket.Substring(0, 14), strdateFormat, koKR, DateTimeStyles.None, out DateTime temp))
                            dttime = temp;
                        else
                            dttime = null;
                    }

                    //계측 유량항목
                    {
                        if (decimal.TryParse(strpacket.Substring(14, 10), out decimal temp))
                            dfl = temp;
                        else
                            dfl = null;
                    }

                    //계측 수압항목
                    {
                        if (decimal.TryParse(strpacket.Substring(24, 10), out decimal temp))
                            dprs = temp;
                    }

                    //계측 수위항목
                    {
                        if (decimal.TryParse(strpacket.Substring(34, 10), out decimal temp))
                            dlev = temp;
                        else
                            dlev = null;
                    }

                    //계측 기타항목
                    {
                        if (decimal.TryParse(strpacket.Substring(44, 10), out decimal temp))
                            detc = temp;
                        else
                            detc = null;
                    }

                    //수집날짜
                    {
                        if (DateTime.TryParseExact(strpacket.Substring(54, 14), strdateFormat, koKR, DateTimeStyles.None, out DateTime temp))
                            dtRX = temp;
                        else
                            dtRX = null;
                    }

                    //성공시 True
                    bpacketStat = true;
                }
                else
                {
                    bpacketStat = false;
                    dttime = null;
                    dtRX = null;
                    dfl = null;
                    dprs = null;
                    dlev = null;
                    detc = null;
                    intRN = 0;
                }
            }
            catch (Exception ex)
            {
                bpacketStat = false;
                dttime = null;
                dtRX = null;
                dfl = null;
                dprs = null;
                dlev = null;
                detc = null;
                intRN = 0;
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
