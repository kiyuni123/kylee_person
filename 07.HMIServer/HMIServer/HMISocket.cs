﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace HMIServer
{
    public class HMISocket
    {
        MainWindow owner;

        /// <summary>
        /// Client가 붙는 로직
        /// 1번 클라이언트가 붙으면 끊어지기 전까지 유지
        /// 2번 클라이언트가 붙으려 시도하면 대기
        /// 1번 클라이언트가 끊어지면 2번 클라이언트 자동연결
        /// </summary>
        TcpListener server;
        int intport;

        Thread serverthread;

        public HMISocket(MainWindow _owner,int _intport)
        {
            owner = _owner;
            intport = _intport;

            serverthread = new Thread(new ThreadStart(threadfx)) { IsBackground = true };
            serverthread.Start();
        }

        Byte[] bytes = new Byte[256]; //길이 256
        String data = null;
        TcpClient client;
        NetworkStream stream;
        private void threadfx()
        {
            IPEndPoint serverEP = new IPEndPoint(IPAddress.Any, intport);
            server = new TcpListener(serverEP);
            server.Start();

            //client.Client.RemoteEndPoint

            while (true)
            {
                client = server.AcceptTcpClient();
                stream = client.GetStream();
                Console.WriteLine("connect: {0}", client.Client.RemoteEndPoint.ToString()); //보낸내용 콘솔

                owner.state(true, client.Client.RemoteEndPoint.ToString());

                int i;
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    data = Encoding.Default.GetString(bytes, 0, i) + DateTime.Now.ToString("yyyyMMddHHmmss"); //받은내용 변환
                    owner.csvwrite(data); //받은내용(패킷) csv 저장

                    #region 패킷 Parsing
                    //패킷 분석 필요
                    HMIPacket hmiPacket = new HMIPacket(data);
                    
                    //정상일경우 ACK
                    if (hmiPacket.bpacketStat)
                    {
                        sendmsg("OK");
                        owner.DataList.Add(hmiPacket);
                    }
                    //비정상일경우 ACK
                    else
                    {
                        sendmsg("NG");
                    }
                    #endregion

                    Console.WriteLine("Received: {0} packeystat: {1}", data, hmiPacket.bpacketStat.ToString()); //받은내용 콘솔 

                    owner.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action(delegate ()
                        {
                            owner.csvread();
                        }));
                }

                client.Close();
                owner.state(false, "");
            }
        }

        public void sendmsg(string strmsg)
        {
            try
            {
                if (stream == null) return;

                byte[] msg = Encoding.Default.GetBytes(strmsg);   //Client 보낼 msg
                stream.Write(msg, 0, msg.Length);
                Console.WriteLine("Send: {0}", strmsg); //보낸내용 콘솔
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
