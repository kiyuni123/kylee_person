﻿using DevExpress.Xpf.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Linq;
using System.Collections;
using DevExpress.Xpf.Grid;

namespace HMIServer
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        //string strcsvpath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop).ToString(), "HMIServer_data.csv");  //바탕화면
        string strcsvpath = Path.Combine(Environment.CurrentDirectory, "packet_data.log");  //실행경로
        FileStream fs;
        HMISocket socket;
        public List<HMIPacket> DataList = new List<HMIPacket>();
        public List<HMIPacket> RawDataList = new List<HMIPacket>();

        public MainWindow()
        {
            InitializeComponent();

            //테마적용
            ThemeApply();

            btnXSignClose.Click += BtnXSignClose_Click;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;

            //로드 및 재로드
            Load();

            socket = new HMISocket(this, 9090);
            
            //기본 기능
            Inputstr.PreviewKeyDown += Inputstr_PreviewKeyDown;
        }

        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    this.Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    this.WindowState = WindowState.Normal;
                }
                this.DragMove();
            }
        }

        private void BtnXSignClose_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private void ThemeApply()
        {
            Theme themeNavy = new Theme("GTINavyTheme");
            themeNavy.AssemblyName = "DevExpress.Xpf.Themes.GTINavyTheme.v19.1";
            Theme.RegisterTheme(themeNavy);
            ThemeManager.SetThemeName(this, "GTINavyTheme");
        }

        private void Load()
        {
            try
            {
                csvread();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void csvread()
        {
            fs = new FileStream(strcsvpath, FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);

            try
            {
                string strLine = string.Empty;
                RawDataList.Clear();
                DataList.Clear();

                while (!sr.EndOfStream)
                {
                    strLine = string.Empty;
                    strLine = sr.ReadLine();
                    readstr.AppendText(strLine + Environment.NewLine);
                    HMIPacket hmipacket = new HMIPacket(strLine);
                    
                    if (hmipacket.bpacketStat)
                    {
                        hmipacket.intRN = RawDataList.Select(x => x.dttime == hmipacket.dttime).Count();
                        RawDataList.Add(hmipacket);
                    }
                }

                //TextBox 스크롤 조절
                readstr.ScrollToEnd();

                //Grid조건
                //중복데이터는 수집시간 최근데이터가 인정되는 데이터

                var vartemp = RawDataList.OrderBy(x => x.dttime).GroupBy(x => x.dttime).Select(y => y.Last());

                HMIPacket LastPackpek = null;
                int intcnt = 0;

                foreach (var item in vartemp)
                {
                    DataList.Add(item);

                    if (vartemp.Count()-1 == intcnt)
                        LastPackpek = item;

                    intcnt++;
                }

                grid.ItemsSource = null;
                grid.ItemsSource = DataList;
                (grid.View as TableView).ScrollIntoView(DataList.Count()-1);

                chart.DataSource = null;
                chart.DataSource = DataList;

                Hashtable htvalue = new Hashtable();
                htvalue.Add("PACTEK", LastPackpek);

                if(DataList.Count > 0)
                {
                    htvalue.Add("FL_MIN", DataList.Min(x => x.dfl).Value);
                    htvalue.Add("FL_MAX", DataList.Max(x => x.dfl).Value);
                    htvalue.Add("PRS_MIN", DataList.Min(x => x.dprs).Value);
                    htvalue.Add("PRS_MAX", DataList.Max(x => x.dprs).Value);
                    htvalue.Add("LEV_MIN", DataList.Min(x => x.dlev).Value);
                    htvalue.Add("LEV_MAX", DataList.Max(x => x.dlev).Value);
                    htvalue.Add("ETC_MIN", DataList.Min(x => x.detc).Value);
                    htvalue.Add("ETC_MAX", DataList.Max(x => x.detc).Value);
                }

                LabelValueBinding(htvalue);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sr.Close();
                fs.Close();
            }
        }


        private void LabelValueBinding(Hashtable htvalue)
        {
            try
            {
                #region Current
                if (htvalue.Contains("PACTEK"))
                {
                    lbflCurval.Content = (htvalue["PACTEK"] as HMIPacket).dfl;
                    lbprsCurval.Content = (htvalue["PACTEK"] as HMIPacket).dprs;
                    lblevCurval.Content = (htvalue["PACTEK"] as HMIPacket).dlev;
                    lbetcCurval.Content = (htvalue["PACTEK"] as HMIPacket).detc;

                    if (DateTime.TryParse((htvalue["PACTEK"] as HMIPacket).dttime.ToString(), out DateTime temp))
                        lbTime.Content = temp.ToString("yyyy년 MM월 dd일 HH시 mm분 ss초");
                }
                else
                {
                    lbflCurval.Content = "-";
                    lbprsCurval.Content = "-";
                    lblevCurval.Content = "-";
                    lbetcCurval.Content = "-";

                    lbTime.Content = "수집전";
                } 
                #endregion

                #region FL
                if (htvalue.Contains("FL_MIN"))
                    lbflminval.Content = htvalue["FL_MIN"];
                else
                    lbflminval.Content = "-";

                if (htvalue.Contains("FL_MAX"))
                    lbflmaxval.Content = htvalue["FL_MAX"];
                else
                    lbflmaxval.Content = "-";
                #endregion

                #region PRS
                if (htvalue.Contains("PRS_MIN"))
                    lbprsminval.Content = htvalue["PRS_MIN"];
                else
                    lbprsminval.Content = "-";

                if (htvalue.Contains("PRS_MAX"))
                    lbprsmaxval.Content = htvalue["PRS_MAX"];
                else
                    lbprsmaxval.Content = "-";
                #endregion

                #region LEV
                if (htvalue.Contains("LEV_MIN"))
                    lblevminval.Content = htvalue["LEV_MIN"];
                else
                    lblevminval.Content = "-";

                if (htvalue.Contains("LEV_MAX"))
                    lblevmaxval.Content = htvalue["LEV_MAX"];
                else
                    lblevmaxval.Content = "-";
                #endregion

                #region ETC
                if (htvalue.Contains("ETC_MIN"))
                    lbetcminval.Content = htvalue["ETC_MIN"];
                else
                    lbetcminval.Content = "-";

                if (htvalue.Contains("ETC_MAX"))
                    lbetcmaxval.Content = htvalue["ETC_MAX"];
                else
                    lbetcmaxval.Content = "-"; 
                #endregion
            }
            catch (Exception ex)
            {
                lbflCurval.Content = "-";
                lbflminval.Content = "-";
                lbflmaxval.Content = "-";

                lbprsCurval.Content = "-";
                lbprsminval.Content = "-";
                lbprsmaxval.Content = "-";

                lblevCurval.Content = "-";
                lblevminval.Content = "-";
                lblevmaxval.Content = "-";

                lbetcCurval.Content = "-";
                lbetcminval.Content = "-";
                lbetcmaxval.Content = "-";
            }
        }

        private void Inputstr_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            string strText = string.Empty;
            if(e.Key == Key.Enter)
            {
                try
                {
                    socket.sendmsg(Inputstr.Text);
                    csvwrite(Inputstr.Text);
                    Inputstr.Clear();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        public void csvwrite(string strtext)
        {
            StreamWriter sw = new StreamWriter(strcsvpath, true);
            sw.WriteLine(strtext);
            sw.Close();

            //this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
            //    new Action(delegate ()
            //    {
            //        readstr.AppendText(strtext + Environment.NewLine);
            //        readstr.ScrollToEnd();
            //    }));
        }

        public void state(bool bstat, string stripport)
        {
            if (bstat)
                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                    new Action(delegate ()
                    {
                        statellipe.Fill = Brushes.LightGreen;
                        lbipport.Content = stripport;
                    }));
            
            else
                this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                    new Action(delegate ()
                    {
                        statellipe.Fill = Brushes.OrangeRed;
                        lbipport.Content = string.Empty;
                    }));
        }
    }
}
