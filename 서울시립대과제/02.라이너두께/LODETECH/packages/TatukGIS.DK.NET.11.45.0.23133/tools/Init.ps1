param($installPath, $toolsPath, $package, $project)



if ( $package[0].Id.IndexOf(".Trial") -GT 0 )
{
  try {
    Get-ItemProperty -Path HKCU:\Software\TatukGIS\DK11.NET.Trial -Name License -ErrorAction Stop
  }
  catch {
    $project.DTE.ItemOperations.Navigate('https://www.tatukgis.com/Landing/DK/NET/NuGet/Trial')
  }
}
else
{
  try {
    Get-ItemProperty -Path HKCU:\Software\TatukGIS\DK11.NET -Name License -ErrorAction Stop
  }
  catch {
    $project.DTE.ItemOperations.Navigate('https://www.tatukgis.com/Landing/DK/NET/NuGet/Retail')
  }
}
