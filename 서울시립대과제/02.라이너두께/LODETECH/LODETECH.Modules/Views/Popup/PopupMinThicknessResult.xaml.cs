﻿using DevExpress.Xpf.Editors;
using GTIFramework.Common.MessageBox;
using LODETECH.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LODETECH.Modules.Views.Popup
{
    /// <summary>
    /// PopupMinThicknessResult.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupMinThicknessResult : Window
    {
        public PopupMinThicknessResult()
        {
            InitializeComponent();
            Load();
            
        }

        private void Load()
        {
            cbGBN.ItemsSource = CreateData.MinThicknessReviewItemcreate();
            InitializeEvent();
            InitializeData();
        }

        private void InitializeEvent()
        {
            btnClose.Click += BtnClose_Click;
            btnXSignClose.Click += BtnClose_Click;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;
            cbGBN.EditValueChanged += CbGBN_EditValueChanged;
        }

        private void CbGBN_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            try
            {
                switch ((sender as ComboBoxEdit).EditValue.ToString())
                {
                    case "000001":
                        scrollViewer.ScrollToVerticalOffset(0);
                        break;
                    case "000002":
                        scrollViewer.ScrollToVerticalOffset(300);
                        break;
                    case "000003":
                        scrollViewer.ScrollToVerticalOffset(510);
                        break;
                    case "000004":
                        scrollViewer.ScrollToVerticalOffset(1020);
                        break;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void InitializeData()
        {
            try
            {
                val1_1.Content = AnalData.class1_1.ToString();
                val1_2.Content = AnalData.class1_2.ToString();
                val1_3.Content = AnalData.class1_3.ToString();
                val1_4.Content = AnalData.class1_4.ToString();
                val1_5.Content = AnalData.class1_5.ToString();
                val1_6.Content = AnalData.class1_6.ToString();

                val2_1.Content = AnalData.class2_1.ToString();
                val2_2.Content = AnalData.class2_2.ToString();
                val2_3.Content = AnalData.class2_3.ToString();

                val3_1.Content = AnalData.class3_1.ToString();
                val3_2.Content = AnalData.class3_2.ToString();
                val3_3.Content = AnalData.class3_3.ToString();
                val3_4.Content = AnalData.class3_4.ToString();
                val3_5.Content = AnalData.class3_5.ToString();
                val3_6.Content = AnalData.class3_6.ToString();
                val3_7.Content = AnalData.class3_7.ToString();
                
                val4_1.Content = AnalData.class4_1.ToString();
                val4_2.Content = AnalData.class4_2.ToString();
                val4_3.Content = AnalData.class4_3.ToString();
                val4_4.Content = AnalData.class4_4.ToString();
                val4_5.Content = AnalData.class4_5.ToString();
                val4_6.Content = AnalData.class4_6.ToString();
                val4_7.Content = AnalData.class4_7.ToString();
                val4_8.Content = AnalData.class4_8.ToString();
                val4_9.Content = AnalData.class4_9.ToString();
                val4_10.Content = AnalData.class4_10.ToString();
                val4_11.Content = AnalData.class4_11.ToString();
                val4_12.Content = AnalData.class4_12.ToString();
                val4_13.Content = AnalData.class4_13.ToString();
                val4_14.Content = AnalData.class4_14.ToString();
                val4_15.Content = AnalData.class4_15.ToString();
                val4_16.Content = AnalData.class4_16.ToString();
                val4_17.Content = AnalData.class4_17.ToString();
                val4_18.Content = AnalData.class4_18.ToString();

                #region Class1 체크
                //IF(liner_analval25 >= class1_3,"OK","FALSE")
                if (Convert.ToDecimal(AnalData.liner_analval25) >= Convert.ToDecimal(AnalData.class1_3))
                    chk1_3.Content = "OK";
                else
                    chk1_3.Content = "FALSE";

                //IF(liner_analval25 >= class1_5 + class1_4, "OK", "FALSE")
                if (Convert.ToDecimal(AnalData.liner_analval25) >= Convert.ToDecimal(AnalData.class1_5 + AnalData.class1_4))
                    chk1_5.Content = "OK";
                else
                    chk1_5.Content = "FALSE"; 
                #endregion

                #region Class2 체크
                //IF(class2_2<=class2_3,"OK","FALSE")
                if (Convert.ToDecimal(AnalData.class2_2) <= Convert.ToDecimal(AnalData.class2_3))
                    chk2_2.Content = "OK";
                else
                    chk2_2.Content = "FALSE"; 
                #endregion

                #region Class3 체크
                //IF(design_analval4 = "000001","TRUE","FALSE")  #000001 = 강성(Rigid)
                //IF(design_analval4 = "000001", "FALSE", "TRUE")  #000001 = 강성(Rigid)
                if (AnalData.design_analval4.Equals("000001"))
                {
                    chk3_3.Content = "TRUE";
                    chk3_4.Content = "FALSE";
                }
                else
                {
                    chk3_4.Content = "FALSE";
                    chk3_4.Content = "TRUE";
                }

                //IF(liner_analval13 >= class3_7,"OK","FALSE")
                if (Convert.ToDecimal(AnalData.liner_analval13) >= Convert.ToDecimal(AnalData.class3_7))
                    chk3_7.Content = "OK";
                else
                    chk3_7.Content = "FALSE";
                #endregion


                //IF(class4_11 >= design_analval9,"OK","FALSE")
                if (Convert.ToDecimal(AnalData.class4_11) >= Convert.ToDecimal(AnalData.design_analval9))
                    chk4_11.Content = "OK";
                else
                    chk4_11.Content = "FALSE";

                //IF(class4_12 >= class4_11,"OK","FALSE")
                if (Convert.ToDecimal(AnalData.class4_12) <= Convert.ToDecimal(AnalData.class4_11))
                    chk4_12.Content = "OK";
                else
                    chk4_12.Content = "FALSE";

                //IF(class4_11<=class4_13,"OK","FALSE")
                if (Convert.ToDecimal(AnalData.class4_13) <= Convert.ToDecimal(AnalData.class4_11))
                    chk4_13.Content = "OK";
                else
                    chk4_13.Content = "FALSE";

                //IF(class4_12<=class4_14,"OK","FALSE")
                if (Convert.ToDecimal(AnalData.class4_14) <= Convert.ToDecimal(AnalData.class4_12))
                    chk4_14.Content = "OK";
                else
                    chk4_14.Content = "FALSE";

                cbGBN.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 윈도우 창 이동 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    this.Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    this.WindowState = WindowState.Normal;
                }
                this.DragMove();
            }
        }

        /// <summary>
        /// 닫기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
