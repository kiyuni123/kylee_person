﻿using DevExpress.Xpf.Editors;
using GTIFramework.Common.MessageBox;
using LODETECH.Models;
using LODETECH.Modules.Views.Popup;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Windows.Controls;

namespace LODETECH.Modules
{
    class UcLinerInputViewModel : BindableBase
    {
        bool bload = true;

        #region 컨트롤
        UcLinerInputView ucLinerInputView;

        TextEdit input1;
        TextEdit input2;
        TextEdit input3;
        TextEdit input4;
        TextEdit input5;
        TextEdit input6;
        TextEdit input7;
        TextEdit input8;
        TextEdit input9;
        TextEdit input10;
        TextEdit input11;
        TextEdit input12;
        TextEdit input13;
        TextEdit input14;
        TextEdit input15;
        TextEdit input16;
        TextEdit input17;
        TextEdit input18;
        TextEdit input19;
        TextEdit input20;
        TextEdit input21;
        TextEdit input22;
        TextEdit input23;
        TextEdit input24;
        TextEdit input25;
        TextEdit anal1;
        TextEdit anal2;
        TextEdit anal3;
        TextEdit anal4;
        TextEdit anal5;
        TextEdit anal6;
        TextEdit anal7;
        TextEdit anal8;
        TextEdit anal9;
        TextEdit anal10;
        TextEdit anal11;
        TextEdit anal12;
        TextEdit anal13;
        TextEdit anal14;
        TextEdit anal15;
        TextEdit anal16;
        TextEdit anal17;
        TextEdit anal18;
        TextEdit anal19;
        TextEdit anal20;
        TextEdit anal21;
        TextEdit anal22;
        TextEdit anal23;
        TextEdit anal24;
        TextEdit anal25;
        #endregion

        /// <summary>
        /// Loaded Command
        /// </summary>
        public DelegateCommand<object> LoadedCommand { get; set; }

        /// <summary>
        /// InitCommand
        /// </summary>
        public DelegateCommand<object> InitCommand { get; set; }

        /// <summary>
        /// AnalCommand
        /// </summary>
        public DelegateCommand<object> AnalCommand { get; set; }

        /// <summary>
        /// EditValueChangedCommand
        /// </summary>
        public DelegateCommand<object> EditValueChangedCommand { get; set; }

        public UcLinerInputViewModel()
        {
            LoadedCommand = new DelegateCommand<object>(OnLoaded);
            InitCommand = new DelegateCommand<object>(InitAction);
            AnalCommand = new DelegateCommand<object>(AnalAction);
            EditValueChangedCommand = new DelegateCommand<object>(EditValueChangedAction);
        }

        /// <summary>
        /// 로드 이벤트
        /// </summary>
        /// <param name="obj"></param>
        private void OnLoaded(object obj)
        {
            if (bload)
            {
                if (obj == null) return;

                var values = (object[])obj;

                try
                {
                    ucLinerInputView = values[0] as UcLinerInputView;

                    input1 = ucLinerInputView.FindName("input1") as TextEdit;
                    input2 = ucLinerInputView.FindName("input2") as TextEdit;
                    input3 = ucLinerInputView.FindName("input3") as TextEdit;
                    input4 = ucLinerInputView.FindName("input4") as TextEdit;
                    input5 = ucLinerInputView.FindName("input5") as TextEdit;
                    input6 = ucLinerInputView.FindName("input6") as TextEdit;
                    input7 = ucLinerInputView.FindName("input7") as TextEdit;
                    input8 = ucLinerInputView.FindName("input8") as TextEdit;
                    input9 = ucLinerInputView.FindName("input9") as TextEdit;
                    input10 = ucLinerInputView.FindName("input10") as TextEdit;
                    input11 = ucLinerInputView.FindName("input11") as TextEdit;
                    input12 = ucLinerInputView.FindName("input12") as TextEdit;
                    input13 = ucLinerInputView.FindName("input13") as TextEdit;
                    input14 = ucLinerInputView.FindName("input14") as TextEdit;
                    input15 = ucLinerInputView.FindName("input15") as TextEdit;
                    input16 = ucLinerInputView.FindName("input16") as TextEdit;
                    input17 = ucLinerInputView.FindName("input17") as TextEdit;
                    input18 = ucLinerInputView.FindName("input18") as TextEdit;
                    input19 = ucLinerInputView.FindName("input19") as TextEdit;
                    input20 = ucLinerInputView.FindName("input20") as TextEdit;
                    input21 = ucLinerInputView.FindName("input21") as TextEdit;
                    input22 = ucLinerInputView.FindName("input22") as TextEdit;
                    input23 = ucLinerInputView.FindName("input23") as TextEdit;
                    input24 = ucLinerInputView.FindName("input24") as TextEdit;
                    input25 = ucLinerInputView.FindName("input25") as TextEdit;
                    anal1 = ucLinerInputView.FindName("anal1") as TextEdit;
                    anal2 = ucLinerInputView.FindName("anal2") as TextEdit;
                    anal3 = ucLinerInputView.FindName("anal3") as TextEdit;
                    anal4 = ucLinerInputView.FindName("anal4") as TextEdit;
                    anal5 = ucLinerInputView.FindName("anal5") as TextEdit;
                    anal6 = ucLinerInputView.FindName("anal6") as TextEdit;
                    anal7 = ucLinerInputView.FindName("anal7") as TextEdit;
                    anal8 = ucLinerInputView.FindName("anal8") as TextEdit;
                    anal9 = ucLinerInputView.FindName("anal9") as TextEdit;
                    anal10 = ucLinerInputView.FindName("anal10") as TextEdit;
                    anal11 = ucLinerInputView.FindName("anal11") as TextEdit;
                    anal12 = ucLinerInputView.FindName("anal12") as TextEdit;
                    anal13 = ucLinerInputView.FindName("anal13") as TextEdit;
                    anal14 = ucLinerInputView.FindName("anal14") as TextEdit;
                    anal15 = ucLinerInputView.FindName("anal15") as TextEdit;
                    anal16 = ucLinerInputView.FindName("anal16") as TextEdit;
                    anal17 = ucLinerInputView.FindName("anal17") as TextEdit;
                    anal18 = ucLinerInputView.FindName("anal18") as TextEdit;
                    anal19 = ucLinerInputView.FindName("anal19") as TextEdit;
                    anal20 = ucLinerInputView.FindName("anal20") as TextEdit;
                    anal21 = ucLinerInputView.FindName("anal21") as TextEdit;
                    anal22 = ucLinerInputView.FindName("anal22") as TextEdit;
                    anal23 = ucLinerInputView.FindName("anal23") as TextEdit;
                    anal24 = ucLinerInputView.FindName("anal24") as TextEdit;
                    anal25 = ucLinerInputView.FindName("anal25") as TextEdit;

                    //값이 있을경우 바인딩
                    input1.EditValue = AnalData.liner_inputval1;
                    input2.EditValue = AnalData.liner_inputval2;
                    input3.EditValue = AnalData.liner_inputval3;
                    input4.EditValue = AnalData.liner_inputval4;
                    input5.EditValue = AnalData.liner_inputval5;
                    input6.EditValue = AnalData.liner_inputval6;
                    input7.EditValue = AnalData.liner_inputval7;
                    input8.EditValue = AnalData.liner_inputval8;
                    input9.EditValue = AnalData.liner_inputval9;
                    input10.EditValue = AnalData.liner_inputval10;
                    input11.EditValue = AnalData.liner_inputval11;
                    input12.EditValue = AnalData.liner_inputval12;
                    input13.EditValue = AnalData.liner_inputval13;
                    input14.EditValue = AnalData.liner_inputval14;
                    input15.EditValue = AnalData.liner_inputval15;
                    input16.EditValue = AnalData.liner_inputval16;
                    input17.EditValue = AnalData.liner_inputval17;
                    input18.EditValue = AnalData.liner_inputval18;
                    input19.EditValue = AnalData.liner_inputval19;
                    input20.EditValue = AnalData.liner_inputval20;
                    input21.EditValue = AnalData.liner_inputval21;
                    input22.EditValue = AnalData.liner_inputval22;
                    input23.EditValue = AnalData.liner_inputval23;
                    input24.EditValue = AnalData.liner_inputval24;
                    input25.EditValue = AnalData.liner_inputval25;
                    anal1.EditValue = AnalData.liner_analval1;
                    anal2.EditValue = AnalData.liner_analval2;
                    anal3.EditValue = AnalData.liner_analval3;
                    anal4.EditValue = AnalData.liner_analval4;
                    anal5.EditValue = AnalData.liner_analval5;
                    anal6.EditValue = AnalData.liner_analval6;
                    anal7.EditValue = AnalData.liner_analval7;
                    anal8.EditValue = AnalData.liner_analval8;
                    anal9.EditValue = AnalData.liner_analval9;
                    anal10.EditValue = AnalData.liner_analval10;
                    anal11.EditValue = AnalData.liner_analval11;
                    anal12.EditValue = AnalData.liner_analval12;
                    anal13.EditValue = AnalData.liner_analval13;
                    anal14.EditValue = AnalData.liner_analval14;
                    anal15.EditValue = AnalData.liner_analval15;
                    anal16.EditValue = AnalData.liner_analval16;
                    anal17.EditValue = AnalData.liner_analval17;
                    anal18.EditValue = AnalData.liner_analval18;
                    anal19.EditValue = AnalData.liner_analval19;
                    anal20.EditValue = AnalData.liner_analval20;
                    anal21.EditValue = AnalData.liner_analval21;
                    anal22.EditValue = AnalData.liner_analval22;
                    anal23.EditValue = AnalData.liner_analval23;
                    anal24.EditValue = AnalData.liner_analval24;
                    anal25.EditValue = AnalData.liner_analval25;

                    #region 테스트 데이터
                    //input1.EditValue = 21092.03;
                    //input6.EditValue = 10546.03;
                    //input9.EditValue = 175.76;
                    //input10.EditValue = 70307;
                    //input11.EditValue = 10.55;
                    //input12.EditValue = 35153.35;
                    //input13.EditValue = 421.84;
                    //input15.EditValue = 421.84;
                    //input18.EditValue = 0.00006m;
                    //input19.EditValue = 0.3;
                    //input20.EditValue = 0.75;
                    //input21.EditValue = 0.65;
                    //input22.EditValue = 42.18;
                    //input23.EditValue = 4;
                    //input24.EditValue = 5;
                    //input25.EditValue = 35.15;

                    //object[] obj1 = { input1 };
                    //object[] obj6 = { input6 };
                    //object[] obj9 = { input9 };
                    //object[] obj10 = { input10 };
                    //object[] obj11 = { input11 };
                    //object[] obj12 = { input12 };
                    //object[] obj13 = { input13 };
                    //object[] obj15 = { input15 };
                    //object[] obj18 = { input18 };
                    //object[] obj19 = { input19 };
                    //object[] obj20 = { input20 };
                    //object[] obj21 = { input21 };
                    //object[] obj22 = { input22 };
                    //object[] obj23 = { input23 };
                    //object[] obj24 = { input24 };
                    //object[] obj25 = { input25 };

                    //EditValueChangedAction(obj1);
                    //EditValueChangedAction(obj6);
                    //EditValueChangedAction(obj9);
                    //EditValueChangedAction(obj10);
                    //EditValueChangedAction(obj11);
                    //EditValueChangedAction(obj12);
                    //EditValueChangedAction(obj13);
                    //EditValueChangedAction(obj15);
                    //EditValueChangedAction(obj18);
                    //EditValueChangedAction(obj19);
                    //EditValueChangedAction(obj20);
                    //EditValueChangedAction(obj21);
                    //EditValueChangedAction(obj22);
                    //EditValueChangedAction(obj23);
                    //EditValueChangedAction(obj24);
                    //EditValueChangedAction(obj25);
                    #endregion             
                }
                catch (Exception ex)
                {
                    Messages.ShowErrMsgBoxLog(ex);
                }
                finally
                {
                    bload = false;
                }
            }
        }

        /// <summary>
        /// 초기화 버튼
        /// </summary>
        /// <param name="obj"></param>
        private void InitAction(object obj)
        {
            try
            {
                for (int i = 1; i < 26; i++)
                {
                    if (ucLinerInputView.FindName("input" + i.ToString()) is TextEdit)
                        (ucLinerInputView.FindName("input" + i.ToString()) as TextEdit).EditValue = null;

                    else if (ucLinerInputView.FindName("input" + i.ToString()) is ComboBoxEdit)
                        (ucLinerInputView.FindName("input" + i.ToString()) as ComboBoxEdit).EditValue = null;

                    if (ucLinerInputView.FindName("anal" + i.ToString()) is TextEdit)
                        (ucLinerInputView.FindName("anal" + i.ToString()) as TextEdit).EditValue = null;

                    else if (ucLinerInputView.FindName("anal" + i.ToString()) is ComboBoxEdit)
                        (ucLinerInputView.FindName("anal" + i.ToString()) as ComboBoxEdit).EditValue = null;
                }

                AnalData.liner_inputval1 = null;
                AnalData.liner_inputval2 = null;
                AnalData.liner_inputval3 = null;
                AnalData.liner_inputval4 = null;
                AnalData.liner_inputval5 = null;
                AnalData.liner_inputval6 = null;
                AnalData.liner_inputval7 = null;
                AnalData.liner_inputval8 = null;
                AnalData.liner_inputval9 = null;
                AnalData.liner_inputval10 = null;
                AnalData.liner_inputval11 = null;
                AnalData.liner_inputval12 = null;
                AnalData.liner_inputval13 = null;
                AnalData.liner_inputval14 = null;
                AnalData.liner_inputval15 = null;
                AnalData.liner_inputval16 = null;
                AnalData.liner_inputval17 = null;
                AnalData.liner_inputval18 = null;
                AnalData.liner_inputval19 = null;
                AnalData.liner_inputval20 = null;
                AnalData.liner_inputval21 = null;
                AnalData.liner_inputval22 = null;
                AnalData.liner_inputval23 = null;
                AnalData.liner_inputval24 = null;
                AnalData.liner_inputval25 = null;
                AnalData.liner_analval1 = null;
                AnalData.liner_analval2 = null;
                AnalData.liner_analval3 = null;
                AnalData.liner_analval4 = null;
                AnalData.liner_analval5 = null;
                AnalData.liner_analval6 = null;
                AnalData.liner_analval7 = null;
                AnalData.liner_analval8 = null;
                AnalData.liner_analval9 = null;
                AnalData.liner_analval10 = null;
                AnalData.liner_analval11 = null;
                AnalData.liner_analval12 = null;
                AnalData.liner_analval13 = null;
                AnalData.liner_analval14 = null;
                AnalData.liner_analval15 = null;
                AnalData.liner_analval16 = null;
                AnalData.liner_analval17 = null;
                AnalData.liner_analval18 = null;
                AnalData.liner_analval19 = null;
                AnalData.liner_analval20 = null;
                AnalData.liner_analval21 = null;
                AnalData.liner_analval22 = null;
                AnalData.liner_analval23 = null;
                AnalData.liner_analval24 = null;
                AnalData.liner_analval25 = null;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 최소두께 계산 버튼
        /// </summary>
        /// <param name="obj"></param>
        private void AnalAction(object obj)
        {
            try
            {
                AnalData.AnalRun();

                if (AnalData.designvaluechk == false)
                {
                    Messages.ShowInfoMsgBox("설계조건입력부를 먼저 입력해야 합니다.");
                    return;
                }
                else if (AnalData.linervaluechk == false)
                {
                    Messages.ShowInfoMsgBox("라이너특성입력부를 먼저 입력해야 합니다.");
                    return;
                }

                PopupMinThicknessResult popup = new PopupMinThicknessResult();
                popup.ShowDialog();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void EditValueChangedAction(object obj)
        {
            if (obj == null) return;

            var values = (object[])obj;

            try
            {
                if (values[0] is Control)
                {
                    decimal dcmval;

                    switch ((values[0] as Control).Name)
                    {
                        case "input1":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal1.EditValue = null;
                                AnalData.liner_analval1 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal1.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval1 = Convert.ToDecimal(anal1.EditValue);
                                    AnalData.liner_inputval1 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal1.EditValue = null;
                                    AnalData.liner_analval1 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal1.EditValue = null;
                                AnalData.liner_analval1 = null;
                            }
                            break;
                        case "input2":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal2.EditValue = null;
                                AnalData.liner_analval2 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal2.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval2 = Convert.ToDecimal(anal2.EditValue);
                                    AnalData.liner_inputval2 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal2.EditValue = null;
                                    AnalData.liner_analval2 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal2.EditValue = null;
                                AnalData.liner_analval2 = null;
                            }
                            break;
                        case "input3":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal3.EditValue = null;
                                AnalData.liner_analval3 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal3.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval3 = Convert.ToDecimal(anal3.EditValue);
                                    AnalData.liner_inputval3 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal3.EditValue = null;
                                    AnalData.liner_analval3 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal3.EditValue = null;
                                AnalData.liner_analval3 = null;
                            }
                            break;
                        case "input4":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal4.EditValue = null;
                                AnalData.liner_analval4 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal4.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval4 = Convert.ToDecimal(anal4.EditValue);
                                    AnalData.liner_inputval4 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal4.EditValue = null;
                                    AnalData.liner_analval4 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal4.EditValue = null;
                                AnalData.liner_analval4 = null;
                            }
                            break;
                        case "input5":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal5.EditValue = null;
                                AnalData.liner_analval5 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal5.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval5 = Convert.ToDecimal(anal5.EditValue);
                                    AnalData.liner_inputval5 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal5.EditValue = null;
                                    AnalData.liner_analval5 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal5.EditValue = null;
                                AnalData.liner_analval5 = null;
                            }
                            break;
                        case "input6":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal6.EditValue = null;
                                AnalData.liner_analval6 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal6.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval6 = Convert.ToDecimal(anal6.EditValue);
                                    AnalData.liner_inputval6 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal6.EditValue = null;
                                    AnalData.liner_analval6 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal6.EditValue = null;
                                AnalData.liner_analval6 = null;
                            }
                            break;
                        case "input7":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal7.EditValue = null;
                                AnalData.liner_analval7 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal7.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval7 = Convert.ToDecimal(anal7.EditValue);
                                    AnalData.liner_inputval7 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal7.EditValue = null;
                                    AnalData.liner_analval7 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal7.EditValue = null;
                                AnalData.liner_analval7 = null;
                            }
                            break;
                        case "input8":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal8.EditValue = null;
                                AnalData.liner_analval8 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal8.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval8 = Convert.ToDecimal(anal8.EditValue);
                                    AnalData.liner_inputval8 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal8.EditValue = null;
                                    AnalData.liner_analval8 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal8.EditValue = null;
                                AnalData.liner_analval8 = null;
                            }
                            break;
                        case "input9":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal9.EditValue = null;
                                AnalData.liner_analval9 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal9.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval9 = Convert.ToDecimal(anal9.EditValue);
                                    AnalData.liner_inputval9 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal9.EditValue = null;
                                    AnalData.liner_analval9 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal9.EditValue = null;
                                AnalData.liner_analval9 = null;
                            }
                            break;
                        case "input10":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal10.EditValue = null;
                                AnalData.liner_analval10 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal10.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval10 = Convert.ToDecimal(anal10.EditValue);
                                    AnalData.liner_inputval10 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal10.EditValue = null;
                                    AnalData.liner_analval10 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal10.EditValue = null;
                                AnalData.liner_analval10 = null;
                            }
                            break;
                        case "input11":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal11.EditValue = null;
                                AnalData.liner_analval11 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal11.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval11 = Convert.ToDecimal(anal11.EditValue);
                                    AnalData.liner_inputval11 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal11.EditValue = null;
                                    AnalData.liner_analval11 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal11.EditValue = null;
                                AnalData.liner_analval11 = null;
                            }
                            break;
                        case "input12":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal12.EditValue = null;
                                AnalData.liner_analval12 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal12.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval12 = Convert.ToDecimal(anal12.EditValue);
                                    AnalData.liner_inputval12 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal12.EditValue = null;
                                    AnalData.liner_analval12 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal12.EditValue = null;
                                AnalData.liner_analval12 = null;
                            }
                            break;
                        case "input13":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal13.EditValue = null;
                                AnalData.liner_analval13 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal13.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval13 = Convert.ToDecimal(anal13.EditValue);
                                    AnalData.liner_inputval13 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal13.EditValue = null;
                                    AnalData.liner_analval13 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal13.EditValue = null;
                                AnalData.liner_analval13 = null;
                            }
                            break;
                        case "input14":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal14.EditValue = null;
                                AnalData.liner_analval14 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal14.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval14 = Convert.ToDecimal(anal14.EditValue);
                                    AnalData.liner_inputval14 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal14.EditValue = null;
                                    AnalData.liner_analval14 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal14.EditValue = null;
                                AnalData.liner_analval14 = null;
                            }
                            break;
                        case "input15":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal15.EditValue = null;
                                AnalData.liner_analval15 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal15.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval15 = Convert.ToDecimal(anal15.EditValue);
                                    AnalData.liner_inputval15 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal15.EditValue = null;
                                    AnalData.liner_analval15 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal15.EditValue = null;
                                AnalData.liner_analval15 = null;
                            }
                            break;
                        case "input16":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal16.EditValue = null;
                                AnalData.liner_analval16 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal16.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval16 = Convert.ToDecimal(anal16.EditValue);
                                    AnalData.liner_inputval16 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal16.EditValue = null;
                                    AnalData.liner_analval16 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal16.EditValue = null;
                                AnalData.liner_analval16 = null;
                            }
                            break;
                        case "input17":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal17.EditValue = null;
                                AnalData.liner_analval17 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal17.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval17 = Convert.ToDecimal(anal17.EditValue);
                                    AnalData.liner_inputval17 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal17.EditValue = null;
                                    AnalData.liner_analval17 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal17.EditValue = null;
                                AnalData.liner_analval17 = null;
                            }
                            break;
                        case "input18":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal18.EditValue = null;
                                AnalData.liner_analval18 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal18.EditValue = Math.Round(dcmval, 5, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval18 = Convert.ToDecimal(anal18.EditValue);
                                    AnalData.liner_inputval18 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal18.EditValue = null;
                                    AnalData.liner_analval18 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal18.EditValue = null;
                                AnalData.liner_analval18 = null;
                            }
                            break;
                        case "input19":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal19.EditValue = null;
                                AnalData.liner_analval19 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal19.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval19 = Convert.ToDecimal(anal19.EditValue);
                                    AnalData.liner_inputval19 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal19.EditValue = null;
                                    AnalData.liner_analval19 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal19.EditValue = null;
                                AnalData.liner_analval19 = null;
                            }
                            break;
                        case "input20":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal20.EditValue = null;
                                AnalData.liner_analval20 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal20.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval20 = Convert.ToDecimal(anal20.EditValue);
                                    AnalData.liner_inputval20 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal20.EditValue = null;
                                    AnalData.liner_analval20 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal20.EditValue = null;
                                AnalData.liner_analval20 = null;
                            }
                            break;
                        case "input21":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal21.EditValue = null;
                                AnalData.liner_analval21 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal21.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval21 = Convert.ToDecimal(anal21.EditValue);
                                    AnalData.liner_inputval21 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal21.EditValue = null;
                                    AnalData.liner_analval21 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal21.EditValue = null;
                                AnalData.liner_analval21 = null;
                            }
                            break;
                        case "input22":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal22.EditValue = null;
                                AnalData.liner_analval22 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal22.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval22 = Convert.ToDecimal(anal22.EditValue);
                                    AnalData.liner_inputval22 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal22.EditValue = null;
                                    AnalData.liner_analval22 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal22.EditValue = null;
                                AnalData.liner_analval22 = null;
                            }
                            break;
                        case "input23":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal23.EditValue = null;
                                AnalData.liner_analval23 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal23.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval23 = Convert.ToDecimal(anal23.EditValue);
                                    AnalData.liner_inputval23 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal23.EditValue = null;
                                    AnalData.liner_analval23 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal23.EditValue = null;
                                AnalData.liner_analval23 = null;
                            }
                            break;
                        case "input24":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal24.EditValue = null;
                                AnalData.liner_analval24 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal24.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval24 = Convert.ToDecimal(anal24.EditValue);
                                    AnalData.liner_inputval24 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal24.EditValue = null;
                                    AnalData.liner_analval24 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal24.EditValue = null;
                                AnalData.liner_analval24 = null;
                            }
                            break;
                        case "input25":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal25.EditValue = null;
                                AnalData.liner_analval25 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal25.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.liner_analval25 = Convert.ToDecimal(anal25.EditValue);
                                    AnalData.liner_inputval25 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal25.EditValue = null;
                                    AnalData.liner_analval22 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal25.EditValue = null;
                                AnalData.liner_analval25 = null;
                            }
                            break;
                    }

                    
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
