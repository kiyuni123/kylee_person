﻿using DevExpress.Xpf.Editors;
using GTIFramework.Common.MessageBox;
using LODETECH.Models;
using LODETECH.Modules.Views.Popup;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Data;
using System.Windows.Controls;

namespace LODETECH.Modules
{
    class UcDesignInputViewModel : BindableBase
    {
        bool bload = true;

        DataTable dtGBN3 = CreateData.DesignGBN3create();
        DataTable dtGBN4 = CreateData.DesignGBN4create();
        DataTable dtGBN22 = CreateData.DesignGBN22create();

        #region 컨트롤
        UcDesignInputView ucDesignInputView;

        TextEdit input1;
        TextEdit input2;
        ComboBoxEdit input3;
        ComboBoxEdit input4;
        TextEdit input5;
        TextEdit input6;
        TextEdit input7;
        TextEdit input8;
        TextEdit input9;
        TextEdit input10;
        TextEdit input11;
        TextEdit input12;
        TextEdit input13;
        TextEdit input14;
        TextEdit input15;
        TextEdit input16;
        TextEdit input17;
        TextEdit input18;
        TextEdit input19;
        TextEdit input20;
        TextEdit input21;
        ComboBoxEdit input22;
        TextEdit anal1;
        TextEdit anal2;
        ComboBoxEdit anal3;
        ComboBoxEdit anal4;
        TextEdit anal5;
        TextEdit anal6;
        TextEdit anal7;
        TextEdit anal8;
        TextEdit anal9;
        TextEdit anal10;
        TextEdit anal11;
        TextEdit anal12;
        TextEdit anal13;
        TextEdit anal14;
        TextEdit anal15;
        TextEdit anal16;
        TextEdit anal17;
        TextEdit anal18;
        TextEdit anal19;
        TextEdit anal20;
        TextEdit anal21;
        ComboBoxEdit anal22; 
        #endregion

        /// <summary>
        /// Loaded Command
        /// </summary>
        public DelegateCommand<object> LoadedCommand { get; set; }

        /// <summary>
        /// InitCommand
        /// </summary>
        public DelegateCommand<object> InitCommand { get; set; }

        /// <summary>
        /// AnalCommand
        /// </summary>
        public DelegateCommand<object> AnalCommand { get; set; }

        /// <summary>
        /// EditValueChangedCommand
        /// </summary>
        public DelegateCommand<object> EditValueChangedCommand { get; set; }



        public UcDesignInputViewModel()
        {
            LoadedCommand = new DelegateCommand<object>(OnLoaded);
            InitCommand = new DelegateCommand<object>(InitAction);
            AnalCommand = new DelegateCommand<object>(AnalAction);
            EditValueChangedCommand = new DelegateCommand<object>(EditValueChangedAction);
        }

        /// <summary>
        /// 로드 이벤트
        /// </summary>
        /// <param name="obj"></param>
        private void OnLoaded(object obj)
        {
            if (bload)
            {
                if (obj == null) return;

                var values = (object[])obj;

                try
                {
                    ucDesignInputView = values[0] as UcDesignInputView;

                    input1 = ucDesignInputView.FindName("input1") as TextEdit;
                    input2 = ucDesignInputView.FindName("input2") as TextEdit;
                    input3 = ucDesignInputView.FindName("input3") as ComboBoxEdit;
                    input4 = ucDesignInputView.FindName("input4") as ComboBoxEdit;
                    input5 = ucDesignInputView.FindName("input5") as TextEdit;
                    input6 = ucDesignInputView.FindName("input6") as TextEdit;
                    input7 = ucDesignInputView.FindName("input7") as TextEdit;
                    input8 = ucDesignInputView.FindName("input8") as TextEdit;
                    input9 = ucDesignInputView.FindName("input9") as TextEdit;
                    input10 = ucDesignInputView.FindName("input10") as TextEdit;
                    input11 = ucDesignInputView.FindName("input11") as TextEdit;
                    input12 = ucDesignInputView.FindName("input12") as TextEdit;
                    input13 = ucDesignInputView.FindName("input13") as TextEdit;
                    input14 = ucDesignInputView.FindName("input14") as TextEdit;
                    input15 = ucDesignInputView.FindName("input15") as TextEdit;
                    input16 = ucDesignInputView.FindName("input16") as TextEdit;
                    input17 = ucDesignInputView.FindName("input17") as TextEdit;
                    input18 = ucDesignInputView.FindName("input18") as TextEdit;
                    input19 = ucDesignInputView.FindName("input19") as TextEdit;
                    input20 = ucDesignInputView.FindName("input20") as TextEdit;
                    input21 = ucDesignInputView.FindName("input21") as TextEdit;
                    input22 = ucDesignInputView.FindName("input22") as ComboBoxEdit;
                    anal1 = ucDesignInputView.FindName("anal1") as TextEdit;
                    anal2 = ucDesignInputView.FindName("anal2") as TextEdit;
                    anal3 = ucDesignInputView.FindName("anal3") as ComboBoxEdit;
                    anal4 = ucDesignInputView.FindName("anal4") as ComboBoxEdit;
                    anal5 = ucDesignInputView.FindName("anal5") as TextEdit;
                    anal6 = ucDesignInputView.FindName("anal6") as TextEdit;
                    anal7 = ucDesignInputView.FindName("anal7") as TextEdit;
                    anal8 = ucDesignInputView.FindName("anal8") as TextEdit;
                    anal9 = ucDesignInputView.FindName("anal9") as TextEdit;
                    anal10 = ucDesignInputView.FindName("anal10") as TextEdit;
                    anal11 = ucDesignInputView.FindName("anal11") as TextEdit;
                    anal12 = ucDesignInputView.FindName("anal12") as TextEdit;
                    anal13 = ucDesignInputView.FindName("anal13") as TextEdit;
                    anal14 = ucDesignInputView.FindName("anal14") as TextEdit;
                    anal15 = ucDesignInputView.FindName("anal15") as TextEdit;
                    anal16 = ucDesignInputView.FindName("anal16") as TextEdit;
                    anal17 = ucDesignInputView.FindName("anal17") as TextEdit;
                    anal18 = ucDesignInputView.FindName("anal18") as TextEdit;
                    anal19 = ucDesignInputView.FindName("anal19") as TextEdit;
                    anal20 = ucDesignInputView.FindName("anal20") as TextEdit;
                    anal21 = ucDesignInputView.FindName("anal21") as TextEdit;
                    anal22 = ucDesignInputView.FindName("anal22") as ComboBoxEdit;

                    input3.ItemsSource = dtGBN3.Copy();
                    input3.SelectedIndex = -1;

                    anal3.ItemsSource = dtGBN3.Copy();
                    anal3.SelectedIndex = -1;


                    input4.ItemsSource = dtGBN4.Copy();
                    input4.SelectedIndex = -1;

                    anal4.ItemsSource = dtGBN4.Copy();
                    anal4.SelectedIndex = -1;

                    input22.ItemsSource = dtGBN22.Copy();
                    input22.SelectedIndex = -1;

                    anal22.ItemsSource = dtGBN22.Copy();
                    anal22.SelectedIndex = -1;

                    //값이 있을경우 바인딩
                    input1.EditValue = AnalData.design_inputval1;
                    input2.EditValue = AnalData.design_inputval2;
                    input3.EditValue = AnalData.design_inputval3;
                    input4.EditValue = AnalData.design_inputval4;
                    input5.EditValue = AnalData.design_inputval5;
                    input6.EditValue = AnalData.design_inputval6;
                    input7.EditValue = AnalData.design_inputval7;
                    input8.EditValue = AnalData.design_inputval8;
                    input9.EditValue = AnalData.design_inputval9;
                    input10.EditValue = AnalData.design_inputval10;
                    input11.EditValue = AnalData.design_inputval11;
                    input12.EditValue = AnalData.design_inputval12;
                    input13.EditValue = AnalData.design_inputval13;
                    input14.EditValue = AnalData.design_inputval14;
                    input15.EditValue = AnalData.design_inputval15;
                    input16.EditValue = AnalData.design_inputval16;
                    input17.EditValue = AnalData.design_inputval17;
                    input18.EditValue = AnalData.design_inputval18;
                    input19.EditValue = AnalData.design_inputval19;
                    input20.EditValue = AnalData.design_inputval20;
                    input21.EditValue = AnalData.design_inputval21;
                    input22.EditValue = AnalData.design_inputval22;
                    anal1.EditValue = AnalData.design_analval1;
                    anal2.EditValue = AnalData.design_analval2;
                    anal3.EditValue = AnalData.design_analval3;
                    anal4.EditValue = AnalData.design_analval4;
                    anal5.EditValue = AnalData.design_analval5;
                    anal6.EditValue = AnalData.design_analval6;
                    anal7.EditValue = AnalData.design_analval7;
                    anal8.EditValue = AnalData.design_analval8;
                    anal9.EditValue = AnalData.design_analval9;
                    anal10.EditValue = AnalData.design_analval10;
                    anal11.EditValue = AnalData.design_analval11;
                    anal12.EditValue = AnalData.design_analval12;
                    anal13.EditValue = AnalData.design_analval13;
                    anal14.EditValue = AnalData.design_analval14;
                    anal15.EditValue = AnalData.design_analval15;
                    anal16.EditValue = AnalData.design_analval16;
                    anal17.EditValue = AnalData.design_analval17;
                    anal18.EditValue = AnalData.design_analval18;
                    anal19.EditValue = AnalData.design_analval19;
                    anal20.EditValue = AnalData.design_analval20;
                    anal21.EditValue = AnalData.design_analval21;
                    anal22.EditValue = AnalData.design_analval22;

                    #region 테스트 데이터
                    //input1.EditValue = 1000;
                    //input2.EditValue = 1033;
                    //input4.EditValue = "000001";
                    //input5.EditValue = 25.4;
                    //input6.EditValue = 2;
                    //input7.EditValue = 1.52;
                    //input8.EditValue = 0.3;
                    //input9.EditValue = 7.03;
                    //input10.EditValue = 3.52;
                    //input11.EditValue = 10.55;
                    //input12.EditValue = 210.92;
                    //input13.EditValue = 1.03;
                    //input14.EditValue = 1.92;
                    //input15.EditValue = 1;
                    //input16.EditValue = 7;
                    //input17.EditValue = 0.1;
                    //input18.EditValue = 0.18;
                    //input20.EditValue = 11.11;
                    //input21.EditValue = 91.44;

                    //object[] obj1 = { input1 };
                    //object[] obj2 = { input2 };
                    //object[] obj4 = { input4 };
                    //object[] obj5 = { input5 };
                    //object[] obj6 = { input6 };
                    //object[] obj7 = { input7 };
                    //object[] obj8 = { input8 };
                    //object[] obj9 = { input9 };
                    //object[] obj10 = { input10 };
                    //object[] obj11 = { input11 };
                    //object[] obj12 = { input12 };
                    //object[] obj13 = { input13 };
                    //object[] obj14 = { input14 };
                    //object[] obj15 = { input15 };
                    //object[] obj16 = { input16 };
                    //object[] obj17 = { input17 };
                    //object[] obj18 = { input18 };
                    //object[] obj20 = { input20 };
                    //object[] obj21 = { input21 };

                    //EditValueChangedAction(obj1);
                    //EditValueChangedAction(obj2);
                    //EditValueChangedAction(obj4);
                    //EditValueChangedAction(obj5);
                    //EditValueChangedAction(obj6);
                    //EditValueChangedAction(obj7);
                    //EditValueChangedAction(obj8);
                    //EditValueChangedAction(obj9);
                    //EditValueChangedAction(obj10);
                    //EditValueChangedAction(obj11);
                    //EditValueChangedAction(obj12);
                    //EditValueChangedAction(obj13);
                    //EditValueChangedAction(obj14);
                    //EditValueChangedAction(obj15);
                    //EditValueChangedAction(obj16);
                    //EditValueChangedAction(obj17);
                    //EditValueChangedAction(obj18);
                    //EditValueChangedAction(obj20);
                    //EditValueChangedAction(obj21);
                    #endregion
                }
                catch (Exception ex)
                {
                    Messages.ShowErrMsgBoxLog(ex);
                }
                finally
                {
                    bload = false;
                }
            }
        }

        /// <summary>
        /// 초기화 버튼
        /// </summary>
        /// <param name="obj"></param>
        private void InitAction(object obj)
        {
            try
            {
                for (int i = 1; i < 23; i++)
                {
                    if (ucDesignInputView.FindName("input" + i.ToString()) is TextEdit)
                        (ucDesignInputView.FindName("input" + i.ToString()) as TextEdit).EditValue = null;

                    else if (ucDesignInputView.FindName("input" + i.ToString()) is ComboBoxEdit)
                        (ucDesignInputView.FindName("input" + i.ToString()) as ComboBoxEdit).EditValue = null;

                    if (ucDesignInputView.FindName("anal" + i.ToString()) is TextEdit)
                        (ucDesignInputView.FindName("anal" + i.ToString()) as TextEdit).EditValue = null;

                    else if (ucDesignInputView.FindName("anal" + i.ToString()) is ComboBoxEdit)
                        (ucDesignInputView.FindName("anal" + i.ToString()) as ComboBoxEdit).EditValue = null;
                }

                AnalData.design_inputval1 = null;
                AnalData.design_inputval2 = null;
                AnalData.design_inputval3 = null;
                AnalData.design_inputval4 = null;
                AnalData.design_inputval5 = null;
                AnalData.design_inputval6 = null;
                AnalData.design_inputval7 = null;
                AnalData.design_inputval8 = null;
                AnalData.design_inputval9 = null;
                AnalData.design_inputval10 = null;
                AnalData.design_inputval11 = null;
                AnalData.design_inputval12 = null;
                AnalData.design_inputval13 = null;
                AnalData.design_inputval14 = null;
                AnalData.design_inputval15 = null;
                AnalData.design_inputval16 = null;
                AnalData.design_inputval17 = null;
                AnalData.design_inputval18 = null;
                AnalData.design_inputval19 = null;
                AnalData.design_inputval20 = null;
                AnalData.design_inputval21 = null;
                AnalData.design_inputval22 = null;
                AnalData.design_analval1 = null;
                AnalData.design_analval2 = null;
                AnalData.design_analval3 = null;
                AnalData.design_analval4 = null;
                AnalData.design_analval5 = null;
                AnalData.design_analval6 = null;
                AnalData.design_analval7 = null;
                AnalData.design_analval8 = null;
                AnalData.design_analval9 = null;
                AnalData.design_analval10 = null;
                AnalData.design_analval11 = null;
                AnalData.design_analval12 = null;
                AnalData.design_analval13 = null;
                AnalData.design_analval14 = null;
                AnalData.design_analval15 = null;
                AnalData.design_analval16 = null;
                AnalData.design_analval17 = null;
                AnalData.design_analval18 = null;
                AnalData.design_analval19 = null;
                AnalData.design_analval20 = null;
                AnalData.design_analval21 = null;
                AnalData.design_analval22 = null;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 최소두께 계산 버튼
        /// </summary>
        /// <param name="obj"></param>
        private void AnalAction(object obj)
        {
            try
            {
                AnalData.AnalRun();

                if (AnalData.designvaluechk == false)
                {
                    Messages.ShowInfoMsgBox("설계조건입력부를 먼저 입력해야 합니다.");
                    return;
                }
                else if (AnalData.linervaluechk == false)
                {
                    Messages.ShowInfoMsgBox("라이너특성입력부를 먼저 입력해야 합니다.");
                    return;
                }

                PopupMinThicknessResult popup = new PopupMinThicknessResult();
                popup.ShowDialog();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void EditValueChangedAction(object obj)
        {
            if (obj == null) return;

            var values = (object[])obj;

            try
            {
                if (values[0] is Control)
                {
                    decimal dcmval;

                    switch ((values[0] as Control).Name)
                    {
                        case "input1":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal1.EditValue = null;
                                AnalData.design_analval1 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal1.EditValue = Math.Round(dcmval / Convert.ToDecimal(25.4), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval1 = Convert.ToDecimal(anal1.EditValue);
                                    AnalData.design_inputval1 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal1.EditValue = null;
                                    AnalData.design_analval1 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal1.EditValue = null;
                                AnalData.design_analval1 = null;
                            }
                            break;
                        case "input2":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal2.EditValue = null;
                                AnalData.design_analval2 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal2.EditValue = Math.Round(dcmval / Convert.ToDecimal(25.4), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval2 = Convert.ToDecimal(anal2.EditValue);
                                    AnalData.design_inputval2 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal2.EditValue = null;
                                    AnalData.design_analval2 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal2.EditValue = null;
                                AnalData.design_analval2 = null;
                            }
                            break;
                        case "input3":
                            anal3.EditValue = (values[0] as ComboBoxEdit).EditValue;
                            AnalData.design_inputval3 = (values[0] as ComboBoxEdit).EditValue?.ToString() ?? "";
                            AnalData.design_analval3 = anal3.EditValue?.ToString() ?? "";
                            break;
                        case "input4":
                            anal4.EditValue = (values[0] as ComboBoxEdit).EditValue;
                            AnalData.design_inputval4 = (values[0] as ComboBoxEdit).EditValue?.ToString() ?? "" ;
                            AnalData.design_analval4 = anal4.EditValue?.ToString() ?? "";
                            break;
                        case "input5":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal5.EditValue = null;
                                AnalData.design_analval5 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal5.EditValue = Math.Round(dcmval / Convert.ToDecimal(25.4), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval5 = Convert.ToDecimal(anal5.EditValue);
                                    AnalData.design_inputval5 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal5.EditValue = null;
                                    AnalData.design_analval5 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal5.EditValue = null;
                                AnalData.design_analval5 = null;
                            }
                            break;
                        case "input6":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal6.EditValue = null;
                                AnalData.design_analval6 = null;
                            }
                            else if(decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal6.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval6 = Convert.ToDecimal(anal6.EditValue);
                                    AnalData.design_inputval6 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal6.EditValue = null;
                                    AnalData.design_analval6 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal6.EditValue = null;
                                AnalData.design_analval6 = null;
                            }
                            break;
                        case "input7":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal7.EditValue = null;
                                AnalData.design_analval7 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal7.EditValue = Math.Round(dcmval * Convert.ToDecimal(3.28084), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval7 = Convert.ToDecimal(anal7.EditValue);
                                    AnalData.design_inputval7 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal7.EditValue = null;
                                    AnalData.design_analval7 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal7.EditValue = null;
                                AnalData.design_analval7 = null;
                            }
                            break;
                        case "input8":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal8.EditValue = null;
                                AnalData.design_analval8 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal8.EditValue = Math.Round(dcmval * Convert.ToDecimal(3.28084), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval8 = Convert.ToDecimal(anal8.EditValue);
                                    AnalData.design_inputval8 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal8.EditValue = null;
                                    AnalData.design_analval8 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal8.EditValue = null;
                                AnalData.design_analval8 = null;
                            }
                            break;
                        case "input9":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal9.EditValue = null;
                                AnalData.design_analval9 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal9.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval9 = Convert.ToDecimal(anal9.EditValue);
                                    AnalData.design_inputval9 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal9.EditValue = null;
                                    AnalData.design_analval9 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal9.EditValue = null;
                                AnalData.design_analval9 = null;
                            }
                            break;
                        case "input10":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal10.EditValue = null;
                                AnalData.design_analval10 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal10.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval10 = Convert.ToDecimal(anal10.EditValue);
                                    AnalData.design_inputval10 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal10.EditValue = null;
                                    AnalData.design_analval10 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal10.EditValue = null;
                                AnalData.design_analval10 = null;
                            }
                            break;
                        case "input11":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal11.EditValue = null;
                                AnalData.design_analval11 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal11.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval11 = Convert.ToDecimal(anal11.EditValue);
                                    AnalData.design_inputval11 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal11.EditValue = null;
                                    AnalData.design_analval11 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal11.EditValue = null;
                                AnalData.design_analval11 = null;
                            }
                            break;
                        case "input12":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal12.EditValue = null;
                                AnalData.design_analval12 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal12.EditValue = dcmval * Convert.ToDecimal(14.223393);
                                    AnalData.design_analval12 = Convert.ToDecimal(anal12.EditValue);
                                    AnalData.design_inputval12 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal12.EditValue = null;
                                    AnalData.design_analval12 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal12.EditValue = null;
                                AnalData.design_analval12 = null;
                            }
                            break;
                        case "input13":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal13.EditValue = null;
                                AnalData.design_analval13 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal13.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval13 = Convert.ToDecimal(anal13.EditValue);
                                    AnalData.design_inputval13 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal13.EditValue = null;
                                    AnalData.design_analval13 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal13.EditValue = null;
                                AnalData.design_analval13 = null;
                            }
                            break;
                        case "input14":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal14.EditValue = null;
                                AnalData.design_analval14 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal14.EditValue = Math.Round(dcmval * Convert.ToDecimal(2204.62262) / Convert.ToDecimal(Math.Pow(3.28084, 3.0)), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval14 = Convert.ToDecimal(anal14.EditValue);
                                    AnalData.design_inputval14 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal14.EditValue = null;
                                    AnalData.design_analval14 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal14.EditValue = null;
                                AnalData.design_analval14 = null;
                            }
                            break;
                        case "input15":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal15.EditValue = null;
                                AnalData.design_analval15 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal15.EditValue = Math.Round(dcmval * Convert.ToDecimal(2204.62262) / Convert.ToDecimal(Math.Pow(3.28084, 3.0)), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval15 = Convert.ToDecimal(anal15.EditValue);
                                    AnalData.design_inputval15 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal15.EditValue = null;
                                    AnalData.design_analval15 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal15.EditValue = null;
                                AnalData.design_analval15 = null;
                            }
                            break;
                        case "input16":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal16.EditValue = null;
                                AnalData.design_analval16 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal16.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval16 = Convert.ToDecimal(anal16.EditValue);
                                    AnalData.design_inputval16 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal16.EditValue = null;
                                    AnalData.design_analval16 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal16.EditValue = null;
                                AnalData.design_analval16 = null;
                            }
                            break;
                        case "input17":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal17.EditValue = null;
                                AnalData.design_analval17 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal17.EditValue = Math.Round(dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval17 = Convert.ToDecimal(anal17.EditValue);
                                    AnalData.design_inputval17 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal17.EditValue = null;
                                    AnalData.design_analval17 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal17.EditValue = null;
                                AnalData.design_analval17 = null;
                            }
                            break;
                        case "input18":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal18.EditValue = null;
                                AnalData.design_analval18 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal18.EditValue = Math.Round(dcmval * Convert.ToDecimal(14.223393), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval18 = Convert.ToDecimal(anal18.EditValue);
                                    AnalData.design_inputval18 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal18.EditValue = null;
                                    AnalData.design_analval18 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal18.EditValue = null;
                                AnalData.design_analval18 = null;
                            }
                            break;
                        case "input19":
                            break;
                        case "input20":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal20.EditValue = null;
                                AnalData.design_analval20 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal20.EditValue = Math.Round(Convert.ToDecimal((9d / 5d)) * dcmval, 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval20 = Convert.ToDecimal(anal20.EditValue);
                                    AnalData.design_inputval20 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal20.EditValue = null;
                                    AnalData.design_analval20 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal20.EditValue = null;
                                AnalData.design_analval20 = null;
                            }
                            break;
                        case "input21":
                            if ((values[0] as TextEdit).EditValue == null)
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal21.EditValue = null;
                                AnalData.design_analval21 = null;
                            }
                            else if (decimal.TryParse((values[0] as TextEdit).EditValue.ToString(), out dcmval))
                            {
                                try
                                {
                                    anal21.EditValue = Math.Round(dcmval * Convert.ToDecimal(3.28084), 2, MidpointRounding.AwayFromZero);
                                    AnalData.design_analval21 = Convert.ToDecimal(anal21.EditValue);
                                    AnalData.design_inputval21 = dcmval;
                                }
                                catch (Exception ex)
                                {
                                    //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                    anal21.EditValue = null;
                                    AnalData.design_analval21 = null;
                                }
                            }
                            else
                            {
                                //Messages.ShowInfoMsgBox("계산이 불가합니다.");
                                anal21.EditValue = null;
                                AnalData.design_analval21 = null;
                            }
                            break;
                        case "input22":
                            anal22.EditValue = (values[0] as ComboBoxEdit).EditValue;
                            AnalData.design_inputval22 = (values[0] as ComboBoxEdit).EditValue?.ToString() ?? "";
                            AnalData.design_analval22 = anal22.EditValue?.ToString() ?? "";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
