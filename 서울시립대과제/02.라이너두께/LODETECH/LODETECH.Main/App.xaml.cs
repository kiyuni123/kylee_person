﻿using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using LODETECH.Main.View;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace LODETECH.Main
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : Application
    {
        Mutex _mutex = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            string strtitle = "장거리-저에너지형 상수관로 갱생기술 최적 시공 운영프로그램";

            bool bcreateNew = false;

            try
            {
                _mutex = new Mutex(true, strtitle, out bcreateNew);

                if (bcreateNew)
                {
                    bool? bres = null;

                    Login login = new Login();
                    login = new Login();
                    bres = login.ShowDialog();

                    if (bres != null)
                    {
                        if (!bres ?? true)
                        {
                            Environment.Exit(0);
                            System.Diagnostics.Process.GetCurrentProcess().Kill();
                        }
                        else
                        {
                            base.OnStartup(e);

                            Bootstrapper bs = new Bootstrapper();
                            bs.Run();
                        }
                    }
                }
                else
                {
                    MessageBox.Show(strtitle + "이 실행중입니다.", "알림", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    Environment.Exit(0);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                Messages.ShowInfoMsgBox(e.Exception.ToString());
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            try
            {
                for (int i = 0; i < Logs.htthread.Count - 1; i++)
                {
                    if (Logs.htthread[i] is Thread)
                    {
                        try
                        {
                            (Logs.htthread[i] as Thread).Abort();
                            Logs.htthread.Remove((Logs.htthread[i] as Thread).Name);
                        }
                        catch (Exception ex) { }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
