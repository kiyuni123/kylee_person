﻿using DevExpress.Xpf.Accordion;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using LODETECH.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace LODETECH.Main
{
    public class MainWinViewModel : BindableBase
    {
        bool bload = true;
        MainWin mainwin; //메인윈도우
        Button btnWindowSize;

        private bool bMenuShowHiden = true;
        DataTable dtmenu = CreateData.Menucreate();
        StackPanel spMenuArea; //대메뉴 StackPanel
        AccordionControl accrMenu; //중메뉴 아코디언
        string strSelectMenu = string.Empty;

        private readonly IRegionManager regionManager;

        #region ==========  Properties 정의 ========== 


        /// <summary>
        /// LoadedCommand
        /// </summary>
        public DelegateCommand<object> LoadedCommand { get; set; }

        /// <summary>
        /// Close Event
        /// </summary>
        public DelegateCommand<object> CloseCommand { get; set; }

        /// <summary>
        /// Maximize Event
        /// </summary>
        public DelegateCommand<object> MaximizeCommand { get; set; }

        /// <summary>
        /// minimize Event
        /// </summary>
        public DelegateCommand<object> MinimizeCommand { get; set; }

        /// <summary>
        /// WindowMove Event
        /// </summary>
        public DelegateCommand<object> WindowMoveCommand { get; set; }

        /// <summary>
        /// 테마 변경 
        /// </summary>
        public DelegateCommand<object> ChangeThemeCommand { get; set; }

        /// <summary>
        /// 메뉴 show/hiden
        /// </summary>
        public DelegateCommand<object> MenuShowHidenCommand { get; set; }



        /// <summary>
        /// MenuControlCommand
        /// </summary>
        public DelegateCommand<object> MenuControlCommand { get; set; }


        #endregion

        public MainWinViewModel(IRegionManager regionManager)
        {
            this.regionManager = regionManager;
            LoadedCommand = new DelegateCommand<object>(OnLoaded);
            CloseCommand = new DelegateCommand<object>(CloseAction);
            MaximizeCommand = new DelegateCommand<object>(MaximizeAction);
            MinimizeCommand = new DelegateCommand<object>(MinimiizeAction);
            WindowMoveCommand = new DelegateCommand<object>(WindowMoveAction);
            MenuShowHidenCommand = new DelegateCommand<object>(MenuShowHidenAction);
            MenuControlCommand = new DelegateCommand<object>(MenuControlAction);
        }

        #region 이벤트
        private void OnLoaded(object obj)
        {
            if (bload)
            {
                if (obj == null) return;

                try
                {
                    mainwin = obj as MainWin;

                    spMenuArea = mainwin.FindName("spMenuArea") as StackPanel;
                    accrMenu = mainwin.FindName("accrMenu") as AccordionControl;
                    btnWindowSize = mainwin.FindName("btnWindowSize") as Button;

                    MenuDataInit();

                    //초기 메뉴
                    BtnMenu_Click(((mainwin.FindName("spMenuArea") as StackPanel).Children[0] as Button), null);
                    accrMenu.SelectedItem = accrMenu.Items[0];
                    MenuControlAction(null);
                }
                catch (Exception ex)
                {
                    Messages.ShowErrMsgBoxLog(ex);
                }
                finally
                {
                    bload = false;
                }
            }
        }

        private void BtnMenu_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!(sender is Button)) return;

            //네이밍 정리
            foreach (AccordionItem item in accrMenu.Items)
            {
                mainwin.UnregisterName(item.Name);

                foreach (AccordionItem citem in item.Items)
                {
                    item.UnregisterName(citem.Name);
                }
            }

            //중메뉴 정리
            accrMenu.Items.Clear();

            foreach (DataRow r in dtmenu.Select("MNU_STEP = '2' AND MNU_UPPER ='" + ((Button)sender).Name.Replace("MN_", "").ToString() + "'", "MNU_ORD ASC"))
            {
                try
                {
                    AccordionItem acctwoitem = new AccordionItem
                    {
                        Name = "MN_" + r["MNU_CD"].ToString(),
                        Header = r["MNU_NM"].ToString(),
                        Foreground = new SolidColorBrush(Colors.White),
                        FontSize = 14,
                        ToolTip = r["MNU_NM"].ToString()
                    };

                    if (ThemeApply.strThemeName.Equals("GTINavyTheme"))
                        acctwoitem.Glyph = new BitmapImage(new Uri("/Resources/Navy/Images/MNUImage/" + r["MNU_IMG"].ToString(), UriKind.Relative));
                    else if (ThemeApply.strThemeName.Equals("GTIBlueTheme"))
                        acctwoitem.Glyph = new BitmapImage(new Uri("/Resources/Blue/Images/MNUImage/" + r["MNU_IMG"].ToString(), UriKind.Relative));

                    mainwin.RegisterName(acctwoitem.Name, acctwoitem);
                    accrMenu.Items.Add(acctwoitem);

                    //소메뉴
                    foreach (DataRow drthree in dtmenu.Select("MNU_STEP = '3' AND MNU_UPPER ='" + acctwoitem.Name.Replace("MN_", "").ToString() + "'", "MNU_ORD ASC"))
                    {
                        try
                        {
                            AccordionItem accthreeitem = new AccordionItem
                            {
                                Name = "MN_" + drthree["MNU_CD"].ToString(),
                                Header = " ⁃ " + drthree["MNU_NM"].ToString(),
                                Foreground = new SolidColorBrush(Colors.White),
                                FontSize = 13,
                                ToolTip = drthree["MNU_NM"].ToString()
                            };
                            
                            mainwin.RegisterName(accthreeitem.Name, accthreeitem);
                            acctwoitem.Items.Add(accthreeitem);
                        }
                        catch (Exception ex)
                        {
                            Messages.ErrLog(ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Messages.ErrLog(ex);
                }
            }

            accrMenu.ExpandAll();
        }

        /// <summary>
        /// 창을 닫는다.
        /// </summary>
        /// <param name="strArg">object</param>
        private void CloseAction(object obj)
        {
            try
            {
                if (Messages.ShowExitMsgBox("장거리-저에너지형 상수관로 갱생기술 최적 시공 운영프로그램") == MessageBoxResult.Yes)
                {
                    Environment.Exit(0);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }

            }
            catch (Exception)
            {
                Environment.Exit(0);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        /// <summary>
        /// 창을 최대화 한다.
        /// </summary>
        /// <param name="obj">object</param>
        private void MaximizeAction(object obj)
        {
            if (mainwin.WindowState == WindowState.Maximized)
            {
                mainwin.WindowState = WindowState.Normal;

                btnWindowSize.Style = (Style)Application.Current.FindResource("MaxWindowButton");
            }
            else if (mainwin.WindowState == WindowState.Normal)
            {
                mainwin.WindowState = WindowState.Maximized;

                btnWindowSize.Style = (Style)Application.Current.FindResource("BeforeWindowButton");
            }
        }

        /// <summary>
        /// 창을 최소화 한다
        /// </summary>
        /// <param name="arg">object</param>
        private void MinimiizeAction(object arg)
        {
            mainwin.WindowState = System.Windows.WindowState.Minimized;
        }

        /// <summary>
        /// 창화면이동 
        /// (튀는 현상 수정 완료)
        /// </summary>
        /// <param name="obj"></param>
        private void WindowMoveAction(object obj)
        {
            try
            {
                if (Mouse.LeftButton == MouseButtonState.Pressed)
                {
                    if (mainwin.WindowState == WindowState.Maximized)
                    {
                        mainwin.Top = Mouse.GetPosition(mainwin).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                        mainwin.Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(mainwin).X + 20;

                        mainwin.WindowState = WindowState.Normal;

                        btnWindowSize.Style = (Style)Application.Current.FindResource("MaxWindowButton");
                    }
                    mainwin.DragMove();
                }

                if (Mouse.LeftButton == MouseButtonState.Released)
                {
                    if (mainwin.WindowState == WindowState.Maximized)
                    {
                        btnWindowSize.Style = (Style)Application.Current.FindResource("BeforeWindowButton");
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 메뉴 Show/Hiden
        /// </summary>
        /// <param name="obj"></param>
        private void MenuShowHidenAction(object obj)
        {
            Storyboard sb;

            Button btn = (Button)mainwin.FindName("btnMenuSlide");

            if (bMenuShowHiden)
            {
                sb = mainwin.FindResource("Menuin") as Storyboard;

                btn.Margin = new Thickness(0, 0, 18, 0);
                accrMenu.RootItemExpandButtonPosition = ExpandButtonPosition.None;

                foreach (AccordionItem item in accrMenu.Items)
                {
                    item.ToolTip = item.Header;

                    foreach (AccordionItem iitem in item.Items)
                    {
                        iitem.ToolTip = iitem.Header.ToString().Replace(" ⁃ ", "");
                    }
                }
            }
            else
            {
                sb = mainwin.FindResource("Menuout") as Storyboard;

                btn.Margin = new Thickness(0, 0, 6, 0);
                accrMenu.RootItemExpandButtonPosition = ExpandButtonPosition.Right;

                foreach (AccordionItem item in accrMenu.Items)
                {
                    item.ToolTip = null;

                    foreach (AccordionItem iitem in item.Items)
                    {
                        iitem.ToolTip = null;
                    }
                }
            }

            sb.Begin(mainwin);
            bMenuShowHiden = !bMenuShowHiden;
        }

        /// <summary>
        /// 메뉴선택 이벤트
        /// </summary>
        /// <param name="obj"></param>
        private void MenuControlAction(object obj)
        {
            try
            {
                AccordionItem accitemSelect = accrMenu.SelectedItem as AccordionItem;

                if (accitemSelect != null)
                {
                    strSelectMenu = accitemSelect.Name.Replace("MN_", "");
                    DataRow[] dr = dtmenu.Select("MNU_CD = '" + strSelectMenu + "' AND MNU_STEP = '2'");

                    if (dr.Length == 1)
                    {
                        if (!dr[0]["MNU_PATH"].ToString().Equals(""))
                        {
                            Label lbTitle = mainwin.FindName("lbTitle") as Label;
                            lbTitle.Content = dr[0]["MNU_NM"].ToString();

                            Grid gridtitle = mainwin.FindName("gridtitle") as Grid;
                            gridtitle.RowDefinitions[0].Height = new GridLength(40, GridUnitType.Pixel);

                            regionManager.Regions["ContentRegion"].RemoveAll();
                            regionManager.RequestNavigate("ContentRegion", new Uri(dr[0]["MNU_PATH"].ToString(), UriKind.Relative));
                        }
                        else
                        {
                            Messages.ShowInfoMsgBox("메뉴 경로가 부적합 합니다.");
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 기능
        /// <summary>
        /// 메뉴 구조 생성
        /// </summary>
        private void MenuDataInit()
        {
            try
            {
                foreach (DataRow r in dtmenu.Select("MNU_STEP = '1'", "MNU_ORD ASC"))
                {
                    try
                    {
                        Button btnMenu = new Button
                        {
                            Name = "MN_" + r["MNU_CD"].ToString(),
                            Content = r["MNU_NM"].ToString(),
                            //Style = Application.Current.Resources["MainMNUButton"] as Style,
                            Tag = "/Resources/Images/MNUImage/" + r["MNU_IMG"].ToString()
                        };

                        if (ThemeApply.strThemeName.Equals("GTINavyTheme"))
                            btnMenu.Tag = "/Resources/Navy/Images/MNUImage/" + r["MNU_IMG"].ToString();
                        else if (ThemeApply.strThemeName.Equals("GTIBlueTheme"))
                            btnMenu.Tag = "/Resources/Blue/Images/MNUImage/" + r["MNU_IMG"].ToString();

                        btnMenu.SetResourceReference(Control.StyleProperty, "MainMNUButton");
                        btnMenu.Click += BtnMenu_Click;
                        mainwin.RegisterName(btnMenu.Name, btnMenu);
                        spMenuArea.Children.Add(btnMenu);
                    }
                    catch (Exception ex)
                    {
                        Messages.ErrLog(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion
    }
}
