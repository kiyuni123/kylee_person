﻿using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Windows;
using System.Windows.Input;

namespace LODETECH.Main.View
{
    /// <summary>
    /// Login.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();

            if (Properties.Settings.Default.strThemeName.Equals(""))
                ThemeApply.strThemeName = "GTINavyTheme";
            else
                ThemeApply.strThemeName = Properties.Settings.Default.strThemeName;
            ThemeApply.ThemeChange(this);
            ThemeApply.Themeapply(this);

            InitializeEvent();
            InitializeData();
        }

        /// <summary>
        /// 이벤트 초기화
        /// </summary>
        private void InitializeEvent()
        {
            btnClose.Click += BtnClose_Click;
            btnLogin.Click += BtnLogin_Click;
            pwdPW.KeyDown += PwdPW_KeyDown;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;
        }

        /// <summary>
        /// 로그인 폼 데이터 초기화
        /// </summary>
        public void InitializeData()
        {
            try
            {
                //최근 저장되어 있는 아이디 바인딩
                if (Properties.Settings.Default.bSaveID)
                {
                    chkSaveID.EditValue = Properties.Settings.Default.bSaveID;
                    txtID.Text = Properties.Settings.Default.strRecentID;
                    pwdPW.Focus();
                }
                else
                    txtID.Focus();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        #region ========== 이벤트 정의 ==========
        /// <summary>
        /// 닫기버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            if (Messages.ShowExitMsgBox("장거리-저에너지형 상수관로 갱생기술 최적 시공 운영프로그램") == MessageBoxResult.Yes)
            {
                Environment.Exit(0);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        /// <summary>
        /// 로그인 버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //ID, PW 하드코딩
                if (txtID.EditValue.ToString().Equals("admin")
                    & pwdPW.EditValue.ToString().Equals("admin"))
                {
                    Properties.Settings.Default.bSaveID = (bool)chkSaveID.EditValue;
                    if ((bool)chkSaveID.EditValue)
                    {
                        Properties.Settings.Default.strRecentID = txtID.Text;
                        Properties.Settings.Default.Save();
                    }
                    else
                    {
                        Properties.Settings.Default.strRecentID = "";
                        Properties.Settings.Default.Save();
                    }

                    DialogResult = true;
                    Close();
                }
                else
                    Messages.ShowInfoMsgBox("로그인 정보가 틀립니다.");
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 패스워드에서 엔터
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PwdPW_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnLogin_Click(null, null);
            }
        }

        /// <summary>
        /// 윈도우 창 이동 이벤트 ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (WindowState == WindowState.Maximized)
                {
                    Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    WindowState = WindowState.Normal;
                }
                DragMove();
            }
        }
        #endregion
    }
}
