﻿using LODETECH.Modules;
using Prism.Unity;
using System;
using System.Windows;
using Unity;

namespace LODETECH.Main
{
    public class Bootstrapper : UnityBootstrapper
    {
        /// <summary>
        /// 순서 (1)
        /// </summary>
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            Container.RegisterTypeForNavigation<UcDesignInputView>("UcDesignInputView");        //설계조건입력부
            Container.RegisterTypeForNavigation<UcLinerInputView>("UcLinerInputView");          //라이너특성입력부
        }

        /// <summary>
        /// 순서 (2)
        /// </summary>
        /// <returns></returns>
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWin>();
        }

        /// <summary>
        /// 순서 (3)
        /// </summary>
        protected override void InitializeShell()
        {
            base.InitializeShell();

            Application.Current.MainWindow = (Window)Shell;
            Application.Current.MainWindow.Show();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class UnityExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="container"></param>
        /// <param name="name"></param>
        public static void RegisterTypeForNavigation<T>(this IUnityContainer container, string name)
        {
            Type type = typeof(T);
            string strviewName = String.IsNullOrWhiteSpace(name) ? type.Name : name;
            container.RegisterType(typeof(object), typeof(T), strviewName);
        }
    }
}
