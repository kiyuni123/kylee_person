﻿using GTIFramework.Common.MessageBox;
using System;
using System.Data;

namespace GTIFramework.Analysis.WaterDataTransfer
{
    public class HMIDataTransfer
    {
        public HMIDataTransfer()
        {

        }

        DataTable dtFLSMC_result;
        DataRow drFLSMC_add;
        double douFLSMC_FLSMMin;
        double douFLSMC_FLSMMax;
        string strFLSMC_FLSMC;
        //string strFLSMC_FLSM;
        //double douFLSMC_BFLSMMin;
        //double douFLSMC_BFLSMMax;

        DataTable dtFL_result;
        DataRow drFL_add;
        string strFL_FLSMC;
        string strFL_FLSM;
        string strFL_FL;
        double douFL_BFLSMMin;
        double douFL_BFLSMMax;

        /// <summary>
        /// 누락 적산차 필링
        /// </summary>
        /// <param name="dtinputData"></param>
        /// <returns></returns>
        public DataTable FLSMCFilling(DataTable dtinputData)
        {
            dtFLSMC_result = new DataTable();
            dtFLSMC_result.Columns.Add("DT");
            dtFLSMC_result.Columns.Add("WEEKFLSM", typeof(decimal));
            dtFLSMC_result.Columns.Add("WEEKFLSMC", typeof(decimal));
            dtFLSMC_result.Columns.Add("PER", typeof(decimal));
            dtFLSMC_result.Columns.Add("FLSMC", typeof(decimal));

            try
            {
                if (double.TryParse(dtinputData.Rows[dtinputData.Rows.Count - 1]["FLSM_MODI_VAL"].ToString(), out douFLSMC_FLSMMax)
                    && double.TryParse(dtinputData.Rows[0]["FLSM_MODI_VAL"].ToString(), out douFLSMC_FLSMMin))

                {
                    //douFLSMC_BFLSMMin = 0;
                    //douFLSMC_BFLSMMax = 0;

                    //strFLSMC_FLSM = string.Empty;
                    strFLSMC_FLSMC = string.Empty;

                    if (dtinputData.Select("WEEK1FLSMC IS NULL").Length == 0)
                    {
                        //strFLSMC_FLSM = "WEEK1FLSM";
                        strFLSMC_FLSMC = "WEEK1FLSMC";
                    }
                    else if (dtinputData.Select("WEEK2FLSMC IS NULL").Length == 0)
                    {
                        //strFLSMC_FLSM = "WEEK2FLSM";
                        strFLSMC_FLSMC = "WEEK2FLSMC";
                    }
                    else if (dtinputData.Select("WEEK3FLSMC IS NULL").Length == 0)
                    {
                        //strFLSMC_FLSM = "WEEK3FLSM";
                        strFLSMC_FLSMC = "WEEK3FLSMC";
                    }
                    else if (dtinputData.Select("WEEK4FLSMC IS NULL").Length == 0)
                    {
                        //strFLSMC_FLSM = "WEEK4FLSM";
                        strFLSMC_FLSMC = "WEEK4FLSMC";
                    }

                    //if (!strFLSMC_FLSM.Equals(string.Empty) && !strFLSMC_FLSMC.Equals(string.Empty))
                    if (!strFLSMC_FLSMC.Equals(string.Empty))
                    {
                        foreach (DataRow dr in dtinputData.Rows)
                        {
                            try
                            {
                                drFLSMC_add = dtFLSMC_result.NewRow();
                                drFLSMC_add["DT"] = dr["MESR_TM"];
                                //drFLSMC_add["WEEKFLSM"] = Convert.ToDouble(dr[strFLSMC_FLSM].ToString());
                                drFLSMC_add["WEEKFLSMC"] = Convert.ToDouble(dr[strFLSMC_FLSMC].ToString());
                                dtFLSMC_result.Rows.Add(drFLSMC_add.ItemArray);
                            }
                            catch (Exception ex) { }
                        }

                        dtFLSMC_result.Rows.RemoveAt(dtFLSMC_result.Rows.Count - 1);

                        dtFLSMC_result.Rows.RemoveAt(0);

                        foreach (DataRow dr in dtFLSMC_result.Rows)
                        {
                            if (Convert.ToDouble(dtFLSMC_result.Compute("SUM(WEEKFLSMC)", "1=1").ToString()) == 0)
                                dr["PER"] = 1.0 / dtFLSMC_result.Rows.Count;
                            else
                                dr["PER"] = Convert.ToDouble(dr["WEEKFLSMC"].ToString()) / Convert.ToDouble(dtFLSMC_result.Compute("SUM(WEEKFLSMC)", "1=1").ToString());

                            dr["FLSMC"] = Math.Round((douFLSMC_FLSMMax - douFLSMC_FLSMMin) * Convert.ToDouble(dr["PER"].ToString()),2, MidpointRounding.AwayFromZero);
                        }

                        //if (double.TryParse(dtFLSMC_result.Rows[0]["WEEKFLSM"].ToString(), out douFLSMC_BFLSMMin)
                        //    && double.TryParse(dtFLSMC_result.Rows[dtFLSMC_result.Rows.Count - 1]["WEEKFLSM"].ToString(), out douFLSMC_BFLSMMax))
                        //{

                        //    dtFLSMC_result.Rows.RemoveAt(0);

                        //    foreach (DataRow dr in dtFLSMC_result.Rows)
                        //    {
                        //        dr["PER"] = Convert.ToDouble(dr["WEEKFLSMC"].ToString()) / (douFLSMC_BFLSMMax - douFLSMC_BFLSMMin);
                        //        dr["FLSMC"] = (douFLSMC_FLSMMax - douFLSMC_FLSMMin) * Convert.ToDouble(dr["PER"].ToString());
                        //    }
                        //}
                    }
                }

                return dtFLSMC_result;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return dtFLSMC_result;
            }
        }

        /// <summary>
        /// 유량 적산차 필링
        /// </summary>
        /// <param name="dtinputData"></param>
        /// <returns></returns>
        public DataTable FLFilling(DataTable dtinputData)
        {
            dtFL_result = new DataTable();
            dtFL_result.Columns.Add("DT");
            dtFL_result.Columns.Add("WEEKFLSM");
            dtFL_result.Columns.Add("WEEKFLSMC");
            dtFL_result.Columns.Add("WEEKFL");
            dtFL_result.Columns.Add("PER");
            dtFL_result.Columns.Add("FL");

            try
            {
                strFL_FLSM = string.Empty;
                strFL_FLSMC = string.Empty;
                strFL_FL = string.Empty;

                if (dtinputData.Select("WEEK1FLSM IS NULL OR WEEK1FLSMC IS NULL OR WEEK1FL IS NULL").Length == 0)
                {
                    strFL_FLSM = "WEEK1FLSM";
                    strFL_FLSMC = "WEEK1FLSMC";
                    strFL_FL = "WEEK1FL";
                }
                else if (dtinputData.Select("WEEK2FLSM IS NULL OR WEEK2FLSMC IS NULL OR WEEK2FL IS NULL").Length == 0)
                {
                    strFL_FLSM = "WEEK2FLSM";
                    strFL_FLSMC = "WEEK2FLSMC";
                    strFL_FL = "WEEK2FL";
                }
                else if (dtinputData.Select("WEEK3FLSM IS NULL OR WEEK3FLSMC IS NULL OR WEEK3FL IS NULL").Length == 0)
                {
                    strFL_FLSM = "WEEK3FLSM";
                    strFL_FLSMC = "WEEK3FLSMC";
                    strFL_FL = "WEEK3FL";
                }
                else if (dtinputData.Select("WEEK4FLSM IS NULL OR WEEK4FLSMC IS NULL OR WEEK4FL IS NULL").Length == 0)
                {
                    strFL_FLSM = "WEEK4FLSM";
                    strFL_FLSMC = "WEEK4FLSMC";
                    strFL_FL = "WEEK4FL";
                }

                if(!strFL_FLSM.Equals(string.Empty) && !strFL_FLSMC.Equals(string.Empty) && !strFL_FL.Equals(string.Empty))
                {
                    foreach (DataRow dr in dtinputData.Rows)
                    {
                        try
                        {
                            drFL_add = dtFL_result.NewRow();
                            drFL_add["DT"] = dr["MESR_TM"];
                            drFL_add["WEEKFLSM"] = Convert.ToDouble(dr[strFL_FLSM].ToString());
                            drFL_add["WEEKFLSMC"] = Convert.ToDouble(dr[strFL_FLSMC].ToString());
                            drFL_add["WEEKFL"] = Convert.ToDouble(dr[strFL_FL].ToString());
                            dtFL_result.Rows.Add(drFL_add.ItemArray);
                        }
                        catch (Exception ex) { }
                    }

                    dtFL_result.Rows.RemoveAt(dtFL_result.Rows.Count - 1);

                    if (double.TryParse(dtFL_result.Rows[0]["WEEKFLSM"].ToString(), out douFL_BFLSMMin)
                        && double.TryParse(dtFL_result.Rows[dtFL_result.Rows.Count - 1]["WEEKFLSM"].ToString(), out douFL_BFLSMMax))
                    {
                        dtFL_result.Rows.RemoveAt(0);

                        foreach (DataRow dr in dtFL_result.Rows)
                        {
                            dr["PER"] = Convert.ToDouble(dr["WEEKFLSMC"].ToString()) / (douFL_BFLSMMax - douFL_BFLSMMin);
                            dr["FL"] = Convert.ToDouble(dr["WEEKFL"].ToString()) + (Convert.ToDouble(dr["WEEKFL"].ToString()) * Convert.ToDouble(dr["PER"].ToString()));
                        }
                    }
                }

                return dtFL_result;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return dtFL_result;
            }
        }
    }
}
