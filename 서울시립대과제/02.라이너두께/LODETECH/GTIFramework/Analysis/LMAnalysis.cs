﻿using GTIFramework.Common.MessageBox;
using RDotNet;
using System;
using System.Collections;
using System.Data;
using System.Linq;

namespace GTIFramework.Analysis
{
    /// <summary>
    /// 선형회귀 분석 R 이용
    /// </summary>
    public class LMAnalysis
    {

        /// <summary>
        /// 회귀분석
        /// </summary>
        /// <param name="dtinput">회귀분석 데이터</param>
        /// <param name="stanidx">회귀분석 기준 레코드 데이터 Col idx</param>
        /// <param name="valueidx">회귀분석 값 데이터 Col idx</param>
        /// <param name="intoption">회귀식 다항식 intoption = 차수</param>
        /// <returns></returns>
        public decimal? lm(DataTable dtinput, int stanidx, int valueidx)
        {
            decimal dcmresult;

            try
            {
                if (dtinput.Rows.Count == 0) return null;
                if (dtinput.Columns.Count < 2) return null;

                string Rcode = string.Empty;
                REngine.SetEnvironmentVariables();
                REngine rEngine = REngine.GetInstance();
                rEngine.Initialize();

                DataFrame dfinput = convertTOFrame(rEngine, dtinput.TableName, dtinput);

                rEngine.Evaluate("linear = lm(" + dtinput.Columns[valueidx].ColumnName + " ~" + dtinput.Columns[stanidx].ColumnName + ", data = " + dtinput.TableName + ")");
                rEngine.Evaluate("m = as.numeric(format(coef(linear)[2], digits = 4))");

                if (decimal.TryParse(rEngine.Evaluate("list(m)").AsCharacter()[0], out dcmresult))
                    return dcmresult;
                else
                    return null;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }

        }

        /// <summary>
        /// 회귀식 다항식
        /// </summary>
        /// <param name="dtinput">회귀분석 데이터</param>
        /// <param name="stanidx">회귀분석 기준 레코드 데이터 Col idx</param>
        /// <param name="valueidx">회귀분석 값 데이터 Col idx</param>
        /// <param name="intoption">회귀식 다항식 intoption = 차수</param>
        /// <returns></returns>
        public Hashtable lm(DataTable dtinput, int stanidx, int valueidx, int intoption)
        {
            Hashtable htresult = new Hashtable();

            try
            {
                if (dtinput.Rows.Count == 0) return null;
                if (dtinput.Columns.Count < 2) return null;

                string Rcode = string.Empty;
                string strList = "list(";
                REngine.SetEnvironmentVariables();
                REngine rEngine = REngine.GetInstance();
                rEngine.Initialize();

                DataFrame dfinput = convertTOFrame(rEngine, dtinput.TableName, dtinput);

                rEngine.Evaluate("linear = lm(" + dtinput.Columns[valueidx].ColumnName + " ~ poly(" + dtinput.Columns[stanidx].ColumnName + ", " + intoption.ToString() + ", raw = T) , data = " + dtinput.TableName + ")");

                for (int i = intoption; i >= 0; i--)
                {
                    if (i > 0)
                    {
                        strList = strList + "m" + i.ToString() + ", ";
                        rEngine.Evaluate("m" + i.ToString() + " = as.numeric(format(coef(linear)[" + (i + 1).ToString() + "], digits = 4))");
                    }
                    else if (i == 0)
                    {
                        strList = strList + "n, R2)";
                        rEngine.Evaluate("n = as.numeric(format(coef(linear)[1], digits = 4))");
                        rEngine.Evaluate("R2 = as.numeric(format(summary(linear)$r.squared, digits = 4))");
                    }
                }

                var listresult = rEngine.Evaluate(strList).AsCharacter();

                for (int i = 0; i < listresult.Length - 1; i++)
                {
                    if (i != listresult.Length - 2)
                    {
                        htresult.Add("m" + (listresult.Length - 2 - i).ToString(), listresult[i].ToString());
                    }
                    else if (i == listresult.Length - 2)
                    {
                        htresult.Add("n", listresult[i].ToString());
                        htresult.Add("R2", listresult[i + 1].ToString());
                    }
                }

                return htresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }

        }

        /// <summary>
        /// 회귀식 다항식(NITE 분석)
        /// </summary>
        /// <param name="dtinput">회귀분석 데이터</param>
        /// <param name="stanidx">회귀분석 기준 레코드 데이터 Col idx</param>
        /// <param name="valueidx">회귀분석 값 데이터 Col idx</param>
        /// <param name="intoption">회귀식 다항식 intoption = 차수</param>
        /// <param name="int_y_incre_Min">감소부 Y MIN</param>
        /// <param name="int_y_decre_Min">증가부 Y MIN</param>
        /// <returns></returns>
        public decimal? NITElm(DataTable dtinput, int stanidx, int valueidx, int intoption, int int_y_incre_Min, int int_y_decre_Min)
        {
            decimal dcmresult;

            try
            {
                if (dtinput.Rows.Count == 0) return null;
                if (dtinput.Columns.Count < 2) return null;

                string Rcode = string.Empty;
                string str_Poly_order_eq = "";
                REngine.SetEnvironmentVariables();
                REngine rEngine = REngine.GetInstance();
                rEngine.Initialize();

                DataFrame dfinput = convertTOFrame(rEngine, dtinput.TableName, dtinput);

                //5. 회귀식 도출(6차 다항식)
                rEngine.Evaluate("linear = lm(" + dtinput.Columns[valueidx].ColumnName + " ~ poly(" + dtinput.Columns[stanidx].ColumnName + ", " + intoption.ToString() + ", raw = T) , data = " + dtinput.TableName + ")");

                for (int i = intoption; i >= 0; i--)
                {
                    if (i > 0)
                    {
                        str_Poly_order_eq = str_Poly_order_eq + "m" + i.ToString() + "*No^" + i.ToString() + "+";
                        rEngine.Evaluate("m" + i.ToString() + " = as.numeric(format(coef(linear)[" + (i + 1).ToString() + "], digits = 4))");
                    }
                    else if (i == 0)
                    {
                        str_Poly_order_eq = str_Poly_order_eq + "n";
                        rEngine.Evaluate("n = as.numeric(format(coef(linear)[1], digits = 4))");
                        rEngine.Evaluate("R2 = as.numeric(format(summary(linear)$r.squared, digits = 4))");
                    }
                }

                //제외구간 No선정
                rEngine.Evaluate("No <- seq(" + int_y_incre_Min.ToString() + ", " + int_y_decre_Min.ToString() + ")");

                //변곡점 
                rEngine.Evaluate("Poly_order_eq <- " + str_Poly_order_eq);

                //스무스하게?
                rEngine.Evaluate("lo <- loess(Poly_order_eq~No, span = 0.5)");

                //회귀선 y값 계산
                rEngine.Evaluate("out = predict(lo,No)");
                rEngine.Evaluate("infl <- c(FALSE, diff(diff(out) > 0) != 0)");

                //6. 기저부 범위 내 변곡점의 시각값(xi) 찾기
                rEngine.Evaluate("Leakage_No <- round(No[infl], digits = 0)");
                rEngine.Evaluate("No <- Leakage_No");

                //7. 6차 다항식에 xi 대입하여 유량 산정
                rEngine.Evaluate("Leakage_MNF <- " + str_Poly_order_eq);

                if (decimal.TryParse(rEngine.Evaluate("list(Leakage_MNF)").AsCharacter()[0], out dcmresult))
                    return dcmresult;
                else
                    return null;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }

        }

        /// <summary>
        /// DataFrame -> DataTable 변환
        /// </summary>
        /// <param name="dfdataset"></param>
        /// <returns></returns>
        private DataTable convertTOtable(DataFrame dfdataset)
        {
            DataTable dttemp = new DataTable();

            try
            {
                for (int i = 0; i < dfdataset.ColumnCount; i++)
                {
                    dttemp.Columns.Add(dfdataset.ColumnNames[i]);
                }

                for (int i = 0; i < dfdataset.RowCount; i++)
                {
                    DataRow r = dttemp.NewRow();

                    for (int j = 0; j < dfdataset.ColumnCount; j++)
                    {
                        r[j] = dfdataset[i, j];
                    }

                    dttemp.Rows.Add(r);
                }

                return dttemp;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
        }

        /// <summary>
        /// DataTable -> DataFrame 변환
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dtdata"></param>
        /// <returns></returns>
        private DataFrame convertTOFrame(REngine rEngine, string name, DataTable dtdata)
        {
            try
            {
                DataFrame dataFrame = null;
                IEnumerable[] columns = new IEnumerable[dtdata.Columns.Count];
                string[] columnNames = dtdata.Columns.Cast<DataColumn>()
                                       .Select(x => x.ColumnName)
                                       .ToArray();
                for (int i = 0; i < dtdata.Columns.Count; i++)
                {
                    switch (Type.GetTypeCode(dtdata.Columns[i].DataType))
                    {
                        case TypeCode.String:
                            columns[i] = dtdata.Rows.Cast<DataRow>().Select(row => row.Field<string>(i)).ToArray();
                            break;
                        case TypeCode.Double:
                            columns[i] = dtdata.Rows.Cast<DataRow>().Select(row => row.Field<double>(i)).ToArray();
                            break;
                        case TypeCode.Int32:
                            columns[i] = dtdata.Rows.Cast<DataRow>().Select(row => row.Field<int>(i)).ToArray();
                            break;
                        case TypeCode.Decimal:
                            columns[i] = dtdata.Rows.Cast<DataRow>().Select(row => Convert.ToDouble(row.Field<decimal>(i))).ToArray();
                            break;
                        case TypeCode.DateTime:
                            columns[i] = dtdata.Rows.Cast<DataRow>().Select(row => Convert.ToDateTime(row.Field<DateTime>(i)).ToString("yyyy-MM-dd HH:mm:ss")).ToArray();
                            break;
                        default:
                            columns[i] = dtdata.Rows.Cast<DataRow>().Select(row => row[i]).ToArray();
                            break;
                    }
                }

                dataFrame = rEngine.CreateDataFrame(columns: columns, columnNames: columnNames, stringsAsFactors: false);
                rEngine.SetSymbol(name, dataFrame);

                return dataFrame;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
        }
    }
}
