﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GTIFramework.Analysis.Epanet
{
    public static class Epanet22md
    {
        public const string EPANETDLL = "epanet22.dll";
        public const string EPANETMSX = "epanetmsx.dll";

        //These are codes used by the DLL functions
        public const int EN_ELEVATION = 0;		//'Node parameters
        public const int EN_BASEDEMAND = 1;
        public const int EN_PATTERN = 2;
        public const int EN_EMITTER = 3;
        public const int EN_INITQUAL = 4;
        public const int EN_SOURCEQUAL = 5;
        public const int EN_SOURCEPAT = 6;
        public const int EN_SOURCETYPE = 7;
        public const int EN_TANKLEVEL = 8;
        public const int EN_DEMAND = 9;
        public const int EN_HEAD = 10;
        public const int EN_PRESSURE = 11;
        public const int EN_QUALITY = 12;
        public const int EN_SOURCEMASS = 13;
        public const int EN_INITVOLUME = 14;
        public const int EN_MIXMODEL = 15;
        public const int EN_MIXZONEVOL = 16;
        public const int EN_TANKDIAM = 17;
        public const int EN_MINVOLUME = 18;
        public const int EN_VOLCURVE = 19;
        public const int EN_MINLEVEL = 20;
        public const int EN_MAXLEVEL = 21;
        public const int EN_MIXFRACTION = 22;
        public const int EN_TANK_KBULK = 23;
        // v3.0 추가
        public const int EN_TANKVOLUME = 24;          //Current computed tank volume (read only)
        public const int EN_MAXVOLUME = 25;           //Tank maximum volume (read only)
        public const int EN_CANOVERFLOW = 26;       //Tank can overflow (= 1) or not (= 0)
        public const int EN_DEMANDDEFICIT = 27;     //Amount that full demand is reduced under PDA (read only)

        public const int EN_DIAMETER = 0;		//'Link parameters
        public const int EN_LENGTH = 1;
        public const int EN_ROUGHNESS = 2;
        public const int EN_MINORLOSS = 3;
        public const int EN_INITSTATUS = 4;
        public const int EN_INITSETTING = 5;
        public const int EN_KBULK = 6;
        public const int EN_KWALL = 7;
        public const int EN_FLOW = 8;
        public const int EN_VELOCITY = 9;
        public const int EN_HEADLOSS = 10;
        public const int EN_STATUS = 11;
        public const int EN_SETTING = 12;
        public const int EN_ENERGY = 13;
        // v3.0 추가
        public const int EN_LINKQUAL = 14;              //Current computed link quality (read only)
        public const int EN_LINKPATTERN = 15;        //Pump speed time pattern index.
        public const int EN_PUMP_STATE = 16;        //Current computed pump state (read only) (see EN_PumpStateType)
        public const int EN_PUMP_EFFIC = 17;         //Current computed pump efficiency (read only)
        public const int EN_PUMP_POWER = 18;       //Pump constant power rating.
        public const int EN_PUMP_HCURVE = 19;      //Pump head v. flow curve index.
        public const int EN_PUMP_ECURVE = 20;      //Pump efficiency v. flow curve index.
        public const int EN_PUMP_ECOST = 21;       //Pump average energy price.
        public const int EN_PUMP_EPAT = 22;         //Pump energy price time pattern index.

        public const int EN_DURATION = 0;		//'Time parameters
        public const int EN_HYDSTEP = 1;
        public const int EN_QUALSTEP = 2;
        public const int EN_PATTERNSTEP = 3;
        public const int EN_PATTERNSTART = 4;
        public const int EN_REPORTSTEP = 5;
        public const int EN_REPORTSTART = 6;
        public const int EN_RULESTEP = 7;
        public const int EN_STATISTIC = 8;
        public const int EN_PERIODS = 9;
        // v3.0 추가
        public const int EN_STARTTIME = 10;             //Simulation starting time of day.
        public const int EN_HTIME = 11;                     //Elapsed time of current hydraulic solution (read only)
        public const int EN_QTIME = 12;                     //Elapsed time of current quality solution (read only)
        public const int EN_HALTFLAG = 13;                //Flag indicating if the simulation was halted (read only)
        public const int EN_NEXTEVENT = 14;              //Shortest time until a tank becomes empty or full (read only)
        public const int EN_NEXTEVENTTANK = 15;      //Index of tank with shortest time to become empty or full (read only)

        public const int EN_ITERATIONS = 0;             //'Analysis convergence statistics(v 3.0 ????)
        public const int EN_RELATIVEERROR = 1;
        public const int EN_MAXHEADERROR = 2;
        public const int EN_MAXFLOWCHANGE = 3;
        public const int EN_MASSBALANCE = 4;
        public const int EN_DEFICIENTNODES = 5;
        public const int EN_DEMANDREDUCTION = 6;

        public const int EN_NODE = 0;                       //'Types of network objects(v 3.0 ???)
        public const int EN_LINK = 1;
        public const int EN_TIMEPAT = 2;
        public const int EN_CURVE = 3;
        public const int EN_CONTROL = 4;
        public const int EN_RULE = 5;

        public const int EN_NODECOUNT = 0;		//'Component counts(Types of objects to count)
        public const int EN_TANKCOUNT = 1;
        public const int EN_LINKCOUNT = 2;
        public const int EN_PATCOUNT = 3;
        public const int EN_CURVECOUNT = 4;
        public const int EN_CONTROLCOUNT = 5;
        // v3.0 추가
        public const int EN_RULECOUNT = 6;          //Number of rule-based controls.

        public const int EN_JUNCTION = 0;		//'Node types
        public const int EN_RESERVOIR = 1;
        public const int EN_TANK = 2;

        public const int EN_CVPIPE = 0;			//'Link types
        public const int EN_PIPE = 1;
        public const int EN_PUMP = 2;
        public const int EN_PRV = 3;
        public const int EN_PSV = 4;
        public const int EN_PBV = 5;
        public const int EN_FCV = 6;
        public const int EN_TCV = 7;
        public const int EN_GPV = 8;

        public const int EN_CLOSED = 0;			//'Link status(v 3.0 ???)
        public const int EN_OPEN = 1;

        public const int EN_PUMP_XHEAD = 0;			//'Pump states(v 3.0 ???)
        public const int EN_PUMP_CLOSED = 2;
        public const int EN_PUMP_OPEN = 3;
        public const int EN_PUMP_XFLOW = 5;

        public const int EN_NONE = 0;			//'Quality analysis types
        public const int EN_CHEM = 1;
        public const int EN_AGE = 2;
        public const int EN_TRACE = 3;

        public const int EN_CONCEN = 0;			//'Source quality types
        public const int EN_MASS = 1;
        public const int EN_SETPOINT = 2;
        public const int EN_FLOWPACED = 3;

        public const int EN_HW = 0;			//'Head loss formulas(v 3.0 ???)
        public const int EN_DW = 1;
        public const int EN_CM = 2;

        public const int EN_CFS = 0;			//'Flow units types
        public const int EN_GPM = 1;
        public const int EN_MGD = 2;
        public const int EN_IMGD = 3;
        public const int EN_AFD = 4;
        public const int EN_LPS = 5;
        public const int EN_LPM = 6;
        public const int EN_MLD = 7;
        public const int EN_CMH = 8;
        public const int EN_CMD = 9;

        public const int EN_DDA = 0;			//'Demand models(v 3.0 ???)
        public const int EN_PDA = 1;

        public const int EN_TRIALS = 0;			//'Misc. options(Simulation options)
        public const int EN_ACCURACY = 1;
        public const int EN_TOLERANCE = 2;
        public const int EN_EMITEXPON = 3;
        public const int EN_DEMANDMULT = 4;
        // v3.0 추가
        public const int EN_HEADERROR = 5;                          //Maximum head loss error for hydraulic convergence.
        public const int EN_FLOWCHANGE = 6;                         //Maximum flow change for hydraulic convergence.
        public const int EN_HEADLOSSFORM = 7;                       //Head loss formula (see EN_HeadLossType)
        public const int EN_GLOBALEFFIC = 8;                            //Global pump efficiency (percent)
        public const int EN_GLOBALPRICE = 9;                            //Global energy price per KWH.
        public const int EN_GLOBALPATTERN = 10;                     //Index of a global energy price pattern.
        public const int EN_DEMANDCHARGE = 11;                      //Energy charge per max. KW usage.
        public const int EN_SP_GRAVITY = 12;                            //Specific gravity.
        public const int EN_SP_VISCOS = 13;                             //Specific viscosity (relative to water at 20 deg C)
        public const int EN_UNBALANCED = 14;                            //Extra trials allowed if hydraulics don't converge.
        public const int EN_CHECKFREQ = 15;                             //Frequency of hydraulic status checks.
        public const int EN_MAXCHECK = 16;                              //Maximum trials for status checking.
        public const int EN_DAMPLIMIT = 17;                             //Accuracy level where solution damping begins.
        public const int EN_SP_DIFFUS = 18;                             //Specific diffusivity (relative to chlorine at 20 deg C)
        public const int EN_BULKORDER = 19;                             //Bulk water reaction order for pipes.
        public const int EN_WALLORDER = 20;                             //Wall reaction order for pipes (either 0 or 1)
        public const int EN_TANKORDER = 21;                             //Bulk water reaction order for tanks.
        public const int EN_CONCENLIMIT = 22;                           //Limiting concentration for growth reactions.

        public const int EN_LOWLEVEL = 0;		//'Control types(Simple control types)
        public const int EN_HILEVEL = 1;
        public const int EN_TIMER = 2;
        public const int EN_TIMEOFDAY = 3;

        public const int EN_AVERAGE = 1;		//'Time statistic types(Reporting statistic choices)
        public const int EN_MINIMUM = 2;
        public const int EN_MAXIMUM = 3;
        public const int EN_RANGE = 4;
        // v3.0 추가
        public const int EN_SERIES = 0;         //Report all time series points.

        public const int EN_MIX1 = 0;			//'Tank mixing models
        public const int EN_MIX2 = 1;
        public const int EN_FIFO = 2;
        public const int EN_LIFO = 3;

        public const int EN_NOSAVE = 0;			//'Hydraulic initialization options
        public const int EN_SAVE = 1;
        public const int EN_INITFLOW = 10;
        public const int EN_SAVE_AND_INIT = 11;

        public const int EN_CONST_HP = 0;			//'Types of pump curves(v 3.0 ???)
        public const int EN_POWER_FUNC = 1;
        public const int EN_CUSTOM = 2;
        public const int EN_NOCURVE = 3;

        public const int EN_VOLUME_CURVE = 0;			//'Types of data curves(v 3.0 ???)
        public const int EN_PUMP_CURVE = 1;
        public const int EN_EFFIC_CURVE = 2;
        public const int EN_HLOSS_CURVE = 3;
        public const int EN_GENERIC_CURVE = 4;

        public const int EN_UNCONDITIONAL = 0;			//'Deletion action codes(v 3.0 ???)
        public const int EN_CONDITIONAL = 1;

        public const int EN_NO_REPORT = 0;			//'Status reporting levels(v 3.0 ???)
        public const int EN_NORMAL_REPORT = 1;
        public const int EN_FULL_REPORT = 2;

        public const int EN_R_NODE = 6;			//'Network objects used in rule-based controls(v 3.0 ???)
        public const int EN_R_LINK = 7;
        public const int EN_R_SYSTEM = 8;

        public const int EN_R_DEMAND = 0;			//'Object variables used in rule-based controls(v 3.0 ???)
        public const int EN_R_HEAD = 1;
        public const int EN_R_GRADE = 2;
        public const int EN_R_LEVEL = 3;
        public const int EN_R_PRESSURE = 4;
        public const int EN_R_FLOW = 5;
        public const int EN_R_STATUS = 6;
        public const int EN_R_SETTING = 7;
        public const int EN_R_POWER = 8;
        public const int EN_R_TIME = 9;
        public const int EN_R_CLOCKTIME = 10;
        public const int EN_R_FILLTIME = 11;
        public const int EN_R_DRAINTIME = 12;

        public const int EN_R_EQ = 0;			//'Comparison operators used in rule-based controls(v 3.0 ???)
        public const int EN_R_NE = 1;
        public const int EN_R_LE = 2;
        public const int EN_R_GE = 3;
        public const int EN_R_LT = 4;
        public const int EN_R_GT = 5;
        public const int EN_R_IS = 6;
        public const int EN_R_NOT = 7;
        public const int EN_R_BELOW = 8;
        public const int EN_R_ABOVE = 9;

        public const int EN_R_IS_OPEN = 1;			//'Link status codes used in rule-based controls(v 3.0 ???)
        public const int EN_R_IS_CLOSED = 2;
        public const int EN_R_IS_ACTIVE = 3;

        //These are codes used by the DLL functions(EPANET_MSX)
        public const int MSX_NODE = 0;
        public const int MSX_LINK = 1;
        public const int MSX_TANK = 2;
        public const int MSX_SPECIES = 3;
        public const int MSX_TERM = 4;
        public const int MSX_PARAMETER = 5;
        public const int MSX_CONSTANT = 6;
        public const int MSX_PATTERN = 7;

        public const int MSX_BULK = 0;
        public const int MSX_WALL = 1;

        public const int MSX_NOSOURCE = -1;
        public const int MSX_CONCEN = 0;
        public const int MSX_MASS = 1;
        public const int MSX_SETPOINT = 2;
        public const int MSX_FLOWPACED = 3;

        #region Epanet Imports
        public delegate void UserSuppliedFunction(string param0);
        /// <summary>
        /// Runs a complete EPANET simulation.
        /// </summary>
        /// <param name="f1">name of the input file</param>
        /// <param name="f2">name of an output report file</param>
        /// <param name="f3">name of an output output file </param>
        /// <param name="vfunc">pointer to a user-supplied function which accepts a character string as its argument</param>
        /// <returns>Returns an error code.</returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENepanet(string f1, string f2, string f3, UserSuppliedFunction vfunc);

        /// <summary>
        /// Opens the Toolkit to analyze a particular distribution system.
        /// </summary>
        /// <param name="param0">name of the input file</param>
        /// <param name="param1">name of an output report file</param>
        /// <param name="param2">name of an output output file</param>
        /// <returns>Returns an error code.</returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENopen(string param0, string param1, string param2);

        /// <summary>
        /// Writes all current network input data to a file using the format of an EPANET input file.
        /// </summary>
        /// <param name="filename">name of the file where data is saved.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsaveinpfile(string filename);

        /// <summary>
        /// Closes down the Toolkit system (including all files being processed).
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENclose();

        /// <summary>
        /// Runs a complete hydraulic simulation with results
        /// for all time periods written to the binary Hydraulics file.
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsolveH();

        /// <summary>
        /// Transfers results of a hydraulic simulation from the binary Hydraulics file to the binary Output file,
        /// where results are only reported at uniform reporting intervals.
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsaveH();

        /// <summary>
        /// Opens the hydraulics analysis system.
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENopenH();

        /// <summary>
        /// Initializes storage tank levels, link status and settings,
        /// and the simulation clock time prior to running a hydraulic analysis.
        /// </summary>
        /// <param name="saveflag">0-1 flag indicating if hydraulic results will be saved to the hydraulics file.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENinitH(int saveflag);

        /// <summary>
        /// Runs a single period hydraulic analysis, retrieving the current simulation clock time t.
        /// </summary>
        /// <param name="t">current simulation clock time in seconds.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENrunH(ref long t);

        /// <summary>
        /// Determines the length of time until the next hydraulic event occurs in an extended period simulation.
        /// </summary>
        /// <param name="tstep">time (in seconds) until next hydraulic event occurs or
        /// 0 if at the end of the simulation period.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENnextH(ref long tstep);

        /// <summary>
        ///  Closes the hydraulic analysis system, freeing all allocated memory.
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENcloseH();

        /// <summary>
        /// Saves the current contents of the binary hydraulics file to a file.
        /// </summary>
        /// <param name="fname">name of the file where the hydraulics results should be saved.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsavehydfile(string fname);

        /// <summary>
        /// Uses the contents of the specified file as the current binary hydraulics file.
        /// </summary>
        /// <param name="fname">name of the file containing hydraulic analysis results for the current network.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENusehydfile(string fname);

        /// <summary>
        /// Runs a complete water quality simulation with results at uniform reporting
        /// intervals written to EPANET's binary Output file.
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsolveQ();

        /// <summary>
        /// Opens the water quality analysis system.
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENopenQ();

        /// <summary>
        /// Initializes water quality and the simulation clock time prior to running a water quality analysis.
        /// </summary>
        /// <param name="saveflag">0-1 flag indicating if analysis results
        /// should be saved to EPANET's binary output file at uniform reporting periods.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENinitQ(int saveflag);

        /// <summary>
        /// Makes available the hydraulic and water quality results that occur
        /// at the start of the next time period of a water quality analysis,
        /// where the start of the period is returned in t.
        /// </summary>
        /// <param name="t">current simulation clock time in seconds.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENrunQ(ref long t);

        /// <summary>
        /// Advances the water quality simulation to the start of the next hydraulic time period.
        /// </summary>
        /// <param name="tstep">time (in seconds) until next hydraulic event occurs or
        /// 0 if at the end of the simulation period.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENnextQ(ref long tstep);

        /// <summary>
        /// Advances the water quality simulation one water quality time step.
        /// The time remaining in the overall simulation is returned in tleft.
        /// </summary>
        /// <param name="tleft">seconds remaining in the overall simulation duration.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENstepQ(ref long tleft);

        /// <summary>
        /// Closes the water quality analysis system, freeing all allocated memory.
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENcloseQ();

        /// <summary>
        /// Writes a line of text to the EPANET report file.
        /// </summary>
        /// <param name="line">text to be written to file.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENwriteline(string line);

        /// <summary>
        /// Writes a formatted text report on simulation results to the Report file.
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENreport();

        /// <summary>
        /// Clears any report formatting commands that either appeared in the
        /// [REPORT] section of the EPANET Input file or were issued with the ENsetreport function.
        /// </summary>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENresetreport();

        /// <summary>
        /// Issues a report formatting command.
        /// Formatting commands are the same as used in the
        /// [REPORT] section of the EPANET Input file.
        /// </summary>
        /// <param name="command">text of a report formatting command.</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetreport(string command);

        /// <summary>
        /// Retrieves the parameters of a simple control statement.
        /// The index of the control is specified in cindex and
        /// the remaining arguments return the control's parameters.
        /// </summary>
        /// <param name="cindex">control statement index</param>
        /// <param name="ctype">control type code</param>
        /// <param name="lindex">index of link being controlled</param>
        /// <param name="setting">value of the control setting</param>
        /// <param name="nindex">index of controlling node</param>
        /// <param name="level">value of controlling water level or
        /// pressure for level controls or of time of control action (in seconds)
        /// for time-based controls</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetcontrol(int cindex, ref int ctype, ref int lindex,
            ref float setting, ref int nindex, ref float level);

        /// <summary>
        /// Retrieves the number of network components of a specified type.
        /// </summary>
        /// <param name="countcode">component code</param>
        /// <param name="count">number of countcode components in the network</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetcount(int countcode, ref int count);

        /// <summary>
        /// Retrieves the value of a particular analysis option.
        /// </summary>
        /// <param name="optioncode">an option code</param>
        /// <param name="value">an option value</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetoption(int optioncode, ref float value);

        /// <summary>
        ///  Retrieves the value of a specific analysis time parameter.
        /// </summary>
        /// <param name="paramcode">time parameter code</param>
        /// <param name="timevalue">value of time parameter in seconds</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgettimeparam(int paramcode, ref long timevalue);

        /// <summary>
        /// Retrieves a code number indicating the units used to express all flow rates.
        /// </summary>
        /// <param name="unitscode">value of a flow units code number</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetflowunits(ref int unitscode);

        /// <summary>
        /// Retrieves the index of a particular time pattern.
        /// </summary>
        /// <param name="id">pattern ID label</param>
        /// <param name="index">pattern index</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetpatternindex(string id, ref int index);

        /// <summary>
        /// Retrieves the ID label of a particular time pattern.
        /// </summary>
        /// <param name="index">pattern index</param>
        /// <param name="id">ID label of pattern</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetpatternid(int index, string id);

        /// <summary>
        /// Retrieves the number of time periods in a specific time pattern.
        /// </summary>
        /// <param name="index">pattern index</param>
        /// <param name="len">number of time periods in the pattern</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetpatternlen(int index, ref int len);

        /// <summary>
        /// Retrieves the multiplier factor for a specific time period in a time pattern.
        /// </summary>
        /// <param name="index">time pattern index</param>
        /// <param name="period">period within time pattern</param>
        /// <param name="value">multiplier factor for the period</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetpatternvalue(int index, int period, ref float value);

        /// <summary>
        /// Retrieves the type of water quality analysis called for.
        /// </summary>
        /// <param name="qualcode">water quality analysis code</param>
        /// <param name="tracenode">index of node traced in a source tracing analysis</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetqualtype(ref int qualcode, ref int tracenode);

        /// <summary>
        /// Retrieves the text of the message associated with a particular error or warning code.
        /// </summary>
        /// <param name="errcode">error or warning code</param>
        /// <param name="errmsg">text of the error or warning message for errcode</param>
        /// <param name="nchar">maximum number of characters that errmsg can hold</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgeterror(int errcode, string errmsg, int nchar);

        /// <summary>
        ///  Retrieves the index of a node with a specified ID.
        /// </summary>
        /// <param name="id">node ID label</param>
        /// <param name="index">node index</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetnodeindex(string id, ref int index);

        /// <summary>
        /// Retrieves the ID label of a node with a specified index
        /// </summary>
        /// <param name="index">node index</param>
        /// <param name="id">ID label of node</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetnodeid(int index, string id);


        /// <summary>
        /// Retrieves the ID label of a node with a specified index
        /// </summary>
        /// <param name="index">node index</param>
        /// <param name="id">ID label of node</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, EntryPoint = "ENgetnodeid")]
        public static extern int ENgetnodeid2(int index, StringBuilder id);


        /// <summary>
        /// Retrieves the node-type code for a specific node.
        /// </summary>
        /// <param name="index">node index</param>
        /// <param name="typecode">node-type code</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetnodetype(int index, ref int typecode);

        /// <summary>
        /// Retrieves the value of a specific link parameter.
        /// </summary>
        /// <param name="index">node index</param>
        /// <param name="paramcode">parameter code</param>
        /// <param name="value">parameter value</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetnodevalue(int index, int paramcode, ref float value);

        /// <summary>
        /// Retrieves the index of a link with a specified ID.
        /// </summary>
        /// <param name="id">link ID label</param>
        /// <param name="index">link index</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetlinkindex(string id, ref int index);

        /// <summary>
        /// Retrieves the ID label of a link with a specified index.
        /// </summary>
        /// <param name="index">link index</param>
        /// <param name="id">ID label of link</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetlinkid(int index, StringBuilder id);


        /// <summary>
        /// Retrieves the ID label of a link with a specified index.
        /// </summary>
        /// <param name="index">link index</param>
        /// <param name="id">ID label of link</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, EntryPoint = "ENgetlinkid")]
        public static extern int ENgetlinkid2(int index, StringBuilder id);

        /// <summary>
        ///  Retrieves the link-type code for a specific link.
        /// </summary>
        /// <param name="index">link index</param>
        /// <param name="typecode">link-type code</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetlinktype(int index, ref int typecode);

        /// <summary>
        /// Retrieves the indexes of the end nodes of a specified link.
        /// </summary>
        /// <param name="index">link index</param>
        /// <param name="fromnode">index of node at start of link</param>
        /// <param name="tonode">index of node at end of link</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetlinknodes(int index, ref int fromnode, ref int tonode);

        /// <summary>
        /// Retrieves the value of a specific link parameter.
        /// </summary>
        /// <param name="index">link index</param>
        /// <param name="paramcode">parameter code</param>
        /// <param name="value">parameter value</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetlinkvalue(int index, int paramcode, ref float value);

        /// <summary>
        /// Retrieves version.
        /// </summary>
        /// <param name="version">Version</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetversion(ref int version);

        /// <summary>
        /// Sets the parameters of a simple control statement.
        /// </summary>
        /// <param name="cindex">control statement index</param>
        /// <param name="ctype">control type code</param>
        /// <param name="lindex">index of link being controlled</param>
        /// <param name="setting">value of the control setting</param>
        /// <param name="nindex">index of controlling node</param>
        /// <param name="level">value of controlling water level or pressure
        /// for level controls or of time of control action (in seconds)
        /// for time-based controls</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetcontrol(int cindex, int ctype, int lindex,
            float setting, int nindex, float level);

        /// <summary>
        ///  Sets the value of a parameter for a specific node.
        /// </summary>
        /// <param name="index">node index</param>
        /// <param name="paramcode">parameter code</param>
        /// <param name="value">parameter value</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetnodevalue(int index, int paramcode, float value);

        /// <summary>
        ///  Sets the value of a parameter for a specific link.
        /// </summary>
        /// <param name="index">link index</param>
        /// <param name="paramcode">parameter code</param>
        /// <param name="value">parameter value</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetlinkvalue(int index, int paramcode, float value);

        /// <summary>
        /// Adds a new time pattern to the network.
        /// </summary>
        /// <param name="id">ID label of pattern</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENaddpattern(string id);

        /// <summary>
        ///  Sets all of the multiplier factors for a specific time pattern.
        /// </summary>
        /// <param name="index">time pattern index</param>
        /// <param name="factors">multiplier factors for the entire pattern</param>
        /// <param name="nfactors">number of factors in the pattern</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetpattern(int index, float[] factors, int nfactors);

        /// <summary>
        ///  Sets the multiplier factor for a specific period within a time pattern.
        /// </summary>
        /// <param name="index">time pattern index</param>
        /// <param name="period">period within time pattern</param>
        /// <param name="value">multiplier factor for the period</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetpatternvalue(int index, int period, float value);

        /// <summary>
        /// Sets the value of a time parameter.
        /// </summary>
        /// <param name="paramcode">time parameter code</param>
        /// <param name="timevalue">value of time parameter in seconds</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsettimeparam(int paramcode, long timevalue);

        /// <summary>
        /// Sets the value of a particular analysis option.
        /// </summary>
        /// <param name="optioncode">an option code</param>
        /// <param name="value">an option value</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetoption(int optioncode, float value);

        /// <summary>
        ///  Sets the level of hydraulic status reporting.
        /// </summary>
        /// <param name="statuslevel">level of status reporting</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetstatusreport(int statuslevel);

        /// <summary>
        /// Sets the type of water quality analysis called for.
        /// </summary>
        /// <param name="qualcode">water quality analysis code</param>
        /// <param name="chemname">name of the chemical being analyzed</param>
        /// <param name="chemunits">units that the chemical is measured in</param>
        /// <param name="tracenode">ID of node traced in a source tracing analysis</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetqualtype(int qualcode, string chemname, string chemunits, string tracenode);


        /// <summary>
        ///  Gets the value of a parameter for a Model
        /// </summary>
        /// <param name="modeltype">Type of demand model</param>
        /// <param name="pmin">Pressure below which there is no demand</param>
        /// <param name="preq">Pressure required to deliver full demand</param>
        /// <param name="pexp">Pressure exponent in demand function</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENgetdemandmodel(ref int modeltype, ref double pmin, ref double preq, ref double pexp);


        /// <summary>
        ///  Sets the value of a parameter for a Model
        /// </summary>
        /// <param name="modeltype">Type of demand model</param>
        /// <param name="pmin">Pressure below which there is no demand</param>
        /// <param name="preq">Pressure required to deliver full demand</param>
        /// <param name="pexp">Pressure exponent in demand function</param>
        /// <returns></returns>
        [DllImport(EPANETDLL, CharSet = CharSet.Ansi)]
        public static extern int ENsetdemandmodel(int modeltype, double pmin, double preq, double pexp);


        #endregion

        #region EPANETMSX Imports
        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXopen(string filename);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsolveH();



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXusehydfile(string filename);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsolveQ();



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXinit(int saveflag);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXstep(ref long t, ref long tleft);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsaveoutfile(string filename);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsavemsxfile(string filename);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXreport();


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXclose();


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetindex(int type, string id, ref int idx);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetIDlen(int type, int idx, ref int len);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetID(int type, int idx, ref StringBuilder id, int len);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetcount(int type, ref int cnt);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetspecies(int inx, ref int type, ref string units, ref double aTol, ref double rTol);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetconstant(int idx, ref double val);




        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetparameter(int type, int dix, int param, ref double val);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetsource(int node, int species, ref int type, ref double lvl, ref int pat);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetpatternlen(int pat, ref int len);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetpatternvalue(int pat, int period, ref double val);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetinitqual(int type, int idx, int species, ref double val);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgetqual(int type, int idx, int species, ref double val);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXgeterror(int code, ref string msg, int len);



        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsetconstant(int idx, double val);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsetparameter(int type, int idx, int param, double val);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsetinitqual(int type, int idx, int species, double val);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsetsource(int node, int species, int type, double lvl, int pat);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsetpatternvalue(int pat, int period, double val);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXsetpattern(int pat, double[] mult, int len);


        [DllImport(EPANETMSX, CharSet = CharSet.Ansi)]
        public static extern int MSXaddpattern(ref string id);

        #endregion
    }
}
