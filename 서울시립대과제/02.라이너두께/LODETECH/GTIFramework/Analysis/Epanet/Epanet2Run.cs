﻿using System;
using System.Collections;
using System.Data;
using System.Text;

namespace GTIFramework.Analysis.Epanet
{
    public class Epanet2Run
    {
        #region Epanet2 관련
        private const int EN_ELEVATION = 0;    ///Node parameters
        private const int EN_BASEDEMAND = 1;
        private const int EN_PATTERN = 2;
        private const int EN_EMITTER = 3;
        private const int EN_INITQUAL = 4;
        private const int EN_SOURCEQUAL = 5;
        private const int EN_SOURCEPAT = 6;
        private const int EN_SOURCETYPE = 7;
        private const int EN_TANKLEVEL = 8;
        private const int EN_DEMAND = 9;
        private const int EN_HEAD = 10;
        private const int EN_PRESSURE = 11;
        private const int EN_QUALITY = 12;
        private const int EN_SOURCEMASS = 13;
        private const int EN_INITVOLUME = 14;
        private const int EN_MIXMODEL = 15;
        private const int EN_MIXZONEVOL = 16;

        private const int EN_TANKDIAM = 17;
        private const int EN_MINVOLUME = 18;
        private const int EN_VOLCURVE = 19;
        private const int EN_MINLEVEL = 20;
        private const int EN_MAXLEVEL = 21;
        private const int EN_MIXFRACTION = 22;
        private const int EN_TANK_KBULK = 23;

        public const int EN_DEMANDDEFICIT = 27;

        private const int EN_DIAMETER = 0;    ///Link parameters
        private const int EN_LENGTH = 1;
        private const int EN_ROUGHNESS = 2;
        private const int EN_MINORLOSS = 3;
        private const int EN_INITSTATUS = 4;
        private const int EN_INITSETTING = 5;
        private const int EN_KBULK = 6;
        private const int EN_KWALL = 7;
        private const int EN_FLOW = 8;
        private const int EN_VELOCITY = 9;
        private const int EN_HEADLOSS = 10;
        private const int EN_STATUS = 11;
        private const int EN_SETTING = 12;
        private const int EN_ENERGY = 13;

        private const int EN_DURATION = 0;    ///Time parameters
        private const int EN_HYDSTEP = 1;
        private const int EN_QUALSTEP = 2;
        private const int EN_PATTERNSTEP = 3;
        private const int EN_PATTERNSTART = 4;
        private const int EN_REPORTSTEP = 5;
        private const int EN_REPORTSTART = 6;
        private const int EN_RULESTEP = 7;
        private const int EN_STATISTIC = 8;
        private const int EN_PERIODS = 9;

        private const int EN_NODECOUNT = 0;    ///Component counts
        private const int EN_TANKCOUNT = 1;
        private const int EN_LINKCOUNT = 2;
        private const int EN_PATCOUNT = 3;
        private const int EN_CURVECOUNT = 4;
        private const int EN_CONTROLCOUNT = 5;

        private const int EN_JUNCTION = 0;    ///Node types
        private const int EN_RESERVOIR = 1;
        private const int EN_TANK = 2;

        private const int EN_CVPIPE = 0;    ///Link types
        private const int EN_PIPE = 1;
        private const int EN_PUMP = 2;
        private const int EN_PRV = 3;
        private const int EN_PSV = 4;
        private const int EN_PBV = 5;
        private const int EN_FCV = 6;
        private const int EN_TCV = 7;
        private const int EN_GPV = 8;

        private const int EN_NONE = 0;   ///Quality analysis types
        private const int EN_CHEM = 1;
        private const int EN_AGE = 2;
        private const int EN_TRACE = 3;

        private const int EN_CONCEN = 0;    ///Source quality types
        private const int EN_MASS = 1;
        private const int EN_SETPOINT = 2;
        private const int EN_FLOWPACED = 3;

        private const int EN_CFS = 0;    ///Flow units types
        private const int EN_GPM = 1;
        private const int EN_MGD = 2;
        private const int EN_IMGD = 3;
        private const int EN_AFD = 4;
        private const int EN_LPS = 5;
        private const int EN_LPM = 6;
        private const int EN_MLD = 7;
        private const int EN_CMH = 8;
        private const int EN_CMD = 9;

        private const int EN_TRIALS = 0;   ///Misc. options             
        private const int EN_ACCURACY = 1;
        private const int EN_TOLERANCE = 2;
        private const int EN_EMITEXPON = 3;
        private const int EN_DEMANDMULT = 4;

        private const int EN_LOWLEVEL = 0;   ///Control types             
        private const int EN_HILEVEL = 1;
        private const int EN_TIMER = 2;
        private const int EN_TIMEOFDAY = 3;

        private const int EN_AVERAGE = 1;   ///Time statistic types.     
        private const int EN_MINIMUM = 2;
        private const int EN_MAXIMUM = 3;
        private const int EN_RANGE = 4;

        private const int EN_MIX1 = 0;   ///Tank mixing models        
        private const int EN_MIX2 = 1;
        private const int EN_FIFO = 2;
        private const int EN_LIFO = 3;

        private const int EN_NOSAVE = 0;   ///Save-results-to-file flag 
        private const int EN_SAVE = 1;
        private const int EN_INITFLOW = 10;  ///Re-initialize flow flag
        #endregion

        #region EPANET_MSX 관련
        //These are codes used by the DLL functions(EPANET_MSX)


        /// <summary>
        /// obj type 종류
        /// </summary>
        public const int MSX_NODE = 0;
        public const int MSX_LINK = 1;                  // 사용안하는것 같음.
        public const int MSX_TANK = 2;


        /// <summary>
        /// type 종류
        /// </summary>
        public const int MSX_SPECIES = 3;
        public const int MSX_TERM = 4;                  // 사용안하는것 같음.
        public const int MSX_PARAMETER = 5;
        public const int MSX_CONSTANT = 6;
        public const int MSX_PATTERN = 7;

        /// <summary>
        /// species type 종류
        /// </summary>
        public const int MSX_BULK = 0;
        public const int MSX_WALL = 1;

        /// <summary>
        /// source 종류 
        /// </summary>
        public const int MSX_NOSOURCE = -1;
        public const int MSX_CONCEN = 0;
        public const int MSX_MASS = 1;
        public const int MSX_SETPOINT = 2;
        public const int MSX_FLOWPACED = 3;

        #endregion

        string DirInp = string.Empty;
        string DirMSX = string.Empty;
        string DirRpt = string.Empty;
        string DirOut = string.Empty;

        public Epanet2Run() { }

        /// <summary>
        /// 수질 염소 실시간
        /// </summary>
        /// <param name="strINPpath">inp 파일 경로</param>
        public Hashtable EpanetQcl(string strINPpath)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            try
            {
                //inp파일 같은 경로에 rpt, out 파일 생성 경로
                DirInp = strINPpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_realtime_2.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_realtime_2.out";

                //Use of epanet functions
                Err = Epanet2md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit

                #region // 수리해석 실행(수질해석을 돌리기 위하여 먼저 수리해석을 돌려야 함) 실시간, 시간 구분
                Err = Epanet2md.ENsolveH();
                Err = Epanet2md.ENopenQ();
                Err = Epanet2md.ENinitQ(0);
                #endregion

                // t :  관망해석시 현재 시뮬레이션 클럭시간(초)(current simulation clock time in seconds). ENrunH에서 t 데이터타입이 long로 지정됨.
                long t = 1;

                Hashtable htAnalysisResult = new Hashtable();

                Epanet2md.ENrunQ(ref t);

                Hashtable htTimeResult = new Hashtable();
                //htTimeResult.Add("Node", ExtractNodeAnalysisResultHashtable());
                //htTimeResult.Add("Link", ExtractLinkAnalysisResultHashtable());
                htTimeResult.Add("Node", ExtractNodeAnalysisResultTable());
                htTimeResult.Add("Link", ExtractLinkAnalysisResultTable());

                htAnalysisResult.Add("00:00", htTimeResult);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
            finally
            {
                //수질해석결과 Close
                Epanet2md.ENcloseQ();
                //Close the toolkit
                Epanet2md.ENclose();
            }
        }

        /// <summary>
        /// 수질 염소 시간변화
        /// </summary>
        /// <param name="strINPpath">inp 파일 경로</param>
        /// <param name="htqual">배수지 수질 초기값 key:ID value:수질 초기값</param>
        public Hashtable EpanetQcl(string strINPpath, Hashtable htqual)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            int iIndex = 9999;

            try
            {
                //inp파일 같은 경로에 rpt, out 파일 생성 경로
                DirInp = strINPpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_time_2.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_time_2.out";

                //Use of epanet functions
                Err = Epanet2md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit


                #region // 수리해석 실행(수질해석을 돌리기 위하여 먼저 수리해석을 돌려야 함) 실시간, 시간 구분
                Err = Epanet2md.ENsolveH();
                Err = Epanet2md.ENopenQ();



                foreach (DictionaryEntry qual in htqual)
                {
                    //inp아이디로 해석객체 index 선별
                    Epanet2md.ENgetnodeindex(qual.Key.ToString(), ref iIndex);
                    //배수지 초기 수질값 세팅
                    Epanet2md.ENsetnodevalue(iIndex, EN_INITQUAL, float.Parse(qual.Value.ToString()));
                }

                Err = Epanet2md.ENinitQ(0);
                #endregion

                // t :  관망해석시 현재 시뮬레이션 클럭시간(초)(current simulation clock time in seconds). ENrunH에서 t 데이터타입이 long로 지정됨.
                long t = 0;

                Hashtable htAnalysisResult = new Hashtable();

                //(수질시간) 구분
                long tstep = 0;
                long lReportStep = 0;

                Epanet2md.ENgettimeparam(EN_REPORTSTEP, ref lReportStep);

                do
                {
                    Epanet2md.ENrunQ(ref t);

                    if ((t % lReportStep) == 0)
                    {
                        Hashtable htTimeResult = new Hashtable();
                        //htTimeResult.Add("Node", ExtractNodeAnalysisResultHashtable());
                        //htTimeResult.Add("Link", ExtractLinkAnalysisResultHashtable());
                        htTimeResult.Add("Node", ExtractNodeAnalysisResultTable());
                        htTimeResult.Add("Link", ExtractLinkAnalysisResultTable());

                        //해석시간을 key로 하여 node,junction 해석결과를 저장
                        htAnalysisResult.Add((Convert.ToString(t / 60 / 60)).PadLeft(2, '0') + ":" + Convert.ToString((t - (t / 60 / 60 * 60 * 60)) / 60).PadLeft(2, '0'), htTimeResult);
                    }

                    Epanet2md.ENnextQ(ref tstep);
                }
                while (tstep > 0);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
            finally
            {
                //수질해석결과 Close
                Epanet2md.ENcloseQ();
                //Close the toolkit
                Epanet2md.ENclose();
            }
        }

        /// <summary>
        /// 수질 체류
        /// </summary>
        /// <param name="strINPpath">inp 파일 경로</param>
        public Hashtable EpanetQage(string strINPpath)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            try
            {
                //inp파일 같은 경로에 rpt, out 파일 생성 경로
                DirInp = strINPpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_age_2.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_age_2.out";

                //Use of epanet functions
                Err = Epanet2md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit

                Epanet2md.ENsetqualtype(EN_AGE, "age", "", "1"); //EN_AGE 와 Node ID 설정(Index 아님)

                #region // 수리해석 실행(수질해석을 돌리기 위하여 먼저 수리해석을 돌려야 함) 실시간, 시간 구분
                Err = Epanet2md.ENsolveH();
                Err = Epanet2md.ENopenQ();
                Err = Epanet2md.ENinitQ(0);
                #endregion

                // t :  관망해석시 현재 시뮬레이션 클럭시간(초)(current simulation clock time in seconds). ENrunH에서 t 데이터타입이 long로 지정됨.
                long t = 1;

                Hashtable htAnalysisResult = new Hashtable();

                Epanet2md.ENrunQ(ref t);

                Hashtable htTimeResult = new Hashtable();
                //htTimeResult.Add("Node", ExtractNodeAnalysisResultHashtable());
                //htTimeResult.Add("Link", ExtractLinkAnalysisResultHashtable());
                htTimeResult.Add("Node", ExtractNodeAnalysisResultTable());
                htTimeResult.Add("Link", ExtractLinkAnalysisResultTable());

                htAnalysisResult.Add("00:00", htTimeResult);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
            finally
            {
                //수질해석결과 Close
                Epanet2md.ENcloseQ();
                //Close the toolkit
                Epanet2md.ENclose();
            }
        }

        /// <summary>
        /// 수질 체류
        /// </summary>
        /// <param name="strINPpath">inp 파일 경로</param>
        public Hashtable EpanetQtrace(string strINPpath)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            try
            {
                //inp파일 같은 경로에 rpt, out 파일 생성 경로
                DirInp = strINPpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_trace_2.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_trace_2.out";

                //Use of epanet functions
                Err = Epanet2md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit

                Epanet2md.ENsetqualtype(EN_TRACE, "trace", "", "1");      //EN_AGE 와 Node ID 설정(Index 아님)

                #region // 수리해석 실행(수질해석을 돌리기 위하여 먼저 수리해석을 돌려야 함) 실시간, 시간 구분
                Err = Epanet2md.ENsolveH();
                Err = Epanet2md.ENopenQ();
                Err = Epanet2md.ENinitQ(0);
                #endregion

                // t :  관망해석시 현재 시뮬레이션 클럭시간(초)(current simulation clock time in seconds). ENrunH에서 t 데이터타입이 long로 지정됨.
                long t = 1;

                Hashtable htAnalysisResult = new Hashtable();

                Epanet2md.ENrunQ(ref t);

                Hashtable htTimeResult = new Hashtable();
                //htTimeResult.Add("Node", ExtractNodeAnalysisResultHashtable());
                //htTimeResult.Add("Link", ExtractLinkAnalysisResultHashtable());
                htTimeResult.Add("Node", ExtractNodeAnalysisResultTable());
                htTimeResult.Add("Link", ExtractLinkAnalysisResultTable());

                htAnalysisResult.Add("00:00", htTimeResult);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
            finally
            {
                //수질해석결과 Close
                Epanet2md.ENcloseQ();
                //Close the toolkit
                Epanet2md.ENclose();
            }
        }

        /// <summary>
        /// MSX
        /// </summary>
        /// <param name="strINPpath"></param>
        /// <param name="strMSXpath"></param>
        /// <returns></returns>
        public Hashtable EpanetMSX(string strINPpath, string strMSXpath, string[] _strarryMSXType)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            float fqual = 9999;
            int iIndex = 9999;

            try
            {
                DirInp = strINPpath;
                DirMSX = strMSXpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_msx_2.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_msx_2.out";

                #region // MSX(MSX을 돌리기 위하여 먼저 수리해석을 돌려야 함)
                Err = Epanet2md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit
                Err = Epanet2md.MSXopen(DirMSX);

                Err = Epanet2md.MSXsolveH();
                Err = Epanet2md.MSXsolveQ();
                Err = Epanet2md.MSXreport();
                #endregion

                int iNodeCnt = 0;                                                //노드의 총 개수
                int iNodeType = 0;                                               //노드의 타입
                StringBuilder sbNodeID = new StringBuilder();                    //노드의 아이디


                int iLinkCnt = 0;                                                //링크의 총 개수
                int iLinkType = 0;                                               //링크의 타입
                StringBuilder sbLinkID = new StringBuilder();                    //링크의 아이디

                string[] strarryMSXType = _strarryMSXType;                       //화학종... 추후에 msx에서 어떻게 파싱해서 가져올지???
                //string[] strarryMSXType = {"AS5"};                       //화학종... 추후에 msx에서 어떻게 파싱해서 가져올지???

                long t = 0;                                                      //분석할 시간 간격(MSX 파일에서 [OPTIONS] TIMESTEP)
                long tstep = 0;                                                  //분석할 총 시간(INP 파일에서 [TIMES]에서 Duration(시간)값을 초단위로 변환, 루프돌면서 t만큼 감소)

                int n = 0;
                int s = 0;

                double c = 0.0;
                double cMax = 0.0;

                DataSet dsNode = new DataSet();
                foreach (string stritem in strarryMSXType)
                {
                    DataTable dtNode = new DataTable() { TableName = stritem };
                    dtNode.Columns.Add("TYPE");
                    dtNode.Columns.Add("ID");
                    dtNode.Columns.Add("TIMESTEP");
                    dtNode.Columns.Add(stritem);
                    dsNode.Tables.Add(dtNode);
                }
                DataRow drNodeVal = null;

                DataSet dsLink = new DataSet();
                foreach (string stritem in strarryMSXType)
                {
                    DataTable dtLink = new DataTable() { TableName = stritem };
                    dtLink.Columns.Add("TYPE");
                    dtLink.Columns.Add("ID");
                    dtLink.Columns.Add("TIMESTEP");
                    dtLink.Columns.Add(stritem);
                    dsLink.Tables.Add(dtLink);
                }
                DataRow drLinkVal = null;

                #region 노드값 가져오기
                Err = Epanet2md.ENgetcount(EN_NODECOUNT, ref iNodeCnt);                //NODE의 총 개수 반환

                for (int i = 1; i <= iNodeCnt; i++)
                {
                    Err = Epanet2md.MSXinit(0);
                    Err = Epanet2md.ENgetnodeid2(i, sbNodeID);                         //NODE의 인덱스를 가지고 NODEID값을 반환
                    Err = Epanet2md.ENgetnodetype(i, ref iNodeType);                   //NODE의 인덱스를 가지고 NODETYPE값을 반환(JUNCTION-0, RESERVOIR-1, TANK-2) //Err = Epanet.ENgetnodeindex(sbNodeID.ToString(), ref n);          

                    do
                    {
                        Err = Epanet2md.MSXstep(ref t, ref tstep);

                        foreach (string stritem in strarryMSXType)
                        {
                            Err = Epanet2md.MSXgetindex(MSX_SPECIES, stritem, ref s);       //화학물질값 적용후 MSX에서 Index값 반환
                            Err = Epanet2md.MSXgetqual(MSX_NODE, i, s, ref c);             //첫번째인자 : 노드인지링크인지 명시, 두번째인자 : 노드의 인덱스, 세번째인자 : MSX에서의 노드 인덱스
                            drNodeVal = dsNode.Tables[stritem].NewRow();
                            drNodeVal["TYPE"] = iNodeType.ToString();
                            drNodeVal["ID"] = sbNodeID.ToString();
                            drNodeVal["TIMESTEP"] = t.ToString();
                            drNodeVal[stritem] = c.ToString();
                            dsNode.Tables[stritem].Rows.Add(drNodeVal);
                        }

                        if (c > cMax) cMax = c;
                    }
                    while (tstep > 0 && Err == 0);
                }
                #endregion

                #region 링크값 가져오기
                Err = Epanet2md.ENgetcount(EN_LINKCOUNT, ref iLinkCnt);                //LINK의 총 개수 반환

                for (int i = 1; i <= iLinkCnt; i++)
                {
                    Err = Epanet2md.MSXinit(0);
                    Err = Epanet2md.ENgetlinkid2(i, sbLinkID);                         //LINK의 인덱스를 가지고 NODEID값을 반환
                    Err = Epanet2md.ENgetlinktype(i, ref iLinkType);                   //LINK의 인덱스를 가지고 NODETYPE값을 반환 //Err = Epanet.ENgetnodeindex(sbLinkID.ToString(), ref n);          

                    do
                    {
                        Err = Epanet2md.MSXstep(ref t, ref tstep);

                        foreach (string stritem in strarryMSXType)
                        {
                            Err = Epanet2md.MSXgetindex(MSX_SPECIES, stritem, ref s);       //화학물질값 적용후 MSX에서 Index값 반환
                            Err = Epanet2md.MSXgetqual(MSX_LINK, i, s, ref c);             //첫번째인자 : 노드인지링크인지 명시, 두번째인자 : 노드의 인덱스, 세번째인자 : MSX에서의 노드 인덱스
                            drLinkVal = dsLink.Tables[stritem].NewRow();
                            drLinkVal["TYPE"] = iLinkType.ToString();
                            drLinkVal["ID"] = sbLinkID.ToString();
                            drLinkVal["TIMESTEP"] = t.ToString();
                            drLinkVal[stritem] = c.ToString();
                            dsLink.Tables[stritem].Rows.Add(drLinkVal);
                        }

                        if (c > cMax) cMax = c;
                    }
                    while (tstep > 0 && Err == 0);
                }
                #endregion

                //Close the MSX toolkit
                Epanet2md.MSXclose();

                //Close the toolkit
                Epanet2md.ENclose();

                Hashtable htAnalysisResult = new Hashtable();
                htAnalysisResult.Add("Node", dsNode);
                htAnalysisResult.Add("Link", dsLink);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
        }

        /// <summary>
        /// Epanet Version
        /// </summary>
        public int EpanetVersion()
        {
            int iversion = 0;
            Epanet2md.ENgetversion(ref iversion);
            return iversion;
        }

        #region 결과 반환 관련
        /// <summary>
        /// 관망해석후 노드에 대한 결과값 반환
        /// </summary>
        /// <returns></returns>
        private Hashtable ExtractNodeAnalysisResultHashtable()
        {

            //전체 node 해석결과를 저장할 Hashtable
            Hashtable htNodeResult = new Hashtable();

            ArrayList alJunctionResult = new ArrayList();            //절점 해석결과
            ArrayList alReserviorResult = new ArrayList();           //배수지 해석결과
            ArrayList alTankResult = new ArrayList();                //tank 해석결과

            //Node Count 발췌
            int iNodeCount = 0;
            Epanet2md.ENgetcount(EN_NODECOUNT, ref iNodeCount);


            //Node 해석결과 변수
            float fElevationValue = 0;
            float fBaseDemandValue = 0;
            float fPatternValue = 0;
            float fEmitterValue = 0;
            float fInitQualValue = 0;
            float fSourceQualValue = 0;
            float fSourcePatValue = 0;
            float fSourceTypeValue = 0;
            float fTankLevelValue = 0;
            float fDemandValue = 0;
            float fHeadValue = 0;
            float fPressureValue = 0;
            float fQualityValue = 0;
            float fSourceMassValue = 0;
            float fInitVolumeValue = 0;
            float fMixModelValue = 0;
            float fMixZoneVolValue = 0;
            float fTankDiamValue = 0;
            float fMinVolumeValue = 0;
            float fVolCurveValue = 0;
            float fMinLevelValue = 0;
            float fMaxLevelValue = 0;
            float fMixFractionValue = 0;
            float fTankKbulkValue = 0;

            for (int i = 1; i <= iNodeCount; i++)
            {
                // NodeID의 기본값을 string.Empty로 안했을경우, 메모리 오류 발생
                StringBuilder sbNodeID = new StringBuilder();

                int iNodeType = 0;

                Epanet2md.ENgetnodeid2(i, sbNodeID);
                Epanet2md.ENgetnodetype(i, ref iNodeType);

                Epanet2md.ENgetnodevalue(i, EN_ELEVATION, ref fElevationValue);          //Elevation 
                Epanet2md.ENgetnodevalue(i, EN_BASEDEMAND, ref fBaseDemandValue);        //Base demand 
                Epanet2md.ENgetnodevalue(i, EN_PATTERN, ref fPatternValue);              //Demand pattern index 
                Epanet2md.ENgetnodevalue(i, EN_EMITTER, ref fEmitterValue);              //Emitter coeff. 
                Epanet2md.ENgetnodevalue(i, EN_INITQUAL, ref fInitQualValue);            //Initial quality 
                Epanet2md.ENgetnodevalue(i, EN_SOURCEQUAL, ref fSourceQualValue);        //Source quality 
                Epanet2md.ENgetnodevalue(i, EN_SOURCEPAT, ref fSourcePatValue);          //Source pattern index
                Epanet2md.ENgetnodevalue(i, EN_SOURCETYPE, ref fSourceTypeValue);        //Source type (See note below) 
                Epanet2md.ENgetnodevalue(i, EN_TANKLEVEL, ref fTankLevelValue);          //Initial water level in tank 
                Epanet2md.ENgetnodevalue(i, EN_DEMAND, ref fDemandValue);                //Actual demand 
                Epanet2md.ENgetnodevalue(i, EN_HEAD, ref fHeadValue);                    //Hydraulic head 
                Epanet2md.ENgetnodevalue(i, EN_PRESSURE, ref fPressureValue);            //Pressure 
                Epanet2md.ENgetnodevalue(i, EN_QUALITY, ref fQualityValue);              //Actual quality 
                Epanet2md.ENgetnodevalue(i, EN_SOURCEMASS, ref fSourceMassValue);        //EN_SOURCEMASS 
                Epanet2md.ENgetnodevalue(i, EN_INITVOLUME, ref fInitVolumeValue);        //Initial water volume 
                Epanet2md.ENgetnodevalue(i, EN_MIXMODEL, ref fMixModelValue);            //Mixing model code (see below)
                Epanet2md.ENgetnodevalue(i, EN_MIXZONEVOL, ref fMixZoneVolValue);        //Inlet/Outlet zone volume in a 2-compartment tank 
                Epanet2md.ENgetnodevalue(i, EN_TANKDIAM, ref fTankDiamValue);            //Tank diameter
                Epanet2md.ENgetnodevalue(i, EN_MINVOLUME, ref fMinVolumeValue);          //Minimum water volume 
                Epanet2md.ENgetnodevalue(i, EN_VOLCURVE, ref fVolCurveValue);            //Index of volume versus depth curve (0 if none assigned)
                Epanet2md.ENgetnodevalue(i, EN_MINLEVEL, ref fMinLevelValue);            //Minimum water level 
                Epanet2md.ENgetnodevalue(i, EN_MAXLEVEL, ref fMaxLevelValue);            //Maximum water level 
                Epanet2md.ENgetnodevalue(i, EN_MIXFRACTION, ref fMixFractionValue);      //Fraction of total volume occupied by the inlet/outlet zone in a 2-compartment tank 
                Epanet2md.ENgetnodevalue(i, EN_TANK_KBULK, ref fTankKbulkValue);         //Bulk reaction rate coefficient 

                Hashtable htTempResult = new Hashtable();

                htTempResult.Add("NODE_ID", sbNodeID.ToString());
                htTempResult.Add("EN_ELEVATION", fElevationValue);
                htTempResult.Add("EN_BASEDEMAND", fBaseDemandValue);
                htTempResult.Add("EN_PATTERN", fPatternValue);
                htTempResult.Add("EN_EMITTER", fEmitterValue);
                htTempResult.Add("EN_INITQUAL", fInitQualValue);
                htTempResult.Add("EN_SOURCEQUAL", fSourceQualValue);
                htTempResult.Add("EN_SOURCEPAT", fSourcePatValue);
                htTempResult.Add("EN_SOURCETYPE", fSourceTypeValue);
                htTempResult.Add("EN_TANKLEVEL", fTankLevelValue);
                htTempResult.Add("EN_DEMAND", fDemandValue);
                htTempResult.Add("EN_HEAD", fHeadValue);
                htTempResult.Add("EN_PRESSURE", fPressureValue);
                htTempResult.Add("EN_QUALITY", fQualityValue);
                htTempResult.Add("EN_SOURCEMASS", fSourceMassValue);
                htTempResult.Add("EN_INITVOLUME", fInitVolumeValue);
                htTempResult.Add("EN_MIXMODEL", fMixModelValue);
                htTempResult.Add("EN_MIXZONEVOL", fMixZoneVolValue);
                htTempResult.Add("EN_TANKDIAM", fTankDiamValue);
                htTempResult.Add("EN_MINVOLUME", fMinVolumeValue);
                htTempResult.Add("EN_VOLCURVE", fVolCurveValue);
                htTempResult.Add("EN_MINLEVEL", fMinLevelValue);
                htTempResult.Add("EN_MAXLEVEL", fMaxLevelValue);
                htTempResult.Add("EN_MIXFRACTION", fMixFractionValue);
                htTempResult.Add("EN_TANK_KBULK", fTankKbulkValue);



                //Type에 따라 구분해서 저장한다.
                if (iNodeType == EN_JUNCTION)
                {
                    //절점해석결과
                    alJunctionResult.Add(htTempResult);
                }
                else if (iNodeType == EN_RESERVOIR)
                {
                    //배수지해석결과
                    alReserviorResult.Add(htTempResult);
                }
                else if (iNodeType == EN_TANK)
                {
                    //탱크해석결과
                    alTankResult.Add(htTempResult);
                }
            }

            htNodeResult.Add("Junction", alJunctionResult);
            htNodeResult.Add("Reservior", alReserviorResult);
            htNodeResult.Add("Tank", alTankResult);


            return htNodeResult;
        }

        /// <summary>
        /// 관망해석후 링크에 대한 결과값 반환
        /// </summary>
        /// <returns></returns>
        private Hashtable ExtractLinkAnalysisResultHashtable()
        {

            //전체 Link 해석결과를 저장할 Hashtable
            Hashtable htLinkResult = new Hashtable();

            ArrayList alCVPipeResult = new ArrayList();              //Pipe with Check Valve
            ArrayList alPipeResult = new ArrayList();                //pipe
            ArrayList alPumpResult = new ArrayList();                //pump
            ArrayList alPRVResult = new ArrayList();                 //Pressure Reducing Valve
            ArrayList alPSVResult = new ArrayList();                 //Pressure Sustaining Valve
            ArrayList alPBVResult = new ArrayList();                 //Pressure Breaker Valve
            ArrayList alFCVResult = new ArrayList();                 //Flow Control Valve
            ArrayList alTCVResult = new ArrayList();                 //Throttle Control Valve
            ArrayList alGPVResult = new ArrayList();                 //General Purpose Valve

            //Link Count 발췌
            int iLinkCount = 0;
            Epanet2md.ENgetcount(EN_LINKCOUNT, ref iLinkCount);

            //Link 해석결과 변수
            float fDiameterValue = 0;
            float fLengthValue = 0;
            float fRoughnessValue = 0;
            float fMinorLossValue = 0;
            float fInitStatusValue = 0;
            float fInitSettingValue = 0;
            float fKbulkValue = 0;
            float fKwallValue = 0;
            float fFlowValue = 0;
            float fVelocityValue = 0;
            float fHeadlossValue = 0;
            float fStatusValue = 0;
            float fSettingValue = 0;
            float fEnergyValue = 0;

            for (int i = 1; i <= iLinkCount; i++)
            {
                StringBuilder sbLinkID = new StringBuilder();

                int iLinkType = 0;

                Epanet2md.ENgetlinkid2(i, sbLinkID);
                Epanet2md.ENgetlinktype(i, ref iLinkType);

                Epanet2md.ENgetlinkvalue(i, EN_DIAMETER, ref fDiameterValue);                                                        //Diameter 
                Epanet2md.ENgetlinkvalue(i, EN_LENGTH, ref fLengthValue);                                                               //Length 
                Epanet2md.ENgetlinkvalue(i, EN_ROUGHNESS, ref fRoughnessValue);                                                  //Roughness coeff. 
                Epanet2md.ENgetlinkvalue(i, EN_MINORLOSS, ref fMinorLossValue);                                                    //Minor loss coeff.
                Epanet2md.ENgetlinkvalue(i, EN_INITSTATUS, ref fInitStatusValue);                                                    //Initial link status (0 = closed, 1 = open)
                Epanet2md.ENgetlinkvalue(i, EN_INITSETTING, ref fInitSettingValue);                                                  //Roughness for pipes, initial speed for pumps, initial setting for valves
                Epanet2md.ENgetlinkvalue(i, EN_KBULK, ref fKbulkValue);                                                                   //Bulk reaction coeff. 
                Epanet2md.ENgetlinkvalue(i, EN_KWALL, ref fKwallValue);                                                                  //Wall reaction coeff.
                Epanet2md.ENgetlinkvalue(i, EN_FLOW, ref fFlowValue);                                                                    //Flow rate 
                Epanet2md.ENgetlinkvalue(i, EN_VELOCITY, ref fVelocityValue);                                                          //Flow velocity
                Epanet2md.ENgetlinkvalue(i, EN_HEADLOSS, ref fHeadlossValue);                                                       //Head loss 
                Epanet2md.ENgetlinkvalue(i, EN_STATUS, ref fStatusValue);                                                              //Actual link status (0 = closed, 1 = open)
                Epanet2md.ENgetlinkvalue(i, EN_SETTING, ref fSettingValue);                                                            //Roughness for pipes, actual speed for pumps, actual setting for valves
                Epanet2md.ENgetlinkvalue(i, EN_ENERGY, ref fEnergyValue);                                                              //Energy expended in kwatts 

                Hashtable htTempResult = new Hashtable();

                htTempResult.Add("LINK_ID", sbLinkID.ToString());
                htTempResult.Add("EN_DIAMETER", fDiameterValue);
                htTempResult.Add("EN_LENGTH", fLengthValue);
                htTempResult.Add("EN_ROUGHNESS", fRoughnessValue);
                htTempResult.Add("EN_MINORLOSS", fMinorLossValue);
                htTempResult.Add("EN_INITSTATUS", fInitStatusValue);
                htTempResult.Add("EN_INITSETTING", fInitSettingValue);
                htTempResult.Add("EN_KBULK", fKbulkValue);
                htTempResult.Add("EN_KWALL", fKwallValue);
                htTempResult.Add("EN_FLOW", fFlowValue);
                htTempResult.Add("EN_VELOCITY", fVelocityValue);
                htTempResult.Add("EN_HEADLOSS", fHeadlossValue);
                htTempResult.Add("EN_STATUS", fStatusValue);
                htTempResult.Add("EN_SETTING", fSettingValue);
                htTempResult.Add("EN_ENERGY", fEnergyValue);

                //Type에 따라 구분해서 저장한다.
                if (iLinkType == EN_CVPIPE)
                {
                    alCVPipeResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PIPE)
                {
                    alPipeResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PUMP)
                {
                    alPumpResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PRV)
                {
                    alPRVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PSV)
                {
                    alPSVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PBV)
                {
                    alPBVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_FCV)
                {
                    alFCVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_TCV)
                {
                    alTCVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_GPV)
                {
                    alGPVResult.Add(htTempResult);
                }
            }

            htLinkResult.Add("CVPipe", alCVPipeResult);
            htLinkResult.Add("Pipe", alPipeResult);
            htLinkResult.Add("Pump", alPumpResult);
            htLinkResult.Add("PRV", alPRVResult);
            htLinkResult.Add("PSV", alPSVResult);
            htLinkResult.Add("PBV", alPBVResult);
            htLinkResult.Add("FCV", alFCVResult);
            htLinkResult.Add("TCV", alTCVResult);
            htLinkResult.Add("GPV", alGPVResult);

            return htLinkResult;
        }

        /// <summary>
        /// 관망해석후 노드에 대한 결과값 반환 Table
        /// </summary>
        /// <returns></returns>
        private DataTable ExtractNodeAnalysisResultTable()
        {
            DataTable dtresult = new DataTable();
            dtresult.Columns.Add("NODE_CAT");           //링크 구분
            dtresult.Columns.Add("NODE_ID");            //link ID
            dtresult.Columns.Add("EN_ELEVATION");       //Elevation 
            dtresult.Columns.Add("EN_BASEDEMAND");      //Base demand 
            dtresult.Columns.Add("EN_PATTERN");         //Demand pattern index 
            dtresult.Columns.Add("EN_EMITTER");         //Emitter coeff. 
            dtresult.Columns.Add("EN_INITQUAL");        //Initial quality 
            dtresult.Columns.Add("EN_SOURCEQUAL");      //Source quality 
            dtresult.Columns.Add("EN_SOURCEPAT");       //Source pattern index
            dtresult.Columns.Add("EN_SOURCETYPE");      //Source type (See note below) 
            dtresult.Columns.Add("EN_TANKLEVEL");       //Initial water level in tank 
            dtresult.Columns.Add("EN_DEMAND");          //Actual demand 
            dtresult.Columns.Add("EN_HEAD");            //Hydraulic head 
            dtresult.Columns.Add("EN_PRESSURE");        //Pressure 
            dtresult.Columns.Add("EN_QUALITY");         //Actual quality 
            dtresult.Columns.Add("EN_SOURCEMASS");      //EN_SOURCEMASS 
            dtresult.Columns.Add("EN_INITVOLUME");      //Initial water volume 
            dtresult.Columns.Add("EN_MIXMODEL");        //Mixing model code (see below)
            dtresult.Columns.Add("EN_MIXZONEVOL");      //Inlet/Outlet zone volume in a 2-compartment tank 
            dtresult.Columns.Add("EN_TANKDIAM");        //Tank diameter
            dtresult.Columns.Add("EN_MINVOLUME");       //Minimum water volume 
            dtresult.Columns.Add("EN_VOLCURVE");        //Index of volume versus depth curve (0 if none assigned)
            dtresult.Columns.Add("EN_MINLEVEL");        //Minimum water level 
            dtresult.Columns.Add("EN_MAXLEVEL");        //Maximum water level 
            dtresult.Columns.Add("EN_MIXFRACTION");     //Fraction of total volume occupied by the inlet/outlet zone in a 2-compartment tank 
            dtresult.Columns.Add("EN_TANK_KBULK");      //Bulk reaction rate coefficient 

            //Node 해석결과 변수
            float fElevationValue = 0;
            float fBaseDemandValue = 0;
            float fPatternValue = 0;
            float fEmitterValue = 0;
            float fInitQualValue = 0;
            float fSourceQualValue = 0;
            float fSourcePatValue = 0;
            float fSourceTypeValue = 0;
            float fTankLevelValue = 0;
            float fDemandValue = 0;
            float fHeadValue = 0;
            float fPressureValue = 0;
            float fQualityValue = 0;
            float fSourceMassValue = 0;
            float fInitVolumeValue = 0;
            float fMixModelValue = 0;
            float fMixZoneVolValue = 0;
            float fTankDiamValue = 0;
            float fMinVolumeValue = 0;
            float fVolCurveValue = 0;
            float fMinLevelValue = 0;
            float fMaxLevelValue = 0;
            float fMixFractionValue = 0;
            float fTankKbulkValue = 0;

            try
            {
                //Node Count 발췌
                int iNodeCount = 0;
                Epanet2md.ENgetcount(EN_NODECOUNT, ref iNodeCount);

                for (int i = 1; i <= iNodeCount; i++)
                {
                    // NodeID의 기본값을 string.Empty로 안했을경우, 메모리 오류 발생
                    StringBuilder sbNodeID = new StringBuilder();

                    int iNodeType = 0;

                    Epanet2md.ENgetnodeid2(i, sbNodeID);
                    Epanet2md.ENgetnodetype(i, ref iNodeType);

                    Epanet2md.ENgetnodevalue(i, EN_ELEVATION, ref fElevationValue);                  //Elevation 
                    Epanet2md.ENgetnodevalue(i, EN_BASEDEMAND, ref fBaseDemandValue);                //Base demand 
                    Epanet2md.ENgetnodevalue(i, EN_PATTERN, ref fPatternValue);                      //Demand pattern index 
                    Epanet2md.ENgetnodevalue(i, EN_EMITTER, ref fEmitterValue);                      //Emitter coeff. 
                    Epanet2md.ENgetnodevalue(i, EN_INITQUAL, ref fInitQualValue);                    //Initial quality 
                    Epanet2md.ENgetnodevalue(i, EN_SOURCEQUAL, ref fSourceQualValue);                //Source quality 
                    Epanet2md.ENgetnodevalue(i, EN_SOURCEPAT, ref fSourcePatValue);                  //Source pattern index
                    Epanet2md.ENgetnodevalue(i, EN_SOURCETYPE, ref fSourceTypeValue);                //Source type (See note below) 
                    Epanet2md.ENgetnodevalue(i, EN_TANKLEVEL, ref fTankLevelValue);                  //Initial water level in tank 
                    Epanet2md.ENgetnodevalue(i, EN_DEMAND, ref fDemandValue);                        //Actual demand 
                    Epanet2md.ENgetnodevalue(i, EN_HEAD, ref fHeadValue);                            //Hydraulic head 
                    Epanet2md.ENgetnodevalue(i, EN_PRESSURE, ref fPressureValue);                    //Pressure 
                    Epanet2md.ENgetnodevalue(i, EN_QUALITY, ref fQualityValue);                      //Actual quality 
                    Epanet2md.ENgetnodevalue(i, EN_SOURCEMASS, ref fSourceMassValue);                //EN_SOURCEMASS 
                    Epanet2md.ENgetnodevalue(i, EN_INITVOLUME, ref fInitVolumeValue);                //Initial water volume 
                    Epanet2md.ENgetnodevalue(i, EN_MIXMODEL, ref fMixModelValue);                    //Mixing model code (see below)
                    Epanet2md.ENgetnodevalue(i, EN_MIXZONEVOL, ref fMixZoneVolValue);                //Inlet/Outlet zone volume in a 2-compartment tank 
                    Epanet2md.ENgetnodevalue(i, EN_TANKDIAM, ref fTankDiamValue);                    //Tank diameter
                    Epanet2md.ENgetnodevalue(i, EN_MINVOLUME, ref fMinVolumeValue);                  //Minimum water volume 
                    Epanet2md.ENgetnodevalue(i, EN_VOLCURVE, ref fVolCurveValue);                    //Index of volume versus depth curve (0 if none assigned)
                    Epanet2md.ENgetnodevalue(i, EN_MINLEVEL, ref fMinLevelValue);                    //Minimum water level 
                    Epanet2md.ENgetnodevalue(i, EN_MAXLEVEL, ref fMaxLevelValue);                    //Maximum water level 
                    Epanet2md.ENgetnodevalue(i, EN_MIXFRACTION, ref fMixFractionValue);              //Fraction of total volume occupied by the inlet/outlet zone in a 2-compartment tank 
                    Epanet2md.ENgetnodevalue(i, EN_TANK_KBULK, ref fTankKbulkValue);                 //Bulk reaction rate coefficient 

                    DataRow dradd = dtresult.NewRow();

                    switch (iNodeType)
                    {
                        case EN_JUNCTION:
                            dradd["NODE_CAT"] = "Junction";
                            break;
                        case EN_RESERVOIR:
                            dradd["NODE_CAT"] = "Reservior";
                            break;
                        case EN_TANK:
                            dradd["NODE_CAT"] = "Tank";
                            break;
                    }

                    dradd["NODE_ID"] = sbNodeID.ToString();
                    dradd["EN_ELEVATION"] = fElevationValue;
                    dradd["EN_BASEDEMAND"] = fBaseDemandValue;
                    dradd["EN_PATTERN"] = fPatternValue;
                    dradd["EN_EMITTER"] = fEmitterValue;
                    dradd["EN_INITQUAL"] = fInitQualValue;
                    dradd["EN_SOURCEQUAL"] = fSourceQualValue;
                    dradd["EN_SOURCEPAT"] = fSourcePatValue;
                    dradd["EN_SOURCETYPE"] = fSourceTypeValue;
                    dradd["EN_TANKLEVEL"] = fTankLevelValue;
                    dradd["EN_DEMAND"] = fDemandValue;
                    dradd["EN_HEAD"] = fHeadValue;
                    dradd["EN_PRESSURE"] = fPressureValue;
                    dradd["EN_QUALITY"] = fQualityValue;
                    dradd["EN_SOURCEMASS"] = fSourceMassValue;
                    dradd["EN_INITVOLUME"] = fInitVolumeValue;
                    dradd["EN_MIXMODEL"] = fMixModelValue;
                    dradd["EN_MIXZONEVOL"] = fMixZoneVolValue;
                    dradd["EN_TANKDIAM"] = fTankDiamValue;
                    dradd["EN_MINVOLUME"] = fMinVolumeValue;
                    dradd["EN_VOLCURVE"] = fVolCurveValue;
                    dradd["EN_MINLEVEL"] = fMinLevelValue;
                    dradd["EN_MAXLEVEL"] = fMaxLevelValue;
                    dradd["EN_MIXFRACTION"] = fMixFractionValue;
                    dradd["EN_TANK_KBULK"] = fTankKbulkValue;

                    dtresult.Rows.Add(dradd.ItemArray);
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 관망해석후 링크에 대한 결과값 반환
        /// </summary>
        /// <returns></returns>
        private DataTable ExtractLinkAnalysisResultTable()
        {
            DataTable dtresult = new DataTable();
            dtresult.Columns.Add("LINK_CAT");           //링크 구분
            dtresult.Columns.Add("LINK_ID");            //link ID
            dtresult.Columns.Add("EN_DIAMETER");        //Diameter 
            dtresult.Columns.Add("EN_LENGTH");          //Length 
            dtresult.Columns.Add("EN_ROUGHNESS");       //Roughness coeff. 
            dtresult.Columns.Add("EN_MINORLOSS");       //Minor loss coeff.
            dtresult.Columns.Add("EN_INITSTATUS");      //Initial link status (0 = closed, 1 = open)
            dtresult.Columns.Add("EN_INITSETTING");     //Roughness for pipes, initial speed for pumps, initial setting for valves
            dtresult.Columns.Add("EN_KBULK");           //Bulk reaction coeff. 
            dtresult.Columns.Add("EN_KWALL");           //Wall reaction coeff.
            dtresult.Columns.Add("EN_FLOW");            //Flow rate 
            dtresult.Columns.Add("EN_VELOCITY");        //Flow velocity
            dtresult.Columns.Add("EN_HEADLOSS");        //Head loss 
            dtresult.Columns.Add("EN_STATUS");          //Actual link status (0 = closed, 1 = open)
            dtresult.Columns.Add("EN_SETTING");         //Roughness for pipes, actual speed for pumps, actual setting for valves
            dtresult.Columns.Add("EN_ENERGY");          //Energy expended in kwatts 

            //Link 해석결과 변수
            float fDiameterValue = 0;
            float fLengthValue = 0;
            float fRoughnessValue = 0;
            float fMinorLossValue = 0;
            float fInitStatusValue = 0;
            float fInitSettingValue = 0;
            float fKbulkValue = 0;
            float fKwallValue = 0;
            float fFlowValue = 0;
            float fVelocityValue = 0;
            float fHeadlossValue = 0;
            float fStatusValue = 0;
            float fSettingValue = 0;
            float fEnergyValue = 0;

            try
            {
                //Link Count 발췌
                int iLinkCount = 0;
                Epanet2md.ENgetcount(EN_LINKCOUNT, ref iLinkCount);

                for (int i = 1; i <= iLinkCount; i++)
                {
                    StringBuilder sbLinkID = new StringBuilder();

                    int iLinkType = 0;

                    Epanet2md.ENgetlinkid2(i, sbLinkID);
                    Epanet2md.ENgetlinktype(i, ref iLinkType);

                    Epanet2md.ENgetlinkvalue(i, EN_DIAMETER, ref fDiameterValue);                                                        //Diameter 
                    Epanet2md.ENgetlinkvalue(i, EN_LENGTH, ref fLengthValue);                                                               //Length 
                    Epanet2md.ENgetlinkvalue(i, EN_ROUGHNESS, ref fRoughnessValue);                                                  //Roughness coeff. 
                    Epanet2md.ENgetlinkvalue(i, EN_MINORLOSS, ref fMinorLossValue);                                                    //Minor loss coeff.
                    Epanet2md.ENgetlinkvalue(i, EN_INITSTATUS, ref fInitStatusValue);                                                    //Initial link status (0 = closed, 1 = open)
                    Epanet2md.ENgetlinkvalue(i, EN_INITSETTING, ref fInitSettingValue);                                                  //Roughness for pipes, initial speed for pumps, initial setting for valves
                    Epanet2md.ENgetlinkvalue(i, EN_KBULK, ref fKbulkValue);                                                                   //Bulk reaction coeff. 
                    Epanet2md.ENgetlinkvalue(i, EN_KWALL, ref fKwallValue);                                                                  //Wall reaction coeff.
                    Epanet2md.ENgetlinkvalue(i, EN_FLOW, ref fFlowValue);                                                                    //Flow rate 
                    Epanet2md.ENgetlinkvalue(i, EN_VELOCITY, ref fVelocityValue);                                                          //Flow velocity
                    Epanet2md.ENgetlinkvalue(i, EN_HEADLOSS, ref fHeadlossValue);                                                       //Head loss 
                    Epanet2md.ENgetlinkvalue(i, EN_STATUS, ref fStatusValue);                                                              //Actual link status (0 = closed, 1 = open)
                    Epanet2md.ENgetlinkvalue(i, EN_SETTING, ref fSettingValue);                                                            //Roughness for pipes, actual speed for pumps, actual setting for valves
                    Epanet2md.ENgetlinkvalue(i, EN_ENERGY, ref fEnergyValue);                                                              //Energy expended in kwatts 

                    DataRow dradd = dtresult.NewRow();

                    switch (iLinkType)
                    {
                        case EN_CVPIPE:
                            dradd["LINK_CAT"] = "CVPipe";
                            break;
                        case EN_PIPE:
                            dradd["LINK_CAT"] = "Pipe";
                            break;
                        case EN_PUMP:
                            dradd["LINK_CAT"] = "Pump";
                            break;
                        case EN_PRV:
                            dradd["LINK_CAT"] = "PRV";
                            break;
                        case EN_PSV:
                            dradd["LINK_CAT"] = "PSV";
                            break;
                        case EN_PBV:
                            dradd["LINK_CAT"] = "PBV";
                            break;
                        case EN_FCV:
                            dradd["LINK_CAT"] = "FCV";
                            break;
                        case EN_TCV:
                            dradd["LINK_CAT"] = "TCV";
                            break;
                        case EN_GPV:
                            dradd["LINK_CAT"] = "GPV";
                            break;
                    }
                    dradd["LINK_ID"] = sbLinkID.ToString();
                    dradd["EN_DIAMETER"] = fDiameterValue;
                    dradd["EN_LENGTH"] = fLengthValue;
                    dradd["EN_ROUGHNESS"] = fRoughnessValue;
                    dradd["EN_MINORLOSS"] = fMinorLossValue;
                    dradd["EN_INITSTATUS"] = fInitStatusValue;
                    dradd["EN_INITSETTING"] = fInitSettingValue;
                    dradd["EN_KBULK"] = fKbulkValue;
                    dradd["EN_KWALL"] = fKwallValue;
                    dradd["EN_FLOW"] = fFlowValue;
                    dradd["EN_VELOCITY"] = fVelocityValue;
                    dradd["EN_HEADLOSS"] = fHeadlossValue;
                    dradd["EN_STATUS"] = fStatusValue;
                    dradd["EN_SETTING"] = fSettingValue;
                    dradd["EN_ENERGY"] = fEnergyValue;

                    dtresult.Rows.Add(dradd.ItemArray);
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
