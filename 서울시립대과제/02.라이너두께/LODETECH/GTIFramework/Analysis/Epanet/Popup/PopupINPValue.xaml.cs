﻿using DevExpress.Xpf.Map;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections;
using System.Data;
using System.Windows;
using System.Windows.Input;
using TatukGIS.NDK;

namespace GTIFramework.Analysis.Epanet.Popup
{
    /// <summary>
    /// PopupINPValue.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupINPValue : Window
    {
        DataTable dtreault = new DataTable();
        DataRow dradd;

        public PopupINPValue()
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);

            btnXSignClose.Click += BtnXSignClose_Click;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;
        }

        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (WindowState == WindowState.Maximized)
                {
                    Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    WindowState = WindowState.Normal;
                }
                DragMove();
            }
        }

        private void BtnXSignClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public void itemChange(MapItem mapItem)
        {
            try
            {
                dtreault = null;
                dtreault = new DataTable();
                dtreault.Columns.Add("KEY");
                dtreault.Columns.Add("VAL");

                foreach (var item in mapItem.Attributes)
                {
                    if (item.Name.Equals("SHP"))
                    {
                        Title = item.Value + " 정보";
                        lbtitle.Content = item.Value + " 정보";
                    }
                    else
                    {
                        dradd = null;
                        dradd = dtreault.NewRow();
                        dradd["KEY"] = item.Name;
                        dradd["VAL"] = item.Value;
                        dtreault.Rows.Add(dradd.ItemArray);
                    }
                }

                gcData.ItemsSource = dtreault;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        public void itemChange(Hashtable htval, string stritem)
        {
            try
            {
                dtreault = null;
                dtreault = new DataTable();
                dtreault.Columns.Add("KEY");
                dtreault.Columns.Add("VAL");

                Title = stritem + " 정보";
                lbtitle.Content = stritem + " 정보";

                foreach (DictionaryEntry de in htval)
                {
                    dradd = null;
                    dradd = dtreault.NewRow();
                    dradd["KEY"] = de.Key;
                    dradd["VAL"] = de.Value;
                    dtreault.Rows.Add(dradd.ItemArray);
                }

                gcData.ItemsSource = dtreault;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }


        //Tatuk
        public void itemChange(TGIS_Shape mapItem, Hashtable htinpFied)
        {
            try
            {
                dtreault = null;
                dtreault = new DataTable();
                dtreault.Columns.Add("KEY");
                dtreault.Columns.Add("VAL");

                Title = mapItem.GetField("SHP").ToString() + " 정보";
                lbtitle.Content = mapItem.GetField("SHP").ToString() + " 정보";

                foreach (string str in htinpFied[mapItem.GetField("SHP").ToString()] as string[])
                {
                    if (str.Equals("SHP"))
                    {
                        Title = mapItem.GetField("SHP").ToString() + " 정보";
                        lbtitle.Content = mapItem.GetField("SHP").ToString() + " 정보";
                    }
                    else
                    {
                        dradd = null;
                        dradd = dtreault.NewRow();
                        dradd["KEY"] = str;
                        dradd["VAL"] = mapItem.GetField(str).ToString();
                        dtreault.Rows.Add(dradd.ItemArray);
                    }
                }
                gcData.ItemsSource = dtreault;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

    }
}
