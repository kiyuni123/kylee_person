﻿using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows;
using System.Windows.Input;

namespace GTIFramework.Analysis.Epanet.Popup
{
    /// <summary>
    /// PopupINPChart.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupINPChart : Window
    {
        public PopupINPChart()
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);

            btnXSignClose.Click += BtnXSignClose_Click;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;
        }

        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (WindowState == WindowState.Maximized)
                {
                    Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    WindowState = WindowState.Normal;
                }
                DragMove();
            }
        }

        private void BtnXSignClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        public void itemChange(List<string> data)
        {
            try
            {
                DataRow dr;

                DataTable dtchart = new DataTable();
                dtchart.Columns.Add("NUM");
                dtchart.Columns.Add("VAL");

                for (int i = 0; i < data.Count; i++)
                {
                    dr = dtchart.NewRow();
                    dr["NUM"] = i;
                    dr["VAL"] = data[i];
                    dtchart.Rows.Add(dr.ItemArray);
                }

                ctRawData.DataSource = dtchart;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
    }
}
