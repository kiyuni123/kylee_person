﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTIFramework.Analysis.Epanet
{
    public class Epanet22Run
    {
        #region Epanet2.2 관련
        //These are codes used by the DLL functions
        public const int EN_ELEVATION = 0;		//'Node parameters
        public const int EN_BASEDEMAND = 1;
        public const int EN_PATTERN = 2;
        public const int EN_EMITTER = 3;
        public const int EN_INITQUAL = 4;
        public const int EN_SOURCEQUAL = 5;
        public const int EN_SOURCEPAT = 6;
        public const int EN_SOURCETYPE = 7;
        public const int EN_TANKLEVEL = 8;
        public const int EN_DEMAND = 9;
        public const int EN_HEAD = 10;
        public const int EN_PRESSURE = 11;
        public const int EN_QUALITY = 12;
        public const int EN_SOURCEMASS = 13;
        public const int EN_INITVOLUME = 14;
        public const int EN_MIXMODEL = 15;
        public const int EN_MIXZONEVOL = 16;
        public const int EN_TANKDIAM = 17;
        public const int EN_MINVOLUME = 18;
        public const int EN_VOLCURVE = 19;
        public const int EN_MINLEVEL = 20;
        public const int EN_MAXLEVEL = 21;
        public const int EN_MIXFRACTION = 22;
        public const int EN_TANK_KBULK = 23;
        // v3.0 추가
        public const int EN_TANKVOLUME = 24;          //Current computed tank volume (read only)
        public const int EN_MAXVOLUME = 25;           //Tank maximum volume (read only)
        public const int EN_CANOVERFLOW = 26;       //Tank can overflow (= 1) or not (= 0)
        public const int EN_DEMANDDEFICIT = 27;     //Amount that full demand is reduced under PDA (read only)

        public const int EN_DIAMETER = 0;		//'Link parameters
        public const int EN_LENGTH = 1;
        public const int EN_ROUGHNESS = 2;
        public const int EN_MINORLOSS = 3;
        public const int EN_INITSTATUS = 4;
        public const int EN_INITSETTING = 5;
        public const int EN_KBULK = 6;
        public const int EN_KWALL = 7;
        public const int EN_FLOW = 8;
        public const int EN_VELOCITY = 9;
        public const int EN_HEADLOSS = 10;
        public const int EN_STATUS = 11;
        public const int EN_SETTING = 12;
        public const int EN_ENERGY = 13;
        // v3.0 추가
        public const int EN_LINKQUAL = 14;              //Current computed link quality (read only)
        public const int EN_LINKPATTERN = 15;        //Pump speed time pattern index.
        public const int EN_PUMP_STATE = 16;        //Current computed pump state (read only) (see EN_PumpStateType)
        public const int EN_PUMP_EFFIC = 17;         //Current computed pump efficiency (read only)
        public const int EN_PUMP_POWER = 18;       //Pump constant power rating.
        public const int EN_PUMP_HCURVE = 19;      //Pump head v. flow curve index.
        public const int EN_PUMP_ECURVE = 20;      //Pump efficiency v. flow curve index.
        public const int EN_PUMP_ECOST = 21;       //Pump average energy price.
        public const int EN_PUMP_EPAT = 22;         //Pump energy price time pattern index.

        public const int EN_DURATION = 0;		//'Time parameters
        public const int EN_HYDSTEP = 1;
        public const int EN_QUALSTEP = 2;
        public const int EN_PATTERNSTEP = 3;
        public const int EN_PATTERNSTART = 4;
        public const int EN_REPORTSTEP = 5;
        public const int EN_REPORTSTART = 6;
        public const int EN_RULESTEP = 7;
        public const int EN_STATISTIC = 8;
        public const int EN_PERIODS = 9;
        // v3.0 추가
        public const int EN_STARTTIME = 10;             //Simulation starting time of day.
        public const int EN_HTIME = 11;                     //Elapsed time of current hydraulic solution (read only)
        public const int EN_QTIME = 12;                     //Elapsed time of current quality solution (read only)
        public const int EN_HALTFLAG = 13;                //Flag indicating if the simulation was halted (read only)
        public const int EN_NEXTEVENT = 14;              //Shortest time until a tank becomes empty or full (read only)
        public const int EN_NEXTEVENTTANK = 15;      //Index of tank with shortest time to become empty or full (read only)

        public const int EN_ITERATIONS = 0;             //'Analysis convergence statistics(v 3.0 ????)
        public const int EN_RELATIVEERROR = 1;
        public const int EN_MAXHEADERROR = 2;
        public const int EN_MAXFLOWCHANGE = 3;
        public const int EN_MASSBALANCE = 4;
        public const int EN_DEFICIENTNODES = 5;
        public const int EN_DEMANDREDUCTION = 6;

        public const int EN_NODE = 0;                       //'Types of network objects(v 3.0 ???)
        public const int EN_LINK = 1;
        public const int EN_TIMEPAT = 2;
        public const int EN_CURVE = 3;
        public const int EN_CONTROL = 4;
        public const int EN_RULE = 5;

        public const int EN_NODECOUNT = 0;		//'Component counts(Types of objects to count)
        public const int EN_TANKCOUNT = 1;
        public const int EN_LINKCOUNT = 2;
        public const int EN_PATCOUNT = 3;
        public const int EN_CURVECOUNT = 4;
        public const int EN_CONTROLCOUNT = 5;
        // v3.0 추가
        public const int EN_RULECOUNT = 6;          //Number of rule-based controls.

        public const int EN_JUNCTION = 0;		//'Node types
        public const int EN_RESERVOIR = 1;
        public const int EN_TANK = 2;

        public const int EN_CVPIPE = 0;			//'Link types
        public const int EN_PIPE = 1;
        public const int EN_PUMP = 2;
        public const int EN_PRV = 3;
        public const int EN_PSV = 4;
        public const int EN_PBV = 5;
        public const int EN_FCV = 6;
        public const int EN_TCV = 7;
        public const int EN_GPV = 8;

        public const int EN_CLOSED = 0;			//'Link status(v 3.0 ???)
        public const int EN_OPEN = 1;

        public const int EN_PUMP_XHEAD = 0;			//'Pump states(v 3.0 ???)
        public const int EN_PUMP_CLOSED = 2;
        public const int EN_PUMP_OPEN = 3;
        public const int EN_PUMP_XFLOW = 5;

        public const int EN_NONE = 0;			//'Quality analysis types
        public const int EN_CHEM = 1;
        public const int EN_AGE = 2;
        public const int EN_TRACE = 3;

        public const int EN_CONCEN = 0;			//'Source quality types
        public const int EN_MASS = 1;
        public const int EN_SETPOINT = 2;
        public const int EN_FLOWPACED = 3;

        public const int EN_HW = 0;			//'Head loss formulas(v 3.0 ???)
        public const int EN_DW = 1;
        public const int EN_CM = 2;

        public const int EN_CFS = 0;			//'Flow units types
        public const int EN_GPM = 1;
        public const int EN_MGD = 2;
        public const int EN_IMGD = 3;
        public const int EN_AFD = 4;
        public const int EN_LPS = 5;
        public const int EN_LPM = 6;
        public const int EN_MLD = 7;
        public const int EN_CMH = 8;
        public const int EN_CMD = 9;

        public const int EN_DDA = 0;			//'Demand models(v 3.0 ???)
        public const int EN_PDA = 1;

        public const int EN_TRIALS = 0;			//'Misc. options(Simulation options)
        public const int EN_ACCURACY = 1;
        public const int EN_TOLERANCE = 2;
        public const int EN_EMITEXPON = 3;
        public const int EN_DEMANDMULT = 4;
        // v3.0 추가
        public const int EN_HEADERROR = 5;                          //Maximum head loss error for hydraulic convergence.
        public const int EN_FLOWCHANGE = 6;                         //Maximum flow change for hydraulic convergence.
        public const int EN_HEADLOSSFORM = 7;                       //Head loss formula (see EN_HeadLossType)
        public const int EN_GLOBALEFFIC = 8;                            //Global pump efficiency (percent)
        public const int EN_GLOBALPRICE = 9;                            //Global energy price per KWH.
        public const int EN_GLOBALPATTERN = 10;                     //Index of a global energy price pattern.
        public const int EN_DEMANDCHARGE = 11;                      //Energy charge per max. KW usage.
        public const int EN_SP_GRAVITY = 12;                            //Specific gravity.
        public const int EN_SP_VISCOS = 13;                             //Specific viscosity (relative to water at 20 deg C)
        public const int EN_UNBALANCED = 14;                            //Extra trials allowed if hydraulics don't converge.
        public const int EN_CHECKFREQ = 15;                             //Frequency of hydraulic status checks.
        public const int EN_MAXCHECK = 16;                              //Maximum trials for status checking.
        public const int EN_DAMPLIMIT = 17;                             //Accuracy level where solution damping begins.
        public const int EN_SP_DIFFUS = 18;                             //Specific diffusivity (relative to chlorine at 20 deg C)
        public const int EN_BULKORDER = 19;                             //Bulk water reaction order for pipes.
        public const int EN_WALLORDER = 20;                             //Wall reaction order for pipes (either 0 or 1)
        public const int EN_TANKORDER = 21;                             //Bulk water reaction order for tanks.
        public const int EN_CONCENLIMIT = 22;                           //Limiting concentration for growth reactions.

        public const int EN_LOWLEVEL = 0;		//'Control types(Simple control types)
        public const int EN_HILEVEL = 1;
        public const int EN_TIMER = 2;
        public const int EN_TIMEOFDAY = 3;

        public const int EN_AVERAGE = 1;		//'Time statistic types(Reporting statistic choices)
        public const int EN_MINIMUM = 2;
        public const int EN_MAXIMUM = 3;
        public const int EN_RANGE = 4;
        // v3.0 추가
        public const int EN_SERIES = 0;         //Report all time series points.

        public const int EN_MIX1 = 0;			//'Tank mixing models
        public const int EN_MIX2 = 1;
        public const int EN_FIFO = 2;
        public const int EN_LIFO = 3;

        public const int EN_NOSAVE = 0;			//'Hydraulic initialization options
        public const int EN_SAVE = 1;
        public const int EN_INITFLOW = 10;
        public const int EN_SAVE_AND_INIT = 11;

        public const int EN_CONST_HP = 0;			//'Types of pump curves(v 3.0 ???)
        public const int EN_POWER_FUNC = 1;
        public const int EN_CUSTOM = 2;
        public const int EN_NOCURVE = 3;

        public const int EN_VOLUME_CURVE = 0;			//'Types of data curves(v 3.0 ???)
        public const int EN_PUMP_CURVE = 1;
        public const int EN_EFFIC_CURVE = 2;
        public const int EN_HLOSS_CURVE = 3;
        public const int EN_GENERIC_CURVE = 4;

        public const int EN_UNCONDITIONAL = 0;			//'Deletion action codes(v 3.0 ???)
        public const int EN_CONDITIONAL = 1;

        public const int EN_NO_REPORT = 0;			//'Status reporting levels(v 3.0 ???)
        public const int EN_NORMAL_REPORT = 1;
        public const int EN_FULL_REPORT = 2;

        public const int EN_R_NODE = 6;			//'Network objects used in rule-based controls(v 3.0 ???)
        public const int EN_R_LINK = 7;
        public const int EN_R_SYSTEM = 8;

        public const int EN_R_DEMAND = 0;			//'Object variables used in rule-based controls(v 3.0 ???)
        public const int EN_R_HEAD = 1;
        public const int EN_R_GRADE = 2;
        public const int EN_R_LEVEL = 3;
        public const int EN_R_PRESSURE = 4;
        public const int EN_R_FLOW = 5;
        public const int EN_R_STATUS = 6;
        public const int EN_R_SETTING = 7;
        public const int EN_R_POWER = 8;
        public const int EN_R_TIME = 9;
        public const int EN_R_CLOCKTIME = 10;
        public const int EN_R_FILLTIME = 11;
        public const int EN_R_DRAINTIME = 12;

        public const int EN_R_EQ = 0;			//'Comparison operators used in rule-based controls(v 3.0 ???)
        public const int EN_R_NE = 1;
        public const int EN_R_LE = 2;
        public const int EN_R_GE = 3;
        public const int EN_R_LT = 4;
        public const int EN_R_GT = 5;
        public const int EN_R_IS = 6;
        public const int EN_R_NOT = 7;
        public const int EN_R_BELOW = 8;
        public const int EN_R_ABOVE = 9;

        public const int EN_R_IS_OPEN = 1;			//'Link status codes used in rule-based controls(v 3.0 ???)
        public const int EN_R_IS_CLOSED = 2;
        public const int EN_R_IS_ACTIVE = 3;
        #endregion

        #region EPANET_MSX 관련
        //These are codes used by the DLL functions(EPANET_MSX)


        /// <summary>
        /// obj type 종류
        /// </summary>
        public const int MSX_NODE = 0;
        public const int MSX_LINK = 1;                  // 사용안하는것 같음.
        public const int MSX_TANK = 2;


        /// <summary>
        /// type 종류
        /// </summary>
        public const int MSX_SPECIES = 3;
        public const int MSX_TERM = 4;                  // 사용안하는것 같음.
        public const int MSX_PARAMETER = 5;
        public const int MSX_CONSTANT = 6;
        public const int MSX_PATTERN = 7;

        /// <summary>
        /// species type 종류
        /// </summary>
        public const int MSX_BULK = 0;
        public const int MSX_WALL = 1;

        /// <summary>
        /// source 종류 
        /// </summary>
        public const int MSX_NOSOURCE = -1;
        public const int MSX_CONCEN = 0;
        public const int MSX_MASS = 1;
        public const int MSX_SETPOINT = 2;
        public const int MSX_FLOWPACED = 3;

        #endregion

        string DirInp = string.Empty;
        string DirMSX = string.Empty;
        string DirRpt = string.Empty;
        string DirOut = string.Empty;

        public Epanet22Run() { }

        /// <summary>
        /// 수질 염소 실시간
        /// </summary>
        /// <param name="strINPpath">inp 파일 경로</param>
        public Hashtable EpanetQcl(string strINPpath)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            try
            {
                //inp파일 같은 경로에 rpt, out 파일 생성 경로
                DirInp = strINPpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_realtime_22.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_realtime_22.out";

                //Use of epanet functions
                Err = Epanet22md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit

                #region // 수리해석 실행(수질해석을 돌리기 위하여 먼저 수리해석을 돌려야 함) 실시간, 시간 구분
                Err = Epanet22md.ENsolveH();
                Err = Epanet22md.ENopenQ();
                Err = Epanet22md.ENinitQ(0);
                #endregion

                // t :  관망해석시 현재 시뮬레이션 클럭시간(초)(current simulation clock time in seconds). ENrunH에서 t 데이터타입이 long로 지정됨.
                long t = 1;

                Hashtable htAnalysisResult = new Hashtable();

                Epanet22md.ENrunQ(ref t);

                Hashtable htTimeResult = new Hashtable();
                //htTimeResult.Add("Node", ExtractNodeAnalysisResultHashtable());
                //htTimeResult.Add("Link", ExtractLinkAnalysisResultHashtable());
                htTimeResult.Add("Node", ExtractNodeAnalysisResultTable());
                htTimeResult.Add("Link", ExtractLinkAnalysisResultTable());

                htAnalysisResult.Add("00:00", htTimeResult);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
            finally
            {
                //수질해석결과 Close
                Epanet22md.ENcloseQ();
                //Close the toolkit
                Epanet22md.ENclose();
            }
        }

        /// <summary>
        /// 수질 염소 시간변화
        /// </summary>
        /// <param name="strINPpath">inp 파일 경로</param>
        /// <param name="htqual">배수지 수질 초기값 key:ID value:수질 초기값</param>
        public Hashtable EpanetQcl(string strINPpath, Hashtable htqual)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            int iIndex = 9999;

            try
            {
                //inp파일 같은 경로에 rpt, out 파일 생성 경로
                DirInp = strINPpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_time_22.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_time_22.out";

                //Use of epanet functions
                Err = Epanet22md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit


                #region // 수리해석 실행(수질해석을 돌리기 위하여 먼저 수리해석을 돌려야 함) 실시간, 시간 구분
                Err = Epanet22md.ENsolveH();
                Err = Epanet22md.ENopenQ();

                foreach (DictionaryEntry qual in htqual)
                {
                    //inp아이디로 해석객체 index 선별
                    Epanet22md.ENgetnodeindex(qual.Key.ToString(), ref iIndex);
                    //배수지 초기 수질값 세팅
                    Epanet22md.ENsetnodevalue(iIndex, EN_INITQUAL, float.Parse(qual.Value.ToString()));
                }

                Err = Epanet22md.ENinitQ(0);
                #endregion

                // t :  관망해석시 현재 시뮬레이션 클럭시간(초)(current simulation clock time in seconds). ENrunH에서 t 데이터타입이 long로 지정됨.
                long t = 0;

                Hashtable htAnalysisResult = new Hashtable();

                //(수질시간변화) 구분
                long tstep = 0;
                long lReportStep = 0;

                Epanet22md.ENgettimeparam(EN_REPORTSTEP, ref lReportStep);

                do
                {
                    Epanet22md.ENrunQ(ref t);

                    if ((t % lReportStep) == 0)
                    {
                        Hashtable htTimeResult = new Hashtable();
                        //htTimeResult.Add("Node", ExtractNodeAnalysisResultHashtable());
                        //htTimeResult.Add("Link", ExtractLinkAnalysisResultHashtable());
                        htTimeResult.Add("Node", ExtractNodeAnalysisResultTable());
                        htTimeResult.Add("Link", ExtractLinkAnalysisResultTable());

                        //해석시간을 key로 하여 node,junction 해석결과를 저장
                        htAnalysisResult.Add((Convert.ToString(t / 60 / 60)).PadLeft(2, '0') + ":" + Convert.ToString((t - (t / 60 / 60 * 60 * 60)) / 60).PadLeft(2, '0'), htTimeResult);
                    }

                    Epanet22md.ENnextQ(ref tstep);
                }
                while (tstep > 0);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
            finally
            {
                //수질해석결과 Close
                Epanet22md.ENcloseQ();
                //Close the toolkit
                Epanet22md.ENclose();
            }
        }

        /// <summary>
        /// 수질 체류
        /// </summary>
        /// <param name="strINPpath">inp 파일 경로</param>
        public Hashtable EpanetQage(string strINPpath)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            try
            {
                //inp파일 같은 경로에 rpt, out 파일 생성 경로
                DirInp = strINPpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_age_22.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_age_22.out";

                //Use of epanet functions
                Err = Epanet22md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit

                Epanet22md.ENsetqualtype(EN_AGE, "age", "", "1"); //EN_AGE 와 Node ID 설정(Index 아님)

                #region // 수리해석 실행(수질해석을 돌리기 위하여 먼저 수리해석을 돌려야 함) 실시간, 시간 구분
                Err = Epanet22md.ENsolveH();
                Err = Epanet22md.ENopenQ();
                Err = Epanet22md.ENinitQ(0);
                #endregion

                // t :  관망해석시 현재 시뮬레이션 클럭시간(초)(current simulation clock time in seconds). ENrunH에서 t 데이터타입이 long로 지정됨.
                long t = 1;

                Hashtable htAnalysisResult = new Hashtable();

                Epanet22md.ENrunQ(ref t);

                Hashtable htTimeResult = new Hashtable();
                //htTimeResult.Add("Node", ExtractNodeAnalysisResultHashtable());
                //htTimeResult.Add("Link", ExtractLinkAnalysisResultHashtable());
                htTimeResult.Add("Node", ExtractNodeAnalysisResultTable());
                htTimeResult.Add("Link", ExtractLinkAnalysisResultTable());

                htAnalysisResult.Add("00:00", htTimeResult);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
            finally
            {
                //수질해석결과 Close
                Epanet22md.ENcloseQ();
                //Close the toolkit
                Epanet22md.ENclose();
            }
        }

        /// <summary>
        /// 수질 체류
        /// </summary>
        /// <param name="strINPpath">inp 파일 경로</param>
        public Hashtable EpanetQtrace(string strINPpath)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            try
            {
                //inp파일 같은 경로에 rpt, out 파일 생성 경로
                DirInp = strINPpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_trace_22.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_trace_22.out";

                //Use of epanet functions
                Err = Epanet22md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit

                Epanet22md.ENsetqualtype(EN_TRACE, "trace", "", "1");      //EN_AGE 와 Node ID 설정(Index 아님)

                #region // 수리해석 실행(수질해석을 돌리기 위하여 먼저 수리해석을 돌려야 함) 실시간, 시간 구분
                Err = Epanet22md.ENsolveH();
                Err = Epanet22md.ENopenQ();
                Err = Epanet22md.ENinitQ(0);
                #endregion

                // t :  관망해석시 현재 시뮬레이션 클럭시간(초)(current simulation clock time in seconds). ENrunH에서 t 데이터타입이 long로 지정됨.
                long t = 1;

                Hashtable htAnalysisResult = new Hashtable();

                Epanet22md.ENrunQ(ref t);

                Hashtable htTimeResult = new Hashtable();
                //htTimeResult.Add("Node", ExtractNodeAnalysisResultHashtable());
                //htTimeResult.Add("Link", ExtractLinkAnalysisResultHashtable());
                htTimeResult.Add("Node", ExtractNodeAnalysisResultTable());
                htTimeResult.Add("Link", ExtractLinkAnalysisResultTable());

                htAnalysisResult.Add("00:00", htTimeResult);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
            finally
            {
                //수질해석결과 Close
                Epanet22md.ENcloseQ();
                //Close the toolkit
                Epanet22md.ENclose();
            }
        }

        /// <summary>
        /// MSX
        /// </summary>
        /// <param name="strINPpath"></param>
        /// <param name="strMSXpath"></param>
        /// <returns></returns>
        public Hashtable EpanetMSX(string strINPpath, string strMSXpath, string[] _strarryMSXType)
        {
            int Err = 0, FlowUnit = 0;
            int nLinks = 0, nNodes = 0;

            float fqual = 9999;
            int iIndex = 9999;

            try
            {
                DirInp = strINPpath;
                DirMSX = strMSXpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_msx_2.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_msx_2.out";

                #region // MSX(MSX을 돌리기 위하여 먼저 수리해석을 돌려야 함)
                Err = Epanet2md.ENopen(DirInp, DirRpt, DirOut); //Open the toolkit
                Err = Epanet2md.MSXopen(DirMSX);

                Err = Epanet2md.MSXsolveH();
                Err = Epanet2md.MSXsolveQ();
                Err = Epanet2md.MSXreport();
                #endregion

                int iNodeCnt = 0;                                                //노드의 총 개수
                int iNodeType = 0;                                               //노드의 타입
                StringBuilder sbNodeID = new StringBuilder();                    //노드의 아이디


                int iLinkCnt = 0;                                                //링크의 총 개수
                int iLinkType = 0;                                               //링크의 타입
                StringBuilder sbLinkID = new StringBuilder();                    //링크의 아이디

                string[] strarryMSXType = _strarryMSXType;                       //화학종... 추후에 msx에서 어떻게 파싱해서 가져올지???
                //string[] strarryMSXType = {"AS5"};                       //화학종... 추후에 msx에서 어떻게 파싱해서 가져올지???

                long t = 0;                                                      //분석할 시간 간격(MSX 파일에서 [OPTIONS] TIMESTEP)
                long tstep = 0;                                                  //분석할 총 시간(INP 파일에서 [TIMES]에서 Duration(시간)값을 초단위로 변환, 루프돌면서 t만큼 감소)

                int n = 0;
                int s = 0;

                double c = 0.0;
                double cMax = 0.0;

                DataSet dsNode = new DataSet();
                foreach (string stritem in strarryMSXType)
                {
                    DataTable dtNode = new DataTable() { TableName = stritem };
                    dtNode.Columns.Add("TYPE");
                    dtNode.Columns.Add("ID");
                    dtNode.Columns.Add("TIMESTEP");
                    dtNode.Columns.Add(stritem);
                    dsNode.Tables.Add(dtNode);
                }
                DataRow drNodeVal = null;

                DataSet dsLink = new DataSet();
                foreach (string stritem in strarryMSXType)
                {
                    DataTable dtLink = new DataTable() { TableName = stritem };
                    dtLink.Columns.Add("TYPE");
                    dtLink.Columns.Add("ID");
                    dtLink.Columns.Add("TIMESTEP");
                    dtLink.Columns.Add(stritem);
                    dsLink.Tables.Add(dtLink);
                }
                DataRow drLinkVal = null;

                #region 노드값 가져오기
                Err = Epanet2md.ENgetcount(EN_NODECOUNT, ref iNodeCnt);                //NODE의 총 개수 반환

                for (int i = 1; i <= iNodeCnt; i++)
                {
                    Err = Epanet2md.MSXinit(0);
                    Err = Epanet2md.ENgetnodeid2(i, sbNodeID);                         //NODE의 인덱스를 가지고 NODEID값을 반환
                    Err = Epanet2md.ENgetnodetype(i, ref iNodeType);                   //NODE의 인덱스를 가지고 NODETYPE값을 반환(JUNCTION-0, RESERVOIR-1, TANK-2) //Err = Epanet.ENgetnodeindex(sbNodeID.ToString(), ref n);          

                    do
                    {
                        Err = Epanet2md.MSXstep(ref t, ref tstep);

                        foreach (string stritem in strarryMSXType)
                        {
                            Err = Epanet2md.MSXgetindex(MSX_SPECIES, stritem, ref s);       //화학물질값 적용후 MSX에서 Index값 반환
                            Err = Epanet2md.MSXgetqual(MSX_NODE, i, s, ref c);             //첫번째인자 : 노드인지링크인지 명시, 두번째인자 : 노드의 인덱스, 세번째인자 : MSX에서의 노드 인덱스
                            drNodeVal = dsNode.Tables[stritem].NewRow();
                            drNodeVal["TYPE"] = iNodeType.ToString();
                            drNodeVal["ID"] = sbNodeID.ToString();
                            drNodeVal["TIMESTEP"] = t.ToString();
                            drNodeVal[stritem] = c.ToString();
                            dsNode.Tables[stritem].Rows.Add(drNodeVal);
                        }

                        if (c > cMax) cMax = c;
                    }
                    while (tstep > 0 && Err == 0);
                }
                #endregion

                #region 링크값 가져오기
                Err = Epanet2md.ENgetcount(EN_LINKCOUNT, ref iLinkCnt);                //LINK의 총 개수 반환

                for (int i = 1; i <= iLinkCnt; i++)
                {
                    Err = Epanet2md.MSXinit(0);
                    Err = Epanet2md.ENgetlinkid2(i, sbLinkID);                         //LINK의 인덱스를 가지고 NODEID값을 반환
                    Err = Epanet2md.ENgetlinktype(i, ref iLinkType);                   //LINK의 인덱스를 가지고 NODETYPE값을 반환 //Err = Epanet.ENgetnodeindex(sbLinkID.ToString(), ref n);          

                    do
                    {
                        Err = Epanet2md.MSXstep(ref t, ref tstep);

                        foreach (string stritem in strarryMSXType)
                        {
                            Err = Epanet2md.MSXgetindex(MSX_SPECIES, stritem, ref s);       //화학물질값 적용후 MSX에서 Index값 반환
                            Err = Epanet2md.MSXgetqual(MSX_LINK, i, s, ref c);             //첫번째인자 : 노드인지링크인지 명시, 두번째인자 : 노드의 인덱스, 세번째인자 : MSX에서의 노드 인덱스
                            drLinkVal = dsLink.Tables[stritem].NewRow();
                            drLinkVal["TYPE"] = iLinkType.ToString();
                            drLinkVal["ID"] = sbLinkID.ToString();
                            drLinkVal["TIMESTEP"] = t.ToString();
                            drLinkVal[stritem] = c.ToString();
                            dsLink.Tables[stritem].Rows.Add(drLinkVal);
                        }

                        if (c > cMax) cMax = c;
                    }
                    while (tstep > 0 && Err == 0);
                }
                #endregion

                //Close the MSX toolkit
                Epanet2md.MSXclose();

                //Close the toolkit
                Epanet2md.ENclose();

                Hashtable htAnalysisResult = new Hashtable();
                htAnalysisResult.Add("Node", dsNode);
                htAnalysisResult.Add("Link", dsLink);

                return htAnalysisResult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
        }

        /// <summary>
        /// Epanet Version
        /// </summary>
        public int EpanetVersion()
        {
            int iversion = 0;
            Epanet22md.ENgetversion(ref iversion);
            return iversion;
        }

        #region 결과 반환 관련
        /// <summary>
        /// 관망해석후 노드에 대한 결과값 반환
        /// </summary>
        /// <returns></returns>
        private Hashtable ExtractNodeAnalysisResultHashtable()
        {

            //전체 node 해석결과를 저장할 Hashtable
            Hashtable htNodeResult = new Hashtable();

            ArrayList alJunctionResult = new ArrayList();            //절점 해석결과
            ArrayList alReserviorResult = new ArrayList();           //배수지 해석결과
            ArrayList alTankResult = new ArrayList();                //tank 해석결과

            //Node Count 발췌
            int iNodeCount = 0;
            Epanet22md.ENgetcount(EN_NODECOUNT, ref iNodeCount);

            //Node 해석결과 변수
            float fElevationValue = 0;
            float fBaseDemandValue = 0;
            float fPatternValue = 0;
            float fEmitterValue = 0;
            float fInitQualValue = 0;
            float fSourceQualValue = 0;
            float fSourcePatValue = 0;
            float fSourceTypeValue = 0;
            float fTankLevelValue = 0;
            float fDemandValue = 0;
            float fHeadValue = 0;
            float fPressureValue = 0;
            float fQualityValue = 0;
            float fSourceMassValue = 0;
            float fInitVolumeValue = 0;
            float fMixModelValue = 0;
            float fMixZoneVolValue = 0;
            float fTankDiamValue = 0;
            float fMinVolumeValue = 0;
            float fVolCurveValue = 0;
            float fMinLevelValue = 0;
            float fMaxLevelValue = 0;
            float fMixFractionValue = 0;
            float fTankKbulkValue = 0;

            float fTankVolumeValue = 0;
            float fMaxVolumeVlaue = 0;
            float fCanOverFlow = 0;
            float fDemandDeficit = 0;

            for (int i = 1; i <= iNodeCount; i++)
            {
                // NodeID의 기본값을 string.Empty로 안했을경우, 메모리 오류 발생
                StringBuilder sbNodeID = new StringBuilder();

                int iNodeType = 0;

                Epanet22md.ENgetnodeid2(i, sbNodeID);
                Epanet22md.ENgetnodetype(i, ref iNodeType);

                Epanet22md.ENgetnodevalue(i, EN_ELEVATION, ref fElevationValue);                                                         //Elevation 
                Epanet22md.ENgetnodevalue(i, EN_BASEDEMAND, ref fBaseDemandValue);                                                //Base demand 
                Epanet22md.ENgetnodevalue(i, EN_PATTERN, ref fPatternValue);                                                              //Demand pattern index 
                Epanet22md.ENgetnodevalue(i, EN_EMITTER, ref fEmitterValue);                                                               //Emitter coeff. 
                Epanet22md.ENgetnodevalue(i, EN_INITQUAL, ref fInitQualValue);                                                             //Initial quality 
                Epanet22md.ENgetnodevalue(i, EN_SOURCEQUAL, ref fSourceQualValue);                                                 //Source quality 
                Epanet22md.ENgetnodevalue(i, EN_SOURCEPAT, ref fSourcePatValue);                                                     //Source pattern index
                Epanet22md.ENgetnodevalue(i, EN_SOURCETYPE, ref fSourceTypeValue);                                                 //Source type (See note below) 
                Epanet22md.ENgetnodevalue(i, EN_TANKLEVEL, ref fTankLevelValue);                                                       //Initial water level in tank 
                Epanet22md.ENgetnodevalue(i, EN_DEMAND, ref fDemandValue);                                                             //Actual demand 
                Epanet22md.ENgetnodevalue(i, EN_HEAD, ref fHeadValue);                                                                      //Hydraulic head 
                Epanet22md.ENgetnodevalue(i, EN_PRESSURE, ref fPressureValue);                                                         //Pressure 
                Epanet22md.ENgetnodevalue(i, EN_QUALITY, ref fQualityValue);                                                              //Actual quality 
                Epanet22md.ENgetnodevalue(i, EN_SOURCEMASS, ref fSourceMassValue);                                              //EN_SOURCEMASS 
                Epanet22md.ENgetnodevalue(i, EN_INITVOLUME, ref fInitVolumeValue);                                                  //Initial water volume 
                Epanet22md.ENgetnodevalue(i, EN_MIXMODEL, ref fMixModelValue);                                                       //Mixing model code (see below)
                Epanet22md.ENgetnodevalue(i, EN_MIXZONEVOL, ref fMixZoneVolValue);                                                //Inlet/Outlet zone volume in a 2-compartment tank 
                Epanet22md.ENgetnodevalue(i, EN_TANKDIAM, ref fTankDiamValue);                                                      //Tank diameter
                Epanet22md.ENgetnodevalue(i, EN_MINVOLUME, ref fMinVolumeValue);                                                  //Minimum water volume 
                Epanet22md.ENgetnodevalue(i, EN_VOLCURVE, ref fVolCurveValue);                                                       //Index of volume versus depth curve (0 if none assigned)
                Epanet22md.ENgetnodevalue(i, EN_MINLEVEL, ref fMinLevelValue);                                                         //Minimum water level 
                Epanet22md.ENgetnodevalue(i, EN_MAXLEVEL, ref fMaxLevelValue);                                                       //Maximum water level 
                Epanet22md.ENgetnodevalue(i, EN_MIXFRACTION, ref fMixFractionValue);                                              //Fraction of total volume occupied by the inlet/outlet zone in a 2-compartment tank 
                Epanet22md.ENgetnodevalue(i, EN_TANK_KBULK, ref fTankKbulkValue);                                                  //Bulk reaction rate coefficient 

                Epanet22md.ENgetnodevalue(i, EN_TANKVOLUME, ref fTankVolumeValue);
                Epanet22md.ENgetnodevalue(i, EN_MAXVOLUME, ref fMaxVolumeVlaue);
                Epanet22md.ENgetnodevalue(i, EN_CANOVERFLOW, ref fCanOverFlow);
                Epanet22md.ENgetnodevalue(i, EN_DEMANDDEFICIT, ref fDemandDeficit);



                Hashtable htTempResult = new Hashtable();

                htTempResult.Add("NODE_ID", sbNodeID.ToString());
                htTempResult.Add("EN_ELEVATION", fElevationValue);
                htTempResult.Add("EN_BASEDEMAND", fBaseDemandValue);
                htTempResult.Add("EN_PATTERN", fPatternValue);
                htTempResult.Add("EN_EMITTER", fEmitterValue);
                htTempResult.Add("EN_INITQUAL", fInitQualValue);
                htTempResult.Add("EN_SOURCEQUAL", fSourceQualValue);
                htTempResult.Add("EN_SOURCEPAT", fSourcePatValue);
                htTempResult.Add("EN_SOURCETYPE", fSourceTypeValue);
                htTempResult.Add("EN_TANKLEVEL", fTankLevelValue);
                htTempResult.Add("EN_DEMAND", fDemandValue);
                htTempResult.Add("EN_HEAD", fHeadValue);
                htTempResult.Add("EN_PRESSURE", fPressureValue);
                htTempResult.Add("EN_QUALITY", fQualityValue);
                htTempResult.Add("EN_SOURCEMASS", fSourceMassValue);
                htTempResult.Add("EN_INITVOLUME", fInitVolumeValue);
                htTempResult.Add("EN_MIXMODEL", fMixModelValue);
                htTempResult.Add("EN_MIXZONEVOL", fMixZoneVolValue);
                htTempResult.Add("EN_TANKDIAM", fTankDiamValue);
                htTempResult.Add("EN_MINVOLUME", fMinVolumeValue);
                htTempResult.Add("EN_VOLCURVE", fVolCurveValue);
                htTempResult.Add("EN_MINLEVEL", fMinLevelValue);
                htTempResult.Add("EN_MAXLEVEL", fMaxLevelValue);
                htTempResult.Add("EN_MIXFRACTION", fMixFractionValue);
                htTempResult.Add("EN_TANK_KBULK", fTankKbulkValue);

                htTempResult.Add("EN_TANKVOLUME", fTankVolumeValue);
                htTempResult.Add("EN_MAXVOLUME", fMaxVolumeVlaue);
                htTempResult.Add("EN_CANOVERFLOW", fCanOverFlow);
                htTempResult.Add("EN_DEMANDDEFICIT", fDemandDeficit);



                //Type에 따라 구분해서 저장한다.
                if (iNodeType == EN_JUNCTION)
                {
                    //절점해석결과
                    alJunctionResult.Add(htTempResult);
                }
                else if (iNodeType == EN_RESERVOIR)
                {
                    //배수지해석결과
                    alReserviorResult.Add(htTempResult);
                }
                else if (iNodeType == EN_TANK)
                {
                    //탱크해석결과
                    alTankResult.Add(htTempResult);
                }
            }

            htNodeResult.Add("Junction", alJunctionResult);
            htNodeResult.Add("Reservior", alReserviorResult);
            htNodeResult.Add("Tank", alTankResult);


            return htNodeResult;
        }

        /// <summary>
        /// 관망해석후 링크에 대한 결과값 반환
        /// </summary>
        /// <returns></returns>
        private Hashtable ExtractLinkAnalysisResultHashtable()
        {

            //전체 Link 해석결과를 저장할 Hashtable
            Hashtable htLinkResult = new Hashtable();

            ArrayList alCVPipeResult = new ArrayList();              //Pipe with Check Valve
            ArrayList alPipeResult = new ArrayList();                //pipe
            ArrayList alPumpResult = new ArrayList();                //pump
            ArrayList alPRVResult = new ArrayList();                 //Pressure Reducing Valve
            ArrayList alPSVResult = new ArrayList();                 //Pressure Sustaining Valve
            ArrayList alPBVResult = new ArrayList();                 //Pressure Breaker Valve
            ArrayList alFCVResult = new ArrayList();                 //Flow Control Valve
            ArrayList alTCVResult = new ArrayList();                 //Throttle Control Valve
            ArrayList alGPVResult = new ArrayList();                 //General Purpose Valve

            //Link Count 발췌
            int iLinkCount = 0;
            Epanet22md.ENgetcount(EN_LINKCOUNT, ref iLinkCount);

            //Link 해석결과 변수
            float fDiameterValue = 0;
            float fLengthValue = 0;
            float fRoughnessValue = 0;
            float fMinorLossValue = 0;
            float fInitStatusValue = 0;
            float fInitSettingValue = 0;
            float fKbulkValue = 0;
            float fKwallValue = 0;
            float fFlowValue = 0;
            float fVelocityValue = 0;
            float fHeadlossValue = 0;
            float fStatusValue = 0;
            float fSettingValue = 0;
            float fEnergyValue = 0;

            float fQualityValue = 0;
            float fPatternValue = 0;
            float fPumpStateValue = 0;
            float fPumpEfficValue = 0;
            float fPumpPowerValue = 0;
            float fPumpHCurveValue = 0;
            float fPumpECurveValue = 0;
            float fPumpECostValue = 0;
            float fPumpEPatValue = 0;

            for (int i = 1; i <= iLinkCount; i++)
            {
                StringBuilder sbLinkID = new StringBuilder();

                int iLinkType = 0;

                Epanet22md.ENgetlinkid(i, sbLinkID);
                Epanet22md.ENgetlinktype(i, ref iLinkType);

                Epanet22md.ENgetlinkvalue(i, EN_DIAMETER, ref fDiameterValue);                                                        //Diameter 
                Epanet22md.ENgetlinkvalue(i, EN_LENGTH, ref fLengthValue);                                                               //Length 
                Epanet22md.ENgetlinkvalue(i, EN_ROUGHNESS, ref fRoughnessValue);                                                  //Roughness coeff. 
                Epanet22md.ENgetlinkvalue(i, EN_MINORLOSS, ref fMinorLossValue);                                                    //Minor loss coeff.
                Epanet22md.ENgetlinkvalue(i, EN_INITSTATUS, ref fInitStatusValue);                                                    //Initial link status (0 = closed, 1 = open)
                Epanet22md.ENgetlinkvalue(i, EN_INITSETTING, ref fInitSettingValue);                                                  //Roughness for pipes, initial speed for pumps, initial setting for valves
                Epanet22md.ENgetlinkvalue(i, EN_KBULK, ref fKbulkValue);                                                                   //Bulk reaction coeff. 
                Epanet22md.ENgetlinkvalue(i, EN_KWALL, ref fKwallValue);                                                                  //Wall reaction coeff.
                Epanet22md.ENgetlinkvalue(i, EN_FLOW, ref fFlowValue);                                                                    //Flow rate 
                Epanet22md.ENgetlinkvalue(i, EN_VELOCITY, ref fVelocityValue);                                                          //Flow velocity
                Epanet22md.ENgetlinkvalue(i, EN_HEADLOSS, ref fHeadlossValue);                                                       //Head loss 
                Epanet22md.ENgetlinkvalue(i, EN_STATUS, ref fStatusValue);                                                              //Actual link status (0 = closed, 1 = open)
                Epanet22md.ENgetlinkvalue(i, EN_SETTING, ref fSettingValue);                                                            //Roughness for pipes, actual speed for pumps, actual setting for valves
                Epanet22md.ENgetlinkvalue(i, EN_ENERGY, ref fEnergyValue);                                                              //Energy expended in kwatts 

                Epanet22md.ENgetlinkvalue(i, EN_LINKQUAL, ref fQualityValue);
                Epanet22md.ENgetlinkvalue(i, EN_LINKPATTERN, ref fPatternValue);
                Epanet22md.ENgetlinkvalue(i, EN_PUMP_STATE, ref fPumpStateValue);
                Epanet22md.ENgetlinkvalue(i, EN_PUMP_EFFIC, ref fPumpEfficValue);
                Epanet22md.ENgetlinkvalue(i, EN_PUMP_POWER, ref fPumpPowerValue);
                Epanet22md.ENgetlinkvalue(i, EN_PUMP_HCURVE, ref fPumpHCurveValue);
                Epanet22md.ENgetlinkvalue(i, EN_PUMP_ECURVE, ref fPumpECurveValue);
                Epanet22md.ENgetlinkvalue(i, EN_PUMP_ECOST, ref fPumpECostValue);
                Epanet22md.ENgetlinkvalue(i, EN_PUMP_EPAT, ref fPumpEPatValue);

                Hashtable htTempResult = new Hashtable();

                htTempResult.Add("LINK_ID", sbLinkID.ToString());
                htTempResult.Add("EN_DIAMETER", fDiameterValue);
                htTempResult.Add("EN_LENGTH", fLengthValue);
                htTempResult.Add("EN_ROUGHNESS", fRoughnessValue);
                htTempResult.Add("EN_MINORLOSS", fMinorLossValue);
                htTempResult.Add("EN_INITSTATUS", fInitStatusValue);
                htTempResult.Add("EN_INITSETTING", fInitSettingValue);
                htTempResult.Add("EN_KBULK", fKbulkValue);
                htTempResult.Add("EN_KWALL", fKwallValue);
                htTempResult.Add("EN_FLOW", fFlowValue);
                htTempResult.Add("EN_VELOCITY", fVelocityValue);
                htTempResult.Add("EN_HEADLOSS", fHeadlossValue);
                htTempResult.Add("EN_STATUS", fStatusValue);
                htTempResult.Add("EN_SETTING", fSettingValue);
                htTempResult.Add("EN_ENERGY", fEnergyValue);

                htTempResult.Add("EN_LINKQUAL", fQualityValue);
                htTempResult.Add("EN_LINKPATTERN", fPatternValue);
                htTempResult.Add("EN_PUMP_STATE", fPumpStateValue);
                htTempResult.Add("EN_PUMP_EFFIC", fPumpEfficValue);
                htTempResult.Add("EN_PUMP_POWER", fPumpPowerValue);
                htTempResult.Add("EN_PUMP_HCURVE", fPumpHCurveValue);
                htTempResult.Add("EN_PUMP_ECURVE", fPumpECurveValue);
                htTempResult.Add("EN_PUMP_ECOST", fPumpECostValue);
                htTempResult.Add("EN_PUMP_EPAT", fPumpEPatValue);

                //Type에 따라 구분해서 저장한다.
                if (iLinkType == EN_CVPIPE)
                {
                    alCVPipeResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PIPE)
                {
                    alPipeResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PUMP)
                {
                    alPumpResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PRV)
                {
                    alPRVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PSV)
                {
                    alPSVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_PBV)
                {
                    alPBVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_FCV)
                {
                    alFCVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_TCV)
                {
                    alTCVResult.Add(htTempResult);
                }
                else if (iLinkType == EN_GPV)
                {
                    alGPVResult.Add(htTempResult);
                }
            }

            htLinkResult.Add("CVPipe", alCVPipeResult);
            htLinkResult.Add("Pipe", alPipeResult);
            htLinkResult.Add("Pump", alPumpResult);
            htLinkResult.Add("PRV", alPRVResult);
            htLinkResult.Add("PSV", alPSVResult);
            htLinkResult.Add("PBV", alPBVResult);
            htLinkResult.Add("FCV", alFCVResult);
            htLinkResult.Add("TCV", alTCVResult);
            htLinkResult.Add("GPV", alGPVResult);

            return htLinkResult;
        }

        /// <summary>
        /// 관망해석후 노드에 대한 결과값 반환 Table
        /// </summary>
        /// <returns></returns>
        private DataTable ExtractNodeAnalysisResultTable()
        {
            DataTable dtresult = new DataTable();
            dtresult.Columns.Add("NODE_CAT");           //링크 구분
            dtresult.Columns.Add("NODE_ID");            //link ID
            dtresult.Columns.Add("EN_ELEVATION");       //Elevation 
            dtresult.Columns.Add("EN_BASEDEMAND");      //Base demand 
            dtresult.Columns.Add("EN_PATTERN");         //Demand pattern index 
            dtresult.Columns.Add("EN_EMITTER");         //Emitter coeff. 
            dtresult.Columns.Add("EN_INITQUAL");        //Initial quality 
            dtresult.Columns.Add("EN_SOURCEQUAL");      //Source quality 
            dtresult.Columns.Add("EN_SOURCEPAT");       //Source pattern index
            dtresult.Columns.Add("EN_SOURCETYPE");      //Source type (See note below) 
            dtresult.Columns.Add("EN_TANKLEVEL");       //Initial water level in tank 
            dtresult.Columns.Add("EN_DEMAND");          //Actual demand 
            dtresult.Columns.Add("EN_HEAD");            //Hydraulic head 
            dtresult.Columns.Add("EN_PRESSURE");        //Pressure 
            dtresult.Columns.Add("EN_QUALITY");         //Actual quality 
            dtresult.Columns.Add("EN_SOURCEMASS");      //EN_SOURCEMASS 
            dtresult.Columns.Add("EN_INITVOLUME");      //Initial water volume 
            dtresult.Columns.Add("EN_MIXMODEL");        //Mixing model code (see below)
            dtresult.Columns.Add("EN_MIXZONEVOL");      //Inlet/Outlet zone volume in a 2-compartment tank 
            dtresult.Columns.Add("EN_TANKDIAM");        //Tank diameter
            dtresult.Columns.Add("EN_MINVOLUME");       //Minimum water volume 
            dtresult.Columns.Add("EN_VOLCURVE");        //Index of volume versus depth curve (0 if none assigned)
            dtresult.Columns.Add("EN_MINLEVEL");        //Minimum water level 
            dtresult.Columns.Add("EN_MAXLEVEL");        //Maximum water level 
            dtresult.Columns.Add("EN_MIXFRACTION");     //Fraction of total volume occupied by the inlet/outlet zone in a 2-compartment tank 
            dtresult.Columns.Add("EN_TANK_KBULK");      //Bulk reaction rate coefficient 

            dtresult.Columns.Add("EN_TANKVOLUME");      
            dtresult.Columns.Add("EN_MAXVOLUME");       
            dtresult.Columns.Add("EN_CANOVERFLOW");     
            dtresult.Columns.Add("EN_DEMANDDEFICIT");   

            //Node 해석결과 변수
            float fElevationValue = 0;
            float fBaseDemandValue = 0;
            float fPatternValue = 0;
            float fEmitterValue = 0;
            float fInitQualValue = 0;
            float fSourceQualValue = 0;
            float fSourcePatValue = 0;
            float fSourceTypeValue = 0;
            float fTankLevelValue = 0;
            float fDemandValue = 0;
            float fHeadValue = 0;
            float fPressureValue = 0;
            float fQualityValue = 0;
            float fSourceMassValue = 0;
            float fInitVolumeValue = 0;
            float fMixModelValue = 0;
            float fMixZoneVolValue = 0;
            float fTankDiamValue = 0;
            float fMinVolumeValue = 0;
            float fVolCurveValue = 0;
            float fMinLevelValue = 0;
            float fMaxLevelValue = 0;
            float fMixFractionValue = 0;
            float fTankKbulkValue = 0;

            float fTankVolumeValue = 0;
            float fMaxVolumeVlaue = 0;
            float fCanOverFlow = 0;
            float fDemandDeficit = 0;

            try
            {
                //Node Count 발췌
                int iNodeCount = 0;
                Epanet22md.ENgetcount(EN_NODECOUNT, ref iNodeCount);

                for (int i = 1; i <= iNodeCount; i++)
                {
                    // NodeID의 기본값을 string.Empty로 안했을경우, 메모리 오류 발생
                    StringBuilder sbNodeID = new StringBuilder();

                    int iNodeType = 0;

                    Epanet22md.ENgetnodeid2(i, sbNodeID);
                    Epanet22md.ENgetnodetype(i, ref iNodeType);

                    Epanet22md.ENgetnodevalue(i, EN_ELEVATION, ref fElevationValue);                  //Elevation 
                    Epanet22md.ENgetnodevalue(i, EN_BASEDEMAND, ref fBaseDemandValue);                //Base demand 
                    Epanet22md.ENgetnodevalue(i, EN_PATTERN, ref fPatternValue);                      //Demand pattern index 
                    Epanet22md.ENgetnodevalue(i, EN_EMITTER, ref fEmitterValue);                      //Emitter coeff. 
                    Epanet22md.ENgetnodevalue(i, EN_INITQUAL, ref fInitQualValue);                    //Initial quality 
                    Epanet22md.ENgetnodevalue(i, EN_SOURCEQUAL, ref fSourceQualValue);                //Source quality 
                    Epanet22md.ENgetnodevalue(i, EN_SOURCEPAT, ref fSourcePatValue);                  //Source pattern index
                    Epanet22md.ENgetnodevalue(i, EN_SOURCETYPE, ref fSourceTypeValue);                //Source type (See note below) 
                    Epanet22md.ENgetnodevalue(i, EN_TANKLEVEL, ref fTankLevelValue);                  //Initial water level in tank 
                    Epanet22md.ENgetnodevalue(i, EN_DEMAND, ref fDemandValue);                        //Actual demand 
                    Epanet22md.ENgetnodevalue(i, EN_HEAD, ref fHeadValue);                            //Hydraulic head 
                    Epanet22md.ENgetnodevalue(i, EN_PRESSURE, ref fPressureValue);                    //Pressure 
                    Epanet22md.ENgetnodevalue(i, EN_QUALITY, ref fQualityValue);                      //Actual quality 
                    Epanet22md.ENgetnodevalue(i, EN_SOURCEMASS, ref fSourceMassValue);                //EN_SOURCEMASS 
                    Epanet22md.ENgetnodevalue(i, EN_INITVOLUME, ref fInitVolumeValue);                //Initial water volume 
                    Epanet22md.ENgetnodevalue(i, EN_MIXMODEL, ref fMixModelValue);                    //Mixing model code (see below)
                    Epanet22md.ENgetnodevalue(i, EN_MIXZONEVOL, ref fMixZoneVolValue);                //Inlet/Outlet zone volume in a 2-compartment tank 
                    Epanet22md.ENgetnodevalue(i, EN_TANKDIAM, ref fTankDiamValue);                    //Tank diameter
                    Epanet22md.ENgetnodevalue(i, EN_MINVOLUME, ref fMinVolumeValue);                  //Minimum water volume 
                    Epanet22md.ENgetnodevalue(i, EN_VOLCURVE, ref fVolCurveValue);                    //Index of volume versus depth curve (0 if none assigned)
                    Epanet22md.ENgetnodevalue(i, EN_MINLEVEL, ref fMinLevelValue);                    //Minimum water level 
                    Epanet22md.ENgetnodevalue(i, EN_MAXLEVEL, ref fMaxLevelValue);                    //Maximum water level 
                    Epanet22md.ENgetnodevalue(i, EN_MIXFRACTION, ref fMixFractionValue);              //Fraction of total volume occupied by the inlet/outlet zone in a 2-compartment tank 
                    Epanet22md.ENgetnodevalue(i, EN_TANK_KBULK, ref fTankKbulkValue);                 //Bulk reaction rate coefficient 

                    Epanet22md.ENgetnodevalue(i, EN_TANKVOLUME, ref fTankVolumeValue);
                    Epanet22md.ENgetnodevalue(i, EN_MAXVOLUME, ref fMaxVolumeVlaue);
                    Epanet22md.ENgetnodevalue(i, EN_CANOVERFLOW, ref fCanOverFlow);
                    //Epanet22md.ENgetnodevalue(i, EN_DEMANDDEFICIT, ref fDemandDeficit);

                    DataRow dradd = dtresult.NewRow();

                    switch (iNodeType)
                    {
                        case EN_JUNCTION:
                            dradd["NODE_CAT"] = "Junction";
                            break;
                        case EN_RESERVOIR:
                            dradd["NODE_CAT"] = "Reservior";
                            break;
                        case EN_TANK:
                            dradd["NODE_CAT"] = "Tank";
                            break;
                    }

                    dradd["NODE_ID"] = sbNodeID.ToString();
                    dradd["EN_ELEVATION"] = fElevationValue;
                    dradd["EN_BASEDEMAND"] = fBaseDemandValue;
                    dradd["EN_PATTERN"] = fPatternValue;
                    dradd["EN_EMITTER"] = fEmitterValue;
                    dradd["EN_INITQUAL"] = fInitQualValue;
                    dradd["EN_SOURCEQUAL"] = fSourceQualValue;
                    dradd["EN_SOURCEPAT"] = fSourcePatValue;
                    dradd["EN_SOURCETYPE"] = fSourceTypeValue;
                    dradd["EN_TANKLEVEL"] = fTankLevelValue;
                    dradd["EN_DEMAND"] = fDemandValue;
                    dradd["EN_HEAD"] = fHeadValue;
                    dradd["EN_PRESSURE"] = fPressureValue;
                    dradd["EN_QUALITY"] = fQualityValue;
                    dradd["EN_SOURCEMASS"] = fSourceMassValue;
                    dradd["EN_INITVOLUME"] = fInitVolumeValue;
                    dradd["EN_MIXMODEL"] = fMixModelValue;
                    dradd["EN_MIXZONEVOL"] = fMixZoneVolValue;
                    dradd["EN_TANKDIAM"] = fTankDiamValue;
                    dradd["EN_MINVOLUME"] = fMinVolumeValue;
                    dradd["EN_VOLCURVE"] = fVolCurveValue;
                    dradd["EN_MINLEVEL"] = fMinLevelValue;
                    dradd["EN_MAXLEVEL"] = fMaxLevelValue;
                    dradd["EN_MIXFRACTION"] = fMixFractionValue;
                    dradd["EN_TANK_KBULK"] = fTankKbulkValue;

                    dradd["EN_TANKVOLUME"] = fTankVolumeValue;
                    dradd["EN_MAXVOLUME"] = fMaxVolumeVlaue;
                    dradd["EN_CANOVERFLOW"] = fCanOverFlow;
                    dradd["EN_DEMANDDEFICIT"] = fDemandDeficit;

                    dtresult.Rows.Add(dradd.ItemArray);
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 관망해석후 링크에 대한 결과값 반환
        /// </summary>
        /// <returns></returns>
        private DataTable ExtractLinkAnalysisResultTable()
        {
            DataTable dtresult = new DataTable();
            dtresult.Columns.Add("LINK_CAT");           //링크 구분
            dtresult.Columns.Add("LINK_ID");            //link ID
            dtresult.Columns.Add("EN_DIAMETER");        //Diameter 
            dtresult.Columns.Add("EN_LENGTH");          //Length 
            dtresult.Columns.Add("EN_ROUGHNESS");       //Roughness coeff. 
            dtresult.Columns.Add("EN_MINORLOSS");       //Minor loss coeff.
            dtresult.Columns.Add("EN_INITSTATUS");      //Initial link status (0 = closed, 1 = open)
            dtresult.Columns.Add("EN_INITSETTING");     //Roughness for pipes, initial speed for pumps, initial setting for valves
            dtresult.Columns.Add("EN_KBULK");           //Bulk reaction coeff. 
            dtresult.Columns.Add("EN_KWALL");           //Wall reaction coeff.
            dtresult.Columns.Add("EN_FLOW");            //Flow rate 
            dtresult.Columns.Add("EN_VELOCITY");        //Flow velocity
            dtresult.Columns.Add("EN_HEADLOSS");        //Head loss 
            dtresult.Columns.Add("EN_STATUS");          //Actual link status (0 = closed, 1 = open)
            dtresult.Columns.Add("EN_SETTING");         //Roughness for pipes, actual speed for pumps, actual setting for valves
            dtresult.Columns.Add("EN_ENERGY");          //Energy expended in kwatts 

            dtresult.Columns.Add("EN_LINKQUAL");          
            dtresult.Columns.Add("EN_LINKPATTERN");       
            dtresult.Columns.Add("EN_PUMP_STATE");        
            dtresult.Columns.Add("EN_PUMP_EFFIC");        
            dtresult.Columns.Add("EN_PUMP_POWER");        
            dtresult.Columns.Add("EN_PUMP_HCURVE");       
            dtresult.Columns.Add("EN_PUMP_ECURVE");       
            dtresult.Columns.Add("EN_PUMP_ECOST");        
            dtresult.Columns.Add("EN_PUMP_EPAT");         

            //Link 해석결과 변수
            float fDiameterValue = 0;
            float fLengthValue = 0;
            float fRoughnessValue = 0;
            float fMinorLossValue = 0;
            float fInitStatusValue = 0;
            float fInitSettingValue = 0;
            float fKbulkValue = 0;
            float fKwallValue = 0;
            float fFlowValue = 0;
            float fVelocityValue = 0;
            float fHeadlossValue = 0;
            float fStatusValue = 0;
            float fSettingValue = 0;
            float fEnergyValue = 0;

            float fQualityValue = 0;
            float fPatternValue = 0;
            float fPumpStateValue = 0;
            float fPumpEfficValue = 0;
            float fPumpPowerValue = 0;
            float fPumpHCurveValue = 0;
            float fPumpECurveValue = 0;
            float fPumpECostValue = 0;
            float fPumpEPatValue = 0;

            try
            {
                //Link Count 발췌
                int iLinkCount = 0;
                Epanet22md.ENgetcount(EN_LINKCOUNT, ref iLinkCount);

                for (int i = 1; i <= iLinkCount; i++)
                {
                    StringBuilder sbLinkID = new StringBuilder();

                    int iLinkType = 0;

                    Epanet22md.ENgetlinkid2(i, sbLinkID);
                    Epanet22md.ENgetlinktype(i, ref iLinkType);

                    Epanet22md.ENgetlinkvalue(i, EN_DIAMETER, ref fDiameterValue);                                                        //Diameter 
                    Epanet22md.ENgetlinkvalue(i, EN_LENGTH, ref fLengthValue);                                                               //Length 
                    Epanet22md.ENgetlinkvalue(i, EN_ROUGHNESS, ref fRoughnessValue);                                                  //Roughness coeff. 
                    Epanet22md.ENgetlinkvalue(i, EN_MINORLOSS, ref fMinorLossValue);                                                    //Minor loss coeff.
                    Epanet22md.ENgetlinkvalue(i, EN_INITSTATUS, ref fInitStatusValue);                                                    //Initial link status (0 = closed, 1 = open)
                    Epanet22md.ENgetlinkvalue(i, EN_INITSETTING, ref fInitSettingValue);                                                  //Roughness for pipes, initial speed for pumps, initial setting for valves
                    Epanet22md.ENgetlinkvalue(i, EN_KBULK, ref fKbulkValue);                                                                   //Bulk reaction coeff. 
                    Epanet22md.ENgetlinkvalue(i, EN_KWALL, ref fKwallValue);                                                                  //Wall reaction coeff.
                    Epanet22md.ENgetlinkvalue(i, EN_FLOW, ref fFlowValue);                                                                    //Flow rate 
                    Epanet22md.ENgetlinkvalue(i, EN_VELOCITY, ref fVelocityValue);                                                          //Flow velocity
                    Epanet22md.ENgetlinkvalue(i, EN_HEADLOSS, ref fHeadlossValue);                                                       //Head loss 
                    Epanet22md.ENgetlinkvalue(i, EN_STATUS, ref fStatusValue);                                                              //Actual link status (0 = closed, 1 = open)
                    Epanet22md.ENgetlinkvalue(i, EN_SETTING, ref fSettingValue);                                                            //Roughness for pipes, actual speed for pumps, actual setting for valves
                    Epanet22md.ENgetlinkvalue(i, EN_ENERGY, ref fEnergyValue);                                                              //Energy expended in kwatts 

                    Epanet22md.ENgetlinkvalue(i, EN_LINKQUAL, ref fQualityValue);
                    Epanet22md.ENgetlinkvalue(i, EN_LINKPATTERN, ref fPatternValue);
                    Epanet22md.ENgetlinkvalue(i, EN_PUMP_STATE, ref fPumpStateValue);
                    Epanet22md.ENgetlinkvalue(i, EN_PUMP_EFFIC, ref fPumpEfficValue);
                    Epanet22md.ENgetlinkvalue(i, EN_PUMP_POWER, ref fPumpPowerValue);
                    Epanet22md.ENgetlinkvalue(i, EN_PUMP_HCURVE, ref fPumpHCurveValue);
                    Epanet22md.ENgetlinkvalue(i, EN_PUMP_ECURVE, ref fPumpECurveValue);
                    Epanet22md.ENgetlinkvalue(i, EN_PUMP_ECOST, ref fPumpECostValue);
                    Epanet22md.ENgetlinkvalue(i, EN_PUMP_EPAT, ref fPumpEPatValue);


                    DataRow dradd = dtresult.NewRow();

                    switch (iLinkType)
                    {
                        case EN_CVPIPE:
                            dradd["LINK_CAT"] = "CVPipe";
                            break;
                        case EN_PIPE:
                            dradd["LINK_CAT"] = "Pipe";
                            break;
                        case EN_PUMP:
                            dradd["LINK_CAT"] = "Pump";
                            break;
                        case EN_PRV:
                            dradd["LINK_CAT"] = "PRV";
                            break;
                        case EN_PSV:
                            dradd["LINK_CAT"] = "PSV";
                            break;
                        case EN_PBV:
                            dradd["LINK_CAT"] = "PBV";
                            break;
                        case EN_FCV:
                            dradd["LINK_CAT"] = "FCV";
                            break;
                        case EN_TCV:
                            dradd["LINK_CAT"] = "TCV";
                            break;
                        case EN_GPV:
                            dradd["LINK_CAT"] = "GPV";
                            break;
                    }
                    dradd["LINK_ID"] = sbLinkID.ToString();
                    dradd["EN_DIAMETER"] = fDiameterValue;
                    dradd["EN_LENGTH"] = fLengthValue;
                    dradd["EN_ROUGHNESS"] = fRoughnessValue;
                    dradd["EN_MINORLOSS"] = fMinorLossValue;
                    dradd["EN_INITSTATUS"] = fInitStatusValue;
                    dradd["EN_INITSETTING"] = fInitSettingValue;
                    dradd["EN_KBULK"] = fKbulkValue;
                    dradd["EN_KWALL"] = fKwallValue;
                    dradd["EN_FLOW"] = fFlowValue;
                    dradd["EN_VELOCITY"] = fVelocityValue;
                    dradd["EN_HEADLOSS"] = fHeadlossValue;
                    dradd["EN_STATUS"] = fStatusValue;
                    dradd["EN_SETTING"] = fSettingValue;
                    dradd["EN_ENERGY"] = fEnergyValue;

                    dradd["EN_LINKQUAL"] = fQualityValue;
                    dradd["EN_LINKPATTERN"] = fPatternValue;
                    dradd["EN_PUMP_STATE"] = fPumpStateValue;
                    dradd["EN_PUMP_EFFIC"] = fPumpEfficValue;
                    dradd["EN_PUMP_POWER"] = fPumpPowerValue;
                    dradd["EN_PUMP_HCURVE"] = fPumpHCurveValue;
                    dradd["EN_PUMP_ECURVE"] = fPumpECurveValue;
                    dradd["EN_PUMP_ECOST"] = fPumpECostValue;
                    dradd["EN_PUMP_EPAT"] = fPumpEPatValue;

                    dtresult.Rows.Add(dradd.ItemArray);
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
