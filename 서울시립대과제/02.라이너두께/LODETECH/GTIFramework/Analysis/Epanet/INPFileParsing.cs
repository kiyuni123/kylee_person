﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace GTIFramework.Analysis.Epanet
{
    public class INPFileParsing
    {
        string INPpath = string.Empty;
        Hashtable htSection;
        Hashtable htINPResultFied;

        string[] strSetions =
            { "JUNCTIONS", "RESERVOIRS", "TANKS", "PIPES", "PUMPS"
                , "VALVES", "DEMANDS", "ENERGY", "TIMES", "OPTIONS"
                , "COORDINATES", "PATTERNS", "CURVES", "TITLE", "STATUS"
                , "CONTROLS", "EMITTERS", "QUALITY", "SOURCES", "REACTIONS"
                , "MIXING", "REPORT", "VERTICES", "LABELS", "BACKDROP"
                , "TAGS", "RULES"};

        string[] strINPpipe = {"SHP", "ID", "Start Node", "End Node", "Description"
                        , "Tag", "Length", "Diameter", "Roughness", "Loss Coeff."
                        , "InitialStatus", "Bulk Coeff.", "Wall Coeff.", "Flow", "Velocity"
                        , "Unit Headloss", "Friction Factor", "Reaction Rate", "Quality", "Status" };
        string[] strINPpump = {"SHP", "ID", "Start Node", "End Node", "Description"
                        , "Tag", "Pump Curve", "Power", "Speed", "Pattern"
                        , "Initial Status", "Effic. Curve", "Energy Price", "Price Pattern", "Flow"
                        , "Headloss", "Quality", "Status" };
        string[] strINPvalve = {"SHP", "ID", "Start Node", "End Node", "Description", "Tag"
                        , "Diameter", "Type", "Setting", "Loss Coeff.", "Fixed Status"
                        , "Flow", "Velocity", "Headloss", "Quality", "Status"};
        string[] strINPjunction = {"SHP", "ID", "X-Coordinate", "Y-Coordinate", "Description"
                        , "Tag", "Elevation", "Base Demand", "Demand Pattern", "Demand Categories"
                        , "Emitter Coeff.", "Initial Quality", "Source Quality", "Actual Demand", "Total Head"
                        , "Pressure", "Quality"};
        string[] strINPtank = {"SHP", "ID", "X-Coordinate", "Y-Coordinate", "Description"
                        , "Tag", "*Elevation", "Initial Level", "MinimumLevel", "MaximumLevel"
                        , "Diameter", "MinimumVolume", "Volum Curve", "Can Overflow", "Mixing Model"
                        , "Mixing Fraction", "Reaction Coeff.", "Initial Quality", "Source Quality", "Net Inflow"
                        , "Elevation", "Pressure", "Quality"};
        string[] strINPreservoir = {"SHP", "ID", "X-Coordinate", "Y-Coordinate", "Description"
                        , "Tag", "Total Head", "Head Pattern", "Initial Quality", "Source Quality"
                        , "Net Inflow", "Elevation", "Pressure", "Quality"};

        public INPFileParsing()
        {
            htINPResultFied = new Hashtable();
            htINPResultFied.Add("PIPE", strINPpipe);
            htINPResultFied.Add("PUMP", strINPpump);
            htINPResultFied.Add("VALVE", strINPvalve);
            htINPResultFied.Add("JUNCTION", strINPjunction);
            htINPResultFied.Add("TANK", strINPtank);
            htINPResultFied.Add("RESERVOIR", strINPreservoir);
        }

        /// <summary>
        /// inp 파일 로드
        /// </summary>
        /// <param name="_INPpath"></param>
        /// <returns></returns>
        public INPFile INPFileLoad(string _INPpath)
        {
            try
            {
                INPpath = _INPpath;
                string strINPData = System.IO.File.ReadAllText(INPpath, Encoding.Default);

                INPFile inpfile = INPParsing(strINPData);

                return inpfile;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }

        }

        /// <summary>
        /// inp 파일 해석
        /// </summary>
        /// <param name="INPParsing"></param>
        /// <returns></returns>
        private INPFile INPParsing(string INPParsing)
        {
            try
            {
                INPFile inpfile = new INPFile();

                string[] INPlines;

                #region INP 문자열 분석
                if (Regex.Match(INPParsing, @"\r\n").Length > 0)
                    INPlines = Regex.Split(INPParsing, "\r\n");
                else if (Regex.Match(INPParsing, @"\n").Length > 0)
                    INPlines = Regex.Split(INPParsing, "\n");
                else
                    INPlines = null;


                //gsub( ";.*$", "", allLines)
                for (int i = 0; i < INPlines.Length - 1; i++)
                {
                    if (INPlines[i].Length > 0)
                    {
                        if (INPlines[i].IndexOf(';') == 0)
                            INPlines[i] = INPlines[i].Remove(INPlines[i].IndexOf(';'), INPlines[i].Length - INPlines[i].IndexOf(';'));
                    }
                }
                #endregion

                #region INP 세션 구분
                htSection = INPSection(INPlines);
                #endregion

                #region INP 분석
                //inpfile.Title = TITLE();
                inpfile.Junctions = JUNCTIONS();
                inpfile.Tanks = TANKS();
                inpfile.Reservoirs = RESERVOIRS();
                inpfile.Pipes = PIPES();
                inpfile.Pumps = PUMPS();
                inpfile.Valves = VALVES();
                inpfile.Demands = DEMANDS();
                inpfile.Status = STATUS();
                inpfile.Emitters = EMITTERS();
                inpfile.Quality = QUALITY();
                inpfile.Sources = SOURCES();
                inpfile.Reactions = REACTIONS();
                inpfile.Mixing = MIXING();
                inpfile.Patterns = PATTERNS();
                //inpfile.Curves = null;
                //inpfile.Controls = CONTROLS();
                //inpfile.Rules = RULES();
                inpfile.Energy = ENERGY();
                inpfile.Times = TIMES();
                //inpfile.Report = REPORT();
                inpfile.Options = OPTIONS();
                inpfile.Coordinates = COORDINATES();
                inpfile.Vertices = VERTICES();
                inpfile.Labels = LABELS();
                //inpfile.Backdrop = null;
                //inpfile.Tags = null;

                inpfile.htINPResultFied = htINPResultFied;

                #endregion

                return inpfile;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }

        #region INPParsing 내에서 사용
        /// <summary>
        /// INP 파일 Section 구분
        /// </summary>
        /// <param name="INPlines"></param>
        /// <returns></returns>
        private Hashtable INPSection(string[] INPlines)
        {
            try
            {
                Hashtable htresult = new Hashtable();

                int endlineflag = 0;

                //세션별 구분
                foreach (string strSetion in strSetions)
                {
                    INPSession session = new INPSession();

                    //세션별 생성
                    for (int i = endlineflag; i < INPlines.Length - 1; i++)
                    {
                        if (session.sline != null)
                        {
                            if (INPlines[i].ToUpper().Contains("["))
                            {
                                session.eline = i;
                                endlineflag = i;
                                break;
                            }

                            session.SectionVal.Add(strclean(INPlines[i]));
                        }

                        if (INPlines[i].ToUpper().Contains("[" + strSetion + "]"))
                        {
                            session.sline = i;
                            session.SectionVal.Add(strclean(INPlines[i]));
                        }
                    }

                    htresult.Add(strSetion, session);
                }

                return htresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }

        private string strclean(string str)
        {
            try
            {
                string[] strarrytemp;

                if(!str.Equals(""))
                {
                    if (str.IndexOf(';') == str.Length - 1)
                        str = str.Remove(str.IndexOf(';')-1);
                }

                if(Regex.Match(str, @"\t").Length > 0)
                {
                    strarrytemp = Regex.Split(str, @"\t");
                    str = string.Empty;
                    for (int i = 0; i < strarrytemp.Length; i++)
                    {
                        strarrytemp[i] = strarrytemp[i].Trim();
                        strarrytemp[i] = strarrytemp[i].Replace(" ", "_");

                        if (i == 0)
                            str = strarrytemp[i];
                        else
                            str = str + " " + strarrytemp[i];
                    }
                }
                else
                {
                    str = str.Trim();
                    strarrytemp = Regex.Split(str, @"\s+");
                    str = string.Empty;

                    for (int i = 0; i < strarrytemp.Length; i++)
                    {
                        strarrytemp[i] = strarrytemp[i].Trim();
                        strarrytemp[i] = strarrytemp[i].Replace(" ", "_");

                        if (i == 0)
                            str = strarrytemp[i];
                        else
                            str = str + " " + strarrytemp[i];
                    }
                }

                return str;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }

        /// <summary>
        /// INP 파일 Section 값 반환 string
        /// </summary>
        /// <param name="strkey"></param>
        /// <returns></returns>
        private string INPSectionSTR(string strkey)
        {
            try
            {
                string strresult = string.Empty;

                if (htSection.ContainsKey(strkey))
                {
                    //.inpSection2char
                    if (htSection[strkey] is INPSession)
                    {
                        List<string> strarry = new List<string>();
                        strarry.Add((htSection[strkey] as INPSession).SectionVal[0]);
                        strarry.Add((htSection[strkey] as INPSession).SectionVal[1]);

                        Regex reg = new Regex(@"\.");
                        MatchCollection mc = reg.Matches(strarry[1]);

                        if (mc.Count == 0)
                            strresult = strarry[1];
                    }
                }

                return strresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        #endregion

        #region INPFile 상세내역 분석
        #region 스트링 return
        //ok
        private string TITLE()
        {
            try
            {
                string strkey = "TITLE";
                return INPSectionSTR(strkey);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private string CONTROLS()
        {
            try
            {
                string strkey = "CONTROLS";
                return INPSectionSTR(strkey);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private string RULES()
        {
            try
            {
                string strkey = "RULES";
                return INPSectionSTR(strkey);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private string REPORT()
        {
            try
            {
                string strkey = "REPORT";
                return INPSectionSTR(strkey);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        #endregion

        #region DataTable return
        //ok
        private DataTable JUNCTIONS()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "ID", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Elev", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Demand", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Pattern", DataType = typeof(string), AllowDBNull = true });
                string strkey = "JUNCTIONS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();


                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j];
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable TANKS()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "ID", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Elevation", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "InitLevel", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "MinLevel", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "MaxLevel", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Diameter", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "MinVol", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "VolCurve", DataType = typeof(object), AllowDBNull = true });
                string strkey = "TANKS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j];
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable RESERVOIRS()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "ID", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Head", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Pattern", DataType = typeof(string), AllowDBNull = true });
                string strkey = "RESERVOIRS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();


                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j];
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable PIPES()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "ID", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Node1", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Node2", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Length", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Diameter", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Roughness", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "MinorLoss", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Status", DataType = typeof(string), AllowDBNull = true });
                string strkey = "PIPES";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();


                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j];
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable PUMPS()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "ID", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Node1", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Node2", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Parameters", DataType = typeof(object), AllowDBNull = true });
                string strkey = "PUMPS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");


                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable VALVES()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "ID", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Node1", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Node2", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Diameter", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Type", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Setting", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "MinorLoss", DataType = typeof(object), AllowDBNull = true });
                string strkey = "VALVES";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable DEMANDS()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Junction", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Demand", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Pattern", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Category", DataType = typeof(object), AllowDBNull = true });
                string strkey = "DEMANDS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable COORDINATES()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Node", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "X-Coord", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Y-Coord", DataType = typeof(object), AllowDBNull = true });
                string strkey = "COORDINATES";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable VERTICES()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Link", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "X-Coord", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Y-Coord", DataType = typeof(object), AllowDBNull = true });
                string strkey = "VERTICES";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();


                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j];
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable LABELS()
        {
            try
            {
                DataTable dtresult = new DataTable();

                dtresult.Columns.Add(new DataColumn() { ColumnName = "X-Coord", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Y-Coord", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Label & Anchor Node", DataType = typeof(object), AllowDBNull = true });
                string strkey = "LABELS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            str = Regex.Replace(str, "\"", "");
                            strarry = str.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                            if (strarry.Length > 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable STATUS()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "ID", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Status/Setting", DataType = typeof(object), AllowDBNull = true });
                string strkey = "STATUS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable EMITTERS()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Junction", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Coefficient", DataType = typeof(object), AllowDBNull = true });
                string strkey = "EMITTERS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable QUALITY()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Node", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "InitQual", DataType = typeof(object), AllowDBNull = true });
                string strkey = "QUALITY";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable SOURCES()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Node", DataType = typeof(string), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Type", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Quality", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Pattern", DataType = typeof(object), AllowDBNull = true });
                string strkey = "SOURCES";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private DataTable MIXING()
        {
            try
            {
                DataTable dtresult = new DataTable();
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Tank", DataType = typeof(object), AllowDBNull = true });
                dtresult.Columns.Add(new DataColumn() { ColumnName = "Model", DataType = typeof(object), AllowDBNull = true });
                string strkey = "MIXING";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1 & strarry.Length <= dtresult.Columns.Count)
                            {
                                DataRow dradd = dtresult.NewRow();

                                for (int j = 0; j < strarry.Length; j++)
                                {
                                    if (strarry.Length > j)
                                        dradd[j] = strarry[j].Replace("_", " ");
                                    else
                                        dradd[j] = "";
                                }

                                dtresult.Rows.Add(dradd.ItemArray);
                            }
                        }
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        #endregion

        #region Hashtable return
        //ok
        private Hashtable PATTERNS()
        {
            try
            {
                Hashtable htresult = new Hashtable();
                List<string> strtemp = new List<string>(); 

                string strkey = "PATTERNS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1)
                            {
                                if(htresult.ContainsKey(strarry[0].ToString()))
                                {
                                    if(htresult[strarry[0].ToString()] is List<string>)
                                    {
                                        strtemp = htresult[strarry[0].ToString()] as List<string>;

                                        for (int j = 0; j < strarry.Length; j++)
                                        {
                                            if (j != 0)
                                                strtemp.Add(strarry[j]);
                                        }

                                        htresult[strarry[0].ToString()] = strtemp;
                                    }
                                }
                                else
                                {
                                    strtemp.Clear();

                                    for (int j = 0; j < strarry.Length; j++)
                                    {
                                        if (j!=0)
                                            strtemp.Add(strarry[j]);
                                    }
                                    
                                    htresult.Add(strarry[0].ToString(), strtemp);
                                }
                            }
                        }
                    }
                }

                return htresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private Hashtable OPTIONS()
        {
            try
            {
                Hashtable htresult = new Hashtable();
                List<string> strtemp = new List<string>();

                string strkey = "OPTIONS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1)
                            {
                                if (!htresult.ContainsKey(strarry[0].Replace("_", " ").ToString()))
                                    htresult.Add(strarry[0].Replace("_", " ").ToString(), strarry[1].Replace("_", " ").ToString());
                            }
                        }
                    }
                }

                return htresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //확인필요
        private Hashtable REACTIONS()
        {
            try
            {
                Hashtable htresult = new Hashtable();
                List<string> strtemp = new List<string>();

                string strkey = "REACTIONS";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1)
                            {
                                if (!htresult.ContainsKey(strarry[0].Replace("_", " ").ToString()))
                                    htresult.Add(strarry[0].Replace("_", " ").ToString(), strarry[1].Replace("_", " ").ToString());
                            }
                        }
                    }
                }

                return htresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private Hashtable TIMES()
        {
            try
            {
                Hashtable htresult = new Hashtable();
                List<string> strtemp = new List<string>();

                string strkey = "TIMES";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1)
                            {
                                if (!htresult.ContainsKey(strarry[0].Replace("_", " ").ToString()))
                                    htresult.Add(strarry[0].Replace("_", " ").ToString(), strarry[1].Replace("_", " ").ToString());
                            }
                        }
                    }
                }

                return htresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        //ok
        private Hashtable ENERGY()
        {
            try
            {
                Hashtable htresult = new Hashtable();
                List<string> strtemp = new List<string>();

                string strkey = "ENERGY";
                string[] strarry;

                if (htSection.ContainsKey(strkey))
                {
                    if (htSection[strkey] is INPSession)
                    {
                        string str = string.Empty;

                        for (int i = 0; i < (htSection[strkey] as INPSession).SectionVal.Count - 1; i++)
                        {
                            str = string.Empty;
                            str = (htSection[strkey] as INPSession).SectionVal[i];
                            strarry = Regex.Split(str, " ");

                            if (strarry.Length != 1)
                            {
                                if (!htresult.ContainsKey(strarry[0].Replace("_", " ").ToString()))
                                    htresult.Add(strarry[0].Replace("_", " ").ToString(), strarry[1].Replace("_", " ").ToString());
                            }
                        }
                    }
                }

                return htresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        #endregion

        private List<object> CURVES()
        {
            try
            {
                return null;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
        
        private void BACKDROP()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        private void TAGS()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        } 
        #endregion
    }

    public class INPFile
    {
        public string Title { get; set; }              //titl 
        public DataTable Junctions { get; set; }       //junc 
        public DataTable Tanks { get; set; }           //tank 
        public DataTable Reservoirs { get; set; }      //resr 
        public DataTable Pipes { get; set; }           //pipe
        public DataTable Pumps { get; set; }           //pump
        public DataTable Valves { get; set; }          //valv
        public DataTable Demands { get; set; }         //dmd  
        public DataTable Status { get; set; }          //stat 
        public DataTable Emitters { get; set; }        //emit    
        public DataTable Quality { get; set; }         //qlty  
        public DataTable Sources { get; set; }         //srcs    
        public Hashtable Reactions { get; set; }       //rxns 
        public DataTable Mixing { get; set; }          //mix
        public Hashtable Patterns { get; set; }        //pats 
        public List<object> Curves { get; set; }       //crvs 
        public string Controls { get; set; }           //ctrl 
        public string Rules { get; set; }              //rul
        public Hashtable Energy { get; set; }          //engy 
        public Hashtable Times { get; set; }           //tims 
        public string Report { get; set; }             //rpt  
        public Hashtable Options { get; set; }         //opts 
        public DataTable Coordinates { get; set; }     //coor 
        public DataTable Vertices { get; set; }        //vert 
        public DataTable Labels { get; set; }          //labs 
        public string Backdrop { get; set; }           //bdrp 
        public string Tags { get; set; }               //tags 
        public Hashtable htINPResultFied { get; set; } //INP RESULT FIED
    }

    public class INPSession
    {
        /// <summary>
        /// 세션 시작 라인 IDX
        /// </summary>
        public int? sline { get; set; } = null;
        /// <summary>
        /// 세션 종료 라인 IDX
        /// </summary>
        public int? eline { get; set; } = null;
        /// <summary>
        /// 세션 라인
        /// </summary>
        public List<string> SectionVal { get; set; } = new List<string>();

        ///// <summary>
        ///// 세션 시작 문자 IDX
        ///// </summary>
        //public int? schar { get; set; }
        ///// <summary>
        ///// 세션 종료 문자 IDX
        ///// </summary>
        //public int? echar { get; set; }
    }
}
