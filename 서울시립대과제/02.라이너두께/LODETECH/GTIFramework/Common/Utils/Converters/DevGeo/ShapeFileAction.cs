﻿using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Map;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Handle;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GTIFramework.Common.Utils.Converters.DevGeo
{
    public class ShapeFileAction : IDisposable
    {
        iniHandle ini = new iniHandle(Environment.CurrentDirectory + "\\GTIConfig.ini");
        string strGISpath = string.Empty;

        public Dictionary<string, Shpvalue> shpData = new Dictionary<string, Shpvalue>();

        public DataTable dtLayerList;
        public Hashtable htLayerKOR = new Hashtable();
        DirectoryInfo Dirif;
        FileInfo[] files;

        MapControl mapcontrol;

        string[] strshpORD = 
            { "WTL_LBLK_AS"
                ,"WTL_MBLK_AS"
                ,"WTL_SBLK_AS"
                ,"WTL_PIPE_LM"
                ,"WTL_SPLY_LS"
                ,"WTL_MANH_PS"
                ,"WTL_STPI_PS"
                ,"WTL_VALV_PS"
                ,"WTL_FLOW_PS"
                ,"WTL_PRGA_PS"
                ,"WTL_FIRE_PS"
                ,"WTL_HEAD_PS"
                ,"WTL_GAIN_PS"
                ,"WTL_PURI_AS"
                ,"WTL_PRES_PS"
                ,"WTL_SERV_PS"
                ,"WTL_META_PS"
                ,"WTL_RSRV_PS" };

        public ShapeFileAction(MapControl _mapcontrol)
        {
            mapcontrol = _mapcontrol;
            strGISpath = ini.GetIniValue("GIS", "strGISpath").ToString();
        }

        /// <summary>
        /// 초기 shp 파일 로드
        /// </summary>
        public void DefaultLoad()
        {
            try
            {
                Dirif = new DirectoryInfo(strGISpath + "/shp/");

                foreach (string strshp in strshpORD)
                {
                    files = null;

                    if(new FileInfo(Dirif + strshp + ".shp").Exists)
                    {
                        files = Dirif.GetFiles(strshp + ".shp");

                        if (files.Length == 1)
                        {
                            ShpLoad(files[0].FullName, files[0].Name);
                            files = null;
                        } 
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 초기 shp 파일 로드
        /// </summary>
        /// <param name="strpath">shp 파일 경로</param>
        public void SelectLoad(string strpath, string[] strshp)
        {
            try
            {
                Dirif = new DirectoryInfo(strGISpath + "/shp/");

                foreach (string sstrshp in strshp)
                {
                    files = null;

                    if (new FileInfo(Dirif + sstrshp + ".shp").Exists)
                    {
                        files = Dirif.GetFiles(sstrshp + ".shp");

                        if (files.Length == 1)
                        {
                            ShpLoad(files[0].FullName, files[0].Name);
                            files = null;
                        } 
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 초기 shp 파일 로드
        /// </summary>
        /// <param name="strpath">shp 파일 경로</param>
        public void SelectLoad(string[] strshp)
        {
            try
            {
                Dirif = new DirectoryInfo(strGISpath + "/shp/");

                foreach (string sstrshp in strshp)
                {
                    files = null;

                    if (new FileInfo(Dirif + sstrshp + ".shp").Exists)
                    {
                        files = Dirif.GetFiles(sstrshp + ".shp");

                        if (files.Length == 1)
                        {
                            ShpLoad(files[0].FullName, files[0].Name);
                            files = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 레이어 추가 및 속성정보 저장
        /// </summary>
        /// <param name="strpath">shp 파일 경로 (FullPath)</param>
        /// <param name="shpname">shp 파일 이름 (xxxx.shp)</param>
        public string ShpLoad(string strpath, string shpname)
        {
            try
            {
                #region 레이어 추가
                VectorLayer vl = FacilVectorLayer(shpname);
                VisibleConverter visibleConverter = new VisibleConverter();

                ShapefileDataAdapter shapefileDataAdapter = new ShapefileDataAdapter();
                shapefileDataAdapter.FileUri = new Uri(strpath);
                shapefileDataAdapter.LoadData();

                MapItemStorage mapItemStorage = new MapItemStorage();
                foreach (var item in shapefileDataAdapter.DisplayItems)
                {
                    if(item is MapPath)
                    {
                        (item as MapPath).Fill = vl.ShapeFill;
                        (item as MapPath).Stroke = vl.ShapeStroke;
                    }
                    else if (item is MapDot)
                    {
                        (item as MapDot).Fill = vl.ShapeFill;
                        (item as MapDot).Stroke = vl.ShapeStroke;
                    }

                    item.Visible = false;
                    mapItemStorage.Items.Add(item);
                }
                vl.Data = mapItemStorage;

                mapcontrol.Layers.Add(vl);

                //속성정보 대분자 변환

                foreach (var item in (vl.Data as MapItemStorage).Items)
                {
                    foreach (var Attributes in item.Attributes)
                    {
                        Attributes.Name = Attributes.Name.ToUpper();
                    }
                }

                #endregion

                #region 속성정보 저장 주석
                //DataTable dttemp = new DataTable();

                //foreach (var Attributes in (vl.Data.DisplayItems as MapVectorItemCollection)[0].Attributes)
                //{
                //    dttemp.Columns.Add(Attributes.Name);
                //}

                //foreach (var item in vl.Data.DisplayItems)
                //{
                //    DataRow dradd = dttemp.NewRow();

                //    foreach (var Attributes in item.Attributes)
                //    {
                //        dradd[Attributes.Name] = Attributes.Value;
                //    }

                //    dttemp.Rows.Add(dradd.ItemArray);
                //}
                shpData.Add(vl.Name, new Shpvalue() { shp = vl, dbf = new DataTable() });
                #endregion

                return vl.Tag.ToString();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return shpname.Replace(".shp", "");
            }
        }

        /// <summary>
        /// 시설레이어 생성
        /// </summary>
        /// <param name="strFacil"></param>
        /// <returns></returns>
        private VectorLayer FacilVectorLayer(string strFacil)
        {
            VectorLayer vl = new VectorLayer();
            ImageBrush imagebrush = new ImageBrush();

            try
            {
                vl.Name = "Layer_" + strFacil.Replace(".shp", "");
                vl.Visibility = Visibility.Collapsed;
                //vl.ViewportChanged += Vl_ViewportChanged;
                //vl.ToolTipPattern = "{FTR_IDN}";

                //선크기
                vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 1 };

                //색상 Layer
                switch (strFacil.Replace(".shp", ""))
                {
                    case "WTL_LBLK_AS"://대블록
                        vl.ShapeStroke = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#9e767e"));
                        vl.ShapeFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#9e767e"));
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 2 };
                        vl.Tag = "대블록";
                        break;            
                    case "WTL_MBLK_AS"://중블록
                        vl.ShapeStroke = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#56a1cb"));
                        vl.ShapeFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#56a1cb"));
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 2 };
                        vl.Tag = "중블록";
                        break;            
                    case "WTL_SBLK_AS"://소블록
                        vl.ShapeStroke = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#fcd126"));
                        vl.ShapeFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#fcd126"));
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 2 };
                        vl.Tag = "소블록";
                        break;            
                    case "WTL_PIPE_LM"://상수관로
                        vl.ShapeStroke = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF0000FF"));
                        vl.ShapeFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF0000FF"));
                        vl.Tag = "상수관로";
                        break;                    
                    case "WTL_SPLY_LS"://급수관로
                        vl.ShapeStroke = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF0033CC"));
                        vl.ShapeFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF0033CC"));
                        vl.Tag = "급수관로";
                        break;
                    case "WTL_MANH_PS"://상수맨홀
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_MANH_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "상수맨홀";
                        break;                    
                    case "WTL_STPI_PS"://스탠드파이프
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_STPI_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "스탠드파이프";
                        break;                    
                    case "WTL_VALV_PS"://변류시설
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_VALV_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "변류시설";
                        break;                   
                    case "WTL_FLOW_PS"://유량계
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_FLOW_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "유량계";
                        break;                    
                    case "WTL_PRGA_PS"://수압계
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_PRGA_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "수압계";
                        break;                    
                    case "WTL_FIRE_PS"://소방시설
                        ImageBrush ib = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_FIRE_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = ib;
                        vl.ShapeFill = ib;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "소방시설";
                        break;                    
                    case "WTL_HEAD_PS"://수원지
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_HEAD_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "수원지";
                        break;                    
                    case "WTL_GAIN_PS"://취수장
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_GAIN_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "취수장";
                        break;                    
                    case "WTL_PURI_AS"://정수장
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_PURI_AS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "정수장";
                        break;                    
                    case "WTL_PRES_PS"://가압장
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_PRES_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "가압장";
                        break;                    
                    case "WTL_SERV_PS"://배수지
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_SERV_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "배수지";
                        break;                    
                    case "WTL_META_PS"://급수전계량기
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_META_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "급수전계량기";
                        break;                    
                    case "WTL_RSRV_PS"://저수조
                        imagebrush = new ImageBrush() { ImageSource = new BitmapImage(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_RSRV_PS.png")), Stretch = Stretch.None };
                        vl.ShapeStroke = imagebrush;
                        vl.ShapeFill = imagebrush;
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 12 };
                        vl.Tag = "저수조";
                        break;                    
                    default://Default
                        vl.ShapeStroke = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF000000"));
                        vl.ShapeFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF000000"));
                        vl.ShapeStrokeStyle = new StrokeStyle() { Thickness = 1 };
                        vl.Tag = strFacil.Replace(".shp", "");
                        break;                                              
                }

                //한글 명칭
                if (htLayerKOR.Contains(strFacil.Replace(".shp", "")))
                {
                    if(htLayerKOR[strFacil.Replace(".shp", "")] is DataRow[])
                    {
                        if ((htLayerKOR[strFacil.Replace(".shp", "")] as DataRow[]).Length > 0)
                            vl.Tag = (htLayerKOR[strFacil.Replace(".shp", "")] as DataRow[])[0]["LAYER_NM"].ToString();
                    }
                }

                //선택 객체 선 스타일
                vl.SelectedShapeStrokeStyle = vl.ShapeStrokeStyle;
                vl.HighlightShapeStrokeStyle = vl.ShapeStrokeStyle;

                //선택 객체 면 색
                vl.SelectedShapeFill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#660000FF"));

                return vl;
            }
            catch (Exception ex)
            {
                return vl;
            }
        }

        private void Vl_ViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            GeoPoint p1 = e.TopLeft;
            GeoPoint p2 = e.BottomRight;

            foreach (var vl in mapcontrol.Layers)
            {
                if(vl is VectorLayer)
                {
                    if((vl as VectorLayer).Visibility == Visibility.Visible)
                    {
                        foreach (var item in ((vl as VectorLayer).Data as MapItemStorage).Items)
                        {
                            if (item is MapDot)
                            {
                                if (p1.GetX() > (item as MapDot).Location.GetX() && (item as MapDot).Location.GetX() > p2.GetX() &&
                                    p2.GetY() > (item as MapDot).Location.GetY() && (item as MapDot).Location.GetY() > p1.GetY())
                                    item.Visible = true;
                                else
                                    item.Visible = false;

                            }
                            if (item is MapPath)
                            {

                            }
                        }
                    }
                }
            }
        }

        public void Dispose()
        {
            try
            {
                foreach (var item in shpData)
                    ((((Shpvalue)item.Value).shp as VectorLayer).Data as MapItemStorage).Items.Clear();

                shpData.Clear();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
            }
        }

        public void ConvertKOR(DataTable dttemp)
        {
            try
            {
                foreach (DataRow dr in dttemp.DefaultView.ToTable(true, "LAYER_CD").Rows)
                {
                    DataRow[] drarry = dttemp.Select("[LAYER_CD] = '" + dr["LAYER_CD"].ToString() + "'");
                    htLayerKOR.Add(dr["LAYER_CD"].ToString(), drarry);
                }
            }
            catch (Exception ex)
            { 

            }
        }
    }

    public struct Shpvalue
    {
        public VectorLayer shp; //shp 파일(형상정보)
        public DataTable dbf; //dbf 파일 (형상속성정보)
    }
}
