﻿using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTIFramework.Common.Utils.Converters.DevGeo
{
    /// <summary>
    /// GeoDBFView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class GeoDBFView : Window
    {
        public GeoDBFView(DataTable dt)
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            gcLayerList.ItemsSource = dt;


            btnXSignClose.Click += BtnXSignClose_Click;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;
        }

        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    this.Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    this.WindowState = WindowState.Normal;
                }
                this.DragMove();
            }
        }

        private void BtnXSignClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
