﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTIFramework.Common.Utils.Converters.DevGeo
{
    /// <summary>
    /// LeakageView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class LeakageView : UserControl
    {
        static object _objOWNER;
        static string _STRBLK_CD;
        static string _STRBLK_NM;
        object objOWNER;
        string STRBLK_CD;
        string STRBLK_NM;

        static double dlbsupp = 0;
        static double dlbuse = 0;
        static double dlbleak = 0;
        static double dlbnmf = 0;

        #region DATA
        public object DATA
        {
            get { return (object)GetValue(DATAProperty); }
            set { SetValue(DATAProperty, value); }
        }

        public static DependencyProperty DATAProperty =
            DependencyProperty.Register(
                "DATA", typeof(object), typeof(LeakageView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(DATAPropertyChanged)));

        public static void DATAPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (LeakageView)obj;

                if (control.DATA is DataRow)
                {
                    if (double.TryParse((control.DATA as DataRow)["SUPP_QTY"].ToString(), out dlbsupp))
                        control.lbsupp.Content = dlbsupp.ToString("N2");
                    else
                        control.lbsupp.Content = (control.DATA as DataRow)["SUPP_QTY"].ToString();

                    if (double.TryParse((control.DATA as DataRow)["USE_QTY"].ToString(), out dlbuse))
                        control.lbuse.Content = dlbuse.ToString("N2");
                    else
                        control.lbuse.Content = (control.DATA as DataRow)["USE_QTY"].ToString();

                    if (double.TryParse((control.DATA as DataRow)["LEAK_QTY"].ToString(), out dlbleak))
                        control.lbleak.Content = dlbleak.ToString("N2");
                    else
                        control.lbleak.Content = (control.DATA as DataRow)["LEAK_QTY"].ToString();

                    if (double.TryParse((control.DATA as DataRow)["NIGHT_MIN_FLOW"].ToString(), out dlbnmf))
                        control.lbnmf.Content = dlbnmf.ToString("N1");
                    else
                        control.lbnmf.Content = (control.DATA as DataRow)["NIGHT_MIN_FLOW"].ToString();
                }
                else if(control.DATA == null)
                {
                    control.lbsupp.Content = "";
                    control.lbuse.Content = "";
                    control.lbleak.Content = "";
                    control.lbnmf.Content = "";

                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        #region OWNER
        public Hashtable OWNER
        {
            get { return (Hashtable)GetValue(OWNERProperty); }
            set { SetValue(OWNERProperty, value); }
        }

        public static DependencyProperty OWNERProperty =
            DependencyProperty.Register(
                "OWNER", typeof(Hashtable), typeof(LeakageView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(OWNERPropertyChanged)));

        public static void OWNERPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (LeakageView)obj;

                if(control.OWNER is Hashtable)
                {
                    control.lbname.Content = (control.OWNER as Hashtable)["BLK_NM"].ToString();

                    _objOWNER = (control.OWNER as Hashtable)["OWNER"] as object;
                    _STRBLK_CD = (control.OWNER as Hashtable)["BLK_CD"].ToString();
                    _STRBLK_NM = (control.OWNER as Hashtable)["BLK_NM"].ToString();

                    control.InfoSave(_objOWNER, _STRBLK_CD, _STRBLK_NM);
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        #region BorderColor
        public string BorderColor
        {
            get { return (string)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static DependencyProperty BorderColorProperty =
            DependencyProperty.Register(
                "BorderColor", typeof(string), typeof(LeakageView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(BorderColorPropertyChanged)));

        public static void BorderColorPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (LeakageView)obj;

                control.BorderTH.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString(control.BorderColor.ToString()));
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        public LeakageView()
        {
            InitializeComponent();
            btnChart.Click += BtnChart_Click;
        }

        private void BtnChart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                object[] objParam = { STRBLK_CD, STRBLK_NM };
                objOWNER.GetType().GetMethod("PopupChart").Invoke(objOWNER, objParam);
            }
            catch (Exception ex)
            {
            }
        }

        private void InfoSave(object OWNER, string BLK_CD, string BLK_NM)
        {
            objOWNER = OWNER;
            STRBLK_CD = BLK_CD;
            STRBLK_NM = BLK_NM;
        }
    }
}
