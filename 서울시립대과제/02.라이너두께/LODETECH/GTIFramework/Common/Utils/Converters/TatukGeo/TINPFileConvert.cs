﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTIFramework.Common.Utils.Converters.TatukGeo
{
    public class TINPFileConvert
    {
        string[] strINPpipe = {"SHP", "ID", "Start Node", "End Node", "Description"
                        , "Tag", "Length", "Diameter", "Roughness", "Loss Coeff."
                        , "InitialStatus", "Bulk Coeff.", "Wall Coeff.", "Flow", "Velocity"
                        , "Unit Headloss", "Friction Factor", "Reaction Rate", "Quality", "Status" };
        string[] strINPpump = {"SHP", "ID", "Start Node", "End Node", "Description"
                        , "Tag", "Pump Curve", "Power", "Speed", "Pattern"
                        , "Initial Status", "Effic. Curve", "Energy Price", "Price Pattern", "Flow"
                        , "Headloss", "Quality", "Status" };
        string[] strINPvalve = {"SHP", "ID", "Start Node", "End Node", "Description", "Tag"
                        , "Diameter", "Type", "Setting", "Loss Coeff.", "Fixed Status"
                        , "Flow", "Velocity", "Headloss", "Quality", "Status"};
        string[] strINPjunction = {"SHP", "ID", "X-Coordinate", "Y-Coordinate", "Description"
                        , "Tag", "Elevation", "Base Demand", "Demand Pattern", "Demand Categories"
                        , "Emitter Coeff.", "Initial Quality", "Source Quality", "Actual Demand", "Total Head"
                        , "Pressure", "Quality"};
        string[] strINPtank = {"SHP", "ID", "X-Coordinate", "Y-Coordinate", "Description"
                        , "Tag", "*Elevation", "Initial Level", "MinimumLevel", "MaximumLevel"
                        , "Diameter", "MinimumVolume", "Volum Curve", "Can Overflow", "Mixing Model"
                        , "Mixing Fraction", "Reaction Coeff.", "Initial Quality", "Source Quality", "Net Inflow"
                        , "Elevation", "Pressure", "Quality"};
        string[] strINPreservoir = {"SHP", "ID", "X-Coordinate", "Y-Coordinate", "Description"
                        , "Tag", "Total Head", "Head Pattern", "Initial Quality", "Source Quality"
                        , "Net Inflow", "Elevation", "Pressure", "Quality"};


    }
}
