﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TatukGIS.NDK;
using TatukGIS.NDK.WPF;

namespace GTIFramework.Common.Utils.Converters.TatukGeo
{
    /// <summary>
    /// TatukMapControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class TatukMapControl : UserControl
    {
        public TGIS_Shape selectshp;
        public TGIS_ViewerWnd mapcontrol;
        double dzoom;

        public TatukMapControl()
        {
            InitializeComponent();
            _mapcontrol.CursorForSelect = Cursors.Arrow;
            _mapcontrol.CursorForDrag = Cursors.Arrow;
            mapcontrol = this._mapcontrol;
        }

        /// <summary>
        /// mousedown & mousemove 했을경우
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed & mapcontrol.Mode == TGIS_ViewerMode.Select)
                    mapcontrol.Mode = TGIS_ViewerMode.Drag;

                TGIS_Point ptg;

                TGIS_Point pt = mapcontrol.ScreenToMap(new System.Drawing.Point(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y));

                ptg = mapcontrol.CS.ToWGS(mapcontrol.ScreenToMap(new System.Drawing.Point(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y)));

                //lbXY1.Content = "X : " + pt.X.ToString() + "(" + TGIS_Utils.GisLongitudeToStr(ptg.X).ToString() + "),  " + pt.Y.ToString() + "(" + TGIS_Utils.GisLatitudeToStr(ptg.Y).ToString() + ")";
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// mousedown 했을경우 select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (mapcontrol.IsEmpty) return;

                selectshp = null;

                foreach (var item in mapcontrol.Items)
                {
                    if (item is TGIS_LayerVector)
                        (item as TGIS_LayerVector).DeselectAll();
                }

                System.Drawing.Point pt = new System.Drawing.Point((int)Math.Truncate(e.GetPosition(mapcontrol).X), (int)Math.Truncate(e.GetPosition(mapcontrol).Y));
                selectshp = (TGIS_Shape)mapcontrol.Locate(mapcontrol.ScreenToMap(pt), 5 / mapcontrol.Zoom);

                if (selectshp == null) return;
                selectshp.IsSelected = !selectshp.IsSelected;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// premousedown 했을경우 drag 모드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if(mapcontrol == null)
                {

                }
                if (mapcontrol.IsEmpty) return;

                mapcontrol.Mode = TGIS_ViewerMode.Drag;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// premouseup 했을경우 select 모드 & Drag로 이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                mapcontrol.Mode = TGIS_ViewerMode.Select;
                mapcontrol.ZoomBy(1, 0, 0);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// 휠 줌 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (mapcontrol.IsEmpty) return;

                //Console.WriteLine(mapcontrol.ZoomEx.ToString());

                Console.WriteLine(e.Delta.ToString());

                if (e.Delta > 120) return;

                if (e.Delta == 120)
                {
                    dzoom = mapcontrol.ZoomEx;

                    if (dzoom > 100) return;

                    mapcontrol.ZoomBy(5 / 4.0, (int)Math.Truncate(e.GetPosition(mapcontrol).X), (int)Math.Truncate(e.GetPosition(mapcontrol).Y));
                }
                else
                {
                    mapcontrol.ZoomBy(4 / 5.0, (int)Math.Truncate(e.GetPosition(mapcontrol).X), (int)Math.Truncate(e.GetPosition(mapcontrol).Y));
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
            finally
            {
                
            }
        }
    }
}
