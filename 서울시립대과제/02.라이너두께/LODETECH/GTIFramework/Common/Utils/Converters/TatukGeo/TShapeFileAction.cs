﻿using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Handle;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TatukGIS.NDK;
using TatukGIS.NDK.WPF;

namespace GTIFramework.Common.Utils.Converters.TatukGeo
{
    public class TShapeFileAction : IDisposable
    {
        FileInfo fileInfo;

        //shp 정의
        string[] strshpORD =
            { "WTL_LBLK_AS"
                ,"WTL_MBLK_AS"
                ,"WTL_SBLK_AS"
                ,"WTL_PIPE_LM"
                ,"WTL_SPLY_LS"
                ,"WTL_MANH_PS"
                ,"WTL_STPI_PS"
                ,"WTL_VALV_PS"
                ,"WTL_FLOW_PS"
                ,"WTL_PRGA_PS"
                ,"WTL_FIRE_PS"
                ,"WTL_HEAD_PS"
                ,"WTL_GAIN_PS"
                ,"WTL_PURI_AS"
                ,"WTL_PRES_PS"
                ,"WTL_SERV_PS"
                ,"WTL_META_PS"
                ,"WTL_RSRV_PS" };

        public Hashtable htLayerKOR = new Hashtable();

        /// <summary>
        /// 초기 shp 전부 로드
        /// </summary>
        /// <param name="mapcontrol">TGIS_ViewerWnd</param>
        public void DefaultLoad(TGIS_ViewerWnd mapcontrol)
        {
            try
            {
                iniHandle ini = new iniHandle(Environment.CurrentDirectory + "\\GTIConfig.ini");
                string url = ini.GetIniValue("GIS", "strGISpath").ToString();
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(url + @"\shp");
                FileInfo fileInfo;

                foreach (string strshp in strshpORD)
                {
                    fileInfo = null;
                    fileInfo = new FileInfo(di + @"\" + strshp + ".shp");

                    if (fileInfo.Exists)
                    {
                        TGIS_Layer GIS_Layer;
                        GIS_Layer = TGIS_Utils.GisCreateLayer(fileInfo.Name.Replace(".shp", ""), fileInfo.FullName) as TGIS_Layer;
                        Layershp(GIS_Layer);
                        mapcontrol.Add(GIS_Layer);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 초기 shp 선택 로드
        /// </summary>
        /// <param name="mapcontrol">TGIS_ViewerWnd</param>
        /// <param name="selectshp">shp 이름 string[]</param>
        public void SelectLoad(TGIS_ViewerWnd mapcontrol, string[] selectshp)
        {
            try
            {
                iniHandle ini = new iniHandle(Environment.CurrentDirectory + "\\GTIConfig.ini");
                string url = ini.GetIniValue("GIS", "strGISpath").ToString();
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(url + @"\shp");

                bool bselect = false;

                foreach (string strshp in strshpORD)
                {
                    bselect = false;

                    foreach (string str in selectshp)
                    {
                        if (strshp.Equals(str))
                            bselect = true;
                    }

                    if (!bselect)
                        continue;

                    if (new FileInfo(di + @"\" + strshp + ".shp").Exists)
                    {
                        FileInfo fileInfo = new FileInfo(di + @"\" + strshp + ".shp");

                        TGIS_Layer GIS_Layer;
                        GIS_Layer = TGIS_Utils.GisCreateLayer(fileInfo.Name.Replace(".shp", ""), fileInfo.FullName) as TGIS_Layer;
                        Layershp(GIS_Layer);
                        mapcontrol.Add(GIS_Layer);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// shp 색상 정의
        /// </summary>
        /// <param name="GIS_Layer"></param>
        public void Layershp(TGIS_Layer GIS_Layer)
        {
            try
            {
                if (GIS_Layer is TGIS_LayerVector)
                {
                    TGIS_LayerVector tGIS_LayerVector = GIS_Layer as TGIS_LayerVector;

                    switch (GIS_Layer.Name)
                    {

                        //case "WTL_LBLK_AS"://대블록
                        //    tGIS_LayerVector.IgnoreShapeParams = false;
                        //    tGIS_LayerVector.DormantMode = TGIS_LayerDormantMode.Agressive;

                        //    foreach (var item in tGIS_LayerVector.Items)
                        //    {
                        //        (item as TGIS_Shape).Params.Line.Color = GISColor(((Color)ColorTranslator.FromHtml("#9e767e")));
                        //        (item as TGIS_Shape).Params.Area.Color = GISColor(((Color)ColorTranslator.FromHtml("#9e767e")));
                        //    }
                            
                        //    tGIS_LayerVector.Caption = "대블록";
                        //    break;
                        //case "WTL_MBLK_AS"://중블록
                        //    tGIS_LayerVector.IgnoreShapeParams = false;
                        //    foreach (var item in tGIS_LayerVector.Items)
                        //    {
                        //        (item as TGIS_Shape).Params.Line.Color = GISColor(((Color)ColorTranslator.FromHtml("#56a1cb")));
                        //        (item as TGIS_Shape).Params.Area.Color = GISColor(((Color)ColorTranslator.FromHtml("#56a1cb")));
                        //    }
                        //    tGIS_LayerVector.Caption = "중블록";
                        //    break;
                        //case "WTL_SBLK_AS"://소블록
                        //    tGIS_LayerVector.IgnoreShapeParams = false;
                        //    foreach (var item in tGIS_LayerVector.Items)
                        //    {
                        //        (item as TGIS_Shape).Params.Line.Color = GISColor(((Color)ColorTranslator.FromHtml("#fcd126")));
                        //        (item as TGIS_Shape).Params.Area.Color = GISColor(((Color)ColorTranslator.FromHtml("#fcd126")));
                        //    }
                        //    tGIS_LayerVector.Caption = "소블록";
                        //    break;

                        case "WTL_LBLK_AS"://대블록
                            tGIS_LayerVector.Params.Line.Color = GISColor(((Color)ColorTranslator.FromHtml("#9e767e")));
                            tGIS_LayerVector.Params.Area.Color = GISColor(((Color)ColorTranslator.FromHtml("#9e767e")));
                            tGIS_LayerVector.IgnoreShapeParams = false;
                            tGIS_LayerVector.Caption = "대블록";
                            break;
                        case "WTL_MBLK_AS"://중블록
                            tGIS_LayerVector.Params.Line.Color = GISColor(((Color)ColorTranslator.FromHtml("#56a1cb")));
                            tGIS_LayerVector.Params.Area.Color = GISColor(((Color)ColorTranslator.FromHtml("#56a1cb")));
                            tGIS_LayerVector.IgnoreShapeParams = false;
                            tGIS_LayerVector.Caption = "중블록";
                            break;
                        case "WTL_SBLK_AS"://소블록
                            tGIS_LayerVector.Params.Line.Color = GISColor(((Color)ColorTranslator.FromHtml("#fcd126")));
                            tGIS_LayerVector.Params.Area.Color = GISColor(((Color)ColorTranslator.FromHtml("#fcd126")));
                            tGIS_LayerVector.IgnoreShapeParams = false;
                            tGIS_LayerVector.Caption = "소블록";
                            break;
                        case "WTL_PIPE_LM"://상수관로
                            tGIS_LayerVector.Params.Line.Color = GISColor(((Color)ColorTranslator.FromHtml("#FF0000FF")));
                            tGIS_LayerVector.Params.Line.Width = -2;
                            tGIS_LayerVector.Caption = "상수관로";
                            break;
                        case "WTL_SPLY_LS"://급수관로
                            tGIS_LayerVector.Params.Line.Color = GISColor(((Color)ColorTranslator.FromHtml("#FF0033CC")));
                            tGIS_LayerVector.Params.Line.Width = -1;
                            tGIS_LayerVector.Caption = "급수관로";
                            break;
                        case "WTL_MANH_PS"://상수맨홀
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_MANH_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "상수맨홀";
                            break;
                        case "WTL_STPI_PS"://스탠드파이프
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_STPI_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "스탠드파이프";
                            break;
                        case "WTL_VALV_PS"://변류시설
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_VALV_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "변류시설";
                            break;
                        case "WTL_FLOW_PS"://유량계
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_FLOW_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "유량계";
                            break;
                        case "WTL_PRGA_PS"://수압계
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_PRGA_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "수압계";
                            break;
                        case "WTL_FIRE_PS"://소방시설
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_FIRE_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "소방시설";
                            break;
                        case "WTL_HEAD_PS"://수원지
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_HEAD_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "수원지";
                            break;
                        case "WTL_GAIN_PS"://취수장
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_GAIN_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "취수장";
                            break;
                        case "WTL_PURI_AS"://정수장
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_PURI_AS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "정수장";
                            break;
                        case "WTL_PRES_PS"://가압장
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_PRES_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "가압장";
                            break;
                        case "WTL_SERV_PS"://배수지
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_SERV_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "배수지";
                            break;
                        case "WTL_META_PS"://급수전계량기
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_META_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "급수전계량기";
                            break;
                        case "WTL_RSRV_PS"://저수조
                            tGIS_LayerVector.Params.Marker.Symbol = new TGIS_SymbolPicture(AppDomain.CurrentDomain.BaseDirectory + "Common/GeoImage/WTL_RSRV_PS.png");
                            tGIS_LayerVector.Params.Marker.Size = -12;
                            tGIS_LayerVector.Caption = "저수조";
                            break;
                        default://Default
                            tGIS_LayerVector.Params.Line.Color = GISColor(((Color)ColorTranslator.FromHtml("#000000")));
                            tGIS_LayerVector.Params.Area.Color = GISColor(((Color)ColorTranslator.FromHtml("#000000")));
                            tGIS_LayerVector.Params.Line.Width = -1;
                            tGIS_LayerVector.Caption = GIS_Layer.Name.Replace(".shp", "");
                            break;
                    }

                    GIS_Layer = tGIS_LayerVector;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// tatuk 색상 리턴
        /// </summary>
        /// <param name="_color"></param>
        /// <returns></returns>
        public TGIS_Color GISColor(Color _color)
        {
            return (TGIS_Color.FromARGB(_color.A, _color.R, _color.G, _color.B));
        }

        /// <summary>
        /// 속성정보 한글화 DB에서 정의
        /// </summary>
        /// <param name="dttemp">DB에서 정의 (필요시 생성)</param>
        public void ConvertKOR(DataTable dttemp)
        {
            try
            {
                foreach (DataRow dr in dttemp.DefaultView.ToTable(true, "LAYER_CD").Rows)
                {
                    DataRow[] drarry = dttemp.Select("[LAYER_CD] = '" + dr["LAYER_CD"].ToString() + "'");
                    htLayerKOR.Add(dr["LAYER_CD"].ToString(), drarry);
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 정리
        /// </summary>
        public void Dispose()
        {
            try
            {
                fileInfo = null;
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
            }
        }
    }
}
