﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using log4net;
using log4net.Config;
using GTIFramework.Common.ConfigClass;
using System.Collections;
using System.Threading;
using System.Windows.Controls;
using GTIFramework.Common.MessageBox;

namespace GTIFramework.Common.Log
{
    public class Logs
    {
        public static Thread WaterRateSavethread;
        public static ProgressBar progressunlimit;

        public static Hashtable htPermission = new Hashtable();
        public static Hashtable htMenuGoParam = new Hashtable();
        public static string strFocusMNU_CD = "";

        public static string strLogin_ID = "";
        public static string strLogin_PW = "";
        public static string strLogin_SITE = "";
        public static string strLogin_IP = "";
        public static string strLogin_NM = "";

        public static ConnectConfig DefaultConfig = new ConnectConfig();
        public static ConnectConfig WNMSConfig = new ConnectConfig();
        public static ConnectConfig ViewConfig = new ConnectConfig();
        public static ConnectConfig WQMSConfig = new ConnectConfig();
        public static ConnectConfig WPMSConfig = new ConnectConfig();
        public static ConnectConfig HMIConfig = new ConnectConfig();

        //실 접속 정보
        public static ConnectConfig useConfig = new ConnectConfig();

        //쓰레드 Watch
        public static Hashtable htthread = new Hashtable();
        
        //쓰레드 MainWatch
        public static Hashtable htMainthread = new Hashtable();

        public static void ErrLogging(Exception e)
        {
            XmlConfigurator.Configure(new FileInfo(Properties.Resources.RES_LOG_CONF));

            if (Properties.Resources.RES_LOG_ERR_YN.ToUpper().Equals("N")) return;

            ILog errLog = LogManager.GetLogger("errLogger");

            errLog.Debug("=Start=========================================================================================================");
            errLog.Debug("");
            errLog.Debug(e);
            errLog.Debug("");
            errLog.Debug("=End===========================================================================================================");
        }

        public static void ErrLogging(string strMessage)
        {
            XmlConfigurator.Configure(new FileInfo(Properties.Resources.RES_LOG_CONF));

            if (Properties.Resources.RES_LOG_ERR_YN.ToUpper().Equals("N")) return;

            ILog errLog = LogManager.GetLogger("errLogger");

            errLog.Debug("=Start=========================================================================================================");
            errLog.Debug("");
            errLog.Debug(strMessage);
            errLog.Debug("");
            errLog.Debug("=End===========================================================================================================");
        }

        public static void DBdefault()
        {
            DefaultConfig.strIP = Properties.Settings.Default.strIP;
            DefaultConfig.strPort = Properties.Settings.Default.strPort;
            DefaultConfig.strSID = Properties.Settings.Default.strSID;
            DefaultConfig.strID = Properties.Settings.Default.strID;
            DefaultConfig.strPWD = Properties.Settings.Default.strPWD;

            configChange(DefaultConfig);
        }

        public static void configChange(ConnectConfig tempconfig)
        {
            useConfig = tempconfig;
        }

        public static void setDBConfig(string strDBCAT)
        {
            Properties.Settings.Default.RES_DB_INS_DEFAULT = strDBCAT;
            Properties.Settings.Default.Save();
        }
    }
}
