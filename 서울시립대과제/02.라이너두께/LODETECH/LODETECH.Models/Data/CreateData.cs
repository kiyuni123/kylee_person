﻿using System.Data;

namespace LODETECH.Models
{
    public class CreateData
    {
        public static DataTable Menucreate()
        {
            DataTable dtMenu = new DataTable();
            dtMenu.Columns.Add("MNU_CD", typeof(string));
            dtMenu.Columns.Add("MNU_UPPER", typeof(string));
            dtMenu.Columns.Add("MNU_IMG", typeof(string));
            dtMenu.Columns.Add("MNU_NM", typeof(string));
            dtMenu.Columns.Add("MNU_PATH", typeof(string));
            dtMenu.Columns.Add("MNU_STEP", typeof(int));
            dtMenu.Columns.Add("MNU_ORD", typeof(int));

            //대메뉴
            dtMenu.Rows.Add("01", null, null, "라이너 두께 설계 프로그램", "", 1, 1);
            dtMenu.Rows.Add("02", null, null, "밀착형 라이닝 운영 프로그램", "", 1, 2);
            dtMenu.Rows.Add("03", null, null, "UV_CIPP 운영 프로그램", "", 1, 3);
            dtMenu.Rows.Add("04", null, null, "사용자 메뉴얼", "", 1, 4);

            //중메뉴
            dtMenu.Rows.Add("0101", "01", null, "설계조건입력부", "UcDesignInputView", 2, 1);
            dtMenu.Rows.Add("0102", "01", null, "라이너특성입력부", "UcLinerInputView", 2, 2);

            //소메뉴
            //dtMenu.Rows.Add("010101", "0101", null, "설계조건입력부", "UcDesignInputView", 3, 1);
            //dtMenu.Rows.Add("010201", "0102", null, "라이너특성입력부", "UcLinerInputView", 3, 2);

            return dtMenu;
        }

        public static DataTable DesignGBN3create()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CD", typeof(string));
            dt.Columns.Add("NM", typeof(string));
            dt.Columns.Add("TOOLTIP", typeof(string));

            dt.Rows.Add("000001", "주철관", "주철관(Cast iron)");
            dt.Rows.Add("000002", "강관", "강관");

            return dt;
        }

        public static DataTable DesignGBN4create()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CD", typeof(string));
            dt.Columns.Add("NM", typeof(string));
            dt.Columns.Add("TOOLTIP", typeof(string));

            dt.Rows.Add("000001", "강성", "강성(Rigid)");
            dt.Rows.Add("000002", "연성", "연성");

            return dt;
        }

        public static DataTable DesignGBN22create()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CD", typeof(string));
            dt.Columns.Add("NM", typeof(string));
            dt.Columns.Add("TOOLTIP", typeof(string));

            dt.Rows.Add("YES", "YES", "YES");
            dt.Rows.Add("NO", "NO", "NO");
            return dt;
        }

        public static DataTable MinThicknessReviewItemcreate()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CD", typeof(string));
            dt.Columns.Add("NM", typeof(string));

            dt.Rows.Add("000001", "AWWA Class 1 검토사항");
            dt.Rows.Add("000002", "AWWA Class 2 검토사항");
            dt.Rows.Add("000003", "AWWA Class 3 검토사항");
            dt.Rows.Add("000004", "AWWA Class 4 검토사항");

            return dt;
        }
    }
}
