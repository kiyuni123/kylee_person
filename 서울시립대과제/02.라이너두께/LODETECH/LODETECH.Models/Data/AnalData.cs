﻿using GTIFramework.Common.MessageBox;
using System;
using System.Linq;

namespace LODETECH.Models
{
    public static class AnalData
    {
        #region design
        public static bool designvaluechk { get; set; } = false;      //값 존재 유무
        public static decimal? design_inputval1 { get; set; }      //기존관 내부 직경
        public static decimal? design_inputval2 { get; set; }      //기존관 외부 직경
        public static string design_inputval3 { get; set; }      //기존관 재료
        public static string design_inputval4 { get; set; }      //기존관 종류(강성 또는 연성)
        public static decimal? design_inputval5 { get; set; }      //구멍 직경(추후 외부 부식)설계 안전계수
        public static decimal? design_inputval6 { get; set; }      //설계 안전계수
        public static decimal? design_inputval7 { get; set; }      //지표면에서 관 상단까지의 깊이
        public static decimal? design_inputval8 { get; set; }      //관 상단에서 측정한 지하수 높이
        public static decimal? design_inputval9 { get; set; }      //내부 작용 압력
        public static decimal? design_inputval10 { get; set; }     //서지 압력
        public static decimal? design_inputval11 { get; set; }     //시험 압력
        public static decimal? design_inputval12 { get; set; }     //제약된 토양 계수
        public static decimal? design_inputval13 { get; set; }     //진공 압력
        public static decimal? design_inputval14 { get; set; }     //과부화된 토양의 단위 중량
        public static decimal? design_inputval15 { get; set; }     //물 단위중량
        public static decimal? design_inputval16 { get; set; }     //향상 계수
        public static decimal? design_inputval17 { get; set; }     //기존관 타원율
        public static decimal? design_inputval18 { get; set; }     //표면 활하중
        public static decimal? design_inputval19 { get; set; }     //Cooper E80 설계 조건(해당되는 경우)
        public static decimal? design_inputval20 { get; set; }     //온도 변화량
        public static decimal? design_inputval21 { get; set; }     //매설 길이
        public static string design_inputval22 { get; set; }     //관 연장 기간 동안 작동이 중지되는가?

        public static decimal? design_analval1 { get; set; }      //기존관 내부 직경
        public static decimal? design_analval2 { get; set; }      //기존관 외부 직경
        public static string design_analval3 { get; set; }      //기존관 재료
        public static string design_analval4 { get; set; }      //기존관 종류(강성 또는 연성)
        public static decimal? design_analval5 { get; set; }      //구멍 직경(추후 외부 부식)설계 안전계수
        public static decimal? design_analval6 { get; set; }      //설계 안전계수
        public static decimal? design_analval7 { get; set; }      //지표면에서 관 상단까지의 깊이
        public static decimal? design_analval8 { get; set; }      //관 상단에서 측정한 지하수 높이
        public static decimal? design_analval9 { get; set; }      //내부 작용 압력
        public static decimal? design_analval10 { get; set; }     //서지 압력
        public static decimal? design_analval11 { get; set; }     //시험 압력
        public static decimal? design_analval12 { get; set; }     //제약된 토양 계수
        public static decimal? design_analval13 { get; set; }     //진공 압력
        public static decimal? design_analval14 { get; set; }     //과부화된 토양의 단위 중량
        public static decimal? design_analval15 { get; set; }     //물 단위중량
        public static decimal? design_analval16 { get; set; }     //향상 계수
        public static decimal? design_analval17 { get; set; }     //기존관 타원율
        public static decimal? design_analval18 { get; set; }     //표면 활하중
        public static decimal? design_analval19 { get; set; }     //Cooper E80 설계 조건(해당되는 경우)
        public static decimal? design_analval20 { get; set; }     //온도 변화량
        public static decimal? design_analval21 { get; set; }     //매설 길이
        public static string design_analval22 { get; set; }     //관 연장 기간 동안 작동이 중지되는가? 
        #endregion

        #region Liner
        public static bool linervaluechk { get; set; } = false;   //값 존재 유무
        public static decimal? liner_inputval1 { get; set; }      //굴곡 탄성률(후프 방향)
        public static decimal? liner_inputval2 { get; set; }      //굽힘 강도(후프 방향)
        public static decimal? liner_inputval3 { get; set; }      //굴곡 탄성률(축 방향)
        public static decimal? liner_inputval4 { get; set; }      //굽힘 강도(축 방향)
        public static decimal? liner_inputval5 { get; set; }      //장기 굴곡 특성 유지
        public static decimal? liner_inputval6 { get; set; }      //장기 굴곡 탄성률(후프 방향)
        public static decimal? liner_inputval7 { get; set; }      //장기 굽힘 강도(후프 방향)
        public static decimal? liner_inputval8 { get; set; }      //장기 굴곡 탄성률(축 방향)
        public static decimal? liner_inputval9 { get; set; }      //장기 굽힘 강도(축 방향)
        public static decimal? liner_inputval10 { get; set; }     //인장 계수(후프 방향)
        public static decimal? liner_inputval11 { get; set; }     //초기 인장 강도(후프 방향)
        public static decimal? liner_inputval12 { get; set; }     //인장 계수(축 방향)
        public static decimal? liner_inputval13 { get; set; }     //초기 인장 강도(축 방향)
        public static decimal? liner_inputval14 { get; set; }     //장기 인장 특성 유지
        public static decimal? liner_inputval15 { get; set; }     //장기 인장 강도(후프 방향)
        public static decimal? liner_inputval16 { get; set; }     //장기 인장 강도(축 방향)
        public static decimal? liner_inputval17 { get; set; }     //압축계수(축 방향)
        public static decimal? liner_inputval18 { get; set; }     //열 팽창/수축 계수
        public static decimal? liner_inputval19 { get; set; }     //라이닝 시스템의 푸아송 비
        public static decimal? liner_inputval20 { get; set; }     //정수 설계 기준 – 응력 기준(psi)
        public static decimal? liner_inputval21 { get; set; }     //정수 설계 기준 – 변형률 기준(in/in)
        public static decimal? liner_inputval22 { get; set; }     //ASTM D1599 당 단기 파열 압력
        public static decimal? liner_inputval23 { get; set; }     //압력 등급 계수(직렬 정렬)
        public static decimal? liner_inputval24 { get; set; }     //압력 등급 계수(굽힘을 통한)
        public static decimal? liner_inputval25 { get; set; }     //기존관 라이너 접착 강도

        public static decimal? liner_analval1 { get; set; }      //굴곡 탄성률(후프 방향)
        public static decimal? liner_analval2 { get; set; }      //굽힘 강도(후프 방향)
        public static decimal? liner_analval3 { get; set; }      //굴곡 탄성률(축 방향)
        public static decimal? liner_analval4 { get; set; }      //굽힘 강도(축 방향)
        public static decimal? liner_analval5 { get; set; }      //장기 굴곡 특성 유지
        public static decimal? liner_analval6 { get; set; }      //장기 굴곡 탄성률(후프 방향)
        public static decimal? liner_analval7 { get; set; }      //장기 굽힘 강도(후프 방향)
        public static decimal? liner_analval8 { get; set; }      //장기 굴곡 탄성률(축 방향)
        public static decimal? liner_analval9 { get; set; }      //장기 굽힘 강도(축 방향)
        public static decimal? liner_analval10 { get; set; }     //인장 계수(후프 방향)
        public static decimal? liner_analval11 { get; set; }     //초기 인장 강도(후프 방향)
        public static decimal? liner_analval12 { get; set; }     //인장 계수(축 방향)
        public static decimal? liner_analval13 { get; set; }     //초기 인장 강도(축 방향)
        public static decimal? liner_analval14 { get; set; }     //장기 인장 특성 유지
        public static decimal? liner_analval15 { get; set; }     //장기 인장 강도(후프 방향)
        public static decimal? liner_analval16 { get; set; }     //장기 인장 강도(축 방향)
        public static decimal? liner_analval17 { get; set; }     //압축계수(축 방향)
        public static decimal? liner_analval18 { get; set; }     //열 팽창/수축 계수
        public static decimal? liner_analval19 { get; set; }     //라이닝 시스템의 푸아송 비
        public static decimal? liner_analval20 { get; set; }     //정수 설계 기준 – 응력 기준(psi)
        public static decimal? liner_analval21 { get; set; }     //정수 설계 기준 – 변형률 기준(in/in)
        public static decimal? liner_analval22 { get; set; }     //ASTM D1599 당 단기 파열 압력
        public static decimal? liner_analval23 { get; set; }     //압력 등급 계수(직렬 정렬)
        public static decimal? liner_analval24 { get; set; }     //압력 등급 계수(굽힘을 통한)
        public static decimal? liner_analval25 { get; set; }     //기존관 라이너 접착 강도
        #endregion

        #region 결과
        public static decimal? class1_1;
        public static decimal? class1_2;
        public static decimal? class1_3;
        public static decimal? class1_4;
        public static decimal? class1_5;
        public static decimal? class1_6;

        public static decimal? class2_1;
        public static decimal? class2_2;
        public static decimal? class2_3;

        public static decimal? class3_1;
        public static decimal? class3_2;
        public static decimal? class3_3;
        public static decimal? class3_4;
        public static decimal? class3_5;
        public static decimal? class3_6;
        public static decimal? class3_7;

        public static decimal? class4_1;
        public static decimal? class4_2;
        public static decimal? class4_3;
        public static decimal? class4_4;
        public static decimal? class4_5;
        public static decimal? class4_6;
        public static decimal? class4_7;
        public static decimal? class4_8;
        public static decimal? class4_9;
        public static decimal? class4_10;
        public static decimal? class4_11;
        public static decimal? class4_12;
        public static decimal? class4_13;
        public static decimal? class4_14;
        public static decimal? class4_15;
        public static decimal? class4_16;
        public static decimal? class4_17;
        public static decimal? class4_18;
        #endregion

        public static void AnalRun()
        {
            try
            {
                #region 설계조건입력부
                if (design_inputval1 == null || design_inputval2 == null || design_inputval4 == null || design_inputval5 == null || design_inputval6 == null 
                    || design_inputval7 == null || design_inputval8 == null || design_inputval9 == null || design_inputval10 == null || design_inputval11 == null 
                    || design_inputval12 == null || design_inputval13 == null || design_inputval14 == null || design_inputval15 == null || design_inputval16 == null 
                    || design_inputval17 == null || design_inputval18 == null || design_inputval20 == null || design_inputval21 == null 
                    
                    || design_analval1 == null || design_analval2 == null || design_analval4 == null || design_analval5 == null || design_analval6 == null 
                    || design_analval7 == null || design_analval8 == null || design_analval9 == null || design_analval10 == null || design_analval11 == null 
                    || design_analval12 == null || design_analval13 == null || design_analval14 == null || design_analval15 == null || design_analval16 == null 
                    || design_analval17 == null || design_analval18 == null || design_analval20 == null || design_analval21 == null)
                    designvaluechk = false;
                else
                    designvaluechk = true;
                #endregion

                #region 라이너특성입력부
                if (liner_inputval1 == null || liner_inputval6 == null || liner_inputval9 == null || liner_inputval10 == null || liner_inputval12 == null 
                    || liner_inputval13 == null || liner_inputval15 == null || liner_inputval18 == null || liner_inputval19 == null || liner_inputval20 == null 
                    || liner_inputval21 == null || liner_inputval22 == null || liner_inputval23 == null || liner_inputval24 == null || liner_inputval25 == null 
                    
                    || liner_analval1 == null || liner_analval6 == null || liner_analval9 == null || liner_analval10 == null || liner_analval12 == null 
                    || liner_analval13 == null || liner_analval15 == null || liner_analval18 == null || liner_analval19 == null || liner_analval20 == null
                    || liner_analval21 == null || liner_analval22 == null || liner_analval23 == null || liner_analval24 == null || liner_analval25 == null)
                    linervaluechk = false;
                else
                    linervaluechk = true;
                #endregion

                if (linervaluechk == false || designvaluechk == false) return;

                #region Class1
                try
                { 
                    //설계구분1 + (설계구분2 - 설계구분1) / 2
                    class1_2 = Convert.ToDecimal(design_analval1) + (Convert.ToDecimal(design_analval2) - Convert.ToDecimal(design_analval1)) / 2m;
                }
                catch (Exception ex) { class1_2 = null; }
                try
                {
                    //라이너구분18 * 라이너구분1 * 설계구분20
                    class1_4 = Convert.ToDecimal(liner_analval18) * Convert.ToDecimal(liner_analval1) * Convert.ToDecimal(design_analval20);
                }
                catch (Exception ex) { class1_4 = null; }
                try
                {
                    //설계구분15 * (설계구분8 + class1-2 / 12) / 144 + 설계구분13
                    class1_1 = (Convert.ToDecimal(design_analval15) * (Convert.ToDecimal(design_analval8) + (class1_2 / 12m)) / 144m) + Convert.ToDecimal(design_analval13);
                }
                catch (Exception ex) { class1_1 = null; }
                try
                {
                    //class1-1 * 설계구분6
                    class1_3 = class1_1 * Convert.ToDecimal(design_analval6);
                }
                catch (Exception ex) { class1_3 = null; }
                try
                {
                    //설계구분15 * (설계구분8 + class1-2 / 12) / 144
                    class1_5 = Convert.ToDecimal(design_analval15) * (Convert.ToDecimal(design_analval8) + (class1_2 / 12m)) / 144m;
                }
                catch (Exception ex) { class1_5 = null; }
                #endregion

                #region Class2
                try
                {
                    //설계구분5 / 설계구분1
                    class2_2 = Convert.ToDecimal(design_analval5) / Convert.ToDecimal(design_analval1);
                }
                catch (Exception ex) { class2_2 = null; }
                try
                {
                    //설계구분1 / (((설계구분1 / 설계구분5) ^ (2) * (5.33 * 라이너구분9 / (설계구분9 * 설계구분6))) ^ (1 / 2) + 1)
                    class2_1 = Convert.ToDecimal(design_analval1) / Convert.ToDecimal(Math.Pow(Math.Pow((Convert.ToDouble(design_analval1) / Convert.ToDouble(design_analval5)), 2) * (5.33 * Convert.ToDouble(liner_analval9) / (Convert.ToDouble(design_analval9) * Convert.ToDouble(design_analval6))), 1 / 2d) + 1);
                }
                catch (Exception ex) { class2_1 = null; }
                try
                {
                    //1.83 * (class2-1 / 설계구분1) ^ (1/2)
                    class2_3 = 1.83m * Convert.ToDecimal(Math.Pow(Convert.ToDouble(class2_1) / Convert.ToDouble(design_analval1), 1 / 2d));
                }
                catch (Exception ex) { class2_3 = null; }
                #endregion

                #region Class3
                try
                {
                    //설계구분18
                    class3_1 = Convert.ToDecimal(design_analval18);
                }
                catch (Exception ex) { class3_1 = null; }
                try
                {
                    //((1 - 설계구분17 / 100) / (1 + 설계구분17 / 100) ^ 2) ^ 3
                    class3_2 = Convert.ToDecimal(Math.Pow((1 - Convert.ToDouble(design_analval17) / 100) / Math.Pow((1 + Convert.ToDouble(design_analval17) / 100), 2d), 3d));
                }
                catch (Exception ex) { class3_2 = null; }
                try
                {
                    //설계구분1 / (((2 * 설계구분16 * 라이너구분1 * class3 - 2) / ((1 - 라이너구분19 ^ 2) * 설계구분6 * class1-1)) ^ (1/3) + 1)
                    class3_3 = Convert.ToDecimal(design_analval1) / Convert.ToDecimal((Math.Pow((2 * Convert.ToDouble(design_analval16) * Convert.ToDouble(liner_analval1) * Convert.ToDouble(class3_2)) / ((1 - Math.Pow(Convert.ToDouble(liner_analval19), 2d)) * Convert.ToDouble(design_analval6) * Convert.ToDouble(class1_1)), 1 / 3d) + 1));
                }
                catch (Exception ex) { class3_3 = null; }
                try
                {
                    //설계구분1 / (((2 * 설계구분16 * 라이너구분1 * class3 - 2) / ((1 - 라이너구분19 ^ 2) * 설계구분6 * (class1-1 + class3-3 / 2))) ^ (1/3) + 1)
                    class3_4 = Convert.ToDecimal(design_analval1) / Convert.ToDecimal((Math.Pow((2 * Convert.ToDouble(design_analval16) * Convert.ToDouble(liner_analval1) * Convert.ToDouble(class3_2)) / ((1 - Math.Pow(Convert.ToDouble(liner_analval19), 2d)) * Convert.ToDouble(design_analval6) * (Convert.ToDouble(class1_1) + (Convert.ToDouble(class3_1) / 2))), 1 / 3d) + 1));
                }
                catch (Exception ex) { class3_4 = null; }
                try
                {
                    //설계구분1 / (((2 * 설계구분16 * 라이너구분6 * class3 - 2) / ((1 - 라이너구분19 ^ 2) * 설계구분6 * class1-1)) ^ (1/3) + 1)
                    class3_5 = Convert.ToDecimal(design_analval1) / Convert.ToDecimal((Math.Pow((2 * Convert.ToDouble(design_analval16) * Convert.ToDouble(liner_analval6) * Convert.ToDouble(class3_2)) / ((1 - Math.Pow(Convert.ToDouble(liner_analval19), 2d)) * Convert.ToDouble(design_analval6) * Convert.ToDouble(class1_1)), 1 / 3d) + 1));
                }
                catch (Exception ex) { class3_5 = null; }
                try
                {
                    //12 * 라이너구분18 * 설계구분21 * 설계구분20
                    class3_6 = 12m * Convert.ToDecimal(liner_analval18) * Convert.ToDecimal(design_analval21) * Convert.ToDecimal(design_analval20);
                }
                catch (Exception ex) { class3_6 = null; }
                try
                {
                    //라이너구분18 * 라이너구분12 * 설계구분20
                    class3_7 = Convert.ToDecimal(liner_analval18) * Convert.ToDecimal(liner_analval12) * Convert.ToDecimal(design_analval20);
                }
                catch (Exception ex) { class3_7 = null; }
                #endregion

                #region Class4
                try
                {
                    //(설계구분1) / (((2 * 라이너구분15 / (설계구분9 * 설계구분6)) + 1))
                    class4_1 = Convert.ToDecimal(design_analval1) / (((2 * Convert.ToDecimal(liner_analval15) / (Convert.ToDecimal(design_analval9) * Convert.ToDecimal(design_analval6))) + 1));
                }
                catch (Exception ex) { class4_1 = null; }
                try
                {
                    //(설계구분1) / (((2 * 라이너구분20 / (설계구분9 * 설계구분6) + 1)))
                    class4_2 = Convert.ToDecimal(design_analval1) / (((2 * Convert.ToDecimal(liner_analval20) / (Convert.ToDecimal(design_analval9) * Convert.ToDecimal(design_analval6)) + 1)));
                }
                catch (Exception ex) { class4_2 = null; }
                try
                {
                    //(설계구분1) / ((2 * 라이너구분21 * 라이너구분10 / (설계구분9 * 설계구분6)) + 1)
                    class4_3 = Convert.ToDecimal(design_analval1) / ((2 * Convert.ToDecimal(liner_analval21) * Convert.ToDecimal(liner_analval10) / (Convert.ToDecimal(design_analval9) * Convert.ToDecimal(design_analval6))) + 1);
                }
                catch (Exception ex) { class4_3 = null; }
                try
                {
                    //(설계구분1) / ((2.8 * 라이너구분15 / ((설계구분9 + 설계구분10) * 설계구분6)) + 1)
                    class4_4 = Convert.ToDecimal(design_analval1) / ((2.8m * Convert.ToDecimal(liner_analval15) / ((Convert.ToDecimal(design_analval9) + Convert.ToDecimal(design_analval10)) * Convert.ToDecimal(design_analval6))) + 1);
                }
                catch (Exception ex) { class4_4 = null; }
                try
                {
                    //(설계구분1) / ((2.8 * 라이너구분20 / ((설계구분9 + 설계구분10) * 설계구분6))) + 1
                    class4_5 = Convert.ToDecimal(design_analval1) / ((2.8m * Convert.ToDecimal(liner_analval20) / ((Convert.ToDecimal(design_analval9) + Convert.ToDecimal(design_analval10)) * Convert.ToDecimal(design_analval6)))) + 1;
                }
                catch (Exception ex) { class4_5 = null; }
                try
                {
                    //(설계구분1) / ((2.8 * 라이너구분21 * 라이너구분10 / ((설계구분9 + 설계구분10) * 설계구분6)) + 1)
                    class4_6 = Convert.ToDecimal(design_analval1) / ((2.8m * Convert.ToDecimal(liner_analval21) * Convert.ToDecimal(liner_analval10) / ((Convert.ToDecimal(design_analval9) + Convert.ToDecimal(design_analval10)) * Convert.ToDecimal(design_analval6))) + 1);
                }
                catch (Exception ex) { class4_6 = null; }
                try
                {
                    //1 - 0.33 * (설계구분8 / 설계구분7)
                    class4_8 = 1m - 0.33m * (Convert.ToDecimal(design_analval8) / Convert.ToDecimal(design_analval7));
                }
                catch (Exception ex) { class4_8 = null; }
                try
                {
                    //0.433 * 설계구분8 + (설계구분14 * 설계구분7 * class4 - 8 / 144) + 설계구분18
                    class4_7 = 0.433m * Convert.ToDecimal(design_analval8) + (Convert.ToDecimal(design_analval14) * Convert.ToDecimal(design_analval7) * class4_8 / 144m) + Convert.ToDecimal(design_analval18);
                }
                catch (Exception ex) { class4_7 = null; }
                try
                {
                    //1 / (1 + 4 * EXP(-0.065 * 설계구분7))
                    class4_9 = 1m / Convert.ToDecimal((1d + 4d * Math.Exp(-0.065 * Convert.ToDouble(design_analval7))));
                }
                catch (Exception ex) { class4_9 = null; }
                try
                {
                    //((((class4 - 7 * 설계구분6) ^ (2)) * ((설계구분1) ^ (3)) * 12) / (32 * class4 - 8 * class4 - 9 * 설계구분12 * 라이너구분6 * class3 - 2)) ^ (1 / 3)
                    class4_10 = Convert.ToDecimal(Math.Pow((((Math.Pow((Convert.ToDouble(class4_7) * Convert.ToDouble(design_analval6)), 2d)) * (Math.Pow((Convert.ToDouble(design_analval1)), 3d)) * 12d) / (32d * Convert.ToDouble(class4_8) * Convert.ToDouble(class4_9) * Convert.ToDouble(design_analval12) * Convert.ToDouble(liner_analval6) * Convert.ToDouble(class3_2))), 1 / 3d));
                }
                catch (Exception ex) { class4_10 = null; }
                try
                {
                    //라이너구분22 / 라이너구분23
                    class4_11 = Convert.ToDecimal(liner_analval22) / Convert.ToDecimal(liner_analval23);
                }
                catch (Exception ex) { class4_11 = null; }
                try
                {
                    //라이너구분22 / 라이너구분24
                    class4_12 = Convert.ToDecimal(liner_analval22) / Convert.ToDecimal(liner_analval24);
                }
                catch (Exception ex) { class4_12 = null; }
                try
                {
                    //(설계구분9 + 설계구분10) / 1.4
                    class4_13 = (Convert.ToDecimal(design_analval9) + Convert.ToDecimal(design_analval10)) / 1.4m;
                }
                catch (Exception ex) { class4_13 = null; }
                try
                {
                    //(설계구분9 + 설계구분10) / 1.4
                    class4_14 = (Convert.ToDecimal(design_analval9) + Convert.ToDecimal(design_analval10)) / 1.4m;
                }
                catch (Exception ex) { class4_14 = null; }
                try
                {
                    //MAX(설계구분9, (설계구분9 + 설계구분10), 설계구분11)
                    decimal?[] dcmlist4_15 = { Convert.ToDecimal(design_analval9), (Convert.ToDecimal(design_analval9) + Convert.ToDecimal(design_analval10)), Convert.ToDecimal(design_analval11) };
                    class4_15 = dcmlist4_15.Max();
                }
                catch (Exception ex) { class4_15 = null; }

                try
                {
                    //설계구분1 / MAX(class4 - 4, class4 - 1, class3 - 3, class2 - 1)
                    decimal?[] dcmlist4_17 = { class4_4, class4_1, class3_3, class2_1 };
                    class4_17 = Convert.ToDecimal(design_analval1) / dcmlist4_17.Max();
                }
                catch (Exception ex) { class4_17 = null; }
                try
                {
                    //(class4-15 * (class4-17 - 1)) / 2
                    class4_16 = (class4_15 * (class4_17 - 1)) / 2;
                }
                catch (Exception ex) { class4_16 = null; }
                try
                {
                    //class4 - 16 * 라이너구분19 * 3.141595 * ((설계구분1) ^ (2)) * ((1 / class4 - 17) - (1 / ((class4 - 17) ^ (2))))
                    class4_18 = class4_16 * Convert.ToDecimal(liner_analval19) * 3.141595m * Convert.ToDecimal(Math.Pow(Convert.ToDouble(design_analval1), 2d) * ((1 / Convert.ToDouble(class4_17)) - (1d / Math.Pow((Convert.ToDouble(class4_17)), 2d))));
                }
                catch (Exception ex) { class4_18 = null; }
                #endregion

                if (class1_2 != null) class1_2 = Math.Round(Convert.ToDecimal(class1_2), 2, MidpointRounding.AwayFromZero); else class1_2 = null;
                if (class1_4 != null) class1_4 = Math.Round(Convert.ToDecimal(class1_4), 2, MidpointRounding.AwayFromZero); else class1_4 = null;
                if (class1_1 != null) class1_1 = Math.Round(Convert.ToDecimal(class1_1), 2, MidpointRounding.AwayFromZero); else class1_1 = null;
                if (class1_3 != null) class1_3 = Math.Round(Convert.ToDecimal(class1_3), 2, MidpointRounding.AwayFromZero); else class1_3 = null;
                if (class1_5 != null) class1_5 = Math.Round(Convert.ToDecimal(class1_5), 2, MidpointRounding.AwayFromZero); else class1_5 = null;

                if (class2_1 != null) class2_1 = Math.Round(Convert.ToDecimal(class2_1), 2, MidpointRounding.AwayFromZero); else class2_1 = null;
                if (class2_2 != null) class2_2 = Math.Round(Convert.ToDecimal(class2_2), 2, MidpointRounding.AwayFromZero); else class2_2 = null;
                if (class2_3 != null) class2_3 = Math.Round(Convert.ToDecimal(class2_3), 2, MidpointRounding.AwayFromZero); else class2_3 = null;

                if (class3_1 != null) class3_1 = Math.Round(Convert.ToDecimal(class3_1), 2, MidpointRounding.AwayFromZero); else class3_1 = null;
                if (class3_2 != null) class3_2 = Math.Round(Convert.ToDecimal(class3_2), 2, MidpointRounding.AwayFromZero); else class3_2 = null;
                if (class3_3 != null) class3_3 = Math.Round(Convert.ToDecimal(class3_3), 2, MidpointRounding.AwayFromZero); else class3_3 = null;
                if (class3_4 != null) class3_4 = Math.Round(Convert.ToDecimal(class3_4), 2, MidpointRounding.AwayFromZero); else class3_4 = null;
                if (class3_5 != null) class3_5 = Math.Round(Convert.ToDecimal(class3_5), 2, MidpointRounding.AwayFromZero); else class3_5 = null;
                if (class3_6 != null) class3_6 = Math.Round(Convert.ToDecimal(class3_6), 2, MidpointRounding.AwayFromZero); else class3_6 = null;
                if (class3_7 != null) class3_7 = Math.Round(Convert.ToDecimal(class3_7), 2, MidpointRounding.AwayFromZero); else class3_7 = null;

                if (class4_1 != null) class4_1 = Math.Round(Convert.ToDecimal(class4_1), 2, MidpointRounding.AwayFromZero); else class4_1 = null;
                if (class4_2 != null) class4_2 = Math.Round(Convert.ToDecimal(class4_2), 2, MidpointRounding.AwayFromZero); else class4_2 = null;
                if (class4_3 != null) class4_3 = Math.Round(Convert.ToDecimal(class4_3), 2, MidpointRounding.AwayFromZero); else class4_3 = null;
                if (class4_4 != null) class4_4 = Math.Round(Convert.ToDecimal(class4_4), 2, MidpointRounding.AwayFromZero); else class4_4 = null;
                if (class4_5 != null) class4_5 = Math.Round(Convert.ToDecimal(class4_5), 2, MidpointRounding.AwayFromZero); else class4_5 = null;
                if (class4_6 != null) class4_6 = Math.Round(Convert.ToDecimal(class4_6), 2, MidpointRounding.AwayFromZero); else class4_6 = null;
                if (class4_7 != null) class4_7 = Math.Round(Convert.ToDecimal(class4_7), 2, MidpointRounding.AwayFromZero); else class4_7 = null;
                if (class4_8 != null) class4_8 = Math.Round(Convert.ToDecimal(class4_8), 2, MidpointRounding.AwayFromZero); else class4_8 = null;
                if (class4_9 != null) class4_9 = Math.Round(Convert.ToDecimal(class4_9), 2, MidpointRounding.AwayFromZero); else class4_9 = null;
                if (class4_10 != null) class4_10 = Math.Round(Convert.ToDecimal(class4_10), 2, MidpointRounding.AwayFromZero); else class4_10 = null;
                if (class4_11 != null) class4_11 = Math.Round(Convert.ToDecimal(class4_11), 2, MidpointRounding.AwayFromZero); else class4_11 = null;
                if (class4_12 != null) class4_12 = Math.Round(Convert.ToDecimal(class4_12), 2, MidpointRounding.AwayFromZero); else class4_12 = null;
                if (class4_13 != null) class4_13 = Math.Round(Convert.ToDecimal(class4_13), 2, MidpointRounding.AwayFromZero); else class4_13 = null;
                if (class4_14 != null) class4_14 = Math.Round(Convert.ToDecimal(class4_14), 2, MidpointRounding.AwayFromZero); else class4_14 = null;
                if (class4_15 != null) class4_15 = Math.Round(Convert.ToDecimal(class4_15), 2, MidpointRounding.AwayFromZero); else class4_15 = null;
                if (class4_16 != null) class4_16 = Math.Round(Convert.ToDecimal(class4_16), 2, MidpointRounding.AwayFromZero); else class4_16 = null;
                if (class4_17 != null) class4_17 = Math.Round(Convert.ToDecimal(class4_17), 2, MidpointRounding.AwayFromZero); else class4_17 = null;
                if (class4_18 != null) class4_18 = Math.Round(Convert.ToDecimal(class4_18), 2, MidpointRounding.AwayFromZero); else class4_18 = null;
            }
            catch (Exception ex)
            {
                class1_2 = null;
                class1_4 = null;
                class1_1 = null;
                class1_3 = null;
                class1_5 = null;

                class2_1 = null;
                class2_2 = null;
                class2_3 = null;

                class3_1 = null;
                class3_2 = null;
                class3_3 = null;
                class3_4 = null;
                class3_5 = null;
                class3_6 = null;
                class3_7 = null;

                class4_1 = null;
                class4_2 = null;
                class4_3 = null;
                class4_4 = null;
                class4_5 = null;
                class4_6 = null;
                class4_7 = null;
                class4_8 = null;
                class4_9 = null;
                class4_10 = null;
                class4_11 = null;
                class4_12 = null;
                class4_13 = null;
                class4_14 = null;
                class4_15 = null;
                class4_16 = null;
                class4_17 = null;
                class4_18 = null;
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
