﻿using GTIFramework.Common.MessageBox;
using NewTechnologyBackEND.Work;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Threading;

namespace DumyDataInsert
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        Timer timer = new Timer() { Interval = 1000 };
        DateTime nowdt;
        DataRow selectdr = null;
        DataTable dtDatatemp = null;
        Hashtable conditions = new Hashtable();
        MainWork work = new MainWork();

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            timer.Elapsed += Timer_Elapsed;
            btnStart.Click += BtnStart_Click;
            btnStop.Click += BtnStop_Click;
        }

        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lbSTAT.Content = "멈춤";
                timer.Stop();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lbSTAT.Content = "동작";
                timer.Start();
                Timer_Elapsed(null, null);
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            nowdt = DateTime.Now;

            if (nowdt.Second == 0)
            {
                if (nowdt.Minute % 10 == 5 || nowdt.Minute % 10 == 0)
                //if(true)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle,
                        new Action((delegate ()
                        {
                            nowdt = DateTime.Now;

                            dtDatatemp = ((DataView)datagrid.ItemsSource).Table;

                            //START
                            if (dtDatatemp.Rows[0]["CHK"].ToString().Equals(""))
                            {
                                selectdr = dtDatatemp.Rows[0];
                                selectdr["CHK"] = nowdt.ToString("yyyy-MM-dd HH:mm:ss");
                            }
                            //MID
                            else
                            {
                                selectdr = dtDatatemp.Rows[dtDatatemp.Rows.IndexOf(selectdr) + 1];
                                selectdr["CHK"] = nowdt.ToString("yyyy-MM-dd HH:mm:ss");

                                //END
                                if (dtDatatemp.Rows.IndexOf(selectdr).Equals(dtDatatemp.Rows.Count - 1))
                                {
                                    timer.Stop();
                                }
                            }

                            datagrid.SelectedIndex = dtDatatemp.Rows.IndexOf(selectdr);

                            conditions.Clear();
                            conditions.Add("DT", nowdt.ToString("yyyyMMddHHmm") + "00");
                            conditions.Add("INST_CODE", FLSMTAG.Text);
                            conditions.Add("VAL", selectdr["FLSM"].ToString());
                            work.Insert_HMI_DATA(conditions);

                            conditions["INST_CODE"] = PRSTAG.Text;
                            conditions["VAL"] = selectdr["PRS"].ToString();
                            work.Insert_HMI_DATA(conditions);
                        })));
                }
            }
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            FLSMTAG.Text = "테스트_유량계1";
            PRSTAG.Text = "테스트_수압계1";

            csvexpimp();
        }

        private void csvexpimp()
        {
            try
            {
                FileInfo fi = new FileInfo("data.csv");

                if (fi.Exists)
                {
                    DataTable temp = new DataTable();

                    StreamReader sr = new StreamReader("data.csv", Encoding.UTF8);
                    string[] headers = sr.ReadLine().Split(',');

                    foreach (string header in headers)
                    {
                        temp.Columns.Add(header);
                    }

                    while (!sr.EndOfStream)
                    {
                        string[] rows = sr.ReadLine().Split(',');
                        DataRow dr = temp.NewRow();
                        for (int i = 0; i < headers.Length; i++)
                        {
                            dr[i] = rows[i];
                        }
                        temp.Rows.Add(dr);
                    }

                    datagrid.ItemsSource = temp.DefaultView;
                }
                else
                {
                    DataTable temp = new DataTable();
                    temp.Columns.Add("FLSM");
                    temp.Columns.Add("PRS");
                    temp.Columns.Add("CHK");

                    csvexp(temp);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        //데이터 저장용 CSV파일 생성
        private void csvexp(DataTable temp)
        {
            try
            {
                //파일 저장을 위해 스트림 생성.
                FileStream fs = new FileStream("data.csv", FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);

                //컬럼 이름들을 ","로 나누고 저장.
                string line = string.Join(",", temp.Columns.Cast<object>());
                sw.WriteLine(line);

                //row들을 ","로 나누고 저장.
                foreach (DataRow item in temp.Rows)
                {
                    line = string.Join(",", item.ItemArray.Cast<object>());
                    sw.WriteLine(line);
                }

                sw.Close();
                fs.Close();

                sw.Dispose();
                fs.Dispose();
            }
            catch (Exception ex){ throw ex; }
        }
    }
}
