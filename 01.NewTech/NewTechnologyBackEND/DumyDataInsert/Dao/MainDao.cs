﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewTechnologyBackEND.Dao
{
    class MainDao
    {
        /// <summary>
        /// HMI_DATA INSERT
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_HMI_DATA(Hashtable conditions)
        {
            DBManager.QueryForInsert("Insert_HMI_DATA", conditions);
        }
    }
}
