﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SerialComPG
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow:Window
    {
        SerialPort sPort = new SerialPort("COM1");
        int linenum = 1;
        string rcvmsg = "";
        string strpacket = "";
        int intpacketsize = 0;
        bool breset = false;  //리셋 여부
        bool eventgo = true;  //이벤트 동작 타이밍 플래그
        bool bend = true;  //이벤트 동작 타이밍 플래그
        DateTime nowdt = new DateTime();
        DataRow selectdr = null;

        Thread thread;
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            csvexpimp();
            timer.Tick += Timer_Tick;
            timer.Disposed += Timer_Disposed;
            txtconsol.KeyDown += Txtconsol_KeyDown;
            btnSave.Click += BtnSave_Click;
            btnStart.Click += BtnStart_Click;
            SerialConnSetting();

            #region 포트 열려있을 경우 RCU890 cmd 전송
            if(!sPort.IsOpen)
            {
                sPort.Open();
                if(sPort.IsOpen)
                {
                    ConsolWrite("COM1 Port 연결 성공");

                    //연결 안정을 위해 1초 딜레이
                    System.Threading.Thread.Sleep(500);

                    //데이터 모드로 변경 
                    sPort.Write("AT+CRM=150\r\n");

                    //연결 안정을 위해 1초 딜레이
                    System.Threading.Thread.Sleep(500);

                    sPort.Write("ATE0\r\n");

                    breset = true;
                }
                else
                {
                    ConsolWrite("COM1 Port 연결 실패");
                }
            }
            #endregion
        }

        #region 타이머 이벤트
        private void Timer_Disposed(object sender, EventArgs e)
        {
            eventgo = true;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            eventgo = true;
        }
        #endregion

        #region Thread 관련 
        public void work()
        {
            DataView dvDatatemp = null;

            while(bend)
            {
                try
                {
                    if(eventgo)
                    {
                        if(breset)
                        {
                            Dispatcher.Invoke(DispatcherPriority.Normal,
                                new Action((delegate ()
                                {
                                    nowdt = DateTime.Now;
                                    dvDatatemp = null;
                                    dvDatatemp = (DataView)datagrid.ItemsSource;

                                    if(dvDatatemp.Table.Rows[dvDatatemp.Table.Rows.Count - 1]["CHK"].ToString().Equals("O"))
                                    {
                                        bend = false;
                                        MessageBox.Show("처리할 데이터가 없습니다.");
                                    }
                                    else if(!dvDatatemp.ToTable().Rows[0]["CHK"].Equals(""))
                                    {
                                        for(int i = dvDatatemp.Table.Rows.Count - 1; i >= 0; i--)
                                        {
                                            if(!dvDatatemp.Table.Rows[i]["CHK"].ToString().Equals(""))
                                            {
                                                selectdr = dvDatatemp.ToTable().Rows[dvDatatemp.Table.Rows.IndexOf(dvDatatemp.Table.Rows[i]) + 1];
                                                break;
                                            }
                                        }

                                        strpacket = MakePacket(selectdr);

                                        Thread.Sleep(200);

                                        //dvDatatemp.Table.Rows[selectdr.Table.Rows.IndexOf(selectdr)]["CHK"] = nowdt.ToString("yyyy-MM-dd HH:mm");

                                        sPort.Write("AT*IB*NETOPEN\r\n");

                                        //csvexp(dvDatatemp.Table);
                                    }
                                    else
                                    {
                                        selectdr = dvDatatemp.ToTable().Rows[0];

                                        strpacket = MakePacket(selectdr);

                                        Thread.Sleep(200);

                                        //dvDatatemp.Table.Rows[selectdr.Table.Rows.IndexOf(selectdr)]["CHK"] = nowdt.ToString("yyyy-MM-dd HH:mm");

                                        sPort.Write("AT*IB*NETOPEN\r\n");

                                        //csvexp(dvDatatemp.Table);
                                    }
                                })));
                        }
                        //리셋했을경우 리셋완료 확인
                        else
                        {
                            sPort.Write("AT\r\n");
                        }

                        eventgo = false;
                    }
                }
                catch(Exception ex)
                {
                    if(ex.ToString().IndexOf("스레드가 중단되었습니다.") != -1)
                    {
                        ConsolWrite("스레드 동작 중지");
                    }
                    else
                    {
                        ConsolWrite("work catch in");
                    }
                }
            }

            timer.Stop();
            timer.Dispose();
            ConsolWrite("시작버튼을 한번 더누르면 스레드가 초기화 됩니다.");
            thread.Abort();
        }
        #endregion

        #region RCU890 제어
        //COMMAND RUC890 제어
        private void Txtconsol_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.Key.Equals(Key.Enter))
                {
                    e.Handled = true;
                    sPort.Write(txtconsol.Text + "\r\n");
                    ConsolWrite("입력 " + DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " : " + txtconsol.Text);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //RCU890 연결설정
        private void SerialConnSetting()
        {
            sPort.PortName = "COM1";
            sPort.BaudRate = 115200;
            sPort.DataBits = 8;
            sPort.StopBits = StopBits.One;
            sPort.Parity = Parity.None;
            sPort.Handshake = Handshake.None;
            sPort.Encoding = Encoding.Default;
            sPort.RtsEnable = true;
            sPort.DataReceived += SPort_DataReceived;
        }

        //수신받은 결과
        private void SPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //통신 안정을 위해 0.1초 딜레이
            System.Threading.Thread.Sleep(100);
            rcvmsg = sPort.ReadExisting().Replace("\n", " ").Replace("\r", " ");

            ConsolWrite("리턴 " + DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " : " + rcvmsg);

            int cmdcnt = 0;

            if(rcvmsg.IndexOf("  *IB*TCPREAD:") != -1)
            {
                rcvmsg = "서버리턴";
                ((DataView)datagrid.ItemsSource).Table.Rows[selectdr.Table.Rows.IndexOf(selectdr)]["CHK"] = nowdt.ToString("yyyy-MM-dd HH:mm");
                selectdr = null;
            }

            if(rcvmsg.IndexOf("ERROR") != -1)
            {
                rcvmsg = "RESET";
            }

            //리턴값을 ATE0으로 설정하여 리턴 전문에서 분기
            switch(rcvmsg)
            {
                #region TCP/IP 통신 확인

                case "  *IB*NETOPEN:-2   OK  ": //NET OPEN 명령어 정상 전달
                    break;

                #region NET에 연결되었거나 연결중이거나
                case "  *IB*NETOPEN:-5   OK  ": //NET OPEN 상태
                    sPort.Write("AT*IB*TCPOPEN=183.110.138.35,9805\r\n");
                    ConsolWrite("SERVER " + DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " : AT*IB*TCPOPEN=183.110.138.35,9805");
                    break;
                case "  *IB*NET_OPENED  OK  ": //NET OPEN 성공
                    sPort.Write("AT*IB*TCPOPEN=183.110.138.35,9805\r\n");
                    ConsolWrite("SERVER " + DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " : AT*IB*TCPOPEN=183.110.138.35,9805");
                    break;
                #endregion

                #region 데이터 전송부분
                case "  *IB*TCP_CONNECTED  OK  ": //NET OPEN 성공

                    intpacketsize = strpacket.Length / 2;
                    sPort.Write("AT*IB*TCPWRITE=" + strpacket + "\r\n");
                    ConsolWrite("SERVER " + DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " : AT*IB*TCPWRITE=" + strpacket);
                    break;

                case "  *IB*TCPOPEN:-2   OK    *IB*TCP_WRITABLE  OK  ": //NET OPEN 성공

                    intpacketsize = strpacket.Length / 2;
                    sPort.Write("AT*IB*TCPWRITE=" + strpacket + "\r\n");
                    ConsolWrite("SERVER " + DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " : AT*IB*TCPWRITE=" + strpacket);
                    break;

                case "   *IB*TCP_WRITABLE  OK  ": //NET OPEN 성공

                    intpacketsize = strpacket.Length / 2;
                    sPort.Write("AT*IB*TCPWRITE=" + strpacket + "\r\n");
                    ConsolWrite("SERVER " + DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " : AT*IB*TCPWRITE=" + strpacket);
                    break;

                case "서버리턴": //NET OPEN 성공
                    sPort.Write("AT*IB*TCPCLOSE\r\n");
                    break;

                case "  *IB*TCP_CLOSED  OK  ": //NET OPEN 성공
                    sPort.Write("AT*IB*NETCLOSE\r\n");
                    break;

                case "  OK  ": //NET OPEN 성공
                    break;
                #endregion


                #endregion

                #region 리셋할경우 처리
                case "AT   OK   ": //리셋했을 경우 처리
                                   //데이터 모드로 변경 
                    sPort.Write("AT+CRM=150\r\n");
                    //연결 안정을 위해 1초 딜레이
                    System.Threading.Thread.Sleep(1000);
                    sPort.Write("ATE0\r\n");
                    breset = true; //리셋 성공시 
                    break;
                case "  *SKT*RESET:1  OK  ": //리셋응답
                    breset = false;
                    break;
                #endregion                                                                                                   

                case "RESET":
                    sPort.Write("AT*SKT*RESET\r\n");
                    break;

            }
        }
        #endregion

        #region UI관련
        //리치박스에 APPEND
        public void ConsolWrite(string strtxt)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal,
                        new Action((delegate ()
                        {
                            richBox.Document.Blocks.Add(new Paragraph(new Run("[" + linenum.ToString() + "]" + strtxt)));
                            richBox.ScrollToEnd();
                            linenum++;

                            if(richBox.Document.Blocks.Count > 1000)
                            {
                                richBox.Document.Blocks.Remove(richBox.Document.Blocks.FirstBlock);
                            }
                        })));
        }

        //csv로드, csv생성, csv저장
        private void csvexpimp()
        {
            try
            {
                FileInfo fi = new FileInfo("data.csv");

                if(fi.Exists)
                {
                    DataTable temp = new DataTable();

                    StreamReader sr = new StreamReader("data.csv", Encoding.UTF8);
                    string[] headers = sr.ReadLine().Split(',');

                    foreach(string header in headers)
                    {
                        temp.Columns.Add(header);
                    }

                    while(!sr.EndOfStream)
                    {
                        string[] rows = sr.ReadLine().Split(',');
                        DataRow dr = temp.NewRow();
                        for(int i = 0; i < headers.Length; i++)
                        {
                            dr[i] = rows[i];
                        }
                        temp.Rows.Add(dr);
                    }

                    datagrid.ItemsSource = temp.DefaultView;
                }
                else
                {
                    DataTable temp = new DataTable();
                    temp.Columns.Add("FL");
                    temp.Columns.Add("FLSM");
                    temp.Columns.Add("PRS");
                    temp.Columns.Add("CHK");

                    csvexp(temp);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //데이터 저장용 CSV파일 생성
        private void csvexp(DataTable temp)
        {
            try
            {
                //파일 저장을 위해 스트림 생성.
                FileStream fs = new FileStream("data.csv", FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);

                //컬럼 이름들을 ","로 나누고 저장.
                string line = string.Join(",", temp.Columns.Cast<object>());
                sw.WriteLine(line);

                //row들을 ","로 나누고 저장.
                foreach(DataRow item in temp.Rows)
                {
                    line = string.Join(",", item.ItemArray.Cast<object>());
                    sw.WriteLine(line);
                }

                sw.Close();
                fs.Close();

                sw.Dispose();
                fs.Dispose();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        //시작버튼
        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bend = true;

                if(thread == null)
                {
                    //timer.Interval = 1000;
                    txtmin.IsEnabled = false;
                    timer.Interval = 1000 * 60 * Convert.ToInt32(txtmin.Text);
                    timer.Start();

                    thread = new Thread(new ThreadStart(work));
                    thread.Start();
                }
                else if(thread.ThreadState == ThreadState.Running || thread.ThreadState == ThreadState.WaitSleepJoin || thread.ThreadState == ThreadState.Stopped)
                {
                    thread.Abort();
                    thread = null;
                    timer.Stop();
                    timer.Dispose();
                    txtmin.IsEnabled = true;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("시작버튼에 에러 발생");
            }
        }

        //그리드 저장버튼
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataView dv = (DataView)datagrid.ItemsSource;
                csvexp(dv.ToTable());

                MessageBox.Show("저장되었습니다.");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        #endregion

        #region 프로토콜관련
        //패킷만들기
        private string MakePacket(DataRow dr)
        {
            try
            {
                byte[] btsizechk;
                string strsubswap;

                //1.국번이 먼저(하드코딩)
                //국번은 여기서 변경!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                string strstation = string.Format("{0:x2}", 2).ToUpper();
                string strresult = "000000" + strstation;

                byte[] bytetemp;
                string strtemp = "";
                int intstart = 40;
                int dtadr = 42;
                int fladr = 44;
                int flsmadr = 46;
                int prsadr = 50;
                int intflt = 52;

                #region 날짜
                //날짜(시간을 uint로 UTC)
                string strdt = nowdt.ToString("yyyyMMddHHmmss");
                int y = Convert.ToInt32(strdt.Substring(0, 4));
                int M = Convert.ToInt32(strdt.Substring(4, 2));
                int d = Convert.ToInt32(strdt.Substring(6, 2));
                int h = Convert.ToInt32(strdt.Substring(8, 2));
                int m = Convert.ToInt32(strdt.Substring(10, 2));
                int s = Convert.ToInt32(00);

                DateTime drvdt = new DateTime(y, M, d, h, m, s);
                TimeSpan ts = drvdt - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                uint uintdt = Convert.ToUInt32(ts.TotalSeconds);
                bytetemp = BitConverter.GetBytes(uintdt);
                //결과
                string strdtresult = uintdt.ToString("x4").ToUpper();

                strresult = strresult + strdtresult;
                #endregion

                #region 유량
                bytetemp = null;
                strtemp = "";
                //결과
                //(Real)
                float fltfl = Convert.ToSingle(dr["FL"]);
                byte[] btfl = BitConverter.GetBytes(fltfl);
                bytetemp = btfl;
                Array.Reverse(btfl);  //Endian 변환
                string strflresult = BitConverter.ToString(btfl).Replace("-", string.Empty);

                //(int)
                //uint fltfl = Convert.ToUInt32(dr["FL"]);
                //bytetemp = BitConverter.GetBytes(fltfl);
                //string strflresult = fltfl.ToString("x8").ToUpper();

                //3.유량값                                
                if(fladr != intstart + strresult.Length / 4)
                {
                    strresult = strresult.PadRight((fladr - intstart) * 4, '0');
                }

                strresult = strresult + strflresult;
                #endregion

                #region 적산
                bytetemp = null;
                strtemp = "";
                //결과
                //(Real)
                float fltflsm = Convert.ToSingle(dr["FLSM"]);
                byte[] btflsm = BitConverter.GetBytes(fltflsm);
                bytetemp = btflsm;
                Array.Reverse(btflsm);  //Endian 변환
                string strflsmresult = BitConverter.ToString(btflsm).Replace("-", string.Empty);

                //(int)
                //uint fltflsm = Convert.ToUInt32(dr["FLSM"]);
                //bytetemp = BitConverter.GetBytes(fltflsm);
                //string strflsmresult = fltflsm.ToString("x8").ToUpper();

                //4.적산값
                if(flsmadr != intstart + strresult.Length / 4)
                {
                    strresult = strresult.PadRight((flsmadr - intstart) * 4, '0');
                }

                strresult = strresult + strflsmresult;
                #endregion

                #region 수압
                bytetemp = null;
                strtemp = "";
                //결과
                //(Real)
                float fltprs = Convert.ToSingle(dr["PRS"]);
                byte[] btprs = BitConverter.GetBytes(fltprs);
                bytetemp = btprs;
                Array.Reverse(btprs);  //Endian 변환
                string strprsresult = BitConverter.ToString(btprs).Replace("-", string.Empty);
                //4.수압값

                if(prsadr != intstart + strresult.Length / 4)
                {
                    strresult = strresult.PadRight((prsadr - intstart) * 4, '0');
                }

                strresult = strresult + strprsresult;
                #endregion

                #region 플래그
                string strflt = "00000000";
                strresult = strresult.PadRight((intflt - intstart) * 4, '0');
                strresult = strresult + strflt;
                #endregion

                #region 헤더
                strsubswap = strstation + "1000280078" + string.Format("{0:x}", strresult.Length / 2).ToUpper() + swap(sub(strresult));

                //10 funtion code
                //0028 start address
                //0078 Quantity
                //byteCount 
                #endregion

                #region CRC16
                byte[] by = HexToByte(strsubswap);

                UInt16 Uint16crc = ComputeCrc(by);

                if(string.Format("{0:x2}", Uint16crc).Length != 4)
                {

                }

                string CRCresult = string.Format("{0:x4}", Uint16crc).ToUpper().Substring(2, 2).ToUpper() + string.Format("{0:x4}", Uint16crc).ToUpper().Substring(0, 2).ToUpper();

                strsubswap = strsubswap + CRCresult;
                #endregion

                return strsubswap;
            }
            catch(Exception ex)
            {
                return null;
                throw ex;
            }
        }

        //4자리씩 끊기
        public static List<string> sub(string strhex2byte)
        {
            try
            {
                if((strhex2byte.Length / 4).GetType().Equals(typeof(int)))
                {
                    int strlenth = strhex2byte.Length / 8;
                    List<string> strbuff = new List<string>();

                    for(int i = 0; i < strlenth; i++)
                    {
                        strbuff.Add(strhex2byte.Substring(i * 8, 8));
                    }

                    return strbuff;
                }

                return null;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        //4자리씩 스왑
        public static string swap(List<string> strbuff)
        {
            try
            {
                string strswapresult = string.Empty;

                foreach(string str in strbuff)
                {
                    strswapresult = strswapresult + str.Substring(4, 4) + str.Substring(0, 4);
                }

                return strswapresult;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        private ushort[] CrcTable = {
            0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
            0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
            0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
            0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
            0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
            0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
            0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
            0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
            0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
            0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
            0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
            0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
            0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
            0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
            0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
            0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
            0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
            0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
            0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
            0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
            0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
            0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
            0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
            0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
            0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
            0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
            0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
            0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
            0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
            0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
            0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
            0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };

        public UInt16 ComputeCrc(byte[] data)
        {
            ushort crc = 0xFFFF;

            foreach(byte datum in data)
            {
                crc = (ushort)((crc >> 8) ^ CrcTable[(crc ^ datum) & 0xFF]);
            }

            return crc;
        }

        public static byte[] HexToByte(string msg)
        {
            msg = msg.Replace(" ", "");
            byte[] comBuffer = new byte[msg.Length / 2];
            for(int i = 0; i < msg.Length; i += 2)
            {
                try
                {
                    comBuffer[i / 2] = (byte)Convert.ToByte(msg.Substring(i, 2), 16);
                }
                catch(ArgumentOutOfRangeException argumentoutofrange)
                {
                    Console.WriteLine("잘못된 형식의 HEX값이 입력되었습니다.");
                }
            }

            return comBuffer;
        }
        #endregion
    }
}
