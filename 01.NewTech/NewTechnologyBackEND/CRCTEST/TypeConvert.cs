﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRCTEST
{
    public class TypeConvert
    {
        public static byte[] HexToByte(string msg)
        {
            msg = msg.Replace(" ", "");
            byte[] comBuffer = new byte[msg.Length / 2];
            for(int i = 0; i < msg.Length; i += 2)
            {
                try
                {
                    comBuffer[i / 2] = (byte)Convert.ToByte(msg.Substring(i, 2), 16);
                }
                catch(ArgumentOutOfRangeException argumentoutofrange)
                {
                    Console.WriteLine("잘못된 형식의 HEX값이 입력되었습니다.");
                }
            }

            return comBuffer;
        }

        public static string ByteToHex(byte[] msg)
        {
            StringBuilder sbd = new StringBuilder(msg.Length * 2);
            foreach(byte data in msg)
            {
                sbd.Append(Convert.ToString(data, 16).PadLeft(2, '0'));
            }

            return sbd.ToString();
        }     

        public static string ASCIITOHex(string ascii)
        {
            StringBuilder sb = new StringBuilder();
            byte[] inputBytes = Encoding.UTF8.GetBytes(ascii);
            foreach(byte b in inputBytes)
            {
                sb.Append(string.Format("{0:x2}", b));
            }

            return sb.ToString();
        }
    }
}
