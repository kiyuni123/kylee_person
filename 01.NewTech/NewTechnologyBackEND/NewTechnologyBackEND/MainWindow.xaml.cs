﻿using GTIFramework.Common.MessageBox;
using NewTechnologyBackEND.Work;
using System;
using System.Collections;
using System.Data;
using System.Threading;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;

namespace NewTechnologyBackEND
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        int linenum = 0;                            //Richbox LineNumber
        Thread thread;                              //분석 진행할 Thread
        System.Timers.Timer timer;                  //분석 주기 설정할 Timer
        MainWork work = new MainWork();

        DataTable dtBlockList;                      //블록리스트 조회
        DataTable dtFlowsignval;                    //유량음수판단
        DataTable dtEventCntCheck;                  //이벤트 갯수 체크
        DataTable dtFlow20Up;                       //유량20%증가
        DataTable dtFlowUpDown;                     //유량증감
        DataTable dtFlowUpDownAddChk;               //유량증감 확인
        DateTime dateNow;
        Hashtable htconditions = new Hashtable();


        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            datagrid.ItemsSource = work.Select_AnalBlockList(null).DefaultView;
            datagrid2.ItemsSource = work.Select_PRSAnalBlockList(null).DefaultView;
            btnStart.Click += BtnStart_Click;
            datagrid.PreviewMouseDown += Datagrid_PreviewMouseDown;
            datagrid2.PreviewMouseDown += Datagrid2_PreviewMouseDown;
            Closed += MainWindow_Closed;
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            try
            {
                timer.Stop();
                timer.Dispose();
                thread.Abort();
            }
            catch (Exception ex) { Messages.ErrLog(ex); }
        }

        #region 시작버튼
        //쓰레드 작동 버튼
        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Timer_Elapsed(null, null);

                if (thread == null)
                {
                    //timer.Interval = 1000 * 60;
                    //TimerFlag = true;
                    ////timer.Interval = 1000 * 60 * 5;
                    //timer.Start();

                    //thread = new Thread(new ThreadStart(threadwork));
                    //thread.Start();

                    runstat.Content = "동작";

                    thread = new Thread(new ThreadStart(thread_FX));
                    thread.Name = "thread";
                    thread.Start();
                }
                else if (thread.ThreadState == ThreadState.Running || thread.ThreadState == ThreadState.WaitSleepJoin || thread.ThreadState == ThreadState.Stopped)
                {
                    //thread.Abort();
                    //thread = null;
                    //timer.Stop();
                    //timer.Dispose();

                    runstat.Content = "멈춤";

                    timer.Stop();
                    timer.Dispose();
                    thread.Abort();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("시작버튼에 에러 발생");
            }
        }
        #endregion

        #region 이벤트 막기
        private void Datagrid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void Datagrid2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        #endregion

        private void thread_FX()
        {
            try
            {
                timer = new System.Timers.Timer();
                timer.Interval = 1000;
                timer.Elapsed += Timer_Elapsed;
                timer.Start();
            }
            catch (Exception ex) { Messages.ErrLog(ex); }
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Thread 처리부분
            try
            {
                dateNow = DateTime.Now;

                //5분간격 0초 작동
                if (dateNow.Second == 5)
                //if (true)
                {
                    if (dateNow.Minute % 10 == 5 || dateNow.Minute % 10 == 0)
                    //if (true)
                    {
                        dtBlockList = ((DataView)datagrid.ItemsSource).Table;

                        //유량!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        #region 유량
                        foreach (DataRow r in dtBlockList.Rows)
                        {
                            try
                            {
                                Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                                    new Action((delegate ()
                                    {
                                        datagrid.SelectedIndex = dtBlockList.Rows.IndexOf(r);
                                        ConsolWrite("=====================================");
                                    })));

                                #region //유량음수판단 (OK)
                                //2019-05-14 테스트 수정
                                //음수 발생시 과거 유량(CAT=F) 이벤트 조회
                                //유량 이벤트 확인 후 조건에 만족시 완료 처리 (STAT = N)
                                //유량 이벤트 확인 후 조건에 불만족시 삭제 처리 (DEL = Y)

                                dtFlowsignval = null;
                                htconditions.Clear();
                                htconditions.Add("IN_INST_CODE", r["INSTRUMENT_CODE"].ToString());
                                dtFlowsignval = work.Select_BlockFlowSign(htconditions);

                                if (dtFlowsignval.Rows.Count == 1)
                                {
                                    if (dtFlowsignval.Rows[0]["SIGNVAL"].ToString().Equals("-1")
                                        || dtFlowsignval.Rows[0]["SIGNVAL"].ToString().Equals("0"))
                                    {
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량 음수판단발생(Event Occurrence)");

                                        htconditions.Add("DECN_CD", "01");
                                        htconditions.Add("INST_CD", dtFlowsignval.Rows[0]["INST_CODE"].ToString());
                                        htconditions.Add("DT", dtFlowsignval.Rows[0]["DT"].ToString());
                                        htconditions.Add("MESR_VAL", dtFlowsignval.Rows[0]["MVAL"].ToString());
                                        htconditions.Add("CAT", "F");
                                        htconditions.Add("UPPER_SEQ", "");
                                        htconditions.Add("CNT", "");
                                        htconditions.Add("STAT", "N");
                                        htconditions.Add("CHK", "");

                                        //1.음수 이벤트가 발생 할 경우 기존 진행중인 이벤트진행완료 처리 또는 이벤트삭제
                                        //2. 1.완료 후 음수이벤트 등록
                                        dtEventCntCheck = work.Select_EventCheck(htconditions);

                                        //음수이벤트 Insert(ok)
                                        htconditions["UPPER_SEQ"] = "";
                                        work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                        foreach (DataRow dr in dtEventCntCheck.Rows)
                                        {
                                            htconditions["UPPER_SEQ"] = dr["SEQ"].ToString();

                                            if (dr["DECN_CD"].Equals("02"))
                                            {
                                                //20%증가시 갯수 확인 후 완료 및 삭제
                                                if (Convert.ToInt32(dr["CNT"].ToString()) > 23) //이벤트 완료 상태 STAT=N
                                                    htconditions["CHK"] = "1";
                                                else //이벤트 삭제
                                                    htconditions["CHK"] = "0";

                                                work.Update_FLPRSEVENTEnd(htconditions);
                                            }
                                            else if (dr["DECN_CD"].Equals("03"))
                                            {
                                                //증감시 갯수 확인 후 완료 및 삭제

                                                if (Convert.ToInt32(dr["CNT"].ToString()) > 3) //이벤트 완료 상태 STAT=N
                                                    htconditions["CHK"] = "1";
                                                else //이벤트 삭제
                                                    htconditions["CHK"] = "0";

                                                work.Update_FLPRSEVENTEnd(htconditions);
                                            }
                                        }
                                        continue;
                                    }
                                    else
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량 음수판단정상");
                                }
                                else
                                    ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량 음수판단 실패(최근데이터 조회 실패)");
                                #endregion

                                #region //이전값 15% 변동
                                //진행 중인 이벤트가 있는지 체크먼저 하고 있으면 최초값 가져와서 비교 없으면 이전값과 비교하여 이벤트 생성
                                //2019-05-14 테스트 수정
                                //가장 최근에 이벤트 상태가 N인 시점부터 미래 1시간 까지 검색범위 수정
                                htconditions.Clear();
                                htconditions.Add("DECN_CD", "02");
                                htconditions.Add("INST_CD", r["INSTRUMENT_CODE"].ToString());
                                htconditions.Add("CAT", "F");

                                dtFlow20Up = null;

                                //진행중인 이벤트가 없을경우
                                if (work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].Equals(DBNull.Value))
                                {
                                    dtFlow20Up = work.Select_BlockFlow15Up(htconditions);

                                    //분석에 필요 데이터가 존재할경우
                                    if (dtFlow20Up.Rows.Count == 2)
                                    {
                                        if (dtFlow20Up.Rows[0]["FLAG"].ToString().Equals("UP") || dtFlow20Up.Rows[0]["FLAG"].ToString().Equals("DOWN"))
                                        {
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량15% 변동 발생(Event Occurrence)");
                                            //새로운 이벤트 추가 (정상데이터(UPPER_SEQ) + 이상데이터)
                                            htconditions.Add("DT", dtFlow20Up.Rows[1]["DT"].ToString());
                                            htconditions.Add("MESR_VAL", dtFlow20Up.Rows[1]["MVAL"].ToString());
                                            htconditions.Add("CNT", "1");
                                            htconditions.Add("STAT", "Y");
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                            htconditions["DT"] = dtFlow20Up.Rows[0]["DT"].ToString();
                                            htconditions["MESR_VAL"] = dtFlow20Up.Rows[0]["MVAL"].ToString();
                                            htconditions["CNT"] = DBNull.Value;
                                            htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);
                                        }
                                        else //새로운 이벤트 발생안됨
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량15% 변동 정상");
                                    }
                                    else
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량15% 변동 판단 실패(최근데이터 조회 실패)");
                                }
                                //기존이벤트 존재할경우 추가 ADD
                                else
                                {
                                    dtFlow20Up = work.Select_BlockFlow15UpAddCheck(htconditions);

                                    //이벤트 연속 확인 (새로운 이벤트 진행 추가)
                                    if ((dtFlow20Up.Rows[0]["FLAG"].ToString().Equals("UP") || dtFlow20Up.Rows[0]["FLAG"].ToString().Equals("DOWN"))
                                        && dtFlow20Up.Rows[0]["FLAG"].ToString().Equals(dtFlow20Up.Rows[1]["FLAG"].ToString()))
                                    {
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량15% 변동 연속발생(Event Occurrence)");
                                        htconditions["DT"] = dtFlow20Up.Rows[0]["DT"].ToString();
                                        htconditions["MESR_VAL"] = dtFlow20Up.Rows[0]["MVAL"].ToString();
                                        htconditions["CNT"] = DBNull.Value;
                                        htconditions.Add("STAT", "Y");

                                        string strFlowUPPER_SEQ = work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString();
                                        htconditions.Add("UPPER_SEQ", strFlowUPPER_SEQ);

                                        work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                        ConsolWrite("유량 15% UPPER_SEQ = " + strFlowUPPER_SEQ);
                                        //이벤트 연속 확인(기존 이벤트 COUNT UPDATE)
                                        work.Update_BlockFlow15Up(htconditions);
                                    }
                                    else
                                    {
                                        //새로운 이벤트 발생안됨
                                        htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());

                                        if (Convert.ToInt32(work.Select_FLPRS15UpEventCnt(htconditions).Rows[0]["CNT"].ToString()) > 23)
                                            htconditions.Add("CHK", "1");
                                        else
                                            htconditions.Add("CHK", "0");

                                        work.Update_FLPRSEVENTEnd(htconditions);
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량15% 변동 종료");
                                    }
                                }
                                #endregion

                                #region //증감 반복
                                htconditions.Clear();
                                htconditions.Add("DECN_CD", "03");
                                htconditions.Add("INST_CD", r["INSTRUMENT_CODE"].ToString());
                                htconditions.Add("CAT", "F");

                                dtFlowUpDown = null;

                                //진행중인 이벤트가 없을경우
                                if (work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].Equals(DBNull.Value))
                                {
                                    dtFlowUpDown = work.Select_BlockFlowUpDown(htconditions);

                                    if (dtFlowUpDown.Rows.Count == 3)
                                    {
                                        if ((dtFlowUpDown.Rows[0]["FLAG"].ToString().Equals("UP") || dtFlowUpDown.Rows[0]["FLAG"].ToString().Equals("DOWN"))
                                            && (dtFlowUpDown.Rows[1]["FLAG"].ToString().Equals("UP") || dtFlowUpDown.Rows[1]["FLAG"].ToString().Equals("DOWN"))
                                            && !dtFlowUpDown.Rows[1]["FLAG"].ToString().Equals(dtFlowUpDown.Rows[0]["FLAG"].ToString()))
                                        {
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량 증감 발생(Event Occurrence)");
                                            //새로운 이벤트 추가 (정상데이터(UPPER_SEQ) + 이상데이터)
                                            htconditions.Add("DT", dtFlowUpDown.Rows[2]["DT"].ToString());
                                            htconditions.Add("MESR_VAL", dtFlowUpDown.Rows[2]["MVAL"].ToString());
                                            htconditions.Add("CNT", DBNull.Value);
                                            htconditions.Add("STAT", "Y");
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                            htconditions["DT"] = dtFlowUpDown.Rows[1]["DT"].ToString();
                                            htconditions["MESR_VAL"] = dtFlowUpDown.Rows[1]["MVAL"].ToString();
                                            htconditions["CNT"] = "2";
                                            htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                            htconditions["DT"] = dtFlowUpDown.Rows[0]["DT"].ToString();
                                            htconditions["MESR_VAL"] = dtFlowUpDown.Rows[0]["MVAL"].ToString();
                                            htconditions["CNT"] = "2";
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);
                                            continue;
                                        }
                                        else
                                        {
                                            //새로운 이벤트 발생안됨
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량 증감 정상");
                                        }
                                    }
                                    else
                                    {
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량 증감 판단 실패(최근데이터 조회 실패)");
                                    }
                                }
                                //기존이벤트 존재할경우 추가 ADD (여기서부터 처리)
                                else
                                {
                                    dtFlowUpDownAddChk = work.Select_BlockFlowUpDownAddCheck(htconditions);

                                    if (dtFlowUpDownAddChk.Rows.Count == 2)
                                    {
                                        if ((dtFlowUpDownAddChk.Rows[0]["FLAG"].ToString().Equals("UP") || dtFlowUpDownAddChk.Rows[0]["FLAG"].ToString().Equals("DOWN"))
                                            && (dtFlowUpDownAddChk.Rows[1]["FLAG"].ToString().Equals("UP") || dtFlowUpDownAddChk.Rows[1]["FLAG"].ToString().Equals("DOWN"))
                                            && !dtFlowUpDownAddChk.Rows[0]["FLAG"].ToString().Equals(dtFlowUpDownAddChk.Rows[1]["FLAG"].ToString()))
                                        {
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량 증감 연속 발생(Event Occurrence)");
                                            htconditions["DT"] = dtFlowUpDownAddChk.Rows[0]["DT"].ToString();
                                            htconditions["MESR_VAL"] = dtFlowUpDownAddChk.Rows[0]["MVAL"].ToString();
                                            htconditions["CNT"] = DBNull.Value;
                                            htconditions.Add("STAT", "Y");
                                            htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                            //카운트 업데이트
                                            work.Update_BlockFlowUpDown(htconditions);
                                        }
                                        else
                                        {
                                            htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());

                                            if (Convert.ToInt32(work.Select_FLPRS15UpEventCnt(htconditions).Rows[0]["CNT"].ToString()) > 3)
                                                htconditions.Add("CHK", "1");
                                            else
                                                htconditions.Add("CHK", "0");

                                            //카운트 업데이트
                                            work.Update_FLPRSEVENTEnd(htconditions);

                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량 증감 이상 종료");
                                        }
                                    }
                                    else
                                    {
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 유량 증감 판단 실패(최근데이터 조회 실패)");
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex) { Messages.ErrLog(ex); }

                            Thread.Sleep(50);
                        } 
                        #endregion

                        Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                                new Action((delegate ()
                                {
                                    dtBlockList = null;
                                    dtBlockList = ((DataView)datagrid2.ItemsSource).Table;
                                })));

                        //수압!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        #region 수압
                        foreach (DataRow r in dtBlockList.Rows)
                        {
                            try
                            {
                                Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                                    new Action((delegate ()
                                    {
                                        datagrid2.SelectedIndex = dtBlockList.Rows.IndexOf(r);
                                        ConsolWrite("=====================================");
                                    })));

                                #region //수압입계치판단 (ok)
                                dtFlowsignval = null;
                                htconditions.Clear();
                                htconditions.Add("IN_INST_CODE", r["INSTRUMENT_CODE"].ToString());
                                dtFlowsignval = work.Select_BlockPRSSign(htconditions);

                                if (dtFlowsignval.Rows.Count == 1)
                                {
                                    if (Math.Sign(Convert.ToDouble(dtFlowsignval.Rows[0]["MVAL"].ToString()) - Convert.ToDouble(r["UP"].ToString())) == 1
                                    || Math.Sign(Convert.ToDouble(dtFlowsignval.Rows[0]["MVAL"].ToString()) - Convert.ToDouble(r["DOWN"].ToString())) == -1)
                                    {
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압 임계치이상발생(Event Occurrence)");

                                        htconditions.Add("DECN_CD", "04");
                                        htconditions.Add("INST_CD", dtFlowsignval.Rows[0]["INST_CODE"].ToString());
                                        htconditions.Add("DT", dtFlowsignval.Rows[0]["DT"].ToString());
                                        htconditions.Add("MESR_VAL", dtFlowsignval.Rows[0]["MVAL"].ToString());
                                        htconditions.Add("CAT", "P");
                                        htconditions.Add("UPPER_SEQ", "");
                                        htconditions.Add("CNT", "");
                                        htconditions.Add("STAT", "N");
                                        htconditions.Add("CHK", "");

                                        //1.임계치 이벤트가 발생 할 경우 기존 진행중인 이벤트진행완료 처리 또는 이벤트삭제
                                        //2. 1.완료 후 임계치이벤트 등록
                                        dtEventCntCheck = work.Select_EventCheck(htconditions);

                                        //음수이벤트 Insert(ok)    
                                        htconditions["UPPER_SEQ"] = "";
                                        work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                        foreach (DataRow dr in dtEventCntCheck.Rows)
                                        {
                                            htconditions["UPPER_SEQ"] = dr["SEQ"].ToString();

                                            if (dr["DECN_CD"].Equals("05"))
                                            {
                                                //20%증가시 갯수 확인 후 완료 및 삭제
                                                if (Convert.ToInt32(dr["CNT"].ToString()) > 23)//이벤트 완료 상태 STAT=N
                                                    htconditions["CHK"] = "1";
                                                else //이벤트 삭제
                                                    htconditions["CHK"] = "0";

                                                work.Update_FLPRSEVENTEnd(htconditions);
                                            }
                                            else if (dr["DECN_CD"].Equals("06"))
                                            {
                                                //증감시 갯수 확인 후 완료 및 삭제
                                                if (Convert.ToInt32(dr["CNT"].ToString()) > 7)//이벤트 완료 상태 STAT=N
                                                    htconditions["CHK"] = "1";
                                                else//이벤트 삭제
                                                    htconditions["CHK"] = "0";

                                                work.Update_FLPRSEVENTEnd(htconditions);
                                            }
                                        }
                                        continue;
                                    }
                                    else
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압 임계치판단정상");
                                }
                                else
                                    ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압 임계치판단 실패(최근데이터 조회 실패)");
                                #endregion

                                #region //이전값 15이상 변동
                                //진행 중인 이벤트가 있는지 체크먼저 하고 있으면 최초값 가져와서 비교 없으면 이전값과 비교하여 이벤트 생성
                                htconditions.Clear();
                                htconditions.Add("DECN_CD", "05");
                                htconditions.Add("INST_CD", r["INSTRUMENT_CODE"].ToString());
                                htconditions.Add("CAT", "P");

                                dtFlow20Up = null;

                                //이벤트 생성
                                if (work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].Equals(DBNull.Value))
                                {
                                    dtFlow20Up = work.Select_BlockPRS15Up(htconditions);

                                    if (dtFlow20Up.Rows.Count == 2)
                                    {
                                        if (dtFlow20Up.Rows[0]["FLAG"].ToString().Equals("UP") || dtFlow20Up.Rows[0]["FLAG"].ToString().Equals("DOWN"))
                                        {
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압15% 변동 발생(Event Occurrence)");

                                            htconditions.Add("STAT", "Y");

                                            //새로운 이벤트 추가 (정상데이터(UPPER_SEQ) + 이상데이터)
                                            htconditions.Add("DT", dtFlow20Up.Rows[1]["DT"].ToString());
                                            htconditions.Add("MESR_VAL", dtFlow20Up.Rows[1]["MVAL"].ToString());
                                            htconditions.Add("CNT", DBNull.Value);
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                            htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());
                                            htconditions["DT"] = dtFlow20Up.Rows[0]["DT"].ToString();
                                            htconditions["MESR_VAL"] = dtFlow20Up.Rows[0]["MVAL"].ToString();
                                            htconditions["CNT"] = "1";
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);
                                        }
                                        else//새로운 이벤트 발생안됨
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압15% 변동 정상");
                                    }
                                    else
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압15% 변동 판단 실패(최근데이터 조회 실패)");
                                }
                                //연속되는 이벤트
                                else
                                {
                                    dtFlow20Up = work.Select_BlockPRS15UpAddCheck(htconditions);

                                    if (dtFlow20Up.Rows.Count == 2)
                                    {
                                        //이벤트 연속 확인 (새로운 이벤트 진행 추가)
                                        if (dtFlow20Up.Rows[0]["FLAG"].ToString().Equals(dtFlow20Up.Rows[1]["FLAG"].ToString()))
                                        {
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압15% 변동 연속발생(Event Occurrence)");
                                            htconditions["DT"] = dtFlow20Up.Rows[0]["DT"].ToString();
                                            htconditions["MESR_VAL"] = dtFlow20Up.Rows[0]["MVAL"].ToString();
                                            htconditions["CNT"] = DBNull.Value;
                                            htconditions.Add("STAT", "Y");

                                            string strPresUPPER_SEQ = work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString();
                                            htconditions.Add("UPPER_SEQ", strPresUPPER_SEQ);
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                            ConsolWrite("수압 15% UPPER_SEQ = " + strPresUPPER_SEQ);

                                            //이벤트 연속 확인 (기존 이벤트 COUNT UPDATE)
                                            work.Update_BlockPRS15UpCNT(htconditions);
                                        }
                                        else
                                        {
                                            //새로운 이벤트 발생안됨
                                            htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());

                                            if (Convert.ToInt32(work.Select_FLPRS15UpEventCnt(htconditions).Rows[0]["CNT"]) > 11)
                                                htconditions.Add("CHK", "1");
                                            else
                                                htconditions.Add("CHK", "0");

                                            work.Update_FLPRSEVENTEnd(htconditions);
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압15% 변동 종료");
                                        }
                                    }
                                    else
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압15% 변동 판단 실패(최근데이터 조회 실패)");
                                }
                                #endregion

                                #region //증감 반복
                                htconditions.Clear();
                                htconditions.Add("DECN_CD", "06");
                                htconditions.Add("INST_CD", r["INSTRUMENT_CODE"].ToString());
                                htconditions.Add("CAT", "P");

                                dtFlowUpDown = null;

                                //증감 이벤트 여부 확인
                                //최초 발생 시
                                if (work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].Equals(DBNull.Value))
                                {
                                    dtFlowUpDown = work.Select_BlockPRSUpDown(htconditions);

                                    if (dtFlowUpDown.Rows.Count == 3)
                                    {
                                        if ((dtFlowUpDown.Rows[0]["FLAG"].ToString().Equals("UP") || dtFlowUpDown.Rows[0]["FLAG"].ToString().Equals("DOWN"))
                                            && (dtFlowUpDown.Rows[1]["FLAG"].ToString().Equals("UP") || dtFlowUpDown.Rows[1]["FLAG"].ToString().Equals("DOWN"))
                                            && !dtFlowUpDown.Rows[1]["FLAG"].ToString().Equals(dtFlowUpDown.Rows[0]["FLAG"].ToString()))
                                        {
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압 증감 발생(Event Occurrence)");
                                            //새로운 이벤트 추가 (정상데이터(UPPER_SEQ) + 이상데이터)
                                            htconditions.Add("DT", dtFlowUpDown.Rows[2]["DT"].ToString());
                                            htconditions.Add("MESR_VAL", dtFlowUpDown.Rows[2]["MVAL"].ToString());
                                            htconditions.Add("CNT", DBNull.Value);
                                            htconditions.Add("STAT", "Y");
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                            htconditions["DT"] = dtFlowUpDown.Rows[1]["DT"].ToString();
                                            htconditions["MESR_VAL"] = dtFlowUpDown.Rows[1]["MVAL"].ToString();
                                            htconditions["CNT"] = "2";
                                            htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                            htconditions["DT"] = dtFlowUpDown.Rows[0]["DT"].ToString();
                                            htconditions["MESR_VAL"] = dtFlowUpDown.Rows[0]["MVAL"].ToString();
                                            htconditions["CNT"] = "2";
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);
                                            continue;
                                        }
                                        else//새로운 이벤트 발생안됨
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압 증감 정상");
                                    }
                                    else
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압 증감 이상 판단 실패(최근데이터 조회 실패)");
                                }
                                else
                                {
                                    dtFlowUpDownAddChk = work.Select_BlockPRSUpDownAddCheck(htconditions);

                                    if (dtFlowUpDownAddChk.Rows.Count == 2)
                                    {
                                        if ((dtFlowUpDownAddChk.Rows[0]["FLAG"].ToString().Equals("UP") || dtFlowUpDownAddChk.Rows[0]["FLAG"].ToString().Equals("DOWN"))
                                            && (dtFlowUpDownAddChk.Rows[1]["FLAG"].ToString().Equals("UP") || dtFlowUpDownAddChk.Rows[1]["FLAG"].ToString().Equals("DOWN"))
                                            && !dtFlowUpDownAddChk.Rows[0]["FLAG"].ToString().Equals(dtFlowUpDownAddChk.Rows[1]["FLAG"].ToString()))
                                        {
                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압 증감 연속 발생(Event Occurrence)");
                                            htconditions["DT"] = dtFlowUpDownAddChk.Rows[0]["DT"].ToString();
                                            htconditions["MESR_VAL"] = dtFlowUpDownAddChk.Rows[0]["MVAL"].ToString();
                                            htconditions["CNT"] = DBNull.Value;
                                            htconditions.Add("STAT", "Y");
                                            htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());
                                            work.Insert_IDEA_DECN_REAL_MST(htconditions);

                                            //카운트 업데이트
                                            work.Update_BlockPRSUpDownCNT(htconditions);
                                        }
                                        else
                                        {
                                            htconditions.Add("UPPER_SEQ", work.Select_FLPRSEVENTCheck(htconditions).Rows[0]["MINSEQ"].ToString());

                                            if (Convert.ToInt32(work.Select_FLPRS15UpEventCnt(htconditions).Rows[0]["CNT"].ToString()) > 7)
                                                htconditions.Add("CHK", "1");
                                            else
                                                htconditions.Add("CHK", "0");

                                            //카운트 업데이트
                                            work.Update_FLPRSEVENTEnd(htconditions);

                                            ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압 증감 이상 종료");
                                        }
                                    }
                                    else
                                        ConsolWrite(r["INSTRUMENT_CODE"].ToString() + " : 수압 증감 이상 판단 실패(최근데이터 조회 실패)");
                                }
                                #endregion
                            }
                            catch (Exception ex) { Messages.ErrLog(ex); }

                            Thread.Sleep(50);
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex) { Messages.ErrLog(ex); }
        }

        #region UI 관련
        //리치박스에 APPEND
        public void ConsolWrite(string strtxt)
        {
            Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action((delegate ()
                        {
                            if (richBox.Document.Blocks.Count > 1000)
                            {
                                richBox.Document.Blocks.Remove(richBox.Document.Blocks.FirstBlock);
                            }

                            richBox.Document.Blocks.Add(new Paragraph(new Run(DateTime.Now.ToString() + "[" + linenum.ToString() + "]" + strtxt)));
                            richBox.ScrollToEnd();
                            linenum++;
                        })));
        }
        #endregion
    }
}
