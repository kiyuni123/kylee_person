﻿using NewTechnologyBackEND.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewTechnologyBackEND.Work
{
    class MainWork
    {
        MainDao dao = new MainDao();

        #region @@@@@@@@@@@@@@@@@@@@@@@@@@공통@@@@@@@@@@@@@@@@@@@@@@@@@@
        //<!--DB Connection Check-->
        public DataTable Select(Hashtable conditions)
        {
            return dao.Select(conditions);
        }

        //<!--의사결정 실시간 테이블 진행하면서 완료-->
        public void Insert_IDEA_DECN_REAL_MST(Hashtable conditions)
        {
            dao.Insert_IDEA_DECN_REAL_MST(conditions);
        }

        //<!--블록 리스트 조회-->
        public DataTable Select_AnalBlockList(Hashtable conditions)
        {
            return dao.Select_AnalBlockList(conditions);
        }

        //<!--수압 리스트 조회-->
        public DataTable Select_PRSAnalBlockList(Hashtable conditions)
        {
            return dao.Select_PRSAnalBlockList(conditions);
        }

        //<!--이벤트 갯수 체크-->
        public DataTable Select_EventCheck(Hashtable conditions)
        {
            return dao.Select_EventCheck(conditions);
        }

        //<!--유량, 수압 기존 이벤트 여부 체크-->
        public DataTable Select_FLPRSEVENTCheck(Hashtable conditions)
        {
            return dao.Select_FLPRSEVENTCheck(conditions);
        }

        //<!--15%증가 기존이벤트 개수확인 유량, 수압 공통-->
        public DataTable Select_FLPRS15UpEventCnt(Hashtable conditions)
        {
            return dao.Select_FLPRS15UpEventCnt(conditions);
        }

        //<!--CHK가 1인경우 진행상황 종료 CHK가 0인경우 삭제-->
        public void Update_FLPRSEVENTEnd(Hashtable conditions)
        {
            dao.Update_FLPRSEVENTEnd(conditions);
        }
        #endregion

        #region @@@@@@@@@@@@@@@@@@@@@@@@@@유량@@@@@@@@@@@@@@@@@@@@@@@@@@

        #region 음수 OR 0
        //<!--유량계측 음수판단-->
        public DataTable Select_BlockFlowSign(Hashtable conditions)
        {
            return dao.Select_BlockFlowSign(conditions);
        } 
        #endregion

        #region 15%
        //<!--유량 15%증가 이벤트 여부 체크-->
        public DataTable Select_BlockFlow15Up(Hashtable conditions)
        {
            return dao.Select_BlockFlow15Up(conditions);
        }

        //<!--유량 15%증가 기존 이벤트 업데이트-->
        public void Update_BlockFlow15Up(Hashtable conditions)
        {
            dao.Update_BlockFlow15Up(conditions);
        }

        //<!--유량 15%증가 기존이벤트+새로들어온데이터 확인-->
        public DataTable Select_BlockFlow15UpAddCheck(Hashtable conditions)
        {
            return dao.Select_BlockFlow15UpAddCheck(conditions);
        }
        #endregion

        #region 증감
        //<!--유량 증감 이벤트 체크-->
        public DataTable Select_BlockFlowUpDown(Hashtable conditions)
        {
            return dao.Select_BlockFlowUpDown(conditions);
        }

        //<!--유량 증감 기존이벤트+새로들어온데이터 확인-->
        public DataTable Select_BlockFlowUpDownAddCheck(Hashtable conditions)
        {
            return dao.Select_BlockFlowUpDownAddCheck(conditions);
        }

        //<!--유량 증감 기존 이벤트 업데이트-->
        public void Update_BlockFlowUpDown(Hashtable conditions)
        {
            dao.Update_BlockFlowUpDown(conditions);
        }
        #endregion

        #endregion

        #region @@@@@@@@@@@@@@@@@@@@@@@@@@수압@@@@@@@@@@@@@@@@@@@@@@@@@@

        #region 임계치
        //<!--수압 임계치 이벤트-->
        public DataTable Select_BlockPRSSign(Hashtable conditions)
        {
            return dao.Select_BlockPRSSign(conditions);
        }
        #endregion

        #region 15%
        //<!--수압 15% 증가 이벤트-->
        public DataTable Select_BlockPRS15Up(Hashtable conditions)
        {
            return dao.Select_BlockPRS15Up(conditions);
        }

        //<!--수압 15%증가 기존이벤트 확인-->
        public DataTable Select_BlockPRS15UpAddCheck(Hashtable conditions)
        {
            return dao.Select_BlockPRS15UpAddCheck(conditions);
        }

        //<!--수압 15% 이벤트 업데이트-->
        public void Update_BlockPRS15UpCNT(Hashtable conditions)
        {
            dao.Update_BlockPRS15UpCNT(conditions);
        }
        #endregion

        #region 증감
        //<!--수압 증감 이벤트-->
        public DataTable Select_BlockPRSUpDown(Hashtable conditions)
        {
            return dao.Select_BlockPRSUpDown(conditions);
        }

        //<!--수압 증감 이벤트 지속 확인-->
        public DataTable Select_BlockPRSUpDownAddCheck(Hashtable conditions)
        {
            return dao.Select_BlockPRSUpDownAddCheck(conditions);
        }

        //<!--수압 증감 기존 이벤트 업데이트-->
        public void Update_BlockPRSUpDownCNT(Hashtable conditions)
        {
            dao.Update_BlockPRSUpDownCNT(conditions);
        }
        #endregion

        #endregion
    }
}
