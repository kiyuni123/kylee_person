﻿using epanet2Dev;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace epanet3Dev
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow:Window
    {
        private const int EN_ELEVATION = 0;    ///Node parameters
        private const int EN_BASEDEMAND = 1;
        private const int EN_PATTERN = 2;
        private const int EN_EMITTER = 3;
        private const int EN_INITQUAL = 4;
        private const int EN_SOURCEQUAL = 5;
        private const int EN_SOURCEPAT = 6;
        private const int EN_SOURCETYPE = 7;
        private const int EN_TANKLEVEL = 8;
        private const int EN_DEMAND = 9;
        private const int EN_HEAD = 10;
        private const int EN_PRESSURE = 11;
        private const int EN_QUALITY = 12;
        private const int EN_SOURCEMASS = 13;
        private const int EN_INITVOLUME = 14;
        private const int EN_MIXMODEL = 15;
        private const int EN_MIXZONEVOL = 16;

        private const int EN_TANKDIAM = 17;
        private const int EN_MINVOLUME = 18;
        private const int EN_VOLCURVE = 19;
        private const int EN_MINLEVEL = 20;
        private const int EN_MAXLEVEL = 21;
        private const int EN_MIXFRACTION = 22;
        private const int EN_TANK_KBULK = 23;

        private const int EN_DIAMETER = 0;    ///Link parameters
        private const int EN_LENGTH = 1;
        private const int EN_ROUGHNESS = 2;
        private const int EN_MINORLOSS = 3;
        private const int EN_INITSTATUS = 4;
        private const int EN_INITSETTING = 5;
        private const int EN_KBULK = 6;
        private const int EN_KWALL = 7;
        private const int EN_FLOW = 8;
        private const int EN_VELOCITY = 9;
        private const int EN_HEADLOSS = 10;
        private const int EN_STATUS = 11;
        private const int EN_SETTING = 12;
        private const int EN_ENERGY = 13;

        private const int EN_DURATION = 0;    ///Time parameters
        private const int EN_HYDSTEP = 1;
        private const int EN_QUALSTEP = 2;
        private const int EN_PATTERNSTEP = 3;
        private const int EN_PATTERNSTART = 4;
        private const int EN_REPORTSTEP = 5;
        private const int EN_REPORTSTART = 6;
        private const int EN_RULESTEP = 7;
        private const int EN_STATISTIC = 8;
        private const int EN_PERIODS = 9;

        private const int EN_NODECOUNT = 0;    ///Component counts
        private const int EN_TANKCOUNT = 1;
        private const int EN_LINKCOUNT = 2;
        private const int EN_PATCOUNT = 3;
        private const int EN_CURVECOUNT = 4;
        private const int EN_CONTROLCOUNT = 5;

        private const int EN_JUNCTION = 0;    ///Node types
        private const int EN_RESERVOIR = 1;
        private const int EN_TANK = 2;

        private const int EN_CVPIPE = 0;    ///Link types
        private const int EN_PIPE = 1;
        private const int EN_PUMP = 2;
        private const int EN_PRV = 3;
        private const int EN_PSV = 4;
        private const int EN_PBV = 5;
        private const int EN_FCV = 6;
        private const int EN_TCV = 7;
        private const int EN_GPV = 8;

        private const int EN_NONE = 0;   ///Quality analysis types
        private const int EN_CHEM = 1;
        private const int EN_AGE = 2;
        private const int EN_TRACE = 3;

        private const int EN_CONCEN = 0;    ///Source quality types
        private const int EN_MASS = 1;
        private const int EN_SETPOINT = 2;
        private const int EN_FLOWPACED = 3;

        private const int EN_CFS = 0;    ///Flow units types
        private const int EN_GPM = 1;
        private const int EN_MGD = 2;
        private const int EN_IMGD = 3;
        private const int EN_AFD = 4;
        private const int EN_LPS = 5;
        private const int EN_LPM = 6;
        private const int EN_MLD = 7;
        private const int EN_CMH = 8;
        private const int EN_CMD = 9;

        private const int EN_TRIALS = 0;   ///Misc. options             
        private const int EN_ACCURACY = 1;
        private const int EN_TOLERANCE = 2;
        private const int EN_EMITEXPON = 3;
        private const int EN_DEMANDMULT = 4;

        private const int EN_LOWLEVEL = 0;   ///Control types             
        private const int EN_HILEVEL = 1;
        private const int EN_TIMER = 2;
        private const int EN_TIMEOFDAY = 3;

        private const int EN_AVERAGE = 1;   ///Time statistic types.     
        private const int EN_MINIMUM = 2;
        private const int EN_MAXIMUM = 3;
        private const int EN_RANGE = 4;

        private const int EN_MIX1 = 0;   ///Tank mixing models        
        private const int EN_MIX2 = 1;
        private const int EN_FIFO = 2;
        private const int EN_LIFO = 3;

        private const int EN_NOSAVE = 0;   ///Save-results-to-file flag 
        private const int EN_SAVE = 1;
        private const int EN_INITFLOW = 10;  ///Re-initialize flow flag

        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();

            openFile.Filter = "Input file (*.INP)|*.INP|Network file (*.NET)|*.NET";

            if(openFile.ShowDialog() == true)
            {
                textBox.Text = openFile.FileName.ToString().Trim();
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if(textBox.Text.Trim().Length != 0)
            {
                string strINPFileName = textBox.Text.Trim();
                string strRPTFileName = "rptResult.rpt";
                string strCurrentDirectoryPath = AppDomain.CurrentDomain.BaseDirectory.ToString();

                int iAnalysisType = 1;
                string strAnalysisFlag = "H";   //H:수리해석, Q:수질해석

                Hashtable htanalysisResult = new Hashtable();


                listBox.Items.Add("======================= start======================= ");

                if(!AnalysisErrorHandling(epanet2Method.ENopen(new StringBuilder(strINPFileName), new StringBuilder(strCurrentDirectoryPath + strRPTFileName), new StringBuilder(""))))
                {
                    return;
                }

                if(!AnalysisErrorHandling(epanet2Method.ENopenH()))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    //AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                    AnalysisErrorHandling(epanet2Method.ENclose());
                    return;
                }

                if(!AnalysisErrorHandling(epanet2Method.ENinitH(0)))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(epanet2Method.ENcloseH());
                    AnalysisErrorHandling(epanet2Method.ENclose());
                    return;
                }

                long t = 0;

                if(iAnalysisType == 0)
                {
                    //주어진 시간만큼 관망해석 실행

                    long tstep = 0;
                    long hydStep = 0;
                    long duration = 0;

                    long reportStep = 0;

                    if(!AnalysisErrorHandling(epanet2Method.ENgettimeparam(EN_HYDSTEP, ref hydStep)))
                    {
                        //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                        AnalysisErrorHandling(epanet2Method.ENcloseH());
                        AnalysisErrorHandling(epanet2Method.ENclose());

                        return;
                    }

                    if(!AnalysisErrorHandling(epanet2Method.ENgettimeparam(EN_DURATION, ref duration)))
                    {
                        //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                        AnalysisErrorHandling(epanet2Method.ENcloseH());
                        AnalysisErrorHandling(epanet2Method.ENclose());

                        return;
                    }

                    if(!AnalysisErrorHandling(epanet2Method.ENgettimeparam(EN_REPORTSTEP, ref reportStep)))
                    {
                        //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                        AnalysisErrorHandling(epanet2Method.ENcloseH());
                        AnalysisErrorHandling(epanet2Method.ENclose());

                        return;
                    }

                    do
                    {
                        if(!AnalysisErrorHandling(epanet2Method.ENrunH(ref t)))
                        {
                            //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                            AnalysisErrorHandling(epanet2Method.ENcloseH());
                            AnalysisErrorHandling(epanet2Method.ENclose());

                            return;
                        }

                        //간혹 step에 맞지않는 시간이 도출되는 경우가 있을때를 필터링 (이유는 불명)
                        //if ((t % hydStep) == 0)
                        if((t % reportStep) == 0)
                        {
                            Hashtable timeResult = new Hashtable();
                            //Console.WriteLine("******************************* 시간 : " + (Convert.ToString(t / 60 / 60)).PadLeft(2, '0') + ":" + Convert.ToString((t - (t / 60 / 60 * 60 * 60)) / 60).PadLeft(2, '0'));

                            timeResult.Add("node", ExtractNodeAnalysisResult());
                            timeResult.Add("link", ExtractLinkAnalysisResult());

                            //해석시간을 key로 하여 node,junction 해석결과를 저장
                            htanalysisResult.Add((Convert.ToString(t / 60 / 60)).PadLeft(2, '0') + ":" + Convert.ToString((t - (t / 60 / 60 * 60 * 60)) / 60).PadLeft(2, '0'), timeResult);
                        }

                        if(!AnalysisErrorHandling(epanet2Method.ENnextH(ref tstep)))
                        {
                            //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                            AnalysisErrorHandling(epanet2Method.ENcloseH());
                            AnalysisErrorHandling(epanet2Method.ENclose());

                            return;
                        }
                    }
                    while(tstep > 0);
                }
                else if(iAnalysisType == 1)
                {
                    //관망해석 1회 실행
                    if(!AnalysisErrorHandling(epanet2Method.ENrunH(ref t)))
                    {
                        //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                        AnalysisErrorHandling(epanet2Method.ENcloseH());
                        AnalysisErrorHandling(epanet2Method.ENclose());

                        return;
                    }

                    Hashtable timeResult = new Hashtable();

                    timeResult.Add("node", ExtractNodeAnalysisResult());
                    timeResult.Add("link", ExtractLinkAnalysisResult());

                    htanalysisResult.Add("00:00", timeResult);
                }

                if(!AnalysisErrorHandling(epanet2Method.ENcloseH()))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(epanet2Method.ENclose());
                    return;
                }

                if(!AnalysisErrorHandling(epanet2Method.ENclose()))
                {
                    return;
                }

                listBox.Items.Add("======================= Complete Execute======================= ");


            }

        }

        private Hashtable ExtractLinkAnalysisResult()
        {
            //Node 해석결과 발췌

            //전체 Link 해석결과를 저장할 Hashtable
            Hashtable linkResult = new Hashtable();

            ArrayList cvpipeResult = new ArrayList();              //Pipe with Check Valve
            ArrayList pipeResult = new ArrayList();                //pipe
            ArrayList pumpResult = new ArrayList();                //pump
            ArrayList prvResult = new ArrayList();                 //Pressure Reducing Valve
            ArrayList psvResult = new ArrayList();                 //Pressure Sustaining Valve
            ArrayList pbvResult = new ArrayList();                 //Pressure Breaker Valve
            ArrayList fcvResult = new ArrayList();                 //Flow Control Valve
            ArrayList tcvResult = new ArrayList();                 //Throttle Control Valve
            ArrayList gpvResult = new ArrayList();                 //General Purpose Valve

            //Link Count 발췌
            int linkCount = 0;
            epanet2Method.ENgetcount(EN_LINKCOUNT, ref linkCount);

            //Link 해석결과 변수
            float diameterValue = 0;
            float lengthValue = 0;
            float roughnessValue = 0;
            float minorlossValue = 0;
            float initstatusValue = 0;
            float initsettingValue = 0;
            float kbulkValue = 0;
            float kwallValue = 0;
            float flowValue = 0;
            float velocityValue = 0;
            float headlossValue = 0;
            float statusValue = 0;
            float settingValue = 0;
            float energyValue = 0;

            for(int i = 1; i <= linkCount; i++)
            {
                StringBuilder linkId = new StringBuilder();

                int linkType = 0;

                epanet2Method.ENgetlinkid(i, linkId);
                epanet2Method.ENgetlinktype(i, ref linkType);

                epanet2Method.ENgetlinkvalue(i, EN_DIAMETER, ref diameterValue);
                epanet2Method.ENgetlinkvalue(i, EN_LENGTH, ref lengthValue);
                epanet2Method.ENgetlinkvalue(i, EN_ROUGHNESS, ref roughnessValue);
                epanet2Method.ENgetlinkvalue(i, EN_MINORLOSS, ref minorlossValue);
                epanet2Method.ENgetlinkvalue(i, EN_INITSTATUS, ref initstatusValue);
                epanet2Method.ENgetlinkvalue(i, EN_INITSETTING, ref initsettingValue);
                epanet2Method.ENgetlinkvalue(i, EN_KBULK, ref kbulkValue);
                epanet2Method.ENgetlinkvalue(i, EN_KWALL, ref kwallValue);
                epanet2Method.ENgetlinkvalue(i, EN_FLOW, ref flowValue);
                epanet2Method.ENgetlinkvalue(i, EN_VELOCITY, ref velocityValue);
                epanet2Method.ENgetlinkvalue(i, EN_HEADLOSS, ref headlossValue);
                epanet2Method.ENgetlinkvalue(i, EN_STATUS, ref statusValue);
                epanet2Method.ENgetlinkvalue(i, EN_SETTING, ref settingValue);
                epanet2Method.ENgetlinkvalue(i, EN_ENERGY, ref energyValue);


                Hashtable tmpResult = new Hashtable();

                //소수점 3자리까지 라운딩 (DB는 저장하기 전 라운딩) KH$&0000001
                tmpResult.Add("LINK_ID", linkId.ToString());
                tmpResult.Add("EN_DIAMETER", Math.Round(diameterValue, 3));
                tmpResult.Add("EN_LENGTH", Math.Round(lengthValue, 3));
                tmpResult.Add("EN_ROUGHNESS", Math.Round(roughnessValue, 3));
                tmpResult.Add("EN_MINORLOSS", Math.Round(minorlossValue, 3));
                tmpResult.Add("EN_INITSTATUS", Math.Round(initstatusValue, 3));
                tmpResult.Add("EN_INITSETTING", Math.Round(initsettingValue, 3));
                tmpResult.Add("EN_KBULK", Math.Round(kbulkValue, 3));
                tmpResult.Add("EN_KWALL", Math.Round(kwallValue, 3));
                tmpResult.Add("EN_FLOW", Math.Round(flowValue, 3));
                tmpResult.Add("EN_VELOCITY", Math.Round(velocityValue, 3));
                tmpResult.Add("EN_HEADLOSS", Math.Round(headlossValue, 3));
                tmpResult.Add("EN_STATUS", Math.Round(statusValue, 3));
                tmpResult.Add("EN_SETTING", Math.Round(settingValue, 3));
                tmpResult.Add("EN_ENERGY", Math.Round(energyValue, 3));

                //Console.WriteLine(linkId + "\t\t" + flowValue + "\t\t" + velocityValue + "\t\t" + headlossValue + "\t\t" + statusValue + "\t\t" + energyValue);

                //Type에 따라 구분해서 저장한다.
                if(linkType == EN_CVPIPE)
                {
                    cvpipeResult.Add(tmpResult);
                }
                else if(linkType == EN_PIPE)
                {
                    pipeResult.Add(tmpResult);
                }
                else if(linkType == EN_PUMP)
                {
                    pumpResult.Add(tmpResult);
                }
                else if(linkType == EN_PRV)
                {
                    prvResult.Add(tmpResult);
                }
                else if(linkType == EN_PSV)
                {
                    psvResult.Add(tmpResult);
                }
                else if(linkType == EN_PBV)
                {
                    pbvResult.Add(tmpResult);
                }
                else if(linkType == EN_FCV)
                {
                    fcvResult.Add(tmpResult);
                }
                else if(linkType == EN_TCV)
                {
                    tcvResult.Add(tmpResult);
                }
                else if(linkType == EN_GPV)
                {
                    gpvResult.Add(tmpResult);
                }
            }

            linkResult.Add("cvPipe", cvpipeResult);
            linkResult.Add("pipe", pipeResult);
            linkResult.Add("pump", pumpResult);
            linkResult.Add("prv", prvResult);
            linkResult.Add("psv", psvResult);
            linkResult.Add("pbv", pbvResult);
            linkResult.Add("fcv", fcvResult);
            linkResult.Add("tcv", tcvResult);
            linkResult.Add("gpv", gpvResult);

            return linkResult;
        }

        private Hashtable ExtractNodeAnalysisResult()
        {
            //Node 해석결과 발췌

            //전체 node 해석결과를 저장할 Hashtable
            Hashtable nodeResult = new Hashtable();

            ArrayList junctionResult = new ArrayList();            //절점 해석결과
            ArrayList reserviorResult = new ArrayList();           //배수지 해석결과
            ArrayList tankResult = new ArrayList();                //tank 해석결과

            //Node Count 발췌
            int nodeCount = 0;
            epanet2Method.ENgetcount(EN_NODECOUNT, ref nodeCount);

            //Node 해석결과 변수
            float elevationValue = 0;
            float basedemandValue = 0;
            float patternValue = 0;
            float emitterValue = 0;
            float initqualValue = 0;
            float sourcequalValue = 0;
            float sourcepatValue = 0;
            float sourcetypeValue = 0;
            float tanklevelValue = 0;
            float demandValue = 0;
            float headValue = 0;
            float pressureValue = 0;
            float qualityValue = 0;
            float sourcemassValue = 0;
            float initvolumeValue = 0;
            float mixmodelValue = 0;
            float mixzonevolValue = 0;
            float tankdiamValue = 0;
            float minvolumeValue = 0;
            float volcurveValue = 0;
            float minlevelValue = 0;
            float maxlevelValue = 0;
            float mixfractionValue = 0;
            float tankKbulkValue = 0;

            for(int i = 1; i <= nodeCount; i++)
            {
                StringBuilder nodeId = new StringBuilder();

                int nodeType = 0;

                epanet2Method.ENgetnodeid(i, nodeId);
                epanet2Method.ENgetnodetype(i, ref nodeType);

                epanet2Method.ENgetnodevalue(i, EN_ELEVATION, ref elevationValue);
                epanet2Method.ENgetnodevalue(i, EN_BASEDEMAND, ref basedemandValue);
                epanet2Method.ENgetnodevalue(i, EN_PATTERN, ref patternValue);
                epanet2Method.ENgetnodevalue(i, EN_EMITTER, ref emitterValue);
                epanet2Method.ENgetnodevalue(i, EN_INITQUAL, ref initqualValue);
                epanet2Method.ENgetnodevalue(i, EN_SOURCEQUAL, ref sourcequalValue);
                epanet2Method.ENgetnodevalue(i, EN_SOURCEPAT, ref sourcepatValue);
                epanet2Method.ENgetnodevalue(i, EN_SOURCETYPE, ref sourcetypeValue);
                epanet2Method.ENgetnodevalue(i, EN_TANKLEVEL, ref tanklevelValue);
                epanet2Method.ENgetnodevalue(i, EN_DEMAND, ref demandValue);
                epanet2Method.ENgetnodevalue(i, EN_HEAD, ref headValue);
                epanet2Method.ENgetnodevalue(i, EN_PRESSURE, ref pressureValue);
                epanet2Method.ENgetnodevalue(i, EN_QUALITY, ref qualityValue);
                epanet2Method.ENgetnodevalue(i, EN_SOURCEMASS, ref sourcemassValue);
                epanet2Method.ENgetnodevalue(i, EN_INITVOLUME, ref initvolumeValue);
                epanet2Method.ENgetnodevalue(i, EN_MIXMODEL, ref mixmodelValue);
                epanet2Method.ENgetnodevalue(i, EN_MIXZONEVOL, ref mixzonevolValue);
                epanet2Method.ENgetnodevalue(i, EN_TANKDIAM, ref tankdiamValue);
                epanet2Method.ENgetnodevalue(i, EN_MINVOLUME, ref minvolumeValue);
                epanet2Method.ENgetnodevalue(i, EN_VOLCURVE, ref volcurveValue);
                epanet2Method.ENgetnodevalue(i, EN_MINLEVEL, ref minlevelValue);
                epanet2Method.ENgetnodevalue(i, EN_MAXLEVEL, ref maxlevelValue);
                epanet2Method.ENgetnodevalue(i, EN_MIXFRACTION, ref mixfractionValue);
                epanet2Method.ENgetnodevalue(i, EN_TANK_KBULK, ref tankKbulkValue);

                Hashtable tmpResult = new Hashtable();

                //소수점 3자리까지 라운딩 (DB는 저장하기 전 라운딩) KH$&0000001
                tmpResult.Add("NODE_ID", nodeId.ToString());
                tmpResult.Add("EN_ELEVATION", Math.Round(elevationValue, 3));
                tmpResult.Add("EN_BASEDEMAND", Math.Round(basedemandValue, 3));
                tmpResult.Add("EN_PATTERN", Math.Round(patternValue, 3));
                tmpResult.Add("EN_EMITTER", Math.Round(emitterValue, 3));
                tmpResult.Add("EN_INITQUAL", Math.Round(initqualValue, 3));
                tmpResult.Add("EN_SOURCEQUAL", Math.Round(sourcequalValue, 3));
                tmpResult.Add("EN_SOURCEPAT", Math.Round(sourcepatValue, 3));
                tmpResult.Add("EN_SOURCETYPE", Math.Round(sourcetypeValue, 3));
                tmpResult.Add("EN_TANKLEVEL", Math.Round(tanklevelValue, 3));
                tmpResult.Add("EN_DEMAND", Math.Round(demandValue, 3));
                tmpResult.Add("EN_HEAD", Math.Round(headValue, 3));
                tmpResult.Add("EN_PRESSURE", Math.Round(pressureValue, 3));
                tmpResult.Add("EN_QUALITY", Math.Round(qualityValue, 3));
                tmpResult.Add("EN_SOURCEMASS", Math.Round(sourcemassValue, 3));
                tmpResult.Add("EN_INITVOLUME", Math.Round(initvolumeValue, 3));
                tmpResult.Add("EN_MIXMODEL", Math.Round(mixmodelValue, 3));
                tmpResult.Add("EN_MIXZONEVOL", Math.Round(mixzonevolValue, 3));
                tmpResult.Add("EN_TANKDIAM", Math.Round(tankdiamValue, 3));
                tmpResult.Add("EN_MINVOLUME", Math.Round(minvolumeValue, 3));
                tmpResult.Add("EN_VOLCURVE", Math.Round(volcurveValue, 3));
                tmpResult.Add("EN_MINLEVEL", Math.Round(minlevelValue, 3));
                tmpResult.Add("EN_MAXLEVEL", Math.Round(maxlevelValue, 3));
                tmpResult.Add("EN_MIXFRACTION", Math.Round(mixfractionValue, 3));
                tmpResult.Add("EN_TANK_KBULK", Math.Round(tankKbulkValue, 3));

                //Console.WriteLine("node : " + nodeId + " : " + Math.Round(qualityValue, 3));

                //Type에 따라 구분해서 저장한다.
                if(nodeType == EN_JUNCTION)
                {
                    //절점해석결과
                    junctionResult.Add(tmpResult);
                }
                else if(nodeType == EN_RESERVOIR)
                {
                    //배수지해석결과
                    reserviorResult.Add(tmpResult);
                }
                else if(nodeType == EN_TANK)
                {
                    //탱크해석결과
                    tankResult.Add(tmpResult);
                }
            }

            nodeResult.Add("junction", junctionResult);
            nodeResult.Add("reservior", reserviorResult);
            nodeResult.Add("tank", tankResult);

            return nodeResult;
        }

        private bool AnalysisErrorHandling(int errorCode)
        {
            bool result = true;

            if(errorCode > 10)
            {
                result = false;

                listBox.Items.Add("해석 오류 : " + errorCode);

            }

            return result;
        }
    }
}
