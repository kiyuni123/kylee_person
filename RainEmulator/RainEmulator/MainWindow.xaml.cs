﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RainEmulator
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        SerialPort sPort = new SerialPort("COM1");
        Random random = new Random();
        int intRain;

        Thread thread;
        System.Timers.Timer timer;
        DateTime nowdt = new DateTime();

        int linenum = 1;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;

            SerialConnSetting();

            if (!sPort.IsOpen)
            {
                sPort.Open();

                if (sPort.IsOpen)
                {
                    ConsolWrite("COM1 Port 연결 성공");

                    thread = new Thread(new ThreadStart(threadFX));
                    thread.Start();
                }
                else
                {
                    ConsolWrite("COM1 Port 연결 실패");
                }
            }
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        //RCU890 연결설정
        private void SerialConnSetting()
        {
            sPort.PortName = "COM1";
            sPort.BaudRate = 9600;
            sPort.DataBits = 8;
            sPort.StopBits = StopBits.One;
            sPort.Parity = Parity.None;
            sPort.Handshake = Handshake.None;
            sPort.Encoding = Encoding.Default;
            sPort.RtsEnable = true;
            sPort.DataReceived += SPort_DataReceived;
        }

        /// <summary>
        /// 수신받은 데이터
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            
        }

        /// <summary>
        /// 리치박스에 쓰기
        /// </summary>
        /// <param name="strtxt"></param>
        public void ConsolWrite(string strtxt)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal,
                        new Action((delegate ()
                        {
                            richBox.Document.Blocks.Add(new Paragraph(new Run("[" + linenum.ToString() + "]" + strtxt)));
                            richBox.ScrollToEnd();
                            linenum++;

                            if (richBox.Document.Blocks.Count > 1000)
                            {
                                richBox.Document.Blocks.Remove(richBox.Document.Blocks.FirstBlock);
                            }
                        })));
        }

        private void threadFX()
        {
            //자동 스케줄 작업시간 체크 및 저장을 위한 타이머 생성 및 선언 (1초마다 확인)
            timer = new System.Timers.Timer();
            timer.Interval = 1000 * 1;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            nowdt = DateTime.Now;

            if (nowdt.Second == 00 || nowdt.Second == 10 || nowdt.Second == 20 || nowdt.Second == 30 || nowdt.Second == 40 || nowdt.Second == 50)
            //if(true)
            {
                intRain = random.Next(0, 30);
                sPort.Write(intRain.ToString() + "\r\n");
                ConsolWrite(intRain.ToString());
            }
        }
    }
}
