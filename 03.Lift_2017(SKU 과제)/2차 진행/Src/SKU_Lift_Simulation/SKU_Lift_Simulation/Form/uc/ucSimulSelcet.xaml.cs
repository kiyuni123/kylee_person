﻿using DevExpress.Xpf.Charts;
using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Analysis;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// ucSimulSelcet.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucSimulSelcet:UserControl
    {
        public ucSimulSelcet()
        {
            InitializeComponent();
            InitialAnal();
        }

        private void InitialAnal()
        {
            try
            {
                Lift_Optimal_Operation LOO = new Lift_Optimal_Operation();
                AnalysisWork work = new AnalysisWork();

                DataTable StackBuilding = new DataTable();  //누적 층고 빌딩 정보
                DataTable UseBuilding = new DataTable();    //분석 사용 빌딩 정보
                DataTable UseLift = new DataTable();        //분석 사용 리프트 정보
                DataTable LiftSpec = new DataTable();       //리프트 스펙 조회

                UseBuilding = work.Select_UseBuildingList(null);
                UseLift = work.Select_AnalLiftList(null);
                StackBuilding = work.Select_StackUseBuilding(null);
                LiftSpec = work.Select_LiftSpec(null);

                Hashtable htresult = LOO.LiftOptimal(StackBuilding, UseBuilding, UseLift, LiftSpec);

                if(htresult == null)
                {
                    Messages.ShowInfoMsgBox("시뮬레이션이 실패했습니다.");
                    return;
                }

                DataSet dsresult = new DataSet();
                dsresult = htresult["dsresult"] as DataSet;
                DataSet dsliftresult = new DataSet();
                dsliftresult = htresult["dsliftresult"] as DataSet;

                ChartData(dsresult);
                GridData(dsresult, dsliftresult);


                

                //gdRange.ItemsSource = dsresult.Tables[dsliftresult.Tables.Count - 1];
                //gdResult.ItemsSource = dsliftresult.Tables[dsliftresult.Tables.Count - 1];
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void ChartData(DataSet dsChart)
        {
            try
            {
                int Seriescnt = 0;

                foreach(DataTable table in dsChart.Tables)
                {
                    if(table.Rows.Count > Seriescnt)
                    {
                        Seriescnt = table.Rows.Count;
                    }
                }

                for(int i = 0; i < Seriescnt; i++)
                {
                    CCchart.Diagram.Series.Add(new BarStackedSeries2D()
                    {
                        Name = "Range" + (Seriescnt).ToString(),
                        Model = new BorderlessSimpleBar2DModel(),
                        ArgumentScaleType = ScaleType.Qualitative
                    });
                }

                foreach(DataTable table in dsChart.Tables)
                {
                    for(int i = 0; i < table.Rows.Count; i++)
                    {
                        if(dsChart.Tables.IndexOf(table).ToString().Equals("0"))
                        {
                            CCchart.Diagram.Series[i].Points.Add(new SeriesPoint("Initial", Convert.ToDouble(table.Rows[i]["CYCTIME"])));
                            CCchart.Diagram.Series[i].DisplayName = table.Rows[i]["Range"].ToString();
                            CCchart.Diagram.Series[i].ShowInLegend = true;
                        }
                        else
                        {
                            CCchart.Diagram.Series[i].Points.Add(new SeriesPoint("local" + dsChart.Tables.IndexOf(table).ToString(), Convert.ToDouble(table.Rows[i]["CYCTIME"])));
                            CCchart.Diagram.Series[i].DisplayName = table.Rows[i]["Range"].ToString();
                            CCchart.Diagram.Series[i].ShowInLegend = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void GridData(DataSet dsGridRange, DataSet dsGridLift)
        {
            try
            {   
                for(int i = 0; i < dsGridRange.Tables.Count; i++)
                {
                    string title;

                    controlSumulSelect ct;

                    if(i == 0)
                    {
                        title = "Initial";
                        ct = new controlSumulSelect(dsGridRange.Tables[i], dsGridLift.Tables[i], title) { Margin = new Thickness(5) };
                        ct.Name = title;
                        ct.Height = 200;
                        ct.HorizontalAlignment = HorizontalAlignment.Stretch;
                    }
                    else
                    {
                        title = "local" + i.ToString();
                        ct = new controlSumulSelect(dsGridRange.Tables[i], dsGridLift.Tables[i], title) { Margin = new Thickness(5) };
                        ct.Name = title;
                        ct.Height = 200;
                        ct.HorizontalAlignment = HorizontalAlignment.Stretch;
                    }

                    Stack.Children.Insert(i, ct);           
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
