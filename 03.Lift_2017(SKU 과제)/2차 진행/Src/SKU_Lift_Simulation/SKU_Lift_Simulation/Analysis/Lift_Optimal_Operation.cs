﻿using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Analysis
{
    public class Lift_Optimal_Operation
    {
        AnalysisWork work = new AnalysisWork();

        /// <summary>
        /// 운영모의 로직 ver1
        /// </summary>
        /// <param name="StackBuilding"></param>
        /// <param name="UseBuilding"></param>
        /// <param name="UseBuilding"></param>
        /// <param name="UseLift"></param>
        /// <param name="LiftSpec"></param>
        /// <returns>htresult(dsresult, dsliftresult)</returns>
        public Hashtable LiftOptimal(DataTable StackBuilding, DataTable UseBuilding, DataTable UseLift, DataTable LiftSpec)
        {
            Hashtable htresult = new Hashtable();       //결과값 범위데이터셋, 리프트셋
            DataSet dsresult = new DataSet();           //결과값 loop돌리고 각 loop마다 결과값 저장
            DataSet dsliftresult = new DataSet();       //loop마다 최적 결과 lift제원(운행층수 중요) 저장
            List<double> val = new List<double>();

            DataTable Range = new DataTable();          //범위 리스트 * 중요

            try
            {
                int stopgap = Convert.ToInt32(UseBuilding.Rows[0]["B_Flgap"].ToString());  //정차층수(층)
                int intime = Convert.ToInt32(UseBuilding.Rows[0]["B_Intime"].ToString());  //탑승시간(sec)
                int outtime = Convert.ToInt32(UseBuilding.Rows[0]["B_Outtime"].ToString()); //하차시간(sec)

                int coverfl = stopgap / 2; //커버층수 (stopgap/2)

                //B_No seq, B_Name 빌딩이름, B_Min 최처층, B_Max 최고층, B_Area 면적, B_Intime 탑승시간, B_OutTime 하차시간, B_Flgap 정차 층수
                #region ######↓ 여기까지 사용자 입력 데이터 (분석에 필요한 기초 데이터)
                if(UseBuilding.Rows.Count != 1)
                {
                    Messages.ShowInfoMsgBox("사용되는 빌딩이 없습니다.");
                    return null;
                }

                if(UseLift.Rows.Count == 0)
                {
                    Messages.ShowInfoMsgBox("사용되는 리프트가 없습니다.");
                    return null;
                }
                #endregion

                //분석 함수 호출 Initial
                Range = SimulAnal(Range, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                Range.TableName = "Initial";
                dsresult.Tables.Add(Range.Copy());
                UseLift.TableName = "Initial";
                dsliftresult.Tables.Add(UseLift.Copy());

                #region ######↓ Range 추출 For문
                DataTable simultemp = new DataTable();
                DataSet dssimultemp;

                Hashtable resulttemp = new Hashtable();
                int simulcnt = 1;
                int localcnt = 1;
                int cyctime_minkey = 1;

                while(true)
                {
                    dssimultemp = null;
                    dssimultemp = new DataSet();
                    resulttemp.Clear();
                    simulcnt = 1;

                    #region 리프트 운행층수 min -5, max +5 번갈아 가면서 표본 및 분석
                    for(int j = 0; j < UseLift.Rows.Count; j++)
                    {
                        for(int i = 4; i < 6; i++)
                        {
                            if(i == 4)
                            {
                                if(Convert.ToInt32(StackBuilding.Compute("MIN(FL)", "1=1")) < Convert.ToInt32(UseLift.Rows[j][i])-5)
                                {
                                    int inttemp = Convert.ToInt32(UseLift.Rows[j][i]);
                                    UseLift.Rows[j][i] = inttemp - 5;

                                    simultemp = SimulAnal(simultemp, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                                    resulttemp.Add(simulcnt.ToString(), UseLift.Copy());
                                    simultemp.TableName = simulcnt.ToString();
                                    dssimultemp.Tables.Add(simultemp);

                                    simulcnt = simulcnt + 1;
                                    simultemp = null;
                                    simultemp = new DataTable();
                                    UseLift.Rows[j][i] = inttemp;
                                }
                                if(Convert.ToInt32(StackBuilding.Compute("MAX(FL)", "1=1")) >= Convert.ToInt32(UseLift.Rows[j][i]) + 5)
                                {
                                    int inttemp = Convert.ToInt32(UseLift.Rows[j][i]);
                                    UseLift.Rows[j][i] = inttemp + 5;

                                    simultemp = SimulAnal(simultemp, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                                    resulttemp.Add(simulcnt.ToString(), UseLift.Copy());
                                    simultemp.TableName = simulcnt.ToString();
                                    dssimultemp.Tables.Add(simultemp);

                                    simulcnt = simulcnt + 1;
                                    simultemp = null;
                                    simultemp = new DataTable();
                                    UseLift.Rows[j][i] = inttemp;
                                }
                            }
                            if(i == 5)
                            {
                                if(Convert.ToInt32(StackBuilding.Compute("MIN(FL)", "1=1")) < Convert.ToInt32(UseLift.Rows[j][i]) - 5)
                                {
                                    int inttemp = Convert.ToInt32(UseLift.Rows[j][i]);
                                    UseLift.Rows[j][i] = inttemp - 5;

                                    simultemp = SimulAnal(simultemp, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                                    resulttemp.Add(simulcnt.ToString(), UseLift.Copy());
                                    simultemp.TableName = simulcnt.ToString();
                                    dssimultemp.Tables.Add(simultemp);

                                    simulcnt = simulcnt + 1;
                                    simultemp = null;
                                    simultemp = new DataTable();
                                    UseLift.Rows[j][i] = inttemp;
                                }
                                if(Convert.ToInt32(StackBuilding.Compute("MAX(FL)", "1=1")) >= Convert.ToInt32(UseLift.Rows[j][i]) + 5)
                                {
                                    int inttemp = Convert.ToInt32(UseLift.Rows[j][i]);
                                    UseLift.Rows[j][i] = inttemp + 5;

                                    simultemp = SimulAnal(simultemp, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                                    resulttemp.Add(simulcnt.ToString(), UseLift.Copy());
                                    simultemp.TableName = simulcnt.ToString();
                                    dssimultemp.Tables.Add(simultemp);

                                    simulcnt = simulcnt + 1;
                                    simultemp = null;
                                    simultemp = new DataTable();
                                    UseLift.Rows[j][i] = inttemp;
                                }
                            }
                        }
                    }
                    #endregion

                    //작은값 선정 같은 값일 경우 작은값중 제일 위값
                    foreach(DataTable table in dssimultemp.Tables)
                    {
                        if(Convert.ToDouble(dssimultemp.Tables[cyctime_minkey - 1].Compute("SUM(CYCTIME)", "")) > Convert.ToDouble(table.Compute("SUM(CYCTIME)", "")))
                        {
                            cyctime_minkey = Convert.ToInt32(table.TableName);
                        }
                    }

                    val.Add(Convert.ToDouble(dssimultemp.Tables[cyctime_minkey - 1].Compute("SUM(CYCTIME)", "")) - Convert.ToDouble(dsresult.Tables[localcnt - 1].Compute("SUM(CYCTIME)", "")));

                    if(Convert.ToDouble(dssimultemp.Tables[cyctime_minkey - 1].Compute("SUM(CYCTIME)", "")) - Convert.ToDouble(dsresult.Tables[localcnt - 1].Compute("SUM(CYCTIME)", "")) >= 0)
                    {
                        DataTable temp = new DataTable();
                        temp = dssimultemp.Tables[cyctime_minkey - 1].Copy();
                        temp.TableName = localcnt.ToString();
                        dsresult.Tables.Add(temp);
                        UseLift = (DataTable)resulttemp[cyctime_minkey.ToString()];
                        UseLift.TableName = localcnt.ToString();
                        dsliftresult.Tables.Add(UseLift.Copy());
                        break;
                    }
                    else
                    {
                        DataTable temp = new DataTable();
                        temp = dssimultemp.Tables[cyctime_minkey - 1].Copy();
                        temp.TableName = localcnt.ToString();
                        dsresult.Tables.Add(temp);
                        UseLift = (DataTable)resulttemp[cyctime_minkey.ToString()];
                        UseLift.TableName = localcnt.ToString();
                        dsliftresult.Tables.Add(UseLift.Copy());
                        localcnt = localcnt + 1;
                    }
                }         
                #endregion
                          
                htresult.Add("dsresult", dsresult);
                htresult.Add("dsliftresult", dsliftresult);
                htresult.Add("val", val);

                return htresult;
            }
            catch(Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }

        /// <summary>
        /// 분석하고 Range DataTable Return CYCTIME 총합은 TableName
        /// </summary>
        /// <param name="Range"></param>
        /// <param name="StackBuilding"></param>
        /// <param name="UseBuilding"></param>
        /// <param name="UseLift"></param>
        /// <param name="LiftSpec"></param>
        /// <param name="stopgap"></param>
        /// <param name="intime"></param>
        /// <param name="outtime"></param>
        /// <param name="coverfl"></param>
        /// <returns></returns>
        private DataTable SimulAnal(DataTable Range, DataTable StackBuilding, DataTable UseBuilding, DataTable UseLift, DataTable LiftSpec, int stopgap, int intime, int outtime, int coverfl)
        {
            try
            {
                #region######↓ Range DataTable columns 생성
                Range.Columns.Add("Range");        //명
                Range.Columns.Add("MINFL");        //최저층
                Range.Columns.Add("MAXFL");        //최고층
                Range.Columns.Add("STOPCNT");      //정차횟수
                Range.Columns.Add("PEOPLE");       //양중인원
                Range.Columns.Add("LIFTGRP");      //속하는리프트
                Range.Columns.Add("LIFTSPEC");     //리프트스펙종류
                                                   //리프트모델 종류
                foreach(DataRow r in LiftSpec.Rows)
                {
                    Range.Columns.Add(r["M_No"].ToString() + "_CNT");
                    Range.Columns.Add(r["M_No"].ToString() + "_PEOPLE");
                }
                Range.Columns.Add("RUNCNT");       //운행횟수
                Range.Columns.Add("CYCTIME", typeof(double));      //싸이클타임
                #endregion

                #region######↓ Range 추출
                List<int> listRange = new List<int>();
                foreach(DataRow r in UseLift.Rows)
                {
                    if(!listRange.Contains(Convert.ToInt32(r["R_MIN"].ToString())))
                    {
                        listRange.Add(Convert.ToInt32(r["R_MIN"].ToString()));
                    }
                    if(!listRange.Contains(Convert.ToInt32(r["R_MAX"].ToString())))
                    {
                        listRange.Add(Convert.ToInt32(r["R_MAX"].ToString()));
                    }
                }
                listRange.Sort();

                for(int i = 0; i < listRange.Count - 1; i++)
                {
                    DataRow r = Range.NewRow();
                    //처음
                    if(i == 0)
                    {
                        r["Range"] = "Range" + (i + 1).ToString();                                      //Range명
                        r["MINFL"] = listRange[i];                                                      //처음시작은 최초값
                        r["MAXFL"] = (listRange[i + 1] / stopgap) * stopgap + coverfl;                    //몫(다음층/정차층수)*정차층수+커버층수
                        //if(Convert.ToInt32(UseBuilding.Rows[0]["B_MAX"]) < Convert.ToInt32(r["MAXFL"]))
                        //{
                        //    r["MAXFL"] = listRange[listRange.Count - 1];
                        //}
                    }
                    //중간
                    else if(i == listRange.Count - 1)
                    {
                        r["Range"] = "Range" + (i + 1).ToString();                                      //Range명
                        r["MINFL"] = Convert.ToInt32(Range.Rows[i - 1]["MAXFL"].ToString()) + 1;        //앞 Range MAXFL의 + 1
                        r["MAXFL"] = listRange[i];                                                      //몫(다음층/정차층수)*정차층수+커버층수
                    }
                    //마지막
                    else
                    {
                        r["Range"] = "Range" + (i + 1).ToString();                                      //Range명
                        r["MINFL"] = Convert.ToInt32(Range.Rows[i - 1]["MAXFL"].ToString()) + 1;        //앞 Range MAXFL의 + 1
                        r["MAXFL"] = (listRange[i + 1] / stopgap) * stopgap + coverfl;                  //마지막은 끝값
                        //if(Convert.ToInt32(UseBuilding.Rows[0]["B_MAX"]) < Convert.ToInt32(r["MAXFL"]))
                        //{
                        //    r["MAXFL"] = listRange[listRange.Count-1];
                        //}
                    }

                    Range.Rows.Add(r);
                }
                #endregion

                #region######↓ Range 데이터 분석 (STOPCNT, PEOPLE, LIFTGRP, LIFTSPEC, 리프트별 갯수, 용량, RUNCNT, CYCTIME)
                foreach(DataRow r in Range.Rows)
                {
                    #region //(최고층-최저층)+1/정차층수
                    r["STOPCNT"] = ((Convert.ToInt32(r["MAXFL"]) - Convert.ToInt32(r["MINFL"]) + 1)) / stopgap;
                    #endregion

                    #region //최저층 최고층 범위내에 작업자인원
                    r["PEOPLE"] = StackBuilding.Compute("SUM(W_LaborCount)", "FL >= " + r["MINFL"].ToString() + " AND FL <= " + r["MAXFL"].ToString());
                    if(r["PEOPLE"].Equals(DBNull.Value))
                    {
                        r["PEOPLE"] = 0;
                    }
                    #endregion

                    #region //속하는리프트 if(range최저층>=lift최저층 그리고 range최고층<=lift최고층)
                    foreach(DataRow dr in UseLift.Rows)
                    {
                        if(Convert.ToInt32(r["MINFL"]) >= Convert.ToInt32(dr["R_Min"]) && Convert.ToInt32(r["MAXFL"]) <= Convert.ToInt32(dr["R_Max"]))
                        {
                            r["LIFTGRP"] = r["LIFTGRP"] + dr["L_No"].ToString() + ",";
                        }
                    }
                    //속하는리프트 마지막 "," 절삭
                    if(r["LIFTGRP"].ToString().Length != 0)
                    {
                        r["LIFTGRP"] = r["LIFTGRP"].ToString().Substring(0, r["LIFTGRP"].ToString().Length - 1);
                    }
                    #endregion

                    #region //range에 속하는 리프트 스펙 종류
                    r["LIFTSPEC"] = (UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().DefaultView.ToTable(true, "M_No").Rows.Count;
                    #endregion

                    #region //리프트 Spec 종류별 갯수, 용량
                    foreach(DataRow dr in LiftSpec.Rows)
                    {
                        //갯수
                        r[dr["M_No"].ToString() + "_CNT"] = ((UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().Select("M_No IN (" + dr["M_No"].ToString() + ")")).Count();
                        //용량
                        r[dr["M_No"].ToString() + "_PEOPLE"] = ((UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().Select("M_No IN (" + dr["M_No"].ToString() + ")")).Count() * Convert.ToInt32(dr["L_Capa"]);
                    }
                    #endregion

                    #region //운행횟수, CycTime 계산
                    if(Convert.ToInt32(r["LIFTSPEC"]) == 1)
                    {
                        //1. 속해있는 리프트 스펙 조회
                        DataTable dttemp = (UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().DefaultView.ToTable(true, "M_No", "L_Time", "L_Distance", "L_Velocity");

                        //정차가능한최고층높이 구하기
                        double doumaxH;
                        double douTime = Convert.ToDouble(dttemp.Rows[0]["L_Time"]);
                        double douDistance = Convert.ToDouble(dttemp.Rows[0]["L_Distance"]);
                        double douVelocity = Convert.ToDouble(dttemp.Rows[0]["L_Velocity"]);

                        if((Convert.ToInt32(r["MAXFL"]) / stopgap) != 0)
                            doumaxH = Convert.ToDouble(StackBuilding.Select("FL = " + (Convert.ToInt32(r["MAXFL"]) / stopgap) * stopgap)[0]["W_ASEC"]);
                        else
                            doumaxH = Convert.ToDouble(StackBuilding.Select("FL = " + (1).ToString())[0]["W_ASEC"]);

                        if(Convert.ToInt32(r["PEOPLE"]) > 0)
                        {
                            //하차인원이 있는경우 
                            r["RUNCNT"] = (int)Math.Ceiling(Convert.ToDouble(r["PEOPLE"]) / Convert.ToDouble(r[(UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().DefaultView.ToTable(true, "M_No").Rows[0]["M_No"].ToString() + "_PEOPLE"]));
                            r["CYCTIME"] = intime + (Convert.ToInt32(r["STOPCNT"]) * outtime) + 2 * (((Convert.ToInt32(r["STOPCNT"]) + 1) * douTime) + (doumaxH - ((Convert.ToInt32(r["STOPCNT"]) + 1) * douDistance)) / douVelocity);
                            r["CYCTIME"] = Convert.ToInt32(r["CYCTIME"]) * Convert.ToInt32(r["RUNCNT"]);
                        }
                        else
                        {
                            //하차인원이 없는경우 
                            //r["RUNCNT"] = 1;
                            //r["CYCTIME"] = 0 + (0 * outtime) + 2 * (((0 + 1) * douTime) + (doumaxH - ((0 + 1) * douDistance)) / douVelocity);
                            r["RUNCNT"] = 0;
                            r["CYCTIME"] = 0;
                        }
                    }

                    if(Convert.ToInt32(r["LIFTSPEC"]) == 2)
                    {
                        //1. 속해있는 리프트 스펙 조회
                        DataTable dttemp = (UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().DefaultView.ToTable(true, "M_No", "L_Time", "L_Distance", "L_Velocity", "L_Capa");
                        dttemp.Columns.Add("CYCTIME");

                        //정차가능한최고층높이 구하기
                        double doumaxH;

                        if((Convert.ToInt32(r["MAXFL"]) / stopgap) != 0)
                            doumaxH = Convert.ToDouble(StackBuilding.Select("FL = " + (Convert.ToInt32(r["MAXFL"]) / stopgap) * stopgap)[0]["W_ASEC"]);
                        else
                            doumaxH = Convert.ToDouble(StackBuilding.Select("FL = " + (1).ToString())[0]["W_ASEC"]);

                        //속해있는 리프트 스펙의 CycTime 계산
                        foreach(DataRow dr in dttemp.Rows)
                        {
                            double douTime = Convert.ToDouble(dr["L_Time"]);
                            double douDistance = Convert.ToDouble(dr["L_Distance"]);
                            double douVelocity = Convert.ToDouble(dr["L_Velocity"]);

                            dr["CYCTIME"] = Math.Round(intime + (Convert.ToInt32(r["STOPCNT"]) * outtime) + 2 * (((Convert.ToInt32(r["STOPCNT"]) + 1) * douTime) + (doumaxH - ((Convert.ToInt32(r["STOPCNT"]) + 1) * douDistance)) / douVelocity), 2, MidpointRounding.ToEven);
                        }

                        //CYCTIME 계산
                        //필요 인자 생성
                        double a = 0;                                                                          // 1번 리프트 스펙의 N회 싸이클타임
                        double b = 0;                                                                          // 2번 리프트 스펙의 N회 싸이클타임
                        double a0 = Convert.ToDouble(dttemp.Rows[0]["CYCTIME"]);                               // 1번 리프트 스펙의 1회 싸이클타임
                        double b0 = Convert.ToDouble(dttemp.Rows[1]["CYCTIME"]);                               // 2번 리프트 스펙의 1회 싸이클타임
                        int Ca = Convert.ToInt32(r[dttemp.Rows[0]["M_No"].ToString() + "_PEOPLE"]);            // 1번 리프트 스펙의 용량
                        int Cb = Convert.ToInt32(r[dttemp.Rows[1]["M_No"].ToString() + "_PEOPLE"]);            // 2번 리프트 스펙의 용량
                        int R = Convert.ToInt32(r["PEOPLE"]);                                                                             // 양중할 남은 인원
                        double CT = 0;                                                                            // 전체 소요 싸이클타임
                        int Na = 1;                                                                            // 1번 리프트가 사용된 횟수
                        int Nb = 1;                                                                            // 2번 리프트가 사용된 횟수

                        a = a0;
                        b = b0;

                        //시작
                        START:
                        //오른쪽
                        if(a > b)
                        {
                            R = R - Cb;
                            if(Math.Sign(R) == 1)
                            {
                                Nb = Nb + 1;
                                b = Nb * b0;
                                goto START;
                            }
                            else
                            {
                                Nb = Nb - 1;
                                if(Nb!=0)
                                    CT = b * Nb;
                                else
                                    CT = b;

                                goto FINISH;
                            }
                        }
                        //왼쪽
                        else if(a < b)
                        {
                            R = R - Ca;
                            if(Math.Sign(R) == 1)
                            {
                                Na = Na + 1;
                                a = Na * a0;
                                goto START;
                            }
                            else
                            {
                                Na = Na - 1;
                                if(Na != 0)
                                    CT = a * Na;
                                else
                                    CT = a;

                                goto FINISH;
                            }
                        }

                        FINISH:
                        r["CYCTIME"] = CT;
                        r["RUNCNT"] = Na + Nb;
                    }
                    #endregion
                }
                #endregion

                //Range.TableName = Convert.ToDouble(Range.Compute("SUM(CYCTIME)", "")).ToString();

                return Range;
            }
            catch(Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }
    }
}
