﻿using SKU_Lift_Simulation.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Work
{
    class UnitProcessMngWork
    {
        UnitProcessMngDao dao = new UnitProcessMngDao();

        /// <summary>
        /// 단위공정리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_UnitProcessList(Hashtable conditions)
        {
            return dao.Select_UnitProcessList(null);
        }

        /// <summary>
        /// 단위공정리스트 추가
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_UnitProcessList(Hashtable conditions)
        {
            dao.Insert_UnitProcessList(conditions);
        }

        /// <summary>
        /// 단위공정리스트 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_UnitProcessList(Hashtable conditions)
        {
            dao.Delete_UnitProcessList(conditions);
        }

        /// <summary>
        /// 단위공정리스트 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_UnitProcessList(Hashtable conditions)
        {
            dao.Update_UnitProcessList(conditions);
        }
    }
}
