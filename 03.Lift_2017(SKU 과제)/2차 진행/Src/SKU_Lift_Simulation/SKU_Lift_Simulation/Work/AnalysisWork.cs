﻿using SKU_Lift_Simulation.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Work
{
    public class AnalysisWork
    {
        AnalysisDao dao = new AnalysisDao();
        BuildingMngDao BuildingMngdao = new BuildingMngDao();
        WorkPlanMngDao WorkPlanMngdao = new WorkPlanMngDao();
        KindsMngDao KindsMngdao = new KindsMngDao();

        public DataTable Select_UseBuildingList(Hashtable conditions)
        {
            return BuildingMngdao.Select_UseBuildingList(conditions);
        }


        public DataTable Select_AnalLiftList(Hashtable conditions)
        {
            return dao.Select_AnalLiftList(conditions);
        }
        
        //<!--누적 빌딩 정보-->
        public DataTable Select_StackUseBuilding(Hashtable conditions)
        {
            return WorkPlanMngdao.Select_WorkPlanList(conditions);
        }

        //<!--스펙 정보-->
        public DataTable Select_LiftSpec(Hashtable conditions)
        {
            return KindsMngdao.Select_KidsList(conditions);
        }
    }
}
