﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Dao
{
    class KindsMngDao
    {
        /// <summary>
        /// 기종리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_KidsList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_KidsList", conditions);
        }

        /// <summary>
        /// 기종리스트 추가
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_KidsList(Hashtable conditions)
        {
            DBManager.QueryForInsert("Insert_KidsList", conditions);
        }

        /// <summary>
        /// 기종리스트 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_KidsList(Hashtable conditions)
        {
            DBManager.QueryForDelete("Delete_KidsList", conditions);
        }

        /// <summary>
        /// 기종리스트 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_KidsList(Hashtable conditions)
        {
            DBManager.QueryForDelete("Update_KidsList", conditions);
        }
    }
}
