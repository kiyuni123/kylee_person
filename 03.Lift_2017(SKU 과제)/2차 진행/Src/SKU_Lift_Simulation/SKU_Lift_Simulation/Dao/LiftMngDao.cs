﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Dao
{
    class LiftMngDao
    {
        /// <summary>
        /// 기종콤보박스 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ComboKidsList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_ComboKidsList", conditions);
        }

        /// <summary>
        /// 기종콤보박스 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ComboBuildingList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_ComboBuildingList", conditions);
        }

        /// <summary>
        /// 기종 스펙 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ModelSpec(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_ModelSpec", conditions);
        }

        /// <summary>
        /// 리프트리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_LiftList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_LiftList", conditions);
        }

        /// <summary>
        /// 리프트리스트 메인조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_MainLiftList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_MainLiftList", conditions);
        }
        
        /// <summary>
        /// 리프트리스트 추가
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_LiftList(Hashtable conditions)
        {
            DBManager.QueryForInsert("Insert_LiftList", conditions);
        }

        /// <summary>
        /// 리프트리스트 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_LiftList(Hashtable conditions)
        {
            DBManager.QueryForDelete("Delete_LiftList", conditions);
        }

        /// <summary>
        /// 리프트리스트 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_LiftList(Hashtable conditions)
        {
            DBManager.QueryForDelete("Update_LiftList", conditions);
        }
    }
}
