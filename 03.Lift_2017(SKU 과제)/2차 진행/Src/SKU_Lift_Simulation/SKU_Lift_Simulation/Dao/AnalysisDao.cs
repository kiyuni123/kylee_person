﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Dao
{
    class AnalysisDao
    {
        public DataTable Select_AnalLiftList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_AnalLiftList", conditions);
        }
        //<!--누적 빌딩 정보-->
        public DataTable Select_StackUseBuilding(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_StackUseBuilding", conditions);
        }
    }
}
