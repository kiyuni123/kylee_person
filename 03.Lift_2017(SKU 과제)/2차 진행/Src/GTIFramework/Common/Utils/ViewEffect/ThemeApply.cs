﻿using DevExpress.Xpf.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GTIFramework.Common.Utils.ViewEffect
{
    public class ThemeApply
    {
        private static bool bregname = false;

        public static void Themeapply(object obj)
        {
            if (obj != null)
            {
                DependencyObject DO = (DependencyObject)obj;

                Theme theme = new Theme("GTIThemeDark", "DevExpress.Xpf.Themes.GTIThemeDark.v17.1");
                theme.AssemblyName = "DevExpress.Xpf.Themes.GTIThemeDark.v17.1";
                if(!bregname)
                {
                    Theme.RegisterTheme(theme);
                    bregname = true;
                }
                ThemeManager.SetTheme(DO, theme);
            }
        }
    }
}
