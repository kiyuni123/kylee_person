﻿using DevExpress.Xpf.Accordion;
using DevExpress.Xpf.Core;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using SKU_Lift_Simulation.Form.uc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form
{
    /// <summary>
    /// Lift_Simulation_Main.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Lift_Simulation_Main : Window
    {
        private bool c = true;

        public Lift_Simulation_Main()
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);

            ucMain uc = new ucMain();
            tabBinding(uc);
        }

        #region 이벤트
        /// <summary>
        /// 타이틀 마우스 액션(Move)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Title_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
            e.Handled = true;
        }

        /// <summary>
        /// 프로그램 닫기 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            if (DXMessageBox.Show("시스템을 종료합니다.", "SKU Lift Simulation", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// 윈도우 최소화 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Winmin_Click(object sender, RoutedEventArgs e)
        {
            WindowState = System.Windows.WindowState.Minimized;
        }

        /// <summary>
        /// 윈도우 최대화 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Winmax_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
                imgMainmax.Source = new BitmapImage(new Uri("/Resources/Image/ic_Window_Max.png", UriKind.Relative));
            }
            else if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Maximized;
                imgMainmax.Source = new BitmapImage(new Uri("/Resources/Image/ic_Window_Resize.png", UriKind.Relative));
            }
        }

        /// <summary>
        /// 작업계획 저장 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkPlanSave_Click(object sender, RoutedEventArgs e)
        {
            DXMessageBox.Show("작업계획 저장 버튼");
        }

        /// <summary>
        /// 작업계획 초기화 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkPlanInit_Click(object sender, RoutedEventArgs e)
        {
            DXMessageBox.Show("작업계획 초기화 버튼");
        }

        /// <summary>
        /// 모의 시뮬레이션 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Simulations_Click(object sender, RoutedEventArgs e)
        {
            DXMessageBox.Show("모의 시뮬레이션 버튼");
        }

        /// <summary>
        /// 모의 저장버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimulationsInit_Click(object sender, RoutedEventArgs e)
        {
            DXMessageBox.Show("모의 저장버튼");
        }

        /// <summary>
        /// 메뉴 히든/쇼 애니메이션
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuSH_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Storyboard sb;

                if (c)
                {
                    sb = FindResource("gridin") as Storyboard;

                    MenuAcc.CollapseAll();
                    MenuAcc.ExpandItemOnHeaderClick = false;
                }
                else
                {
                    sb = FindResource("gridout") as Storyboard;

                    MenuAcc.ExpandAll();
                    MenuAcc.ExpandItemOnHeaderClick = true;
                }

                sb.Begin(this);
                c = !c;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 메뉴 클릭 이벤트
        /// <summary>
        /// 기종관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KindsMng_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucKindsMng uc = new ucKindsMng();
            tabBinding(uc);
        }

        /// <summary>
        /// 건물관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildingMng_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucBuildingMng uc = new ucBuildingMng();
            tabBinding(uc);
        }

        /// <summary>
        /// 리프트관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LiftMng_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucLiftMng uc = new ucLiftMng();
            tabBinding(uc);
        }

        /// <summary>
        /// 단위공정 관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnitProcessMng_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucUnitProcessMng uc = new ucUnitProcessMng();
            tabBinding(uc);
        }

        private void ucWorkPlanMng_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucWorkPlanMng uc = new ucWorkPlanMng();
            tabBinding(uc);
        }

        /// <summary>
        /// 모의결과 관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucSimulSelect_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucSimulSelcet uc = new ucSimulSelcet();
            tabBinding(uc);
        }
           
        /// <summary>
        /// 메인화면 선택
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucMain uc = new ucMain();
            tabBinding(uc);
        }
        #endregion 

        /// <summary>
        /// 탭 아이템 바인딩
        /// </summary>
        /// <param name="uc"></param>
        private void tabBinding(UserControl uc)
        {
            DXTabControl tab = TabContent.tabSubMenu;

            if(tab.Items.Count>0)
            {
                for (int i = 0; i < tab.Items.Count; i++)
                {
                    tab.Items.RemoveAt(i);
                }
            }

            DXTabItem item = new DXTabItem();
            item.Header = uc.Name;
            item.Content = uc;
            tab.Items.Add(item);
        }

        private void winCaseNumber_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            winCaseNumber win = new winCaseNumber();
            win.Show();
            win.Topmost = true;
        }
    }
}
