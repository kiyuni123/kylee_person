﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// controlSumulSelect.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class controlSumulSelect:UserControl
    {
        public controlSumulSelect(DataTable result, DataTable liftresult, string title)
        {
            InitializeComponent();
            BindingData(result, liftresult, title);
        }

        private void BindingData(DataTable result, DataTable liftresult, string title)
        {
            gdRange.ItemsSource = result;
            gdLift.ItemsSource = liftresult;

            lbliftcnt.Content = title;
            lbrangecnt.Content = title;

        }
    }
}
