﻿using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using SKU_Lift_Simulation.Analysis;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// winCaseNumber.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class winCaseNumber:Window
    {
        Thread thread = null;
        Lift_Optimal_Operation LOO = new Lift_Optimal_Operation();
        AnalysisWork work = new AnalysisWork();
        DataTable StackBuilding = new DataTable();  //누적 층고 빌딩 정보
        DataTable UseBuilding = new DataTable();    //분석 사용 빌딩 정보
        DataTable UseLift = new DataTable();        //분석 사용 리프트 정보
        DataTable LiftSpec = new DataTable();       //리프트 스펙 조회
        Hashtable htresult = new Hashtable();
        Paragraph paragraph = new Paragraph();

        public winCaseNumber()
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            Loaded += WinCaseNumber_Loaded;
            Closed += WinCaseNumber_Closed;
            btnStart.Click += BtnStart_Click;
            btnChange.Click += BtnChange_Click;
        }

        private void BtnChange_Click(object sender, RoutedEventArgs e)
        {
            foreach(DataRow r in UseLift.Rows)
            {
                DataRow[] dr = ((DataTable)gdResult.ItemsSource).Select("L_No = " + r["L_No"].ToString());
                if(dr.Length == 1)
                {
                    r["R_Min"] = dr[0]["R_Min"];
                    r["R_Max"] = dr[0]["R_Max"];
                }
            }
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            thread = new Thread(ThreadWork);
            thread.Start();
        }

        private void WinCaseNumber_Loaded(object sender, RoutedEventArgs e)
        {
            UseBuilding = work.Select_UseBuildingList(null);
            UseLift = work.Select_AnalLiftList(null);
            StackBuilding = work.Select_StackUseBuilding(null);
            LiftSpec = work.Select_LiftSpec(null);
            gdLift.ItemsSource = UseLift;

            DataTable dtrunnct = new DataTable();
            dtrunnct.Columns.Add("name");
            dtrunnct.Columns.Add("value");
            for(int i = 1; i < 11; i++)
            {
                DataRow r = dtrunnct.NewRow();
                r[0] = i;
                r[1] = i;
                dtrunnct.Rows.Add(r);
            }

            cbRuncnt.ItemsSource = dtrunnct;
            cbRuncnt.SelectedIndex = 0;
        }

        private void WinCaseNumber_Closed(object sender, EventArgs e)
        {
            if(thread != null)
            {
                thread.Abort();
            }

        }

        /// <summary>
        /// 메인 Log기록 (listbox, Logfile)
        /// </summary>
        /// <param name="strText"></param>
        public void ListBoxlog(string strText)
        {
            //string.Format(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss : ") +
            
            paragraph.Inlines.Add(new Run(strText + "\n"));
            richbox_Log.Document.Blocks.Add(paragraph);
            //마지막행 포커스
            richbox_Log.Focus();
            richbox_Log.ScrollToEnd();

            if(richbox_Log.Document.Blocks.Count > 100)
            {
                richbox_Log.Document.Blocks.Remove(richbox_Log.Document.Blocks.FirstBlock);
            }
        }

        public void ThreadWork()
        {
            int intruncnt=0;

            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                new Action((delegate ()
                {
                    intruncnt = Convert.ToInt32(cbRuncnt.EditValue.ToString());
                    cbRuncnt.IsEnabled = false;
                })));

            for(int i = 0; i < intruncnt; i++)
            {
                LOO.LiftOptimal_New(StackBuilding, UseBuilding, UseLift, LiftSpec, this);

                if(i < intruncnt-1)
                {
                    this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                   new Action((delegate ()
                   {
                       BtnChange_Click(null, null);
                   })));

                    Thread.Sleep(500);
                }
            }

            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                new Action((delegate ()
                {
                    cbRuncnt.IsEnabled = true;
                })));
        }
    }
}
