﻿using DevExpress.Xpf.Core;
using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// ucKindsMng.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucKindsMng : UserControl
    {
        KindsMngWork work = new KindsMngWork();
        bool breg = false;

        public ucKindsMng()
        {
            InitializeComponent();
            InitData();
        }

        #region 이벤트
        /// <summary>
        /// 삭제버튼(OK)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KindsDel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DXMessageBox.Show("선택한 항목을 삭제 하시겠습니까?", "삭제", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                {
                    Hashtable conditions = new Hashtable();
                    DataRowView dr = (DataRowView)gdList.SelectedItem;

                    conditions.Add("M_No", dr["M_No"].ToString());
                    work.Delete_KidsList(conditions);

                    InitData();
                    Messages.ShowOkMsgBox();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 저장버튼(OK)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KindsSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("M_Name", txtName.Text);
                conditions.Add("L_Velocity", txtVelocity.Text);
                conditions.Add("L_Time", txtTime.Text);
                conditions.Add("L_Distance", txtDistance.Text);
                conditions.Add("L_Capa", txtCapa.Text);
                conditions.Add("L_Acceleration", txtAcceleration.Text);

                if (breg)
                {
                    work.Insert_KidsList(conditions);
                }
                else
                {
                    DataRowView dr = (DataRowView)gdList.SelectedItem;
                    conditions.Add("M_No", dr["M_No"].ToString());
                    work.Update_KidsList(conditions);
                }

                InitData();
                Messages.ShowOkMsgBox();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 추가버튼(OK)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KindsAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                breg = true;

                txtName.Text = string.Empty;
                txtVelocity.Text = string.Empty;
                txtTime.Text = string.Empty;
                txtDistance.Text = string.Empty;
                txtCapa.Text = string.Empty;
                txtAcceleration.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 그리드 행 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gdList_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            try
            {
                breg = false;
                DataRowView dr = (DataRowView)gdList.SelectedItem;

                if (dr != null)
                {
                    txtName.Text = dr["M_Name"].ToString();
                    txtVelocity.Text = dr["L_Velocity"].ToString();
                    txtTime.Text = dr["L_Time"].ToString();
                    txtDistance.Text = dr["L_Distance"].ToString();
                    txtCapa.Text = dr["L_Capa"].ToString();
                    txtAcceleration.Text = dr["L_Acceleration"].ToString();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        private void InitData()
        {
            DataTable KindsListdt = new DataTable();
            KindsListdt = work.Select_KidsList(null);
            gdList.ItemsSource = KindsListdt;
        }
    }
}
