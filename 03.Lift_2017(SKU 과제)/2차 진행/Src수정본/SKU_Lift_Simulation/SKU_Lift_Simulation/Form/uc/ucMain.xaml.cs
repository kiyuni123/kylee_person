﻿using DevExpress.Xpf.Charts;
using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Analysis;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// ucMain.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucMain : UserControl
    {
        public ucMain()
        {
            InitializeComponent();
            InitData();
            InitialAnal();
        }

        private void SimulationsInit_Click(object sender, RoutedEventArgs e)
        {
            Messages.ShowInfoMsgBox("시뮬레이션");
        }

        private void Simulations_Click(object sender, RoutedEventArgs e)
        {
            Lift_Optimal_Operation LOO = new Lift_Optimal_Operation();
            AnalysisWork work = new AnalysisWork();

            DataTable StackBuilding = new DataTable();  //누적 층고 빌딩 정보
            DataTable UseBuilding = new DataTable();    //분석 사용 빌딩 정보
            DataTable UseLift = new DataTable();        //분석 사용 리프트 정보
            DataTable LiftSpec = new DataTable();       //리프트 스펙 조회

            UseBuilding = work.Select_UseBuildingList(null);
            UseLift = work.Select_AnalLiftList(null);
            StackBuilding = work.Select_StackUseBuilding(null);
            LiftSpec = work.Select_LiftSpec(null);

            if(LOO.LiftOptimal(StackBuilding, UseBuilding, UseLift, LiftSpec) == null)
            {
                Messages.ShowInfoMsgBox("시뮬레이션이 실패했습니다.");
            }
            else
            {
                Messages.ShowInfoMsgBox("시뮬레이션이 성공하였습니다.");
            }
        }

        /// <summary>
        /// 데이터
        /// </summary>
        private void InitData()
        {
            BuildingMngWork Buildingwork = new BuildingMngWork();
            LiftMngWork liftwork = new LiftMngWork();
            WorkPlanMngWork workwork = new WorkPlanMngWork();

            DataTable dtBuilding = new DataTable();
            dtBuilding = Buildingwork.Select_UseBuildingList(null);

            if(dtBuilding.Rows.Count == 1)
            {
                lbBuilding.Content = dtBuilding.Rows[0]["B_Name"].ToString();
                lbminFloor.Content = dtBuilding.Rows[0]["B_Min"].ToString();
                lbmaxFloor.Content = dtBuilding.Rows[0]["B_Max"].ToString();

                lbIntime.Content = dtBuilding.Rows[0]["B_Intime"].ToString();
                lbOuttime.Content = dtBuilding.Rows[0]["B_Outtime"].ToString();
                lbflgap.Content = dtBuilding.Rows[0]["B_Flgap"].ToString();

                Hashtable conditions = new Hashtable();
                conditions.Add("B_No", dtBuilding.Rows[0]["B_No"].ToString());

                DataTable dtLift = new DataTable();
                dtLift = liftwork.Select_MainLiftList(conditions);
                gdLift.ItemsSource = dtLift;

                DataTable dtwork = new DataTable();
                dtwork = workwork.Select_WorkPlanList(conditions);
                gdWork.ItemsSource = dtwork;
            }
            
            else if(dtBuilding.Rows.Count == 0)
            {
                lbBuilding.Content = "사용 건물을 선택하세요.";
                lbminFloor.Content = "-";
                lbmaxFloor.Content = "-";
            }
        }

        /// <summary>
        /// 로컬 차트
        /// </summary>
        private void ChartData(DataTable dtChart)
        {
            Chart.Diagram.Series.Add(new BarSideBySideSeries2D()
            {
                Name = "local",
                Model = new BorderlessSimpleBar2DModel(),
                ArgumentScaleType = ScaleType.Qualitative
            });

            foreach (DataRow dr in dtChart.Rows)
            {
                if (dr["y"] != DBNull.Value)
                {
                    diagram.Series[0].Points.Add(new SeriesPoint(dr["x"].ToString(), Convert.ToDouble(dr["y"].ToString())));
                    diagram.Series[0].DisplayName = "value";
                    diagram.Series[0].ShowInLegend = true;
                }
                else if (dr["y"] == DBNull.Value)
                {
                    diagram.Series[0].Points.Add(new SeriesPoint(dr["x"].ToString(), 0));
                    diagram.Series[0].DisplayName = "value";
                    diagram.Series[0].ShowInLegend = true;
                }
            }
        }

        private void InitialAnal()
        {
            try
            {
                Lift_Optimal_Operation LOO = new Lift_Optimal_Operation();
                AnalysisWork work = new AnalysisWork();

                DataTable StackBuilding = new DataTable();  //누적 층고 빌딩 정보
                DataTable UseBuilding = new DataTable();    //분석 사용 빌딩 정보
                DataTable UseLift = new DataTable();        //분석 사용 리프트 정보
                DataTable LiftSpec = new DataTable();       //리프트 스펙 조회

                UseBuilding = work.Select_UseBuildingList(null);
                UseLift = work.Select_AnalLiftList(null);
                StackBuilding = work.Select_StackUseBuilding(null);
                LiftSpec = work.Select_LiftSpec(null);

                Hashtable htresult = LOO.LiftOptimal(StackBuilding, UseBuilding, UseLift, LiftSpec);

                if(htresult == null)
                {
                    Messages.ShowInfoMsgBox("시뮬레이션이 실패했습니다.");
                    return;
                }

                DataTable dtChart = new DataTable();
                dtChart.Columns.Add("y");
                dtChart.Columns.Add("x");

                List<double> val = new List<double>();
                val = htresult["val"] as List<double>;

                for(int i = 0; i < val.Count; i++)
                {
                    DataRow r = dtChart.NewRow();
                    r["y"] = val[i];
                    r["x"] = "local" + (i + 1).ToString(); 
                    dtChart.Rows.Add(r);
                }

                ChartData(dtChart);

                DataSet dsresult = new DataSet();
                DataSet dsliftresult = new DataSet();

                dsresult = htresult["dsresult"] as DataSet;
                dsliftresult = htresult["dsliftresult"] as DataSet;

                gdRange.ItemsSource = dsresult.Tables[dsliftresult.Tables.Count - 1];
                gdResult.ItemsSource = dsliftresult.Tables[dsliftresult.Tables.Count - 1];
            }
            catch(Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
