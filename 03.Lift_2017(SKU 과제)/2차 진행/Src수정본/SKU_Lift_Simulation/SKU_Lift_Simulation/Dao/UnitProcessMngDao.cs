﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Dao
{
    class UnitProcessMngDao
    {
        /// <summary>
        /// 단위공정리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_UnitProcessList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_UnitProcessList", conditions);
        }

        /// <summary>
        /// 단위공정리스트 추가
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_UnitProcessList(Hashtable conditions)
        {
            DBManager.QueryForInsert("Insert_UnitProcessList", conditions);
        }

        /// <summary>
        /// 단위공정리스트 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_UnitProcessList(Hashtable conditions)
        {
            DBManager.QueryForDelete("Delete_UnitProcessList", conditions);
        }

        /// <summary>
        /// 단위공정리스트 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_UnitProcessList(Hashtable conditions)
        {
            DBManager.QueryForDelete("Update_UnitProcessList", conditions);
        }
    }
}
