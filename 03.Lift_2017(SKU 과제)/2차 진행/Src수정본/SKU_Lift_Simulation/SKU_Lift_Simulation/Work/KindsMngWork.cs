﻿using SKU_Lift_Simulation.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Work
{
    class KindsMngWork
    {
        KindsMngDao dao = new KindsMngDao();

        /// <summary>
        /// 기종리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_KidsList(Hashtable conditions)
        {
            return dao.Select_KidsList(conditions);
        }

        /// <summary>
        /// 기종리스트 추가
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_KidsList(Hashtable conditions)
        {
            dao.Insert_KidsList(conditions);
        }

        /// <summary>
        /// 기종리스트 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_KidsList(Hashtable conditions)
        {
            dao.Delete_KidsList(conditions);
        }

        /// <summary>
        /// 기종리스트 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_KidsList(Hashtable conditions)
        {
            dao.Update_KidsList(conditions);
        }
    }
}
