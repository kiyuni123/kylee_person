﻿using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Form.uc;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SKU_Lift_Simulation.Analysis
{
    public class Lift_Optimal_Operation
    {
        AnalysisWork work = new AnalysisWork();

        //System.IO.StreamWriter file = new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\Data.txt", true, System.Text.Encoding.GetEncoding("euc-kr"));

        /// <summary>
        /// 운영모의 로직 ver1
        /// </summary>
        /// <param name="StackBuilding"></param>
        /// <param name="UseBuilding"></param>
        /// <param name="UseBuilding"></param>
        /// <param name="UseLift"></param>
        /// <param name="LiftSpec"></param>
        /// <returns>htresult(dsresult, dsliftresult)</returns>
        public Hashtable LiftOptimal(DataTable StackBuilding, DataTable UseBuilding, DataTable UseLift, DataTable LiftSpec)
        {
            Hashtable htresult = new Hashtable();       //결과값 범위데이터셋, 리프트셋
            DataSet dsresult = new DataSet();           //결과값 loop돌리고 각 loop마다 결과값 저장
            DataSet dsliftresult = new DataSet();       //loop마다 최적 결과 lift제원(운행층수 중요) 저장
            List<double> val = new List<double>();

            DataTable Range = new DataTable();          //범위 리스트 * 중요

            try
            {
                int stopgap = Convert.ToInt32(UseBuilding.Rows[0]["B_Flgap"].ToString());  //정차층수(층)
                int intime = Convert.ToInt32(UseBuilding.Rows[0]["B_Intime"].ToString());  //탑승시간(sec)
                int outtime = Convert.ToInt32(UseBuilding.Rows[0]["B_Outtime"].ToString()); //하차시간(sec)

                int coverfl = stopgap / 2; //커버층수 (stopgap/2)

                //B_No seq, B_Name 빌딩이름, B_Min 최처층, B_Max 최고층, B_Area 면적, B_Intime 탑승시간, B_OutTime 하차시간, B_Flgap 정차 층수
                #region ######↓ 여기까지 사용자 입력 데이터 (분석에 필요한 기초 데이터)
                if(UseBuilding.Rows.Count != 1)
                {
                    Messages.ShowInfoMsgBox("사용되는 빌딩이 없습니다.");
                    return null;
                }

                if(UseLift.Rows.Count == 0)
                {
                    Messages.ShowInfoMsgBox("사용되는 리프트가 없습니다.");
                    return null;
                }
                #endregion

                //분석 함수 호출 Initial
                Range = SimulAnal(Range, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                Range.TableName = "Initial";
                dsresult.Tables.Add(Range.Copy());
                UseLift.TableName = "Initial";
                dsliftresult.Tables.Add(UseLift.Copy());

                #region ######↓ Range 추출 For문
                DataTable simultemp = new DataTable();
                DataSet dssimultemp;

                Hashtable resulttemp = new Hashtable();
                int simulcnt = 1;
                int localcnt = 1;
                int cyctime_minkey = 1;

                while(true)
                {
                    dssimultemp = null;
                    dssimultemp = new DataSet();
                    resulttemp.Clear();
                    simulcnt = 1;

                    #region 리프트 운행층수 min -5, max +5 번갈아 가면서 표본 및 분석
                    for(int j = 0; j < UseLift.Rows.Count; j++)
                    {
                        for(int i = 4; i < 6; i++)
                        {
                            if(i == 4)
                            {
                                if(Convert.ToInt32(StackBuilding.Compute("MIN(FL)", "1=1")) < Convert.ToInt32(UseLift.Rows[j][i]) - 5)
                                {
                                    int inttemp = Convert.ToInt32(UseLift.Rows[j][i]);
                                    UseLift.Rows[j][i] = inttemp - 5;

                                    simultemp = SimulAnal(simultemp, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                                    resulttemp.Add(simulcnt.ToString(), UseLift.Copy());
                                    simultemp.TableName = simulcnt.ToString();
                                    dssimultemp.Tables.Add(simultemp);

                                    simulcnt = simulcnt + 1;
                                    simultemp = null;
                                    simultemp = new DataTable();
                                    UseLift.Rows[j][i] = inttemp;
                                }
                                if(Convert.ToInt32(StackBuilding.Compute("MAX(FL)", "1=1")) >= Convert.ToInt32(UseLift.Rows[j][i]) + 5)
                                {
                                    int inttemp = Convert.ToInt32(UseLift.Rows[j][i]);
                                    UseLift.Rows[j][i] = inttemp + 5;

                                    simultemp = SimulAnal(simultemp, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                                    resulttemp.Add(simulcnt.ToString(), UseLift.Copy());
                                    simultemp.TableName = simulcnt.ToString();
                                    dssimultemp.Tables.Add(simultemp);

                                    simulcnt = simulcnt + 1;
                                    simultemp = null;
                                    simultemp = new DataTable();
                                    UseLift.Rows[j][i] = inttemp;
                                }
                            }
                            if(i == 5)
                            {
                                if(Convert.ToInt32(StackBuilding.Compute("MIN(FL)", "1=1")) < Convert.ToInt32(UseLift.Rows[j][i]) - 5)
                                {
                                    int inttemp = Convert.ToInt32(UseLift.Rows[j][i]);
                                    UseLift.Rows[j][i] = inttemp - 5;

                                    simultemp = SimulAnal(simultemp, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                                    resulttemp.Add(simulcnt.ToString(), UseLift.Copy());
                                    simultemp.TableName = simulcnt.ToString();
                                    dssimultemp.Tables.Add(simultemp);

                                    simulcnt = simulcnt + 1;
                                    simultemp = null;
                                    simultemp = new DataTable();
                                    UseLift.Rows[j][i] = inttemp;
                                }
                                if(Convert.ToInt32(StackBuilding.Compute("MAX(FL)", "1=1")) >= Convert.ToInt32(UseLift.Rows[j][i]) + 5)
                                {
                                    int inttemp = Convert.ToInt32(UseLift.Rows[j][i]);
                                    UseLift.Rows[j][i] = inttemp + 5;

                                    simultemp = SimulAnal(simultemp, StackBuilding, UseBuilding, UseLift, LiftSpec, stopgap, intime, outtime, coverfl);
                                    resulttemp.Add(simulcnt.ToString(), UseLift.Copy());
                                    simultemp.TableName = simulcnt.ToString();
                                    dssimultemp.Tables.Add(simultemp);

                                    simulcnt = simulcnt + 1;
                                    simultemp = null;
                                    simultemp = new DataTable();
                                    UseLift.Rows[j][i] = inttemp;
                                }
                            }
                        }
                    }
                    #endregion

                    //작은값 선정 같은 값일 경우 작은값중 제일 위값
                    foreach(DataTable table in dssimultemp.Tables)
                    {
                        if(Convert.ToDouble(dssimultemp.Tables[cyctime_minkey - 1].Compute("SUM(CYCTIME)", "")) > Convert.ToDouble(table.Compute("SUM(CYCTIME)", "")))
                        {
                            cyctime_minkey = Convert.ToInt32(table.TableName);
                        }
                    }

                    val.Add(Convert.ToDouble(dssimultemp.Tables[cyctime_minkey - 1].Compute("SUM(CYCTIME)", "")) - Convert.ToDouble(dsresult.Tables[localcnt - 1].Compute("SUM(CYCTIME)", "")));

                    if(Convert.ToDouble(dssimultemp.Tables[cyctime_minkey - 1].Compute("SUM(CYCTIME)", "")) - Convert.ToDouble(dsresult.Tables[localcnt - 1].Compute("SUM(CYCTIME)", "")) >= 0)
                    {
                        DataTable temp = new DataTable();
                        temp = dssimultemp.Tables[cyctime_minkey - 1].Copy();
                        temp.TableName = localcnt.ToString();
                        dsresult.Tables.Add(temp);
                        UseLift = (DataTable)resulttemp[cyctime_minkey.ToString()];
                        UseLift.TableName = localcnt.ToString();
                        dsliftresult.Tables.Add(UseLift.Copy());
                        break;
                    }
                    else
                    {
                        DataTable temp = new DataTable();
                        temp = dssimultemp.Tables[cyctime_minkey - 1].Copy();
                        temp.TableName = localcnt.ToString();
                        dsresult.Tables.Add(temp);
                        UseLift = (DataTable)resulttemp[cyctime_minkey.ToString()];
                        UseLift.TableName = localcnt.ToString();
                        dsliftresult.Tables.Add(UseLift.Copy());
                        localcnt = localcnt + 1;
                    }
                }
                #endregion

                htresult.Add("dsresult", dsresult);
                htresult.Add("dsliftresult", dsliftresult);
                htresult.Add("val", val);

                return htresult;
            }
            catch(Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }

        /// <summary>
        /// 분석하고 Range DataTable Return CYCTIME 총합은 TableName
        /// </summary>
        /// <param name="Range"></param>
        /// <param name="StackBuilding"></param>
        /// <param name="UseBuilding"></param>
        /// <param name="UseLift"></param>
        /// <param name="LiftSpec"></param>
        /// <param name="stopgap"></param>
        /// <param name="intime"></param>
        /// <param name="outtime"></param>
        /// <param name="coverfl"></param>
        /// <returns></returns>
        private DataTable SimulAnal(DataTable Range, DataTable StackBuilding, DataTable UseBuilding, DataTable UseLift, DataTable LiftSpec, int stopgap, int intime, int outtime, int coverfl)
        {
            try
            {
                #region######↓ Range DataTable columns 생성
                Range.Columns.Add("Range");        //명
                Range.Columns.Add("MINFL");        //최저층
                Range.Columns.Add("MAXFL");        //최고층
                Range.Columns.Add("STOPCNT");      //정차횟수
                Range.Columns.Add("PEOPLE");       //양중인원
                Range.Columns.Add("LIFTGRP");      //속하는리프트
                Range.Columns.Add("LIFTSPEC");     //리프트스펙종류
                                                   //리프트모델 종류
                foreach(DataRow r in LiftSpec.Rows)
                {
                    Range.Columns.Add(r["M_No"].ToString() + "_CNT");
                    Range.Columns.Add(r["M_No"].ToString() + "_PEOPLE");
                }
                Range.Columns.Add("RUNCNT");       //운행횟수
                Range.Columns.Add("CYCTIME", typeof(double));      //싸이클타임
                #endregion

                #region######↓ Range 추출
                List<int> listRange = new List<int>();
                foreach(DataRow r in UseLift.Rows)
                {
                    if(!listRange.Contains(Convert.ToInt32(r["R_MIN"].ToString())))
                    {
                        listRange.Add(Convert.ToInt32(r["R_MIN"].ToString()));
                    }
                    if(!listRange.Contains(Convert.ToInt32(r["R_MAX"].ToString())))
                    {
                        listRange.Add(Convert.ToInt32(r["R_MAX"].ToString()));
                    }
                }
                listRange.Sort();

                for(int i = 0; i < listRange.Count - 1; i++)
                {
                    DataRow r = Range.NewRow();
                    //처음
                    if(i == 0)
                    {
                        r["Range"] = "Range" + (i + 1).ToString();                                      //Range명
                        r["MINFL"] = listRange[i];                                                      //처음시작은 최초값
                        r["MAXFL"] = (listRange[i + 1] / stopgap) * stopgap + coverfl;                  //몫(다음층/정차층수)*정차층수+커버층수
                    }
                    //중간
                    else if(i == listRange.Count - 1)
                    {
                        r["Range"] = "Range" + (i + 1).ToString();                                      //Range명
                        r["MINFL"] = Convert.ToInt32(Range.Rows[i - 1]["MAXFL"].ToString()) + 1;        //앞 Range MAXFL의 + 1
                        r["MAXFL"] = listRange[i];                                                      //몫(다음층/정차층수)*정차층수+커버층수
                    }
                    //마지막
                    else
                    {
                        r["Range"] = "Range" + (i + 1).ToString();                                      //Range명
                        r["MINFL"] = Convert.ToInt32(Range.Rows[i - 1]["MAXFL"].ToString()) + 1;        //앞 Range MAXFL의 + 1
                        r["MAXFL"] = (listRange[i + 1] / stopgap) * stopgap + coverfl;                  //마지막은 끝값
                    }

                    Range.Rows.Add(r);
                }
                #endregion

                #region######↓ Range 데이터 분석 (STOPCNT, PEOPLE, LIFTGRP, LIFTSPEC, 리프트별 갯수, 용량, RUNCNT, CYCTIME)
                foreach(DataRow r in Range.Rows)
                {
                    #region //(최고층-최저층)+1/정차층수
                    r["STOPCNT"] = ((Convert.ToInt32(r["MAXFL"]) - Convert.ToInt32(r["MINFL"]) + 1)) / stopgap;
                    #endregion

                    #region //최저층 최고층 범위내에 작업자인원
                    r["PEOPLE"] = StackBuilding.Compute("SUM(W_LaborCount)", "FL >= " + r["MINFL"].ToString() + " AND FL <= " + r["MAXFL"].ToString());
                    if(r["PEOPLE"].Equals(DBNull.Value))
                    {
                        r["PEOPLE"] = 0;
                    }
                    #endregion

                    #region //속하는리프트 if(range최저층>=lift최저층 그리고 range최고층<=lift최고층)
                    foreach(DataRow dr in UseLift.Rows)
                    {
                        if(Convert.ToInt32(r["MINFL"]) >= Convert.ToInt32(dr["R_Min"]) && Convert.ToInt32(r["MAXFL"]) <= Convert.ToInt32(dr["R_Max"]))
                        {
                            r["LIFTGRP"] = r["LIFTGRP"] + dr["L_No"].ToString() + ",";
                        }
                    }
                    //속하는리프트 마지막 "," 절삭
                    if(r["LIFTGRP"].ToString().Length != 0)
                    {
                        r["LIFTGRP"] = r["LIFTGRP"].ToString().Substring(0, r["LIFTGRP"].ToString().Length - 1);
                    }
                    #endregion

                    #region //range에 속하는 리프트 스펙 종류
                    r["LIFTSPEC"] = (UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().DefaultView.ToTable(true, "M_No").Rows.Count;
                    #endregion

                    #region //리프트 Spec 종류별 갯수, 용량
                    foreach(DataRow dr in LiftSpec.Rows)
                    {
                        //갯수
                        r[dr["M_No"].ToString() + "_CNT"] = ((UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().Select("M_No IN (" + dr["M_No"].ToString() + ")")).Count();
                        //용량
                        r[dr["M_No"].ToString() + "_PEOPLE"] = ((UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().Select("M_No IN (" + dr["M_No"].ToString() + ")")).Count() * Convert.ToInt32(dr["L_Capa"]);
                    }
                    #endregion

                    #region //운행횟수, CycTime 계산
                    if(Convert.ToInt32(r["LIFTSPEC"]) == 1)
                    {
                        //1. 속해있는 리프트 스펙 조회
                        DataTable dttemp = (UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().DefaultView.ToTable(true, "M_No", "L_Time", "L_Distance", "L_Velocity");

                        //정차가능한최고층높이 구하기
                        double doumaxH;
                        double douTime = Convert.ToDouble(dttemp.Rows[0]["L_Time"]);
                        double douDistance = Convert.ToDouble(dttemp.Rows[0]["L_Distance"]);
                        double douVelocity = Convert.ToDouble(dttemp.Rows[0]["L_Velocity"]);

                        if((Convert.ToInt32(r["MAXFL"]) / stopgap) != 0)
                            doumaxH = Convert.ToDouble(StackBuilding.Select("FL = " + (Convert.ToInt32(r["MAXFL"]) / stopgap) * stopgap)[0]["W_ASEC"]);
                        else
                            doumaxH = Convert.ToDouble(StackBuilding.Select("FL = " + (1).ToString())[0]["W_ASEC"]);

                        if(Convert.ToInt32(r["PEOPLE"]) > 0)
                        {
                            //하차인원이 있는경우 
                            r["RUNCNT"] = (int)Math.Ceiling(Convert.ToDouble(r["PEOPLE"]) / Convert.ToDouble(r[(UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().DefaultView.ToTable(true, "M_No").Rows[0]["M_No"].ToString() + "_PEOPLE"]));
                            r["CYCTIME"] = intime + (Convert.ToInt32(r["STOPCNT"]) * outtime) + 2 * (((Convert.ToInt32(r["STOPCNT"]) + 1) * douTime) + (doumaxH - ((Convert.ToInt32(r["STOPCNT"]) + 1) * douDistance)) / douVelocity);
                            r["CYCTIME"] = Convert.ToInt32(r["CYCTIME"]) * Convert.ToInt32(r["RUNCNT"]);
                        }
                        else
                        {
                            //하차인원이 없는경우 
                            //r["RUNCNT"] = 1;
                            //r["CYCTIME"] = 0 + (0 * outtime) + 2 * (((0 + 1) * douTime) + (doumaxH - ((0 + 1) * douDistance)) / douVelocity);
                            r["RUNCNT"] = 0;
                            r["CYCTIME"] = 0;
                        }
                    }

                    if(Convert.ToInt32(r["LIFTSPEC"]) == 2)
                    {
                        //1. 속해있는 리프트 스펙 조회
                        DataTable dttemp = (UseLift.Select("L_No IN (" + r["LIFTGRP"].ToString() + ")")).CopyToDataTable().DefaultView.ToTable(true, "M_No", "L_Time", "L_Distance", "L_Velocity", "L_Capa");
                        dttemp.Columns.Add("CYCTIME");

                        //정차가능한최고층높이 구하기
                        double doumaxH;

                        if((Convert.ToInt32(r["MAXFL"]) / stopgap) != 0)
                            doumaxH = Convert.ToDouble(StackBuilding.Select("FL = " + (Convert.ToInt32(r["MAXFL"]) / stopgap) * stopgap)[0]["W_ASEC"]);
                        else
                            doumaxH = Convert.ToDouble(StackBuilding.Select("FL = " + (1).ToString())[0]["W_ASEC"]);

                        //속해있는 리프트 스펙의 CycTime 계산
                        foreach(DataRow dr in dttemp.Rows)
                        {
                            double douTime = Convert.ToDouble(dr["L_Time"]);
                            double douDistance = Convert.ToDouble(dr["L_Distance"]);
                            double douVelocity = Convert.ToDouble(dr["L_Velocity"]);

                            dr["CYCTIME"] = Math.Round(intime + (Convert.ToInt32(r["STOPCNT"]) * outtime) + 2 * (((Convert.ToInt32(r["STOPCNT"]) + 1) * douTime) + (doumaxH - ((Convert.ToInt32(r["STOPCNT"]) + 1) * douDistance)) / douVelocity), 2, MidpointRounding.ToEven);
                        }

                        //CYCTIME 계산
                        //필요 인자 생성
                        double a = 0;                                                                          // 1번 리프트 스펙의 N회 싸이클타임
                        double b = 0;                                                                          // 2번 리프트 스펙의 N회 싸이클타임
                        double a0 = Convert.ToDouble(dttemp.Rows[0]["CYCTIME"]);                               // 1번 리프트 스펙의 1회 싸이클타임
                        double b0 = Convert.ToDouble(dttemp.Rows[1]["CYCTIME"]);                               // 2번 리프트 스펙의 1회 싸이클타임
                        int Ca = Convert.ToInt32(r[dttemp.Rows[0]["M_No"].ToString() + "_PEOPLE"]);            // 1번 리프트 스펙의 용량
                        int Cb = Convert.ToInt32(r[dttemp.Rows[1]["M_No"].ToString() + "_PEOPLE"]);            // 2번 리프트 스펙의 용량
                        int R = Convert.ToInt32(r["PEOPLE"]);                                                  // 양중할 남은 인원
                        double CT = 0;                                                                         // 전체 소요 싸이클타임
                        int Na = 1;                                                                            // 1번 리프트가 사용된 횟수
                        int Nb = 1;                                                                            // 2번 리프트가 사용된 횟수

                        a = a0;
                        b = b0;

                        string strChk = "";

                        //시작
                        START:
                        //오른쪽
                        if(a > b)
                        {
                            R = R - Cb;
                            strChk = "R";
                            if(Math.Sign(R) == 1)
                            {
                                Nb = Nb + 1;
                                b = Nb * b0;
                                goto START;
                            }
                            else
                            {            
                                if(Nb != 0)
                                    CT = b0 * Nb;
                                else
                                    CT = b0;

                                goto FINISH;
                            }
                        }
                        //왼쪽
                        else if(a < b)
                        {
                            R = R - Ca;
                            strChk = "L";
                            if(Math.Sign(R) == 1)
                            {
                                Na = Na + 1;
                                a = Na * a0;
                                goto START;
                            }
                            else
                            {            
                                if(Na != 0)
                                    CT = a0 * Na;
                                else
                                    CT = a0;

                                goto FINISH;
                            }
                        }

                        FINISH:
                        r["CYCTIME"] = CT;

                        if(strChk.Equals("R"))
                        {
                            r["RUNCNT"] = Nb;
                        }
                        else if(strChk.Equals("L"))
                        {
                            r["RUNCNT"] = Na;
                        }
                        
                    }
                    #endregion
                }
                #endregion

                return Range;
            }
            catch(Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }

        public void LiftOptimal_New(DataTable StackBuilding, DataTable UseBuilding, DataTable UseLift, DataTable LiftSpec, winCaseNumber win)
        {
            DataSet dsliftNo = new DataSet();           //전체 경우의 수 531441
            DataTable Range = new DataTable();          //범위 리스트 * 중요
            List<int> lsint = new List<int>();

            //Text File Append
            System.IO.StreamWriter file = new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\test.txt", true, System.Text.Encoding.GetEncoding("euc-kr"));

            try
            {
                int stopgap = Convert.ToInt32(UseBuilding.Rows[0]["B_Flgap"].ToString());   //정차층수(층)
                int maxfl = Convert.ToInt32(UseBuilding.Rows[0]["B_Max"].ToString());       //최고층
                int liftcnt = UseLift.Rows.Count;                                       //리프트 갯수
                int intime = Convert.ToInt32(UseBuilding.Rows[0]["B_Intime"].ToString());   //탑승시간(sec)
                int outtime = Convert.ToInt32(UseBuilding.Rows[0]["B_Outtime"].ToString()); //하차시간(sec)
                int coverfl = stopgap / 2; //커버층수 (stopgap/2)
                maxfl = maxfl + coverfl;  // 맥스층 보다 한단계 정차층수 높은 MAX설정

                DataRow[] laborr = StackBuilding.Select("W_LaborCount>0", "FL ASC");
                int minlabor = Convert.ToInt32(laborr[0]["FL"].ToString());
                int maxlabor = Convert.ToInt32(laborr[laborr.Length - 1]["FL"].ToString());

                int filterMinFL = 0;
                int filterMaxFL = 0;

                //최소층
                if ((minlabor % stopgap) <= Convert.ToDouble(stopgap) / 2)
                {
                    filterMinFL = ((minlabor / stopgap) * stopgap) - coverfl;
                }
                else if ((minlabor % stopgap) > Convert.ToDouble(stopgap) / 2)
                {
                    filterMinFL = (((minlabor / stopgap) + 1) * stopgap) - coverfl;
                }

                //최대층
                if ((maxlabor % stopgap) <= Convert.ToDouble(stopgap) / 2)
                {
                    filterMaxFL = ((maxlabor / stopgap) * stopgap) + coverfl;
                }
                else if((maxlabor % stopgap) > Convert.ToDouble(stopgap) / 2)
                {
                    filterMaxFL = (((maxlabor / stopgap) + 1) * stopgap) + coverfl;
                }

                //B_No seq, B_Name 빌딩이름, B_Min 최처층, B_Max 최고층, B_Area 면적, B_Intime 탑승시간, B_OutTime 하차시간, B_Flgap 정차 층수
                #region ######↓ 여기까지 사용자 입력 데이터 (분석에 필요한 기초 데이터)
                if(UseBuilding.Rows.Count != 1)
                {
                    Messages.ShowInfoMsgBox("사용되는 빌딩이 없습니다.");
                }

                if(UseLift.Rows.Count == 0)
                {
                    Messages.ShowInfoMsgBox("사용되는 리프트가 없습니다.");
                }
                #endregion

                #region ######↓ 리프트 운행층수 경우의 수 표본 각 리프트의 최저, 최고층에 ±B_Flgap, 0
                int cntNO = 1;
                Hashtable htCyctimeMin = new Hashtable();
                double dcycval = 0;
                double dlowcycval = 0;

                win.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                new Action((delegate ()
                {
                    win.progress.Minimum = 0;
                    win.progress.Maximum = Math.Pow(3, 12);
                    win.progress.Value = 0;
                })));

                int analcnt = 1;

                #region 3^12 경우의 수
                for(int i1 = -1; i1 < 2; i1++)
                {
                    for(int i2 = -1; i2 < 2; i2++)
                    {
                        for(int i3 = -1; i3 < 2; i3++)
                        {
                            for(int i4 = -1; i4 < 2; i4++)
                            {
                                for(int i5 = -1; i5 < 2; i5++)
                                {
                                    for(int i6 = -1; i6 < 2; i6++)
                                    {
                                        for(int i7 = -1; i7 < 2; i7++)
                                        {
                                            for(int i8 = -1; i8 < 2; i8++)
                                            {
                                                for(int i9 = -1; i9 < 2; i9++)
                                                {
                                                    for(int i10 = -1; i10 < 2; i10++)
                                                    {
                                                        for(int i11 = -1; i11 < 2; i11++)
                                                        {
                                                            for(int i12 = -1; i12 < 2; i12++)
                                                            {
                                                                try
                                                                {
                                                                    //경우의수 다 저장할경우 메모리 풀 확인 후 작은 경우에만 저장
                                                                    DataTable temp = UseLift.Copy();
                                                                    temp.Rows[0]["R_Min"] = Convert.ToInt32(temp.Rows[0]["R_Min"].ToString()) + i1 * stopgap;
                                                                    temp.Rows[0]["R_Max"] = Convert.ToInt32(temp.Rows[0]["R_Max"].ToString()) + i2 * stopgap;
                                                                    temp.Rows[1]["R_Min"] = Convert.ToInt32(temp.Rows[1]["R_Min"].ToString()) + i3 * stopgap;
                                                                    temp.Rows[1]["R_Max"] = Convert.ToInt32(temp.Rows[1]["R_Max"].ToString()) + i4 * stopgap;
                                                                    temp.Rows[2]["R_Min"] = Convert.ToInt32(temp.Rows[2]["R_Min"].ToString()) + i5 * stopgap;
                                                                    temp.Rows[2]["R_Max"] = Convert.ToInt32(temp.Rows[2]["R_Max"].ToString()) + i6 * stopgap;
                                                                    temp.Rows[3]["R_Min"] = Convert.ToInt32(temp.Rows[3]["R_Min"].ToString()) + i7 * stopgap;
                                                                    temp.Rows[3]["R_Max"] = Convert.ToInt32(temp.Rows[3]["R_Max"].ToString()) + i8 * stopgap;
                                                                    temp.Rows[4]["R_Min"] = Convert.ToInt32(temp.Rows[4]["R_Min"].ToString()) + i9 * stopgap;
                                                                    temp.Rows[4]["R_Max"] = Convert.ToInt32(temp.Rows[4]["R_Max"].ToString()) + i10 * stopgap;
                                                                    temp.Rows[5]["R_Min"] = Convert.ToInt32(temp.Rows[5]["R_Min"].ToString()) + i11 * stopgap;
                                                                    temp.Rows[5]["R_Max"] = Convert.ToInt32(temp.Rows[5]["R_Max"].ToString()) + i12 * stopgap;

                                                                    if(Convert.ToInt32(temp.Compute("COUNT(R_Min)", "R_Min<0")) != 0)
                                                                    {
                                                                        file.WriteLine(cntNO.ToString() + " : 1 MIN 값이 0이하 count  => " + temp.Compute("COUNT(R_Min)", "R_Min<0").ToString()
                                                                            + "(" + temp.Rows[0]["R_Min"].ToString() + ", " + temp.Rows[0]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[1]["R_Min"].ToString() + ", " + temp.Rows[1]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[2]["R_Min"].ToString() + ", " + temp.Rows[2]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[3]["R_Min"].ToString() + ", " + temp.Rows[3]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[4]["R_Min"].ToString() + ", " + temp.Rows[4]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[5]["R_Min"].ToString() + ", " + temp.Rows[5]["R_Max"].ToString() + ") ");
                                                                    }
                                                                    else if(Convert.ToInt32(temp.Compute("COUNT(R_Max)", "R_Max>" + maxfl.ToString())) != 0)
                                                                    {
                                                                        file.WriteLine(cntNO.ToString() + " : 2 MAX 값이 " + maxfl.ToString() + " 이상 count  => " + temp.Compute("COUNT(R_Max)", "R_Max>" + maxfl.ToString())
                                                                            + "(" + temp.Rows[0]["R_Min"].ToString() + ", " + temp.Rows[0]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[1]["R_Min"].ToString() + ", " + temp.Rows[1]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[2]["R_Min"].ToString() + ", " + temp.Rows[2]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[3]["R_Min"].ToString() + ", " + temp.Rows[3]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[4]["R_Min"].ToString() + ", " + temp.Rows[4]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[5]["R_Min"].ToString() + ", " + temp.Rows[5]["R_Max"].ToString() + ") ");
                                                                    }
                                                                    else if(!(Convert.ToInt32(temp.Compute("COUNT(R_Min)", "R_Min<" + filterMinFL.ToString())) == 0))
                                                                    {
                                                                        file.WriteLine(cntNO.ToString() + " : 3 최소층(필터)보다 작은값이 존재"
                                                                            + "(" + temp.Rows[0]["R_Min"].ToString() + ", " + temp.Rows[0]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[1]["R_Min"].ToString() + ", " + temp.Rows[1]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[2]["R_Min"].ToString() + ", " + temp.Rows[2]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[3]["R_Min"].ToString() + ", " + temp.Rows[3]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[4]["R_Min"].ToString() + ", " + temp.Rows[4]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[5]["R_Min"].ToString() + ", " + temp.Rows[5]["R_Max"].ToString() + ") ");
                                                                    }
                                                                    else if(!(Convert.ToInt32(temp.Compute("COUNT(R_Max)", "R_Max>=" + filterMaxFL.ToString())) > 0))
                                                                    {
                                                                        file.WriteLine(cntNO.ToString() + " : 4 최대층(필터)보다 크거나 같은 값이 존재하지 않음"
                                                                            + "(" + temp.Rows[0]["R_Min"].ToString() + ", " + temp.Rows[0]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[1]["R_Min"].ToString() + ", " + temp.Rows[1]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[2]["R_Min"].ToString() + ", " + temp.Rows[2]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[3]["R_Min"].ToString() + ", " + temp.Rows[3]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[4]["R_Min"].ToString() + ", " + temp.Rows[4]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[5]["R_Min"].ToString() + ", " + temp.Rows[5]["R_Max"].ToString() + ") ");
                                                                    }
                                                                    else if(!(Convert.ToInt32(temp.Compute("COUNT(R_Min)", "R_Min=" + filterMinFL.ToString())) > 0))
                                                                    {
                                                                        file.WriteLine(cntNO.ToString() + " : 5 최소층(필터)와 같은 값이 존재하지 않음"
                                                                            + "(" + temp.Rows[0]["R_Min"].ToString() + ", " + temp.Rows[0]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[1]["R_Min"].ToString() + ", " + temp.Rows[1]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[2]["R_Min"].ToString() + ", " + temp.Rows[2]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[3]["R_Min"].ToString() + ", " + temp.Rows[3]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[4]["R_Min"].ToString() + ", " + temp.Rows[4]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[5]["R_Min"].ToString() + ", " + temp.Rows[5]["R_Max"].ToString() + ") ");
                                                                    }

                                                                    if(Convert.ToInt32(temp.Compute("COUNT(R_Min)", "R_Min<0")) == 0
                                                                        && Convert.ToInt32(temp.Compute("COUNT(R_Max)", "R_Max>" + maxfl.ToString())) == 0
                                                                        && Convert.ToInt32(temp.Compute("COUNT(R_Min)", "R_Min<" + filterMinFL.ToString())) == 0
                                                                        && Convert.ToInt32(temp.Compute("COUNT(R_Max)", "R_Max>=" + filterMaxFL.ToString())) > 0
                                                                        && Convert.ToInt32(temp.Compute("COUNT(R_Min)", "R_Min=" + filterMinFL.ToString())) > 0)
                                                                    {
                                                                        Range.Clear();
                                                                        Range.Columns.Clear();
                                                                        Range = SimulAnal(Range, StackBuilding, UseBuilding, temp, LiftSpec, stopgap, intime, outtime, coverfl);
                                                                        Range.TableName = "Range" + cntNO.ToString();

                                                                        dcycval = Convert.ToDouble(Range.Compute("SUM(CYCTIME)", ""));

                                                                        file.WriteLine(cntNO.ToString() + " : 분석(" + dcycval.ToString() + ") "
                                                                            + "(" + temp.Rows[0]["R_Min"].ToString() + ", " + temp.Rows[0]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[1]["R_Min"].ToString() + ", " + temp.Rows[1]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[2]["R_Min"].ToString() + ", " + temp.Rows[2]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[3]["R_Min"].ToString() + ", " + temp.Rows[3]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[4]["R_Min"].ToString() + ", " + temp.Rows[4]["R_Max"].ToString() + ") "
                                                                            + "(" + temp.Rows[5]["R_Min"].ToString() + ", " + temp.Rows[5]["R_Max"].ToString() + ") ");

                                                                        if(htCyctimeMin.Count == 0)
                                                                        {
                                                                            htCyctimeMin.Add("Key", cntNO);

                                                                            temp.TableName = "Table" + cntNO.ToString();
                                                                            htCyctimeMin.Add("UseTable", temp);

                                                                            lsint.Clear();
                                                                            lsint.Add(i1);
                                                                            lsint.Add(i2);
                                                                            lsint.Add(i3);
                                                                            lsint.Add(i4);
                                                                            lsint.Add(i5);
                                                                            lsint.Add(i6);
                                                                            lsint.Add(i7);
                                                                            lsint.Add(i8);
                                                                            lsint.Add(i9);
                                                                            lsint.Add(i10);
                                                                            lsint.Add(i11);
                                                                            lsint.Add(i12);
                                                                            htCyctimeMin.Add("List", lsint);
                                                                            htCyctimeMin.Add("CycTime", dcycval);
                                                                            dlowcycval = dcycval;
                                                                        }
                                                                        else if(dcycval < dlowcycval)
                                                                        {
                                                                            htCyctimeMin["Key"] = cntNO;

                                                                            temp.TableName = "Table" + cntNO.ToString();
                                                                            htCyctimeMin["UseTable"] = temp;

                                                                            lsint.Clear();
                                                                            lsint.Add(i1);
                                                                            lsint.Add(i2);
                                                                            lsint.Add(i3);
                                                                            lsint.Add(i4);
                                                                            lsint.Add(i5);
                                                                            lsint.Add(i6);
                                                                            lsint.Add(i7);
                                                                            lsint.Add(i8);
                                                                            lsint.Add(i9);
                                                                            lsint.Add(i10);
                                                                            lsint.Add(i11);
                                                                            lsint.Add(i12);
                                                                            htCyctimeMin["List"] = lsint;
                                                                            htCyctimeMin["CycTime"] = dcycval;
                                                                            dlowcycval = dcycval;
                                                                        }

                                                                        analcnt++;
                                                                    }
                                                                }
                                                                catch(Exception ex) { }

                                                                win.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                                                                    new Action((delegate ()
                                                                    {
                                                                        win.progress.Value = cntNO;
                                                                    })));
                                                                cntNO++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
                #endregion

                #region ######↓ UI 처리부분
                win.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                        new Action((delegate ()
                        {
                            if(htCyctimeMin.Count == 0)
                            {
                                Messages.ShowInfoMsgBox("분석 결과가 없습니다.");
                            }
                            else
                            {
                                win.gdResult.ItemsSource = htCyctimeMin["UseTable"] as DataTable;
                                win.txtCycValue.Content = ((double)htCyctimeMin["CycTime"]).ToString();

                                string logText = "";

                                foreach(DataRow r in ((DataTable)htCyctimeMin["UseTable"]).Rows)
                                {
                                    logText = logText + r["L_Name"].ToString() + "[" + r["R_Min"].ToString() + ", " + r["R_Max"].ToString() + "] ";
                                }

                                logText = logText + "Cyctime : " + ((double)htCyctimeMin["CycTime"]).ToString();

                                win.ListBoxlog(logText);
                            }
                        })));
                #endregion
            }
            catch(Exception ex)
            {
                win.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                new Action((delegate ()
                {
                    win.ListBoxlog(ex.ToString());
                    file.Dispose();
                })));
            }

            file.Dispose();
        }
    }
}
