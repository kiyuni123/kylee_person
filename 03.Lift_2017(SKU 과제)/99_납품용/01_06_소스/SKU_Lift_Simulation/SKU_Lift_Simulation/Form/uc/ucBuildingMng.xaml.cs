﻿using DevExpress.Xpf.Core;
using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// ucBuildingMng.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucBuildingMng : UserControl
    {
        BuildingMngWork work = new BuildingMngWork();
        bool breg = false;

        public ucBuildingMng()
        {
            InitializeComponent();
            InitData();
        }

        #region 이벤트
        /// <summary>
        /// 추가버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildingAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                breg = true;

                txtName.Text = string.Empty;
                txtArea.Text = string.Empty;
                txtmin.Text = string.Empty;
                txtMax.Text = string.Empty;
                chkUse.IsChecked = false;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 저장버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildingSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool bUse = (bool)chkUse.IsChecked;

                DataTable dtUseBuilding = new DataTable();
                dtUseBuilding = work.Select_UseBuildingList(null);

                //사용하는 건물이 있을경우
                if (dtUseBuilding.Rows.Count == 1 && bUse)
                {
                    if (DXMessageBox.Show("건물 " + dtUseBuilding.Rows[0]["B_Name"].ToString() + "이 사용중입니다. 변경 하시겠습니까?", "저장", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                    {
                        Hashtable useconditions = new Hashtable();
                        useconditions.Add("B_No", dtUseBuilding.Rows[0]["B_No"].ToString());
                        work.Update_UseBuildingList(useconditions);

                        Hashtable conditions = new Hashtable();
                        conditions.Add("B_Name", txtName.Text);
                        conditions.Add("B_Min", txtmin.Text);
                        conditions.Add("B_Max", txtMax.Text);
                        conditions.Add("B_Area", txtArea.Text);
                        if (bUse)
                        {
                            conditions.Add("B_Use", 1);
                        }
                        else if (!bUse)
                        {
                            conditions.Add("B_Use", 0);
                        }

                        if (breg)
                        {
                            work.Insert_BuildingList(conditions);
                        }
                        else
                        {
                            DataRowView dr = (DataRowView)gdList.SelectedItem;
                            conditions.Add("B_No", dr["B_No"].ToString());
                            work.Update_BuildingList(conditions);
                        }

                        InitData();
                    }
                }
                //사용하는 건물이 없을경우
                else if (dtUseBuilding.Rows.Count == 0)
                {
                    Hashtable conditions = new Hashtable();
                    conditions.Add("B_Name", txtName.Text);
                    conditions.Add("B_Min", txtmin.Text);
                    conditions.Add("B_Max", txtMax.Text);
                    conditions.Add("B_Area", txtArea.Text);
                    if (bUse)
                    {
                        conditions.Add("B_Use", 1);
                    }
                    else if (!bUse)
                    {
                        conditions.Add("B_Use", 0);
                    }

                    if (breg)
                    {
                        work.Insert_BuildingList(conditions);
                    }
                    else
                    {
                        DataRowView dr = (DataRowView)gdList.SelectedItem;
                        conditions.Add("B_No", dr["B_No"].ToString());
                        work.Update_BuildingList(conditions);
                    }

                    InitData();
                }

                Messages.ShowOkMsgBox();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 삭제버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildingDel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DXMessageBox.Show("선택한 항목을 삭제 하시겠습니까?", "삭제", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                {
                    Hashtable conditions = new Hashtable();
                    DataRowView dr = (DataRowView)gdList.SelectedItem;

                    conditions.Add("B_No", dr["B_No"].ToString());
                    work.Delete_BuildingList(conditions);

                    InitData();
                    Messages.ShowOkMsgBox();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 그리드 행 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gdList_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            try
            {
                breg = false;
                DataRowView dr = (DataRowView)gdList.SelectedItem;

                if (dr != null)
                {
                    txtName.Text = dr["B_Name"].ToString();
                    txtArea.Text = dr["B_Area"].ToString();
                    txtmin.Text = dr["B_Min"].ToString();
                    txtMax.Text = dr["B_Max"].ToString();

                    if (dr["B_Use"].ToString() == "1")
                    {
                        chkUse.IsChecked = true;
                    }
                    else if (dr["B_Use"].ToString() == "0")
                    {
                        chkUse.IsChecked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        private void InitData()
        {
            DataTable BuildingListdt = new DataTable();
            BuildingListdt = work.Select_BuildingList(null);
            gdList.ItemsSource = BuildingListdt;
        }
    }
}
