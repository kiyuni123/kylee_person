﻿using DevExpress.Xpf.Charts;
using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// ucMain.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucMain : UserControl
    {
        public ucMain()
        {
            InitializeComponent();
            InitData();
        }

        private void SimulationsInit_Click(object sender, RoutedEventArgs e)
        {
            Messages.ShowInfoMsgBox("시뮬레이션 초기화");
        }

        private void Simulations_Click(object sender, RoutedEventArgs e)
        {
            Messages.ShowInfoMsgBox("시뮬레이션");
        }

        /// <summary>
        /// 데이터
        /// </summary>
        private void InitData()
        {
            BuildingMngWork Buildingwork = new BuildingMngWork();
            LiftMngWork liftwork = new LiftMngWork();
            WorkPlanMngWork workwork = new WorkPlanMngWork();

            DataTable dtLift = new DataTable();
            dtLift = liftwork.Select_MainLiftList(null);
            gdLift.ItemsSource = dtLift;

            DataTable dtwork = new DataTable();
            dtwork = workwork.Select_WorkPlanList(null);
            gdWork.ItemsSource = dtwork;

            DataTable dtBuilding = new DataTable();
            dtBuilding = Buildingwork.Select_UseBuildingList(null);

            if(dtBuilding.Rows.Count == 1)
            {
                lbBuilding.Content = dtBuilding.Rows[0]["B_Name"].ToString();
                lbminFloor.Content = dtBuilding.Rows[0]["B_Min"].ToString();
                lbmaxFloor.Content = dtBuilding.Rows[0]["B_Max"].ToString();
            }
            else if(dtBuilding.Rows.Count == 0)
            {
                lbBuilding.Content = "사용 건물을 선택하세요.";
                lbminFloor.Content = "-";
                lbmaxFloor.Content = "-";
            }

            ChartData();
            GridData();
        }

        /// <summary>
        /// 로컬 차트
        /// </summary>
        private void ChartData()
        {
            Chart.Diagram.Series.Add(new BarSideBySideSeries2D()
            {
                Name = "local",
                Model = new BorderlessSimpleBar2DModel(),
                ArgumentScaleType = ScaleType.Qualitative
            });

            DataTable temp = new DataTable();
            temp.Columns.Add("x");
            temp.Columns.Add("y");

            DataRow r1 = temp.NewRow();
            r1[0] = "Local1";
            r1[1] = -1368;
            temp.Rows.Add(r1);

            DataRow r2 = temp.NewRow();
            r2[0] = "Local2";
            r2[1] = -585;
            temp.Rows.Add(r2);

            DataRow r3 = temp.NewRow();
            r3[0] = "Loca3";
            r3[1] = -585;
            temp.Rows.Add(r3);

            DataRow r4 = temp.NewRow();
            r4[0] = "Local4";
            r4[1] = -357.833;
            temp.Rows.Add(r4);

            DataRow r5 = temp.NewRow();
            r5[0] = "Local5";
            r5[1] = -1357.688;
            temp.Rows.Add(r5);

            DataRow r6 = temp.NewRow();
            r6[0] = "Local6";
            r6[1] = -169.477;
            temp.Rows.Add(r6);

            DataRow r7 = temp.NewRow();
            r7[0] = "Local7";
            r7[1] = -947.542;
            temp.Rows.Add(r7);

            DataRow r8 = temp.NewRow();
            r8[0] = "Local8";
            r8[1] = -69.068;
            temp.Rows.Add(r8);

            DataRow r9 = temp.NewRow();
            r9[0] = "Local9";
            r9[1] = -204.020;
            temp.Rows.Add(r9);

            DataRow r10 = temp.NewRow();
            r10[0] = "Local10";
            r10[1] = -464.444;
            temp.Rows.Add(r10);

            DataRow r11 = temp.NewRow();
            r11[0] = "Local11";
            r11[1] = -253.868;
            temp.Rows.Add(r11);

            DataRow r12 = temp.NewRow();
            r12[0] = "Local12";
            r12[1] = -144.596;
            temp.Rows.Add(r12);

            DataRow r13 = temp.NewRow();
            r13[0] = "Local13";
            r13[1] = -152.320;
            temp.Rows.Add(r13);

            DataRow r14 = temp.NewRow();
            r14[0] = "Local14";
            r14[1] = -140.285;
            temp.Rows.Add(r14);

            DataRow r15 = temp.NewRow();
            r15[0] = "Local15";
            r15[1] = 0;
            temp.Rows.Add(r15);


            foreach (DataRow dr in temp.Rows)
            {
                if (dr["y"] != DBNull.Value)
                {
                    diagram.Series[0].Points.Add(new SeriesPoint(dr["x"].ToString(), Convert.ToDouble(dr["y"].ToString())));
                    diagram.Series[0].DisplayName = "value";
                    diagram.Series[0].ShowInLegend = true;
                }
                else if (dr["y"] == DBNull.Value)
                {
                    diagram.Series[0].Points.Add(new SeriesPoint(dr["x"].ToString(), 0));
                    diagram.Series[0].DisplayName = "value";
                    diagram.Series[0].ShowInLegend = true;
                }
            }
        }

        private void GridData()
        {
            //Range정보
            DataTable dtRange = new DataTable();
            dtRange.Columns.Add("Range");
            dtRange.Columns.Add("Top");
            dtRange.Columns.Add("Bottom");
            dtRange.Columns.Add("Stop");
            dtRange.Columns.Add("Cycle");

            DataRow r1 = dtRange.NewRow();
            r1[0] = "Range A";
            r1[1] = 102;
            r1[2] = 5;
            r1[3] = 20;
            r1[4] = 12483;
            dtRange.Rows.Add(r1);

            DataRow r2 = dtRange.NewRow();
            r2[0] = "Range B";
            r2[1] = "";
            r2[2] = 103;
            r2[3] = "";
            r2[4] = "";
            dtRange.Rows.Add(r2);

            gdRange.ItemsSource = dtRange;

            //결과정보
            DataTable dtresult = new DataTable();
            dtresult.Columns.Add("Lift");
            dtresult.Columns.Add("Top");
            dtresult.Columns.Add("Bottom");
            dtresult.Columns.Add("Stop");
            dtresult.Columns.Add("Cycle");

            DataRow dr1 = dtresult.NewRow();
            dr1[0] = "리프트A";
            dr1[1] = 69;
            dr1[2] = 20;
            dr1[3] = 10;
            dr1[4] = 0;
            dtresult.Rows.Add(dr1);

            DataRow dr2 = dtresult.NewRow();
            dr2[0] = "리프트B";
            dr2[1] = 89;
            dr2[2] = 10;
            dr2[3] = 16;
            dr2[4] = 0;
            dtresult.Rows.Add(dr2);

            DataRow dr3 = dtresult.NewRow();
            dr3[0] = "리프트C";
            dr3[1] = 99;
            dr3[2] = 5;
            dr3[3] = 19;
            dr3[4] = 3900;
            dtresult.Rows.Add(dr3);

            DataRow dr4 = dtresult.NewRow();
            dr4[0] = "리프트D";
            dr4[1] = 104;
            dr4[2] = 5;
            dr4[3] = 20;
            dr4[4] = 1063.616;
            dtresult.Rows.Add(dr4);

            DataRow dr5 = dtresult.NewRow();
            dr5[0] = "리프트E";
            dr5[1] = 104;
            dr5[2] = 5;
            dr5[3] = 20;
            dr5[4] = 493.070;
            dtresult.Rows.Add(dr5);

            DataRow dr6 = dtresult.NewRow();
            dr6[0] = "리프트F";
            dr6[1] = 104;
            dr6[2] = 5;
            dr6[3] = 20;
            dr6[4] = 227.166;
            dtresult.Rows.Add(dr6);

            gdResult.ItemsSource = dtresult;
        }
    }
}
