﻿using DevExpress.Xpf.Core;
using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// ucWorkPlanMng.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucWorkPlanMng : UserControl
    {
        WorkPlanMngWork work = new WorkPlanMngWork();
        LiftMngWork Liftwork = new LiftMngWork();
        bool breg = false;

        public ucWorkPlanMng()
        {
            InitializeComponent();
            InitData();
        }

        #region 이벤트
        /// <summary>
        /// 추가버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkPlanAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                breg = true;

                cbBName.SelectedIndex = -1;
                txtSFloor.Text = string.Empty;
                txtEFloor.Text = string.Empty;
                txtLaborCount.Text = string.Empty;
                txtAsec.Text = string.Empty;
                cbPName.SelectedIndex = -1;
                dtPDate.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 저장버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkPlanSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DXMessageBox.Show("입력한 항목이 입력 또는 수정 됩니다. 진행하시겠습니까?", "확인", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                {
                    if (chkfloor.IsChecked == true)
                    {
                        for (int i = Convert.ToInt32(txtSFloor.Text); i < Convert.ToInt32(txtEFloor.Text)+1; i++)
                        {
                            Hashtable conditions = new Hashtable();
                            conditions.Add("B_No", cbBName.EditValue);
                            conditions.Add("B_Floor", i);
                            conditions.Add("W_LaborCount", txtLaborCount.Text);
                            conditions.Add("P_No", cbPName.EditValue);
                            conditions.Add("P_Date", dtPDate.Text);
                            conditions.Add("W_ASEC", txtAsec.Text);

                            work.MergeInsert_WorkPlanList(conditions);
                        }
                    }
                    else
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("B_No", cbBName.EditValue);
                        conditions.Add("B_Floor", txtSFloor.Text);
                        conditions.Add("W_LaborCount", txtLaborCount.Text);
                        conditions.Add("P_No", cbPName.EditValue);
                        conditions.Add("P_Date", dtPDate.Text);
                        conditions.Add("W_ASEC", txtAsec.Text);

                        work.MergeInsert_WorkPlanList(conditions);
                    }

                    InitData();
                    Messages.ShowOkMsgBox();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 삭제버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkPlanDel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DXMessageBox.Show("선택한 항목을 삭제 하시겠습니까?", "삭제", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                {
                    Hashtable conditions = new Hashtable();
                    DataRowView dr = (DataRowView)gdList.SelectedItem;

                    conditions.Add("B_No", dr["B_No"].ToString());
                    conditions.Add("B_Floor", dr["B_Floor"].ToString());
                    work.Delete_WorkPlanList(conditions);

                    InitData();
                    Messages.ShowOkMsgBox();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 그리드 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gdList_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            try
            {
                breg = false;
                DataRowView dr = (DataRowView)gdList.SelectedItem;

                chkfloor.IsChecked = false;

                if (dr != null)
                {
                    cbBName.Text = dr["B_Name"].ToString();
                    txtSFloor.Text = dr["B_Floor"].ToString();
                    txtLaborCount.Text = dr["W_LaborCount"].ToString();
                    txtAsec.Text = dr["W_ASEC"].ToString();
                    cbPName.Text = dr["P_Name"].ToString();
                    dtPDate.Text = dr["P_Date"].ToString();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 층구역 기능
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckEdit_Check(object sender, RoutedEventArgs e)
        {
            if (chkfloor.IsChecked == true)
            {
                GridLength lbcolgl = new GridLength(10, GridUnitType.Pixel);
                lbcol.Width = lbcolgl;

                GridLength txtcolgl = new GridLength(1, GridUnitType.Star);
                txtcol.Width = txtcolgl;
            }
            if (chkfloor.IsChecked == false)
            {
                GridLength gl = new GridLength(0, GridUnitType.Pixel);
                lbcol.Width = gl;
                txtcol.Width = gl;
            }
        }
        #endregion

        private void InitData()
        {
            DataTable dtBuildingList = new DataTable();
            dtBuildingList = Liftwork.Select_ComboBuildingList(null);
            cbBName.ItemsSource = dtBuildingList;

            DataTable dtUnitProcessList = new DataTable();
            dtUnitProcessList = work.Select_ComboUnitProcessList(null);
            cbPName.ItemsSource = dtUnitProcessList;

            DataTable dtWorkPlanList = new DataTable();
            dtWorkPlanList = work.Select_WorkPlanList(null);
            gdList.ItemsSource = dtWorkPlanList;
        }


    }
}
