﻿using DevExpress.Xpf.Core;
using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// ucLiftMng.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucLiftMng : UserControl
    {
        LiftMngWork work = new LiftMngWork();
        bool breg = false;

        public ucLiftMng()
        {
            InitializeComponent();
            InitData();
        }

        #region 이벤트
        /// <summary>
        /// 추가버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LiftAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                breg = true;

                txtName.Text = string.Empty;
                cbModel.SelectedIndex = -1;
                cbBuilding.SelectedIndex = -1;
                txtMin.Text = string.Empty;
                txtMax.Text = string.Empty;
                txtGap.Text = string.Empty;
                chkUse.IsChecked = false;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 저장버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LiftSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool bUse = (bool)chkUse.IsChecked;

                Hashtable conditions = new Hashtable();
                conditions.Add("L_Name", txtName.Text);
                conditions.Add("B_No", cbBuilding.EditValue);
                conditions.Add("M_No", cbModel.EditValue);
                conditions.Add("R_Min", txtMin.Text);
                conditions.Add("R_Max", txtMax.Text);
                conditions.Add("R_Gap", txtGap.Text);

                if (bUse)
                {
                    conditions.Add("L_Use", 1);
                }
                else if (!bUse)
                {
                    conditions.Add("L_Use", 0);
                }

                if (breg)
                {
                    work.Insert_LiftList(conditions);
                }
                else
                {
                    DataRowView dr = (DataRowView)gdList.SelectedItem;
                    conditions.Add("L_No", dr["L_No"].ToString());
                    work.Update_LiftList(conditions);
                }

                InitData();
                Messages.ShowOkMsgBox();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 삭제버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LiftDel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DXMessageBox.Show("선택한 항목을 삭제 하시겠습니까?", "삭제", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                {
                    Hashtable conditions = new Hashtable();
                    DataRowView dr = (DataRowView)gdList.SelectedItem;

                    conditions.Add("L_No", dr["L_No"].ToString());
                    work.Delete_LiftList(conditions);

                    InitData();
                    Messages.ShowOkMsgBox();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 그리드 행 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gdList_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            try
            {
                breg = false;
                DataRowView dr = (DataRowView)gdList.SelectedItem;

                if (dr != null)
                {
                    txtName.Text = dr["L_Name"].ToString();
                    cbModel.Text = dr["M_Name"].ToString();
                    cbBuilding.Text = dr["B_Name"].ToString();
                    txtMin.Text = dr["R_Min"].ToString();
                    txtMax.Text = dr["R_Max"].ToString();
                    txtGap.Text = dr["R_Gap"].ToString();
                    if (dr["L_Use"].ToString() == "1")
                    {
                        chkUse.IsChecked = true;
                    }
                    else if (dr["L_Use"].ToString() == "0")
                    {
                        chkUse.IsChecked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 모델선택시 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbModel_SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if(cbModel.SelectedIndex!=-1)
                {
                    DataTable temp = new DataTable();
                    Hashtable conditions = new Hashtable();
                    conditions.Add("M_No", cbModel.EditValue.ToString());
                    temp = work.Select_ModelSpec(conditions);

                    if (temp.Rows.Count == 1)
                    {
                        txtMName.Text = temp.Rows[0]["M_Name"].ToString();
                        txtAcceleration.Text = temp.Rows[0]["L_Acceleration"].ToString();
                        txtCapa.Text = temp.Rows[0]["L_Capa"].ToString();
                        txtDistance.Text = temp.Rows[0]["L_Distance"].ToString();
                        txtTime.Text = temp.Rows[0]["L_Time"].ToString();
                        txtVelocity.Text = temp.Rows[0]["L_Velocity"].ToString();
                    }
                    else
                    {
                        Messages.ShowInfoMsgBox("선택하신 모델정보가 없습니다.");
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        private void InitData()
        {
            DataTable dtcombo = new DataTable();
            dtcombo = work.Select_ComboKidsList(null);
            cbModel.ItemsSource = dtcombo;

            DataTable dtBuildingcombo = new DataTable();
            dtBuildingcombo = work.Select_ComboBuildingList(null);
            cbBuilding.ItemsSource = dtBuildingcombo;

            DataTable LiftListdt = new DataTable();
            LiftListdt = work.Select_LiftList(null);
            gdList.ItemsSource = LiftListdt;
        }

        
    }
}
