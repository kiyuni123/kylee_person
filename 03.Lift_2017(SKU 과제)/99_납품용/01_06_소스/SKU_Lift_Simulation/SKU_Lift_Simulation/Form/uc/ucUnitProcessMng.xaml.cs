﻿using DevExpress.Xpf.Core;
using GTIFramework.Common.MessageBox;
using SKU_Lift_Simulation.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SKU_Lift_Simulation.Form.uc
{
    /// <summary>
    /// ucUnitProcessMng.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucUnitProcessMng : UserControl
    {
        UnitProcessMngWork work = new UnitProcessMngWork();
        bool breg = false;

        public ucUnitProcessMng()
        {
            InitializeComponent();
            InitData();
        }

        #region 이벤트

        /// <summary>
        /// 추가버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnitProcessAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                breg = true;

                txtName.Text = string.Empty;
                txtPeriod.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 저장버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnitProcessSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("P_Name", txtName.Text);
                conditions.Add("P_Period", txtPeriod.Text);

                if (breg)
                {
                    work.Insert_UnitProcessList(conditions);
                }
                else
                {
                    DataRowView dr = (DataRowView)gdList.SelectedItem;
                    conditions.Add("P_No", dr["P_No"].ToString());
                    work.Update_UnitProcessList(conditions);
                }

                InitData();
                Messages.ShowOkMsgBox();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 삭제버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnitProcessDel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DXMessageBox.Show("선택한 항목을 삭제 하시겠습니까?", "삭제", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                {
                    Hashtable conditions = new Hashtable();
                    DataRowView dr = (DataRowView)gdList.SelectedItem;

                    conditions.Add("P_No", dr["P_No"].ToString());
                    work.Delete_UnitProcessList(conditions);

                    InitData();
                    Messages.ShowOkMsgBox();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 그리드 행 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gdList_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            try
            {
                breg = false;
                DataRowView dr = (DataRowView)gdList.SelectedItem;

                if (dr != null)
                {
                    txtName.Text = dr["P_Name"].ToString();
                    txtPeriod.Text = dr["P_Period"].ToString();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        private void InitData()
        {
            DataTable UnitListdt = new DataTable();
            UnitListdt = work.Select_UnitProcessList(null);
            gdList.ItemsSource = UnitListdt;
        }

        
    }
}
