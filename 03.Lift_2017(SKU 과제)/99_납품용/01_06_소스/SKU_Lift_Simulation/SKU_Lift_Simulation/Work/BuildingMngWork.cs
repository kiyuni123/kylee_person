﻿using SKU_Lift_Simulation.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Work
{
    class BuildingMngWork
    {
        BuildingMngDao dao = new BuildingMngDao();

        /// <summary>
        /// 빌딩리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_BuildingList(Hashtable conditions)
        {
            return dao.Select_BuildingList(conditions);
        }

        /// <summary>
        /// 빌딩사용 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_UseBuildingList(Hashtable conditions)
        {
            return dao.Select_UseBuildingList(conditions);
        }

        /// <summary>
        /// 빌딩리스트 추가
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_BuildingList(Hashtable conditions)
        {
            dao.Insert_BuildingList(conditions);
        }

        /// <summary>
        /// 빌딩리스트 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_BuildingList(Hashtable conditions)
        {
            dao.Delete_BuildingList(conditions);
        }

        /// <summary>
        /// 빌딩리스트 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_BuildingList(Hashtable conditions)
        {
            dao.Update_BuildingList(conditions);
        }

        /// <summary>
        /// 빌딩리스트 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_UseBuildingList(Hashtable conditions)
        {
            dao.Update_UseBuildingList(conditions);
        }
    }
}
