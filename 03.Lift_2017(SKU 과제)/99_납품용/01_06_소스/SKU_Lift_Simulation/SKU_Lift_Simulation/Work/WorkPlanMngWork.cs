﻿using SKU_Lift_Simulation.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Work
{
    class WorkPlanMngWork
    {
        WorkPlanMngDao dao = new WorkPlanMngDao();

        /// <summary>
        /// 단위공정 콤보박스 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ComboUnitProcessList(Hashtable conditions)
        {
            return dao.Select_ComboUnitProcessList(conditions);
        }

        /// <summary>
        /// 작업관리 리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_WorkPlanList(Hashtable conditions)
        {
            return dao.Select_WorkPlanList(conditions);
        }

        /// <summary>
        /// 작업관리 리스트 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_WorkPlanList(Hashtable conditions)
        {
            dao.Delete_WorkPlanList(conditions);
        }

        /// <summary>
        /// 작업관리 리스트 MergeInsert
        /// </summary>
        /// <param name="conditions"></param>
        public void MergeInsert_WorkPlanList(Hashtable conditions)
        {
            dao.MergeInsert_WorkPlanList(conditions);
        }
    }
}
