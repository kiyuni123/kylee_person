﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKU_Lift_Simulation.Dao
{
    class BuildingMngDao
    {
        /// <summary>
        /// 빌딩리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_BuildingList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_BuildingList", conditions);
        }

        /// <summary>
        /// 빌딩사용 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_UseBuildingList(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_UseBuildingList", conditions);
        }

        /// <summary>
        /// 빌딩리스트 추가
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_BuildingList(Hashtable conditions)
        {
            DBManager.QueryForInsert("Insert_BuildingList", conditions);
        }

        /// <summary>
        /// 빌딩리스트 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_BuildingList(Hashtable conditions)
        {
            DBManager.QueryForDelete("Delete_BuildingList", conditions);
        }

        /// <summary>
        /// 빌딩리스트 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_BuildingList(Hashtable conditions)
        {
            DBManager.QueryForDelete("Update_BuildingList", conditions);
        }

        /// <summary>
        /// 빌딩리스트 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_UseBuildingList(Hashtable conditions)
        {
            DBManager.QueryForDelete("Update_UseBuildingList", conditions);
        }
    }
}
