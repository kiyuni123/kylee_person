﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using DevExpress.Xpf.Core;

using CMFramework.Common.Log;

namespace CMFramework.Common.MessageBox
{
    public class Messages
    {
        public static string MAPPER_DEFINE_ERROR = "정의되지 않은 코드입니다.";
        private static string strOkMsg = "정상적으로 처리되었습니다.";
        //private static string strErrMsg = "오류가 발생했습니다. \n담당자에게 문의바랍니다.";
        private static string strErrMsg = "로그 확인이 필요합니다.";

        /// <summary>
        /// 정상처리
        /// </summary>
        public static void ShowOkMsgBox()
        {
            DXMessageBox.Show(Messages.strOkMsg, "확인", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// YesNo 확인 처리
        /// </summary>
        /// <param name="srt"></param>
        /// <returns></returns>
        public static MessageBoxResult ShowYesNoMsgBox(string srt)
        {
            return DXMessageBox.Show(srt, "확인", MessageBoxButton.YesNo, MessageBoxImage.Question);
        }


        /// <summary>
        /// 에러처리 NoLoging
        /// </summary>
        public static void ShowErrMsgBox()
        {
            DXMessageBox.Show(Messages.strErrMsg, "오류", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// 에러처리 NoLoging, 전달Message 입력
        /// </summary>
        public static void ShowErrMsgBox(String str)
        {
            DXMessageBox.Show(str, "오류", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// 정보 전달Message 입력
        /// </summary>
        public static void ShowInfoMsgBox(String str)
        {
            DXMessageBox.Show(str, "확인", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// 에러처리 Loging
        /// </summary>
        /// <param name="e"></param>
        public static void ShowErrMsgBoxLog(Exception e)
        {
            Logs.ErrLogging(e);
            DXMessageBox.Show(Messages.strErrMsg, "오류", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// 에러처리 Loging, 전달Message 입력
        /// </summary>
        /// <param name="e"></param>
        /// <param name="strErrContent"></param>
        public static void ShowErrMsgBoxLog(Exception e, string str)
        {
            Logs.ErrLogging(e);
            DXMessageBox.Show(str, "오류", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// 에러처리 Loging
        /// </summary>
        /// <param name="e"></param>
        /// <param name="strErrContent"></param>
        public static void ErrLog(Exception e)
        {
            Logs.ErrLogging(e);
        }

    }



}
