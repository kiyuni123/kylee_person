﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace CMFramework.Common.Utils.Converters
{
    public class ExcelUtil
    {
        /// <summary>
        /// Excel데이터 DataTable 리턴
        /// </summary>
        /// <returns></returns>
        public static DataSet ExcelImport(string FilePath)
        {
            DataSet dataSetResult = new DataSet();

            try
            {
                Excel.Application excel = new Excel.Application();

                Excel.Workbook workbook = excel.Workbooks.Open(FilePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                for (int i = 0; i < workbook.Sheets.Count; i++)
                {
                    Excel.Worksheet worksheet = (Excel.Worksheet)workbook.Sheets.get_Item(i+1);

                    Excel.Range range = worksheet.UsedRange;

                    object[,] valueArray = (object[,])range.get_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault);

                    dataSetResult.Tables.Add(ProcessObjects(valueArray));
                }

                int hwnd;
                GetWindowThreadProcessId(excel.Hwnd, out hwnd);


                Process p = Process.GetProcessById(hwnd);
                p.Kill();

                return dataSetResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region ##Excel Import 필요 함수
        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

        /// <summary>
        /// object[,]를 DataTable로
        /// </summary>
        /// <param name="valueArray"></param>
        /// <returns></returns>
        private static DataTable ProcessObjects(object[,] valueArray)
        {
            DataTable dt = new DataTable();

            try
            {
                //컬럼이름 생성
                for (int k = 1; k <= valueArray.GetLength(1); k++)
                {
                    dt.Columns.Add((string)valueArray[1, k]);
                }

                //데이터 바인딩
                object[] singleDValue = new object[valueArray.GetLength(1)];

                for (int i = 2; i <= valueArray.GetLength(0); i++)
                {
                    for (int j = 0; j < valueArray.GetLength(1); j++)
                    {
                        if (valueArray[i, j + 1] != null)
                        {
                            singleDValue[j] = valueArray[i, j + 1].ToString();
                        }
                        else
                        {
                            singleDValue[j] = valueArray[i, j + 1];
                        }
                    }
                    dt.LoadDataRow(singleDValue, LoadOption.PreserveChanges);
                }

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        #endregion
    }
}
