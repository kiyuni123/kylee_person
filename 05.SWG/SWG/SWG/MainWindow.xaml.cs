﻿using CMFramework.Common.Utils.ViewEffect;
using DevExpress.Xpf.Core;
using SWG.Form.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWG
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool c = true;

        public static MainWindow mainform;

        public MainWindow()
        {
            InitializeComponent();

            ThemeApply.Themeapply(this);

            ucContentEconomics uc = new ucContentEconomics();
            //ucContentMain uc = new ucContentMain();
            ContentBinding(uc);

            mainform = this;
        }

        /// <summary>
        /// 팝업 띄울시 뒤화면 포커스 아웃
        /// </summary>
        public static void screenfade()
        {
            if(mainform.IsActive)
            {
                mainform.Splash.Visibility = Visibility.Hidden;
            }
            else
            {
                mainform.Splash.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// window 이동 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Title_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(e.ClickCount == 2)
            {
                Winmax_Click(null, null);
            }
            else
            {
                DragMove();
                e.Handled = true;
            }
        }

        /// <summary>
        /// 최소화 버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Winmin_Click(object sender, MouseButtonEventArgs e)
        {
            WindowState = System.Windows.WindowState.Minimized;
        }

        /// <summary>
        /// 최대화 버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Winmax_Click(object sender, MouseButtonEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
                imgMainmax.Source = new BitmapImage(new Uri("/Resources/Image/ic_Window_Max.png", UriKind.Relative));
            }
            else if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Maximized;
                imgMainmax.Source = new BitmapImage(new Uri("/Resources/Image/ic_Window_Resize.png", UriKind.Relative));
            }
        }

        /// <summary>
        /// 닫기 버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Click(object sender, MouseButtonEventArgs e)
        {
            if (DXMessageBox.Show("시스템을 종료합니다.", "SWG", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// 메뉴 Show Hiden 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuSH_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Storyboard sb;

                if (c)
                {
                    sb = FindResource("gridin") as Storyboard;

                    MenuAcc.CollapseAll();
                    MenuAcc.ExpandItemOnHeaderClick = false;
                }
                else
                {
                    sb = FindResource("gridout") as Storyboard;

                    MenuAcc.ExpandAll();
                    MenuAcc.ExpandItemOnHeaderClick = true;
                }

                sb.Begin(this);
                c = !c;
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// 컨텐트 아이템 바인딩
        /// </summary>
        /// <param name="uc"></param>
        private void ContentBinding(UserControl uc)
        {
            object content = ContentBox.Content;

            ContentBox.Content = uc;
        }

        #region 메뉴 선택 이벤트
        /// <summary>
        /// (메뉴)메인페이지
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentMain uc = new ucContentMain();
            ContentBinding(uc);
        }

        /// <summary>
        /// (메뉴)공정도
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_Process_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentProcess uc = new ucContentProcess();
            ContentBinding(uc);
        }

        /// <summary>
        /// (메뉴)위치도
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_Location_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentLocation uc = new ucContentLocation();
            ContentBinding(uc);
        }

        /// <summary>
        /// (메뉴)의사결정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_Decision_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        /// <summary>
        /// (메뉴)경제성 분석
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_Economics_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentEconomics uc = new ucContentEconomics();
            ContentBinding(uc);
        }

        /// <summary>
        /// (메뉴)운전설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_Operation_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentOperationSetting uc = new ucContentOperationSetting();
            ContentBinding(uc);
        }

        /// <summary>
        /// 계측기데이터
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_InstrumentData_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentInstrumentData uc = new ucContentInstrumentData();
            ContentBinding(uc);
        }

        /// <summary>
        /// 블랜딩
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_Blending_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentBlending uc = new ucContentBlending();
            ContentBinding(uc);
        }

        /// <summary>
        /// 원수
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_RawWater_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentRawWater uc = new ucContentRawWater();
            ContentBinding(uc);
        }

        /// <summary>
        /// 생산
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_Production_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentProduction uc = new ucContentProduction();
            ContentBinding(uc);
        }

        /// <summary>
        /// 전력소비량
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_Power_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentPower uc = new ucContentPower();
            ContentBinding(uc);
        }

        /// <summary>
        /// 블랜딩 및 생산 비교
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mn_B_P_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ucContentB_P uc = new ucContentB_P();
            ContentBinding(uc);
        }
        #endregion


    }
}
