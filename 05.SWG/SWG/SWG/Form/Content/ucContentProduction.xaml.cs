﻿using CMFramework.Common.MessageBox;
using DevExpress.Xpf.Charts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWG.Form.Content
{
    /// <summary>
    /// ucContentProduction.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucContentProduction : UserControl
    {
        public ucContentProduction()
        {
            InitializeComponent();
            Loaded += UcContentProduction_Loaded;
        }

        private void UcContentProduction_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        private void InitializeData()
        {
            try
            {
                DataTable dtcbGBNData = new DataTable();
                dtcbGBNData.Columns.Add("code");
                dtcbGBNData.Columns.Add("name");

                DataRow r1item = dtcbGBNData.NewRow();
                r1item["code"] = "code1";
                r1item["name"] = "일간";
                dtcbGBNData.Rows.Add(r1item);

                DataRow r2item = dtcbGBNData.NewRow();
                r2item["code"] = "code2";
                r2item["name"] = "주간";
                dtcbGBNData.Rows.Add(r2item);

                DataRow r3item = dtcbGBNData.NewRow();
                r3item["code"] = "code3";
                r3item["name"] = "월간";
                dtcbGBNData.Rows.Add(r3item);

                DataRow r4item = dtcbGBNData.NewRow();
                r4item["code"] = "code3";
                r4item["name"] = "년간";
                dtcbGBNData.Rows.Add(r4item);

                cbGBN.ItemsSource = dtcbGBNData;

                cbGBN.SelectedIndex = 0;
                dtStart.DateTime = DateTime.Now.AddDays(-7);
                dtEnd.DateTime = DateTime.Now;

                ///그리드 바인딩
                grid.ItemsSource = TimlyDemandData.GetDemandList();

                ///차트 바인딩
                chart.Diagram.Series.Add(new BarStackedSeries2D()
                {
                    Name = "RO",
                    Model = new BorderlessSimpleBar2DModel(),
                    ArgumentScaleType = ScaleType.Qualitative
                });

                chart.Diagram.Series.Add(new BarStackedSeries2D()
                {
                    Name = "UF",
                    Model = new BorderlessSimpleBar2DModel(),
                    ArgumentScaleType = ScaleType.Qualitative
                });

                foreach (TimlyDemandData item in TimlyDemandData.GetDemandList())
                {
                    chart.Diagram.Series[0].Points.Add(new SeriesPoint(item.strDate, Convert.ToInt32(item.strDemandValue)));
                    chart.Diagram.Series[0].DisplayName = "RO";
                    chart.Diagram.Series[0].ShowInLegend = true;

                    chart.Diagram.Series[1].Points.Add(new SeriesPoint(item.strDate, Convert.ToInt32(item.strResult)));
                    chart.Diagram.Series[1].DisplayName = "UF";
                    chart.Diagram.Series[1].ShowInLegend = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
