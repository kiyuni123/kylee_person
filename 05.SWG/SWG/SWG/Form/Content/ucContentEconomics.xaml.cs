﻿using CMFramework.Common.MessageBox;
using DevExpress.Xpf.Charts;
using DevExpress.Xpf.Grid;
using SWG.Form.Popup;
using SWG.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWG.Form.Content
{
    /// <summary>
    /// ucContentEconomics.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucContentEconomics : UserControl
    {
        SelectWork work = new SelectWork();
        DataTable dttemp = new DataTable();
        DataTable dtresult1 = new DataTable();
        DataTable dtresult2 = new DataTable();
        DataTable dtresult3 = new DataTable();
        DataTable dtresult4 = new DataTable();

        public ucContentEconomics()
        {
            InitializeComponent();
            Loaded += UcContentEconomics_Loaded;
        }

        private void UcContentEconomics_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeEvent();
            InitializeData();
        }

        private void InitializeEvent()
        {
            dtStart.EditValueChanged += dtGap_EditValueChanged;
            dtEnd.EditValueChanged += dtGap_EditValueChanged;
            btnhrSetting.Click += BtnhrSetting_Click;
            btnTime.Click += Btntest_Click;
            btnRunSelect.Click += BtnRunSelect_Click;
        }

        private void BtnRunSelect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRow[] dr;
                dr = null;

                if (radiogbn1.IsChecked == true)
                {
                    dr = dttemp.Select("GBN = '2'");
                }
                if (radiogbn2.IsChecked == true)
                {
                    dr = dttemp.Select("GBN = '3'");
                }
                if (radiogbn3.IsChecked == true)
                {
                    dr = dttemp.Select("GBN = '4'");
                }
                if (radiogbn4.IsChecked == true)
                {
                    dr = dttemp.Select("GBN = '5'");
                }

                if (dr != null)
                {
                    Hashtable conditions = new Hashtable();
                    conditions.Add("SET_SEQ", dr[0]["SET_SEQ"].ToString());
                    conditions.Add("MSRC", dr[0]["MSRC"].ToString());
                    conditions.Add("GBN", dr[0]["GBN"].ToString());
                    conditions.Add("RANK", dr[0]["RANK"].ToString());
                    work.Update_ANALS_RESULT_BLENDING_RUNYN_N(conditions);
                    work.Update_ANALS_RESULT_BLENDING_RUNYN_Y(conditions);

                    Messages.ShowOkMsgBox();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Btntest_Click(object sender, RoutedEventArgs e)
        {
            popupRunTimeSetting popup = new popupRunTimeSetting();
            popup.ShowDialog();
        }

        private void BtnhrSetting_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;

                //시작날짜
                DateTime dtAnalStart = new DateTime(dtStart.DateTime.Year, dtStart.DateTime.Month, dtStart.DateTime.Day);
                //종료날짜
                DateTime dtAnalEnd = new DateTime(dtEnd.DateTime.Year, dtEnd.DateTime.Month, dtEnd.DateTime.Day);
                //운전일수
                int intrunDay = Convert.ToInt32(lbDayGap.Content);
                //일일 생산요구량
                int intdayFlw = Convert.ToInt32(spinDAYFLW.EditValue.ToString());

                Anals.Anals anal = new Anals.Anals();

                Hashtable inputconditions = new Hashtable();
                inputconditions.Add("SEAWTR_TDS", "28000");
                inputconditions.Add("UGRWTR_TDS", "1000");
                inputconditions.Add("BRKWTR_TDS", "500");
                inputconditions.Add("GRDWTR_TDS", "30");
                inputconditions.Add("SEAWTR_HOLD", "100");
                inputconditions.Add("UGRWTR_HOLD", "20");
                inputconditions.Add("BRKWTR_HOLD", "80");
                inputconditions.Add("GRDWTR_HOLD", "20");

                Hashtable htresult = new Hashtable();
                htresult = anal.Run(dtAnalStart, dtAnalEnd, intrunDay, intdayFlw, inputconditions);
                SelectGrid();

                if (htresult == null)
                {
                    this.Cursor = Cursors.Arrow;
                    return;
                }
                else
                {
                    this.Cursor = Cursors.Arrow;
                    Messages.ShowOkMsgBox();
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// 초기화 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtGap_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (dtEnd.DateTime < dtStart.DateTime)
            {
                Messages.ShowInfoMsgBox("날짜 기간이 잘못 되었습니다.");
                return;
            }

            TimeSpan tsGap = dtEnd.DateTime - dtStart.DateTime;

            lbDayGap.Content = tsGap.Days.ToString();
        }

        /// <summary>
        /// 해수 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBand_Click(object sender, RoutedEventArgs e)
        {
            PopupROResultDTL popupROResultDTL;

            switch (((Button)sender).Name)
            {
                case "btnehead":
                    if (dtresult3.Rows.Count != 0)
                    {
                        popupROResultDTL = new PopupROResultDTL(dtresult3.Rows[0]["ecol1"].ToString(), "2");
                        popupROResultDTL.Closed += PopupROResultDTL_Closed;
                        popupROResultDTL.ShowDialog();
                    }
                    break;

                case "btnsamhead":
                    if (dtresult3.Rows.Count != 0)
                    {
                        popupROResultDTL = new PopupROResultDTL(dtresult3.Rows[0]["ecol1"].ToString(), "3");
                        popupROResultDTL.Closed += PopupROResultDTL_Closed;
                        popupROResultDTL.ShowDialog();
                    }
                    break;

                case "btnsahead":
                    Messages.ShowInfoMsgBox("사중");
                    break;

                case "btncustom":
                    if (dtresult3.Rows.Count != 0)
                    {
                        popupCustomSetting popup = new popupCustomSetting(dtresult3.Rows[0]["customcol1"].ToString(), "5");
                        popup.Closed += PopupROResultDTL_Closed;
                        popup.ShowDialog();
                    }

                    break;
            }
        }

        private void PopupROResultDTL_Closed(object sender, EventArgs e)
        {
            SelectGrid();
        }

        /// <summary>
        /// 초기화 데이터
        /// </summary>
        private void InitializeData()
        {
            dtStart.DateTime = DateTime.Now.AddDays(-1);
            dtEnd.DateTime = DateTime.Now;

            ///차트 바인딩
            #region Chart1
            chart1.Diagram.Series.Add(new BarStackedSeries2D()
            {
                Name = "RO",
                Model = new BorderlessSimpleBar2DModel(),
                ArgumentScaleType = ScaleType.Qualitative
            });

            chart1.Diagram.Series.Add(new BarStackedSeries2D()
            {
                Name = "UF",
                Model = new BorderlessSimpleBar2DModel(),
                ArgumentScaleType = ScaleType.Qualitative
            });

            foreach (TimlyDemandData item in TimlyDemandData.GetDemandList())
            {
                chart1.Diagram.Series[0].Points.Add(new SeriesPoint(item.strDate, Convert.ToInt32(item.strDemandValue)));
                chart1.Diagram.Series[0].DisplayName = "RO";
                chart1.Diagram.Series[0].ShowInLegend = true;

                chart1.Diagram.Series[1].Points.Add(new SeriesPoint(item.strDate, Convert.ToInt32(item.strResult)));
                chart1.Diagram.Series[1].DisplayName = "UF";
                chart1.Diagram.Series[1].ShowInLegend = true;
            }
            #endregion

            #region Chart2
            chart2.Diagram.Series.Add(new BarSideBySideSeries2D()
            {
                Name = "RO",
                Model = new BorderlessSimpleBar2DModel(),
                ArgumentScaleType = ScaleType.Qualitative
            });

            chart2.Diagram.Series.Add(new BarSideBySideSeries2D()
            {
                Name = "UF",
                Model = new BorderlessSimpleBar2DModel(),
                ArgumentScaleType = ScaleType.Qualitative
            });

            foreach (TimlyDemandData item in TimlyDemandData.GetDemandList())
            {
                chart2.Diagram.Series[0].Points.Add(new SeriesPoint(item.strDate, Convert.ToInt32(item.strDemandValue)));
                chart2.Diagram.Series[0].DisplayName = "RO";
                chart2.Diagram.Series[0].ShowInLegend = true;

                chart2.Diagram.Series[1].Points.Add(new SeriesPoint(item.strDate, Convert.ToInt32(item.strResult)));
                chart2.Diagram.Series[1].DisplayName = "UF";
                chart2.Diagram.Series[1].ShowInLegend = true;
            }
            #endregion

            SelectGrid();
        }

        public void SelectGrid()
        {
            try
            {
                dtresult1 = null;
                dtresult1 = new DataTable();
                dtresult1.Columns.Add("col1");
                dtresult1.Columns.Add("col2");
                dtresult1.Columns.Add("ecol1");
                dtresult1.Columns.Add("ecol2");
                dtresult1.Columns.Add("samcol1");
                dtresult1.Columns.Add("samcol2");
                dtresult1.Columns.Add("sacol1");
                dtresult1.Columns.Add("sacol2");
                dtresult1.Columns.Add("customcol1");
                dtresult1.Columns.Add("customcol2");

                dtresult2 = null;
                dtresult2 = new DataTable();
                dtresult2.Columns.Add("col1");
                dtresult2.Columns.Add("col2");
                dtresult2.Columns.Add("ecol1");
                dtresult2.Columns.Add("samcol1");
                dtresult2.Columns.Add("sacol1");
                dtresult2.Columns.Add("customcol1");

                dtresult3 = null;
                dtresult3 = new DataTable();
                dtresult3.Columns.Add("col1");
                dtresult3.Columns.Add("col2");
                dtresult3.Columns.Add("ecol1");
                dtresult3.Columns.Add("samcol1");
                dtresult3.Columns.Add("sacol1");
                dtresult3.Columns.Add("customcol1");

                dtresult4 = null;
                dtresult4 = new DataTable();
                dtresult4.Columns.Add("col1");
                dtresult4.Columns.Add("col2");
                dtresult4.Columns.Add("ecol1");
                dtresult4.Columns.Add("samcol1");
                dtresult4.Columns.Add("sacol1");
                dtresult4.Columns.Add("customcol1");

                DataTable dtresult = new DataTable();

                dtresult = work.Select_Economics(null);

                dttemp = dtresult.DefaultView.ToTable(true, "SET_SEQ", "MSRC", "RANK", "GBN", "B_TDS", "UF_MSRC", "UF_FILTER", "UF_TIME", "UF_FLUX", "RO_MSRC", "RO_PRESSR", "RO_FLUX", "RO_CTL", "RO_TOTHR", "RO_TOTMIN", "ER_UF", "ER_RO", "ER_TOT", "USE_YN", "RUN_YN");

                DataRow[] dr;

                DataRow drdrow;
                DataRow drtrow;
                DataRow drqrow;
                DataRow drCustomrow;

                dr = dttemp.Select("GBN = '2'");
                if (dr.Length == 1)
                    drdrow = dr[0];
                else
                    return;
                dr = dttemp.Select("GBN = '3'");
                if (dr.Length == 1)
                    drtrow = dr[0];
                else
                    return;
                dr = dttemp.Select("GBN = '4'");
                if (dr.Length == 1)
                    drqrow = dr[0];
                else
                    return;
                dr = dttemp.Select("GBN = '5'");
                if (dr.Length == 1)
                    drCustomrow = dr[0];
                else
                    drCustomrow = null;


                #region dtresult1
                DataRow r1 = dtresult1.NewRow();
                DataRow r2 = dtresult1.NewRow();
                DataRow r3 = dtresult1.NewRow();
                DataRow r4 = dtresult1.NewRow();
                DataRow r5 = dtresult1.NewRow();

                r1["col1"] = "Blending";
                r1["col2"] = "Blending 원수 TDS";
                r1["ecol1"] = drdrow["B_TDS"].ToString();
                r1["ecol2"] = drdrow["B_TDS"].ToString();
                r1["samcol1"] = drtrow["B_TDS"].ToString();
                r1["samcol2"] = drtrow["B_TDS"].ToString();
                r1["sacol1"] = drqrow["B_TDS"].ToString();
                r1["sacol2"] = drqrow["B_TDS"].ToString();
                if (drCustomrow != null)
                {
                    r1["customcol1"] = drCustomrow["B_TDS"].ToString();
                    r1["customcol2"] = drCustomrow["B_TDS"].ToString();
                }

                dtresult1.Rows.Add(r1);

                r2["col1"] = "Blending";
                r2["col2"] = "4순위";
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '2' AND DTL_RANK = '4' AND MSRC='" + drdrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r2["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r2["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '3' AND DTL_RANK = '4' AND MSRC='" + drtrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r2["samcol1"] = dr[0]["KIND_NM"].ToString();
                    r2["samcol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '4' AND DTL_RANK = '4' AND MSRC='" + drqrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r2["sacol1"] = dr[0]["KIND_NM"].ToString();
                    r2["sacol2"] = dr[0]["RUN_RATE"].ToString();
                }
                if (drCustomrow != null)
                {
                    dr = dtresult.Select("USE_YN = 'Y' AND GBN = '5' AND DTL_RANK = '4' AND MSRC='" + drCustomrow["MSRC"] + "'", "B_TDS");
                    if (dr.Length != 0)
                    {
                        r2["customcol1"] = dr[0]["KIND_NM"].ToString();
                        r2["customcol2"] = dr[0]["RUN_RATE"].ToString();
                    }
                }

                dtresult1.Rows.Add(r2);

                r3["col1"] = "Blending";
                r3["col2"] = "3순위";
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '2' AND DTL_RANK = '3' AND MSRC='" + drdrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r3["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r3["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '3' AND DTL_RANK = '3' AND MSRC='" + drtrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r3["samcol1"] = dr[0]["KIND_NM"].ToString();
                    r3["samcol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '4' AND DTL_RANK = '3' AND MSRC='" + drqrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r3["sacol1"] = dr[0]["KIND_NM"].ToString();
                    r3["sacol2"] = dr[0]["RUN_RATE"].ToString();
                }
                if (drCustomrow != null)
                {
                    dr = dtresult.Select("USE_YN = 'Y' AND GBN = '5' AND DTL_RANK = '3' AND MSRC='" + drCustomrow["MSRC"] + "'", "B_TDS");
                    if (dr.Length != 0)
                    {
                        r3["customcol1"] = dr[0]["KIND_NM"].ToString();
                        r3["customcol2"] = dr[0]["RUN_RATE"].ToString();
                    }
                }
                dtresult1.Rows.Add(r3);

                r4["col1"] = "Blending";
                r4["col2"] = "2순위";
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '2' AND DTL_RANK = '2' AND MSRC='" + drdrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r4["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r4["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '3' AND DTL_RANK = '2' AND MSRC='" + drtrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r4["samcol1"] = dr[0]["KIND_NM"].ToString();
                    r4["samcol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '4' AND DTL_RANK = '2' AND MSRC='" + drqrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r4["sacol1"] = dr[0]["KIND_NM"].ToString();
                    r4["sacol2"] = dr[0]["RUN_RATE"].ToString();
                }
                if (drCustomrow != null)
                {
                    dr = dtresult.Select("USE_YN = 'Y' AND GBN = '5' AND DTL_RANK = '2' AND MSRC='" + drCustomrow["MSRC"] + "'", "B_TDS");
                    if (dr.Length != 0)
                    {
                        r4["customcol1"] = dr[0]["KIND_NM"].ToString();
                        r4["customcol2"] = dr[0]["RUN_RATE"].ToString();
                    }
                }
                dtresult1.Rows.Add(r4);

                r5["col1"] = "Blending";
                r5["col2"] = "1순위";
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '2' AND DTL_RANK = '1' AND MSRC='" + drdrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r5["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r5["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '3' AND DTL_RANK = '1' AND MSRC='" + drtrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r5["samcol1"] = dr[0]["KIND_NM"].ToString();
                    r5["samcol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dr = dtresult.Select("USE_YN = 'Y' AND GBN = '4' AND DTL_RANK = '1' AND MSRC='" + drqrow["MSRC"] + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r5["sacol1"] = dr[0]["KIND_NM"].ToString();
                    r5["sacol2"] = dr[0]["RUN_RATE"].ToString();
                }
                if (drCustomrow != null)
                {
                    dr = dtresult.Select("USE_YN = 'Y' AND GBN = '5' AND DTL_RANK = '1' AND MSRC='" + drCustomrow["MSRC"] + "'", "B_TDS");
                    if (dr.Length != 0)
                    {
                        r5["customcol1"] = dr[0]["KIND_NM"].ToString();
                        r5["customcol2"] = dr[0]["RUN_RATE"].ToString();
                    }
                }
                dtresult1.Rows.Add(r5);
                #endregion

                #region dtresult2
                DataRow r6 = dtresult2.NewRow();
                DataRow r7 = dtresult2.NewRow();
                DataRow r8 = dtresult2.NewRow();
                DataRow r9 = dtresult2.NewRow();
                DataRow r10 = dtresult2.NewRow();

                r6["col1"] = "UF운전조건";
                r6["col2"] = "UF 운전 조건 (UF1/UF2)";
                r6["ecol1"] = drdrow["UF_MSRC"].ToString();
                r6["samcol1"] = drtrow["UF_MSRC"].ToString();
                r6["sacol1"] = drqrow["UF_MSRC"].ToString();
                if (drCustomrow != null)
                    r6["customcol1"] = drCustomrow["UF_MSRC"].ToString();
                dtresult2.Rows.Add(r6);

                r7["col1"] = "UF운전조건";
                r7["col2"] = "여과시간(min)";
                r7["ecol1"] = drdrow["UF_FILTER"].ToString();
                r7["samcol1"] = drtrow["UF_FILTER"].ToString();
                r7["sacol1"] = drqrow["UF_FILTER"].ToString();
                if (drCustomrow != null)
                    r7["customcol1"] = drCustomrow["UF_FILTER"].ToString();
                dtresult2.Rows.Add(r7);

                r8["col1"] = "UF운전조건";
                r8["col2"] = "역세시간(min)";
                r8["ecol1"] = drdrow["UF_TIME"].ToString();
                r8["samcol1"] = drtrow["UF_TIME"].ToString();
                r8["sacol1"] = drqrow["UF_TIME"].ToString();
                if (drCustomrow != null)
                    r8["customcol1"] = drCustomrow["UF_TIME"].ToString();
                dtresult2.Rows.Add(r8);

                r9["col1"] = "UF운전조건";
                r9["col2"] = "여과 Flux(lmh)";
                r9["ecol1"] = drdrow["UF_FLUX"].ToString();
                r9["samcol1"] = drtrow["UF_FLUX"].ToString();
                r9["sacol1"] = drqrow["UF_FLUX"].ToString();
                if (drCustomrow != null)
                    r9["customcol1"] = drCustomrow["UF_FLUX"].ToString();
                dtresult2.Rows.Add(r9);

                r10["col1"] = "UF운전조건";
                r10["col2"] = "UF 전력소비량(kwh/㎥)";
                r10["ecol1"] = drdrow["ER_UF"].ToString();
                r10["samcol1"] = drtrow["ER_UF"].ToString();
                r10["sacol1"] = drqrow["ER_UF"].ToString();
                if (drCustomrow != null)
                    r10["customcol1"] = drCustomrow["ER_UF"].ToString();
                dtresult2.Rows.Add(r10);
                #endregion

                #region dtresult3
                DataRow r11 = dtresult3.NewRow();
                DataRow r12 = dtresult3.NewRow();
                DataRow r13 = dtresult3.NewRow();
                DataRow r14 = dtresult3.NewRow();
                DataRow r15 = dtresult3.NewRow();
                DataRow r16 = dtresult3.NewRow();
                DataRow r17 = dtresult3.NewRow();

                r11["col1"] = "RO운전조건";
                r11["col2"] = "운전조건(SWRO/BWRO)";
                r11["ecol1"] = drdrow["RO_MSRC"].ToString();
                r11["samcol1"] = drtrow["RO_MSRC"].ToString();
                r11["sacol1"] = drqrow["RO_MSRC"].ToString();
                if (drCustomrow != null)
                    r11["customcol1"] = drCustomrow["RO_MSRC"].ToString();
                dtresult3.Rows.Add(r11);

                r12["col1"] = "RO운전조건";
                r12["col2"] = "운전압력(bar)";
                r12["ecol1"] = drdrow["RO_PRESSR"].ToString();
                r12["samcol1"] = drtrow["RO_PRESSR"].ToString();
                r12["sacol1"] = drqrow["RO_PRESSR"].ToString();
                if (drCustomrow != null)
                    r12["customcol1"] = drCustomrow["RO_PRESSR"].ToString();
                dtresult3.Rows.Add(r12);

                r13["col1"] = "RO운전조건";
                r13["col2"] = "예상 Flux(lmh)";
                r13["ecol1"] = drdrow["RO_FLUX"].ToString();
                r13["samcol1"] = drtrow["RO_FLUX"].ToString();
                r13["sacol1"] = drqrow["RO_FLUX"].ToString();
                if (drCustomrow != null)
                    r13["customcol1"] = drCustomrow["RO_FLUX"].ToString();
                dtresult3.Rows.Add(r13);

                r14["col1"] = "RO운전조건";
                r14["col2"] = "컨트롤벨브";
                r14["ecol1"] = drdrow["RO_CTL"].ToString();
                r14["samcol1"] = drtrow["RO_CTL"].ToString();
                r14["sacol1"] = drqrow["RO_CTL"].ToString();
                if (drCustomrow != null)
                    r14["customcol1"] = drCustomrow["RO_CTL"].ToString();
                dtresult3.Rows.Add(r14);

                r15["col1"] = "RO운전조건";
                r15["col2"] = "총운전시간 (hr)";
                r15["ecol1"] = drdrow["RO_TOTHR"].ToString();
                r15["samcol1"] = drtrow["RO_TOTHR"].ToString();
                r15["sacol1"] = drqrow["RO_TOTHR"].ToString();
                if (drCustomrow != null)
                    r15["customcol1"] = drCustomrow["RO_TOTHR"].ToString();
                dtresult3.Rows.Add(r15);

                r16["col1"] = "RO운전조건";
                r16["col2"] = "총운전시간 (min)";
                r16["ecol1"] = drdrow["RO_TOTMIN"].ToString();
                r16["samcol1"] = drtrow["RO_TOTMIN"].ToString();
                r16["sacol1"] = drqrow["RO_TOTMIN"].ToString();
                if (drCustomrow != null)
                    r16["customcol1"] = drCustomrow["RO_TOTMIN"].ToString();
                dtresult3.Rows.Add(r16);

                r17["col1"] = "RO운전조건";
                r17["col2"] = "RO 전력소비량(kwh/㎥)";
                r17["ecol1"] = drdrow["ER_RO"].ToString();
                r17["samcol1"] = drtrow["ER_RO"].ToString();
                r17["sacol1"] = drqrow["ER_RO"].ToString();
                if (drCustomrow != null)
                    r17["customcol1"] = drCustomrow["ER_RO"].ToString();
                dtresult3.Rows.Add(r17);
                #endregion

                #region dtresult4
                DataRow r18 = dtresult4.NewRow();
                r18["col1"] = "총에너지소비량";
                r18["col2"] = "에너지소비량 (UF+RO)";
                r18["ecol1"] = drdrow["ER_TOT"].ToString();
                r18["samcol1"] = drtrow["ER_TOT"].ToString();
                r18["sacol1"] = drqrow["ER_TOT"].ToString();
                if (drCustomrow != null)
                    r18["customcol1"] = drCustomrow["ER_TOT"].ToString();
                dtresult4.Rows.Add(r18);
                #endregion

                grid1.ItemsSource = dtresult1;
                grid2.ItemsSource = dtresult2;
                grid3.ItemsSource = dtresult3;
                grid4.ItemsSource = dtresult4;

                dr = dttemp.Select("RUN_YN = 'Y'");
                if (dr.Length == 1)
                {
                    string runGBN = dr[0]["GBN"].ToString();

                    if (runGBN.Equals("2"))
                        radiogbn1.IsChecked = true;
                    if (runGBN.Equals("3"))
                        radiogbn2.IsChecked = true;
                    if (runGBN.Equals("4"))
                        radiogbn3.IsChecked = true;
                    if (runGBN.Equals("5"))
                        radiogbn4.IsChecked = true;
                }
                else
                {
                    radiogbn1.IsChecked = false;
                    radiogbn2.IsChecked = false;
                    radiogbn3.IsChecked = false;
                    radiogbn4.IsChecked = false;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
