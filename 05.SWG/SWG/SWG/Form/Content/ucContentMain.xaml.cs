﻿using CMFramework.Common.MessageBox;
using DevExpress.Xpf.Charts;
using DevExpress.Xpf.LayoutControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace SWG.Form.Content
{
    /// <summary>
    /// ucContentMain.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucContentMain : UserControl
    {
        public ucContentMain()
        {
            InitializeComponent();
            Loaded += UcContentMain_Loaded;
        }

        private void UcContentMain_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeData();
            //testImage.MouseLeftButtonDown += TestImage_MouseLeftButtonDown;
        }

        private void InitializeData()
        {
            try
            {
                //데이터 Grid 생성
                DataTable dtmainGrid = new DataTable();

                dtmainGrid.Columns.Add("GBN");
                dtmainGrid.Columns.Add("NAME");
                dtmainGrid.Columns.Add("FL");
                dtmainGrid.Columns.Add("PRI");
                dtmainGrid.Columns.Add("LEV");

                Random rnd = new Random();

                for (int i = 0; i < 12; i++)
                {
                    DataRow r = dtmainGrid.NewRow();
                    r["GBN"] = "정수장";
                    r["NAME"] = "지점" + (i+1).ToString();
                    r["FL"] = rnd.Next(1, 100).ToString();
                    r["PRI"] = rnd.Next(1, 5).ToString();
                    r["LEV"] = rnd.Next(1, 3).ToString();
                    dtmainGrid.Rows.Add(r);
                }

                mainGrid.ItemsSource = dtmainGrid;

                ctTimlyFore.DataSource = TimlyDemandData.GetDemandList();
                ctbarTimlyFore.DataSource = TimlyDemandData.GetDemandList();
                ctareaTimlyFore.DataSource = TimlyDemandData.GetDemandList();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }

    #region TEST 진행
    //public class PercentageConverter : IValueConverter
    //{
    //    public object Convert(object value,
    //        Type targetType,
    //        object parameter,
    //        System.Globalization.CultureInfo culture)
    //    {
    //        return System.Convert.ToDouble(value) *
    //               System.Convert.ToDouble(parameter);
    //    }

    //    public object ConvertBack(object value,
    //        Type targetType,
    //        object parameter,
    //        System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //} 
    #endregion
}
