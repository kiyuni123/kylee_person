﻿using CMFramework.Common.MessageBox;
using DevExpress.Map;
using DevExpress.Map.Native;
using DevExpress.Xpf.Map;
using SWG.Form.Usercontrol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWG.Form.Content
{
    /// <summary>
    /// ucContentLocation.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucContentLocation : UserControl
    {
        public ucContentLocation()
        {
            InitializeComponent();
            Loaded += UcContentLocation_Loaded;
        }

        private void UcContentLocation_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeMarker();
            InitializeUserControlAdd();
        }

        private void InitializeMarker()
        {
            try
            {
                MapPushpin pin1 = new MapPushpin()
                {
                    Text = "해수",
                    Location = new GeoPoint(37.8293, 124.7137),
                    MarkerTemplate = (DataTemplate)Resources["markerTemplate"],
                    Template = (ControlTemplate)Resources["pushpinTemplate"],
                    Brush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#F6453B")),
                    
                };

                //벡터 포멧 확인
                MapPushpin pin2 = new MapPushpin()
                {
                    Text = "우수",
                    Location = new GeoPoint(37.825, 124.7168),
                    MarkerTemplate = (DataTemplate)Resources["markerTemplate"],
                    Template = (ControlTemplate)Resources["pushpinTemplate"],
                    Brush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#3B8AF6")),
                };

                MapPushpin pin3 = new MapPushpin()
                {
                    Text = "지하수",
                    Location = new GeoPoint(37.815, 124.717),
                    MarkerTemplate = (DataTemplate)Resources["markerTemplate"],
                    Template = (ControlTemplate)Resources["pushpinTemplate"],
                    Brush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#F9A401")),
                };

                storage.Items.Add(pin1);
                storage.Items.Add(pin2);
                storage.Items.Add(pin3);
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void InitializeUserControlAdd()
        {
            try
            {
                if (storage.Items != null)
                {
                    foreach (MapPushpin mpinitem in storage.Items)
                    {
                        //1.지점이름을 입력하기
                        //2.지점UserControl 등록하고 추가하기
                        Hashtable htParam = new Hashtable();
                        htParam.Add("Name", mpinitem.Text);
                        ucValuelookupLocation ucvaluelookupLocation = new ucValuelookupLocation(htParam);
                        Addusercontrol.RegisterName(mpinitem.Text + "_value", ucvaluelookupLocation);
                        Addusercontrol.Children.Add(ucvaluelookupLocation);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
