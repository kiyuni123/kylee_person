﻿using CMFramework.Common.MessageBox;
using DevExpress.Xpf.Charts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWG.Form.Content
{
    /// <summary>
    /// ucContentB_P.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucContentB_P : UserControl
    {
        public ucContentB_P()
        {
            InitializeComponent();
            Loaded += UcContentBlending_Loaded;
        }

        private void UcContentBlending_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        private void InitializeData()
        {
            try
            {
                dtStart.DateTime = DateTime.Now.AddDays(-7);
                dtEnd.DateTime = DateTime.Now;

                grid.ItemsSource = TimlyDemandData.GetDemandList();

                chart.Diagram.Series.Add(new BarSideBySideSeries2D()
                {
                    Name = "series1",
                    Model = new BorderlessSimpleBar2DModel(),
                    ArgumentScaleType = ScaleType.Qualitative
                });

                chart.Diagram.Series.Add(new BarSideBySideSeries2D()
                {
                    Name = "series2",
                    Model = new BorderlessSimpleBar2DModel(),
                    ArgumentScaleType = ScaleType.Qualitative
                });

                chart.Diagram.Series.Add(new BarSideBySideSeries2D()
                {
                    Name = "series3",
                    Model = new BorderlessSimpleBar2DModel(),
                    ArgumentScaleType = ScaleType.Qualitative
                });

                foreach (TimlyDemandData item in TimlyDemandData.GetDemandList())
                {
                    chart.Diagram.Series[0].Points.Add(new SeriesPoint(item.strDate, Convert.ToInt32(item.strDemandValue)));
                    chart.Diagram.Series[0].DisplayName = "series1";
                    chart.Diagram.Series[0].ShowInLegend = true;

                    chart.Diagram.Series[1].Points.Add(new SeriesPoint(item.strDate, Convert.ToInt32(item.strResult)));
                    chart.Diagram.Series[1].DisplayName = "series2";
                    chart.Diagram.Series[1].ShowInLegend = true;

                    chart.Diagram.Series[2].Points.Add(new SeriesPoint(item.strDate, Convert.ToInt32(item.strAllDemandValue)));
                    chart.Diagram.Series[2].DisplayName = "series3";
                    chart.Diagram.Series[2].ShowInLegend = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
