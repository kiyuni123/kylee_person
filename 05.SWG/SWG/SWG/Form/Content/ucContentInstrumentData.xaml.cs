﻿using CMFramework.Common.MessageBox;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWG.Form.Content
{
    /// <summary>
    /// ucContentInstrumentData.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucContentInstrumentData : UserControl
    {
        public ucContentInstrumentData()
        {
            InitializeComponent();
            Loaded += UcContentInstrumentData_Loaded;
        }

        private void UcContentInstrumentData_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        private void InitializeData()
        {
            try
            {
                DataTable dtcbGBNData = new DataTable();
                dtcbGBNData.Columns.Add("code");
                dtcbGBNData.Columns.Add("name");

                DataRow r1item = dtcbGBNData.NewRow();
                r1item["code"] = "code1";
                r1item["name"] = "지하수위";
                dtcbGBNData.Rows.Add(r1item);

                DataRow r2item = dtcbGBNData.NewRow();
                r2item["code"] = "code2";
                r2item["name"] = "우수";
                dtcbGBNData.Rows.Add(r2item);

                DataRow r3item = dtcbGBNData.NewRow();
                r3item["code"] = "code3";
                r3item["name"] = "기저";
                dtcbGBNData.Rows.Add(r3item);

                DataRow r4item = dtcbGBNData.NewRow();
                r4item["code"] = "code4";
                r4item["name"] = "생산가능수량";
                dtcbGBNData.Rows.Add(r4item);

                cbGBN.ItemsSource = dtcbGBNData;

                cbGBN.SelectedIndex = 0;
                dtStart.DateTime = DateTime.Now.AddDays(-7);
                dtEnd.DateTime = DateTime.Now;

                chart.DataSource = TimlyDemandData.GetDemandList();
                grid.ItemsSource = TimlyDemandData.GetDemandList();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
