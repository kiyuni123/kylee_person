﻿using CMFramework.Common.MessageBox;
using CMFramework.Common.Utils.Converters;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using SWG.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWG.Form.Content
{
    /// <summary>
    /// ucContentOperationSetting.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucContentOperationSetting : UserControl
    {
        OperationSettingWork work = new OperationSettingWork();
        //펌프 초기화 플래그
        bool binitPump = false;
        //UF 초기화 플래그
        bool binitUF = false;
        //UF 초기화 플래그
        bool binitSWBW = false;
        //SWBW 설정 초기화 플래그
        bool binitSWBWSetting = false;
        //기본설정 초기화 플래그
        bool binitBaseSetting = false;

        public ucContentOperationSetting()
        {
            InitializeComponent();
            Loaded += UcContentOperationSetting_Loaded;
        }

        private void UcContentOperationSetting_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeData();
            InitializeEvent();
        }

        private void InitializeEvent()
        {
            //펌프이벤트 등록
            btnPumpDown.Click += BtnDown_Click;
            btnPumpUpload.Click += BtnUpload_Click;
            btnPumpInit.Click += BtnInit_Click;
            btnPumpDel.Click += BtnDel_Click;
            btnPumpSave.Click += BtnSave_Click;
            gridPUMP.SelectedItemChanged += GridPUMP_SelectedItemChanged;
            gridPUMP.Loaded += Grid_Loaded;


            btnUFDown.Click += BtnDown_Click;
            btnUFUpload.Click += BtnUpload_Click;
            btnUFInit.Click += BtnInit_Click;
            btnUFDel.Click += BtnDel_Click;
            btnUFSave.Click += BtnSave_Click;
            gridUF.SelectedItemChanged += GridPUMP_SelectedItemChanged;
            gridUF.Loaded += Grid_Loaded;

            btnSWBWSettingDown.Click += BtnDown_Click;
            btnSWBWSettingUpload.Click += BtnUpload_Click;
            btnSWBWSettingInit.Click += BtnInit_Click;
            btnSWBWSettingDel.Click += BtnDel_Click;
            btnSWBWSettingSave.Click += BtnSave_Click;
            gridSWBWSetting.SelectedItemChanged += GridPUMP_SelectedItemChanged;
            gridSWBWSetting.Loaded += Grid_Loaded;

            btnSWBWDown.Click += BtnDown_Click;
            btnSWBWUpload.Click += BtnUpload_Click;
            btnSWBWInit.Click += BtnInit_Click;
            btnSWBWDel.Click += BtnDel_Click;
            btnSWBWSave.Click += BtnSave_Click;
            gridSWBW.SelectedItemChanged += GridPUMP_SelectedItemChanged;
            gridSWBW.Loaded += Grid_Loaded;

            btnBaseInit.Click += BtnBaseInit_Click;
            btnBaseSave.Click += BtnBaseSave_Click;
        }

        private void BtnBaseSave_Click(object sender, RoutedEventArgs e)
        {
            #region 기본설정 저장
            if (true)
            {
                //ANALS_BASE_SETTING
                //파라미터 정리
                Hashtable conditions = new Hashtable();
                conditions.Add("SEAWTR_TDS", DBNull.Value);
                conditions.Add("UGRWTR_TDS", DBNull.Value);
                conditions.Add("BRKWTR_TDS", DBNull.Value);
                conditions.Add("GRDWTR_TDS", DBNull.Value);
                conditions.Add("SEAWTR_HOLD", DBNull.Value);
                conditions.Add("UGRWTR_HOLD", DBNull.Value);
                conditions.Add("BRKWTR_HOLD", DBNull.Value);
                conditions.Add("GRDWTR_HOLD", DBNull.Value);

                //보존율
                conditions.Add("SEAWTR_PRSRV", spinSEA_PRSRV.EditValue.ToString());
                conditions.Add("UGRWTR_PRSRV", spinUGR_PRSRV.EditValue.ToString());
                conditions.Add("BRKWTR_PRSRV", spinBRK_PRSRV.EditValue.ToString());
                conditions.Add("GRDWTR_PRSRV", spinGDR_PRSRV.EditValue.ToString());
                //최소사용율
                conditions.Add("SEAWTR_MUMMVAL", spinSEA_MUMMVAL.EditValue.ToString());
                conditions.Add("UGRWTR_MUMMVAL", spinUGR_MUMMVAL.EditValue.ToString());
                conditions.Add("BRKWTR_MUMMVAL", spinBRK_MUMMVAL.EditValue.ToString());
                conditions.Add("GRDWTR_MUMMVAL", spinGDR_MUMMVAL.EditValue.ToString());
                //필수사용유무
                if (chkSEA_USEYN.IsChecked == true)
                    conditions.Add("SEAWTR_USEYN", 'Y');
                else
                    conditions.Add("SEAWTR_USEYN", 'N');

                if (chkUGR_USEYN.IsChecked == true)
                    conditions.Add("UGRWTR_USEYN", 'Y');
                else
                    conditions.Add("UGRWTR_USEYN", 'N');

                if (chkBRK_USEYN.IsChecked == true)
                    conditions.Add("BRKWTR_USEYN", 'Y');
                else
                    conditions.Add("BRKWTR_USEYN", 'N');

                if (chkGDR_USEYN.IsChecked == true)
                    conditions.Add("GRDWTR_USEYN", 'Y');
                else
                    conditions.Add("GRDWTR_USEYN", 'N');
                //생산여유율
                conditions.Add("PRDCTN_MRGN", spinPRDCTN_MRGN.EditValue.ToString());
                //회수율
                conditions.Add("SWRO_RTRVL", spinSWRO_RTRVL.EditValue.ToString());
                conditions.Add("BWRO_RTRVL", spinBWRO_RTRVL.EditValue.ToString());
                conditions.Add("UF_RTRVL", spinUF_RTRVL.EditValue.ToString());
                //전기사용량
                conditions.Add("PWRER_LLOAD", spinPWRER_LLOAD.EditValue.ToString());
                conditions.Add("PWRER_HLOAD", spinPWRER_HLOAD.EditValue.ToString());
                conditions.Add("PWRER_OPERTIME", spinPWRER_OPERTIME.EditValue.ToString());
                //컨트롤밸브운전구간
                conditions.Add("CTL_SCTN", spinCTL_SCTN.EditValue.ToString());

                conditions.Add("ANAL_YN", 'N');

                work.Insert_ANALS_BASE_SETTING(conditions);

                //ANALS_UF_PWRER_PREDICT
                conditions.Clear();
                for (int i = 1; i < 6; i++)
                {
                    conditions.Clear();

                    Label begin = this.FindName("lb_uf_BeginTDS"+i.ToString()) as Label;
                    Label end = this.FindName("lb_uf_EndTDS" + i.ToString()) as Label;
                    ComboBoxEdit cbufMSRC = this.FindName("cbUFMSRC" + i.ToString()) as ComboBoxEdit;
                    SpinEdit spin_uf_FILTER = this.FindName("spin_uf_FILTER" + i.ToString()) as SpinEdit;
                    SpinEdit spin_uf_TIME = this.FindName("spin_uf_TIME" + i.ToString()) as SpinEdit;

                    conditions.Add("BEGIN_TDS", begin.Content);
                    conditions.Add("END_TDS", end.Content);
                    conditions.Add("STDR", cbufMSRC.EditValue);
                    conditions.Add("FILTER", spin_uf_FILTER.EditValue);
                    conditions.Add("TIME", spin_uf_TIME.EditValue);

                    work.Update_ANALS_UF_PWRER_PREDICT(conditions);
                }

                //ANALS_UF_RUNCND
                conditions.Clear();
                for (int i = 1; i < 4; i++)
                {
                    conditions.Clear();

                    SpinEdit spinRUNCNDUFFLW = this.FindName("spinRUNCNDUFFLW" + i.ToString()) as SpinEdit;
                    SpinEdit spinRUNCNDUFAR = this.FindName("spinRUNCNDUFAR" + i.ToString()) as SpinEdit;
                    SpinEdit spinRUNCNDUFCNT = this.FindName("spinRUNCNDUFCNT" + i.ToString()) as SpinEdit;
                    SpinEdit spinRUNCNDUFFLUX = this.FindName("spinRUNCNDUFFLUX" + i.ToString()) as SpinEdit;

                    conditions.Add("GBN", (i+1).ToString());
                    conditions.Add("MXMM_FLUX", spinRUNCNDUFFLUX.EditValue);
                    conditions.Add("PRTITN_AR", spinRUNCNDUFAR.EditValue);
                    conditions.Add("PRTITN_CNT", spinRUNCNDUFCNT.EditValue);
                    conditions.Add("PRDCTN_FLW", spinRUNCNDUFFLW.EditValue);

                    work.Update_ANALS_UF_RUNCND(conditions);
                }

                //ANALS_RO_RUNCND
                conditions.Clear();
                for (int i = 1; i < 3; i++)
                {
                    conditions.Clear();

                    SpinEdit spinROTDSSTDR = this.FindName("spinROTDSSTDR" + i.ToString()) as SpinEdit;
                    SpinEdit spinROFLUXBMTB = this.FindName("spinROFLUXBMTB" + i.ToString()) as SpinEdit;
                    SpinEdit spinRORUNCNDUFAR = this.FindName("spinRORUNCNDUFAR" + i.ToString()) as SpinEdit;
                    SpinEdit spinRORUNCNDUFCNT = this.FindName("spinRORUNCNDUFCNT" + i.ToString()) as SpinEdit;

                    if(i == 1)
                        conditions.Add("MSRC", "SWRO");
                    else if(i == 2)
                        conditions.Add("MSRC", "BWRO");

                    conditions.Add("TDS_STDR", spinROTDSSTDR.EditValue);
                    conditions.Add("FLUX_BMTB", spinROFLUXBMTB.EditValue);
                    conditions.Add("PRTITN_AR", spinRORUNCNDUFAR.EditValue);
                    conditions.Add("PRTITN_CNT", spinRORUNCNDUFCNT.EditValue);

                    work.Update_ANALS_RO_RUNCND(conditions);
                }

                Data_Select();
            }

            Messages.ShowOkMsgBox();
            #endregion
        }

        /// <summary>
        /// 기본설정 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBaseInit_Click(object sender, RoutedEventArgs e)
        {
            spinSEA_PRSRV.EditValue = "";
            spinUGR_PRSRV.EditValue = "";
            spinBRK_PRSRV.EditValue = "";
            spinGDR_PRSRV.EditValue = "";
            spinSEA_MUMMVAL.EditValue = "";
            spinUGR_MUMMVAL.EditValue = "";
            spinBRK_MUMMVAL.EditValue = "";
            spinGDR_MUMMVAL.EditValue = "";
            spinPRDCTN_MRGN.EditValue = "";
            spinSWRO_RTRVL.EditValue = "";
            spinBWRO_RTRVL.EditValue = "";
            spinUF_RTRVL.EditValue = "";
            spinPWRER_LLOAD.EditValue = "";
            spinPWRER_HLOAD.EditValue = "";
            spinPWRER_OPERTIME.EditValue = "";
            spinCTL_SCTN.EditValue = "";

            for (int i = 1; i < 6; i++)
            {
                ComboBoxEdit cbufGBN = this.FindName("cbUFGBN" + i.ToString()) as ComboBoxEdit;
                SpinEdit spin_uf_FILTER = this.FindName("spin_uf_FILTER" + i.ToString()) as SpinEdit;
                SpinEdit spin_uf_TIME = this.FindName("spin_uf_TIME" + i.ToString()) as SpinEdit;

                cbufGBN.SelectedIndex = -1;
                spin_uf_FILTER.EditValue = "";
                spin_uf_TIME.EditValue = "";
            }

            for (int i = 1; i < 4; i++)
            {
                ComboBoxEdit cbRUNCNDUFGBN = this.FindName("cbRUNCNDUFGBN" + i.ToString()) as ComboBoxEdit;
                SpinEdit spinRUNCNDUFFLW = this.FindName("spinRUNCNDUFFLW" + i.ToString()) as SpinEdit;
                SpinEdit spinRUNCNDUFAR = this.FindName("spinRUNCNDUFAR" + i.ToString()) as SpinEdit;
                SpinEdit spinRUNCNDUFCNT = this.FindName("spinRUNCNDUFCNT" + i.ToString()) as SpinEdit;
                SpinEdit spinRUNCNDUFFLUX = this.FindName("spinRUNCNDUFFLUX" + i.ToString()) as SpinEdit;

                cbRUNCNDUFGBN.SelectedIndex = -1;
                spinRUNCNDUFFLW.EditValue = "";
                spinRUNCNDUFAR.EditValue = "";
                spinRUNCNDUFCNT.EditValue = "";
                spinRUNCNDUFFLUX.EditValue = "";
            }

            for (int i = 1; i < 3; i++)
            {
                SpinEdit spinROTDSSTDR = this.FindName("spinROTDSSTDR" + i.ToString()) as SpinEdit;
                SpinEdit spinROFLUXBMTB = this.FindName("spinROFLUXBMTB" + i.ToString()) as SpinEdit;
                SpinEdit spinRORUNCNDUFAR = this.FindName("spinRORUNCNDUFAR" + i.ToString()) as SpinEdit;
                SpinEdit spinRORUNCNDUFCNT = this.FindName("spinRORUNCNDUFCNT" + i.ToString()) as SpinEdit;

                spinROTDSSTDR.EditValue = "";
                spinROFLUXBMTB.EditValue = "";
                spinRORUNCNDUFAR.EditValue = "";
                spinRORUNCNDUFCNT.EditValue = "";
            }

            binitBaseSetting = true;
        }

        private void BtnUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strfilePath = "";

                System.Windows.Forms.OpenFileDialog openFile = new System.Windows.Forms.OpenFileDialog();
                openFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                openFile.Title = "엑셀 업로드";
                openFile.Filter = "All xlsx Files | *.xlsx";

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    strfilePath = openFile.FileName;
                }

                DataSet dataSetResult = new DataSet();

                if (!strfilePath.Equals(""))
                {
                    if (Messages.ShowYesNoMsgBox("기존 데이터를 덮어씁니다. 진행하시겠습니까?") == MessageBoxResult.Yes)
                    {
                        switch ((sender as Button).Name)
                        {
                            case "btnPumpUpload":
                                #region Pump엑셀 업로드
                                dataSetResult = ExcelUtil.ExcelImport(strfilePath);

                                if (dataSetResult.Tables.Count == 1)
                                {
                                    #region //엑셀 양식 검증_(헤더만 체크)
                                    if (!dataSetResult.Tables[0].Columns[0].ColumnName.Equals("해수 펌프 전력소비량"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    if (!dataSetResult.Tables[0].Columns[3].ColumnName.Equals("지하수 펌프 전력소비량"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    if (!dataSetResult.Tables[0].Columns[6].ColumnName.Equals("지표수(우수) 펌프 전력소비량"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    if (!dataSetResult.Tables[0].Columns[9].ColumnName.Equals("기수 펌프 전력소비량"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    #endregion

                                    dataSetResult.Tables[0].Rows.RemoveAt(0);

                                    foreach (DataRow r in dataSetResult.Tables[0].Rows)
                                    {
                                        Hashtable seaconditions = new Hashtable();
                                        seaconditions.Add("MSRC", "SEAWTR");
                                        seaconditions.Add("RATE", r[0].ToString());
                                        seaconditions.Add("VAL", r[1].ToString());
                                        seaconditions.Add("PWRER_VAL", r[2].ToString());
                                        work.Insert_ANALS_PUMP_PWRER(seaconditions);

                                        Hashtable ugrconditions = new Hashtable();
                                        ugrconditions.Add("MSRC", "UGRWTR");
                                        ugrconditions.Add("RATE", r[3].ToString());
                                        ugrconditions.Add("VAL", r[4].ToString());
                                        ugrconditions.Add("PWRER_VAL", r[5].ToString());
                                        work.Insert_ANALS_PUMP_PWRER(ugrconditions);

                                        Hashtable brkconditions = new Hashtable();
                                        brkconditions.Add("MSRC", "BRKWTR");
                                        brkconditions.Add("RATE", r[6].ToString());
                                        brkconditions.Add("VAL", r[7].ToString());
                                        brkconditions.Add("PWRER_VAL", r[8].ToString());
                                        work.Insert_ANALS_PUMP_PWRER(brkconditions);

                                        Hashtable grdconditions = new Hashtable();
                                        grdconditions.Add("MSRC", "GRDWTR");
                                        grdconditions.Add("RATE", r[9].ToString());
                                        grdconditions.Add("VAL", r[10].ToString());
                                        grdconditions.Add("PWRER_VAL", r[11].ToString());
                                        work.Insert_ANALS_PUMP_PWRER(grdconditions);
                                    }
                                }
                                else
                                {
                                    Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                    return;
                                }

                                Messages.ShowOkMsgBox();
                                #endregion
                                break;

                            case "btnUFUpload":
                                #region UF엑셀 업로드
                                dataSetResult = ExcelUtil.ExcelImport(strfilePath);

                                if (dataSetResult.Tables.Count == 1)
                                {
                                    #region //엑셀 양식 검증_(헤더만 체크)
                                    if (!dataSetResult.Tables[0].Columns[0].ColumnName.Equals("UF1 펌프 전력소비량"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    if (!dataSetResult.Tables[0].Columns[3].ColumnName.Equals("UF2 펌프 전력소비량"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    #endregion

                                    dataSetResult.Tables[0].Rows.RemoveAt(0);

                                    foreach (DataRow r in dataSetResult.Tables[0].Rows)
                                    {
                                        Hashtable uf1conditions = new Hashtable();
                                        uf1conditions.Add("MSRC", "UF1");
                                        uf1conditions.Add("RATE", r[0].ToString());
                                        uf1conditions.Add("VAL", r[1].ToString());
                                        uf1conditions.Add("PWRER_VAL", r[2].ToString());
                                        work.Insert_ANALS_UFPUMP_PWRER(uf1conditions);

                                        Hashtable uf2conditions = new Hashtable();
                                        uf2conditions.Add("MSRC", "UF2");
                                        uf2conditions.Add("RATE", r[3].ToString());
                                        uf2conditions.Add("VAL", r[4].ToString());
                                        uf2conditions.Add("PWRER_VAL", r[5].ToString());
                                        work.Insert_ANALS_UFPUMP_PWRER(uf2conditions);
                                    }
                                }
                                else
                                {
                                    Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                    return;
                                }

                                Messages.ShowOkMsgBox();
                                #endregion
                                break;

                            case "btnSWBWUpload":
                                #region SWBW엑셀 업로드
                                dataSetResult = ExcelUtil.ExcelImport(strfilePath);

                                if (dataSetResult.Tables.Count == 1)
                                {
                                    #region //엑셀 양식 검증_(헤더만 체크)
                                    if (!dataSetResult.Tables[0].Columns[0].ColumnName.Equals("SWRO 전력소비량"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    if (!dataSetResult.Tables[0].Columns[2].ColumnName.Equals("BWRO 전력소비량"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    #endregion

                                    dataSetResult.Tables[0].Rows.RemoveAt(0);

                                    foreach (DataRow r in dataSetResult.Tables[0].Rows)
                                    {
                                        if (!r[0].ToString().Equals(""))
                                        {
                                            Hashtable SWROconditions = new Hashtable();
                                            SWROconditions.Add("MSRC", "SWRO");
                                            SWROconditions.Add("PRESSR", r[0].ToString());
                                            SWROconditions.Add("PWRER_VAL", r[1].ToString());
                                            work.Insert_ANALS_SWBW_PWRER(SWROconditions);
                                        }

                                        if (!r[2].ToString().Equals(""))
                                        {
                                            Hashtable BWROconditions = new Hashtable();
                                            BWROconditions.Add("MSRC", "BWRO");
                                            BWROconditions.Add("PRESSR", r[2].ToString());
                                            BWROconditions.Add("PWRER_VAL", r[3].ToString());
                                            work.Insert_ANALS_SWBW_PWRER(BWROconditions);
                                        }
                                    }
                                }
                                else
                                {
                                    Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                    return;
                                }

                                Messages.ShowOkMsgBox();
                                #endregion
                                break;

                            case "btnSWBWSettingUpload":
                                #region SWBW엑셀 업로드
                                dataSetResult = ExcelUtil.ExcelImport(strfilePath);

                                if (dataSetResult.Tables.Count == 1)
                                {
                                    #region //엑셀 양식 검증_(헤더만 체크)
                                    if (!dataSetResult.Tables[0].Columns[0].ColumnName.Equals("SWRO"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    if (!dataSetResult.Tables[0].Columns[9].ColumnName.Equals("BWRO"))
                                    {
                                        Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                        return;
                                    }
                                    #endregion

                                    dataSetResult.Tables[0].Rows.RemoveAt(0);

                                    foreach (DataRow r in dataSetResult.Tables[0].Rows)
                                    {
                                        if (!r[0].ToString().Equals(""))
                                        {
                                            for (int i = 1; i < 5; i++)
                                            {
                                                Hashtable SWROconditions = new Hashtable();
                                                SWROconditions.Add("MSRC", "SWRO");
                                                SWROconditions.Add("GBN", i.ToString());
                                                SWROconditions.Add("BEGIN_TDS", r[0].ToString());
                                                SWROconditions.Add("END_TDS", r[1].ToString());
                                                SWROconditions.Add("PRESSR", r[2].ToString());
                                                SWROconditions.Add("INFLW", r[3].ToString());
                                                SWROconditions.Add("OUTFLW", r[4].ToString());
                                                SWROconditions.Add("FLUX", r[5].ToString());
                                                SWROconditions.Add("TRND_CAL", r[6].ToString());
                                                SWROconditions.Add("INFLW_CAL", r[7].ToString());
                                                SWROconditions.Add("FLUX_CAL", r[8].ToString());
                                                work.Insert_ANALS_SWBW_SETTING(SWROconditions);
                                            }
                                        }

                                        if (!r[9].ToString().Equals(""))
                                        {
                                            for (int i = 1; i < 5; i++)
                                            {
                                                Hashtable BWROconditions = new Hashtable();
                                                BWROconditions.Add("MSRC", "BWRO");
                                                BWROconditions.Add("GBN", i.ToString());
                                                BWROconditions.Add("BEGIN_TDS", r[9].ToString());
                                                BWROconditions.Add("END_TDS", r[10].ToString());
                                                BWROconditions.Add("PRESSR", r[11].ToString());
                                                BWROconditions.Add("INFLW", r[12].ToString());
                                                BWROconditions.Add("OUTFLW", r[13].ToString());
                                                BWROconditions.Add("FLUX", r[14].ToString());
                                                BWROconditions.Add("TRND_CAL", r[15].ToString());
                                                BWROconditions.Add("INFLW_CAL", r[16].ToString());
                                                BWROconditions.Add("FLUX_CAL", r[17].ToString());
                                                work.Insert_ANALS_SWBW_SETTING(BWROconditions);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Messages.ShowInfoMsgBox("해당 엑셀업로드 양식이 아닙니다.");
                                    return;
                                }

                                Messages.ShowOkMsgBox();
                                #endregion
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
            finally
            {
                Data_Select();
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            DataRowView drv;

            switch ((sender as GridControl).Name)
            {
                case "gridPUMP":
                    drv = gridPUMP.SelectedItem as DataRowView;
                    if (drv != null)
                    {
                        cbPumpMSRC.EditValue = drv["MSRC"].ToString();
                        spinPumpRate.EditValue = drv["RATE"].ToString();
                        spinPumpVAL.EditValue = drv["VAL"].ToString();
                        spinPumpPWRERVAL.EditValue = drv["PWRER_VAL"].ToString();
                    }
                    break;

                case "gridUF":
                    drv = gridUF.SelectedItem as DataRowView;
                    if (drv != null)
                    {
                        cbUFMSRC.EditValue = drv["MSRC"].ToString();
                        spinUFRate.EditValue = drv["RATE"].ToString();
                        spinUFVAL.EditValue = drv["VAL"].ToString();
                        spinUFPWRERVAL.EditValue = drv["PWRER_VAL"].ToString();
                    }
                    break;

                case "gridSWBW":
                    drv = gridSWBW.SelectedItem as DataRowView;
                    if (drv != null)
                    {
                        cbSWBWMSRC.EditValue = drv["MSRC"].ToString();
                        spinSWBWPRESSR.EditValue = drv["PRESSR"].ToString();
                        spinSWBWPWRERVAL.EditValue = drv["PWRER_VAL"].ToString();
                    }
                    break;
                case "gridSWBWSetting":
                    drv = gridSWBWSetting.SelectedItem as DataRowView;
                    if (drv != null)
                    {
                        cbSWBWSettingMSRC.EditValue = drv["MSRC"].ToString();
                        cbSWBWSettingGBN.EditValue = drv["GBN"].ToString();
                        spinSWBWSettingBegin_TDS.EditValue = drv["BEGIN_TDS"].ToString();
                        spinSWBWSettingEnd_TDS.EditValue = drv["END_TDS"].ToString();
                        spinSWBWSettingPRESSR.EditValue = drv["PRESSR"].ToString();
                        spinSWBWSettingINFLW.EditValue = drv["INFLW"].ToString();
                        spinSWBWSettingOUTFLW.EditValue = drv["OUTFLW"].ToString();
                        spinSWBWSettingFLUX.EditValue = drv["FLUX"].ToString();
                        spinSWBWSettingTREND_CAL.EditValue = drv["TRND_CAL"].ToString();
                        spinSWBWSettingINFLOW_CAL.EditValue = drv["INFLW_CAL"].ToString();
                        spinSWBWSettingFLUX_CAL.EditValue = drv["FLUX_CAL"].ToString();
                    }
                    break;
            }
        }

        /// <summary>
        /// 삭제버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDel_Click(object sender, RoutedEventArgs e)
        {
            Hashtable conditions = new Hashtable();

            try
            {
                switch ((sender as Button).Name)
                {
                    case "btnPumpDel":
                        #region PUMP 삭제
                        if ((gridPUMP.SelectedItem as DataRowView) == null)
                        {
                            Messages.ShowInfoMsgBox("선택하신 데이터가 없습니다.");
                            return;
                        }

                        if (Messages.ShowYesNoMsgBox("선택하신 데이터를 삭제 하시겠습니까?") == MessageBoxResult.Yes)
                        {
                            //파라미터 정리
                            conditions.Add("MSRC", (gridPUMP.SelectedItem as DataRowView)["MSRC"].ToString());
                            conditions.Add("RATE", (gridPUMP.SelectedItem as DataRowView)["RATE"].ToString());

                            //삭제
                            work.Delete_ANALS_PUMP_PWRER(conditions);

                            Messages.ShowOkMsgBox();
                        }
                        #endregion
                        break;

                    case "btnUFDel":
                        #region UF 삭제
                        if ((gridUF.SelectedItem as DataRowView) == null)
                        {
                            Messages.ShowInfoMsgBox("선택하신 데이터가 없습니다.");
                            return;
                        }

                        if (Messages.ShowYesNoMsgBox("선택하신 데이터를 삭제 하시겠습니까?") == MessageBoxResult.Yes)
                        {
                            //파라미터 정리
                            conditions.Add("MSRC", (gridUF.SelectedItem as DataRowView)["MSRC"].ToString());
                            conditions.Add("RATE", (gridUF.SelectedItem as DataRowView)["RATE"].ToString());

                            //삭제
                            work.Delete_ANALS_UFPUMP_PWRER(conditions);

                            Messages.ShowOkMsgBox();
                        }
                        #endregion
                        break;

                    case "btnSWBWDel":
                        #region SWBW 삭제
                        if ((gridSWBW.SelectedItem as DataRowView) == null)
                        {
                            Messages.ShowInfoMsgBox("선택하신 데이터가 없습니다.");
                            return;
                        }

                        if (Messages.ShowYesNoMsgBox("선택하신 데이터를 삭제 하시겠습니까?") == MessageBoxResult.Yes)
                        {
                            //파라미터 정리
                            conditions.Add("MSRC", (gridSWBW.SelectedItem as DataRowView)["MSRC"].ToString());
                            conditions.Add("PRESSR", (gridSWBW.SelectedItem as DataRowView)["PRESSR"].ToString());

                            //삭제
                            work.Delete_ANALS_SWBW_PWRER(conditions);

                            Messages.ShowOkMsgBox();
                        }
                        #endregion
                        break;

                    case "btnSWBWSettingDel":
                        #region SWBW 삭제
                        if ((gridSWBWSetting.SelectedItem as DataRowView) == null)
                        {
                            Messages.ShowInfoMsgBox("선택하신 데이터가 없습니다.");
                            return;
                        }

                        if (Messages.ShowYesNoMsgBox("선택하신 데이터를 삭제 하시겠습니까?") == MessageBoxResult.Yes)
                        {
                            //파라미터 정리
                            conditions.Add("MSRC", (gridSWBWSetting.SelectedItem as DataRowView)["MSRC"].ToString());
                            conditions.Add("GBN", (gridSWBWSetting.SelectedItem as DataRowView)["GBN"].ToString());
                            conditions.Add("BEGIN_TDS", (gridSWBWSetting.SelectedItem as DataRowView)["BEGIN_TDS"].ToString());

                            //삭제
                            work.Delete_ANALS_SWBW_SETTING(conditions);

                            Messages.ShowOkMsgBox();
                        }
                        #endregion
                        break;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
            finally
            {
                Data_Select();
            }
        }

        /// <summary>
        /// 그리드 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPUMP_SelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
            DataRowView drv;

            switch ((sender as GridControl).Name)
            {
                case "gridPUMP":
                    #region 펌프 선택
                    drv = gridPUMP.SelectedItem as DataRowView;
                    if (drv != null)
                    {
                        cbPumpMSRC.EditValue = drv["MSRC"].ToString();
                        spinPumpRate.EditValue = drv["RATE"].ToString();
                        spinPumpVAL.EditValue = drv["VAL"].ToString();
                        spinPumpPWRERVAL.EditValue = drv["PWRER_VAL"].ToString();
                    }
                    else
                    {
                        cbPumpMSRC.EditValue = "";
                        spinPumpRate.EditValue = "";
                        spinPumpVAL.EditValue = "";
                        spinPumpPWRERVAL.EditValue = "";
                    }
                    #endregion
                    break;

                case "gridUF":
                    #region UF선택
                    drv = gridUF.SelectedItem as DataRowView;
                    if (drv != null)
                    {
                        cbUFMSRC.EditValue = drv["MSRC"].ToString();
                        spinUFRate.EditValue = drv["RATE"].ToString();
                        spinUFVAL.EditValue = drv["VAL"].ToString();
                        spinUFPWRERVAL.EditValue = drv["PWRER_VAL"].ToString();
                    }
                    else
                    {
                        cbPumpMSRC.EditValue = "";
                        spinUFRate.EditValue = "";
                        spinUFVAL.EditValue = "";
                        spinUFVAL.EditValue = "";
                    }
                    #endregion
                    break;

                case "gridSWBW":
                    #region SWBW 선택
                    drv = gridSWBW.SelectedItem as DataRowView;
                    if (drv != null)
                    {
                        cbSWBWMSRC.EditValue = drv["MSRC"].ToString();
                        spinSWBWPRESSR.EditValue = drv["PRESSR"].ToString();
                        spinSWBWPWRERVAL.EditValue = drv["PWRER_VAL"].ToString();
                    }
                    else
                    {
                        cbSWBWMSRC.EditValue = "";
                        spinSWBWPRESSR.EditValue = "";
                        spinSWBWPWRERVAL.EditValue = "";
                    }
                    #endregion
                    break;

                case "gridSWBWSetting":
                    #region SWBW Setting 선택
                    drv = gridSWBWSetting.SelectedItem as DataRowView;
                    if (drv != null)
                    {
                        cbSWBWSettingMSRC.EditValue = drv["MSRC"].ToString();
                        cbSWBWSettingGBN.EditValue = drv["GBN"].ToString();
                        spinSWBWSettingBegin_TDS.EditValue = drv["BEGIN_TDS"].ToString();
                        spinSWBWSettingEnd_TDS.EditValue = drv["END_TDS"].ToString();
                        spinSWBWSettingPRESSR.EditValue = drv["PRESSR"].ToString();
                        spinSWBWSettingINFLW.EditValue = drv["INFLW"].ToString();
                        spinSWBWSettingOUTFLW.EditValue = drv["OUTFLW"].ToString();
                        spinSWBWSettingFLUX.EditValue = drv["FLUX"].ToString();
                        spinSWBWSettingTREND_CAL.EditValue = drv["TRND_CAL"].ToString();
                        spinSWBWSettingINFLOW_CAL.EditValue = drv["INFLW_CAL"].ToString();
                        spinSWBWSettingFLUX_CAL.EditValue = drv["FLUX_CAL"].ToString();
                    }
                    else
                    {
                        cbSWBWSettingMSRC.EditValue = "";
                        cbSWBWSettingGBN.EditValue = "";
                        spinSWBWSettingBegin_TDS.EditValue = "";
                        spinSWBWSettingEnd_TDS.EditValue = "";
                        spinSWBWSettingPRESSR.EditValue = "";
                        spinSWBWSettingINFLW.EditValue = "";
                        spinSWBWSettingOUTFLW.EditValue = "";
                        spinSWBWSettingFLUX.EditValue = "";
                        spinSWBWSettingTREND_CAL.EditValue = "";
                        spinSWBWSettingINFLOW_CAL.EditValue = "";
                        spinSWBWSettingFLUX_CAL.EditValue = "";
                    }
                    #endregion
                    break;
            }

            cbPumpMSRC.IsEnabled = false;
            spinPumpRate.IsEnabled = false;
            cbUFMSRC.IsEnabled = false;
            spinUFRate.IsEnabled = false;
            cbSWBWMSRC.IsEnabled = false;
            spinSWBWPRESSR.IsEnabled = false;
            cbSWBWSettingGBN.IsEnabled = false;
            cbSWBWSettingMSRC.IsEnabled = false;
            spinSWBWSettingBegin_TDS.IsEnabled = false;

            binitPump = false;
            binitUF = false;
            binitSWBWSetting = false;
            binitSWBW = false;
        }

        /// <summary>
        /// 초기화 플래그 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnInit_Click(object sender, RoutedEventArgs e)
        {
            switch ((sender as Button).Name)
            {
                case "btnPumpInit":
                    binitPump = true;
                    cbPumpMSRC.IsEnabled = true;
                    spinPumpRate.IsEnabled = true;

                    binitUF = false;
                    cbUFMSRC.IsEnabled = false;
                    spinUFRate.IsEnabled = false;

                    binitSWBW = false;
                    cbSWBWMSRC.IsEnabled = false;
                    spinSWBWPRESSR.IsEnabled = false;

                    binitSWBWSetting = false;
                    cbSWBWSettingGBN.IsEnabled = false;
                    cbSWBWSettingMSRC.IsEnabled = false;
                    spinSWBWSettingBegin_TDS.IsEnabled = false;

                    cbPumpMSRC.SelectedIndex = -1;
                    spinPumpRate.EditValue = "";
                    spinPumpVAL.EditValue = "";
                    spinPumpPWRERVAL.EditValue = "";

                    break;

                case "btnUFInit":
                    binitPump = false;
                    cbPumpMSRC.IsEnabled = false;
                    spinPumpRate.IsEnabled = false;

                    binitUF = true;
                    cbUFMSRC.IsEnabled = true;
                    spinUFRate.IsEnabled = true;

                    binitSWBW = false;
                    cbSWBWMSRC.IsEnabled = false;
                    spinSWBWPRESSR.IsEnabled = false;

                    binitSWBWSetting = false;
                    cbSWBWSettingGBN.IsEnabled = false;
                    cbSWBWSettingMSRC.IsEnabled = false;
                    spinSWBWSettingBegin_TDS.IsEnabled = false;

                    cbUFMSRC.SelectedIndex = -1;
                    spinUFRate.EditValue = "";
                    spinUFVAL.EditValue = "";
                    spinUFPWRERVAL.EditValue = "";
                    break;

                case "btnSWBWInit":
                    binitPump = false;
                    cbPumpMSRC.IsEnabled = false;
                    spinPumpRate.IsEnabled = false;

                    binitUF = false;
                    cbUFMSRC.IsEnabled = false;
                    spinUFRate.IsEnabled = false;

                    binitSWBW = true;
                    cbSWBWMSRC.IsEnabled = true;
                    spinSWBWPRESSR.IsEnabled = true;

                    binitSWBWSetting = false;
                    cbSWBWSettingGBN.IsEnabled = false;
                    cbSWBWSettingMSRC.IsEnabled = false;
                    spinSWBWSettingBegin_TDS.IsEnabled = false;

                    cbSWBWMSRC.SelectedIndex = -1;
                    spinSWBWPRESSR.EditValue = "";
                    spinSWBWPWRERVAL.EditValue = "";
                    break;

                case "btnSWBWSettingInit":
                    binitPump = false;
                    cbPumpMSRC.IsEnabled = false;
                    spinPumpRate.IsEnabled = false;

                    binitUF = false;
                    cbUFMSRC.IsEnabled = false;
                    spinUFRate.IsEnabled = false;

                    binitSWBW = false;
                    cbSWBWMSRC.IsEnabled = false;
                    spinSWBWPRESSR.IsEnabled = false;

                    binitSWBWSetting = true;
                    cbSWBWSettingGBN.IsEnabled = true;
                    cbSWBWSettingMSRC.IsEnabled = true;
                    spinSWBWSettingBegin_TDS.IsEnabled = true;

                    cbSWBWSettingMSRC.SelectedIndex = -1;
                    cbSWBWSettingGBN.SelectedIndex = -1;
                    spinSWBWSettingBegin_TDS.EditValue = "";
                    spinSWBWSettingEnd_TDS.EditValue = "";
                    spinSWBWSettingPRESSR.EditValue = "";
                    spinSWBWSettingINFLW.EditValue = "";
                    spinSWBWSettingOUTFLW.EditValue = "";
                    spinSWBWSettingFLUX.EditValue = "";
                    spinSWBWSettingTREND_CAL.EditValue = "";
                    spinSWBWSettingINFLOW_CAL.EditValue = "";
                    spinSWBWSettingFLUX_CAL.EditValue = "";
                    break;


            }
        }

        /// <summary>
        /// 저장버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            Hashtable conditions = new Hashtable();

            try
            {
                switch ((sender as Button).Name)
                {
                    case "btnPumpSave":
                        #region PUMP 저장
                        if (binitPump)
                        {
                            //파라미터 정리
                            conditions.Add("MSRC", cbPumpMSRC.EditValue.ToString());
                            conditions.Add("RATE", spinPumpRate.EditValue.ToString());
                            conditions.Add("VAL", spinPumpVAL.EditValue.ToString());
                            conditions.Add("PWRER_VAL", spinPumpPWRERVAL.EditValue.ToString());

                            //유무 확인
                            if (work.Select_ANALS_PUMP_PWRER_OverLapCheck(conditions).Rows.Count == 1)
                            {
                                Messages.ShowInfoMsgBox("기존 데이터가 존재합니다.");
                                return;
                            }
                            else
                            {
                                work.Insert_ANALS_PUMP_PWRER(conditions);
                            }
                        }
                        else
                        {
                            conditions.Add("MSRC", (gridPUMP.SelectedItem as DataRowView)["MSRC"].ToString());
                            conditions.Add("RATE", (gridPUMP.SelectedItem as DataRowView)["RATE"].ToString());
                            conditions.Add("VAL", spinPumpVAL.EditValue.ToString());
                            conditions.Add("PWRER_VAL", spinPumpPWRERVAL.EditValue.ToString());

                            work.Update_ANALS_PUMP_PWRER(conditions);
                        }

                        Messages.ShowOkMsgBox();
                        #endregion
                        break;

                    case "btnUFSave":
                        #region UF저장
                        if (binitUF)
                        {
                            //파라미터 정리
                            conditions.Add("MSRC", cbUFMSRC.EditValue.ToString());
                            conditions.Add("RATE", spinUFRate.EditValue.ToString());
                            conditions.Add("VAL", spinUFVAL.EditValue.ToString());
                            conditions.Add("PWRER_VAL", spinUFPWRERVAL.EditValue.ToString());

                            //유무 확인
                            if (work.Select_ANALS_UFPUMP_PWRER_OverLapCheck(conditions).Rows.Count == 1)
                            {
                                Messages.ShowInfoMsgBox("기존 데이터가 존재합니다.");
                                return;
                            }
                            else
                            {
                                work.Insert_ANALS_UFPUMP_PWRER(conditions);
                            }
                        }
                        else
                        {
                            conditions.Add("MSRC", (gridUF.SelectedItem as DataRowView)["MSRC"].ToString());
                            conditions.Add("RATE", (gridUF.SelectedItem as DataRowView)["RATE"].ToString());
                            conditions.Add("VAL", spinUFVAL.EditValue.ToString());
                            conditions.Add("PWRER_VAL", spinUFPWRERVAL.EditValue.ToString());

                            work.Update_ANALS_UFPUMP_PWRER(conditions);
                        }

                        Messages.ShowOkMsgBox();
                        #endregion
                        break;

                    case "btnSWBWSave":
                        #region SWBW저장
                        if (binitSWBW)
                        {
                            //파라미터 정리
                            conditions.Add("MSRC", cbSWBWMSRC.EditValue.ToString());
                            conditions.Add("PRESSR", spinSWBWPRESSR.EditValue.ToString());
                            conditions.Add("PWRER_VAL", spinSWBWPWRERVAL.EditValue.ToString());

                            //유무 확인
                            if (work.Select_ANALS_SWBW_PWRER_OverLapCheck(conditions).Rows.Count == 1)
                            {
                                Messages.ShowInfoMsgBox("기존 데이터가 존재합니다.");
                                return;
                            }
                            else
                            {
                                work.Insert_ANALS_SWBW_PWRER(conditions);
                            }
                        }
                        else
                        {
                            conditions.Add("MSRC", (gridSWBW.SelectedItem as DataRowView)["MSRC"].ToString());
                            conditions.Add("PRESSR", (gridSWBW.SelectedItem as DataRowView)["PRESSR"].ToString());
                            conditions.Add("PWRER_VAL", spinSWBWPWRERVAL.EditValue.ToString());

                            work.Update_ANALS_SWBW_PWRER(conditions);
                        }

                        Messages.ShowOkMsgBox();
                        #endregion
                        break;

                    case "btnSWBWSettingSave":
                        #region SWBWSetting 저장
                        if (binitSWBWSetting)
                        {
                            //파라미터 정리
                            conditions.Add("MSRC", cbSWBWSettingMSRC.EditValue.ToString());
                            conditions.Add("GBN", cbSWBWSettingGBN.EditValue.ToString());
                            conditions.Add("BEGIN_TDS", spinSWBWSettingBegin_TDS.EditValue.ToString());
                            conditions.Add("END_TDS", spinSWBWSettingEnd_TDS.EditValue.ToString());
                            conditions.Add("PRESSR", spinSWBWSettingPRESSR.EditValue.ToString());
                            conditions.Add("INFLW", spinSWBWSettingINFLW.EditValue.ToString());
                            conditions.Add("OUTFLW", spinSWBWSettingOUTFLW.EditValue.ToString());
                            conditions.Add("FLUX", spinSWBWSettingFLUX.EditValue.ToString());
                            conditions.Add("TRND_CAL", spinSWBWSettingTREND_CAL.EditValue.ToString());
                            conditions.Add("INFLW_CAL", spinSWBWSettingINFLOW_CAL.EditValue.ToString());
                            conditions.Add("FLUX_CAL", spinSWBWSettingFLUX_CAL.EditValue.ToString());

                            //유무 확인
                            if (work.Select_ANALS_SWBW_SETTING_OverLapCheck(conditions).Rows.Count == 1)
                            {
                                Messages.ShowInfoMsgBox("기존 데이터가 존재합니다.");
                                return;
                            }
                            else
                            {
                                work.Insert_ANALS_SWBW_SETTING(conditions);
                            }
                        }
                        else
                        {
                            conditions.Add("MSRC", (gridSWBWSetting.SelectedItem as DataRowView)["MSRC"].ToString());
                            conditions.Add("GBN", (gridSWBWSetting.SelectedItem as DataRowView)["GBN"].ToString());
                            conditions.Add("BEGIN_TDS", (gridSWBWSetting.SelectedItem as DataRowView)["BEGIN_TDS"].ToString());
                            conditions.Add("END_TDS", spinSWBWSettingEnd_TDS.EditValue.ToString());
                            conditions.Add("PRESSR", spinSWBWSettingPRESSR.EditValue.ToString());
                            conditions.Add("INFLW", spinSWBWSettingINFLW.EditValue.ToString());
                            conditions.Add("OUTFLW", spinSWBWSettingOUTFLW.EditValue.ToString());
                            conditions.Add("FLUX", spinSWBWSettingFLUX.EditValue.ToString());
                            conditions.Add("TRND_CAL", spinSWBWSettingTREND_CAL.EditValue.ToString());
                            conditions.Add("INFLW_CAL", spinSWBWSettingINFLOW_CAL.EditValue.ToString());
                            conditions.Add("FLUX_CAL", spinSWBWSettingFLUX_CAL.EditValue.ToString());

                            work.Update_ANALS_SWBW_SETTING(conditions);
                        }

                        Messages.ShowOkMsgBox();
                        #endregion
                        break;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
            finally
            {
                Data_Select();
            }
        }

        /// <summary>
        /// 양식 다운로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDown_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strfileName = "";
                System.Windows.Forms.SaveFileDialog savefile = new System.Windows.Forms.SaveFileDialog();
                savefile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                savefile.Title = "양식 다운로드";
                savefile.Filter = "All xlsx Files | *.xlsx";

                switch ((sender as Button).Name)
                {
                    case "btnPumpDown":
                        savefile.FileName = "펌프 전력소비량.xlsx";
                        strfileName = "펌프 전력소비량.xlsx";
                        break;
                    case "btnUFDown":
                        savefile.FileName = "UF펌프 전력소비량.xlsx";
                        strfileName = "UF펌프 전력소비량.xlsx";
                        break;
                    case "btnSWBWDown":
                        savefile.FileName = "SWRO, BWRO 전력소비량.xlsx";
                        strfileName = "SWRO, BWRO 전력소비량.xlsx";
                        break;
                    case "btnSWBWSettingDown":
                        savefile.FileName = "SWRO, BWRO 설정.xlsx";
                        strfileName = "SWRO, BWRO 설정.xlsx";
                        break;
                }

                if (savefile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    FileInfo file = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "Resources\\UploadFormat\\" + strfileName);

                    if (file.Exists)
                    {
                        file.CopyTo(savefile.FileName, true);
                    }
                    else
                    {
                        Messages.ShowInfoMsgBox("양식파일이 없습니다.");
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void InitializeData()
        {

            #region //펌프 구분 콤보박스 하드코딩
            DataTable dtcbPump = new DataTable();

            dtcbPump.Columns.Add("CODE");
            dtcbPump.Columns.Add("NAME");

            DataRow dr1 = dtcbPump.NewRow();
            dr1[0] = "SEAWTR";
            dr1[1] = "해수";
            dtcbPump.Rows.Add(dr1);

            DataRow dr2 = dtcbPump.NewRow();
            dr2[0] = "UGRWTR";
            dr2[1] = "지하수";
            dtcbPump.Rows.Add(dr2);

            DataRow dr3 = dtcbPump.NewRow();
            dr3[0] = "BRKWTR";
            dr3[1] = "지표수";
            dtcbPump.Rows.Add(dr3);

            DataRow dr4 = dtcbPump.NewRow();
            dr4[0] = "GRDWTR";
            dr4[1] = "기수";
            dtcbPump.Rows.Add(dr4);

            cbPumpMSRC.ItemsSource = dtcbPump;
            #endregion

            #region //UF펌프 구분 콤보박스 하드코딩
            DataTable dtcbUF = new DataTable();

            dtcbUF.Columns.Add("CODE");
            dtcbUF.Columns.Add("NAME");

            DataRow dr5 = dtcbUF.NewRow();
            dr5[0] = "UF1";
            dr5[1] = "UF1";
            dtcbUF.Rows.Add(dr5);

            DataRow dr6 = dtcbUF.NewRow();
            dr6[0] = "UF2";
            dr6[1] = "UF2";
            dtcbUF.Rows.Add(dr6);

            cbUFMSRC.ItemsSource = dtcbUF.Copy();
            cbUFMSRC1.ItemsSource = dtcbUF.Copy();
            cbUFMSRC2.ItemsSource = dtcbUF.Copy();
            cbUFMSRC3.ItemsSource = dtcbUF.Copy();
            cbUFMSRC4.ItemsSource = dtcbUF.Copy();
            cbUFMSRC5.ItemsSource = dtcbUF.Copy();
            #endregion

            #region //SWBW 구분 콤보박스 하드코딩
            DataTable dtcbSWBW = new DataTable();

            dtcbSWBW.Columns.Add("CODE");
            dtcbSWBW.Columns.Add("NAME");

            DataRow dr7 = dtcbSWBW.NewRow();
            dr7[0] = "SWRO";
            dr7[1] = "SWRO";
            dtcbSWBW.Rows.Add(dr7);

            DataRow dr8 = dtcbSWBW.NewRow();
            dr8[0] = "BWRO";
            dr8[1] = "BWRO";
            dtcbSWBW.Rows.Add(dr8);

            cbSWBWMSRC.ItemsSource = dtcbSWBW.Copy();
            cbSWBWSettingMSRC.ItemsSource = dtcbSWBW.Copy();
            #endregion

            #region //SWBW 구분 콤보박스 하드코딩
            DataTable dtcbSWBWMSRC = new DataTable();

            dtcbSWBWMSRC.Columns.Add("CODE");
            dtcbSWBWMSRC.Columns.Add("NAME");

            DataRow dr9 = dtcbSWBWMSRC.NewRow();
            dr9[0] = "1";
            dr9[1] = "해수";
            dtcbSWBWMSRC.Rows.Add(dr9);

            DataRow dr10 = dtcbSWBWMSRC.NewRow();
            dr10[0] = "2";
            dr10[1] = "이중수원";
            dtcbSWBWMSRC.Rows.Add(dr10);

            DataRow dr11 = dtcbSWBWMSRC.NewRow();
            dr11[0] = "3";
            dr11[1] = "삼중수원";
            dtcbSWBWMSRC.Rows.Add(dr11);

            DataRow dr12 = dtcbSWBWMSRC.NewRow();
            dr12[0] = "4";
            dr12[1] = "사중수원";
            dtcbSWBWMSRC.Rows.Add(dr12);

            cbSWBWSettingGBN.ItemsSource = dtcbSWBWMSRC.Copy();
            #endregion

            Data_Select();
        }

        /// <summary>
        /// 데이터 조회
        /// </summary>
        private void Data_Select()
        {
            try
            {
                //Pump 데이터 조회
                gridPUMP.ItemsSource = work.Select_ANALS_PUMP_PWRER(null);
                gridUF.ItemsSource = work.Select_ANALS_UFPUMP_PWRER(null);
                gridSWBW.ItemsSource = work.Select_ANALS_SWBW_PWRER(null);
                gridSWBWSetting.ItemsSource = work.Select_ANALS_SWBW_SETTING(null);

                #region //기본설정 부분 조회
                //ANALS_BASE_SETTING
                DataTable dtBaseSetting = new DataTable();
                dtBaseSetting = work.Select_ANALS_BASE_SETTING(null);

                if (dtBaseSetting.Rows.Count == 1)
                {
                    spinSEA_PRSRV.EditValue = dtBaseSetting.Rows[0]["SEAWTR_PRSRV"].ToString();
                    spinUGR_PRSRV.EditValue = dtBaseSetting.Rows[0]["SEAWTR_PRSRV"].ToString();
                    spinBRK_PRSRV.EditValue = dtBaseSetting.Rows[0]["SEAWTR_PRSRV"].ToString();
                    spinGDR_PRSRV.EditValue = dtBaseSetting.Rows[0]["SEAWTR_PRSRV"].ToString();
                    spinSEA_MUMMVAL.EditValue = dtBaseSetting.Rows[0]["SEAWTR_MUMMVAL"].ToString();
                    spinUGR_MUMMVAL.EditValue = dtBaseSetting.Rows[0]["UGRWTR_MUMMVAL"].ToString();
                    spinBRK_MUMMVAL.EditValue = dtBaseSetting.Rows[0]["BRKWTR_MUMMVAL"].ToString();
                    spinGDR_MUMMVAL.EditValue = dtBaseSetting.Rows[0]["GRDWTR_MUMMVAL"].ToString();
                    if (dtBaseSetting.Rows[0]["SEAWTR_USEYN"].ToString().Equals("Y"))
                        chkSEA_USEYN.IsChecked = true;
                    else
                        chkSEA_USEYN.IsChecked = false;

                    if (dtBaseSetting.Rows[0]["UGRWTR_USEYN"].ToString().Equals("Y"))
                        chkUGR_USEYN.IsChecked = true;
                    else
                        chkUGR_USEYN.IsChecked = false;

                    if (dtBaseSetting.Rows[0]["BRKWTR_USEYN"].ToString().Equals("Y"))
                        chkBRK_USEYN.IsChecked = true;
                    else
                        chkBRK_USEYN.IsChecked = false;

                    if (dtBaseSetting.Rows[0]["GRDWTR_USEYN"].ToString().Equals("Y"))
                        chkGDR_USEYN.IsChecked = true;
                    else
                        chkGDR_USEYN.IsChecked = false;

                    spinPRDCTN_MRGN.EditValue = dtBaseSetting.Rows[0]["PRDCTN_MRGN"].ToString();
                    spinSWRO_RTRVL.EditValue = dtBaseSetting.Rows[0]["SWRO_RTRVL"].ToString();
                    spinBWRO_RTRVL.EditValue = dtBaseSetting.Rows[0]["BWRO_RTRVL"].ToString();
                    spinUF_RTRVL.EditValue = dtBaseSetting.Rows[0]["UF_RTRVL"].ToString();
                    spinPWRER_LLOAD.EditValue = dtBaseSetting.Rows[0]["PWRER_LLOAD"].ToString();
                    spinPWRER_HLOAD.EditValue = dtBaseSetting.Rows[0]["PWRER_HLOAD"].ToString();
                    spinPWRER_OPERTIME.EditValue = dtBaseSetting.Rows[0]["PWRER_OPERTIME"].ToString();
                    spinCTL_SCTN.EditValue = dtBaseSetting.Rows[0]["CTL_SCTN"].ToString();
                }

                //ANALS_UF_PWRER_PREDICT
                DataTable dtBaseUFPREDICT = new DataTable();
                dtBaseUFPREDICT = work.Select_ANALS_UF_PWRER_PREDICT(null);

                if (dtBaseUFPREDICT.Rows.Count == 5)
                {
                    lb_uf_BeginTDS1.Content = dtBaseUFPREDICT.Rows[0]["BEGIN_TDS"].ToString();
                    lb_uf_BeginTDS2.Content = dtBaseUFPREDICT.Rows[1]["BEGIN_TDS"].ToString();
                    lb_uf_BeginTDS3.Content = dtBaseUFPREDICT.Rows[2]["BEGIN_TDS"].ToString();
                    lb_uf_BeginTDS4.Content = dtBaseUFPREDICT.Rows[3]["BEGIN_TDS"].ToString();
                    lb_uf_BeginTDS5.Content = dtBaseUFPREDICT.Rows[4]["BEGIN_TDS"].ToString();

                    lb_uf_EndTDS1.Content = dtBaseUFPREDICT.Rows[0]["END_TDS"].ToString();
                    lb_uf_EndTDS2.Content = dtBaseUFPREDICT.Rows[1]["END_TDS"].ToString();
                    lb_uf_EndTDS3.Content = dtBaseUFPREDICT.Rows[2]["END_TDS"].ToString();
                    lb_uf_EndTDS4.Content = dtBaseUFPREDICT.Rows[3]["END_TDS"].ToString();
                    lb_uf_EndTDS5.Content = dtBaseUFPREDICT.Rows[4]["END_TDS"].ToString();

                    cbUFMSRC1.EditValue = dtBaseUFPREDICT.Rows[0]["STDR"].ToString();
                    cbUFMSRC2.EditValue = dtBaseUFPREDICT.Rows[1]["STDR"].ToString();
                    cbUFMSRC3.EditValue = dtBaseUFPREDICT.Rows[2]["STDR"].ToString();
                    cbUFMSRC4.EditValue = dtBaseUFPREDICT.Rows[3]["STDR"].ToString();
                    cbUFMSRC5.EditValue = dtBaseUFPREDICT.Rows[4]["STDR"].ToString();

                    spin_uf_FILTER1.EditValue = dtBaseUFPREDICT.Rows[0]["FILTER"].ToString();
                    spin_uf_FILTER2.EditValue = dtBaseUFPREDICT.Rows[1]["FILTER"].ToString();
                    spin_uf_FILTER3.EditValue = dtBaseUFPREDICT.Rows[2]["FILTER"].ToString();
                    spin_uf_FILTER4.EditValue = dtBaseUFPREDICT.Rows[3]["FILTER"].ToString();
                    spin_uf_FILTER5.EditValue = dtBaseUFPREDICT.Rows[4]["FILTER"].ToString();

                    spin_uf_TIME1.EditValue = dtBaseUFPREDICT.Rows[0]["TIME"].ToString();
                    spin_uf_TIME2.EditValue = dtBaseUFPREDICT.Rows[1]["TIME"].ToString();
                    spin_uf_TIME3.EditValue = dtBaseUFPREDICT.Rows[2]["TIME"].ToString();
                    spin_uf_TIME4.EditValue = dtBaseUFPREDICT.Rows[3]["TIME"].ToString();
                    spin_uf_TIME5.EditValue = dtBaseUFPREDICT.Rows[4]["TIME"].ToString();
                }

                //ANALS_UF_RUNCND
                DataTable dtBaseUFRUNCND = new DataTable();
                dtBaseUFRUNCND = work.Select_ANALS_UF_RUNCND(null);

                if (dtBaseUFRUNCND.Rows.Count == 3)
                {
                    spinRUNCNDUFFLW1.EditValue = dtBaseUFRUNCND.Rows[0]["PRDCTN_FLW"].ToString();
                    spinRUNCNDUFFLW2.EditValue = dtBaseUFRUNCND.Rows[1]["PRDCTN_FLW"].ToString();
                    spinRUNCNDUFFLW3.EditValue = dtBaseUFRUNCND.Rows[2]["PRDCTN_FLW"].ToString();

                    spinRUNCNDUFAR1.EditValue = dtBaseUFRUNCND.Rows[0]["PRTITN_AR"].ToString();
                    spinRUNCNDUFAR2.EditValue = dtBaseUFRUNCND.Rows[1]["PRTITN_AR"].ToString();
                    spinRUNCNDUFAR3.EditValue = dtBaseUFRUNCND.Rows[2]["PRTITN_AR"].ToString();

                    spinRUNCNDUFCNT1.EditValue = dtBaseUFRUNCND.Rows[0]["PRTITN_CNT"].ToString();
                    spinRUNCNDUFCNT2.EditValue = dtBaseUFRUNCND.Rows[1]["PRTITN_CNT"].ToString();
                    spinRUNCNDUFCNT3.EditValue = dtBaseUFRUNCND.Rows[2]["PRTITN_CNT"].ToString();

                    spinRUNCNDUFFLUX1.EditValue = dtBaseUFRUNCND.Rows[0]["MXMM_FLUX"].ToString();
                    spinRUNCNDUFFLUX2.EditValue = dtBaseUFRUNCND.Rows[1]["MXMM_FLUX"].ToString();
                    spinRUNCNDUFFLUX3.EditValue = dtBaseUFRUNCND.Rows[2]["MXMM_FLUX"].ToString();
                }

                //ANALS_RO_RUNCND
                DataTable dtBaseRORUNCND = new DataTable();
                dtBaseRORUNCND = work.Select_ANALS_RO_RUNCND(null);

                if (dtBaseRORUNCND.Rows.Count == 2)
                {
                    spinROTDSSTDR1.EditValue = dtBaseRORUNCND.Rows[0]["TDS_STDR"].ToString();
                    spinROTDSSTDR2.EditValue = dtBaseRORUNCND.Rows[1]["TDS_STDR"].ToString();

                    spinROFLUXBMTB1.EditValue = dtBaseRORUNCND.Rows[0]["FLUX_BMTB"].ToString();
                    spinROFLUXBMTB2.EditValue = dtBaseRORUNCND.Rows[1]["FLUX_BMTB"].ToString();

                    spinRORUNCNDUFAR1.EditValue = dtBaseRORUNCND.Rows[0]["PRTITN_AR"].ToString();
                    spinRORUNCNDUFAR2.EditValue = dtBaseRORUNCND.Rows[1]["PRTITN_AR"].ToString();

                    spinRORUNCNDUFCNT1.EditValue = dtBaseRORUNCND.Rows[0]["PRTITN_CNT"].ToString();
                    spinRORUNCNDUFCNT2.EditValue = dtBaseRORUNCND.Rows[1]["PRTITN_CNT"].ToString();
                }
                #endregion
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

    }
}
