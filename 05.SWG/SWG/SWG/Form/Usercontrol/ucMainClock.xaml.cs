﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWG.Form.Usercontrol
{
    /// <summary>
    /// ucMainClock.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucMainClock : UserControl
    {
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        public ucMainClock()
        {
            InitializeComponent();
            Loaded += UcMainClock_Loaded;
        }

        private void UcMainClock_Loaded(object sender, RoutedEventArgs e)
        {
            timer.Tick += Timer_Tick;
            timer.Interval = 1000;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            lbDate.Content = string.Format(CultureInfo.GetCultureInfo("en-US"), @"{0:yyyy-MM-dd}", DateTime.Now).ToUpper();
            digitalClock.Text = string.Format(CultureInfo.GetCultureInfo("en-US"), @"{0:tt hh:mm:ss}", DateTime.Now).ToUpper();
            digitalClock.ToolTip = string.Format(CultureInfo.GetCultureInfo("en-US"), @"{0:yyyy-MM-dd tt hh:mm:ss}", DateTime.Now).ToUpper();
        }
    }
}
