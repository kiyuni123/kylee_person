﻿using CMFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace SWG.Form.Usercontrol
{
    /// <summary>
    /// ucMainWeather.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ucMainWeather : UserControl
    {
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        string strurl = "http://api.openweathermap.org/data/2.5/weather?lat=37.82&lon=124.71&mode=xml&appid=02cbec888a03f973303ad3615267238f";   //구글
        //string strurl = "http://api.openweathermap.org/data/2.5/weather?lat=37.82&lon=124.71&mode=xml&appid=893505cc5171909ae1947ecb9f1d995f"; //네이버
        
        static Hashtable htresult = null;
        static XmlDocument xmlDoc = null;

        public ucMainWeather()
        {
            InitializeComponent();
            Loaded += UcMainWeather_Loaded;
        }

        private void UcMainWeather_Loaded(object sender, RoutedEventArgs e)
        {
            timer.Tick += Timer_Tick;
            timer.Interval = 1000;
            timer.Start();

            SelectWEATHER(strurl);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Second == 00)
            {
                if (DateTime.Now.Minute == 00 || DateTime.Now.Minute == 10 || DateTime.Now.Minute == 20 || DateTime.Now.Minute == 30 || DateTime.Now.Minute == 40 || DateTime.Now.Minute == 50)
                {
                    SelectWEATHER(strurl);
                }
            }
        }

        private Hashtable SelectWEATHER(string url)
        {
            htresult = new Hashtable();

            try
            {
                xmlDoc = new XmlDocument();
                xmlDoc.Load(url);

                if (xmlDoc != null)
                {
                    htresult = XmlParsing(xmlDoc);

                    //아이콘
                    if (((Hashtable)htresult["htweather"]).ContainsKey("NUMBER"))
                    {
                        if(!((Hashtable)htresult["htweather"])["NUMBER"].ToString().Equals(""))
                        {
                            char timePeriod = (((Hashtable)htresult["htweather"])["ICON"].ToString().ToCharArray())[2];
                            int intnumber = 0;
                            int.TryParse(((Hashtable)htresult["htweather"])["NUMBER"].ToString(), out intnumber);
                            string imguri = string.Empty;

                            if (intnumber >= 200 && intnumber < 300) imguri = "thunderstorm.png";
                            else if (intnumber >= 300 && intnumber < 500) imguri = "drizzle.png";
                            else if (intnumber >= 500 && intnumber < 600) imguri = "rain.png";
                            else if (intnumber >= 600 && intnumber < 700) imguri = "snow.png";
                            else if (intnumber >= 700 && intnumber < 800) imguri = "atmosphere.png";
                            else if (intnumber == 800) imguri = (timePeriod == 'd') ? "clear_day.png" : "clear_night.png";
                            else if (intnumber == 801) imguri = (timePeriod == 'd') ? "few_clouds_day.png" : "few_clouds_night.png";
                            else if (intnumber == 802 || intnumber == 803) imguri = (timePeriod == 'd') ? "broken_clouds_day.png" : "broken_clouds_night.png";
                            else if (intnumber == 804) imguri = "overcast_clouds.png";
                            else if (intnumber >= 900 && intnumber < 903) imguri = "extreme.png";
                            else if (intnumber == 903) imguri = "cold.png";
                            else if (intnumber == 904) imguri = "hot.png";
                            else if (intnumber == 905 || intnumber >= 951) imguri = "windy.png";
                            else if (intnumber == 906) imguri = "hail.png";

                            Uri source = new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\Resources\\Image\\WeatherImg\\" + imguri, UriKind.Absolute);

                            BitmapImage bmp = new BitmapImage();
                            bmp.BeginInit();
                            bmp.UriSource = source;
                            bmp.EndInit();

                            imgWeather.Source = bmp;
                        }
                    }

                    //기온
                    if (((Hashtable)htresult["httemperature"]).ContainsKey("VALUE"))
                    {
                        lbtpvalue.Content = ((Hashtable)htresult["httemperature"])["VALUE"].ToString();
                        lbtpmax.Content = ((Hashtable)htresult["httemperature"])["MAX"].ToString();
                        lbtpmin.Content = ((Hashtable)htresult["httemperature"])["MIN"].ToString();
                        lbtpmax2.Content = ((Hashtable)htresult["httemperature"])["MAX"].ToString();
                        lbtpmin2.Content = ((Hashtable)htresult["httemperature"])["MIN"].ToString();
                    }

                    //풍속
                    if (((Hashtable)htresult["htwindspeed"]).ContainsKey("VALUE"))
                    {
                        double douwindspeed = 0;

                        if(double.TryParse(((Hashtable)htresult["htwindspeed"])["VALUE"].ToString(), out douwindspeed))
                        {
                            lbwindspeed.Content = Math.Round((douwindspeed / 2.237), 2, MidpointRounding.AwayFromZero).ToString() + " m/s";
                            lbwindspeed2.Content = Math.Round((douwindspeed / 2.237), 2, MidpointRounding.AwayFromZero).ToString() + " m/s";
                        }
                    }

                    //업데이트 시간
                    if (((Hashtable)htresult["htlastupdate"]).ContainsKey("VALUE"))
                    {
                        

                        int intYear = 0;
                        int intMonth = 0;
                        int intDay = 0;
                        int intHour = 0;
                        int intMinute = 0;
                        int intSecond = 0;

                        if(int.TryParse(((Hashtable)htresult["htlastupdate"])["VALUE"].ToString().Substring(0, 4), out intYear)
                            && int.TryParse(((Hashtable)htresult["htlastupdate"])["VALUE"].ToString().Substring(5, 2), out intMonth)
                            && int.TryParse(((Hashtable)htresult["htlastupdate"])["VALUE"].ToString().Substring(8, 2), out intDay)
                            && int.TryParse(((Hashtable)htresult["htlastupdate"])["VALUE"].ToString().Substring(11, 2), out intHour)
                            && int.TryParse(((Hashtable)htresult["htlastupdate"])["VALUE"].ToString().Substring(14, 2), out intMinute)
                            && int.TryParse(((Hashtable)htresult["htlastupdate"])["VALUE"].ToString().Substring(17, 2), out intSecond))
                        {
                            DateTime dtLastUpdate = new DateTime(intYear, intMonth, intDay, intHour, intMinute, intSecond);
                            lbLastUpdate.Content = dtLastUpdate.AddHours(9).ToString("yyyy-MM-dd HH:mm:ss");
                        }
                    }

                    

                    ////일출
                    //if (((Hashtable)htresult["htsun"]).ContainsKey("RISE"))
                    //{
                    //    lbrise.Content = ((Hashtable)htresult["htsun"])["RISE"].ToString().Substring(11, 8);
                    //}

                    ////일몰
                    //if (((Hashtable)htresult["htsun"]).ContainsKey("SET"))
                    //{
                    //    lbset.Content = ((Hashtable)htresult["htsun"])["SET"].ToString().Substring(11, 8);
                    //}

                    ////습도
                    //if (((Hashtable)htresult["hthumidity"]).ContainsKey("VALUE"))
                    //{
                    //    lbhumidity.Content = ((Hashtable)htresult["hthumidity"])["VALUE"].ToString() + "%";
                    //}

                    ////기압
                    //if (((Hashtable)htresult["htpressure"]).ContainsKey("VALUE"))
                    //{
                    //    lbpressure.Content = ((Hashtable)htresult["htpressure"])["VALUE"].ToString() + "hPa";
                    //}

                    //구름
                    //if (((Hashtable)htresult["htclouds"]).ContainsKey("VALUE"))
                    //{
                    //    lbpressure.Content = ((Hashtable)htresult["htclouds"])["VALUE"].ToString() + "hPa";
                    //}
                }
            }
            catch (Exception e)
            {
                htresult = null;
            }
            finally
            {
                xmlDoc = null;
            }

            return htresult;
        }

        /// <summary>
        /// XmlParsing
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private static Hashtable XmlParsing(XmlDocument xmlDoc)
        {
            XmlNodeList xmlNode = null;
            Hashtable htresult = new Hashtable();

            try
            {
                //기온 정보 Parsing
                xmlNode = xmlDoc.SelectNodes("/current/temperature");
                if (xmlNode.Count == 1)
                {
                    Hashtable httemperature = new Hashtable();

                    foreach (XmlAttribute item in xmlNode[0].Attributes)
                    {
                        double tempdou;
                        if(double.TryParse(item.Value, out tempdou))
                        {
                            httemperature[item.Name.ToUpper()] = Math.Round(tempdou - 273.15, 2, MidpointRounding.AwayFromZero).ToString();
                        }
                    }

                    htresult["httemperature"] = httemperature;
                }

                //날씨
                xmlNode = xmlDoc.SelectNodes("/current/weather");
                if (xmlNode.Count == 1)
                {
                    Hashtable htweather = new Hashtable();

                    foreach (XmlAttribute item in xmlNode[0].Attributes)
                    {
                        htweather[item.Name.ToUpper()] = item.Value;
                    }

                    htresult["htweather"] = htweather;
                }

                //습도
                xmlNode = xmlDoc.SelectNodes("/current/humidity");
                if (xmlNode.Count == 1)
                {
                    Hashtable hthumidity = new Hashtable();

                    foreach (XmlAttribute item in xmlNode[0].Attributes)
                    {
                        hthumidity[item.Name.ToUpper()] = item.Value;
                    }

                    htresult["hthumidity"] = hthumidity;
                }

                //기압
                xmlNode = xmlDoc.SelectNodes("/current/pressure");
                if (xmlNode.Count == 1)
                {
                    Hashtable htpressure = new Hashtable();

                    foreach (XmlAttribute item in xmlNode[0].Attributes)
                    {
                        htpressure[item.Name.ToUpper()] = item.Value;
                    }

                    htresult["htpressure"] = htpressure;
                }

                //구름
                xmlNode = xmlDoc.SelectNodes("/current/clouds");
                if (xmlNode.Count == 1)
                {
                    Hashtable htclouds = new Hashtable();

                    foreach (XmlAttribute item in xmlNode[0].Attributes)
                    {
                        htclouds[item.Name.ToUpper()] = item.Value;
                    }

                    htresult["htclouds"] = htclouds;
                }

                //일출/일몰
                xmlNode = xmlDoc.SelectNodes("/current/city/sun");
                if (xmlNode.Count == 1)
                {
                    Hashtable htsun = new Hashtable();

                    foreach (XmlAttribute item in xmlNode[0].Attributes)
                    {
                        htsun[item.Name.ToUpper()] = item.Value;
                    }

                    htresult["htsun"] = htsun;
                }

                //풍속
                xmlNode = xmlDoc.SelectNodes("/current/wind/speed");
                if (xmlNode.Count == 1)
                {
                    Hashtable htwindspeed = new Hashtable();

                    foreach (XmlAttribute item in xmlNode[0].Attributes)
                    {
                        htwindspeed[item.Name.ToUpper()] = item.Value;
                    }

                    htresult["htwindspeed"] = htwindspeed;
                }

                //업데이트시간
                xmlNode = xmlDoc.SelectNodes("/current/lastupdate");
                if (xmlNode.Count == 1)
                {
                    Hashtable htlastupdate = new Hashtable();

                    foreach (XmlAttribute item in xmlNode[0].Attributes)
                    {
                        htlastupdate[item.Name.ToUpper()] = item.Value;
                    }

                    htresult["htlastupdate"] = htlastupdate;
                }
            }
            catch (Exception e)
            {
                htresult = null;
            }
            finally
            {
                xmlDoc = null;
                xmlNode = null;
            }

            return htresult;
        }
    }
}
