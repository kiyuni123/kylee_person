﻿using CMFramework.Common.MessageBox;
using CMFramework.Common.Utils.ViewEffect;
using SWG.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWG.Form.Popup
{
    /// <summary>
    /// popupCustomSetting.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class popupCustomSetting : Window
    {
        SelectWork work = new SelectWork();
        AnalsWork analwork = new AnalsWork();
        DataTable dttemp = new DataTable();
        string strMSRC;
        string strGBN;

        public popupCustomSetting(string strMSRC_, string strGBN_)
        {
            strMSRC = strMSRC_;
            strGBN = strGBN_;
            InitializeComponent();
            Loaded += PopupCustomSetting_Loaded;
        }

        private void PopupCustomSetting_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow.screenfade();
            ThemeApply.Themeapply(this);
            InitializeEvent();
            InitializeData();
        }

        private void InitializeEvent()
        {
            btnOk.Click += BtnOk_Click;
            btnhrSetting.Click += BtnhrSetting_Click;
            radioone.Checked += Radio_Checked;
            radiotwo.Checked += Radio_Checked;
            radiothree.Checked += Radio_Checked;
            radiofour.Checked += Radio_Checked;
        }

        private void BtnhrSetting_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double d1 = 0;
                double d2 = 0;
                double d3 = 0;
                double d4 = 0;

                #region 100% 확인
                if (radioone.IsChecked == true)
                {
                    if (cbone.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("모든 수원을 선택해야 합니다.");
                        return;
                    }

                    if (txtoneper.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("모든 비율값을 입력해야 됩니다.");
                        return;
                    }

                    double.TryParse(txtoneper.EditValue.ToString(), out d1);

                    if (d1 != 100)
                    {
                        Messages.ShowInfoMsgBox("모든 비율의 합이 100%가 되어야 합니다.");
                        return;
                    }
                }
                else if (radiotwo.IsChecked == true)
                {
                    if (cbone.Text.Equals("") && cbtwo.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("모든 수원을 선택해야 합니다.");
                        return;
                    }

                    if (txtoneper.Text.Equals("") && txttwoper.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("모든 비율값을 입력해야 됩니다.");
                        return;
                    }

                    double.TryParse(txtoneper.EditValue.ToString(), out d1);
                    double.TryParse(txttwoper.EditValue.ToString(), out d2);

                    if (d1 + d2 != 100)
                    {
                        Messages.ShowInfoMsgBox("모든 비율의 합이 100%가 되어야 합니다.");
                        return;
                    }


                }
                else if (radiothree.IsChecked == true)
                {
                    if (cbone.Text.Equals("") && cbtwo.Text.Equals("") && cbthree.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("모든 수원을 선택해야 합니다.");
                        return;
                    }

                    if (txtoneper.Text.Equals("") && txttwoper.Text.Equals("") && txtthreeper.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("모든 비율값을 입력해야 됩니다.");
                        return;
                    }

                    double.TryParse(txtoneper.EditValue.ToString(), out d1);
                    double.TryParse(txttwoper.EditValue.ToString(), out d2);
                    double.TryParse(txtthreeper.EditValue.ToString(), out d3);

                    if (d1 + d2 + d3 != 100)
                    {
                        Messages.ShowInfoMsgBox("모든 비율의 합이 100%가 되어야 합니다.");
                        return;
                    }
                }
                else
                {
                    if (cbone.Text.Equals("") && cbtwo.Text.Equals("") && cbthree.Text.Equals("") && cbfour.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("모든 수원을 선택해야 합니다.");
                        return;
                    }

                    if (txtoneper.Text.Equals("") && txttwoper.Text.Equals("") && txtthreeper.Text.Equals("") && txtfourper.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("모든 비율값을 입력해야 됩니다.");
                        return;
                    }

                    double.TryParse(txtoneper.EditValue.ToString(), out d1);
                    double.TryParse(txttwoper.EditValue.ToString(), out d2);
                    double.TryParse(txtthreeper.EditValue.ToString(), out d3);
                    double.TryParse(txtfourper.EditValue.ToString(), out d4);

                    if (d1 + d2 + d3 + d4 != 100)
                    {
                        Messages.ShowInfoMsgBox("모든 비율의 합이 100%가 되어야 합니다.");
                        return;
                    }
                }
                #endregion

                DataTable dtbasicinfo = new DataTable();
                dtbasicinfo = work.Select_Custom_ANALS_SETTING(null);

                DataTable dtresult = new DataTable();
                dtresult.Columns.Add("GBN");
                dtresult.Columns.Add("DTL_RANK");
                dtresult.Columns.Add("TDS", typeof(double));
                dtresult.Columns.Add("RUN_RATE", typeof(double));
                dtresult.Columns.Add("USEFLW", typeof(double));

                if (dtbasicinfo.Rows.Count == 1)
                {
                    DataRow r1 = dtresult.NewRow();
                    DataRow r2 = dtresult.NewRow();
                    DataRow r3 = dtresult.NewRow();
                    DataRow r4 = dtresult.NewRow();

                    if (radioone.IsChecked == true)
                    {

                        r1["GBN"] = cbone.EditValue;
                        r1["DTL_RANK"] = 1;
                        r1["RUN_RATE"] = Convert.ToDouble(txtoneper.EditValue);
                        r1["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbone.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r1);
                    }

                    if (radiotwo.IsChecked == true)
                    {
                        r1["GBN"] = cbone.EditValue;
                        r1["DTL_RANK"] = 1;
                        r1["RUN_RATE"] = Convert.ToDouble(txtoneper.EditValue);
                        r1["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbone.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r1);

                        r2["GBN"] = cbtwo.EditValue;
                        r2["DTL_RANK"] = 2;
                        r2["RUN_RATE"] = Convert.ToDouble(txttwoper.EditValue);
                        r2["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbtwo.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r2);
                    }

                    if (radiothree.IsChecked == true)
                    {
                        r1["GBN"] = cbone.EditValue;
                        r1["DTL_RANK"] = 1;
                        r1["RUN_RATE"] = Convert.ToDouble(txtoneper.EditValue);
                        r1["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbone.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r1);

                        r2["GBN"] = cbtwo.EditValue;
                        r2["DTL_RANK"] = 2;
                        r2["RUN_RATE"] = Convert.ToDouble(txttwoper.EditValue);
                        r2["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbtwo.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r2);

                        r3["GBN"] = cbthree.EditValue;
                        r3["DTL_RANK"] = 3;
                        r3["RUN_RATE"] = Convert.ToDouble(txtthreeper.EditValue);
                        r3["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbthree.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r3);
                    }

                    if (radiofour.IsChecked == true)
                    {
                        r1["GBN"] = cbone.EditValue;
                        r1["DTL_RANK"] = 1;
                        r1["RUN_RATE"] = Convert.ToDouble(txtoneper.EditValue);
                        r1["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbone.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r1);

                        r2["GBN"] = cbtwo.EditValue;
                        r2["DTL_RANK"] = 2;
                        r2["RUN_RATE"] = Convert.ToDouble(txttwoper.EditValue);
                        r2["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbtwo.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r2);

                        r3["GBN"] = cbthree.EditValue;
                        r3["DTL_RANK"] = 3;
                        r3["RUN_RATE"] = Convert.ToDouble(txtthreeper.EditValue);
                        r3["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbthree.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r3);

                        r4["GBN"] = cbfour.EditValue;
                        r4["DTL_RANK"] = 4;
                        r4["RUN_RATE"] = Convert.ToDouble(txtfourper.EditValue);
                        r4["TDS"] = Convert.ToDouble(dtbasicinfo.Rows[0][cbfour.EditValue + "_TDS"]);
                        dtresult.Rows.Add(r4);
                    }

                    DataRow rblending1 = dtresult.NewRow();
                    rblending1["GBN"] = "Blending";
                    rblending1["RUN_RATE"] = 0;
                    rblending1["USEFLW"] = 0;

                    double B_TDS = 0;

                    foreach (DataRow r in dtresult.Rows)
                    {
                        rblending1["RUN_RATE"] = Convert.ToDouble(rblending1["RUN_RATE"]) + Convert.ToDouble(r["RUN_RATE"]);
                        B_TDS = B_TDS + (Convert.ToDouble(r["TDS"]) * Convert.ToDouble(r["RUN_RATE"]) / 100);
                    }
                    rblending1["TDS"] = B_TDS;

                    dtresult.Rows.Add(rblending1);

                    DateTime dtAnalStart = Convert.ToDateTime(dtbasicinfo.Rows[0]["BEGIN_DT"].ToString());
                    DateTime dtAnalEnd = Convert.ToDateTime(dtbasicinfo.Rows[0]["END_DT"].ToString());

                    DataTable uftemp = new DataTable();
                    DataTable rotemp = new DataTable();

                    for (DateTime dt = dtAnalStart; dt < dtAnalEnd.AddDays(1); dt = dt.AddDays(1))
                    {
                        Hashtable conditionsBlending = new Hashtable();
                        conditionsBlending.Add("SET_SEQ", dtbasicinfo.Rows[0]["SET_SEQ"].ToString());
                        conditionsBlending.Add("GBN", 5);
                        conditionsBlending.Add("RANK", 1);
                        conditionsBlending.Add("USE_YN", "Y");
                        conditionsBlending.Add("B_TDS", rblending1["TDS"].ToString());
                        conditionsBlending.Add("B_RUN_RATE", rblending1["RUN_RATE"].ToString());
                        conditionsBlending.Add("B_USEFLW", rblending1["USEFLW"].ToString());
                        conditionsBlending.Add("DT", dt.ToString("yyyyMMddHHmmss"));

                        //UF 분석

                        Hashtable ufconditions = new Hashtable();
                        if (radioone.IsChecked == true)
                            ufconditions.Add("GBN", 1);
                        else if (radiotwo.IsChecked == true)
                            ufconditions.Add("GBN", 2);
                        else if (radiothree.IsChecked == true)
                            ufconditions.Add("GBN", 3);
                        else
                            ufconditions.Add("GBN", 4);
                        ufconditions.Add("B_TDS", rblending1["TDS"].ToString());


                        uftemp = analwork.Select_ANALS_UF_ANAL(ufconditions);
                        conditionsBlending.Add("UF_MSRC", uftemp.Rows[0]["STDR"].ToString());
                        conditionsBlending.Add("UF_FILTER", uftemp.Rows[0]["FILTER"].ToString());
                        conditionsBlending.Add("UF_TIME", uftemp.Rows[0]["TIME"].ToString());
                        conditionsBlending.Add("UF_FLUX", uftemp.Rows[0]["FLUX"].ToString());
                        conditionsBlending.Add("ER_UF", uftemp.Rows[0]["PWRER_VAL"].ToString());

                        //RO 분석
                        conditionsBlending.Add("DAYFLW", dtbasicinfo.Rows[0]["DAY_FLW"].ToString());

                        ufconditions.Add("DAYFLW", dtbasicinfo.Rows[0]["DAY_FLW"].ToString());
                        rotemp = analwork.Select_ANALS_RO_ANAL(ufconditions);
                        conditionsBlending.Add("RO_MSRC", rotemp.Rows[0]["MSRC"].ToString());
                        conditionsBlending.Add("RO_PRESSR", rotemp.Rows[0]["TRND_CAL"].ToString());
                        conditionsBlending.Add("RO_FLUX", rotemp.Rows[0]["FLUX_BMTB"].ToString());
                        if (rotemp.Rows[0]["CTLVALV"].ToString().Equals("1"))
                            conditionsBlending.Add("RO_CTL", "Y");
                        else
                            conditionsBlending.Add("RO_CTL", "N");
                        conditionsBlending.Add("RO_TOTHR", rotemp.Rows[0]["RO_TOTHR"].ToString());
                        conditionsBlending.Add("RO_TOTMIN", rotemp.Rows[0]["RO_TOTMIN"].ToString());
                        conditionsBlending.Add("ER_RO", rotemp.Rows[0]["PWRER_VAL"].ToString());
                        conditionsBlending.Add("ER_TOT", Convert.ToDouble(rotemp.Rows[0]["PWRER_VAL"]) + Convert.ToDouble(uftemp.Rows[0]["PWRER_VAL"]));

                        conditionsBlending.Add("MSRC", rotemp.Rows[0]["MSRC"].ToString());

                        double dNeedFLW = Convert.ToDouble(dtbasicinfo.Rows[0][rotemp.Rows[0]["MSRC"].ToString().Substring(0, 2) + "NEED_FLW"]);

                        if (radioone.IsChecked == true)
                        {
                            r1["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                        }
                        if (radiotwo.IsChecked == true)
                        {
                            r1["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                            r2["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                        }
                        if (radiothree.IsChecked == true)
                        {
                            r1["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                            r2["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                            r3["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                        }
                        if (radiofour.IsChecked == true)
                        {
                            r1["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                            r2["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                            r3["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                            r4["USEFLW"] = dNeedFLW * Convert.ToDouble(r1["RUN_RATE"]) / 100;
                        }

                        foreach (DataRow r in dtresult.Rows)
                        {
                            rblending1["USEFLW"] = Convert.ToDouble(rblending1["USEFLW"]) + Convert.ToDouble(r["USEFLW"]);
                        }

                        //기존 데이터 Delete
                        if(dttemp.Rows.Count == 1)
                        {
                            Hashtable Deleteconditions = new Hashtable();
                            Deleteconditions.Add("SET_SEQ", dttemp.Rows[0]["SET_SEQ"].ToString());
                            Deleteconditions.Add("GBN", dttemp.Rows[0]["GBN"].ToString());
                            analwork.Delete_ANALS_RESULT_BLENDING(Deleteconditions);
                        }
                        

                        analwork.Insert_ANALS_RESULT_BLENDING(conditionsBlending);

                        foreach (DataRow r in dtresult.Rows)
                        {
                            if (!r["GBN"].Equals("Blending"))
                            {
                                Hashtable conditionsDTL = new Hashtable();
                                conditionsDTL.Add("KIND", r["GBN"].ToString());
                                conditionsDTL.Add("DT", dt.ToString("yyyyMMddHHmmss"));
                                conditionsDTL.Add("MSRC", rotemp.Rows[0]["MSRC"].ToString());
                                conditionsDTL.Add("GBN", 5);
                                conditionsDTL.Add("RANK", 1);
                                conditionsDTL.Add("TDS", Math.Round(Convert.ToDouble(r["TDS"]), 5, MidpointRounding.ToEven));
                                conditionsDTL.Add("RUN_RATE", Math.Round(Convert.ToDouble(r["RUN_RATE"]), 2, MidpointRounding.ToEven));
                                conditionsDTL.Add("USEFLW", Math.Round(Convert.ToDouble(r["USEFLW"]), 5, MidpointRounding.ToEven));
                                conditionsDTL.Add("DTL_RANK", r["DTL_RANK"].ToString());
                                analwork.Insert_ANALS_RESULT_DTL(conditionsDTL);
                            }
                        }
                    }
                }

                Messages.ShowOkMsgBox();
                this.Close();
                MainWindow.screenfade();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                MainWindow.screenfade();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Radio_Checked(object sender, RoutedEventArgs e)
        {
            switch (((RadioButton)sender).Name)
            {
                case "radioone":
                    cbone.IsEnabled = true;
                    txtoneper.IsEnabled = true;
                    txtoneintake.IsEnabled = true;

                    cbtwo.IsEnabled = false;
                    txttwoper.IsEnabled = false;
                    txttwointake.IsEnabled = false;

                    cbthree.IsEnabled = false;
                    txtthreeper.IsEnabled = false;
                    txtthreeintake.IsEnabled = false;

                    cbfour.IsEnabled = false;
                    txtfourper.IsEnabled = false;
                    txtfourintake.IsEnabled = false;
                    break;
                case "radiotwo":
                    cbone.IsEnabled = true;
                    txtoneper.IsEnabled = true;
                    txtoneintake.IsEnabled = true;

                    cbtwo.IsEnabled = true;
                    txttwoper.IsEnabled = true;
                    txttwointake.IsEnabled = true;

                    cbthree.IsEnabled = false;
                    txtthreeper.IsEnabled = false;
                    txtthreeintake.IsEnabled = false;

                    cbfour.IsEnabled = false;
                    txtfourper.IsEnabled = false;
                    txtfourintake.IsEnabled = false;
                    break;
                case "radiothree":
                    cbone.IsEnabled = true;
                    txtoneper.IsEnabled = true;
                    txtoneintake.IsEnabled = true;

                    cbtwo.IsEnabled = true;
                    txttwoper.IsEnabled = true;
                    txttwointake.IsEnabled = true;

                    cbthree.IsEnabled = true;
                    txtthreeper.IsEnabled = true;
                    txtthreeintake.IsEnabled = true;

                    cbfour.IsEnabled = false;
                    txtfourper.IsEnabled = false;
                    txtfourintake.IsEnabled = false;
                    break;
                case "radiofour":
                    cbone.IsEnabled = true;
                    txtoneper.IsEnabled = true;
                    txtoneintake.IsEnabled = true;

                    cbtwo.IsEnabled = true;
                    txttwoper.IsEnabled = true;
                    txttwointake.IsEnabled = true;

                    cbthree.IsEnabled = true;
                    txtthreeper.IsEnabled = true;
                    txtthreeintake.IsEnabled = true;

                    cbfour.IsEnabled = true;
                    txtfourper.IsEnabled = true;
                    txtfourintake.IsEnabled = true;
                    break;
            }

            //cbone.SelectedIndex = -1;
            //cbtwo.SelectedIndex = -1;
            //cbthree.SelectedIndex = -1;
            //cbfour.SelectedIndex = -1;
        }

        private void InitializeData()
        {
            DataTable dtcbGBNData = new DataTable();
            dtcbGBNData.Columns.Add("code");
            dtcbGBNData.Columns.Add("name");

            DataRow r1item = dtcbGBNData.NewRow();
            r1item["code"] = "SEAWTR";
            r1item["name"] = "해수";
            dtcbGBNData.Rows.Add(r1item);

            DataRow r2item = dtcbGBNData.NewRow();
            r2item["code"] = "UGRWTR";
            r2item["name"] = "지하수";
            dtcbGBNData.Rows.Add(r2item);

            DataRow r3item = dtcbGBNData.NewRow();
            r3item["code"] = "BRKWTR";
            r3item["name"] = "지표수";
            dtcbGBNData.Rows.Add(r3item);

            DataRow r4item = dtcbGBNData.NewRow();
            r4item["code"] = "GRDWTR";
            r4item["name"] = "기수";
            dtcbGBNData.Rows.Add(r4item);

            cbone.ItemsSource = dtcbGBNData.Copy();
            cbtwo.ItemsSource = dtcbGBNData.Copy();
            cbthree.ItemsSource = dtcbGBNData.Copy();
            cbfour.ItemsSource = dtcbGBNData.Copy();

            cbone.SelectedIndex = -1;
            cbtwo.SelectedIndex = -1;
            cbthree.SelectedIndex = -1;
            cbfour.SelectedIndex = -1;

            SelectData();
        }

        private void SelectData()
        {
            try
            {   
                DataTable dtresult = new DataTable();

                DataTable dtresultDTL1 = new DataTable();
                dtresultDTL1.Columns.Add("col1");
                dtresultDTL1.Columns.Add("col2");
                dtresultDTL1.Columns.Add("col3");

                DataRow rDTL1 = dtresultDTL1.NewRow();
                DataRow rDTL2 = dtresultDTL1.NewRow();
                DataRow rDTL3 = dtresultDTL1.NewRow();
                DataRow rDTL4 = dtresultDTL1.NewRow();
                DataRow rDTL5 = dtresultDTL1.NewRow();
                DataRow rDTL6 = dtresultDTL1.NewRow();
                DataRow rDTL7 = dtresultDTL1.NewRow();
                DataRow rDTL8 = dtresultDTL1.NewRow();
                DataRow rDTL9 = dtresultDTL1.NewRow();
                DataRow rDTL10 = dtresultDTL1.NewRow();
                DataRow rDTL11 = dtresultDTL1.NewRow();
                DataRow rDTL12 = dtresultDTL1.NewRow();
                DataRow rDTL13 = dtresultDTL1.NewRow();

                rDTL1["col1"] = "UF운전조건";
                rDTL2["col1"] = "UF운전조건";
                rDTL3["col1"] = "UF운전조건";
                rDTL4["col1"] = "UF운전조건";
                rDTL5["col1"] = "UF운전조건";
                rDTL6["col1"] = "RO운전조건";
                rDTL7["col1"] = "RO운전조건";
                rDTL8["col1"] = "RO운전조건";
                rDTL9["col1"] = "RO운전조건";
                rDTL10["col1"] = "RO운전조건";
                rDTL11["col1"] = "RO운전조건";
                rDTL12["col1"] = "RO운전조건";
                rDTL13["col1"] = "총에너지소비량";

                rDTL1["col2"] = "UF 운전 조건 (UF1/UF2)";
                rDTL2["col2"] = "여과시간(min)";
                rDTL3["col2"] = "역세시간(min)";
                rDTL4["col2"] = "여과 Flux(lmh)";
                rDTL5["col2"] = "UF 전력소비량(kwh/㎥)";
                rDTL6["col2"] = "운전조건(SWRO/BWRO)";
                rDTL7["col2"] = "운전압력(bar)";
                rDTL8["col2"] = "예상 Flux(lmh)";
                rDTL9["col2"] = "컨트롤벨브";
                rDTL10["col2"] = "총운전시간 (hr)";
                rDTL11["col2"] = "총운전시간 (min)";
                rDTL12["col2"] = "RO 전력소비량(kwh/㎥)";
                rDTL13["col2"] = "에너지소비량 (UF+RO)";

                if (strMSRC == "")
                    return;
                else
                {
                    Hashtable conditions = new Hashtable();
                    conditions.Add("MSRC", strMSRC);
                    conditions.Add("GBN", strGBN);
                    dtresult = work.Select_EconomicsDTL(conditions);
                    dttemp = dtresult.DefaultView.ToTable(true, "SET_SEQ", "MSRC", "RANK", "GBN", "B_TDS", "UF_MSRC", "UF_FILTER", "UF_TIME", "UF_FLUX", "RO_MSRC", "RO_PRESSR", "RO_FLUX", "RO_CTL", "RO_TOTHR", "RO_TOTMIN", "ER_UF", "ER_RO", "ER_TOT", "USE_YN");
                }

                switch (dtresult.Rows.Count)
                {
                    case 1:
                        radioone.IsChecked = true;
                        break;
                    case 2:
                        radiotwo.IsChecked = true;
                        break;
                    case 3:
                        radiothree.IsChecked = true;
                        break;
                    case 4:
                        radiofour.IsChecked = true;
                        break;
                }


                foreach (DataRow r in dtresult.Rows)
                {
                    if (r["DTL_RANK"].ToString().Equals("1"))
                    {
                        cbone.EditValue = r["KIND"].ToString();
                        txtoneper.EditValue = r["RUN_RATE"].ToString();
                    }

                    if (r["DTL_RANK"].ToString().Equals("2"))
                    {
                        cbtwo.EditValue = r["KIND"].ToString();
                        txttwoper.EditValue = r["RUN_RATE"].ToString();
                    }

                    if (r["DTL_RANK"].ToString().Equals("3"))
                    {
                        cbthree.EditValue = r["KIND"].ToString();
                        txtthreeper.EditValue = r["RUN_RATE"].ToString();
                    }

                    if (r["DTL_RANK"].ToString().Equals("4"))
                    {
                        cbfour.EditValue = r["KIND"].ToString();
                        txtfourper.EditValue = r["RUN_RATE"].ToString();
                    }
                }

                rDTL1["col3"] = dttemp.Rows[0]["UF_MSRC"].ToString();
                rDTL2["col3"] = dttemp.Rows[0]["UF_FILTER"].ToString();
                rDTL3["col3"] = dttemp.Rows[0]["UF_TIME"].ToString();
                rDTL4["col3"] = dttemp.Rows[0]["UF_FLUX"].ToString();
                rDTL5["col3"] = dttemp.Rows[0]["ER_UF"].ToString();
                rDTL6["col3"] = dttemp.Rows[0]["RO_MSRC"].ToString();
                rDTL7["col3"] = dttemp.Rows[0]["RO_PRESSR"].ToString();
                rDTL8["col3"] = dttemp.Rows[0]["RO_FLUX"].ToString();
                rDTL9["col3"] = dttemp.Rows[0]["RO_CTL"].ToString();
                rDTL10["col3"] = dttemp.Rows[0]["RO_TOTHR"].ToString();
                rDTL11["col3"] = dttemp.Rows[0]["RO_TOTMIN"].ToString();
                rDTL12["col3"] = dttemp.Rows[0]["ER_RO"].ToString();
                rDTL13["col3"] = dttemp.Rows[0]["ER_TOT"].ToString();

                dtresultDTL1.Rows.Add(rDTL1);
                dtresultDTL1.Rows.Add(rDTL2);
                dtresultDTL1.Rows.Add(rDTL3);
                dtresultDTL1.Rows.Add(rDTL4);
                dtresultDTL1.Rows.Add(rDTL5);
                dtresultDTL1.Rows.Add(rDTL6);
                dtresultDTL1.Rows.Add(rDTL7);
                dtresultDTL1.Rows.Add(rDTL8);
                dtresultDTL1.Rows.Add(rDTL9);
                dtresultDTL1.Rows.Add(rDTL10);
                dtresultDTL1.Rows.Add(rDTL11);
                dtresultDTL1.Rows.Add(rDTL12);
                dtresultDTL1.Rows.Add(rDTL13);

                gridDTL1.ItemsSource = dtresultDTL1;

            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
