﻿using CMFramework.Common.MessageBox;
using CMFramework.Common.Utils.ViewEffect;
using DevExpress.Xpf.Grid;
using SWG.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWG.Form.Popup
{
    /// <summary>
    /// PopupROResultDTL.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupROResultDTL : Window
    {
        string strMSRC;
        string strGBN;

        DataRow dr1;
        DataRow dr2;
        DataRow dr3;

        SelectWork work = new SelectWork();

        public PopupROResultDTL(string strMSRC_, string strGBN_)
        {
            strMSRC = strMSRC_;
            strGBN = strGBN_;
            InitializeComponent();
            InitializeEvent();
            Loaded += PopupROResultDTL_Loaded;
        }

        private void PopupROResultDTL_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow.screenfade();
            ThemeApply.Themeapply(this);
            SelectData();
        }

        private void InitializeEvent()
        {
            btnOk.Click += BtnOk_Click;
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DataRow dr;
            Hashtable conditions = new Hashtable();

            if (radiogbn1.IsChecked == true)
                dr = dr1;
            else if (radiogbn2.IsChecked == true)
                dr = dr2;
            else
                dr = dr3;

            conditions.Add("SET_SEQ", dr["SET_SEQ"].ToString());
            conditions.Add("MSRC", dr["MSRC"].ToString());
            conditions.Add("GBN", dr["GBN"].ToString());
            conditions.Add("RANK", dr["RANK"].ToString());

            work.Update_ANALS_RESULT_BLENDING_USEYN_N(conditions);
            work.Update_ANALS_RESULT_BLENDING_USEYN_Y(conditions);

            this.Close();
            MainWindow.screenfade();
        }

        private void SelectData()
        {
            try
            {
                Hashtable conditions = new Hashtable();

                DataTable dtresult = new DataTable();
                conditions.Add("MSRC", strMSRC);
                conditions.Add("GBN", strGBN);
                dtresult = work.Select_EconomicsDTL(conditions);

                DataTable dttemp = new DataTable();
                dttemp = dtresult.DefaultView.ToTable(true, "SET_SEQ", "MSRC", "RANK", "GBN", "B_TDS", "UF_MSRC", "UF_FILTER", "UF_TIME", "UF_FLUX", "RO_MSRC", "RO_PRESSR", "RO_FLUX", "RO_CTL", "RO_TOTHR", "RO_TOTMIN", "ER_UF", "ER_RO", "ER_TOT", "USE_YN");

                DataRow[] dr;

                dr = dttemp.Select("RANK = '1'");
                if (dr.Length == 1)
                {
                    dr1 = dr[0];
                    B_TDS1.Content = dr1["B_TDS"].ToString();
                    if(dr1["USE_YN"].ToString().Equals("Y"))
                        radiogbn1.IsChecked = true;
                }
                else
                    return;

                dr = dttemp.Select("RANK = '2'");
                if (dr.Length == 1)
                {
                    dr2 = dr[0];
                    B_TDS2.Content = dr2["B_TDS"].ToString();
                    if (dr2["USE_YN"].ToString().Equals("Y"))
                        radiogbn2.IsChecked = true;
                }
                else
                    return;

                dr = dttemp.Select("RANK = '3'");
                if (dr.Length == 1)
                {
                    dr3 = dr[0];
                    B_TDS3.Content = dr3["B_TDS"].ToString();
                    if (dr3["USE_YN"].ToString().Equals("Y"))
                        radiogbn3.IsChecked = true;
                }
                else
                    return;

                #region 순위별
                DataTable dtresult1 = new DataTable();
                DataTable dtresult2 = new DataTable();
                DataTable dtresult3 = new DataTable();
                dtresult1.Columns.Add("col1");
                dtresult1.Columns.Add("col2");
                dtresult1.Columns.Add("ecol1");
                dtresult1.Columns.Add("ecol2");
                dtresult2 = dtresult1.Clone();
                dtresult3 = dtresult1.Clone();

                DataRow r1 = dtresult1.NewRow();
                DataRow r2 = dtresult1.NewRow();
                DataRow r3 = dtresult1.NewRow();
                DataRow r4 = dtresult1.NewRow();
                r1["col1"] = "Blending";
                r1["col2"] = "4순위";
                dr = dtresult.Select("RANK = '1' AND  DTL_RANK = '4' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r1["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r1["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult1.Rows.Add(r1);

                r2["col1"] = "Blending";
                r2["col2"] = "3순위";
                dr = dtresult.Select("RANK = '1' AND  DTL_RANK = '3' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r2["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r2["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult1.Rows.Add(r2);

                r3["col1"] = "Blending";
                r3["col2"] = "2순위";
                dr = dtresult.Select("RANK = '1' AND  DTL_RANK = '2' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r3["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r3["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult1.Rows.Add(r3);

                r4["col1"] = "Blending";
                r4["col2"] = "1순위";
                dr = dtresult.Select("RANK = '1' AND  DTL_RANK = '1' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r4["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r4["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult1.Rows.Add(r4);

                DataRow r5 = dtresult2.NewRow();
                DataRow r6 = dtresult2.NewRow();
                DataRow r7 = dtresult2.NewRow();
                DataRow r8 = dtresult2.NewRow();
                r5["col1"] = "Blending";
                r5["col2"] = "4순위";
                dr = dtresult.Select("RANK = '2' AND  DTL_RANK = '4' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r5["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r5["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult2.Rows.Add(r5);

                r6["col1"] = "Blending";
                r6["col2"] = "3순위";
                dr = dtresult.Select("RANK = '2' AND  DTL_RANK = '3' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r6["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r6["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult2.Rows.Add(r6);

                r7["col1"] = "Blending";
                r7["col2"] = "2순위";
                dr = dtresult.Select("RANK = '2' AND  DTL_RANK = '2' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r7["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r7["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult2.Rows.Add(r7);

                r8["col1"] = "Blending";
                r8["col2"] = "1순위";
                dr = dtresult.Select("RANK = '2' AND  DTL_RANK = '1' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r8["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r8["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult2.Rows.Add(r8);

                DataRow r9 = dtresult3.NewRow();
                DataRow r10 = dtresult3.NewRow();
                DataRow r11 = dtresult3.NewRow();
                DataRow r12 = dtresult3.NewRow();
                r9["col1"] = "Blending";
                r9["col2"] = "4순위";
                dr = dtresult.Select("RANK = '3' AND  DTL_RANK = '4' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r9["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r9["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult3.Rows.Add(r9);

                r10["col1"] = "Blending";
                r10["col2"] = "3순위";
                dr = dtresult.Select("RANK = '3' AND  DTL_RANK = '3' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r10["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r10["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult3.Rows.Add(r10);

                r11["col1"] = "Blending";
                r11["col2"] = "2순위";
                dr = dtresult.Select("RANK = '3' AND  DTL_RANK = '2' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r11["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r11["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult3.Rows.Add(r11);

                r12["col1"] = "Blending";
                r12["col2"] = "1순위";
                dr = dtresult.Select("RANK = '3' AND  DTL_RANK = '1' AND MSRC='" + strMSRC + "'", "B_TDS");
                if (dr.Length != 0)
                {
                    r12["ecol1"] = dr[0]["KIND_NM"].ToString();
                    r12["ecol2"] = dr[0]["RUN_RATE"].ToString();
                }
                dtresult3.Rows.Add(r12);

                grid1.ItemsSource = dtresult1;
                grid2.ItemsSource = dtresult2;
                grid3.ItemsSource = dtresult3;
                #endregion

                #region UF, RO, 에너지 소비량 분석

                for (int i = 1; i < 4; i++)
                {
                    DataTable dtresultDTL1 = new DataTable();
                    dtresultDTL1.Columns.Add("col1");
                    dtresultDTL1.Columns.Add("col2");
                    dtresultDTL1.Columns.Add("col3");

                    DataRow rDTL1 = dtresultDTL1.NewRow();
                    DataRow rDTL2 = dtresultDTL1.NewRow();
                    DataRow rDTL3 = dtresultDTL1.NewRow();
                    DataRow rDTL4 = dtresultDTL1.NewRow();
                    DataRow rDTL5 = dtresultDTL1.NewRow();
                    DataRow rDTL6 = dtresultDTL1.NewRow();
                    DataRow rDTL7 = dtresultDTL1.NewRow();
                    DataRow rDTL8 = dtresultDTL1.NewRow();
                    DataRow rDTL9 = dtresultDTL1.NewRow();
                    DataRow rDTL10 = dtresultDTL1.NewRow();
                    DataRow rDTL11 = dtresultDTL1.NewRow();
                    DataRow rDTL12 = dtresultDTL1.NewRow();
                    DataRow rDTL13 = dtresultDTL1.NewRow();

                    dr = dttemp.Select("RANK = '" + i.ToString() + "'");
                    if (dr.Length != 1) return;

                    rDTL1["col1"] = "UF운전조건";
                    rDTL2["col1"] = "UF운전조건";
                    rDTL3["col1"] = "UF운전조건";
                    rDTL4["col1"] = "UF운전조건";
                    rDTL5["col1"] = "UF운전조건";
                    rDTL6["col1"] = "RO운전조건";
                    rDTL7["col1"] = "RO운전조건";
                    rDTL8["col1"] = "RO운전조건";
                    rDTL9["col1"] = "RO운전조건";
                    rDTL10["col1"] = "RO운전조건";
                    rDTL11["col1"] = "RO운전조건";
                    rDTL12["col1"] = "RO운전조건";
                    rDTL13["col1"] = "총에너지소비량";

                    rDTL1["col2"] = "UF 운전 조건 (UF1/UF2)";
                    rDTL2["col2"] = "여과시간(min)";
                    rDTL3["col2"] = "역세시간(min)";
                    rDTL4["col2"] = "여과 Flux(lmh)";
                    rDTL5["col2"] = "UF 전력소비량(kwh/㎥)";
                    rDTL6["col2"] = "운전조건(SWRO/BWRO)";
                    rDTL7["col2"] = "운전압력(bar)";
                    rDTL8["col2"] = "예상 Flux(lmh)";
                    rDTL9["col2"] = "컨트롤벨브";
                    rDTL10["col2"] = "총운전시간 (hr)";
                    rDTL11["col2"] = "총운전시간 (min)";
                    rDTL12["col2"] = "RO 전력소비량(kwh/㎥)";
                    rDTL13["col2"] = "에너지소비량 (UF+RO)";

                    rDTL1["col3"] = dr[0]["UF_MSRC"].ToString();
                    rDTL2["col3"] = dr[0]["UF_FILTER"].ToString();
                    rDTL3["col3"] = dr[0]["UF_TIME"].ToString();
                    rDTL4["col3"] = dr[0]["UF_FLUX"].ToString();
                    rDTL5["col3"] = dr[0]["ER_UF"].ToString();
                    rDTL6["col3"] = dr[0]["RO_MSRC"].ToString();
                    rDTL7["col3"] = dr[0]["RO_PRESSR"].ToString();
                    rDTL8["col3"] = dr[0]["RO_FLUX"].ToString();
                    rDTL9["col3"] = dr[0]["RO_CTL"].ToString();
                    rDTL10["col3"] = dr[0]["RO_TOTHR"].ToString();
                    rDTL11["col3"] = dr[0]["RO_TOTMIN"].ToString();
                    rDTL12["col3"] = dr[0]["ER_RO"].ToString();
                    rDTL13["col3"] = dr[0]["ER_TOT"].ToString();

                    dtresultDTL1.Rows.Add(rDTL1);
                    dtresultDTL1.Rows.Add(rDTL2);
                    dtresultDTL1.Rows.Add(rDTL3);
                    dtresultDTL1.Rows.Add(rDTL4);
                    dtresultDTL1.Rows.Add(rDTL5);
                    dtresultDTL1.Rows.Add(rDTL6);
                    dtresultDTL1.Rows.Add(rDTL7);
                    dtresultDTL1.Rows.Add(rDTL8);
                    dtresultDTL1.Rows.Add(rDTL9);
                    dtresultDTL1.Rows.Add(rDTL10);
                    dtresultDTL1.Rows.Add(rDTL11);
                    dtresultDTL1.Rows.Add(rDTL12);
                    dtresultDTL1.Rows.Add(rDTL13);

                    GridControl grid = this.FindName("gridDTL" + i.ToString()) as GridControl;
                    grid.ItemsSource = dtresultDTL1.Copy();
                }
                #endregion

            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

    }
}
