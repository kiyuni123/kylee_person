﻿using CMFramework.Common.MessageBox;
using CMFramework.Common.Utils.ViewEffect;
using SWG.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWG.Form.Popup
{
    /// <summary>
    /// popupRunTimeSetting.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class popupRunTimeSetting : Window
    {
        OperationSettingWork work = new OperationSettingWork();

        public popupRunTimeSetting()
        {
            InitializeComponent();
            InitializeEvent();
            Loaded += PopupRunTimeSetting_Loaded;
        }

        private void InitializeEvent()
        {
            btnOk.Click += BtnOk_Click;
            btnInit.Click += BtnInit_Click;
        }

        private void BtnInit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                for (int i = 1; i < 25; i++)
                {
                    Border border = this.FindName("bdTime" + i) as Border;
                    border.Background = Brushes.White;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 1; i < 25; i++)
            {
                Border border = this.FindName("bdTime" + i) as Border;

                Hashtable conditions = new Hashtable();

                conditions.Add("TIME", i.ToString());
                if(border.Background.Equals(Brushes.White))
                    conditions.Add("RUN_YN", "0");
                else
                    conditions.Add("RUN_YN", "1");

                work.Insert_ANALS_OPERTIME(conditions);
            }
            work.Update_ANALS_BASE_SETTING_OPERTIME(null);

            this.Close();
            MainWindow.screenfade();
        }

        private void PopupRunTimeSetting_Loaded(object sender, RoutedEventArgs e)
        {
            DataTable temp = new DataTable();
            temp = work.Selects_ANALS_OPERTIME(null);

            foreach (DataRow r in temp.Rows)
            {
                Border border = this.FindName("bdTime" + r["TIME"].ToString()) as Border;

                if (r["RUN_YN"].Equals("1"))
                    border.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#98e0ee"));
                else
                    border.Background = Brushes.White;
            }
            
            MainWindow.screenfade();
            ThemeApply.Themeapply(this);
        }

        private void Border_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(((Border)sender).Background != Brushes.White)
            {
                ((Border)sender).Background = Brushes.White;
            }
            else
            {
                ((Border)sender).Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#98e0ee"));
            }
        }

        private void Border_MouseEnter(object sender, MouseEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Pressed)
            {
                if (((Border)sender).Background != Brushes.White)
                {
                    ((Border)sender).Background = Brushes.White;
                }
                else
                {
                    ((Border)sender).Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#98e0ee"));
                }
            }
        }
    }
}
