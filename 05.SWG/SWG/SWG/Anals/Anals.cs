﻿using CMFramework.Common.MessageBox;
using SWG.Work;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWG.Anals
{
    public class Anals
    {
        AnalsWork work = new AnalsWork();

        //분석 로직 함수
        public Hashtable Run(DateTime dtAnalStart, DateTime dtAnalEnd, int intrunDay, int intdayFlw, Hashtable inputconditions)
        {
            //결과 DataSet
            Hashtable htresult = new Hashtable();
            //ANALS_BASE_SETTING 데이터
            DataTable dtBaseSetting = new DataTable();

            //inputconditions TDS, HOLD
            DataTable dtTDSASC = new DataTable();

            #region ANALS_BASE_SETTING 데이터 확인
            dtBaseSetting = work.Select_ANALS_BASE_SETTING_ANAL(null);

            if (dtBaseSetting.Rows.Count != 1)
            {
                Messages.ShowInfoMsgBox("분석에 필요한 기본정보가 없습니다.");
                return null;
            }
            #endregion

            #region inputconditions 보유량
            double dSEAWTR_HOLD = Convert.ToDouble(inputconditions["SEAWTR_HOLD"].ToString());
            double dUGRWTR_HOLD = Convert.ToDouble(inputconditions["UGRWTR_HOLD"].ToString());
            double dBRKWTR_HOLD = Convert.ToDouble(inputconditions["BRKWTR_HOLD"].ToString());
            double dGRDWTR_HOLD = Convert.ToDouble(inputconditions["GRDWTR_HOLD"].ToString());
            #endregion

            #region inputconditions TDS
            dtTDSASC.Columns.Add("GBN");
            dtTDSASC.Columns.Add("TDS", typeof(double));
            dtTDSASC.Columns.Add("HOLD", typeof(double));
            dtTDSASC.Columns.Add("MUMMVAL", typeof(double));
            dtTDSASC.Columns.Add("PRSRVAL", typeof(double));
            dtTDSASC.Columns.Add("SWRONEEDINFLOW", typeof(double));
            dtTDSASC.Columns.Add("BWRONEEDINFLOW", typeof(double));

            DataRow dtTDSASCr1 = dtTDSASC.NewRow();
            dtTDSASCr1["GBN"] = "SEAWTR";
            dtTDSASCr1["TDS"] = Convert.ToDouble(inputconditions["SEAWTR_TDS"].ToString());
            dtTDSASCr1["HOLD"] = Convert.ToDouble(inputconditions["SEAWTR_HOLD"].ToString());
            
            dtTDSASC.Rows.Add(dtTDSASCr1);

            DataRow dtTDSASCr2 = dtTDSASC.NewRow();
            dtTDSASCr2["GBN"] = "UGRWTR";
            dtTDSASCr2["TDS"] = Convert.ToDouble(inputconditions["UGRWTR_TDS"].ToString());
            dtTDSASCr2["HOLD"] = Convert.ToDouble(inputconditions["UGRWTR_HOLD"].ToString());
            dtTDSASC.Rows.Add(dtTDSASCr2);

            DataRow dtTDSASCr3 = dtTDSASC.NewRow();
            dtTDSASCr3["GBN"] = "BRKWTR";
            dtTDSASCr3["TDS"] = Convert.ToDouble(inputconditions["BRKWTR_TDS"].ToString());
            dtTDSASCr3["HOLD"] = Convert.ToDouble(inputconditions["BRKWTR_HOLD"].ToString());
            dtTDSASC.Rows.Add(dtTDSASCr3);

            DataRow dtTDSASCr4 = dtTDSASC.NewRow();
            dtTDSASCr4["GBN"] = "GRDWTR";
            dtTDSASCr4["TDS"] = Convert.ToDouble(inputconditions["GRDWTR_TDS"].ToString());
            dtTDSASCr4["HOLD"] = Convert.ToDouble(inputconditions["GRDWTR_HOLD"].ToString());
            dtTDSASC.Rows.Add(dtTDSASCr4); 
            #endregion

            try
            {
                #region #######SWRO, BWRO 필요 유입유량####### out : dinFlwSWRO, dinFlwBWRO
                //여유율
                double dmrgn = 100 + Convert.ToDouble(dtBaseSetting.Rows[0]["PRDCTN_MRGN"].ToString());
                //총 생산유량
                int inttotflw = intdayFlw * intrunDay;
                //UF회수율
                double dUF_RTRVL = Convert.ToDouble(dtBaseSetting.Rows[0]["UF_RTRVL"].ToString());
                //SWRO회수율
                double dSWRO_RTRVL = Convert.ToDouble(dtBaseSetting.Rows[0]["SWRO_RTRVL"].ToString());
                //BWRO회수율
                double dBWRO_RTRVL = Convert.ToDouble(dtBaseSetting.Rows[0]["BWRO_RTRVL"].ToString());

                //SWRO 유입유량 산출 = 필요 유입유량
                double dinFlwSWRO = ((dmrgn / 100) * 100) * (inttotflw / dUF_RTRVL * 100) / dSWRO_RTRVL;
                //BWRO 유입유량 산출 = 필요 유입유량
                double dinFlwBWRO = ((dmrgn / 100) * 100) * (inttotflw / dUF_RTRVL * 100) / dBWRO_RTRVL;
                #endregion

                #region #######SWRO, BWRO 사용량산출####### out : dMUMMFlwSWRO_SEAWTR, dMUMMFlwBWRO_SEAWTR, dMUMMFlwSWRO_UGRWTR, dMUMMFlwBWRO_UGRWTR, dMUMMFlwSWRO_BRKWTR, dMUMMFlwBWRO_BRKWTR, dMUMMFlwSWRO_GRDWTR, dMUMMFlwBWRO_GRDWTR
                DataRow[] rselect;

                //해수 최소사용율
                double dSEAWTR_MUMMVAL = Convert.ToDouble(dtBaseSetting.Rows[0]["SEAWTR_MUMMVAL"].ToString());
                rselect = dtTDSASC.Select("GBN = 'SEAWTR'");
                if (rselect.Length == 1)
                {
                    rselect[0]["MUMMVAL"] = dSEAWTR_MUMMVAL;
                }
                //지하수 최소사용율
                double dUGRWTR_MUMMVAL = Convert.ToDouble(dtBaseSetting.Rows[0]["UGRWTR_MUMMVAL"].ToString());
                rselect = dtTDSASC.Select("GBN = 'UGRWTR'");
                if (rselect.Length == 1)
                {
                    rselect[0]["MUMMVAL"] = dUGRWTR_MUMMVAL;
                }
                //지표수 최소사용율
                double dBRKWTR_MUMMVAL = Convert.ToDouble(dtBaseSetting.Rows[0]["BRKWTR_MUMMVAL"].ToString());
                rselect = dtTDSASC.Select("GBN = 'BRKWTR'");
                if (rselect.Length == 1)
                {
                    rselect[0]["MUMMVAL"] = dBRKWTR_MUMMVAL;
                }
                //기수 최소사용율
                double dGRDWTR_MUMMVAL = Convert.ToDouble(dtBaseSetting.Rows[0]["GRDWTR_MUMMVAL"].ToString());
                rselect = dtTDSASC.Select("GBN = 'GRDWTR'");
                if (rselect.Length == 1)
                {
                    rselect[0]["MUMMVAL"] = dGRDWTR_MUMMVAL;
                }

                //SWRO_SEAWTR 사용량산출
                double dMUMMFlwSWRO_SEAWTR = dinFlwSWRO * dSEAWTR_MUMMVAL / 100;
                //BWRO_SEAWTR 사용량산출
                double dMUMMFlwBWRO_SEAWTR = dinFlwBWRO * dSEAWTR_MUMMVAL / 100;
                //SWRO_UGRWTR 사용량산출
                double dMUMMFlwSWRO_UGRWTR = dinFlwSWRO * dUGRWTR_MUMMVAL / 100;
                //BWRO_UGRWTR 사용량산출
                double dMUMMFlwBWRO_UGRWTR = dinFlwBWRO * dUGRWTR_MUMMVAL / 100;
                //SWRO_BRKWTR 사용량산출
                double dMUMMFlwSWRO_BRKWTR = dinFlwSWRO * dBRKWTR_MUMMVAL / 100;
                //BWRO_BRKWTR 사용량산출
                double dMUMMFlwBWRO_BRKWTR = dinFlwBWRO * dBRKWTR_MUMMVAL / 100;
                //SWRO_GRDWTR 사용량산출
                double dMUMMFlwSWRO_GRDWTR = dinFlwSWRO * dGRDWTR_MUMMVAL / 100;
                //BWRO_GRDWTR 사용량산출
                double dMUMMFlwBWRO_GRDWTR = dinFlwBWRO * dGRDWTR_MUMMVAL / 100;
                #endregion

                #region #######최소사용량 사용가능여부#######
                double dSEAWTR_PRSRVAL = dSEAWTR_HOLD - (dSEAWTR_HOLD * Convert.ToDouble(dtBaseSetting.Rows[0]["SEAWTR_PRSRV"].ToString()) / 100);
                double dUGRWTR_PRSRVAL = dUGRWTR_HOLD - (dUGRWTR_HOLD * Convert.ToDouble(dtBaseSetting.Rows[0]["UGRWTR_PRSRV"].ToString()) / 100);
                double dBRKWTR_PRSRVAL = dBRKWTR_HOLD - (dBRKWTR_HOLD * Convert.ToDouble(dtBaseSetting.Rows[0]["BRKWTR_PRSRV"].ToString()) / 100);
                double dGRDWTR_PRSRVAL = dGRDWTR_HOLD - (dGRDWTR_HOLD * Convert.ToDouble(dtBaseSetting.Rows[0]["GRDWTR_PRSRV"].ToString()) / 100);

                rselect = dtTDSASC.Select("GBN = 'SEAWTR'");
                if (rselect.Length == 1)
                {
                    rselect[0]["SWRONEEDINFLOW"] = 100;
                    rselect[0]["BWRONEEDINFLOW"] = 100;
                    rselect[0]["PRSRVAL"] = dSEAWTR_PRSRVAL;
                }

                rselect = dtTDSASC.Select("GBN = 'UGRWTR'");
                if (rselect.Length == 1)
                {
                    rselect[0]["SWRONEEDINFLOW"] = dUGRWTR_PRSRVAL / dinFlwSWRO * 100;
                    rselect[0]["BWRONEEDINFLOW"] = dUGRWTR_PRSRVAL / dinFlwBWRO * 100;
                    rselect[0]["PRSRVAL"] = dUGRWTR_PRSRVAL;
                }

                rselect = dtTDSASC.Select("GBN = 'BRKWTR'");
                if (rselect.Length == 1)
                {
                    rselect[0]["SWRONEEDINFLOW"] = dBRKWTR_PRSRVAL / dinFlwSWRO * 100;
                    rselect[0]["BWRONEEDINFLOW"] = dBRKWTR_PRSRVAL / dinFlwBWRO * 100;
                    rselect[0]["PRSRVAL"] = dBRKWTR_PRSRVAL;
                }

                rselect = dtTDSASC.Select("GBN = 'GRDWTR'");
                if (rselect.Length == 1)
                {
                    rselect[0]["SWRONEEDINFLOW"] = dGRDWTR_PRSRVAL / dinFlwSWRO * 100;
                    rselect[0]["BWRONEEDINFLOW"] = dGRDWTR_PRSRVAL / dinFlwBWRO * 100;
                    rselect[0]["PRSRVAL"] = dGRDWTR_PRSRVAL;
                }
                #endregion

                #region TDS낮은 순서대로 원수 적용
                DataTable temp = new DataTable();
                temp = dtTDSASC.Clone();
                foreach (DataRow r in dtTDSASC.Select("1=1", "TDS"))
                {
                    temp.ImportRow(r);
                }
                dtTDSASC = temp.Copy();
                #endregion

                #region 사용가능 판단 ()
                //SWRO(이중수원)
                //해수는 사용필수 조건
                bool boolSWRODoubleUGRWTR = false;
                bool boolSWRODoubleBRKWTR = false;
                bool boolSWRODoubleGRDWTR = false;

                //1번째 지하수
                if (dUGRWTR_PRSRVAL > dMUMMFlwSWRO_UGRWTR)
                    boolSWRODoubleUGRWTR = true;
                //2번째 지표수
                if (dBRKWTR_PRSRVAL > dMUMMFlwSWRO_BRKWTR)
                    boolSWRODoubleBRKWTR = true;
                //3번째 기수
                if (dGRDWTR_PRSRVAL > dMUMMFlwSWRO_GRDWTR)
                    boolSWRODoubleGRDWTR = true;

                //BWRO(이중수원)
                //해수는 사용필수 조건
                bool boolBWRODoubleUGRWTR = false;
                bool boolBWRODoubleBRKWTR = false;
                bool boolBWRODoubleGRDWTR = false;

                //1번째 지하수
                if (dUGRWTR_PRSRVAL > dMUMMFlwBWRO_UGRWTR)
                    boolBWRODoubleUGRWTR = true;
                //2번째 지표수
                if (dBRKWTR_PRSRVAL > dMUMMFlwBWRO_BRKWTR)
                    boolBWRODoubleBRKWTR = true;
                //3번째 기수
                if (dGRDWTR_PRSRVAL > dMUMMFlwBWRO_GRDWTR)
                    boolBWRODoubleGRDWTR = true;

                //SWRO(삼중수원)
                //해수는 사용필수 조건
                bool boolSWROTripletUGRWTR = false;
                bool boolSWROTripletBRKWTR = false;
                bool boolSWROTripletGRDWTR = false;

                //1번째 지하수
                if (dUGRWTR_PRSRVAL > dMUMMFlwSWRO_UGRWTR)
                    boolSWROTripletUGRWTR = true;
                //2번째 지표수
                if (dBRKWTR_PRSRVAL > dMUMMFlwSWRO_BRKWTR)
                    boolSWROTripletBRKWTR = true;
                //3번째 기수
                if (dGRDWTR_PRSRVAL > dMUMMFlwSWRO_GRDWTR)
                    boolSWROTripletGRDWTR = true;

                //BWRO(삼중수원)
                //해수는 사용필수 조건
                bool boolBWROTripletUGRWTR = false;
                bool boolBWROTripletBRKWTR = false;
                bool boolBWROTripletGRDWTR = false;

                //1번째 지하수
                if (dUGRWTR_PRSRVAL > dMUMMFlwBWRO_UGRWTR)
                    boolBWROTripletUGRWTR = true;
                //2번째 지표수
                if (dBRKWTR_PRSRVAL > dMUMMFlwBWRO_BRKWTR)
                    boolBWROTripletBRKWTR = true;
                //3번째 기수
                if (dGRDWTR_PRSRVAL > dMUMMFlwBWRO_GRDWTR)
                    boolBWROTripletGRDWTR = true;
                #endregion

                #region SWRO, BWRO 이중 삼중 사중 분석
                DataSet dsDoubletemp = new DataSet();
                DataSet dsTriplettemp = new DataSet();
                DataSet dsQuadrupletemp = new DataSet();

                DataTable dtRank = new DataTable();
                rselect = dtTDSASC.Select("GBN = 'SEAWTR'");
                int intrank = 0;

                //TDS낮은 순서대로 원수 적용
                foreach (DataRow r in dtTDSASC.Rows)
                {
                    if (!r["GBN"].Equals("SEAWTR"))
                    {
                        intrank++;

                        #region 이중수원 SWRO 분석
                        dtRank = null;
                        dtRank = new DataTable();
                        dtRank.TableName = "SWRO_Double_Rank_" + intrank;

                        dtRank.Columns.Add("GBN");
                        dtRank.Columns.Add("DTL_RANK");
                        dtRank.Columns.Add("TDS", typeof(double));
                        dtRank.Columns.Add("RUN_RATE", typeof(double));
                        dtRank.Columns.Add("USEFLW", typeof(double));

                        //1순위
                        DataRow r1 = dtRank.NewRow();
                        r1["GBN"] = r["GBN"];
                        r1["DTL_RANK"] = dtTDSASC.Rows.IndexOf(r)+1;

                        if (r["GBN"].ToString().Equals("SEAWTR"))
                            r1["RUN_RATE"] = 0;
                        if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) > 100)
                            r1["RUN_RATE"] = 0;
                        if (Convert.ToDouble(r["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL)
                            r1["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL;
                        if (Convert.ToDouble(r["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL)
                            r1["RUN_RATE"] = Convert.ToDouble(r["SWRONEEDINFLOW"]);

                        if (Convert.ToDouble(r1["RUN_RATE"]) == 0)
                            r1["TDS"] = 0;
                        else
                            r1["TDS"] = r["TDS"];

                        if (Convert.ToDouble(r1["RUN_RATE"]) == 0)
                            r1["USEFLW"] = 0;
                        else
                            r1["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r1["RUN_RATE"]) / 100;

                        dtRank.Rows.Add(r1);

                        //해수
                        DataRow r2 = dtRank.NewRow();
                        r2["GBN"] = rselect[0]["GBN"];
                        r2["DTL_RANK"] = 4;
                        r2["TDS"] = rselect[0]["TDS"];
                        r2["RUN_RATE"] = 100 - Convert.ToDouble(r1["RUN_RATE"]);
                        r2["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r2["RUN_RATE"]) / 100;
                        dtRank.Rows.Add(r2);

                        dsDoubletemp.Tables.Add(dtRank.Copy());
                        #endregion

                        #region 이중수원 BWRO 분석
                        dtRank = null;
                        dtRank = new DataTable();
                        dtRank.TableName = "BWRO_Double_Rank_" + intrank;

                        dtRank.Columns.Add("GBN");
                        dtRank.Columns.Add("DTL_RANK");
                        dtRank.Columns.Add("TDS", typeof(double));
                        dtRank.Columns.Add("RUN_RATE", typeof(double));
                        dtRank.Columns.Add("USEFLW", typeof(double));

                        //1순위
                        DataRow r3 = dtRank.NewRow();
                        r3["GBN"] = r["GBN"];
                        r3["DTL_RANK"] = dtTDSASC.Rows.IndexOf(r) + 1;
                        if (r["GBN"].ToString().Equals("SEAWTR"))
                            r3["RUN_RATE"] = 0;
                        if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) > 100)
                            r3["RUN_RATE"] = 0;
                        if (Convert.ToDouble(r["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL)
                            r3["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL;
                        if (Convert.ToDouble(r["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL)
                            r3["RUN_RATE"] = Convert.ToDouble(r["BWRONEEDINFLOW"]);

                        if (Convert.ToDouble(r3["RUN_RATE"]) == 0)
                            r3["TDS"] = 0;
                        else
                            r3["TDS"] = r["TDS"];

                        if (Convert.ToDouble(r3["RUN_RATE"]) == 0)
                            r3["USEFLW"] = 0;
                        else
                            r3["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r3["RUN_RATE"]) / 100;

                        dtRank.Rows.Add(r3);

                        //해수
                        DataRow r4 = dtRank.NewRow();
                        r4["GBN"] = rselect[0]["GBN"];
                        r4["DTL_RANK"] = 4;
                        r4["TDS"] = rselect[0]["TDS"];
                        r4["RUN_RATE"] = 100 - Convert.ToDouble(r3["RUN_RATE"]);
                        r4["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r4["RUN_RATE"]) / 100;
                        dtRank.Rows.Add(r4);

                        dsDoubletemp.Tables.Add(dtRank.Copy());
                        #endregion

                        #region 삼중수원 SWRO 분석
                        dtRank = null;
                        dtRank = new DataTable();
                        dtRank.TableName = "SWRO_Triplet_Rank_" + intrank;

                        dtRank.Columns.Add("GBN");
                        dtRank.Columns.Add("DTL_RANK");
                        dtRank.Columns.Add("TDS", typeof(double));
                        dtRank.Columns.Add("RUN_RATE", typeof(double));
                        dtRank.Columns.Add("USEFLW", typeof(double));

                        if (intrank == 1)
                        {
                            #region //1순위
                            DataRow r5 = dtRank.NewRow();
                            r5["GBN"] = r["GBN"];
                            r5["DTL_RANK"] = 1;
                            if (r["GBN"].ToString().Equals("SEAWTR"))
                                r5["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) > 100)
                                r5["RUN_RATE"] = 0;
                            if (Convert.ToDouble(r["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r5["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]);
                            if (Convert.ToDouble(r["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r5["RUN_RATE"] = Convert.ToDouble(r["SWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["TDS"] = 0;
                            else
                                r5["TDS"] = r["TDS"];

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["USEFLW"] = 0;
                            else
                                r5["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r5["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r5);
                            #endregion

                            #region //2순위
                            DataRow r6 = dtRank.NewRow();
                            r6["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"];
                            r6["DTL_RANK"] = 2;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"].ToString().Equals("SEAWTR"))
                                r6["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) > 100)
                                r6["RUN_RATE"] = 0;
                            if (Convert.ToDouble(r["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]);
                            if (Convert.ToDouble(r["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = Convert.ToDouble(r["SWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["TDS"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["PRSRVAL"]) == 0)
                                r6["TDS"] = 0;
                            else
                                r6["TDS"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["TDS"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["USEFLW"] = 0;
                            else
                                r6["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r6["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r6);
                            #endregion

                            #region //해수
                            DataRow r7 = dtRank.NewRow();
                            r7["GBN"] = rselect[0]["GBN"];
                            r7["DTL_RANK"] = 4;
                            r7["TDS"] = rselect[0]["TDS"];
                            r7["RUN_RATE"] = 100 - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]);
                            r7["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r7["RUN_RATE"]) / 100;
                            dtRank.Rows.Add(r7);
                            #endregion
                        }
                        else if (intrank == 2)
                        {
                            #region //1순위
                            DataRow r5 = dtRank.NewRow();
                            r5["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["GBN"];
                            r5["DTL_RANK"] = 1;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["GBN"].ToString().Equals("SEAWTR"))
                                r5["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) > 100)
                                r5["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r5["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r5["RUN_RATE"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["SWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["TDS"] = 0;
                            else
                                r5["TDS"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["TDS"];

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["USEFLW"] = 0;
                            else
                                r5["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r5["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r5);
                            #endregion                            

                            #region //3순위
                            DataRow r6 = dtRank.NewRow();
                            r6["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"];
                            r6["DTL_RANK"] = 3;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"].ToString().Equals("SEAWTR"))
                                r6["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) > 100)
                                r6["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["SWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["TDS"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["PRSRVAL"]) == 0)
                                r6["TDS"] = 0;
                            else
                                r6["TDS"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["TDS"];

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["USEFLW"] = 0;
                            else
                                r6["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r6["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r6);
                            #endregion

                            #region //해수
                            DataRow r7 = dtRank.NewRow();
                            r7["GBN"] = rselect[0]["GBN"];
                            r7["DTL_RANK"] = 4;
                            r7["TDS"] = rselect[0]["TDS"];
                            r7["RUN_RATE"] = 100 - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]);
                            r7["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r7["RUN_RATE"]) / 100;
                            dtRank.Rows.Add(r7);
                            #endregion
                        }
                        else if (intrank == 3)
                        {
                            #region //2순위
                            DataRow r5 = dtRank.NewRow();
                            r5["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["GBN"];
                            r5["DTL_RANK"] = 2;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["GBN"].ToString().Equals("SEAWTR"))
                                r5["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["MUMMVAL"]) > 100)
                                r5["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r["MUMMVAL"]))
                                r5["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r["MUMMVAL"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r["MUMMVAL"]))
                                r5["RUN_RATE"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["SWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["TDS"] = 0;
                            else
                                r5["TDS"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["TDS"];

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["USEFLW"] = 0;
                            else
                                r5["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r5["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r5);
                            #endregion

                            #region //3순위
                            DataRow r6 = dtRank.NewRow();
                            r6["GBN"] = r["GBN"];
                            r6["DTL_RANK"] = 3;

                            if (r["GBN"].ToString().Equals("SEAWTR"))
                                r6["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["MUMMVAL"]) + Convert.ToDouble(r["MUMMVAL"]) > 100)
                                r6["RUN_RATE"] = 0;
                            if (Convert.ToDouble(r["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]);
                            if (Convert.ToDouble(r["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = Convert.ToDouble(r["SWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["TDS"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["PRSRVAL"]) == 0)
                                r6["TDS"] = 0;
                            else
                                r6["TDS"] = r["TDS"];

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["USEFLW"] = 0;
                            else
                                r6["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r6["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r6);
                            #endregion

                            #region //해수
                            DataRow r7 = dtRank.NewRow();
                            r7["GBN"] = rselect[0]["GBN"];
                            r7["DTL_RANK"] = 4;
                            r7["TDS"] = rselect[0]["TDS"];
                            r7["RUN_RATE"] = 100 - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]);
                            r7["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r7["RUN_RATE"]) / 100;
                            dtRank.Rows.Add(r7);
                            #endregion
                        }

                        dsTriplettemp.Tables.Add(dtRank.Copy());
                        #endregion

                        #region 삼중수원 BWRO 분석
                        dtRank = null;
                        dtRank = new DataTable();
                        dtRank.TableName = "BWRO_Triplet_Rank_" + intrank;

                        dtRank.Columns.Add("GBN");
                        dtRank.Columns.Add("DTL_RANK");
                        dtRank.Columns.Add("TDS", typeof(double));
                        dtRank.Columns.Add("RUN_RATE", typeof(double));
                        dtRank.Columns.Add("USEFLW", typeof(double));

                        if (intrank == 1)
                        {
                            #region //1순위
                            DataRow r5 = dtRank.NewRow();
                            r5["GBN"] = r["GBN"];
                            r5["DTL_RANK"] = 1;

                            if (r["GBN"].ToString().Equals("SEAWTR"))
                                r5["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) > 100)
                                r5["RUN_RATE"] = 0;
                            if (Convert.ToDouble(r["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r5["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]);
                            if (Convert.ToDouble(r["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r5["RUN_RATE"] = Convert.ToDouble(r["BWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["TDS"] = 0;
                            else
                                r5["TDS"] = r["TDS"];

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["USEFLW"] = 0;
                            else
                                r5["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r5["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r5);
                            #endregion

                            #region //2순위
                            DataRow r6 = dtRank.NewRow();
                            r6["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"];
                            r6["DTL_RANK"] = 2;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"].ToString().Equals("SEAWTR"))
                                r6["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) > 100)
                                r6["RUN_RATE"] = 0;
                            if (Convert.ToDouble(r["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]);
                            if (Convert.ToDouble(r["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = Convert.ToDouble(r["BWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["TDS"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["PRSRVAL"]) == 0)
                                r6["TDS"] = 0;
                            else
                                r6["TDS"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["TDS"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["USEFLW"] = 0;
                            else
                                r6["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r6["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r6);
                            #endregion

                            #region //해수
                            DataRow r7 = dtRank.NewRow();
                            r7["GBN"] = rselect[0]["GBN"];
                            r7["DTL_RANK"] = 4;
                            r7["TDS"] = rselect[0]["TDS"];
                            r7["RUN_RATE"] = 100 - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]);
                            r7["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r7["RUN_RATE"]) / 100;
                            dtRank.Rows.Add(r7);
                            #endregion
                        }
                        else if (intrank == 2)
                        {
                            #region //1순위
                            DataRow r5 = dtRank.NewRow();
                            r5["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["GBN"];
                            r5["DTL_RANK"] = 1;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["GBN"].ToString().Equals("SEAWTR"))
                                r5["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) > 100)
                                r5["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r5["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r5["RUN_RATE"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["BWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["TDS"] = 0;
                            else
                                r5["TDS"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["TDS"];

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["USEFLW"] = 0;
                            else
                                r5["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r5["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r5);
                            #endregion                            

                            #region //3순위
                            DataRow r6 = dtRank.NewRow();
                            r6["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"];
                            r6["DTL_RANK"] = 3;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"].ToString().Equals("SEAWTR"))
                                r6["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) > 100)
                                r6["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["BWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["TDS"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["PRSRVAL"]) == 0)
                                r6["TDS"] = 0;
                            else
                                r6["TDS"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["TDS"];

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["USEFLW"] = 0;
                            else
                                r6["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r6["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r6);
                            #endregion

                            #region //해수
                            DataRow r7 = dtRank.NewRow();
                            r7["GBN"] = rselect[0]["GBN"];
                            r7["DTL_RANK"] = 4;
                            r7["TDS"] = rselect[0]["TDS"];
                            r7["RUN_RATE"] = 100 - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]);
                            r7["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r7["RUN_RATE"]) / 100;
                            dtRank.Rows.Add(r7);
                            #endregion
                        }
                        else if (intrank == 3)
                        {
                            #region //2순위
                            DataRow r5 = dtRank.NewRow();
                            r5["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["GBN"];
                            r5["DTL_RANK"] = 2;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["GBN"].ToString().Equals("SEAWTR"))
                                r5["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["MUMMVAL"]) > 100)
                                r5["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r["MUMMVAL"]))
                                r5["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r["MUMMVAL"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r["MUMMVAL"]))
                                r5["RUN_RATE"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["BWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["TDS"] = 0;
                            else
                                r5["TDS"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["TDS"];

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["USEFLW"] = 0;
                            else
                                r5["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r5["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r5);
                            #endregion

                            #region //3순위
                            DataRow r6 = dtRank.NewRow();
                            r6["GBN"] = r["GBN"];
                            r6["DTL_RANK"] = 3;

                            if (r["GBN"].ToString().Equals("SEAWTR"))
                                r6["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) - 1]["MUMMVAL"]) + Convert.ToDouble(r["MUMMVAL"]) > 100)
                                r6["RUN_RATE"] = 0;
                            if (Convert.ToDouble(r["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]);
                            if (Convert.ToDouble(r["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]))
                                r6["RUN_RATE"] = Convert.ToDouble(r["BWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["TDS"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["PRSRVAL"]) == 0)
                                r6["TDS"] = 0;
                            else
                                r6["TDS"] = r["TDS"];

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["USEFLW"] = 0;
                            else
                                r6["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r6["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r6);
                            #endregion

                            #region //해수
                            DataRow r7 = dtRank.NewRow();
                            r7["GBN"] = rselect[0]["GBN"];
                            r7["DTL_RANK"] = 4;
                            r7["TDS"] = rselect[0]["TDS"];
                            r7["RUN_RATE"] = 100 - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]);
                            r7["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r7["RUN_RATE"]) / 100;
                            dtRank.Rows.Add(r7);
                            #endregion
                        }

                        dsTriplettemp.Tables.Add(dtRank.Copy());
                        #endregion

                        #region 사중수원 SWRO 분석
                        dtRank = null;
                        dtRank = new DataTable();
                        dtRank.TableName = "SWRO_Quadruple_Rank_" + intrank;

                        dtRank.Columns.Add("GBN");
                        dtRank.Columns.Add("DTL_RANK");
                        dtRank.Columns.Add("TDS", typeof(double));
                        dtRank.Columns.Add("RUN_RATE", typeof(double));
                        dtRank.Columns.Add("USEFLW", typeof(double));

                        if (intrank == 1)
                        {
                            #region //1순위
                            DataRow r5 = dtRank.NewRow();
                            r5["GBN"] = r["GBN"];
                            r5["DTL_RANK"] = 1;

                            if (r["GBN"].ToString().Equals("SEAWTR"))
                                r5["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]) > 100)
                                r5["RUN_RATE"] = 0;
                            if (Convert.ToDouble(r["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]))
                                r5["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]);
                            if (Convert.ToDouble(r["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]))
                                r5["RUN_RATE"] = Convert.ToDouble(r["SWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r5["RUN_RATE"]) == Convert.ToDouble(rselect[0]["TDS"]))
                                r5["TDS"] = 0;
                            else
                                r5["TDS"] = r["TDS"];

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["USEFLW"] = 0;
                            else
                                r5["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r5["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r5);
                            #endregion

                            #region //2순위
                            DataRow r6 = dtRank.NewRow();
                            r6["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"];
                            r6["DTL_RANK"] = 2;
                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"].ToString().Equals("SEAWTR"))
                                r6["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]) > 100)
                                r6["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r6["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r6["RUN_RATE"] = Convert.ToDouble(r["SWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == Convert.ToDouble(rselect[0]["TDS"]))
                                r6["TDS"] = 0;
                            else
                                r6["TDS"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["TDS"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["USEFLW"] = 0;
                            else
                                r6["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r6["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r6);
                            #endregion

                            #region //3순위
                            DataRow r7 = dtRank.NewRow();
                            r7["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["GBN"];
                            r7["DTL_RANK"] = 3;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["GBN"].ToString().Equals("SEAWTR"))
                                r7["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]) > 100)
                                r7["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["SWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]))
                                r7["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["SWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]))
                                r7["RUN_RATE"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["SWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r7["RUN_RATE"]) == Convert.ToDouble(rselect[0]["TDS"]))
                                r7["TDS"] = 0;
                            else if (Convert.ToDouble(r7["RUN_RATE"]) == 0)
                                r7["TDS"] = 0;
                            else
                                r7["TDS"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["TDS"]);

                            if (Convert.ToDouble(r7["RUN_RATE"]) == 0)
                                r7["USEFLW"] = 0;
                            else
                                r7["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r7["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r7);
                            #endregion

                            #region //해수
                            DataRow r8 = dtRank.NewRow();
                            r8["GBN"] = rselect[0]["GBN"];
                            r8["DTL_RANK"] = 4;
                            r8["TDS"] = rselect[0]["TDS"];
                            r8["RUN_RATE"] = 100 - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]) - Convert.ToDouble(r7["RUN_RATE"]);
                            r8["USEFLW"] = dinFlwSWRO * Convert.ToDouble(r8["RUN_RATE"]) / 100;
                            dtRank.Rows.Add(r8);

                            dsQuadrupletemp.Tables.Add(dtRank.Copy());
                            #endregion
                        }
                        #endregion

                        #region 사중수원 BWRO 분석
                        dtRank = null;
                        dtRank = new DataTable();
                        dtRank.TableName = "BWRO_Quadruple_Rank_" + intrank;

                        dtRank.Columns.Add("GBN");
                        dtRank.Columns.Add("DTL_RANK");
                        dtRank.Columns.Add("TDS", typeof(double));
                        dtRank.Columns.Add("RUN_RATE", typeof(double));
                        dtRank.Columns.Add("USEFLW", typeof(double));

                        if (intrank == 1)
                        {
                            #region //1순위
                            DataRow r5 = dtRank.NewRow();
                            r5["GBN"] = r["GBN"];
                            r5["DTL_RANK"] = 1;

                            if (r["GBN"].ToString().Equals("SEAWTR"))
                                r5["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]) > 100)
                                r5["RUN_RATE"] = 0;
                            if (Convert.ToDouble(r["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]))
                                r5["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]);
                            if (Convert.ToDouble(r["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]))
                                r5["RUN_RATE"] = Convert.ToDouble(r["BWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r5["RUN_RATE"]) == Convert.ToDouble(rselect[0]["TDS"]))
                                r5["TDS"] = 0;
                            else
                                r5["TDS"] = r["TDS"];

                            if (Convert.ToDouble(r5["RUN_RATE"]) == 0)
                                r5["USEFLW"] = 0;
                            else
                                r5["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r5["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r5);
                            #endregion

                            #region //2순위
                            DataRow r6 = dtRank.NewRow();
                            r6["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"];
                            r6["DTL_RANK"] = 2;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["GBN"].ToString().Equals("SEAWTR"))
                                r6["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]) > 100)
                                r6["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r6["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]))
                                r6["RUN_RATE"] = Convert.ToDouble(r["BWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == Convert.ToDouble(rselect[0]["TDS"]))
                                r6["TDS"] = 0;
                            else
                                r6["TDS"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["TDS"]);

                            if (Convert.ToDouble(r6["RUN_RATE"]) == 0)
                                r6["USEFLW"] = 0;
                            else
                                r6["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r6["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r6);
                            #endregion

                            #region //3순위
                            DataRow r7 = dtRank.NewRow();
                            r7["GBN"] = dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["GBN"];
                            r7["DTL_RANK"] = 3;

                            if (dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["GBN"].ToString().Equals("SEAWTR"))
                                r7["RUN_RATE"] = 0;
                            if (dSEAWTR_MUMMVAL + Convert.ToDouble(r["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 1]["MUMMVAL"]) + Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["MUMMVAL"]) > 100)
                                r7["RUN_RATE"] = 0;
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["BWRONEEDINFLOW"]) >= 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]))
                                r7["RUN_RATE"] = 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]);
                            if (Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["BWRONEEDINFLOW"]) < 100 - dSEAWTR_MUMMVAL - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]))
                                r7["RUN_RATE"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["BWRONEEDINFLOW"]);

                            if (Convert.ToDouble(r7["RUN_RATE"]) == Convert.ToDouble(rselect[0]["TDS"]))
                                r7["TDS"] = 0;
                            else if (Convert.ToDouble(r7["RUN_RATE"]) == 0)
                                r7["TDS"] = 0;
                            else
                                r7["TDS"] = Convert.ToDouble(dtTDSASC.Rows[dtTDSASC.Rows.IndexOf(r) + 2]["TDS"]);

                            if (Convert.ToDouble(r7["RUN_RATE"]) == 0)
                                r7["USEFLW"] = 0;
                            else
                                r7["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r7["RUN_RATE"]) / 100;

                            dtRank.Rows.Add(r7);
                            #endregion

                            #region //해수
                            DataRow r8 = dtRank.NewRow();
                            r8["GBN"] = rselect[0]["GBN"];
                            r8["DTL_RANK"] = 4;
                            r8["TDS"] = rselect[0]["TDS"];
                            r8["RUN_RATE"] = 100 - Convert.ToDouble(r5["RUN_RATE"]) - Convert.ToDouble(r6["RUN_RATE"]) - Convert.ToDouble(r7["RUN_RATE"]);
                            r8["USEFLW"] = dinFlwBWRO * Convert.ToDouble(r8["RUN_RATE"]) / 100;
                            dtRank.Rows.Add(r8);

                            dsQuadrupletemp.Tables.Add(dtRank.Copy());
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion


                //정렬하기 위해서 List사용
                List<KeyValuePair<DataTable, double>> SWRODoublelist = new List<KeyValuePair<DataTable, double>>();
                List<KeyValuePair<DataTable, double>> SWROTripletlist = new List<KeyValuePair<DataTable, double>>();
                List<KeyValuePair<DataTable, double>> BWRODoublelist = new List<KeyValuePair<DataTable, double>>();
                List<KeyValuePair<DataTable, double>> BWROTripletlist = new List<KeyValuePair<DataTable, double>>();

                #region SWRO, BWRO 결과 Blending 계산 및 dsresult ADD
                //Blending 산출 및 결과DataSet에 ADD (Double) 이중
                foreach (DataTable dt in dsDoubletemp.Tables)
                {
                    string strGBN = dt.Rows[0]["GBN"].ToString();

                    //Blending 산출
                    DataRow rblending1 = dt.NewRow();
                    rblending1["GBN"] = "Blending";
                    rblending1["RUN_RATE"] = 0;
                    rblending1["USEFLW"] = 0;
                    foreach (DataRow r in dt.Rows)
                    {
                        rblending1["RUN_RATE"] = Convert.ToDouble(rblending1["RUN_RATE"]) + Convert.ToDouble(r["RUN_RATE"]);
                        rblending1["USEFLW"] = Convert.ToDouble(rblending1["USEFLW"]) + Convert.ToDouble(r["USEFLW"]);
                    }
                    rblending1["TDS"] = (Convert.ToDouble(dt.Rows[0]["TDS"]) * Convert.ToDouble(dt.Rows[0]["RUN_RATE"]) / 100) + (Convert.ToDouble(dt.Rows[1]["TDS"]) * Convert.ToDouble(dt.Rows[1]["RUN_RATE"]) / 100);

                    dt.Rows.Add(rblending1);

                    string[] strsplit = dt.TableName.Split('_');

                    if (strsplit[0].Equals("SWRO"))
                    {
                        SWRODoublelist.Add(new KeyValuePair<DataTable, double>(dt, Convert.ToDouble(rblending1["TDS"])));
                    }
                    else if (strsplit[0].Equals("BWRO"))
                    {
                        BWRODoublelist.Add(new KeyValuePair<DataTable, double>(dt, Convert.ToDouble(rblending1["TDS"])));
                    }
                }

                //Blending 산출 및 결과DataSet에 ADD (Triplet) 삼중
                foreach (DataTable dt in dsTriplettemp.Tables)
                {
                    string strGBN = dt.Rows[0]["GBN"].ToString();

                    //Blending 산출
                    DataRow rblending1 = dt.NewRow();
                    rblending1["GBN"] = "Blending";
                    rblending1["RUN_RATE"] = 0;
                    rblending1["USEFLW"] = 0;
                    foreach (DataRow r in dt.Rows)
                    {
                        rblending1["RUN_RATE"] = Convert.ToDouble(rblending1["RUN_RATE"]) + Convert.ToDouble(r["RUN_RATE"]);
                        rblending1["USEFLW"] = Convert.ToDouble(rblending1["USEFLW"]) + Convert.ToDouble(r["USEFLW"]);
                    }
                    rblending1["TDS"] = (Convert.ToDouble(dt.Rows[0]["TDS"]) * Convert.ToDouble(dt.Rows[0]["RUN_RATE"]) / 100) + (Convert.ToDouble(dt.Rows[1]["TDS"]) * Convert.ToDouble(dt.Rows[1]["RUN_RATE"]) / 100) + (Convert.ToDouble(dt.Rows[2]["TDS"]) * Convert.ToDouble(dt.Rows[2]["RUN_RATE"]) / 100);

                    dt.Rows.Add(rblending1);

                    string[] strsplit = dt.TableName.Split('_');

                    if (strsplit[0].Equals("SWRO"))
                    {
                        SWROTripletlist.Add(new KeyValuePair<DataTable, double>(dt, Convert.ToDouble(rblending1["TDS"])));
                    }
                    else if (strsplit[0].Equals("BWRO"))
                    {
                        BWROTripletlist.Add(new KeyValuePair<DataTable, double>(dt, Convert.ToDouble(rblending1["TDS"])));
                    }
                }

                //Blending 산출 및 결과DataSet에 ADD (Quadruple) 사중
                foreach (DataTable dt in dsQuadrupletemp.Tables)
                {
                    string strGBN = dt.Rows[0]["GBN"].ToString();

                    //Blending 산출
                    DataRow rblending1 = dt.NewRow();
                    rblending1["GBN"] = "Blending";
                    rblending1["RUN_RATE"] = 0;
                    rblending1["USEFLW"] = 0;
                    foreach (DataRow r in dt.Rows)
                    {
                        rblending1["RUN_RATE"] = Convert.ToDouble(rblending1["RUN_RATE"]) + Convert.ToDouble(r["RUN_RATE"]);
                        rblending1["USEFLW"] = Convert.ToDouble(rblending1["USEFLW"]) + Convert.ToDouble(r["USEFLW"]);
                    }
                    rblending1["TDS"] = (Convert.ToDouble(dt.Rows[0]["TDS"]) * Convert.ToDouble(dt.Rows[0]["RUN_RATE"]) / 100) + (Convert.ToDouble(dt.Rows[1]["TDS"]) * Convert.ToDouble(dt.Rows[1]["RUN_RATE"]) / 100) + (Convert.ToDouble(dt.Rows[2]["TDS"]) * Convert.ToDouble(dt.Rows[2]["RUN_RATE"]) / 100) + (Convert.ToDouble(dt.Rows[3]["TDS"]) * Convert.ToDouble(dt.Rows[3]["RUN_RATE"]) / 100);

                    dt.Rows.Add(rblending1);
                }
                #endregion

                htresult.Add("drDoubleresult", dsDoubletemp);
                htresult.Add("drTripletresult", dsTriplettemp);
                htresult.Add("drQuadrupleresult", dsQuadrupletemp);

                SWRODoublelist.Sort((x, y) => (y.Value.CompareTo(x.Value)));
                SWROTripletlist.Sort((x, y) => (y.Value.CompareTo(x.Value)));
                BWRODoublelist.Sort((x, y) => (y.Value.CompareTo(x.Value)));
                BWROTripletlist.Sort((x, y) => (y.Value.CompareTo(x.Value)));

                if (htresult.Count == 3)
                {
                    Hashtable conditionsSetting = new Hashtable();
                    conditionsSetting.Add("SEAWTR_TDS", inputconditions["SEAWTR_TDS"].ToString());
                    conditionsSetting.Add("UGRWTR_TDS", inputconditions["UGRWTR_TDS"].ToString());
                    conditionsSetting.Add("BRKWTR_TDS", inputconditions["BRKWTR_TDS"].ToString());
                    conditionsSetting.Add("GRDWTR_TDS", inputconditions["GRDWTR_TDS"].ToString());
                    conditionsSetting.Add("SEAWTR_HOLD", inputconditions["SEAWTR_HOLD"].ToString());
                    conditionsSetting.Add("UGRWTR_HOLD", inputconditions["UGRWTR_HOLD"].ToString());
                    conditionsSetting.Add("BRKWTR_HOLD", inputconditions["BRKWTR_HOLD"].ToString());
                    conditionsSetting.Add("GRDWTR_HOLD", inputconditions["GRDWTR_HOLD"].ToString());
                    conditionsSetting.Add("BEGIN_DT", dtAnalStart.ToString("yyyyMMddHHmmss"));
                    conditionsSetting.Add("END_DT", dtAnalEnd.ToString("yyyyMMddHHmmss"));
                    conditionsSetting.Add("TOT_FLW", inttotflw);
                    conditionsSetting.Add("DAY_FLW", intdayFlw);
                    conditionsSetting.Add("BASE_SEQ", dtBaseSetting.Rows[0]["BASE_SEQ"].ToString());
                    conditionsSetting.Add("SWNEED_FLW", dinFlwSWRO.ToString());
                    conditionsSetting.Add("BWNEED_FLW", dinFlwBWRO.ToString());
                    work.Insert_ANALS_SETTING(conditionsSetting);
                    
                    string strSET_SEQ = (work.Select_ANALS_SETTING_SEQ(null) as DataTable).Rows[0][0].ToString();

                    int intblendrank = 1;

                    #region SWRODoublelist INSERT
                    for (int i = SWRODoublelist.Count - 1; i >= 0; i--)
                    {
                        for (DateTime dt = dtAnalStart; dt < dtAnalEnd.AddDays(1); dt = dt.AddDays(1))
                        {
                            DataTable uftemp = new DataTable();
                            DataTable rotemp = new DataTable();
                            Hashtable conditionsBlending = new Hashtable();
                            string[] strsplit = SWRODoublelist[i].Key.TableName.Split('_');
                            conditionsBlending.Add("SET_SEQ", strSET_SEQ);
                            conditionsBlending.Add("MSRC", strsplit[0]);
                            conditionsBlending.Add("GBN", 2);
                            conditionsBlending.Add("RANK", intblendrank);

                            if(intblendrank==1)
                            {
                                if(Convert.ToDouble(SWRODoublelist[2].Value) < Convert.ToDouble(BWRODoublelist[2].Value))
                                    conditionsBlending.Add("USE_YN", "Y");
                                else
                                    conditionsBlending.Add("USE_YN", "N");
                            }
                            else
                                conditionsBlending.Add("USE_YN", "N");

                            conditionsBlending.Add("B_TDS", Math.Round(Convert.ToDouble(SWRODoublelist[i].Key.Rows[2]["TDS"]), 5, MidpointRounding.ToEven));
                            conditionsBlending.Add("B_RUN_RATE", SWRODoublelist[i].Key.Rows[2]["RUN_RATE"].ToString());
                            conditionsBlending.Add("B_USEFLW", SWRODoublelist[i].Key.Rows[2]["USEFLW"].ToString());
                            conditionsBlending.Add("DT", dt.ToString("yyyyMMddHHmmss"));

                            //UF 분석
                            uftemp = work.Select_ANALS_UF_ANAL(conditionsBlending);
                            conditionsBlending.Add("UF_MSRC", uftemp.Rows[0]["STDR"].ToString());
                            conditionsBlending.Add("UF_FILTER", uftemp.Rows[0]["FILTER"].ToString());
                            conditionsBlending.Add("UF_TIME", uftemp.Rows[0]["TIME"].ToString());
                            conditionsBlending.Add("UF_FLUX", uftemp.Rows[0]["FLUX"].ToString());
                            conditionsBlending.Add("ER_UF", uftemp.Rows[0]["PWRER_VAL"].ToString());

                            //RO 분석
                            conditionsBlending.Add("DAYFLW", intdayFlw.ToString());
                            rotemp = work.Select_ANALS_RO_ANAL(conditionsBlending);
                            conditionsBlending.Add("RO_MSRC", rotemp.Rows[0]["MSRC"].ToString());
                            conditionsBlending.Add("RO_PRESSR", rotemp.Rows[0]["TRND_CAL"].ToString());
                            conditionsBlending.Add("RO_FLUX", rotemp.Rows[0]["FLUX_BMTB"].ToString());
                            if(rotemp.Rows[0]["CTLVALV"].ToString().Equals("1"))
                                conditionsBlending.Add("RO_CTL", "Y");
                            else
                                conditionsBlending.Add("RO_CTL", "N");
                            conditionsBlending.Add("RO_TOTHR", rotemp.Rows[0]["RO_TOTHR"].ToString());
                            conditionsBlending.Add("RO_TOTMIN", rotemp.Rows[0]["RO_TOTMIN"].ToString());
                            conditionsBlending.Add("ER_RO", rotemp.Rows[0]["PWRER_VAL"].ToString());
                            conditionsBlending.Add("ER_TOT", Convert.ToDouble(rotemp.Rows[0]["PWRER_VAL"]) + Convert.ToDouble(uftemp.Rows[0]["PWRER_VAL"]));
                            conditionsBlending.Add("RUN_YN", "N");

                            //분석결과
                            work.Insert_ANALS_RESULT_BLENDING(conditionsBlending);

                            foreach(DataRow r in SWRODoublelist[i].Key.Rows)
                            {
                                if(!r["GBN"].Equals("Blending"))
                                {
                                    Hashtable conditionsDTL = new Hashtable();
                                    conditionsDTL.Add("KIND", r["GBN"].ToString());
                                    conditionsDTL.Add("DT", dt.ToString("yyyyMMddHHmmss"));
                                    conditionsDTL.Add("MSRC", strsplit[0]);
                                    conditionsDTL.Add("GBN", 2);
                                    conditionsDTL.Add("RANK", intblendrank);
                                    conditionsDTL.Add("TDS", Math.Round(Convert.ToDouble(r["TDS"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("RUN_RATE", Math.Round(Convert.ToDouble(r["RUN_RATE"]), 2, MidpointRounding.ToEven));
                                    conditionsDTL.Add("USEFLW", Math.Round(Convert.ToDouble(r["USEFLW"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("DTL_RANK", r["DTL_RANK"].ToString());
                                    work.Insert_ANALS_RESULT_DTL(conditionsDTL);
                                }
                            }
                        }

                        intblendrank++;
                    }
                    #endregion

                    #region BWRODoublelist INSERT
                    intblendrank = 1;
                    for (int i = BWRODoublelist.Count - 1; i >= 0; i--)
                    {
                        for (DateTime dt = dtAnalStart; dt < dtAnalEnd.AddDays(1); dt = dt.AddDays(1))
                        {
                            DataTable uftemp = new DataTable();
                            DataTable rotemp = new DataTable();
                            Hashtable conditionsBlending = new Hashtable();
                            string[] strsplit = BWRODoublelist[i].Key.TableName.Split('_');
                            conditionsBlending.Add("SET_SEQ", strSET_SEQ);
                            conditionsBlending.Add("MSRC", strsplit[0]);
                            conditionsBlending.Add("GBN", 2);
                            conditionsBlending.Add("RANK", intblendrank);

                            if (intblendrank == 1)
                            {
                                if (Convert.ToDouble(BWRODoublelist[2].Value) < Convert.ToDouble(SWRODoublelist[2].Value))
                                    conditionsBlending.Add("USE_YN", "Y");
                                else
                                    conditionsBlending.Add("USE_YN", "N");
                            }
                            else
                                conditionsBlending.Add("USE_YN", "N");

                            conditionsBlending.Add("B_TDS", Math.Round(Convert.ToDouble(BWRODoublelist[i].Key.Rows[2]["TDS"]), 6, MidpointRounding.ToEven));
                            conditionsBlending.Add("B_RUN_RATE", BWRODoublelist[i].Key.Rows[2]["RUN_RATE"].ToString());
                            conditionsBlending.Add("B_USEFLW", BWRODoublelist[i].Key.Rows[2]["USEFLW"].ToString());
                            conditionsBlending.Add("DT", dt.ToString("yyyyMMddHHmmss"));

                            //UF 분석
                            uftemp = work.Select_ANALS_UF_ANAL(conditionsBlending);
                            conditionsBlending.Add("UF_MSRC", uftemp.Rows[0]["STDR"].ToString());
                            conditionsBlending.Add("UF_FILTER", uftemp.Rows[0]["FILTER"].ToString());
                            conditionsBlending.Add("UF_TIME", uftemp.Rows[0]["TIME"].ToString());
                            conditionsBlending.Add("UF_FLUX", uftemp.Rows[0]["FLUX"].ToString());
                            conditionsBlending.Add("ER_UF", uftemp.Rows[0]["PWRER_VAL"].ToString());

                            //RO 분석
                            conditionsBlending.Add("DAYFLW", intdayFlw.ToString());
                            rotemp = work.Select_ANALS_RO_ANAL(conditionsBlending);
                            conditionsBlending.Add("RO_MSRC", rotemp.Rows[0]["MSRC"].ToString());
                            conditionsBlending.Add("RO_PRESSR", rotemp.Rows[0]["TRND_CAL"].ToString());
                            conditionsBlending.Add("RO_FLUX", rotemp.Rows[0]["FLUX_BMTB"].ToString());
                            if (rotemp.Rows[0]["CTLVALV"].ToString().Equals("1"))
                                conditionsBlending.Add("RO_CTL", "Y");
                            else
                                conditionsBlending.Add("RO_CTL", "N");
                            conditionsBlending.Add("RO_TOTHR", rotemp.Rows[0]["RO_TOTHR"].ToString());
                            conditionsBlending.Add("RO_TOTMIN", rotemp.Rows[0]["RO_TOTMIN"].ToString());
                            conditionsBlending.Add("ER_RO", rotemp.Rows[0]["PWRER_VAL"].ToString());
                            conditionsBlending.Add("ER_TOT", Convert.ToDouble(rotemp.Rows[0]["PWRER_VAL"]) + Convert.ToDouble(uftemp.Rows[0]["PWRER_VAL"]));
                            conditionsBlending.Add("RUN_YN", "N");

                            work.Insert_ANALS_RESULT_BLENDING(conditionsBlending);

                            foreach (DataRow r in BWRODoublelist[i].Key.Rows)
                            {
                                if (!r["GBN"].Equals("Blending"))
                                {
                                    Hashtable conditionsDTL = new Hashtable();
                                    conditionsDTL.Add("KIND", r["GBN"].ToString());
                                    conditionsDTL.Add("DT", dt.ToString("yyyyMMddHHmmss"));
                                    conditionsDTL.Add("MSRC", strsplit[0]);
                                    conditionsDTL.Add("GBN", 2);
                                    conditionsDTL.Add("RANK", intblendrank);
                                    conditionsDTL.Add("TDS", Math.Round(Convert.ToDouble(r["TDS"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("RUN_RATE", Math.Round(Convert.ToDouble(r["RUN_RATE"]), 2, MidpointRounding.ToEven));
                                    conditionsDTL.Add("USEFLW", Math.Round(Convert.ToDouble(r["USEFLW"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("DTL_RANK", r["DTL_RANK"].ToString());
                                    work.Insert_ANALS_RESULT_DTL(conditionsDTL);
                                }
                            }
                        }

                        intblendrank++;
                    }
                    #endregion

                    #region SWROTripletlist INSERT
                    intblendrank = 1;
                    for (int i = SWROTripletlist.Count - 1; i >= 0; i--)
                    {
                        for (DateTime dt = dtAnalStart; dt < dtAnalEnd.AddDays(1); dt = dt.AddDays(1))
                        {
                            DataTable uftemp = new DataTable();
                            DataTable rotemp = new DataTable();
                            Hashtable conditionsBlending = new Hashtable();
                            string[] strsplit = SWROTripletlist[i].Key.TableName.Split('_');
                            conditionsBlending.Add("SET_SEQ", strSET_SEQ);
                            conditionsBlending.Add("MSRC", strsplit[0]);
                            conditionsBlending.Add("GBN", 3);
                            conditionsBlending.Add("RANK", intblendrank);

                            if (intblendrank == 1)
                            {
                                if (Convert.ToDouble(SWROTripletlist[2].Value) < Convert.ToDouble(BWROTripletlist[2].Value))
                                    conditionsBlending.Add("USE_YN", "Y");
                                else
                                    conditionsBlending.Add("USE_YN", "N");
                            }
                            else
                                conditionsBlending.Add("USE_YN", "N");

                            conditionsBlending.Add("B_TDS", Math.Round(Convert.ToDouble(SWROTripletlist[i].Key.Rows[3]["TDS"]), 6, MidpointRounding.ToEven));
                            conditionsBlending.Add("B_RUN_RATE", SWROTripletlist[i].Key.Rows[3]["RUN_RATE"].ToString());
                            conditionsBlending.Add("B_USEFLW", SWROTripletlist[i].Key.Rows[3]["USEFLW"].ToString());
                            conditionsBlending.Add("DT", dt.ToString("yyyyMMddHHmmss"));

                            //UF 분석
                            uftemp = work.Select_ANALS_UF_ANAL(conditionsBlending);
                            conditionsBlending.Add("UF_MSRC", uftemp.Rows[0]["STDR"].ToString());
                            conditionsBlending.Add("UF_FILTER", uftemp.Rows[0]["FILTER"].ToString());
                            conditionsBlending.Add("UF_TIME", uftemp.Rows[0]["TIME"].ToString());
                            conditionsBlending.Add("UF_FLUX", uftemp.Rows[0]["FLUX"].ToString());
                            conditionsBlending.Add("ER_UF", uftemp.Rows[0]["PWRER_VAL"].ToString());

                            //RO 분석
                            conditionsBlending.Add("DAYFLW", intdayFlw.ToString());
                            rotemp = work.Select_ANALS_RO_ANAL(conditionsBlending);
                            conditionsBlending.Add("RO_MSRC", rotemp.Rows[0]["MSRC"].ToString());
                            conditionsBlending.Add("RO_PRESSR", rotemp.Rows[0]["TRND_CAL"].ToString());
                            conditionsBlending.Add("RO_FLUX", rotemp.Rows[0]["FLUX_BMTB"].ToString());
                            if (rotemp.Rows[0]["CTLVALV"].ToString().Equals("1"))
                                conditionsBlending.Add("RO_CTL", "Y");
                            else
                                conditionsBlending.Add("RO_CTL", "N");
                            conditionsBlending.Add("RO_TOTHR", rotemp.Rows[0]["RO_TOTHR"].ToString());
                            conditionsBlending.Add("RO_TOTMIN", rotemp.Rows[0]["RO_TOTMIN"].ToString());
                            conditionsBlending.Add("ER_RO", rotemp.Rows[0]["PWRER_VAL"].ToString());
                            conditionsBlending.Add("ER_TOT", Convert.ToDouble(rotemp.Rows[0]["PWRER_VAL"]) + Convert.ToDouble(uftemp.Rows[0]["PWRER_VAL"]));
                            conditionsBlending.Add("RUN_YN", "N");

                            work.Insert_ANALS_RESULT_BLENDING(conditionsBlending);

                            foreach (DataRow r in SWROTripletlist[i].Key.Rows)
                            {
                                if (!r["GBN"].Equals("Blending"))
                                {
                                    Hashtable conditionsDTL = new Hashtable();
                                    conditionsDTL.Add("KIND", r["GBN"].ToString());
                                    conditionsDTL.Add("DT", dt.ToString("yyyyMMddHHmmss"));
                                    conditionsDTL.Add("MSRC", strsplit[0]);
                                    conditionsDTL.Add("GBN", 3);
                                    conditionsDTL.Add("RANK", intblendrank);
                                    conditionsDTL.Add("TDS", Math.Round(Convert.ToDouble(r["TDS"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("RUN_RATE", Math.Round(Convert.ToDouble(r["RUN_RATE"]), 2, MidpointRounding.ToEven));
                                    conditionsDTL.Add("USEFLW", Math.Round(Convert.ToDouble(r["USEFLW"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("DTL_RANK", r["DTL_RANK"].ToString());
                                    work.Insert_ANALS_RESULT_DTL(conditionsDTL);
                                }
                            }
                        }

                        intblendrank++;
                    }
                    #endregion

                    #region BWROTripletlist INSERT
                    intblendrank = 1;
                    for (int i = BWROTripletlist.Count - 1; i >= 0; i--)
                    {
                        for (DateTime dt = dtAnalStart; dt < dtAnalEnd.AddDays(1); dt = dt.AddDays(1))
                        {
                            DataTable uftemp = new DataTable();
                            DataTable rotemp = new DataTable();
                            Hashtable conditionsBlending = new Hashtable();
                            string[] strsplit = BWROTripletlist[i].Key.TableName.Split('_');
                            conditionsBlending.Add("SET_SEQ", strSET_SEQ);
                            conditionsBlending.Add("MSRC", strsplit[0]);
                            conditionsBlending.Add("GBN", 3);
                            conditionsBlending.Add("RANK", intblendrank);

                            if (intblendrank == 1)
                            {
                                if (Convert.ToDouble(BWROTripletlist[2].Value) < Convert.ToDouble(SWROTripletlist[2].Value))
                                    conditionsBlending.Add("USE_YN", "Y");
                                else
                                    conditionsBlending.Add("USE_YN", "N");
                            }
                            else
                                conditionsBlending.Add("USE_YN", "N");

                            conditionsBlending.Add("B_TDS", Math.Round(Convert.ToDouble(BWROTripletlist[i].Key.Rows[3]["TDS"]), 6, MidpointRounding.ToEven));
                            conditionsBlending.Add("B_RUN_RATE", BWROTripletlist[i].Key.Rows[3]["RUN_RATE"].ToString());
                            conditionsBlending.Add("B_USEFLW", BWROTripletlist[i].Key.Rows[3]["USEFLW"].ToString());
                            conditionsBlending.Add("DT", dt.ToString("yyyyMMddHHmmss"));

                            //UF 분석
                            uftemp = work.Select_ANALS_UF_ANAL(conditionsBlending);
                            conditionsBlending.Add("UF_MSRC", uftemp.Rows[0]["STDR"].ToString());
                            conditionsBlending.Add("UF_FILTER", uftemp.Rows[0]["FILTER"].ToString());
                            conditionsBlending.Add("UF_TIME", uftemp.Rows[0]["TIME"].ToString());
                            conditionsBlending.Add("UF_FLUX", uftemp.Rows[0]["FLUX"].ToString());
                            conditionsBlending.Add("ER_UF", uftemp.Rows[0]["PWRER_VAL"].ToString());

                            //RO 분석
                            conditionsBlending.Add("DAYFLW", intdayFlw.ToString());
                            rotemp = work.Select_ANALS_RO_ANAL(conditionsBlending);
                            conditionsBlending.Add("RO_MSRC", rotemp.Rows[0]["MSRC"].ToString());
                            conditionsBlending.Add("RO_PRESSR", rotemp.Rows[0]["TRND_CAL"].ToString());
                            conditionsBlending.Add("RO_FLUX", rotemp.Rows[0]["FLUX_BMTB"].ToString());
                            if (rotemp.Rows[0]["CTLVALV"].ToString().Equals("1"))
                                conditionsBlending.Add("RO_CTL", "Y");
                            else
                                conditionsBlending.Add("RO_CTL", "N");
                            conditionsBlending.Add("RO_TOTHR", rotemp.Rows[0]["RO_TOTHR"].ToString());
                            conditionsBlending.Add("RO_TOTMIN", rotemp.Rows[0]["RO_TOTMIN"].ToString());
                            conditionsBlending.Add("ER_RO", rotemp.Rows[0]["PWRER_VAL"].ToString());
                            conditionsBlending.Add("ER_TOT", Convert.ToDouble(rotemp.Rows[0]["PWRER_VAL"]) + Convert.ToDouble(uftemp.Rows[0]["PWRER_VAL"]));
                            conditionsBlending.Add("RUN_YN", "N");

                            work.Insert_ANALS_RESULT_BLENDING(conditionsBlending);

                            foreach (DataRow r in BWROTripletlist[i].Key.Rows)
                            {
                                if (!r["GBN"].Equals("Blending"))
                                {
                                    Hashtable conditionsDTL = new Hashtable();
                                    conditionsDTL.Add("KIND", r["GBN"].ToString());
                                    conditionsDTL.Add("DT", dt.ToString("yyyyMMddHHmmss"));
                                    conditionsDTL.Add("MSRC", strsplit[0]);
                                    conditionsDTL.Add("GBN", 3);
                                    conditionsDTL.Add("RANK", intblendrank);
                                    conditionsDTL.Add("TDS", Math.Round(Convert.ToDouble(r["TDS"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("RUN_RATE", Math.Round(Convert.ToDouble(r["RUN_RATE"]), 2, MidpointRounding.ToEven));
                                    conditionsDTL.Add("USEFLW", Math.Round(Convert.ToDouble(r["USEFLW"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("DTL_RANK", r["DTL_RANK"].ToString());
                                    work.Insert_ANALS_RESULT_DTL(conditionsDTL);
                                }
                            }
                        }

                        intblendrank++;
                    }
                    #endregion

                    #region Quadruple INSERT
                    foreach (DataTable dt in dsQuadrupletemp.Tables)
                    {
                        for (DateTime date = dtAnalStart; date < dtAnalEnd.AddDays(1); date = date.AddDays(1))
                        {
                            DataTable uftemp = new DataTable();
                            DataTable rotemp = new DataTable();
                            Hashtable conditionsBlending = new Hashtable();
                            string[] strsplit = dt.TableName.Split('_');
                            conditionsBlending.Add("SET_SEQ", strSET_SEQ);
                            conditionsBlending.Add("MSRC", strsplit[0]);
                            conditionsBlending.Add("GBN", 4);
                            conditionsBlending.Add("RANK", 1);

                            if (dsQuadrupletemp.Tables.IndexOf(dt) == 0)
                            {
                                if (Convert.ToDouble(dsQuadrupletemp.Tables[0].Rows[4]["TDS"]) < Convert.ToDouble(dsQuadrupletemp.Tables[1].Rows[4]["TDS"]))
                                {
                                    conditionsBlending.Add("USE_YN", "Y");
                                }
                                else
                                    conditionsBlending.Add("USE_YN", "N");
                            }
                            else if(dsQuadrupletemp.Tables.IndexOf(dt) == 1)
                            {
                                if (Convert.ToDouble(dsQuadrupletemp.Tables[1].Rows[4]["TDS"]) < Convert.ToDouble(dsQuadrupletemp.Tables[0].Rows[4]["TDS"]))
                                {
                                    conditionsBlending.Add("USE_YN", "Y");
                                }
                                else
                                    conditionsBlending.Add("USE_YN", "N");
                            }
                            else
                            {
                                conditionsBlending.Add("USE_YN", "N");
                            }

                            conditionsBlending.Add("B_TDS", Math.Round(Convert.ToDouble(dt.Rows[4]["TDS"]), 6, MidpointRounding.ToEven));
                            conditionsBlending.Add("B_RUN_RATE", dt.Rows[4]["RUN_RATE"].ToString());
                            conditionsBlending.Add("B_USEFLW", dt.Rows[4]["USEFLW"].ToString());
                            conditionsBlending.Add("DT", date.ToString("yyyyMMddHHmmss"));

                            //UF 분석
                            uftemp = work.Select_ANALS_UF_ANAL(conditionsBlending);
                            conditionsBlending.Add("UF_MSRC", uftemp.Rows[0]["STDR"].ToString());
                            conditionsBlending.Add("UF_FILTER", uftemp.Rows[0]["FILTER"].ToString());
                            conditionsBlending.Add("UF_TIME", uftemp.Rows[0]["TIME"].ToString());
                            conditionsBlending.Add("UF_FLUX", uftemp.Rows[0]["FLUX"].ToString());
                            conditionsBlending.Add("ER_UF", uftemp.Rows[0]["PWRER_VAL"].ToString());

                            //RO 분석
                            conditionsBlending.Add("DAYFLW", intdayFlw.ToString());
                            rotemp = work.Select_ANALS_RO_ANAL(conditionsBlending);
                            conditionsBlending.Add("RO_MSRC", rotemp.Rows[0]["MSRC"].ToString());
                            conditionsBlending.Add("RO_PRESSR", rotemp.Rows[0]["TRND_CAL"].ToString());
                            conditionsBlending.Add("RO_FLUX", rotemp.Rows[0]["FLUX_BMTB"].ToString());
                            if (rotemp.Rows[0]["CTLVALV"].ToString().Equals("1"))
                                conditionsBlending.Add("RO_CTL", "Y");
                            else
                                conditionsBlending.Add("RO_CTL", "N");
                            conditionsBlending.Add("RO_TOTHR", rotemp.Rows[0]["RO_TOTHR"].ToString());
                            conditionsBlending.Add("RO_TOTMIN", rotemp.Rows[0]["RO_TOTMIN"].ToString());
                            conditionsBlending.Add("ER_RO", rotemp.Rows[0]["PWRER_VAL"].ToString());
                            conditionsBlending.Add("ER_TOT", Convert.ToDouble(rotemp.Rows[0]["PWRER_VAL"]) + Convert.ToDouble(uftemp.Rows[0]["PWRER_VAL"]));
                            conditionsBlending.Add("RUN_YN", "N");

                            work.Insert_ANALS_RESULT_BLENDING(conditionsBlending);

                            foreach (DataRow r in dt.Rows)
                            {
                                if (!r["GBN"].Equals("Blending"))
                                {
                                    Hashtable conditionsDTL = new Hashtable();
                                    conditionsDTL.Add("KIND", r["GBN"].ToString());
                                    conditionsDTL.Add("DT", date.ToString("yyyyMMddHHmmss"));
                                    conditionsDTL.Add("MSRC", strsplit[0]);
                                    conditionsDTL.Add("GBN", 4);
                                    conditionsDTL.Add("RANK", 1);
                                    conditionsDTL.Add("TDS", Math.Round(Convert.ToDouble(r["TDS"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("RUN_RATE", Math.Round(Convert.ToDouble(r["RUN_RATE"]), 2, MidpointRounding.ToEven));
                                    conditionsDTL.Add("USEFLW", Math.Round(Convert.ToDouble(r["USEFLW"]), 5, MidpointRounding.ToEven));
                                    conditionsDTL.Add("DTL_RANK", r["DTL_RANK"].ToString());
                                    work.Insert_ANALS_RESULT_DTL(conditionsDTL);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
                return null;
            }

            return htresult;
        }
    }
}
