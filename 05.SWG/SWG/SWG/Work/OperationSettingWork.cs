﻿using SWG.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWG.Work
{
    public class OperationSettingWork
    {
        OperationSettingDao dao = new OperationSettingDao();

        #region ##################ANALS_PUMP_PWRER##################
        /// <summary>
        /// ANALS_PUMP_PWRER 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_PUMP_PWRER(Hashtable conditions)
        {
            return dao.Select_ANALS_PUMP_PWRER(conditions);
        }

        /// <summary>
        /// ANALS_PUMP_PWRER 중복 확인
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_PUMP_PWRER_OverLapCheck(Hashtable conditions)
        {
            return dao.Select_ANALS_PUMP_PWRER_OverLapCheck(conditions);
        }

        /// <summary>
        /// ANALS_PUMP_PWRER 삽입
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_ANALS_PUMP_PWRER(Hashtable conditions)
        {
            dao.Insert_ANALS_PUMP_PWRER(conditions);
        }

        /// <summary>
        /// ANALS_PUMP_PWRER 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_PUMP_PWRER(Hashtable conditions)
        {
            dao.Update_ANALS_PUMP_PWRER(conditions);
        }

        /// <summary>
        /// ANALS_PUMP_PWRER 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_ANALS_PUMP_PWRER(Hashtable conditions)
        {
            dao.Delete_ANALS_PUMP_PWRER(conditions);
        }
        #endregion

        #region ##################ANALS_PUMP_PWRER##################
        /// <summary>
        /// ANALS_UFPUMP_PWRER 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_UFPUMP_PWRER(Hashtable conditions)
        {
            return dao.Select_ANALS_UFPUMP_PWRER(conditions);
        }

        /// <summary>
        /// ANALS_UFPUMP_PWRER 중복 확인
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_UFPUMP_PWRER_OverLapCheck(Hashtable conditions)
        {
            return dao.Select_ANALS_UFPUMP_PWRER_OverLapCheck(conditions);
        }

        /// <summary>
        /// ANALS_UFPUMP_PWRER 삽입
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_ANALS_UFPUMP_PWRER(Hashtable conditions)
        {
            dao.Insert_ANALS_UFPUMP_PWRER(conditions);
        }

        /// <summary>
        /// ANALS_UFPUMP_PWRER 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_UFPUMP_PWRER(Hashtable conditions)
        {
            dao.Update_ANALS_UFPUMP_PWRER(conditions);
        }

        /// <summary>
        /// ANALS_UFPUMP_PWRER 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_ANALS_UFPUMP_PWRER(Hashtable conditions)
        {
            dao.Delete_ANALS_UFPUMP_PWRER(conditions);
        }
        #endregion

        #region ##################ANALS_SWBW_PWRER##################
        /// <summary>
        /// ANALS_SWBW_PWRER 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_SWBW_PWRER(Hashtable conditions)
        {
            return dao.Select_ANALS_SWBW_PWRER(conditions);
        }

        /// <summary>
        /// ANALS_SWBW_PWRER 중복 확인
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_SWBW_PWRER_OverLapCheck(Hashtable conditions)
        {
            return dao.Select_ANALS_SWBW_PWRER_OverLapCheck(conditions);
        }

        /// <summary>
        /// ANALS_SWBW_PWRER 삽입
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_ANALS_SWBW_PWRER(Hashtable conditions)
        {
            dao.Insert_ANALS_SWBW_PWRER(conditions);
        }

        /// <summary>
        /// ANALS_SWBW_PWRER 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_SWBW_PWRER(Hashtable conditions)
        {
            dao.Update_ANALS_SWBW_PWRER(conditions);
        }

        /// <summary>
        /// ANALS_SWBW_PWRER 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_ANALS_SWBW_PWRER(Hashtable conditions)
        {
            dao.Delete_ANALS_SWBW_PWRER(conditions);
        }
        #endregion

        #region ##################ANALS_SWBW_SETTING##################
        /// <summary>
        /// ANALS_SWBW_SETTING 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_SWBW_SETTING(Hashtable conditions)
        {
            return dao.Select_ANALS_SWBW_SETTING(conditions);
        }

        /// <summary>
        /// ANALS_SWBW_SETTING 중복 확인
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_SWBW_SETTING_OverLapCheck(Hashtable conditions)
        {
            return dao.Select_ANALS_SWBW_SETTING_OverLapCheck(conditions);
        }

        /// <summary>
        /// ANALS_SWBW_SETTING 삽입
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_ANALS_SWBW_SETTING(Hashtable conditions)
        {
            dao.Insert_ANALS_SWBW_SETTING(conditions);
        }

        /// <summary>
        /// ANALS_SWBW_SETTING 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_SWBW_SETTING(Hashtable conditions)
        {
            dao.Update_ANALS_SWBW_SETTING(conditions);
        }

        /// <summary>
        /// ANALS_SWBW_SETTING 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_ANALS_SWBW_SETTING(Hashtable conditions)
        {
            dao.Delete_ANALS_SWBW_SETTING(conditions);
        }
        #endregion

        #region ##################ANALS_BASE_SETTING##################
        /// <summary>
        /// ANALS_SWBW_SETTING 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_BASE_SETTING(Hashtable conditions)
        {
            return dao.Select_ANALS_BASE_SETTING(conditions);
        }

        /// <summary>
        /// ANALS_BASE_SETTING 삽입
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_ANALS_BASE_SETTING(Hashtable conditions)
        {
            dao.Insert_ANALS_BASE_SETTING(conditions);
        }
        #endregion

        #region ##################ANALS_UF_PWRER_PREDICT##################
        /// <summary>
        /// ANALS_UF_PWRER_PREDICT 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_UF_PWRER_PREDICT(Hashtable conditions)
        {
            return dao.Select_ANALS_UF_PWRER_PREDICT(conditions);
        }

        /// <summary>
        /// ANALS_UF_PWRER_PREDICT 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_UF_PWRER_PREDICT(Hashtable conditions)
        {
            dao.Update_ANALS_UF_PWRER_PREDICT(conditions);
        }
        #endregion

        #region ##################ANALS_UF_RUNCND##################
        /// <summary>
        /// ANALS_UF_RUNCND 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_UF_RUNCND(Hashtable conditions)
        {
            return dao.Select_ANALS_UF_RUNCND(conditions);
        }

        /// <summary>
        /// ANALS_UF_RUNCND 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_UF_RUNCND(Hashtable conditions)
        {
            dao.Update_ANALS_UF_RUNCND(conditions);
        }
        #endregion

        #region ##################ANALS_RO_RUNCND##################
        /// <summary>
        /// ANALS_RO_RUNCND 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_RO_RUNCND(Hashtable conditions)
        {
            return dao.Select_ANALS_RO_RUNCND(conditions);
        }

        /// <summary>
        /// ANALS_RO_RUNCND 수정
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_RO_RUNCND(Hashtable conditions)
        {
            dao.Update_ANALS_RO_RUNCND(conditions);
        }
        #endregion

        /// <summary>
        /// ANALS_OPERTIME 삽입
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_ANALS_OPERTIME(Hashtable conditions)
        {
            dao.Insert_ANALS_OPERTIME(conditions);
        }

        /// <summary>
        /// ANALS_OPERTIME
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_BASE_SETTING_OPERTIME(Hashtable conditions)
        {
            dao.Update_ANALS_BASE_SETTING_OPERTIME(conditions);
        }

        /// <summary>
        /// ANALS_OPERTIME 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Selects_ANALS_OPERTIME(Hashtable conditions)
        {
            return dao.Selects_ANALS_OPERTIME(conditions);
        }
    }
}
