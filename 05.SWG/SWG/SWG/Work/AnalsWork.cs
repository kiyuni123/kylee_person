﻿using SWG.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWG.Work
{
    class AnalsWork
    {
        AnalsDao dao = new AnalsDao();

        #region ##################정보 SELECT##################
        /// <summary>
        /// 분석에 필요한 기본정보 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ANALS_BASE_SETTING_ANAL(Hashtable conditions)
        {
            return dao.Select_ANALS_BASE_SETTING_ANAL(conditions);
        }

        /// <summary>
        /// ANALS_SETTING 삽입
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_ANALS_SETTING(Hashtable conditions)
        {
            dao.Insert_ANALS_SETTING(conditions);
        }

        /// <summary>
        /// ANALS_SETTING 조회
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_ANALS_SETTING_SEQ(Hashtable conditions)
        {
            return dao.Select_ANALS_SETTING_SEQ(conditions);
        }

        /// <summary>
        /// ANALS_RESULT_BLENDING 삽입
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_ANALS_RESULT_BLENDING(Hashtable conditions)
        {
            dao.Insert_ANALS_RESULT_BLENDING(conditions);
        }

        /// <summary>
        /// ANALS_RESULT_DTL 삽입
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_ANALS_RESULT_DTL(Hashtable conditions)
        {
            dao.Insert_ANALS_RESULT_DTL(conditions);
        }

        /// <summary>
        /// UF분석 결과 조회
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_ANALS_UF_ANAL(Hashtable conditions)
        {
            return dao.Select_ANALS_UF_ANAL(conditions);
        }

        /// <summary>
        /// RO분석 결과 조회
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_ANALS_RO_ANAL(Hashtable conditions)
        {
            return dao.Select_ANALS_RO_ANAL(conditions);
        }

        /// <summary>
        /// Delete Custom Setting 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_ANALS_RESULT_BLENDING(Hashtable conditions)
        {
            dao.Delete_ANALS_RESULT_BLENDING(conditions);
        }
        #endregion
    }
}
