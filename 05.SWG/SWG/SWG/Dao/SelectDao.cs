﻿using CMFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWG.Dao
{
    class SelectDao
    {
        #region ##################분석 SELECT##################
        /// <summary>
        /// Select_Economics
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_Economics(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_Economics", conditions);
        }

        /// <summary>
        /// Select_EconomicsDTL
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_EconomicsDTL(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_EconomicsDTL", conditions);
        }

        /// <summary>
        /// USE 변경(N)
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_RESULT_BLENDING_USEYN_N(Hashtable conditions)
        {
            DBManager.QueryForUpdate("Update_ANALS_RESULT_BLENDING_USEYN_N", conditions);
        }

        /// <summary>
        /// USE 변경(Y)
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_RESULT_BLENDING_USEYN_Y(Hashtable conditions)
        {
            DBManager.QueryForUpdate("Update_ANALS_RESULT_BLENDING_USEYN_Y", conditions);
        }        

        /// <summary>
        /// Select_Custom_Basic_Info
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_Custom_ANALS_SETTING(Hashtable conditions)
        {
            return DBManager.QueryForTable("Select_Custom_ANALS_SETTING", conditions);
        }

        /// <summary>
        /// RUN 변경(N)
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_RESULT_BLENDING_RUNYN_N(Hashtable conditions)
        {
            DBManager.QueryForUpdate("Update_ANALS_RESULT_BLENDING_RUNYN_N", conditions);
        }

        /// <summary>
        /// RUN 변경(Y)
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_ANALS_RESULT_BLENDING_RUNYN_Y(Hashtable conditions)
        {
            DBManager.QueryForUpdate("Update_ANALS_RESULT_BLENDING_RUNYN_Y", conditions);
        }


        #endregion
    }
}