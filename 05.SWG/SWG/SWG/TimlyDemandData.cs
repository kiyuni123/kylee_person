﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWG
{
    public class TimlyDemandData
    {
        public string strWKind { get; set; }
        public string strArea { get; set; }
        public string strDate { get; set; }
        public string strDemandValue { get; set; }
        public string strAddDemandValue { get; set; }
        public string strAllDemandValue { get; set; }
        public string strResult { get; set; }
        public string strRatio { get; set; }

        public static List<TimlyDemandData> GetDemandList()
        {
            List<TimlyDemandData> DemandTimeData = new List<TimlyDemandData>();

            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "01:00", strDemandValue = "1547", strAddDemandValue = "0", strAllDemandValue = "1547", strResult = "1420", strRatio = "109" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "02:00", strDemandValue = "1654", strAddDemandValue = "0", strAllDemandValue = "1654", strResult = "1700", strRatio = "97.2" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "03:00", strDemandValue = "1600", strAddDemandValue = "20", strAllDemandValue = "1620", strResult = "1500", strRatio = "108" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "04:00", strDemandValue = "1725", strAddDemandValue = "0", strAllDemandValue = "1725", strResult = "1760", strRatio = "98" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "05:00", strDemandValue = "1695", strAddDemandValue = "0", strAllDemandValue = "1695", strResult = "1750", strRatio = "96.9" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "06:00", strDemandValue = "1685", strAddDemandValue = "0", strAllDemandValue = "1685", strResult = "1680", strRatio = "100.3" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "07:00", strDemandValue = "1650", strAddDemandValue = "150", strAllDemandValue = "1800", strResult = "1740", strRatio = "103.5" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "08:00", strDemandValue = "1621", strAddDemandValue = "0", strAllDemandValue = "1621", strResult = "1700", strRatio = "95.4" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "09:00", strDemandValue = "1670", strAddDemandValue = "200", strAllDemandValue = "1970", strResult = "2000", strRatio = "98.5" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "10:00", strDemandValue = "1800", strAddDemandValue = "0", strAllDemandValue = "1800", strResult = "1720", strRatio = "104.7" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "11:00", strDemandValue = "1789", strAddDemandValue = "0", strAllDemandValue = "1789", strResult = "1710", strRatio = "104.6" });

            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "12:00", strDemandValue = "1654", strAddDemandValue = "0", strAllDemandValue = "1654", strResult = "1700", strRatio = "97.2" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "13:00", strDemandValue = "1600", strAddDemandValue = "20", strAllDemandValue = "1620", strResult = "1500", strRatio = "108" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "14:00", strDemandValue = "1725", strAddDemandValue = "0", strAllDemandValue = "1725", strResult = "1760", strRatio = "98" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "15:00", strDemandValue = "1695", strAddDemandValue = "0", strAllDemandValue = "1695", strResult = "1750", strRatio = "96.9" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "16:00", strDemandValue = "1685", strAddDemandValue = "0", strAllDemandValue = "1685", strResult = "1680", strRatio = "100.3" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "17:00", strDemandValue = "1650", strAddDemandValue = "150", strAllDemandValue = "1800", strResult = "1740", strRatio = "103.5" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "18:00", strDemandValue = "1621", strAddDemandValue = "0", strAllDemandValue = "1621", strResult = "1700", strRatio = "95.4" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "19:00", strDemandValue = "1670", strAddDemandValue = "200", strAllDemandValue = "1970", strResult = "2000", strRatio = "98.5" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "20:00", strDemandValue = "1800", strAddDemandValue = "0", strAllDemandValue = "1800", strResult = "1720", strRatio = "104.7" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "21:00", strDemandValue = "1789", strAddDemandValue = "0", strAllDemandValue = "1789", strResult = "1710", strRatio = "104.6" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "22:00", strDemandValue = "1654", strAddDemandValue = "0", strAllDemandValue = "1654", strResult = "1700", strRatio = "97.2" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "23:00", strDemandValue = "1600", strAddDemandValue = "20", strAllDemandValue = "1620", strResult = "1500", strRatio = "108" });
            DemandTimeData.Add(new TimlyDemandData() { strWKind = "구분1", strArea = "지역1", strDate = "24:00", strDemandValue = "1725", strAddDemandValue = "0", strAllDemandValue = "1725", strResult = "1760", strRatio = "98" });

            return DemandTimeData;
        }
    }
}
