<?xml version='1.0' encoding='windows-1252'?>
<!-- Copyright (c) 2004, Oracle. All Rights Reserved. -->
<!-- ref host for Windows -->
<HOST PLATID="912">
  <SYSTEM>
    <MEMORY>
      <PHYSICAL_MEMORY VALUE="128" UNIT="MB"/>
      <!--AVAILABLE_MEMORY VALUE="512" UNIT="MB"/-->
    </MEMORY>
  </SYSTEM> 
  <CERTIFIED_SYSTEMS>
    <OPERATING_SYSTEM>
    <!--Microsoft Windows 2000-->
      <VERSION VALUE="5.0"/>
      <SERVICE_PACK VALUE="1"/> 
    </OPERATING_SYSTEM>
    <OPERATING_SYSTEM>
    <!--Microsoft Windows XP-->
      <VERSION VALUE="5.1"/>
      <SERVICE_PACK VALUE="1"/> 
    </OPERATING_SYSTEM>
    <OPERATING_SYSTEM>
    <!--Microsoft Windows 2003-->
      <VERSION VALUE="5.2"/>
    </OPERATING_SYSTEM>
    <!--Microsoft Windows Vista-->
    <OPERATING_SYSTEM>
      <VERSION VALUE="6.0"/>
    </OPERATING_SYSTEM>
  </CERTIFIED_SYSTEMS>
  <ORACLE_HOME>
    <COMPATIBILITY_MATRIX>
      <ALLOW>
        <NEW_HOME/>
	<COMP NAME="oracle.server" ATLEAST="@<DB_VERSION_FIRST_DIGIT>@.@<DB_VERSION_SECOND_DIGIT>@.0.0.0" ATMOST="@<DB_VERSION_FIRST_DIGIT>@.@<DB_VERSION_SECOND_DIGIT>@.9.9.9"/>
	<COMP NAME="oracle.client" ATLEAST="@<DB_VERSION_FIRST_DIGIT>@.@<DB_VERSION_SECOND_DIGIT>@.0.0.0" ATMOST="@<DB_VERSION_FIRST_DIGIT>@.@<DB_VERSION_SECOND_DIGIT>@.9.9.9"/>
      </ALLOW>   
      <DISALLOW>
	<COMP NAME="oracle.server" ATLEAST="8.1.0.0.0" ATMOST="10.2.9.9.9"/>
	<COMP NAME="oracle.client" ATLEAST="8.1.0.0.0" ATMOST="9.2.0.9.0"/>
	<COMP NAME="oracle.iappserver.iapptop" ATLEAST="9.0.2.0.0" ATMOST="9.0.9.0.0"/>
	<COMP NAME="oracle.iappserver.infrastructure" ATLEAST="9.0.2.0.0" ATMOST="9.0.9.0.0"/>
	<COMP NAME="oracle.iappserver.devcorner " ATLEAST="9.0.2.0.0" ATMOST="9.0.9.0.0"/>
	<COMP NAME="oracle.ids.toplevel.development" ATLEAST="9.0.0.0.0" ATMOST="9.0.9.0.0"/>
	<COMP NAME="oracle.install.instcommon" ATLEAST="8.1.3.0.0" ATMOST="9.2.9.9.9"/>
	<COMP NAME="oracle.networking.netclt" ATLEAST="8.1.3.0.0" ATMOST="9.2.0.9.0"/>
	<ORCA_HOME/> 
      </DISALLOW>     
    </COMPATIBILITY_MATRIX>
  </ORACLE_HOME>
</HOST>
