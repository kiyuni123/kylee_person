﻿using System;
using System.Collections.Generic;
using Infragistics.Win.UltraWinGrid;
using System.Drawing;
using System.Data;
using Infragistics.Win;
using System.Windows.Forms;
using System.Collections;

namespace EMFrame.manager
{
    public class GridManager
    {
        public GridManager() { }

        private IList<UltraGrid> _items = new List<UltraGrid>();

        public IList<UltraGrid> Items
        {
            get
            {
                return this._items;
            }
        }

        public void Add(UltraGrid item)
        {
            this._items.Add(item);
            this.InitializeGrid(item);
            this.InitializeEvent(item);
        }

        private void InitializeGrid(UltraGrid item)
        {
            item.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            item.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.TopFixed;

            item.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            item.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            item.DisplayLayout.GroupByBox.BorderStyle = UIElementBorderStyle.Solid;
            item.DisplayLayout.GroupByBox.Hidden = true;
            item.DisplayLayout.MaxColScrollRegions = 1;
            item.DisplayLayout.MaxRowScrollRegions = 1;

            item.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Solid;
            item.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Solid;
            item.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
            item.DisplayLayout.Override.CellPadding = 0;
            item.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            item.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;
            item.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            item.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            item.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            item.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            item.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            item.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;

            item.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            item.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;
            item.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.Default;
            //DSOH 추가. Row Select는 무조건 Single만 되고, Drag가 되지 않게.
            item.DisplayLayout.Override.SelectTypeRow = SelectType.SingleAutoDrag;

            item.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;
            item.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            item.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            item.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            item.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            item.Font = new Font("굴림", 9f, FontStyle.Regular, GraphicsUnit.Point, 129);

            for (int I = 0; I <= item.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                item.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                item.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                item.DisplayLayout.Bands[0].Columns[I].Header.Appearance.BackGradientStyle = GradientStyle.None;
                item.DisplayLayout.Bands[0].Columns[I].CellActivation = Activation.NoEdit;
            }

            item.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            item.DisplayLayout.Override.HeaderAppearance.TextVAlign = VAlign.Middle;

            item.DisplayLayout.Override.GroupByColumnHeaderAppearance.TextHAlign = HAlign.Center;
            item.DisplayLayout.Override.GroupByColumnHeaderAppearance.TextVAlign = VAlign.Middle;

            item.DisplayLayout.Appearance.BorderColor = SystemColors.InactiveCaption;
            item.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.ColumnChooserButton;
            item.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            item.Font = new Font("굴림", 9f);

            item.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            item.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            item.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            foreach (UltraGridColumn gridColumn in item.DisplayLayout.Bands[0].Columns)
            {
                gridColumn.Header.Appearance.TextVAlign = VAlign.Middle;
                gridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            }

            item.Select();
        }

        public void SetRowClick(UltraGrid item, bool isClick)
        {
            if (isClick)
            {
                item.DisplayLayout.Override.ActiveRowAppearance.BackColor = SystemColors.Highlight;
                item.DisplayLayout.Override.ActiveRowAppearance.ForeColor = SystemColors.HighlightText;
            }

            if (!isClick)
            {
                item.DisplayLayout.Override.ActiveRowAppearance.BackColor = Color.Empty;
                item.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Empty;
            }
        }

        public void SetCellClick(UltraGrid item)
        {
            item.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;
            item.DisplayLayout.Override.SelectTypeCell = SelectType.Single;
            item.DisplayLayout.SelectionOverlayColor = Color.Empty;
            item.DisplayLayout.SelectionOverlayBorderColor = Color.Blue;
            item.DisplayLayout.Override.ActiveCellBorderThickness = 3;
            item.DisplayLayout.Override.ActiveCellAppearance.BorderColor = Color.Blue;
            item.DisplayLayout.Override.ActiveCellAppearance.BorderColor2 = Color.FromArgb(128, 128, 255);
        }


        private void InitializeEvent(UltraGrid item)
        {
            item.KeyDown += new KeyEventHandler(item_KeyDown);
        }

        private void item_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ((UltraGrid)sender).PerformAction(UltraGridAction.ExitEditMode);
            }
        }

        /// <summary>
        /// 그래드매니저 클래스에 등록된 그리드의 컬럼을 추가하고 스타일을 설정한다.
        /// 인자로 컬럼의 키값을 갖으며 기본적인 컬럼의 스타일은 수치형데이터에 스타일이다.
        /// </summary>
        /// <param name="key">Column Key Property</param>
        private void AddColumn(UltraGrid item, string key)
        {
            int columnsCount = item.DisplayLayout.Bands[0].Columns.Count;
            UltraGridColumn column = item.DisplayLayout.Bands[0].Columns.Insert(columnsCount, key);
            column.DataType = typeof(int);
            column.Format = "###,###,###";

            column.Width = 60;
            column.Key = key;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Caption = key;
        }

        /// <summary>
        /// 그래드매니저 클래스에 등록된 그리드의 컬럼을 추가하고 스타일을 설정한다.
        /// 인자로 컬럼의 키값을 갖으며 기본적인 컬럼의 스타일은 수치형데이터에 스타일이다.
        /// </summary>
        /// <param name="key">Column Key Property</param>
        private void AddColumn(UltraGrid item, string key, string format)
        {
            int columnsCount = item.DisplayLayout.Bands[0].Columns.Count;
            UltraGridColumn column = item.DisplayLayout.Bands[0].Columns.Insert(columnsCount, key);
            column.DataType = typeof(double);
            column.Format = format;

            column.Width = 60;
            column.Key = key;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Caption = key;
        }


        /// <summary>
        /// 그래드매니저 클래스에 등록된 그리드의 동적으로 추가된 컬럼을 모두 삭제한다.
        /// 삭제를 하지 않으면 컬럼추가시에 기존에 생성한 컬럼과 남아있는 컬럼이 뒤섞이게 된다.
        /// </summary>
        public void RemoveDynamicComumns(UltraGrid item)
        {
            ColumnsCollection columns = item.DisplayLayout.Bands[0].Columns;
            for (int i = columns.Count - 1; i >= 0; i--)
            {
                if (!columns[i].IsBound)
                {
                    columns.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// 데이터 소스 삭제
        /// </summary>
        public void ClearDataSource(UltraGrid item)
        {
            if (item.DataSource != null)
            {
                if (item.DataSource is BindingSource)
                {
                    BindingSource source = item.DataSource as BindingSource;
                    source.Clear();
                }

                if (item.DataSource is IList)
                {
                    IList source = item.DataSource as IList;
                    source.Clear();
                }

                if (item.DataSource is DataTable)
                {
                    DataTable source = item.DataSource as DataTable;
                    source.Clear();
                }
            }
        }

        /// <summary>
        /// 기본설정 : 그리드의 빈값을 설정한다.
        /// 빈 로우 생성/
        /// </summary>
        public void DefaultColumnsMapping(UltraGrid ultraGrid)
        {
            DataTable dataTable = new DataTable();

            foreach (UltraGridColumn gridColumn in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                dataTable.Columns.Add(gridColumn.Key, typeof(double));
            }

            DataRow dataRow = dataTable.NewRow();

            foreach (DataColumn dataColumn in dataTable.Columns)
            {
                dataRow[dataColumn.ColumnName] = DBNull.Value;
            }

            dataTable.Rows.Add(dataRow);

            ultraGrid.DataSource = dataTable;
        }

        /// <summary>
        /// 기본설정 : 그리드의 빈값을 설정한다.
        /// 빈 로우 생성/
        /// </summary>
        public void DefaultColumnsMapping(UltraGrid ultraGrid, Type t)
        {
            DataTable dataTable = new DataTable();

            foreach (UltraGridColumn gridColumn in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                dataTable.Columns.Add(gridColumn.Key, t);
            }

            DataRow dataRow = dataTable.NewRow();

            foreach (DataColumn dataColumn in dataTable.Columns)
            {
                dataRow[dataColumn.ColumnName] = DBNull.Value;
            }

            dataTable.Rows.Add(dataRow);

            ultraGrid.DataSource = dataTable;
        }

        /// <summary>
        /// 기본설정 : 그리드의 빈값을 설정한다.
        /// 로우 생성 안함
        /// </summary>
        public void DefaultColumnsMapping2(UltraGrid ultraGrid)
        {
            DataTable dataTable = new DataTable();

            foreach (UltraGridColumn gridColumn in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                dataTable.Columns.Add(gridColumn.Key);
            }

            ultraGrid.DataSource = dataTable;
        }
    }
}
