﻿using System;
using System.Collections.Generic;
using ChartFX.WinForms;
using System.Drawing;
using ChartFX.WinForms.Adornments;

namespace EMFrame.manager
{
    public class ChartManager
    {
        private IList<Chart> _items = new List<Chart>();

        public IList<Chart> Items
        {
            get
            {
                return this._items;
            }
        }

        public ChartManager() {}

        public void Add(Chart item)
        {
            this._items.Add(item);
            this.InitializeChart(item);
            this.InitializeEvent(item);
        }

        private void InitializeEvent(Chart item)
        {
            //item.MouseDoubleClick += new ChartFX.WinForms.HitTestEventHandler(item_MouseDoubleClick);
            item.MouseClick += new HitTestEventHandler(item_MouseClick);
        }

        private void item_MouseClick(object sender, HitTestEventArgs e)
        {
            Chart chart = (Chart)sender;

            e = chart.HitTest(e.AbsoluteLocation.X, e.AbsoluteLocation.Y, true);

            if (e.HitType.ToString() == "LegendBox")
            {
                if (chart.Series.Count == 1)
                {
                    return;
                }

                if (e.Series != -1)
                {
                    SeriesAttributes series = chart.Series[e.Series];

                    //숨겨진 목록에 있음 -> 목록에 제거하고 보임
                    if (this.hiddenSeries.ContainsKey(series))
                    {
                        List<double> data = hiddenSeries[series];

                        for (int i = 0; i < chart.Data.Points; i++)
                        {
                            chart.Data[e.Series, i] = data[i];
                        }

                        this.hiddenSeries.Remove(series);

                        chart.LegendBox.ItemAttributes[chart.Series, e.Series].TextColor = Color.Black;

                    }
                    //숨겨진 목록에 없음 -> 목록에 추가하고 숨김
                    else if (!this.hiddenSeries.ContainsKey(series))
                    {
                        List<double> data = new List<double>();
                        for (int i = 0; i < chart.Data.Points; i++)
                        {
                            data.Add(chart.Data[e.Series, i]);
                            chart.Data[e.Series, i] = Chart.Hidden;
                        }

                        this.hiddenSeries.Add(series, data);

                        chart.LegendBox.ItemAttributes[chart.Series, e.Series].TextColor = Color.DarkGray;
                    }
                }
            }
        }

        private Dictionary<SeriesAttributes, List<Double>> hiddenSeries = new Dictionary<SeriesAttributes, List<double>>();

        private void item_MouseDoubleClick(object sender, ChartFX.WinForms.HitTestEventArgs e)
        {
            Chart chart = (Chart)sender;

            e = chart.HitTest(e.AbsoluteLocation.X, e.AbsoluteLocation.Y, true);

            if (e.HitType.ToString() == "LegendBox")
            {
                if (chart.Series.Count == 1)
                {
                    return;
                }

                if (e.Series != -1)
                {
                    SeriesAttributes series = chart.Series[e.Series];

                    //숨겨진 목록에 있음
                    if (this.hiddenSeries.ContainsKey(series))
                    {
                        List<double> data = hiddenSeries[series];

                        for (int i = 0; i < chart.Data.Points; i++)
                        {
                            chart.Data[e.Series, i] = data[i];
                        }

                        this.hiddenSeries.Remove(series);

                        //차트 시리즈 제목 색 전환
                    }
                    //숨겨진 목록에 없음
                    else if (!this.hiddenSeries.ContainsKey(series))
                    {
                        List<double> data = new List<double>();
                        for (int i = 0; i < chart.Data.Points; i++)
                        {
                            data.Add(chart.Data[e.Series, i]);
                            chart.Data[e.Series, i] = Chart.Hidden;
                        }
                        this.hiddenSeries.Add(series, data);

                        //차트 시리즈 제목 색 전환
                    }
                }
            }
        }

        private void InitializeChart(Chart item)
        {
            item.LegendBox.Visible = true;
            item.LegendBox.Dock = DockArea.Bottom;
            item.LegendBox.MarginY = 1;
            //item.LegendBox.Visible = false;

            item.AxisX.Staggered = true;
            item.BackColor = Color.White;

            GradientBackground gradient = new GradientBackground(GradientType.Vertical);
            gradient.ColorTo = Color.White;
            gradient.ColorFrom = Color.White;
            item.Background = gradient;

            this.AllClear(item);
            this.AllHiddenSeries(item);
        }

        public void AllClear(int itemIndex)
        {
            this.AllClear(this._items[itemIndex]);
        }

        public void AllHiddenSeries(Chart item)
        {
            foreach (SeriesAttributes series in item.Series)
            {
                series.Visible = false;
            }
        }

        public void AllShowSeries(Chart item)
        {
            foreach (SeriesAttributes series in item.Series)
            {
                series.Visible = true;
            }
        }

        public void AllClear(Chart item)
        {
            item.Series.Clear();
            item.AxesX.Clear();
            item.AxesY.Clear();
            item.DataSourceSettings.Fields.Clear();
            item.Data.Clear();
        }

        public void SetAxisXFormat(int itemIndex, string format)
        {
            this._items[itemIndex].AxisX.DataFormat.Format = AxisFormat.DateTime;
            this._items[itemIndex].AxisX.DataFormat.CustomFormat = format;
        }

        public void SetAxisXFormat(Chart item, string format)
        {
            foreach (Chart chart in _items)
            {
                if (chart.Equals(item))
                {
                    chart.AxisX.DataFormat.Format = AxisFormat.DateTime;
                    chart.AxisX.DataFormat.CustomFormat = format;
                }
            }
        }

        public void ZeroHidden(Chart item)
        {
            //값이 0 인경우 차트에 표시안함(차트 조건절 설정)
            ConditionalAttributes conditionalAttributes = new ConditionalAttributes();
            conditionalAttributes.Condition.From = 0;
            conditionalAttributes.Condition.To = 0;
            conditionalAttributes.MarkerShape = MarkerShape.None;
            item.ConditionalAttributes.Add(conditionalAttributes);
        }
    }
}
