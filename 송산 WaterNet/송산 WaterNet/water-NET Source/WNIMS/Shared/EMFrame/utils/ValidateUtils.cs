﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMFrame.utils
{
    public class ValidateUtils
    {
        public static bool IsDouble(object target)
        {
            double result = 0;
            return Double.TryParse(target.ToString(), out result);
        }

        public static bool IsDouble(object target, double result)
        {
            return Double.TryParse(target.ToString(), out result);
        }

        //0보다 큰 양의 정수인지 확인
        public static bool IsPositiveInt(object value)
        {
            bool result = false;

            try
            {
                if (int.Parse(value.ToString()) > 0)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                return result;
            }

            return result;
        }
    }
}
