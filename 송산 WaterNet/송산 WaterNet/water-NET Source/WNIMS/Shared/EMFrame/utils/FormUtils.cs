﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System.Data;

namespace EMFrame.utils
{
    public class FormUtils
    {
        /// <summary>
        /// 특정폼안에 Type이 현재 존재 하는지 체크함
        /// ex) HashOwnedForm(oForm, typeof(Test.form.subForm))
        /// </summary>
        /// <param name="form"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool HashOwnedForm(Form form, Type type)
        {
            bool result = false;
            foreach (Form oForm in form.OwnedForms)
            {
                if (oForm.GetType().Equals(type))
                {
                    oForm.WindowState = FormWindowState.Normal;
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// 특정객체를 Hashtable로 변환한다.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static Hashtable ConverToHashtable(object target)
        {
            Hashtable parameter = null;

            if (target is UltraGridRow)
            {
                UltraGridRow row = target as UltraGridRow;
                parameter = new Hashtable();

                foreach (UltraGridCell cell in row.Cells)
                {
                    if (cell.Value is DateTime)
                    {
                        parameter.Add(cell.Column.Key, Convert.ToDateTime(cell.Value).ToString("yyyyMMdd"));
                    }
                    else
                    {
                        parameter.Add(cell.Column.Key, cell.Value);
                    }
                }
            }
            else if (target is Control)
            {
                Control control = target as Control;
                parameter = new Hashtable();
                AddHashtableToControls(parameter, control);
            }
            else if (target is DataRow)
            {
                DataRow row = target as DataRow;
                parameter = new Hashtable();

                foreach (DataColumn column in row.Table.Columns)
                {
                    if (row[column] is DateTime)
                    {
                        parameter.Add(column.ColumnName, Convert.ToDateTime(row[column]).ToString("yyyyMMdd"));
                    }
                    else
                    {
                        parameter.Add(column.ColumnName, row[column]);
                    }
                }
            }

            return parameter;
        }

        /// <summary>
        /// 콘트롤내 하위컨트롤 포함 Hashtable로 변환한다.
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="target"></param>
        private static void AddHashtableToControls(Hashtable parameter, Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    AddHashtableToControls(parameter, control);
                }
                else
                {
                    string parameterName = control.Name.ToUpper();
                    object parameterValue = null;

                    if (control is TextBox)
                    {
                        parameterValue = control.Text;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        parameterValue = combobox.SelectedValue;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;

                        if (date.MaskInput == "{date} hh:mm")
                        {
                            parameterValue = Convert.ToDateTime(date.Value).ToString("yyyyMMddHHmm");
                        }

                        if (date.MaskInput == "{date}")
                        {
                            parameterValue = Convert.ToDateTime(date.Value).ToString("yyyyMMdd");
                        }

                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is RichTextBox)
                    {
                        parameterValue = control.Text;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is CheckBox)
                    {
                        CheckBox cb = control as CheckBox;
                        parameterValue = cb.Checked.ToString();
                        if (parameterValue == null)
                        {
                            parameterValue = "false";
                        }
                        parameter.Add(parameterName, parameterValue);
                    }
                }
            }
        }

        /// <summary>
        /// 컨트롤내 입력을 초기화한다.
        /// </summary>
        /// <param name="target"></param>
        public static void ClearControls(Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    ClearControls(control);
                }
                else
                {
                    if (control is TextBox)
                    {
                        control.Text = string.Empty;
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        if (combobox.Items.Count > 0)
                        {
                            combobox.SelectedIndex = 0;
                        }
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        date.Value = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    }

                    if (control is RichTextBox)
                    {
                        control.Text = string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// 컨트롤내 입력에 Hashtable값을 준다.
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="target"></param>
        public static void SetControlDataSetting(Hashtable parameter, Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    SetControlDataSetting(parameter, control);
                }
                else
                {
                    string parameterName = control.Name.ToUpper();

                    if (control is TextBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            control.Text = parameter[parameterName].ToString();
                        }
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        if (parameter.ContainsKey(parameterName))
                        {
                            combobox.SelectedValue = parameter[parameterName];
                        }
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        if (parameter.ContainsKey(parameterName))
                        {
                            date.Value = parameter[parameterName].ToString();
                        }
                    }

                    if (control is RichTextBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            control.Text = parameter[parameterName].ToString();
                        }
                    }

                    if (control is CheckBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            CheckBox cb = control as CheckBox;
                            cb.Checked = Convert.ToBoolean(parameter[parameterName]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 그리드에 새로운 행 추가
        /// </summary>
        /// <param name="grid"></param>
        public static void AddRow(UltraGrid grid)
        {
            grid.ActiveRow = null;

            UltraGridRow newRow = grid.DisplayLayout.Bands[0].AddNew();
            newRow.Activated = true;
            newRow.Selected = true;
            newRow.Appearance.BackColor = Color.LemonChiffon; 

            grid.ActiveRowScrollRegion.ScrollRowIntoView(newRow);
        }

        /// <summary>
        /// 그리드의 변경사항을 저장
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="keyColumns"></param>
        /// <returns></returns>
        public static bool SaveData(UltraGrid grid, ArrayList keyColumns)
        {
            bool result = false;

            DataTable tmpTable = (DataTable)grid.DataSource;

            bool isChanged = false;
            bool isKeyValidate = true;

            foreach (DataRow row in tmpTable.Rows)
            {
                if (row.RowState == DataRowState.Added || row.RowState == DataRowState.Modified)
                {
                    foreach (string keyColumnName in keyColumns)
                    {
                        if ("".Equals((row[keyColumnName]).ToString().Trim()))
                        {
                            isKeyValidate = false;
                            break;
                        }
                    }

                    isChanged = true;
                }
            }

            if (!isChanged)
            {
                MessageBox.Show("변경된 행이 없습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (!isKeyValidate)
            {
                MessageBox.Show("열의 Key가 빈값인 행이 존재합니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("저장하시겠습니까?", "데이터 저장", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialogResult == DialogResult.Yes)
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// 그리드의 특정 행을 삭제
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static bool DeleteRow(UltraGrid grid)
        {
            bool result = false;

            if (grid.Rows.Count > 0)
            {
                DataRowView rowView = grid.ActiveRow.ListObject as DataRowView;

                if (rowView.Row.RowState == DataRowState.Detached || rowView.Row.RowState == DataRowState.Added)
                {
                    //추가행인 경우 그냥 삭제
                    grid.ActiveRow.Delete(false);

                    if (grid.Rows.Count > 0)
                    {
                        grid.Rows[grid.Rows.Count - 1].Activated = true;
                        grid.ActiveRowScrollRegion.ScrollRowIntoView(grid.Rows[grid.Rows.Count - 1]);
                    }
                }
                else
                {
                    //그외의 경우는 DB 삭제
                    DialogResult dialogResult = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (dialogResult == DialogResult.Yes)
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 선택된 Row의 특정 Column값을 반환
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public static Object GetGridValue(UltraGrid grid, string keyName)
        {
            return grid.ActiveRow.Cells[keyName].Value;
        }

        //해당 Control내의 하위 Control을 key가 Control명 value가 Control인 Hashtable로 반환
        public static Hashtable GetControlList(System.Windows.Forms.Control.ControlCollection controls)
        {
            Hashtable result = new Hashtable();
            Hashtable labelList = new Hashtable();
            Hashtable textBoxList = new Hashtable();

            foreach (Control ctr in controls)
            {
                if ("System.Windows.Forms.Label".Equals(ctr.GetType().ToString()))
                {
                    if (ctr.Name.Contains("_"))
                    {
                        labelList.Add(ctr.Name, ctr);
                    }
                }
                else if ("Infragistics.Win.UltraWinEditors.UltraTextEditor".Equals(ctr.GetType().ToString()))
                {
                    if (ctr.Name.Contains("_"))
                    {
                        textBoxList.Add(ctr.Name, ctr);
                    }
                }
            }

            result.Add("labelList", labelList);
            result.Add("textBoxList", textBoxList);

            return result;
        }

        public static Form Launchform(string assemblyname, string formname)
        {
            string fname = string.Concat(assemblyname, ".", formname);
            foreach (Form f in Application.OpenForms)
            {
                if (string.Concat(f.ProductName, ".", f.Name).Equals(fname))
                {
                    f.Activate();
                    return f;
                }
            }
            return null;
        }

        public static Form Launchform(string fname)
        {
            foreach (Form f in Application.OpenForms)
            {
                if (fname.Equals(f.GetType().ToString()))
                {
                    f.Activate();
                    return f;
                }
            }
            return null;
        }

        public static Form Launchform(Form form)
        {
            foreach (Form f in Application.OpenForms)
            {
                if (form.Equals(f))
                {
                    f.Activate();
                    return f;
                }
            }
            return null;
        }
    }
}
