﻿namespace WS_UserManager.forms
{
    partial class frmUserManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ugLMenu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ugGMenu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkAll = new System.Windows.Forms.CheckBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnHrBank = new System.Windows.Forms.Button();
            this.btnDelete2 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cboManageDept = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboHEADQUARTER = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ugData = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.txtimsipassword = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkGlobal = new System.Windows.Forms.CheckBox();
            this.chkLocal = new System.Windows.Forms.CheckBox();
            this.cboGUserGroup = new System.Windows.Forms.ComboBox();
            this.cboLUserGroup = new System.Windows.Forms.ComboBox();
            this.txtSabun = new System.Windows.Forms.TextBox();
            this.gbUsed = new System.Windows.Forms.GroupBox();
            this.rdUseYN_0 = new System.Windows.Forms.RadioButton();
            this.rdUseYN_1 = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gbExtYN = new System.Windows.Forms.GroupBox();
            this.rdExtYN_0 = new System.Windows.Forms.RadioButton();
            this.rdExtYN_1 = new System.Windows.Forms.RadioButton();
            this.rdExtYN_2 = new System.Windows.Forms.RadioButton();
            this.gbUserRight = new System.Windows.Forms.GroupBox();
            this.rdRight_2 = new System.Windows.Forms.RadioButton();
            this.rdRight_0 = new System.Windows.Forms.RadioButton();
            this.rdRight_1 = new System.Windows.Forms.RadioButton();
            this.rdRight_3 = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cboManageDept2 = new System.Windows.Forms.ComboBox();
            this.cboHEADQUARTER2 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUDivision = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNCPWD = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPWD = new System.Windows.Forms.TextBox();
            this.txtUID = new System.Windows.Forms.TextBox();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugLMenu)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugGMenu)).BeginInit();
            this.panel1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugData)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbUsed.SuspendLayout();
            this.gbExtYN.SuspendLayout();
            this.gbUserRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ugLMenu);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(582, 393);
            // 
            // ugLMenu
            // 
            this.ugLMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugLMenu.Location = new System.Drawing.Point(0, 0);
            this.ugLMenu.Name = "ugLMenu";
            this.ugLMenu.Size = new System.Drawing.Size(582, 393);
            this.ugLMenu.TabIndex = 37;
            this.ugLMenu.Text = "사용자";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ugGMenu);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(582, 393);
            // 
            // ugGMenu
            // 
            this.ugGMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugGMenu.Location = new System.Drawing.Point(0, 0);
            this.ugGMenu.Name = "ugGMenu";
            this.ugGMenu.Size = new System.Drawing.Size(582, 393);
            this.ugGMenu.TabIndex = 38;
            this.ugGMenu.Text = "사용자";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkAll);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.btnHrBank);
            this.panel1.Controls.Add(this.btnDelete2);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.cboManageDept);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cboHEADQUARTER);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 40);
            this.panel1.TabIndex = 36;
            // 
            // chkAll
            // 
            this.chkAll.AutoSize = true;
            this.chkAll.Location = new System.Drawing.Point(706, 14);
            this.chkAll.Name = "chkAll";
            this.chkAll.Size = new System.Drawing.Size(48, 16);
            this.chkAll.TabIndex = 68;
            this.chkAll.Text = "전체";
            this.chkAll.UseVisualStyleBackColor = true;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(559, 10);
            this.txtName.MaxLength = 32;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(136, 21);
            this.txtName.TabIndex = 67;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(507, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 12);
            this.label14.TabIndex = 43;
            this.label14.Text = "사용자 :";
            // 
            // btnHrBank
            // 
            this.btnHrBank.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHrBank.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnHrBank.Location = new System.Drawing.Point(915, 4);
            this.btnHrBank.Name = "btnHrBank";
            this.btnHrBank.Size = new System.Drawing.Size(75, 34);
            this.btnHrBank.TabIndex = 42;
            this.btnHrBank.Text = "HRBANK 동기화";
            this.btnHrBank.UseVisualStyleBackColor = true;
            this.btnHrBank.Visible = false;
            this.btnHrBank.Click += new System.EventHandler(this.btnHrBank_Click);
            // 
            // btnDelete2
            // 
            this.btnDelete2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete2.Location = new System.Drawing.Point(834, 4);
            this.btnDelete2.Name = "btnDelete2";
            this.btnDelete2.Size = new System.Drawing.Size(75, 34);
            this.btnDelete2.TabIndex = 41;
            this.btnDelete2.Text = "삭제";
            this.btnDelete2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete2.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(256, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 12);
            this.label8.TabIndex = 12;
            this.label8.Text = "관리센터 :";
            // 
            // cboManageDept
            // 
            this.cboManageDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboManageDept.FormattingEnabled = true;
            this.cboManageDept.Location = new System.Drawing.Point(321, 10);
            this.cboManageDept.Name = "cboManageDept";
            this.cboManageDept.Size = new System.Drawing.Size(180, 20);
            this.cboManageDept.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "지역본부 :";
            // 
            // cboHEADQUARTER
            // 
            this.cboHEADQUARTER.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHEADQUARTER.FormattingEnabled = true;
            this.cboHEADQUARTER.Location = new System.Drawing.Point(70, 10);
            this.cboHEADQUARTER.Name = "cboHEADQUARTER";
            this.cboHEADQUARTER.Size = new System.Drawing.Size(174, 20);
            this.cboHEADQUARTER.TabIndex = 9;
            this.cboHEADQUARTER.SelectedIndexChanged += new System.EventHandler(this.cboHEADQUARTER_SelectedIndexChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(754, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 34);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "조회";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(5, 45);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ugData);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(997, 629);
            this.splitContainer1.SplitterDistance = 206;
            this.splitContainer1.TabIndex = 37;
            // 
            // ugData
            // 
            this.ugData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugData.Location = new System.Drawing.Point(0, 0);
            this.ugData.Name = "ugData";
            this.ugData.Size = new System.Drawing.Size(997, 206);
            this.ugData.TabIndex = 36;
            this.ugData.Text = "사용자";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.txtimsipassword);
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer2.Panel1.Controls.Add(this.txtSabun);
            this.splitContainer2.Panel1.Controls.Add(this.gbUsed);
            this.splitContainer2.Panel1.Controls.Add(this.label13);
            this.splitContainer2.Panel1.Controls.Add(this.label12);
            this.splitContainer2.Panel1.Controls.Add(this.gbExtYN);
            this.splitContainer2.Panel1.Controls.Add(this.gbUserRight);
            this.splitContainer2.Panel1.Controls.Add(this.label11);
            this.splitContainer2.Panel1.Controls.Add(this.btnAdd);
            this.splitContainer2.Panel1.Controls.Add(this.cboManageDept2);
            this.splitContainer2.Panel1.Controls.Add(this.cboHEADQUARTER2);
            this.splitContainer2.Panel1.Controls.Add(this.label10);
            this.splitContainer2.Panel1.Controls.Add(this.label9);
            this.splitContainer2.Panel1.Controls.Add(this.btnDelete);
            this.splitContainer2.Panel1.Controls.Add(this.btnSave);
            this.splitContainer2.Panel1.Controls.Add(this.label7);
            this.splitContainer2.Panel1.Controls.Add(this.label6);
            this.splitContainer2.Panel1.Controls.Add(this.txtUDivision);
            this.splitContainer2.Panel1.Controls.Add(this.label5);
            this.splitContainer2.Panel1.Controls.Add(this.txtUName);
            this.splitContainer2.Panel1.Controls.Add(this.label4);
            this.splitContainer2.Panel1.Controls.Add(this.txtNCPWD);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.txtPWD);
            this.splitContainer2.Panel1.Controls.Add(this.txtUID);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.ultraTabControl1);
            this.splitContainer2.Size = new System.Drawing.Size(997, 419);
            this.splitContainer2.SplitterDistance = 407;
            this.splitContainer2.TabIndex = 62;
            // 
            // txtimsipassword
            // 
            this.txtimsipassword.Enabled = false;
            this.txtimsipassword.Location = new System.Drawing.Point(46, 388);
            this.txtimsipassword.MaxLength = 12;
            this.txtimsipassword.Name = "txtimsipassword";
            this.txtimsipassword.Size = new System.Drawing.Size(63, 21);
            this.txtimsipassword.TabIndex = 91;
            this.txtimsipassword.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkGlobal);
            this.groupBox1.Controls.Add(this.chkLocal);
            this.groupBox1.Controls.Add(this.cboGUserGroup);
            this.groupBox1.Controls.Add(this.cboLUserGroup);
            this.groupBox1.Location = new System.Drawing.Point(113, 275);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(281, 65);
            this.groupBox1.TabIndex = 90;
            this.groupBox1.TabStop = false;
            // 
            // chkGlobal
            // 
            this.chkGlobal.AutoSize = true;
            this.chkGlobal.Location = new System.Drawing.Point(11, 39);
            this.chkGlobal.Name = "chkGlobal";
            this.chkGlobal.Size = new System.Drawing.Size(48, 16);
            this.chkGlobal.TabIndex = 88;
            this.chkGlobal.Text = "광역";
            this.chkGlobal.UseVisualStyleBackColor = true;
            // 
            // chkLocal
            // 
            this.chkLocal.AutoSize = true;
            this.chkLocal.Location = new System.Drawing.Point(11, 14);
            this.chkLocal.Name = "chkLocal";
            this.chkLocal.Size = new System.Drawing.Size(48, 16);
            this.chkLocal.TabIndex = 87;
            this.chkLocal.Text = "지방";
            this.chkLocal.UseVisualStyleBackColor = true;
            // 
            // cboGUserGroup
            // 
            this.cboGUserGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGUserGroup.FormattingEnabled = true;
            this.cboGUserGroup.Location = new System.Drawing.Point(64, 37);
            this.cboGUserGroup.Name = "cboGUserGroup";
            this.cboGUserGroup.Size = new System.Drawing.Size(209, 20);
            this.cboGUserGroup.TabIndex = 86;
            // 
            // cboLUserGroup
            // 
            this.cboLUserGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLUserGroup.FormattingEnabled = true;
            this.cboLUserGroup.Location = new System.Drawing.Point(64, 12);
            this.cboLUserGroup.Name = "cboLUserGroup";
            this.cboLUserGroup.Size = new System.Drawing.Size(209, 20);
            this.cboLUserGroup.TabIndex = 85;
            // 
            // txtSabun
            // 
            this.txtSabun.Enabled = false;
            this.txtSabun.Location = new System.Drawing.Point(111, 440);
            this.txtSabun.MaxLength = 12;
            this.txtSabun.Name = "txtSabun";
            this.txtSabun.Size = new System.Drawing.Size(281, 21);
            this.txtSabun.TabIndex = 88;
            this.txtSabun.Visible = false;
            // 
            // gbUsed
            // 
            this.gbUsed.Controls.Add(this.rdUseYN_0);
            this.gbUsed.Controls.Add(this.rdUseYN_1);
            this.gbUsed.Location = new System.Drawing.Point(113, 341);
            this.gbUsed.Name = "gbUsed";
            this.gbUsed.Size = new System.Drawing.Size(281, 34);
            this.gbUsed.TabIndex = 87;
            this.gbUsed.TabStop = false;
            // 
            // rdUseYN_0
            // 
            this.rdUseYN_0.AutoSize = true;
            this.rdUseYN_0.Location = new System.Drawing.Point(19, 13);
            this.rdUseYN_0.Name = "rdUseYN_0";
            this.rdUseYN_0.Size = new System.Drawing.Size(71, 16);
            this.rdUseYN_0.TabIndex = 55;
            this.rdUseYN_0.Text = "사용안함";
            this.rdUseYN_0.UseVisualStyleBackColor = true;
            // 
            // rdUseYN_1
            // 
            this.rdUseYN_1.AutoSize = true;
            this.rdUseYN_1.Checked = true;
            this.rdUseYN_1.Location = new System.Drawing.Point(149, 13);
            this.rdUseYN_1.Name = "rdUseYN_1";
            this.rdUseYN_1.Size = new System.Drawing.Size(47, 16);
            this.rdUseYN_1.TabIndex = 54;
            this.rdUseYN_1.TabStop = true;
            this.rdUseYN_1.Text = "사용";
            this.rdUseYN_1.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(27, 353);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 12);
            this.label13.TabIndex = 86;
            this.label13.Text = "사용여부 :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(8, 289);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 12);
            this.label12.TabIndex = 84;
            this.label12.Text = "메뉴권한 :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbExtYN
            // 
            this.gbExtYN.Controls.Add(this.rdExtYN_0);
            this.gbExtYN.Controls.Add(this.rdExtYN_1);
            this.gbExtYN.Controls.Add(this.rdExtYN_2);
            this.gbExtYN.Location = new System.Drawing.Point(113, 240);
            this.gbExtYN.Name = "gbExtYN";
            this.gbExtYN.Size = new System.Drawing.Size(281, 34);
            this.gbExtYN.TabIndex = 83;
            this.gbExtYN.TabStop = false;
            // 
            // rdExtYN_0
            // 
            this.rdExtYN_0.AutoSize = true;
            this.rdExtYN_0.Location = new System.Drawing.Point(19, 13);
            this.rdExtYN_0.Name = "rdExtYN_0";
            this.rdExtYN_0.Size = new System.Drawing.Size(71, 16);
            this.rdExtYN_0.TabIndex = 55;
            this.rdExtYN_0.Text = "사용안함";
            this.rdExtYN_0.UseVisualStyleBackColor = true;
            // 
            // rdExtYN_1
            // 
            this.rdExtYN_1.AutoSize = true;
            this.rdExtYN_1.Location = new System.Drawing.Point(108, 13);
            this.rdExtYN_1.Name = "rdExtYN_1";
            this.rdExtYN_1.Size = new System.Drawing.Size(71, 16);
            this.rdExtYN_1.TabIndex = 54;
            this.rdExtYN_1.Text = "지역본부";
            this.rdExtYN_1.UseVisualStyleBackColor = true;
            // 
            // rdExtYN_2
            // 
            this.rdExtYN_2.AutoSize = true;
            this.rdExtYN_2.Location = new System.Drawing.Point(204, 13);
            this.rdExtYN_2.Name = "rdExtYN_2";
            this.rdExtYN_2.Size = new System.Drawing.Size(47, 16);
            this.rdExtYN_2.TabIndex = 57;
            this.rdExtYN_2.Text = "본사";
            this.rdExtYN_2.UseVisualStyleBackColor = true;
            // 
            // gbUserRight
            // 
            this.gbUserRight.Controls.Add(this.rdRight_2);
            this.gbUserRight.Controls.Add(this.rdRight_0);
            this.gbUserRight.Controls.Add(this.rdRight_1);
            this.gbUserRight.Controls.Add(this.rdRight_3);
            this.gbUserRight.Location = new System.Drawing.Point(113, 179);
            this.gbUserRight.Name = "gbUserRight";
            this.gbUserRight.Size = new System.Drawing.Size(281, 62);
            this.gbUserRight.TabIndex = 82;
            this.gbUserRight.TabStop = false;
            // 
            // rdRight_2
            // 
            this.rdRight_2.AutoSize = true;
            this.rdRight_2.Location = new System.Drawing.Point(19, 40);
            this.rdRight_2.Name = "rdRight_2";
            this.rdRight_2.Size = new System.Drawing.Size(83, 16);
            this.rdRight_2.TabIndex = 54;
            this.rdRight_2.Text = "본부관리자";
            this.rdRight_2.UseVisualStyleBackColor = true;
            // 
            // rdRight_0
            // 
            this.rdRight_0.AutoSize = true;
            this.rdRight_0.Location = new System.Drawing.Point(19, 14);
            this.rdRight_0.Name = "rdRight_0";
            this.rdRight_0.Size = new System.Drawing.Size(83, 16);
            this.rdRight_0.TabIndex = 38;
            this.rdRight_0.Text = "일반사용자";
            this.rdRight_0.UseVisualStyleBackColor = true;
            // 
            // rdRight_1
            // 
            this.rdRight_1.AutoSize = true;
            this.rdRight_1.Location = new System.Drawing.Point(146, 14);
            this.rdRight_1.Name = "rdRight_1";
            this.rdRight_1.Size = new System.Drawing.Size(83, 16);
            this.rdRight_1.TabIndex = 37;
            this.rdRight_1.Text = "센터관리자";
            this.rdRight_1.UseVisualStyleBackColor = true;
            // 
            // rdRight_3
            // 
            this.rdRight_3.AutoSize = true;
            this.rdRight_3.Location = new System.Drawing.Point(146, 40);
            this.rdRight_3.Name = "rdRight_3";
            this.rdRight_3.Size = new System.Drawing.Size(83, 16);
            this.rdRight_3.TabIndex = 53;
            this.rdRight_3.Text = "본사관리자";
            this.rdRight_3.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(27, 252);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 12);
            this.label11.TabIndex = 81;
            this.label11.Text = "타부서 조회 :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(157, 383);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 28);
            this.btnAdd.TabIndex = 80;
            this.btnAdd.Text = "추가";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // cboManageDept2
            // 
            this.cboManageDept2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboManageDept2.FormattingEnabled = true;
            this.cboManageDept2.Location = new System.Drawing.Point(114, 35);
            this.cboManageDept2.Name = "cboManageDept2";
            this.cboManageDept2.Size = new System.Drawing.Size(281, 20);
            this.cboManageDept2.TabIndex = 79;
            // 
            // cboHEADQUARTER2
            // 
            this.cboHEADQUARTER2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHEADQUARTER2.FormattingEnabled = true;
            this.cboHEADQUARTER2.Location = new System.Drawing.Point(114, 10);
            this.cboHEADQUARTER2.Name = "cboHEADQUARTER2";
            this.cboHEADQUARTER2.Size = new System.Drawing.Size(281, 20);
            this.cboHEADQUARTER2.TabIndex = 78;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(27, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 12);
            this.label10.TabIndex = 77;
            this.label10.Text = "관리센터 :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(27, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 12);
            this.label9.TabIndex = 76;
            this.label9.Text = "지역본부 :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(317, 383);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 28);
            this.btnDelete.TabIndex = 69;
            this.btnDelete.Text = "삭제";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(237, 383);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 68;
            this.btnSave.Text = "저장";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(27, 195);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 12);
            this.label7.TabIndex = 75;
            this.label7.Text = "사용자 권한 :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(27, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 12);
            this.label6.TabIndex = 74;
            this.label6.Text = "사용자 부서 :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUDivision
            // 
            this.txtUDivision.Location = new System.Drawing.Point(114, 158);
            this.txtUDivision.MaxLength = 32;
            this.txtUDivision.Name = "txtUDivision";
            this.txtUDivision.Size = new System.Drawing.Size(281, 21);
            this.txtUDivision.TabIndex = 67;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(27, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 12);
            this.label5.TabIndex = 73;
            this.label5.Text = "사용자명 :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUName
            // 
            this.txtUName.Location = new System.Drawing.Point(114, 60);
            this.txtUName.MaxLength = 32;
            this.txtUName.Name = "txtUName";
            this.txtUName.Size = new System.Drawing.Size(281, 21);
            this.txtUName.TabIndex = 66;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(18, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 72;
            this.label4.Text = "비밀번호확인 :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNCPWD
            // 
            this.txtNCPWD.Enabled = false;
            this.txtNCPWD.Location = new System.Drawing.Point(114, 133);
            this.txtNCPWD.MaxLength = 12;
            this.txtNCPWD.Name = "txtNCPWD";
            this.txtNCPWD.PasswordChar = '*';
            this.txtNCPWD.Size = new System.Drawing.Size(281, 21);
            this.txtNCPWD.TabIndex = 65;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(27, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 12);
            this.label2.TabIndex = 71;
            this.label2.Text = "사번 :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(27, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 12);
            this.label1.TabIndex = 70;
            this.label1.Text = "비밀번호 :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPWD
            // 
            this.txtPWD.BackColor = System.Drawing.Color.Ivory;
            this.txtPWD.Enabled = false;
            this.txtPWD.Location = new System.Drawing.Point(114, 109);
            this.txtPWD.MaxLength = 12;
            this.txtPWD.Name = "txtPWD";
            this.txtPWD.PasswordChar = '*';
            this.txtPWD.Size = new System.Drawing.Size(281, 21);
            this.txtPWD.TabIndex = 64;
            // 
            // txtUID
            // 
            this.txtUID.Enabled = false;
            this.txtUID.Location = new System.Drawing.Point(114, 85);
            this.txtUID.MaxLength = 12;
            this.txtUID.Name = "txtUID";
            this.txtUID.Size = new System.Drawing.Size(281, 21);
            this.txtUID.TabIndex = 63;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(586, 419);
            this.ultraTabControl1.TabIndex = 38;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "메뉴권한(지방)";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "메뉴권한(광역)";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(582, 393);
            // 
            // frmUserManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 679);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUserManager";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "사용자등록/삭제";
            this.Load += new System.EventHandler(this.frmUserManager_Load);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugLMenu)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugGMenu)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugData)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbUsed.ResumeLayout(false);
            this.gbUsed.PerformLayout();
            this.gbExtYN.ResumeLayout(false);
            this.gbExtYN.PerformLayout();
            this.gbUserRight.ResumeLayout(false);
            this.gbUserRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugData;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboManageDept;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboHEADQUARTER;
        private System.Windows.Forms.Button btnDelete2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugLMenu;
        private System.Windows.Forms.ComboBox cboLUserGroup;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox gbExtYN;
        private System.Windows.Forms.RadioButton rdExtYN_0;
        private System.Windows.Forms.RadioButton rdExtYN_1;
        private System.Windows.Forms.RadioButton rdExtYN_2;
        private System.Windows.Forms.GroupBox gbUserRight;
        private System.Windows.Forms.RadioButton rdRight_2;
        private System.Windows.Forms.RadioButton rdRight_0;
        private System.Windows.Forms.RadioButton rdRight_1;
        private System.Windows.Forms.RadioButton rdRight_3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox cboManageDept2;
        private System.Windows.Forms.ComboBox cboHEADQUARTER2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUDivision;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNCPWD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPWD;
        private System.Windows.Forms.TextBox txtUID;
        private System.Windows.Forms.GroupBox gbUsed;
        private System.Windows.Forms.RadioButton rdUseYN_0;
        private System.Windows.Forms.RadioButton rdUseYN_1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSabun;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkGlobal;
        private System.Windows.Forms.CheckBox chkLocal;
        private System.Windows.Forms.ComboBox cboGUserGroup;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugGMenu;
        private System.Windows.Forms.Button btnHrBank;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtimsipassword;
        private System.Windows.Forms.CheckBox chkAll;
    }
}