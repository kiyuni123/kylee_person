﻿using System;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Text.RegularExpressions;

namespace WS_UserManager.forms
{
    public partial class frmUserManager : EMFrame.form.BaseForm
    {
        public frmUserManager()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 조회 콤보박스 설정
        /// </summary>
        private void InitializeControlSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ROHQCD CD, ROHQNM || '(' || ROHQCD || ')' CDNM from REGIONAL_HEADQUARTER ORDER BY ROHQCD ASC");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), false);
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER2, oStringBuilder.ToString(), false);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("select USER_GROUP_ID CD, USER_GROUP_NAME || '(' || USER_GROUP_ID || ')' CDNM from CM_GROUP_MASTER WHERE ZONE_GBN = '지방'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboLUserGroup, oStringBuilder.ToString(), false);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("select USER_GROUP_ID CD, USER_GROUP_NAME || '(' || USER_GROUP_ID || ')' CDNM from CM_GROUP_MASTER WHERE ZONE_GBN = '광역'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboGUserGroup, oStringBuilder.ToString(), false);
        }

        /// <summary>
        /// 그리드 컬럼 설정
        /// </summary>
        private void InitializeGridColumnSetting()
        {
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "ROHQCD", "지역본부코드", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "ROHQNM", "지역본부", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MGDPCD", "센터코드", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MGDPNM", "관리센터", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_ID", "사용자ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_NAME", "사용자", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_PWD", "암호", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "SABUN", "사번", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_RIGHT", "권한", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "EXT_YN", "타부서조회", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "L_USER_GROUP_ID", "메뉴그룹(지방)", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "G_USER_GROUP_ID", "메뉴그룹(광역)", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                     Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USE_YN", "사용여부", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList,
                     Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_DIVISION", "부서", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "ZONE_GBN", "지방/광역", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
            //-------------------------------------------------------------------------------
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "MU_ID", "메뉴ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "MENU1", "1차메뉴", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "MENU2", "2차메뉴", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "MENU3", "3차메뉴", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "TOP_YN", "최상위메뉴", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "USE_YN", "사용여부", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "P_MU_ID", "상위메뉴", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "ORDERNO", "순서", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "MENU_AUTH", "사용권한", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "REMARK", "설명", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugLMenu, "LVL", "LVL", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                     Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugLMenu);
            this.ugLMenu.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
            //-------------------------------------------------------------------------------
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "MU_ID", "메뉴ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "MENU1", "1차메뉴", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "MENU2", "2차메뉴", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "MENU3", "3차메뉴", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "TOP_YN", "최상위메뉴", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "USE_YN", "사용여부", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "P_MU_ID", "상위메뉴", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "ORDERNO", "순서", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "MENU_AUTH", "사용권한", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "REMARK", "설명", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugGMenu, "LVL", "LVL", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                     Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugGMenu);
            this.ugGMenu.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;

        }

        private void InitializeValueListSetting()
        {
            object[,] aoSource = {
                {"0",  "일반사용자"},
                {"1",  "센터관리자"},
                {"2",  "본부관리자"},
                {"3",  "본사관리자"}
            };
            WS_Common.utils.gridUtils.SetValueList(this.ugData, "USER_RIGHT", aoSource);

            object[,] aoSource2 = {
                {"0",  "사용안함"},
                {"1",  "지역본부"},
                {"2",  "본사"}
            };
            WS_Common.utils.gridUtils.SetValueList(this.ugData, "EXT_YN", aoSource2);

            object[,] aoSource3 = {
                {"0",  "사용안함"},
                {"1",  "사용"}
            };
            WS_Common.utils.gridUtils.SetValueList(this.ugData, "USE_YN", aoSource3);

            object[,] aoSource4 = {
                {"0",  "편집허용"},
                {"1",  "조회허용"},
                {"2",  "사용안함"}
            };

            WS_Common.utils.gridUtils.SetValueList(this.ugLMenu, "MENU_AUTH", aoSource4);
            WS_Common.utils.gridUtils.SetValueList(this.ugGMenu, "MENU_AUTH", aoSource4);
        }

        /// <summary>
        /// 그리드 이벤트 설정
        /// </summary>
        private void InitializeControlEvents()
        {
            this.ugData.ClickCell +=new ClickCellEventHandler(ugData_ClickCell);
            this.btnSearch.Click +=new EventHandler(btnSearch_Click);
            this.btnSave.Click +=new EventHandler(btnSave_Click);
            this.btnDelete.Click +=new EventHandler(btnDelete_Click);
            this.btnDelete2.Click += new EventHandler(btnDelete_Click);
            this.btnAdd.Click +=new EventHandler(btnAdd_Click);

            this.cboHEADQUARTER.SelectedIndexChanged +=new EventHandler(cboHEADQUARTER_SelectedIndexChanged);
            this.cboHEADQUARTER2.SelectedIndexChanged += new EventHandler(cboHEADQUARTER2_SelectedIndexChanged);
            this.cboManageDept2.SelectedIndexChanged += new EventHandler(cboManageDept2_SelectedIndexChanged);

            this.chkGlobal.CheckedChanged += new EventHandler(checkbox_CheckedChanged);
            this.chkLocal.CheckedChanged += new EventHandler(checkbox_CheckedChanged);

            this.rdExtYN_0.CheckedChanged += new EventHandler(radiobutton_CheckedChanged);
            this.rdExtYN_1.CheckedChanged += new EventHandler(radiobutton_CheckedChanged);
            this.rdExtYN_2.CheckedChanged += new EventHandler(radiobutton_CheckedChanged);

            this.cboLUserGroup.SelectedIndexChanged += new EventHandler(cboUserGroup_SelectedIndexChanged);
            this.cboGUserGroup.SelectedIndexChanged += new EventHandler(cboUserGroup_SelectedIndexChanged);
        }

        /// <summary>
        /// 타부서조회권한(사용안함,지역본부,본사)에 따른 메뉴권한설정 변경
        /// 사용안함 : 해당지역(광역,지방)만 설정가능하도록 변경
        /// 관리센터 : 선택시 해당 관리센터의 ZONE_GBN 값 = ZONE_GBN;
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radiobutton_CheckedChanged(object sender, EventArgs e)
        {
            if (((string)this.ZONE_GBN).Equals("지방"))
            {
                switch (((RadioButton)sender).Name)
                {
                    case "rdExtYN_0":
                        chkLocal.Enabled = true; chkGlobal.Enabled = false;
                        cboLUserGroup.Enabled = true; cboGUserGroup.Enabled = false;
                        break;
                    case "rdExtYN_1":
                    case "rdExtYN_2":
                        chkLocal.Enabled = true; chkGlobal.Enabled = true;
                        cboLUserGroup.Enabled = true; cboGUserGroup.Enabled = true;
                        break;
                }
            }
            else
            {
                switch (((RadioButton)sender).Name)
                {
                    case "rdExtYN_0":
                        chkLocal.Enabled = false; chkGlobal.Enabled = true;
                        cboLUserGroup.Enabled = false; cboGUserGroup.Enabled = true;
                        break;
                    case "rdExtYN_1":
                    case "rdExtYN_2":
                        chkLocal.Enabled = true; chkGlobal.Enabled = true;
                        cboLUserGroup.Enabled = true; cboGUserGroup.Enabled = true;
                        break;
                }
            }
        }

        /// <summary>
        /// 광역/지방 메뉴권한 체크박스 선택시 메뉴설정탭 보이기/숨기기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkbox_CheckedChanged(object sender, EventArgs e)
        {
            switch (((CheckBox)sender).Name)
	        {
                case "chkLocal":
                    this.ultraTabControl1.Tabs[0].Visible = ((CheckBox)sender).Checked;
                    this.ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[0].Visible ? ultraTabControl1.Tabs[0] : ultraTabControl1.Tabs[1];
                    this.cboLUserGroup.Enabled = ((CheckBox)sender).Checked;
                    if (this.cboLUserGroup.Enabled && this.cboLUserGroup.Items.Count > 0 && this.cboLUserGroup.SelectedItem == null)
                        this.cboLUserGroup.SelectedIndex = 0;
                    break;
                case "chkGlobal":
                    this.ultraTabControl1.Tabs[1].Visible = ((CheckBox)sender).Checked;
                    this.ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1].Visible ? ultraTabControl1.Tabs[1] : ultraTabControl1.Tabs[0];
                    this.cboGUserGroup.Enabled = ((CheckBox)sender).Checked;
                    if (this.cboGUserGroup.Enabled && this.cboGUserGroup.Items.Count > 0 && this.cboGUserGroup.SelectedItem == null)
                        this.cboGUserGroup.SelectedIndex = 0;
                    break;
	        } 
            
        }

        private object ZONE_GBN = EMFrame.statics.AppStatic.ZONE_GBN;
        /// <summary>
        /// 관리센터2 선택
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboManageDept2_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ZONE_GBN from MANAGEMENT_DEPARTMENT WHERE MGDPCD = '" + this.cboManageDept2.SelectedValue + "'");
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                this.ZONE_GBN = mapper.ExecuteScriptScalar(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

        }

        /// <summary>
        /// 사용자그룹 콤보박스 선택시 해당 그룹의 메뉴정보를 표시한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboUserGroup_SelectedIndexChanged(object sender, EventArgs e)
        {   
            System.Collections.Hashtable param = new System.Collections.Hashtable();

            switch (((ComboBox)sender).Name)
            {
                case "cboLUserGroup":  //지방
                    param.Add("ZONE_GBN", "지방");
                    param.Add("USER_GROUP_ID", this.cboLUserGroup.SelectedValue);
                    this.ugLMenu.DataSource = WS_MenuManager.work.Menuwork.GetInstance().GetMenuMasterData(param);
                    WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugLMenu);
                    break;
                case "cboGUserGroup":  //광역
                    param.Add("ZONE_GBN", "광역");
                    param.Add("USER_GROUP_ID", this.cboGUserGroup.SelectedValue);
                    this.ugGMenu.DataSource = WS_MenuManager.work.Menuwork.GetInstance().GetMenuMasterData(param);
                    WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugGMenu);
                    break;
            }
        }

        /// <summary>
        /// 지역본부 선택시 해당 지방센터를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboHEADQUARTER_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept, oStringBuilder.ToString(), true);
        }

        private void cboHEADQUARTER2_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER2.SelectedValue + "'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept2, oStringBuilder.ToString(), false);
        }

   

        private void frmUserManager_Load(object sender, EventArgs e)
        {
            this.InitializeGridColumnSetting();
            this.InitializeValueListSetting();
            this.InitializeControlSetting();
            this.InitializeControlEvents();

            cboHEADQUARTER_SelectedIndexChanged(this, new EventArgs());
            cboHEADQUARTER2_SelectedIndexChanged(this, new EventArgs());

            if (EMFrame.statics.AppStatic.USER_RIGHT.Equals("1"))
            {
                this.chkAll.Enabled = false;
            }
            this.LoginUser_ControlSetting();
            this.GetUserData();

        }

        /// <summary>
        /// 로그인사용자의 권한에 따른 화면 컨트롤 재설정
        /// </summary>
        private void LoginUser_ControlSetting()
        {
            switch (EMFrame.statics.AppStatic.USER_RIGHT)
            {
                case "0":
                    break;
                case "1":  //센터관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.cboManageDept.Enabled = false;
                    this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;
                    this.cboHEADQUARTER2.Enabled = false;
                    this.cboHEADQUARTER2.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.cboManageDept2.Enabled = false;
                    this.cboManageDept2.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;
                    this.rdExtYN_0.Enabled = false; this.rdExtYN_1.Enabled = false; this.rdExtYN_2.Enabled = false;
                    this.rdRight_0.Enabled = true; this.rdRight_1.Enabled = true; this.rdRight_2.Enabled = false; this.rdRight_3.Enabled = false;
                    break;
                case "2":  //본부관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.cboHEADQUARTER2.Enabled = false;
                    this.cboHEADQUARTER2.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.rdExtYN_0.Enabled = true; this.rdExtYN_1.Enabled = true; this.rdExtYN_2.Enabled = false;
                    this.rdRight_0.Enabled = true; this.rdRight_1.Enabled = true; this.rdRight_2.Enabled = true; this.rdRight_3.Enabled = false;
                    break;
                case "3":  //본사관리자
                    break;
            }
        }

        #region Button Events

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.ugData.Selected.Rows.Count == 0)
            {
                MessageBox.Show("사용자가 선택되지 않았습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (!chkGlobal.Checked && !chkLocal.Checked)
            {
                MessageBox.Show("메뉴권한(지방/광역)은 최소한 1개이상은 선택되어야 합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            
            //if (txtPWD.Enabled) //사용자추가
            if (this.txtimsipassword.Text.Equals("추가")) //사용자추가
            {
                if (this.IsDataValid(0) != true) return;
                if (this.IsDataValid(2) != true) return;
                if (this.IsDataValid(3) != true) return;
                if (this.IsDataValid(4) != true) return;
                if (this.IsDataValid(5) != true) return;

                this.SaveUser(true);
                this.GetUserData();
            }
            else               //사용자수정
            {
                if (this.IsDataValid(0) != true) return;                
                if (this.IsDataValid(3) != true) return;
                if (this.IsDataValid(4) != true) return;                

                this.SaveUser(false);
            }
            
        }
        
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.ugData.Selected.Rows.Count == 0)
            {
                MessageBox.Show("사용자가 선택되지 않았습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            DialogResult oDRtn = MessageBox.Show("선택한 사용자(" + this.ugData.ActiveRow.Cells["USER_ID"].Text + ")를 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oDRtn == DialogResult.Yes)
            {
                this.DeleteUser();
                MessageBox.Show("선택한 사용자를 삭제했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            this.GetUserData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.txtimsipassword.Text = "추가";

            this.txtUID.Text = string.Empty; this.txtUName.Text = string.Empty; this.txtUDivision.Text = string.Empty;
            this.txtPWD.Text = string.Empty; this.txtNCPWD.Text = string.Empty; this.txtSabun.Text = string.Empty;
            this.isEditableControl(true);
        }

        private void btnHrBank_Click(object sender, EventArgs e)
        {
            DialogResult oDRtn = MessageBox.Show("HRBANK 사용자정보를 동기화합니다.\n\r3 ~ 5분정도의 시간이 소요됩니다. 계속 진행하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (oDRtn != DialogResult.Yes) return;

            DataTable hrbank = work.usermanagerwork.GetInstance().SelectHrbankData();
            if (hrbank == null || hrbank.Rows.Count == 0) return;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                System.Collections.ArrayList hrbankdata = new System.Collections.ArrayList();
                foreach (DataRow row in hrbank.Rows)
                {
                    string strEncUPWD = EMFrame.utils.ConvertUtils.AESEncrypt256(Convert.ToString(row["KORNAME"]).Trim() + Convert.ToString(row["EMPNO"]).Trim());

                    //Console.WriteLine(strEncUPWD + " : " + Convert.ToString(row["KORNAME"]).Trim() + " , " + Convert.ToString(row["EMPNO"]).Trim());
                    System.Collections.Hashtable param = new System.Collections.Hashtable();
                    param.Add("USER_ID", row["EMPNO"]);
                    param.Add("USER_NAME", row["KORNAME"]);
                    param.Add("USER_PWD", strEncUPWD);
                    param.Add("MGDPCD", row["MGDPCD"]);
                    param.Add("SABUN", row["EMPNO"]);
                    param.Add("USER_DIVISION", row["USER_DIVISION"]);
                    param.Add("REG_UID", EMFrame.statics.AppStatic.USER_ID);

                    hrbankdata.Add(param);
                }

                work.usermanagerwork.GetInstance().SaveHrBankUserData(hrbankdata);

                MessageBox.Show("사용자정보 동기화가 완료되었습니다.");
            }
            catch { }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// 조회버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            if (chkAll.Checked)
                this.GetUserData2();
            else
                this.GetUserData();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #region Grid Events
        private void ugData_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (this.ugData.Rows.Count <= 0) return;
            if (e.Cell.Row == null) return;

            UltraGridRow row = e.Cell.Row;

            this.cboHEADQUARTER2.SelectedValue = this.ugData.ActiveRow.Cells["ROHQCD"].Value;
            this.cboManageDept2.SelectedValue = this.ugData.ActiveRow.Cells["MGDPCD"].Value;
            this.txtUID.Text = Convert.ToString(this.ugData.ActiveRow.Cells["USER_ID"].Value);
            this.txtPWD.Text = Convert.ToString(this.ugData.ActiveRow.Cells["USER_PWD"].Value);
            this.txtNCPWD.Text = Convert.ToString(this.ugData.ActiveRow.Cells["USER_PWD"].Value);
            this.txtUName.Text = Convert.ToString(this.ugData.ActiveRow.Cells["USER_NAME"].Value);
            this.txtSabun.Text = Convert.ToString(this.ugData.ActiveRow.Cells["SABUN"].Value);
            this.txtUDivision.Text = Convert.ToString(this.ugData.ActiveRow.Cells["USER_DIVISION"].Value);
            this.ZONE_GBN = Convert.ToString(this.ugData.ActiveRow.Cells["ZONE_GBN"].Value);

            ///본사,본부관리자이면서, 등록되지 않은사용자일경우
            if ((new string[] { "2", "3" }).Contains(EMFrame.statics.AppStatic.USER_RIGHT))
            {
                if (this.cboHEADQUARTER2.SelectedValue == null)
                {
                    this.cboHEADQUARTER2.Enabled = true;
                }
                if (this.cboManageDept2.SelectedValue == null)
                {
                    this.cboManageDept2.Enabled = true;
                }
            }

            if (Convert.IsDBNull(this.ugData.ActiveRow.Cells["L_USER_GROUP_ID"].Value))
                chkLocal.Checked = false;
            else
                chkLocal.Checked = true;
            checkbox_CheckedChanged(this.chkLocal, new EventArgs());

            if (Convert.IsDBNull(this.ugData.ActiveRow.Cells["G_USER_GROUP_ID"].Value))
                chkGlobal.Checked = false;
            else
                chkGlobal.Checked = true;
            checkbox_CheckedChanged(this.chkGlobal, new EventArgs());


            this.cboLUserGroup.SelectedValue = this.ugData.ActiveRow.Cells["L_USER_GROUP_ID"].Value;
            this.cboGUserGroup.SelectedValue = this.ugData.ActiveRow.Cells["G_USER_GROUP_ID"].Value;
            cboUserGroup_SelectedIndexChanged(cboLUserGroup, new EventArgs());
            cboUserGroup_SelectedIndexChanged(cboGUserGroup, new EventArgs());

            switch (Convert.ToString(this.ugData.ActiveRow.Cells["USER_RIGHT"].Value))
            {
                case "0":
                    this.rdRight_0.Checked = true;
                    break;
                case "1":
                    this.rdRight_1.Checked = true;
                    break;
                case "2":
                    this.rdRight_2.Checked = true;
                    break;
                case "3":
                    this.rdRight_3.Checked = true;
                    break;
            }

            switch (Convert.ToString(this.ugData.ActiveRow.Cells["EXT_YN"].Value))
            {
                case "0":
                    this.rdExtYN_0.Checked = true;
                    break;
                case "1":
                    this.rdExtYN_1.Checked = true;
                    break;
                case "2":
                    this.rdExtYN_2.Checked = true;
                    break;
            }

            switch (Convert.ToString(this.ugData.ActiveRow.Cells["USE_YN"].Value))
            {
                case "0":
                    this.rdUseYN_0.Checked = true;
                    break;
                case "1":
                    this.rdUseYN_1.Checked = true;
                    break;
            }

            //현재 암호화된 패스워드를 임시로 저장한다.
            this.txtimsipassword.Text = Convert.ToString(this.ugData.ActiveRow.Cells["USER_PWD"].Value);
            this.txtUID.Enabled = false;
            this.txtUName.Enabled = false;
            this.txtPWD.Enabled = true;
            this.txtNCPWD.Enabled = true;
        }
        #endregion Grid Events

        #region Control Events



        private void txtUID_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtPWD.Focus();
            }
        }

        private void txtPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtNCPWD.Focus();
            }
        }

        private void txtNCPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtUName.Focus();
            }
        }

        private void txtUName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtUDivision.Focus();
            }
        }

        private void txtUDivision_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.rdRight_0.Focus();
            }
        }

        #endregion

        
        #region User Function

        /// <summary>
        /// User Data를 Select해서 그리드에 Set
        /// </summary>
        private void GetUserData()
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("ROHQCD", (string)this.cboHEADQUARTER.SelectedValue);
            param.Add("MGDPCD", (string)this.cboManageDept.SelectedValue);
            param.Add("USER_NAME", (string)this.txtName.Text);

            this.ugData.DataSource = WS_Common.work.Commonwork.GetInstance().GetUserInfoData(param);
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
        }

        private void GetUserData2()
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_NAME", (string)this.txtName.Text);

            this.ugData.DataSource = WS_Common.work.Commonwork.GetInstance().GetUserInfoData2(param);
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
        }
        /// <summary>
        /// User Data의 적합성 검증
        /// </summary>
        /// <param name="iType">0 : User ID, 1 : New Password, 2 : New Confirm Password</param>
        /// 3 : User Name, 4 : User Division</param>
        /// <returns></returns>
        /// 
        private bool IsDataValid(int iType)
        {
            bool blRtn = true;

            switch (iType)
            {
                case 0:
                    if (string.IsNullOrEmpty(this.txtUID.Text.Trim()) || this.txtUID.Text.Trim().Length > 12)
                    {
                        blRtn = false;
                        this.txtUID.Text = "";
                        this.txtUID.Focus();
                        MessageBox.Show("사용자 ID를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    //if (this.IsUserIDExists() == true)
                    //{
                    //    blRtn = false;
                    //    this.txtUID.Text = "";
                    //    this.txtUID.Focus();
                    //    MessageBox.Show("이미 사용중인 사용자 ID입니다. 다른 사용자 ID를 입력하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //}
                    break;
                case 1:
                    //if ((this.txtPWD.Text.Trim().Equals(this.txtUID.Text.Trim())) || (this.txtPWD.Text.Trim().Length > 12))
                    if ((this.txtPWD.Text.Trim().Length < 9))
                    {
                        blRtn = false;
                        this.txtPWD.Text = "";
                        this.txtPWD.Focus();
                        MessageBox.Show("등록할 비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:
                    //if ((this.txtNCPWD.Text.Trim() == this.txtUID.Text.Trim()) || (this.txtNCPWD.Text.Trim() != this.txtPWD.Text.Trim()) || (this.txtNCPWD.Text.Trim().Length > 12))
                    if ((this.txtNCPWD.Text.Trim() != this.txtPWD.Text.Trim()))
                    {
                        blRtn = false;
                        this.txtNCPWD.Text = "";
                        this.txtNCPWD.Focus();
                        MessageBox.Show("확인 비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:
                    if ((this.txtUName.Text.Trim().Length <= 0) || (this.txtUName.Text.Trim().Length > 32))
                    {
                        blRtn = false;
                        this.txtUName.Text = "";
                        this.txtUName.Focus();
                        MessageBox.Show("사용자 성명을 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 4:
                    if ((this.txtUDivision.Text.Trim().Length <= 0) || (this.txtUDivision.Text.Trim().Length > 100))
                    {
                        blRtn = false;
                        this.txtUDivision.Text = "";
                        this.txtUDivision.Focus();
                        MessageBox.Show("사용자 부서를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 5:
                    if (Regex.IsMatch(this.txtPWD.Text, @"^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{9,16}$") == false)
                    {
                        blRtn = false;
                        this.txtPWD.Text = "";
                        this.txtPWD.Focus();
                        MessageBox.Show("비밀번호는 문자, 숫자, 특수문자를 포함한 9자리 이상이어야 합니다..", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
            return blRtn;
        }

        ///// <summary>
        ///// 입력한 User ID의 사용여부
        ///// </summary>
        ///// <returns></returns>
        //private bool IsUserIDExists()
        //{
        //    bool blRtn = false;

        //    EMFrame.dm.EMapper mapper = new EMFrame.dm.EMapper("SVR");

        //    if (mapper != null)
        //    {
        //        StringBuilder oStringBuilder = new StringBuilder();

        //        DataSet pDS = new DataSet();

        //        oStringBuilder.Remove(0, oStringBuilder.Length);
        //        oStringBuilder.AppendLine("SELECT   USER_ID");
        //        oStringBuilder.AppendLine("FROM     CM_USER");
        //        oStringBuilder.AppendLine("WHERE    USER_ID = '" + this.txtUID.Text.Trim() + "'");

        //        pDS = mapper.ExecuteScriptDataSet(oStringBuilder.ToString(), new IDataParameter[] {}, "CM_USER");

        //        if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
        //        {
        //            foreach (DataRow oDRow in pDS.Tables[0].Rows)
        //            {
        //                blRtn = true;
        //            }
        //        }
        //        else
        //        {
        //            blRtn = false;
        //        }
        //    }
        //    else
        //    {
        //        blRtn = false;
        //    }

        //    mapper.Close();

        //    return blRtn;
        //}

        /// <summary>
        /// 사용자를 Delete한다.
        /// </summary>
        private void DeleteUser()
        {
            EMFrame.dm.EMapper mapper = new EMFrame.dm.EMapper("SVR");

            if (mapper == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   CM_USER");
            oStringBuilder.AppendLine("WHERE    USER_ID = '" + this.txtUID.Text.Trim() + "'");

            try
            {
                mapper.ExecuteScript(oStringBuilder.ToString(), new IDataParameter[] { });
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                mapper.Close();
            }
            
        }

        /// <summary>
        /// 사용자를 Save한다.
        /// </summary>
        private void SaveUser(bool IsAdd)
        {
            string strURight = string.Empty;
            string strUExtYN = string.Empty;
            string strUUseYN = string.Empty;
            if (rdRight_0.Checked) strURight = "0";
            else if (rdRight_1.Checked) strURight = "1";
            else if (rdRight_2.Checked) strURight = "2";
            else if (rdRight_3.Checked) strURight = "3";
            else strURight = "0";

            if (this.rdExtYN_0.Checked) strUExtYN = "0";
            else if (this.rdExtYN_1.Checked) strUExtYN = "1";
            else if (this.rdExtYN_2.Checked) strUExtYN = "2";
            else strUExtYN = "0";

            if (this.rdUseYN_0.Checked) strUUseYN = "0";
            else if (this.rdUseYN_1.Checked) strUUseYN = "1";
            else strUUseYN = "1";

            string strEncUPWD = string.Empty;
            if (IsAdd)
                strEncUPWD = EMFrame.utils.ConvertUtils.AESEncrypt256(this.txtUName.Text.Trim() + this.txtPWD.Text.Trim());
            else
            {
                if (this.txtimsipassword.Text.Equals(this.txtPWD.Text))
                    strEncUPWD = this.txtPWD.Text.Trim();
                else
                    strEncUPWD = EMFrame.utils.ConvertUtils.AESEncrypt256(this.txtUName.Text.Trim() + this.txtPWD.Text.Trim());
            }
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_ID", this.txtUID.Text.Trim());
            param.Add("USER_NAME", this.txtUName.Text.Trim());
            param.Add("USER_PWD", strEncUPWD);
            param.Add("MGDPCD", this.cboManageDept2.SelectedValue);
            param.Add("USER_DIVISION", this.txtUDivision.Text.Trim());
            param.Add("USER_RIGHT", strURight);
            param.Add("SABUN", this.txtSabun.Text.Trim());
            param.Add("EXT_YN", strUExtYN);
            param.Add("USE_YN", strUUseYN);
            param.Add("L_USER_GROUP_ID", chkLocal.Checked ? this.cboLUserGroup.SelectedValue : string.Empty);
            param.Add("G_USER_GROUP_ID", chkGlobal.Checked ? this.cboGUserGroup.SelectedValue : string.Empty);
            param.Add("REG_UID", EMFrame.statics.AppStatic.USER_ID);

            //param.Add("ZONE_GBN", this.rbLocal.Checked ? "지방" : "광역");

            work.usermanagerwork.GetInstance().SaveUserInfoData(param);
            this.isEditableControl(false);

            if (!IsAdd) //수정
            {
                this.ugData.ActiveRow.Cells["USER_ID"].Value = param["USER_ID"];
                this.ugData.ActiveRow.Cells["USER_NAME"].Value = param["USER_NAME"];
                this.ugData.ActiveRow.Cells["USER_PWD"].Value = param["USER_PWD"];
                this.ugData.ActiveRow.Cells["MGDPCD"].Value = param["MGDPCD"];
                this.ugData.ActiveRow.Cells["USER_DIVISION"].Value = param["USER_DIVISION"];
                this.ugData.ActiveRow.Cells["USER_RIGHT"].Value = param["USER_RIGHT"];
                this.ugData.ActiveRow.Cells["SABUN"].Value = param["SABUN"];
                this.ugData.ActiveRow.Cells["EXT_YN"].Value = param["EXT_YN"];
                this.ugData.ActiveRow.Cells["USE_YN"].Value = param["USE_YN"];
                this.ugData.ActiveRow.Cells["L_USER_GROUP_ID"].Value = param["L_USER_GROUP_ID"];
                this.ugData.ActiveRow.Cells["G_USER_GROUP_ID"].Value = param["G_USER_GROUP_ID"];
            }

            MessageBox.Show("입력한 사용자를 저장했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void isEditableControl(bool edit)
        {
            //사용자권한 : 0:일반, 1:센터, 2:본부, 3:본사
            switch (Convert.ToInt32(EMFrame.statics.AppStatic.USER_RIGHT)) 
            {
                case 0:  //해당없음(서버사용권한 없음)
                    break;
                case 1:
                    this.cboHEADQUARTER2.Enabled = false;
                    this.cboManageDept2.Enabled = false;
                    break;
                case 2:
                    this.cboHEADQUARTER2.Enabled = false;
                    this.cboManageDept2.Enabled = true;
                    break;
                case 3:
                    this.cboHEADQUARTER2.Enabled = edit;
                    this.cboManageDept2.Enabled = true;
                    break;
            }

            //this.txtSabun.Enabled = edit;
            this.txtUID.Enabled = edit;
            this.txtPWD.Enabled = edit;
            this.txtNCPWD.Enabled = edit;
            this.txtUName.Enabled = edit;
        }

        #endregion



    }
}
