﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;

namespace WS_UserManager.forms
{
    public partial class frmUserSync : EMFrame.form.BaseForm
    {
        public frmUserSync()
        {
            InitializeComponent();
        }

        private void frmUserSync_Load(object sender, EventArgs e)
        {
            this.InitializeGridColumnSetting();
            this.InitializeValueListSetting();            
            this.InitializeControlEvents();
            this.ud_start.Value = DateTime.Now.AddMonths(-3);
            this.ud_end.Value = DateTime.Now;
        }

        /// <summary>
        /// 그리드 설정
        /// </summary>
        private void InitializeGridColumnSetting()
        {
            #region 컬럼추가 내용
            UltraGridColumn column;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SELECTED";
            column.Header.Caption = "";
            column.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            column.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.None;
            column.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;

            column.SortIndicator = SortIndicator.Disabled;
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 30;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "COM";
            column.Header.Caption = "비교";            
            column.Hidden = true;

            //column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            //column.Key = "W_CODE";
            //column.Header.Caption = "water-NET코드";
            //column.Hidden = true;

            //column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            //column.Key = "H_CODE";
            //column.Header.Caption = "HR_BANK코드";
            //column.Hidden = true;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "USER_ID";
            column.Header.Caption = "사용자ID";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "USER_NAME";
            column.Header.Caption = "사용자명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "W_MGDPCD";
            column.Header.Caption = "water-NET센터코드";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 50;
            column.Hidden = false;
            
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MGDPNM";
            column.Header.Caption = "관리센터";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H_MGDPCD";
            column.Header.Caption = "HR_BANK센터코드";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "HEAD";
            column.Header.Caption = "HEAD";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DEPT";
            column.Header.Caption = "DEPT";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SECT";
            column.Header.Caption = "SECT";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TEAM";
            column.Header.Caption = "TEAM";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;
                        
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "W_CODE";
            column.Header.Caption = "water-NET코드";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 50;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "W_NAME";
            column.Header.Caption = "water-NET관리단";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H_CODE";
            column.Header.Caption = "HR_BANK코드";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 50;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "H_NAME";
            column.Header.Caption = "HR_BANK관리단";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 150;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "KIND";
            column.Header.Caption = "지방광역구분";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 50;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ALT_DATE";
            column.Header.Caption = "변경일자";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 50;
            column.Hidden = false;
            #endregion
        }

        /// <summary>
        /// 컨트롤 셋팅
        /// </summary>
        private void InitializeValueListSetting()
        {
            object[,] aoSource = {
                {"0",  "선택"},
                {"L",  "지방"},
                {"G",  "광역"}
            };
            WS_Common.utils.gridUtils.SetValueList(this.ultraGrid1, "KIND", aoSource);
        }

        /// <summary>
        /// 이벤트 설정
        /// </summary>
        private void InitializeControlEvents()
        {
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);            
            this.checkBox1.CheckedChanged += new EventHandler(checkBox1_CheckedChanged);
            this.checkBox2.CheckedChanged += new EventHandler(checkBox2_CheckedChanged);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.ultraGrid1.AfterHeaderCheckStateChanged += new AfterHeaderCheckStateChangedEventHandler(ultraGrid1_AfterHeaderCheckStateChanged);
            //this.ultraGrid1.AfterCellListCloseUp +=new CellEventHandler(ultraGrid1_AfterCellListCloseUp);
            this.ultraGrid1.AfterCellUpdate += new CellEventHandler(ultraGrid1_AfterCellUpdate);
            this.user_id.KeyPress += new KeyPressEventHandler(user_id_KeyPress);
            this.user_name.KeyPress += new KeyPressEventHandler(user_name_KeyPress);
        }

       

        private void user_name_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)13)
            {
                this.SearchUser();
            }
        }

        private void user_id_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)13)
            {
                this.SearchUser();
            }
        }

        
        private void ultraGrid1_AfterCellUpdate(object sender, CellEventArgs e)
        {            
            
            Hashtable param = new Hashtable();
            
            param.Add("KIND", e.Cell.Value.ToString());
            param.Add("CODE", e.Cell.Row.Cells["H_CODE"].Value.ToString());

            object kind = work.usermanagerwork.GetInstance().GetKind(param);
            object kindname = work.usermanagerwork.GetInstance().GetKindName(param);

            if (kind == null)
            {
                if (!"".Equals(e.Cell.Row.Cells["H_MGDPCD"].Value.ToString()))
                {
                    e.Cell.Row.Cells["H_MGDPCD"].Appearance.BackColor = Color.PeachPuff;
                    e.Cell.Row.Cells["MGDPNM"].Appearance.BackColor = Color.PeachPuff;
                }
            }
            else
            {
                if (!kind.Equals(e.Cell.Row.Cells["H_MGDPCD"].Value))
                {
                    e.Cell.Row.Cells["H_MGDPCD"].Value = kind;
                    e.Cell.Row.Cells["H_MGDPCD"].Appearance.BackColor = Color.PeachPuff;
                    e.Cell.Row.Cells["MGDPNM"].Value = kindname;
                    e.Cell.Row.Cells["MGDPNM"].Appearance.BackColor = Color.PeachPuff;
                }
            }
            e.Cell.Refresh();
        }

        
        /// <summary>
        /// 그리드 DropDownList 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void ultraGrid1_AfterCellListCloseUp(object sender, CellEventArgs e)
        //{
        //    Hashtable param = new Hashtable();

        //    param.Add("KIND", e.Cell.Value.ToString());
        //    param.Add("CODE", e.Cell.Row.Cells["H_CODE"].Value.ToString());

        //    object kind = work.usermanagerwork.GetInstance().GetKind(param);

        //    if (!kind.Equals(e.Cell.Row.Cells["H_MGDPCD"].Value))
        //    {
        //        e.Cell.Row.Cells["H_MGDPCD"].Value = kind;
        //        e.Cell.Row.Cells["H_MGDPCD"].Appearance.BackColor = Color.PeachPuff;
        //    }            
        //}
        
        /// <summary>
        /// 검색(사용자, CM_USER, HR_BANK 조회)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
                
        private void searchBtn_Click(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            
            this.SearchUser();
            
            this.Cursor = System.Windows.Forms.Cursors.Default;

            Cursor currentCursor = this.Cursor;
        }
        
        ///// <summary>
        ///// 버튼 선택 시 값이 그리드 값이 변경 된다.
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void saveBtn_Click(object sender, EventArgs e)
        //{
       
        //    if (this.ultraGrid1.ActiveRow == null)
        //    {
        //        return;
        //    }

        //    DialogResult qe = MessageBox.Show("선택항목을 저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        //    if (qe == DialogResult.Yes)
        //    {
        //        Cursor currentCursor = this.Cursor;                
        //    }                
       
        //}

        

        /// <summary>
        /// DB에 적용
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow == null)
            {
                return;
            }

            DialogResult qe = MessageBox.Show("선택항목을 저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                Cursor currentCursor = this.Cursor;

                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    this.UpdateUser();
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
                finally
                {
                    this.Cursor = currentCursor;
                }
            }
        }

        private void UpdateUser()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                System.Collections.ArrayList hrbankdata = new System.Collections.ArrayList();

                foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
                {
                    if (Convert.ToBoolean(gridRow.Cells["SELECTED"].Value))
                    {
                        Hashtable parameter = new Hashtable();

                        string strEncUPWD = EMFrame.utils.ConvertUtils.AESEncrypt256(gridRow.Cells["USER_NAME"].Value.ToString().Trim() + gridRow.Cells["USER_ID"].Value.ToString().Trim());
                        string division = gridRow.Cells["HEAD"].Value.ToString() + "->" + gridRow.Cells["DEPT"].Value.ToString() + "->" + gridRow.Cells["SECT"].Value.ToString() + "->" + gridRow.Cells["TEAM"].Value.ToString();

                        parameter.Add("USER_ID", gridRow.Cells["USER_ID"].Value.ToString());
                        parameter.Add("USER_NAME", gridRow.Cells["USER_NAME"].Value.ToString());
                        parameter.Add("USER_PWD", strEncUPWD);
                        if ("".Equals(gridRow.Cells["H_MGDPCD"].Value.ToString()))
                        {
                            if (!"".Equals(gridRow.Cells["W_MGDPCD"].Value.ToString()))
                                parameter.Add("MGDPCD", gridRow.Cells["W_MGDPCD"].Value.ToString());
                        }
                        else                        
                            parameter.Add("MGDPCD", gridRow.Cells["H_MGDPCD"].Value.ToString());
                        parameter.Add("SABUN", gridRow.Cells["USER_ID"].Value.ToString());
                        parameter.Add("USER_DIVISION", division);
                        parameter.Add("CODE", gridRow.Cells["H_CODE"].Value.ToString());
                        parameter.Add("REG_UID", EMFrame.statics.AppStatic.USER_ID);
                        
                        hrbankdata.Add(parameter);                                               
                    }
                }
                work.usermanagerwork.GetInstance().UpdateUser(hrbankdata);
                MessageBox.Show("사용자정보 동기화가 완료되었습니다.");
            }             
            catch { }
            finally
            {
                this.Cursor = Cursors.Default;
                this.SearchUser();
            }
        }

        /// <summary>
        /// 체크박스 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                row.Hidden = false;
                
                if (checkBox1.Checked && row.Cells["COM"].Value.ToString() == "0")
                {
                    row.Hidden = true;
                }
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                this.label3.Visible = true;
                this.label4.Visible = true;
                this.ud_start.Visible = true;
                this.ud_end.Visible = true;
                this.ud_start.Value = DateTime.Now.AddMonths(-3);
                this.ud_end.Value = DateTime.Now;
                
            }
            else
            {
                this.label3.Visible = false;
                this.label4.Visible = false;
                this.ud_start.Visible = false;
                this.ud_end.Visible = false;
                this.ud_start.Value = null;
                this.ud_end.Value = null;
            }
        }

        private void SearchUser()
        {
            Hashtable param = new Hashtable();
            param.Add("ID", this.user_id.Text);
            param.Add("NAME", this.user_name.Text);
            if (this.ud_start.Value != null)
            {
                param["STARTDATE"] = ((DateTime)this.ud_start.Value).ToString("yyyyMMdd");
                param["ENDDATE"] = ((DateTime)this.ud_end.Value).ToString("yyyyMMdd");
            }            
            this.ultraGrid1.DataSource = work.usermanagerwork.GetInstance().SearchUser(param);
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ultraGrid1);
            this.SetGridColumn();            
        }

        private void SetGridColumn()
        {
            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                if (row.Cells["W_CODE"].Value.ToString() != row.Cells["H_CODE"].Value.ToString())
                {
                    row.Cells["W_CODE"].Appearance.BackColor = Color.PeachPuff;
                    row.Cells["W_NAME"].Appearance.BackColor = Color.PeachPuff;
                    row.Cells["H_CODE"].Appearance.BackColor = Color.PeachPuff;
                    row.Cells["H_NAME"].Appearance.BackColor = Color.PeachPuff;
                }
                
                row.Hidden = false;

                if (row.Cells["KIND"].Value.ToString() == "")
                {
                    row.Hidden = true;
                }

            }

            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                row.Hidden = false;

                if (checkBox1.Checked && row.Cells["COM"].Value.ToString() == "0")
                {
                    row.Hidden = true;
                }
            }
        }

        //체크박스 전체 선택 / 전체 해제
        private void ultraGrid1_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;

            bool result = false;

            if (e.Column.GetHeaderCheckedState(e.Rows) == CheckState.Checked)
            {
                result = true;
            }

            foreach (UltraGridRow row in ultraGrid.Rows)
            {
                if (!row.Hidden)
                {
                    row.Cells["SELECTED"].Value = result;
                }
            }
            ultraGrid.UpdateData();
        }

        //그리드 초기화할때 수정 못하게 태그명을 수정 못하게 한다.
        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {            
            e.Layout.Bands[0].Columns["SELECTED"].SetHeaderCheckedState(e.Layout.Rows, false);
        }
    }
}
