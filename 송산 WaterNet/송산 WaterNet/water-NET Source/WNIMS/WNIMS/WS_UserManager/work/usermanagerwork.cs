﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WS_UserManager.work
{
    public class usermanagerwork : EMFrame.work.BaseWork
    {
        private static usermanagerwork work = null;
        public static usermanagerwork GetInstance()
        {
            if (work == null)
            {
                work = new usermanagerwork();
            }

            return work;
        }

        public DataTable SearchUser(Hashtable param)
        {
            DataTable result = null;
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("WITH USR AS                                                          ");
            oStringBuilder.AppendLine("        (                                                            ");
            oStringBuilder.AppendLine("        SELECT DISTINCT A.USER_ID, A.USER_NAME, A.MGDPCD, A.CODE, B.CD_NAME, B.MC_NAME, A.ALT_DATE      ");
            oStringBuilder.AppendLine("          FROM CM_USER A                                                      ");
            oStringBuilder.AppendLine("              ,CM_CODE B                                                      ");
            oStringBuilder.AppendLine("         WHERE A.CODE = B.CODE(+)                                    ");
            oStringBuilder.AppendLine("         AND   A.MGDPCD = B.MGDPCD(+)                                    ");
            oStringBuilder.AppendLine("        )                                                            ");
            oStringBuilder.AppendLine("         SELECT * FROM                                 ");
            oStringBuilder.AppendLine("         (                                 ");
            oStringBuilder.AppendLine("         SELECT DISTINCT AH.EMPNO USER_ID                                 ");
            oStringBuilder.AppendLine("                , USR.MGDPCD W_MGDPCD                                         ");
            oStringBuilder.AppendLine("                , MD.MGDPNM                                          ");
            oStringBuilder.AppendLine("                , CASE                                                ");
            oStringBuilder.AppendLine("                  WHEN USR.MGDPCD = CD.MGDPCD THEN USR.MGDPCD       ");
            oStringBuilder.AppendLine("                  ELSE CD.MGDPCD                                                           ");
            oStringBuilder.AppendLine("                  END H_MGDPCD                                                             ");
            oStringBuilder.AppendLine("                , AH.KORNAME USER_NAME                                       ");
            oStringBuilder.AppendLine("                , AH.HEAD                                            ");
            oStringBuilder.AppendLine("                , AH.DEPT                                            ");
            oStringBuilder.AppendLine("                , AH.SECT                                            ");
            oStringBuilder.AppendLine("                , AH.TEAM                                            ");
            oStringBuilder.AppendLine("                , USR.CODE W_CODE                                    ");
            oStringBuilder.AppendLine("                , AH.CODE H_CODE                                     ");
            oStringBuilder.AppendLine("                , CASE WHEN AH.CODE = USR.CODE THEN CD.CD_NAME                                  ");
            oStringBuilder.AppendLine("                  ELSE NULL                                  ");
            oStringBuilder.AppendLine("                  END W_NAME                                  ");
            oStringBuilder.AppendLine("                , CASE WHEN AH.CODE = CD.CODE THEN CD.CD_NAME   ");
            oStringBuilder.AppendLine("                  ELSE NULL   ");
            oStringBuilder.AppendLine("                  END H_NAME   ");
            oStringBuilder.AppendLine("                , CASE WHEN USR.CODE<>AH.CODE THEN 1                 ");
            oStringBuilder.AppendLine("                  WHEN USR.CODE IS NULL AND AH.CODE IS NOT NULL THEN 1 ");
            oStringBuilder.AppendLine("                  ELSE 0                                             ");
            oStringBuilder.AppendLine("                  END COM                                            ");
            oStringBuilder.AppendLine("                , 'False' SELECTED                                   ");
            oStringBuilder.AppendLine("                , CASE WHEN USR.MGDPCD = CD.MGDPCD THEN CD.REMARK                                  ");
            oStringBuilder.AppendLine("                       WHEN USR.MGDPCD is null AND CD.MGDPCD is not null THEN CD.REMARK                                  ");
            oStringBuilder.AppendLine("                       WHEN USR.MGDPCD is not null AND CD.MGDPCD is not null AND USR.MGDPCD <> CD.MGDPCD THEN NULL                                   ");
            oStringBuilder.AppendLine("                       ELSE '0'                                   ");
            oStringBuilder.AppendLine("                       END KIND                                   ");
            oStringBuilder.AppendLine("                , USR.ALT_DATE                                            ");
            oStringBuilder.AppendLine("                FROM                                                 ");
            oStringBuilder.AppendLine("                    USR                                              ");
            oStringBuilder.AppendLine("                    , HRBANK.AHAMBASE AH                                    ");
            oStringBuilder.AppendLine("                    , CM_CODE CD                                     ");
            oStringBuilder.AppendLine("                    , MANAGEMENT_DEPARTMENT MD                       ");
            oStringBuilder.AppendLine("                    WHERE USR.USER_ID(+) = AH.EMPNO                  ");
            oStringBuilder.AppendLine("                      AND USR.USER_NAME(+) = AH.KORNAME              ");
            oStringBuilder.AppendLine("                      AND USR.MGDPCD = MD.MGDPCD(+)                  ");
            oStringBuilder.AppendLine("                      AND AH.CODE = CD.CODE(+)                       ");
            oStringBuilder.AppendLine("                      AND AH.EMPNO LIKE :ID || '%'                ");
            oStringBuilder.AppendLine("                      AND AH.KORNAME LIKE :NAME || '%'            ");
            if (param.Keys.Count > 2)
            {
                oStringBuilder.AppendLine("                  AND USR.ALT_DATE BETWEEN :STARTDATE AND :ENDDATE            ");
            }
            oStringBuilder.AppendLine("         )                                                            ");
            oStringBuilder.AppendLine("         WHERE KIND IS NOT NULL                                 ");
            oStringBuilder.AppendLine("                  ORDER BY USER_ID                            ");
                                    
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
            return result;
        }

        public object GetKind(Hashtable param)
        {
            object result = null;

            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.AppendLine("SELECT MGDPCD        ");
            oStringBuilder.AppendLine("FROM CM_CODE         ");
            oStringBuilder.AppendLine("WHERE REMARK = :KIND ");
            oStringBuilder.AppendLine("AND   CODE   = :CODE ");


            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptScalar(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
            return result;
        }

        public object GetKindName(Hashtable param)
        {
            object result = null;

            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.AppendLine("SELECT MC_NAME        ");
            oStringBuilder.AppendLine("FROM CM_CODE         ");
            oStringBuilder.AppendLine("WHERE REMARK = :KIND ");
            oStringBuilder.AppendLine("AND   CODE   = :CODE ");


            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptScalar(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
            return result;
        }

        public DataTable SelectHrbankData()
        {
            DataTable table = null;
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("WITH USR AS");
            oStringBuilder.AppendLine("(");
            oStringBuilder.AppendLine("SELECT A.USER_ID, A.MGDPCD, DEP.HRBANK_CODE");
            oStringBuilder.AppendLine("  FROM CM_USER A");
            oStringBuilder.AppendLine("      ,MANAGEMENT_DEPARTMENT DEP");
            oStringBuilder.AppendLine(" WHERE A.MGDPCD = DEP.MGDPCD(+)");
            oStringBuilder.AppendLine(")");
            oStringBuilder.AppendLine("SELECT");
            oStringBuilder.AppendLine("      DISTINCT CASE (SELECT HRBANK_CODE FROM USR WHERE USER_ID = HR.EMPNO)");
            oStringBuilder.AppendLine("            WHEN HR.CODE THEN (SELECT MGDPCD FROM USR WHERE USER_ID = HR.EMPNO)");
            oStringBuilder.AppendLine("            ELSE DEP.MGDPCD");
            oStringBuilder.AppendLine("       END MGDPCD");
            oStringBuilder.AppendLine("      ,HR.EMPNO, UTL_RAW.CAST_TO_VARCHAR2(HR.KORNAME) KORNAME");
            oStringBuilder.AppendLine("      ,(UTL_RAW.CAST_TO_VARCHAR2(HR.HEAD) || '->' || UTL_RAW.CAST_TO_VARCHAR2(HR.DEPT) ");
            oStringBuilder.AppendLine("       || '->' || UTL_RAW.CAST_TO_VARCHAR2(HR.SECT) || '->' || UTL_RAW.CAST_TO_VARCHAR2(HR.TEAM)) USER_DIVISION");
            oStringBuilder.AppendLine("  FROM MANAGEMENT_DEPARTMENT DEP, HRBANK.AHAMBASE_RAW HR");
            oStringBuilder.AppendLine(" WHERE DEP.HRBANK_CODE(+) = HR.CODE");


            //oStringBuilder.AppendLine("SELECT DISTINCT DECODE((SELECT MGDPCD FROM CM_USER WHERE USER_ID = HR.EMPNO)");
            //oStringBuilder.AppendLine("               ,NULL,DEP.MGDPCD");
            //oStringBuilder.AppendLine("               ,(SELECT MGDPCD FROM CM_USER WHERE USER_ID = HR.EMPNO)) MGDPCD");
            //oStringBuilder.AppendLine("       ,HR.EMPNO, HR.KORNAME");
            //oStringBuilder.AppendLine("      ,HR.HEAD || '->' || HR.DEPT || '->' || HR.SECT || '->' || HR.TEAM USER_DIVISION");
            //oStringBuilder.AppendLine("  FROM MANAGEMENT_DEPARTMENT DEP, HRBANK.AHAMBASE HR");
            //oStringBuilder.AppendLine(" WHERE DEP.HRBANK_CODE(+) = HR.CODE");
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new IDataParameter[] { });
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
            return table;
        }

        public void SaveHrBankUserData(ArrayList hrBankData)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("MERGE INTO CM_USER A                                          ");
            oStringBuilder.AppendLine("USING (SELECT :USER_ID   USER_ID                              ");
            oStringBuilder.AppendLine("             ,:USER_NAME USER_NAME                            ");
            oStringBuilder.AppendLine("             ,:USER_PWD  USER_PWD                             ");
            oStringBuilder.AppendLine("             ,:MGDPCD    MGDPCD                               ");
            oStringBuilder.AppendLine("             ,:SABUN     SABUN                                ");
            oStringBuilder.AppendLine("             ,:USER_DIVISION  USER_DIVISION                   ");
            oStringBuilder.AppendLine("             ,:REG_UID REG_UID                                ");
            oStringBuilder.AppendLine("        FROM DUAL                                             ");
            oStringBuilder.AppendLine("       ) B                                                    ");
            oStringBuilder.AppendLine("   ON (                                                       ");
            oStringBuilder.AppendLine("       A.USER_ID = B.USER_ID                                  ");
            oStringBuilder.AppendLine("      )                                                       ");
            oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
            oStringBuilder.AppendLine("      UPDATE SET A.USER_NAME = B.USER_NAME                    ");
            oStringBuilder.AppendLine("                ,A.USER_PWD = B.USER_PWD                      ");
            oStringBuilder.AppendLine("                ,A.MGDPCD = B.MGDPCD                          ");
            oStringBuilder.AppendLine("                ,A.USER_DIVISION = B.USER_DIVISION            ");
            //oStringBuilder.AppendLine("                ,A.USER_RIGHT = B.USER_RIGHT                  ");
            //oStringBuilder.AppendLine("                ,A.EXT_YN = B.EXT_YN                          ");
            //oStringBuilder.AppendLine("                ,A.L_USER_GROUP_ID = B.L_USER_GROUP_ID        ");
            //oStringBuilder.AppendLine("                ,A.G_USER_GROUP_ID = B.G_USER_GROUP_ID        ");
            oStringBuilder.AppendLine("                ,A.SABUN = B.SABUN                            ");
            //oStringBuilder.AppendLine("                ,A.REG_UID = B.REG_UID                        ");
            //oStringBuilder.AppendLine("                ,A.USE_YN = B.USE_YN                          ");
            oStringBuilder.AppendLine("                ,A.REG_DATE = TO_CHAR(SYSDATE,'YYYYMMDD')     ");
            oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
            oStringBuilder.AppendLine("      INSERT (A.USER_ID                                       ");
            oStringBuilder.AppendLine("              ,A.USER_NAME                                    ");
            oStringBuilder.AppendLine("              ,A.USER_PWD                                     ");
            oStringBuilder.AppendLine("              ,A.MGDPCD                                       ");
            oStringBuilder.AppendLine("              ,A.USER_DIVISION                                ");
            //oStringBuilder.AppendLine("              ,A.USER_RIGHT                                   ");
            //oStringBuilder.AppendLine("              ,A.EXT_YN                                       ");
            //oStringBuilder.AppendLine("              ,A.L_USER_GROUP_ID                              ");
            //oStringBuilder.AppendLine("              ,A.G_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,A.SABUN                                        ");
            oStringBuilder.AppendLine("              ,A.REG_UID                                      ");
            oStringBuilder.AppendLine("              ,A.REG_DATE)                                    ");
            //oStringBuilder.AppendLine("              ,A.USE_YN)                                      ");
            oStringBuilder.AppendLine("      VALUES (B.USER_ID                                       ");
            oStringBuilder.AppendLine("              ,B.USER_NAME                                    ");
            oStringBuilder.AppendLine("              ,B.USER_PWD                                     ");
            oStringBuilder.AppendLine("              ,B.MGDPCD                                       ");
            oStringBuilder.AppendLine("              ,B.USER_DIVISION                                ");
            //oStringBuilder.AppendLine("              ,B.USER_RIGHT                                   ");
            //oStringBuilder.AppendLine("              ,B.EXT_YN                                       ");
            //oStringBuilder.AppendLine("              ,B.L_USER_GROUP_ID                              ");
            //oStringBuilder.AppendLine("              ,B.G_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,B.SABUN                                        ");
            oStringBuilder.AppendLine("              ,B.REG_UID                                      ");
            oStringBuilder.AppendLine("              ,TO_CHAR(SYSDATE,'YYYYMMDD'))                   ");
            //oStringBuilder.AppendLine("              ,B.USE_YN)                                      ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                foreach (Hashtable param in hrBankData)
                {
                    int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
                }
                //param.Add("USER_ID", row["EMPNO"]);
                //param.Add("USER_NAME", row["KORNAME"]);
                //param.Add("USER_PWD", strEncUPWD);
                //param.Add("MGDPCD", row["MGDPCD"]);
                //param.Add("USER_DIVISION", row["USER_DIVISION"]);
                //param.Add("REG_UID", EMFrame.statics.AppStatic.USER_ID);


            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
        }

        public void SaveUserInfoData(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("MERGE INTO CM_USER A                                          ");
            oStringBuilder.AppendLine("USING (SELECT :USER_ID   USER_ID                              ");
            oStringBuilder.AppendLine("             ,:USER_NAME USER_NAME                            ");
            oStringBuilder.AppendLine("             ,:USER_PWD  USER_PWD                             ");
            oStringBuilder.AppendLine("             ,:MGDPCD    MGDPCD                               ");
            oStringBuilder.AppendLine("             ,:USER_DIVISION  USER_DIVISION                   ");
            oStringBuilder.AppendLine("             ,:USER_RIGHT USER_RIGHT                          ");
            oStringBuilder.AppendLine("             ,:EXT_YN EXT_YN                                  ");
            oStringBuilder.AppendLine("             ,:L_USER_GROUP_ID L_USER_GROUP_ID                ");
            oStringBuilder.AppendLine("             ,:G_USER_GROUP_ID G_USER_GROUP_ID                ");
            oStringBuilder.AppendLine("             ,:SABUN SABUN                                    ");
            oStringBuilder.AppendLine("             ,:USE_YN USE_YN                                  ");
            oStringBuilder.AppendLine("             ,:REG_UID REG_UID                                ");
            oStringBuilder.AppendLine("        FROM DUAL                                             ");
            oStringBuilder.AppendLine("       ) B                                                    ");
            oStringBuilder.AppendLine("   ON (                                                       ");
            oStringBuilder.AppendLine("       A.USER_ID = B.USER_ID                                  ");
            oStringBuilder.AppendLine("      )                                                       ");
            oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
            oStringBuilder.AppendLine("      UPDATE SET A.USER_NAME = B.USER_NAME                    ");
            oStringBuilder.AppendLine("                ,A.USER_PWD = B.USER_PWD                      ");
            oStringBuilder.AppendLine("                ,A.MGDPCD = B.MGDPCD                          ");
            oStringBuilder.AppendLine("                ,A.USER_DIVISION = B.USER_DIVISION            ");
            oStringBuilder.AppendLine("                ,A.USER_RIGHT = B.USER_RIGHT                  ");
            oStringBuilder.AppendLine("                ,A.EXT_YN = B.EXT_YN                          ");
            oStringBuilder.AppendLine("                ,A.L_USER_GROUP_ID = B.L_USER_GROUP_ID        ");
            oStringBuilder.AppendLine("                ,A.G_USER_GROUP_ID = B.G_USER_GROUP_ID        ");
            oStringBuilder.AppendLine("                ,A.SABUN = B.SABUN                            ");
            oStringBuilder.AppendLine("                ,A.REG_UID = B.REG_UID                        ");
            oStringBuilder.AppendLine("                ,A.USE_YN = B.USE_YN                          ");
            oStringBuilder.AppendLine("                ,A.REG_DATE = TO_CHAR(SYSDATE,'YYYYMMDD')     ");
            oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
            oStringBuilder.AppendLine("      INSERT (A.USER_ID                                       ");
            oStringBuilder.AppendLine("              ,A.USER_NAME                                    ");
            oStringBuilder.AppendLine("              ,A.USER_PWD                                     ");
            oStringBuilder.AppendLine("              ,A.MGDPCD                                       ");
            oStringBuilder.AppendLine("              ,A.USER_DIVISION                                ");
            oStringBuilder.AppendLine("              ,A.USER_RIGHT                                   ");
            oStringBuilder.AppendLine("              ,A.EXT_YN                                       ");
            oStringBuilder.AppendLine("              ,A.L_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,A.G_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,A.SABUN                                        ");
            oStringBuilder.AppendLine("              ,A.REG_UID                                      ");
            oStringBuilder.AppendLine("              ,A.REG_DATE                                     ");
            oStringBuilder.AppendLine("              ,A.PWDT                                         ");
            oStringBuilder.AppendLine("              ,A.USE_YN)                                      ");            
            oStringBuilder.AppendLine("      VALUES (B.USER_ID                                       ");
            oStringBuilder.AppendLine("              ,B.USER_NAME                                    ");
            oStringBuilder.AppendLine("              ,B.USER_PWD                                     ");
            oStringBuilder.AppendLine("              ,B.MGDPCD                                       ");
            oStringBuilder.AppendLine("              ,B.USER_DIVISION                                ");
            oStringBuilder.AppendLine("              ,B.USER_RIGHT                                   ");
            oStringBuilder.AppendLine("              ,B.EXT_YN                                       ");
            oStringBuilder.AppendLine("              ,B.L_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,B.G_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,B.SABUN                                        ");
            oStringBuilder.AppendLine("              ,B.REG_UID                                      ");
            oStringBuilder.AppendLine("              ,TO_CHAR(SYSDATE,'YYYYMMDD')                    ");
            oStringBuilder.AppendLine("              ,TO_CHAR(SYSDATE,'YYYYMMDD')                    ");  
            oStringBuilder.AppendLine("              ,B.USE_YN)                                      ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }

        }

        public void UpdateUser(ArrayList hrBankData)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("MERGE INTO CM_USER A                                          ");
            oStringBuilder.AppendLine("USING (SELECT :USER_ID   USER_ID                              ");
            oStringBuilder.AppendLine("             ,:USER_NAME USER_NAME                            ");
            oStringBuilder.AppendLine("             ,:USER_PWD  USER_PWD                             ");
            oStringBuilder.AppendLine("             ,:MGDPCD    MGDPCD                               ");
            oStringBuilder.AppendLine("             ,:SABUN     SABUN                                ");
            oStringBuilder.AppendLine("             ,:USER_DIVISION  USER_DIVISION                   ");
            oStringBuilder.AppendLine("             ,:CODE      CODE                                 ");
            oStringBuilder.AppendLine("             ,:REG_UID REG_UID                                ");
            oStringBuilder.AppendLine("        FROM DUAL                                             ");
            oStringBuilder.AppendLine("       ) B                                                    ");
            oStringBuilder.AppendLine("   ON (                                                       ");
            oStringBuilder.AppendLine("       A.USER_ID = B.USER_ID                                  ");
            oStringBuilder.AppendLine("      )                                                       ");
            oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
            oStringBuilder.AppendLine("      UPDATE SET A.USER_NAME = B.USER_NAME                    ");
            //oStringBuilder.AppendLine("                ,A.USER_PWD = B.USER_PWD                      ");
            oStringBuilder.AppendLine("                ,A.MGDPCD = B.MGDPCD                          ");
            oStringBuilder.AppendLine("                ,A.USER_DIVISION = B.USER_DIVISION            ");
            //oStringBuilder.AppendLine("                ,A.USER_RIGHT = B.USER_RIGHT                  ");
            //oStringBuilder.AppendLine("                ,A.EXT_YN = B.EXT_YN                          ");
            //oStringBuilder.AppendLine("                ,A.L_USER_GROUP_ID = B.L_USER_GROUP_ID        ");
            //oStringBuilder.AppendLine("                ,A.G_USER_GROUP_ID = B.G_USER_GROUP_ID        ");
            oStringBuilder.AppendLine("                ,A.SABUN = B.SABUN                            ");
            oStringBuilder.AppendLine("                ,A.CODE = B.CODE                              ");
            //oStringBuilder.AppendLine("                ,A.REG_UID = B.REG_UID                        ");
            //oStringBuilder.AppendLine("                ,A.USE_YN = B.USE_YN                          ");
            oStringBuilder.AppendLine("                ,A.ALT_DATE = TO_CHAR(SYSDATE,'YYYYMMDD')     ");
            oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
            oStringBuilder.AppendLine("      INSERT (A.USER_ID                                       ");
            oStringBuilder.AppendLine("              ,A.USER_NAME                                    ");
            oStringBuilder.AppendLine("              ,A.USER_PWD                                     ");
            oStringBuilder.AppendLine("              ,A.MGDPCD                                       ");
            oStringBuilder.AppendLine("              ,A.USER_DIVISION                                ");
            //oStringBuilder.AppendLine("              ,A.USER_RIGHT                                   ");
            //oStringBuilder.AppendLine("              ,A.EXT_YN                                       ");
            //oStringBuilder.AppendLine("              ,A.L_USER_GROUP_ID                              ");
            //oStringBuilder.AppendLine("              ,A.G_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,A.SABUN                                        ");
            oStringBuilder.AppendLine("              ,A.CODE                                         ");
            oStringBuilder.AppendLine("              ,A.REG_UID                                      ");
            oStringBuilder.AppendLine("              ,A.REG_DATE                                     ");
            oStringBuilder.AppendLine("              ,A.ALT_DATE)                                    ");            
            //oStringBuilder.AppendLine("              ,A.USE_YN)                                      ");
            oStringBuilder.AppendLine("      VALUES (B.USER_ID                                       ");
            oStringBuilder.AppendLine("              ,B.USER_NAME                                    ");
            oStringBuilder.AppendLine("              ,B.USER_PWD                                     ");
            oStringBuilder.AppendLine("              ,B.MGDPCD                                       ");
            oStringBuilder.AppendLine("              ,B.USER_DIVISION                                ");
            //oStringBuilder.AppendLine("              ,B.USER_RIGHT                                   ");
            //oStringBuilder.AppendLine("              ,B.EXT_YN                                       ");
            //oStringBuilder.AppendLine("              ,B.L_USER_GROUP_ID                              ");
            //oStringBuilder.AppendLine("              ,B.G_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,B.SABUN                                        ");
            oStringBuilder.AppendLine("              ,B.CODE                                         ");
            oStringBuilder.AppendLine("              ,B.REG_UID                                      ");
            oStringBuilder.AppendLine("              ,TO_CHAR(SYSDATE,'YYYYMMDD')                    ");
            oStringBuilder.AppendLine("              ,TO_CHAR(SYSDATE,'YYYYMMDD'))                    ");            
            //oStringBuilder.AppendLine("              ,B.USE_YN)                                      ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                foreach (Hashtable param in hrBankData)
                {
                    int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
                }
                //param.Add("USER_ID", row["EMPNO"]);
                //param.Add("USER_NAME", row["KORNAME"]);
                //param.Add("USER_PWD", strEncUPWD);
                //param.Add("MGDPCD", row["MGDPCD"]);
                //param.Add("USER_DIVISION", row["USER_DIVISION"]);
                //param.Add("REG_UID", EMFrame.statics.AppStatic.USER_ID);


            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
        }

        public void UpdateUser(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("MERGE INTO CM_USER A                                          ");
            oStringBuilder.AppendLine("USING (SELECT :USER_ID   USER_ID                              ");
            oStringBuilder.AppendLine("             ,:USER_NAME USER_NAME                            ");
            oStringBuilder.AppendLine("             ,:USER_PWD  USER_PWD                             ");
            oStringBuilder.AppendLine("             ,:MGDPCD    MGDPCD                               ");
            oStringBuilder.AppendLine("             ,:USER_DIVISION  USER_DIVISION                   ");
            oStringBuilder.AppendLine("             ,:USER_RIGHT USER_RIGHT                          ");
            oStringBuilder.AppendLine("             ,:EXT_YN EXT_YN                                  ");
            oStringBuilder.AppendLine("             ,:L_USER_GROUP_ID L_USER_GROUP_ID                ");
            oStringBuilder.AppendLine("             ,:G_USER_GROUP_ID G_USER_GROUP_ID                ");
            oStringBuilder.AppendLine("             ,:SABUN SABUN                                    ");
            oStringBuilder.AppendLine("             ,:USE_YN USE_YN                                  ");
            oStringBuilder.AppendLine("             ,:REG_UID REG_UID                                ");
            oStringBuilder.AppendLine("        FROM DUAL                                             ");
            oStringBuilder.AppendLine("       ) B                                                    ");
            oStringBuilder.AppendLine("   ON (                                                       ");
            oStringBuilder.AppendLine("       A.USER_ID = B.USER_ID                                  ");
            oStringBuilder.AppendLine("      )                                                       ");
            oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
            oStringBuilder.AppendLine("      UPDATE SET A.USER_NAME = B.USER_NAME                    ");
            oStringBuilder.AppendLine("                ,A.USER_PWD = B.USER_PWD                      ");
            oStringBuilder.AppendLine("                ,A.MGDPCD = B.MGDPCD                          ");
            oStringBuilder.AppendLine("                ,A.USER_DIVISION = B.USER_DIVISION            ");
            oStringBuilder.AppendLine("                ,A.USER_RIGHT = B.USER_RIGHT                  ");
            oStringBuilder.AppendLine("                ,A.EXT_YN = B.EXT_YN                          ");
            oStringBuilder.AppendLine("                ,A.L_USER_GROUP_ID = B.L_USER_GROUP_ID        ");
            oStringBuilder.AppendLine("                ,A.G_USER_GROUP_ID = B.G_USER_GROUP_ID        ");
            oStringBuilder.AppendLine("                ,A.SABUN = B.SABUN                            ");
            oStringBuilder.AppendLine("                ,A.REG_UID = B.REG_UID                        ");
            oStringBuilder.AppendLine("                ,A.USE_YN = B.USE_YN                          ");
            oStringBuilder.AppendLine("                ,A.REG_DATE = TO_CHAR(SYSDATE,'YYYYMMDD')     ");
            oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
            oStringBuilder.AppendLine("      INSERT (A.USER_ID                                       ");
            oStringBuilder.AppendLine("              ,A.USER_NAME                                    ");
            oStringBuilder.AppendLine("              ,A.USER_PWD                                     ");
            oStringBuilder.AppendLine("              ,A.MGDPCD                                       ");
            oStringBuilder.AppendLine("              ,A.USER_DIVISION                                ");
            oStringBuilder.AppendLine("              ,A.USER_RIGHT                                   ");
            oStringBuilder.AppendLine("              ,A.EXT_YN                                       ");
            oStringBuilder.AppendLine("              ,A.L_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,A.G_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,A.SABUN                                        ");
            oStringBuilder.AppendLine("              ,A.REG_UID                                      ");
            oStringBuilder.AppendLine("              ,A.REG_DATE                                     ");
            oStringBuilder.AppendLine("              ,A.USE_YN)                                      ");
            oStringBuilder.AppendLine("      VALUES (B.USER_ID                                       ");
            oStringBuilder.AppendLine("              ,B.USER_NAME                                    ");
            oStringBuilder.AppendLine("              ,B.USER_PWD                                     ");
            oStringBuilder.AppendLine("              ,B.MGDPCD                                       ");
            oStringBuilder.AppendLine("              ,B.USER_DIVISION                                ");
            oStringBuilder.AppendLine("              ,B.USER_RIGHT                                   ");
            oStringBuilder.AppendLine("              ,B.EXT_YN                                       ");
            oStringBuilder.AppendLine("              ,B.L_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,B.G_USER_GROUP_ID                              ");
            oStringBuilder.AppendLine("              ,B.SABUN                                        ");
            oStringBuilder.AppendLine("              ,B.REG_UID                                      ");
            oStringBuilder.AppendLine("              ,TO_CHAR(SYSDATE,'YYYYMMDD')                    ");
            oStringBuilder.AppendLine("              ,B.USE_YN)                                      ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
        }

    }
}
