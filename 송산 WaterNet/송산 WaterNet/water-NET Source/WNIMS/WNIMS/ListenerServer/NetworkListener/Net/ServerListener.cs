﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace NetworkListener.Net
{
    public delegate void Send_Message(object sender);
    public delegate void Close_Message(object sender);

    public class ServerListener : IDisposable
    {
        private TcpListener server = null;
        private forms.ServerMain wnd = null;
        private Thread th = null;
        //private Thread ConnectCheckth = null;

        private int port = 3000;

        public ServerListener(forms.ServerMain wnd, int port)
        {
            this.wnd = wnd;
            this.port = port;
        }

        #region IDisposable 멤버
        public void Dispose()
        {
            try
            {
                //if (ConnectCheckth != null && ConnectCheckth.IsAlive) ConnectCheckth.Abort();

                this.BroadCast("C_SERVER_OUT:통합서버 시스템이 종료합니다.\n\r클라이언트 시스템을 종료하십시오.");
                //if (cgroup != null) cgroup.Dispose();
                if (th != null && th.IsAlive) th.Abort();
                if (server != null) this.server.Stop();


            }
            catch { }
        }

        #endregion

        public void ServerStart()
        {
            try
            {
                th = new Thread(new ThreadStart(StartListener));
                th.IsBackground = true;
                th.Start();

                this.threadCheck();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private System.Threading.Timer timer = null;
        public void threadCheck()
        {
            if (timer == null) timer = new System.Threading.Timer(ConnectCheck, null, 0, 1000 * 60 * 5);
        }

        private void ConnectCheck(object state)
        {
            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                try
                {
                    for (int i = ConnectedClients.GetInstance().ConnectedClient.Count - 1; i >= 0; i--)
                    {
                        System.Collections.Hashtable p = ConnectedClients.GetInstance().ConnectedClient.ElementAt(i).Value;
                        string key = ConnectedClients.GetInstance().ConnectedClient.ElementAt(i).Key;

                        if (p.ContainsKey("SESSION_DT"))
                        {
                            DateTime dt = (DateTime)p["SESSION_DT"];
                            if (dt < DateTime.Now.AddMinutes(-10))
                            {
                                ConnectedClients.GetInstance().ConnectedClient.Remove(key);

                                System.Collections.Hashtable param = new System.Collections.Hashtable();

                                EMFrame.utils.DatabaseUtils.GetInstance().forceLogout_info(p);
                            }
                        }
                    }
                }
                catch { }
            }
        }

        public void ServerStop()
        {
            try
            {
                if (th.IsAlive) th.Abort();
                this.server.Stop();

            }
            catch { }
        }

        public void StartListener()
        {
            try
            {
                server = new TcpListener(IPAddress.Any, this.port);
                server.Start();
                this.wnd.GetReceiveMessage("[접속대기중]");

                while (true)
                {
                    TcpClient client = server.AcceptTcpClient();
                    IPEndPoint ip = (IPEndPoint)client.Client.RemoteEndPoint;
                    this.wnd.GetReceiveMessage("[클라이언트] :" + ip.Address.ToString() + " : " + ip.Port.ToString());

                    if (client.Connected)  //클라이언트가 접속하면
                    {
                        Client obj = new Client(this.wnd, client); //Client Class 객체 생성

                        lock (EMFrame.utils.LoggerUtils.logger.Logger)
                        {
                            EMFrame.utils.LoggerUtils.logger.Info("Start => " + client.Client.RemoteEndPoint);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.wnd.GetReceiveMessage(ex.ToString());
            }
        }

        public bool BroadCast(string msg)
        {
            bool result = false;

            //string[] token = msg.Split(':', ',');
            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                foreach (System.Collections.Hashtable param in ConnectedClients.GetInstance().ConnectedClient.Values)
                {
                    Client client = (Client)param["SOCKET"];
                    if (client != null && client.Connect)
                    {
                        client.Send(msg);    
                    }
                }
                result = true;
            }
            return result;
        }

        //public bool BroadCast(string msg, string userid)
        //{
        //    bool result = false;

        //    string[] token = msg.Split(':', ',');
        //    lock (ConnectedClients.GetInstance().ConnectedClient)
        //    {
        //        foreach (System.Collections.Hashtable param in ConnectedClients.GetInstance().ConnectedClient.Values)
        //        {
        //            if (((string)param["USER_ID"]).Equals(userid))
        //            {
        //                Client client = (Client)param["SOCKET"];
        //                if (client != null && client.Connect)
        //                {
        //                    client.Send(msg);
        //                }
        //                break;
        //            }
        //        }
        //        result = true;
        //    }
        //    return result;
        //}
    }


    public class Client : IDisposable
    {
        private TcpClient client = null;
        private string client_ip = string.Empty;
        private Thread th = null;
        private NetworkListener.forms.ServerMain wnd = null;

        private NetworkStream stream = null;
        private StreamReader reader = null;
        private StreamWriter writer = null;

        //public event Send_Message Send_All;
        //public event Close_Message Close_MSG;
        //public Msg_Queue msg_queue = null;

        public Client(NetworkListener.forms.ServerMain wnd, TcpClient socket)
        {
            this.wnd = wnd;
            this.client = socket;
            this.stream = this.client.GetStream();
            this.reader = new StreamReader(stream);
            this.writer = new StreamWriter(stream);
            
            IPEndPoint ip = (IPEndPoint)this.client.Client.RemoteEndPoint;
            this.client_ip = ip.Address.ToString();

            try    //클라이언트가 보내는 데이터 수신할 스레드 생성
            {
                th = new Thread(new ThreadStart(Receive));
                th.IsBackground = true;
                th.Start();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool CanCommunicate
        {
            get { return (this.stream.CanRead && this.stream.CanWrite); }
        }

        public bool Connect
        {
            get
            {
                return this.client.Connected;
            }
        }

        public string Client_IP
        {
            get
            {
                return this.client_ip;
            }
        }

        /// <summary>
        /// 클라이언트가 보내는 메세지 수신하기
        /// </summary>
        public void Receive()
        {
            try
            {
                while (client != null && client.Connected)
                {
                    string msg = reader.ReadLine();
                    if (msg == null) return;
                    string[] token = msg.Split(':');

                    lock (utils.LoggerUtils.logger.Logger)
                    {
                        utils.LoggerUtils.logger.Info(DateTime.Now.ToString() + " : " + "Receive => " + msg + " , " + this.client_ip);
                    }

                    switch (token[0])
                    {
                        case "C_REQ_DUAL_LOGIN": //이중접속


                            break;
                        case "C_SERVER_OUT": //통합서버 종료


                            break;
                        case "C_REQ_MAXIMUM_OUT": //센터의 라이센스 수 초과

                            break;
                        case "C_REQ_REFRESH":    //클라이언트 실행중
                            activeRefresh(msg);
                            //this.Send(msg);      //클라이언트에게 리턴메세지 전송
                            this.wnd.GetReceiveMessage("실행중 : " + msg + "|" + ConnectedClients.GetInstance().ConnectedClient.Count.ToString());
                            break;
                        case "C_REQ_LOGIN":    //클라이언트 접속 - 라이센스체크, 이중접속체크, 정상접속
                            //센터에 허용된 라이센스 수 체크
                            if (ProcessLicenseCheck(msg))
                            {
                                this.Send("C_REQ_MAXIMUM_OUT:" + "허용된 라이센스를 초과하였습니다.");
                                break;
                            }
                            //클라이언트에서 접속했을 경우, 이중접속 여부를 판단하여
                            //서버에서 동일사용자로 로그인한 이전 클라이언트로 메세지를 보낸다.
                            //클라이언트에서 확인후 시스템종료한다.
                            System.Collections.Hashtable param = this.IsDuplicateLogin(msg);  //기 접속된 사용자 체크하여, 접속목록에서 삭제
                            if (param != null) DestoryBeforeLogin(msg, param); //이중접속시 이전 접속클라이언트에게 메세지 전송

                            ProcessNewLogin(msg); //새로운 사용자 접속
                            this.Send(msg);  //클라이언트에게 리턴메세지 전송
                            this.wnd.GetReceiveMessage("접속 : " + msg + "|" + ConnectedClients.GetInstance().ConnectedClient.Count.ToString());
                            break;
                        case "C_REQ_LOGOUT":    //클라이언트 접속종료
                            this.ProcessLogout(msg);
                            this.wnd.GetReceiveMessage("접속종료 : " + msg + "|" + ConnectedClients.GetInstance().ConnectedClient.Count.ToString());
                            break;
                        case "C_REQ_FORCE_LOGOUT":    //클라이언트 강제접속종료
                            this.ProcessForceLogout(msg);
                            this.wnd.GetReceiveMessage("강제접속종료 : " + msg + "|" + ConnectedClients.GetInstance().ConnectedClient.Count.ToString());
                            break;
                    }
                }
            }
            catch (ObjectDisposedException) //소켓의 접속 끊어진 경우
            {
                ;
            }
            catch (SocketException ex)
            {
                this.wnd.GetReceiveMessage(ex.ToString());
            }
            catch (Exception ex)
            {
                this.wnd.GetReceiveMessage(ex.ToString());
            }
        }

        /// <summary>
        /// 접속한 상대방에 데이터 전송
        /// </summary>
        /// <param name="msg"></param>
        public void Send(string msg)
        {
            try
            {
                if (client != null && client.Connected)  //상대방과 연결되어 있는경우
                {
                    lock (utils.LoggerUtils.logger.Logger)
                    {
                        utils.LoggerUtils.logger.Info(DateTime.Now.ToString() + " : " + "Send => " + msg + " , " + this.client.Client.RemoteEndPoint);
                    }
                    
                    this.wnd.GetSendMessage(msg + "|" + this.client_ip);
                    writer.WriteLine(msg);
                    writer.Flush();
                }
                else
                {
                    ;
                }
            }
            catch (Exception ex)
            {
                this.wnd.GetReceiveMessage(ex.ToString());
            }
        }

        private void activeRefresh(string msg)
        {
            string[] token = msg.Split(':', ',');
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_ID", string.IsNullOrEmpty(token[1]) ? string.Empty : token[1]);
            param.Add("LOGIN_IP", string.IsNullOrEmpty(token[2]) ? string.Empty : token[2]);
            param.Add("LOGIN_DT", string.IsNullOrEmpty(token[3]) ? string.Empty : token[3]);
            param.Add("SGCCD", string.IsNullOrEmpty(token[4]) ? string.Empty : token[4]);
            param.Add("ZONE_GBN", string.IsNullOrEmpty(token[5]) ? string.Empty : token[5]);
            param.Add("SOCKET", this);

            //USER_ID-LOGIN_IP-LOGIN_DT-ZONE_GBN
            string key = token[1] + "-" + token[2] + "-" + token[3] + "-" + token[5];

            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                if (ConnectedClients.GetInstance().ConnectedClient.ContainsKey(key))
                {
                    System.Collections.Hashtable p = ConnectedClients.GetInstance().ConnectedClient[key];
                    if (!p.ContainsKey("SESSION_DT")) p.Add("SESSION_DT", DateTime.Now);
                    else p["SESSION_DT"] = DateTime.Now;
                }
            }

            //DB에 로그정보 session_dt 에 업데이트 
            EMFrame.utils.DatabaseUtils.GetInstance().SessionInfo(param);
        }

        private void ProcessForceLogout(string msg)
        {
            string[] token = msg.Split(':', ',');
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("PROC", string.IsNullOrEmpty(token[0]) ? string.Empty : token[0]);
            param.Add("USER_ID", string.IsNullOrEmpty(token[1]) ? string.Empty : token[1]);
            param.Add("LOGIN_IP", string.IsNullOrEmpty(token[2]) ? string.Empty : token[2]);
            param.Add("LOGIN_DT", string.IsNullOrEmpty(token[3]) ? string.Empty : token[3]);
            param.Add("SGCCD", string.IsNullOrEmpty(token[4]) ? string.Empty : token[4]);
            param.Add("ZONE_GBN", string.IsNullOrEmpty(token[5]) ? string.Empty : token[5]);
            param.Add("LOGOUT_GBN", string.IsNullOrEmpty(token[5]) ? string.Empty : token[6]);
            param.Add("SOCKET", this);

            //USER_ID-LOGIN_IP-LOGIN_DT-ZONE_GBN
            string key = token[1] + "-" + token[2] + "-" + token[3] + "-" + token[5];

            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                if (ConnectedClients.GetInstance().ConnectedClient.ContainsKey(key))
                {
                    ConnectedClients.GetInstance().ConnectedClient.Remove(key);
                    lock (utils.LoggerUtils.logger.Logger)
                    {
                        utils.LoggerUtils.logger.Info("Logout => " + msg + " , " + key);
                    }
                }
            }

            //DB에 로그정보 UPdate
            EMFrame.utils.DatabaseUtils.GetInstance().SuccessLogoutInfo(param);

        }

        private void ProcessLogout(string msg)
        {
            string[] token = msg.Split(':', ',');
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("PROC", string.IsNullOrEmpty(token[0]) ? string.Empty : token[0]);
            param.Add("USER_ID", string.IsNullOrEmpty(token[1]) ? string.Empty : token[1]);
            param.Add("LOGIN_IP", string.IsNullOrEmpty(token[2]) ? string.Empty : token[2]);
            param.Add("LOGIN_DT", string.IsNullOrEmpty(token[3]) ? string.Empty : token[3]);
            param.Add("SGCCD", string.IsNullOrEmpty(token[4]) ? string.Empty : token[4]);
            param.Add("ZONE_GBN", string.IsNullOrEmpty(token[5]) ? string.Empty : token[5]);
            param.Add("SOCKET", this);

            //USER_ID-LOGIN_IP-LOGIN_DT-ZONE_GBN
            string key = token[1] + "-" + token[2] + "-" + token[3] + "-" + token[5];

            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                if (ConnectedClients.GetInstance().ConnectedClient.ContainsKey(key))
                {
                    ConnectedClients.GetInstance().ConnectedClient.Remove(key);
                    lock (utils.LoggerUtils.logger.Logger)
                    {
                        utils.LoggerUtils.logger.Info("Logout => " + msg + " , " + key);
                    }
                }
            }

            //DB에 로그정보 UPdate
            if (param["PROC"].Equals("C_REQ_LOGOUT")) //접속종료
            {
                param.Add("LOGOUT_GBN", "0");
                EMFrame.utils.DatabaseUtils.GetInstance().SuccessLogoutInfo(param);
            }
            
        }

        /// <summary>
        /// 이중접속시 이전 접속된 시스템을 종료하기 위해 클라이너트에게 메세지 전송
        /// 이중접속 메세지를 클라이언트에 전송한다.=>클라이언트는 접속을 종료하고, 시스템을 종료한다.
        /// 이중접속 메세지 : 현재 접속된 ID, IP, 시간, 센터코드 포함
        /// </summary>
        /// <param name="param"></param>
        private void DestoryBeforeLogin(string msg, System.Collections.Hashtable param)
        {
            string[] token = msg.Split(':', ',');

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("C_REQ_DUAL_LOGIN" + ":");
            oStringBuilder.Append(token[1] + ",");
            oStringBuilder.Append(token[2] + ",");
            oStringBuilder.Append(token[3] + ",");
            oStringBuilder.Append(token[4] + ",");
            oStringBuilder.Append(token[5]);

            Client remote = (Client)param["SOCKET"];
            remote.Send(oStringBuilder.ToString());
        }

        /// <summary>
        /// 접속사용자 목록에서 동일사용자 접속여부 체크
        /// 동일사용자 있을경우 목록에서 삭제
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private System.Collections.Hashtable IsDuplicateLogin(string msg)
        {
            string[] token = msg.Split(':', ',');

            System.Collections.Hashtable result = null;

            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_ID", string.IsNullOrEmpty(token[1]) ? string.Empty : token[1]);
            param.Add("LOGIN_IP", string.IsNullOrEmpty(token[2]) ? string.Empty : token[2]);
            param.Add("LOGIN_DT", string.IsNullOrEmpty(token[3]) ? string.Empty : token[3]);
            param.Add("SGCCD", string.IsNullOrEmpty(token[4]) ? string.Empty : token[4]);
            param.Add("ZONE_GBN", string.IsNullOrEmpty(token[5]) ? string.Empty : token[5]);
            param.Add("SOCKET", this);

            //USER_ID-LOGIN_DT-ZONE_GBN
            //string key = token[1] + "-" + token[3] + "-" + token[5];

            try
            {
                List<int> n = new List<int>();
                lock (ConnectedClients.GetInstance().ConnectedClient)
                {
                    for (int i = 0; i < ConnectedClients.GetInstance().ConnectedClient.Count; i++)  //중복로그인 체크
                    {
                        System.Collections.Hashtable temp = ConnectedClients.GetInstance().ConnectedClient.ElementAt(i).Value;

                        if ((Convert.ToString(param["USER_ID"]).Equals(Convert.ToString(temp["USER_ID"])))
                           && (Convert.ToString(param["ZONE_GBN"]).Equals(Convert.ToString(temp["ZONE_GBN"]))))
                        {
                            n.Add(i);
                        }
                    }
                    if (n.Count > 0)
                    {
                        result = ConnectedClients.GetInstance().ConnectedClient.ElementAt(n[0]).Value.Clone() as System.Collections.Hashtable;

                        //ConnectedClients.GetInstance().ConnectedClient.Remove(ConnectedClients.GetInstance().ConnectedClient.ElementAt(n[0]).Key); //기존 로그인 정보 삭제
                    }
                    //if (ConnectedClients.GetInstance().ConnectedClient.ContainsKey(key))  //중복로그인
                    //{
                    //    result = ConnectedClients.GetInstance().ConnectedClient[key].Clone() as System.Collections.Hashtable;

                    //    ConnectedClients.GetInstance().ConnectedClient.Remove(key); //기존 로그인 정보 삭제
                    //}
                }
            }
            catch { }

            return result;

        }

        /// <summary>
        /// 새로운 사용자를 접속목록에 추가하고
        /// 접속한 소켓을 저장한다.
        /// ConnectClient = key(LOGIN_ID + SGCCD + ZONE_GBN)
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private System.Collections.Hashtable ProcessNewLogin(string msg)
        {
            System.Collections.Hashtable param = null;
            string[] token = msg.Split(':', ',');

            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                param = new System.Collections.Hashtable();
                param.Add("USER_ID", string.IsNullOrEmpty(token[1]) ? string.Empty : token[1]);
                param.Add("LOGIN_IP", string.IsNullOrEmpty(token[2]) ? string.Empty : token[2]);
                param.Add("LOGIN_DT", string.IsNullOrEmpty(token[3]) ? string.Empty : token[3]);
                param.Add("SGCCD", string.IsNullOrEmpty(token[4]) ? string.Empty : token[4]);
                param.Add("ZONE_GBN", string.IsNullOrEmpty(token[5]) ? string.Empty : token[5]);
                param.Add("SOCKET", this);

                //USER_ID-LOGIN_IP-LOGIN_DT-ZONE_GBN
                string key = token[1] + "-" + token[2] + "-" + token[3] + "-" + token[5];
                ConnectedClients.GetInstance().ConnectedClient.Add(key, param);

                lock (utils.LoggerUtils.logger.Logger)
                {
                    utils.LoggerUtils.logger.Info("newLogin => " + msg + " , " + key);
                }

                EMFrame.utils.DatabaseUtils.GetInstance().LoginInfo(param);
            }

            return param;
        }

        /// <summary>
        /// 서비스센터의 라이센스 수 체크하여, 접속된사용자의 건수 비교
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private bool ProcessLicenseCheck(string msg)
        {
            bool result = false;
            string[] token = msg.Split(':', ',');
            
            int license = -1; int connected = 0;
            lock (EMFrame.statics.AppStatic.LICENSE_ACCOUNT)
            {
                license = EMFrame.statics.AppStatic.LICENSE_ACCOUNT[token[4]];
            }

            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                foreach (System.Collections.Hashtable param in ConnectedClients.GetInstance().ConnectedClient.Values)
                {
                    if (Convert.ToString(param["SGCCD"]).Equals(token[4]))
                    {
                        connected++;
                    }
                }
            }

            if (license < connected)
            {
                result = true;
            }

            return result;
        }

        #region IDisposable 멤버
        public void Dispose()
        {
            try
            {
                if (this.reader != null) reader.Close();
                if (this.writer != null) writer.Close();
                if (this.stream != null) stream.Close();
                if (this.client != null) client.Close();
                if (th != null && th.IsAlive) th.Abort();

                this.wnd = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion
    }

    public class ConnectedClients
    {
        private static ConnectedClients work = null;
        private Dictionary<string, System.Collections.Hashtable> _Dic = new Dictionary<string, System.Collections.Hashtable>();

        public static ConnectedClients GetInstance()
        {
            if (work == null)
            {
                work = new ConnectedClients();
            }

            return work;
        }

        public Dictionary<string, System.Collections.Hashtable> ConnectedClient
        {
            get
            {
                return _Dic;
            }
        }
    }
   
}
