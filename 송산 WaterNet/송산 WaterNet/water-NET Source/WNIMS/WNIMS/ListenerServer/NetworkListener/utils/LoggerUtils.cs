﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace NetworkListener.utils
{
    public class LoggerUtils
    {
        public static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
