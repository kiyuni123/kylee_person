﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace NetworkListener.log
{
    public class Logger
    {
        protected static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Error(Exception ex)
        {
            if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
            {
                logger.Error(ex.ToString());
            }
        }
        public static void Error(string ex)
        {
            if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
            {
                logger.Error(ex);
            }
        }
    }
}