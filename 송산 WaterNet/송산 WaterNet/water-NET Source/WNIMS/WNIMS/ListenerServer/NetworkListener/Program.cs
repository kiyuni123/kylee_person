﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace NetworkListener
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                //오라클 환경변수 
                Environment.SetEnvironmentVariable("NLS_LANG", "KOREAN_KOREA.KO16MSWIN949");
                //시스템실행시 초기환경 설정
                SetDatabaseConnectString();
                SetLicense_Account();

                config.EConfig.INITIALIZE_LOG();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw ex;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new forms.ServerMain());
        }

        /// <summary>
        /// 연결 문자열 구하기 - 통합서버의 DB경로 설정
        /// 연결환경을 XML file로 변경함
        /// </summary>
        /// <returns>연결 문자열</returns>
        /// <remarks></remarks>
        private static string GetConnectionString()
        {
            WS_Common.xml.ReadXml doc = new WS_Common.xml.ReadXml(EMFrame.statics.AppStatic.DB_CONFIG_FILE_PATH);

            string svcName = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/servicename"));
            string ip = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/ip"));
            string id = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/id"));
            string passwd = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/password"));
            string port = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/port"));

            //string conStr = "Data Source=(DESCRIPTION="
            //                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=" + port + ")))"
            //                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
            //                + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";

            string conStr = "Data Source=(DESCRIPTION="
                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=" + port + ")))"
                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
                + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";
            return conStr;
        }

        #region 프로그램 실행시 한번만 실행 - static 변수들 설정
        /// <summary>
        /// 데이터베이스 접근경로 등록
        /// </summary>
        private static void SetDatabaseConnectString()
        {
            
            if (EMFrame.dm.EMapper.ConnectionString.ContainsKey("SVR")) return;

            string conStr = GetConnectionString();
            EMFrame.dm.EMapper.ConnectionString.Add("SVR", conStr);
        }

        /// <summary>
        /// 사업장(관리부서)의 접근 라이센스수 정의
        /// </summary>
        private static void SetLicense_Account()
        {
            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                System.Text.StringBuilder oStringBuilder = new System.Text.StringBuilder();
                oStringBuilder.AppendLine("SELECT MGDPCD, ACCCNT FROM MANAGEMENT_DEPARTMENT");

                System.Data.DataTable dsSource = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });
                foreach (System.Data.DataRow row in dsSource.Rows)
                {
                    EMFrame.statics.AppStatic.LICENSE_ACCOUNT.Add(Convert.ToString(row["MGDPCD"]), Convert.ToInt32(row["ACCCNT"]));
                }

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        #endregion 프로그램 실행시 한번만 실행 - static 변수들 설정
    }
}
