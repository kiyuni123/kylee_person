﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net.Config;
using log4net;
using log4net.Appender;
using System.Windows.Forms;

namespace NetworkListener.config
{
    public class EConfig
    {
        public static void INITIALIZE_LOG()
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("config/log4net.xml"));
            log4net.Repository.Hierarchy.Hierarchy h =
            (log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository();

            foreach (IAppender a in h.Root.Appenders)
            {
                if (a is FileAppender)
                {
                    FileAppender fa = (FileAppender)a;
                    fa.File = Application.StartupPath + @"\logs\log2.txt";
                    fa.ActivateOptions();
                    break;
                }
            }
        }
    }
}
