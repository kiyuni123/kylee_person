﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace NetworkListener.forms
{
    public partial class ServerMain : Form
    {
        private delegate void PrintLogMessage(string value);
        private NetworkListener.Net.ServerListener server = null;

        public ServerMain()
        {
            InitializeComponent();
        }

        private void ServerMain_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            this.Hide();

            this.rtRecieve.TextChanged += new EventHandler(rt_TextChanged);
            this.rtSend.TextChanged += new EventHandler(rt_TextChanged);
            this.rtRecieve.MouseDown += new MouseEventHandler(rt_MouseDown);
            this.rtSend.MouseDown += new MouseEventHandler(rt_MouseDown);
            this.notifyIcon1.MouseDoubleClick += new MouseEventHandler(notifyIcon1_MouseDoubleClick);


            server = new NetworkListener.Net.ServerListener(this, (int)3000);
            server.ServerStart();
        }

        void rt_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.contextMenuStrip2.Show(((RichTextBox)sender), e.Location);    
            }
            
        }

        void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.Visible = true;
        }

        void rt_TextChanged(object sender, EventArgs e)
        {
            if (((RichTextBox)sender).Lines.Count() > 10000) ((RichTextBox)sender).Clear();
        }

        private void ServerMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
            this.Hide();
        }

        #region Delegate Method
        public void GetReceiveMessage(string msg)
        {
            if (this.rtRecieve == null) return;

            try
            {
                ///서로다른 프로세스에서 객체를 크로스로 접근하면 예외가 발생하므로 
                ///이를 해결하기 위해 Invoke 메소드를 사용하게 된다.
                ///진행바가 현재 Invoke가 필요한 상태인지 파악하여 필요하다면 대기상태에 있다가
                ///접근가능할때 백그라운드 작업을 진행하고, 필요한 상태가 아니라면
                ///진행바의 해당 속성에 바로 대입한다.
                if (this.rtRecieve.InvokeRequired)
                {
                    //delegate 생성
                    PrintLogMessage dele = new PrintLogMessage(GetReceiveMessage);
                    //대기상태에 있다가 접근가능한 상태일때 SetProgBar 간접호출
                    this.rtRecieve.Invoke(dele, new object[] { DateTime.Now.ToString() + " : " + msg });
                }
                else
                {
                    this.rtRecieve.AppendText(msg + "\r\n");
                    this.rtRecieve.ScrollToCaret();
                }
            }
            catch { }

        }

        public void GetSendMessage(string msg)
        {
            if (this.rtSend == null) return;

            try
            {
                ///서로다른 프로세스에서 객체를 크로스로 접근하면 예외가 발생하므로 
                ///이를 해결하기 위해 Invoke 메소드를 사용하게 된다.
                ///진행바가 현재 Invoke가 필요한 상태인지 파악하여 필요하다면 대기상태에 있다가
                ///접근가능할때 백그라운드 작업을 진행하고, 필요한 상태가 아니라면
                ///진행바의 해당 속성에 바로 대입한다.
                if (this.rtSend.InvokeRequired)
                {
                    //delegate 생성
                    PrintLogMessage dele = new PrintLogMessage(GetSendMessage);
                    //대기상태에 있다가 접근가능한 상태일때 SetProgBar 간접호출
                    this.rtSend.Invoke(dele, new object[] { msg });
                }
                else
                {
                    this.rtSend.AppendText(msg + "\r\n");
                    this.rtSend.ScrollToCaret();
                }
            }
            catch { }

        }
        #endregion Delegate Method

        private void tsExit_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("프로그램을 종료하시겠습니까?", "종료", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)) != DialogResult.Yes)
            {
                return;
            }
            server.Dispose();
            this.notifyIcon1.Visible = false;
            this.Close();
            this.Dispose();
            Application.Exit();
        }

        private void tsView_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.Visible = true;
        }

        private void tsClear_Click(object sender, EventArgs e)
        {
            this.rtRecieve.Clear();
            this.rtSend.Clear();
        }

    }
}
