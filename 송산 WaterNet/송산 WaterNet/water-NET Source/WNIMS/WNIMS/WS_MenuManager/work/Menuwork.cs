﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WS_MenuManager.work
{
    public class Menuwork : EMFrame.work.BaseWork
    {
        private static Menuwork work = null;
        public static Menuwork GetInstance()
        {
            if (work == null)
            {
                work = new Menuwork();
            }

            return work;
        }

        /// <summary>
        /// 사용자그룹 테이블 저장(CM_GROUP_MASTER)
        /// </summary>
        /// <param name="param"></param>
        public void SaveGroupMasterData(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("MERGE INTO CM_GROUP_MASTER A                                  ");
            oStringBuilder.AppendLine("USING (SELECT :USER_GROUP_ID      USER_GROUP_ID               ");
            oStringBuilder.AppendLine("             ,:USER_GROUP_NAME    USER_GROUP_NAME             ");
            oStringBuilder.AppendLine("             ,:DESCRIPTION        DESCRIPTION                 ");
            oStringBuilder.AppendLine("             ,:ZONE_GBN           ZONE_GBN                    ");
            oStringBuilder.AppendLine("             ,:REG_UID            REG_UID                     ");
            oStringBuilder.AppendLine("        FROM DUAL                                             ");
            oStringBuilder.AppendLine("       ) B                                                    ");
            oStringBuilder.AppendLine("   ON (                                                       ");
            oStringBuilder.AppendLine("       A.USER_GROUP_ID = B.USER_GROUP_ID                      ");
            oStringBuilder.AppendLine("      )                                                       ");
            oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
            oStringBuilder.AppendLine("      UPDATE SET A.USER_GROUP_NAME = B.USER_GROUP_NAME        ");
            oStringBuilder.AppendLine("                ,A.DESCRIPTION = B.DESCRIPTION                ");
            oStringBuilder.AppendLine("                ,A.ZONE_GBN = B.ZONE_GBN                      ");
            oStringBuilder.AppendLine("                ,A.REG_UID = B.REG_UID                        ");
            oStringBuilder.AppendLine("                ,A.REG_DATE = TO_CHAR(SYSDATE,'YYYYMMDD') ");
            oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
            oStringBuilder.AppendLine("      INSERT (A.USER_GROUP_ID                                 ");
            oStringBuilder.AppendLine("              ,A.USER_GROUP_NAME                              ");
            oStringBuilder.AppendLine("              ,A.DESCRIPTION                                  ");
            oStringBuilder.AppendLine("              ,A.ZONE_GBN                                    ");
            oStringBuilder.AppendLine("              ,A.REG_UID                                    ");
            oStringBuilder.AppendLine("              ,A.REG_DATE)                                    ");
            oStringBuilder.AppendLine("      VALUES (B.USER_GROUP_ID                                 ");
            oStringBuilder.AppendLine("              ,B.USER_GROUP_NAME                              ");
            oStringBuilder.AppendLine("              ,B.DESCRIPTION                                  ");
            oStringBuilder.AppendLine("              ,B.ZONE_GBN                                    ");
            oStringBuilder.AppendLine("              ,B.REG_UID                                     ");
            oStringBuilder.AppendLine("              ,TO_CHAR(SYSDATE,'YYYYMMDD') )      ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
        }

        /// <summary>
        /// 사용자 그룹 메뉴권한 테이블 저장(CM_GROUP_MENU)
        /// </summary>
        /// <param name="param"></param>
        public void SaveGroupMenuData(System.Collections.Generic.Dictionary<int, System.Collections.Hashtable> dic)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("MERGE INTO CM_GROUP_MENU A                                     ");
            oStringBuilder.AppendLine("USING (SELECT :USER_GROUP_ID      USER_GROUP_ID                ");
            oStringBuilder.AppendLine("             ,:MU_ID    MU_ID                                  ");
            oStringBuilder.AppendLine("             ,:MENU_AUTH    MENU_AUTH                          ");
            oStringBuilder.AppendLine("             ,:ZONE_GBN    ZONE_GBN                            ");
            oStringBuilder.AppendLine("        FROM DUAL                                              ");
            oStringBuilder.AppendLine("       ) B                                                     ");
            oStringBuilder.AppendLine("   ON (                                                        ");
            oStringBuilder.AppendLine("       A.USER_GROUP_ID = B.USER_GROUP_ID AND A.MU_ID = B.MU_ID ");
            oStringBuilder.AppendLine("      )                                                       ");
            oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
            oStringBuilder.AppendLine("      UPDATE SET A.MENU_AUTH = B.MENU_AUTH                    ");
            oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
            oStringBuilder.AppendLine("      INSERT (A.USER_GROUP_ID                                 ");
            oStringBuilder.AppendLine("              ,A.MU_ID                                        ");
            oStringBuilder.AppendLine("              ,A.MENU_AUTH                                    ");
            oStringBuilder.AppendLine("              ,A.ZONE_GBN)                                    ");
            oStringBuilder.AppendLine("      VALUES (B.USER_GROUP_ID                                 ");
            oStringBuilder.AppendLine("              ,B.MU_ID                                        ");
            oStringBuilder.AppendLine("              ,B.MENU_AUTH                                    ");
            oStringBuilder.AppendLine("              ,B.ZONE_GBN)                                    ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                mapper.BeginTransaction();

                foreach (System.Collections.Hashtable param in dic.Values)
                {
                    int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
                }

                mapper.CommitTransaction();
            }
            catch (Exception oException)
            {
                mapper.RollbackTransaction();
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
        }

        /// <summary>
        /// MENU_MASTER 저장하기
        /// </summary>
        /// <param name="param"></param>
        public void SaveMenuMasterData(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("MERGE INTO MENU_MASTER A                                      ");
            oStringBuilder.AppendLine("USING (SELECT :MU_ID      MU_ID                               ");
            oStringBuilder.AppendLine("             ,:MU_NM      MU_NM                               ");
            oStringBuilder.AppendLine("             ,:MDU_VER    MDU_VER                             ");
            oStringBuilder.AppendLine("             ,:CLASS_NM   CLASS_NM                            ");
            oStringBuilder.AppendLine("             ,:MDU_FLNM   MDU_FLNM                            ");
            oStringBuilder.AppendLine("             ,:MDU_PATH   MDU_PATH                            ");
            oStringBuilder.AppendLine("             ,:REMARK     REMARK                              ");
            oStringBuilder.AppendLine("             ,:ZONE_GBN   ZONE_GBN                            ");
            oStringBuilder.AppendLine("        FROM DUAL                                             ");
            oStringBuilder.AppendLine("       ) B                                                    ");
            oStringBuilder.AppendLine("   ON (                                                       ");
            oStringBuilder.AppendLine("       A.MU_ID = B.MU_ID                                      ");
            oStringBuilder.AppendLine("      )                                                       ");
            oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
            oStringBuilder.AppendLine("      UPDATE SET A.MU_NM = B.MU_NM                            ");
            oStringBuilder.AppendLine("                ,A.MDU_VER = B.MDU_VER                        ");
            oStringBuilder.AppendLine("                ,A.CLASS_NM = B.CLASS_NM                      ");
            oStringBuilder.AppendLine("                ,A.MDU_FLNM = B.MDU_FLNM                      ");
            oStringBuilder.AppendLine("                ,A.MDU_PATH = B.MDU_PATH                      ");
            oStringBuilder.AppendLine("                ,A.MDU_UPDT = SYSDATE                         ");
            oStringBuilder.AppendLine("                ,A.REMARK = B.REMARK                          ");
            oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
            oStringBuilder.AppendLine("      INSERT (A.MU_ID                                         ");
            oStringBuilder.AppendLine("              ,A.MU_NM                                        ");
            oStringBuilder.AppendLine("              ,A.MDU_VER                                      ");
            oStringBuilder.AppendLine("              ,A.CLASS_NM                                     ");
            oStringBuilder.AppendLine("              ,A.MDU_FLNM                                     ");
            oStringBuilder.AppendLine("              ,A.MDU_PATH                                     ");
            oStringBuilder.AppendLine("              ,A.MDU_UPDT                                     ");
            oStringBuilder.AppendLine("              ,A.REMARK                                       ");
            oStringBuilder.AppendLine("              ,A.ZONE_GBN)                                    ");
            oStringBuilder.AppendLine("      VALUES (B.MU_ID                                         ");
            oStringBuilder.AppendLine("              ,B.MU_NM                                        ");
            oStringBuilder.AppendLine("              ,B.MDU_VER                                      ");
            oStringBuilder.AppendLine("              ,B.CLASS_NM                                     ");
            oStringBuilder.AppendLine("              ,B.MDU_FLNM                                     ");
            oStringBuilder.AppendLine("              ,B.MDU_PATH                                     ");
            oStringBuilder.AppendLine("              ,SYSDATE                                        ");
            oStringBuilder.AppendLine("              ,B.REMARK                                       ");
            oStringBuilder.AppendLine("              ,B.ZONE_GBN)                                    ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }

        }

        public System.Data.DataTable GetUserGroupMasterData(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT USER_GROUP_ID                 ");
            oStringBuilder.AppendLine("      ,USER_GROUP_NAME               ");
            oStringBuilder.AppendLine("      ,DESCRIPTION                   ");
            oStringBuilder.AppendLine("      ,ZONE_GBN                      ");
            oStringBuilder.AppendLine("  FROM CM_GROUP_MASTER               ");
            if (param["ZONE_GBN"] != null)
                oStringBuilder.AppendLine(" WHERE ZONE_GBN in (:ZONE_GBN)       ");    
            
            oStringBuilder.AppendLine(" ORDER BY USER_GROUP_ID ASC          ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        public bool IsGroupMenuData(Hashtable param)
        {
            bool result = false;

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT distinct user_group_id FROM CM_GROUP_MENU WHERE USER_GROUP_ID = :USER_GROUP_ID  ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                object o = mapper.ExecuteScriptScalar(oStringBuilder.ToString(), param);

                if (o != null)
                {
                    result = true;
                }
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public System.Data.DataTable GetMenuMasterData()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("with menu as                                         ");
                oStringBuilder.AppendLine("("                                                    );
                oStringBuilder.AppendLine("   SELECT A.MU_ID                                    ");
                oStringBuilder.AppendLine("         ,A.MU_NM                                    ");
                oStringBuilder.AppendLine("         ,A.TOP_YN                                   ");
                oStringBuilder.AppendLine("         ,A.USE_YN                                   ");
                oStringBuilder.AppendLine("         ,A.P_MU_ID                                  ");
                oStringBuilder.AppendLine("         ,A.ORDERNO                                  ");
                oStringBuilder.AppendLine("         ,A.REMARK                                   ");
                oStringBuilder.AppendLine("     FROM MENU_MASTER A                              ");
                oStringBuilder.AppendLine("    WHERE A.USE_YN = '1' AND A.ZONE_GBN = :ZONE_GBN  ");
                oStringBuilder.AppendLine(")"                                                    );
                oStringBuilder.AppendLine("SELECT A.MU_ID                                        ");
                oStringBuilder.AppendLine("      ,DECODE(LEVEL, 1, A.MU_NM, NULL) MENU1          ");
                oStringBuilder.AppendLine("      ,DECODE(LEVEL, 2, A.MU_NM, NULL) MENU2          ");
                oStringBuilder.AppendLine("      ,DECODE(LEVEL, 3, A.MU_NM, NULL) MENU3          ");
                oStringBuilder.AppendLine("      ,A.TOP_YN                                       ");
                oStringBuilder.AppendLine("      ,A.USE_YN                                       ");
                oStringBuilder.AppendLine("      ,A.P_MU_ID                                      ");
                oStringBuilder.AppendLine("      ,A.ORDERNO                                      ");
                oStringBuilder.AppendLine("      ,'0' AS MENU_AUTH                               ");
                oStringBuilder.AppendLine("      ,A.REMARK REMARK                                ");
                oStringBuilder.AppendLine("      ,LEVEL LEVEL                                    ");
                oStringBuilder.AppendLine("  FROM MENU A                                  ");
                oStringBuilder.AppendLine("  START WITH TOP_YN = '1'                             ");
                oStringBuilder.AppendLine("  CONNECT BY PRIOR MU_ID = P_MU_ID                    ");
                oStringBuilder.AppendLine(" ORDER SIBLINGS BY ORDERNO                            ");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new Hashtable());

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;

        }

        /// <summary>
        /// MENU_MASTER 질의하기
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public System.Data.DataTable GetMenuMasterData(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                oStringBuilder.AppendLine("with menu as                                         ");
                oStringBuilder.AppendLine("("                                                    );
                oStringBuilder.AppendLine("   SELECT A.MU_ID                                    ");
                oStringBuilder.AppendLine("         ,A.MU_NM                                    ");
                oStringBuilder.AppendLine("         ,A.TOP_YN                                   ");                               
                oStringBuilder.AppendLine("         ,A.USE_YN                                   ");                            
                oStringBuilder.AppendLine("         ,A.P_MU_ID                                  ");                         
                oStringBuilder.AppendLine("         ,A.ORDERNO                                  ");                      
                oStringBuilder.AppendLine("         ,A.REMARK                                   ");                   
                oStringBuilder.AppendLine("     FROM MENU_MASTER A                              ");                            
                oStringBuilder.AppendLine("    WHERE A.USE_YN = '1' AND A.ZONE_GBN = :ZONE_GBN  ");
                oStringBuilder.AppendLine(")"                                                    );
                oStringBuilder.AppendLine("SELECT A.MU_ID                                         ");
                oStringBuilder.AppendLine("      ,A.MENU1                                         ");
                oStringBuilder.AppendLine("      ,A.MENU2                                         ");
                oStringBuilder.AppendLine("      ,A.MENU3                                         ");
                oStringBuilder.AppendLine("      ,A.TOP_YN                                        ");
                oStringBuilder.AppendLine("      ,A.USE_YN                                        ");
                oStringBuilder.AppendLine("      ,A.P_MU_ID                                       ");
                oStringBuilder.AppendLine("      ,A.ORDERNO                                       ");
                oStringBuilder.AppendLine("      ,NVL(B.MENU_AUTH, '0') MENU_AUTH                 ");
                oStringBuilder.AppendLine("      ,A.REMARK                                        ");
                oStringBuilder.AppendLine("      ,A.LVL                                           ");
                oStringBuilder.AppendLine("  FROM (                                               ");
                oStringBuilder.AppendLine("       SELECT A.MU_ID                                  ");
                oStringBuilder.AppendLine("             ,DECODE(LEVEL, 1, A.MU_NM, NULL) MENU1    ");
                oStringBuilder.AppendLine("             ,DECODE(LEVEL, 2, A.MU_NM, NULL) MENU2    ");
                oStringBuilder.AppendLine("             ,DECODE(LEVEL, 3, A.MU_NM, NULL) MENU3    ");
                oStringBuilder.AppendLine("             ,A.TOP_YN                                 ");
                oStringBuilder.AppendLine("             ,A.USE_YN                                 ");
                oStringBuilder.AppendLine("             ,A.P_MU_ID                                ");
                oStringBuilder.AppendLine("             ,A.ORDERNO                                ");
                oStringBuilder.AppendLine("             ,A.REMARK                                 ");
                oStringBuilder.AppendLine("             ,LEVEL LVL                                ");
                oStringBuilder.AppendLine("         FROM MENU A                            ");
                oStringBuilder.AppendLine("        START WITH A.TOP_YN = '1'                      ");
                oStringBuilder.AppendLine("        CONNECT BY PRIOR A.MU_ID = A.P_MU_ID           ");
                oStringBuilder.AppendLine("        ORDER SIBLINGS BY A.ORDERNO                    ");
                oStringBuilder.AppendLine("       ) A                                             ");
                oStringBuilder.AppendLine("      ,CM_GROUP_MENU B                                 ");
                oStringBuilder.AppendLine(" WHERE B.USER_GROUP_ID(+) = :USER_GROUP_ID             ");
                oStringBuilder.AppendLine("   AND B.MU_ID(+) = A.MU_ID                            ");

                //oStringBuilder.AppendLine("WITH MENU AS                                                     ");
                //oStringBuilder.AppendLine("(SELECT A.MU_ID, A.MU_NM, A.TOP_YN, A.USE_YN, A.REMARK           ");
                //oStringBuilder.AppendLine("       ,A.P_MU_ID, A.ORDERNO, B.MENU_AUTH, B.USER_GROUP_ID       ");
                //oStringBuilder.AppendLine("  FROM MENU_MASTER A                                             ");
                //oStringBuilder.AppendLine("      ,CM_GROUP_MENU B                                           ");
                //if (param.Count == 0)
                //    oStringBuilder.AppendLine(" WHERE A.MU_ID = B.MU_ID(+)                                  ");    
                //else
                //    oStringBuilder.AppendLine(" WHERE A.MU_ID = B.MU_ID(+) AND B.USER_GROUP_ID(+) = :USER_GROUP_ID    ");
                //oStringBuilder.AppendLine(")                                                                ");
                //oStringBuilder.AppendLine("SELECT distinct A.MU_ID                                          ");
                //oStringBuilder.AppendLine("      ,DECODE(LEVEL, 1, A.MU_NM, NULL) MENU1                     ");
                //oStringBuilder.AppendLine("      ,DECODE(LEVEL, 2, A.MU_NM, NULL) MENU2                     ");
                //oStringBuilder.AppendLine("      ,DECODE(LEVEL, 3, A.MU_NM, NULL) MENU3                     ");
                //oStringBuilder.AppendLine("      ,A.TOP_YN                                                  ");
                //oStringBuilder.AppendLine("      ,A.USE_YN                                                  ");
                //oStringBuilder.AppendLine("      ,A.P_MU_ID                                                 ");
                //oStringBuilder.AppendLine("      ,A.ORDERNO                                                 ");
                //if (param.Count == 0)
                //    oStringBuilder.AppendLine("      ,'0' as MENU_AUTH                                    ");
                //else
                //    oStringBuilder.AppendLine("      ,A.MENU_AUTH                                           ");
                //oStringBuilder.AppendLine("      ,A.REMARK                                                  ");
                //oStringBuilder.AppendLine("      ,LEVEL                                                     ");
                //oStringBuilder.AppendLine("  FROM MENU A                                                    ");
                //oStringBuilder.AppendLine("  START WITH TOP_YN = '1'                                        ");
                //oStringBuilder.AppendLine("  CONNECT BY PRIOR MU_ID = P_MU_ID AND USE_YN = '1'              ");
                //oStringBuilder.AppendLine(" ORDER SIBLINGS BY ORDERNO                                       ");


                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
                
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        ///// <summary>
        ///// 사용자 메뉴를 outer join 하여 질의한다.
        ///// </summary>
        ///// <param name="param"></param>
        ///// <returns></returns>
        //public System.Data.DataTable GetUserMenuData(Hashtable param)
        //{
        //    StringBuilder oStringBuilder = new StringBuilder();
        //    oStringBuilder.AppendLine("WITH U_MENU AS                                         ");
        //    oStringBuilder.AppendLine("(                                                      ");
        //    oStringBuilder.AppendLine("  SELECT A.USER_ID, A.USER_NAME, B.MU_ID, B.MENU_AUTH  ");
        //    oStringBuilder.AppendLine("    FROM CM_USER A                                     ");
        //    oStringBuilder.AppendLine("        ,USER_MENU B                                   ");
        //    oStringBuilder.AppendLine("   WHERE A.USER_ID = B.USER_ID                         ");
        //    oStringBuilder.AppendLine("     AND A.USER_ID = :USER_ID                          ");
        //    oStringBuilder.AppendLine(")                                                      ");
        //    oStringBuilder.AppendLine("SELECT A.MU_ID                                         ");
        //    oStringBuilder.AppendLine("      ,A.MU_NM                                         ");
        //    oStringBuilder.AppendLine("      ,A.MDU_VER                                       ");
        //    oStringBuilder.AppendLine("      ,A.MDU_UPDT                                      ");
        //    oStringBuilder.AppendLine("      ,A.CLASS_NM                                      ");
        //    oStringBuilder.AppendLine("      ,A.MDU_FLNM                                      ");
        //    oStringBuilder.AppendLine("      ,A.MDU_PATH                                      ");
        //    oStringBuilder.AppendLine("      ,A.REMARK                                        ");
        //    oStringBuilder.AppendLine("      ,NVL(B.MENU_AUTH, '0') MENU_AUTH                 ");
        //    oStringBuilder.AppendLine("  FROM MENU_MASTER A                                   ");
        //    oStringBuilder.AppendLine("      ,U_MENU B                                        ");
        //    oStringBuilder.AppendLine(" WHERE A.MU_ID = B.MU_ID(+)                            ");
        //    oStringBuilder.AppendLine("ORDER BY A.MU_ID ASC                                   ");

        //    System.Data.DataTable result = null;
        //    EMFrame.dm.EMapper mapper = null;
        //    try
        //    {
        //        mapper = new EMFrame.dm.EMapper("SVR");

        //        result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
        //    }
        //    catch (Exception oException)
        //    {
        //        EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
        //    }
        //    finally
        //    {
        //        mapper.Close();
        //    }

        //    return result;
        //}

        //public void SaveUserMenuData(Dictionary<int, Hashtable> dic)
        //{
        //    EMFrame.dm.EMapper mapper = null;
        //    try
        //    {
        //        mapper = new EMFrame.dm.EMapper("SVR");
        //        mapper.BeginTransaction();

        //        StringBuilder oStringBuilder = new StringBuilder();
        //        oStringBuilder.AppendLine("MERGE INTO USER_MENU A                                        ");
        //        oStringBuilder.AppendLine("USING (SELECT :USER_ID   USER_ID                              ");
        //        oStringBuilder.AppendLine("             ,:MU_ID MU_ID                                    ");
        //        oStringBuilder.AppendLine("             ,:MENU_AUTH  MENU_AUTH                           ");
        //        oStringBuilder.AppendLine("        FROM DUAL                                             ");
        //        oStringBuilder.AppendLine("       ) B                                                    ");
        //        oStringBuilder.AppendLine("   ON (                                                       ");
        //        oStringBuilder.AppendLine("       A.USER_ID = B.USER_ID                                  ");
        //        oStringBuilder.AppendLine("       AND A.MU_ID = B.MU_ID                                  ");
        //        oStringBuilder.AppendLine("      )                                                       ");
        //        oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
        //        oStringBuilder.AppendLine("      UPDATE SET A.MENU_AUTH = B.MENU_AUTH                    ");
        //        oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
        //        oStringBuilder.AppendLine("      INSERT (A.USER_ID                                       ");
        //        oStringBuilder.AppendLine("              ,A.MU_ID                                        ");
        //        oStringBuilder.AppendLine("              ,A.MENU_AUTH)                                   ");
        //        oStringBuilder.AppendLine("      VALUES (B.USER_ID                                       ");
        //        oStringBuilder.AppendLine("              ,B.MU_ID                                        ");
        //        oStringBuilder.AppendLine("              ,B.MENU_AUTH)                                   ");

        //        foreach (System.Collections.Hashtable param in dic.Values)
        //        {
        //            mapper.ExecuteScript(oStringBuilder.ToString(), param);
        //        }

        //        mapper.CommitTransaction();
        //    }
        //    catch (Exception oException)
        //    {
        //        mapper.RollbackTransaction();
        //        EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
        //    }
        //    finally
        //    {
        //        mapper.Close();
        //    }
        //}

        //public void SaveUserMenuData(ref EMFrame.dm.EMapper mapper, Hashtable param)
        //{
        //    StringBuilder oStringBuilder = new StringBuilder();
        //    oStringBuilder.AppendLine("MERGE INTO USER_MENU A                                        ");
        //    oStringBuilder.AppendLine("USING (SELECT :USER_ID   USER_ID                              ");
        //    oStringBuilder.AppendLine("             ,:MU_ID MU_ID                                    ");
        //    oStringBuilder.AppendLine("             ,:MENU_AUTH  MENU_AUTH                           ");
        //    oStringBuilder.AppendLine("        FROM DUAL                                             ");
        //    oStringBuilder.AppendLine("       ) B                                                    ");
        //    oStringBuilder.AppendLine("   ON (                                                       ");
        //    oStringBuilder.AppendLine("       A.USER_ID = B.USER_ID                                  ");
        //    oStringBuilder.AppendLine("       AND A.MU_ID = B.MU_ID                                  ");
        //    oStringBuilder.AppendLine("      )                                                       ");
        //    oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
        //    oStringBuilder.AppendLine("      UPDATE SET A.MENU_AUTH = B.MENU_AUTH                    ");
        //    oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
        //    oStringBuilder.AppendLine("      INSERT (A.USER_ID                                       ");
        //    oStringBuilder.AppendLine("              ,A.MU_ID                                        ");
        //    oStringBuilder.AppendLine("              ,A.MENU_AUTH)                                   ");
        //    oStringBuilder.AppendLine("      VALUES (B.USER_ID                                       ");
        //    oStringBuilder.AppendLine("              ,B.MU_ID                                        ");
        //    oStringBuilder.AppendLine("              ,B.MENU_AUTH)                                   ");

        //    try
        //    {
        //        int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
        //    }
        //    catch (Exception oException)
        //    {
        //        EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
        //        throw;
        //    }

        //}

        //public System.Data.DataTable GetUserMenuData2(Hashtable param)
        //{
        //    StringBuilder oStringBuilder = new StringBuilder();
        //    oStringBuilder.AppendLine("WITH U_MENU AS                                         ");
        //    oStringBuilder.AppendLine("(                                                      ");
        //    oStringBuilder.AppendLine("  SELECT A.USER_ID, A.USER_NAME, B.MU_ID, B.MENU_AUTH  ");
        //    oStringBuilder.AppendLine("    FROM CM_USER A                                     ");
        //    oStringBuilder.AppendLine("        ,USER_MENU B                                   ");
        //    oStringBuilder.AppendLine("   WHERE A.USER_ID = B.USER_ID                         ");
        //    oStringBuilder.AppendLine("     AND A.USER_ID = :USER_ID                          ");
        //    oStringBuilder.AppendLine(")                                                      ");
        //    oStringBuilder.AppendLine("SELECT A.MU_ID                                         ");
        //    oStringBuilder.AppendLine("      ,A.MU_NM                                         ");
        //    oStringBuilder.AppendLine("      ,A.MDU_VER                                       ");
        //    oStringBuilder.AppendLine("      ,A.MDU_UPDT                                      ");
        //    oStringBuilder.AppendLine("      ,A.CLASS_NM                                      ");
        //    oStringBuilder.AppendLine("      ,A.MDU_FLNM                                      ");
        //    oStringBuilder.AppendLine("      ,A.MDU_PATH                                      ");
        //    oStringBuilder.AppendLine("      ,A.REMARK                                        ");
        //    oStringBuilder.AppendLine("      ,NVL(B.MENU_AUTH, '0') MENU_AUTH                 ");
        //    oStringBuilder.AppendLine("  FROM MENU_MASTER A                                   ");
        //    oStringBuilder.AppendLine("      ,U_MENU B                                        ");
        //    oStringBuilder.AppendLine(" WHERE A.MU_ID = B.MU_ID                               ");
        //    oStringBuilder.AppendLine("ORDER BY A.MU_ID ASC                                   ");

        //    System.Data.DataTable result = null;
        //    EMFrame.dm.EMapper mapper = null;
        //    try
        //    {
        //        mapper = new EMFrame.dm.EMapper("SVR");

        //        result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
        //    }
        //    catch (Exception oException)
        //    {
        //        EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
        //    }
        //    finally
        //    {
        //        mapper.Close();
        //    }

        //    return result;
        //}

        public System.Data.DataTable GetUserID(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT A.USER_ID                                         ");
            oStringBuilder.AppendLine("      ,A.USER_NAME                                       ");
            oStringBuilder.AppendLine("  FROM CM_USER A                                         ");
            oStringBuilder.AppendLine("ORDER BY A.USER_NAME ASC                                 ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;

        }
    }
}
