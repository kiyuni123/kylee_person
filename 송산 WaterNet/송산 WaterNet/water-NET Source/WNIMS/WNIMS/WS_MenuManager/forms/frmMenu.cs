﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WS_MenuManager.forms
{
    public partial class frmMenu : EMFrame.form.BaseForm
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 그리드 컬럼 초기화
        /// </summary>
        private void InitializeGridColumnSetting()
        {
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_GROUP_ID", "그룹ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_GROUP_NAME", "그룹명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "DESCRIPTION", "설명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "ZONE_GBN", "지방/광역", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);

            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "MU_ID", "메뉴ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "MENU1", "1차메뉴", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "MENU2", "2차메뉴", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "MENU3", "3차메뉴", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "TOP_YN", "최상위메뉴", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "USE_YN", "사용여부", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "P_MU_ID", "상위메뉴", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "ORDERNO", "순서", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "MENU_AUTH", "사용권한", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "REMARK", "설명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugMenu, "LVL", "LVL", 50, true, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                     Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugMenu);
            WS_Common.utils.gridUtils.Set_ColumeAllowEdit(this.ugMenu, "MENU_AUTH");
            this.ugMenu.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
        }

        /// <summary>
        /// 그리드 콤보박스 초기화
        /// </summary>
        private void InitializeValueListSetting()
        {
            object[,] aoSource = {
                {"0",  "편집허용"},
                {"1",  "조회허용"},
                {"2",  "사용안함"}
            };

            WS_Common.utils.gridUtils.SetValueList(this.ugMenu, "MENU_AUTH", aoSource);
        }

        /// <summary>
        /// 컨트롤 이벤트 초기화
        /// </summary>
        private void InitializeControlEvents()
        {
            this.ugData.Click += new EventHandler(ugData_Click);
            this.ugMenu.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(ugMenu_CellListSelect);
            this.btnAdd.Click += new EventHandler(btnAdd_Click);
            this.btnSave.Click += new EventHandler(btnSave_Click);
            this.btnDelete.Click += new EventHandler(btnDelete_Click);

            this.rbLocal.Click += new EventHandler(rbZone_Click);
            this.rbWide.Click += new EventHandler(rbZone_Click);
        }

        private void rbZone_Click(object sender, EventArgs e)
        {

            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_GROUP_ID", "");
            param.Add("ZONE_GBN", rbLocal.Checked ? "지방" : "광역");

            this.GetUserGroupMaster(param);
            this.GetGroupMenu(param);

        }

        /// <summary>
        /// 메뉴그리드 권한 셀 선택(이벤트)시 하부메뉴 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ugMenu_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (!e.Cell.Column.Key.Equals("MENU_AUTH")) return;
            e.Cell.Row.Update();
            if (Convert.ToInt32(e.Cell.Row.Cells["LVL"].Value) < 3)
            {
                for (int i = e.Cell.Row.Index + 1; i < ugMenu.Rows.Count; i++)
                {
                    if (Convert.ToInt32(e.Cell.Row.Cells["LVL"].Value) < Convert.ToInt32(this.ugMenu.Rows[i].Cells["LVL"].Value))
                        this.ugMenu.Rows[i].Cells["MENU_AUTH"].Value = e.Cell.Value;
                    else break;
                }

            }
        }

        /// <summary>
        /// 사용자그룹 그리드 Row 선택(이벤트)시 메뉴 불러오기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ugData_Click(object sender, EventArgs e)
        {
            if (this.ugData.Rows.Count <= 0) return;
            if (this.ugData.ActiveRow == null) return;

            this.txtGroupID.Text = Convert.ToString(this.ugData.ActiveRow.Cells["USER_GROUP_ID"].Value);
            this.txtGroupName.Text = Convert.ToString(this.ugData.ActiveRow.Cells["USER_GROUP_NAME"].Value);
            this.txtDesc.Text = Convert.ToString(this.ugData.ActiveRow.Cells["DESCRIPTION"].Value);
            switch (Convert.ToString(this.ugData.ActiveRow.Cells["ZONE_GBN"].Value))
            {
                case "지방":
                    this.rbLocal.Checked = true;
                    break;
                case "광역":
                    this.rbWide.Checked = true;
                    break;
            }

            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_GROUP_ID", this.txtGroupID.Text);
            param.Add("ZONE_GBN", rbLocal.Checked ? "지방" : "광역");
            if (work.Menuwork.GetInstance().IsGroupMenuData(param)) //동일 USER_GROUP_ID 존재함
            {
                this.GetGroupMenu(param);
            }
            else
            {
                //param.Clear();
                param["USER_GROUP_ID"] = string.Empty;
                this.GetGroupMenu(param);
            }
        }

        /// <summary>
        /// 삭제버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// 화면오픈 : 그리드, 그리드콤보, 컨트롤 이벤트 초기화
        /// 사용자그룹 불러오기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMenu_Load(object sender, EventArgs e)
        {
            this.InitializeGridColumnSetting();
            this.InitializeValueListSetting();
            this.InitializeControlEvents();

            if (EMFrame.statics.AppStatic.USER_RIGHT.Equals("1")) //센터관리자이면 지방/광역 선택할수 없으며, 해당Zone의 그룹메뉴만 불러온다.
            {
                this.rbLocal.Enabled = false; this.rbWide.Enabled = false;
            }

            if (EMFrame.statics.AppStatic.ZONE_GBN.Equals("지방"))
                this.rbLocal.Checked = true;
            else
                this.rbWide.Checked = true;

            this.rbZone_Click(sender, e);
        }

        /// <summary>
        /// 추가버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.txtGroupName.Text = string.Empty; this.txtGroupID.Text = string.Empty; this.txtDesc.Text = string.Empty;
            if (EMFrame.statics.AppStatic.USER_RIGHT.Equals("1")) return;  //센터관리자이면 지방/광역 선택할수 없다.
            this.rbLocal.Enabled = true; this.rbWide.Enabled = true; 
        }


        /// <summary>
        /// 저장버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            this.SaveMenuData();
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_GROUP_ID", this.txtGroupID.Text);
            param.Add("ZONE_GBN", rbLocal.Checked ? "지방" : "광역");
            if (work.Menuwork.GetInstance().IsGroupMenuData(param)) //동일 USER_GROUP_ID 존재함
            {
                this.GetGroupMenu(param);
            }
            else
            {
                param["USER_GROUP_ID"] = string.Empty;
                this.GetGroupMenu(param);
            }
        }

        /// <summary>
        /// 그룹 마스타 테이블(CM_GROUP_MASTER) 
        /// </summary>
        private void GetUserGroupMaster(System.Collections.Hashtable param)
        {
            this.ugData.DataSource = work.Menuwork.GetInstance().GetUserGroupMasterData(param);
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
        }

        /// <summary>
        /// 그룹 메뉴권한 테이블(cm_group_menu)
        /// </summary>
        /// <param name="param"></param>
        private void GetGroupMenu(System.Collections.Hashtable param)
        {
            this.ugMenu.DataSource = work.Menuwork.GetInstance().GetMenuMasterData(param);
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugMenu);
        }

        /// <summary>
        /// 사용자그룹, 사용자그룹메뉴 등록
        /// </summary>
        private void SaveMenuData()
        {
            if (string.IsNullOrEmpty(this.txtGroupID.Text)) 
            {
                this.txtGroupID.Focus();
                return;
            }
            if (string.IsNullOrEmpty(this.txtGroupName.Text))
            {
                this.txtGroupName.Focus();
                return;
            }

            //사용자그룹 마스타 테이블 저장
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_GROUP_ID", this.txtGroupID.Text);
            param.Add("USER_GROUP_NAME", this.txtGroupName.Text);
            param.Add("DESCRIPTION", this.txtDesc.Text);
            param.Add("ZONE_GBN", rbLocal.Checked ? "지방" : "광역");
            param.Add("REG_UID", EMFrame.statics.AppStatic.USER_ID);

            work.Menuwork.GetInstance().SaveGroupMasterData(param);

            //사용자그룹 메뉴 권한 테이블 저장
            System.Collections.Generic.Dictionary<int, System.Collections.Hashtable> dic = new Dictionary<int, System.Collections.Hashtable>();
            foreach (Infragistics.Win.UltraWinGrid.UltraGridRow row in this.ugMenu.Rows)
            {
                System.Collections.Hashtable param2 = new System.Collections.Hashtable();
                param2.Add("USER_GROUP_ID", param["USER_GROUP_ID"]);
                param2.Add("MU_ID", row.Cells["MU_ID"].Value);
                param2.Add("MENU_AUTH", row.Cells["MENU_AUTH"].Value);
                param2.Add("ZONE_GBN", rbLocal.Checked ? "지방" : "광역");
                dic.Add(row.Index, param2);
            }

            try
            {
                work.Menuwork.GetInstance().SaveGroupMenuData(dic);
            }
            catch (Exception)
            {
                MessageBox.Show("저장에 실패하였습니다.", "안내", MessageBoxButtons.OK);
            }
        }

    }
}
