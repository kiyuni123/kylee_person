﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace WS_MenuManager.forms
{
    public partial class frmCustomMenu : EMFrame.form.BaseForm
    {
        private string USER_ID = string.Empty;

        public frmCustomMenu()
        {
            InitializeComponent();
        }

        public frmCustomMenu(string UserID)
        {
            InitializeComponent();

            USER_ID = UserID;
        }

        private void InitializeControlSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select USER_ID CD, USER_ID || '(' || USER_NAME || ')' CDNM from CM_USER WHERE USER_ID = '" + this.USER_ID + "'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboUserID, oStringBuilder.ToString(), false);
        }

        private void InitializeGridColumnSetting()
        {
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MU_ID", "메뉴ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MU_NM", "메뉴명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MENU_AUTH", "조회", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList,
                     Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MDU_VER", "버전", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MDU_UPDT", "등록일시", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "CLASS_NM", "클래스명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MDU_FLNM", "파일명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MDU_PATH", "경로", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "REMARK", "설명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            
            WS_Common.utils.gridUtils.Set_ColumeAllowEdit(this.ugData, "MENU_AUTH");

            //-----------------------------------------------------
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "CHECKED", "선택", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "USER_ID", "사용자ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "USER_NAME", "사용자명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.Set_ColumeAllowEdit(this.ugUser, "CHECKED");

            //-----------------------------------------------------
            WS_Common.utils.gridUtils.AddColumn(this.ugUserMenu, "MU_ID", "메뉴ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUserMenu, "MU_NM", "메뉴명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUserMenu, "MENU_AUTH", "조회", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUserMenu, "MDU_VER", "버전", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUserMenu, "MDU_UPDT", "등록일시", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUserMenu, "CLASS_NM", "클래스명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUserMenu, "MDU_FLNM", "파일명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUserMenu, "MDU_PATH", "경로", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUserMenu, "REMARK", "설명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            
        }

        private void InitializeValueListSetting()
        {
            object[,] aoSource = {
                {"0",  "편집허용"},
                {"1",  "조회허용"},
                {"2",  "사용안함"}
            };

            WS_Common.utils.gridUtils.SetValueList(this.ugData, "MENU_AUTH", aoSource);
            WS_Common.utils.gridUtils.SetValueList(this.ugUserMenu, "MENU_AUTH", aoSource);
        }

        private void InitializeControlEvents()
        {
            //this.ugUser.Click += new EventHandler(ugUser_Click);
            this.ugUser.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(ugUser_CellChange);
            this.btnSave.Click += new EventHandler(btnSave_Click);
        }

        void ugUser_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;

            if (e.Cell.Column.Key.Equals("CHECKED"))
            {
                this.InitializeGridCheckBox(e);

                CheckIndicatorUIElement checkindicatorUIElement = grid.DisplayLayout.UIElement.LastElementEntered as CheckIndicatorUIElement;
                if (checkindicatorUIElement != null)
                {
                    UltraGridCell cell = checkindicatorUIElement.GetContext(typeof(UltraGridCell)) as UltraGridCell;
                    if (cell != null)
                    {
                        bool cellValue;
                        if (cell.Value is DBNull)
                        {
                            cellValue = false;
                        }
                        else
                            cellValue = Convert.ToBoolean(cell.Value);

                        cell.Value = !cellValue;

                        if (!cellValue)
                        {
                            if (this.ugUserMenu.Rows.Count > 0)
                            {
                                this.ugData.DataSource = ((DataTable)this.ugUserMenu.DataSource).Copy();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 선택된 Cell을 제외한 CheckBox모두 False
        /// </summary>
        private void InitializeGridCheckBox(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            foreach (UltraGridRow  row in this.ugUser.Rows)
            {
                if (row.Index != e.Cell.Row.Index)
                {
                    row.Cells["CHECKED"].Value = false;
                }
            }
        }


        private void btnCopy_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.ugData.Rows.Count == 0) return;

            this.SetUserMenuData();
            this.GetUserMenuData();
        }

        private void frmCustomMenu_Load(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = true;

            this.InitializeControlSetting();
            this.InitializeGridColumnSetting();
            this.InitializeValueListSetting();
            this.InitializeControlEvents();

            this.GetUserMenuData();
            this.GetUserIDData();
        }

        private void ugUser_Click(object sender, EventArgs e)
        {
            if (this.ugUser.Rows.Count <= 0) return;
            if (this.ugUser.ActiveRow == null) return;

            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_ID", this.ugUser.ActiveRow.Cells["USER_ID"].Value);

            this.ugUserMenu.DataSource = work.Menuwork.GetInstance().GetUserMenuData2(param);
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugUserMenu);
       
        }

        private void GetUserMenuData()
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_ID", USER_ID);

            this.ugData.DataSource = work.Menuwork.GetInstance().GetUserMenuData(param);

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
        }

        private void GetUserIDData()
        {
            this.ugUser.DataSource = work.Menuwork.GetInstance().GetUserID(new System.Collections.Hashtable());

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugUser);

            foreach (UltraGridRow row in this.ugUser.Rows)
            {
                row.Cells["CHECKED"].Value = false;
            }
        }

        private void SetUserMenuData()
        {   
            Dictionary<int, System.Collections.Hashtable> dic = new Dictionary<int, System.Collections.Hashtable>();
            foreach (Infragistics.Win.UltraWinGrid.UltraGridRow row in this.ugData.Rows)
            {
                System.Collections.Hashtable param = new System.Collections.Hashtable();
                param.Add("USER_ID", USER_ID);
                param.Add("MU_ID", (string)row.Cells["MU_ID"].Value);
                param.Add("MENU_AUTH", (string)row.Cells["MENU_AUTH"].Value);

                dic.Add(row.Index, param);
            }
            work.Menuwork.GetInstance().SaveUserMenuData(dic);
        }

    }
}
