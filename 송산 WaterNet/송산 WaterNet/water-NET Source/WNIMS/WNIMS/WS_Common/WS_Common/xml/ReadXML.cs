﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace WS_Common.xml
{
    public class ReadXml
    {
        private XmlDocument m_doc = null;

        public ReadXml(string strFilePath)
        {
            m_doc = new XmlDocument();
            m_doc.Load(strFilePath);
        }

        public XmlDocument doc
        {
            get
            {
                return m_doc;
            }
        }

        /// <summary>
        /// XML 파일에서 xpath에 의한 값을 리턴한다.
        /// </summary>
        /// <param name="key">xpath</param>
        /// <returns>노드 값(여기서는 설정 값)</returns>
        public string getProp(string key)
        {
            string returnValue = "";

            XmlNodeList nodes = m_doc.SelectNodes(key);

            foreach (XmlNode node in nodes)
            {
                returnValue = node.InnerText;
            }

            return returnValue;
        }
    }
}
