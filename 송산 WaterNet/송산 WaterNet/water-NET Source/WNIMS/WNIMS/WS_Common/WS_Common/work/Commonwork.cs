﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace WS_Common.work
{
    public class Commonwork:EMFrame.work.BaseWork
    {
        private static Commonwork work = null;
        public static Commonwork GetInstance()
        {
            if (work == null)
            {
                work = new Commonwork();
            }

            return work;
        }

        
        /// <summary>
        /// 지역본부, 관리부서(센터) 코드를 인자로 사용자를 쿼리한다.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public DataTable GetUserInfoData2(Hashtable param)
        {
            if (!param.ContainsKey("USER_NAME"))
                param.Add("USER_NAME", string.Empty);

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("WITH MANAGER AS                                                          ");
            oStringBuilder.AppendLine("(                                                                        ");
            oStringBuilder.AppendLine(" SELECT HEAD.ROHQCD, HEAD.ROHQNM, DEP.MGDPCD, DEP.MGDPNM, DEP.ZONE_GBN   ");
            oStringBuilder.AppendLine("   FROM REGIONAL_HEADQUARTER HEAD ,                                      ");
            oStringBuilder.AppendLine("        MANAGEMENT_DEPARTMENT DEP                                        ");
            oStringBuilder.AppendLine("  WHERE HEAD.ROHQCD = DEP.ROHQCD                                         ");
            oStringBuilder.AppendLine(")                                                                        ");
            oStringBuilder.AppendLine("SELECT A.ROHQCD, A.ROHQNM, A.MGDPCD, A.MGDPNM, A.ZONE_GBN,               ");
            oStringBuilder.AppendLine("  CM_USER.USER_ID ,                                                      ");
            oStringBuilder.AppendLine("  CM_USER.USER_NAME ,                                                    ");
            oStringBuilder.AppendLine("  CM_USER.USER_PWD ,                                                     ");
            oStringBuilder.AppendLine("  CM_USER.USER_DIVISION ,                                                ");
            oStringBuilder.AppendLine("  CM_USER.USER_RIGHT ,                                                   ");
            oStringBuilder.AppendLine("  CM_USER.USE_YN ,                                                       ");
            oStringBuilder.AppendLine("  CM_USER.EXT_YN ,                                                       ");
            oStringBuilder.AppendLine("  CM_USER.L_USER_GROUP_ID,                                               ");
            oStringBuilder.AppendLine("  CM_USER.G_USER_GROUP_ID,                                               ");
            oStringBuilder.AppendLine("  CM_USER.SABUN                                                          ");
            oStringBuilder.AppendLine("  FROM MANAGER A,                                                        ");
            oStringBuilder.AppendLine("       CM_USER                                                           ");
            oStringBuilder.AppendLine(" WHERE A.MGDPCD(+) = CM_USER.MGDPCD                                      ");
            oStringBuilder.AppendLine("   AND USER_NAME LIKE :USER_NAME || '%' ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }
        
        public DataTable GetUserInfoData(Hashtable param)
        {
            if (!param.ContainsKey("USER_NAME"))
                param.Add("USER_NAME", string.Empty);

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("WITH MANAGER AS                                                          ");
            oStringBuilder.AppendLine("(                                                                        ");
            oStringBuilder.AppendLine(" SELECT HEAD.ROHQCD, HEAD.ROHQNM, DEP.MGDPCD, DEP.MGDPNM, DEP.ZONE_GBN   ");
            oStringBuilder.AppendLine("   FROM REGIONAL_HEADQUARTER HEAD ,                                      ");
            oStringBuilder.AppendLine("        MANAGEMENT_DEPARTMENT DEP                                        ");
            oStringBuilder.AppendLine("  WHERE HEAD.ROHQCD = DEP.ROHQCD                                         ");
            oStringBuilder.AppendLine(")                                                                        ");
            oStringBuilder.AppendLine("SELECT A.ROHQCD, A.ROHQNM, A.MGDPCD, A.MGDPNM, A.ZONE_GBN,               ");
            oStringBuilder.AppendLine("  CM_USER.USER_ID ,                                                      ");
            oStringBuilder.AppendLine("  CM_USER.USER_NAME ,                                                    ");
            oStringBuilder.AppendLine("  CM_USER.USER_PWD ,                                                     ");
            oStringBuilder.AppendLine("  CM_USER.USER_DIVISION ,                                                ");
            oStringBuilder.AppendLine("  CM_USER.USER_RIGHT ,                                                   ");
            oStringBuilder.AppendLine("  CM_USER.USE_YN ,                                                       ");
            oStringBuilder.AppendLine("  CM_USER.EXT_YN ,                                                       ");
            oStringBuilder.AppendLine("  CM_USER.L_USER_GROUP_ID,                                               ");
            oStringBuilder.AppendLine("  CM_USER.G_USER_GROUP_ID,                                               ");
            oStringBuilder.AppendLine("  CM_USER.SABUN                                                          ");
            oStringBuilder.AppendLine("  FROM MANAGER A,                                                        ");
            oStringBuilder.AppendLine("       CM_USER                                                           ");
            oStringBuilder.AppendLine(" WHERE A.MGDPCD(+) = CM_USER.MGDPCD                                      ");
            oStringBuilder.AppendLine("   AND A.ROHQCD = DECODE(:ROHQCD,'', A.ROHQCD, :ROHQCD          ) ");
            oStringBuilder.AppendLine("   AND A.MGDPCD = DECODE(:MGDPCD, '', A.MGDPCD, :MGDPCD           ) ");
            oStringBuilder.AppendLine("   AND USER_NAME LIKE :USER_NAME || '%' ");        

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        public DataTable GetScriptData(string strScript)
        {
            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(strScript, new IDataParameter[] { });
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }
        
    }
}
