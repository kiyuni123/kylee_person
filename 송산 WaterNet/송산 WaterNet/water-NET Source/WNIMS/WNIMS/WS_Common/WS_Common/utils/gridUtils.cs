﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Data;

namespace WS_Common.utils
{
    public class gridUtils
    {
        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="iCol">Column Index</param>
        public static void Set_ColumeAllowEdit(UltraGrid item, int iStCol, int iEdCol)
        {
            for (int i = iStCol; i <= iEdCol; i++)
            {
                if (i < item.DisplayLayout.Bands[0].Columns.Count)
                {
                    item.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
                    item.DisplayLayout.Bands[0].Columns[i].CellAppearance.BackColor = System.Drawing.Color.DarkGray;
                    item.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.EditAndSelectText;
                }

            }
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="iCol">Column Index</param>
        public static void Set_ColumeAllowEdit(UltraGrid item, int iCol)
        {
            item.DisplayLayout.Bands[0].Columns[iCol].CellActivation = Activation.AllowEdit;
            item.DisplayLayout.Bands[0].Columns[iCol].CellAppearance.BackColor = System.Drawing.Color.DarkGray;
            item.DisplayLayout.Bands[0].Columns[iCol].CellClickAction = CellClickAction.EditAndSelectText;
        }

        public static void Set_ColumeAllowEdit(UltraGrid item, string iCol)
        {
            item.DisplayLayout.Bands[0].Columns[iCol].CellActivation = Activation.AllowEdit;
            item.DisplayLayout.Bands[0].Columns[iCol].CellAppearance.BackColor = System.Drawing.Color.DarkGray;
            item.DisplayLayout.Bands[0].Columns[iCol].CellClickAction = CellClickAction.EditAndSelectText;
        }

        public static UltraGridColumn AddColumn(UltraGrid item, string key, string caption, int width, bool hidden, Infragistics.Win.UltraWinGrid.ColumnStyle style,
                                     Activation cellactivation, HAlign halign, VAlign valign)
        {
            UltraGridColumn column = AddColumn(item, key, caption, typeof(string), width, hidden, style, cellactivation, halign, valign);

            return column;

        }

        public static UltraGridColumn AddColumn(UltraGrid item, string key, string caption, System.Type datatype, int width, bool hidden, Infragistics.Win.UltraWinGrid.ColumnStyle style,
                              Activation cellactivation, HAlign halign, VAlign valign)
        {
            UltraGridColumn column = null;
            int idx = item.DisplayLayout.Bands[0].Columns.IndexOf(key);
            if (idx == -1)
            {
                column = item.DisplayLayout.Bands[0].Columns.Add();
                column.Key = key;
            }
            else
                column = item.DisplayLayout.Bands[0].Columns[idx];

            column.Header.Caption = caption;
            column.DataType = datatype;
            column.CellActivation = cellactivation;
            column.Style = style;
            column.CellAppearance.TextHAlign = halign;
            column.CellAppearance.TextVAlign = valign;
            column.Width = width;
            column.Hidden = hidden;

            return column;

        }

        public static void SetGridStyle_PerformAutoResize(UltraGrid item)
        {
            for (int I = 0; I <= item.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                item.DisplayLayout.Bands[0].Columns[I].PerformAutoResize();
            }
        }

        #region ValueList
        public static ValueList GetValueList(UltraGrid ultraGrid, string ColumnName)
        {
            ValueList oValueList = null;
            UltraGridColumn column = ultraGrid.DisplayLayout.Bands[0].Columns[ColumnName];
            if (column != null)
            {
                oValueList = column.ValueList as ValueList;
            }

            return oValueList;
        }

        public static void SetValueList(UltraGrid ultraGrid, string ColumnName, object[,] aoSource)
        {
            ValueList oValueList = new ValueList();

            for (int I = 0; I <= aoSource.GetUpperBound(0); I++)
                oValueList.ValueListItems.Add(Convert.ToString(aoSource[I, 0]).ToString(), Convert.ToString(aoSource[I, 1]).ToString());

            UltraGridColumn column = ultraGrid.DisplayLayout.Bands[0].Columns[ColumnName];
            column.ValueList = oValueList;

        }

        public static void SetValueList(UltraGrid ultraGrid, string ColumnName, DataTable table)
        {
            ValueList oValueList = new ValueList();

            foreach (System.Data.DataRow row in table.Rows)
                oValueList.ValueListItems.Add(Convert.ToString(row[0]), Convert.ToString(row[1]));

            UltraGridColumn column = ultraGrid.DisplayLayout.Bands[0].Columns[ColumnName];
            column.ValueList = oValueList;
        }

        #endregion ValueList
    }
}
