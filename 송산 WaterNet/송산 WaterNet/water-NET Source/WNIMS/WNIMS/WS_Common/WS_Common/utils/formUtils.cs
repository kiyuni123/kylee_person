﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WS_Common.utils
{
    public class formUtils
    {
        #region "콤보 상자 외부 값 설정하기 - SetComboBoxEX(cbTarget, strSqlScript, bTotal)"
        /// <summary>
        /// 콤보 상자 외부 값 설정하기
        /// </summary>
        /// <param name="cbTarget">대상 콤보 상자</param>
        /// <param name="strSqlScript">SQL 스크립트</param>
        /// <param name="bBlank">전체 추가 여부</param>
        /// <remarks></remarks>
        public static void SetComboBoxEX(System.Windows.Forms.ComboBox cbTarget, string strSqlScript, bool bBlank)
        {
            DataSet dsSource = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                dsSource = mapper.ExecuteScriptDataSet(strSqlScript,new System.Data.IDataParameter[] {});

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            DataSet dsTarget = CreateCodeDataSet();

            if (bBlank == true)
            {
                DataRow drTarget = dsTarget.Tables[0].NewRow();

                drTarget["CodeName"] = "";
                drTarget["CodeValue"] = "";
                dsTarget.Tables[0].Rows.Add(drTarget);
            }

            try
            {
                foreach (DataRow item in dsSource.Tables[0].Rows)
                {
                    DataRow drTarget = dsTarget.Tables[0].NewRow();

                    drTarget["CodeName"] = item["CD"].ToString();
                    drTarget["CodeValue"] = item["CDNM"].ToString();
                    dsTarget.Tables[0].Rows.Add(drTarget);
                }
                cbTarget.DataSource = dsTarget.Tables[0].DefaultView;
                cbTarget.DisplayMember = "CodeValue";
                cbTarget.ValueMember = "CodeName";
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
        }

        #endregion

        #region "콤보 상자 외부 값 설정하기 - SetComboBoxEX2(cbTarget, aoSource, bTotal)"

        /// <summary>
        /// 콤보 상자 사용자정의 값 설정하기
        /// </summary>
        /// <param name="cbTarget">대상 콤보 상자</param>
        /// <param name="aoSource">사용자정의 항목</param>
        /// <param name="bBlank">전체 추가 여부</param>
        /// <remarks></remarks>
        public static void SetComboBoxEX(System.Windows.Forms.ComboBox cbTarget, object[,] aoSource, bool bBlank)
        {
            DataSet dsTarget = null;

            dsTarget = CreateCodeDataSet();

            if (bBlank)
            {
                DataRow drTarget = dsTarget.Tables[0].NewRow();

                drTarget["CodeName"] = "";
                drTarget["CodeValue"] = "";
                dsTarget.Tables[0].Rows.Add(drTarget);
            }

            for (int I = 0; I <= aoSource.GetUpperBound(0); I++)
            {
                DataRow drTarget = dsTarget.Tables[0].NewRow();

                drTarget["CodeName"] = Convert.ToString(aoSource[I, 0]).ToString();
                drTarget["CodeValue"] = Convert.ToString(aoSource[I, 1]).ToString();
                dsTarget.Tables[0].Rows.Add(drTarget);
            }

            cbTarget.DataSource = dsTarget.Tables[0].DefaultView;
            cbTarget.DisplayMember = "CodeValue";
            cbTarget.ValueMember = "CodeName";
        }

        #endregion

        private static DataSet CreateCodeDataSet()
        {
            DataSet oDataSet = new DataSet();
            DataTable oDataTable = new DataTable();
            DataColumn oDataColumn = default(DataColumn);

            oDataSet.Tables.Add(oDataTable);

            oDataTable.TableName = "CodeTable";

            oDataColumn = new DataColumn("CodeName", Type.GetType("System.String"));
            oDataTable.Columns.Add(oDataColumn);

            oDataColumn = new DataColumn("CodeValue", Type.GetType("System.String"));
            oDataTable.Columns.Add(oDataColumn);

            return oDataSet;
        }

        /// <summary>
        /// 간단한 Input Dialog 보이기
        /// </summary>
        /// <param name="title"></param>
        /// <param name="promptText"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static System.Windows.Forms.DialogResult InputBox(string title, string promptText, ref string value)
        {
            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            System.Windows.Forms.Label label = new System.Windows.Forms.Label();
            System.Windows.Forms.TextBox textBox = new System.Windows.Forms.TextBox();
            System.Windows.Forms.Button buttonOk = new System.Windows.Forms.Button();
            System.Windows.Forms.Button buttonCancel = new System.Windows.Forms.Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | System.Windows.Forms.AnchorStyles.Right;
            buttonOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;

            form.ClientSize = new System.Drawing.Size(396, 107);
            form.Controls.AddRange(new System.Windows.Forms.Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new System.Drawing.Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            System.Windows.Forms.DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }
    }
}
