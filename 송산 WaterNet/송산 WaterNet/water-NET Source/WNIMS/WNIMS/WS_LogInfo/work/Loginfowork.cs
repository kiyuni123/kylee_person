﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace WS_LogInfo.work
{
    public class Loginfowork : EMFrame.work.BaseWork
    {
        private static Loginfowork work = null;
        public static Loginfowork GetInstance()
        {
            if (work == null)
            {
                work = new Loginfowork();
            }

            return work;
        }

        public DataTable GetLogHistoryData(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT A.ROHQCD                     ");
            oStringBuilder.AppendLine("      ,A.ROHQNM                     ");
            oStringBuilder.AppendLine("      ,A.MGDPCD                     ");
            oStringBuilder.AppendLine("      ,A.MGDPNM                     ");
            oStringBuilder.AppendLine("      ,A.USER_ID                    ");
            oStringBuilder.AppendLine("      ,A.USER_NAME                  ");
            oStringBuilder.AppendLine("      ,B.CON_MGDPCD                 ");
            oStringBuilder.AppendLine("      ,B.LOGIN_DT                   ");
            oStringBuilder.AppendLine("      ,B.LOGOUT_DT                  ");
            oStringBuilder.AppendLine("      ,B.LOGIN_IP                   ");
            oStringBuilder.AppendLine("      ,(case when b.logout_gbn = '9' and B.SESSION_DT<SYSDATE-1/24/60*5 then '99' else b.logout_gbn end) logout_gbn   ");
            oStringBuilder.AppendLine("  FROM V_USER A                     ");
            oStringBuilder.AppendLine("      ,LOG_INFO B                   ");
            oStringBuilder.AppendLine(" WHERE A.USER_ID = B.USER_ID        ");
            ///개발팀 부분 삭제
            //oStringBuilder.AppendLine("   AND A.ROHQCD <> '999'      ");
            oStringBuilder.AppendLine("   AND A.ROHQCD = DECODE(:ROHQCD, NULL, A.ROHQCD, :ROHQCD      )");
            oStringBuilder.AppendLine("   AND A.MGDPCD = DECODE(:MGDPCD, NULL, A.MGDPCD, :MGDPCD      )");
            oStringBuilder.AppendLine("   AND A.USER_NAME LIKE DECODE(:USER_NAME, NULL, A.USER_NAME, :USER_NAME  ) || '%'");
            oStringBuilder.AppendLine("   AND B.LOGIN_DT BETWEEN TO_DATE(:START_DAY, 'yyyymmddhh24miss') AND TO_DATE(:END_DAY, 'yyyymmddhh24miss')      ");
            oStringBuilder.AppendLine(" ORDER BY B.LOGIN_DT DESC           ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        public DataTable GetConnectedData(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT A.ROHQCD                     ");
            oStringBuilder.AppendLine("      ,A.ROHQNM                     ");
            oStringBuilder.AppendLine("      ,A.MGDPCD                     ");
            oStringBuilder.AppendLine("      ,A.MGDPNM                     ");
            oStringBuilder.AppendLine("      ,A.USER_ID                    ");
            oStringBuilder.AppendLine("      ,B.CON_MGDPCD                 ");
            oStringBuilder.AppendLine("      ,A.USER_NAME                  ");
            oStringBuilder.AppendLine("      ,B.LOGIN_DT                   ");
            //oStringBuilder.AppendLine("      ,B.LOGOUT_DT                  ");
            oStringBuilder.AppendLine("      ,B.LOGIN_IP                   ");
            //oStringBuilder.AppendLine("      ,B.LOGOUT_GBN                 ");
            oStringBuilder.AppendLine("  FROM V_USER A                     ");
            oStringBuilder.AppendLine("      ,LOG_INFO B                   ");
            oStringBuilder.AppendLine(" WHERE A.USER_ID = B.USER_ID  and logout_gbn = '9'");
            //oStringBuilder.AppendLine("   AND A.ROHQCD <> '999'      ");
            oStringBuilder.AppendLine("   AND A.ROHQCD = DECODE(:ROHQCD, NULL, A.ROHQCD, :ROHQCD      )");
            oStringBuilder.AppendLine("   AND A.MGDPCD = DECODE(:MGDPCD, NULL, A.MGDPCD, :MGDPCD      )");
            oStringBuilder.AppendLine("   AND B.LOGOUT_DT = TO_DATE('99991231', 'yyyymmdd')            ");
            oStringBuilder.AppendLine("   AND B.SESSION_DT > SYSDATE - 1/24/60 * 5                    ");
            oStringBuilder.AppendLine(" ORDER BY A.USER_ID ASC, B.LOGIN_DT DESC           ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }
    }
}
