﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WS_LogInfo.forms
{
    public partial class frmLogHistory : EMFrame.form.BaseForm
    {
        public frmLogHistory()
        {
            InitializeComponent();
        }

        private void InitializeControlSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ROHQCD CD, ROHQNM || '(' || ROHQCD || ')' CDNM from REGIONAL_HEADQUARTER");
            if (EMFrame.statics.AppStatic.USER_RIGHT.Equals("3"))
                WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), true);
            else
                WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), false);

        }

        private void InitializeGridColumnSetting()
        {
            WS_Common.utils.gridUtils.AddColumn(this.ugData,"ROHQCD","지역본부코드",50,false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit,  Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "ROHQNM", "지역본부", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MGDPCD", "센터코드", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MGDPNM", "부서명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "CON_MGDPCD", "(접속)센터", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_ID", "사용자ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_NAME", "사용자", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "LOGIN_DT", "접속일", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "LOGOUT_DT", "종료일", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "LOGIN_IP", "접속IP", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "LOGOUT_GBN", "종료", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
        }

        private void InitializeValueListSetting()
        {
            object[,] aoSource = {
                {"0",  "정상종료"},
                {"1",  "서버종료"},
                {"2",  "이중접속"},
                {"3",  "접속초과"},
                {"9",  "접속중"},
                {"99", "비정상종료"}
            };

            WS_Common.utils.gridUtils.SetValueList(this.ugData, "LOGOUT_GBN", aoSource);

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select DISTINCT MGDPCD CD, MGDPNM CDNM from MANAGEMENT_DEPARTMENT");
            DataTable datatable = WS_Common.work.Commonwork.GetInstance().GetScriptData(oStringBuilder.ToString());
            WS_Common.utils.gridUtils.SetValueList(this.ugData, "CON_MGDPCD", datatable);
        }

        private void InitializeEventSetting()
        {
            this.cboHEADQUARTER.SelectedIndexChanged += new EventHandler(cboHEADQUARTER_SelectedIndexChanged);
            //this.cboManageDept.SelectedIndexChanged += new EventHandler(cboManageDept_SelectedIndexChanged);

        }

        private void frmLogHistory_Load(object sender, EventArgs e)
        {
            this.InitializeGridColumnSetting();
            this.InitializeValueListSetting();
            this.InitializeControlSetting();
            this.InitializeEventSetting();

            this.cboHEADQUARTER_SelectedIndexChanged(cboHEADQUARTER, new EventArgs());
            //this.cboManageDept_SelectedIndexChanged(cboManageDept, new EventArgs());

            this.LoginUser_ControlSetting();

            this.btnSearch_Click(this, new EventArgs());
        }

        /// <summary>
        /// 로그인사용자의 권한에 따른 화면 컨트롤 재설정
        /// </summary>
        private void LoginUser_ControlSetting()
        {
            switch (EMFrame.statics.AppStatic.USER_RIGHT)
            {
                case "0":
                    break;
                case "1":  //센터관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.cboManageDept.Enabled = false;
                    this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;
                    break;
                case "2":  //본부관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    break;
                case "3":  //본사관리자
                    break;
            }
        }

        #region Control Events
        private void cboHEADQUARTER_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "'");

            WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept, oStringBuilder.ToString(), true);
        }

        private void cboManageDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            //StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.AppendLine("SELECT CM_USER.USER_ID CD       ");
            //oStringBuilder.AppendLine("      ,CM_USER.USER_NAME CDNM   ");
            //oStringBuilder.AppendLine("  FROM CM_USER                  ");
            //oStringBuilder.AppendLine(" WHERE CM_USER.MGDPCD in (SELECT MGDPCD FROM MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "')");
            //if (!string.IsNullOrEmpty(Convert.ToString(this.cboManageDept.SelectedValue)))
            //{
            //    oStringBuilder.AppendLine("   AND CM_USER.MGDPCD = '" + this.cboManageDept.SelectedValue + "'");
            //}


            //WS_Common.utils.formUtils.SetComboBoxEX(this.cboUser, oStringBuilder.ToString(), true);
        }

        #endregion Control Events

        private void btnSearch_Click(object sender, EventArgs e)
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("ROHQCD", this.cboHEADQUARTER.SelectedValue);
            param.Add("MGDPCD", this.cboManageDept.SelectedValue);
            //param.Add("USER_ID", this.cboUser.SelectedValue);
            param.Add("USER_NAME", this.cboUser.Text);
            param.Add("START_DAY",((DateTime)this.dt_StartDay.Value).ToString("yyyyMMddHHmmss"));
            param.Add("END_DAY", ((DateTime)this.dt_EndDay.Value).AddDays(1).AddMinutes(-1).ToString("yyyyMMddHHmmss"));

            this.ugData.DataSource = work.Loginfowork.GetInstance().GetLogHistoryData(param);

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
        }


    }
}
