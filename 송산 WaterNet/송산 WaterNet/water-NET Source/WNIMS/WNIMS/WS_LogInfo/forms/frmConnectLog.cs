﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WS_LogInfo.forms
{
    public partial class frmConnectLog : EMFrame.form.BaseForm
    {
        public frmConnectLog()
        {
            InitializeComponent();
        }

        private void InitializeControlSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ROHQCD CD, ROHQNM || '(' || ROHQCD || ')' CDNM from REGIONAL_HEADQUARTER");
            if (EMFrame.statics.AppStatic.USER_RIGHT.Equals("3"))
                WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), true);
            else
                WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), false);
        }

        private void InitializeGridColumnSetting()
        {
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "ROHQCD", "지역본부코드", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "ROHQNM", "지역본부", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MGDPCD", "센터코드", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MGDPNM", "부서명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "CON_MGDPCD", "(접속)센터", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                     Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_ID", "사용자ID", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "USER_NAME", "사용자", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "LOGIN_DT", "접속일", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "LOGIN_IP", "접속IP", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
        }


        /// <summary>
        /// 울트라그리드 밸류리스트 초기화
        /// </summary>
        private void InitializeValueListSetting()
        {
            //------------------------------------------------------------
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select DISTINCT MGDPCD CD, MGDPNM CDNM from MANAGEMENT_DEPARTMENT");
            DataTable datatable = WS_Common.work.Commonwork.GetInstance().GetScriptData(oStringBuilder.ToString());
            WS_Common.utils.gridUtils.SetValueList(this.ugData, "CON_MGDPCD", datatable);

        }

        private void frmConnectLog_Load(object sender, EventArgs e)
        {
            this.InitializeGridColumnSetting();
            this.InitializeControlSetting();
            this.InitializeValueListSetting();

            cboHEADQUARTER_SelectedIndexChanged(this, new EventArgs());

            this.LoginUser_ControlSetting();

            this.btnSearch_Click(this, new EventArgs());
        }

        /// <summary>
        /// 로그인사용자의 권한에 따른 화면 컨트롤 재설정
        /// </summary>
        private void LoginUser_ControlSetting()
        {
            switch (EMFrame.statics.AppStatic.USER_RIGHT)
            {
                case "0":
                    break;
                case "1":  //센터관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.cboManageDept.Enabled = false;
                    this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;
                    break;
                case "2":  //본부관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    break;
                case "3":  //본사관리자
                    break;
            }
        }

        #region Control Events
        private void cboHEADQUARTER_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept, oStringBuilder.ToString(), true);
        }
        #endregion Control Events

        private void btnSearch_Click(object sender, EventArgs e)
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("ROHQCD", this.cboHEADQUARTER.SelectedValue);
            param.Add("MGDPCD", this.cboManageDept.SelectedValue);

            this.ugData.DataSource = work.Loginfowork.GetInstance().GetConnectedData(param);

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
        }

    }
}
