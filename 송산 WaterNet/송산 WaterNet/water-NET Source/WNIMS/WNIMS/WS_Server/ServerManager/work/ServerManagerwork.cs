﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerManager.work
{
    public class ServerManagerwork : EMFrame.work.BaseWork
    {
        private static ServerManagerwork work = null;
        public static ServerManagerwork GetInstance()
        {
            if (work == null)
            {
                work = new ServerManagerwork();
            }

            return work;
        }


        /// <summary>
        /// 종료된 세션을 정리한다.
        /// </summary>
        /// <param name="param"></param>
        public void SetDisConnectData(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE LOG_INFO SET                         ");
            oStringBuilder.AppendLine("       LOGOUT_DT = SYSDATE                  ");
            oStringBuilder.AppendLine(" WHERE SESSION_DT < SYSDATE - 1/24/60 * 5  ");
            oStringBuilder.AppendLine("   AND LOGOUT_DT = TO_DATE('99991231','yyyymmdd')     ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

    }
}

