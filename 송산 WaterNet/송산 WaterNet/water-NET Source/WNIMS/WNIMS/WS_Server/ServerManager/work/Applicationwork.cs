﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerManager.work
{
    public class Applicationwork
    {

        public static void InitApplication()
        {
            SetDatabaseConnectString();
            SetLicense_Account();
        }



        #region 프로그램 실행시 한번만 실행 - static 변수들 설정
        /// <summary>
        /// 데이터베이스 접근경로 등록
        /// </summary>
        private static void SetDatabaseConnectString()
        {
            string conStr = string.Empty;
            conStr = "Data Source=(DESCRIPTION="
                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=175.121.89.106)(PORT=1521)))"
                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=orcl)));"
                + "User Id=stpd;Password=stpd;Enlist=false";
            EMFrame.dm.EMapper.ConnectionString.Add("STPD", conStr);

            conStr = "Data Source=(DESCRIPTION="
                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=175.121.89.106)(PORT=11493)))"
                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=waternet)));"
                + "User Id=waternetsvr;Password=waternet123;Enlist=false";
            EMFrame.dm.EMapper.ConnectionString.Add("SVR", conStr);
        }

        /// <summary>
        /// 사업장(관리부서)의 접근 라이센스수 정의
        /// </summary>
        private static void SetLicense_Account()
        {
            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT MGDPCD, ACCCNT FROM MANAGEMENT_DEPARTMENT");

                System.Data.DataTable dsSource = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });
                foreach (System.Data.DataRow row in dsSource.Rows)
                {
                    EMFrame.statics.AppStatic.LICENSE_ACCOUNT.Add(Convert.ToString(row["MGDPCD"]), Convert.ToInt32(row["ACCCNT"]));
                }

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        #endregion 프로그램 실행시 한번만 실행 - static 변수들 설정
    }
}
