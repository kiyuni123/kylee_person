﻿namespace ServerManager.forms
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.StatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsUserManager = new System.Windows.Forms.ToolStripMenuItem();
            this.tsUser = new System.Windows.Forms.ToolStripMenuItem();
            this.tsManagerDepart = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDepartment = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuManager = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tsLogManager = new System.Windows.Forms.ToolStripMenuItem();
            this.tsLogInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsConnectLogInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsVersionManager = new System.Windows.Forms.ToolStripMenuItem();
            this.tsVersion = new System.Windows.Forms.ToolStripMenuItem();
            this.tsVersionUpload = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.uTabContents = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsThisClose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsAllClose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsNthisClose = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rtMessage = new System.Windows.Forms.RichTextBox();
            this.사용자동기화ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabContents)).BeginInit();
            this.uTabContents.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel1});
            this.statusStrip.Location = new System.Drawing.Point(2, 488);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(832, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip1";
            // 
            // StatusLabel1
            // 
            this.StatusLabel1.AutoSize = false;
            this.StatusLabel1.Name = "StatusLabel1";
            this.StatusLabel1.Size = new System.Drawing.Size(300, 17);
            this.StatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.AutoSize = false;
            this.toolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.toolStripStatusLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(10, 17);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.AutoSize = false;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(400, 17);
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsUserManager,
            this.tsManagerDepart,
            this.tsMenuManager,
            this.tsLogManager,
            this.tsVersionManager});
            this.menuStrip1.Location = new System.Drawing.Point(2, 2);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(832, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsUserManager
            // 
            this.tsUserManager.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsUser,
            this.사용자동기화ToolStripMenuItem});
            this.tsUserManager.Name = "tsUserManager";
            this.tsUserManager.Size = new System.Drawing.Size(79, 20);
            this.tsUserManager.Text = "사용자관리";
            // 
            // tsUser
            // 
            this.tsUser.Name = "tsUser";
            this.tsUser.Size = new System.Drawing.Size(163, 22);
            this.tsUser.Text = "사용자등록/삭제";
            this.tsUser.Click += new System.EventHandler(this.tsUser_Click);
            // 
            // tsManagerDepart
            // 
            this.tsManagerDepart.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsDepartment});
            this.tsManagerDepart.Name = "tsManagerDepart";
            this.tsManagerDepart.Size = new System.Drawing.Size(84, 20);
            this.tsManagerDepart.Text = "관리단/센터";
            this.tsManagerDepart.Visible = false;
            // 
            // tsDepartment
            // 
            this.tsDepartment.Name = "tsDepartment";
            this.tsDepartment.Size = new System.Drawing.Size(179, 22);
            this.tsDepartment.Text = "지방센터 등록/삭제";
            this.tsDepartment.Click += new System.EventHandler(this.tsDepartment_Click);
            // 
            // tsMenuManager
            // 
            this.tsMenuManager.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenu});
            this.tsMenuManager.Name = "tsMenuManager";
            this.tsMenuManager.Size = new System.Drawing.Size(67, 20);
            this.tsMenuManager.Text = "메뉴관리";
            // 
            // tsMenu
            // 
            this.tsMenu.Name = "tsMenu";
            this.tsMenu.Size = new System.Drawing.Size(122, 22);
            this.tsMenu.Text = "메뉴관리";
            this.tsMenu.Click += new System.EventHandler(this.tsMenu_Click);
            // 
            // tsLogManager
            // 
            this.tsLogManager.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLogInfo,
            this.tsConnectLogInfo});
            this.tsLogManager.Name = "tsLogManager";
            this.tsLogManager.Size = new System.Drawing.Size(91, 20);
            this.tsLogManager.Text = "접속정보관리";
            // 
            // tsLogInfo
            // 
            this.tsLogInfo.Name = "tsLogInfo";
            this.tsLogInfo.Size = new System.Drawing.Size(162, 22);
            this.tsLogInfo.Text = "접속이력 조회";
            this.tsLogInfo.Click += new System.EventHandler(this.tsLogInfo_Click);
            // 
            // tsConnectLogInfo
            // 
            this.tsConnectLogInfo.Name = "tsConnectLogInfo";
            this.tsConnectLogInfo.Size = new System.Drawing.Size(162, 22);
            this.tsConnectLogInfo.Text = "접속사용자 조회";
            this.tsConnectLogInfo.Click += new System.EventHandler(this.tsConnectLogInfo_Click);
            // 
            // tsVersionManager
            // 
            this.tsVersionManager.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsVersion,
            this.tsVersionUpload});
            this.tsVersionManager.Name = "tsVersionManager";
            this.tsVersionManager.Size = new System.Drawing.Size(67, 20);
            this.tsVersionManager.Text = "버전관리";
            // 
            // tsVersion
            // 
            this.tsVersion.Name = "tsVersion";
            this.tsVersion.Size = new System.Drawing.Size(150, 22);
            this.tsVersion.Text = "버전정보 조회";
            this.tsVersion.Click += new System.EventHandler(this.tsVersion_Click);
            // 
            // tsVersionUpload
            // 
            this.tsVersionUpload.Name = "tsVersionUpload";
            this.tsVersionUpload.Size = new System.Drawing.Size(150, 22);
            this.tsVersionUpload.Text = "최신버전 등록";
            this.tsVersionUpload.Click += new System.EventHandler(this.tsVersionUpload_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Location = new System.Drawing.Point(2, 26);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.toolStrip1.Size = new System.Drawing.Size(832, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // uTabContents
            // 
            this.uTabContents.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabContents.Location = new System.Drawing.Point(0, 0);
            this.uTabContents.Name = "uTabContents";
            this.uTabContents.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabContents.Size = new System.Drawing.Size(832, 437);
            this.uTabContents.TabIndex = 3;
            this.uTabContents.TabPageMargins.Bottom = 3;
            this.uTabContents.TabPageMargins.Left = 3;
            this.uTabContents.TabPageMargins.Right = 3;
            this.uTabContents.TabPageMargins.Top = 3;
            this.uTabContents.TabClosed += new Infragistics.Win.UltraWinTabControl.TabClosedEventHandler(this.uTabContents_TabClosed);
            this.uTabContents.DoubleClick += new System.EventHandler(this.uTabContents_DoubleClick);
            this.uTabContents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTabContents_MouseDown);
            this.uTabContents.TabClosing += new Infragistics.Win.UltraWinTabControl.TabClosingEventHandler(this.uTabContents_TabClosing);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(4, 23);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(822, 408);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsThisClose,
            this.tsAllClose,
            this.tsNthisClose});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(195, 70);
            // 
            // tsThisClose
            // 
            this.tsThisClose.Name = "tsThisClose";
            this.tsThisClose.Size = new System.Drawing.Size(194, 22);
            this.tsThisClose.Text = "현재화면 닫기";
            this.tsThisClose.ToolTipText = "현재화면을 닫습니다.";
            this.tsThisClose.Click += new System.EventHandler(this.tsThisClose_Click);
            // 
            // tsAllClose
            // 
            this.tsAllClose.Name = "tsAllClose";
            this.tsAllClose.Size = new System.Drawing.Size(194, 22);
            this.tsAllClose.Text = "전체화면 닫기";
            this.tsAllClose.ToolTipText = "전체화면을 닫습니다.";
            this.tsAllClose.Click += new System.EventHandler(this.tsAllClose_Click);
            // 
            // tsNthisClose
            // 
            this.tsNthisClose.Name = "tsNthisClose";
            this.tsNthisClose.Size = new System.Drawing.Size(194, 22);
            this.tsNthisClose.Text = "전체닫기(현재화면 외)";
            this.tsNthisClose.ToolTipText = "전체화면(현재화면 외)를 닫습니다.";
            this.tsNthisClose.Click += new System.EventHandler(this.tsNthisClose_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(2, 51);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.uTabContents);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.rtMessage);
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(832, 437);
            this.splitContainer1.SplitterDistance = 314;
            this.splitContainer1.TabIndex = 4;
            // 
            // rtMessage
            // 
            this.rtMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtMessage.Location = new System.Drawing.Point(0, 0);
            this.rtMessage.Name = "rtMessage";
            this.rtMessage.Size = new System.Drawing.Size(150, 46);
            this.rtMessage.TabIndex = 0;
            this.rtMessage.Text = "";
            // 
            // 사용자동기화ToolStripMenuItem
            // 
            this.사용자동기화ToolStripMenuItem.Name = "사용자동기화ToolStripMenuItem";
            this.사용자동기화ToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.사용자동기화ToolStripMenuItem.Text = "사용자동기화";
            this.사용자동기화ToolStripMenuItem.Click += new System.EventHandler(this.사용자동기화ToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(836, 512);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Text = "water-Net 관리 시스템";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabContents)).EndInit();
            this.uTabContents.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabContents;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.ToolStripMenuItem tsUserManager;
        private System.Windows.Forms.ToolStripMenuItem tsManagerDepart;
        private System.Windows.Forms.ToolStripMenuItem tsMenuManager;
        private System.Windows.Forms.ToolStripMenuItem tsLogManager;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem tsThisClose;
        private System.Windows.Forms.ToolStripMenuItem tsAllClose;
        private System.Windows.Forms.ToolStripMenuItem tsNthisClose;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem tsUser;
        private System.Windows.Forms.ToolStripMenuItem tsLogInfo;
        private System.Windows.Forms.ToolStripMenuItem tsConnectLogInfo;
        private System.Windows.Forms.ToolStripMenuItem tsDepartment;
        private System.Windows.Forms.ToolStripMenuItem tsVersionManager;
        private System.Windows.Forms.ToolStripMenuItem tsVersion;
        private System.Windows.Forms.ToolStripMenuItem tsMenu;
        private System.Windows.Forms.ToolStripMenuItem tsVersionUpload;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox rtMessage;
        private System.Windows.Forms.ToolStripMenuItem 사용자동기화ToolStripMenuItem;

    }
}