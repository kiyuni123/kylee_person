﻿using System;
using System.Data;
using System.Net;
using System.Text;
using System.Windows.Forms;


namespace ServerManager.forms
{
    /// <summary>
    /// Water-NET 로그온
    /// </summary>
    public partial class frmLogOn : Form
    {
        #region 프로퍼티

        private bool _LOGON;
        private string username;
        /// <summary>
        /// 로그온 성공여부 (true 성공)
        /// </summary>
        public bool IsLogOn
        {
            get
            {
                return _LOGON;
            }
            set
            {
                _LOGON = value;
            }
        }

        #endregion

        public frmLogOn()
        {
            InitializeComponent();
        
        }

        private void InitializeControlSetting()
        {
            this.splitContainer1.Panel2Collapsed = true;
            this.Size = new System.Drawing.Size(340, 153);

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ROHQCD CD, ROHQNM || '(' || ROHQCD || ')' CDNM from REGIONAL_HEADQUARTER ORDER BY ROHQCD ASC");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), false);

        }

        private void frmLogOn_Load(object sender, EventArgs e)
        {
            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "ID.ini";

            if (!System.IO.File.Exists(strPath + strFile)) this.txtUID.Text = string.Empty;
            else
            {
                System.IO.StreamReader sr = null;
                try
                {
                    sr = System.IO.File.OpenText(strPath + strFile);
                    if (sr.Peek() >= 0)
                    {
                        this.txtUID.Text = sr.ReadLine();
                    }
                }
                catch (Exception)
                {
                    this.txtUID.Text = string.Empty;
                }
                finally
                {
                    sr.Close();
                }
            }

            this.InitializeControlSetting();

            this.cboHEADQUARTER.SelectedIndexChanged += new EventHandler(cboHEADQUARTER_SelectedIndexChanged);
        }

        private void cboHEADQUARTER_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || '-' || ZONE_GBN || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept, oStringBuilder.ToString(), false);
        }

        #region Button Events



        private void btnLogon_Click(object sender, EventArgs e)
        {
            frmDuplicateUser oform = new frmDuplicateUser(this.txtUID.Text.Trim());
            oform.TopMost = true;
            oform.StartPosition = FormStartPosition.CenterScreen;
            oform.ShowDialog(this);
            if (string.IsNullOrEmpty(oform.USERID))
            {
                MessageBox.Show("등록된 사용자가 없습니다. 다시 확인하십시오");
                return;
            }
            Console.WriteLine(oform.USERID);
            this.username = oform.USERNAME;
            //로그정보에서 중복된 사용자 체크
            _LOGON = this.LogOnProcess(oform.USERID);
            if (!_LOGON) return;
            
            ///사용자의 지역본부 및 센터를 기본으로 표시한다.
            this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
            this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;


            this.SecondLogOnProcess();

            this.Size = new System.Drawing.Size(340, 290);
        }

        private void SecondLogOnProcess()
        {
            this.splitContainer1.Panel2Collapsed = false;

            switch (EMFrame.statics.AppStatic.USER_RIGHT)
            {
                case "1":  //센터관리자
                    //로그인 성공이면, 서비스센터를 선택하는 부분 표시한다.
                    this.splitContainer1.Panel2Collapsed = true;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;
                    //센터관리자일경우, 접속버튼 클릭
                    btnConnect_Click(this, new EventArgs());
                    break;
                case "2":  //지역본부 관리자
                    //로그인 성공이면, 서비스센터를 선택하는 부분 표시한다.
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER_SelectedIndexChanged(this.cboHEADQUARTER, new EventArgs());
                    break;
                case "3":  //본사 관리자
                    //로그인 성공이면, 서비스센터를 선택하는 부분 표시한다.
                    this.cboHEADQUARTER.Enabled = true;
                    this.cboHEADQUARTER_SelectedIndexChanged(this.cboHEADQUARTER, new EventArgs());
                    break;
            }
        }

        /// <summary>
        /// 실제접속 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (this.cboHEADQUARTER.SelectedItem == null) return;
            if (this.cboManageDept.SelectedItem == null)
            {
                MessageBox.Show("접속할 서비스센터를 선택하지 않았습니다.\n\r다시 선택하십시오.","안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            StringBuilder oStringBuilder = new StringBuilder();

            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                if (mapper == null)
                {
                    MessageBox.Show(Convert.ToString(this.cboManageDept.SelectedValue) + " 데이터베이스 경로가 설정되지 않았습니다.");
                    return;
                }

                ///통합서버 로그인시 로그를 남기지 않는다.
                //System.Collections.Hashtable param = new System.Collections.Hashtable();
                //param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
                //param.Add("LOGIN_IP", EMFrame.statics.AppStatic.USER_IP);
                //param.Add("LOGIN_DT", EMFrame.statics.AppStatic.LOGIN_DT);
                //EMFrame.utils.DatabaseUtils.GetInstance().LoginInfo(param);

                oStringBuilder.AppendLine("SELECT DISTINCT b.rohqcd, b.rohqnm, a.mgdpcd, a.mgdpnm, a.data_path, a.zone_gbn FROM MANAGEMENT_DEPARTMENT a, REGIONAL_HEADQUARTER b ");
                oStringBuilder.AppendLine("WHERE a.rohqcd = b.rohqcd and a.MGDPCD = '" + this.cboManageDept.SelectedValue + "'");
                DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new IDataParameter[] { });

                if (table == null || table.Rows.Count == 0)
                {
                    EMFrame.statics.AppStatic.DATA_DIR = Application.StartupPath + @"\WaterNET-Data";
                    EMFrame.statics.AppStatic.USER_SGCNM = Convert.ToString(this.cboManageDept.SelectedText);
                    EMFrame.statics.AppStatic.USER_SGCCD = Convert.ToString(this.cboManageDept.SelectedValue);
                }
                else
                {
                    EMFrame.statics.AppStatic.DATA_DIR = @Convert.ToString(table.Rows[0]["DATA_PATH"]);
                    EMFrame.statics.AppStatic.USER_SGCCD = Convert.ToString(table.Rows[0]["MGDPCD"]);
                    EMFrame.statics.AppStatic.USER_SGCNM = Convert.ToString(table.Rows[0]["MGDPNM"]);
                    EMFrame.statics.AppStatic.USER_HEADQUARTECD = Convert.ToString(table.Rows[0]["ROHQCD"]);
                    EMFrame.statics.AppStatic.USER_HEADQUARTENM = Convert.ToString(table.Rows[0]["ROHQCD"]);
                    EMFrame.statics.AppStatic.ZONE_GBN = Convert.ToString(table.Rows[0]["ZONE_GBN"]);
                }
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _LOGON = false;
            this.Close();
        }



        #endregion


        #region Control Events

        private void txtUID_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                this.txtPWD.Focus();
            }
        }

        private void txtPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //문자 or 숫자 && Back Space && \r (Return)
            //if ((Char.IsLetterOrDigit(e.KeyChar) != true) && (e.KeyChar != 8) && (e.KeyChar != 13))
            //{
            //    e.Handled = true;
            //}

            if (e.KeyChar == 13)
            {
                this.btnLogon.Focus();
            }
        }

        #endregion


        #region User Function

        /// <summary>
        /// 로그온 성공여부
        /// </summary>
        /// <returns></returns>
        private bool LogOnProcess(string strUserID)
        {
            bool blRtn = false;
            string strEncUID = string.Empty;
            string strEncUPWD = string.Empty;

            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                //User ID는 그대로 Password는 UserID+Password로 인크립트
                strEncUID = strUserID; // this.txtUID.Text.Trim();
                strEncUPWD = EMFrame.utils.ConvertUtils.AESEncrypt256(username.Trim() + this.txtPWD.Text.Trim());

                //Console.WriteLine(strEncUPWD);
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT * FROM V_USER                   ");
                oStringBuilder.AppendLine(" WHERE USER_ID = '" + strEncUID + "'"   );
                oStringBuilder.AppendLine("   AND USER_PWD = '" + strEncUPWD + "'");
                oStringBuilder.AppendLine("   AND USE_YN = '1'");

                DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });

                if (table.Rows.Count == 0)
                {
                    MessageBox.Show("Water-NET 사용자 ID 및 비밀번호를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (table.Rows[0]["USER_RIGHT"].ToString().Equals("0"))
                    {
                        MessageBox.Show("일반운영자는 관리시스템을 사용할수 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        EMFrame.statics.AppStatic.ZONE_GBN = table.Rows[0]["ZONE_GBN"].ToString();
                        EMFrame.statics.AppStatic.USER_ID = this.txtUID.Text.Trim();
                        EMFrame.statics.AppStatic.USER_NAME = table.Rows[0]["USER_NAME"].ToString();
                        EMFrame.statics.AppStatic.USER_DIVISION = table.Rows[0]["USER_DIVISION"].ToString();
                        EMFrame.statics.AppStatic.USER_RIGHT = table.Rows[0]["USER_RIGHT"].ToString();
                        switch ((string)EMFrame.statics.AppStatic.ZONE_GBN)
                        {
                            case "지방":
                                EMFrame.statics.AppStatic.USER_GROUP = table.Rows[0]["L_USER_GROUP_ID"].ToString();
                                break;
                            case "광역":
                                EMFrame.statics.AppStatic.USER_GROUP = table.Rows[0]["G_USER_GROUP_ID"].ToString();
                                break;
                        }
                        
                        EMFrame.statics.AppStatic.EXT_YN = table.Rows[0]["EXT_YN"].ToString();
                        EMFrame.statics.AppStatic.USER_HEADQUARTECD = table.Rows[0]["ROHQCD"].ToString();
                        EMFrame.statics.AppStatic.USER_HEADQUARTENM = table.Rows[0]["ROHQNM"].ToString();
                        EMFrame.statics.AppStatic.USER_SGCCD = table.Rows[0]["MGDPCD"].ToString();
                        EMFrame.statics.AppStatic.USER_SGCNM = table.Rows[0]["MGDPNM"].ToString();
                        EMFrame.statics.AppStatic.LOGIN_DT = DateTime.Now.ToString("yyyyMMddHHmmss");                        

                        //Dns.GetHostName();
                        IPHostEntry MyIPHostEntry = Dns.GetHostEntry(Dns.GetHostName());
                        foreach (IPAddress MyIpAddress in MyIPHostEntry.AddressList)
                        {
                            if (MyIpAddress.IsIPv6LinkLocal != true) //IPv4의 IP인 경우 즉, IPv6가 아닌 경우
                            {
                                EMFrame.statics.AppStatic.USER_IP = MyIpAddress.ToString();
                                break;
                            }
                        }

                        blRtn = true;
                    }
                }

                EMFrame.utils.LoggerUtils.logger.Info("로그인");
            }
            catch (Exception oException)
            {
                
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return blRtn;
        }

        /// <summary>
        /// 중복접속 사용자를 체크한다.
        /// 나중에 네트워크로 접속하는 방안으로 변경필요.
        /// </summary>
        private void DuplicateIsLogin()
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("USER_ID", this.txtUID.Text.Trim());

            System.Data.DataTable datatable = EMFrame.utils.DatabaseUtils.GetInstance().IsBeforeLogin(param);

            if (datatable.Rows.Count > 0)
            {
                MessageBox.Show(this.txtUID.Text.Trim() + " 는 이미 로그인되어 있습니다." + "\n\r" +
                                "기존 로그인 정보를 삭제합니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);

                EMFrame.utils.DatabaseUtils.GetInstance().DisableBeforeLogin(param);
            }
        }

        /// <summary>
        /// 지자체, 사업장, 공정, 시험항목 등 Data를 받아 코드 부분만 추출해서 리턴
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) 산정정수장 (001) 중 001을 반환</param>
        /// <returns>string Code</returns>
        private string SplitToCode(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[1].Substring(0, strArr[1].Length - 1);
            }
            else
            {
                if (strArr[0].Trim() == "전체")
                {
                    strCode = " ";
                }
                else
                {
                    strCode = strArr[0].Trim();
                }
            }

            return strCode;
        }

        /// <summary>
        /// 지자체, 사업장, 공정, 시험항목 등 Data를 받아 코드명 부분만 추출해서 리턴
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) 산정정수장 (001) 중 산정정수장을 반환</param>
        /// <returns>string Code Name</returns>
        private string SplitToCodeName(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[0].Substring(0, strArr[0].Length);
            }
            else
            {
                strCode = strArr[0].Trim();
            }

            return strCode;
        }

        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtUID.Text.Trim()))
            {
                MessageBox.Show(" 사용자ID가 입력되지 않았습니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "ID.ini";

            System.IO.FileStream oFileStream = null;
            try
            {
                oFileStream = new System.IO.FileStream(strPath + strFile, System.IO.FileMode.Create);

                oFileStream.Write(UTF8Encoding.UTF8.GetBytes(this.txtUID.Text.Trim()), 0, UTF8Encoding.UTF8.GetByteCount(this.txtUID.Text.Trim()));
                oFileStream.Flush();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                oFileStream.Close();
                oFileStream.Dispose();
            }
        }


    }
}
