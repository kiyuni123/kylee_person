﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

#region Infragistics
using Infragistics.Win.UltraWinTabControl;
#endregion Infragistics

namespace ServerManager.forms
{
    public partial class frmMain : EMFrame.form.BaseForm
    {
        //private delegate void PrintLogMessage(string value);
        ////백그라운드 네트워크 서버 : 클라이언트와 네트워크 통신
        //private ServerManager.Net.ServerListener server = null; 

        public frmMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 폼 초기화설정
        /// </summary>
        private void InitializeSetting()
        {
            uTabContents.CloseButtonLocation = Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None;

            tsAllClose.MouseEnter += new EventHandler(ContextMenu_MouseEnter);
            tsNthisClose.MouseEnter += new EventHandler(ContextMenu_MouseEnter);
            tsThisClose.MouseEnter += new EventHandler(ContextMenu_MouseEnter);

            tsUser.MouseEnter += new EventHandler(ContextMenu_MouseEnter);
            tsLogInfo.MouseEnter += new EventHandler(ContextMenu_MouseEnter);
            tsConnectLogInfo.MouseEnter += new EventHandler(ContextMenu_MouseEnter);
            tsDepartment.MouseEnter += new EventHandler(ContextMenu_MouseEnter);
            tsVersion.MouseEnter += new EventHandler(ContextMenu_MouseEnter);
            tsMenu.MouseEnter += new EventHandler(ContextMenu_MouseEnter);
            tsVersionUpload.MouseEnter += new EventHandler(ContextMenu_MouseEnter);

            this.KeyDown += new KeyEventHandler(frmMain_KeyDown);
        }


        //public void GetReceiveMessage(string msg)
        //{
        //    if (this.rtMessage == null) return;

        //    try
        //    {
        //        ///서로다른 프로세스에서 객체를 크로스로 접근하면 예외가 발생하므로 
        //        ///이를 해결하기 위해 Invoke 메소드를 사용하게 된다.
        //        ///진행바가 현재 Invoke가 필요한 상태인지 파악하여 필요하다면 대기상태에 있다가
        //        ///접근가능할때 백그라운드 작업을 진행하고, 필요한 상태가 아니라면
        //        ///진행바의 해당 속성에 바로 대입한다.
        //        if (this.rtMessage.InvokeRequired)
        //        {
        //            //delegate 생성
        //            PrintLogMessage dele = new PrintLogMessage(GetReceiveMessage);
        //            //대기상태에 있다가 접근가능한 상태일때 SetProgBar 간접호출
        //            this.rtMessage.Invoke(dele, new object[] { msg });
        //        }
        //        else
        //        {
        //            this.rtMessage.AppendText(msg + "\r");
        //            this.rtMessage.ScrollToCaret();
        //        }
        //    }
        //    catch { }

        //}


        /// <summary>
        /// 메뉴 선택시 상태바에 메세지 표시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextMenu_MouseEnter(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                switch (((ToolStripMenuItem)sender).Name)
                {
                    case "tsAllClose":
                        this.StatusLabel1.Text = "전체화면을 닫습니다.";
                        break;
                    case "tsNthisClose":
                        this.StatusLabel1.Text = "전체화면(현재화면제외)를 닫습니다.";
                        break;
                    case "tsThisClose":
                        this.StatusLabel1.Text = "현재화면을 닫습니다.";
                        break;
                    case "tsUser":
                        this.StatusLabel1.Text = "사용자를 등록 및 삭제합니다.";
                        break;
                    case "tsLogInfo":
                        this.StatusLabel1.Text = "접속이력정보를 조회합니다.";
                        break;
                    case "tsConnectLogInfo":
                        this.StatusLabel1.Text = "현재 접속된 사용자정보를 조회합니다.";
                        break;
                    case "tsDepartment":
                        this.StatusLabel1.Text = "지방센터 및 관리단을 등록 및 삭제합니다.";
                        break;
                    case "tsVersion":
                        this.StatusLabel1.Text = "버전정보를 조회합니다.";
                        break;
                    case "tsMenu": 
                        this.StatusLabel1.Text = "메뉴정보를 등록 및 삭제합니다.";
                        break;
                    case "tsVersionUpload": 
                        this.StatusLabel1.Text = "Shape 및 시스템파일을 등록합니다.";
                        break;
                }
            }
        }

        #region uTabContents-화면표시, 지우기
        /// <summary>
        /// 화면보이기-탭
        /// </summary>
        /// <param name="_form"></param>
        private void ShowForm(Form _form)
        {
            Infragistics.Win.UltraWinTabControl.UltraTab tab = null;

            if (this.uTabContents.Tabs.IndexOf(_form.GetType().ToString()) == -1)
            {
                tab = this.uTabContents.Tabs.Add(_form.GetType().ToString(), _form.Text);

                _form.FormBorderStyle = FormBorderStyle.None;
                _form.TopLevel = false;
                _form.Dock = DockStyle.Fill;
                _form.Parent = tab.TabPage;
                _form.Show();

            }
            else
            {
                tab = this.uTabContents.Tabs[_form.GetType().ToString()];
            }

            
            this.uTabContents.SelectedTab = tab;
        }

        /// <summary>
        /// 화면지우기-탭
        /// 해당 폼에 스레드가 동작중일때는 종료하지 못함
        /// </summary>
        /// <param name="key"></param>
        private bool RemoveForm(string key)
        {
            bool result = true;
            if (this.uTabContents.Tabs.IndexOf(key) != -1)
            {
                foreach (Control ctr in this.uTabContents.Tabs[key].TabPage.Controls)
                {
                    if (ctr is Form && ctr is EMFrame.form.BaseForm)
                    {
                        Form _form = ctr as Form;
                        if (!((EMFrame.form.BaseForm)_form).isCloseEnable) return false;

                        _form.Close();
                        _form.Dispose();
                    }
                }

                this.uTabContents.Tabs.Remove(this.uTabContents.Tabs[key]);
            }

            return result;
        }

        /// <summary>
        /// 탭 지우기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTabContents_TabClosed(object sender, TabClosedEventArgs e)
        {
            e.Tab.Dispose();
        }

        /// <summary>
        /// 탭 지우기-화면지우기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTabContents_TabClosing(object sender, TabClosingEventArgs e)
        {
            bool result = this.RemoveForm(e.Tab.Key);
            if (!result) e.Cancel = true;
        }

        /// <summary>
        /// 팝업메뉴 표시-화면지우기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTabContents_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.uTabContents.Tabs.Count == 0) return;
            if (e.Button == MouseButtons.Right) contextMenu.Show(this.uTabContents, e.Location);
        }

        /// <summary>
        /// 현재화면 지우기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsThisClose_Click(object sender, EventArgs e)
        {
            this.uTabContents.SelectedTab.Close();
        }

        /// <summary>
        /// 전체화면 지우기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsAllClose_Click(object sender, EventArgs e)
        {
            for (int i = this.uTabContents.Tabs.Count - 1; i >= 0; i--)
            {
                this.uTabContents.Tabs[i].Close();
            }

            this.uTabContents.Tabs.Clear();
        }

        /// <summary>
        /// 전체화면-현재화면외 지우기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsNthisClose_Click(object sender, EventArgs e)
        {
            for (int i = this.uTabContents.Tabs.Count - 1; i >= 0; i--)
            {
                UltraTab tab = uTabContents.Tabs[i];
                if (tab.Equals(uTabContents.SelectedTab)) continue;

                tab.Close();
            }
        }


        #endregion uTabContents-화면표시, 지우기


        #region 메뉴실행
        /// <summary>
        /// 사용자 등록 및 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsUser_Click(object sender, EventArgs e)
        {
            Form oform = new WS_UserManager.forms.frmUserManager();
            this.ShowForm(oform);
        }

        private void tsLogInfo_Click(object sender, EventArgs e)
        {
            Form oform = new WS_LogInfo.forms.frmLogHistory();
            this.ShowForm(oform);
        }

        private void tsConnectLogInfo_Click(object sender, EventArgs e)
        {
            Form oform = new WS_LogInfo.forms.frmConnectLog();
            this.ShowForm(oform);
        }

        private void tsDepartment_Click(object sender, EventArgs e)
        {
            Form oform = new WS_Department.forms.frmDepartment();
            this.ShowForm(oform);
        }

        private void tsVersion_Click(object sender, EventArgs e)
        {
            Form oform = new WS_VersionManager.forms.frmVersionHistory();
            this.ShowForm(oform);
        }

        private void tsVersionUpload_Click(object sender, EventArgs e)
        {
            Form oform = new WS_VersionManager.forms.frmVersion();
            this.ShowForm(oform);
        }

        private void tsMenu_Click(object sender, EventArgs e)
        {
            Form oform = new WS_MenuManager.forms.frmMenu();
            this.ShowForm(oform);
        }

        private void 사용자동기화ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form oform = new WS_UserManager.forms.frmUserSync();
            this.ShowForm(oform);
        }

        #endregion 메뉴실행

        #region 폼 실행 및 종료
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeSetting();
            this.WindowState = FormWindowState.Maximized;
            //server = new ServerManager.Net.ServerListener(this, (int)3000);
            //server.ServerStart();

        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Invoke(new MethodInvoker(() =>
            {
                Application.ExitThread();
                Application.Exit();
            }));
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((MessageBox.Show("프로그램을 종료하시겠습니까?","종료", MessageBoxButtons.YesNo, MessageBoxIcon.Information,  MessageBoxDefaultButton.Button1)) != DialogResult.Yes)
            {
                e.Cancel = true;
                return;
            }

            /////백그라운드 네트워크 소켓 서버 종료
            //server.Dispose();

            //System.Collections.Hashtable param = new System.Collections.Hashtable();
            //param.Add("USER_ID", EMFrame.statics.AppStatic.USER_ID);
            //param.Add("LOGIN_DT", EMFrame.statics.AppStatic.LOGIN_DT);
            //EMFrame.utils.DatabaseUtils.GetInstance().SuccessLogoutInfo(param);

        }
        #endregion 폼 실행 및 종료


        bool key = false;
        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            key = false;
            if (e.Control && e.Shift)  key = true;
        }

        private void uTabContents_DoubleClick(object sender, EventArgs e)
        {
            if (key)
            {
                this.splitContainer1.Panel2Collapsed = !this.splitContainer1.Panel2Collapsed;
            }
            key = false;
        }

        
    }
}
