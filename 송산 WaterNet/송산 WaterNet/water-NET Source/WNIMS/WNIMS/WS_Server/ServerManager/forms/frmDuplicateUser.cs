﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace ServerManager.forms
{
    public partial class frmDuplicateUser : Form
    {
        private string _username = string.Empty;
        private string _userid = string.Empty;
        public frmDuplicateUser(string userid)
        {
            InitializeComponent();
            this._userid = userid;
        }

        public string USERID
        {
            get { return this._userid; }
        }

        public string USERNAME
        {
            get { return this._username; }
        }

        private void InitializeGridColumnSetting()
        {
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "USER_NAME", "사용자", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "USER_ID", "사번", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "ROHQNM", "지역본부", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugUser, "MGDPNM", "서비스센터", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);

            this.SetGridStyle(this.ugUser);
        }

        private void SetGridStyle(UltraGrid ugList)
        {
            ugList.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            ugList.DisplayLayout.GroupByBox.BorderStyle = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.GroupByBox.Hidden = true;
            //ugList.DisplayLayout.UseFixedHeaders = true;
            ugList.DisplayLayout.MaxColScrollRegions = 1;
            ugList.DisplayLayout.MaxRowScrollRegions = 1;

            ugList.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
            ugList.DisplayLayout.Override.CellPadding = 0;
            ugList.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            ugList.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;
            ugList.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            ugList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            ugList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            ugList.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            ugList.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            ugList.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;

            ugList.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            ugList.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;
            ugList.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.Default;
            //DSOH 추가. Row Select는 무조건 Single만 되고, Drag가 되지 않게.
            ugList.DisplayLayout.Override.SelectTypeRow = SelectType.SingleAutoDrag;

            ugList.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;
            ugList.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            ugList.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            ugList.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            ugList.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            ugList.Font = new Font("굴림", 9f, FontStyle.Regular, GraphicsUnit.Point, 129);

            for (int I = 0; I <= ugList.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.BackGradientStyle = GradientStyle.None;
                ugList.DisplayLayout.Bands[0].Columns[I].CellActivation = Activation.NoEdit;
            }
        }

        private void frmDuplicateUser_Load(object sender, EventArgs e)
        {
            this.InitializeGridColumnSetting();

            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                //Console.WriteLine(strEncUPWD);
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT user_name, user_id, rohqnm, mgdpnm FROM V_USER                   ");
                oStringBuilder.AppendLine(" WHERE USER_ID = '" + this._userid + "'");
                oStringBuilder.AppendLine("   AND USE_YN = '1'");

                DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });
                if (table.Rows.Count == 0)
                {
                    this._userid = string.Empty;
                    this.Close();
                }
                else if (table.Rows.Count == 1)
                {
                    this._username = Convert.ToString(table.Rows[0]["USER_NAME"]);
                    this.Close();
                }
                this.ugUser.DataSource = table;
            }
            catch (Exception oException)
            {

                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            this._userid = Convert.ToString(this.ugUser.ActiveRow.Cells["USER_ID"].Value);
            this.Close();
        }
    }
}
