﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerManager.Dao
{
    public class ServerManagerDao
    {
        private static ServerManagerDao dao = null;
        public static ServerManagerDao GetInstance()
        {
            if (dao == null)
            {
                dao = new ServerManagerDao();
            }
            return dao;
        }
    }
}
