﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace ServerManager.Net
{
    public delegate void Send_Message(object sender);
    public delegate void Close_Message(object sender);

    public class ServerListener : IDisposable
    {
        private TcpListener server = null;
        private forms.frmMain wnd = null;
        private Thread th = null;
        //private ClientGroup cgroup = null;
        private int port = 3000;

        public ServerListener(forms.frmMain wnd, int port)
        {
            this.wnd = wnd;
            this.port = port;
        }

        #region IDisposable 멤버
        public void Dispose()
        {
            try
            {
                this.BroadCast("C_SERVER_OUT:통합서버 시스템이 종료합니다.\n\r클라이언트 시스템을 종료하십시오.");
                //if (cgroup != null) cgroup.Dispose();
                if (th != null && th.IsAlive) th.Abort();
                if (server != null) this.server.Stop();

            }
            catch { }
        }

        #endregion

        public void ServerStart()
        {
            try
            {
                //if (cgroup == null) cgroup = ClientGroup.GetInstance();
                th = new Thread(new ThreadStart(StartListener));
                th.IsBackground = true;
                th.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ServerStop()
        {
            try
            {
                //cgroup.Dispose();
                if (th.IsAlive) th.Abort();
                this.server.Stop();

            }
            catch { }
        }

        public void StartListener()
        {
            //try
            //{
            //    server = new TcpListener(IPAddress.Any, this.port);
            //    server.Start();
            //    //this.wnd.GetReceiveMessage("[접속대기중]");

            //    while (true)
            //    {
            //        TcpClient client = server.AcceptTcpClient();
            //        IPEndPoint ip = (IPEndPoint)client.Client.RemoteEndPoint;
            //        this.wnd.GetReceiveMessage("[클라이언트] :" + ip.Address.ToString() + " : " + ip.Port.ToString());

            //        if (client.Connected)  //클라이언트가 접속하면
            //        {
            //            Client obj = new Client(this.wnd, client); //Client Class 객체 생성
            //            //cgroup.AddMember(obj);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    this.wnd.GetReceiveMessage(ex.ToString());
            //}
        }

        public bool BroadCast(string msg)
        {
            bool result = false;

            //string[] token = msg.Split(':', ',');
            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                foreach (System.Collections.Hashtable param in ConnectedClients.GetInstance().ConnectedClient.Values)
                {
                    Client client = (Client)param["SOCKET"];
                    if (client != null && client.Connect)
                    {
                        client.Send(msg);    
                    }
                }
                result = true;
            }
            return result;
        }

        public bool BroadCast(string msg, string userid)
        {
            bool result = false;

            string[] token = msg.Split(':', ',');
            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                foreach (System.Collections.Hashtable param in ConnectedClients.GetInstance().ConnectedClient.Values)
                {
                    if (((string)param["USER_ID"]).Equals(userid))
                    {
                        Client client = (Client)param["SOCKET"];
                        if (client != null && client.Connect)
                        {
                            client.Send(msg);
                        }
                        break;
                    }

                }
                result = true;
            }
            return result;
        }
    }

    //public class ClientGroup : IDisposable
    //{
    //    private static ClientGroup clientGroup = null;
    //    private System.Collections.ArrayList member = null;
    //    public static ClientGroup GetInstance()
    //    {
    //        if (clientGroup == null)
    //        {
    //            clientGroup = new ClientGroup();
    //        }

    //        return clientGroup;
    //    }

    //    private ClientGroup()
    //    {
    //        if (member == null) member = new System.Collections.ArrayList();
    //    }

    //    #region IDisposable 멤버
    //    public void Dispose()
    //    {
    //        foreach (Client obj in member)
    //        {
    //            obj.Dispose();
    //        }
    //    }

    //    #endregion

    //    public int Length
    //    {
    //        get
    //        {
    //            return member.Count;
    //        }
    //    }

    //    public void AddMember(Client client)
    //    {
    //        client.Send_All += new Send_Message(Send_All);
    //        client.Close_MSG += new Close_Message(Close_MSG);
    //        //client.msg_queue = this.msg_queue;
    //        member.Add(client);
    //    }

    //    public void DeleteMember(string ip)
    //    {
    //        int index = -1;
    //        foreach (Client obj in member)
    //        {
    //            if (obj.Client_IP == ip)
    //            {
    //                member.RemoveAt(index);
    //                obj.Dispose();

    //                break;
    //            }
    //            index++;
    //        }
    //    }

    //    public void Send_All(object sender)
    //    {
    //        string msg = string.Empty;
    //        foreach (Client obj in member)
    //        {
    //            if (obj != sender)
    //            {
    //                obj.Send(msg);
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// 서버에 접속한 클라이언트 개체 제거(접속종료)
    //    /// </summary>
    //    /// <param name="sender"></param>
    //    public void Close_MSG(object sender)
    //    {
    //        this.DeleteMember(((Client)sender).Client_IP);
    //    }

    //    public bool BroadCast(string msg)
    //    {
    //        try
    //        {
    //            lock (member)
    //            {
    //                foreach (Client obj in member)
    //                {
    //                    if (obj.Connect)
    //                    {
    //                        obj.Send(msg);
    //                    }
    //                }
    //            }
    //            return true;
    //        }
    //        catch (Exception)
    //        {   
    //            return false;
    //        }
    //    }

    //    public bool BroadCast(string msg, string ips)
    //    {
    //        try
    //        {
    //            string[] ip = ips.Split(';');
    //            for (int i = 0; i < ip.Length; i++)
    //            {
    //                lock (member)
    //                {
    //                    foreach (Client obj in member)
    //                    {
    //                        if (obj.Client_IP == ip[i] && obj.Connect)
    //                        {
    //                            obj.Send(msg);
    //                        }
    //                    }
    //                }
    //            }

    //            return true;
    //        }
    //        catch (Exception)
    //        {
    //            return false;
    //        }
    //    }


    //}

    public class Client : IDisposable
    {
        private TcpClient client = null;
        private string client_ip = string.Empty;
        private Thread th = null;
        private ServerManager.forms.frmMain wnd = null;

        private NetworkStream stream = null;
        private StreamReader reader = null;
        private StreamWriter writer = null;

        //public event Send_Message Send_All;
        //public event Close_Message Close_MSG;
        //public Msg_Queue msg_queue = null;

        public Client(ServerManager.forms.frmMain wnd, TcpClient socket)
        {
            this.wnd = wnd;
            this.client = socket;
            this.stream = this.client.GetStream();
            this.reader = new StreamReader(stream);
            this.writer = new StreamWriter(stream);

            IPEndPoint ip = (IPEndPoint)this.client.Client.RemoteEndPoint;
            this.client_ip = ip.Address.ToString();

            try    //클라이언트가 보내는 데이터 수신할 스레드 생성
            {
                th = new Thread(new ThreadStart(Receive));
                th.IsBackground = true;
                th.Start();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool Connect
        {
            get
            {
                return this.client.Connected;
            }
        }

        public string Client_IP
        {
            get
            {
                return this.client_ip;
            }
        }

        /// <summary>
        /// 클라이언트가 보내는 메세지 수신하기
        /// </summary>
        public void Receive()
        {
            try
            {
                while (client != null && client.Connected)
                {
                    string msg = reader.ReadLine();
                    string[] token = msg.Split(':');

                    switch (token[0])
                    {
                        case "C_REQ_DUAL_LOGIN": //이중접속
                            this.wnd.GetReceiveMessage("이중접속 : " + msg + "," + ConnectedClients.GetInstance().ConnectedClient.Count.ToString());
                            
                            break;
                        case "C_REQ_REFRESH":    //클라이언트 실행중
                            activeRefresh(msg);
                            this.Send(msg);      //클라이언트에게 리턴메세지 전송
                            this.wnd.GetReceiveMessage("실행중 : " + msg + "," + ConnectedClients.GetInstance().ConnectedClient.Count.ToString());
                            break;
                        case "C_REQ_LOGIN":    //클라이언트 접속
                            //센터에 허용된 라이센스 수 체크
                            if (ProcessLicenseCheck(msg))
                            {
                                this.Send("C_REQ_MAXIMUM_OUT:" + "허용된 라이센스를 초과하였습니다.");
                                break;
                            }
                            //클라이언트에서 접속했을 경우, 이중접속 여부를 판단하여
                            //서버에서 동일사용자로 로그인한 이전 클라이언트로 메세지를 보낸다.
                            //클라이언트에서 확인후 시스템종료한다.
                            System.Collections.Hashtable param = this.IsDuplicateLogin(msg);  //기 접속된 사용자 체크하여, 접속목록에서 삭제
                            if (param != null)  DestoryBeforeLogin(msg, param); //이중접속시 이전 접속클라이언트에게 메세지 전송

                            ProcessNewLogin(msg); //새로운 사용자 접속
                            this.Send(msg);  //클라이언트에게 리턴메세지 전송
                            this.wnd.GetReceiveMessage("접속 : " + msg + "," + ConnectedClients.GetInstance().ConnectedClient.Count.ToString());
                            break;
                        case "C_REQ_LOGOUT":    //클라이언트 접속종료
                            this.ProcessLogout(msg);
                            this.wnd.GetReceiveMessage("접속종료 : " + msg + "," + ConnectedClients.GetInstance().ConnectedClient.Count.ToString());
                            break;
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 접속한 상대방에 데이터 전송
        /// </summary>
        /// <param name="msg"></param>
        public void Send(string msg)
        {
            try
            {
                if (client != null && client.Connected)  //상대방과 연결되어 있는경우
                {
                    //this.wnd.GetSendMessage(msg);
                    writer.WriteLine(msg);
                    writer.Flush();
                }
                else
                {
                    ;
                }
            }
            catch (Exception ex)
            {
                ;
                //this.wnd.GetReceiveMessage(ex.ToString());
            }
        }

        private void activeRefresh(string msg)
        {
            string[] token = msg.Split(':', ',');
            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                if (ConnectedClients.GetInstance().ConnectedClient.ContainsKey(token[1]))  
                {
                    System.Collections.Hashtable param = ConnectedClients.GetInstance().ConnectedClient[token[1]];
                    if (!param.ContainsKey("SESSION_DT")) param.Add("SESSION_DT", DateTime.Now);
                    else param["SESSION_DT"] = DateTime.Now;
                }
            }
        }

        private void ProcessLogout(string msg)
        {
            string[] token = msg.Split(':', ',');
            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                if (ConnectedClients.GetInstance().ConnectedClient.ContainsKey(token[1]))  
                {
                    ConnectedClients.GetInstance().ConnectedClient.Remove(token[1]); 
                }
            }
        }

        /// <summary>
        /// 이중접속시 이전 접속된 시스템을 종료하기 위해 클라이너트에게 메세지 전송
        /// 이중접속 메세지를 클라이언트에 전송한다.=>클라이언트는 접속을 종료하고, 시스템을 종료한다.
        /// 이중접속 메세지 : 현재 접속된 ID, IP, 시간, 센터코드 포함
        /// </summary>
        /// <param name="param"></param>
        private void DestoryBeforeLogin(string msg, System.Collections.Hashtable param)
        {
            string[] token = msg.Split(':', ',');

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("C_REQ_DUAL_LOGIN" + ":");
            oStringBuilder.Append(token[1] + ",");
            oStringBuilder.Append(token[2] + ",");
            oStringBuilder.Append(token[3] + ",");
            oStringBuilder.Append(token[4]);

            Client remote = (Client)param["SOCKET"];
            remote.Send(oStringBuilder.ToString());
        }

        /// <summary>
        /// 접속사용자 목록에서 동일사용자 접속여부 체크
        /// 동일사용자 있을경우 목록에서 삭제
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private System.Collections.Hashtable IsDuplicateLogin(string msg)
        {
            System.Collections.Hashtable param = null;
            string[] token = msg.Split(':', ',');
            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                if (ConnectedClients.GetInstance().ConnectedClient.ContainsKey(token[1]))  //중복로그인
                {
                    param = ConnectedClients.GetInstance().ConnectedClient[token[1]];

                    ConnectedClients.GetInstance().ConnectedClient.Remove(token[1]); //기존 로그인 정보 삭제
                }
            }

            return param;

        }

        /// <summary>
        /// 새로운 사용자를 접속목록에 추가하고
        /// 접속한 소켓을 저장한다.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private System.Collections.Hashtable ProcessNewLogin(string msg)
        {
            System.Collections.Hashtable param = null;
            string[] token = msg.Split(':', ',');
            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                param = new System.Collections.Hashtable();
                param.Add("USER_ID", token[1]);
                param.Add("LOGIN_IP", token[2]);
                param.Add("LOGIN_DT", token[3]);
                param.Add("SGCCD", token[4]);
                param.Add("SOCKET", this);
                ConnectedClients.GetInstance().ConnectedClient.Add(token[1], param);
            }

            return param;
        }

        private bool ProcessLicenseCheck(string msg)
        {
            bool result = false;
            string[] token = msg.Split(':', ',');
            int license = -1; int connected = 0;
            lock (EMFrame.statics.AppStatic.LICENSE_ACCOUNT)
            {
                license = EMFrame.statics.AppStatic.LICENSE_ACCOUNT[token[4]];
            }

            lock (ConnectedClients.GetInstance().ConnectedClient)
            {
                foreach (System.Collections.Hashtable param in ConnectedClients.GetInstance().ConnectedClient.Values)
                {
                    if (Convert.ToString(param["SGCCD"]).Equals(token[4]))
                    {
                        connected++;
                    }
                }
            }

            if (license < connected++)
            {
                result = true;
            }

            return result;
        }

        #region IDisposable 멤버
        public void Dispose()
        {
            try
            {
                if (this.reader != null) reader.Close();
                if (this.writer != null) writer.Close();
                if (this.stream != null) stream.Close();
                if (this.client != null) client.Close();
                if (th != null && th.IsAlive) th.Abort();

                this.wnd = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion
    }

    public class ConnectedClients
    {
        private static ConnectedClients work = null;
        private Dictionary<string, System.Collections.Hashtable> _Dic = new Dictionary<string, System.Collections.Hashtable>();

        public static ConnectedClients GetInstance()
        {
            if (work == null)
            {
                work = new ConnectedClients();
            }

            return work;
        }

        public Dictionary<string, System.Collections.Hashtable> ConnectedClient
        {
            get
            {
                return _Dic;
            }
        }
    }
   
}
