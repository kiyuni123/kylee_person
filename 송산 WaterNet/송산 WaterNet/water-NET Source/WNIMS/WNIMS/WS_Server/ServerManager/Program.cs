﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Text;
using System.Runtime.InteropServices;

namespace ServerManager
{
    static class Program
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void BringWindowToTop(IntPtr hWnd);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void SetForegroundWindow(IntPtr hWnd);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);


        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);


        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            bool createdNew;
            Mutex mutex = new Mutex(true, "ServerManager.frmMain", out createdNew);
            if (createdNew)
            {
                //Database Connection 정보 확인
                if (System.IO.File.Exists(EMFrame.statics.AppStatic.DB_CONFIG_FILE_PATH) != true)
                {
                    MessageBox.Show("Water-NET 통합서버 데이터베이스 환경설정파일을 찾을 수 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    forms.frmDatabase oForm = new forms.frmDatabase();
                    oForm.ShowDialog();
                    Application.Exit(); return;
                }

                try
                {
                    //오라클 환경변수 
                    Environment.SetEnvironmentVariable("NLS_LANG", "KOREAN_KOREA.KO16MSWIN949");
                    //Logging 설정
                    EMFrame.config.EConfig.INITIALIZE_LOG();

                    //시스템실행시 초기환경 설정
                    SetDatabaseConnectString();
                    SetLicense_Account();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    Application.Exit(); return; 
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                //자동로그인-클러스터링 환경(원주)
                string id = IniReadValue("ADMIN", "ID");
                string pass = IniReadValue("ADMIN", "PASS");
                if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(pass))
                {
                    //프로그램 로그온
                    EMFrame.dm.EMapper mapper = null;
                    try
                    {
                        mapper = new EMFrame.dm.EMapper("SVR");

                        StringBuilder oStringBuilder = new StringBuilder();
                        oStringBuilder.AppendLine("SELECT * FROM V_USER                   ");
                        oStringBuilder.AppendLine(" WHERE USER_ID = '" + id + "'");
                        oStringBuilder.AppendLine("   AND USER_PWD = '" + pass + "'");
                        oStringBuilder.AppendLine("   AND USE_YN = '1'");

                        System.Data.DataTable table = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });

                        if (table == null || table.Rows.Count == 0)
                        {
                            MessageBox.Show(string.Format("관리자가 없습니다.\nr{0}을 확인하십시오.", Application.StartupPath + @"\Config\Settings.ini"));
                            Application.Exit();
                        }
                        else
                        {
                            EMFrame.statics.AppStatic.DATA_DIR = @Convert.ToString(table.Rows[0]["DATA_PATH"]);
                            EMFrame.statics.AppStatic.USER_SGCCD = Convert.ToString(table.Rows[0]["MGDPCD"]);
                            EMFrame.statics.AppStatic.USER_SGCNM = Convert.ToString(table.Rows[0]["MGDPNM"]);
                            EMFrame.statics.AppStatic.USER_HEADQUARTECD = Convert.ToString(table.Rows[0]["ROHQCD"]);
                            EMFrame.statics.AppStatic.USER_HEADQUARTENM = Convert.ToString(table.Rows[0]["ROHQCD"]);
                            EMFrame.statics.AppStatic.ZONE_GBN = Convert.ToString(table.Rows[0]["ZONE_GBN"]);
                        }
                    }
                    catch (Exception oException)
                    {
                        EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                    }
                    finally
                    {
                        mapper.Close();
                    }
                }
                else
                {
                    //Login
                    forms.frmLogOn login = new forms.frmLogOn();
                    login.ShowDialog();
                    if (!((forms.frmLogOn)login).IsLogOn)
                    {
                        Application.Exit();
                        return;
                    }
                }
               
                if (args.Length == 0)
                {
                    processKill("NetworkListener");

                    ProcessStartInfo pStartInfo = new ProcessStartInfo();
                    pStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    pStartInfo.WorkingDirectory = Application.StartupPath;
                    pStartInfo.FileName = "NetworkListener.exe";
                    Process Proc = new Process();
                    Proc.StartInfo = pStartInfo;
                    Proc.Start();
                }

                Application.Run(new forms.frmMain());
                mutex.ReleaseMutex();
            }
            else
            {
                IntPtr wHandle = FindWindow(null, "ServerManager.frmMain");
                if (wHandle != IntPtr.Zero)
                {
                    ShowWindow(wHandle, 1);
                    BringWindowToTop(wHandle);
                    SetForegroundWindow(wHandle);
                }
                Application.Exit();
                return;
            }
        }

        private static string IniReadValue(string Section, string Key)
        {
            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "Settings.ini";

            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, strPath + strFile);
            return temp.ToString();
        }

        private static void IniWriteValue(string Section, string Key, string Value)
        {
            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "Settings.ini";

            WritePrivateProfileString(Section, Key, Value, strPath + strFile);
        }

        private static void processKill(string processName)
        {
            Process[] process = Process.GetProcessesByName(processName);
            Process currentProcess = Process.GetCurrentProcess();
            foreach (Process p in process)
            {
                if (p.Id != currentProcess.Id)
                    p.Kill();
            }
        }

        /// <summary>
        /// 연결 문자열 구하기 - 통합서버의 DB경로 설정
        /// 연결환경을 XML file로 변경함
        /// </summary>
        /// <returns>연결 문자열</returns>
        /// <remarks></remarks>
        private static string GetConnectionString()
        {
            WS_Common.xml.ReadXml doc = new WS_Common.xml.ReadXml(EMFrame.statics.AppStatic.DB_CONFIG_FILE_PATH);

            string svcName = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/servicename"));
            string ip = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/ip"));
            string id = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/id"));
            string passwd = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/password"));
            string port = EMFrame.utils.ConvertUtils.DecryptKey(doc.getProp("/Root/waternet/port"));

            //string conStr = "Data Source=(DESCRIPTION="
            //                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=" + port + ")))"
            //                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
            //                + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";

            string conStr = "Data Source=(DESCRIPTION="
                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=" + port + ")))"
                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
                + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";
            return conStr;
        }

        #region 프로그램 실행시 한번만 실행 - static 변수들 설정
        /// <summary>
        /// 데이터베이스 접근경로 등록
        /// </summary>
        private static void SetDatabaseConnectString()
        {
            string conStr = GetConnectionString();
            EMFrame.dm.EMapper.ConnectionString.Add("SVR", conStr);
        }

        /// <summary>
        /// 사업장(관리부서)의 접근 라이센스수 정의
        /// </summary>
        private static void SetLicense_Account()
        {
            //프로그램 로그온
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT MGDPCD, ACCCNT FROM MANAGEMENT_DEPARTMENT");

                System.Data.DataTable dsSource = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });
                foreach (System.Data.DataRow row in dsSource.Rows)
                {
                    EMFrame.statics.AppStatic.LICENSE_ACCOUNT.Add(Convert.ToString(row["MGDPCD"]), Convert.ToInt32(row["ACCCNT"]));
                }

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Error("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        #endregion 프로그램 실행시 한번만 실행 - static 변수들 설정
    }

}
