﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Xml;

namespace WS_VersionManager.forms
{
    public partial class frmVersionHistory : EMFrame.form.BaseForm
    {
        public frmVersionHistory()
        {
            InitializeComponent();
        }

        private void frmVersionHistory_Load(object sender, EventArgs e)
        {
            this.InitializeGridColumnSetting();
            this.InitializeValueListSetting();
            this.InitializeControlSetting();
            this.InitializeEventSetting();
            this.radioButton1.Checked = true;
            this.cboHEADQUARTER_SelectedIndexChanged(this, new EventArgs());

            this.LoginUser_ControlSetting();
            btnSearch_Click(this, new EventArgs());
        }

        /// <summary>
        /// 로그인사용자의 권한에 따른 화면 컨트롤 재설정
        /// </summary>
        private void LoginUser_ControlSetting()
        {
            switch (EMFrame.statics.AppStatic.USER_RIGHT)
            {
                case "0":
                    break;
                case "1":  //센터관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.cboManageDept.Enabled = false;
                    this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;
                    break;
                case "2":  //본부관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    break;
                case "3":  //본사관리자
                    break;
            }
        }

        #region 초기화

        /// <summary>
        /// 초기화면 컨트롤 초기화
        /// </summary>
        private void InitializeControlSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ROHQCD CD, ROHQNM || '(' || ROHQCD || ')' CDNM from REGIONAL_HEADQUARTER");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), false);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept, oStringBuilder.ToString(), false);

            dt_StartDay.Value = DateTime.Now.AddDays(-1);
        }

        /// <summary>
        /// 울트라그리드 컬럼 초기화
        /// </summary>
        private void InitializeGridColumnSetting()
        {
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "ZONE_GBN", "지방/광역", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "MGDPCD", "센터명", 200, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "REVISION_NO", "버전No", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "DESCRIPTION", "설명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "REG_UID", "등록자", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugData, "REG_DATE", "등록일시", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);

            //-------------------------------------------------------------------------------------
            WS_Common.utils.gridUtils.AddColumn(this.ugDataAll, "MGDPCD", "센터명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataAll, "REVISION_NO", "버전No", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataAll, "FILE_NAME", "파일명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataAll, "FILE_TYPE", "파일타입", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataAll, "FILE_RNAME", "임시파일", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataAll, "REG_UID", "등록자", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataAll, "REG_DATE", "등록일시", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataAll, "FILE_DATE", "파일일시", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime,
                     Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);

            this.ugDataAll.DisplayLayout.Bands[0].Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;

            //--------------------------------------------------------------------------------------
            WS_Common.utils.gridUtils.AddColumn(this.ugDataCurrent, "MGDPCD", "센터명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataCurrent, "REVISION_NO", "버전No", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataCurrent, "FILE_NAME", "파일명", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataCurrent, "FILE_TYPE", "파일타입", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataCurrent, "FILE_RNAME", "임시파일", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataCurrent, "REG_UID", "등록자", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.Default,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataCurrent, "REG_DATE", "등록일시", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            WS_Common.utils.gridUtils.AddColumn(this.ugDataCurrent, "FILE_DATE", "파일일시", 50, false, Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime,
                                 Infragistics.Win.UltraWinGrid.Activation.NoEdit, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle);
            
        }

        /// <summary>
        /// 울트라그리드 밸류리스트 초기화
        /// </summary>
        private void InitializeValueListSetting()
        {
            object[,] aoSource = {
                {"S",  "시스템"},
                {"P",  "상수관망"},
                {"T",  "지형도"},
                {"N",  "계통도"}
            };

            WS_Common.utils.gridUtils.SetValueList(this.ugDataAll, "FILE_TYPE", aoSource);
            WS_Common.utils.gridUtils.SetValueList(this.ugDataCurrent, "FILE_TYPE", aoSource);
            //------------------------------------------------------------
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select DISTINCT MGDPCD CD, MGDPNM CDNM from MANAGEMENT_DEPARTMENT");
            DataTable datatable = WS_Common.work.Commonwork.GetInstance().GetScriptData(oStringBuilder.ToString());
            WS_Common.utils.gridUtils.SetValueList(this.ugData, "MGDPCD", datatable);
            WS_Common.utils.gridUtils.SetValueList(this.ugDataAll, "MGDPCD", datatable);
            WS_Common.utils.gridUtils.SetValueList(this.ugDataCurrent, "MGDPCD", datatable);

        }

        /// <summary>
        /// 이벤트 초기화
        /// </summary>
        private void InitializeEventSetting()
        {
            this.cboHEADQUARTER.SelectedIndexChanged +=new EventHandler(cboHEADQUARTER_SelectedIndexChanged);
            this.cboManageDept.SelectedIndexChanged +=new EventHandler(cboManageDept_SelectedIndexChanged);

            this.ugData.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(ugData_ClickCell);
        }


        #endregion 초기화

        #region 컨트롤 - 그리드, 버튼, 콤보박스 이벤트 실행
        private void ugData_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            if (e.Cell.Row == null) return;

            this.rtDescription.Text = Convert.ToString(e.Cell.Row.Cells["DESCRIPTION"].Value);
            this.VersionHistoryAllData(e.Cell.Row);
            this.VersionHistoryCurrentData(e.Cell.Row);
        }

        private void cboHEADQUARTER_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept, oStringBuilder.ToString(), false);
        }

        private void cboManageDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboManageDept.SelectedIndex == -1) return;

            this.RevisionData();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.RevisionData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            System.Collections.Generic.Dictionary<int, System.Collections.Hashtable> dic = new Dictionary<int, System.Collections.Hashtable>();

            foreach (Infragistics.Win.UltraWinGrid.UltraGridRow row in this.ugDataAll.Selected.Rows)
            {
                System.Collections.Hashtable param = new System.Collections.Hashtable();
                param.Add("MGDPCD", row.Cells["MGDPCD"].Value);
                param.Add("REVISION_NO", row.Cells["REVISION_NO"].Value);
                string file = System.IO.Path.GetFileNameWithoutExtension(Convert.ToString(row.Cells["FILE_NAME"].Value));
                param.Add("FILE_NAME", file);

                dic.Add(row.Index, param);
            }

            try
            {
                work.VersionManagerwork.GetInstance().DelVersionHistoryData(dic);

                this.VersionHistoryAllData(this.ugData.ActiveRow);
            }
            catch (Exception)
            {
                MessageBox.Show("삭제에 실패하였습니다.", "안내", MessageBoxButtons.OK);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("MGDPCD", this.ugData.ActiveRow.Cells["MGDPCD"].Value);
            param.Add("REVISION_NO", this.ugData.ActiveRow.Cells["REVISION_NO"].Value);
            param.Add("DESCRIPTION", this.rtDescription.Text);

            try
            {
                work.VersionManagerwork.GetInstance().SetRevisionDescription(param);

                this.ugData.ActiveRow.Cells["DESCRIPTION"].Value = this.rtDescription.Text;
            }
            catch (Exception)
            {
                this.ugData.ActiveRow.Cells["DESCRIPTION"].Value = this.ugData.ActiveRow.Cells["DESCRIPTION"].OriginalValue;
                MessageBox.Show("저장에 실패하였습니다.", "안내", MessageBoxButtons.OK);
            }

            //this.RevisionData();
        }
        #endregion 컨트롤 - 그리드, 버튼, 콤보박스 이벤트 실행

        //이충섭 수정 2013.02.13 - 검색조건 변경
        #region Button Function
        private void RevisionData()
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            
            param.Add("STARTDAY", ((DateTime)this.dt_StartDay.Value).ToString("yyyyMMdd"));
            param.Add("ENDDAY", ((DateTime)this.dt_EndDay.Value).ToString("yyyyMMdd"));

            if (this.radioButton1.Checked)
            {
                param.Add("MGDPCD", this.cboManageDept.SelectedValue);
                this.ugData.DataSource = work.VersionManagerwork.GetInstance().GetRevisionHistoryData(param);
            }
            else
            {
                if (this.checkBox1.Checked)
                {
                    param.Add("ZONE_GBN", "광역");
                    this.ugData.DataSource = work.VersionManagerwork.GetInstance().GetRevisionHistoryData2(param);
                }
                else
                {
                    param.Add("ZONE_GBN", "지방");
                    this.ugData.DataSource = work.VersionManagerwork.GetInstance().GetRevisionHistoryData2(param);
                }
            }

            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugData);
        }

        private void VersionHistoryAllData(Infragistics.Win.UltraWinGrid.UltraGridRow row)
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            
            param.Add("REVISION_NO", row.Cells["REVISION_NO"].Value);
            param.Add("ZONE_GBN", row.Cells["ZONE_GBN"].Value);
            
            //이충섭 수정 2013.02.13 - 검색조건 변경
            if (this.radioButton1.Checked)
            {
                param.Add("MGDPCD", row.Cells["MGDPCD"].Value);
                this.ugDataAll.DataSource = work.VersionManagerwork.GetInstance().GetVersionHistoryAllData(param);
            }
            else
            {
                this.ugDataAll.DataSource = work.VersionManagerwork.GetInstance().GetVersionHistoryAllData2(param);             
            }
            
            //if (this.chkAll.Checked)
            //    this.ugDataAll.DataSource = work.VersionManagerwork.GetInstance().GetVersionHistoryAllData(param);    
            //else
            //    this.ugDataAll.DataSource = work.VersionManagerwork.GetInstance().GetVersionHistoryAllData2(param);
            
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugDataAll);
        }

        private void VersionHistoryCurrentData(Infragistics.Win.UltraWinGrid.UltraGridRow row)
        {
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("MGDPCD", row.Cells["MGDPCD"].Value);
            param.Add("REVISION_NO", row.Cells["REVISION_NO"].Value);

            this.ugDataCurrent.DataSource = work.VersionManagerwork.GetInstance().GetVersionHistoryCurrentData(param);
            WS_Common.utils.gridUtils.SetGridStyle_PerformAutoResize(this.ugDataCurrent);
        }
        #endregion Button Function

        #region RadioButton, CheckBox 이벤트
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            this.radioButton2.Checked = false;
            this.cboHEADQUARTER.Enabled = true;
            this.cboManageDept.Enabled = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            this.radioButton1.Checked = false;
            this.cboHEADQUARTER.Enabled = false;
            this.cboManageDept.Enabled = false;
        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            this.radioButton1.Checked = true;
            this.radioButton2.Checked = false;
            this.checkBox1.Visible = false;
            this.cboHEADQUARTER.Enabled = true;
            this.cboManageDept.Enabled = true;
            this.ugData.DisplayLayout.Bands[0].Columns["MGDPCD"].Hidden = false;
            this.RevisionData();
        }

        private void radioButton2_Click(object sender, EventArgs e)
        {
            this.radioButton2.Checked = true;
            this.radioButton1.Checked = false;
            this.checkBox1.Visible = true;
            this.cboHEADQUARTER.Enabled = false;
            this.cboManageDept.Enabled = false;
            this.ugData.DisplayLayout.Bands[0].Columns["MGDPCD"].Hidden = true;
            this.RevisionData();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {            
            this.RevisionData();
        }
        #endregion RadioButton, CheckBox 이벤트


    }
}
