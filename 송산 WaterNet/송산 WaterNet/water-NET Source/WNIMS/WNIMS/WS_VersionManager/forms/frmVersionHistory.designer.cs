﻿namespace WS_VersionManager.forms
{
    partial class frmVersionHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.dt_EndDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dt_StartDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cboManageDept = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboHEADQUARTER = new System.Windows.Forms.ComboBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.ugData = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.rtDescription = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabAll = new System.Windows.Forms.TabPage();
            this.ugDataAll = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.tabCurrent = new System.Windows.Forms.TabPage();
            this.ugDataCurrent = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_EndDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_StartDay)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugData)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugDataAll)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabCurrent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugDataCurrent)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.dt_EndDay);
            this.panel1.Controls.Add(this.dt_StartDay);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.cboManageDept);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cboHEADQUARTER);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1187, 42);
            this.panel1.TabIndex = 38;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(1048, 11);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(48, 16);
            this.checkBox1.TabIndex = 29;
            this.checkBox1.Text = "광역";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(946, 11);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(94, 16);
            this.radioButton2.TabIndex = 28;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "System 파일";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Click += new System.EventHandler(this.radioButton2_Click);
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(848, 11);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(87, 16);
            this.radioButton1.TabIndex = 28;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Shape 파일";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Click += new System.EventHandler(this.radioButton1_Click);
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // dt_EndDay
            // 
            this.dt_EndDay.Location = new System.Drawing.Point(720, 9);
            this.dt_EndDay.Name = "dt_EndDay";
            this.dt_EndDay.Size = new System.Drawing.Size(112, 21);
            this.dt_EndDay.TabIndex = 27;
            // 
            // dt_StartDay
            // 
            this.dt_StartDay.Location = new System.Drawing.Point(585, 9);
            this.dt_StartDay.Name = "dt_StartDay";
            this.dt_StartDay.Size = new System.Drawing.Size(112, 21);
            this.dt_StartDay.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(701, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 10);
            this.label4.TabIndex = 25;
            this.label4.Text = "~";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(530, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 12);
            this.label1.TabIndex = 21;
            this.label1.Text = "등록일 :";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(1105, 7);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 26);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "조회";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(270, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 12);
            this.label8.TabIndex = 16;
            this.label8.Text = "지자체 :";
            // 
            // cboManageDept
            // 
            this.cboManageDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboManageDept.FormattingEnabled = true;
            this.cboManageDept.Location = new System.Drawing.Point(325, 10);
            this.cboManageDept.Name = "cboManageDept";
            this.cboManageDept.Size = new System.Drawing.Size(197, 20);
            this.cboManageDept.TabIndex = 15;
            this.cboManageDept.SelectedIndexChanged += new System.EventHandler(this.cboManageDept_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "지역본부 :";
            // 
            // cboHEADQUARTER
            // 
            this.cboHEADQUARTER.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHEADQUARTER.FormattingEnabled = true;
            this.cboHEADQUARTER.Location = new System.Drawing.Point(73, 10);
            this.cboHEADQUARTER.Name = "cboHEADQUARTER";
            this.cboHEADQUARTER.Size = new System.Drawing.Size(190, 20);
            this.cboHEADQUARTER.TabIndex = 13;
            this.cboHEADQUARTER.SelectedIndexChanged += new System.EventHandler(this.cboHEADQUARTER_SelectedIndexChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(5, 47);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(1187, 551);
            this.splitContainer1.SplitterDistance = 242;
            this.splitContainer1.TabIndex = 39;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(27, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.ugData);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.rtDescription);
            this.splitContainer2.Panel2.Controls.Add(this.panel3);
            this.splitContainer2.Size = new System.Drawing.Size(1160, 242);
            this.splitContainer2.SplitterDistance = 153;
            this.splitContainer2.TabIndex = 40;
            // 
            // ugData
            // 
            this.ugData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugData.Location = new System.Drawing.Point(0, 0);
            this.ugData.Name = "ugData";
            this.ugData.Size = new System.Drawing.Size(1160, 153);
            this.ugData.TabIndex = 39;
            this.ugData.Text = "Shape정보";
            // 
            // rtDescription
            // 
            this.rtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtDescription.Location = new System.Drawing.Point(0, 0);
            this.rtDescription.Name = "rtDescription";
            this.rtDescription.Size = new System.Drawing.Size(1109, 85);
            this.rtDescription.TabIndex = 2;
            this.rtDescription.Text = "";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnSave);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1109, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(51, 85);
            this.panel3.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(4, 29);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(43, 33);
            this.btnSave.TabIndex = 21;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(27, 242);
            this.label2.TabIndex = 39;
            this.label2.Text = "버전번호";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabAll);
            this.tabControl1.Controls.Add(this.tabCurrent);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1187, 305);
            this.tabControl1.TabIndex = 0;
            // 
            // tabAll
            // 
            this.tabAll.Controls.Add(this.ugDataAll);
            this.tabAll.Controls.Add(this.panel2);
            this.tabAll.Location = new System.Drawing.Point(4, 22);
            this.tabAll.Name = "tabAll";
            this.tabAll.Padding = new System.Windows.Forms.Padding(3);
            this.tabAll.Size = new System.Drawing.Size(1179, 279);
            this.tabAll.TabIndex = 0;
            this.tabAll.Text = "전체버전";
            this.tabAll.UseVisualStyleBackColor = true;
            // 
            // ugDataAll
            // 
            this.ugDataAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugDataAll.Location = new System.Drawing.Point(3, 3);
            this.ugDataAll.Name = "ugDataAll";
            this.ugDataAll.Size = new System.Drawing.Size(1173, 240);
            this.ugDataAll.TabIndex = 40;
            this.ugDataAll.Text = "Shape정보";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnDelete);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 243);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1173, 33);
            this.panel2.TabIndex = 39;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(1087, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 26);
            this.btnDelete.TabIndex = 19;
            this.btnDelete.Text = "삭제";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // tabCurrent
            // 
            this.tabCurrent.Controls.Add(this.ugDataCurrent);
            this.tabCurrent.Location = new System.Drawing.Point(4, 22);
            this.tabCurrent.Name = "tabCurrent";
            this.tabCurrent.Padding = new System.Windows.Forms.Padding(3);
            this.tabCurrent.Size = new System.Drawing.Size(1272, 279);
            this.tabCurrent.TabIndex = 1;
            this.tabCurrent.Text = "현재버전";
            this.tabCurrent.UseVisualStyleBackColor = true;
            // 
            // ugDataCurrent
            // 
            this.ugDataCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugDataCurrent.Location = new System.Drawing.Point(3, 3);
            this.ugDataCurrent.Name = "ugDataCurrent";
            this.ugDataCurrent.Size = new System.Drawing.Size(1266, 273);
            this.ugDataCurrent.TabIndex = 41;
            this.ugDataCurrent.Text = "Shape정보";
            // 
            // frmVersionHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 603);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Name = "frmVersionHistory";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "버전정보 조회";
            this.Load += new System.EventHandler(this.frmVersionHistory_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_EndDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_StartDay)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugData)).EndInit();
            this.panel3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugDataAll)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tabCurrent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugDataCurrent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboManageDept;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboHEADQUARTER;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugData;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabAll;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugDataAll;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TabPage tabCurrent;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugDataCurrent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dt_EndDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dt_StartDay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox rtDescription;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}