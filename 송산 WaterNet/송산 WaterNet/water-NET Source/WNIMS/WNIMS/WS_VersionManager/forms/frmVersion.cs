﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace WS_VersionManager.forms
{
    public partial class frmVersion : EMFrame.form.BaseForm
    {
        #region Socket Client 및 File 관련 정의
        //FTP Information
        private struct STRC_TCP_ENV_COMMON
        {
            public string FTP_IP;
            public string FTP_PORT;
            public string FTP_UID;
            public string FTP_PWD;
            public string FTP_PATH;
            public string FTP_SGCCD;
        }

        private STRC_TCP_ENV_COMMON m_STRC_TCP_ENV_COMMON;
        //private FTPClient.FTPConnection m_FTPCLIENT = null;
        EnterpriseDT.Net.Ftp.FTPConnection _FTPCLIENT = null;
        #endregion

        System.Threading.Thread t1 = null;

        public frmVersion()
        {
            InitializeComponent();
        }

        private void frmVersionManager_Load(object sender, EventArgs e)
        {
            this.InitializeControlSetting();
            this.InitializeEventSetting();



            this.GetTCPInfor();

            if (m_STRC_TCP_ENV_COMMON.FTP_IP == null)
            {
                MessageBox.Show("FTP 서버의 정보 취득에 실패했습니다. 관리자에게 문의하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            this.SetCombo_VersionFileType(this.cboFileType);
            this.cboHEADQUARTER_SelectedIndexChanged(this, new EventArgs());
            this.cboManageDept_SelectedIndexChanged(this, new EventArgs());

            this.LoginUser_ControlSetting();
        }

        /// <summary>
        /// 이벤트 초기화
        /// </summary>
        private void InitializeEventSetting()
        {
            this.cboHEADQUARTER.SelectedIndexChanged += new EventHandler(cboHEADQUARTER_SelectedIndexChanged);
            this.cboManageDept.SelectedIndexChanged += new EventHandler(cboManageDept_SelectedIndexChanged);
        }

        private void cboHEADQUARTER_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select MGDPCD CD, MGDPNM || '(' || MGDPCD || ')' CDNM from MANAGEMENT_DEPARTMENT WHERE ROHQCD = '" + this.cboHEADQUARTER.SelectedValue + "'");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboManageDept, oStringBuilder.ToString(), false);

        }

        private object ZONE_GBN = EMFrame.statics.AppStatic.ZONE_GBN;

        private void cboManageDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.m_STRC_TCP_ENV_COMMON.FTP_SGCCD = string.IsNullOrEmpty(this.m_STRC_TCP_ENV_COMMON.FTP_PATH) ? Convert.ToString(cboManageDept.SelectedValue) :
                                                                   this.m_STRC_TCP_ENV_COMMON.FTP_PATH + "\\" + Convert.ToString(cboManageDept.SelectedValue);

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ZONE_GBN from MANAGEMENT_DEPARTMENT WHERE MGDPCD = '" + this.cboManageDept.SelectedValue + "'");
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                this.ZONE_GBN = mapper.ExecuteScriptScalar(oStringBuilder.ToString(), new System.Data.IDataParameter[] { });

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            //Console.WriteLine(this.m_STRC_TCP_ENV_COMMON.FTP_SGCCD);
        }

        /// <summary>
        /// 로그인사용자의 권한에 따른 화면 컨트롤 재설정
        /// </summary>
        private void LoginUser_ControlSetting()
        {
            switch (Convert.ToInt32(EMFrame.statics.AppStatic.USER_RIGHT)) 
            {
                case 0:
                    break;
                case 1:  //센터관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    this.cboManageDept.Enabled = false;
                    this.cboManageDept.SelectedValue = EMFrame.statics.AppStatic.USER_SGCCD;
                    break;
                case 2:  //본부관리자
                    this.cboHEADQUARTER.Enabled = false;
                    this.cboHEADQUARTER.SelectedValue = EMFrame.statics.AppStatic.USER_HEADQUARTECD;
                    break;
                case 3:  //본사관리자
                    break;
            }
        }



        private void frmVersionManager_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.FTP_Disconnect();
        }


        #region Button Events

        private void btnOpen_Click(object sender, EventArgs e)
        {
            this.OpenVersionFile();
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            this.lstFile.Items.Clear();
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (this.lstFile.Items.Count == 0) return;
            if (string.IsNullOrEmpty(this.m_STRC_TCP_ENV_COMMON.FTP_SGCCD))
            {
                MessageBox.Show("지역센터를 선택하지 않았습니다! 지역센터를 선택하시고 다시하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                this.FTP_Connect();

                if (!this._FTPCLIENT.IsConnected)
                {
                    MessageBox.Show("FTP 서버에 연결 실패! FTP 환경정보를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\r" + "FTP 서버에 연결 실패! FTP 환경정보를 확인하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.progressBar.Maximum = this.lstFile.Items.Count;
            System.Collections.Hashtable param = new System.Collections.Hashtable();
            param.Add("MGDPCD", this.cboManageDept.SelectedValue);
            param.Add("DESCRIPTION", this.rtDescription.Text);
            param.Add("COUNT", this.lstFile.Items.Count);
            param.Add("SGCCD", this.m_STRC_TCP_ENV_COMMON.FTP_SGCCD);
            param.Add("ZONE_GBN", this.ZONE_GBN);

            //스레드 프로시저에 필요한 파라미터 값을 클래스의 생성자 파라미터로 설정 한다.  반환값을 확인할 콜백 대리자도 파라미터로 설정한다.
            Ftp_Upload upload = new Ftp_Upload(param, new WorkingClassCallBack(ResultCallback));
            upload.progbar = this.progressBar;
            upload.listView = this.lstFile;
            upload.FTPCLIENT = this._FTPCLIENT;

            if (t1 != null)
            {
                if (t1.IsAlive)
                {
                    MessageBox.Show("스레드가 동작중임");
                    return;
                }
                else
                {
                    t1 = new System.Threading.Thread(new System.Threading.ThreadStart(upload.StartUpload));
                    t1.Start();

                    base.CloseEnable = false;
                }
            }
            else
            {
                t1 = new System.Threading.Thread(new System.Threading.ThreadStart(upload.StartUpload));
                t1.Start();

                base.CloseEnable = false;
                
            }

            Console.WriteLine("메인스레드");

        }

        //반환값을 출력할 콜백 메서드
        private void ResultCallback(bool result)
        {
            base.isCloseEnable = result;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion


        #region Control Events


        #endregion


        #region User Function

        private void InitializeControlSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("select ROHQCD CD, ROHQNM || '(' || ROHQCD || ')' CDNM from REGIONAL_HEADQUARTER");
            WS_Common.utils.formUtils.SetComboBoxEX(this.cboHEADQUARTER, oStringBuilder.ToString(), false);
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 버전 파일 타입
        /// 대상 Table : CM_CODE
        /// </summary>
        /// <param name="oCombo"></param>
        private void SetCombo_VersionFileType(ComboBox oCombo)
        {
            object[,] aoSource = {
                {"P",  "상수관망도"},
                {"T",  "지형도"},
                {"N",  "계통도"},
                {"S",  "시스템파일"}
            };

            WS_Common.utils.formUtils.SetComboBoxEX(oCombo, aoSource, false);
        }

        /// <summary>
        /// 인자에서 (로 Split하여 (뒤 글자들을 반환
        /// </summary>
        /// <param name="strOrgData">Code를 추출할 원본 데이터 ex) XXXX (YYY) 중 YYY을 반환</param>
        /// <returns>string Code</returns>
        private string SplitToCode(string strOrgData)
        {
            string strCode = string.Empty;

            string[] strArr = strOrgData.Split('(');

            if (strOrgData.Length > 0 && strArr.Length > 1)
            {
                strCode = strArr[1].Substring(0, strArr[1].Length - 1);
            }
            else
            {
                strCode = strArr[0].Trim();
            }

            return strCode;
        }

        /// <summary>
        /// FTP 정보를 취득
        /// </summary>
        private void GetTCPInfor()
        {
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT   IP_ADDR, FTP_PORT, FTP_UID, FTP_PWD, FTP_PATH");
                oStringBuilder.AppendLine("FROM     CM_ENV_TCP");
                oStringBuilder.AppendLine("WHERE    ROWNUM = 1");

                DataTable datatable = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), new IDataParameter[] { });

                this.m_STRC_TCP_ENV_COMMON = new STRC_TCP_ENV_COMMON();

                foreach (DataRow oDRow in datatable.Rows)
                {
                    this.m_STRC_TCP_ENV_COMMON.FTP_IP = oDRow["IP_ADDR"].ToString().Trim();
                    this.m_STRC_TCP_ENV_COMMON.FTP_PORT = oDRow["FTP_PORT"].ToString().Trim();
                    this.m_STRC_TCP_ENV_COMMON.FTP_UID = oDRow["FTP_UID"].ToString().Trim();
                    this.m_STRC_TCP_ENV_COMMON.FTP_PWD = EMFrame.utils.ConvertUtils.DecryptKey(oDRow["FTP_PWD"].ToString().Trim());
                    //this.m_STRC_TCP_ENV_COMMON.FTP_PATH = oDRow["FTP_PATH"].ToString().Trim();
                }

            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        /// <summary>
        /// OpenFileDialog를 이용해서 업로드할 파일을 ListView에 등록한다.
        /// </summary>
        private void OpenVersionFile()
        {
            string[] arrFullName;
            string strFileType = string.Empty;
            string strInitPath = Application.StartupPath;
            string strFileTypeName = string.Empty;

            strFileType = this.SplitToCode(this.cboFileType.SelectedValue.ToString());

            OpenFileDialog oOFD = new OpenFileDialog();
            oOFD.Multiselect = true;
            oOFD.FilterIndex = 1;
            oOFD.RestoreDirectory = true;

            switch (strFileType)
            {
                case "C":
                case "D":
                case "S":
                    //strInitPath = Application.StartupPath;
                    oOFD.Filter = "모든 파일 (*.*)|*.*";
                    strFileTypeName = "시스템";
                    break;
                case "P":
                    //strInitPath = @"C:\Water-NET\WaterNet-Data\Pipegraphic";
                    oOFD.Filter = "상수관망도 (*.shp)|*.shp";
                    strFileTypeName = "상수관망";
                    break;
                case "T":
                    //strInitPath = @"C:\Water-NET\WaterNet-Data\Topographic";
                    oOFD.Filter = "지형도 (*.shp)|*.shp";
                    strFileTypeName = "지형도";
                    break;
                case "N":
                    //strInitPath = @"C:\Water-NET\WaterNet-Data\Topographic";
                    oOFD.Filter = "계통도 (*.xml)|*.xml";
                    strFileTypeName = "계통도";
                    break;
            }

            oOFD.InitialDirectory = strInitPath;

            //this.lstFile.Items.Clear();

            if (oOFD.ShowDialog() == DialogResult.OK)
            {
                arrFullName = oOFD.FileNames;

                for (int i = 0; i < arrFullName.Length; i++)
                {
                    string sPath = System.IO.Path.GetDirectoryName(arrFullName[i]);
                    string strFileName = System.IO.Path.GetFileName(arrFullName[i]);

                    ListViewItem oLItem = new ListViewItem(strFileName);

                    oLItem.SubItems.Add(strFileTypeName);
                    oLItem.SubItems.Add(this.GetFileSize(arrFullName[i]));
                    oLItem.SubItems.Add(sPath);
                    oLItem.SubItems.Add("준비");
                    if (strFileType.Equals("P") || strFileType.Equals("T")) 
                        oLItem.SubItems.Add(GetSubFiles(arrFullName[i]));
                    else 
                        oLItem.SubItems.Add(strFileName);
                    
                    this.lstFile.Items.Add(oLItem);
                }
            }
        }

        /// <summary>
        /// 상수관망도, 지형도(Shape)의 dbf,shx,prj를 찾기위한 함수
        /// </summary>
        /// <param name="fullpath"></param>
        /// <returns></returns>
        private string GetSubFiles(string fullpath)
        {
            string sPath = System.IO.Path.GetDirectoryName(fullpath);
            string strFileName = System.IO.Path.GetFileNameWithoutExtension(fullpath) + ".*";

            string[] files = System.IO.Directory.GetFiles(sPath, strFileName, SearchOption.TopDirectoryOnly);

            string subfiles = string.Empty;

            foreach (string sfile in files)
            {
                subfiles += System.IO.Path.GetFileName(sfile) + ",";
            }
            subfiles = subfiles.TrimEnd(',');
            return subfiles;
        }

        /// <summary>
        /// File의 Size를 구해 리턴한다.
        /// </summary>
        /// <param name="strSrcFilePath">Size를 구할 File의 Full Path</param>
        /// <returns>File Size</returns>
        private string GetFileSize(string strFullName)
        {
            try
            {
                string strSize = string.Empty;

                string strPath = string.Empty;
                string strFileName = string.Empty;

                string[] arrSplit = strFullName.Split('\\');
                strFileName = arrSplit[arrSplit.Length-1].ToString();
                strPath = strFullName.Substring(0, strFullName.Length - strFileName.Length - 1);
                DirectoryInfo diRootDir = new DirectoryInfo(strPath);
                FileInfo[] arrFileInfor = diRootDir.GetFiles(strFileName);

                foreach (FileInfo arrFileInfo in arrFileInfor)
                {
                    strSize = arrFileInfo.Length.ToString();
                }
                return strSize;
            }
            catch
            {
                return "0";
            }
        }


        /// <summary>
        /// DateTime을 받아 YYYYMMDD Format의 String로 변환해 반환한다.
        /// </summary>
        /// <param name="oDateTime">DateTime Object</param>
        /// <returns>string YYYYMMDD</returns>
        private string StringToDateTime(DateTime oDateTime)
        {
            string strRtn = string.Empty;

            strRtn = Convert.ToString(oDateTime.Year) + Convert.ToString(oDateTime.Month).PadLeft(2, '0') + Convert.ToString(oDateTime.Day).PadLeft(2, '0');

            return strRtn;
        }


        #endregion


        #region Socket FTP Function

        /// <summary>
        /// FTP Server에 Connect
        /// </summary>     
        private void FTP_Connect()
        {
            int intPort = Int32.Parse(m_STRC_TCP_ENV_COMMON.FTP_PORT);

            try
            {
                if (_FTPCLIENT == null) _FTPCLIENT = new EnterpriseDT.Net.Ftp.FTPConnection();
                if (_FTPCLIENT.IsConnected) _FTPCLIENT.Close();

                if (!_FTPCLIENT.IsConnected)
                {
                    _FTPCLIENT.ServerAddress = m_STRC_TCP_ENV_COMMON.FTP_IP;
                    _FTPCLIENT.ServerPort = intPort;
                    _FTPCLIENT.UserName = m_STRC_TCP_ENV_COMMON.FTP_UID;
                    _FTPCLIENT.Password = m_STRC_TCP_ENV_COMMON.FTP_PWD;

                    _FTPCLIENT.Connect();

                    _FTPCLIENT.ServerDirectory = "/";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        private void frmVersion_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (t1 != null && t1.ThreadState != System.Threading.ThreadState.Stopped)
            {
                MessageBox.Show("스레드가 동작중이므로, 종료할 수 없습니다.");
                e.Cancel = true;
            }
        }
    }

    public delegate void WorkingClassCallBack(bool result); //콜백 메서드의 대리자(종료시점의 반환대리자)

    /// <summary>
    /// FTP Upload 스레드
    /// </summary>
    public class Ftp_Upload
    {
        private delegate void SetLabelCallBack(string str);

        private delegate void SetProgCallBack(int vv);
        private delegate string GetListViewValue(int v1, int v2);
        private delegate void SetListViewValue(int v1, int v2, string value);
        private delegate void SetListViewSelectValue(int v1, bool value);
        private delegate void ClearListView();

        private ProgressBar progressBar = null;
        private System.Windows.Forms.ListView lstFile = null;
        private EnterpriseDT.Net.Ftp.FTPConnection _FTPCLIENT = null;
        //private FTPClient.FTPConnection m_FTPCLIENT = null;

        private string MGDPCD = string.Empty;
        private string DESCRIPTION = string.Empty;
        private string REG_UID = string.Empty;
        private string SGCCD = string.Empty;
        private string ZONE_GBN = string.Empty;
        private int nCOUNT = -1;

        private WorkingClassCallBack callback;

        public Ftp_Upload(System.Collections.Hashtable param, WorkingClassCallBack callback)
        {
            this.MGDPCD = Convert.ToString(param["MGDPCD"]);
            this.DESCRIPTION = Convert.ToString(param["DESCRIPTION"]);
            this.REG_UID = EMFrame.statics.AppStatic.USER_ID;
            this.SGCCD = Convert.ToString(param["SGCCD"]);
            this.ZONE_GBN = Convert.ToString(param["ZONE_GBN"]);
            this.nCOUNT = Convert.ToInt32(param["COUNT"]);

            this.callback = callback;
        }

        public EnterpriseDT.Net.Ftp.FTPConnection FTPCLIENT
        {
            set
            {
                _FTPCLIENT = value;
            }
        }

        public ProgressBar progbar
        {
            set
            {
                this.progressBar = value;
            }
        }

        public System.Windows.Forms.ListView listView
        {
            set
            {
                this.lstFile = value;
            }
        }

        public void StartUpload()
        {
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                mapper.BeginTransaction();

                //------------------------------------------------------------------------
                //리비전 테이블 Save
                //------------------------------------------------------------------------
                System.Collections.Hashtable param = new System.Collections.Hashtable();
                param.Add("MGDPCD", this.MGDPCD);
                param.Add("DESCRIPTION", this.DESCRIPTION);
                param.Add("REG_UID", EMFrame.statics.AppStatic.USER_ID);
                param.Add("ZONE_GBN", this.ZONE_GBN);
                string revision_NO = Convert.ToString(work.VersionManagerwork.GetInstance().SaveRevisionInfo(mapper, param));

                //------------------------------------------------------------------------
                //파일개수만큼 SAVE
                //------------------------------------------------------------------------
                for (int i = 0; i < this.lstFile.Items.Count; i++)
                {
                    this.SetProgBar(i + 1);
                    this.SetValue(i, 4, "전송중");
                    this.SetSelectValue(i, true);

                    //Shape을 위하여 SubFile생성
                    string[] split = this.GetValue(i,5).Split(',');

                    foreach (string sfile in split)
                    {
                        System.Threading.Thread.Sleep(500);
                        string strNewFileName = this.GenerationWQSerialNumber("VI");
                        string strFileName = this.GetValue(i, 3).TrimEnd('\\');
                        strFileName = strFileName + "\\" + sfile;

                        if (this.GetValue(i, 1).Equals("시스템"))
                        {
                            if (!_FTPCLIENT.ServerDirectory.Equals("/System"))
                            {
                                FTP_ChangeDirectory("System");    
                            }
                        }
                        else
                        {
                            if (!_FTPCLIENT.ServerDirectory.Equals("/" + this.SGCCD))
                            {
                                FTP_ChangeDirectory(this.SGCCD);
                            }
                        }
                        //Upload할 기존 서버에 있는 File을 Delete한다.
                        FTP_DeleteFile(strNewFileName);
                        
                        //Upload할 File 정보를 정리한다.
                        FileStream oFileStream = new FileStream(strFileName, FileMode.Open, FileAccess.Read, FileShare.Read);

                        try
                        {
                            //Encoding encode = Encoding.GetEncoding(51949);
                            //byte[] utf8Bytes = encode.GetBytes(strNewFileName);
                            //Console.WriteLine(strNewFileName + "," + encode.GetString(utf8Bytes) + " , " + utf8Bytes.ToString());
                            _FTPCLIENT.UploadStream(oFileStream, strNewFileName);
                        }
                        catch (Exception)
                        {
                            this.SetValue(i, 4, "실패");
                        }
                        finally
                        {
                            oFileStream.Close();
                        }

                        if (this.GetValue(i, 4) != "실패")
                        {
                            int intFileSize = Convert.ToInt32(GetFileSize(strFileName));
                            param.Clear();
                            param.Add("MGDPCD", this.MGDPCD);
                            param.Add("REG_UID", EMFrame.statics.AppStatic.USER_ID);
                            param.Add("FILE_NAME", sfile);
                            param.Add("REVISION_NO", revision_NO);
                            param.Add("FILE_RNAME", strNewFileName);
                            param.Add("FILE_SIZE", intFileSize);
                            param.Add("FILE_PATH", this.GetValue(i, 3));
                            param.Add("ZONE_GBN", this.ZONE_GBN);
                            param.Add("FILE_DATE", this.GetFileCreationDate(strFileName).ToString("yyyyMMddHHmmss"));
                            //Console.WriteLine(param["FILE_DATE"].ToString());
                            switch (this.GetValue(i, 1))
                            {
                                case "시스템":
                                    param.Add("FILE_TYPE", "S");
                                    break;
                                case "상수관망":
                                    param.Add("FILE_TYPE", "P");
                                    break;
                                case "지형도":
                                    param.Add("FILE_TYPE", "T");
                                    break;
                                case "계통도":
                                    param.Add("FILE_TYPE", "N");
                                    break;
                                default:
                                    param.Add("FILE_TYPE", "");
                                    break;
                            }

                            work.VersionManagerwork.GetInstance().SaveVersionInfo(mapper, param);
                            this.SetValue(i, 4, "성공");
                        }
                    }
                }

                mapper.CommitTransaction();
            }
            catch (Exception oException)
            {
                mapper.RollbackTransaction();
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                MessageBox.Show(oException.Message, "에러", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                this.Exit();
            }
            finally
            {
                mapper.Close();
                this.FTP_Disconnect();
            }

            MessageBox.Show("파일 업로드 완료", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.ClearList();
            this.SetProgBar(0);

            this.Exit();
        }

        private string GetValue(int idx, int index)
        {
            if (this.lstFile == null) return string.Empty;

            ///서로다른 프로세스에서 객체를 크로스로 접근하면 예외가 발생하므로 
            ///이를 해결하기 위해 Invoke 메소드를 사용하게 된다.
            ///진행바가 현재 Invoke가 필요한 상태인지 파악하여 필요하다면 대기상태에 있다가
            ///접근가능할때 백그라운드 작업을 진행하고, 필요한 상태가 아니라면
            ///진행바의 해당 속성에 바로 대입한다.
            if (this.lstFile.InvokeRequired)
            {
                //delegate 생성
                GetListViewValue dele = new GetListViewValue(GetValue);
                //대기상태에 있다가 접근가능한 상태일때 SetProgBar 간접호출
                return (string)this.lstFile.Invoke(dele, new object[] { idx, index });
            }
            else
            {
                return this.lstFile.Items[idx].SubItems[index].Text;
            }
        }

        private void SetValue(int idx, int index, string value)
        {
            if (this.lstFile == null) return;

            ///서로다른 프로세스에서 객체를 크로스로 접근하면 예외가 발생하므로 
            ///이를 해결하기 위해 Invoke 메소드를 사용하게 된다.
            ///진행바가 현재 Invoke가 필요한 상태인지 파악하여 필요하다면 대기상태에 있다가
            ///접근가능할때 백그라운드 작업을 진행하고, 필요한 상태가 아니라면
            ///진행바의 해당 속성에 바로 대입한다.
            if (this.lstFile.InvokeRequired)
            {
                //delegate 생성
                SetListViewValue dele = new SetListViewValue(SetValue);
                //대기상태에 있다가 접근가능한 상태일때 SetProgBar 간접호출
                this.lstFile.Invoke(dele, new object[] { idx, index, value });
            }
            else
            {
                this.lstFile.Items[idx].SubItems[index].Text = value;
            }
        }

        private void SetSelectValue(int idx, bool value)
        {
            if (this.lstFile == null) return;

            ///서로다른 프로세스에서 객체를 크로스로 접근하면 예외가 발생하므로 
            ///이를 해결하기 위해 Invoke 메소드를 사용하게 된다.
            ///진행바가 현재 Invoke가 필요한 상태인지 파악하여 필요하다면 대기상태에 있다가
            ///접근가능할때 백그라운드 작업을 진행하고, 필요한 상태가 아니라면
            ///진행바의 해당 속성에 바로 대입한다.
            if (this.lstFile.InvokeRequired)
            {
                //delegate 생성
                SetListViewSelectValue dele = new SetListViewSelectValue(SetSelectValue);
                //대기상태에 있다가 접근가능한 상태일때 SetProgBar 간접호출
                this.lstFile.Invoke(dele, new object[] { idx, value });
            }
            else
            {
                this.lstFile.Items[idx].Selected = value;
            }
        }

        private void ClearList()
        {
            if (this.lstFile == null) return;

            ///서로다른 프로세스에서 객체를 크로스로 접근하면 예외가 발생하므로 
            ///이를 해결하기 위해 Invoke 메소드를 사용하게 된다.
            ///진행바가 현재 Invoke가 필요한 상태인지 파악하여 필요하다면 대기상태에 있다가
            ///접근가능할때 백그라운드 작업을 진행하고, 필요한 상태가 아니라면
            ///진행바의 해당 속성에 바로 대입한다.
            if (this.lstFile.InvokeRequired)
            {
                //delegate 생성
                ClearListView dele = new ClearListView(ClearList);
                //대기상태에 있다가 접근가능한 상태일때 SetProgBar 간접호출
                this.lstFile.Invoke(dele, new object[] { });
            }
            else
            {
                this.lstFile.Items.Clear();
            }
        }
        /// <summary>
        /// t1 스레드에서 만들어진 값(vv)을 메인스레드(폼)의 진행바에 지정하기 위한 메소드
        /// </summary>
        /// <param name="vv"></param>
        private void SetProgBar(int vv)
        {
            if (this.progressBar == null) return;

            ///서로다른 프로세스에서 객체를 크로스로 접근하면 예외가 발생하므로 
            ///이를 해결하기 위해 Invoke 메소드를 사용하게 된다.
            ///진행바가 현재 Invoke가 필요한 상태인지 파악하여 필요하다면 대기상태에 있다가
            ///접근가능할때 백그라운드 작업을 진행하고, 필요한 상태가 아니라면
            ///진행바의 해당 속성에 바로 대입한다.
            if (this.progressBar.InvokeRequired)
            {
                //delegate 생성
                SetProgCallBack dele = new SetProgCallBack(SetProgBar);
                //대기상태에 있다가 접근가능한 상태일때 SetProgBar 간접호출
                this.progressBar.Invoke(dele, new object[] { vv });
            }
            else
            {
                this.progressBar.Value = vv;
            }
        }

        /// <summary>
        /// t1스레드에서 만들어진 값(str)을 메인스레드(폼)의 레이블에 지정하기 위한 메소드
        /// </summary>
        /// <param name="str"></param>
        private void SetLabel(string str)
        {

        }

        /// <summary>
        /// t1 스레드에서 메인스레드(폼)의 종료메소드(Close)를 지정하기위한 메소드
        /// </summary>
        private void Exit()
        {
            if (callback != null) callback(true);

            //ExitCallBack dele = new ExitCallBack(Close);
            //this.Invoke(dele); //창닫기
        }

        /// <summary>
        /// FTP Server에서 특정 Directory로 이동한다.
        /// Directory가 없으면, 생성한다.
        /// </summary>
        /// <param name="strPath">이동할 경로</param>
        private void FTP_ChangeDirectory(string strPath)
        {
            if (_FTPCLIENT == null || !_FTPCLIENT.IsConnected) return;

            try
            {
                _FTPCLIENT.ServerDirectory = "/";
                if (!_FTPCLIENT.DirectoryExists(strPath))
                {
                    _FTPCLIENT.CreateDirectory(strPath);
                }

                _FTPCLIENT.ServerDirectory = strPath;

                //string[] array = _FTPCLIENT.GetFiles(strPath);
                //System.Collections.ArrayList array = m_FTPCLIENT.Dir(strPath);
                //if (array.Count == 0)
                //{
                //    m_FTPCLIENT.MakeDir(strPath);
                //}
                //m_FTPCLIENT.SetCurrentDirectory(strPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// FTP Server에서 특정 경로의 File을 Delete한다.
        /// </summary>
        /// <param name="strPath">이동할 경로</param>
        private void FTP_DeleteFile(string strPath)
        {
            if (_FTPCLIENT == null || !_FTPCLIENT.IsConnected) return;

            try
            {
                if (_FTPCLIENT.Exists(strPath))
                {
                    _FTPCLIENT.DeleteFile(strPath);
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Substring(0, 3).Equals("550"))
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// FTP Server와 연결을 끊는다.
        /// </summary>
        private void FTP_Disconnect()
        {
            try
            {
                if (_FTPCLIENT != null && _FTPCLIENT.IsConnected)
                {
                    _FTPCLIENT.Close();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 수질용 일련번호 생성 함수
        /// 기본 길이 15Byte = Key 2Byte + YYYYMMDD 8Byte + 초 2Byte + 밀리초 3Byte
        /// 전체 길이 16Byte = 기본길이 14Byte + 랜덤숫자 1Byte
        /// </summary>
        /// <param name="strKey">일련번호 앞에 들어갈 대표문자 2Byte (영문)</param>
        /// <returns></returns>
        private String GenerationWQSerialNumber(string strKey)
        {
            Random pRandom = new Random();

            string strRtn = string.Empty;
            string strWQSN = string.Empty;

            strWQSN = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            strWQSN += "-" + (pRandom.Next(1, 99999).ToString()).PadLeft(5, '0');
            strRtn = strKey + strWQSN;

            return strRtn;
        }

        /// <summary>
        /// File의 Size를 구해 리턴한다.
        /// </summary>
        /// <param name="strSrcFilePath">Size를 구할 File의 Full Path</param>
        /// <returns>File Size</returns>
        private string GetFileSize(string strFullName)
        {
            try
            {
                string strSize = string.Empty;

                string strPath = string.Empty;
                string strFileName = string.Empty;

                string[] arrSplit = strFullName.Split('\\');
                strFileName = arrSplit[arrSplit.Length - 1].ToString();
                strPath = strFullName.Substring(0, strFullName.Length - strFileName.Length - 1);
                DirectoryInfo diRootDir = new DirectoryInfo(strPath);
                FileInfo[] arrFileInfor = diRootDir.GetFiles(strFileName);

                foreach (FileInfo arrFileInfo in arrFileInfor)
                {
                    strSize = arrFileInfo.Length.ToString();
                }
                return strSize;
            }
            catch
            {
                return "0";
            }
        }

        private DateTime GetFileCreationDate(string strFullName)
        {
            DateTime result = DateTime.MinValue;
            if (System.IO.File.Exists(strFullName))
            {
                result = System.IO.File.GetLastWriteTime(strFullName);
                //Console.WriteLine(result.ToString("yyyyMMddHHmmss"));
            }
            return result;
         
        }
    }
}
