﻿namespace WS_VersionManager.forms
{
    partial class frmVersion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstFile = new System.Windows.Forms.ListView();
            this.FileName = new System.Windows.Forms.ColumnHeader();
            this.FileType = new System.Windows.Forms.ColumnHeader();
            this.FileSize = new System.Windows.Forms.ColumnHeader();
            this.FilePath = new System.Windows.Forms.ColumnHeader();
            this.UpFlag = new System.Windows.Forms.ColumnHeader();
            this.subfiles = new System.Windows.Forms.ColumnHeader();
            this.cboFileType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cboManageDept = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboHEADQUARTER = new System.Windows.Forms.ComboBox();
            this.rtDescription = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnInit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstFile
            // 
            this.lstFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstFile.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.FileName,
            this.FileType,
            this.FileSize,
            this.FilePath,
            this.UpFlag,
            this.subfiles});
            this.lstFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstFile.FullRowSelect = true;
            this.lstFile.GridLines = true;
            this.lstFile.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstFile.HideSelection = false;
            this.lstFile.Location = new System.Drawing.Point(5, 5);
            this.lstFile.MultiSelect = false;
            this.lstFile.Name = "lstFile";
            this.lstFile.Size = new System.Drawing.Size(799, 315);
            this.lstFile.TabIndex = 38;
            this.lstFile.TabStop = false;
            this.lstFile.UseCompatibleStateImageBehavior = false;
            this.lstFile.View = System.Windows.Forms.View.Details;
            // 
            // FileName
            // 
            this.FileName.Text = "파일명";
            this.FileName.Width = 252;
            // 
            // FileType
            // 
            this.FileType.Text = "파일 타입";
            this.FileType.Width = 68;
            // 
            // FileSize
            // 
            this.FileSize.Text = "파일 사이즈";
            // 
            // FilePath
            // 
            this.FilePath.Text = "파일 경로";
            // 
            // UpFlag
            // 
            this.UpFlag.Text = "전송여부";
            // 
            // subfiles
            // 
            this.subfiles.Text = "SubFiles";
            this.subfiles.Width = 2000;
            // 
            // cboFileType
            // 
            this.cboFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFileType.FormattingEnabled = true;
            this.cboFileType.Location = new System.Drawing.Point(87, 401);
            this.cboFileType.Name = "cboFileType";
            this.cboFileType.Size = new System.Drawing.Size(281, 20);
            this.cboFileType.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 407);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 40;
            this.label1.Text = "파일 타입 :";
            // 
            // progressBar
            // 
            this.progressBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar.Location = new System.Drawing.Point(5, 483);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(799, 19);
            this.progressBar.TabIndex = 41;
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(222, 443);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(70, 34);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "열기";
            this.btnOpen.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(298, 443);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(70, 34);
            this.btnUpload.TabIndex = 2;
            this.btnUpload.Text = "등록";
            this.btnUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 375);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 43;
            this.label2.Text = "지역 센터 :";
            // 
            // cboManageDept
            // 
            this.cboManageDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboManageDept.FormattingEnabled = true;
            this.cboManageDept.Location = new System.Drawing.Point(87, 369);
            this.cboManageDept.Name = "cboManageDept";
            this.cboManageDept.Size = new System.Drawing.Size(281, 20);
            this.cboManageDept.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(15, 339);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 46;
            this.label3.Text = "지역 본부 :";
            // 
            // cboHEADQUARTER
            // 
            this.cboHEADQUARTER.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHEADQUARTER.FormattingEnabled = true;
            this.cboHEADQUARTER.Location = new System.Drawing.Point(87, 337);
            this.cboHEADQUARTER.Name = "cboHEADQUARTER";
            this.cboHEADQUARTER.Size = new System.Drawing.Size(281, 20);
            this.cboHEADQUARTER.TabIndex = 45;
            // 
            // rtDescription
            // 
            this.rtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtDescription.Location = new System.Drawing.Point(398, 337);
            this.rtDescription.MaxLength = 2000;
            this.rtDescription.Name = "rtDescription";
            this.rtDescription.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtDescription.Size = new System.Drawing.Size(403, 140);
            this.rtDescription.TabIndex = 47;
            this.rtDescription.Text = "변경내역 및 기타 설명을 입력하세요!";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(377, 337);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 140);
            this.label4.TabIndex = 48;
            this.label4.Text = "최신버전 설명";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnInit
            // 
            this.btnInit.Location = new System.Drawing.Point(146, 443);
            this.btnInit.Name = "btnInit";
            this.btnInit.Size = new System.Drawing.Size(70, 34);
            this.btnInit.TabIndex = 49;
            this.btnInit.Text = "초기화";
            this.btnInit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnInit.UseVisualStyleBackColor = true;
            this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
            // 
            // frmVersion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 507);
            this.Controls.Add(this.btnInit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.rtDescription);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboHEADQUARTER);
            this.Controls.Add(this.cboManageDept);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboFileType);
            this.Controls.Add(this.lstFile);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnUpload);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVersion";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "최신버전 등록";
            this.Load += new System.EventHandler(this.frmVersionManager_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmVersionManager_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVersion_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.ListView lstFile;
        private System.Windows.Forms.ColumnHeader FileName;
        private System.Windows.Forms.ColumnHeader FileType;
        private System.Windows.Forms.ComboBox cboFileType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader FileSize;
        private System.Windows.Forms.ColumnHeader FilePath;
        private System.Windows.Forms.ColumnHeader UpFlag;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboManageDept;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboHEADQUARTER;
        private System.Windows.Forms.ColumnHeader subfiles;
        private System.Windows.Forms.RichTextBox rtDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnInit;
    }
}