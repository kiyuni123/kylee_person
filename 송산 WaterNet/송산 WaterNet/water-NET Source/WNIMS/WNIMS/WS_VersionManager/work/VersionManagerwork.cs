﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WS_VersionManager.work
{
    public class VersionManagerwork : EMFrame.work.BaseWork
    {
        private static VersionManagerwork work = null;
        public static VersionManagerwork GetInstance()
        {
            if (work == null)
            {
                work = new VersionManagerwork();
            }

            return work;
        }

        /// <summary>
        /// 현재 리비전 이하의 모든 파일을 표시한다. 
        /// 데이터 및 시스템관련 파일전체를 표시한다.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public System.Data.DataTable GetVersionHistoryAllData(System.Collections.Hashtable param)
        {
            //이충섭 수정 2013.02.13 - 검색조건 변경
            StringBuilder oStringBuilder = new StringBuilder();
            
            //oStringBuilder.AppendLine("WITH VER AS                                                                   ");
            //oStringBuilder.AppendLine("(                                                                             ");
            //oStringBuilder.AppendLine("    SELECT A.MGDPCD                                                           ");
            //oStringBuilder.AppendLine("          ,C.FILE_NAME, C.FILE_TYPE                                           ");
            //oStringBuilder.AppendLine("          ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE    ");
            //oStringBuilder.AppendLine("      FROM CM_REVISION A                                                      ");
            //oStringBuilder.AppendLine("         , CM_VERSION B                                                       ");
            //oStringBuilder.AppendLine("         , CM_VERSION_FILE C                                                  ");
            //oStringBuilder.AppendLine("     WHERE A.MGDPCD = :MGDPCD                                                 ");
            //oStringBuilder.AppendLine("       AND A.MGDPCD = B.MGDPCD                                                ");
            //oStringBuilder.AppendLine("       AND A.ZONE_GBN = :ZONE_GBN                                             ");
            //oStringBuilder.AppendLine("       AND A.REVISION_NO = B.REVISION_NO                                      ");
            //oStringBuilder.AppendLine("       AND C.FILE_NAME = B.FILE_NAME                                          ");
            //oStringBuilder.AppendLine("       AND C.FILE_TYPE IN ('P','T')                                           ");
            //oStringBuilder.AppendLine("       AND A.REVISION_NO <= TO_NUMBER(:REVISION_NO)                           ");
            //oStringBuilder.AppendLine("     UNION ALL                                                                ");
            //oStringBuilder.AppendLine("     SELECT :MGDPCD MGDPCD                                                    ");
            //oStringBuilder.AppendLine("            ,C.FILE_NAME, C.FILE_TYPE                                         ");
            //oStringBuilder.AppendLine("            ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE  ");
            //oStringBuilder.AppendLine("       FROM CM_REVISION A                                                     ");
            //oStringBuilder.AppendLine("          , CM_VERSION B                                                      ");
            //oStringBuilder.AppendLine("          , CM_VERSION_FILE C                                                 ");
            //oStringBuilder.AppendLine("      WHERE A.REVISION_NO = B.REVISION_NO                                     ");
            //oStringBuilder.AppendLine("        AND A.ZONE_GBN = :ZONE_GBN                                            ");
            //oStringBuilder.AppendLine("        AND C.FILE_NAME = B.FILE_NAME                                         ");
            //oStringBuilder.AppendLine("        AND C.FILE_TYPE IN ('S')                                              ");
            //oStringBuilder.AppendLine("        AND A.REVISION_NO <= TO_NUMBER(:REVISION_NO)                          ");
            //oStringBuilder.AppendLine(")                                                                             ");
            //oStringBuilder.AppendLine("SELECT VER.MGDPCD, VER.REVISION_NO, VER.FILE_NAME                             ");
            //oStringBuilder.AppendLine("      ,VER.FILE_TYPE, VER.FILE_RNAME, VER.REG_UID                             ");
            //oStringBuilder.AppendLine("      ,VER.REG_DATE, VER.FILE_DATE                                            ");
            //oStringBuilder.AppendLine("  FROM VER                                                                    ");
            //oStringBuilder.AppendLine("       ,(SELECT FILE_NAME, MAX(REVISION_NO) REVISION_NO                       ");
            //oStringBuilder.AppendLine("           FROM VER                                                           ");
            //oStringBuilder.AppendLine("          GROUP BY FILE_NAME                                                  ");
            //oStringBuilder.AppendLine("        ) B                                                                   ");
            //oStringBuilder.AppendLine("  WHERE VER.FILE_NAME = B.FILE_NAME                                           ");
            //oStringBuilder.AppendLine("    AND VER.REVISION_NO = B.REVISION_NO                                       ");
            //oStringBuilder.AppendLine(" ORDER BY VER.REVISION_NO DESC, VER.FILE_NAME ASC                             ");

            oStringBuilder.AppendLine("WITH VER AS                                                                   ");
            oStringBuilder.AppendLine("(                                                                             ");
            oStringBuilder.AppendLine("    SELECT A.MGDPCD                                                           ");
            oStringBuilder.AppendLine("          ,C.FILE_NAME, C.FILE_TYPE                                           ");
            oStringBuilder.AppendLine("          ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE    ");
            oStringBuilder.AppendLine("      FROM CM_REVISION A                                                      ");
            oStringBuilder.AppendLine("         , CM_VERSION B                                                       ");
            oStringBuilder.AppendLine("         , CM_VERSION_FILE C                                                  ");
            oStringBuilder.AppendLine("     WHERE A.MGDPCD = :MGDPCD                                                 ");
            oStringBuilder.AppendLine("       AND A.MGDPCD = B.MGDPCD                                                ");
            oStringBuilder.AppendLine("       AND A.ZONE_GBN = :ZONE_GBN                                             ");
            oStringBuilder.AppendLine("       AND A.REVISION_NO = B.REVISION_NO                                      ");
            oStringBuilder.AppendLine("       AND C.FILE_NAME = B.FILE_NAME                                          ");
            oStringBuilder.AppendLine("       AND C.FILE_TYPE IN ('P','T')                                           ");
            oStringBuilder.AppendLine("       AND A.REVISION_NO <= TO_NUMBER(:REVISION_NO)                           ");
            oStringBuilder.AppendLine("       ORDER BY A.REVISION_NO DESC                                            ");
            oStringBuilder.AppendLine(")                                                                             ");
            oStringBuilder.AppendLine("SELECT VER.MGDPCD, VER.REVISION_NO, VER.FILE_NAME                             ");
            oStringBuilder.AppendLine("      ,VER.FILE_TYPE, VER.FILE_RNAME, VER.REG_UID                             ");
            oStringBuilder.AppendLine("      ,VER.REG_DATE, VER.FILE_DATE                                            ");
            oStringBuilder.AppendLine("  FROM VER                                                                    ");
            oStringBuilder.AppendLine("       ,(SELECT FILE_NAME, MAX(REVISION_NO) REVISION_NO                       ");
            oStringBuilder.AppendLine("           FROM VER                                                           ");
            oStringBuilder.AppendLine("          GROUP BY FILE_NAME                                                  ");
            oStringBuilder.AppendLine("        ) B                                                                   ");
            oStringBuilder.AppendLine("  WHERE VER.FILE_NAME = B.FILE_NAME                                           ");
            oStringBuilder.AppendLine("    AND VER.REVISION_NO = B.REVISION_NO                                       ");
            oStringBuilder.AppendLine(" ORDER BY VER.REVISION_NO DESC, VER.FILE_NAME ASC                             ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        /// <summary>
        /// 현재 리비전 이하의 모든 파일을 표시한다. 
        /// 데이터 파일만 표시하고, 시스템파일은 표시하지 않는다.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public System.Data.DataTable GetVersionHistoryAllData2(System.Collections.Hashtable param)
        {
            //이충섭 수정 2013.02.13 - 검색조건 변경

            StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.AppendLine("WITH VER AS                                                                   ");
            //oStringBuilder.AppendLine("(                                                                             ");
            //oStringBuilder.AppendLine("    SELECT A.MGDPCD                                                           ");
            //oStringBuilder.AppendLine("          ,C.FILE_NAME, C.FILE_TYPE                                           ");
            //oStringBuilder.AppendLine("          ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE    ");
            //oStringBuilder.AppendLine("      FROM CM_REVISION A                                                      ");
            //oStringBuilder.AppendLine("         , CM_VERSION B                                                       ");
            //oStringBuilder.AppendLine("         , CM_VERSION_FILE C                                                  ");
            //oStringBuilder.AppendLine("     WHERE A.MGDPCD = :MGDPCD                                                 ");
            //oStringBuilder.AppendLine("       AND A.MGDPCD = B.MGDPCD                                                ");
            //oStringBuilder.AppendLine("       AND A.ZONE_GBN = :ZONE_GBN                                             ");
            //oStringBuilder.AppendLine("       AND A.REVISION_NO = B.REVISION_NO                                      ");
            //oStringBuilder.AppendLine("       AND C.FILE_NAME = B.FILE_NAME                                          ");
            //oStringBuilder.AppendLine("       AND C.FILE_TYPE IN ('P','T')                                           ");
            //oStringBuilder.AppendLine("       AND A.REVISION_NO <= TO_NUMBER(:REVISION_NO)                           ");
            ////oStringBuilder.AppendLine("     UNION ALL                                                                ");
            ////oStringBuilder.AppendLine("     SELECT :MGDPCD MGDPCD                                                    ");
            ////oStringBuilder.AppendLine("            ,C.FILE_NAME, C.FILE_TYPE                                         ");
            ////oStringBuilder.AppendLine("            ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE  ");
            ////oStringBuilder.AppendLine("       FROM CM_REVISION A                                                     ");
            ////oStringBuilder.AppendLine("          , CM_VERSION B                                                      ");
            ////oStringBuilder.AppendLine("          , CM_VERSION_FILE C                                                 ");
            ////oStringBuilder.AppendLine("      WHERE A.REVISION_NO = B.REVISION_NO                                     ");
            ////oStringBuilder.AppendLine("        AND A.ZONE_GBN = :ZONE_GBN                                            ");
            ////oStringBuilder.AppendLine("        AND C.FILE_NAME = B.FILE_NAME                                         ");
            ////oStringBuilder.AppendLine("        AND C.FILE_TYPE IN ('S')                                              ");
            ////oStringBuilder.AppendLine("        AND A.REVISION_NO <= TO_NUMBER(:REVISION_NO)                          ");
            //oStringBuilder.AppendLine(")                                                                             ");
            //oStringBuilder.AppendLine("SELECT VER.MGDPCD, VER.REVISION_NO, VER.FILE_NAME                             ");
            //oStringBuilder.AppendLine("      ,VER.FILE_TYPE, VER.FILE_RNAME, VER.REG_UID                             ");
            //oStringBuilder.AppendLine("      ,VER.REG_DATE, VER.FILE_DATE                                            ");
            //oStringBuilder.AppendLine("  FROM VER                                                                    ");
            //oStringBuilder.AppendLine("       ,(SELECT FILE_NAME, MAX(REVISION_NO) REVISION_NO                       ");
            //oStringBuilder.AppendLine("           FROM VER                                                           ");
            //oStringBuilder.AppendLine("          GROUP BY FILE_NAME                                                  ");
            //oStringBuilder.AppendLine("        ) B                                                                   ");
            //oStringBuilder.AppendLine("  WHERE VER.FILE_NAME = B.FILE_NAME                                           ");
            //oStringBuilder.AppendLine("    AND VER.REVISION_NO = B.REVISION_NO                                       ");
            //oStringBuilder.AppendLine(" ORDER BY VER.REVISION_NO DESC, VER.FILE_NAME ASC                             ");

            oStringBuilder.AppendLine("WITH VER AS                                                                      ");
            oStringBuilder.AppendLine("(                                                                                ");
            oStringBuilder.AppendLine("     SELECT A.MGDPCD                                                             ");
            oStringBuilder.AppendLine("            ,C.FILE_NAME, C.FILE_TYPE                                            ");
            oStringBuilder.AppendLine("            ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE     ");
            oStringBuilder.AppendLine("       FROM CM_REVISION A                                                        ");
            oStringBuilder.AppendLine("          , CM_VERSION B                                                         ");
            oStringBuilder.AppendLine("          , CM_VERSION_FILE C                                                    ");
            oStringBuilder.AppendLine("      WHERE A.REVISION_NO = B.REVISION_NO                                        ");
            oStringBuilder.AppendLine("        AND C.FILE_NAME = B.FILE_NAME                                            ");
            oStringBuilder.AppendLine("        AND A.ZONE_GBN =:ZONE_GBN                                                ");
            oStringBuilder.AppendLine("        AND C.FILE_TYPE IN ('S')                                                 ");
            oStringBuilder.AppendLine("        AND A.REVISION_NO <= TO_NUMBER(:REVISION_NO)                             ");
            oStringBuilder.AppendLine(")                                                                                ");
            oStringBuilder.AppendLine("SELECT VER.MGDPCD, VER.REVISION_NO, VER.FILE_NAME                                ");
            oStringBuilder.AppendLine("      ,VER.FILE_TYPE, VER.FILE_RNAME, VER.REG_UID                                ");
            oStringBuilder.AppendLine("      ,VER.REG_DATE, VER.FILE_DATE                                               ");
            oStringBuilder.AppendLine("  FROM VER                                                                       ");
            oStringBuilder.AppendLine("       ,(SELECT FILE_NAME, MAX(REVISION_NO) REVISION_NO                          ");
            oStringBuilder.AppendLine("           FROM VER                                                              ");
            oStringBuilder.AppendLine("          GROUP BY FILE_NAME                                                     ");
            oStringBuilder.AppendLine("        ) B                                                                      ");
            oStringBuilder.AppendLine("  WHERE VER.FILE_NAME = B.FILE_NAME                                              ");
            oStringBuilder.AppendLine(" ORDER BY VER.REVISION_NO DESC, VER.FILE_NAME ASC		                        ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        /// <summary>
        /// 현재 리비전의 파일목록을 표시한다.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public System.Data.DataTable GetVersionHistoryCurrentData(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("WITH VER AS                                                                   ");
            oStringBuilder.AppendLine("(                                                                             ");
            oStringBuilder.AppendLine("    SELECT A.MGDPCD                                                           ");
            oStringBuilder.AppendLine("          ,C.FILE_NAME, C.FILE_TYPE                                           ");
            oStringBuilder.AppendLine("          ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE    ");
            oStringBuilder.AppendLine("      FROM CM_REVISION A                                                      ");
            oStringBuilder.AppendLine("         , CM_VERSION B                                                       ");
            oStringBuilder.AppendLine("         , CM_VERSION_FILE C                                                  ");
            oStringBuilder.AppendLine("     WHERE A.MGDPCD = :MGDPCD                                                 ");
            oStringBuilder.AppendLine("       AND A.MGDPCD = B.MGDPCD                                                ");
            oStringBuilder.AppendLine("       AND A.REVISION_NO = B.REVISION_NO                                      ");
            oStringBuilder.AppendLine("       AND C.FILE_NAME = B.FILE_NAME                                          ");
            oStringBuilder.AppendLine("       AND C.FILE_TYPE IN ('P','T')                                           ");
            oStringBuilder.AppendLine("       AND A.REVISION_NO = TO_NUMBER(:REVISION_NO)                            ");
            oStringBuilder.AppendLine("     UNION ALL                                                                ");
            oStringBuilder.AppendLine("     SELECT :MGDPCD MGDPCD                                                    ");
            oStringBuilder.AppendLine("            ,C.FILE_NAME, C.FILE_TYPE                                         ");
            oStringBuilder.AppendLine("            ,B.REVISION_NO, B.FILE_RNAME, B.REG_UID, B.REG_DATE, B.FILE_DATE  ");
            oStringBuilder.AppendLine("       FROM CM_REVISION A                                                     ");
            oStringBuilder.AppendLine("          , CM_VERSION B                                                      ");
            oStringBuilder.AppendLine("          , CM_VERSION_FILE C                                                 ");
            oStringBuilder.AppendLine("      WHERE A.REVISION_NO = B.REVISION_NO                                     ");
            oStringBuilder.AppendLine("        AND C.FILE_NAME = B.FILE_NAME                                         ");
            oStringBuilder.AppendLine("        AND C.FILE_TYPE IN ('S')                                              ");
            oStringBuilder.AppendLine("        AND A.REVISION_NO = TO_NUMBER(:REVISION_NO)                           ");
            oStringBuilder.AppendLine(")                                                                             ");
            oStringBuilder.AppendLine("SELECT VER.MGDPCD, VER.REVISION_NO, VER.FILE_NAME                             ");
            oStringBuilder.AppendLine("      ,VER.FILE_TYPE, VER.FILE_RNAME, VER.REG_UID                             ");
            oStringBuilder.AppendLine("      ,VER.REG_DATE, VER.FILE_DATE                                            ");
            oStringBuilder.AppendLine("  FROM VER                                                                    ");
            oStringBuilder.AppendLine("       ,(SELECT FILE_NAME, MAX(REVISION_NO) REVISION_NO                       ");
            oStringBuilder.AppendLine("           FROM VER                                                           ");
            oStringBuilder.AppendLine("          GROUP BY FILE_NAME                                                  ");
            oStringBuilder.AppendLine("        ) B                                                                   ");
            oStringBuilder.AppendLine("  WHERE VER.FILE_NAME = B.FILE_NAME                                           ");
            oStringBuilder.AppendLine("    AND VER.REVISION_NO = B.REVISION_NO                                       ");
            oStringBuilder.AppendLine(" ORDER BY VER.REVISION_NO DESC, VER.FILE_NAME ASC                            ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        public System.Data.DataTable GetRevisionHistoryData(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            //수정(2013.02.13)
            //oStringBuilder.AppendLine("SELECT A.MGDPCD, A.REVISION_NO, A.DESCRIPTION, A.REG_UID, A.REG_DATE, B.ZONE_GBN  ");
            //oStringBuilder.AppendLine("  FROM CM_REVISION A, MANAGEMENT_DEPARTMENT B"                                     );
            //oStringBuilder.AppendLine("WHERE A.MGDPCD = B.MGDPCD AND A.MGDPCD = :MGDPCD                                  ");
            //oStringBuilder.AppendLine("  AND A.REG_DATE BETWEEN TO_DATE(:STARTDAY,'YYYYMMDD')                        ");
            //oStringBuilder.AppendLine("                     AND TO_DATE(:ENDDAY,'YYYYMMDD') + 1 - 1/24/60            ");
            //oStringBuilder.AppendLine("ORDER BY A.REVISION_NO DESC                                                   ");

            oStringBuilder.AppendLine("SELECT A.MGDPCD, A.REVISION_NO, A.DESCRIPTION, A.REG_UID, A.REG_DATE, B.ZONE_GBN                 ");
            oStringBuilder.AppendLine("           FROM CM_REVISION A, MANAGEMENT_DEPARTMENT B, CM_VERSION C, CM_VERSION_FILE D          ");
            oStringBuilder.AppendLine("           WHERE A.MGDPCD = B.MGDPCD AND A.MGDPCD = :MGDPCD                                      ");
            oStringBuilder.AppendLine("             AND A.MGDPCD = C.MGDPCD                                                             ");
            oStringBuilder.AppendLine("             AND A.REVISION_NO = C.REVISION_NO                                                   ");
            oStringBuilder.AppendLine("             AND D.FILE_NAME = C.FILE_NAME                                                       ");
            oStringBuilder.AppendLine("             AND D.FILE_TYPE IN ('P','T')                                                        ");
            oStringBuilder.AppendLine("             AND A.REG_DATE BETWEEN TO_DATE(:STARTDAY,'YYYYMMDD')                                ");
            oStringBuilder.AppendLine("            AND TO_DATE(:ENDDAY,'YYYYMMDD') + 1 - 1/24/60                                        ");
            oStringBuilder.AppendLine("           GROUP BY A.MGDPCD, A.REVISION_NO, A.DESCRIPTION, A.REG_UID, A.REG_DATE, B.ZONE_GBN    ");
            oStringBuilder.AppendLine("           ORDER BY A.REVISION_NO DESC                                                           ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        public System.Data.DataTable GetRevisionHistoryData2(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.AppendLine("SELECT A.MGDPCD, A.REVISION_NO, A.DESCRIPTION, A.REG_UID, A.REG_DATE, B.ZONE_GBN                 ");
            oStringBuilder.AppendLine("           FROM CM_REVISION A, MANAGEMENT_DEPARTMENT B, CM_VERSION C, CM_VERSION_FILE D          ");
            oStringBuilder.AppendLine("           WHERE A.MGDPCD = B.MGDPCD                                                             ");
            oStringBuilder.AppendLine("             AND A.MGDPCD = C.MGDPCD                                                             ");
            oStringBuilder.AppendLine("             AND A.REVISION_NO = C.REVISION_NO                                                   ");
            oStringBuilder.AppendLine("             AND D.FILE_NAME = C.FILE_NAME                                                       ");
            oStringBuilder.AppendLine("             AND D.FILE_TYPE IN ('S')                                                            ");
            oStringBuilder.AppendLine("             AND A.REG_DATE BETWEEN TO_DATE(:STARTDAY,'YYYYMMDD')                                ");
            oStringBuilder.AppendLine("            AND TO_DATE(:ENDDAY,'YYYYMMDD') + 1 - 1/24/60                                        ");
            oStringBuilder.AppendLine("            AND A.ZONE_GBN = :ZONE_GBN                                                           ");
            oStringBuilder.AppendLine("           GROUP BY A.MGDPCD, A.REVISION_NO, A.DESCRIPTION, A.REG_UID, A.REG_DATE, B.ZONE_GBN    ");
            oStringBuilder.AppendLine("           ORDER BY A.REVISION_NO DESC															");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        public void SetRevisionDescription(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE CM_REVISION SET DESCRIPTION = :DESCRIPTION   ");
            oStringBuilder.AppendLine("WHERE MGDPCD = :MGDPCD                              ");
            oStringBuilder.AppendLine("  AND REVISION_NO = :REVISION_NO                    ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
        }

        public void DelVersionHistoryData(System.Collections.Generic.Dictionary<int, System.Collections.Hashtable> dic)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("DELETE FROM CM_VERSION                            ");
            oStringBuilder.AppendLine(" WHERE MGDPCD = :MGDPCD                           ");
            oStringBuilder.AppendLine("   AND REVISION_NO = TO_NUMBER(:REVISION_NO)  ");
            oStringBuilder.AppendLine("   AND FILE_NAME LIKE :FILE_NAME || '%'           ");

            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                mapper.BeginTransaction();

                foreach (System.Collections.Hashtable param in dic.Values)
	            {
                    int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
	            }

                mapper.CommitTransaction();
            }
            catch (Exception oException)
            {
                mapper.RollbackTransaction();
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
                throw;
            }
            finally
            {
                mapper.Close();
            }
        }

        ///// <summary>
        ///// 업로드 성공한 파일 정보 Save
        ///// </summary>
        //public void SaveVersionInfo(string strFileName, string strNewFileName, string strFileType, string strFileSize, string strFilePath)
        //{
        //    EMFrame.dm.EMapper mapper = null;
        //    try
        //    {
        //        mapper = new EMFrame.dm.EMapper("SVR");

        //        this.DeleteVersionInfor(strFileName);

        //        StringBuilder oStringBuilder = new StringBuilder();

        //        string strParam = "('" + strFileName + "', '" + strNewFileName + "', '" + strFileType + "', '" + strFileSize + "', '" + strFilePath + "', '" + EMFrame.statics.AppStatic.USER_ID + "', TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS'))";
        //        oStringBuilder.Remove(0, oStringBuilder.Length);
        //        oStringBuilder.AppendLine("INSERT   INTO    CM_VERSION");
        //        oStringBuilder.AppendLine("(FILE_NAME, FILE_RNAME, FILE_TYPE, FILE_SIZE, FILE_PATH, REG_UID, REG_DATE)");
        //        oStringBuilder.AppendLine("VALUES");
        //        oStringBuilder.AppendLine(strParam);


        //        mapper.ExecuteScript(oStringBuilder.ToString(), new IDataParameter[] { });
        //    }
        //    catch (Exception oException)
        //    {
        //        EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
        //    }
        //    finally
        //    {
        //        mapper.Close();
        //    }
        //}

        ///// <summary>
        ///// File Name으로 Version 정보 Delete
        ///// </summary>
        ///// <param name="strFileName"></param>
        //private void DeleteVersionInfor(string strFileName)
        //{
        //    EMFrame.dm.EMapper mapper = null;
        //    try
        //    {
        //        mapper = new EMFrame.dm.EMapper("SVR");


        //        StringBuilder oStringBuilder = new StringBuilder();
        //        oStringBuilder.AppendLine("DELETE   CM_VERSION");
        //        oStringBuilder.AppendLine("WHERE    FILE_NAME = '" + strFileName + "'");


        //        mapper.ExecuteScript(oStringBuilder.ToString(), new IDataParameter[] { });
        //    }
        //    catch (Exception oException)
        //    {
        //        EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
        //    }
        //    finally
        //    {
        //        mapper.Close();
        //    }
        //}

        #region 리비전-파일업로드 정보
        public void SaveVersionInfo(EMFrame.dm.EMapper mapper, System.Collections.Hashtable param)
        {
            try
            {
                SaveVersionFileInfo(mapper, param);

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("MERGE INTO CM_VERSION A                                          ");
                oStringBuilder.AppendLine("USING (SELECT :MGDPCD        MGDPCD                              ");
                oStringBuilder.AppendLine("             ,:FILE_NAME     FILE_NAME                           ");
                oStringBuilder.AppendLine("             ,:REVISION_NO REVISION_NO                       ");
                oStringBuilder.AppendLine("             ,:FILE_RNAME    FILE_RNAME                          ");
                oStringBuilder.AppendLine("             ,:FILE_SIZE     FILE_SIZE                           ");
                oStringBuilder.AppendLine("             ,:FILE_PATH     FILE_PATH                           ");
                oStringBuilder.AppendLine("             ,:REG_UID       REG_UID                             ");
                oStringBuilder.AppendLine("             ,:ZONE_GBN      ZONE_GBN                            ");
                oStringBuilder.AppendLine("             ,:FILE_DATE     FILE_DATE                           ");
                oStringBuilder.AppendLine("        FROM DUAL                                             ");
                oStringBuilder.AppendLine("       ) B                                                    ");
                oStringBuilder.AppendLine("   ON (                                                       ");
                oStringBuilder.AppendLine("       A.MGDPCD = B.MGDPCD AND A.FILE_NAME = B.FILE_NAME AND A.REVISION_NO = B.REVISION_NO ");
                oStringBuilder.AppendLine("      )                                                       ");
                oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
                oStringBuilder.AppendLine("      UPDATE SET A.FILE_RNAME = B.FILE_RNAME                    ");
                oStringBuilder.AppendLine("                ,A.FILE_SIZE = B.FILE_SIZE                      ");
                oStringBuilder.AppendLine("                ,A.FILE_PATH = B.FILE_PATH                      ");
                oStringBuilder.AppendLine("                ,A.REG_UID = B.REG_UID                          ");
                oStringBuilder.AppendLine("                ,A.REG_DATE = SYSDATE                           ");
                oStringBuilder.AppendLine("                ,A.FILE_DATE = TO_DATE(B.FILE_DATE,'YYYYMMDDHH24MISS')   ");
                oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                          ");
                oStringBuilder.AppendLine("      INSERT (A.MGDPCD                                          ");
                oStringBuilder.AppendLine("              ,A.FILE_NAME                                      ");
                oStringBuilder.AppendLine("              ,A.REVISION_NO                                  ");
                oStringBuilder.AppendLine("              ,A.FILE_RNAME                                     ");
                oStringBuilder.AppendLine("              ,A.FILE_SIZE                                      ");
                oStringBuilder.AppendLine("              ,A.FILE_PATH                                      ");
                oStringBuilder.AppendLine("              ,A.REG_UID                                        ");
                oStringBuilder.AppendLine("              ,A.ZONE_GBN                                       ");
                oStringBuilder.AppendLine("              ,A.FILE_DATE                                      ");
                oStringBuilder.AppendLine("              ,A.REG_DATE               )                       ");
                oStringBuilder.AppendLine("      VALUES (B.MGDPCD                                          ");
                oStringBuilder.AppendLine("              ,B.FILE_NAME                                      ");
                oStringBuilder.AppendLine("              ,B.REVISION_NO                                  ");
                oStringBuilder.AppendLine("              ,B.FILE_RNAME                                     ");
                oStringBuilder.AppendLine("              ,B.FILE_SIZE                                      ");
                oStringBuilder.AppendLine("              ,B.FILE_PATH                                      ");
                oStringBuilder.AppendLine("              ,B.REG_UID                                        ");
                oStringBuilder.AppendLine("              ,B.ZONE_GBN                                       ");
                oStringBuilder.AppendLine("              ,TO_DATE(B.FILE_DATE,'YYYYMMDDHH24MISS')          ");
                oStringBuilder.AppendLine("              ,SYSDATE                 )                        ");

                int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
                Console.WriteLine(i.ToString());

            }
            catch (Exception)
            {
                throw;
            }

        }

        public void SaveVersionFileInfo(EMFrame.dm.EMapper mapper, System.Collections.Hashtable param)
        {
            try
            {
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("MERGE INTO CM_VERSION_FILE A                                  ");
                oStringBuilder.AppendLine("USING (SELECT :FILE_NAME FILE_NAME                            ");
                oStringBuilder.AppendLine("             ,:FILE_TYPE FILE_TYPE                            ");
                oStringBuilder.AppendLine("             ,:ZONE_GBN  ZONE_GBN                             ");
                oStringBuilder.AppendLine("        FROM DUAL                                             ");
                oStringBuilder.AppendLine("       ) B                                                    ");
                oStringBuilder.AppendLine("   ON (                                                       ");
                oStringBuilder.AppendLine("       A.FILE_NAME = B.FILE_NAME                              ");
                oStringBuilder.AppendLine("      )                                                       ");
                oStringBuilder.AppendLine(" WHEN MATCHED THEN                                            ");
                oStringBuilder.AppendLine("      UPDATE SET A.FILE_TYPE = B.FILE_TYPE                    ");
                oStringBuilder.AppendLine(" WHEN NOT MATCHED THEN                                        ");
                oStringBuilder.AppendLine("      INSERT (A.FILE_NAME                                     ");
                oStringBuilder.AppendLine("             ,A.FILE_TYPE                                     ");
                oStringBuilder.AppendLine("             ,A.ZONE_GBN                 )                    ");
                oStringBuilder.AppendLine("      VALUES (B.FILE_NAME                                     ");
                oStringBuilder.AppendLine("             ,B.FILE_TYPE                                     ");
                oStringBuilder.AppendLine("             ,B.ZONE_GBN                 )                    ");

                mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public object SaveRevisionInfo(EMFrame.dm.EMapper mapper, System.Collections.Hashtable param)
        {
            object o = null;
            try
            {
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("INSERT INTO CM_REVISION (                            ");
                oStringBuilder.AppendLine("        MGDPCD                                       ");
                oStringBuilder.AppendLine("       ,REVISION_NO                                  ");
                oStringBuilder.AppendLine("       ,DESCRIPTION                                  ");
                oStringBuilder.AppendLine("       ,REG_UID                                      ");
                oStringBuilder.AppendLine("       ,ZONE_GBN                                     ");
                oStringBuilder.AppendLine("       ,REG_DATE                           )         ");
                oStringBuilder.AppendLine("VALUES (:MGDPCD                                        ");
                oStringBuilder.AppendLine("      ,(SELECT DECODE(MAX(REVISION_NO), NULL, 1, MAX(REVISION_NO)+1) REVISION_NO FROM CM_REVISION )");
                oStringBuilder.AppendLine("      ,:DESCRIPTION                                    ");
                oStringBuilder.AppendLine("      ,:REG_UID                                        ");
                oStringBuilder.AppendLine("      ,:ZONE_GBN                                       ");
                oStringBuilder.AppendLine("      ,SYSDATE                               )         ");

                mapper.ExecuteScript(oStringBuilder.ToString(), param);

                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("SELECT MAX(REVISION_NO) REVISION_NO FROM CM_REVISION ");
                o = mapper.ExecuteScriptScalar(oStringBuilder.ToString(), param);

            }
            catch (Exception)
            {
                throw;
            }

            return o;
        }


        #endregion 리비전-파일업로드 정보


    }
}
