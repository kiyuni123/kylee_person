﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using log4net;
using System.Collections.Generic;
using EMFrame.utils;

namespace EMFrame.dm
{
    /// <summary>
    /// 오라클 데이터베이스 관리자
    /// </summary>
    /// <remarks></remarks>
    public class EMapper : IDisposable
    {
        #region "필드 선언"
        private OracleConnection m_oConnection = null;
        private OracleTransaction m_oTransaction = null;

        private string m_strConnectionString = null;
        protected static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static IDictionary<string, string> _connectionString = new Dictionary<string, string>();
        public static IDictionary<string, string> ConnectionString
        {
            get
            {
                return _connectionString;
            }
        }
        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////////////////// Property

        #region "연결 - Connection"

        /// <summary>
        /// 연결
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public OracleConnection Connection
        {
            get 
            {
                return this.m_oConnection; 
            }

            set 
            {
                this.m_oConnection = value; 
            }
        }

        #endregion

        #region "트랜잭션 - Transaction"

        /// <summary>
        /// 트랜잭션
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public OracleTransaction Transaction
        {
            get
            {
                return this.m_oTransaction;
            }

            set
            {
                this.m_oTransaction = value;
            }
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////////////////// Method

        //////////////////////////////////////////////////////////////////////////////////////////// Public

        ////////////////////////////////////////////////////////////////////////////////// Common

        public EMapper()
        {
        }

        public EMapper(string connectionName)
        {
            foreach (string key in EMapper._connectionString.Keys)
            {
                if (key == connectionName)
                {
                    this.m_strConnectionString = EMapper._connectionString[key];
                }
            }

            this.Open();
        }

        public void SetConnectionString(string connectionName)
        {
            foreach (string key in EMapper._connectionString.Keys)
            {
                if (key == connectionName)
                {
                    this.m_strConnectionString = EMapper._connectionString[key];
                }
            }
        }

        #region "열기 - Open()"

        /// <summary>
        /// 열기
        /// </summary>
        /// <remarks></remarks>

        public void Open()
        {
            if (string.IsNullOrEmpty(m_strConnectionString))
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("Open() : DB 연결 주소가 설정되지 않았습니다.");
                }
                
                throw new Exception();
            }

            if (this.m_oConnection == null)
            {
                try
                {
                    this.m_oConnection = new OracleConnection();
                }
                catch (Exception ex)
                {
                    if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                    {
                        logger.Error("Open() : DB 연결 주소가 설정되지 않았습니다.");
                    }

                    throw ex;
                }
            }

            if (this.m_oConnection.State == ConnectionState.Closed)
            {
                try
                {
                    this.m_oConnection.ConnectionString = this.m_strConnectionString;
                    this.m_oConnection.Open();
                }
                catch (Exception ex)
                {
                    if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                    {
                        logger.Error("Open() : DB 접속을 실패했습니다.\n" + ex.Message);
                    }
                    
                    throw ex;
                }
            }
            else
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("Open() : DB 접속이 이미 연결되어 있습니다.");
                }
                
                throw new Exception();
            }
        }

        #endregion


        public bool IsBeginTransaction()
        {
            return this.m_oTransaction != null;
        }

        #region "트랜잭션 시작하기 - BeginTransaction()"

        /// <summary>
        /// 트랜잭션 시작하기
        /// </summary>
        /// <remarks></remarks>
        public void BeginTransaction()
        {
            if (this.m_oTransaction == null)
            {
                if (this.m_oConnection == null)
                {
                    if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                    {
                        logger.Error("BeginTransaction() : DB 연결이 되어 있지 않아 트랜잭션을 시작할 수 없습니다.");
                    }
                    
                    throw new Exception();
                }
                else
                {
                    if (this.m_oConnection.State == ConnectionState.Open)
                    {
                        try
                        {
                            this.m_oTransaction = this.m_oConnection.BeginTransaction();
                        }
                        catch (Exception ex)
                        {
                            if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                            {
                                logger.Error("BeginTransaction() : DB 트랜잭션 시작을 실패했습니다.\n" + ex.Message);
                            }
                            
                            throw ex;
                        }
                    }
                    else
                    {
                        if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                        {
                            logger.Error("BeginTransaction() : DB 연결이 되어 있지 않아 트랜잭션을 시작할 수 없습니다.");
                        }
                        
                        throw new Exception();
                    }
                }
            }
            else
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("BeginTransaction() : DB 트랜잭션이 이미 설정되어 있습니다.");
                }
                
                throw new Exception();
            }
        }

        #endregion

        #region "트랜잭션 완료하기 - CommitTransaction()"

        /// <summary>
        /// 트랜잭션 완료하기
        /// </summary>
        /// <remarks></remarks>
        public void CommitTransaction()
        {
            if (this.m_oTransaction == null)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("CommitTransaction() : DB 트랜잭션이 설정되어있지 않아 트랜잭션을 Commit할 수 없습니다.");
                }
                
                throw new Exception();
            }
            else
            {
                try
                {
                    this.m_oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                    {
                        logger.Error("CommitTransaction() : DB 트랜잭션 Commit을 실패했습니다.\n" + ex.Message);
                    }
                    
                    throw ex;
                }
            }
        }

        #endregion

        #region "트랜잭션 취소하기 - RollbackTransaction()"

        /// <summary>
        /// 트랜잭션 취소하기
        /// </summary>
        /// <remarks></remarks>
        public void RollbackTransaction()
        {
            if (this.m_oTransaction == null)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("RollbackTransaction() : DB 트랜잭션이 설정되어있지 않아 트랜잭션을 Rollback할 수 없습니다.");
                }
                
                throw new Exception();
            }
            else
            {
                try
                {
                    this.m_oTransaction.Rollback();
                }
                catch (Exception ex)
                {
                    if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                    {
                        logger.Error("RollbackTransaction() : DB 트랜잭션 Rollback을 실패했습니다.\n" + ex.Message);
                    }
                    
                    throw ex;
                }
            }
        }

        public void CloseTransaction()
        {
            try
            {
                if (this.m_oTransaction != null)
                {
                    this.m_oTransaction.Dispose();
                    this.m_oTransaction = null;
                }
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("CloseTransaction() : DB 트랜잭션 Close을 실패했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }
        }

        #endregion

        #region "닫기 - Close()"

        /// <summary>
        /// 닫기
        /// </summary>
        /// <remarks></remarks>
        public void Close()
        {
            try
            {
                if (this.m_oConnection != null)
                {
                    if (this.m_oConnection.State != ConnectionState.Closed)
                    {
                        try
                        {
                            this.m_oConnection.Close();
                            this.m_oConnection.Dispose();
                            this.m_oConnection = null;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("Close() : DB 연결 Close을 실패했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////// Script

        //////////////////////////////////////////////////////////////////////// Insert, Update, Delete

        #region "스크립트 실행하기 - ExecuteScript(strScript, aoDataParameter)"

        /// <summary>
        /// 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public int ExecuteScript(string strScript, IDataParameter[] aoDataParameter)
        {
            int nRowCount = 0;

            try
            {
                ExecuteScript(strScript, aoDataParameter, ref nRowCount);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScript(string strScript, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }
            
            return nRowCount;
        }

        /// <summary>
        /// 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public int ExecuteScript(string strScript, object dataParameter)
        {
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = this.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScript(string strScript, object dataParameter) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return ExecuteScript(strScript, aoDataParameter);

        }
        #endregion

        #region "스크립트 실행하기 - ExecuteScript(strScript, aoDataParameter, nRowCount)"

        /// <summary>
        /// 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="nRowCount">행 수</param>
        /// <remarks></remarks>
        public void ExecuteScript(string strScript, IDataParameter[] aoDataParameter, ref int nRowCount)
        {
            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);
                nRowCount = oCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScript(string strScript, IDataParameter[] aoDataParameter, ref int nRowCount) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }
        }

        /// <summary>
        /// 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="nRowCount">행 수</param>
        /// <remarks></remarks>
        public void ExecuteScript(string strScript, object dataParameter, ref int nRowCount)
        {
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = this.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScript(string strScript, object dataParameter, ref int nRowCount) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            ExecuteScript(strScript, aoDataParameter, ref nRowCount);

        }
        #endregion

        #region "결과값을 돌려주는 스크립트 실행하기 - ExecuteScriptReturnValue(strScript, aoDataParameter, eSqlDbTyoe, nSize)"

        /// <summary>
        /// 결과값을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteScriptReturnValue(string strScript, IDataParameter[] aoDataParameter, DbType eDbType, int nSize)
        {
            object data = null;
            try
            {
                data = ExecuteScriptReturnValue(strScript, aoDataParameter, eDbType, nSize);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptReturnValue(string strScript, IDataParameter[] aoDataParameter, DbType eDbType, int nSize) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return data;
        }

        /// <summary>
        /// 결과값을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteScriptReturnValue(string strScript, object dataParameter, DbType eDbType, int nSize)
        {
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = this.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptReturnValue(string strScript, object dataParameter, DbType eDbType, int nSize) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return ExecuteScriptReturnValue(strScript, aoDataParameter, eDbType, nSize);

        }
        #endregion

        #region "결과값을 돌려주는 스크립트 실행하기 - ExecuteScriptReturnValue(strScript, aoDataParameter, eSqlDbTyoe, nSize, nRowCount)"

        /// <summary>
        /// 결과값을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">SQL 데이터 종류</param>
        /// <param name="nSize">크기</param>
        /// <param name="nRowCount">행 수</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteScriptReturnValue(string strScript, IDataParameter[] aoDataParameter, DbType eDbType, int nSize, ref int nRowCount)
        {
            object oReturnValue = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommandReturnValue(strScript, aoDataParameter, eDbType, nSize);
                nRowCount = oCommand.ExecuteNonQuery();
                oReturnValue = oCommand.Parameters["ReturnValue"].Value;
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptReturnValue(string strScript, IDataParameter[] aoDataParameter, DbType eDbType, int nSize, ref int nRowCount) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oReturnValue;
        }

        /// <summary>
        /// 결과값을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">SQL 데이터 종류</param>
        /// <param name="nSize">크기</param>
        /// <param name="nRowCount">행 수</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteScriptReturnValue(string strScript, object dataParameter, DbType eDbType, int nSize, ref int nRowCount)
        {
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = this.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptReturnValue(string strScript, object dataParameter, DbType eDbType, int nSize, ref int nRowCount) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return ExecuteScriptReturnValue(strScript, aoDataParameter, eDbType, nSize, ref nRowCount);
        }
        #endregion

        #region "해시 테이블을 돌려주는 스크립트 실행하기 - ExecuteScriptHashtable(strScript, aoDataParameter)"

        /// <summary>
        /// 해시 테이블을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable ExecuteScriptHashtable(string strScript, IDataParameter[] aoDataParameter)
        {
            Hashtable oHashtable = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);
                oCommand.ExecuteNonQuery();
                oHashtable = new Hashtable();
                foreach (OracleParameter oParameter in oCommand.Parameters)
                {
                    if (oParameter.Direction == ParameterDirection.InputOutput | oParameter.Direction == ParameterDirection.Output)
                    {
                        oHashtable.Add(oParameter.ParameterName, oParameter.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptHashtable(string strScript, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oHashtable;
        }

        /// <summary>
        /// 해시 테이블을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable ExecuteScriptHashtable(string strScript, object dataParameter)
        {
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = this.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptHashtable(string strScript, object dataParameter) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return ExecuteScriptHashtable(strScript, aoDataParameter);
        }
        #endregion

        //////////////////////////////////////////////////////////////////////// Select

        #region "스칼라 값을 돌려주는 스크립트 실행하기 - ExecuteScriptScalar(strScript, aoDataParameter)"

        /// <summary>
        /// 스칼라 값을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>스칼라 값</returns>
        /// <remarks></remarks>
        public object ExecuteScriptScalar(string strScript, IDataParameter[] aoDataParameter)
        {
            object oScalar = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);
                oScalar = oCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptScalar(string strScript, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oScalar;
        }

        /// <summary>
        /// 스칼라 값을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>스칼라 값</returns>
        /// <remarks></remarks>
        public object ExecuteScriptScalar(string strScript, object dataParameter)
        {
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = this.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptScalar(string strScript, object dataParameter) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return ExecuteScriptScalar(strScript, aoDataParameter);

        }
        #endregion

        #region "데이터 리더를 돌려주는 스크립트 실행하기 - ExecuteScriptDataReader(strScript, aoDataParameter)"

        /// <summary>
        /// 데이터 리더를 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>데이터 리더</returns>
        /// <remarks></remarks>
        public OracleDataReader ExecuteScriptDataReader(string strScript, IDataParameter[] aoDataParameter)
        {
            OracleDataReader oDataReader = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataReader = oCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataReader(string strScript, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oDataReader;
        }

        /// <summary>
        /// 데이터 리더를 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>데이터 리더</returns>
        /// <remarks></remarks>
        public OracleDataReader ExecuteScriptDataReader(string strScript, object dataParameter)
        {
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = this.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataReader(string strScript, object dataParameter) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return ExecuteScriptDataReader(strScript, aoDataParameter);
        }
        #endregion

        #region "데이터Table을 돌려주는 스크립트 실행하기 - ExecuteScriptDataTable(strScript, aoDataParameter, strTableName)"
        public DataTable ExecuteScriptDataTable(string strScript, IDataParameter[] aoDataParameter)
        {
            DataTable data = null;

            try
            {
                data = ExecuteScriptDataTable(strScript, aoDataParameter, "Table0");
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataTable(string strScript, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }

                throw ex;
            }

            return data;
        }

        public DataTable ExecuteScriptDataTable(string strScript, object dataParameter)
        {
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = this.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataTable(string strScript, object dataParameter) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return ExecuteScriptDataTable(strScript, aoDataParameter);

        }

        public DataTable ExecuteScriptDataTable(string strScript, IDataParameter[] aoDataParameter, string strTableName)
        {
            DataTable data = new DataTable();

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                data.TableName = strTableName;
                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(data);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataTable(string strScript, IDataParameter[] aoDataParameter, string strTableName) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }

                throw ex;
            }

            return data;
        }

        public DataTable ExecuteScriptDataTable(string strScript, object dataParameter, string strTableName)
        {
            DataTable data = new DataTable();
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = this.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataTable(string strScript, object dataParameter, string strTableName) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return ExecuteScriptDataTable(strScript, aoDataParameter, strTableName);
        }
        #endregion

        #region "데이터셋을 돌려주는 스크립트 실행하기 - ExecuteScriptDataSet(strScript, aoDataParameter, strTableName)"
        public DataSet ExecuteScriptDataSet(string strScript, IDataParameter[] aoDataParameter)
        {
            DataSet data = new DataSet();

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(data, "Table0");
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataSet(string strScript, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return data;
        }

        public DataSet ExecuteScriptDataSet(string strScript, object dataParameter)
        {
            DataSet data = new DataSet();
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = ConvertUtils.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataSet(string strScript, object dataParameter) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(data, "Table0");
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataSet(string strScript, object dataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return data;
        }

        public DataSet ExecuteScriptDataSet(string strScript, IDataParameter[] aoDataParameter, string strTableName)
        {
            DataSet data = new DataSet();

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(data, strTableName);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataSet(string strScript, IDataParameter[] aoDataParameter, string strTableName) : DB 작업중 오류가 발생했습니다..\n" + ex.Message);
                }
                
                throw ex;
            }

            return data;
        }

        public DataSet ExecuteScriptDataSet(string strScript, object dataParameter, string strTableName)
        {
            DataSet data = new DataSet();
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = ConvertUtils.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataSet(string strScript, object dataParameter, string strTableName) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(data, strTableName);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteScriptDataSet(string strScript, object dataParameter, string strTableName) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return data;
        }
        #endregion

        #region "스크립트를 실행해 데이터셋 채우기 - FillDataSetScript(oDataSet, strTableName, strScript, aoDataParameter)"

        /// <summary>
        /// 스크립트를 실행해 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public void FillDataSetScript(DataSet oDataSet, string strTableName, string strScript, IDataParameter[] aoDataParameter)
        {
            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("FillDataSetScript(DataSet oDataSet, string strTableName, string strScript, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }
        }

        /// <summary>
        /// 스크립트를 실행해 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public void FillDataSetScript(DataSet oDataSet, string strTableName, string strScript, object dataParameter)
        {
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = ConvertUtils.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("FillDataSetScript(DataSet oDataSet, string strTableName, string strScript, object dataParameter) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("FillDataSetScript(DataSet oDataSet, string strTableName, string strScript, object dataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }
        }
        #endregion

        #region "스크립트를 실행해 해시 테이블을 돌려주고 데이터셋 채우기 - FillDataSetScriptHashtable(oDataSet, strTableName, strScript, aoDataParameter)"

        /// <summary>
        /// 스크립트를 실행해 해시 테이블을 돌려주고 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable FillDataSetScriptHashtable(DataSet oDataSet, string strTableName, string strScript, IDataParameter[] aoDataParameter)
        {
            Hashtable oHashtable = null;

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateStoredProcedureCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(oDataSet, strTableName);

                foreach (OracleParameter oParameter in oDataAdapter.SelectCommand.Parameters)
                {
                    if (oParameter.Direction == ParameterDirection.InputOutput | oParameter.Direction == ParameterDirection.Output)
                    {
                        oHashtable.Add(oParameter.ParameterName, oParameter.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("FillDataSetScriptHashtable(DataSet oDataSet, string strTableName, string strScript, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oHashtable;
        }

        /// <summary>
        /// 스크립트를 실행해 해시 테이블을 돌려주고 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable FillDataSetScriptHashtable(DataSet oDataSet, string strTableName, string strScript, object dataParameter)
        {
            Hashtable oHashtable = null;
            IDataParameter[] aoDataParameter = null;

            try
            {
                aoDataParameter = ConvertUtils.ConvertDataParameter(strScript, dataParameter);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("FillDataSetScriptHashtable(DataSet oDataSet, string strTableName, string strScript, object dataParameter) : 매개변수 생성에 실패하였습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateStoredProcedureCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(oDataSet, strTableName);

                foreach (OracleParameter oParameter in oDataAdapter.SelectCommand.Parameters)
                {
                    if (oParameter.Direction == ParameterDirection.InputOutput | oParameter.Direction == ParameterDirection.Output)
                    {
                        oHashtable.Add(oParameter.ParameterName, oParameter.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("FillDataSetScriptHashtable(DataSet oDataSet, string strTableName, string strScript, object dataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oHashtable;
        }
        #endregion

        ////////////////////////////////////////////////////////////////////////////////// Stored Procedure

        //////////////////////////////////////////////////////////////////////// Insert, Update, Delete

        #region "저장 프로시저 실행하기 - ExecuteStoredProcedure(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public void ExecuteStoredProcedure(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            int nRowCount = 0;

            try
            {
                ExecuteStoredProcedure(strStoredProcedureName, aoDataParameter, ref nRowCount);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteStoredProcedure(string strStoredProcedureName, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }
        }

        #endregion

        #region "저장 프로시저 실행하기 - ExecuteStoredProcedure(strStoredProcedureName, aoDataParameter, nRowCount)"

        /// <summary>
        /// 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="nRowCount">행 수</param>
        /// <remarks></remarks>
        public void ExecuteStoredProcedure(string strStoredProcedureName, IDataParameter[] aoDataParameter, ref int nRowCount)
        {
            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                nRowCount = oCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteStoredProcedure(string strStoredProcedureName, IDataParameter[] aoDataParameter, ref int nRowCount) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }
        }

        #endregion

        #region "결과값을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureReturnValue(strStoredProcedureName, aoDataParameter, eSqlDbTyoe, nSize)"

        /// <summary>
        /// 결과값을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteStoredProcedureReturnValue(string strStoredProcedureName, IDataParameter[] aoDataParameter, DbType eDbType, int nSize)
        {
            int nRowCount = 0;
            try
            {
                ExecuteStoredProcedureReturnValue(strStoredProcedureName, aoDataParameter, eDbType, nSize, ref nRowCount);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteStoredProcedureReturnValue(string strStoredProcedureName, IDataParameter[] aoDataParameter, DbType eDbType, int nSize) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return nRowCount;
        }

        #endregion

        #region "결과값을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureReturnValue(strStoredProcedureName, aoDataParameter, eSqlDbTyoe, nSize, nRowCount)"

        /// <summary>
        /// 결과값을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <param name="nRowCount">행 수</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteStoredProcedureReturnValue(string strStoredProcedureName, IDataParameter[] aoDataParameter, DbType eDbType, int nSize, ref int nRowCount)
        {
            object oReturnValue = null;

            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommandReturnValue(strStoredProcedureName, aoDataParameter, eDbType, nSize);
                nRowCount = oCommand.ExecuteNonQuery();
                oReturnValue = oCommand.Parameters["ReturnValue"].Value;
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteStoredProcedureReturnValue(string strStoredProcedureName, IDataParameter[] aoDataParameter, DbType eDbType, int nSize, ref int nRowCount) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oReturnValue;
        }

        #endregion

        #region "해시 테이블을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureHashtable(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 해시 테이블을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable ExecuteStoredProcedureHashtable(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            Hashtable oHashtable = null;

            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                oCommand.ExecuteNonQuery();
                oHashtable = new Hashtable();
                foreach (OracleParameter oParameter in oCommand.Parameters)
                {
                    if (oParameter.Direction == ParameterDirection.InputOutput | oParameter.Direction == ParameterDirection.Output)
                    {
                        oHashtable.Add(oParameter.ParameterName, oParameter.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteStoredProcedureHashtable(string strStoredProcedureName, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oHashtable;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////// Select

        #region "스칼라 값을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureScalar(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 스칼라 값을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>스칼라 값</returns>
        /// <remarks></remarks>
        public object ExecuteStoredProcedureScalar(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            object oScalar = null;

            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                oScalar = oCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteStoredProcedureScalar(string strStoredProcedureName, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oScalar;
        }

        #endregion

        #region "데이터 리더를 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureOracleDataReader(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 데이터 리더를 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>데이터 리더</returns>
        /// <remarks></remarks>
        public OracleDataReader ExecuteStoredProcedureOracleDataReader(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            OracleDataReader oDataReader = null;

            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                oDataReader = oCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteStoredProcedureOracleDataReader(string strStoredProcedureName, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oDataReader;
        }

        #endregion

        #region "데이터셋을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureDataSet(strStoredProcedureName, aoDataParameter, strTableName)"

        /// <summary>
        /// 데이터셋을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="strTableName">테이블명</param>
        /// <returns>데이터셋</returns>
        /// <remarks></remarks>
        public DataSet ExecuteStoredProcedureDataSet(string strStoredProcedureName, IDataParameter[] aoDataParameter, string strTableName)
        {
            DataSet oDataSet = null;

            try
            {
                oDataSet = new DataSet();
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("ExecuteStoredProcedureDataSet(string strStoredProcedureName, IDataParameter[] aoDataParameter, string strTableName) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oDataSet;
        }

        #endregion

        #region "저장 프로시저를 실행해 데이터셋 채우기 - FillDataSetStoredProcedure(oDataSet, strTableName, strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 저장 프로시저를 실행해 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public void FillDataSetStoredProcedure(DataSet oDataSet, string strTableName, string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("FillDataSetStoredProcedure(DataSet oDataSet, string strTableName, string strStoredProcedureName, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }
        }

        #endregion

        #region "저장 프로시저를 실행해 해시 테이블을 돌려주고 데이터셋 채우기 - FillDataSetStoredProcedureHashtable(oDataSet, strTableName, strStoredProcedure, aoDataParameter)"

        /// <summary>
        /// 저장 프로시저를 실행해 해시 테이블을 돌려주고 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable FillDataSetStoredProcedureHashtable(DataSet oDataSet, string strTableName, string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            Hashtable oHashtable = null;

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.SelectCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                oDataAdapter.Fill(oDataSet, strTableName);

                foreach (OracleParameter oParameter in oDataAdapter.SelectCommand.Parameters)
                {
                    if (oParameter.Direction == ParameterDirection.InputOutput | oParameter.Direction == ParameterDirection.Output)
                    {
                        oHashtable.Add(oParameter.ParameterName, oParameter.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                if (logger.Logger.Repository.Configured && logger.IsErrorEnabled)
                {
                    logger.Error("FillDataSetStoredProcedureHashtable(DataSet oDataSet, string strTableName, string strStoredProcedureName, IDataParameter[] aoDataParameter) : DB 작업중 오류가 발생했습니다.\n" + ex.Message);
                }
                
                throw ex;
            }

            return oHashtable;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////// Private

        ////////////////////////////////////////////////////////////////////////////////// Script

        #region "스크립트 명령 생성하기 - CreateScriptCommand(strScript, aoDataParameter)"

        /// <summary>
        /// 스크립트 명령 생성하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>생성 명령</returns>
        /// <remarks></remarks>
        private OracleCommand CreateScriptCommand(string strScript, IDataParameter[] aoDataParameter)
        {
            OracleCommand oCommand = new OracleCommand(strScript, m_oConnection);
            oCommand.CommandType = CommandType.Text;
            try
            {

                if (aoDataParameter != null)
                {
                    foreach (OracleParameter oParameter in aoDataParameter)
                    {
                        oCommand.Parameters.Add(oParameter);
                    }
                }

                if (m_oTransaction != null)
                {
                    oCommand.Transaction = m_oTransaction;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return oCommand;
        }

        #endregion

        #region "결과값을 돌려주는 스크립트 명령 생성하기 - CreateScriptCommandReturnValue(strScript, adoDataParameter, eDBType, nSize)"

        /// <summary>
        /// 결과값을 돌려주는 스크립트 명령 생성하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <returns>생성 명령</returns>
        /// <remarks></remarks>
        private OracleCommand CreateScriptCommandReturnValue(string strScript, IDataParameter[] aoDataParameter, DbType eDbType, int nSize)
        {
            OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);
            OracleParameter oParameter = new OracleParameter();
            oParameter.ParameterName = "ReturnValue";
            oParameter.DbType = eDbType;
            oParameter.Direction = ParameterDirection.ReturnValue;
            oParameter.Size = nSize;
            oCommand.Parameters.Add(oParameter);
            return oCommand;
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////// Stored Procedure

        #region "저장 프로시저 명령 생성하기 - CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 저장 프로시저 명령 생성하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>생성 명령</returns>
        /// <remarks></remarks>
        private OracleCommand CreateStoredProcedureCommand(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            OracleCommand oCommand = new OracleCommand(strStoredProcedureName, m_oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            if (aoDataParameter != null)
            {
                foreach (OracleParameter oParameter in aoDataParameter)
                {
                    oCommand.Parameters.Add(oParameter);
                }
            }

            if (m_oTransaction != null)
            {
                oCommand.Transaction = m_oTransaction;
            }

            return oCommand;
        }

        #endregion

        #region "결과값을 돌려주는 저장 프로시저 명령 생성하기 - CreateStoredProcedureCommandReturnValue(strStoredProcedureName, aoDataParameter, eDbType, nSize)"

        /// <summary>
        /// 결과값을 돌려주는 저장 프로시저 명령 생성하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <returns>생성 명령</returns>
        /// <remarks></remarks>
        private OracleCommand CreateStoredProcedureCommandReturnValue(string strStoredProcedureName, IDataParameter[] aoDataParameter, DbType eDbType, int nSize)
        {
            OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
            OracleParameter oParameter = new OracleParameter();
            oParameter.ParameterName = "ReturnValue";
            oParameter.DbType = eDbType;
            oParameter.Direction = ParameterDirection.ReturnValue;
            oParameter.Size = nSize;
            oCommand.Parameters.Add(oParameter);
            return oCommand;
        }

        #endregion

        #region IDisposable 멤버

        public void Dispose()
        {
            this.CloseTransaction();
            this.Close();
        }

        #endregion

        //Hashtable 형태의 파라미터를 오라클 데이터 파라미터 형식으로 반환한다.
        private IDataParameter[] ConvertDataParameter(string query, object aoDataParameter)
        {
            Hashtable parameters = aoDataParameter as Hashtable;

            if (parameters == null)
            {
                return null;
            }

            IList<IDataParameter> parameter = new List<IDataParameter>();
            int strCount = 0;

            for (int i = 0; i < query.Length; i++)
            {
                if (query[i] == '\'')
                {
                    strCount++;
                }

                if (query[i] == ':' && strCount % 2 == 0)
                {
                    if (i < query.Length - 1)
                    {
                        int startIndex = i + 1;
                        int endIndex = 0;

                        for (int j = i; j < query.Length; j++)
                        {
                            if (query[j] == ' ' || query[j] == ',' || query[j] == ')')
                            {
                                endIndex = j;
                                break;
                            }

                            if (j == query.Length - 1)
                            {
                                endIndex = j - 1;
                                break;
                            }
                        }

                        if (endIndex <= 0)
                        {
                            endIndex = query.Length;
                        }

                        string parameter_name = query.Substring(startIndex, endIndex - startIndex);
                        OracleParameter oracleParameter = new OracleParameter(parameter_name, OracleDbType.Varchar2);
                        oracleParameter.Value = parameters[parameter_name];
                        parameter.Add(oracleParameter);
                    }
                }
            }

            if (parameter.Count == 0)
            {
                return null;
            }

            return parameter.ToArray();
        }
    }
}
