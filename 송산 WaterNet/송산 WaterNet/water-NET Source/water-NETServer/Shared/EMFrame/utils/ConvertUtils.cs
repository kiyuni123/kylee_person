﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using Oracle.DataAccess.Client;
using System.Collections;

namespace EMFrame.utils
{
    public class ConvertUtils
    {

        //Hashtable 형태의 파라미터를 오라클 데이터 파라미터 형식으로 반환한다.
        public static IDataParameter[] ConvertDataParameter(string query, object aoDataParameter)
        {
            Hashtable parameters = aoDataParameter as Hashtable;

            if (parameters == null)
            {
                return null;
            }

            IList<IDataParameter> parameter = new List<IDataParameter>();
            int strCount = 0;

            for (int i = 0; i < query.Length; i++)
            {
                if (query[i] == '\'')
                {
                    strCount++;
                }

                if (query[i] == ':' && strCount % 2 == 0)
                {
                    if (i < query.Length - 1)
                    {
                        int startIndex = i + 1;
                        int endIndex = 0;

                        for (int j = i; j < query.Length; j++)
                        {
                            if (query[j] == ' ' || query[j] == ',' || query[j] == ')')
                            {
                                endIndex = j;
                                break;
                            }

                            if (j == query.Length - 1)
                            {
                                endIndex = j - 1;
                                break;
                            }
                        }

                        if (endIndex <= 0)
                        {
                            endIndex = query.Length;
                        }

                        string parameter_name = query.Substring(startIndex, endIndex - startIndex);
                        OracleParameter oracleParameter = new OracleParameter(parameter_name, OracleDbType.Varchar2);
                        oracleParameter.Value = parameters[parameter_name];
                        parameter.Add(oracleParameter);
                    }
                }
            }

            if (parameter.Count == 0)
            {
                return null;
            }

            return parameter.ToArray();
        }

        public static double ToDouble(object value)
        {
            if (value == null) return 0.0;
            if (string.IsNullOrEmpty(Convert.ToString(value))) return 0.0;
            double result = 0;
            if (double.TryParse(Convert.ToString(value), out result)) { ; }
            return result;
        }

        public static float ToSingle(object value)
        {
            if (value == null) return 0;
            if (string.IsNullOrEmpty(Convert.ToString(value))) return 0;
            float result = 0;
            if (float.TryParse(Convert.ToString(value), out result)) { ; }
            return result;
        }

        public static string ToString(object value)
        {
            if (value == null) return string.Empty;
            if (string.IsNullOrEmpty(Convert.ToString(value))) return string.Empty;

            return Convert.ToString(value);
        }

        //널체크 (널을 빈문자열로...)
        public static string NullToString(object obj)
        {
            string result = "";

            if(obj != null)
            {
                result = obj.ToString();
            }

            return result;
        }

        //일련번호 생성 (15자리의 숫자로 구성된 일련번호 반환)
        public static string GetSerialNo()
        {
            Random random = new Random();

            return (DateTime.Now.Year.ToString()).Substring(2,2)
                    + (DateTime.Now.Month.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Day.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Hour.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Minute.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Second.ToString()).PadLeft(2, '0')
                    + (random.Next(1, 999).ToString()).PadLeft(3, '0');
        }

        #region C# 암호화관련 Function

        /// <summary>
        /// 현재는 8자리의 암호키를 설정해야함.
        /// 추후 기능개선 필요
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string EncryptKey(string strToEncrypt)
        {
            try
            {
                byte[] key = { };
                byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80};
                byte[] inputByteArray; //Convert.ToByte(stringToEncrypt.Length) 

                key = Encoding.UTF8.GetBytes("_EMATRIX");
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Encoding.UTF8.GetBytes(strToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        /// <summary>
        /// AES 256 암호화
        /// </summary>
        /// <param name="strToEncrypt"></param>
        /// <returns></returns>
        public static string AESEncrypt256(string strToEncrypt)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            //입력받은 문자열을 바이트 배열로 변환
            byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(strToEncrypt);

            //딕셔너리 공격을 대비해서 키를 더 풀기 어렵게 만들기 위해서
            //Salt를 사용한다.
            byte[] Salt = Encoding.ASCII.GetBytes(strToEncrypt.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(strToEncrypt, Salt);

            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

            MemoryStream memoryStream = new MemoryStream();

            CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);

            cryptoStream.Write(PlainText, 0, PlainText.Length);

            cryptoStream.FlushFinalBlock();

            byte[] CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            cryptoStream.Close();

            string EncryptedData = Convert.ToBase64String(CipherBytes);

            return EncryptedData;

            //byte[] inputText = System.Text.Encoding.Unicode.GetBytes(strToEncrypt);
            //byte[] passwordSalt = Encoding.ASCII.GetBytes(strToEncrypt.Length.ToString());

            //rijAlg.IV = secretKey.GetBytes(16);

            //ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

            //MemoryStream msEncrypt = new MemoryStream();

            //CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

            //csEncrypt.Write(inputText, 0, inputText.Length);

            //csEncrypt.FlushFinalBlock();

            //byte[] encryptBytes = msEncrypt.ToArray();

            //msEncrypt.Close();
            //csEncrypt.Close();

            //// Base64 
            //string encryptedData = Convert.ToBase64String(encryptBytes);

            //return encryptedData;

            //RijndaelManaged aes = new RijndaelManaged();
            //aes.KeySize = 256;
            //aes.BlockSize = 128;
            //aes.Mode = CipherMode.CBC;
            //aes.Padding = PaddingMode.PKCS7;
            //aes.Key = Encoding.UTF8.GetBytes("_EMATRIX");
            //aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            //var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
            //byte[] xBuff = null;
            //using (var ms = new MemoryStream())
            //{
            //    using (var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
            //    {
            //        byte[] xXml = Encoding.UTF8.GetBytes(strToEncrypt);
            //        cs.Write(xXml, 0, xXml.Length);
            //    }

            //    xBuff = ms.ToArray();
            //}

            //String Output = Convert.ToBase64String(xBuff);
            //return Output;
        }

        /// <summary>
        /// 현재는 8자리의 암호키를 설정해야함.[추후 기능개선 필요]
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string DecryptKey(string strToDecrypt)
        {
            try
            {
                byte[] key = { };
                byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
                byte[] inputByteArray = new byte[strToDecrypt.Length];
                key = Encoding.UTF8.GetBytes("_EMATRIX");
                //key = Encoding.UTF8.GetBytes("WATERNET");
                
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();

                inputByteArray = Convert.FromBase64String(strToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        #endregion
    }
}
