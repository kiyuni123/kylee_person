﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using WaterNETServer.Common.DB.HistorianOLE;
using EMFrame.log;

namespace WaterNETServer.IF_iwater.dao
{
    public class OleDBWork : OleDBBaseDao
    {
        WaterNETServer.Common.utils.Common utils = new WaterNETServer.Common.utils.Common();

        private OleDBDao hDao = null;

        public OleDBWork()
        {
            hDao = new OleDBDao();
        }

        // 실시간 태그 목록에 따른 실시간 데이터를 조회
        public DataSet selectRawData(string tableName, DataSet result1, Hashtable cTime)
        {
            DataSet result = new DataSet();
            DataTable resultTable = new DataTable();
            #region 컬럼리스트
            resultTable.Columns.Add("TAGNAME");
            resultTable.Columns.Add("TIMESTAMP");
            resultTable.Columns.Add("VALUE");
            resultTable.Columns.Add("QUALITY");
            resultTable.Columns.Add("OPCQUALITYVALID");
            resultTable.Columns.Add("OPCQUALITY");
            resultTable.Columns.Add("SAMPLINGMODE");
            resultTable.Columns.Add("DIRECTION");
            resultTable.Columns.Add("NUMBEROFSAMPLES");
            resultTable.Columns.Add("INTERVALMILLISECONDS");
            resultTable.Columns.Add("CALCULATIONMODE");
            resultTable.Columns.Add("FILTERTAG");
            resultTable.Columns.Add("FILTERMODE");
            resultTable.Columns.Add("FILTERCOMPARISONMODE");
            resultTable.Columns.Add("FILTERVALUE");
            resultTable.Columns.Add("TIMEZONE");
            resultTable.Columns.Add("DAYLIGHTSAVINGTIME");
            resultTable.Columns.Add("ROWCOUNT");
            #endregion
            try
            {
                ConnectionOpen();
                DataRowCollection rows = result1.Tables[0].Rows;
                foreach (DataRow row in rows)
                {
                    string key = row["TAGNAME"].ToString();
                    hDao.selectRawData_Dao(oleDBManager, result, tableName, key, cTime);

                    if (result.Tables.Count > 0)
                    {
                        DataRowCollection tempRows = result.Tables[0].Rows;

                        foreach (DataRow tempRow in tempRows)
                        {
                            DataRow resultRow = resultTable.NewRow();
                            #region 컬럼목록
                            resultRow["TAGNAME"]                = tempRow["TAGNAME"];
                            resultRow["TIMESTAMP"]              = tempRow["TIMESTAMP"];
                            resultRow["VALUE"]                  = tempRow["VALUE"];
                            resultRow["QUALITY"]                = tempRow["QUALITY"];
                            resultRow["OPCQUALITYVALID"]        = tempRow["OPCQUALITYVALID"];
                            resultRow["OPCQUALITY"]             = tempRow["OPCQUALITY"];
                            resultRow["SAMPLINGMODE"]           = tempRow["SAMPLINGMODE"];
                            resultRow["DIRECTION"]              = tempRow["DIRECTION"];
                            resultRow["NUMBEROFSAMPLES"]        = tempRow["NUMBEROFSAMPLES"];
                            resultRow["INTERVALMILLISECONDS"]   = tempRow["INTERVALMILLISECONDS"];
                            resultRow["CALCULATIONMODE"]        = tempRow["CALCULATIONMODE"];
                            resultRow["FILTERTAG"]              = tempRow["FILTERTAG"];
                            resultRow["FILTERMODE"]             = tempRow["FILTERMODE"];
                            resultRow["FILTERCOMPARISONMODE"]   = tempRow["FILTERCOMPARISONMODE"];
                            resultRow["FILTERVALUE"]            = tempRow["FILTERVALUE"];
                            resultRow["TIMEZONE"]               = tempRow["TIMEZONE"];
                            resultRow["DAYLIGHTSAVINGTIME"]     = tempRow["DAYLIGHTSAVINGTIME"];
                            resultRow["ROWCOUNT"]               = tempRow["ROWCOUNT"];
                            #endregion
                            resultTable.Rows.Add(resultRow);
                        }
                        result.Tables.RemoveAt(0);
                    }
                }
                resultTable.TableName = tableName;
                result.Tables.Add(resultTable);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                ConnectionClose();
            }
            return result;
        }

        /// <summary>
        /// 실시간 데이터 누락 태그 조회
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="tagName"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public DataSet selectMissingData(string tableName, string tagName, Hashtable param)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                hDao.selectRawData_Dao(oleDBManager, result, tableName, tagName, param);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                ConnectionClose();
            }

            return result;
        }


        // Historian 에서 최종 시간을 조회 후 시간 조건절을 반환한다.
        // CurrentValue : 2010-10-26 10:38
        //  : <= 2010-10-26 10:37
        //  : >  2010-10-26 10:36
        // --> CurrentValue의 최종시간으로 사용하게되면 미처 데이터가 수집되지 않은 경우도 있으므로
        //     1분 작은 시간을 조건절로 사용한다.
        public Hashtable selectCurrentTimeStr()
        {
            Hashtable param = new Hashtable();
            try
            {
                ConnectionOpen();

                DateTime cTime = (DateTime)hDao.selectCurrentTimeDao(oleDBManager);

                // CurrentValue의 timestamp 보다 1분 작은 시간을 조회한다.
                // 3분 작은 시간으로 수정 (2011.03.23.)
                // : 1분 작은 시간을 조회하는데도 누락(미수집)되는 건이 생김
                param = utils.GetTimestampHistorianCondition(cTime, -3);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                ConnectionClose();
            }

            return param;
        }

        public Hashtable selectYesterdayStr()
        {
            Hashtable param = new Hashtable();
            try
            {
                ConnectionOpen();

                // 서버 시간을 잴 필요가 없다.
                //DateTime cTime = (DateTime)hDao.selectCurrentTimeDao(oleDBManager);

                // CurrentValue의 timestamp 의 하루전 날짜의 23시 59분 시간을 세팅한다.


                DateTime cTime = DateTime.Today;

                cTime = cTime.AddMinutes(-1);
                param["<="] = cTime.Year.ToString()
                        + "-" + cTime.Month.ToString()
                        + "-" + cTime.Day.ToString()
                        + " " + cTime.Hour.ToString()
                        + ":" + cTime.Minute.ToString();

                cTime = cTime.AddMinutes(-1);
                param[">"] = cTime.Year.ToString()
                        + "-" + cTime.Month.ToString()
                        + "-" + cTime.Day.ToString()
                        + " " + cTime.Hour.ToString()
                        + ":" + cTime.Minute.ToString();

                //Console.WriteLine((param["<="]).ToString() + " ### " + (param[">"]).ToString());
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                ConnectionClose();
            }

            return param;
        }

        // Historian 에서 최종 시간을 조회 후 시간을 반환한다.
        public DateTime selectCurrentTime()
        {
            DateTime cTime = DateTime.Now;
            try
            {
                ConnectionOpen();

                cTime = (DateTime)hDao.selectCurrentTimeDao(oleDBManager);

            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                ConnectionClose();
            }

            return cTime;
        }
    }
}
