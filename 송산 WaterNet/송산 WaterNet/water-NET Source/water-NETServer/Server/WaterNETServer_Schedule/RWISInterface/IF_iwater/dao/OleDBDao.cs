﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using WaterNETServer.Common.DB.HistorianOLE;
using System.Diagnostics;
using System.Data.OleDb;
using EMFrame.log;

namespace WaterNETServer.IF_iwater.dao
{
    public class OleDBDao
    {
        public OleDBDao(){}

        /// <summary>
        /// 실시간 태그 목록에 따른 실시간 데이터를 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="tableName"></param>
        /// <param name="tagKey"></param>
        /// <param name="timeStamp"></param>
        public void selectRawData_Dao(OleDBManager manager, DataSet dataset, string tableName, string tagKey, Hashtable timeStamp)
        {
            StringBuilder query = new StringBuilder();
            try
            {
                query.Append(" SELECT TOP 1 * FROM IHRAWDATA WHERE TAGNAME = '").Append(tagKey).AppendLine("'");
                query.Append(" AND TIMESTAMP <= '").Append(timeStamp["<="].ToString()).AppendLine("'");
                query.Append(" AND TIMESTAMP >  '").Append(timeStamp[">"].ToString()).AppendLine("'");
                query.AppendLine(" ORDER BY TIMESTAMP DESC");

                manager.FillDataSetScript(dataset, tableName, query.ToString(), null);
            }
            catch (System.Data.OleDb.OleDbException oe)
            {
                if ("API Error: API TIMEOUT".Equals(oe.Message))
                {
                    Logger.Error("Historian 접속이 원활하지 않습니다");
                }
                else
                {
                    Logger.Error(oe.ToString());
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
            finally
            {
                query.Remove(0, query.Length);
            }
        }

        /// <summary>
        /// Historian 에서 최종 시간을 조회
        /// </summary>
        public Object selectCurrentTimeDao(OleDBManager manager)
        {
            Object returnValue = new Object();
            StringBuilder query = new StringBuilder();
            try
            {
                query.AppendLine(" SELECT distinct max(timestamp) FROM IHRAWDATA WHERE samplingmode = 'CurrentValue' and tagname like '%F_CV'");

                returnValue = manager.ExecuteScriptScalar(query.ToString(), null);
            }
            catch (Exception e)
            {
                Logger.Error(query.ToString());
                Logger.Error(e.ToString());
            }
            finally
            {
                query.Remove(0, query.Length);
            }
            return returnValue;
        }

    }
}
