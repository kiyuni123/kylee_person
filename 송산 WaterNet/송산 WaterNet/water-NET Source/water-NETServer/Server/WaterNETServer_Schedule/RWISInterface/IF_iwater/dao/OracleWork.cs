﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using WaterNETServer.Common.DB.Oracle;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;

namespace WaterNETServer.IF_iwater.dao
{
    public class OracleWork : BaseWork
    {
        private OracleDao wDao = null;
        public OracleWork()
        {
            wDao = new OracleDao();
        }

        /// <summary>
        /// 태그구분별 카운트를 가져온다.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public int selectTagCount(string taggbn)
        {
            EMapper manager = null;
            int result = 0;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = wDao.selectTagCount_Dao(manager, taggbn);
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 태그구분별 카운트를 가져온다.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public int selectTagCount()
        {
            EMapper manager = null;
            int result = 0;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = wDao.selectTagCount_Dao(manager);
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 실시간 수집 태그 전체 목록을 가져온다.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public DataSet selectRealtimeTags(string tableName)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.selectRealtimeTags_Dao(manager, result, tableName);
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 1시간 적산데이터 수집
        /// </summary>
        public void insertAccumulation_Hour(string cTime)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.insertAccumulation_Hour_Dao(manager, cTime);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 1시간 적산데이터 삭제
        /// </summary>
        /// <param name="cTime"></param>
        public void deleteAccumulation_Hour(string cTime)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.deleteAccumulation_Hour_Dao(manager, cTime);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 1일 적산데이터 수집
        /// </summary>
        public void insertAccumulation_Day(string cTime)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.insertAccumulation_Day_Dao(manager, cTime);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 1일 적산데이터 보정
        /// </summary>
        public void insertModification_Day(string cTime, string cTime2)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.insertModification_Day_Dao(manager, cTime, cTime2);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 금일 적산 (계산)
        /// </summary>
        public void InsertAccumulation_ToDay(string cTime)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.InsertAccumulation_ToDay_Dao(manager, cTime);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 금일 적산 삭제 (10분전 데이터 삭제)
        /// </summary>
        /// <param name="cTime"></param>
        public void DeleteAccumulation_Today(string cTime)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.DeleteAccumulation_ToDay_Dao(manager, cTime);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// ihRawData 에서 가져온 데이터를 IF_GATHER_REALTIME 테이블에 넣는다. 
        /// </summary>
        /// <param name="dsName"></param>
        /// <param name="tableName"></param>
        public int insertRawData(DataSet dsName1, DataSet dsName, string tableName)
        {
            int returnCnt = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnCnt = wDao.insertRawData_Dao(manager, dsName1, dsName, tableName);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return returnCnt;
        }

        /// <summary>
        /// 실시간 데이터 삭제
        /// </summary>
        /// <param name="time"></param>
        public void DeleteRealtime(string time)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.DeleteRealtime_Dao(manager, time);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 실시간 데이터 인덱스 리빌드
        /// </summary>
        public void IndexRebuildRealtime()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.IndexRebuildRealtime_Dao(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 1시간 적산 데이터 인덱스 리빌드
        /// </summary>
        public void IndexRebuildAccumulationHour()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.IndexRebuildAccumulationHour_Dao(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 수집대상이지만 현재시간에 수집되지 않은 것들 기록
        /// </summary>
        /// <param name="result1">실시간 태그목록</param>
        /// <param name="result2">수집해온 실시간 태그값</param>
        /// <param name="cTime">수집 시간</param>
        [Obsolete("Not used anymore", true)]
        public int MissingRawData(DataSet result1, DataSet result2, Hashtable cTime)
        {
            int returnValue = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnValue = wDao.MissingRawData_Dao(manager, result1, result2, cTime);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// 이전 수집 데이터 있는지 확인값을 if_missing_realtime에 넣는다
        /// </summary>
        /// <param name="result3">최종시간 -1분의 데이터</param>
        /// <param name="cTime">현재 수집시간 -1</param>
        public int MissingRawData(DataSet result3)
        {
            int returnValue = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnValue = wDao.MissingRawData_Dao(manager, result3);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// 미수집된 목록을 가져온다.
        /// </summary>
        /// <returns></returns>
        public DataSet selectMissingRawData(string tableName, string code, int cof)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.selectMissingRawData_Dao(manager, result, tableName, code, cof);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 미수집 목록에서 수집한 목록은 수집체크를 한다.
        /// </summary>
        /// <param name="tagname"></param>
        /// <param name="timestr"></param>
        public int UpdateMissingData(string tagname, string timestr)
        {
            int updateCnt = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                updateCnt = wDao.UpdateMissingData_Dao(manager, tagname, timestr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return updateCnt;
        }

        /// <summary>
        /// 미수집 목록에서 수집한 목록에 대한 경고메시지 업데이트를 한다.
        /// </summary>
        public void UpdateWarning(DateTime cTime, int cnt)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.UpdateWarning_Dao(manager, cTime, cnt);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 조회한 최종시간 데이터가 이미 수집 됐는지 확인
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool IsGatherRealtimeData(string cTime)
        {
            bool returnValue = false;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                int cnt = wDao.IsGatherRealtimeData_Dao(manager, cTime);

                if (cnt > 0)
                {
                    returnValue = true;
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// 누락 데이터 확인
        /// </summary>
        /// <param name="hTableName3"></param>
        /// <param name="cTime"></param>
        /// <returns></returns>
        public DataSet selectPastRawData(string hTableName3)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.selectPastRawData_Dao(manager, result, hTableName3);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 자동보정
        /// </summary>
        public void checkAutoModify(string cTime)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.checkAutoModify_dao(manager, cTime);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 자동보정 시간반환
        /// </summary>
        /// <returns></returns>
        public string getTimeAutoModify()
        {
            string returnValue = string.Empty;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnValue = wDao.getTimeAutoModify(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }
    }
}