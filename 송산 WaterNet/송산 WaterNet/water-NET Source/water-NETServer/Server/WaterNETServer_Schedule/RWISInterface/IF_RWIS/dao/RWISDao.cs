﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMFrame.dm;
using System.Collections;
using System.Data;
using Oracle.DataAccess.Client;
using WaterNETServer.Common;

namespace WaterNETServer.IF_RWIS.dao
{
    public class RWISDao
    {
        private static RWISDao dao = null;
        private RWISDao() { }

        public static RWISDao GetInstance()
        {
            if (dao == null)
            {
                dao = new RWISDao();
            }
            return dao;
        }

        /// <summary>
        /// 실시간(1분) 데이터 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestmap"></param>
        /// <returns></returns>
        public object SelectGatherRealtimeData(EMapper manager, string tagname, DateTime timestmap)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT VAL");
            //query.AppendLine("  FROM OPN_RDR01MI_VW");
            query.AppendLine("  FROM " + AppStatic.REALTIME_TABLE);
            query.AppendLine(" WHERE TAGSN = :TAGNAME");
            query.AppendLine("   AND LOG_TIME = :TIMESTAMP");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = tagname;
            parameters[1].Value = timestmap.ToString("yyyyMMddHHmm");

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        /// <summary>
        /// 실시간(1분) 데이터 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestmap"></param>
        /// <returns></returns>
        public DataTable SelectGatherRealtimeData(EMapper manager, IList<string> tagList, DateTime timestmap)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TAGSN");
            query.AppendLine("      ,VAL");
            //query.AppendLine("  FROM OPN_RDR01MI_VW");
            query.AppendLine("  FROM " + AppStatic.REALTIME_TABLE);
            query.AppendLine(" WHERE LOG_TIME = :TIMESTAMP");

            if (tagList != null && tagList.Count != 0)
            {
                query.Append("       AND TAGSN IN (");

                foreach (string tagname in tagList)
                {
                    if (tagname.Equals(tagList[0]))
                    {
                        query.Append("'").Append(tagname).Append("'");
                    }
                    else
                    {
                        query.Append(",'").Append(tagname).Append("'");
                    }
                }

                query.AppendLine(")");
            }

            IDataParameter[] parameters =  {
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = timestmap.ToString("yyyyMMddHHmm");

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        /// <summary>
        /// 1시간적산차 데이터 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestmap"></param>
        /// <returns></returns>
        public object SelectAccumulationHourData(EMapper manager, string tagname, DateTime timestmap)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT  VAL");
            //query.AppendLine("  FROM OPN_RDF01HH_VW");
            query.AppendLine(" FROM " + AppStatic.ACCUMULATION_HOUR_TABLE);
            query.AppendLine(" WHERE TAGSN = :TAGNAME");
            query.AppendLine(" AND LOG_TIME = :TIMESTAMP");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = tagname;

            string strTimestamp = "";
            if (timestmap.Hour == 0)
            {
                strTimestamp = timestmap.AddDays(-1).ToString("yyyyMMdd") + "24";
            }
            else
            {
                strTimestamp = timestmap.ToString("yyyyMMddHH");
            }

            parameters[1].Value = strTimestamp;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        /// <summary>
        /// 1시간적산차 데이터 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestmap"></param>
        /// <returns></returns>
        public DataTable SelectAccumulationHourData(EMapper manager, IList<string> tagList, DateTime timestmap)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT  TAGSN");
            query.AppendLine("      , VAL");
            //query.AppendLine("  FROM OPN_RDF01HH_VW");
            query.AppendLine(" FROM " + AppStatic.ACCUMULATION_HOUR_TABLE);
            query.AppendLine(" WHERE LOG_TIME = :TIMESTAMP");

            if (tagList != null && tagList.Count != 0)
            {
                query.Append(" AND TAGSN IN (");

                foreach (string tagname in tagList)
                {

                    if (tagname.Equals(tagList[0]))
                    {
                        query.Append("'").Append(tagname).Append("'");
                    }
                    else
                    {
                        query.Append(",'").Append(tagname).Append("'");
                    }


                    //if (tagname.Equals(tagList[0]))
                    //{
                    //    query.Append("'").Append(Convert.ToInt32(tagname)).Append("'");
                    //}
                    //else
                    //{
                    //    query.Append(",'").Append(Convert.ToInt32(tagname)).Append("'");
                    //}
                }

                query.AppendLine(")");
            }

            IDataParameter[] parameters =  {
                        new OracleParameter("TIMESTAMP", OracleDbType.Char)
                    };

            string strTimestamp = "";
            if (timestmap.Hour == 0)
            {
                strTimestamp = timestmap.AddDays(-1).ToString("yyyyMMdd") + "24";
            }
            else
            {
                strTimestamp = timestmap.ToString("yyyyMMddHH");
            }

            parameters[0].Value = strTimestamp;

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        /// <summary>
        /// 보정로그 데이터 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestmap"></param>
        /// <returns></returns>
        public DataTable SelectAccumulationHourLogData(EMapper manager, string tagname)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT                                        ");
            query.AppendLine("       DECODE(SUBSTR(LOG_TIME,9,2),'24'       ");
            query.AppendLine("       , TO_CHAR(TO_DATE(SUBSTR(LOG_TIME,1,8) ");
            query.AppendLine("			,'yyyyMMdd')+1,'yyyyMMddHH24')      ");
            query.AppendLine("       , LOG_TIME) LOG_TIME                   ");
            query.AppendLine("       ,TAGSN                                 ");
            query.AppendLine("       ,OLD_VAL                               ");
            query.AppendLine("       ,CUR_VAL                               ");
            query.AppendLine("  FROM RDF01HHMOD_TB                          ");
            query.AppendLine(" WHERE TAGSN = :TAGNAME                       ");
            query.AppendLine("   AND USE_YN = 'N'                           ");
            query.AppendLine(" ORDER BY LOG_TIME                            ");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2)                        
                    };

            parameters[0].Value = tagname;

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }


        /// <summary>
        /// 보정로그 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestmap"></param>
        /// <returns></returns>
        public DataTable SelectAccumulationHourLogData(EMapper manager, IList<string> tagList)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT                                        ");
            query.AppendLine("       DECODE(SUBSTR(LOG_TIME,9,2),'24'       ");
            query.AppendLine("       , TO_CHAR(TO_DATE(SUBSTR(LOG_TIME,1,8) ");
            query.AppendLine("			,'yyyyMMdd')+1,'yyyyMMddHH24')      ");
            query.AppendLine("       , LOG_TIME) LOG_TIME                   ");
            query.AppendLine("      ,TAGSN                                  ");
            query.AppendLine("      ,CUR_VAL                                ");
            query.AppendLine("      ,USE_YN                                 ");
            query.AppendLine("  FROM  RDF01HHMOD_TB                         ");
            query.AppendLine(" WHERE   USE_YN = 'N'                         ");

            if (tagList != null && tagList.Count != 0)
            {
                query.Append("   AND TAGSN IN (");

                foreach (string tagname in tagList)
                {
                    if (tagname.Equals(tagList[0]))
                    {
                        query.Append("'").Append(Convert.ToInt32(tagname)).Append("'");
                    }
                    else
                    {
                        query.Append(",'").Append(Convert.ToInt32(tagname)).Append("'");
                    }
                }

                query.AppendLine(")");
            }

            return manager.ExecuteScriptDataTable(query.ToString(), null);
        }

        /// <summary>
        /// 보정로그 수정 데이터 사용후 'Y'로 변경
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestmap"></param>
        /// <returns></returns>
        public void UpdateLogData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("UPDATE RDF01HHMOD_TB          ");
            query.AppendLine("   SET USE_YN = 'Y'           ");
            query.AppendLine("   WHERE TAGSN = :TAGNAME     ");
            query.AppendLine("    AND  LOG_TIME = :TIMESTAMP");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2),
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
