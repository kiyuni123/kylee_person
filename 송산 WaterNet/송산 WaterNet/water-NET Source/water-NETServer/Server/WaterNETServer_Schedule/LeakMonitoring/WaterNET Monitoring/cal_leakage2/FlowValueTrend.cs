﻿using System;
using System.Data;

using WaterNETServer.LeakMonitoring.work;
using WaterNETServer.LeakMonitoring.utils;
using WaterNETServer.LeakMonitoring.interface1;

namespace WaterNETServer.LeakMonitoring.cal_leakage2
{
    public class FlowValueTrend : ICal
    {
        private string type = "1230";
        private int count = 0;
        private string result = "F";
        private int count_max = 3;

        //전일기준 감시
        public FlowValueTrend(string loc_code, DateTime dateTime, DataTable value)
        {
            //이전일의 누수감시 카운트를 가져온다.
            count = Convert.ToInt32(MoniteringWork.GetInstance().SelectLeakageCount(loc_code, dateTime.AddDays(-1).ToString("yyyyMMdd"), type));
            this.Cal(dateTime, value);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        //누수결과
        //F : 누수아님(False)
        //T : 누수임 (True)
        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        //계산식 적용
        private void Cal(DateTime dateTime, DataTable value)
        {
            if (value == null || value.Rows.Count == 0)
            {
                return;
            }
            double todayValue = 0;
            double week3AverageValue = 0;
            double minusweek01Value = 0;

            int minusweek01Count = 0;
            int minusweek03Count = 0;

            foreach (DataRow row in value.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayValue = Utils.ToDouble(row["VALUE"]);
                }

                if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek01Count++;
                    minusweek03Count++;
                    minusweek01Value += Utils.ToDouble(row["VALUE"]);
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek01Count++;
                    minusweek03Count++;
                    minusweek01Value += Utils.ToDouble(row["VALUE"]);
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek01Count++;
                    minusweek03Count++;
                    minusweek01Value += Utils.ToDouble(row["VALUE"]);
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek01Count++;
                    minusweek03Count++;
                    minusweek01Value += Utils.ToDouble(row["VALUE"]);
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek01Count++;
                    minusweek03Count++;
                    minusweek01Value += Utils.ToDouble(row["VALUE"]);
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek01Count++;
                    minusweek03Count++;
                    minusweek01Value += Utils.ToDouble(row["VALUE"]);
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek01Count++;
                    minusweek03Count++;
                    minusweek01Value += Utils.ToDouble(row["VALUE"]);
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-15).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-16).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-17).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-18).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-19).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-20).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-21).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd") && Utils.ToDouble(row["VALUE"]) > 0)
                {
                    minusweek03Count++;
                    week3AverageValue += Utils.ToDouble(row["VALUE"]);
                }
            }

            //평균계산
            if(minusweek01Count != 0)
                minusweek01Value = minusweek01Value / minusweek01Count;
            if(minusweek03Count != 0)
                week3AverageValue = week3AverageValue / minusweek03Count;

            //조건을 충족못하면 카운트를 초기화하고 로직을 중단한다.
            if (todayValue <= 0 || week3AverageValue <= 0 || minusweek01Value <= 0)
            {                
                this.result = "N";
                return;
            }

            if (minusweek01Value <= 20)
            {
                if ((minusweek01Value + ((minusweek01Value / 100) * 20)) >= todayValue)
                {
                    this.count = 0;
                    return;
                }
            }
            else
            {
                if ((minusweek01Value + ((minusweek01Value / 100) * 10)) >= todayValue)
                {
                    this.count = 0;
                    return;
                }
            }

            ////첫번쨰는 일주일 평균의 +20%으로 비교하고 그외는 일주일 평균으로 비교한다.
            //if (this.count == 0)
            //{
            //    if (minusweek01Value <= 20)
            //    {
            //        if ((minusweek01Value + ((minusweek01Value / 100) * 20)) >= todayValue)
            //        {
            //            this.count = 0;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        if ((minusweek01Value + ((minusweek01Value / 100) * 10)) >= todayValue)
            //        {
            //            this.count = 0;
            //            return;
            //        }
            //    }
            //}
            //else
            //{
            //    //금일 야간최소유량이 1주일 야간최소유량의 평균 초과못하면 카운트를 0으로 초기화하고 로직을 중단한다.
            //    if (minusweek01Value >= todayValue)
            //    {
            //        this.count = 0;
            //        return;
            //    }
            //}

            //조건을 충족못하면 카운트를 초기화하고 로직을 중단한다.
            if (todayValue <= week3AverageValue)
            {
                this.count = 0;
                return;
            }
            
            //카운트를 1단계 상승시킨다.
            this.count++;

            //카운트가 3이상 이면 누수로 판단한다.
            if (this.count_max <= this.count)
            {
                this.result = "T";
            }
        }
    }
}
