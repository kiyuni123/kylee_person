﻿
namespace WaterNETServer.LeakMonitoring.interface1
{
    public interface ICal
    {
        string MonitorType { get; }
        string MonitorResult { get; set; }
        int MonitorCount { get; }
        int MonitorCountMax { get; }
    }
}
