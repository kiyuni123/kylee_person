﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace WaterNETServer.WaterNETNetwork
{
    static class Program
    {
        private static LicenseInitializer m_AOLicenseInitializer = new LicenseInitializer();
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool createdNew;
            Mutex mutex = new System.Threading.Mutex(true, "frmWUMain", out createdNew);
            if (createdNew)
            {
                if (!m_AOLicenseInitializer.InitializeApplication(new ESRI.ArcGIS.esriSystem.esriLicenseProductCode[] { ESRI.ArcGIS.esriSystem.esriLicenseProductCode.esriLicenseProductCodeEngine },
                                                                  new ESRI.ArcGIS.esriSystem.esriLicenseExtensionCode[] { }))
                {
                    MessageBox.Show("ArcEngine Runtime License가 없습니다.", "오류",MessageBoxButtons.OK, MessageBoxIcon.Error);
                    m_AOLicenseInitializer.ShutdownApplication();
                    Application.Exit();
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmWUMain());
                mutex.ReleaseMutex();

                m_AOLicenseInitializer.ShutdownApplication();
            }
        }
    }
}
