﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;
using WaterNETServer.BatchJobs.dao;

namespace WaterNETServer.BatchJobs.dao
{
    public class BatchJobsWork : BaseWork
    {
        private BatchJobsDao wDao = null;
        public BatchJobsWork()
        {
            wDao = new BatchJobsDao();
        }

        /// <summary>
        /// 타임 스템프를 만든다. (1분 단위)
        /// </summary>
        public void TimestampMinute(DateTime from, DateTime to)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                List<string> listTimestamp = new List<string>();
                Hashtable condition = new Hashtable();

                DateTime _from = from;
                DateTime _to = to;

                while (_from <= _to)
                {
                    listTimestamp.Add(String.Format("{0:yyyyMMddHHmm}", _from));
                    _from = _from.AddMinutes(1);
                }
                condition.Add("Timestamp_yyyyMMddHHmm", listTimestamp);

                wDao.TimestampMinuteDelete_dao(manager, from.ToString("yyyyMMddHHmm"), to.ToString("yyyyMMddHHmm"));
                wDao.TimestampMinute_dao(manager, condition);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 타임 스템프를 만든다. (1시간 단위)
        /// </summary>
        public void TimestampHour(DateTime from, DateTime to)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                List<string> listTimestamp = new List<string>();
                Hashtable condition = new Hashtable();

                DateTime _from = from;
                DateTime _to = to;

                while (_from <= _to)
                {
                    listTimestamp.Add(String.Format("{0:yyyyMMddHH}", _from));
                    _from = _from.AddHours(1);
                }
                condition.Add("Timestamp_yyyyMMddHH", listTimestamp);

                wDao.TimestampHourDelete_dao(manager, from.ToString("yyyyMMddHH"), to.ToString("yyyyMMddHH"));
                wDao.TimestampHour_dao(manager, condition);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 일별 야간 최소 유량
        /// </summary>
        public void MinimumNightFlow()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.MinimumNightFlow_Dao(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 야간 최소유량 (지정일) 보정
        /// </summary>
        /// <param name="cTime2">보정 시작 날짜</param>
        /// <param name="cTime3">보정 끝 날짜</param>
        public void MinimumNightFlow_modify(string cTime, string cTime2)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.MinimumNightFlow_modify_Dao(manager, cTime, cTime2);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 1시간 수압(정시의 수압)
        /// </summary>
        public int AveragePressure()
        {
            EMapper manager = null;
            int result = 0;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = wDao.AveragePressure_Dao(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 1시간 수압(정시의 수압) (지정일) 보정
        /// </summary>
        /// <param name="cTime2">보정 시작 날짜</param>
        /// <param name="cTime3">보정 끝 날짜</param>
        public void AveragePressure_Modify(string pri_ctime2, string pri_ctime3)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.AveragePressure_Modify_Dao(manager, pri_ctime2, pri_ctime3);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }



/////////////////// 수량관련
        
        /// <summary>
        /// 전체 블록 갯수 반환
        /// 수집 대상 건수 활용
        /// </summary>
        /// <returns></returns>
        public int WV_BlockCount()
        {
            EMapper manager = null;
            int result = 0;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = wDao.WV_BlockCount(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 블록 수용가 일별 통계 수정 / 등록 : 가정가구수, 비가정가구수, 급수전수, 계량기교체건수
        /// 블록 수용가통계 수정 / 등록 : 전수, 개전수, 폐전수, 중지/정지전수
        /// </summary>
        public void WV_BlockDMBatch_Day()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                string DATEE = DateTime.Now.ToString("yyyyMMdd");

                wDao.WV_SmallBlockDMBatch_Day(manager, DATEE);
                wDao.WV_MiddleBlockDMBatch_Day(manager, DATEE);
                wDao.WV_LargeBlockDMBatch_Day(manager, DATEE);

                wDao.WV_SmallBlockDMBatch_Total(manager);
                wDao.WV_MiddleBlockDMBatch_Total(manager);
                wDao.WV_LargeBlockDMBatch_Total(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 블록 수용가 일별 통계 수정 / 등록 : 가정가구수, 비가정가구수, 급수전수, 계량기교체건수
        /// 블록 수용가통계 수정 / 등록 : 전수, 개전수, 폐전수, 중지/정지전수
        /// </summary>
        public int WV_BlockDMBatch_Day(DateTime TARETDATE)
        {
            EMapper manager = null;
            int result = 0;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                string DATEE = TARETDATE.ToString("yyyyMMdd");

                result += wDao.WV_SmallBlockDMBatch_Day(manager, DATEE);
                result += wDao.WV_MiddleBlockDMBatch_Day(manager, DATEE);
                result += wDao.WV_LargeBlockDMBatch_Day(manager, DATEE);

                wDao.WV_SmallBlockDMBatch_Total(manager);
                wDao.WV_MiddleBlockDMBatch_Total(manager);
                wDao.WV_LargeBlockDMBatch_Total(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 블록 수용가 일별 통계 수정 / 등록 : 가정가구수, 비가정가구수, 급수전수, 계량기교체건수
        /// 블록 수용가통계 수정 / 등록 : 전수, 개전수, 폐전수, 중지/정지전수
        /// </summary>
        /// <param name="STARTDATE"></param>
        /// <param name="ENDDATE"></param>
        public void WV_BlockDMBatch_Day(DateTime STARTDATE, DateTime ENDDATE)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                for (DateTime i = STARTDATE; i <= ENDDATE; i = i.AddDays(1))
                {
                    wDao.WV_SmallBlockDMBatch_Day(manager, i.ToString("yyyyMMdd"));
                    wDao.WV_MiddleBlockDMBatch_Day(manager, i.ToString("yyyyMMdd"));
                    wDao.WV_LargeBlockDMBatch_Day(manager, i.ToString("yyyyMMdd"));
                }

                wDao.WV_SmallBlockDMBatch_Total(manager);
                wDao.WV_MiddleBlockDMBatch_Total(manager);
                wDao.WV_LargeBlockDMBatch_Total(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 블록 수용가 월별 통계 수정 / 등록 : 가정사용량, 비가정사용량, 사용량
        /// </summary>
        public void WV_BlockDMBatch_Month()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                string year_mon = string.Empty;

                //대블록 목록
                DataTable dataTable = wDao.SelectLBlock(manager).Tables[0];

                if (dataTable == null)
                {
                    return;
                }

                foreach (DataRow dataRow in dataTable.Rows)
                {
                    int rd = Convert.ToInt32(wDao.GetRevenueFix(manager, dataRow["LOC_CODE"].ToString()));

                    if (DateTime.Now.Day > rd)
                    {
                        year_mon = DateTime.Now.AddMonths(-1).ToString("yyyyMM");
                    }
                    else
                    {
                        year_mon = DateTime.Now.AddMonths(-2).ToString("yyyyMM");
                    }

                    wDao.WV_SmallBlockDMBatch_Month(manager, dataRow["LOC_CODE"].ToString(), year_mon);
                    wDao.WV_MiddleBlockDMBatch_Month(manager, year_mon);
                    wDao.WV_LargeBlockDMBatch_Month(manager, year_mon);
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 블록 수용가 월별 통계 수정 / 등록 : 가정사용량, 비가정사용량, 사용량
        /// </summary>
        public void WV_BlockDMBatch_Month(DateTime TARETDATE)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                string year_mon = string.Empty;

                //대블록 목록
                DataTable dataTable = wDao.SelectLBlock(manager).Tables[0];

                if (dataTable == null)
                {
                    return;
                }

                foreach (DataRow dataRow in dataTable.Rows)
                {
                    int rd = Convert.ToInt32(wDao.GetRevenueFix(manager, dataRow["LOC_CODE"].ToString()));

                    if (DateTime.Now.Day > rd)
                    {
                        year_mon = TARETDATE.AddMonths(-1).ToString("yyyyMM");
                    }
                    else
                    {
                        year_mon = TARETDATE.AddMonths(-2).ToString("yyyyMM");
                    }

                    wDao.WV_SmallBlockDMBatch_Month(manager, dataRow["LOC_CODE"].ToString(), year_mon);
                    wDao.WV_MiddleBlockDMBatch_Month(manager, year_mon);
                    wDao.WV_LargeBlockDMBatch_Month(manager, year_mon);
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 블록 수용가 월별 통계 수정 / 등록 : 가정사용량, 비가정사용량, 사용량
        /// </summary>
        /// <param name="STARTDATE"></param>
        /// <param name="ENDDATE"></param>
        public void WV_BlockDMBatch_Month(DateTime STARTDATE, DateTime ENDDATE)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                for (DateTime i = STARTDATE; i <= ENDDATE; i = i.AddMonths(1))
                {
                    //대블록 목록
                    DataTable dataTable = wDao.SelectLBlock(manager).Tables[0];

                    if (dataTable == null)
                    {
                        return;
                    }

                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        int rd = Convert.ToInt32(wDao.GetRevenueFix(manager, dataRow["LOC_CODE"].ToString()));

                        if (this.CanRevenueUpdate(i.ToString("yyyyMM").ToString(), "yyyyMM", rd))
                        {
                            wDao.WV_SmallBlockDMBatch_Month(manager, dataRow["LOC_CODE"].ToString(), i.ToString("yyyyMM"));
                            wDao.WV_MiddleBlockDMBatch_Month(manager, i.ToString("yyyyMM"));
                            wDao.WV_LargeBlockDMBatch_Month(manager, i.ToString("yyyyMM"));
                        }
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 블록 급수관로, 상수관로 사용구분별 연장 현황
        /// </summary>
        public void WV_BlockPipeBatch_Day()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                string DATEE = DateTime.Now.ToString("yyyyMMdd");

                wDao.WV_Pipe_Ls_Day(manager, DATEE, DATEE);
                wDao.WV_Pipe_Ls_Middle_Day(manager, DATEE);
                wDao.WV_Pipe_Ls_Large_Day(manager, DATEE);

                wDao.WV_Sply_Ls_Day(manager, DATEE, DATEE);
                wDao.WV_Sply_Ls_Middle_Day(manager, DATEE);
                wDao.WV_Sply_Ls_Large_Day(manager, DATEE);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 블록 급수관로, 상수관로 사용구분별 연장 현황
        /// </summary>
        /// <param name="STARTDATE"></param>
        /// <param name="ENDDATE"></param>
        public void WV_BlockPipeBatch_Day(DateTime STARTDATE, DateTime ENDDATE)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                for (DateTime i = STARTDATE; i <= ENDDATE; i = i.AddDays(1))
                {
                    wDao.WV_Pipe_Ls_Day(manager, i.ToString("yyyyMMdd"), i.ToString("yyyyMMdd"));
                    wDao.WV_Pipe_Ls_Middle_Day(manager, i.ToString("yyyyMMdd"));
                    wDao.WV_Pipe_Ls_Large_Day(manager, i.ToString("yyyyMMdd"));

                    wDao.WV_Sply_Ls_Day(manager, i.ToString("yyyyMMdd"), i.ToString("yyyyMMdd"));
                    wDao.WV_Sply_Ls_Middle_Day(manager, i.ToString("yyyyMMdd"));
                    wDao.WV_Sply_Ls_Large_Day(manager, i.ToString("yyyyMMdd"));
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 블록 유수율분석
        /// </summary>
        public void WV_BlockRevenueRatioBatch_Month()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                //대블록 목록
                DataTable dataTable = wDao.SelectLBlock(manager).Tables[0];

                if (dataTable == null)
                {
                    return;
                }

                foreach (DataRow dataRow in dataTable.Rows)
                {
                    int rd = Convert.ToInt32(wDao.GetRevenueFix(manager, dataRow["LOC_CODE"].ToString()));
                    int sd = Convert.ToInt32(wDao.GetSuppliedDay(manager, dataRow["LOC_CODE"].ToString()));

                    string year_mon = string.Empty;

                    if (DateTime.Now.Day > rd)
                    {
                        year_mon = DateTime.Now.AddMonths(-1).ToString("yyyyMM");
                    }
                    else
                    {
                        year_mon = DateTime.Now.AddMonths(-2).ToString("yyyyMM");
                    }

                    string startDate = this.GetSuppliedStartdate(year_mon, "yyyyMM", sd);
                    string endDate = this.GetSuppliedEnddate(year_mon, "yyyyMM", sd);

                    wDao.WV_Revenue_Ratio_Small(manager, year_mon, startDate, endDate);
                    wDao.WV_Revenue_Ratio_Middle(manager, year_mon, startDate, endDate);
                    wDao.WV_Revenue_Ratio_Large(manager, year_mon, startDate, endDate);
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 블록 유수율분석
        /// </summary>
        /// <param name="STARTDATE"></param>
        /// <param name="ENDDATE"></param>
        public void WV_BlockRevenueRatioBatch_Month(DateTime STARTDATE, DateTime ENDDATE)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                for (DateTime i = STARTDATE; i <= ENDDATE; i = i.AddMonths(1))
                {
                    //대블록 목록
                    DataTable dataTable = wDao.SelectLBlock(manager).Tables[0];

                    if (dataTable == null)
                    {
                        return;
                    }

                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        int rd = Convert.ToInt32(wDao.GetRevenueFix(manager, dataRow["LOC_CODE"].ToString()));
                        int sd = Convert.ToInt32(wDao.GetSuppliedDay(manager, dataRow["LOC_CODE"].ToString()));

                        if (this.CanRevenueUpdate(i.ToString("yyyyMM").ToString(), "yyyyMM", rd))
                        {
                            string year_mon = i.ToString("yyyyMM");
                            string startDate = this.GetSuppliedStartdate(year_mon, "yyyyMM", sd);
                            string endDate = this.GetSuppliedEnddate(year_mon, "yyyyMM", sd);

                            wDao.WV_Revenue_Ratio_Small(manager, year_mon, startDate, endDate);
                            wDao.WV_Revenue_Ratio_Middle(manager, year_mon, startDate, endDate);
                            wDao.WV_Revenue_Ratio_Large(manager, year_mon, startDate, endDate);
                        }
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 실시간수리해석결과
        /// </summary>
        public void WV_BlockWHHpresResultBatch_Day()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                string DATEE = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");

                wDao.WV_WHHpresResultBatch_Day(manager, DATEE, DATEE);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 실시간수리해석결과
        /// </summary>
        /// <param name="STARTDATE"></param>
        /// <param name="ENDDATE"></param>
        public void WV_BlockWHHpresResultBatch_Day(DateTime STARTDATE, DateTime ENDDATE)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                for (DateTime i = STARTDATE; i <= ENDDATE; i = i.AddDays(1))
                {
                    wDao.WV_WHHpresResultBatch_Day(manager, i.ToString("yyyyMMdd"), i.ToString("yyyyMMdd"));
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        private bool CanRevenueUpdate(string year_mon, string format, int st)
        {
            bool result = false;

            int st1 = 0;

            int lastDay = DateTime.ParseExact(year_mon + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day;
            DateTime Months = DateTime.ParseExact(year_mon, "yyyyMM", null);

            if (lastDay <= st)
            {
                st1 = lastDay;
            }
            else
            {
                st1 = st;
            }

            if (DateTime.Now > Months.AddMonths(1).AddDays(st1 - 1))
            {
                result = true;
            }

            return result;
        }

        //월기준 공급량 산정 시작일자,
        private string GetSuppliedStartdate(string year_mon, string format, int st)
        {
            int st1 = 0;

            int lastDay = DateTime.ParseExact(year_mon + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day;

            if (lastDay <= st)
            {
                st1 = lastDay;
            }
            else
            {
                st1 = st;
            }

            DateTime dt = DateTime.ParseExact(year_mon+"01", "yyyyMMdd", null);

            DateTime rt;

            if (DateTime.ParseExact(year_mon + st1.ToString("00"), "yyyyMMdd", null) <= dt)
            {
                rt = DateTime.ParseExact(year_mon + st1.ToString("00"), "yyyyMMdd", null);
            }
            else
            {
                rt = DateTime.ParseExact(year_mon + st1.ToString("00"), "yyyyMMdd", null).AddMonths(-1);

                if (rt.ToString("yyyyMM") != year_mon && (st >= DateTime.ParseExact(rt.ToString("yyyyMM") + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day))
                {
                    rt = DateTime.ParseExact(rt.ToString("yyyyMM") + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1);
                }

                if (rt.ToString("yyyyMM") != year_mon && (st1 < DateTime.ParseExact(rt.ToString("yyyyMM") + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day))
                {
                    rt = DateTime.ParseExact(rt.ToString("yyyyMM") + st.ToString("00"), "yyyyMMdd", null);
                }

            }

            return rt.ToString("yyyyMMdd");
        }

        //월기준 공급량 산정 종료일자,
        private string GetSuppliedEnddate(string year_mon, string format, int st)
        {
            int rt = st;
            st = st - 1;

            if (st == 0)
            {
                st = 31;
            }

            DateTime dt = DateTime.ParseExact(GetSuppliedStartdate(year_mon, format, rt), "yyyyMMdd", null).AddMonths(1).AddDays(-1);
            int lastDay = DateTime.ParseExact(dt.ToString("yyyyMM") + "01", "yyyyMMdd", null).AddMonths(1).AddDays(-1).Day;

            if (st == 31)
            {
                dt = DateTime.ParseExact(dt.ToString("yyyyMM") + lastDay.ToString("00"), "yyyyMMdd", null);
            }

            if (st <= lastDay)
            {
                dt = DateTime.ParseExact(dt.ToString("yyyyMM") + st.ToString("00"), "yyyyMMdd", null);
            }

            return dt.ToString("yyyyMMdd");
        }
    }
}
