﻿using System.Collections.Generic;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using System.Collections;
using EMFrame.dm;
using System;

namespace WaterNETServer.BatchJobs.dao
{
    public class BatchJobsDao
    {
        public BatchJobsDao() { }

        #region 타임스템프
        /// <summary>
        /// 타임스템프를 만든다. (1분 단위)
        /// </summary>
        /// <param name="manager"></param>
        internal void TimestampMinute_dao(EMapper manager, Hashtable condition)
        {
            // 타임스템프 테이블 인서트문
            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_TIMESTAMP_YYYYMMDDHH24MI VALUES(:1)");

            OracleCommand oracleCommand = new OracleCommand();
            oracleCommand.CommandText = query.ToString();
            oracleCommand.Connection = manager.Connection;
            oracleCommand.ArrayBindCount = ((List<string>)condition["Timestamp_yyyyMMddHHmm"]).Count;

            OracleParameter param = new OracleParameter("1", OracleDbType.Varchar2);
            param.Direction = ParameterDirection.Input;
            param.Value = ((List<string>)condition["Timestamp_yyyyMMddHHmm"]).ToArray();
            oracleCommand.Parameters.Add(param);

            oracleCommand.ExecuteNonQuery();
        }

        /// <summary>
        /// 타임프템프 지정된 기간 데이터 삭제
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="p"></param>
        /// <param name="p_3"></param>
        internal void TimestampMinuteDelete_dao(EMapper manager, string from, string to)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_TIMESTAMP_YYYYMMDDHH24MI");
            query.Append(" WHERE YYYYMMDDHH24MI >= '").Append(from).AppendLine("'");
            query.Append("   AND YYYYMMDDHH24MI <= '").Append(to  ).AppendLine("'");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 타임스템프를 만든다. (1시간 단위)
        /// </summary>
        /// <param name="manager"></param>
        internal void TimestampHour_dao(EMapper manager, Hashtable condition)
        {
            // 타임스템프 테이블 인서트문
            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_TIMESTAMP_YYYYMMDDHH24 VALUES(:1)");

            OracleCommand oracleCommand = new OracleCommand();
            oracleCommand.CommandText = query.ToString();
            oracleCommand.Connection = manager.Connection;
            oracleCommand.ArrayBindCount = ((List<string>)condition["Timestamp_yyyyMMddHH"]).Count;

            OracleParameter param = new OracleParameter("1", OracleDbType.Varchar2);
            param.Direction = ParameterDirection.Input;
            param.Value = ((List<string>)condition["Timestamp_yyyyMMddHH"]).ToArray();
            oracleCommand.Parameters.Add(param);

            oracleCommand.ExecuteNonQuery();
        }

        /// <summary>
        /// 타임프템프 지정된 기간 데이터 삭제
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="p"></param>
        /// <param name="p_3"></param>
        internal void TimestampHourDelete_dao(EMapper manager, string from, string to)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_TIMESTAMP_YYYYMMDDHH24");
            query.Append(" WHERE YYYYMMDDHH24 >= '").Append(from).AppendLine("'");
            query.Append("   AND YYYYMMDDHH24 <= '").Append(to).AppendLine("'");

            manager.ExecuteScript(query.ToString(), null);
        }

        #endregion 

        /// <summary>
        /// 일별 야간 최소 유량 (매일 오전(8시30분)에 00~08시까지의 야간 최소유량을 구함)
        /// </summary>
        internal void MinimumNightFlow_Dao(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_WV_MNF A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT /*+ rule */ A.TAGNAME");
            query.AppendLine("             , TO_DATE(TO_CHAR(A.TIMESTAMP, 'YYYYMMDD'), 'YYYYMMDD') TIMESTAMP");
            query.AppendLine("             , ROUND(AVG(B.VALUE), 2) MIN");
            query.AppendLine("             , ROUND(MIN(A.MAVG), 2) MVAVG_MIN");
            query.AppendLine("             , MIN(TO_CHAR(A.TIMESTAMP, 'YYYYMMDDHH24MI')) KEEP(DENSE_RANK FIRST ORDER BY A.MAVG ASC) MVAVG_MINTIME  -- 이동평균이 최소일때의 시간");
            query.AppendLine("         FROM ( ");
            query.AppendLine("               SELECT TAGNAME, TIMESTAMP, VALUE, MAVG");
            query.AppendLine("                 FROM (");
            query.AppendLine("                       SELECT A.TAGNAME");
            query.AppendLine("                            , A.TIMESTAMP");
            query.AppendLine("                            , TO_NUMBER(A.VALUE) VALUE");
            query.AppendLine("                            , AVG(TO_NUMBER(A.VALUE)) OVER (PARTITION BY A.TAGNAME ORDER BY A.TAGNAME, TIMESTAMP ROWS 60 - 1 PRECEDING) AS MAVG    -- 1시간 이동평균");
            query.AppendLine("                         FROM IF_GATHER_REALTIME A");
            query.AppendLine("                            , IF_TAG_GBN B");
            query.AppendLine("                        WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("                          AND A.TIMESTAMP BETWEEN TRUNC(SYSDATE, 'DD') AND TRUNC(SYSDATE + 1, 'DD') - 1/24/60 ");
            query.AppendLine("                          AND B.TAG_GBN = 'MNF'");
            query.AppendLine("                      )");
            query.AppendLine("                WHERE TIMESTAMP BETWEEN TRUNC(SYSDATE, 'DD') AND TRUNC(SYSDATE, 'DD') + 7/24");
            query.AppendLine("              ) A");
            query.AppendLine("            , (");
            query.AppendLine("                SELECT TAGNAME");
            query.AppendLine("                     , TIMESTAMP");
            query.AppendLine("                     , VALUE");
            query.AppendLine("                     , NU");
            query.AppendLine("                  FROM (");
            query.AppendLine("                        SELECT A.TAGNAME");
            query.AppendLine("                             , A.TIMESTAMP");
            query.AppendLine("                             , TO_NUMBER(A.VALUE) AS VALUE");
            query.AppendLine("                             , ROW_NUMBER() OVER ");
            query.AppendLine("                                   (PARTITION BY A.TAGNAME, TRUNC(A.TIMESTAMP, 'DD')");
            query.AppendLine("                                        ORDER BY A.TAGNAME, TRUNC(A.TIMESTAMP, 'DD'), TO_NUMBER(A.VALUE))");
            query.AppendLine("                               NU -- 순시값 순위 ");
            query.AppendLine("                          FROM IF_GATHER_REALTIME A");
            query.AppendLine("                             , IF_TAG_GBN B");
            query.AppendLine("                         WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("                           AND A.TIMESTAMP BETWEEN TRUNC(SYSDATE, 'DD') AND TRUNC(SYSDATE + 1, 'DD') - 1/24/60 ");
            query.AppendLine("                           AND TO_CHAR(A.TIMESTAMP, 'HH24MI') BETWEEN '0000'AND '0700'");
            query.AppendLine("                           AND B.TAG_GBN = 'MNF'");
            query.AppendLine("                       ) A");
            query.AppendLine("                 WHERE NU BETWEEN 3 AND 8");
            query.AppendLine("              ) B");
            query.AppendLine("         WHERE A.TAGNAME = B.TAGNAME (+)");
            query.AppendLine("           AND A.TIMESTAMP = B.TIMESTAMP (+)");
            query.AppendLine("         GROUP BY A.TAGNAME, TO_CHAR(A.TIMESTAMP, 'YYYYMMDD')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON (     A.TAGNAME   = B.TAGNAME");
            query.AppendLine("        AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("    UPDATE SET A.MIN           = B.MIN");
            query.AppendLine("             , A.MVAVG_MIN     = B.MVAVG_MIN");
            query.AppendLine("             , A.MVAVG_MINTIME = B.MVAVG_MINTIME");
            query.AppendLine("             , A.FILTERING     = DECODE(A.FILTERING, NULL, B.MVAVG_MIN, A.FILTERING)");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("    INSERT (A.TAGNAME, A.TIMESTAMP, A.MIN, A.MVAVG_MIN, A.MVAVG_MINTIME, A.FILTERING)");
            query.AppendLine("    VALUES (B.TAGNAME, B.TIMESTAMP, B.MIN, B.MVAVG_MIN, B.MVAVG_MINTIME, B.MVAVG_MIN)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 야간 최소유량 (지정일) 보정
        /// </summary>
        /// <param name="cTime2">보정 시작 날짜</param>
        /// <param name="cTime3">보정 끝 날짜</param>
        internal void MinimumNightFlow_modify_Dao(EMapper manager, string cTime, string cTime2)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_WV_MNF A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT /*+ rule */ A.TAGNAME");
            query.AppendLine("             , TO_DATE(TO_CHAR(A.TIMESTAMP, 'YYYYMMDD'), 'YYYYMMDD') TIMESTAMP");
            query.AppendLine("             , ROUND(AVG(B.VALUE), 2) MIN");
            query.AppendLine("             , ROUND(MIN(A.MAVG), 2) MVAVG_MIN");
            query.AppendLine("             , MIN(TO_CHAR(A.TIMESTAMP, 'YYYYMMDDHH24MI')) KEEP(DENSE_RANK FIRST ORDER BY A.MAVG ASC) MVAVG_MINTIME  -- 이동평균이 최소일때의 시간");
            query.AppendLine("         FROM ( ");
            query.AppendLine("               SELECT TAGNAME, TIMESTAMP, VALUE, MAVG");
            query.AppendLine("                 FROM (");
            query.AppendLine("                       SELECT A.TAGNAME");
            query.AppendLine("                            , A.TIMESTAMP");
            query.AppendLine("                            , TO_NUMBER(A.VALUE) VALUE");
            query.AppendLine("                            , AVG(TO_NUMBER(A.VALUE)) OVER (PARTITION BY A.TAGNAME ORDER BY A.TAGNAME, TIMESTAMP ROWS 60 - 1 PRECEDING) AS MAVG    -- 1시간 이동평균");
            query.AppendLine("                         FROM IF_GATHER_REALTIME A");
            query.AppendLine("                            , IF_TAG_GBN B");
            query.AppendLine("                        WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("                          AND A.TIMESTAMP BETWEEN TO_DATE('" + cTime + "', 'YYYYMMDD') - 59/24/60 AND ((TO_DATE('" + cTime2 + "', 'YYYYMMDD') + 1) - 1/24/60)  ");
            query.AppendLine("                          AND B.TAG_GBN = 'MNF'");
            query.AppendLine("                      )");
            query.AppendLine("                WHERE TIMESTAMP BETWEEN TRUNC(TO_DATE('" + cTime + "', 'YYYYMMDD'), 'DD') AND TRUNC(TO_DATE('" + cTime2 + "', 'YYYYMMDD'), 'DD') + 7/24");
            query.AppendLine("              ) A");
            query.AppendLine("            , (");
            query.AppendLine("                SELECT TAGNAME");
            query.AppendLine("                     , TIMESTAMP");
            query.AppendLine("                     , VALUE");
            query.AppendLine("                     , NU");
            query.AppendLine("                  FROM (");
            query.AppendLine("                        SELECT A.TAGNAME");
            query.AppendLine("                             , A.TIMESTAMP");
            query.AppendLine("                             , TO_NUMBER(A.VALUE) AS VALUE");
            query.AppendLine("                             , ROW_NUMBER() OVER ");
            query.AppendLine("                                   (PARTITION BY A.TAGNAME, TRUNC(A.TIMESTAMP, 'DD')");
            query.AppendLine("                                        ORDER BY A.TAGNAME, TRUNC(A.TIMESTAMP, 'DD'), TO_NUMBER(A.VALUE))");
            query.AppendLine("                               NU -- 순시값 순위 ");
            query.AppendLine("                          FROM IF_GATHER_REALTIME A");
            query.AppendLine("                             , IF_TAG_GBN B");
            query.AppendLine("                         WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("                           AND A.TIMESTAMP BETWEEN TO_DATE('" + cTime + "', 'YYYYMMDD') AND ((TO_DATE('" + cTime2 + "', 'YYYYMMDD') + 1) - 1/24/60)  ");
            query.AppendLine("                           AND TO_CHAR(A.TIMESTAMP, 'HH24MI') BETWEEN '0000'AND '0700'");
            query.AppendLine("                           AND B.TAG_GBN = 'MNF'");
            query.AppendLine("                       ) A");
            query.AppendLine("                 WHERE NU BETWEEN 3 AND 8");
            query.AppendLine("              ) B");
            query.AppendLine("         WHERE A.TAGNAME = B.TAGNAME (+)");
            query.AppendLine("           AND A.TIMESTAMP = B.TIMESTAMP (+)");
            query.AppendLine("         GROUP BY A.TAGNAME, TO_CHAR(A.TIMESTAMP, 'YYYYMMDD')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON (     A.TAGNAME   = B.TAGNAME");
            query.AppendLine("        AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("    UPDATE SET A.MIN           = B.MIN");
            query.AppendLine("             , A.MVAVG_MIN     = B.MVAVG_MIN");
            query.AppendLine("             , A.MVAVG_MINTIME = B.MVAVG_MINTIME");
            query.AppendLine("             , A.FILTERING     = B.MVAVG_MIN");
            //query.AppendLine("             , A.FILTERING     = DECODE(A.FILTERING, NULL, B.MVAVG_MIN, A.FILTERING)");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("    INSERT (A.TAGNAME, A.TIMESTAMP, A.MIN, A.MVAVG_MIN, A.MVAVG_MINTIME, A.FILTERING)");
            query.AppendLine("    VALUES (B.TAGNAME, B.TIMESTAMP, B.MIN, B.MVAVG_MIN, B.MVAVG_MINTIME, B.MVAVG_MIN)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 1시간 수압(정시의 수압)
        /// </summary>
        internal int AveragePressure_Dao(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_WV_PRAVG");
            query.AppendLine("SELECT A.TAGNAME, A.TIMESTAMP, ROUND(TO_NUMBER(A.VALUE), 2)");
            query.AppendLine("  FROM IF_GATHER_REALTIME A");
            query.AppendLine("     , IF_TAG_GBN B");
            query.AppendLine(" WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("   AND A.TIMESTAMP = TRUNC(SYSDATE, 'HH')");
            query.AppendLine("   AND B.TAG_GBN = 'PRI'");

            return manager.ExecuteScript(query.ToString(), null);
        }


        /// <summary>
        /// 1시간 수압(정시의 수압) (지정일) 보정
        /// </summary>
        /// <param name="cTime2">보정 시작 날짜</param>
        /// <param name="cTime3">보정 끝 날짜</param>
        internal void AveragePressure_Modify_Dao(EMapper manager, string cTime, string cTime2)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_WV_PRAVG A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT A.TAGNAME, A.TIMESTAMP, ROUND(TO_NUMBER(A.VALUE), 2) TAVG");
            query.AppendLine("          FROM IF_GATHER_REALTIME A");
            query.AppendLine("             , IF_TAG_GBN B");
            query.AppendLine("             , IF_TIMESTAMP_YYYYMMDDHH24 C");
            query.AppendLine("         WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("           AND A.TIMESTAMP BETWEEN TO_DATE('" + cTime + "', 'YYYYMMDD') AND ((TO_DATE('" + cTime2 + "', 'YYYYMMDD') + 1) - 1/24/60)");
            query.AppendLine("           AND A.TIMESTAMP = TO_DATE(C.YYYYMMDDHH24, 'YYYYMMDDHH24')");
            query.AppendLine("           AND B.TAG_GBN = 'PRI'");
            query.AppendLine("      ) B");
            query.AppendLine("   ON (     A.TAGNAME   = B.TAGNAME");
            query.AppendLine("        AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("    UPDATE SET A.TAVG = B.TAVG");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("    INSERT (A.TAGNAME, A.TIMESTAMP, A.TAVG)");
            query.AppendLine("    VALUES (B.TAGNAME, B.TIMESTAMP, B.TAVG)");

            manager.ExecuteScript(query.ToString(), null);
        }



        /////////////////// 수량관련

        internal int WV_BlockCount(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT COUNT(*)");
            query.AppendLine("  FROM cm_location");
            query.AppendLine(" WHERE ftr_code in ('BZ001','BZ002','BZ003')");

            return Convert.ToInt32(manager.ExecuteScriptScalar(query.ToString(), null));
        }

        /// <summary>
        /// 소블록 수용가통계 수정 / 등록 : 전수, 개전수, 폐전수, 중지/정지전수
        /// </summary>
        internal int WV_SmallBlockDMBatch_Total(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_consumer a                                             ");
            query.AppendLine("using (															   ");
            query.AppendLine("select loc.loc_code												   ");
            query.AppendLine("      ,decode(count(*),1, 0, count(*)) junsu						   ");
            query.AppendLine("      ,sum(decode(hydrntstat, '개전', 1, 0)) gejunsu				   ");
            query.AppendLine("      ,sum(decode(hydrntstat, '폐전', 1, 0)) pejunsu				   ");
            query.AppendLine("      ,sum(decode(hydrntstat, '중지', 1, 0)) 						   ");
            query.AppendLine("      +sum(decode(hydrntstat, '정지', 1, 0)) stopjunsu			   ");
            query.AppendLine("  from															   ");
            query.AppendLine("      (															   ");
            query.AppendLine("      select dmi.lftridn											   ");
            query.AppendLine("            ,dmi.mftridn											   ");
            query.AppendLine("            ,dmi.sftridn											   ");
            query.AppendLine("            ,substr(dmw.hydrntstat,1,2) hydrntstat				   ");
            query.AppendLine("        from wi_dminfo dmi										   ");
            query.AppendLine("            ,wi_dmwsrsrc dmw										   ");
            query.AppendLine("       where dmi.dmno = dmw.dmno									   ");
            query.AppendLine("      ) dmi														   ");
            query.AppendLine("     ,(															   ");
            query.AppendLine("      select loc_code												   ");
            query.AppendLine("            ,ftr_idn												   ");
            query.AppendLine("        from cm_location 											   ");
            query.AppendLine("       where ftr_code = 'BZ003'									   ");
            query.AppendLine("      ) loc														   ");
            query.AppendLine("  where dmi.sftridn(+) = loc.ftr_idn								   ");
            query.AppendLine("  group by loc.loc_code											   ");
            query.AppendLine(") b																   ");
            query.AppendLine("   on (a.loc_code = b.loc_code)									   ");
            query.AppendLine(" when matched then												   ");
            query.AppendLine("      update set a.junsu = b.junsu								   ");
            query.AppendLine("                ,a.gejunsu = b.gejunsu							   ");
            query.AppendLine("                ,a.pejunsu = b.pejunsu							   ");
            query.AppendLine("                ,a.stopjunsu = b.stopjunsu						   ");
            query.AppendLine(" when not matched then											   ");
            query.AppendLine("      insert (a.loc_code, a.junsu, a.gejunsu, a.pejunsu, a.stopjunsu)");
            query.AppendLine("      values (b.loc_code, b.junsu, b.gejunsu, b.pejunsu, b.stopjunsu)");

            return manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 중블록 수용가통계 수정 / 등록 : 전수, 개전수, 폐전수, 중지/정지전수
        /// </summary>
        internal int WV_MiddleBlockDMBatch_Total(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_consumer a                                             ");
            query.AppendLine("using (															   ");
            query.AppendLine("select loc.ploc_code loc_code										   ");
            query.AppendLine("      ,sum(wco.junsu) junsu										   ");
            query.AppendLine("      ,sum(wco.gejunsu) gejunsu									   ");
            query.AppendLine("      ,sum(wco.pejunsu) pejunsu									   ");
            query.AppendLine("      ,sum(wco.stopjunsu) stopjunsu								   ");
            query.AppendLine("  from wv_consumer wco											   ");
            query.AppendLine("      ,(															   ");
            query.AppendLine("       select ploc_code, loc_code									   ");
            query.AppendLine("         from cm_location											   ");
            query.AppendLine("        where ftr_code = 'BZ003'									   ");
            query.AppendLine("      ) loc														   ");
            query.AppendLine(" where wco.loc_code = loc.loc_code								   ");
            query.AppendLine("group by loc.ploc_code											   ");
            query.AppendLine(") b																   ");
            query.AppendLine("   on (a.loc_code = b.loc_code)									   ");
            query.AppendLine(" when matched then												   ");
            query.AppendLine("      update set a.junsu = b.junsu								   ");
            query.AppendLine("                ,a.gejunsu = b.gejunsu							   ");
            query.AppendLine("                ,a.pejunsu = b.pejunsu							   ");
            query.AppendLine("                ,a.stopjunsu = b.stopjunsu						   ");
            query.AppendLine(" when not matched then											   ");
            query.AppendLine("      insert (a.loc_code, a.junsu, a.gejunsu, a.pejunsu, a.stopjunsu)");
            query.AppendLine("      values (b.loc_code, b.junsu, b.gejunsu, b.pejunsu, b.stopjunsu)");

            return manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 대블록 수용가통계 수정 / 등록 : 전수, 개전수, 폐전수, 중지/정지전수
        /// </summary>
        internal int WV_LargeBlockDMBatch_Total(EMapper manager)
        { 
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_consumer a                                             ");
            query.AppendLine("using (															   ");
            query.AppendLine("select loc.ploc_code loc_code										   ");
            query.AppendLine("      ,sum(wco.junsu) junsu										   ");
            query.AppendLine("      ,sum(wco.gejunsu) gejunsu									   ");
            query.AppendLine("      ,sum(wco.pejunsu) pejunsu									   ");
            query.AppendLine("      ,sum(wco.stopjunsu) stopjunsu								   ");
            query.AppendLine("  from wv_consumer wco											   ");
            query.AppendLine("      ,(															   ");
            query.AppendLine("       select ploc_code, loc_code									   ");
            query.AppendLine("         from cm_location											   ");
            query.AppendLine("        where ftr_code = 'BZ002'									   ");
            query.AppendLine("      ) loc														   ");
            query.AppendLine(" where wco.loc_code = loc.loc_code								   ");
            query.AppendLine("group by loc.ploc_code											   ");
            query.AppendLine(") b																   ");
            query.AppendLine("   on (a.loc_code = b.loc_code)									   ");
            query.AppendLine(" when matched then												   ");
            query.AppendLine("      update set a.junsu = b.junsu								   ");
            query.AppendLine("                ,a.gejunsu = b.gejunsu							   ");
            query.AppendLine("                ,a.pejunsu = b.pejunsu							   ");
            query.AppendLine("                ,a.stopjunsu = b.stopjunsu						   ");
            query.AppendLine(" when not matched then											   ");
            query.AppendLine("      insert (a.loc_code, a.junsu, a.gejunsu, a.pejunsu, a.stopjunsu)");
            query.AppendLine("      values (b.loc_code, b.junsu, b.gejunsu, b.pejunsu, b.stopjunsu)");

            return manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 소블록 수용가 일별 통계 수정 / 등록 : 가정가구수, 비가정가구수, 급수전수, 계량기교체건수
        /// 
        /// 현재 급수전수 산정시에 데이터가 부족함.
        /// 현재 시작일자와 중지일자 두가지로 판단
        /// </summary>
        internal int WV_SmallBlockDMBatch_Day(EMapper manager, string DATEE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_consumer_day a																	 ");
            query.AppendLine("using (																						 ");
            query.AppendLine("select loc.loc_code																			 ");
            query.AppendLine("      ,:DATEE datee																			 ");
            query.AppendLine("      ,sum(decode(dmi.wsrepbizkndcd,'1000', dmi.wsnohshd, 0)) gajung_gagusu					 ");
            query.AppendLine("      ,sum(decode(dmi.wsrepbizkndcd,'1000', 0, dmi.wsnohshd)) bgajung_gagusu					 ");
            query.AppendLine("      ,count(*) gupsujunsu																	 ");
            query.AppendLine("      ,sum(dmi.count) mtrchgsu																 ");
            query.AppendLine("  from																						 ");
            query.AppendLine("      (																						 ");
            query.AppendLine("      select dmi.lftridn																		 ");
            query.AppendLine("            ,dmi.mftridn																		 ");
            query.AppendLine("            ,dmi.sftridn																		 ");
            query.AppendLine("            ,substr(dmw.hydrntstat,1,2) hydrntstat											 ");
            query.AppendLine("            ,dmw.wsrepbizkndcd																 ");
            query.AppendLine("            ,dmw.wsnohshd																		 ");
            query.AppendLine("            ,dmw.statappdt																	 ");
            query.AppendLine("            ,mdt.count																		 ");
            query.AppendLine("        from wi_dminfo dmi																	 ");
            query.AppendLine("            ,wi_dmwsrsrc dmw																	 ");
            query.AppendLine("            ,(																				 ");
            query.AppendLine("             select dmno, 1 count from wi_mtrchginfo where mddt = :DATEE    					 ");
            query.AppendLine("             union all                                                                       	 ");
            query.AppendLine("             select dmno, 1 count from wv_mtrchg where mddt = :DATEE   						 ");
            query.AppendLine("            ) mdt																				 ");
            query.AppendLine("       where dmw.dmno = dmi.dmno																 ");
            query.AppendLine("         and (dmw.strtdt <= :DATEE or dmw.strtdt is null)                                      ");
            query.AppendLine("         and (dmi.dmclass = '주' or dmi.dmclass is null)                                       ");
            query.AppendLine("         and mdt.dmno(+) = dmi.dmno															 ");
            query.AppendLine("      ) dmi																					 ");
            query.AppendLine("     ,(																						 ");
            query.AppendLine("      select loc_code																			 ");
            query.AppendLine("            ,ftr_idn																			 ");
            query.AppendLine("        from cm_location 																		 ");
            query.AppendLine("       where ftr_code = 'BZ003'																 ");
            query.AppendLine("      ) loc																					 ");
            query.AppendLine("  where dmi.sftridn(+) = loc.ftr_idn															 ");
            query.AppendLine("  group by loc.loc_code																		 ");
            query.AppendLine(") b																							 ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee)											 ");
            query.AppendLine(" when matched then																			 ");
            query.AppendLine("      update set a.gajung_gagusu = b.gajung_gagusu											 ");
            query.AppendLine("                ,a.bgajung_gagusu = b.bgajung_gagusu											 ");
            query.AppendLine("                ,a.gupsujunsu = b.gupsujunsu													 ");
            query.AppendLine("                ,a.mtrchgsu = b.mtrchgsu														 ");
            query.AppendLine(" when not matched then																		 ");
            query.AppendLine("      insert (a.loc_code, a.datee, a.gajung_gagusu, a.bgajung_gagusu, a.gupsujunsu, a.mtrchgsu)");
            query.AppendLine("      values (b.loc_code, b.datee, b.gajung_gagusu, b.bgajung_gagusu, b.gupsujunsu, b.mtrchgsu)");

            IDataParameter[] parameters =  {
                     new OracleParameter("DATEE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = DATEE;
            parameters[1].Value = DATEE;
            parameters[2].Value = DATEE;
            parameters[3].Value = DATEE;

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 중블록 수용가 일별 통계 수정 / 등록 : 가정가구수, 비가정가구수, 급수전수, 계량기교체건수
        /// </summary>
        internal int WV_MiddleBlockDMBatch_Day(EMapper manager, string DATEE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_consumer_day a																	 ");
            query.AppendLine("using (																						 ");
            query.AppendLine("select loc.ploc_code loc_code																	 ");
            query.AppendLine("      ,wcd.datee datee																		 ");
            query.AppendLine("      ,sum(wcd.gajung_gagusu) gajung_gagusu													 ");
            query.AppendLine("      ,sum(wcd.bgajung_gagusu) bgajung_gagusu													 ");
            query.AppendLine("      ,sum(wcd.gupsujunsu) gupsujunsu															 ");
            query.AppendLine("      ,sum(wcd.mtrchgsu) mtrchgsu																 ");
            query.AppendLine("  from wv_consumer_day wcd																	 ");
            query.AppendLine("      ,(																						 ");
            query.AppendLine("       select ploc_code, loc_code																 ");
            query.AppendLine("             ,ftr_idn																			 ");
            query.AppendLine("         from cm_location 																	 ");
            query.AppendLine("        where ftr_code = 'BZ003'																 ");
            query.AppendLine("       ) loc																					 ");
            query.AppendLine("  where wcd.loc_code = loc.loc_code															 ");
            query.AppendLine("    and wcd.datee = :DATEE																	 ");
            query.AppendLine("  group by loc.ploc_code, wcd.datee															 ");
            query.AppendLine(") b																							 ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee)											 ");
            query.AppendLine(" when matched then																			 ");
            query.AppendLine("      update set a.gajung_gagusu = b.gajung_gagusu											 ");
            query.AppendLine("                ,a.bgajung_gagusu = b.bgajung_gagusu											 ");
            query.AppendLine("                ,a.gupsujunsu = b.gupsujunsu													 ");
            query.AppendLine("                ,a.mtrchgsu = b.mtrchgsu														 ");
            query.AppendLine(" when not matched then																		 ");
            query.AppendLine("      insert (a.loc_code, a.datee, a.gajung_gagusu, a.bgajung_gagusu, a.gupsujunsu, a.mtrchgsu)");
            query.AppendLine("      values (b.loc_code, b.datee, b.gajung_gagusu, b.bgajung_gagusu, b.gupsujunsu, b.mtrchgsu)");

            IDataParameter[] parameters =  {
                     new OracleParameter("DATEE", OracleDbType.Varchar2)
                };
            parameters[0].Value = DATEE;

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 대블록 수용가 일별 통계 수정 / 등록 : 가정가구수, 비가정가구수, 급수전수, 계량기교체건수
        /// </summary>
        internal int WV_LargeBlockDMBatch_Day(EMapper manager, string DATEE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_consumer_day a																	 ");
            query.AppendLine("using (																						 ");
            query.AppendLine("select loc.ploc_code loc_code																	 ");
            query.AppendLine("      ,wcd.datee datee																		 ");
            query.AppendLine("      ,sum(wcd.gajung_gagusu) gajung_gagusu													 ");
            query.AppendLine("      ,sum(wcd.bgajung_gagusu) bgajung_gagusu													 ");
            query.AppendLine("      ,sum(wcd.gupsujunsu) gupsujunsu															 ");
            query.AppendLine("      ,sum(wcd.mtrchgsu) mtrchgsu																 ");
            query.AppendLine("  from wv_consumer_day wcd																	 ");
            query.AppendLine("      ,(																						 ");
            query.AppendLine("       select ploc_code, loc_code																 ");
            query.AppendLine("             ,ftr_idn																			 ");
            query.AppendLine("         from cm_location 																	 ");
            query.AppendLine("        where ftr_code = 'BZ002'																 ");
            query.AppendLine("       ) loc																					 ");
            query.AppendLine("  where wcd.loc_code = loc.loc_code															 ");
            query.AppendLine("    and wcd.datee = :DATEE																	 ");
            query.AppendLine("  group by loc.ploc_code, wcd.datee															 ");
            query.AppendLine(") b																							 ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee)											 ");
            query.AppendLine(" when matched then																			 ");
            query.AppendLine("      update set a.gajung_gagusu = b.gajung_gagusu											 ");
            query.AppendLine("                ,a.bgajung_gagusu = b.bgajung_gagusu											 ");
            query.AppendLine("                ,a.gupsujunsu = b.gupsujunsu													 ");
            query.AppendLine("                ,a.mtrchgsu = b.mtrchgsu														 ");
            query.AppendLine(" when not matched then																		 ");
            query.AppendLine("      insert (a.loc_code, a.datee, a.gajung_gagusu, a.bgajung_gagusu, a.gupsujunsu, a.mtrchgsu)");
            query.AppendLine("      values (b.loc_code, b.datee, b.gajung_gagusu, b.bgajung_gagusu, b.gupsujunsu, b.mtrchgsu)");

            IDataParameter[] parameters =  {
                     new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = DATEE;

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 소블록 수용가 월별 통계 수정 / 등록 : 가정사용량, 비가정사용량, 사용량
        /// </summary>
        internal void WV_SmallBlockDMBatch_Month(EMapper manager, string LOC_CODE, string MONTH)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("  merge into wv_consumer_mon a                                                                   ");
            query.AppendLine("  using (                                                                                        ");
            query.AppendLine("  select dmi.loc_code                                                                            ");
            query.AppendLine("        ,to_char(to_date(dmi.stym,'yyyymm'),'yyyymm') year_mon                    ");
            query.AppendLine("        ,sum(decode(dmi.wsrepbizkndcd,'1000', dmi.wsstvol, 0)) gajung_amtuse                     ");
            query.AppendLine("        ,sum(decode(dmi.wsrepbizkndcd,'1000', 0, dmi.wsstvol)) bgajung_amtuse                    ");
            query.AppendLine("        ,sum(dmi.wsstvol) amtuse                                                                 ");
            query.AppendLine("    from                                                                                         ");
            query.AppendLine("        (                                                                                        ");
            query.AppendLine("        select loc.loc_code                                                                      ");
            query.AppendLine("              ,dmi.lftridn                                                                       ");
            query.AppendLine("              ,dmi.mftridn                                                                       ");
            query.AppendLine("              ,dmi.sftridn                                                                       ");
            query.AppendLine("              ,stw.stym                                                                          ");
            query.AppendLine("              ,dmw.wsrepbizkndcd                                                                 ");
            query.AppendLine("              ,stw.wsstvol                                                                       ");
            query.AppendLine("          from (select loc_code                                                                  ");
            query.AppendLine("                      ,sgccd                                                                     ");
            query.AppendLine("                      ,(select pftr_idn from cm_location where ftr_idn = a.pftr_idn and  ftr_code = 'BZ002') lftridn     ");
            query.AppendLine("                      ,pftr_idn mftridn, ftr_idn from cm_location a where ftr_code = 'BZ003') loc");
            query.AppendLine("              ,wi_dminfo dmi                                                                        ");
            query.AppendLine("              ,wi_dmwsrsrc dmw                                                                      ");
            query.AppendLine("              ,wi_stwchrg stw                                                                       ");
            query.AppendLine("         where 1 = 1                                                                             ");
            query.AppendLine("           and loc.loc_code                                                                      ");
            query.AppendLine("               in (select loc_code from cm_location where ftr_code = 'BZ003'                     ");
            query.AppendLine("                    start with loc_code = :LOC_CODE connect by prior loc_code = ploc_code)       ");
            query.AppendLine("           and dmi.sgccd = loc.sgccd                                                             ");
            query.AppendLine("           and dmi.lftridn = loc.lftridn                                                         ");
            query.AppendLine("           and dmi.mftridn = loc.mftridn                                                         ");
            query.AppendLine("           and dmi.sftridn = loc.ftr_idn                                                         ");
            //query.AppendLine("           and (dmi.dmclass = '주' or dmi.dmclass is null)                                       ");
            query.AppendLine("           and dmw.dmno = dmi.dmno                                                               ");
            query.AppendLine("           and stw.dmno = dmw.dmno                                                               ");

            //유수율분석과 요금조정 테이블의 데이터와는 1개월의 차이가 난다.
            //2월의 유수율분석의 사용량은 요금조정 테이블의 1월의 데이터를 사용해야 한다.

            query.AppendLine("           and stw.stym = to_char(to_date(:MONTH,'yyyymm'),'yyyymm')               ");
            query.AppendLine("       ) dmi                                                                                     ");
            query.AppendLine("   group by dmi.loc_code, dmi.stym                                                               ");
            query.AppendLine("  ) b                                                                                            ");
            query.AppendLine("     on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)                                    ");
            query.AppendLine("   when matched then                                                                             ");
            query.AppendLine("        update set a.gajung_amtuse = b.gajung_amtuse                                             ");
            query.AppendLine("                  ,a.bgajung_amtuse = b.bgajung_amtuse                                           ");
            query.AppendLine("                  ,a.amtuse = b.amtuse                                                           ");
            query.AppendLine("   when not matched then                                                                         ");
            query.AppendLine("        insert (a.loc_code, a.year_mon, a.gajung_amtuse, a.bgajung_amtuse, a.amtuse)             ");
            query.AppendLine("        values (b.loc_code, b.year_mon, b.gajung_amtuse, b.bgajung_amtuse, b.amtuse)             ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("MONTH", OracleDbType.Varchar2)
                };

            parameters[0].Value = LOC_CODE;
            parameters[1].Value = MONTH;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 중블록 수용가 월별 통계 수정 / 등록 : 가정사용량, 비가정사용량, 사용량
        /// </summary>
        internal void WV_MiddleBlockDMBatch_Month(EMapper manager, string MONTH)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_consumer_mon a														");
            query.AppendLine("using (																			");
            query.AppendLine("select loc.ploc_code loc_code														");
            query.AppendLine("      ,wcm.year_mon year_mon														");
            query.AppendLine("      ,sum(wcm.gajung_amtuse) gajung_amtuse										");
            query.AppendLine("      ,sum(wcm.bgajung_amtuse) bgajung_amtuse										");
            query.AppendLine("      ,sum(wcm.amtuse) amtuse														");
            query.AppendLine("  from wv_consumer_mon wcm														");
            query.AppendLine("      ,(																			");
            query.AppendLine("       select ploc_code, loc_code													");
            query.AppendLine("             ,ftr_idn																");
            query.AppendLine("         from cm_location 														");
            query.AppendLine("        where ftr_code = 'BZ003'													");
            //query.AppendLine("          and (kt_gbn != '003' or kt_gbn is null)									");
            query.AppendLine("       ) loc																		");
            query.AppendLine("  where wcm.loc_code = loc.loc_code												");
            query.AppendLine("    and wcm.year_mon = :MONTH														");
            query.AppendLine("  group by loc.ploc_code, wcm.year_mon											");
            query.AppendLine(") b																				");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)						");
            query.AppendLine(" when matched then																");
            query.AppendLine("      update set a.gajung_amtuse = b.gajung_amtuse								");
            query.AppendLine("                ,a.bgajung_amtuse = b.bgajung_amtuse								");
            query.AppendLine("                ,a.amtuse = b.amtuse												");
            query.AppendLine(" when not matched then															");
            query.AppendLine("      insert (a.loc_code, a.year_mon, a.gajung_amtuse, a.bgajung_amtuse, a.amtuse)");
            query.AppendLine("      values (b.loc_code, b.year_mon, b.gajung_amtuse, b.bgajung_amtuse, b.amtuse)");

            IDataParameter[] parameters =  {
                     new OracleParameter("MONTH", OracleDbType.Varchar2)
                };

            parameters[0].Value = MONTH;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 대블록 수용가 월별 통계 수정 / 등록 : 가정사용량, 비가정사용량, 사용량
        /// </summary>
        internal void WV_LargeBlockDMBatch_Month(EMapper manager, string MONTH)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_consumer_mon a														");
            query.AppendLine("using (																			");
            query.AppendLine("select loc.ploc_code loc_code														");
            query.AppendLine("      ,wcm.year_mon year_mon														");
            query.AppendLine("      ,sum(wcm.gajung_amtuse) gajung_amtuse										");
            query.AppendLine("      ,sum(wcm.bgajung_amtuse) bgajung_amtuse										");
            query.AppendLine("      ,sum(wcm.amtuse) amtuse														");
            query.AppendLine("  from wv_consumer_mon wcm														");

            query.AppendLine("      ,(																			");
            query.AppendLine("       select (select loc_code from cm_location where ftr_code = 'BZ001'          ");
            query.AppendLine("                start with loc_code = loc.loc_code 							    ");
            query.AppendLine("              connect by loc_code = prior ploc_code							    ");
            query.AppendLine("              ) ploc_code													        ");
            query.AppendLine("             ,loc.loc_code													    ");
            query.AppendLine("             ,loc.ftr_idn													        ");
            query.AppendLine("         from cm_location loc												        ");
            query.AppendLine("        where ftr_code = 'BZ003'												    ");
            query.AppendLine("       ) loc																		");

            query.AppendLine("  where wcm.loc_code = loc.loc_code												");
            query.AppendLine("    and wcm.year_mon = :MONTH														");
            query.AppendLine("  group by loc.ploc_code, wcm.year_mon											");
            query.AppendLine(") b																				");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)						");
            query.AppendLine(" when matched then																");
            query.AppendLine("      update set a.gajung_amtuse = b.gajung_amtuse								");
            query.AppendLine("                ,a.bgajung_amtuse = b.bgajung_amtuse								");
            query.AppendLine("                ,a.amtuse = b.amtuse												");
            query.AppendLine(" when not matched then															");
            query.AppendLine("      insert (a.loc_code, a.year_mon, a.gajung_amtuse, a.bgajung_amtuse, a.amtuse)");
            query.AppendLine("      values (b.loc_code, b.year_mon, b.gajung_amtuse, b.bgajung_amtuse, b.amtuse)");

            IDataParameter[] parameters =  {
                     new OracleParameter("MONTH", OracleDbType.Varchar2)
                };

            parameters[0].Value = MONTH;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 상수관로 일자별현황
        /// </summary>
        internal void WV_Pipe_Ls_Day(EMapper manager, string STARTDATE, string ENDDATE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_pipe_lm_day a                                                                               ");
            query.AppendLine("using (                                                                                                   ");
            query.AppendLine("select b.loc_code                                                                                         ");
            query.AppendLine("      ,tmp.datee                                                                                          ");
            query.AppendLine("      ,a.saa_cde                                                                                          ");
            query.AppendLine("      ,sum(a.pip_len) pip_len                                                                             ");
            query.AppendLine("  from si_pipe_block a                                                                                    ");
            query.AppendLine("      ,cm_location b                                                                                      ");
            query.AppendLine("      ,(                                                                                                  ");
            query.AppendLine("       select to_char(to_date(:STARTDATE, 'yyyymmdd') -1 + rownum, 'yyyymmdd') datee                      ");
            query.AppendLine("         from dual connect by to_date(:STARTDATE, 'yyyymmdd') -1 + rownum <= to_date(:ENDDATE, 'yyyymmdd')");
            query.AppendLine("       ) tmp                                                                                              ");
            query.AppendLine(" where 1 = 1                                                                                              ");
            query.AppendLine("   and b.ftr_code = 'BZ003'                                                                               ");
            query.AppendLine("   and a.b_ftr_idn = b.ftr_idn                                                                            ");
            query.AppendLine("   and a.s_name = 'WTL_PIPE_LS'                                                                           ");
            query.AppendLine(" group by                                                                                                 ");
            query.AppendLine("       b.loc_code                                                                                         ");
            query.AppendLine("      ,tmp.datee                                                                                          ");
            query.AppendLine("      ,a.saa_cde                                                                                          ");
            query.AppendLine(") b                                                                                                       ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee and a.saa_cde = b.saa_cde)                           ");
            query.AppendLine(" when matched then                                                                                        ");
            query.AppendLine("      update set a.pip_len = b.pip_len                                                                    ");
            query.AppendLine(" when not matched then                                                                                    ");
            query.AppendLine("      insert (a.loc_code, a.datee, a.saa_cde, a.pip_len)                                                  ");
            query.AppendLine("      values (b.loc_code, b.datee, b.saa_cde, b.pip_len)                                                  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = STARTDATE;
            parameters[1].Value = STARTDATE;
            parameters[2].Value = ENDDATE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 상수관로 일자별현황 중블록
        /// </summary>
        internal void WV_Pipe_Ls_Middle_Day(EMapper manager, string DATEE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_pipe_lm_day a                                                         ");
            query.AppendLine("using (                                                                             ");
            query.AppendLine("select loc.ploc_code loc_code                                                       ");
            query.AppendLine("      ,wsl.datee datee                                                              ");
            query.AppendLine("      ,wsl.saa_cde saa_cde                                                          ");
            query.AppendLine("      ,sum(wsl.pip_len) pip_len                                                     ");
            query.AppendLine("  from wv_pipe_lm_day wsl                                                           ");
            query.AppendLine("      ,(                                                                            ");
            query.AppendLine("       select ploc_code, loc_code                                                   ");
            query.AppendLine("             ,ftr_idn                                                               ");
            query.AppendLine("         from cm_location                                                           ");
            query.AppendLine("        where ftr_code = 'BZ003'                                                    ");
            query.AppendLine("       ) loc                                                                        ");
            query.AppendLine("  where wsl.loc_code = loc.loc_code                                                 ");
            query.AppendLine("    and wsl.datee = :DATEE                                                          ");
            query.AppendLine("  group by loc.ploc_code, wsl.datee, wsl.saa_cde                                    ");
            query.AppendLine(") b                                                                                 ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee and a.saa_cde = b.saa_cde)     ");
            query.AppendLine(" when matched then                                                                  ");
            query.AppendLine("      update set a.pip_len = b.pip_len                                              ");
            query.AppendLine(" when not matched then                                                              ");
            query.AppendLine("      insert (a.loc_code, a.datee, a.saa_cde, a.pip_len)                            ");
            query.AppendLine("      values (b.loc_code, b.datee, b.saa_cde, b.pip_len)                            ");

            IDataParameter[] parameters =  {
                     new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = DATEE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 상수관로 일자별현황 대블록
        /// </summary>
        internal void WV_Pipe_Ls_Large_Day(EMapper manager, string DATEE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_pipe_lm_day a                                                         ");
            query.AppendLine("using (                                                                             ");
            query.AppendLine("select loc.ploc_code loc_code                                                       ");
            query.AppendLine("      ,wsl.datee datee                                                              ");
            query.AppendLine("      ,wsl.saa_cde saa_cde                                                          ");
            query.AppendLine("      ,sum(wsl.pip_len) pip_len                                                     ");
            query.AppendLine("  from wv_pipe_lm_day wsl                                                           ");
            query.AppendLine("      ,(                                                                            ");
            query.AppendLine("       select ploc_code, loc_code                                                   ");
            query.AppendLine("             ,ftr_idn                                                               ");
            query.AppendLine("         from cm_location                                                           ");
            query.AppendLine("        where ftr_code = 'BZ002'                                                    ");
            query.AppendLine("       ) loc                                                                        ");
            query.AppendLine("  where wsl.loc_code = loc.loc_code                                                 ");
            query.AppendLine("    and wsl.datee = :DATEE                                                          ");
            query.AppendLine("  group by loc.ploc_code, wsl.datee, wsl.saa_cde                                    ");
            query.AppendLine(") b                                                                                 ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee and a.saa_cde = b.saa_cde)     ");
            query.AppendLine(" when matched then                                                                  ");
            query.AppendLine("      update set a.pip_len = b.pip_len                                              ");
            query.AppendLine(" when not matched then                                                              ");
            query.AppendLine("      insert (a.loc_code, a.datee, a.saa_cde, a.pip_len)                            ");
            query.AppendLine("      values (b.loc_code, b.datee, b.saa_cde, b.pip_len)                            ");

            IDataParameter[] parameters =  {
                     new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = DATEE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 급수관로 일자별현황
        /// </summary>
        internal void WV_Sply_Ls_Day(EMapper manager, string STARTDATE, string ENDDATE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_sply_ls_day a                                                                               ");
            query.AppendLine("using (                                                                                                   ");
            query.AppendLine("select b.loc_code                                                                                         ");
            query.AppendLine("      ,tmp.datee                                                                                          ");
            query.AppendLine("      ,a.saa_cde                                                                                          ");
            query.AppendLine("      ,sum(a.pip_len) pip_len                                                                             ");
            query.AppendLine("  from si_pipe_block a                                                                                    ");
            query.AppendLine("      ,cm_location b                                                                                      ");
            query.AppendLine("      ,(                                                                                                  ");
            query.AppendLine("       select to_char(to_date(:STARTDATE, 'yyyymmdd') -1 + rownum, 'yyyymmdd') datee                      ");
            query.AppendLine("         from dual connect by to_date(:STARTDATE, 'yyyymmdd') -1 + rownum <= to_date(:ENDDATE, 'yyyymmdd')");
            query.AppendLine("       ) tmp                                                                                              ");
            query.AppendLine(" where 1 = 1                                                                                              ");
            query.AppendLine("   and b.ftr_code = 'BZ003'                                                                               ");
            query.AppendLine("   and a.b_ftr_idn = b.ftr_idn                                                                            ");
            query.AppendLine("   and a.s_name = 'WTL_SPLY_LS'                                                                           ");
            query.AppendLine(" group by                                                                                                 ");
            query.AppendLine("       b.loc_code                                                                                         ");
            query.AppendLine("      ,tmp.datee                                                                                          ");
            query.AppendLine("      ,a.saa_cde                                                                                          ");
            query.AppendLine(") b                                                                                                       ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee and a.saa_cde = b.saa_cde)                           ");
            query.AppendLine(" when matched then                                                                                        ");
            query.AppendLine("      update set a.pip_len = b.pip_len                                                                    ");
            query.AppendLine(" when not matched then                                                                                    ");
            query.AppendLine("      insert (a.loc_code, a.datee, a.saa_cde, a.pip_len)                                                  ");
            query.AppendLine("      values (b.loc_code, b.datee, b.saa_cde, b.pip_len)                                                  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = STARTDATE;
            parameters[1].Value = STARTDATE;
            parameters[2].Value = ENDDATE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 급수관로 일자별현황 중블록
        /// </summary>
        internal void WV_Sply_Ls_Middle_Day(EMapper manager, string DATEE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_sply_ls_day a                                                         ");
            query.AppendLine("using (                                                                             ");
            query.AppendLine("select loc.ploc_code loc_code                                                       ");
            query.AppendLine("      ,wsl.datee datee                                                              ");
            query.AppendLine("      ,wsl.saa_cde saa_cde                                                          ");
            query.AppendLine("      ,sum(wsl.pip_len) pip_len                                                     ");
            query.AppendLine("  from wv_sply_ls_day wsl                                                           ");
            query.AppendLine("      ,(                                                                            ");
            query.AppendLine("       select ploc_code, loc_code                                                   ");
            query.AppendLine("             ,ftr_idn                                                               ");
            query.AppendLine("         from cm_location                                                           ");
            query.AppendLine("        where ftr_code = 'BZ003'                                                    ");
            query.AppendLine("       ) loc                                                                        ");
            query.AppendLine("  where wsl.loc_code = loc.loc_code                                                 ");
            query.AppendLine("    and wsl.datee = :DATEE                                                          ");
            query.AppendLine("  group by loc.ploc_code, wsl.datee, wsl.saa_cde                                    ");
            query.AppendLine(") b                                                                                 ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee and a.saa_cde = b.saa_cde)     ");
            query.AppendLine(" when matched then                                                                  ");
            query.AppendLine("      update set a.pip_len = b.pip_len                                              ");
            query.AppendLine(" when not matched then                                                              ");
            query.AppendLine("      insert (a.loc_code, a.datee, a.saa_cde, a.pip_len)                            ");
            query.AppendLine("      values (b.loc_code, b.datee, b.saa_cde, b.pip_len)                            ");

            IDataParameter[] parameters =  {
                     new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = DATEE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 급수관로 일자별현황 대블록
        /// </summary>
        internal void WV_Sply_Ls_Large_Day(EMapper manager, string DATEE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_sply_ls_day a                                                         ");
            query.AppendLine("using (                                                                             ");
            query.AppendLine("select loc.ploc_code loc_code                                                       ");
            query.AppendLine("      ,wsl.datee datee                                                              ");
            query.AppendLine("      ,wsl.saa_cde saa_cde                                                          ");
            query.AppendLine("      ,sum(wsl.pip_len) pip_len                                                     ");
            query.AppendLine("  from wv_sply_ls_day wsl                                                           ");
            query.AppendLine("      ,(                                                                            ");
            query.AppendLine("       select ploc_code, loc_code                                                   ");
            query.AppendLine("             ,ftr_idn                                                               ");
            query.AppendLine("         from cm_location                                                           ");
            query.AppendLine("        where ftr_code = 'BZ002'                                                    ");
            query.AppendLine("       ) loc                                                                        ");
            query.AppendLine("  where wsl.loc_code = loc.loc_code                                                 ");
            query.AppendLine("    and wsl.datee = :DATEE                                                          ");
            query.AppendLine("  group by loc.ploc_code, wsl.datee, wsl.saa_cde                                    ");
            query.AppendLine(") b                                                                                 ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee and a.saa_cde = b.saa_cde)     ");
            query.AppendLine(" when matched then                                                                  ");
            query.AppendLine("      update set a.pip_len = b.pip_len                                              ");
            query.AppendLine(" when not matched then                                                              ");
            query.AppendLine("      insert (a.loc_code, a.datee, a.saa_cde, a.pip_len)                            ");
            query.AppendLine("      values (b.loc_code, b.datee, b.saa_cde, b.pip_len)                            ");

            IDataParameter[] parameters =  {
                     new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = DATEE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 유수율분석 특정월 (소블록)
        /// </summary>
        internal void WV_Revenue_Ratio_Small(EMapper manager, string YEAR_MON, string STARTDATE, string ENDDATE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio a                                                                                 ");
            query.AppendLine("using (																										");
            query.AppendLine("select loc.loc_code																							");
            query.AppendLine("      ,:YEAR_MON year_mon																						");
            query.AppendLine("      ,round(sum(iay.value),2) water_supplied																	");
            query.AppendLine("      ,(select amtuse																							");
            query.AppendLine("          from wv_consumer_mon																				");
            query.AppendLine("         where loc_code = loc.loc_code																		");
            query.AppendLine("           and year_mon = :YEAR_MON																			");
            query.AppendLine("       ) revenue																								");
            query.AppendLine("  from cm_location loc																						");
            query.AppendLine("      ,if_ihtags iih																							");
            query.AppendLine("      ,if_tag_gbn itg																							");
            query.AppendLine("      ,if_accumulation_yesterday iay																			");
            query.AppendLine(" where 1 = 1																									");
            query.AppendLine("   and loc.ftr_code = 'BZ003'                                                                                 ");
            query.AppendLine("   and iih.loc_code = decode(loc.kt_gbn, '002', (select loc_code from cm_location where ploc_code = loc.loc_code), loc.loc_code) 																			");
            query.AppendLine("   and itg.tagname = iih.tagname																				");
            query.AppendLine("   and itg.tag_gbn = 'YD'																						");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                             ");
            query.AppendLine("   and iay.tagname = itg.tagname																				");
            query.AppendLine("   and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')					");
            query.AppendLine(" group by																										");
            query.AppendLine("       loc.loc_code																							");
            query.AppendLine(") b																											");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)													");
            query.AppendLine(" when matched then																							");
            query.AppendLine("      update set a.water_supplied = b.water_supplied, a.revenue = b.revenue							   	    ");
            query.AppendLine(" when not matched then																						");
            query.AppendLine("      insert (a.loc_code, a.year_mon, a.water_supplied, a.revenue)											");
            query.AppendLine("      values (b.loc_code, b.year_mon, b.water_supplied, b.revenue)											");

            IDataParameter[] parameters =  {
                     new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = YEAR_MON;
            parameters[1].Value = YEAR_MON;
            parameters[2].Value = STARTDATE;
            parameters[3].Value = ENDDATE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 유수율분석 특정월 (중블록)
        /// </summary>
        internal void WV_Revenue_Ratio_Middle(EMapper manager, string YEAR_MON, string STARTDATE, string ENDDATE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio a                                                                                 ");
            query.AppendLine("using (																										");
            query.AppendLine("select loc.loc_code																							");
            query.AppendLine("      ,:YEAR_MON year_mon																						");
            query.AppendLine("      ,round(sum(iay.value),2) water_supplied																	");
            query.AppendLine("      ,(select amtuse																							");
            query.AppendLine("          from wv_consumer_mon																				");
            query.AppendLine("         where loc_code = loc.loc_code																		");
            query.AppendLine("           and year_mon = :YEAR_MON																			");
            query.AppendLine("       ) revenue																								");
            query.AppendLine("  from cm_location loc																						");
            query.AppendLine("      ,if_ihtags iih																							");
            query.AppendLine("      ,if_tag_gbn itg																							");
            query.AppendLine("      ,if_accumulation_yesterday iay																			");
            query.AppendLine(" where 1 = 1																									");
            query.AppendLine("   and loc.ftr_code = 'BZ002'                                                                                 ");
            query.AppendLine("   and iih.loc_code = decode(loc.kt_gbn, '002', (select loc_code from cm_location where ploc_code = loc.loc_code), loc.loc_code) ");
            query.AppendLine("   and itg.tagname = iih.tagname																				");
            query.AppendLine("   and itg.tag_gbn = 'YD'																						");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                             ");
            query.AppendLine("   and iay.tagname = itg.tagname																				");
            query.AppendLine("   and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')					");
            query.AppendLine(" group by																										");
            query.AppendLine("       loc.loc_code																							");
            query.AppendLine(") b																											");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)													");
            query.AppendLine(" when matched then																							");
            query.AppendLine("      update set a.water_supplied = b.water_supplied, a.revenue = b.revenue							   	    ");
            query.AppendLine(" when not matched then																						");
            query.AppendLine("      insert (a.loc_code, a.year_mon, a.water_supplied, a.revenue)											");
            query.AppendLine("      values (b.loc_code, b.year_mon, b.water_supplied, b.revenue)											");

            IDataParameter[] parameters =  {
                     new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = YEAR_MON;
            parameters[1].Value = YEAR_MON;
            parameters[2].Value = STARTDATE;
            parameters[3].Value = ENDDATE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 유수율분석 특정월 (대블록)
        /// </summary>
        internal void WV_Revenue_Ratio_Large(EMapper manager, string YEAR_MON, string STARTDATE, string ENDDATE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio a                                                                                   ");
            query.AppendLine("using (																										  ");
            query.AppendLine("select loc.loc_code                                                                                             ");
            query.AppendLine("      ,loc.year_mon																							  ");
            query.AppendLine("      ,round(sum(loc.water_supplied),2) water_supplied														  ");
            query.AppendLine("	  ,(select amtuse																							  ");
            query.AppendLine("		  from wv_consumer_mon																					  ");
            query.AppendLine("		 where loc_code = loc.loc_code																			  ");
            query.AppendLine("		   and year_mon = :YEAR_MON																				  ");
            query.AppendLine("	   ) revenue																								  ");
            query.AppendLine("  from																										  ");
            query.AppendLine("      (																										  ");
            query.AppendLine("select (select loc_code from cm_location where ftr_code = 'BZ001'												  ");
            query.AppendLine("		 start with loc_code = loc.loc_code																		  ");
            query.AppendLine("	   connect by loc_code = prior ploc_code																	  ");
            query.AppendLine("	   ) loc_code																								  ");
            query.AppendLine("	  ,:YEAR_MON year_mon																						  ");
            query.AppendLine("	  ,sum(iay.value) water_supplied																			  ");
            query.AppendLine("  from cm_location loc																						  ");
            query.AppendLine("	  ,if_ihtags iih																							  ");
            query.AppendLine("	  ,if_tag_gbn itg																							  ");
            query.AppendLine("	  ,if_accumulation_yesterday iay																			  ");
            query.AppendLine(" where 1 = 1																									  ");
            query.AppendLine("   and (loc.ftr_code = 'BZ002' or (loc.ftr_code = 'BZ003' and loc.kt_gbn = '003'))							  ");
            query.AppendLine("   and iih.loc_code =																							  ");
            query.AppendLine("	   decode(loc.kt_gbn, '002', (select loc_code from cm_location where ploc_code = loc.loc_code), loc.loc_code) ");
            query.AppendLine("   and itg.tagname = iih.tagname																				  ");
            query.AppendLine("   and itg.tag_gbn = 'YD'																						  ");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')								  ");
            query.AppendLine("   and iay.tagname = itg.tagname																				  ");
            query.AppendLine("   and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')					  ");
            query.AppendLine(" group by loc.loc_code																						  ");
            query.AppendLine("      ) loc																									  ");
            query.AppendLine(" group by loc.loc_code, loc.year_mon																			  ");
            query.AppendLine(") b																											  ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)												  	  ");
            query.AppendLine(" when matched then																							  ");
            query.AppendLine("      update set a.water_supplied = b.water_supplied, a.revenue = b.revenue							   	      ");
            query.AppendLine(" when not matched then																						  ");
            query.AppendLine("      insert (a.loc_code, a.year_mon, a.water_supplied, a.revenue)											  ");
            query.AppendLine("      values (b.loc_code, b.year_mon, b.water_supplied, b.revenue)											  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = YEAR_MON;
            parameters[1].Value = YEAR_MON;
            parameters[2].Value = STARTDATE;
            parameters[3].Value = ENDDATE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 실시간수리해석결과 (평균수압지점 수압값)
        /// </summary>
        internal void WV_WHHpresResultBatch_Day(EMapper manager, string STARTDATE, string ENDDATE)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_hpres_avg a                                                        ");
            query.AppendLine("using (                                                                          ");
            query.AppendLine("select b.loc_code loc_code                                                                                      ");
            query.AppendLine("      ,substr(a.target_date,1,10)||'00' dates																	  ");
            query.AppendLine("      ,avg(a.value)/10 value																						  ");
            query.AppendLine("  from																										  ");
            query.AppendLine("      (																										  ");
            query.AppendLine("      select regexp_substr(d.position_info,'[^|]+', 1, 2) ftr_code											  ");
            query.AppendLine("            ,regexp_substr(d.position_info,'[^|]+', 1, 3) ftr_idn												  ");
            query.AppendLine("            ,b.target_date																					  ");
            query.AppendLine("            ,c.node_id																						  ");
            query.AppendLine("            ,min(b.target_date) over (partition by d.position_info, substr(b.target_date,1,10)) min_target_date ");
            query.AppendLine("            ,(																								  ");
            query.AppendLine("             select pressure 																					  ");
            query.AppendLine("               from wh_rpt_nodes 																				  ");
            query.AppendLine("              where rpt_number = c.rpt_number 																  ");
            query.AppendLine("                and inp_number = c.inp_number 																  ");
            query.AppendLine("                and node_id = c.node_id 																		  ");
            query.AppendLine("                and analysis_time = c.analysis_time															  ");
            query.AppendLine("             ) value																							  ");
            query.AppendLine("        from wh_title a																						  ");
            query.AppendLine("            ,wh_rpt_master b																					  ");
            query.AppendLine("            ,wh_rpt_nodes c																					  ");
            query.AppendLine("            ,wh_tags d																						  ");
            query.AppendLine("       where a.use_gbn = 'WH'																					  ");
            query.AppendLine("         and b.inp_number = a.inp_number																		  ");
            query.AppendLine("         and b.target_date between :STARTDATE||'0000' and :ENDDATE||'2359'									  ");
            query.AppendLine("         and b.auto_manual = 'A'																				  ");
            query.AppendLine("         and c.rpt_number = b.rpt_number																		  ");
            query.AppendLine("         and c.inp_number = b.inp_number																		  ");
            query.AppendLine("         and d.inp_number = c.inp_number																		  ");
            query.AppendLine("         and d.id = c.node_id																					  ");
            query.AppendLine("         and d.type = 'NODE'																					  ");
            query.AppendLine("      ) a																										  ");
            query.AppendLine("      ,(																										  ");
            query.AppendLine("      select a.loc_code																						  ");
            query.AppendLine("            ,a.ftr_idn																						  ");
            query.AppendLine("            ,b.id																								  ");
            query.AppendLine("       from cm_location a																						  ");
            query.AppendLine("           ,wh_avg_pres_node b																				  ");
            query.AppendLine("      where a.ftr_code = 'BZ003'																				  ");
            query.AppendLine("        and b.loc_code(+) = a.loc_code																		  ");
            query.AppendLine("      ) b																										  ");
            query.AppendLine(" where a.target_date = a.min_target_date																		  ");
            query.AppendLine("   and a.ftr_code = 'BZ003'																					  ");
            query.AppendLine("   and a.ftr_idn = b.ftr_idn																					  ");
            query.AppendLine("   and a.node_id = decode(b.id, null, a.node_id, b.id)														  ");
            query.AppendLine(" group by 																									  ");
            query.AppendLine("       b.loc_code																								  ");
            query.AppendLine("      ,substr(a.target_date,1,10)																				  ");
            query.AppendLine(") b                                                                              ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.dates = b.dates)                            ");
            query.AppendLine(" when matched then                                                               ");
            query.AppendLine("      update set a.value = b.value                                               ");
            query.AppendLine(" when not matched then                                                           ");
            query.AppendLine("      insert (a.loc_code, a.dates, a.value)                                      ");
            query.AppendLine("      values (b.loc_code, b.dates, b.value)                                      ");

            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = STARTDATE;
            parameters[1].Value = ENDDATE;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //대블록 목록을 조회한다.
        public DataSet SelectLBlock(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" select loc_code from cm_location where ftr_code = 'BZ001'                       ");

            return manager.ExecuteScriptDataSet(query.ToString(), null, "RESULT");
        }


        public object GetSuppliedDay(EMapper manager, string loc_code)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("select supplied_day                                                              ");
            query.AppendLine("  from																		   ");
            query.AppendLine("      (																		   ");
            query.AppendLine("      select loc_code															   ");
            query.AppendLine("        from cm_location														   ");
            query.AppendLine("       where ftr_code = 'BZ001'												   ");
            query.AppendLine("       start with loc_code = :LOC_CODE										   ");
            query.AppendLine("       connect by loc_code = prior ploc_code									   ");
            query.AppendLine("      ) a																		   ");
            query.AppendLine("     ,wv_block_default_option b												   ");
            query.AppendLine(" where b.loc_code = a.loc_code												   ");

            IDataParameter[] parameters =  {
                          new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object GetRevenueFix(EMapper manager, string loc_code)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select revenue_fix                                                               ");
            query.AppendLine("  from																		   ");
            query.AppendLine("      (																		   ");
            query.AppendLine("      select loc_code															   ");
            query.AppendLine("        from cm_location														   ");
            query.AppendLine("       where ftr_code = 'BZ001'												   ");
            query.AppendLine("       start with loc_code = :LOC_CODE										   ");
            query.AppendLine("       connect by loc_code = prior ploc_code									   ");
            query.AppendLine("      ) a																		   ");
            query.AppendLine("     ,wv_block_default_option b												   ");
            query.AppendLine(" where b.loc_code = a.loc_code												   ");

            IDataParameter[] parameters =  {
                          new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }
    }
}
