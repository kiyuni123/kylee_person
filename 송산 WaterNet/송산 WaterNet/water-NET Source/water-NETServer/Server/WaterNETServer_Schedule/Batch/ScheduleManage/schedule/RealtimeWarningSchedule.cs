﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using WaterNETServer.IF_iwater.dao;
using System.Threading;
using EMFrame.log;
using WaterNETServer.ScheduleManage.interface1;
using WaterNETServer.Warning.AddonWarning;
using System.Windows.Forms;



/// 실시간 경고

namespace WaterNETServer.ScheduleManage.schedule
{
    public class RealtimeWarningSchedule : BaseSchedule
    {
        public RealtimeWarningSchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                OleDBWork tWork = new OleDBWork();
                OracleWork oWork = new OracleWork();

                this.autorun_target_count = 2;

                // 수질경보처리
                QualityWarning qWarning = new QualityWarning();
                qWarning.Analysis_RTData();

                // 감압밸브경고처리
                ReducePressureWarning rWarning = new ReducePressureWarning();
                rWarning.Analysis_RTData();

                this.autorun_value = 100;
                this.autorun_complete_count = this.autorun_target_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return false;
        }
    }
}
