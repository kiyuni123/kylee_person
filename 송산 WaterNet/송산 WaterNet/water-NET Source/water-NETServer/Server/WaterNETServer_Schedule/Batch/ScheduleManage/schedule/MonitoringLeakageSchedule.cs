﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using EMFrame.log;
using WaterNETServer.BatchJobs.dao;
using WaterNETServer.LeakMonitoring.Control;
using WaterNETServer.LeakMonitoring.enum1;
using System.Xml;
using WaterNETServer.ScheduleManage.interface1;
using System.Windows.Forms;



/// 누수감시

namespace WaterNETServer.ScheduleManage.schedule
{
    public class MonitoringLeakageSchedule : BaseSchedule
    {
        public MonitoringLeakageSchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                Monitering monitering = new Monitering();
                this.autorun_target_count = monitering.SelectLocationCount();
                monitering.AddAll();
                monitering.Start(TARGETDATE.AddDays(-1), MONITOR_TYPE.LEAKAGE);
                monitering.WriteWarning();
                this.autorun_value = 100;
                this.autorun_complete_count = this.autorun_target_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                TimeSpan span = ENDDATE - STARTDATE;
                double progress = 0;

                Monitering monitering = new Monitering();
                monitering.AddAll();

                for (int i = 0; i <= span.TotalDays; i++)
                {
                    monitering.Start(STARTDATE.AddDays(i), MONITOR_TYPE.LEAKAGE);
                    //monitering.WriteWarning();
                    progress += 100.0 / (span.TotalDays + 1.0);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }
    }
}
