﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using WaterNETServer.IF_RWIS.work;
using System.Threading;
using WaterNETServer.IF_iwater.dao;
using System.Collections;
using System.Data;
using EMFrame.log;
using System.Windows.Forms;

namespace WaterNETServer.ScheduleManage.schedule
{
    public class RecordPasttimeScheduleHour : BaseSchedule
    {
        public RecordPasttimeScheduleHour(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                int target_count = 0;
                int complete_count = 0;
                IList<string> tagList = null;

                DateTime STARTDATE = TARGETDATE.AddMinutes(-600);
                DateTime ENDDATE = TARGETDATE;

                TimeSpan SPAN = ENDDATE - STARTDATE;
                double progress = 0;

                for (int i = 0; i <= SPAN.TotalHours; i++)
                {
                    tagList = WaterNET.GetInstance().SelectMissingHourTagListAll(STARTDATE.AddHours(i));
                    target_count += tagList.Count;

                    if (tagList.Count > 0)
                    {
                        complete_count += WaterNET.GetInstance().InsertAccumulationHourData(tagList, STARTDATE.AddHours(i));
                    }

                    progress += 100.0 / (SPAN.TotalHours + 1.0);
                    this.autorun_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.AUTORUN);
                    Thread.Sleep(500);
                }

                this.autorun_target_count = target_count;
                this.autorun_complete_count = complete_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                IList<string> tagList = null;

                TimeSpan SPAN = ENDDATE - STARTDATE;
                double progress = 0;

                for (int i = 0; i <= SPAN.TotalHours; i++)
                {
                    if (!"run".Equals(this.manual_state.ToString()))
                    {
                        throw new Exception("사용자가 작업을 중단 했습니다.");
                    }

                    tagList = WaterNET.GetInstance().SelectMissingHourTagListAll(STARTDATE.AddHours(i));

                    if (tagList.Count > 0)
                    {
                        WaterNET.GetInstance().InsertAccumulationHourData(tagList, STARTDATE.AddHours(i));
                    }

                    progress += 100.0 / (SPAN.TotalHours + 1.0);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                    Thread.Sleep(1);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }

        /// <summary>
        /// 실시간 데이터 수집 누락 자동 보정(재수집)
        /// </summary>
        private void autoModify_GatherRealtime(string code)
        {
            try
            {
                OracleWork oWork = new OracleWork();
                OleDBWork hWork = new OleDBWork();
                Common.utils.Common utils = new WaterNETServer.Common.utils.Common();

                Hashtable param = new Hashtable();
                Hashtable warnHashtable = new Hashtable();

                string missingTableName = "MissingRawDatas";
                string hTableName1 = "RealTimeTags";
                string hTableName2 = "RealTimeRawDatas";

                // Oracle에서 실시간 태그 목록을 조회
                // cof(계수)값을 실시간 값에 곱해줘야하기때문에 실시간 태그 목록 조회가 필요함
                DataSet result1 = oWork.selectRealtimeTags(hTableName1);

                // 누락된 데이터 조회 (태그+시간)
                // 10분전 이전의 모든 데이터 조회
                DataSet missingResult = oWork.selectMissingRawData(missingTableName, code, -10);

                DataTable list = missingResult.Tables[missingTableName];
                foreach (DataRow row in list.Rows)
                {
                    // 히스토리안 날짜조건 만듬
                    param = utils.GetTimestampHistorianCondition(Convert.ToDateTime(row[1].ToString()), 0);
                    // 히스토리안에서 데이터를 수집
                    DataSet result2 = hWork.selectMissingData(missingTableName, row[0].ToString(), param);
                    // 수집된 데이터 입력
                    if (oWork.insertRawData(result1, result2, hTableName2) > 0)
                    {
                        // 미수집 기록에 수집체크
                        if (oWork.UpdateMissingData(row[0].ToString(), row[1].ToString()) > 0)
                        {
                            // 시간(키) + 카운트(수정횟수)를 Hashtable에 기록
                            // 경고메시지에 몇시 경고를 몇회 수정했는지 기록 하기 위함
                            if (warnHashtable.ContainsKey(row[1].ToString()))
                            {
                                // 기록된 시간이 있으면 카운트를 받아 1증가
                                int cnt = Convert.ToInt32(warnHashtable[row[1].ToString()]);
                                warnHashtable.Remove(row[1].ToString());
                                warnHashtable.Add(row[1].ToString(), cnt + 1);
                            }
                            else
                            {
                                // 처음 기록되는 시간이면 카운트를 1기록
                                warnHashtable.Add(row[1].ToString(), 1);
                            }
                        }
                    }

                    // 000001	실시간 수집 오류 누락(Historian데이터 누락)
                    if ("000001".Equals(code))
                    {
                        // 경고메시지 업데이트
                        // 기록된 시간별 카운트
                        foreach (DictionaryEntry de in warnHashtable)
                        {
                            oWork.UpdateWarning(Convert.ToDateTime(de.Key.ToString()), Convert.ToInt32(de.Value.ToString()));
                        }
                    }

                    // 000002	실시간 수집 오류 누락(타임 싱크 오류)
                    if ("000002".Equals(code))
                    {
                        // 이건 업데이트 할 방법이 없음 warning 관리가 공통으로 사용하는것이라 시간 범위 지정을 할 수가 없음
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }   
    }
}
