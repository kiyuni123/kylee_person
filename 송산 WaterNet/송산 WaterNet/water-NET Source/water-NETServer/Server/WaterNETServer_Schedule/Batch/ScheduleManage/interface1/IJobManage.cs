﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNETServer.ScheduleManage.interface1
{
    /// <summary>
    /// 스케줄 관리 데이터 형식 인터페이스
    /// </summary>
    public interface IJobManage
    {
        object SCHEDULE_NAME
        {
            get;
        }

        object SCHEDULE_KEY
        {
            get;
        }

        object MANUAL_STARTDATE
        {
            get;
            set;
        }

        object MANUAL_ENDDATE
        {
            get;
            set;
        }

        object MANUAL_STATE
        {
            get;
            set;
        }

        object MANUAL_VALUE
        {
            get;
        }

        object MANUAL_TARGET
        {
            get;
            set;
        }
    }
}
