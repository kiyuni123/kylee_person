﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading;
using EMFrame.log;
using System.Collections;
using System.Data;
using WaterNETServer.IF_iwater.dao;
using WaterNETServer.ScheduleManage.interface1;
using System.Windows.Forms;
using WaterNETServer.IF_RWIS.work;
using WaterNETServer.Common;



/// 금일적산 (1분)
/// 수동보정 없음

namespace WaterNETServer.ScheduleManage.schedule
{
    public class AccumulationToDaySchedule : BaseSchedule
    {
        string[] kinds = WaterNET.GetInstance().SelectTagGbn(AppStatic.DAYCODE);

        public AccumulationToDaySchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                this.autorun_target_count = WaterNET.GetInstance().SelectTagList(this.kinds).Count;
                WaterNET.GetInstance().InsertAccumulationToDayData(TARGETDATE.AddMinutes(-4));
                this.autorun_value = 100;
                this.autorun_complete_count = autorun_target_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return false;
        }
    }
}
