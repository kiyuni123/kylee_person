﻿namespace WaterNETServer.ScheduleManage.form
{
    partial class frmScheduleManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groScheduleMonitoring = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel3 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.griScheduleAuto = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraSplitter2 = new Infragistics.Win.Misc.UltraSplitter();
            this.groScheduleLog = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel2 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.butScheduleTrend = new System.Windows.Forms.Button();
            this.butScheduleAllEnd = new System.Windows.Forms.Button();
            this.butScheduleAllStart = new System.Windows.Forms.Button();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.groScheduleManage = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.griScheduleManual = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.texScheduleLog = new System.Windows.Forms.RichTextBox();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groScheduleMonitoring)).BeginInit();
            this.groScheduleMonitoring.SuspendLayout();
            this.ultraExpandableGroupBoxPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griScheduleAuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groScheduleLog)).BeginInit();
            this.groScheduleLog.SuspendLayout();
            this.ultraExpandableGroupBoxPanel2.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groScheduleManage)).BeginInit();
            this.groScheduleManage.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griScheduleManual)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel17.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(908, 10);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 744);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(908, 10);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 734);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(898, 10);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(10, 734);
            this.panel4.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.groScheduleMonitoring);
            this.panel6.Controls.Add(this.ultraSplitter2);
            this.panel6.Controls.Add(this.groScheduleLog);
            this.panel6.Controls.Add(this.panel12);
            this.panel6.Controls.Add(this.ultraSplitter1);
            this.panel6.Controls.Add(this.groScheduleManage);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(10, 10);
            this.panel6.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(888, 734);
            this.panel6.TabIndex = 1;
            // 
            // groScheduleMonitoring
            // 
            this.groScheduleMonitoring.Controls.Add(this.ultraExpandableGroupBoxPanel3);
            this.groScheduleMonitoring.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groScheduleMonitoring.ExpandedSize = new System.Drawing.Size(888, 324);
            this.groScheduleMonitoring.Location = new System.Drawing.Point(0, 269);
            this.groScheduleMonitoring.Name = "groScheduleMonitoring";
            this.groScheduleMonitoring.Size = new System.Drawing.Size(888, 324);
            this.groScheduleMonitoring.TabIndex = 5;
            this.groScheduleMonitoring.Text = "데이터 수집";
            // 
            // ultraExpandableGroupBoxPanel3
            // 
            this.ultraExpandableGroupBoxPanel3.Controls.Add(this.griScheduleAuto);
            this.ultraExpandableGroupBoxPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel3.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel3.Name = "ultraExpandableGroupBoxPanel3";
            this.ultraExpandableGroupBoxPanel3.Size = new System.Drawing.Size(882, 302);
            this.ultraExpandableGroupBoxPanel3.TabIndex = 0;
            // 
            // griScheduleAuto
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.griScheduleAuto.DisplayLayout.Appearance = appearance13;
            this.griScheduleAuto.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griScheduleAuto.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.griScheduleAuto.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griScheduleAuto.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.griScheduleAuto.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griScheduleAuto.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.griScheduleAuto.DisplayLayout.MaxColScrollRegions = 1;
            this.griScheduleAuto.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.griScheduleAuto.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.griScheduleAuto.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.griScheduleAuto.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.griScheduleAuto.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.griScheduleAuto.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.griScheduleAuto.DisplayLayout.Override.CellAppearance = appearance20;
            this.griScheduleAuto.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.griScheduleAuto.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.griScheduleAuto.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.griScheduleAuto.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.griScheduleAuto.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griScheduleAuto.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.griScheduleAuto.DisplayLayout.Override.RowAppearance = appearance23;
            this.griScheduleAuto.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.griScheduleAuto.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.griScheduleAuto.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griScheduleAuto.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griScheduleAuto.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.griScheduleAuto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griScheduleAuto.Location = new System.Drawing.Point(0, 0);
            this.griScheduleAuto.Name = "griScheduleAuto";
            this.griScheduleAuto.Size = new System.Drawing.Size(882, 302);
            this.griScheduleAuto.TabIndex = 1;
            this.griScheduleAuto.Text = "ultraGrid1";
            // 
            // ultraSplitter2
            // 
            appearance39.BackColor = System.Drawing.SystemColors.Control;
            appearance39.BackColor2 = System.Drawing.SystemColors.Control;
            appearance39.BorderColor = System.Drawing.SystemColors.Control;
            appearance39.BorderColor2 = System.Drawing.SystemColors.Control;
            this.ultraSplitter2.Appearance = appearance39;
            this.ultraSplitter2.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter2.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            appearance41.BackColor = System.Drawing.Color.LightSteelBlue;
            appearance41.BackColor2 = System.Drawing.Color.LightSteelBlue;
            this.ultraSplitter2.ButtonAppearance = appearance41;
            this.ultraSplitter2.ButtonExtent = 50;
            this.ultraSplitter2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D;
            this.ultraSplitter2.CollapseUIType = Infragistics.Win.Misc.CollapseUIType.None;
            this.ultraSplitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter2.Location = new System.Drawing.Point(0, 593);
            this.ultraSplitter2.MinSize = 0;
            this.ultraSplitter2.Name = "ultraSplitter2";
            this.ultraSplitter2.RestoreExtent = 136;
            this.ultraSplitter2.Size = new System.Drawing.Size(888, 5);
            this.ultraSplitter2.TabIndex = 4;
            // 
            // groScheduleLog
            // 
            this.groScheduleLog.Controls.Add(this.ultraExpandableGroupBoxPanel2);
            this.groScheduleLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groScheduleLog.ExpandedSize = new System.Drawing.Size(888, 136);
            this.groScheduleLog.Location = new System.Drawing.Point(0, 598);
            this.groScheduleLog.Name = "groScheduleLog";
            this.groScheduleLog.Size = new System.Drawing.Size(888, 136);
            this.groScheduleLog.TabIndex = 3;
            this.groScheduleLog.Text = "실행로그";
            // 
            // ultraExpandableGroupBoxPanel2
            // 
            this.ultraExpandableGroupBoxPanel2.Controls.Add(this.texScheduleLog);
            this.ultraExpandableGroupBoxPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel2.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel2.Name = "ultraExpandableGroupBoxPanel2";
            this.ultraExpandableGroupBoxPanel2.Size = new System.Drawing.Size(882, 114);
            this.ultraExpandableGroupBoxPanel2.TabIndex = 0;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.butScheduleTrend);
            this.panel12.Controls.Add(this.butScheduleAllEnd);
            this.panel12.Controls.Add(this.butScheduleAllStart);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 244);
            this.panel12.Margin = new System.Windows.Forms.Padding(0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(888, 25);
            this.panel12.TabIndex = 0;
            // 
            // butScheduleTrend
            // 
            this.butScheduleTrend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butScheduleTrend.Location = new System.Drawing.Point(620, 1);
            this.butScheduleTrend.Name = "butScheduleTrend";
            this.butScheduleTrend.Size = new System.Drawing.Size(118, 23);
            this.butScheduleTrend.TabIndex = 4;
            this.butScheduleTrend.Text = "실시간 트랜드 조회";
            this.butScheduleTrend.UseVisualStyleBackColor = true;
            // 
            // butScheduleAllEnd
            // 
            this.butScheduleAllEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butScheduleAllEnd.Location = new System.Drawing.Point(819, 1);
            this.butScheduleAllEnd.Name = "butScheduleAllEnd";
            this.butScheduleAllEnd.Size = new System.Drawing.Size(69, 23);
            this.butScheduleAllEnd.TabIndex = 2;
            this.butScheduleAllEnd.Text = "일괄종료";
            this.butScheduleAllEnd.UseVisualStyleBackColor = true;
            // 
            // butScheduleAllStart
            // 
            this.butScheduleAllStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butScheduleAllStart.Location = new System.Drawing.Point(744, 1);
            this.butScheduleAllStart.Name = "butScheduleAllStart";
            this.butScheduleAllStart.Size = new System.Drawing.Size(69, 23);
            this.butScheduleAllStart.TabIndex = 3;
            this.butScheduleAllStart.Text = "일괄시작";
            this.butScheduleAllStart.UseVisualStyleBackColor = true;
            // 
            // ultraSplitter1
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Control;
            appearance37.BackColor2 = System.Drawing.SystemColors.Control;
            appearance37.BorderColor = System.Drawing.SystemColors.Control;
            appearance37.BorderColor2 = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Appearance = appearance37;
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            appearance40.BackColor = System.Drawing.Color.LightSteelBlue;
            appearance40.BackColor2 = System.Drawing.Color.LightSteelBlue;
            this.ultraSplitter1.ButtonAppearance = appearance40;
            this.ultraSplitter1.ButtonExtent = 50;
            this.ultraSplitter1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D;
            this.ultraSplitter1.CollapseUIType = Infragistics.Win.Misc.CollapseUIType.None;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 239);
            this.ultraSplitter1.MinSize = 0;
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 146;
            this.ultraSplitter1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ultraSplitter1.Size = new System.Drawing.Size(888, 5);
            this.ultraSplitter1.TabIndex = 2;
            // 
            // groScheduleManage
            // 
            this.groScheduleManage.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.groScheduleManage.Dock = System.Windows.Forms.DockStyle.Top;
            this.groScheduleManage.ExpandedSize = new System.Drawing.Size(888, 239);
            this.groScheduleManage.Location = new System.Drawing.Point(0, 0);
            this.groScheduleManage.Name = "groScheduleManage";
            this.groScheduleManage.Size = new System.Drawing.Size(888, 239);
            this.groScheduleManage.TabIndex = 1;
            this.groScheduleManage.Text = "데이터 보정";
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.griScheduleManual);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(882, 217);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // griScheduleManual
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.griScheduleManual.DisplayLayout.Appearance = appearance25;
            this.griScheduleManual.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griScheduleManual.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.griScheduleManual.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griScheduleManual.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.griScheduleManual.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griScheduleManual.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.griScheduleManual.DisplayLayout.MaxColScrollRegions = 1;
            this.griScheduleManual.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.griScheduleManual.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.griScheduleManual.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.griScheduleManual.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.griScheduleManual.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.griScheduleManual.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.griScheduleManual.DisplayLayout.Override.CellAppearance = appearance32;
            this.griScheduleManual.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.griScheduleManual.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.griScheduleManual.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.griScheduleManual.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.griScheduleManual.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griScheduleManual.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.griScheduleManual.DisplayLayout.Override.RowAppearance = appearance35;
            this.griScheduleManual.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.griScheduleManual.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.griScheduleManual.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griScheduleManual.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griScheduleManual.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.griScheduleManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griScheduleManual.Location = new System.Drawing.Point(0, 0);
            this.griScheduleManual.Name = "griScheduleManual";
            this.griScheduleManual.Size = new System.Drawing.Size(882, 217);
            this.griScheduleManual.TabIndex = 2;
            this.griScheduleManual.Text = "ultraGrid2";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.panel17, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(0, 30);
            this.panel17.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(200, 70);
            this.panel17.TabIndex = 1;
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(263, 70);
            this.panel18.TabIndex = 0;
            // 
            // panel19
            // 
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Margin = new System.Windows.Forms.Padding(0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(200, 150);
            this.panel19.TabIndex = 0;
            // 
            // texScheduleLog
            // 
            this.texScheduleLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.texScheduleLog.Location = new System.Drawing.Point(0, 0);
            this.texScheduleLog.Name = "texScheduleLog";
            this.texScheduleLog.Size = new System.Drawing.Size(882, 114);
            this.texScheduleLog.TabIndex = 0;
            this.texScheduleLog.Text = "";
            // 
            // frmScheduleManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 754);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmScheduleManage";
            this.Text = "frmScheduleManage";
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groScheduleMonitoring)).EndInit();
            this.groScheduleMonitoring.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griScheduleAuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groScheduleLog)).EndInit();
            this.groScheduleLog.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel2.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groScheduleManage)).EndInit();
            this.groScheduleManage.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griScheduleManual)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel12;
        private Infragistics.Win.UltraWinGrid.UltraGrid griScheduleAuto;
        private System.Windows.Forms.Button butScheduleAllStart;
        private System.Windows.Forms.Button butScheduleAllEnd;
        private Infragistics.Win.UltraWinGrid.UltraGrid griScheduleManual;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        //private System.Windows.Forms.Label label2;
        //private System.Windows.Forms.Label labSystemTime;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox groScheduleManage;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox groScheduleMonitoring;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel3;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter2;
        private Infragistics.Win.Misc.UltraExpandableGroupBox groScheduleLog;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel2;
        private System.Windows.Forms.Button butScheduleTrend;
        private System.Windows.Forms.RichTextBox texScheduleLog;
    }
}