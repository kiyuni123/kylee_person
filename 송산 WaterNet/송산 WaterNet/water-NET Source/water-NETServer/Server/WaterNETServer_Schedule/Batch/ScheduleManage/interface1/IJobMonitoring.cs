﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNETServer.ScheduleManage.interface1
{
    /// <summary>
    /// 스케줄 감시 데이터 형식 인터페이스
    /// </summary>
    public interface IJobMonitoring
    {
        object SCHEDULE_NAME
        {
            get;
        }

        object SCHEDULE_KEY
        {
            get;
        }

        object AUTORUN_LAST_TIME
        {
            get;
        }

        object AUTORUN_TARGET_COUNT
        {
            get;
        }

        object AUTORUN_COMPLETE_COUNT
        {
            get;
        }

        object AUTORUN_INTERVAL
        {
            get;
            set;
        }

        object AUTORUN_TIME
        {
            get;
            set;
        }

        object AUTORUN_STATE
        {
            get;
            set;
        }

        object BATCH_YN
        {
            get;
            set;
        }

        object AUTORUN_VALUE
        {
            get;
        }
    }
}
