﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNETServer.ScheduleManage.schedule;
using EMFrame.log;
using System.Xml;
using System.Windows.Forms;

namespace WaterNETServer.ScheduleManage.manager
{
    public class ScheduleManager
    {
        public static BaseSchedule GetSchedule(XmlElement scheduleConfig)
        {
            BaseSchedule schedule = null;
            
            try
            {
                string clazz = "WaterNETServer.ScheduleManage.schedule." + scheduleConfig.GetElementsByTagName("key")[0].InnerText;
                System.Type p = System.Type.GetType(clazz);
                schedule = Activator.CreateInstance(p, scheduleConfig) as BaseSchedule;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                MessageBox.Show("스케줄 정보를 불러오는데 실패했습니다.");
            }

            return schedule;
        }
    }
}
