﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNETServer.Common.utils;
using System.Xml;
using IF_WaterINFOS.IF;
using IF_WaterINFOS.dao;
using System.Data;
using System.Threading;
using EMFrame.log;
using WaterNETServer.ScheduleManage.interface1;
using System.Windows.Forms;



/// 수용가정보

namespace WaterNETServer.ScheduleManage.schedule
{
    public class DMINFOSchedule : BaseSchedule
    {
        private string _strSVCKEY = string.Empty;  // 서비스코드
        private string _strSGCCD = string.Empty;   // 지자체코드
        private string _regmngrip = string.Empty;   // 데이터 받는 IP (Water-NET) 

        GlobalVariable gVar = new GlobalVariable();

        public DMINFOSchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
            this._strSVCKEY = gVar.SVCKEY;//.get_strSVCKEY();
            this._strSGCCD = gVar.SGCCD;//.getSgccdCode();
            this._regmngrip = gVar.IPADDR;//.getRegmngrip();
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                GetInfosWebService infos = new GetInfosWebService();
                InfosWork infosWork = new InfosWork();

                DateTime if_date = DateTime.Now;

                // Water-INFOS에서 수용가정보 받아오기
                DataSet dSet = infos.GetDMINFO
                    (
                    this._strSVCKEY,
                    this._regmngrip, 
                    this._strSGCCD,
                    string.Empty,
                    DateTime.MinValue.ToString("yyyyMMdd")
                    );

                this.autorun_target_count = dSet.Tables[0].Rows.Count;

                // IF_DMINFO에 수용가 정보 넣기
                infosWork.GetDMINFO(dSet, if_date);

                // WI_DMINFO에 수용가 정보 넣기 (대/중/소 블록을 매핑 코드로 변경 후)
                infosWork.MdfyDMINFO(if_date.ToString("yyyyMMddHHmmss"));

                //infosWork.MdfyDMINFO2(dtStr);

                // IF_DMINFO의 2일전 데이터 삭제 (임시테이블 데이터 보관 불필요)
                infosWork.DeleteDMINFO(if_date.ToString("yyyyMMdd"));

                this.autorun_value = 100;
                this.autorun_complete_count = this.autorun_target_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                EMFrame.log.Logger.Error(ex.ToString());
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                GetInfosWebService infos = new GetInfosWebService();
                InfosWork infosWork = new InfosWork();
                Common.utils.Common utils = new Common.utils.Common();
                TimeSpan span = ENDDATE - STARTDATE;
                double progress = 0;

                for (int i = 0; i <= span.TotalDays; i++)
                {
                    DataSet dSet = infos.GetDMINFO
                        (
                        this._strSVCKEY,
                        this._regmngrip,
                        this._strSGCCD,
                        string.Empty,
                        DateTime.MinValue.ToString("yyyyMMdd")
                        );

                    DateTime if_date = DateTime.Now;
                    infosWork.GetDMINFO(dSet, if_date);
                    infosWork.MdfyDMINFO(if_date.ToString("yyyyMMddHHmmss"));
                    progress += 100.0 / (span.TotalDays + 1.0);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                }

                infosWork.DeleteDMINFO(DateTime.Today.ToString("yyyyMMdd"));
            }
            catch (Exception ex)
            {
                EMFrame.log.Logger.Error(ex.ToString());
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }
    }
}
