﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections;
using WaterNETServer.Common;
using WaterNETServer.ThreadManager.runThread;
using EMFrame.log;

namespace WaterNETServer.ThreadManager.timer
{
    public class WvTimer
    {
        private Hashtable _threadPool = null;

        WvRunThread tExecute = null;
        WaterNETServer.Common.utils.Common utils = new WaterNETServer.Common.utils.Common();
        WaterNETServer.Common.utils.Configuration conf = new WaterNETServer.Common.utils.Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
        string _path = "/Root/schedule/job";

        public WvTimer(){}
        public WvTimer(Hashtable threadPool){this._threadPool = threadPool;}

#region 화면 제어 부분 #######################################
        /// <summary>
        /// 쓰레드 상태 표시 버튼
        /// </summary>
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Button _button4;

        public void setButton1(Button button) { this._button1 = button; }
        public void setButton2(Button button) { this._button2 = button; }
        public void setButton3(Button button) { this._button3 = button; }
        public void setButton4(Button button) { this._button4 = button; }

        /// <summary>
        /// 쓰레드 상태 표시 프로그래스바
        /// </summary>
        private ProgressBar _progressbar1;
        private ProgressBar _progressbar2;
        private ProgressBar _progressbar3;

        public void setProgressBar1(ProgressBar progressbar) { this._progressbar1 = progressbar; }
        public void setProgressBar2(ProgressBar progressbar) { this._progressbar2 = progressbar; }
        public void setProgressBar3(ProgressBar progressbar) { this._progressbar3 = progressbar; }
#endregion ##########################################


#region 수량관련 배치 프로그램 #######################################

        #region 야간최소유량
        private System.Timers.Timer timer_MinimumNightFlow = null;
        private Thread thread_MinimumNightFlow = null;
        private Hashtable hash_MinimumNightFlow = null;
        string time_MinimumNightFlow = string.Empty;
        string type_MinimumNightFlow = string.Empty;
        public void MinimumNightFlow_ThreadStart()
        {
            try
            {
                hash_MinimumNightFlow = conf.getSchedule(_path, "MinimumNightFlow_Execute");
                time_MinimumNightFlow = hash_MinimumNightFlow["time"].ToString();
                type_MinimumNightFlow = hash_MinimumNightFlow["type"].ToString();
                thread_MinimumNightFlow = new Thread(new ThreadStart(MinimumNightFlow_Timer));
                thread_MinimumNightFlow.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void MinimumNightFlow_Timer()
        {
            try
            {
                timer_MinimumNightFlow = new System.Timers.Timer();
                timer_MinimumNightFlow.Interval = 1000;
                timer_MinimumNightFlow.Elapsed += new ElapsedEventHandler(MinimumNightFlow_Execute);
                timer_MinimumNightFlow.Start();

                if (this._threadPool.ContainsKey(timer_MinimumNightFlow.GetHashCode()))
                {
                    this._threadPool.Remove(timer_MinimumNightFlow.GetHashCode());
                }
                _threadPool.Add(timer_MinimumNightFlow.GetHashCode(), timer_MinimumNightFlow);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer MinimumNightFlow_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_MinimumNightFlow.GetHashCode()];
        }
        public void MinimumNightFlow_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_MinimumNightFlow).Equals((string)(utils.GetTime()[type_MinimumNightFlow])))
                {
                    tExecute = new WvRunThread();
                    tExecute.setButton1(this._button1);
                    tExecute.runThread_MinimumNightFlow();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 야간최소유량 보정
        private System.Timers.Timer timer_MinimumNightFlow_modify = null;
        private Thread thread_MinimumNightFlow_modify = null;

        private string cTime_MinimumNightFlow_modify = string.Empty;
        private string cTime2_MinimumNightFlow_modify = string.Empty;

        public void MinimumNightFlow_modify_ThreadStart(string cTime, string cTime2)
        {
            cTime_MinimumNightFlow_modify = cTime;
            cTime2_MinimumNightFlow_modify = cTime2;
            thread_MinimumNightFlow_modify = new Thread(new ThreadStart(MinimumNightFlow_modify_Timer));
            thread_MinimumNightFlow_modify.Start();
        }

        public void MinimumNightFlow_modify_Timer()
        {
            timer_MinimumNightFlow_modify = new System.Timers.Timer();
            timer_MinimumNightFlow_modify.Interval = 1000;
            timer_MinimumNightFlow_modify.Start();
            timer_MinimumNightFlow_modify.Elapsed += new ElapsedEventHandler(MinimumNightFlow_modify_Execute);

            Thread.Sleep(1500);
            timer_MinimumNightFlow_modify.Stop();
        }

        public void MinimumNightFlow_modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new WvRunThread(_threadPool);
                tExecute.setProgressBar2(this._progressbar2);
                tExecute.runThread_MinimumNightFlow_modify(cTime_MinimumNightFlow_modify, cTime2_MinimumNightFlow_modify);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion ######################################################

        #region 1시간 정시 수압
        private System.Timers.Timer timer_AveragePressure = null;
        private Thread thread_AveragePressure = null;
        private Hashtable hash_AveragePressure = null;
        string time_AveragePressure = string.Empty;
        string type_AveragePressure = string.Empty;
        public void AveragePressure_ThreadStart()
        {
            try
            {
                hash_AveragePressure = conf.getSchedule(_path, "AveragePressure_Execute");
                time_AveragePressure = hash_AveragePressure["time"].ToString();
                type_AveragePressure = hash_AveragePressure["type"].ToString();
                thread_AveragePressure = new Thread(new ThreadStart(AveragePressure_Timer));
                thread_AveragePressure.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void AveragePressure_Timer()
        {
            try
            {
                timer_AveragePressure = new System.Timers.Timer();
                timer_AveragePressure.Interval = 1000;
                timer_AveragePressure.Elapsed += new ElapsedEventHandler(AveragePressure_Execute);
                timer_AveragePressure.Start();

                if (this._threadPool.ContainsKey(timer_AveragePressure.GetHashCode()))
                {
                    this._threadPool.Remove(timer_AveragePressure.GetHashCode());
                }
                _threadPool.Add(timer_AveragePressure.GetHashCode(), timer_AveragePressure);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer AveragePressure_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_AveragePressure.GetHashCode()];
        }
        public void AveragePressure_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_AveragePressure).Equals((string)(utils.GetTime()[type_AveragePressure])))
                {
                    tExecute = new WvRunThread();
                    tExecute.setButton2(this._button2);
                    tExecute.runThread_AveragePressure();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 1시간 정시 수압 보정
        private System.Timers.Timer timer_AveragePressure_Modify = null;
        private Thread thread_AveragePressure_Modify = null;

        private string cTime_AveragePressure_Modify = string.Empty;
        private string cTime2_AveragePressure_Modify = string.Empty;

        public void AveragePressure_Modify_ThreadStart(string cTime, string cTime2)
        {
            cTime_AveragePressure_Modify = cTime;
            cTime2_AveragePressure_Modify = cTime2;
            thread_AveragePressure_Modify = new Thread(new ThreadStart(AveragePressure_Modify_Timer));
            thread_AveragePressure_Modify.Start();
        }

        public void AveragePressure_Modify_Timer()
        {
            timer_AveragePressure_Modify = new System.Timers.Timer();
            timer_AveragePressure_Modify.Interval = 1000;
            timer_AveragePressure_Modify.Start();
            timer_AveragePressure_Modify.Elapsed += new ElapsedEventHandler(AveragePressure_Modify_Execute);

            Thread.Sleep(1500);
            timer_AveragePressure_Modify.Stop();
        }

        public void AveragePressure_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new WvRunThread(_threadPool);
                tExecute.setProgressBar3(this._progressbar3);
                tExecute.runThread_AveragePressure_Modify(cTime_AveragePressure_Modify, cTime2_AveragePressure_Modify);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion ########################################################

        #region 누수감시
        private System.Timers.Timer timer_MonitoringLeakage = null;
        private Thread thread_MonitoringLeakage = null;
        private Hashtable hash_MonitoringLeakage = null;
        string time_MonitoringLeakage = string.Empty;
        string type_MonitoringLeakage = string.Empty;
        public void MonitoringLeakage_ThreadStart()
        {
            try
            {
                hash_MonitoringLeakage = conf.getSchedule(_path, "MonitoringLeakage_Execute");
                time_MonitoringLeakage = hash_MonitoringLeakage["time"].ToString();
                type_MonitoringLeakage = hash_MonitoringLeakage["type"].ToString();
                thread_MonitoringLeakage = new Thread(new ThreadStart(MonitoringLeakage_Timer));
                thread_MonitoringLeakage.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void MonitoringLeakage_Timer()
        {
            try
            {
                timer_MonitoringLeakage = new System.Timers.Timer();
                timer_MonitoringLeakage.Interval = 1000;
                timer_MonitoringLeakage.Elapsed += new ElapsedEventHandler(MonitoringLeakage_Execute);
                timer_MonitoringLeakage.Start();

                if (this._threadPool.ContainsKey(timer_MonitoringLeakage.GetHashCode()))
                {
                    this._threadPool.Remove(timer_MonitoringLeakage.GetHashCode());
                }
                _threadPool.Add(timer_MonitoringLeakage.GetHashCode(), timer_MonitoringLeakage);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer MonitoringLeakage_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_MonitoringLeakage.GetHashCode()];
        }
        public void MonitoringLeakage_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_MonitoringLeakage).Equals((string)(utils.GetTime()[type_MonitoringLeakage])))
                {
                    tExecute = new WvRunThread();
                    tExecute.setButton3(this._button3);
                    tExecute.runThread_MonitoringLeakage();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        #endregion ######################################################

        #region 누수감시 보정
        private System.Timers.Timer timer_MonitoringLeakage_Modify = null;
        private Thread thread_MonitoringLeakage_Modify = null;

        private string cTime_MonitoringLeakage_Modify = string.Empty;
        private string cTime2_MonitoringLeakage_Modify = string.Empty;

        public void MonitoringLeakage_Modify_ThreadStart(string cTime, string cTime2)
        {
            cTime_MonitoringLeakage_Modify = cTime;
            cTime2_MonitoringLeakage_Modify = cTime2;
            thread_MonitoringLeakage_Modify = new Thread(new ThreadStart(MonitoringLeakage_Modify_Timer));
            thread_MonitoringLeakage_Modify.Start();
        }

        public void MonitoringLeakage_Modify_Timer()
        {
            timer_MonitoringLeakage_Modify = new System.Timers.Timer();
            timer_MonitoringLeakage_Modify.Interval = 1000;
            timer_MonitoringLeakage_Modify.Start();
            timer_MonitoringLeakage_Modify.Elapsed += new ElapsedEventHandler(MonitoringLeakage_Modify_Execute);

            Thread.Sleep(1500);
            timer_MonitoringLeakage_Modify.Stop();
        }

        public void MonitoringLeakage_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new WvRunThread(_threadPool);
                tExecute.setProgressBar1(this._progressbar1);
                tExecute.runThread_MonitoringLeakage_Modify(cTime_MonitoringLeakage_Modify, cTime2_MonitoringLeakage_Modify);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

#endregion
    }
}
