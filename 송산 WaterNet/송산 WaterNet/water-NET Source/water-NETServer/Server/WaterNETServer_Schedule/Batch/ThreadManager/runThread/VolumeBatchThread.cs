﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using WaterNETServer.BatchJobs.dao;
using EMFrame.log;

namespace WaterNETServer.ThreadManager.runThread
{
    public class VolumeBatchThread
    {
        private Hashtable _threadPool = null;

        WaterNETServer.Common.utils.Common utils = new WaterNETServer.Common.utils.Common();

        public VolumeBatchThread() { }
        public VolumeBatchThread(Hashtable threadPool)
        {
            this._threadPool = threadPool;
        }


#region 화면 제어 부분 ###########################################
        #region 버튼선언
        /// <summary>
        /// 쓰레드 상태 표시 버튼
        /// </summary>
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Button _button4;
        private Button _button5;

        public void setButton1(Button button) { this._button1 = button; }
        public void setButton2(Button button) { this._button2 = button; }
        public void setButton3(Button button) { this._button3 = button; }
        public void setButton4(Button button) { this._button4 = button; }
        public void setButton5(Button button) { this._button5 = button; }
        #endregion

        #region 프로그래스바선언
        /// <summary>
        /// 쓰레드 상태 표시 프로그래스바
        /// </summary>
        private ProgressBar _progressbar_DM_Info_Modify;
        private ProgressBar _progressbar_DM_Used_Modify;
        private ProgressBar _progressbar_Pipe_Info_Modify;
        private ProgressBar _progressbar_RevenueRatio_Analysis_Modify;
        private ProgressBar _progressbar_WHHpress_Result_Modify;
        public void setProgressBar_DM_Info_Modify(ProgressBar progressbar) { this._progressbar_DM_Info_Modify = progressbar; }
        public void setProgressBar_DM_Used_Modify(ProgressBar progressbar) { this._progressbar_DM_Used_Modify = progressbar; }
        public void setProgressBar_Pipe_Info_Modify(ProgressBar progressbar) { this._progressbar_Pipe_Info_Modify = progressbar; }
        public void setProgressBar_RevenueRatio_Analysis_Modify(ProgressBar progressbar) { this._progressbar_RevenueRatio_Analysis_Modify = progressbar; }
        public void setProgressBar_WHHpress_Result_Modify(ProgressBar progressbar) { this._progressbar_WHHpress_Result_Modify = progressbar; }
        #endregion

        #region 버튼제어
        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback1(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage1(int i)
        {
            if (_button1.InvokeRequired)
            {
                SetButtonImageCallback1 d = new SetButtonImageCallback1(SetButtonImage1);
                this._button1.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback2(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage2(int i)
        {
            if (_button2.InvokeRequired)
            {
                SetButtonImageCallback2 d = new SetButtonImageCallback2(SetButtonImage2);
                this._button2.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback3(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage3(int i)
        {
            if (_button3.InvokeRequired)
            {
                SetButtonImageCallback3 d = new SetButtonImageCallback3(SetButtonImage3);
                this._button3.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback4(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage4(int i)
        {
            if (_button4.InvokeRequired)
            {
                SetButtonImageCallback4 d = new SetButtonImageCallback4(SetButtonImage4);
                this._button4.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback5(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage5(int i)
        {
            if (_button5.InvokeRequired)
            {
                SetButtonImageCallback4 d = new SetButtonImageCallback4(SetButtonImage5);
                this._button5.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button5.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button5.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button5.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }
        #endregion

        #region 프로그래스바제어
        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_DM_Info_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_DM_Info_Modify(int i)
        {
            if (_progressbar_DM_Info_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DM_Info_Modify d = new SetProgressBarCallback_DM_Info_Modify(SetProgressBar_DM_Info_Modify);
                this._progressbar_DM_Info_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DM_Info_Modify.Value = i;
            }
        }
        public void SetProgressBarMaximun_DM_Info_Modify(int i)
        {
            if (_progressbar_DM_Info_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DM_Info_Modify d = new SetProgressBarCallback_DM_Info_Modify(SetProgressBarMaximun_DM_Info_Modify);
                this._progressbar_DM_Info_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DM_Info_Modify.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_DM_Used_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_DM_Used_Modify(int i)
        {
            if (_progressbar_DM_Used_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DM_Used_Modify d = new SetProgressBarCallback_DM_Used_Modify(SetProgressBar_DM_Used_Modify);
                this._progressbar_DM_Used_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DM_Used_Modify.Value = i;
            }
        }
        public void SetProgressBarMaximun_DM_Used_Modify(int i)
        {
            if (_progressbar_DM_Used_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DM_Used_Modify d = new SetProgressBarCallback_DM_Used_Modify(SetProgressBarMaximun_DM_Used_Modify);
                this._progressbar_DM_Used_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DM_Used_Modify.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_Pipe_Info_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_Pipe_Info_Modify(int i)
        {
            if (_progressbar_Pipe_Info_Modify.InvokeRequired)
            {
                SetProgressBarCallback_Pipe_Info_Modify d = new SetProgressBarCallback_Pipe_Info_Modify(SetProgressBar_Pipe_Info_Modify);
                this._progressbar_Pipe_Info_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_Pipe_Info_Modify.Value = i;
            }
        }
        public void SetProgressBarMaximun_Pipe_Info_Modify(int i)
        {
            if (_progressbar_Pipe_Info_Modify.InvokeRequired)
            {
                SetProgressBarCallback_Pipe_Info_Modify d = new SetProgressBarCallback_Pipe_Info_Modify(SetProgressBarMaximun_Pipe_Info_Modify);
                this._progressbar_Pipe_Info_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_Pipe_Info_Modify.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_RevenueRatio_Analysis_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_RevenueRatio_Analysis_Modify(int i)
        {
            if (_progressbar_RevenueRatio_Analysis_Modify.InvokeRequired)
            {
                SetProgressBarCallback_RevenueRatio_Analysis_Modify d = new SetProgressBarCallback_RevenueRatio_Analysis_Modify(SetProgressBar_RevenueRatio_Analysis_Modify);
                this._progressbar_RevenueRatio_Analysis_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_RevenueRatio_Analysis_Modify.Value = i;
            }
        }
        public void SetProgressBarMaximun_RevenueRatio_Analysis_Modify(int i)
        {
            if (_progressbar_RevenueRatio_Analysis_Modify.InvokeRequired)
            {
                SetProgressBarCallback_RevenueRatio_Analysis_Modify d = new SetProgressBarCallback_RevenueRatio_Analysis_Modify(SetProgressBarMaximun_RevenueRatio_Analysis_Modify);
                this._progressbar_RevenueRatio_Analysis_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_RevenueRatio_Analysis_Modify.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_WHHpress_Result_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_WHHpress_Result_Modify(int i)
        {
            if (_progressbar_WHHpress_Result_Modify.InvokeRequired)
            {
                SetProgressBarCallback_WHHpress_Result_Modify d = new SetProgressBarCallback_WHHpress_Result_Modify(SetProgressBar_WHHpress_Result_Modify);
                this._progressbar_WHHpress_Result_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_WHHpress_Result_Modify.Value = i;
            }
        }
        public void SetProgressBarMaximun_WHHpress_Result_Modify(int i)
        {
            if (_progressbar_WHHpress_Result_Modify.InvokeRequired)
            {
                SetProgressBarCallback_WHHpress_Result_Modify d = new SetProgressBarCallback_WHHpress_Result_Modify(SetProgressBarMaximun_WHHpress_Result_Modify);
                this._progressbar_WHHpress_Result_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_WHHpress_Result_Modify.Maximum = i;
            }
        }
        #endregion

#endregion

        #region 급수전수계산
        /// <summary>
        /// 급수전수계산
        /// </summary>
        public void runThread_DM_Info()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage1(1);

                BatchJobsWork jobs = new BatchJobsWork();

                jobs.WV_BlockDMBatch_Day();

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage1(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 급수전수계산 보정
        internal void runThread_DM_Info_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 급수전수계산 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_DM_Info_Modify"))
            {
                this._threadPool.Remove("runThread_DM_Info_Modify");
            }
            this._threadPool.Add("runThread_DM_Info_Modify", "RUN");

            #endregion

            BatchJobsWork jobs = new BatchJobsWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = (DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null).Subtract(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null)).Days + 1);
                SetProgressBarMaximun_DM_Info_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {
                    SetProgressBar_DM_Info_Modify(i);

                    jobs.WV_BlockDMBatch_Day(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null), DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null));

                    d = d.AddDays(1);
                }
                SetProgressBar_DM_Info_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 급수전수계산 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_DM_Info_Modify"))
            {
                this._threadPool.Remove("runThread_DM_Info_Modify");
            }
            this._threadPool.Add("runThread_DM_Info_Modify", "STOP");

            #endregion
        }
        #endregion 

        #region 블록별사용량계산
        /// <summary>
        /// 블록별사용량계산
        /// </summary>
        public void runThread_DM_Used()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage2(1);

                BatchJobsWork jobs = new BatchJobsWork();

                jobs.WV_BlockDMBatch_Month();

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage2(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 블록별사용량계산 보정
        internal void runThread_DM_Used_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 블록별사용량계산 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_DM_Used_Modify"))
            {
                this._threadPool.Remove("runThread_DM_Used_Modify");
            }
            this._threadPool.Add("runThread_DM_Used_Modify", "RUN");

            #endregion

            BatchJobsWork jobs = new BatchJobsWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = utils.MonthBetween(DateTime.ParseExact(cTime.Substring(0, 6), "yyyyMM", null), DateTime.ParseExact(cTime2.Substring(0, 6), "yyyyMM", null)) + 1;

                SetProgressBarMaximun_DM_Used_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 6), "yyyyMM", null); d <= DateTime.ParseExact(cTime2.Substring(0, 6), "yyyyMM", null); i++)
                {
                    SetProgressBar_DM_Used_Modify(i);

                    jobs.WV_BlockDMBatch_Month(DateTime.ParseExact(cTime.Substring(0, 6), "yyyyMM", null), DateTime.ParseExact(cTime2.Substring(0, 6), "yyyyMM", null));

                    d = d.AddMonths(1);
                }
                SetProgressBar_DM_Used_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 블록별사용량계산 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_DM_Used_Modify"))
            {
                this._threadPool.Remove("runThread_DM_Used_Modify");
            }
            this._threadPool.Add("runThread_DM_Used_Modify", "STOP");

            #endregion
        }
        #endregion 

        #region 관로길이계산
        /// <summary>
        /// 관로길이계산
        /// </summary>
        public void runThread_Pipe_Info()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage3(1);

                BatchJobsWork jobs = new BatchJobsWork();

                jobs.WV_BlockPipeBatch_Day();

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage3(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 관로길이계산 보정
        internal void runThread_Pipe_Info_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 관로길이계산 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_Pipe_Info_Modify"))
            {
                this._threadPool.Remove("runThread_Pipe_Info_Modify");
            }
            this._threadPool.Add("runThread_Pipe_Info_Modify", "RUN");

            #endregion

            BatchJobsWork jobs = new BatchJobsWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = (DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null).Subtract(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null)).Days + 1);
                SetProgressBarMaximun_Pipe_Info_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {
                    SetProgressBar_Pipe_Info_Modify(i);

                    jobs.WV_BlockPipeBatch_Day(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null), DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null));

                    d = d.AddDays(1);
                }
                SetProgressBar_Pipe_Info_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 관로길이계산 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_Pipe_Info_Modify"))
            {
                this._threadPool.Remove("runThread_Pipe_Info_Modify");
            }
            this._threadPool.Add("runThread_Pipe_Info_Modify", "STOP");

            #endregion
        }
        #endregion 

        #region 유수율분석
        /// <summary>
        /// 유수율분석
        /// </summary>
        public void runThread_RevenueRatio_Analysis()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage4(1);

                BatchJobsWork jobs = new BatchJobsWork();

                jobs.WV_BlockRevenueRatioBatch_Month();

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage4(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 유수율분석 보정
        internal void runThread_RevenueRatio_Analysis_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 유수율분석 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_RevenueRatio_Analysis_Modify"))
            {
                this._threadPool.Remove("runThread_RevenueRatio_Analysis_Modify");
            }
            this._threadPool.Add("runThread_RevenueRatio_Analysis_Modify", "RUN");

            #endregion

            BatchJobsWork jobs = new BatchJobsWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = utils.MonthBetween(DateTime.ParseExact(cTime.Substring(0, 6), "yyyyMM", null), DateTime.ParseExact(cTime2.Substring(0, 6), "yyyyMM", null)) + 1;

                SetProgressBarMaximun_RevenueRatio_Analysis_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 6), "yyyyMM", null); d <= DateTime.ParseExact(cTime2.Substring(0, 6), "yyyyMM", null); i++)
                {
                    SetProgressBar_RevenueRatio_Analysis_Modify(i);

                    jobs.WV_BlockRevenueRatioBatch_Month(DateTime.ParseExact(cTime.Substring(0, 6), "yyyyMM", null), DateTime.ParseExact(cTime2.Substring(0, 6), "yyyyMM", null));

                    d = d.AddMonths(1);
                }
                SetProgressBar_RevenueRatio_Analysis_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 유수율분석 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_RevenueRatio_Analysis_Modify"))
            {
                this._threadPool.Remove("runThread_RevenueRatio_Analysis_Modify");
            }
            this._threadPool.Add("runThread_RevenueRatio_Analysis_Modify", "STOP");

            #endregion
        }
        #endregion 

        #region 평균수압지점계산
        /// <summary>
        /// 평균수압지점계산
        /// </summary>
        public void runThread_WHHpress_Result()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage5(1);

                BatchJobsWork jobs = new BatchJobsWork();

                jobs.WV_BlockWHHpresResultBatch_Day();

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage5(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 평균수압지점계산 보정
        internal void runThread_WHHpress_Result_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 평균수압지점계산 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_WHHpress_Result_Modify"))
            {
                this._threadPool.Remove("runThread_WHHpress_Result_Modify");
            }
            this._threadPool.Add("runThread_WHHpress_Result_Modify", "RUN");

            #endregion

            BatchJobsWork jobs = new BatchJobsWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = (DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null).Subtract(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null)).Days + 1);

                SetProgressBarMaximun_WHHpress_Result_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {
                    SetProgressBar_WHHpress_Result_Modify(i);

                    jobs.WV_BlockWHHpresResultBatch_Day(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null), DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null));

                    d = d.AddDays(1);;
                }
                SetProgressBar_WHHpress_Result_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 평균수압지점계산 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_WHHpress_Result_Modify"))
            {
                this._threadPool.Remove("runThread_WHHpress_Result_Modify");
            }
            this._threadPool.Add("runThread_WHHpress_Result_Modify", "STOP");

            #endregion
        }
        #endregion 
    }
}
