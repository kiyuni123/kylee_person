﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNETServer.Common.utils;
using IF_WaterINFOS.dao;
using IF_WaterINFOS.IF;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using EMFrame.log;

namespace WaterNETServer.ThreadManager.runThread
{
    public class WaterInfosRunThread
    {
        #region 선언부
        private Hashtable _threadPool = null;

        private string _strSGCCD    = string.Empty;   // 지자체코드
        private string _regmngrip   = string.Empty;   // 데이터 받는 IP (Water-NET) 

        WaterNETServer.Common.utils.Common utils   = new WaterNETServer.Common.utils.Common();
        GlobalVariable gVar         = new GlobalVariable();
        GetInfosWebService infos    = new GetInfosWebService();

        public WaterInfosRunThread() 
        {
            this._strSGCCD = gVar.getSgccdCode();
            this._regmngrip = gVar.getRegmngrip();
         }
        public WaterInfosRunThread(Hashtable threadPool)
        {
            this._strSGCCD = gVar.getSgccdCode();
            this._regmngrip = gVar.getRegmngrip();
            this._threadPool = threadPool;
        }
        #endregion

#region 화면 제어 부분 ###########################################

        #region 버튼 선언
        /// <summary>
        /// 쓰레드 상태 표시 버튼
        /// </summary>
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Button _button4;
        private Button _button5;

        public void setButton1(Button button) { this._button1 = button; }
        public void setButton2(Button button) { this._button2 = button; }
        public void setButton3(Button button) { this._button3 = button; }
        public void setButton4(Button button) { this._button4 = button; }
        public void setButton5(Button button) { this._button5 = button; }
        #endregion

        #region 프로그래스바 선언
        /// <summary>
        /// 쓰레드 상태 표시 프로그래스바
        /// </summary>
        private ProgressBar _progressbar_DMINFO_Modify;
        private ProgressBar _progressbar_DMWSRSRC_Modify;
        private ProgressBar _progressbar_CAINFO_Modify;
        private ProgressBar _progressbar_MTRCHGINFO_Modify;
        private ProgressBar _progressbar_STWCHRG_Modify;
        public void setProgressBar_DMINFO_Modify(ProgressBar progressbar) { this._progressbar_DMINFO_Modify = progressbar; }
        public void setProgressBar_DMWSRSRC_Modify(ProgressBar progressbar) { this._progressbar_DMWSRSRC_Modify = progressbar; }
        public void setProgressBar_CAINFO_Modify(ProgressBar progressbar) { this._progressbar_CAINFO_Modify = progressbar; }
        public void setProgressBar_MTRCHGINFO_Modify(ProgressBar progressbar) { this._progressbar_MTRCHGINFO_Modify = progressbar; }
        public void setProgressBar_STWCHRG_Modify(ProgressBar progressbar) { this._progressbar_STWCHRG_Modify = progressbar; }
        #endregion

        #region 버튼제어
        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback1(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage1(int i)
        {
            if (_button1.InvokeRequired)
            {
                SetButtonImageCallback1 d = new SetButtonImageCallback1(SetButtonImage1);
                this._button1.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback2(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage2(int i)
        {
            if (_button2.InvokeRequired)
            {
                SetButtonImageCallback2 d = new SetButtonImageCallback2(SetButtonImage2);
                this._button2.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback3(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage3(int i)
        {
            if (_button3.InvokeRequired)
            {
                SetButtonImageCallback3 d = new SetButtonImageCallback3(SetButtonImage3);
                this._button3.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback4(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage4(int i)
        {
            if (_button4.InvokeRequired)
            {
                SetButtonImageCallback4 d = new SetButtonImageCallback4(SetButtonImage4);
                this._button4.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback5(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage5(int i)
        {
            if (_button5.InvokeRequired)
            {
                SetButtonImageCallback5 d = new SetButtonImageCallback5(SetButtonImage5);
                this._button5.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button5.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button5.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button5.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }
        #endregion

        #region 프로그래스바제어
        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_DMINFO_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_DMINFO_Modify(int i)
        {
            if (_progressbar_DMINFO_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DMINFO_Modify d = new SetProgressBarCallback_DMINFO_Modify(SetProgressBar_DMINFO_Modify);
                this._progressbar_DMINFO_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DMINFO_Modify.Value = i;
            }
        }
        public void SetProgressBarMaximun_DMINFO_Modify(int i)
        {
            if (_progressbar_DMINFO_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DMINFO_Modify d = new SetProgressBarCallback_DMINFO_Modify(SetProgressBarMaximun_DMINFO_Modify);
                this._progressbar_DMINFO_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DMINFO_Modify.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_DMWSRSRC_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_DMWSRSRC_Modify(int i)
        {
            if (_progressbar_DMWSRSRC_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DMWSRSRC_Modify d = new SetProgressBarCallback_DMWSRSRC_Modify(SetProgressBar_DMWSRSRC_Modify);
                this._progressbar_DMWSRSRC_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DMWSRSRC_Modify.Value = i;
            }
        }

        public void SetProgressBarMaximun_DMWSRSRC_Modify(int i)
        {
            if (_progressbar_DMWSRSRC_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DMWSRSRC_Modify d = new SetProgressBarCallback_DMWSRSRC_Modify(SetProgressBarMaximun_DMWSRSRC_Modify);
                this._progressbar_DMWSRSRC_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DMWSRSRC_Modify.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_CAINFO_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_CAINFO_Modify(int i)
        {
            if (_progressbar_CAINFO_Modify.InvokeRequired)
            {
                SetProgressBarCallback_CAINFO_Modify d = new SetProgressBarCallback_CAINFO_Modify(SetProgressBar_CAINFO_Modify);
                this._progressbar_CAINFO_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_CAINFO_Modify.Value = i;
            }
        }

        public void SetProgressBarMaximun_CAINFO_Modify(int i)
        {
            if (_progressbar_CAINFO_Modify.InvokeRequired)
            {
                SetProgressBarCallback_CAINFO_Modify d = new SetProgressBarCallback_CAINFO_Modify(SetProgressBarMaximun_CAINFO_Modify);
                this._progressbar_CAINFO_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_CAINFO_Modify.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_MTRCHGINFO_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_MTRCHGINFO_Modify(int i)
        {
            if (_progressbar_MTRCHGINFO_Modify.InvokeRequired)
            {
                SetProgressBarCallback_MTRCHGINFO_Modify d = new SetProgressBarCallback_MTRCHGINFO_Modify(SetProgressBar_MTRCHGINFO_Modify);
                this._progressbar_MTRCHGINFO_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_MTRCHGINFO_Modify.Value = i;
            }
        }

        public void SetProgressBarMaximun_MTRCHGINFO_Modify(int i)
        {
            if (_progressbar_MTRCHGINFO_Modify.InvokeRequired)
            {
                SetProgressBarCallback_MTRCHGINFO_Modify d = new SetProgressBarCallback_MTRCHGINFO_Modify(SetProgressBarMaximun_MTRCHGINFO_Modify);
                this._progressbar_MTRCHGINFO_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_MTRCHGINFO_Modify.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_STWCHRG_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_STWCHRG_Modify(int i)
        {
            if (_progressbar_STWCHRG_Modify.InvokeRequired)
            {
                SetProgressBarCallback_STWCHRG_Modify d = new SetProgressBarCallback_STWCHRG_Modify(SetProgressBar_STWCHRG_Modify);
                this._progressbar_STWCHRG_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_STWCHRG_Modify.Value = i;
            }
        }

        public void SetProgressBarMaximun_STWCHRG_Modify(int i)
        {
            if (_progressbar_STWCHRG_Modify.InvokeRequired)
            {
                SetProgressBarCallback_STWCHRG_Modify d = new SetProgressBarCallback_STWCHRG_Modify(SetProgressBarMaximun_STWCHRG_Modify);
                this._progressbar_STWCHRG_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_STWCHRG_Modify.Maximum = i;
            }
        }
        #endregion
#endregion

#region 실행 로직 #########################################
        #region 수용가정보 DMINFO
        /// <summary>
        /// 수용가정보
        /// </summary>
        /// <param name="threadKey"></param>
        public void runThread_DMINFO()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage1(1);

                // I/F 시간
                DateTime dt = DateTime.Now;
                string dtStr = utils.GetTimeStr(dt);

                // Water-INFOS에서 수용가정보 받아오기
                DataSet dSet = infos.GetDMINFO(_regmngrip, _strSGCCD, dtStr.Substring(0, 8), dtStr.Substring(0, 8), string.Empty);

                // IF_DMINFO에 수용가 정보 넣기
                InfosWork infosWork = new InfosWork();
                infosWork.GetDMINFO(dSet, dt);

                // WI_DMINFO에 수용가 정보 넣기 (대/중/소 블록을 매핑 코드로 변경 후)
                infosWork.MdfyDMINFO(dtStr);

                //infosWork.MdfyDMINFO2(dtStr);

                // IF_DMINFO의 2일전 데이터 삭제 (임시테이블 데이터 보관 불필요)
                infosWork.DeleteDMINFO(dtStr.Substring(0, 8));

                // 쓰레드 시작 이미지 표시(파랑)
                SetButtonImage1(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 수용가정보 보정
        internal void runThread_DMINFO_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 수용가정보 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_DMINFO_Modify"))
            {
                this._threadPool.Remove("runThread_DMINFO_Modify");
            }
            this._threadPool.Add("runThread_DMINFO_Modify", "RUN");

            #endregion

            InfosWork infosWork = new InfosWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = (DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null).Subtract(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null)).Days + 1);
                SetProgressBarMaximun_DMINFO_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {

                    SetProgressBar_DMINFO_Modify(i);
                    DataSet dSet = infos.GetDMINFO(_regmngrip, _strSGCCD, d.ToString("yyyyMMdd"), d.ToString("yyyyMMdd"), string.Empty);

                    infosWork.GetDMINFO(dSet, dt);
                    infosWork.MdfyDMINFO(dtStr);
                    //infosWork.MdfyDMINFO2(dtStr);
                    d = d.AddDays(1);
                }

                // for debugging
                //DataSet dSet = infos.GetDMINFO(utils.GetFirstIPv4(), _strSGCCD, "20110401", "20111231", string.Empty);
                //infosWork.GetDMINFO(dSet, dt);
                //infosWork.MdfyDMINFO(dtStr);
                //infosWork.MdfyDMINFO2(dtStr);

                infosWork.DeleteDMINFO(dtStr.Substring(0, 8));
                SetProgressBar_DMINFO_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 수용가정보 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_DMINFO_Modify"))
            {
                this._threadPool.Remove("runThread_DMINFO_Modify");
            }
            this._threadPool.Add("runThread_DMINFO_Modify", "STOP");

            #endregion
        }
        #endregion

        #region 상하수도자원 DMWSRSRC
        /// <summary>
        /// 상하수도자원
        /// </summary>
        /// <param name="threadKey"></param>
        public void runThread_DMWSRSRC()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage2(1);

                // I/F 시간
                DateTime dt = DateTime.Now;
                string dtStr = utils.GetTimeStr(dt);

                // Water-INFOS에서 상하수도자원 받아오기
                DataSet dSet = infos.GetDMWSRSRC(_regmngrip, _strSGCCD, dtStr.Substring(0, 8), dtStr.Substring(0, 8), string.Empty);

                // IF_DMWSRSRCO에 상하수도자원 넣기
                InfosWork infosWork = new InfosWork();
                infosWork.GetDMWSRSRC(dSet, dt);

                // WI_DMWSRSRC에 상하수도자원 넣기
                infosWork.MdfyDMWSRSRC(dtStr);

                //infosWork.MdfyDMWSRSRC2(dtStr);

                // IF_DMWSRSRC의 2일전 데이터 삭제 (임시테이블 데이터 보관 불필요)
                infosWork.DeleteDMWSRSRC(dtStr.Substring(0, 8));

                // 쓰레드 시작 이미지 표시(파랑)
                SetButtonImage2(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 상하수도자원 보정
        internal void runThread_DMWSRSRC_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 상하수도자원 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_DMWSRSRC_Modify"))
            {
                this._threadPool.Remove("runThread_DMWSRSRC_Modify");
            }
            this._threadPool.Add("runThread_DMWSRSRC_Modify", "RUN");

            #endregion

            InfosWork infosWork = new InfosWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = (DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null).Subtract(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null)).Days + 1);
                SetProgressBarMaximun_DMWSRSRC_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {
                    SetProgressBar_DMWSRSRC_Modify(i);
                    DataSet dSet = infos.GetDMWSRSRC(_regmngrip, _strSGCCD, d.ToString("yyyyMMdd"), d.ToString("yyyyMMdd"), string.Empty);

                    infosWork.GetDMWSRSRC(dSet, dt);
                    infosWork.MdfyDMWSRSRC(dtStr);
                    //infosWork.MdfyDMWSRSRC2(dtStr);
                    d = d.AddDays(1);

                }

                infosWork.DeleteDMWSRSRC(dtStr.Substring(0, 8));
                SetProgressBar_DMWSRSRC_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 상하수도자원 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_DMWSRSRC_Modify"))
            {
                this._threadPool.Remove("runThread_DMWSRSRC_Modify");
            }
            this._threadPool.Add("runThread_DMWSRSRC_Modify", "STOP");

            #endregion
        }
        #endregion

        #region 민원기본정보 CAINFO

        /// <summary>
        /// 민원기본정보
        /// </summary>
        /// <param name="threadKey"></param>
        public void runThread_CAINFO()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage3(1);

                // I/F 시간
                DateTime dt = DateTime.Now;
                string dtStr = utils.GetTimeStr(dt);

                // Water-INFOS에서 민원기본정보 받아오기
                DataSet dSet = infos.GetCAINFO(_regmngrip, _strSGCCD, string.Empty, string.Empty, dtStr.Substring(0, 8), dtStr.Substring(0, 8));

                // IF_CAINFO에 민원기본정보 넣기
                InfosWork infosWork = new InfosWork();
                infosWork.GetCAINFO(dSet, dt);

                // WI_CAINFO에 민원기본정보 넣기
                infosWork.MdfyCAINFO(dtStr);

                //infosWork.MdfyCAINFO2(dtStr);

                // IF_CAINFO의 2일전 데이터 삭제 (임시테이블 데이터 보관 불필요)
                infosWork.DeleteCAINFO(dtStr.Substring(0, 8));

                // 쓰레드 시작 이미지 표시(파랑)
                SetButtonImage3(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 민원기본정보 보정
        internal void runThread_CAINFO_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 민원기본정보 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_CAINFO_Modify"))
            {
                this._threadPool.Remove("runThread_CAINFO_Modify");
            }
            this._threadPool.Add("runThread_CAINFO_Modify", "RUN");

            #endregion

            InfosWork infosWork = new InfosWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = (DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null).Subtract(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null)).Days + 1);
                SetProgressBarMaximun_CAINFO_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {

                    SetProgressBar_CAINFO_Modify(i);
                    DataSet dSet = infos.GetCAINFO(_regmngrip, _strSGCCD, string.Empty, string.Empty, d.ToString("yyyyMMdd"), d.ToString("yyyyMMdd"));

                    infosWork.GetCAINFO(dSet, dt);
                    infosWork.MdfyCAINFO(dtStr);
                    //infosWork.MdfyCAINFO2(dtStr);
                    d = d.AddDays(1);

                }
                infosWork.DeleteCAINFO(dtStr.Substring(0, 8));
                SetProgressBar_CAINFO_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 민원기본정보 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_CAINFO_Modify"))
            {
                this._threadPool.Remove("runThread_CAINFO_Modify");
            }
            this._threadPool.Add("runThread_CAINFO_Modify", "STOP");

            #endregion
        }
        #endregion

        #region 계량기교체정보 MTRCHGINFO

        /// <summary>
        /// 계량기교체정보
        /// </summary>
        /// <param name="threadKey"></param>
        public void runThread_MTRCHGINFO()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage4(1);

                // I/F 시간
                DateTime dt = DateTime.Now;
                string dtStr = utils.GetTimeStr(dt);

                // Water-INFOS에서 계량기교체정보 받아오기
                DataSet dSet = infos.GetMTRCHGINFO(_regmngrip, _strSGCCD, string.Empty, string.Empty, dtStr.Substring(0, 8), dtStr.Substring(0, 8));

                // IF_MTRCHGINFO에 계량기교체정보 넣기
                InfosWork infosWork = new InfosWork();
                infosWork.GetMTRCHGINFO(dSet, dt);

                // WI_MTRCHGINFO에 계량기교체정보 넣기
                infosWork.MdfyMTRCHGINFO(dtStr);

                //infosWork.MdfyMTRCHGINFO2(dtStr);

                // IF_MTRCHGINFO의 2일전 데이터 삭제 (임시테이블 데이터 보관 불필요)
                infosWork.DeleteMTRCHGINFO(dtStr.Substring(0, 8));

                // 쓰레드 시작 이미지 표시(파랑)
                SetButtonImage4(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 계량기교체정보 보정
        internal void runThread_MTRCHGINFO_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 계량기교체정보 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_MTRCHGINFO_Modify"))
            {
                this._threadPool.Remove("runThread_MTRCHGINFO_Modify");
            }
            this._threadPool.Add("runThread_MTRCHGINFO_Modify", "RUN");

            #endregion

            InfosWork infosWork = new InfosWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = (DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null).Subtract(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null)).Days + 1);
                SetProgressBarMaximun_MTRCHGINFO_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {
                    SetProgressBar_MTRCHGINFO_Modify(i);
                    DataSet dSet = infos.GetMTRCHGINFO(_regmngrip, _strSGCCD, string.Empty, string.Empty, d.ToString("yyyyMMdd"), d.ToString("yyyyMMdd"));

                    infosWork.GetMTRCHGINFO(dSet, dt);
                    infosWork.MdfyMTRCHGINFO(dtStr);
                    //infosWork.MdfyMTRCHGINFO2(dtStr);
                    d = d.AddDays(1);

                }
                infosWork.DeleteMTRCHGINFO(dtStr.Substring(0, 8));
                SetProgressBar_MTRCHGINFO_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 계량기교체정보 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_MTRCHGINFO_Modify"))
            {
                this._threadPool.Remove("runThread_MTRCHGINFO_Modify");
            }
            this._threadPool.Add("runThread_MTRCHGINFO_Modify", "STOP");

            #endregion
        }
        #endregion

        #region 요금조정 STWCHRG

        /// <summary>
        /// 요금조정
        /// </summary>
        /// <param name="threadKey"></param>
        public void runThread_STWCHRG()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage5(1);

                // I/F 시간
                DateTime dt = DateTime.Now;
                string dtStr = utils.GetTimeStr(dt);

                // 요금 조정 가져올 시작달
                string yfStr = utils.GetTimeStr(dt.AddMonths(-2)).Substring(0, 6);

                // Water-INFOS에서 요금조정 받아오기
                DataSet dSet = infos.GetSTWCHRG(_regmngrip, _strSGCCD, string.Empty, dtStr.Substring(0, 6), dtStr.Substring(0, 8));

                // IF_STWCHRG에 요금조정 넣기
                InfosWork infosWork = new InfosWork();
                infosWork.GetSTWCHRG(dSet, dt);

                // WI_STWCHRG에 요금조정 넣기
                infosWork.MdfySTWCHRG(dtStr);

                //infosWork.MdfySTWCHRG2(dtStr);

                // IF_STWCHRG의 2일전 데이터 삭제 (임시테이블 데이터 보관 불필요)
                infosWork.DeleteSTWCHRG(dtStr.Substring(0, 8));

                // 쓰레드 시작 이미지 표시(파랑)
                SetButtonImage5(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 요금조정 보정
        internal void runThread_STWCHRG_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 민원기본정보 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_STWCHRG_Modify"))
            {
                this._threadPool.Remove("runThread_STWCHRG_Modify");
            }
            this._threadPool.Add("runThread_STWCHRG_Modify", "RUN");

            #endregion

            InfosWork infosWork = new InfosWork();
            DateTime dt = DateTime.Now;
            string dtStr = utils.GetTimeStr(dt);
            try
            {
                int gatherCnt = utils.MonthBetween(DateTime.ParseExact(cTime.Substring(0, 6), "yyyyMM", null), DateTime.ParseExact(cTime2.Substring(0, 6), "yyyyMM", null)) + 1;

                SetProgressBarMaximun_STWCHRG_Modify(gatherCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 6), "yyyyMM", null); d <= DateTime.ParseExact(cTime2.Substring(0, 6), "yyyyMM", null); i++)
                {
                    SetProgressBar_STWCHRG_Modify(i);
                    DataSet dSet = infos.GetSTWCHRG(_regmngrip, _strSGCCD, string.Empty, d.ToString("yyyyMM"), d.ToString("yyyyMM"));

                    infosWork.GetSTWCHRG(dSet, dt);
                    infosWork.MdfySTWCHRG(dtStr);
                    //infosWork.MdfySTWCHRG2(dtStr);

                    d = d.AddMonths(1);

                }

                // for debugging
                //DataSet dSet = infos.GetSTWCHRG(utils.GetFirstIPv4(), _strSGCCD, "201104", "201104", "20110412", "20110412");
                //infosWork.GetSTWCHRG(dSet, dt);
                //infosWork.MdfySTWCHRG(dtStr);
                //infosWork.MdfySTWCHRG2(dtStr);


                infosWork.DeleteSTWCHRG(dtStr.Substring(0, 8));
                SetProgressBar_STWCHRG_Modify(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 민원기본정보 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_STWCHRG_Modify"))
            {
                this._threadPool.Remove("runThread_STWCHRG_Modify");
            }
            this._threadPool.Add("runThread_STWCHRG_Modify", "STOP");

            #endregion
        }
        #endregion
#endregion #########################################

    }
}
