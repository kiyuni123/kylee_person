﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Timers;
using System.Collections;
using WaterNETServer.Common;
using WaterNETServer.ThreadManager.runThread;
using EMFrame.log;

namespace WaterNETServer.ThreadManager.timer
{
    public class VolumeBatchTimer
    {
        VolumeBatchThread tExecute = null;
        WaterNETServer.Common.utils.Common utils = new WaterNETServer.Common.utils.Common();
        WaterNETServer.Common.utils.Configuration conf = new WaterNETServer.Common.utils.Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
        string _path = "/Root/schedule/job";

        private Hashtable _threadPool = null;
        public VolumeBatchTimer(){}

        public VolumeBatchTimer(Hashtable threadPool)
        {
            this._threadPool = threadPool;
        }


#region 화면 제어 부분 #######################################
        /// <summary>
        /// 쓰레드 상태 표시 버튼
        /// </summary>
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Button _button4;
        private Button _button5;

        public void setButton1(Button button) { this._button1 = button; }
        public void setButton2(Button button) { this._button2 = button; }
        public void setButton3(Button button) { this._button3 = button; }
        public void setButton4(Button button) { this._button4 = button; }
        public void setButton5(Button button) { this._button5 = button; }

        /// <summary>
        /// 쓰레드 상태 표시 프로그래스바
        /// </summary>
        private ProgressBar _progressbar_DM_Info_Modify;
        private ProgressBar _progressbar_DM_Used_Modify;
        private ProgressBar _progressbar_Pipe_Info_Modify;
        private ProgressBar _progressbar_RevenueRatio_Analysis_Modify;
        private ProgressBar _progressbar_WHHpress_Result_Modify;
        public void setProgressBar_DM_Info_Modify(ProgressBar progressbar) { this._progressbar_DM_Info_Modify = progressbar; }
        public void setProgressBar_DM_Used_Modify(ProgressBar progressbar) { this._progressbar_DM_Used_Modify = progressbar; }
        public void setProgressBar_Pipe_Info_Modify(ProgressBar progressbar) { this._progressbar_Pipe_Info_Modify = progressbar; }
        public void setProgressBar_RevenueRatio_Analysis_Modify(ProgressBar progressbar) { this._progressbar_RevenueRatio_Analysis_Modify = progressbar; }
        public void setProgressBar_WHHpress_Result_Modify(ProgressBar progressbar) { this._progressbar_WHHpress_Result_Modify = progressbar; }

        #region SetProgressBar_DM_Info_Modify
        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_DM_Info_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_DM_Info_Modify(int i)
        {
            if (_progressbar_DM_Info_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DM_Info_Modify d = new SetProgressBarCallback_DM_Info_Modify(SetProgressBar_DM_Info_Modify);
                this._progressbar_DM_Info_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DM_Info_Modify.Value = i;
            }
        }

        public void SetProgressBarMaximun_DM_Info_Modify(int i)
        {
            if (_progressbar_DM_Info_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DM_Info_Modify d = new SetProgressBarCallback_DM_Info_Modify(SetProgressBarMaximun_DM_Info_Modify);
                this._progressbar_DM_Info_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DM_Info_Modify.Maximum = i;
            }
        }
        #endregion 

        #region SetProgressBar_DM_Used_Modify
        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_DM_Used_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_DM_Used_Modify(int i)
        {
            if (_progressbar_DM_Used_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DM_Used_Modify d = new SetProgressBarCallback_DM_Used_Modify(SetProgressBar_DM_Used_Modify);
                this._progressbar_DM_Used_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DM_Used_Modify.Value = i;
            }
        }

        public void SetProgressBarMaximun_DM_Used_Modify(int i)
        {
            if (_progressbar_DM_Used_Modify.InvokeRequired)
            {
                SetProgressBarCallback_DM_Used_Modify d = new SetProgressBarCallback_DM_Used_Modify(SetProgressBarMaximun_DM_Used_Modify);
                this._progressbar_DM_Used_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_DM_Used_Modify.Maximum = i;
            }
        }
        #endregion 

        #region SetProgressBar_Pipe_Info_Modify
        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_Pipe_Info_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_Pipe_Info_Modify(int i)
        {
            if (_progressbar_Pipe_Info_Modify.InvokeRequired)
            {
                SetProgressBarCallback_Pipe_Info_Modify d = new SetProgressBarCallback_Pipe_Info_Modify(SetProgressBar_Pipe_Info_Modify);
                this._progressbar_Pipe_Info_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_Pipe_Info_Modify.Value = i;
            }
        }

        public void SetProgressBarMaximun_Pipe_Info_Modify(int i)
        {
            if (_progressbar_Pipe_Info_Modify.InvokeRequired)
            {
                SetProgressBarCallback_Pipe_Info_Modify d = new SetProgressBarCallback_Pipe_Info_Modify(SetProgressBarMaximun_Pipe_Info_Modify);
                this._progressbar_Pipe_Info_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_Pipe_Info_Modify.Maximum = i;
            }
        }
        #endregion 

        #region SetProgressBar_RevenueRatio_Analysis_Modify
        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_RevenueRatio_Analysis_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_RevenueRatio_Analysis_Modify(int i)
        {
            if (_progressbar_RevenueRatio_Analysis_Modify.InvokeRequired)
            {
                SetProgressBarCallback_RevenueRatio_Analysis_Modify d = new SetProgressBarCallback_RevenueRatio_Analysis_Modify(SetProgressBar_RevenueRatio_Analysis_Modify);
                this._progressbar_RevenueRatio_Analysis_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_RevenueRatio_Analysis_Modify.Value = i;
            }
        }

        public void SetProgressBarMaximun_RevenueRatio_Analysis_Modify(int i)
        {
            if (_progressbar_RevenueRatio_Analysis_Modify.InvokeRequired)
            {
                SetProgressBarCallback_RevenueRatio_Analysis_Modify d = new SetProgressBarCallback_RevenueRatio_Analysis_Modify(SetProgressBarMaximun_RevenueRatio_Analysis_Modify);
                this._progressbar_RevenueRatio_Analysis_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_RevenueRatio_Analysis_Modify.Maximum = i;
            }
        }
        #endregion 

        #region SetProgressBar_WHHpress_Result_Modify
        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback_WHHpress_Result_Modify(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar_WHHpress_Result_Modify(int i)
        {
            if (_progressbar_WHHpress_Result_Modify.InvokeRequired)
            {
                SetProgressBarCallback_WHHpress_Result_Modify d = new SetProgressBarCallback_WHHpress_Result_Modify(SetProgressBar_WHHpress_Result_Modify);
                this._progressbar_WHHpress_Result_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_WHHpress_Result_Modify.Value = i;
            }
        }

        public void SetProgressBarMaximun_WHHpress_Result_Modify(int i)
        {
            if (_progressbar_WHHpress_Result_Modify.InvokeRequired)
            {
                SetProgressBarCallback_WHHpress_Result_Modify d = new SetProgressBarCallback_WHHpress_Result_Modify(SetProgressBarMaximun_WHHpress_Result_Modify);
                this._progressbar_WHHpress_Result_Modify.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar_WHHpress_Result_Modify.Maximum = i;
            }
        }
        #endregion 
#endregion ##########################################


#region 수량관련 배치 프로그램 #######################################

        #region 급수전수계산
        private System.Timers.Timer timer_DM_Info = null;
        private Thread thread_DM_Info = null;
        private Hashtable hash_DM_Info = null;
        string time_DM_Info = string.Empty;
        string type_DM_Info = string.Empty;
        public void DM_Info_ThreadStart()
        {
            try
            {
                hash_DM_Info = conf.getSchedule(_path, "DM_Info_Execute");
                time_DM_Info = hash_DM_Info["time"].ToString();
                type_DM_Info = hash_DM_Info["type"].ToString();
                thread_DM_Info = new Thread(new ThreadStart(DM_Info_Timer));
                thread_DM_Info.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void DM_Info_Timer()
        {
            try
            {
                timer_DM_Info = new System.Timers.Timer();
                timer_DM_Info.Interval = 1000;
                timer_DM_Info.Elapsed += new ElapsedEventHandler(DM_Info_Execute);
                timer_DM_Info.Start();

                if (this._threadPool.ContainsKey(timer_DM_Info.GetHashCode()))
                {
                    this._threadPool.Remove(timer_DM_Info.GetHashCode());
                }
                _threadPool.Add(timer_DM_Info.GetHashCode(), timer_DM_Info);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer DM_Info_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_DM_Info.GetHashCode()];
        }
        public void DM_Info_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_DM_Info).Equals((string)(utils.GetTime()[type_DM_Info])))
                {
                    tExecute = new VolumeBatchThread();
                    tExecute.setButton1(this._button1);
                    tExecute.runThread_DM_Info();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 급수전수계산 보정
        private System.Timers.Timer timer_DM_Info_Modify = null;
        private Thread thread_DM_Info_Modify = null;

        private string cTime_DM_Info = string.Empty;
        private string cTime2_DM_Info = string.Empty;

        public void DM_Info_Modify_ThreadStart(string cTime, string cTime2)
        {
            this.cTime_DM_Info = cTime;
            this.cTime2_DM_Info = cTime2;
            thread_DM_Info_Modify = new Thread(new ThreadStart(DM_Info_Modify_Timer));
            thread_DM_Info_Modify.Start();
        }

        public void DM_Info_Modify_Timer()
        {
            timer_DM_Info_Modify = new System.Timers.Timer();
            timer_DM_Info_Modify.Interval = 1000;
            timer_DM_Info_Modify.Start();
            timer_DM_Info_Modify.Elapsed += new ElapsedEventHandler(DM_Info_Modify_Execute);

            Thread.Sleep(1500);
            timer_DM_Info_Modify.Stop();
        }

        public void DM_Info_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new VolumeBatchThread(_threadPool);
                tExecute.setProgressBar_DM_Info_Modify(this._progressbar_DM_Info_Modify);
                tExecute.runThread_DM_Info_Modify(cTime_DM_Info, cTime2_DM_Info);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 블록별사용량계산
        private System.Timers.Timer timer_DM_Used = null;
        private Thread thread_DM_Used = null;
        private Hashtable hash_DM_Used = null;
        string time_DM_Used = string.Empty;
        string type_DM_Used = string.Empty;
        public void DM_Used_ThreadStart()
        {
            try
            {
                hash_DM_Used = conf.getSchedule(_path, "DM_Used_Execute");
                time_DM_Used = hash_DM_Used["time"].ToString();
                type_DM_Used = hash_DM_Used["type"].ToString();
                thread_DM_Used = new Thread(new ThreadStart(DM_Used_Timer));
                thread_DM_Used.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void DM_Used_Timer()
        {
            try
            {
                timer_DM_Used = new System.Timers.Timer();
                timer_DM_Used.Interval = 1000;
                timer_DM_Used.Elapsed += new ElapsedEventHandler(DM_Used_Execute);
                timer_DM_Used.Start();

                if (this._threadPool.ContainsKey(timer_DM_Used.GetHashCode()))
                {
                    this._threadPool.Remove(timer_DM_Used.GetHashCode());
                }
                _threadPool.Add(timer_DM_Used.GetHashCode(), timer_DM_Used);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer DM_Used_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_DM_Used.GetHashCode()];
        }
        public void DM_Used_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_DM_Used).Equals((string)(utils.GetTime()[type_DM_Used])))
                {
                    tExecute = new VolumeBatchThread();
                    tExecute.setButton2(this._button2);
                    tExecute.runThread_DM_Used();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 블록별사용량계산 보정
        private System.Timers.Timer timer_DM_Used_Modify = null;
        private Thread thread_DM_Used_Modify = null;

        private string cTime_DM_Used = string.Empty;
        private string cTime2_DM_Used = string.Empty;

        public void DM_Used_Modify_ThreadStart(string cTime, string cTime2)
        {
            this.cTime_DM_Used = cTime;
            this.cTime2_DM_Used = cTime2;
            thread_DM_Used_Modify = new Thread(new ThreadStart(DM_Used_Modify_Timer));
            thread_DM_Used_Modify.Start();
        }

        public void DM_Used_Modify_Timer()
        {
            timer_DM_Used_Modify = new System.Timers.Timer();
            timer_DM_Used_Modify.Interval = 1000;
            timer_DM_Used_Modify.Start();
            timer_DM_Used_Modify.Elapsed += new ElapsedEventHandler(DM_Used_Modify_Execute);

            Thread.Sleep(1500);
            timer_DM_Used_Modify.Stop();
        }

        public void DM_Used_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new VolumeBatchThread(_threadPool);
                tExecute.setProgressBar_DM_Used_Modify(this._progressbar_DM_Used_Modify);
                tExecute.runThread_DM_Used_Modify(cTime_DM_Used, cTime2_DM_Used);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 관로길이계산
        private System.Timers.Timer timer_Pipe_Info = null;
        private Thread thread_Pipe_Info = null;
        private Hashtable hash_Pipe_Info = null;
        string time_Pipe_Info = string.Empty;
        string type_Pipe_Info = string.Empty;
        public void Pipe_Info_ThreadStart()
        {
            try
            {
                hash_Pipe_Info = conf.getSchedule(_path, "Pipe_Info_Execute");
                time_Pipe_Info = hash_Pipe_Info["time"].ToString();
                type_Pipe_Info = hash_Pipe_Info["type"].ToString();
                thread_Pipe_Info = new Thread(new ThreadStart(Pipe_Info_Timer));
                thread_Pipe_Info.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void Pipe_Info_Timer()
        {
            try
            {
                timer_Pipe_Info = new System.Timers.Timer();
                timer_Pipe_Info.Interval = 1000;
                timer_Pipe_Info.Elapsed += new ElapsedEventHandler(Pipe_Info_Execute);
                timer_Pipe_Info.Start();

                if (this._threadPool.ContainsKey(timer_Pipe_Info.GetHashCode()))
                {
                    this._threadPool.Remove(timer_Pipe_Info.GetHashCode());
                }
                _threadPool.Add(timer_Pipe_Info.GetHashCode(), timer_Pipe_Info);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer Pipe_Info_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_Pipe_Info.GetHashCode()];
        }
        public void Pipe_Info_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_Pipe_Info).Equals((string)(utils.GetTime()[type_Pipe_Info])))
                {
                    tExecute = new VolumeBatchThread();
                    tExecute.setButton3(this._button3);
                    tExecute.runThread_Pipe_Info();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 관로길이계산 보정
        private System.Timers.Timer timer_Pipe_Info_Modify = null;
        private Thread thread_Pipe_Info_Modify = null;

        private string cTime_Pipe_Info = string.Empty;
        private string cTime2_Pipe_Info = string.Empty;

        public void Pipe_Info_Modify_ThreadStart(string cTime, string cTime2)
        {
            this.cTime_Pipe_Info = cTime;
            this.cTime2_Pipe_Info = cTime2;
            thread_Pipe_Info_Modify = new Thread(new ThreadStart(Pipe_Info_Modify_Timer));
            thread_Pipe_Info_Modify.Start();
        }

        public void Pipe_Info_Modify_Timer()
        {
            timer_Pipe_Info_Modify = new System.Timers.Timer();
            timer_Pipe_Info_Modify.Interval = 1000;
            timer_Pipe_Info_Modify.Start();
            timer_Pipe_Info_Modify.Elapsed += new ElapsedEventHandler(Pipe_Info_Modify_Execute);

            Thread.Sleep(1500);
            timer_Pipe_Info_Modify.Stop();
        }

        public void Pipe_Info_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new VolumeBatchThread(_threadPool);
                tExecute.setProgressBar_Pipe_Info_Modify(this._progressbar_Pipe_Info_Modify);
                tExecute.runThread_Pipe_Info_Modify(cTime_Pipe_Info, cTime2_Pipe_Info);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 유수율분석
        private System.Timers.Timer timer_RevenueRatio_Analysis = null;
        private Thread thread_RevenueRatio_Analysis = null;
        private Hashtable hash_RevenueRatio_Analysis = null;
        string time_RevenueRatio_Analysis = string.Empty;
        string type_RevenueRatio_Analysis = string.Empty;
        public void RevenueRatio_Analysis_ThreadStart()
        {
            try
            {
                hash_RevenueRatio_Analysis = conf.getSchedule(_path, "RevenueRatio_Analysis_Execute");
                time_RevenueRatio_Analysis = hash_RevenueRatio_Analysis["time"].ToString();
                type_RevenueRatio_Analysis = hash_RevenueRatio_Analysis["type"].ToString();
                thread_RevenueRatio_Analysis = new Thread(new ThreadStart(RevenueRatio_Analysis_Timer));
                thread_RevenueRatio_Analysis.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void RevenueRatio_Analysis_Timer()
        {
            try
            {
                timer_RevenueRatio_Analysis = new System.Timers.Timer();
                timer_RevenueRatio_Analysis.Interval = 1000;
                timer_RevenueRatio_Analysis.Elapsed += new ElapsedEventHandler(RevenueRatio_Analysis_Execute);
                timer_RevenueRatio_Analysis.Start();

                if (this._threadPool.ContainsKey(timer_RevenueRatio_Analysis.GetHashCode()))
                {
                    this._threadPool.Remove(timer_RevenueRatio_Analysis.GetHashCode());
                }
                _threadPool.Add(timer_RevenueRatio_Analysis.GetHashCode(), timer_RevenueRatio_Analysis);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer RevenueRatio_Analysis_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_RevenueRatio_Analysis.GetHashCode()];
        }
        public void RevenueRatio_Analysis_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_RevenueRatio_Analysis).Equals((string)(utils.GetTime()[type_RevenueRatio_Analysis])))
                {
                    tExecute = new VolumeBatchThread();
                    tExecute.setButton4(this._button4);
                    tExecute.runThread_RevenueRatio_Analysis();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 유수율분석 보정
        private System.Timers.Timer timer_RevenueRatio_Analysis_Modify = null;
        private Thread thread_RevenueRatio_Analysis_Modify = null;

        private string cTime_RevenueRatio_Analysis = string.Empty;
        private string cTime2_RevenueRatio_Analysis = string.Empty;

        public void RevenueRatio_Analysis_Modify_ThreadStart(string cTime, string cTime2)
        {
            this.cTime_RevenueRatio_Analysis = cTime;
            this.cTime2_RevenueRatio_Analysis = cTime2;
            thread_RevenueRatio_Analysis_Modify = new Thread(new ThreadStart(RevenueRatio_Analysis_Modify_Timer));
            thread_RevenueRatio_Analysis_Modify.Start();
        }

        public void RevenueRatio_Analysis_Modify_Timer()
        {
            timer_RevenueRatio_Analysis_Modify = new System.Timers.Timer();
            timer_RevenueRatio_Analysis_Modify.Interval = 1000;
            timer_RevenueRatio_Analysis_Modify.Start();
            timer_RevenueRatio_Analysis_Modify.Elapsed += new ElapsedEventHandler(RevenueRatio_Analysis_Modify_Execute);

            Thread.Sleep(1500);
            timer_RevenueRatio_Analysis_Modify.Stop();
        }

        public void RevenueRatio_Analysis_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new VolumeBatchThread(_threadPool);
                tExecute.setProgressBar_RevenueRatio_Analysis_Modify(this._progressbar_RevenueRatio_Analysis_Modify);
                tExecute.runThread_RevenueRatio_Analysis_Modify(cTime_RevenueRatio_Analysis, cTime2_RevenueRatio_Analysis);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 평균수압지점계산
        private System.Timers.Timer timer_WHHpress_Result = null;
        private Thread thread_WHHpress_Result = null;
        private Hashtable hash_WHHpress_Result = null;
        string time_WHHpress_Result = string.Empty;
        string type_WHHpress_Result = string.Empty;
        public void WHHpress_Result_ThreadStart()
        {
            try
            {
                hash_WHHpress_Result = conf.getSchedule(_path, "WHHpress_Result_Execute");
                time_WHHpress_Result = hash_WHHpress_Result["time"].ToString();
                type_WHHpress_Result = hash_WHHpress_Result["type"].ToString();
                thread_WHHpress_Result = new Thread(new ThreadStart(WHHpress_Result_Timer));
                thread_WHHpress_Result.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void WHHpress_Result_Timer()
        {
            try
            {
                timer_WHHpress_Result = new System.Timers.Timer();
                timer_WHHpress_Result.Interval = 1000;
                timer_WHHpress_Result.Elapsed += new ElapsedEventHandler(WHHpress_Result_Execute);
                timer_WHHpress_Result.Start();

                if (this._threadPool.ContainsKey(timer_WHHpress_Result.GetHashCode()))
                {
                    this._threadPool.Remove(timer_WHHpress_Result.GetHashCode());
                }
                _threadPool.Add(timer_WHHpress_Result.GetHashCode(), timer_WHHpress_Result);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer WHHpress_Result_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_WHHpress_Result.GetHashCode()];
        }
        public void WHHpress_Result_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_WHHpress_Result).Equals((string)(utils.GetTime()[type_WHHpress_Result])))
                {
                    tExecute = new VolumeBatchThread();
                    tExecute.setButton5(this._button5);
                    tExecute.runThread_WHHpress_Result();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 평균수압지점계산 보정
        private System.Timers.Timer timer_WHHpress_Result_Modify = null;
        private Thread thread_WHHpress_Result_Modify = null;

        private string cTime_WHHpress_Result = string.Empty;
        private string cTime2_WHHpress_Result = string.Empty;

        public void WHHpress_Result_Modify_ThreadStart(string cTime, string cTime2)
        {
            this.cTime_WHHpress_Result = cTime;
            this.cTime2_WHHpress_Result = cTime2;
            thread_WHHpress_Result_Modify = new Thread(new ThreadStart(WHHpress_Result_Modify_Timer));
            thread_WHHpress_Result_Modify.Start();
        }

        public void WHHpress_Result_Modify_Timer()
        {
            timer_WHHpress_Result_Modify = new System.Timers.Timer();
            timer_WHHpress_Result_Modify.Interval = 1000;
            timer_WHHpress_Result_Modify.Start();
            timer_WHHpress_Result_Modify.Elapsed += new ElapsedEventHandler(WHHpress_Result_Modify_Execute);

            Thread.Sleep(1500);
            timer_WHHpress_Result_Modify.Stop();
        }

        public void WHHpress_Result_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new VolumeBatchThread(_threadPool);
                tExecute.setProgressBar_WHHpress_Result_Modify(this._progressbar_WHHpress_Result_Modify);
                tExecute.runThread_WHHpress_Result_Modify(cTime_WHHpress_Result, cTime2_WHHpress_Result);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion
#endregion

    }
}
