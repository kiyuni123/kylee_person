﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

using WaterNETServer.LeakMonitoring.Control;
using WaterNETServer.BatchJobs.dao;
using WaterNETServer.LeakMonitoring.enum1;
using EMFrame.log;

namespace WaterNETServer.ThreadManager.runThread
{
    public class WvRunThread
    {
        private Hashtable _threadPool = null;

        public WvRunThread() { }
        public WvRunThread(Hashtable threadPool)
        {
            this._threadPool = threadPool;
        }

        #region 화면 제어 부분 ###########################################
        /// <summary>
        /// 쓰레드 상태 표시 버튼
        /// </summary>
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Button _button4;

        public void setButton1(Button button) { this._button1 = button; }
        public void setButton2(Button button) { this._button2 = button; }
        public void setButton3(Button button) { this._button3 = button; }
        public void setButton4(Button button) { this._button4 = button; }


        /// <summary>
        /// 쓰레드 상태 표시 프로그래스바
        /// </summary>
        private ProgressBar _progressbar1;
        private ProgressBar _progressbar2;
        private ProgressBar _progressbar3;
        private ProgressBar _progressbar4;
        private ProgressBar _progressbar5;
        public void setProgressBar1(ProgressBar progressbar) { this._progressbar1 = progressbar; }
        public void setProgressBar2(ProgressBar progressbar) { this._progressbar2 = progressbar; }
        public void setProgressBar3(ProgressBar progressbar) { this._progressbar3 = progressbar; }
        public void setProgressBar4(ProgressBar progressbar) { this._progressbar4 = progressbar; }
        public void setProgressBar5(ProgressBar progressbar) { this._progressbar5 = progressbar; }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback1(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage1(int i)
        {
            if (_button1.InvokeRequired)
            {
                SetButtonImageCallback1 d = new SetButtonImageCallback1(SetButtonImage1);
                this._button1.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback2(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage2(int i)
        {
            if (_button2.InvokeRequired)
            {
                SetButtonImageCallback2 d = new SetButtonImageCallback2(SetButtonImage2);
                this._button2.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback3(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage3(int i)
        {
            if (_button3.InvokeRequired)
            {
                SetButtonImageCallback3 d = new SetButtonImageCallback3(SetButtonImage3);
                this._button3.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback4(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage4(int i)
        {
            if (_button4.InvokeRequired)
            {
                SetButtonImageCallback4 d = new SetButtonImageCallback4(SetButtonImage4);
                this._button4.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }


        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback1(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar1(int i)
        {
            if (_progressbar1.InvokeRequired)
            {
                SetProgressBarCallback1 d = new SetProgressBarCallback1(SetProgressBar1);
                this._progressbar1.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar1.Value = i;
            }
        }
        public void SetProgressBarMaximun1(int i)
        {
            if (_progressbar1.InvokeRequired)
            {
                SetProgressBarCallback1 d = new SetProgressBarCallback1(SetProgressBarMaximun1);
                this._progressbar1.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar1.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback2(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar2(int i)
        {
            if (_progressbar2.InvokeRequired)
            {
                SetProgressBarCallback2 d = new SetProgressBarCallback2(SetProgressBar2);
                this._progressbar2.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar2.Value = i;
            }
        }

        public void SetProgressBarMaximun2(int i)
        {
            if (_progressbar2.InvokeRequired)
            {
                SetProgressBarCallback2 d = new SetProgressBarCallback2(SetProgressBarMaximun2);
                this._progressbar2.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar2.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback3(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar3(int i)
        {
            if (_progressbar3.InvokeRequired)
            {
                SetProgressBarCallback3 d = new SetProgressBarCallback3(SetProgressBar3);
                this._progressbar3.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar3.Value = i;
            }
        }

        public void SetProgressBarMaximun3(int i)
        {
            if (_progressbar3.InvokeRequired)
            {
                SetProgressBarCallback3 d = new SetProgressBarCallback3(SetProgressBarMaximun3);
                this._progressbar3.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar3.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback4(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar4(int i)
        {
            if (_progressbar4.InvokeRequired)
            {
                SetProgressBarCallback4 d = new SetProgressBarCallback4(SetProgressBar4);
                this._progressbar4.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar4.Value = i;
            }
        }

        public void SetProgressBarMaximun4(int i)
        {
            if (_progressbar4.InvokeRequired)
            {
                SetProgressBarCallback4 d = new SetProgressBarCallback4(SetProgressBarMaximun4);
                this._progressbar4.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar4.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback5(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar5(int i)
        {
            if (_progressbar5.InvokeRequired)
            {
                SetProgressBarCallback5 d = new SetProgressBarCallback5(SetProgressBar5);
                this._progressbar5.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar5.Value = i;
            }
        }

        public void SetProgressBarMaximun5(int i)
        {
            if (_progressbar5.InvokeRequired)
            {
                SetProgressBarCallback5 d = new SetProgressBarCallback5(SetProgressBarMaximun5);
                this._progressbar5.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar5.Maximum = i;
            }
        }
        #endregion


        /// <summary>
        /// 누수감시
        /// </summary>
        /// <param name="param1"></param>
        public void runThread_MonitoringLeakage()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage3(1);

                Monitering monitering = new Monitering();
                monitering.AddAll();

                monitering.Start(DateTime.Now.AddDays(-1), MONITOR_TYPE.LEAKAGE);
                monitering.WriteWarning();

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage3(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }


        /// <summary>
        /// 누수감시 보정
        /// </summary>
        /// <param name="modifyHourString"></param>
        /// <param name="modifyHourString2"></param>
        internal void runThread_MonitoringLeakage_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("누수감시 보정 시작");

            // 전역변수에 누수감시 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_MonitoringLeakage_Modify"))
            {
                this._threadPool.Remove("runThread_MonitoringLeakage_Modify");
            }
            this._threadPool.Add("runThread_MonitoringLeakage_Modify", "RUN");

            #endregion

            try
            {
                Monitering monitering = new Monitering();

                int accuHourCnt = (DateTime.ParseExact(cTime2, "yyyyMMddHHmm", null).Subtract(DateTime.ParseExact(cTime, "yyyyMMddHHmm", null)).Days + 1);

                SetProgressBarMaximun1(accuHourCnt);

                int i = 1;
                monitering.AddAll();
                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {
                    SetProgressBar1(i);
                    monitering.Start(d, MONITOR_TYPE.LEAKAGE);
                    d = d.AddDays(1);
                }
                SetProgressBar1(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }

            #region 종료 메시지

            MessageBox.Show("누수감시 보정 종료");

            // 전역변수에 누수감시 보정 종료 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_MonitoringLeakage_Modify"))
            {
                this._threadPool.Remove("runThread_MonitoringLeakage_Modify");
            }
            this._threadPool.Add("runThread_MonitoringLeakage_Modify", "STOP");

            #endregion
        }

        /// <summary>
        /// 야간 최소유량
        /// </summary>
        public void runThread_MinimumNightFlow()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage1(1);

                BatchJobsWork jobs = new BatchJobsWork();
                jobs.MinimumNightFlow();

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage1(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        /// <summary>
        /// 야간 최소유량 (지정일) 보정
        /// </summary>
        /// <param name="cTime2">보정 시작 날짜</param>
        /// <param name="cTime3">보정 끝 날짜</param>
        public void runThread_MinimumNightFlow_modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 데이터 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_MinimumNightFlow_modify"))
            {
                this._threadPool.Remove("runThread_MinimumNightFlow_modify");
            }
            this._threadPool.Add("runThread_MinimumNightFlow_modify", "RUN");

            #endregion

            try
            {
                BatchJobsWork jobs = new BatchJobsWork();

                int accuHourCnt = (DateTime.ParseExact(cTime2, "yyyyMMddHHmm", null).Subtract(DateTime.ParseExact(cTime, "yyyyMMddHHmm", null)).Days + 1);

                SetProgressBarMaximun2(accuHourCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {
                    SetProgressBar2(i);
                    jobs.MinimumNightFlow_modify(d.ToString("yyyyMMdd"), d.ToString("yyyyMMdd"));
                    d = d.AddDays(1);
                }
                SetProgressBar2(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 데이터 보정 종료 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_MinimumNightFlow_modify"))
            {
                this._threadPool.Remove("runThread_MinimumNightFlow_modify");
            }
            this._threadPool.Add("runThread_MinimumNightFlow_modify", "STOP");

            #endregion
        }

        /// <summary>
        /// 1시간 정시 수압
        /// </summary>
        public void runThread_AveragePressure()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage2(1);

                BatchJobsWork jobs = new BatchJobsWork();
                jobs.AveragePressure();

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage2(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        /// <summary>
        /// 1시간 정시 수압 (지정일) 보정
        /// </summary>
        /// <param name="cTime2">보정 시작 날짜</param>
        /// <param name="cTime3">보정 끝 날짜</param>
        public void runThread_AveragePressure_Modify(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 데이터 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_AveragePressure_Modify"))
            {
                this._threadPool.Remove("runThread_AveragePressure_Modify");
            }
            this._threadPool.Add("runThread_AveragePressure_Modify", "RUN");

            #endregion

            try
            {
                BatchJobsWork jobs = new BatchJobsWork();

                int accuHourCnt = (DateTime.ParseExact(cTime2, "yyyyMMddHHmm", null).Subtract(DateTime.ParseExact(cTime, "yyyyMMddHHmm", null)).Days + 1);

                SetProgressBarMaximun3(accuHourCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {
                    SetProgressBar3(i);
                    jobs.AveragePressure_Modify(d.ToString("yyyyMMdd"), d.ToString("yyyyMMdd"));
                    d = d.AddDays(1);
                }
                SetProgressBar3(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 데이터 보정 종료 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_AveragePressure_Modify"))
            {
                this._threadPool.Remove("runThread_AveragePressure_Modify");
            }
            this._threadPool.Add("runThread_AveragePressure_Modify", "STOP");

            #endregion
        }
    }
}
