﻿using System;
using System.Threading;

namespace WaterNETTest
{
    public class ThreadExam1
    {
        public static void Print1()
        {
            Console.WriteLine("첫번째 Thread ***");
        }

        public void Print2()
        {
            Console.WriteLine("두번째 Thread ***");
        }


        public void myMain()
        {
            // static 인 쓰레드 시작방법
            Thread thread = new Thread(new ThreadStart(Print1));
            thread.Start();

            // static 형태가 아닌 쓰레드 시작방법
            thread = new Thread(new ThreadStart((new ThreadExam1()).Print2));
            thread.Start();

            Console.WriteLine("세번째 Thread ***");
        }
    }
}
