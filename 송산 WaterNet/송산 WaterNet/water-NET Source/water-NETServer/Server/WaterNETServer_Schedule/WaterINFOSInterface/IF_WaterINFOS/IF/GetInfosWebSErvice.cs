﻿using System.Data;
using System;
using System.Diagnostics;
using System.ServiceModel;
using WaterNETServer.Common.utils;
using EMFrame.log;

namespace IF_WaterINFOS.IF
{
    public class GetInfosWebService
    {
        Common utils = new Common();
        GlobalVariable gVar = new GlobalVariable();

        private string _strSGCCD = string.Empty;   // 지자체코드

        public GetInfosWebService()
        {
            _strSGCCD = gVar.SGCCD; //.getSgccdCode();
        }

        /// <summary>
        /// 수용가번호 인터페이스
        /// </summary>
        /// <param name="strSGCCD">지자체코드</param>
        /// <param name="strREGDTF">등록일 From(null 가능)</param>
        /// <param name="strREGDTT">등록일 To(null 가능)</param>
        /// <param name="strDMNO">수용가번호(null 가능)</param>
        /// <returns></returns>
        public DataSet GetDMINFO(string strSVCKEY, string strREGMNGRIP, string strSGCCD, string strDMNO, string strSTRTDT)
        {
            //Console.WriteLine("IF_WaterINFOS.GetDMINFO : ({0}), ({1}))", strREGDTF, strREGDTT);
            DataSet dSet = new DataSet();

            try
            {
                switch (gVar.URL)
                {
                    case "sd":
                        using (IF_WaterINFOS.sd.Service1SoapClient client = new IF_WaterINFOS.sd.Service1SoapClient())
                        {
                            dSet = client.GetDMINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "cc":
                        using (IF_WaterINFOS.cc.Service1SoapClient client = new IF_WaterINFOS.cc.Service1SoapClient())
                        {
                            dSet = client.GetDMINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "jb":
                        using (IF_WaterINFOS.jb.Service1SoapClient client = new IF_WaterINFOS.jb.Service1SoapClient())
                        {
                            dSet = client.GetDMINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "jn":
                        using (IF_WaterINFOS.jn.Service1SoapClient client = new IF_WaterINFOS.jn.Service1SoapClient())
                        {
                            dSet = client.GetDMINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "gb":
                        using (IF_WaterINFOS.gb.Service1SoapClient client = new IF_WaterINFOS.gb.Service1SoapClient())
                        {
                            dSet = client.GetDMINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "gn":
                        using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                        {
                            dSet = client.GetDMINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                }
                //using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                //{
                //    dSet = client.GetDMINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.StackTrace);

                Logger.Error(e.ToString());
                //Logger.Error(e.StackTrace);
            }
            return dSet;
        }

        /// <summary>
		/// 상하수도자원
		/// </summary>
		/// <param name="strSGCCD">지자체코드</param>
		/// <param name="strREGDTF">등록일 From(null 가능)</param>
		/// <param name="strREGDTT">등록일 To(null 가능)</param>
		/// <param name="strDMNO">수용가번호(null 가능)</param>
		/// <returns></returns>
        public DataSet GetDMWSRSRC(string strSVCKEY, string strREGMNGRIP, string strSGCCD, string strDMNO, string strSTRTDT)
        {
            DataSet dSet = new DataSet();
            try
            {
                switch (gVar.URL)
                {
                    case "sd":
                        using (IF_WaterINFOS.sd.Service1SoapClient client = new IF_WaterINFOS.sd.Service1SoapClient())
                        {
                            dSet = client.GetDMWSRSRC(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "cc":
                        using (IF_WaterINFOS.cc.Service1SoapClient client = new IF_WaterINFOS.cc.Service1SoapClient())
                        {
                            dSet = client.GetDMWSRSRC(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "jb":
                        using (IF_WaterINFOS.jb.Service1SoapClient client = new IF_WaterINFOS.jb.Service1SoapClient())
                        {
                            dSet = client.GetDMWSRSRC(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "jn":
                        using (IF_WaterINFOS.jn.Service1SoapClient client = new IF_WaterINFOS.jn.Service1SoapClient())
                        {
                            dSet = client.GetDMWSRSRC(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "gb":
                        using (IF_WaterINFOS.gb.Service1SoapClient client = new IF_WaterINFOS.gb.Service1SoapClient())
                        {
                            dSet = client.GetDMWSRSRC(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                    case "gn":
                        using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                        {
                            dSet = client.GetDMWSRSRC(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strSTRTDT);
                        }
                        break;
                }
                //using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                //{
                //    dSet = client.GetDMWSRSRC(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO,strSTRTDT);
                //}
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
            return dSet;
        }

		/// <summary>
		/// 민원기본정보
		/// </summary>
		/// <param name="strSGCCD">지자체코드</param>
		/// <param name="strREGDTF">등록일 From(null 가능)</param>
		/// <param name="strREGDTT">등록일 To(null 가능)</param>
		/// <param name="strCANO">민원번호(null 가능)</param>
		/// <returns></returns>
        public DataSet GetCAINFO(string strSVCKEY, string strREGMNGRIP, string strSGCCD, string strCANO, string strCAAPPLDTFR, string strCAAPPLDTTO)
		{
            DataSet dSet = new DataSet();
            try
            {
                switch (gVar.URL)
                {
                    case "sd":
                        using (IF_WaterINFOS.sd.Service1SoapClient client = new IF_WaterINFOS.sd.Service1SoapClient())
                        {
                            dSet = client.GetCAINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strCANO, strCAAPPLDTFR, strCAAPPLDTTO);
                        }
                        break;
                    case "cc":
                        using (IF_WaterINFOS.cc.Service1SoapClient client = new IF_WaterINFOS.cc.Service1SoapClient())
                        {
                            dSet = client.GetCAINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strCANO, strCAAPPLDTFR, strCAAPPLDTTO);
                        }
                        break;
                    case "jb":
                        using (IF_WaterINFOS.jb.Service1SoapClient client = new IF_WaterINFOS.jb.Service1SoapClient())
                        {
                            dSet = client.GetCAINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strCANO, strCAAPPLDTFR, strCAAPPLDTTO);
                        }
                        break;
                    case "jn":
                        using (IF_WaterINFOS.jn.Service1SoapClient client = new IF_WaterINFOS.jn.Service1SoapClient())
                        {
                            dSet = client.GetCAINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strCANO, strCAAPPLDTFR, strCAAPPLDTTO);
                        }
                        break;
                    case "gb":
                        using (IF_WaterINFOS.gb.Service1SoapClient client = new IF_WaterINFOS.gb.Service1SoapClient())
                        {
                            dSet = client.GetCAINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strCANO, strCAAPPLDTFR, strCAAPPLDTTO);
                        }
                        break;
                    case "gn":
                        using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                        {
                            dSet = client.GetCAINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strCANO, strCAAPPLDTFR, strCAAPPLDTTO);
                        }
                        break;
                }

                //using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                //{
                //    dSet = client.GetCAINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strCANO, strCAAPPLDTFR, strCAAPPLDTTO);
                //}
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
            return dSet;
        }

 		/// <summary>
		/// 계량기교체내역
		/// </summary>
		/// <param name="strSGCCD">지자체코드</param>
		/// <param name="strREGDTF">등록일 From(null 가능)</param>
		/// <param name="strREGDTT">등록일 To(null 가능)</param>
		/// <param name="strMTRCHGSERNO">계량기번호(null 가능)</param>
		/// <returns></returns>
        public DataSet GetMTRCHGINFO(string strSVCKEY, string strREGMNGRIP, string strSGCCD, string strDMNO, string strMDDTFR, string strMDDTTO)
		{
            //Console.WriteLine("IF_WaterINFOS.GetMTRCHGINFO : ({0}), ({1}))", strREGDTF, strREGDTT);
            DataSet dSet = new DataSet();
            try
            {
                switch (gVar.URL)
                {
                    case "sd":
                        using (IF_WaterINFOS.sd.Service1SoapClient client = new IF_WaterINFOS.sd.Service1SoapClient())
                        {
                            dSet = client.GetMTRCHGINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strMDDTFR, strMDDTTO);
                        }
                        break;
                    case "cc":
                        using (IF_WaterINFOS.cc.Service1SoapClient client = new IF_WaterINFOS.cc.Service1SoapClient())
                        {
                            dSet = client.GetMTRCHGINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strMDDTFR, strMDDTTO);
                        }
                        break;
                    case "jb":
                        using (IF_WaterINFOS.jb.Service1SoapClient client = new IF_WaterINFOS.jb.Service1SoapClient())
                        {
                            dSet = client.GetMTRCHGINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strMDDTFR, strMDDTTO);
                        }
                        break;
                    case "jn":
                        using (IF_WaterINFOS.jn.Service1SoapClient client = new IF_WaterINFOS.jn.Service1SoapClient())
                        {
                            dSet = client.GetMTRCHGINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strMDDTFR, strMDDTTO);
                        }
                        break;
                    case "gb":
                        using (IF_WaterINFOS.gb.Service1SoapClient client = new IF_WaterINFOS.gb.Service1SoapClient())
                        {
                            dSet = client.GetMTRCHGINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strMDDTFR, strMDDTTO);
                        }
                        break;
                    case "gn":
                        using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                        {
                            dSet = client.GetMTRCHGINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strMDDTFR, strMDDTTO);
                        }
                        break;
                }
                //using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                //{
                //    dSet = client.GetMTRCHGINFO(strSVCKEY, strREGMNGRIP, strSGCCD, strDMNO, strMDDTFR, strMDDTTO);
                //}
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
            return dSet;
        }

 		/// <summary>
		/// 요금조정
		/// </summary>
		/// <param name="strSGCCD">지자체코드</param>
		/// <param name="strSTYMF">요금조정일 From</param>
		/// <param name="strSTYMT">요금조정일 To</param>
		/// <param name="strREGDTF">등록일 From(null 가능)</param>
		/// <param name="strREGDTT">등록일 To(null 가능)</param>
		/// <returns></returns>
        public DataSet GetSTWCHRG(string strSVCKEY, string strREGMNGRIP, string strSGCCD, string strSTYMFR, string strSTYMTO)
		{
            //Console.WriteLine("IF_WaterINFOS.GetSTWCHRG : ({0}), ({1}), ({2}), ({3}))", strSTYMF, strSTYMT, strREGDTF, strREGDTT);
            DataSet dSet = new DataSet();
            try
            {
                switch (gVar.URL)
                {
                    case "sd":
                        using (IF_WaterINFOS.sd.Service1SoapClient client = new IF_WaterINFOS.sd.Service1SoapClient())
                        {
                            dSet = client.GetSTWCHRG(strSVCKEY, strREGMNGRIP, strSGCCD, strSTYMFR, strSTYMTO);
                        }
                        break;
                    case "cc":
                        using (IF_WaterINFOS.cc.Service1SoapClient client = new IF_WaterINFOS.cc.Service1SoapClient())
                        {
                            dSet = client.GetSTWCHRG(strSVCKEY, strREGMNGRIP, strSGCCD, strSTYMFR, strSTYMTO);
                        }
                        break;
                    case "jb":
                        using (IF_WaterINFOS.jb.Service1SoapClient client = new IF_WaterINFOS.jb.Service1SoapClient())
                        {
                            dSet = client.GetSTWCHRG(strSVCKEY, strREGMNGRIP, strSGCCD, strSTYMFR, strSTYMTO);
                        }
                        break;
                    case "jn":
                        using (IF_WaterINFOS.jn.Service1SoapClient client = new IF_WaterINFOS.jn.Service1SoapClient())
                        {
                            dSet = client.GetSTWCHRG(strSVCKEY, strREGMNGRIP, strSGCCD, strSTYMFR, strSTYMTO);
                        }
                        break;
                    case "gb":
                        using (IF_WaterINFOS.gb.Service1SoapClient client = new IF_WaterINFOS.gb.Service1SoapClient())
                        {
                            dSet = client.GetSTWCHRG(strSVCKEY, strREGMNGRIP, strSGCCD, strSTYMFR, strSTYMTO);
                        }
                        break;
                    case "gn":
                        using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                        {
                            dSet = client.GetSTWCHRG(strSVCKEY, strREGMNGRIP, strSGCCD, strSTYMFR, strSTYMTO);
                        }
                        break;
                }
                //using (IF_WaterINFOS.gn.Service1SoapClient client = new IF_WaterINFOS.gn.Service1SoapClient())
                //{
                //    dSet = client.GetSTWCHRG(strSVCKEY, strREGMNGRIP, strSGCCD, strSTYMFR, strSTYMTO);
                //}
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
            return dSet;
        }
    }
}
