﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNETServer.DF_Algorithm.NN_Core
{
    /// <summary>
    /// 기본 신경망 레이어 클래스
    /// </summary>
    /// 
    /// <remarks> 뉴런들의 집합체이다.</remarks>
    /// 
    public class Layer
    {
        /// <summary>
        /// 레이어의 입력 수
        /// </summary>
        public int n_Inputs;

        /// <summary>
        /// 레이어의 뉴런 수
        /// </summary>
        public int n_Neurons;

        /// <summary>
        /// 레이어의 뉴런
        /// </summary>
        public Neuron[] neurons;

        /// <summary>
        /// 레이어의 출력 벡터
        /// </summary>
        public double[] f_Output;

        /// <summary>
        /// <see cref="Layer"/> 클래스의 객체 초기화
        /// </summary>
        /// 
        /// <param name="neuronsCount">레이의 뉴런 수</param>
        /// <param name="inputsCount">레이어의 입력 수</param>
        /// 
        public Layer(int n_Neurons, int n_Inputs)
        {
            this.n_Inputs = Math.Max(1, n_Inputs);
            this.n_Neurons = Math.Max(1, n_Neurons);

            neurons = new Neuron[this.n_Neurons];
            f_Output = new double[this.n_Neurons];
        }

        /// <summary>
        /// 레이어의 출력 벡터 계산 
        /// </summary>
        /// 
        /// <param name="input">입력 벡터</param>
        /// 
        /// <returns>레이어의 출력 벡터 반환</returns>
        /// 
        /// <remarks> 실제 레이어의 출력 벡터는 레이어의 뉴런들의 출력 값으로 결정됨.
        /// <see cref="f_Output"/> property.</remarks>
        /// 
        public virtual double[] Compute(double[] input)
        {
            for (int i = 0; i < n_Neurons; i++)
            {
                f_Output[i] = neurons[i].Compute(input);
            }
            return f_Output;
        }

        /// <summary>
        /// 레이의 뉴런들 랜덤화하기
        /// </summary>
        /// 
        /// <remarks> 각 뉴런의 <see cref="Neuron.Randomize"/> 메소드를 호출함으로써 
        /// 레이어의 뉴런들을 랜덤화한다.</remarks>
        /// 
        public virtual void Randomize()
        {
            foreach (Neuron neuron in neurons)
                neuron.Randomize();
        }
    }
}
