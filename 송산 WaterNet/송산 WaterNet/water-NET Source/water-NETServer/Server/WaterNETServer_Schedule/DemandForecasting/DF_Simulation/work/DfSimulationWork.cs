﻿using System;
using System.Collections;
using System.Data;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;
using WaterNETServer.DF_Simulation.dao;
using WaterNETServer.DF_Simulation.util;
using WaterNETServer.DF_Simulation.data;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.work;
using EMFrame.log;

namespace WaterNETServer.DF_Simulation.work
{
    public class DfSimulationWork : BaseWork
    {
        private static DfSimulationWork work = null;
        private DfSimulationDao dao = null;

        private DfSimulationWork()
        {
            dao = DfSimulationDao.GetInstance();
        }

        public static DfSimulationWork GetInstance()
        {
            if (work == null)
            {
                work = new DfSimulationWork();
            }

            return work;
        }

        public DataTable SelectDfSimulation()
        {
            DataTable result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();


                DataSet dataSet = dao.SelectDfSimulation(manager);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측관리 조회
        public DataTable SelectDfSimulationBlock(Hashtable parameter)
        {
            DataTable result = new DataTable();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationBlock(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                if (result != null)
                {
                    foreach (DataRow dataRow in result.Rows)
                    {
                        parameter["LOC_CODE"] = dataRow["LOC_CODE"].ToString();

                        dataSet = dao.SelectDfSimulationResult(manager, parameter);

                        if (dataSet.Tables.Count > 0)
                        {
                            DataTable dt = dataSet.Tables[0];

                            foreach (DataRow row in dt.Rows)
                            {
                                string columnName = Convert.ToDateTime(row["DF_RL_DT"]).ToString("yyyyMMdd");

                                if (result.Columns.IndexOf(columnName) == -1)
                                {
                                    result.Columns.Add(columnName);
                                }

                                dataRow[columnName] = row["RESULT"];
                            }
                        }
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측관리 조회
        public DataTable SelectDfSimulationReservoir(Hashtable parameter)
        {
            DataTable result = new DataTable();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationReservoir(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                if (result != null)
                {
                    foreach (DataRow dataRow in result.Rows)
                    {
                        parameter["LOC_CODE"] = dataRow["LOC_CODE"].ToString();

                        dataSet = dao.SelectDfSimulationResult(manager, parameter);

                        if (dataSet.Tables.Count > 0)
                        {
                            DataTable dt = dataSet.Tables[0];

                            foreach (DataRow row in dt.Rows)
                            {
                                string columnName = Convert.ToDateTime(row["DF_RL_DT"]).ToString("yyyyMMdd");

                                if (result.Columns.IndexOf(columnName) == -1)
                                {
                                    result.Columns.Add(columnName);
                                }

                                dataRow[columnName] = row["RESULT"];
                            }
                        }
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측기본설정 조회
        public Hashtable SelectDfSimulationSettingBlock(Hashtable parameter)
        {
            Hashtable result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingBlock(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);

                        if (result["STUDYMETHOD"] == DBNull.Value)
                        {
                            result["STUDYMETHOD"] = DfSetting.DefaultSetting["STUDYMETHOD"];
                        }

                        if (result["ABNORMALDATA"] == DBNull.Value)
                        {
                            result["ABNORMALDATA"] = DfSetting.DefaultSetting["ABNORMALDATA"];
                        }

                        if (result["TIMEFORECASTING"] == DBNull.Value)
                        {
                            result["TIMEFORECASTING"] = DfSetting.DefaultSetting["TIMEFORECASTING"];
                        }

                        if (result["DAYKIND"] == DBNull.Value)
                        {
                            result["DAYKIND"] = DfSetting.DefaultSetting["DAYKIND"];
                        }

                        if (result["STUDYDAY"] == DBNull.Value)
                        {
                            result["STUDYDAY"] = DfSetting.DefaultSetting["STUDYDAY"];
                        }

                        if (result["STUDYPATTERN"] == DBNull.Value)
                        {
                            result["STUDYPATTERN"] = DfSetting.DefaultSetting["STUDYPATTERN"];
                        }
                        if (result["USE_YN"] == DBNull.Value)
                        {
                            result["USE_YN"] = DfSetting.DefaultSetting["USE_YN"];
                        }
                    }
                    else
                    {
                        result = DfSetting.DefaultSetting;
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측기본설정 조회
        public Hashtable SelectDfSimulationSettingReservoir(Hashtable parameter)
        {
            Hashtable result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();


                DataSet dataSet = dao.SelectDfSimulationSettingReservoir(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);

                        if (result["STUDYMETHOD"] == DBNull.Value)
                        {
                            result["STUDYMETHOD"] = DfSetting.DefaultSetting["STUDYMETHOD"];
                        }

                        if (result["ABNORMALDATA"] == DBNull.Value)
                        {
                            result["ABNORMALDATA"] = DfSetting.DefaultSetting["ABNORMALDATA"];
                        }

                        if (result["TIMEFORECASTING"] == DBNull.Value)
                        {
                            result["TIMEFORECASTING"] = DfSetting.DefaultSetting["TIMEFORECASTING"];
                        }

                        if (result["DAYKIND"] == DBNull.Value)
                        {
                            result["DAYKIND"] = DfSetting.DefaultSetting["DAYKIND"];
                        }

                        if (result["STUDYDAY"] == DBNull.Value)
                        {
                            result["STUDYDAY"] = DfSetting.DefaultSetting["STUDYDAY"];
                        }

                        if (result["STUDYPATTERN"] == DBNull.Value)
                        {
                            result["STUDYPATTERN"] = DfSetting.DefaultSetting["STUDYPATTERN"];
                        }
                        if (result["USE_YN"] == DBNull.Value)
                        {
                            result["USE_YN"] = DfSetting.DefaultSetting["USE_YN"];
                        }
                    }
                    else
                    {
                        result = DfSetting.DefaultSetting;
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측기본설정 조회
        public Hashtable SelectDfSimulationSetting(Hashtable parameter)
        {
            Hashtable result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSetting(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);

                        if (result["STUDYMETHOD"] == DBNull.Value)
                        {
                            result["STUDYMETHOD"] = DfSetting.DefaultSetting["STUDYMETHOD"];
                        }

                        if (result["ABNORMALDATA"] == DBNull.Value)
                        {
                            result["ABNORMALDATA"] = DfSetting.DefaultSetting["ABNORMALDATA"];
                        }

                        if (result["TIMEFORECASTING"] == DBNull.Value)
                        {
                            result["TIMEFORECASTING"] = DfSetting.DefaultSetting["TIMEFORECASTING"];
                        }

                        if (result["DAYKIND"] == DBNull.Value)
                        {
                            result["DAYKIND"] = DfSetting.DefaultSetting["DAYKIND"];
                        }

                        if (result["STUDYDAY"] == DBNull.Value)
                        {
                            result["STUDYDAY"] = DfSetting.DefaultSetting["STUDYDAY"];
                        }

                        if (result["STUDYPATTERN"] == DBNull.Value)
                        {
                            result["STUDYPATTERN"] = DfSetting.DefaultSetting["STUDYPATTERN"];
                        }
                        if (result["USE_YN"] == DBNull.Value)
                        {
                            result["USE_YN"] = DfSetting.DefaultSetting["USE_YN"];
                        }
                    }
                    else
                    {
                        result = DfSetting.DefaultSetting;
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측기본설정 조회
        public Hashtable SelectDfSimulationSettingNN(Hashtable parameter)
        {
            Hashtable result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingNN(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = Utils.ConverToHashtable(dataSet.Tables[0].Rows[0]);
                    }
                    else
                    {
                        result = DfSetting.DefaultSettingNN;
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측기본설정 조회
        public DataTable SelectDfSimulationSettingMRM(Hashtable parameter)
        {
            DataTable result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingMRM(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측기본설정 조회
        public DataTable SelectDfSimulationSettingOutlier(Hashtable parameter)
        {
            DataTable result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();


                DataSet dataSet = dao.SelectDfSimulationSettingOutlier(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        public void UpdateDfSimulationUse(UltraGrid use)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                foreach (UltraGridRow row in use.Rows)
                {
                    Hashtable use_parameter = Utils.ConverToHashtable(row);
                    dao.UpdateDfSimulationUse(manager, use_parameter);
                }

                MessageBox.Show("정상적으로 처리되었습니다.");
                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        //수요예측기본설정 수정
        public void UpdateDfSimulationSetting(Hashtable parameter, Hashtable setting, Hashtable nn, UltraGrid mrm, UltraGrid outlier)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                setting["LOC_CODE"] = parameter["LOC_CODE"];
                setting["USE_YN"] = dao.SelectDfSimulationUse(manager, parameter);
                nn["LOC_CODE"] = parameter["LOC_CODE"];

                dao.DeleteDfSimulationSetting(manager, parameter);
                dao.DeleteDfSimulationSettingNN(manager, parameter);
                dao.DeleteDfSimulationSettingMRM(manager, parameter);
                dao.DeleteDfSimulationSettingOutlier(manager, parameter);

                dao.InsertDfSimulationSetting(manager, setting);
                dao.InsertDfSimulationSettingNN(manager, nn);

                foreach (UltraGridRow row in mrm.Rows)
                {
                    Hashtable mrm_parameter = Utils.ConverToHashtable(row);
                    mrm_parameter["LOC_CODE"] = parameter["LOC_CODE"];
                    dao.InsertDfSimulationSettingMRM(manager, mrm_parameter);
                }

                foreach (UltraGridRow row in outlier.Rows)
                {
                    Hashtable outlier_parameter = Utils.ConverToHashtable(row);
                    outlier_parameter["LOC_CODE"] = parameter["LOC_CODE"];
                    dao.InsertDfSimulationSettingOutlier(manager, outlier_parameter);
                }

                MessageBox.Show("정상적으로 처리되었습니다.");
                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        //수요예측 실측 데이터조회
        public DataTable SelectDfSimulationSettingDataBlock(Hashtable parameter)
        {
            DataTable result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                DataSet dataSet = dao.SelectDfSimulationSettingDataBlock(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측 실측 데이터조회
        public DataTable SelectDfSimulationSettingDataReservoir(Hashtable parameter)
        {
            DataTable result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();


                DataSet dataSet = dao.SelectDfSimulationSettingDataReservoir(manager, parameter);

                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        result = dataSet.Tables[0];
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //수요예측 실측 데이터조회
        public double SelectDfSimulationTimeDataBlock(Hashtable parameter)
        {
            object result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectDfSimulationTimeDataBlock(manager, parameter);

                if (result == DBNull.Value)
                {
                    result = 0;
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return Convert.ToDouble(result);
        }

        //수요예측 실측 데이터조회
        public double SelectDfSimulationTimeDataReservoir(Hashtable parameter)
        {
            object result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectDfSimulationTimeDataReservoir(manager, parameter);

                if (result == DBNull.Value)
                {
                    result = 0;
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return Convert.ToDouble(result);
        }

        //일예측량을 등록 및 수정 한다.
        public void UpdateDayPrediction(Hashtable parameter)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                dao.UpdateDayPrediction(manager, parameter);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        //시간예측량을 등록 및 수정 한다.
        public void UpdateHourPrediction(Hashtable parameter)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                dao.UpdateHourPrediction(manager, parameter);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        //시간예측량을 등록 및 수정 한다.
        public void UpdateHourPrediction(Hashtable parameter, double[] value)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                for (int i = 0; i < value.Length; i++)
                {
                    parameter["DF_RL_HOUR"] = i.ToString("00");
                    parameter["DF_THFQ"] = Math.Round(value[i], 3);

                    dao.UpdateHourPrediction(manager, parameter);
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        //태그존재여부를 조회한다.
        public bool IsTagErrorBlock(Hashtable parameter)
        {
            bool result = false;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                object temp = dao.IsTagErrorBlock(manager, parameter);

                if (temp == null)
                {
                    result = true;
                }
                else if (temp != null)
                {
                    result = false;
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //태그존재여부를 조회한다.
        public bool IsTagErrorReservoir(Hashtable parameter)
        {
            bool result = false;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                object temp = dao.IsTagErrorReservoir(manager, parameter);

                if (temp == null)
                {
                    result = true;
                }
                else if (temp != null)
                {
                    result = false;
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }
    }
}
