﻿using System;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;
using System.Data;
using Infragistics.Win.UltraWinEditors;

namespace WaterNETServer.DF_Simulation.util
{
    public class Utils
    {
        public static Hashtable ConverToHashtable(object target)
        {
            Hashtable parameter = null;

            if (target is UltraGridRow)
            {
                UltraGridRow row = target as UltraGridRow;
                parameter = new Hashtable();

                foreach (UltraGridCell cell in row.Cells)
                {
                    if (cell.Value is DateTime)
                    {
                        parameter.Add(cell.Column.Key, Convert.ToDateTime(cell.Value).ToString("yyyyMMdd"));
                    }
                    else
                    {
                        parameter.Add(cell.Column.Key, cell.Value);
                    }
                }
            }
            else if (target is Control)
            {
                Control control = target as Control;
                parameter = new Hashtable();
                AddHashtableToControls(parameter, control);
            }
            else if (target is DataRow)
            {
                DataRow row = target as DataRow;
                parameter = new Hashtable();

                foreach (DataColumn column in row.Table.Columns)
                {
                    if (row[column] is DateTime)
                    {
                        parameter.Add(column.ColumnName, Convert.ToDateTime(row[column]).ToString("yyyyMMdd"));
                    }
                    else
                    {
                        parameter.Add(column.ColumnName, row[column]);
                    }
                }
            }

            return parameter;
        }

        public static void AddHashtableToControls(Hashtable parameter, Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    AddHashtableToControls(parameter, control);
                }
                else
                {
                    string parameterName = control.Name.ToUpper();
                    object parameterValue = null;

                    if (control is TextBox)
                    {
                        parameterValue = control.Text;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        parameterValue = combobox.SelectedValue;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        parameterValue = Convert.ToDateTime(date.Value).ToString("yyyyMMdd");

                        string strDate = parameterValue as string;

                        if (strDate == "00010101")
                        {
                            parameterValue = string.Empty;
                        }

                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is RichTextBox)
                    {
                        parameterValue = control.Text;
                        if (parameterValue == null)
                        {
                            parameterValue = string.Empty;
                        }
                        parameter.Add(parameterName, parameterValue);
                    }

                    if (control is CheckBox)
                    {
                        CheckBox cb = control as CheckBox;
                        parameterValue = cb.Checked.ToString();
                        if (parameterValue == null)
                        {
                            parameterValue = "false";
                        }
                        parameter.Add(parameterName, parameterValue);
                    }
                }
            }
        }

        public static void ClearControls(Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    ClearControls(control);
                }
                else
                {
                    if (control is TextBox)
                    {
                        control.Text = string.Empty;
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        if (combobox.Items.Count > 0)
                        {
                            combobox.SelectedIndex = 0;
                        }
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        date.Value = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    }

                    if (control is RichTextBox)
                    {
                        control.Text = string.Empty;
                    }
                }
            }
        }

        public static void SetControlDataSetting(Hashtable parameter, Control target)
        {
            foreach (Control control in target.Controls)
            {
                if (control.Controls.Count > 0)
                {
                    SetControlDataSetting(parameter, control);
                }
                else
                {
                    string parameterName = control.Name.ToUpper();

                    if (control is TextBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            control.Text = parameter[parameterName].ToString();
                        }
                    }

                    if (control is ComboBox)
                    {
                        ComboBox combobox = control as ComboBox;
                        if (parameter.ContainsKey(parameterName))
                        {
                            combobox.SelectedValue = parameter[parameterName];
                        }
                    }

                    if (control is UltraDateTimeEditor)
                    {
                        UltraDateTimeEditor date = control as UltraDateTimeEditor;
                        if (parameter.ContainsKey(parameterName))
                        {
                            date.Value = parameter[parameterName].ToString();
                        }
                    }

                    if (control is RichTextBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            control.Text = parameter[parameterName].ToString();
                        }
                    }

                    if (control is CheckBox)
                    {
                        if (parameter.ContainsKey(parameterName))
                        {
                            CheckBox cb = control as CheckBox;
                            cb.Checked = Convert.ToBoolean(parameter[parameterName]);
                        }
                    }
                }
            }
        }
    }

    public class ComboBoxUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="valueMember"></param>
        /// <param name="displayMemeber"></param>
        /// <param name="text"></param>
        /// <param name="interval"></param>
        public static void AddData(ComboBox target, string valueMember, string displayMemeber, string text, int interval)
        {
            SetMember(target, valueMember, displayMemeber);
            target.DataSource = DataUtils.GetRequstData(valueMember, displayMemeber, text, interval);
        }

        public static void AddDefaultOption(ComboBox target, string display, string value, bool isSelected)
        {
            if (target == null)
            {
                return;
            }

            DataTable table = target.DataSource as DataTable;

            if (table == null)
            {
                if (target.DataSource != null)
                {
                    table = ((DataView)target.DataSource).Table;
                }
            }

            if (table == null)
            {
                table = new DataTable();
                table.Columns.Add(target.DisplayMember);
                table.Columns.Add(target.ValueMember);
            }

            DataRow row = table.NewRow();
            row[target.DisplayMember] = display;
            row[target.ValueMember] = value;
            table.Rows.InsertAt(row, 0);

            if (target.DataSource == null)
            {
                target.DataSource = table;
            }

            if (isSelected)
            {
                target.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 콤보박스 기본옵션 설정
        /// </summary>
        /// <param name="target">콤보박스</param>
        /// <param name="isSelected">옵션값 선택 / 전체 여부</param>
        public static void AddDefaultOption(ComboBox target, bool isSelected)
        {
            if (target == null)
            {
                return;
            }

            string text = isSelected ? "선택" : "전체";
            string value = isSelected ? "X" : "O";

            string displayMember = target.DisplayMember != null ? target.DisplayMember : "CODE_NAME";
            string valueMember = target.ValueMember != null ? target.ValueMember : "CODE";

            SetMember(target, valueMember, displayMember);

            DataTable table = target.DataSource as DataTable;

            if (table == null)
            {
                table = new DataTable();
                table.Columns.Add(displayMember);
                table.Columns.Add(valueMember);
                DataRow row = table.NewRow();
                row[displayMember] = text;
                row[valueMember] = value;
                table.Rows.InsertAt(row, 0);
                target.DataSource = table;
                target.SelectedIndex = 0;
            }
            else
            {
                if (table.Rows.Count != 0)
                {
                    if (table.Rows[0][valueMember].ToString() == "X" || table.Rows[0][valueMember].ToString() == "O")
                    {
                        return;
                    }
                }

                DataRow row = table.NewRow();
                row[displayMember] = text;
                row[valueMember] = value;
                table.Rows.InsertAt(row, 0);
                target.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 콤보박스 기본 멤버 설정 및 기본옵션 설정
        /// </summary>
        /// <param name="target">콤보박스</param>
        /// <param name="isSelected">옵션값 선택 / 전체 여부</param>
        /// <param name="valueMember">값 필드멤버명</param>
        /// <param name="displayMember">텍스트 필드멤버명</param>
        public static void AddDefaultOption(ComboBox target, bool isSelected, string valueMember, string displayMember)
        {
            SetMember(target, valueMember, displayMember);
            AddDefaultOption(target, isSelected);
        }

        /// <summary>
        /// 콤보박스 기본 멤버 설정
        /// </summary>
        /// <param name="target">콤보박스</param>
        /// <param name="valueMember">값 필드멤버명</param>
        /// <param name="displayMemeber">텍스트 필드멤버명</param>
        public static void SetMember(ComboBox target, string valueMember, string displayMemeber)
        {
            target.ValueMember = valueMember;
            target.DisplayMember = displayMemeber;
        }


        /// <summary>
        /// 콤보박스의 displayValue를 기준으로 같은 아이템을 선택해준다.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="displayValue"></param>
        public static void SelectItemByDisplayMember(ComboBox target, string displayMember)
        {
            DataTable Items = target.DataSource as DataTable;

            if (Items == null)
            {
                Items = ((DataView)target.DataSource).Table;
            }

            int index = 0;

            foreach (DataRow row in Items.Rows)
            {
                if (row[target.DisplayMember].ToString() == displayMember)
                {
                    break;
                }
                index++;
            }

            target.SelectedIndex = index;
        }


        public static void SelectItemByValueMember(ComboBox target, string valueMember)
        {
            DataTable Items = target.DataSource as DataTable;

            if (Items == null)
            {
                Items = ((DataView)target.DataSource).Table;
            }

            int index = 0;

            foreach (DataRow row in Items.Rows)
            {
                if (row[target.ValueMember].ToString() == valueMember)
                {
                    break;
                }
                index++;
            }

            target.SelectedIndex = index;
        }
    }

    public class DataUtils
    {
        public static DataTable DataTableWapperClone(DataTable target)
        {
            DataTable result = new DataTable();
            foreach (DataColumn column in target.Columns)
            {
                result.Columns.Add(column.ColumnName, column.DataType);
            }

            return result;
        }

        public static DataTable GetRequstData(string valueColumn, string textColumn, string text, int interval)
        {
            DataTable result = new DataTable();
            result.Columns.Add(valueColumn);
            result.Columns.Add(textColumn);
            for (int i = 1; i <= interval; i++)
            {
                DataRow line = result.NewRow();
                line[valueColumn] = i.ToString("00");
                line[textColumn] = i.ToString() + text;
                result.Rows.Add(line);
            }

            return result;
        }

        public static Hashtable ConvertDataRowViewToHashtable(DataRowView target)
        {
            DataColumnCollection columns = target.Row.Table.Columns;

            Hashtable result = new Hashtable();

            for (int i = 0; i < columns.Count; i++)
            {
                result[columns[i].ColumnName] = target[columns[i].ColumnName];
            }

            return result;
        }


        public static double NanToZero(double value)
        {
            if (Double.IsNaN(value))
            {
                return 0;
            }

            if (value.Equals(DBNull.Value))
            {
                value = 0;
            }
            return value;
        }


        public static object NullToZero(object value)
        {
            if (value == DBNull.Value)
            {
                return 0;
            }

            return value;
        }

        public static string StringToFormatString(string value, string format)
        {
            string result = string.Empty;

            if (value.Length == 8)
            {
                if (format == "yyyy-MM-dd")
                {
                    result = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2);
                }
            }

            return result;
        }
    }
}
