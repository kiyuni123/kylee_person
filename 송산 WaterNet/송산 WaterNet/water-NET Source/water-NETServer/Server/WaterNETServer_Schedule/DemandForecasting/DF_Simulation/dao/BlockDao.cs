﻿using System.Text;
using System.Collections;
using System.Data;
using EMFrame.dm;

namespace WaterNETServer.DF_Simulation.dao
{
    /// <summary>
    /// 데이터베이스에서 센터, 정수장, 대블록, 중블록, 소블록을 검색한다.
    /// </summary>
    public class BlockDao
    {
        private static BlockDao dao = null;
        private BlockDao() { }
        public static BlockDao GetInstance()
        {
            if (dao == null)
            {
                dao = new BlockDao();
            }
            return dao;
        }

        public object SelectMiddleBlockTag(EMapper manager, string loc_code, string tag_gbn)
        {
            StringBuilder query = new StringBuilder();

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        public object SelectSmallBlockTag(EMapper manager, string loc_code, string tag_gbn)
        {
            StringBuilder query = new StringBuilder();

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        public object HasReservoir(EMapper manager, string loc_code)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select nvl((select decode(loc_gbn, '배수지', 'True') from cm_location where loc_name = a.rel_loc_name),'False')");
            query.AppendLine("  from cm_location a");
            query.Append(" where a.loc_code = '").Append(loc_code).AppendLine("'");

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        /// <summary>
        /// 센터코드와 센터명 검색
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        public DataSet SelectCenter(EMapper manager, Hashtable parameter, string dataMember)
        {
            StringBuilder query = new StringBuilder();
            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }

        /// <summary>
        /// 정수장코드와 정수장 검색
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        public DataSet SelectJungSuJang(EMapper manager, Hashtable parameter, string dataMember)
        {
            StringBuilder query = new StringBuilder();
            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }

        /// <summary>
        /// 모든 블럭 목록을 조회한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <param name="dataMember"></param>
        /// <returns></returns>
        public DataSet SelectBlockListAll(EMapper manager, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select c1.loc_code                                                                                                                                         ");
            query.AppendLine("      ,c1.sgccd                                                                                                                                            ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))                            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                                             ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                                             ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))                             ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                                              ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                                              ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                                                    ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                                         ");
            query.AppendLine("      ,c1.ftr_idn                                                                                                                                          ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                                ");
            query.AppendLine("      ,c1.kt_gbn                                                                                                                                           ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                                                     ");
            query.AppendLine("      ,c1.ord                                                                                                                                              ");
            query.AppendLine("  from                                                                                                                                                     ");
            query.AppendLine("      (                                                                                                                                                    ");
            query.AppendLine("       select sgccd                                                                                                                                        ");
            query.AppendLine("             ,loc_code                                                                                                                                     ");
            query.AppendLine("             ,ploc_code                                                                                                                                    ");
            query.AppendLine("             ,loc_name                                                                                                                                     ");
            query.AppendLine("             ,ftr_idn                                                                                                                                      ");
            query.AppendLine("             ,ftr_code                                                                                                                                     ");
            query.AppendLine("             ,rel_loc_name                                                                                                                                 ");
            query.AppendLine("             ,kt_gbn                                                                                                                                       ");
            query.AppendLine("             ,rownum ord                                                                                                                                   ");
            query.AppendLine("         from cm_location                                                                                                                                  ");
            query.AppendLine("        where 1 = 1                                                                                                                                        ");
            query.AppendLine("        start with ftr_code = 'BZ001'                                                                                                                      ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                              ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                                          ");
            query.AppendLine("      ) c1                                                                                                                                                 ");
            query.AppendLine("       ,cm_location c2                                                                                                                                     ");
            query.AppendLine("       ,cm_location c3                                                                                                                                     ");
            query.AppendLine(" where 1 = 1                                                                                                                                               ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                                       ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                                       ");
            query.AppendLine(" order by c1.ord                                                                                                                                           ");

            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }

        /// <summary>
        /// 블럭 목록을 조회한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <param name="dataMember"></param>
        /// <returns></returns>
        public DataSet SelectBlockListWhereLevel(EMapper manager, Hashtable parameter, string dataMember)
        {
            StringBuilder query = new StringBuilder();
            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }

        /// <summary>
        /// 블럭 목록을 조회한다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <param name="dataMember"></param>
        /// <returns></returns>
        public DataSet SelectBlockListWhereCode(EMapper manager, Hashtable parameter, string dataMember)
        {
            StringBuilder query = new StringBuilder();
            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }

        //해당계층조회
        public DataSet SelectBlockListByLevel(EMapper manager, Hashtable parameter, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT loc_code                                                                             ");
            query.AppendLine("      ,ploc_code                                                                            ");
            query.AppendLine("      ,loc_name                                                                             ");
            query.AppendLine("      ,ftr_idn                                                                              ");
            query.AppendLine("      ,ftr_code                                                                             ");
            query.AppendLine("      ,rel_loc_name                                                                         ");
            query.AppendLine("      ,kt_gbn                                                                               ");
            query.AppendLine("  FROM cm_location                                                                          ");
            query.AppendLine(" WHERE 1 = 1                                                                                ");


            if (parameter.ContainsKey("LOC_CODE"))
            {
                query.Append("   AND ploc_code = ").Append("'").Append(parameter["LOC_CODE"].ToString()).AppendLine("'");
            }
            else if (parameter.ContainsKey("FTR_CODE"))
            {
                query.Append("   AND ftr_code = ").Append("'").Append(parameter["FTR_CODE"].ToString()).AppendLine("'");
            }

            if (parameter.ContainsKey("FTR_CODE"))
            {
                query.Append(" START WITH ftr_code = ").Append("'").Append(parameter["FTR_CODE"].ToString()).AppendLine("'");
            }

            else if (parameter.ContainsKey("PLOC_CODE"))
            {
                query.Append(" START WITH loc_code = ").Append("'").Append(parameter["PLOC_CODE"].ToString()).AppendLine("'");
            }

            query.AppendLine(" CONNECT BY PRIOR loc_code = ploc_code  ");
            query.AppendLine(" ORDER SIBLINGS BY orderby   ");

            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }

        //해당계층조회
        public void SelectBlockListByLevel(EMapper manager, string ftr_code, DataSet dataSet, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT loc_code                                                                             ");
            query.AppendLine("      ,ploc_code                                                                            ");
            query.AppendLine("      ,loc_name                                                                             ");
            query.AppendLine("      ,ftr_idn                                                                              ");
            query.AppendLine("      ,ftr_code                                                                             ");
            query.AppendLine("      ,rel_loc_name                                                                         ");
            query.AppendLine("      ,kt_gbn                                                                               ");
            query.AppendLine("  FROM cm_location                                                                          ");
            query.AppendLine(" WHERE 1 = 1                                                                                ");

            query.Append("   AND ftr_code = ").Append("'").Append(ftr_code).AppendLine("'");
            query.Append(" START WITH ftr_code = ").Append("'").Append(ftr_code).AppendLine("'");

            query.AppendLine(" CONNECT BY PRIOR loc_code = ploc_code  ");
            query.AppendLine(" ORDER SIBLINGS BY orderby   ");

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), null);
        }

        public DataSet SelectSmallBlock(EMapper manager, Hashtable parameter, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select c1.loc_code                                                                                                                              ");
            query.AppendLine("      ,c1.sgccd                                                                                                                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))                 ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                                         ");

            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))                  ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                                         ");

            query.AppendLine("      ,c1.rel_loc_name                                                                                                                          ");
            query.AppendLine("      ,c1.ord                                                                                                                                   ");
            query.AppendLine("  from                                                                                                                                          ");
            query.AppendLine("      (                                                                                                                                         ");
            query.AppendLine("       select sgccd                                                                                                                             ");
            query.AppendLine("             ,loc_code                                                                                                                          ");
            query.AppendLine("             ,ploc_code                                                                                                                         ");
            query.AppendLine("             ,loc_name                                                                                                                          ");
            query.AppendLine("             ,ftr_idn                                                                                                                           ");
            query.AppendLine("             ,ftr_code                                                                                                                          ");
            query.AppendLine("             ,rel_loc_name                                                                                                                      ");
            query.AppendLine("             ,rownum ord                                                                                                                        ");
            query.AppendLine("         from cm_location                                                                                                                       ");
            query.AppendLine("        where 1 = 1                                                                                                                             ");
            query.AppendLine("        start with loc_code = ").Append("'").Append(parameter["LOC_CODE"].ToString()).Append("'");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                                   ");
            query.AppendLine("        order SIBLINGS by orderby                                                                                                               ");
            query.AppendLine("      ) c1                                                                                                                                      ");
            query.AppendLine("       ,cm_location c2                                                                                                                          ");
            query.AppendLine("       ,cm_location c3                                                                                                                          ");
            query.AppendLine(" where 1 = 1                                                                                                                                    ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code                                                                                                               ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code                                                                                                               ");
            query.AppendLine(" order by c1.ord                                                                                                                                ");

            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }

        public DataSet SelectBlockInfoByBlockCode(EMapper manager, Hashtable parameter, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select c1.loc_code                                                                                                                  ");
            query.AppendLine("      ,c1.sgccd                                                                                                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))     ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                      ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                      ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                             ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))      ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                       ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))            ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                             ");
            query.AppendLine("                                                                                                                                    ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                              ");
            query.AppendLine("      ,c1.ord                                                                                                                       ");
            query.AppendLine("  from                                                                                                                              ");
            query.AppendLine("      (                                                                                                                             ");
            query.AppendLine("       select sgccd                                                                                                                 ");
            query.AppendLine("             ,loc_code                                                                                                              ");
            query.AppendLine("             ,ploc_code                                                                                                             ");
            query.AppendLine("             ,loc_name                                                                                                              ");
            query.AppendLine("             ,ftr_idn                                                                                                               ");
            query.AppendLine("             ,ftr_code                                                                                                              ");
            query.AppendLine("             ,rel_loc_name                                                                                                          ");
            query.AppendLine("             ,rownum ord                                                                                                            ");
            query.AppendLine("         from cm_location                                                                                                           ");
            query.AppendLine("        where 1 = 1                                                                                                                 ");

            query.Append("          and ftr_code = ").Append("'").Append(parameter["FTR_CODE"].ToString()).AppendLine("'");
            query.Append("          and ftr_idn = ").Append("'").Append(parameter["FTR_IDN"].ToString()).AppendLine("'");
            query.Append("        start with ftr_code = ").Append("'").Append(parameter["FTR_CODE"].ToString()).AppendLine("'");

            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                       ");
            query.AppendLine("        order SIBLINGS by orderby                                                                                                   ");
            query.AppendLine("      ) c1                                                                                                                          ");
            query.AppendLine("       ,cm_location c2                                                                                                              ");
            query.AppendLine("       ,cm_location c3                                                                                                              ");
            query.AppendLine(" where 1 = 1                                                                                                                        ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                                ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                                ");
            query.AppendLine(" order by c1.ord                                                                                                                    ");

            return manager.ExecuteScriptDataSet(query.ToString(), null, dataMember);
        }
    }
}
