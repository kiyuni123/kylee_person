﻿using System;
using System.Collections;
using System.Data;


namespace WaterNETServer.WH_Common.utils
{
    public class Utils
    {
        private static Utils utils = null;
        static Random random = new Random();

        private Utils()
        {
        }

        public Utils GetInsatance()
        {
            if (utils == null)
            {
                utils = new Utils();
            }

            return utils;
        }

        //일련번호 생성
        public static String GetSerialNumber(string workFlag)
        {
            return workFlag
                    + DateTime.Now.Year.ToString()
                    + (DateTime.Now.Month.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Day.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Second.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Millisecond.ToString()).PadLeft(3, '0')
                    + (random.Next(1, 9999).ToString()).PadLeft(4, '0');
        }

        //넘겨받은 DataSet을 해체하여 Console에 출력
        public static void ShowDataSet(DataSet dSet)
        {
            IEnumerator tableEnu = dSet.Tables.GetEnumerator();

            while (tableEnu.MoveNext())
            {
                string tableName = tableEnu.Current.ToString();

                Console.WriteLine("테이블명 : " + tableName);

                for (int i = 0; i < (dSet.Tables[tableName].Rows.Count); i++)
                {
                    //DataRow row = dSet.Tables[tableName].Rows[i];

                    Console.WriteLine("=================" + i + "번째=================");

                    Object[] obj = dSet.Tables[tableName].Rows[i].ItemArray;

                    for (int j = 0; j < obj.Length; j++)
                    {
                        Console.WriteLine(obj[j]);
                    }
                }
            }
        }

        //null to string
        public static string nts(object arg)
        {
            string result = "";


            if (arg != DBNull.Value && arg != null)
            {
                result = (string)arg;
            }

            return result;
        }

        //현재시간을 반납한다.
        public static Hashtable GetTime()
        {
            Hashtable result = new Hashtable();

            // Get current time
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;
            int hour = DateTime.Now.Hour;
            int min = DateTime.Now.Minute;
            int sec = DateTime.Now.Second;

            // Format current time into string
            result.Add("hh", (hour.ToString()).PadLeft(2, '0'));
            result.Add("mm", (min.ToString()).PadLeft(2, '0'));
            result.Add("ss", (sec.ToString()).PadLeft(2, '0'));
            result.Add("hhmm", (hour.ToString()).PadLeft(2, '0') + ":" + (min.ToString()).PadLeft(2, '0'));
            result.Add("hhmmss", (hour.ToString()).PadLeft(2, '0') + ":" + (min.ToString()).PadLeft(2, '0') + ":" + (sec.ToString()).PadLeft(2, '0'));
            result.Add("yyyymmdd", year + "-" + (month.ToString()).PadLeft(2, '0') + "-" + (day.ToString()).PadLeft(2, '0'));
            result.Add("yyyymmddhhmmss", year + "-" + (month.ToString()).PadLeft(2, '0') + "-" + (day.ToString()).PadLeft(2, '0') + " " + (hour.ToString()).PadLeft(2, '0') + ":" + (min.ToString()).PadLeft(2, '0') + ":" + (sec.ToString()).PadLeft(2, '0'));
            result.Add("deleteCheck", year + "" + (month.ToString()).PadLeft(2, '0') + "" + (day.ToString()).PadLeft(2, '0') + "" + (hour.ToString()).PadLeft(2, '0') + "" + (min.ToString()).PadLeft(2, '0'));

            return result;
        }

        //연산이 필요한 시간값을 반납한다.
        public static Hashtable GetCalcTime(string flag, int offset)
        {
            Hashtable result = new Hashtable();

            int year = 0;
            int month = 0;
            int day = 0;
            int hour = 0;
            int min = 0;
            int sec = 0;

            DateTime today = DateTime.Now;

            if ("yyyy".Equals(flag))
            {
                //today.AddYears(offset);
                year = today.AddYears(offset).Year;
                month = today.AddYears(offset).Month;
                day = today.AddYears(offset).Day;
                hour = today.AddYears(offset).Hour;
                min = today.AddYears(offset).Minute;
                sec = today.AddYears(offset).Second;
            }
            else if ("mm".Equals(flag))
            {
                today.AddMonths(offset);
                year = today.AddMonths(offset).Year;
                month = today.AddMonths(offset).Month;
                day = today.AddMonths(offset).Day;
                hour = today.AddMonths(offset).Hour;
                min = today.AddMonths(offset).Minute;
                sec = today.AddMonths(offset).Second;
            }
            else if ("dd".Equals(flag))
            {
                today.AddDays(offset);
                year = today.AddDays(offset).Year;
                month = today.AddDays(offset).Month;
                day = today.AddDays(offset).Day;
                hour = today.AddDays(offset).Hour;
                min = today.AddDays(offset).Minute;
                sec = today.AddDays(offset).Second;
            }
            else if ("hh".Equals(flag))
            {
                today.AddHours(offset);
                year = today.AddHours(offset).Year;
                month = today.AddHours(offset).Month;
                day = today.AddHours(offset).Day;
                hour = today.AddHours(offset).Hour;
                min = today.AddHours(offset).Minute;
                sec = today.AddHours(offset).Second;
            }
            else if ("mi".Equals(flag))
            {
                today.AddMinutes(offset);
                year = today.AddMinutes(offset).Year;
                month = today.AddMinutes(offset).Month;
                day = today.AddMinutes(offset).Day;
                hour = today.AddMinutes(offset).Hour;
                min = today.AddMinutes(offset).Minute;
                sec = today.AddMinutes(offset).Second;
            }
            else if ("ss".Equals(flag))
            {
                today.AddSeconds(offset);
                year = today.AddSeconds(offset).Year;
                month = today.AddSeconds(offset).Month;
                day = today.AddSeconds(offset).Day;
                hour = today.AddSeconds(offset).Hour;
                min = today.AddSeconds(offset).Minute;
                sec = today.AddSeconds(offset).Second;
            }

            // Format current time into string
            result.Add("hh", (hour.ToString()).PadLeft(2, '0'));
            result.Add("mm", (min.ToString()).PadLeft(2, '0'));
            result.Add("ss", (sec.ToString()).PadLeft(2, '0'));
            result.Add("hhmmss", (hour.ToString()).PadLeft(2, '0') + ":" + (min.ToString()).PadLeft(2, '0') + ":" + (sec.ToString()).PadLeft(2, '0'));
            result.Add("yyyymmdd", year + "-" + (month.ToString()).PadLeft(2, '0') + "-" + (day.ToString()).PadLeft(2, '0'));
            result.Add("yyyymmddhhmm", year + "-" + (month.ToString()).PadLeft(2, '0') + "-" + (day.ToString()).PadLeft(2, '0') + " " + (hour.ToString()).PadLeft(2, '0') + ":" + (min.ToString()).PadLeft(2, '0'));
            result.Add("yyyymmddhhmmss", year + "-" + (month.ToString()).PadLeft(2, '0') + "-" + (day.ToString()).PadLeft(2, '0') + " " + (hour.ToString()).PadLeft(2, '0') + ":" + (min.ToString()).PadLeft(2, '0') + ":" + (sec.ToString()).PadLeft(2, '0'));
            result.Add("resultTimeStamp", year + "" + (month.ToString()).PadLeft(2, '0') + "" + (day.ToString()).PadLeft(2, '0') + "" + (hour.ToString()).PadLeft(2, '0') + "" + (min.ToString()).PadLeft(2, '0'));
            result.Add("TARGETDATE", year + "" + (month.ToString()).PadLeft(2, '0') + "" + (day.ToString()).PadLeft(2, '0') + "" + (hour.ToString()).PadLeft(2, '0') + "" + (min.ToString()).PadLeft(2, '0'));
            result.Add("checkDate", year + "" + (month.ToString()).PadLeft(2, '0') + "" + (day.ToString()).PadLeft(2, '0'));

            return result;
        }

        //현재 요일을 반납한다.
        public static Hashtable GetDayOfWeek()
        {
            DayOfWeek today = DateTime.Now.DayOfWeek;
            string dayOfWeekString = "";
            string dayOfWeekNumber = "";

            if ("Monday".Equals(today.ToString()))
            {
                dayOfWeekString = "월";
                dayOfWeekNumber = "2";
            }
            else if ("Tuesday".Equals(today.ToString()))
            {
                dayOfWeekString = "화";
                dayOfWeekNumber = "3";
            }
            else if ("Wednesday".Equals(today.ToString()))
            {
                dayOfWeekString = "수";
                dayOfWeekNumber = "4";
            }
            else if ("Thursday".Equals(today.ToString()))
            {
                dayOfWeekString = "목";
                dayOfWeekNumber = "5";
            }
            else if ("Friday".Equals(today.ToString()))
            {
                dayOfWeekString = "금";
                dayOfWeekNumber = "6";
            }
            else if ("Saturday".Equals(today.ToString()))
            {
                dayOfWeekString = "토";
                dayOfWeekNumber = "7";
            }
            else if ("Sunday".Equals(today.ToString()))
            {
                dayOfWeekString = "일";
                dayOfWeekNumber = "1";
            }

            Hashtable result = new Hashtable();

            result.Add("dayString", dayOfWeekString);
            result.Add("dayNumber", dayOfWeekNumber);

            return result;
        }
    }
}
