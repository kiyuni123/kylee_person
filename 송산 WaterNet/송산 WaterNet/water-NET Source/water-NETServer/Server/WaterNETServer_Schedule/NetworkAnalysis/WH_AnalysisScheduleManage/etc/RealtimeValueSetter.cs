﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;

using WaterNETServer.Common.DB.Oracle;
using WaterNETServer.WH_AnalysisScheduleManage.dao;

using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;

namespace WaterNETServer.WH_AnalysisScheduleManage.etc
{
    public class RealtimeValueSetter : BaseWork
    {
        private static RealtimeValueSetter realtimeValueSetter = null;
        private AnalysisScheduleManageDao dao = null;

        private RealtimeValueSetter()
        {
            dao = AnalysisScheduleManageDao.GetInstance();
        }

        public static RealtimeValueSetter GetInstance()
        {
            if (realtimeValueSetter == null)
            {
                realtimeValueSetter = new RealtimeValueSetter();
            }

            return realtimeValueSetter;
        }

        //업무별 실시간 계측값을 반환한다. (모델번호의 접두어 2자리를 이용한다.)
        public Hashtable GetRealtimeValue(Hashtable conditions)
        {
            Hashtable result = new Hashtable();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                if ("WH".Equals(((string)conditions["INP_NUMBER"]).Substring(0, 2)))
                {
                    //수리
                    Hashtable demandOnJunctions = new Hashtable();
                    Hashtable levelOnReservoir = new Hashtable();
                    Hashtable pressureOnValves = new Hashtable();
                    Hashtable pumpStatusValves = new Hashtable(); //펌프 미가동정보
                    //블록별 최종시간의 순시유량과 BaseDemand 총량 조회
                    DataSet flowSet = dao.SelectRealtimeFlow(manager, conditions);

                    if (flowSet.Tables["realtimeFlow"].Rows.Count == 0)
                    {
                        //실시간 데이터가 없다면 종료
                        return null;
                    }

                    Hashtable junctionConditions = null;
                    Hashtable largeConsumerInfo = null;

                    //블록별 절점정보 조회
                    foreach (DataRow flowData in flowSet.Tables["realtimeFlow"].Rows)
                    {
                        //해당 블록정보의 Junction List를 조회
                        junctionConditions = new Hashtable();

                        junctionConditions.Add("POSITION_INFO", flowData["BSM_CDE"] + "|" + flowData["BSM_IDN"]);
                        junctionConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());

                        DataSet junctionSet = dao.SelectJunctionListInBlockExceptLargeConsumer(manager, junctionConditions);

                        //넘겨받은 인자 중 시간데이터 발췌
                        Hashtable timeData = (Hashtable)conditions["timeData"];

                        //해당 블록의 대수용가 정보 연산
                        Hashtable largeConsumerConditions = new Hashtable();

                        largeConsumerConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        largeConsumerConditions.Add("LOC_CODE", flowData["LOC_CODE"].ToString());
                        largeConsumerConditions.Add("FTR_CODE", flowData["BSM_CDE"].ToString());
                        largeConsumerConditions.Add("FTR_IDN", flowData["BSM_IDN"].ToString());
                        largeConsumerConditions.Add("TARGETDATE", timeData["yyyymmddhhmm"]);

                        largeConsumerInfo = ApplyLageConsumer(manager, largeConsumerConditions);

                        //절점별 사용량 배분
                        foreach (DataRow junctionData in junctionSet.Tables["WH_JUNCTIONS"].Rows)
                        {
                            decimal demand = decimal.Round(decimal.Parse((string)junctionData["DEMAND"]), 3);
                            decimal sumFlow = 0;

                            if (largeConsumerInfo["largeConsumerList"] != null)
                            {
                                //실측유량단위가 ton/hour라서 ton/day로 보정하기위해 24를 곱한다
                                //sumFlow = (decimal.Parse((string)flowData["VALUE"]) * 24) - decimal.Parse(largeConsumerInfo["sumAppliedIncrease"].ToString());
                                sumFlow = decimal.Parse((string)flowData["VALUE"]) - decimal.Parse(largeConsumerInfo["sumAppliedIncrease"].ToString());
                            }
                            else
                            {
                                //실측유량단위가 ton/hour라서 ton/day로 보정하기위해 24를 곱한다
                                //sumFlow = decimal.Round((decimal.Parse((string)flowData["VALUE"]) * 24), 3);
                                sumFlow = decimal.Round((decimal.Parse((string)flowData["VALUE"])) , 3);
                            }

                            decimal sumBaseDemand = decimal.Round(decimal.Parse((string)flowData["SUM_BASEDEMAND"]), 3);

                            if (sumBaseDemand != 0)
                            {
                                if (sumFlow == 0)
                                {
                                    demandOnJunctions.Add((string)junctionData["ID"], "0");
                                }
                                else
                                {
                                    decimal deviedDemand = (decimal.Round((demand * sumFlow) / sumBaseDemand, 3));
                                    demandOnJunctions.Add((string)junctionData["ID"], deviedDemand.ToString());
                                }
                            }
                        }
                    }

                    //펌프의 상태정보 조회(미가동만)
                    DataSet pumpStatus = dao.SelectPumpStatusValue(manager, conditions);
                    foreach (DataRow row in pumpStatus.Tables["pump_status"].Rows)
                    {
                        pumpStatusValves.Add(row["ID"].ToString(), row["VALUE"].ToString());
                    }

                    //배수지의 수위 조회
                    DataSet reservoirLevelSet = dao.SelectReservoirLevel(manager, conditions);

                    //배수지별 수위
                    foreach (DataRow row in reservoirLevelSet.Tables["reservoir_level"].Rows)
                    {
                        levelOnReservoir.Add(row["ID"].ToString(), row["VALUE"].ToString());
                    }

                    //PRV의 압력 조회
                    DataSet valvePressureSet = dao.SelectValvePressure(manager, conditions);

                    //PRV별 압력
                    foreach (DataRow row in valvePressureSet.Tables["valve_pressure"].Rows)
                    {
                        pressureOnValves.Add(row["ID"].ToString(), row["VALUE"].ToString());
                    }

                    //사용량 배분 결과가 있을때 저장
                    if (demandOnJunctions.Count > 0)
                    {
                        result.Add("demand", demandOnJunctions);
                    }

                    //대수용가 결과가 있을때 저장
                    if (largeConsumerInfo["largeConsumerList"] != null)
                    {
                        result.Add("largeConsumer", largeConsumerInfo["largeConsumerList"]);
                    }

                    //펌프미가동 결과가 있을때 저장
                    if (pumpStatusValves.Count > 0)
                    {
                        result.Add("status", pumpStatusValves);
                    }

                    //배수지 수위 결과가 있을때 저장
                    if (levelOnReservoir.Count > 0)
                    {
                        result.Add("reservoir", levelOnReservoir);
                    }

                    //PRV 압력 결과가 있을때 저장
                    if (pressureOnValves.Count > 0)
                    {
                        result.Add("valve", pressureOnValves);
                    }
                }
                else if ("EN".Equals(((string)conditions["INP_NUMBER"]).Substring(0, 2)))
                {
                    //에너지
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //대수용가 사용량 처리
        public Hashtable ApplyLageConsumer(EMapper manager, Hashtable conditions)
        {
            AnalysisScheduleManageDao dao = AnalysisScheduleManageDao.GetInstance();

            //활성화 대수용가
            ArrayList activateLargeConsumerList = new ArrayList();

            string hydraCycle = "";

            //해당블록내 대수용가 리스트 조회 (현재 적용리스트에 등록된 내용 제외)
            Hashtable largeConsumerConditions = new Hashtable();

            largeConsumerConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
            largeConsumerConditions.Add("APPLY_YN", "Y");
            largeConsumerConditions.Add("FTR_CODE", conditions["FTR_CODE"].ToString());
            largeConsumerConditions.Add("FTR_IDN", conditions["FTR_IDN"].ToString());

            DataSet largeConsumerDataSet = dao.SelectAppliableLargeConsumerList(manager, largeConsumerConditions);
            DataTable largeConsumerDataTable = largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"];

            if (largeConsumerDataTable.Rows.Count != 0)
            {
                ArrayList applyLargeConsumerList = new ArrayList();

                //대수용가 정보가 존재할때만 동작
                //Console.WriteLine("대수용가 리스트 조회완료 : " + largeConsumerDataTable.Rows.Count + " 건");

                //해당 모델의 해석 주기 조회
                Hashtable cycleConditions = new Hashtable();

                cycleConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());

                DataSet cycleDataSet = dao.SelectHydraCycle(manager, cycleConditions);

                if (cycleDataSet.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    hydraCycle = cycleDataSet.Tables["WH_HYDRA_SCHEDULE"].Rows[0]["HYDRA_CYCLE"].ToString();

                    //Console.WriteLine("해석주기 조회 완료 : " + hydraCycle);
                }

                //특정 블록의 Target Date로 부터 15분 전 1분간격 유량 데이터를 조회한다.
                Hashtable flowHistoryConditions = new Hashtable();

                flowHistoryConditions.Add("LOC_CODE", conditions["LOC_CODE"].ToString());
                flowHistoryConditions.Add("TARGETDATE", conditions["TARGETDATE"].ToString());
                flowHistoryConditions.Add("HYDRA_CYCLE", hydraCycle);

                DataSet flowHistoryDataSet = dao.SelectFlowHistoryByAnalysis(manager, flowHistoryConditions);

                //15분 전까지의 유량 히스토리가 적어도 10개 이상인경우만 동작 (증감동작 검출용)
                if (flowHistoryDataSet.Tables["IF_IHTAGS"].Rows.Count >= 10)
                {
                    //활성화 대수용가 조회 및 추가
                    Hashtable savedActivateConditions = new Hashtable();
                    savedActivateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                    savedActivateConditions.Add("FTR_CODE", conditions["FTR_CODE"].ToString());
                    savedActivateConditions.Add("FTR_IDN", conditions["FTR_IDN"].ToString());

                    DataSet savedActivateDataSet = dao.SelectActivateLargeConsumerList(manager, savedActivateConditions);

                    foreach (DataRow savedRow in savedActivateDataSet.Tables["WH_LCONSUMER_ACTIVATE"].Rows)
                    {
                        Hashtable savedActivateConsumer = new Hashtable();

                        savedActivateConsumer.Add("activateTime", savedRow["ACTIVATE_TIME"].ToString());
                        savedActivateConsumer.Add("ID", savedRow["ID"].ToString());
                        savedActivateConsumer.Add("status", savedRow["STATUS"].ToString());
                        savedActivateConsumer.Add("consumerData", savedRow);

                        activateLargeConsumerList.Add(savedActivateConsumer);
                    }

                    //현재유량
                    double targetFlow = 0;
                    //증가량
                    double increaseValue = 0;
                    //감소량
                    double decreaseValue = 0;
                    //누적증가량
                    double sumIncreaseValue = 0;
                    //누적감소량
                    double sumDecreaseValue = 0;
                    //직전유량
                    double beforFlow = 0;
                    //차이값
                    double valueGap = 0;
                    //증감상태
                    string variationState = "";

                    foreach (DataRow row in flowHistoryDataSet.Tables["IF_IHTAGS"].Rows)
                    {
                        //현재 존재하는 대수용가 정보 중 최소 증가량을 조회
                        double minIncrease = 0;

                        //Console.WriteLine("현재 대수용가 리스트 사이즈 : " + largeConsumerDataTable.Rows.Count);

                        foreach (DataRow tmpConsumerRow in largeConsumerDataTable.Rows)
                        {
                            if (minIncrease == 0 || minIncrease > double.Parse(tmpConsumerRow["APPLY_INCREASE"].ToString()))
                            {
                                minIncrease = double.Parse(tmpConsumerRow["APPLY_INCREASE"].ToString());
                            }
                        }

                        targetFlow = double.Parse(row["VALUE"].ToString());

                        double activateSumFlow = 0;

                        for (int i = 0; i < activateLargeConsumerList.Count; i++)
                        {
                            //활성화 대수용가

                            DataRow tmp = (DataRow)((Hashtable)activateLargeConsumerList[i])["consumerData"];

                            activateSumFlow = activateSumFlow + double.Parse(tmp["APPLY_INCREASE"].ToString());
                        }

                        if (beforFlow == 0)
                        {
                            beforFlow = targetFlow;
                        }

                        valueGap = Math.Abs(beforFlow - targetFlow);

                        if (beforFlow / 100 * 5 > valueGap)
                        {
                            //변화폭이 이전유량의 5% 미만인 경우는 상태를 유지하고 수치연산만 해준다.
                            if ("I".Equals(variationState))
                            {
                                increaseValue = targetFlow - beforFlow;
                                sumIncreaseValue = sumIncreaseValue + increaseValue;
                            }
                            else if ("D".Equals(variationState))
                            {
                                decreaseValue = targetFlow - beforFlow;
                                sumDecreaseValue = sumDecreaseValue + decreaseValue;
                            }
                        }
                        else
                        {
                            //변화폭이 이전유량의 5% 이상인 경우는 정상처리
                            if (beforFlow < targetFlow)
                            {
                                //증가
                                #region 증가로직
                                //감소량 초기화
                                decreaseValue = 0;
                                sumDecreaseValue = 0;

                                increaseValue = targetFlow - beforFlow;
                                sumIncreaseValue = sumIncreaseValue + increaseValue;

                                //Console.WriteLine("증가 : " + increaseValue + " ==누적증가량 : " + sumIncreaseValue);

                                variationState = "I";

                                if (sumIncreaseValue >= minIncrease)
                                {
                                    //Console.WriteLine("대수용가 동작 - 최소유량 : " + minIncrease +" 누적증가량 : " + sumIncreaseValue + " 시간 : " + row["TIMESTAMP"]);

                                    //대수용가 동작 조건을 맞췄다면 증가량에 알맞는 대수용가의 조합을 찾아낸다.

                                    //가장 부합하는 대수용가 정보
                                    Hashtable fitLargeConsumerData = new Hashtable();

                                    string tmpKey = "";

                                    //Console.WriteLine("대수용가 건수 : " + largeConsumerDataTable.Rows.Count);

                                    //데이터 복제
                                    DataSet clonedDataSet = largeConsumerDataSet.Copy();
                                    DataTable clonedDataTable = clonedDataSet.Tables["WH_LCONSUMER_INFO"];

                                    for (int i = 0; i < clonedDataTable.Rows.Count; i++)
                                    {
                                        ArrayList tmp = new ArrayList();

                                        tmp.Add(clonedDataTable.Rows[i]);

                                        tmpKey = "";

                                        for (int tmpIdx = 0; tmpIdx < tmp.Count; tmpIdx++)
                                        {
                                            tmpKey = tmpKey + ((DataRow)tmp[tmpIdx])["IDX"];
                                        }

                                        //입력하는 키가 중복이라면 패스한다.
                                        if (fitLargeConsumerData[tmpKey] == null)
                                        {
                                            fitLargeConsumerData.Add(tmpKey, tmp.Clone());
                                        }

                                        for (int j = 1; j < clonedDataTable.Rows.Count; j++)
                                        {
                                            for (int k = i + j; k < clonedDataTable.Rows.Count; k++)
                                            {
                                                ArrayList subTmp = (ArrayList)tmp.Clone();

                                                tmp.Add(clonedDataTable.Rows[k]);

                                                tmpKey = "";

                                                for (int tmpIdx = 0; tmpIdx < tmp.Count; tmpIdx++)
                                                {
                                                    tmpKey = tmpKey + ((DataRow)tmp[tmpIdx])["IDX"];
                                                }

                                                //입력하는 키가 중복이라면 패스한다.
                                                if (fitLargeConsumerData[tmpKey] == null)
                                                {
                                                    fitLargeConsumerData.Add(tmpKey, tmp.Clone());
                                                }

                                                if (k + 1 < clonedDataTable.Rows.Count)
                                                {
                                                    subTmp.Add(clonedDataTable.Rows[k + 1]);

                                                    tmpKey = "";

                                                    for (int tmpIdx = 0; tmpIdx < tmp.Count; tmpIdx++)
                                                    {
                                                        tmpKey = tmpKey + ((DataRow)subTmp[tmpIdx])["IDX"];
                                                    }

                                                    //입력하는 키가 중복이라면 패스한다.
                                                    if (fitLargeConsumerData[tmpKey] == null)
                                                    {
                                                        fitLargeConsumerData.Add(tmpKey, subTmp.Clone());
                                                    }
                                                }
                                            }

                                            tmp.Clear();
                                            tmp.Add(clonedDataTable.Rows[i]);
                                        }
                                    }

                                    //Console.WriteLine("대수용가 수요조합 산정 완료 : " + fitLargeConsumerData.Count);

                                    //산출된 조합 중 증가량에 가장 근접한(나머지가 가장 작은) 조합을 산출.
                                    string fittestKey = "";
                                    double fittestFlowGap = 0;

                                    //조건에 맞는 대수용가 집합
                                    Hashtable fitLargeConsumer = new Hashtable();

                                    //최적조건 산출
                                    foreach (string key in fitLargeConsumerData.Keys)
                                    {
                                        ArrayList tmp = (ArrayList)fitLargeConsumerData[key];

                                        double sumIncreaseLargeConsumer = 0;

                                        for (int i = 0; i < tmp.Count; i++)
                                        {
                                            DataRow tmpRow = (DataRow)tmp[i];
                                            sumIncreaseLargeConsumer = sumIncreaseLargeConsumer + double.Parse(tmpRow["APPLY_INCREASE"].ToString());
                                        }

                                        if (sumIncreaseValue > sumIncreaseLargeConsumer)
                                        {
                                            double flowGap = sumIncreaseValue - sumIncreaseLargeConsumer;

                                            if ("".Equals(fittestKey))
                                            {
                                                //Console.WriteLine("==최초등록 : " + key + " 차이유량 : " + flowGap);

                                                fittestKey = key;
                                                fittestFlowGap = flowGap;
                                            }

                                            if (!"".Equals(fittestKey) && fittestFlowGap > flowGap)
                                            {
                                                //Console.WriteLine("==최적연산 : " + key + " 차이유량 : " + flowGap);

                                                fittestKey = key;
                                                fittestFlowGap = flowGap;
                                            }
                                        }
                                    }

                                    //Console.WriteLine("최적조건 산출완료 - key : " + fittestKey + " 차이유량 : " + fittestFlowGap);

                                    //조건에 맞는 대수용가 집합 중 최적조건이 복수개 존재하는 경우를 상정하여 동작패턴으로 우선순위를 구분하고 같은 우선순위인 경우 랜덤으로 선택
                                    ArrayList fittestLargeConsumerList = new ArrayList();

                                    //가장높은 부함도 점수
                                    int topFitGrade = 0;

                                    //최적 차이유량에 부합하는 대수용가 조합 조회
                                    foreach (string key in fitLargeConsumerData.Keys)
                                    {
                                        ArrayList tmp = (ArrayList)fitLargeConsumerData[key];

                                        double sumIncreaseLargeConsumer = 0;

                                        for (int i = 0; i < tmp.Count; i++)
                                        {
                                            DataRow largeConsumer = (DataRow)tmp[i];
                                            sumIncreaseLargeConsumer = sumIncreaseLargeConsumer + double.Parse(largeConsumer["APPLY_INCREASE"].ToString());
                                        }

                                        double flowGap = sumIncreaseValue - sumIncreaseLargeConsumer;

                                        //최적량과 차이유량이 같은경우 (적용대상 조합)
                                        if (fittestFlowGap == flowGap)
                                        {
                                            Hashtable fitTmp = new Hashtable();

                                            fitTmp.Add("key", key);
                                            fitTmp.Add("flogGap", flowGap);
                                            fitTmp.Add("consumerList", tmp.Clone());

                                            //시간정보
                                            //Hashtable analysisTimeData = Utils.GetTime();

                                            //부합도
                                            int fitGrade = 0;
                                            //대수용가 조합 수수량
                                            double sumIncrease = 0;
                                            //가장 짧은 발동시간
                                            int minIncreaseTime = 0;

                                            for (int i = 0; i < tmp.Count; i++)
                                            {
                                                DataRow largeConsumerRow = (DataRow)tmp[i];

                                                Hashtable settingConditions = new Hashtable();

                                                settingConditions.Add("time", (row["TIMESTAMP"].ToString()).Substring(11, 2));
                                                settingConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                                                settingConditions.Add("ID", largeConsumerRow["ID"]);
                                                settingConditions.Add("analysisTime", (row["TIMESTAMP"].ToString()).Substring(0, 10));

                                                DataTable dTable = dao.SelectLargeConsumerSetting(manager, settingConditions).Tables["WH_LCONSUMER_TIME"];

                                                if (dTable.Rows.Count > 0)
                                                {
                                                    if ("Y".Equals(dTable.Rows[0][0].ToString()))
                                                    {
                                                        //현재날짜 시간이 동작조건에 부합하는 경우 (점수를 1점 올려준다.)
                                                        fitGrade = fitGrade + 1;
                                                    }
                                                }

                                                sumIncrease = sumIncrease + double.Parse(largeConsumerRow["APPLY_INCREASE"].ToString());

                                                if (minIncreaseTime == 0 || minIncreaseTime > int.Parse(largeConsumerRow["APPLY_CONTINUE_TIME"].ToString()))
                                                {
                                                    minIncreaseTime = int.Parse(largeConsumerRow["APPLY_CONTINUE_TIME"].ToString());
                                                }
                                            }

                                            fitTmp.Add("fitGrade", fitGrade);
                                            fitTmp.Add("sumIncrease", sumIncrease);
                                            fitTmp.Add("minIncreaseTime", minIncreaseTime);

                                            //가장 높은 부합도 점수 입력
                                            if (topFitGrade == 0 || topFitGrade < fitGrade)
                                            {
                                                topFitGrade = fitGrade;
                                            }

                                            fittestLargeConsumerList.Add((Hashtable)fitTmp.Clone());
                                        }
                                    }

                                    //Console.WriteLine("최적조건에 부합하는 조합 리스트 조회 완료 : " + fittestLargeConsumerList.Count + " 최대 부합도 : " + topFitGrade);

                                    //최대 부합도를 가진 수용가 조합
                                    ArrayList topGradeConsumer = new ArrayList();

                                    //최대부합도를 가진 대수용가 조합을 산출
                                    if (fittestLargeConsumerList.Count > 1)
                                    {
                                        //최대 부합도를 가진 조합을 산출
                                        for (int i = 0; i < fittestLargeConsumerList.Count; i++)
                                        {
                                            Hashtable tmp = (Hashtable)((Hashtable)fittestLargeConsumerList[i]).Clone();

                                            //최대 부합도를 가진 대수용가 조합만 따로 저장
                                            if (((int)tmp["fitGrade"]) == topFitGrade)
                                            {
                                                topGradeConsumer.Add(tmp.Clone());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        topGradeConsumer.Add(((Hashtable)fittestLargeConsumerList[0]).Clone());
                                    }

                                    //최종적으로 선정된 최적합 대수용가 조합
                                    Hashtable finalfitLargeConsumer = null;

                                    if (topGradeConsumer.Count > 1)
                                    {
                                        //Console.WriteLine("최대부합도를 가진 대수용가 정보가 복수개 존재 : " + topGradeConsumer.Count + "개");

                                        //최대부합도를 가진 조합이 복수개인 경우 랜덤으로 선택
                                        Random random = new Random();
                                        int randomIdx = random.Next(0, topGradeConsumer.Count - 1);

                                        finalfitLargeConsumer = (Hashtable)((Hashtable)topGradeConsumer[randomIdx]).Clone();
                                    }
                                    else
                                    {
                                        //최대부합도를 가진 조합이 1개인 경우
                                        finalfitLargeConsumer = (Hashtable)((Hashtable)topGradeConsumer[0]).Clone();
                                    }

                                    //Console.WriteLine("최종 적용 대수용가 산정완료 - key : " + finalfitLargeConsumer["key"] + " 최소증가시간 : " + finalfitLargeConsumer["minIncreaseTime"] + " 부합도 : " + finalfitLargeConsumer["fitGrade"] + " 증가량 합 : " + finalfitLargeConsumer["sumIncrease"]);

                                    //해당 조합의 가장 작은 증가시간동안 대수용가 수수량 조합만큼의 차이량이 발생하는지 확인
                                    DateTime analysisTime = DateTime.Parse(row["TIMESTAMP"].ToString());

                                    bool confirm = false;

                                    for (int i = 0; i < (int)finalfitLargeConsumer["minIncreaseTime"]; i++)
                                    {
                                        //1분씩 이전 데이터를 찾아서 차이량에 부합하는지 확인
                                        DateTime compareTime = analysisTime.AddMinutes(-(i + 1));
                                        string compareTimeString = compareTime.Year + "-" + compareTime.Month + "-" + compareTime.Day + " " + compareTime.Hour + ":" + compareTime.Minute;

                                        foreach (DataRow tmpRow in flowHistoryDataSet.Tables["IF_IHTAGS"].Rows)
                                        {
                                            if (compareTimeString.Equals(tmpRow["TIMESTAMP"].ToString()))
                                            {
                                                double compareFlow = double.Parse(tmpRow["VALUE"].ToString());

                                                //Console.WriteLine((i + 1) + "분전 데이터 비교 - 현재유량 " + targetFlow + " 비교유량 : " + compareFlow + " 차이량 " + (targetFlow - compareFlow));

                                                //현재유량과의 차이 비교
                                                if (targetFlow - compareFlow > (double)finalfitLargeConsumer["sumIncrease"])
                                                {
                                                    confirm = true;

                                                    //적합 판정이면 실행대기 대수용가 리스트에 추가한다.
                                                    ArrayList consumerList = (ArrayList)((ArrayList)finalfitLargeConsumer["consumerList"]).Clone();

                                                    for (int j = 0; j < consumerList.Count; j++)
                                                    {
                                                        //해석시간 문자열
                                                        string analysisString = analysisTime.Year + "-" + analysisTime.Month + "-" + analysisTime.Day + " " + analysisTime.Hour + ":" + analysisTime.Minute;

                                                        Hashtable activateLargeConsumerData = new Hashtable();
                                                        activateLargeConsumerData.Add("activateTime", analysisString);
                                                        activateLargeConsumerData.Add("ID", ((DataRow)consumerList[j])["ID"].ToString());
                                                        activateLargeConsumerData.Add("status", "S");
                                                        activateLargeConsumerData.Add("consumerData", (DataRow)consumerList[j]);

                                                        activateLargeConsumerList.Add(activateLargeConsumerData.Clone());

                                                        //적용된 대수용가 수수량을 누적증가량에서 제한다.
                                                        sumIncreaseValue = sumIncreaseValue - double.Parse(((DataRow)consumerList[j])["APPLY_INCREASE"].ToString());

                                                        //적용된 대수용가는 대수용가 리스트에서 제외한다.
                                                        for (int k = 0; k < largeConsumerDataTable.Rows.Count; k++)
                                                        {
                                                            if ((((DataRow)consumerList[j])["ID"].ToString()).Equals(largeConsumerDataTable.Rows[k]["ID"].ToString()))
                                                            {
                                                                largeConsumerDataTable.Rows[k].Delete();
                                                                largeConsumerDataTable.AcceptChanges();
                                                                //Console.WriteLine("적용 대수용가 삭제 : " + largeConsumerDataTable.Rows.Count);
                                                            }
                                                        }
                                                    }

                                                    break;
                                                }
                                            }
                                        }

                                        if (confirm)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            //모든조건에 부합하지만 증가시간에 부합하지 않는다면 완만한 증가를 나타내며 누적 증가량을 초기화한다.
                                            sumIncreaseValue = 0;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else if (beforFlow > targetFlow)
                            {
                                #region 감소로직
                                //감소

                                //증가량 초기화
                                increaseValue = 0;
                                sumIncreaseValue = 0;
                                variationState = "D";

                                decreaseValue = targetFlow - beforFlow;
                                sumDecreaseValue = sumDecreaseValue + decreaseValue;

                                //Console.WriteLine("감소 - 감소량 : " + decreaseValue + " 누적감소량 : " + sumDecreaseValue);

                                ////감소로직 수행을 위한 사전작업 (적용대기/적용중인 대수용가의 ID만을 가진 ArrayList 생성)
                                ArrayList decreaseConsumerIdList = new ArrayList();

                                ////동작중인 대수용가의 최소증가량 발췌 (감소로직을 가동할지 여부를 파악하기 위함)
                                double minIncreaseValue = 0;

                                //동작중 항목에서 ID만 발췌해서 추가
                                for (int i = 0; i < activateLargeConsumerList.Count; i++)
                                {
                                    double applyIncrease = double.Parse(((DataRow)((Hashtable)activateLargeConsumerList[i])["consumerData"])["APPLY_INCREASE"].ToString());

                                    if (minIncreaseValue == 0 || minIncreaseValue > applyIncrease)
                                    {
                                        minIncreaseValue = applyIncrease;
                                    }
                                }

                                if (Math.Abs(sumDecreaseValue) >= minIncreaseValue)
                                {
                                    //동작중인 대수용가의 최소 증가량보다 큰폭으로 감소했을 경우에만 발동한다.
                                    //Console.WriteLine("대폭감소 - 최소 증가량 : " + minIncreaseValue + " 누적증가량 절대치 : " + Math.Abs(sumDecreaseValue));

                                    //동작중인 대수용가 정보의 조합을 생성
                                    ArrayList clonedActivationConsumerList = new ArrayList();
                                    clonedActivationConsumerList = (ArrayList)activateLargeConsumerList.Clone();

                                    Hashtable cancelConsumer = new Hashtable();
                                    string tmpKey = "";

                                    //Console.WriteLine("동작중 대수용가 복사본 - 사이즈 : " + clonedActivationConsumerList.Count);

                                    for (int i = 0; i < clonedActivationConsumerList.Count; i++)
                                    {
                                        ArrayList tmp = new ArrayList();

                                        tmp.Add(clonedActivationConsumerList[i]);

                                        tmpKey = "";

                                        for (int tmpIdx = 0; tmpIdx < tmp.Count; tmpIdx++)
                                        {
                                            tmpKey = tmpKey + ((Hashtable)tmp[tmpIdx])["ID"];
                                        }

                                        //입력하는 키가 중복이라면 패스한다.
                                        if (cancelConsumer[tmpKey] == null)
                                        {
                                            cancelConsumer.Add(tmpKey, tmp.Clone());
                                        }

                                        for (int j = 1; j < clonedActivationConsumerList.Count; j++)
                                        {
                                            for (int k = i + j; k < clonedActivationConsumerList.Count; k++)
                                            {
                                                ArrayList subTmp = (ArrayList)tmp.Clone();

                                                tmp.Add(clonedActivationConsumerList[k]);

                                                tmpKey = "";

                                                for (int tmpIdx = 0; tmpIdx < tmp.Count; tmpIdx++)
                                                {
                                                    tmpKey = tmpKey + ((Hashtable)tmp[tmpIdx])["ID"];
                                                }

                                                //입력하는 키가 중복이라면 패스한다.
                                                if (cancelConsumer[tmpKey] == null)
                                                {
                                                    cancelConsumer.Add(tmpKey, tmp.Clone());
                                                }

                                                if (k + 1 < clonedActivationConsumerList.Count)
                                                {
                                                    subTmp.Add(clonedActivationConsumerList[k + 1]);

                                                    tmpKey = "";

                                                    for (int tmpIdx = 0; tmpIdx < tmp.Count; tmpIdx++)
                                                    {
                                                        tmpKey = tmpKey + ((Hashtable)subTmp[tmpIdx])["ID"];
                                                    }

                                                    //입력하는 키가 중복이라면 패스한다.
                                                    if (cancelConsumer[tmpKey] == null)
                                                    {
                                                        cancelConsumer.Add(tmpKey, subTmp.Clone());
                                                    }
                                                }
                                            }

                                            tmp.Clear();
                                            tmp.Add(clonedActivationConsumerList[i]);
                                        }
                                    }

                                    //조합 중 감소량과 차에서 가장 최적인 유량을 선정.
                                    double fitFlowGap = 0;

                                    foreach (string key in cancelConsumer.Keys)
                                    {
                                        ArrayList tmp = (ArrayList)cancelConsumer[key];

                                        double tmpIncreaseValue = 0;

                                        for (int i = 0; i < tmp.Count; i++)
                                        {
                                            tmpIncreaseValue = tmpIncreaseValue + double.Parse(((DataRow)((Hashtable)tmp[i])["consumerData"])["APPLY_INCREASE"].ToString());
                                        }

                                        if (Math.Abs(sumDecreaseValue) - tmpIncreaseValue > 0)
                                        {
                                            if (fitFlowGap == 0 || fitFlowGap > Math.Abs(sumDecreaseValue) - tmpIncreaseValue)
                                            {
                                                fitFlowGap = Math.Abs(sumDecreaseValue) - tmpIncreaseValue;
                                            }
                                        }
                                    }

                                    //Console.WriteLine("최적오차조합과 연산결과 : " + fitFlowGap);

                                    ArrayList fitConsumerList = new ArrayList();

                                    //증가량 합
                                    double consumerDecreaseSum = 0;

                                    //가장 최적인 유량을 가진 조합을 찾아낸다.
                                    foreach (string key in cancelConsumer.Keys)
                                    {
                                        ArrayList tmp = (ArrayList)cancelConsumer[key];

                                        double tmpIncreaseValue = 0;

                                        for (int i = 0; i < tmp.Count; i++)
                                        {
                                            tmpIncreaseValue = tmpIncreaseValue + double.Parse(((DataRow)((Hashtable)tmp[i])["consumerData"])["APPLY_INCREASE"].ToString());
                                        }

                                        if (fitFlowGap == Math.Abs(sumDecreaseValue) - tmpIncreaseValue)
                                        {
                                            //최적조건과 부합
                                            fitConsumerList.Add(((ArrayList)cancelConsumer[key]).Clone());
                                            consumerDecreaseSum = tmpIncreaseValue;
                                        }

                                    }

                                    //Console.WriteLine("다시 찾은 증가량 합 : " + consumerDecreaseSum);

                                    ArrayList finalfitLargeConsumer = new ArrayList();

                                    //최적조건에 부합하는 조합이 복수개 존재한다면 랜덤으로 선택한다.
                                    if (fitConsumerList.Count > 1)
                                    {
                                        Random random = new Random();
                                        int randomIdx = random.Next(0, fitConsumerList.Count - 1);

                                        finalfitLargeConsumer = (ArrayList)((ArrayList)fitConsumerList[randomIdx]).Clone();
                                    }
                                    else
                                    {
                                        finalfitLargeConsumer = (ArrayList)((ArrayList)fitConsumerList[0]).Clone();
                                    }

                                    //최종적으로 선정된 조합의 가장 짧은 발동시간을 발췌해 가동여부를 판단한다.

                                    int minIncreseTime = 0;

                                    for (int i = 0; i < finalfitLargeConsumer.Count; i++)
                                    {
                                        if (minIncreseTime == 0 || minIncreseTime > int.Parse(((DataRow)((Hashtable)finalfitLargeConsumer[i])["consumerData"])["APPLY_CONTINUE_TIME"].ToString()))
                                        {
                                            minIncreseTime = int.Parse(((DataRow)((Hashtable)finalfitLargeConsumer[i])["consumerData"])["APPLY_CONTINUE_TIME"].ToString());
                                        }
                                    }

                                    //Console.WriteLine("조합중 가장 작은 증가시간 : " + minIncreseTime);

                                    DateTime analysisTime = DateTime.Parse(row["TIMESTAMP"].ToString());

                                    bool confirm = false;

                                    for (int i = 0; i < minIncreseTime; i++)
                                    {
                                        //1분씩 이전 데이터를 찾아서 차이량에 부합하는지 확인
                                        DateTime compareTime = analysisTime.AddMinutes(-(i + 1));
                                        string compareTimeString = compareTime.Year + "-" + compareTime.Month + "-" + compareTime.Day + " " + compareTime.Hour + ":" + compareTime.Minute;

                                        foreach (DataRow tmpRow in flowHistoryDataSet.Tables["IF_IHTAGS"].Rows)
                                        {
                                            if (compareTimeString.Equals(tmpRow["TIMESTAMP"].ToString()))
                                            {
                                                double compareFlow = double.Parse(tmpRow["VALUE"].ToString());

                                                //Console.WriteLine((i + 1) + "분전 데이터 비교 - 현재유량 " + targetFlow + " 비교유량 : " + compareFlow + " 차이량 " + (targetFlow - compareFlow));

                                                //현재유량과의 차이 비교 (주의)
                                                if (Math.Abs(compareFlow - targetFlow) > consumerDecreaseSum)
                                                {
                                                    confirm = true;

                                                    //가동중인 대수용가 정보에서 해당 대수용가삭제 후 대수용가 리스트에 추가
                                                    for (int j = 0; j < finalfitLargeConsumer.Count; j++)
                                                    {
                                                        string id = (((DataRow)((Hashtable)finalfitLargeConsumer[j])["consumerData"]))["ID"].ToString();

                                                        for (int k = 0; k < activateLargeConsumerList.Count; k++)
                                                        {
                                                            DataRow consumerRow = (DataRow)((Hashtable)activateLargeConsumerList[k])["consumerData"];

                                                            if (id.Equals(consumerRow["ID"].ToString()))
                                                            {
                                                                //ID가 같다면 원래 대수용가 리스트에 대수용가 정보를 추가하고 가동중 리스트에서는 삭제
                                                                largeConsumerDataTable.ImportRow(consumerRow);
                                                                activateLargeConsumerList.RemoveAt(k);
                                                            }
                                                        }
                                                    }

                                                    sumDecreaseValue = sumDecreaseValue - consumerDecreaseSum;
                                                }
                                            }
                                        }

                                        if (confirm)
                                        {
                                            break;
                                        }
                                    }

                                    if (confirm)
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        sumDecreaseValue = 0;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                variationState = "K";
                            }
                        }
                        beforFlow = double.Parse(row["VALUE"].ToString());
                    }
                }
            }

            //활성화된 대수용가 리스트 중 활성화 대상은 활성화 처리 후 DB 저장
            Hashtable deleteActivationConditions = new Hashtable();
            deleteActivationConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());

            dao.DeleteLargeConsumerActivationData(manager, deleteActivationConditions);

            Hashtable appliedLargeConsumer = new Hashtable();

            //가동중인 대수용가의 총 수수량
            double sumAppliedIncrease = 0;

            for (int i = 0; i < activateLargeConsumerList.Count; i++)
            {
                Hashtable activateConsumer = (Hashtable)activateLargeConsumerList[i];
                DataRow consumerData = (DataRow)activateConsumer["consumerData"];

                string appliedIncreasement = "0";

                if ("S".Equals(activateConsumer["status"].ToString()))
                {
                    DateTime analysisTime = DateTime.Parse(activateConsumer["activateTime"].ToString());
                    DateTime targetTime = DateTime.Parse(conditions["TARGETDATE"].ToString());

                    TimeSpan span = targetTime - analysisTime;

                    //3분이상 지속될때 적용상태로 바꿈
                    if (span.Minutes > 3)
                    {
                        activateConsumer["status"] = "A";
                        appliedIncreasement = consumerData["APPLY_INCREASE"].ToString();
                    }
                }
                else
                {
                    appliedIncreasement = consumerData["APPLY_INCREASE"].ToString();
                }

                //반환값 설정
                appliedLargeConsumer.Add(consumerData["ID"].ToString(), appliedIncreasement);
                sumAppliedIncrease = sumAppliedIncrease + double.Parse(appliedIncreasement);

                Hashtable insertActivateConsumerConditions = new Hashtable();

                insertActivateConsumerConditions.Add("INP_NUMBER", consumerData["INP_NUMBER"].ToString());
                insertActivateConsumerConditions.Add("ID", consumerData["ID"].ToString());
                insertActivateConsumerConditions.Add("STATUS", activateConsumer["status"].ToString());
                insertActivateConsumerConditions.Add("INCREASE_AMOUNT", consumerData["APPLY_INCREASE"].ToString());
                insertActivateConsumerConditions.Add("ACTIVATE_TIME", activateConsumer["activateTime"].ToString());

                dao.InsertLargeConsumerActivationData(manager, insertActivateConsumerConditions);
            }

            //미가동 대수용가정보 추가
            foreach (DataRow row in largeConsumerDataTable.Rows)
            {
                appliedLargeConsumer.Add(row["ID"].ToString(), "0");
            }

            //TAG정보가 있고 실측정보가 있는 대수용가 조회
            Hashtable observerableLargeConsumerConditions = new Hashtable();

            observerableLargeConsumerConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
            observerableLargeConsumerConditions.Add("TIMESTAMP", conditions["TARGETDATE"]);
            observerableLargeConsumerConditions.Add("FTR_CODE", conditions["FTR_CODE"]);
            observerableLargeConsumerConditions.Add("FTR_IDN", conditions["FTR_IDN"]);

            DataSet observableLargeConsumerDataSet = dao.SelectObservableLargeConsumer(manager, observerableLargeConsumerConditions);

            foreach (DataRow row in observableLargeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows)
            {
                appliedLargeConsumer.Add(row["ID"].ToString(), row["VALUE"].ToString());
                sumAppliedIncrease = sumAppliedIncrease + double.Parse(row["VALUE"].ToString());
            }

            //결과 확인
            Console.WriteLine("=============결과 확인=================" + conditions["LOC_CODE"]);
            foreach (string key in appliedLargeConsumer.Keys)
            {
                Console.WriteLine(key + " : " + appliedLargeConsumer[key]);
            }


            //대수용가 동작상태 반환값 생성
            Hashtable result = new Hashtable();
            result.Add("sumAppliedIncrease", sumAppliedIncrease);
            result.Add("largeConsumerList", appliedLargeConsumer);

            return result;
        }
    }
}
