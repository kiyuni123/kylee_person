﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;
using System.Timers;

using WaterNETServer.WH_AnalysisScheduleManage.work;
using WaterNETServer.WH_AnalysisScheduleManage.etc;
using WaterNETServer.WH_Common.utils;
using WaterNETServer.Common.utils;

using EMFrame.form;
using EMFrame.log;

namespace WaterNETServer.WH_AnalysisScheduleManage.form
{
    public partial class frmAnalysisScheduleManage : BaseForm, IForminterface 
    {
        private AnalysisScheduleManageWork work = null;
        private FormUtils frmUtil = null;

        //Watcher로 부터 Push가 불가능하기 때문에 App에서 일정시간 간격으로 Watcher의 Log를 조회하기 위한 타이머
        //(Push가 불가능한 이유는 System의 재기동 시 Scheduling job table을 읽어 상태가 실행중인 Job에 대해 화면이 없는 채로 일괄로 실행시키기 때문이며
        //소켓등을 사용하기 전에는 해당 textArea 객체를 넘겨야 해서 참조 할 방법이 없음)
        private System.Timers.Timer timer = new System.Timers.Timer();

        //Status창에 Watcher의 상태를 쓰기위한 Delegate 정의
        private delegate void SetStatusTextCallback(string text);

        public frmAnalysisScheduleManage()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //메인폼에서 호출
        public void Open()
        {
        }

        //메인폼에서 호출
        private void frmWQMain_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        //화면초기화
        private void InitializeSetting()
        {
            //work = AnalysisScheduleManageWork.GetInstance();
            work = new AnalysisScheduleManageWork();
            frmUtil = new FormUtils();

            //실시간 관망해석 작업 리스트
            UltraGridColumn jobColumn;

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "IDX";
            jobColumn.Header.Caption = "작업일련번호";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 180;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "WSP_NAM";
            jobColumn.Header.Caption = "계통명";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "LFTRIDN";
            jobColumn.Header.Caption = "대블록";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "MFTRIDN";
            jobColumn.Header.Caption = "중블록";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "SFTRIDN";
            jobColumn.Header.Caption = "소블록";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "INP_NUMBER";
            jobColumn.Header.Caption = "모델 일련번호";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 180;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "TITLE";
            jobColumn.Header.Caption = "모델제목";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 250;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "HYDRA_CYCLE";
            jobColumn.Header.Caption = "실행주기(분)";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Right;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 100;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "LAST_EXE_TIME";
            jobColumn.Header.Caption = "최종실행시간";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 180;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "STATUS_CODE";
            jobColumn.Header.Caption = "현재상태";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "STATUS_NAME";
            jobColumn.Header.Caption = "현재상태";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "INS_DATE";
            jobColumn.Header.Caption = "등록일자";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 180;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "SAVE_RESULT";
            jobColumn.Header.Caption = "결과저장여부";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 100;
            jobColumn.Hidden = false;   //필드 보이기

            //실시간 관망해석 작업 리스트 스타일 확정
            frmUtil.SetGridStyle(griJobList);
        }

        //form 최초로딩 시 실행
        private void frmAnalysisScheduleManage_Load(object sender, EventArgs e)
        {
            try
            {
                //Watcher log 조회용 타이머 활성화
                timer.Interval = 10000;
                timer.Start();
                timer.Elapsed += new ElapsedEventHandler(StartSearch);


                GlobalVariable gval = new GlobalVariable();

                //if (gval.isEPA())
                //{
                    //실행중으로 등록된 작업에 대해 일괄수행한다.
                    DataTable scheduleTable = (work.SelectRealtimeAnalysisJobList(new Hashtable())).Tables["WH_HYDRA_SCHEDULE"];

                    foreach (DataRow row in scheduleTable.Rows)
                    {
                        string statusCode = row["STATUS_CODE"].ToString();

                        if ("000002".Equals(statusCode))
                        {
                            Hashtable conditions = new Hashtable();

                            conditions.Add("IDX", row["IDX"].ToString());
                            conditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                            conditions.Add("HYDRA_CYCLE", row["HYDRA_CYCLE"].ToString());
                            conditions.Add("SAVE_RESULT", row["SAVE_RESULT"].ToString());

                            conditions.Add("TIME_DURATION", row["TIMES_STATEMENT"].ToString().Trim());

                            conditions.Add("STATUS_CODE", "000002");
                            work.UpdateRunningStatus(conditions);
                        }
                    }
                //}

                GetScheduledJobList(false);
            }
            catch (Exception e1)
            {
                Logger.Error(e1.ToString());
            }
        }

        //타이머의 interval마다 실행되는 메소드로 Watcher의 Log 내용을 richText에 뿌려주는 역할과 진행중인 작업 리스트를 갱신하는 역할을 수행
        public void StartSearch(Object sender, EventArgs eArgs)
        {
            SetStatusText(WatcherThread.GetThreadStatus());
        }

        //Status창에 Watcher의 상태를 쓰기위한 Delegate (Cross Thread 오류때문에 사용해야함)
        private void SetStatusText(string text)
        {
            if (ricJobStatus.InvokeRequired)
            {
                SetStatusTextCallback setStatusTextCallback = new SetStatusTextCallback(SetStatusText);
                this.Invoke(setStatusTextCallback, new object[] { text });
            }
            else
            {
                try
                {
                    if (ricJobStatus.Lines.Length > 500)
                    {
                        ricJobStatus.Clear();
                    }

                    ricJobStatus.AppendText(text);
                    ricJobStatus.ScrollToCaret();
                    GetScheduledJobList(true);
                }
                catch (Exception ex)
                {
                    timer.Stop();
                    timer.Dispose();
                    Logger.Error(ex);
                }
            }
        }

        //등록된 작업리스트 조회
        public void GetScheduledJobList(bool lastExecuteUpdate)
        {
            try
            {
                DataTable scheduleTable = (work.SelectRealtimeAnalysisJobList(new Hashtable())).Tables["WH_HYDRA_SCHEDULE"];

                if (lastExecuteUpdate)
                {
                    //최종해석시간 update
                    
                    DataTable gridTable = (DataTable)griJobList.DataSource;

                    if(gridTable.Rows.Count != 0)
                    {
                        foreach (DataRow row in scheduleTable.Rows)
                        {
                            for (int i = 0; i < gridTable.Rows.Count; i++)
                            {
                                string idx = griJobList.Rows[i].GetCellValue("IDX").ToString();

                                if (idx.Equals(row["IDX"].ToString()))
                                {
                                    griJobList.Rows[i].Cells["LAST_EXE_TIME"].Value = row["LAST_EXE_TIME"];
                                    griJobList.Update();
                                }
                            }
                        }
                    }
                }
                else
                {
                    //일반조회

                    griJobList.DataSource = scheduleTable;
                    frmUtil.SetGridStyle_PerformAutoResize(griJobList);

                    //상태에 따라 Grid Row 컬러 변경 (정지나 등록상태면 붉은색)
                    for (int i = 0; i < griJobList.Rows.Count; i++)
                    {
                        if ("000001".Equals(griJobList.Rows[i].GetCellValue("STATUS_CODE").ToString()) || "000003".Equals(griJobList.Rows[i].GetCellValue("STATUS_CODE").ToString()))
                        {
                            griJobList.Rows[i].Appearance.BackColor = Color.Red;
                        }
                        else
                        {
                            griJobList.Rows[i].Appearance.BackColor = Color.Lime;
                        }
                    }

                    //조회 종료 시 첫행에 focus를 주고 해당 내용을 채움
                    if (griJobList.Rows.Count > 0)
                    {
                        griJobList.Rows[0].Selected = true;
                    }
                }
            }
            catch (Exception e1)
            {
                Logger.Error(e1.ToString());
            }
        }

        //그리드의 버튼을 클릭 시 
        private void griJobList_ClickCellButton(object sender, CellEventArgs e)
        {
            Hashtable conditions = new Hashtable();

            conditions.Add("IDX", Utils.nts(e.Cell.Row.GetCellValue("IDX")));
            conditions.Add("INP_NUMBER", Utils.nts(e.Cell.Row.GetCellValue("INP_NUMBER")));
            conditions.Add("HYDRA_CYCLE", Utils.nts(e.Cell.Row.GetCellValue("HYDRA_CYCLE")));
            conditions.Add("SAVE_RESULT", Utils.nts(e.Cell.Row.GetCellValue("SAVE_RESULT")));
            conditions.Add("TIME_DURATION", Utils.nts(e.Cell.Row.GetCellValue("TIMES_STATEMENT")));

            string color = "";

            if ("000001".Equals(Utils.nts(e.Cell.Row.GetCellValue("STATUS_CODE"))) || "000003".Equals(Utils.nts(e.Cell.Row.GetCellValue("STATUS_CODE"))))
            {
                //등록, 정지상태 -> 실행
                conditions.Add("STATUS_CODE", "000002");
                color = "lime";
            }
            else
            {
                //실행중 상태 -> 정지
                conditions.Add("STATUS_CODE", "000003");
                color = "red";
            }

            try
            {
                work.UpdateRunningStatus(conditions);
                GetScheduledJobList(false);
                MessageBox.Show("정상적으로 처리되었습니다.");

                if ("lime".Equals(color))
                {
                    e.Cell.Row.Appearance.BackColor = Color.Lime;
                }
                else
                {
                    e.Cell.Row.Appearance.BackColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        //그리드의 Row를 클릭 시
        private void griJobList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griJobList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griJobList.DataSource).Rows[griJobList.Selected.Rows[0].Index]);
                if ("000002".Equals((string)row["STATUS_CODE"]))
                {
                    //현재상태가 실행중인경우는 읽기전용 처리
                    texHydraCycle.ReadOnly = true;
                    cheIsRemainResult.Enabled = false;
                }
                else
                {
                    texHydraCycle.ReadOnly = false;
                    cheIsRemainResult.Enabled = true;
                }

                texHydraCycle.Text = Utils.nts(griJobList.Selected.Rows[0].GetCellValue("HYDRA_CYCLE"));

                if ("Y".Equals((string)griJobList.Selected.Rows[0].GetCellValue("SAVE_RESULT")))
                {
                    cheIsRemainResult.Checked = true;
                }
                else
                {
                    cheIsRemainResult.Checked = false;
                }
            }
        }

        //스케줄 저장버튼 클릭 시
        private void butSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                if (griJobList.Selected.Rows.Count != 0)
                {
                    //수정이 가능한 상태인 경우만 동작
                    if (!texHydraCycle.ReadOnly)
                    {
                        if ("".Equals(texHydraCycle.Text))
                        {
                            MessageBox.Show("실행주기에 값을 입력하십시오.");
                            texHydraCycle.Focus();
                        }

                        //숫자값만 오게하는 로직 필요...

                        Hashtable conditions = new Hashtable();

                        conditions.Add("IDX", griJobList.Selected.Rows[0].GetCellValue("IDX"));
                        conditions.Add("HYDRA_CYCLE", texHydraCycle.Text);

                        if (cheIsRemainResult.Checked)
                        {
                            conditions.Add("SAVE_RESULT", "Y");
                        }
                        else
                        {
                            conditions.Add("SAVE_RESULT", "N");
                        }

                        work.UpdateJobData(conditions);
                        GetScheduledJobList(false);
                        MessageBox.Show("정상적으로 처리되었습니다.");
                    }
                    else
                    {
                        MessageBox.Show("작업이 진행중일때는 수정/삭제가 불가능합니다.");
                    }
                }
            }
            catch (Exception e1)
            {
                Logger.Error(e1.ToString());
            }
        }

        //스케줄 삭제버튼 클릭 시
        private void butDeleteSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                if (griJobList.Selected.Rows.Count != 0)
                {
                    //삭제가 가능한 상태인 경우만 동작
                    if (!texHydraCycle.ReadOnly)
                    {
                        DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (result == DialogResult.Yes)
                        {
                            Hashtable conditions = new Hashtable();

                            conditions.Add("IDX", griJobList.Selected.Rows[0].GetCellValue("IDX"));

                            work.DeleteJobData(conditions);
                            GetScheduledJobList(false);
                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("작업이 진행중일때는 수정/삭제가 불가능합니다.");
                    }
                }
            }
            catch(Exception e1)
            {
                Logger.Error(e1.ToString());
            }
        }

        //스케줄 추가버튼 클릭 시
        private void butAddSchedule_Click(object sender, EventArgs e)
        {
            frmAnalysisJobRegist form = new frmAnalysisJobRegist();
            form.parentForm = this;
            form.ShowDialog();
        }

        //일괄실행 버튼 클릭 시
        private void butAllStart_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable scheduleTable = (work.SelectRealtimeAnalysisJobList(new Hashtable())).Tables["WH_HYDRA_SCHEDULE"];

                foreach (DataRow row in scheduleTable.Rows)
                {
                    string statusCode = row["STATUS_CODE"].ToString();

                    if ("000001".Equals(statusCode) || "000003".Equals(statusCode))
                    {
                        Hashtable conditions = new Hashtable();

                        conditions.Add("IDX", row["IDX"].ToString());
                        conditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                        conditions.Add("HYDRA_CYCLE", row["HYDRA_CYCLE"].ToString());
                        conditions.Add("SAVE_RESULT", row["SAVE_RESULT"].ToString());

                        conditions.Add("TIME_DURATION", row["TIMES_STATEMENT"].ToString().Trim());

                        conditions.Add("STATUS_CODE", "000002");
                        work.UpdateRunningStatus(conditions);
                    }
                }

                GetScheduledJobList(false);
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e1)
            {
                Logger.Error(e1.ToString());
            }
        }

        //일괄정지 버튼 클릭 시
        private void butAllStop_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable scheduleTable = (work.SelectRealtimeAnalysisJobList(new Hashtable())).Tables["WH_HYDRA_SCHEDULE"];

                foreach (DataRow row in scheduleTable.Rows)
                {
                    string statusCode = row["STATUS_CODE"].ToString();

                    if ("000002".Equals(statusCode))
                    {
                        Hashtable conditions = new Hashtable();

                        conditions.Add("IDX", row["IDX"].ToString());
                        conditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                        conditions.Add("HYDRA_CYCLE", row["HYDRA_CYCLE"].ToString());
                        conditions.Add("SAVE_RESULT", row["SAVE_RESULT"].ToString());

                        conditions.Add("TIME_DURATION", row["TIMES_STATEMENT"].ToString().Trim());

                        conditions.Add("STATUS_CODE", "000003");
                        work.UpdateRunningStatus(conditions);
                    }
                }

                GetScheduledJobList(false);
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e1)
            {
                Logger.Error(e1.ToString());
            }
        }

        //폼 종료될때 호출
        private void frmAnalysisScheduleManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                timer.Stop();
                timer.Dispose();
            }
            catch (Exception e1)
            {
                Logger.Error(e1.ToString());
            }
        }
    }
}
