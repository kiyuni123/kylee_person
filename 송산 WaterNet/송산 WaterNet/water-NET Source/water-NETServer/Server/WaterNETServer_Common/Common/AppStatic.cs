﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNETServer.Common
{
    /// <summary>
    /// Water-NET 프로그램을 사용하는데 있어 공통 정보를 갖는 Class
    /// </summary>
    public class AppStatic
    {
        #region 로그온한 사용자 정보

        public static string USER_ID = string.Empty;        //로그온한 사용자 ID
        public static string USER_NAME = string.Empty;      //로그온한 사용자 성명
        public static string USER_DIVISION = string.Empty;  //로그온한 사용자 부서
        public static string USER_RIGHT = string.Empty;     //로그온한 사용자 권한
        public static string USER_IP = string.Empty;        //로그온한 사용자의 IP
        public static string USER_SGCCD = string.Empty;     //로그온한 사용자의 지자체 코드
        public static string USER_SGCNM = string.Empty;     //로그온한 사용자의 지자체 명

        #endregion

        #region Config File 정보

        public static string DB_CONFIG_FILE_PATH = Application.StartupPath + @"\Config\DB_Config.xml";          // Database Config File 경로 및 파일 명
        public static string GLOBAL_CONFIG_FILE_PATH = Application.StartupPath + @"\Config\Global_Config.xml";  // Global Config File 경로 및 파일 명
        public static string LOG4NET_CONFIG_PATH = Application.StartupPath + @"\Config\log4net.xml";            // Log4NET config file 경로 및 파일 명
        public static string LOG4NET_LOG_PATH = Application.StartupPath + @"\Logs\";

        #endregion

        #region Realtime 관련 정보

        public static bool RT_AUTO_MODIFY = false;

        #endregion

        //로컬 데이터베이스
        public static string WATERNET_DATABASE = "waternet";

        //RWIS 데이터베이스
        public static string RWIS_DATABASE = "rwis";

        public static string REALTIME_TABLE = "OPN_RDR01MI_HQGNDB";
        public static string ACCUMULATION_HOUR_TABLE = "OPN_RDF01HH_HQGNDB";
        public static string EPANET_INP_DELETE = string.Empty;

        //수집 GBN 코드 목록 (실시간, 적산차, 야간최소유량, 전일적산, 압력)
        public static string REALTIMECODE = "7100";
        public static string HOURCODE = "7101";
        public static string MNFCODE = "7102";
        public static string DAYCODE = "7103";
        public static string PRESSURECODE = "7104";
    }
}
