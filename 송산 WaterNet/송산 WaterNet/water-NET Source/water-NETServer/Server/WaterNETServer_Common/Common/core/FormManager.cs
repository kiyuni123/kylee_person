﻿using System;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Shared;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinGrid.ExcelExport;
using Infragistics.Excel;
using WaterNETServer.Common.DB.Oracle;
using WaterNETServer.Common.core;

namespace WaterNETServer.core
{
    /// <summary>
    /// 폼 관리자
    /// </summary>
    /// <remarks></remarks>
    public class FormManager
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////// Field

        //////////////////////////////////////////////////////////////////////////////////////////// Public
        /// <summary>
        /// 상세화면 오픈시 추가, 수정, 보기 설정
        /// </summary>
        public enum SubFormEventType
        {
            isAppend = 1,
            isUpdate = 2,
            isViewer = 3
        }
        //////////////////////////////////////////////////////////////////////////////////////////// Private


        //private const char m_chDelimeter = '▧';
        ////////////////////////////////////////////////////////////////////////////////////////////////////// Method

        //////////////////////////////////////////////////////////////////////////////////////////// Public

        #region "그리드 스타일 설정하기 - SetGridStyle(ugTarget)"

        public static void SetGridStyle_Update(UltraGrid ugList)
        {
            ugList.DisplayLayout.GroupByBox.Hidden = true;
            //ugList.DisplayLayout.UseFixedHeaders = true;

            ugList.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;

            ugList.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            ugList.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;
            ugList.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            ugList.DisplayLayout.Override.ActiveRowAppearance.BackColor = Color.LightYellow;
            ugList.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Blue;
            ugList.DisplayLayout.Override.RowAppearance.BackColor = Color.White;
            ugList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            ugList.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            ugList.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            ugList.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;

            ugList.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            ugList.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;
            //현재 선택된 셀만 백칼라 = YellowGreen
            ugList.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            ugList.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

            ugList.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            ugList.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            ugList.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            ugList.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            ugList.Font = new Font("굴림", 10f, FontStyle.Regular, GraphicsUnit.Point, 129);

            for (int I = 0; I <= ugList.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.BackGradientStyle = GradientStyle.None;
                ugList.DisplayLayout.Bands[0].Columns[I].CellActivation = Activation.AllowEdit;
                ugList.DisplayLayout.Bands[0].Columns[I].CellClickAction = CellClickAction.EditAndSelectText;
            }
        }

        public static void SetGridStyle_Append(UltraGrid ugList)
        {
            ugList.DisplayLayout.GroupByBox.Hidden = true;
            //ugList.DisplayLayout.UseFixedHeaders = true;

            ugList.DisplayLayout.Override.AllowAddNew = AllowAddNew.FixedAddRowOnTop;
            ugList.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;

            ugList.DisplayLayout.Override.TemplateAddRowAppearance.BackColor = Color.FromArgb(245, 250, 255);
            ugList.DisplayLayout.Override.TemplateAddRowAppearance.ForeColor = SystemColors.GrayText;

            //ugList.DisplayLayout.Override.AddRowAppearance.BackColor = Color.LightYellow;
            //ugList.DisplayLayout.Override.AddRowAppearance.ForeColor = Color.Blue;
            ugList.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            ugList.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;
            ugList.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            ugList.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.TemplateAddRow;
            ugList.DisplayLayout.Override.SpecialRowSeparatorAppearance.BackColor = SystemColors.Control;

            ugList.DisplayLayout.Override.TemplateAddRowPrompt = "신규 데이터는 여기에 입력하세요...";

            ugList.DisplayLayout.Override.TemplateAddRowPromptAppearance.ForeColor = Color.Maroon;
            ugList.DisplayLayout.Override.TemplateAddRowPromptAppearance.FontData.Bold = DefaultableBoolean.True;
            ugList.DisplayLayout.Override.RowAppearance.BackColor = Color.White;
            ugList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;

            ugList.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            ugList.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            ugList.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;
            ugList.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            ugList.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;

            //현재 선택된 셀만 백칼라 = YellowGreen
            ugList.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            ugList.DisplayLayout.Override.CellAppearance.BackColor = Color.White;


            ugList.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            ugList.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            ugList.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            ugList.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            ugList.Font = new Font("굴림", 10f, FontStyle.Regular, GraphicsUnit.Point, 129);

            for (int I = 0; I <= ugList.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.BackGradientStyle = GradientStyle.None;
                ugList.DisplayLayout.Bands[0].Columns[I].CellActivation = Activation.AllowEdit;
                ugList.DisplayLayout.Bands[0].Columns[I].CellClickAction = CellClickAction.EditAndSelectText;
            }
        }

        public static void SetGridStyle(UltraGrid ugList)
        {
            ugList.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            ugList.DisplayLayout.GroupByBox.BorderStyle = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.GroupByBox.Hidden = true;
            //ugList.DisplayLayout.UseFixedHeaders = true;
            ugList.DisplayLayout.MaxColScrollRegions = 1;
            ugList.DisplayLayout.MaxRowScrollRegions = 1;

            ugList.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
            ugList.DisplayLayout.Override.CellPadding = 0;
            ugList.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            ugList.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;
            ugList.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            ugList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            ugList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            ugList.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            ugList.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            ugList.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;

            ugList.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            ugList.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;
            ugList.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.Default;
            //DSOH 추가. Row Select는 무조건 Single만 되고, Drag가 되지 않게.
            ugList.DisplayLayout.Override.SelectTypeRow = SelectType.SingleAutoDrag;

            ugList.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;
            ugList.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            ugList.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            ugList.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            ugList.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            ugList.Font = new Font("굴림", 9f, FontStyle.Regular, GraphicsUnit.Point, 129);

            for (int I = 0; I <= ugList.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.BackGradientStyle = GradientStyle.None;
                ugList.DisplayLayout.Bands[0].Columns[I].CellActivation = Activation.NoEdit;
            }
        }

        #region - UltraGrid Setting (오두석)

        /// <summary>
        /// UltraGrid의 모든 Row에 대해 Append 모드를 해제하고, 모든 Cell에 대해 Edit 못하게 수정
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        public static void SetGridStyle_UnAppend(UltraGrid ugList)
        {
            ugList.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;

            for (int I = 0; I <= ugList.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.BackGradientStyle = GradientStyle.None;
                ugList.DisplayLayout.Bands[0].Columns[I].CellActivation = Activation.NoEdit;
                ugList.DisplayLayout.Bands[0].Columns[I].CellClickAction = CellClickAction.RowSelect;
            }
        }

        /// <summary>
        /// UltraGrid에 Set된 Data의 Length에 맞게 자동으로 Column Size를 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        public static void SetGridStyle_PerformAutoResize(UltraGrid ugList)
        {
            for (int I = 0; I <= ugList.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                ugList.DisplayLayout.Bands[0].Columns[I].PerformAutoResize();
            }
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="iCol">Column Index</param>
        public static void SetGridStyle_ColumeAllowEdit(UltraGrid ugList, int iCol)
        {
            ugList.DisplayLayout.Bands[0].Columns[iCol].CellActivation = Activation.AllowEdit;
            ugList.DisplayLayout.Bands[0].Columns[iCol].CellClickAction = CellClickAction.EditAndSelectText;
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="iCol">Column Key</param>
        public static void SetGridStyle_ColumeAllowEdit(UltraGrid ugList, string iCol)
        {
            ugList.DisplayLayout.Bands[0].Columns[iCol].CellActivation = Activation.AllowEdit;
            ugList.DisplayLayout.Bands[0].Columns[iCol].CellClickAction = CellClickAction.EditAndSelectText;
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iSCol">NoEdit할 시작 Column Index</param>
        /// <param name="iECol">NoEdit할 종료 Column Index (Columns.Count)</param>
        public static void SetGridStyle_ColumeAllowEdit(UltraGrid ugList, int iSCol, int iECol)
        {
            for (int i = iSCol; i <= iECol; i++)
            {
                ugList.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
                ugList.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.EditAndSelectText;
            }
        }

        /// <summary>
        /// UltraGrid의 특정 Row의 Cell을 Disable 모드로 변경한다.
        /// 단 UltraGrid의 기본 셋팅이 Activation.AllowEdit 이어야 한다.(이건 좀더 확인 필요)
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iRow">Row Index</param>
        /// <param name="iCol">Column Index</param>
        public static void SetGridStyle_CellDisable(UltraGrid ugList, int iRow, int iCol)
        {
            ugList.Rows[iRow].Cells[iCol].Appearance.BackColorDisabled = Color.LightSalmon;
            ugList.Rows[iRow].Cells[iCol].Activation = Activation.Disabled;
        }

        /// <summary>
        /// UltraGrid의 특정 Row의 Cell을 AllowEdit 모드로 변경한다.
        /// 단 UltraGrid의 기본 셋팅이 Activation.NoEdit 이어야 한다.(이건 좀더 확인 필요)
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iRow">Row Index</param>
        /// <param name="iCol">Column Index</param>
        public static void SetGridStyle_CellAllowEdit(UltraGrid ugList, int iRow, int iCol)
        {
            ugList.Rows[iRow].Cells[iCol].Appearance.BackColor = Color.Lime;
            ugList.Rows[iRow].Cells[iCol].Activation = Activation.AllowEdit;
            Application.DoEvents();
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 NoEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="strCol">Column Name. Empty이면 0번 Column<</param>
        public static void SetGridStyle_ColumeNoEdit(UltraGrid ugList, string strCol)
        {
            if (strCol == "")
            {
                ugList.DisplayLayout.Bands[0].Columns[0].CellActivation = Activation.NoEdit;
                ugList.DisplayLayout.Bands[0].Columns[0].CellClickAction = CellClickAction.RowSelect;
            }
            else
            {
                ugList.DisplayLayout.Bands[0].Columns[strCol].CellActivation = Activation.NoEdit;
                ugList.DisplayLayout.Bands[0].Columns[strCol].CellClickAction = CellClickAction.RowSelect;
            }
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 NoEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iSCol">NoEdit할 시작 Column Index</param>
        /// <param name="iECol">NoEdit할 종료 Column Index (Columns.Count)</param>
        public static void SetGridStyle_ColumeNoEdit(UltraGrid ugList, int iSCol, int iECol)
        {
            for (int i = iSCol; i < iECol; i++)
            {
                ugList.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
                ugList.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.RowSelect;
            }
        }

        /// <summary>
        /// UltraGrid의 모든 Row에 대해 Append 모드를 해제하고, 모든 Cell에 대해 Edit 못하게 수정
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="iRow">UltraGrid의 Row Index</param>
        /// <param name="iCell">UltraGrid의 Cell Index</param>
        public static void SetGridStyle_EditToCell(UltraGrid ugList, int iRow, int iCell)
        {
            ugList.DisplayLayout.Bands[0].Columns[3].CellClickAction = CellClickAction.EditAndSelectText;
            ugList.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }

        /// <summary>
        /// 특정 Column에 Default Data를 Set한다.
        /// Grid에 Row를 Add 한 다음, Popup Form에서 조회 결과를 리턴받아 Grid에 자동으로 Set하고 싶은 경우 사용하면 된다.
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="strColumn">UltraGrid의 Column Name. Empty이면 0번 Column</param>
        /// <param name="strDefaultData">Default로 Set할 Data</param>
        public static void SetGridStyle_SetDefaultCellValue(UltraGrid ugList, string strColumn, string strDefaultData)
        {
            if (strColumn == "")
            {
                ugList.DisplayLayout.Bands[0].Columns[0].DefaultCellValue = strDefaultData;
            }
            else
            {
                ugList.DisplayLayout.Bands[0].Columns[strColumn].DefaultCellValue = strDefaultData;
            }
        }

        /// <summary>
        /// UltraGrid에서 RowSelectorImage를 모두 Null로 표시 함.
        /// </summary>
        /// <param name="oUGrid"></param>
        public static void SetGridStyle_NoRowSelectorImages(UltraGrid oUGrid)
        {
            Image[] oImage = new Image[] { null, null, null, null, null };

            oUGrid.DisplayLayout.RowSelectorImages.ActiveRowImage = oImage[0];
            oUGrid.DisplayLayout.RowSelectorImages.ActiveAndDataChangedImage = oImage[1];
            oUGrid.DisplayLayout.RowSelectorImages.ActiveAndAddNewRowImage = oImage[2];
            oUGrid.DisplayLayout.RowSelectorImages.AddNewRowImage = oImage[3];
            oUGrid.DisplayLayout.RowSelectorImages.DataChangedImage = oImage[4];
        }

        #endregion'

        #endregion

        #region "실행되어 있는 폼 가져오기 - GetActiveForm()"
        /// <summary>
        /// 실행되어 있는 폼 가져오기
        /// </summary>
        /// <returns>폼 이름</returns>
        /// <remarks></remarks>
        public static Form GetActiveForm(string frmName)
        {
            foreach (Form ActiveForm in Application.OpenForms)
            {
                if (ActiveForm.Name == frmName)
                {
                    return ActiveForm;
                }
            }
            return null;
        }
        #endregion


        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iSCol">NoEdit할 시작 Column Index</param>
        /// <param name="iECol">NoEdit할 종료 Column Index (Columns.Count)</param>
        public static void ColumeAllowEdit(UltraGrid oUGrid, int iSCol, int iECol)
        {
            ////현재 선택된 셀만 백칼라 = YellowGreen
            oUGrid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.YellowGreen;
            oUGrid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;
            oUGrid.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;
            for (int i = iSCol; i < iECol; i++)
            {
                oUGrid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
                oUGrid.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.EditAndSelectText;
            }
        }

        #region ultraGrid Row구분,Insert, update
        public static void AfterRowInsert(object sender, RowEventArgs e)
        {
            e.Row.Tag = 'I';
        }

        public static void AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.DataChanged)
            {
                if ((e.Cell.Row.Tag) == null)
                {
                    e.Cell.Row.Tag = 'U';
                }
                else
                {
                    switch (Convert.ToChar(e.Cell.Row.Tag))
                    {
                        case 'U':
                        case 'I':
                            break;
                        default:
                            e.Cell.Row.Tag = 'U';
                            break;
                    }
                }
            }
        }

        public static void DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            e.Cell.Column.CellClickAction = CellClickAction.Edit;
        }
        #endregion

        #region "그리드 초기화하기 - InitializeGrid(ugList, dsList)"
        /// <summary>
        /// 그리드 초기화하기
        /// </summary>
        /// <param name="ugList">목록 울트라 그리드</param>
        /// <param name="dsList">목록 데이터 셋</param>
        /// <remarks></remarks>
        public static void InitializeGrid(UltraGrid ugList, DataSet dsList)
        {
            if (ugList != null)
            {
                if (ugList.DataSource != null)
                {
                    DataView oDataView = (DataView)ugList.DataSource;

                    dsList = oDataView.Table.DataSet;
                    dsList = dsList.Clone();
                    ugList.DataSource = dsList.Tables[0].DefaultView;
                }
            }
        }

        public static void InitializeGrid(UltraGrid ugList, DataSet dsList, string tablename)
        {
            if (ugList != null)
            {
                if (ugList.DataSource != null)
                {
                    DataView oDataView = (DataView)ugList.DataSource;

                    dsList = oDataView.Table.DataSet;
                    dsList = dsList.Clone();
                    ugList.DataSource = dsList.Tables[tablename].DefaultView;
                }
            }
        }
        #endregion

        #region Ultra Grid를 Excel File로 Export (오두석)

        /// <summary>
        /// UltraGrid에서 Excel로 Export 한다.
        /// Grid에 데이터가 없는 경우 Export되지 않는다.
        /// </summary>
        /// <param name="oUGrid">Excel로 Export할 UltraGrid</param>
        /// <param name="strTitle">File Name에 들어갈 Title</param>
        public static void ExportToExcelFromUltraGrid(UltraGrid oUGrid, string strTitle)
        {
            string strDate = string.Empty;
            string strFullName = string.Empty;

            SaveFileDialog oSFD = new SaveFileDialog();
            UltraGridExcelExporter oUGExcelExp = new UltraGridExcelExporter();

            if (oUGrid.Rows.Count > 0)
            {
                strDate = strDate = Convert.ToString(DateTime.Now.Year) + Convert.ToString(DateTime.Now.Month).PadLeft(2, '0') + Convert.ToString(DateTime.Now.Day).PadLeft(2, '0');

                oSFD.Filter = "Excel files (*.xls)|*.xls";
                oSFD.FilterIndex = 1;
                oSFD.RestoreDirectory = true;

                // File Save용 CommonDialog를 DP하고, OK명 Export한다. 완료 후 Message를 띄어 Excel File을 볼지 여부를 DP
                oSFD.FileName = strTitle + "-" + strDate + "";
                if (oSFD.ShowDialog() == DialogResult.OK)
                {
                    strFullName = oSFD.FileName;

                    oUGExcelExp.Export(oUGrid, strFullName);

                    if (DialogResult.Yes == MessageBox.Show("엑셀파일을 열어보시겠습니까?", "내용보기", MessageBoxButtons.YesNo))
                    {
                        System.Diagnostics.Process oProc = new System.Diagnostics.Process();
                        System.Diagnostics.ProcessStartInfo oProcStartInfor = new System.Diagnostics.ProcessStartInfo("Excel.exe");

                        oProcStartInfor.FileName = strFullName;
                        oProcStartInfor.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden; // 콘솔 윈도우가 보이지 않게 됩니다.

                        oProc.StartInfo = oProcStartInfor;
                        oProc.Start();
                    }
                }
            }
            else
            {
                MessageBox.Show("엑셀파일로 만들 데이터가 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// 두개의 UltraGrid를 한개의 Excel Sheet로 Export 한다.
        /// 각 Grid는 Excel File에서 Sheet로 Export된다.
        /// Grid에 데이터가 없는 경우 Export되지 않는다.
        /// </summary>
        /// <param name="oUGrid1">Excel로 Export할 UltraGrid</param>
        /// <param name="oUGrid2">Excel로 Export할 UltraGrid</param>
        /// <param name="strTitle">File Name에 들어갈 Title</param>
        /// <param name="iMergePos">oUGrid2의 내용이 들어갈 위치. 0 : oUGrid1 의 위, 1 : 아래, 2 : 좌측, 3 : 우측</param>
        public static void ExportToExcelFromUltraGrid(UltraGrid oUGrid1, UltraGrid oUGrid2, string strTitle, int iMergePos)
        {
            string strDate = string.Empty;
            string strFullName = string.Empty;

            SaveFileDialog oSFD = new SaveFileDialog();
            UltraGridExcelExporter oUGExcelExp = new UltraGridExcelExporter();

            Workbook oUGExcelWB = new Workbook();

            strDate = strDate = Convert.ToString(DateTime.Now.Year) + Convert.ToString(DateTime.Now.Month).PadLeft(2, '0') + Convert.ToString(DateTime.Now.Day).PadLeft(2, '0');

            oSFD.Filter = "Excel files (*.xls)|*.xls";
            oSFD.FilterIndex = 1;
            oSFD.RestoreDirectory = true;

            // File Save용 CommonDialog를 DP하고, OK명 Export한다. 완료 후 Message를 띄어 Excel File을 볼지 여부를 DP
            oSFD.FileName = strTitle + "-" + strDate + "";
            if (oSFD.ShowDialog() == DialogResult.OK)
            {
                strFullName = oSFD.FileName;

                oUGExcelWB.Worksheets.Add(strTitle);

                switch (iMergePos)
                {
                    case 0:
                        if (oUGrid2.Rows.Count > 0) oUGExcelExp.Export(oUGrid2, oUGExcelWB, 0, 0);
                        if (oUGrid1.Rows.Count > 0) oUGExcelExp.Export(oUGrid1, oUGExcelWB, oUGrid2.Rows.Count + 2, 0);
                        break;
                    case 1:
                        if (oUGrid1.Rows.Count > 0) oUGExcelExp.Export(oUGrid1, oUGExcelWB, 0, 0);
                        if (oUGrid2.Rows.Count > 0) oUGExcelExp.Export(oUGrid2, oUGExcelWB, oUGrid1.Rows.Count + 2, 0);
                        break;
                    case 2:
                        if (oUGrid2.Rows.Count > 0) oUGExcelExp.Export(oUGrid2, oUGExcelWB, 0, 0);
                        if (oUGrid1.Rows.Count > 0) oUGExcelExp.Export(oUGrid1, oUGExcelWB, 0, oUGrid2.DisplayLayout.Bands[0].Columns.Count + 2);
                        break;
                    case 3:
                        if (oUGrid1.Rows.Count > 0) oUGExcelExp.Export(oUGrid1, oUGExcelWB, 0, 0);
                        if (oUGrid2.Rows.Count > 0) oUGExcelExp.Export(oUGrid2, oUGExcelWB, 0, oUGrid1.DisplayLayout.Bands[0].Columns.Count + 2);
                        break;
                }

                oUGExcelWB.Save(strFullName);

                if (DialogResult.Yes == MessageBox.Show("엑셀파일을 열어보시겠습니까?", "내용보기", MessageBoxButtons.YesNo))
                {
                    System.Diagnostics.Process oProc = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo oProcStartInfor = new System.Diagnostics.ProcessStartInfo("Excel.exe");

                    oProcStartInfor.FileName = strFullName;
                    oProcStartInfor.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden; // 콘솔 윈도우가 보이지 않게 됩니다.

                    oProc.StartInfo = oProcStartInfor;
                    oProc.Start();
                }
                else
                {
                    MessageBox.Show("엑셀파일로 만들 데이터가 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion

        #region "코드 데이터셋 생성하기 - CreateCodeDataSet()"

        /// <summary>
        /// 코드 데이터셋 생성하기
        /// </summary>
        /// <returns>코드 데이터셋</returns>
        /// <remarks>콤보 상자에 코드 설정시 바인딩 데이터셋 생성용</remarks>
        private static DataSet CreateCodeDataSet()
        {
            DataSet oDataSet = new DataSet();
            DataTable oDataTable = new DataTable();
            DataColumn oDataColumn = default(DataColumn);

            oDataSet.Tables.Add(oDataTable);

            oDataTable.TableName = "CodeTable";

            oDataColumn = new DataColumn("CodeName", Type.GetType("System.String"));
            oDataTable.Columns.Add(oDataColumn);

            oDataColumn = new DataColumn("CodeValue", Type.GetType("System.String"));
            oDataTable.Columns.Add(oDataColumn);

            return oDataSet;
        }

        #endregion

        #region "패널 초기화하기 - InitializePanel(oPanel)"
        /// <summary>
        /// 패널 초기화하기
        /// </summary>
        /// <param name="oPanel">패널</param>
        /// <remarks>패널의 자식 컨트롤들을 초기화한다.</remarks>
        public static void InitializePanel(Panel oPanel)
        {
            foreach (Control oSource in oPanel.Controls)
            {
                if (oSource is Panel)
                {
                    InitializePanel((Panel)oSource);
                    continue;
                }
                // 콤보 상자
                if (oSource is ComboBox)
                {
                    ((ComboBox)oSource).SelectedIndex = -1;
                    continue;
                }

                // 텍스트 편집기
                if (oSource is TextBox)
                {
                    ((TextBox)oSource).Text = "";
                    continue;
                }

                // 체크박스 편집기
                if (oSource is CheckBox)
                {
                    ((CheckBox)oSource).Checked = false;
                    continue;
                }

                // 목록 상자                
                if (oSource is ListBox)
                {
                    ListBox lbSource = (ListBox)oSource;

                    for (int I = 0; I <= lbSource.Items.Count - 1; I++)
                    {
                        if (lbSource.GetSelected(I) == true)
                        {
                            lbSource.SetSelected(I, false);
                        }
                    }

                    continue;
                }

                // 트리뷰
                if (oSource is TreeView)
                {
                    ((TreeView)oSource).SelectedNode = null;

                    continue;
                }
            }
        }

        #endregion

        #region "컨트롤 값 설정하기 - SetValueToControls(oGroupBox, pRow)"

        /// <summary>
        /// 컨트롤 값 설정하기
        /// </summary>
        /// <param name="oGroupBox">그룹박스</param>    
        /// <param name="pRow">행 인터페이스</param>
        /// <remarks></remarks>
        public static void SetValueToControls(Panel oPanel, UltraGridRow oRow)
        {
            foreach (UltraGridCell item in oRow.Cells)
            {
                foreach (Control control in oPanel.Controls)
                {
                    if (control.Tag == null) continue;
                    if (item.Column.Key.ToString() == control.Tag.ToString())
                    {
                        try
                        {
                            #region CheckBox
                            if (control is CheckBox)  //CheckBox
                            {
                                object strValue = item.Value.ToString().ToUpper();
                                if (Convert.ToString(strValue) == "Y" | Convert.ToString(strValue) == "1")
                                {
                                    ((CheckBox)control).Checked = true;
                                }
                                else if (Convert.ToString(strValue) == "N" | Convert.ToString(strValue) == "0")
                                {
                                    ((CheckBox)control).Checked = false;
                                }
                                else
                                {
                                    ((CheckBox)control).Checked = false;
                                }
                            #endregion
                            }
                            else if (control is ComboBox)  //ComboBox
                            {
                                ((ComboBox)control).SelectedValue = item.Value;
                            }
                            else if (control is TextBox)  //TextBox
                            {
                                ((TextBox)control).Text = item.Value.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
            }
        }

        #endregion

        #region "컨트롤 값 설정하기 - SetValueToControls(oPanel, oDataTable)"

        /// <summary>
        /// 컨트롤 값 설정하기
        /// </summary>
        /// <param name="oPanel">패널</param>    
        /// <param name="oDataTable">데이터 테이블</param>
        /// <remarks></remarks>
        public static void SetValueToControls(Panel oPanel, DataTable oDataTable)
        {
            if (oDataTable.Rows.Count == 0) return;

            foreach (DataColumn item in oDataTable.Columns)
            {
                foreach (Control control in oPanel.Controls)
                {
                    if (control.Tag == null) continue;
                    if (item.ColumnName.ToString() == control.Tag.ToString())
                    {
                        try
                        {
                            #region CheckBox
                            if (control is CheckBox)  //CheckBox
                            {
                                object strValue = oDataTable.Rows[0][item.ColumnName.ToUpper()];
                                if (Convert.ToString(strValue) == "Y" | Convert.ToString(strValue) == "1")
                                {
                                    ((CheckBox)control).Checked = true;
                                }
                                else if (Convert.ToString(strValue) == "N" | Convert.ToString(strValue) == "0")
                                {
                                    ((CheckBox)control).Checked = false;
                                }
                                else
                                {
                                    ((CheckBox)control).Checked = false;
                                }
                            #endregion
                            }
                            else if (control is ComboBox)  //ComboBox
                            {
                                ((ComboBox)control).SelectedValue = oDataTable.Rows[0][item.ColumnName];
                            }
                            else if (control is TextBox)  //TextBox
                            {
                                ((TextBox)control).Text = Convert.ToString(oDataTable.Rows[0][item.ColumnName]);
                            }
                            else if (control is Label)  //Label
                            {
                                ((Label)control).Text = Convert.ToString(oDataTable.Rows[0][item.ColumnName]);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// 컨트롤 Enabled 설정하기
        /// </summary>
        /// <param name="oPanel"></param>
        /// <param name="Enable"></param>
        public static void SetEnabledToControls(Panel oPanel, bool Enable)
        {
            foreach (Control control in oPanel.Controls)
            {
                if (control is Panel)
                {
                    SetEnabledToControls((Panel)control, Enable);
                }
                else if (!(control is Label))
                    control.Enabled = Enable;
            }
        }

        #region "컨트롤 검증 - isValidate()"
        /// <summary>
        /// 컨트롤 검증
        /// </summary>
        /// <param name="oPanel">패널</param>
        /// <remarks>패널의 자식 컨트롤들을 초기화한다.</remarks>
        public static bool isValidateControl(object oSource, string sName)
        {
            // 콤보 상자
            if (oSource is ComboBox)
            {
                if (((ComboBox)oSource).SelectedIndex == -1)
                {
                    MessageManager.ShowExclamationMessage(sName + " 은(는) 입력되지 않았습니다. \n\r" + sName + " 은(는) 필수항목입니다.");
                    ((ComboBox)oSource).Focus();
                    return false;
                }
            }
            else if (oSource is TextBox)
            {
                if (string.IsNullOrEmpty(((TextBox)oSource).Text))
                {
                    MessageManager.ShowExclamationMessage(sName + " 은(는) 입력되지 않았습니다. \n\r" + sName + " 은(는) 필수항목입니다.");
                    ((TextBox)oSource).Focus();
                    return false;
                }
            }
            else if (oSource is CheckBox)
            {
                if (!((CheckBox)oSource).Checked)
                {
                    MessageManager.ShowExclamationMessage(sName + " 은(는) 입력되지 않았습니다. \n\r" + sName + " 은(는) 필수항목입니다.");
                    ((CheckBox)oSource).Focus();
                    return false;
                }
            }
            return true;
        }

        #endregion

        /// <summary>
        /// 숫자형 문자열의 ','를 삭제한다.
        /// </summary>
        /// <param name="strData"></param>
        /// <returns></returns>
        public static string StringRemoveComma(string strData)
        {
            string strTemp = strData;
            if (strTemp.Length <= 0) return strData;

            strTemp = strTemp.Replace(",", "");
            return strTemp;
        }

        /// <summary>
        /// 숫자형 문자열에 세자리마다 ',' 를 추가한다.
        /// </summary>
        /// <param name="strData"></param>
        /// <returns></returns>
        public static string StringInsertComma(string strData)
        {
            string strTemp = strData;
            if (strTemp.Length <= 0) return strData;

            strTemp = strTemp.Replace(",", "");
            double dTemp = 0;
            try
            {
                dTemp = double.Parse(strTemp);
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
                return string.Empty;
            }

            strTemp = string.Format("{0:#,#}", dTemp);
            return strTemp;

        }
    }
}
