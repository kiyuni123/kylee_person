﻿
namespace WaterNETServer.Common.utils
{
    public interface IForminterface
    {
        string FormID  { get; }
        string FormKey { get; }
    }
}
