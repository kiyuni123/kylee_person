﻿using System.Xml;
using System.IO;
using System;
using System.Collections;
using EMFrame.log;

namespace WaterNETServer.Common.utils
{
    public class Configuration
    {
        private XmlDocument m_doc = null;

        public Configuration(string strFilePath)
        {
            try
            {
                m_doc = new XmlDocument();
                m_doc.Load(strFilePath);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        public XmlDocument doc
        {
            get
            {
                return m_doc;
            }
        }

        public Hashtable getSchedule(string path, string key)
        {
            Hashtable returnValue = new Hashtable();
            XmlNodeList nodes = m_doc.SelectNodes(path);

            foreach (XmlNode node in nodes)
            {
                if (key.Equals(node["key"].InnerText))
                {
                    returnValue["type"] = node["type"].InnerText;
                    returnValue["time"] = node["time"].InnerText;
                }
            }

            return returnValue;
        }

        /// <summary>
        /// XML 파일에서 xpath에 의한 값을 리턴한다.
        /// </summary>
        /// <param name="key">xpath</param>
        /// <returns>노드 값(여기서는 설정 값)</returns>
        public string getProp(string key)
        {
            string returnValue = "";

            XmlNodeList nodes = m_doc.SelectNodes(key);

            foreach (XmlNode node in nodes)
            {
                returnValue = node.InnerText;
            }

            return returnValue;
        }

        #region "Directory 존재 여부 및 생성 및 Remove"

        /// <summary>
        /// Directory의 존재 여부를 반환한다.
        /// </summary>
        /// <param name="strDirPath">존재 여부를 알아 볼 Directory의 Full Path</param>
        public static bool IsDirectoryExists(string strDirPath)
        {
            bool blRtn;

            blRtn = Directory.Exists(strDirPath);

            return blRtn;
        }

        /// <summary>
        /// Directory를 생성한다.
        /// </summary>
        /// <param name="strDirPath">생성할 Directory의 Full Path</param>
        public static void CreateDirectory(string strDirPath)
        {
            Directory.CreateDirectory(strDirPath);
        }

        /// <summary>
        /// Directory를 Remove한다.(무조건 Remove)
        /// </summary>
        /// <param name="strDirPath">Remove할 Directory의 Full Path</param>
        /// <param name="SubDirDelete">하위 디렉토리 까지 삭제할지 여부</param>
        public static void RemoveDirectory(string strDirPath, bool SubDirDelete)
        {
            Directory.Delete(strDirPath, SubDirDelete);
        }

        #endregion
    }
}
