﻿using Infragistics.Win.UltraWinGrid;
using System.Data;
using Infragistics.Win;
using System.Drawing;

namespace WaterNETServer.Common.utils
{
    public class FormUtils
    {
#region UltraGrid 속성 설정
        /// <summary>
        /// UltraGrid에 Set된 Data의 Length에 맞게 자동으로 Column Size를 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        public void SetGridStyle_PerformAutoResize(UltraGrid ugList)
        {
            for (int I = 0; I <= ugList.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                ugList.DisplayLayout.Bands[0].Columns[I].PerformAutoResize();
            }
        }
        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList"></param>
        /// <param name="iSCol">NoEdit할 시작 Column Index</param>
        /// <param name="iECol">NoEdit할 종료 Column Index (Columns.Count)</param>
        public void ColumeAllowEdit(UltraGrid oUGrid, int iSCol, int iECol)
        {
            ////현재 선택된 셀만 백칼라 
            oUGrid.DisplayLayout.Override.ActiveCellAppearance.BackColor = Color.Yellow;
            oUGrid.DisplayLayout.Override.CellAppearance.BackColor = Color.White;
            oUGrid.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;
            for (int i = iSCol; i < iECol; i++)
            {
                oUGrid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
                oUGrid.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.EditAndSelectText;
            }
        }
        /// <summary>
        /// UltraGrid 컬럼 헤더 기본 설정 
        /// </summary>
        /// <param name="cols"></param>
        public void InitializeSettingTagColumns(UltraGrid ultraGrid, DataColumnCollection cols)
        {
            UltraGridColumn oUltraGridColumn;
            foreach (DataColumn i in cols)
            {
                oUltraGridColumn = ultraGrid.DisplayLayout.Bands[0].Columns.Add();
                oUltraGridColumn.Key = i.ColumnName;
                oUltraGridColumn.Header.Caption = i.ColumnName;
                oUltraGridColumn.CellActivation = Activation.NoEdit;
                //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
                oUltraGridColumn.CellClickAction = CellClickAction.CellSelect;
                //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
                //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
                //oUltraGridColumn.Width = 120;
                //oUltraGridColumn.Hidden = false;
            }
            //WaterNetCore.FormManager.SetGridStyle(ultraGrid1);
        }
        /// <summary>
        /// UltraGrid 스타일 기본 설정
        /// </summary>
        /// <param name="ugList"></param>
        public void SetGridStyle(UltraGrid ugList)
        {
            ugList.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            ugList.DisplayLayout.GroupByBox.BorderStyle = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.GroupByBox.Hidden = true;
            //ugList.DisplayLayout.UseFixedHeaders = true;
            ugList.DisplayLayout.MaxColScrollRegions = 1;
            ugList.DisplayLayout.MaxRowScrollRegions = 1;

            ugList.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Solid;
            ugList.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
            ugList.DisplayLayout.Override.CellPadding = 0;
            ugList.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            ugList.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;
            ugList.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            //ugList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            //ugList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            //ugList.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            ugList.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            ugList.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;

            ugList.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            ugList.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;
            ugList.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.Default;
            //DSOH 추가. Row Select는 무조건 Single만 되고, Drag가 되지 않게.
            //ugList.DisplayLayout.Override.SelectTypeRow = SelectType.SingleAutoDrag;

            ugList.DisplayLayout.Override.SelectTypeCol = SelectType.ExtendedAutoDrag;

            ugList.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;
            ugList.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            ugList.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            ugList.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            ugList.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            ugList.Font = new Font("굴림", 9f, FontStyle.Regular, GraphicsUnit.Point, 129);

            for (int I = 0; I <= ugList.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                ugList.DisplayLayout.Bands[0].Columns[I].Header.Appearance.BackGradientStyle = GradientStyle.None;
                ugList.DisplayLayout.Bands[0].Columns[I].CellActivation = Activation.NoEdit;
            }
            
            // ActiveRow.BackColor & ForeColor 설정
            ugList.DisplayLayout.Override.ActiveRowAppearance.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(230)), ((System.Byte)(238)), ((System.Byte)(240)));
            ugList.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Black;

            // Scroll이 최하단으로 내려갔을 때 빈 공간이 없도록 설정
            ugList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
        }
        /// <summary>
        /// UltraGird 스타일 설정 (색깔 이쁘게)
        /// </summary>
        /// <param name="ultraGrid"></param>
        public void SetGridStyle2(UltraGrid ultraGrid)
        {

            // Row 스타일 설정 (border, font 색)
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();

            //appearance4.BorderColor = System.Drawing.Color.FromArgb(((System.Byte)(222)), ((System.Byte)(235)), ((System.Byte)(237)));
            appearance4.BorderColor = System.Drawing.Color.FromArgb(((System.Byte)(168)), ((System.Byte)(167)), ((System.Byte)(191)));
            appearance4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            ultraGrid.DisplayLayout.Override.RowAppearance = appearance4;
            ultraGrid.DisplayLayout.Override.DefaultRowHeight = 25;


            // Header 스타일 설정
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();

            appearance3.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(214)), ((System.Byte)(233)), ((System.Byte)(244)));
            appearance3.BackColor2 = System.Drawing.Color.FromArgb(((System.Byte)(214)), ((System.Byte)(233)), ((System.Byte)(244)));
            appearance3.BorderColor = System.Drawing.Color.FromArgb(((System.Byte)(193)), ((System.Byte)(216)), ((System.Byte)(231)));
            appearance3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(109)), ((System.Byte)(151)), ((System.Byte)(177)));
            appearance3.FontData.Bold = DefaultableBoolean.True;
            appearance3.FontData.Name = "굴림";
            appearance3.FontData.SizeInPoints = 10;

            ultraGrid.DisplayLayout.Override.HeaderAppearance = appearance3;

            // ActiveRow.BackColor & ForeColor 설정
            ultraGrid.DisplayLayout.Override.ActiveRowAppearance.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(230)), ((System.Byte)(238)), ((System.Byte)(240)));
            ultraGrid.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Black;

            // Scroll이 최하단으로 내려갔을 때 빈 공간이 없도록 설정
            ultraGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
        }
        /// <summary>
        /// 그리드 스타일 설정
        /// </summary>
        /// <param name="ultraGrid"></param>
        public void SetGridStyle3(UltraGrid ultraGrid)
        {
            // Row 스타일 설정 (border, font 색)
            Infragistics.Win.Appearance appearance = new Infragistics.Win.Appearance();

            appearance.BorderColor = System.Drawing.Color.FromArgb(((System.Byte)(168)), ((System.Byte)(167)), ((System.Byte)(191)));
            //appearance.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(121)), ((System.Byte)(121)), ((System.Byte)(121)));

            ultraGrid.DisplayLayout.Override.RowAppearance = appearance;
        }
        /// <summary>
        /// 그리드 스타일 설정
        /// </summary>
        /// <param name="ultraGrid"></param>
        public void SetGridStyle4(UltraGrid ultraGrid)
        {
            // 컬럼 헤더 위치 이동시 화살표 색
            ultraGrid.DisplayLayout.Override.DragDropIndicatorSettings.Color = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(255)));

            // Active Cell의 보더 색
            ultraGrid.DisplayLayout.Override.ActiveCellAppearance.BorderColor =  System.Drawing.Color.FromArgb(((System.Byte)(128)), ((System.Byte)(128)), ((System.Byte)(255)));
            // Set the ActiveCellBorderThickness
            ultraGrid.DisplayLayout.Override.ActiveCellBorderThickness = 2;

            // Set the SelectionOverlayColor. Note that if a solid color is applied, the grid
            // will make it semi-transparent. But if the applied color is already semi-transparent
            // (Alpha < 255), then it will be used as-is. 
            ultraGrid.DisplayLayout.SelectionOverlayColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(192)), ((System.Byte)(255)));
            // Set the SelectionOverlayBorderColor 
            ultraGrid.DisplayLayout.SelectionOverlayBorderColor = System.Drawing.Color.FromArgb(((System.Byte)(128)), ((System.Byte)(128)), ((System.Byte)(255)));
            // Set the SelectionOverlayBorderThickness
            ultraGrid.DisplayLayout.SelectionOverlayBorderThickness = 2;

            // 선택된 컬럼 헤더 색 변경
            ultraGrid.DisplayLayout.Override.ActiveCellColumnHeaderAppearance.BackColor= System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(255)));
            // 선택된 Row selector 변경
            ultraGrid.DisplayLayout.Override.ActiveCellRowSelectorAppearance.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(255)));

        }

        /// <summary>
        /// 태그 임포트 화면에서 사용할 그리드 셋팅
        /// </summary>
        /// <param name="ultraGrid"></param>
        public void SetGridStyle_TagImport(UltraGrid ultraGrid)
        {
            ultraGrid.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            ultraGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            ultraGrid.DisplayLayout.GroupByBox.BorderStyle = UIElementBorderStyle.Solid;
            ultraGrid.DisplayLayout.GroupByBox.Hidden = true;
            ultraGrid.DisplayLayout.MaxColScrollRegions = 1;
            ultraGrid.DisplayLayout.MaxRowScrollRegions = 1;

            ultraGrid.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Solid;
            ultraGrid.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Solid;
            ultraGrid.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
            ultraGrid.DisplayLayout.Override.CellPadding = 0;
            ultraGrid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            ultraGrid.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;
            ultraGrid.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            ultraGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            ultraGrid.DisplayLayout.MaxColScrollRegions = 1;
            ultraGrid.DisplayLayout.MaxRowScrollRegions = 1;
        }
#endregion
    }
}
