﻿using System;
using System.Collections;
using System.Data;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;

namespace WaterNETServer.dao
{
    public class OracleWork : BaseWork
    {
        private OracleDao wDao = null;

        public OracleWork()
        {
            wDao = new OracleDao();
        }

        /// <summary>
        /// 태그 정보 조회
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        internal DataSet SelectTagInformation(string tableName, Hashtable param)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.SelectTagInformationDao(manager, result, tableName, param);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 공통코드 값을 조회
        /// 코드 마스터에 의한 코드명과 코드 값을 조회한다
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        internal DataSet SelectCodeList(string tableName, Hashtable param)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.selectCodeListDao(manager, result, tableName, param);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 공통코드 값을 조회
        /// 코드 마스터에 의한 코드명과 코드 값을 조회한다
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        internal DataSet SelectCodeWithValueList(string tableName, Hashtable param)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.selectCodeListWithValueDao(manager, result, tableName, param);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        internal DataSet SelectTagGbn(string tableName, Hashtable param)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.SelectTagGbn(manager, result, tableName, param);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        internal DataSet SelectIOGbn(string tableName, Hashtable param)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.SelectIOGbn(manager, result, tableName, param);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        internal DataSet SelectCmLocation(string tableName, Hashtable param)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.SelectCmLocation(manager, result, tableName, param);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 태그 정보 업데이트
        /// </summary>
        /// <param name="dsName"></param>
        /// <param name="tableName"></param>
        internal void UpdateTags(DataTable datatable)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.UpdateTags(manager, datatable);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// Timestamp가 써진 최종시간을 조회
        /// </summary>
        internal DateTime SelectMaxTimestampMinute()
        {
            DateTime dt = new DateTime();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                string rtnTimestamp = wDao.SelectMaxTimestampMinute_Dao(manager);

                if ("".Equals(rtnTimestamp))
                {
                    dt = DateTime.ParseExact("200801010000", "yyyyMMddHHmm", null);
                }
                else
                {
                    dt = DateTime.ParseExact(rtnTimestamp, "yyyyMMddHHmm", null);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return dt;
        }

        /// <summary>
        /// Timestamp가 써진 최종시간을 조회
        /// </summary>
        /// <returns></returns>
        internal DateTime SelectMaxTimestampHour()
        {
            DateTime dt = new DateTime();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                string rtnTimestamp = wDao.SelectMaxTimestampHour_Dao(manager);

                if ("".Equals(rtnTimestamp))
                {
                    dt = DateTime.ParseExact("2008010100", "yyyyMMddHH", null);
                }
                else
                {
                    dt = DateTime.ParseExact(rtnTimestamp, "yyyyMMddHH", null);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return dt;
        }

        /// <summary>
        /// 태그 임포트
        /// Water-NET 태그 리스트를 조회
        /// </summary>
        internal DataSet GetWaternetTagList(string tableName, Hashtable cond)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.GetWaternetTagList(manager, result, tableName, cond);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        internal DataSet GetRWISTagList(string tableName, Hashtable cond)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.RWIS_DATABASE);
                wDao.GetRWISTagList(manager, result, tableName, cond);
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            finally
            {
                manager.Dispose();
            }
            return result;
        }

        /// <summary>
        /// 태그 목록을 Water-NET에 저장
        /// </summary>
        internal int CommitTagList(DataTable updatedGrid)
        {
            int returnValue = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnValue = wDao.CommitTagList(manager, updatedGrid);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// 테스트용 : Historian DB 접속 대용으로 임시 사용
        /// </summary>
        internal DataSet GetHistorianTagList(string tableName, Hashtable cond)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.GetHistorianTagList(manager, result, tableName, cond);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Water-NET 태그 목록을 삭제
        /// </summary>
        internal int DeleteSelectedTag(DataTable dt)
        {
            int returnValue = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnValue = wDao.DeleteSelectedTag(manager, dt);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// 태그 구분
        /// Water-NET 태그 리스트 조회
        /// </summary>
        internal DataSet GetTagList(string tableName, Hashtable cond)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.GetTagList(manager, result, tableName, cond);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 가상태그 생성
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="vtag_gbn">
        /// 1H : 시간적산태그
        /// TD : 금일적산태그
        /// YD : 전일적산태그
        /// </param>
        /// <returns></returns>
        internal int CreateVirtualTag(DataTable dt, string vtag_gbn)
        {
            int returnValue = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnValue = wDao.CreateVirtualTag(manager, dt, vtag_gbn);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }
        /// <summary>
        /// 태그 구분 조회
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="cond"></param>
        /// <returns></returns>
        internal DataSet GetTagGbn(string tableName, Hashtable cond)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                wDao.GetTagGbn(manager, result, tableName, cond);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }
        /// <summary>
        /// 태그구분 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal int DeleteTagGbn(DataTable dt)
        {
            int returnValue = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnValue = wDao.DeleteTagGbn(manager, dt);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }

        /// <summary>
        /// 태그구분 입력
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal int ImportTagGbn(DataTable dt, string vtag_gbn, Hashtable MessagePool)
        {
            int returnValue = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnValue = wDao.ImportTagGbn(manager, dt, vtag_gbn, MessagePool);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }
        /// <summary>
        /// 태그리스트 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal int DeleteTagList(DataTable dt)
        {
            int returnValue = 0;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                returnValue = wDao.DeleteTagList(manager, dt);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return returnValue;
        }
    }
}