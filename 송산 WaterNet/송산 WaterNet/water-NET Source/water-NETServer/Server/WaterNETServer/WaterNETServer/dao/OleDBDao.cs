﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNETServer.Common.DB.HistorianOLE;
using System.Data;
using System.Collections;
using EMFrame.log;

namespace WaterNETServer.dao
{
    public class OleDBDao
    {
        public OleDBDao() { }

        internal void GetHistorianTagList(OleDBManager oleDBManager, DataSet result, string tableName, Hashtable cond)
        {
            StringBuilder query = new StringBuilder();
            int whereCnt = 0;
            try
            {
                query.AppendLine("SELECT TAGNAME, DESCRIPTION FROM IHTAGS");
                if (!cond["TAGNAME"].ToString().Equals(""))
                {
                    if (whereCnt == 0)
                    {
                        query.Append("WHERE ");
                        whereCnt++;
                    }
                    else query.Append("AND ");

                    query.Append("TAGNAME LIKE '%").Append(cond["TAGNAME"].ToString()).AppendLine("%'");
                }
                if (!cond["DESCRIPTION"].ToString().Equals(""))
                {
                    if (whereCnt == 0)
                    {
                        query.Append("WHERE ");
                        whereCnt++;
                    }
                    else query.Append("AND ");

                    query.Append("DESCRIPTION LIKE '%").Append(cond["DESCRIPTION"].ToString()).AppendLine("%'");
                }

                oleDBManager.FillDataSetScript(result, tableName, query.ToString(), null);
            }
            catch (Exception e)
            {
                Logger.Error(query.ToString());
                Logger.Error(e.ToString());
            }
        }
    }
}
