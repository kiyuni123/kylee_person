﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using Oracle.DataAccess.Client;
using EMFrame.dm;
using EMFrame.log;

namespace WaterNETServer.dao
{
    public class OracleDao
    {
        public OracleDao() { }

        /// <summary>
        /// 공통코드 값을 조회
        /// 코드 마스터에 의한 코드명과 코드 값을 조회한다
        /// 
        /// 사용
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="dataMember"></param>
        /// <param name="param">코드마스터 키</param>
        public void selectCodeListDao(EMapper manager, DataSet dataset, string dataMember, Hashtable param)
        {
            StringBuilder query = new StringBuilder();
            
            query.AppendLine("select b.code");
            query.AppendLine("     , b.code_name");
            query.AppendLine("  from cm_code_master a, cm_code b");
            query.AppendLine(" where A.PCODE = B.PCODE");
            query.AppendLine("   and a.pcode = '" + param["PCODE"].ToString() + "'");
            query.AppendLine("   and b.use_yn = 'Y' ");

            manager.FillDataSetScript(dataset, dataMember, query.ToString(), null);
        }

        /// <summary>
        /// 공통코드 값을 조회
        /// 코드 마스터에 의한 코드명과 코드 값을 조회한다
        /// 
        /// 사용
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="dataMember"></param>
        /// <param name="param">코드마스터 키</param>
        public void selectCodeListWithValueDao(EMapper manager, DataSet dataset, string dataMember, Hashtable param)
        {
            StringBuilder query = new StringBuilder();
            
            query.AppendLine("select b.code");
            query.AppendLine("     , '(' || b.code || ') ' ||  b.code_name as code_name");
            query.AppendLine("  from cm_code_master a, cm_code b");
            query.AppendLine(" where A.PCODE = B.PCODE");
            query.AppendLine("   and a.pcode = '" + param["PCODE"].ToString() + "'");
            query.AppendLine("   and b.use_yn = 'Y' ");

            manager.FillDataSetScript(dataset, dataMember, query.ToString(), null);
        }

        /// <summary>
        /// 태그구분 값을 조회
        /// 태그정보에서 태그 구분을 조회한다 (태그구분 = 레이어명)
        /// 
        /// 사용
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="dataMember"></param>
        /// <param name="param"></param>
        internal void SelectTagGbn(EMapper manager, DataSet dataset, string dataMember, Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select distinct tag_gbn, tag_gbn");
            query.AppendLine("  from if_ihtags");
            query.AppendLine(" where tag_gbn is not null");

            manager.FillDataSetScript(dataset, dataMember, query.ToString(), null);
        }

        /// <summary>
        /// 입출력 구분을 조회
        /// 
        /// 사용
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="dataMember"></param>
        /// <param name="param"></param>
        internal void SelectIOGbn(EMapper manager, DataSet dataset, string dataMember, Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select distinct io_gbn, io_gbn");
            query.AppendLine("  from if_ihtags");
            query.AppendLine(" where io_gbn is not null");

            manager.FillDataSetScript(dataset, dataMember, query.ToString(), null);
        }

        /// <summary>
        /// 위치코드를 조회
        /// 
        /// 사용
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="dataMember"></param>
        /// <param name="param"></param>
        internal void SelectCmLocation(EMapper manager, DataSet dataset, string dataMember, Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc_code, loc_name");
            query.AppendLine("  from cm_location");

            manager.FillDataSetScript(dataset, dataMember, query.ToString(), null);
        }

        /// <summary>
        /// 태그 정보 조회
        /// 
        /// 사용
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="result"></param>
        /// <param name="tableName"></param>
        internal void SelectTagInformationDao(EMapper manager, DataSet result, string tableName, Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TAGNAME as 태그명");
            query.AppendLine(" 	, BONBU as 본부");
            query.AppendLine("	, SAMUSO as 사무소");
            query.AppendLine("	, SAUPJANG as 사업장");
            query.AppendLine("	, BR_CODE as 변량기호");
            query.AppendLine("	, FN_CODE as 기능기호");
            query.AppendLine("	, GT_CODE as 계통코드");
            query.AppendLine("	, GB_NO as 구분번호");
            query.AppendLine("	, BESUJI as 배수지구분");
            query.AppendLine("	, TAG_GBN as 태그구분");
            query.AppendLine("	, IO_GBN as 입출구분");
            //query.AppendLine("	, PTAGNAME as 관련태그");
            query.AppendLine("	, DESCRIPTION as 태그설명");
            query.AppendLine("	, USE_YN as 사용여부");
            query.AppendLine("	, FTR_IDN as GIS코드");
            query.AppendLine("	, LOC_CODE as 블록정보");
            query.AppendLine("	, LOC_CODE2 as 블록정보2");
            query.AppendLine("	, COEFFICIENT as 승수");
            query.AppendLine("  FROM IF_IHTAGS ");
            query.AppendLine(" WHERE 1=1");

            if (!param["textbox1"].ToString().Equals(""))
            {
                query.AppendLine("   AND TAGNAME like '%" + param["textbox1"].ToString() + "%'");
            }
            if (!param["textbox2"].ToString().Equals(""))
            {
                query.AppendLine("   AND DESCRIPTION like '%" + param["textbox2"].ToString() + "%'");
            }
            if (!param["combobox1"].ToString().Equals(""))
            {
                query.AppendLine("   AND BONBU = '" + param["combobox1"].ToString() + "'");
            }
            if (!param["combobox2"].ToString().Equals(""))
            {
                query.AppendLine("   AND SAMUSO = '" + param["combobox2"].ToString() + "'");
            }
            if (!param["combobox3"].ToString().Equals(""))
            {
                query.AppendLine("   AND SAUPJANG = '" + param["combobox3"].ToString() + "'");
            }
            if (!param["combobox4"].ToString().Equals(""))
            {
                query.AppendLine("   AND BR_CODE = '" + param["combobox4"].ToString() + "'");
            }
            if (!param["combobox5"].ToString().Equals(""))
            {
                query.AppendLine("   AND FN_CODE = '" + param["combobox5"].ToString() + "'");
            }
            if (!param["combobox6"].ToString().Equals(""))
            {
                query.AppendLine("   AND TAG_GBN = '" + param["combobox6"].ToString() + "'");
            }
            if (!param["combobox7"].ToString().Equals(""))
            {
                query.AppendLine("   AND IO_GBN = '" + param["combobox7"].ToString() + "'");
            }
            if (!param["combobox8"].ToString().Equals(""))
            {
                query.AppendLine("   AND LOC_CODE = '" + param["combobox8"].ToString() + "'");
            }
            if (!param["combobox9"].ToString().Equals(""))
            {
                query.AppendLine("   AND USE_YN = '" + param["combobox9"].ToString() + "'");
            }

            manager.FillDataSetScript(result, tableName, query.ToString(), null);
        }

        /// <summary>
        /// 수정된 Grid 값에 의해 Tag정보를 수정한다
        /// 
        /// 사용
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="datatable"></param>
        public void UpdateTags(EMapper manager, DataTable datatable)
        {
            StringBuilder query = new StringBuilder();

            #region 쿼리문
            query.AppendLine("UPDATE IF_IHTAGS SET ");
            query.AppendLine("	 BONBU = :1");
            query.AppendLine("	,SAMUSO = :2");
            query.AppendLine("	,SAUPJANG = :3");
            query.AppendLine("	,BR_CODE = :4");
            query.AppendLine("	,FN_CODE = :5");
            query.AppendLine("	,GT_CODE = :6");
            query.AppendLine("	,GB_NO = :7");
            query.AppendLine("	,BESUJI = :8");
            query.AppendLine("	,TAG_GBN = :9");
            query.AppendLine("	,IO_GBN = :10");
            //query.AppendLine("	,PTAGNAME = :11");
            query.AppendLine("	,DESCRIPTION = :11");
            query.AppendLine("	,USE_YN = :12");
            query.AppendLine("	,FTR_IDN = :13");
            query.AppendLine("	,LOC_CODE = :14");
            query.AppendLine("	,LOC_CODE2 = :15");
            query.AppendLine("	,COEFFICIENT = :16");
            query.AppendLine("WHERE TAGNAME = :17");
            #endregion

            foreach (DataRow row in datatable.Rows)
            {
                #region 파라미터
                IDataParameter[] parameters =  
                    {
                         new OracleParameter("1", OracleDbType.Varchar2)
                        ,new OracleParameter("2", OracleDbType.Varchar2)
                        ,new OracleParameter("3", OracleDbType.Varchar2)
                        ,new OracleParameter("4", OracleDbType.Varchar2)
                        ,new OracleParameter("5", OracleDbType.Varchar2)
                        ,new OracleParameter("6", OracleDbType.Varchar2)
                        ,new OracleParameter("7", OracleDbType.Varchar2)
                        ,new OracleParameter("8", OracleDbType.Varchar2)
                        ,new OracleParameter("9", OracleDbType.Varchar2)
                        ,new OracleParameter("10", OracleDbType.Varchar2)
                        ,new OracleParameter("11", OracleDbType.Varchar2)
                        ,new OracleParameter("12", OracleDbType.Varchar2)
                        ,new OracleParameter("13", OracleDbType.Varchar2)
                        ,new OracleParameter("14", OracleDbType.Varchar2)
                        ,new OracleParameter("15", OracleDbType.Varchar2)
                        ,new OracleParameter("16", OracleDbType.Varchar2)
                        ,new OracleParameter("17", OracleDbType.Varchar2)
                    };
                #endregion

                parameters[16].Value = row[0].ToString();
                for (int i = 0; i < datatable.Columns.Count - 1; i++)
                {
                    parameters[i].Value = row[i + 1].ToString();
                }
                manager.ExecuteScript(query.ToString(), parameters);
            }
        }


        /// <summary>
        /// Timestamp가 써진 최종시간을 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        internal string SelectMaxTimestampMinute_Dao(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT MAX(YYYYMMDDHH24MI) FROM IF_TIMESTAMP_YYYYMMDDHH24MI");

            return manager.ExecuteScriptScalar(query.ToString(), null).ToString();
        }

        /// <summary>
        /// Timestamp가 써진 최종시간을 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        internal string SelectMaxTimestampHour_Dao(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT MAX(YYYYMMDDHH24) FROM IF_TIMESTAMP_YYYYMMDDHH24");

            return manager.ExecuteScriptScalar(query.ToString(), null).ToString();
        }

        /// <summary>
        /// 태그 임포트
        /// Water-NET 태그 리스트를 조회
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="result"></param>
        /// <param name="tableName"></param>
        /// <param name="cond"></param>
        internal void GetWaternetTagList(EMapper manager, DataSet result, string tableName, Hashtable cond)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TAGNAME, DESCRIPTION FROM IF_IHTAGS WHERE TAGNAME NOT LIKE '%_1H' AND TAGNAME NOT LIKE '%_TD' AND TAGNAME NOT LIKE '%_YD'");

            manager.FillDataSetScript(result, tableName, query.ToString(), null);
        }

        internal void GetRWISTagList(EMapper manager, DataSet result, string tableName, Hashtable cond)
        {
            StringBuilder query = new StringBuilder();
            int whereCnt = 0;
            try
            {
                query.AppendLine("SELECT TAGSN TAGNAME, TAG_DESC DESCRIPTION FROM RDITAG_VW");
                if (!cond["TAGNAME"].ToString().Equals(""))
                {
                    if (whereCnt == 0)
                    {
                        query.Append("WHERE ");
                        whereCnt++;
                    }
                    else query.Append("AND ");

                    query.Append("TAGSN LIKE '%").Append(cond["TAGNAME"].ToString()).AppendLine("%'");
                }
                if (!cond["DESCRIPTION"].ToString().Equals(""))
                {
                    if (whereCnt == 0)
                    {
                        query.Append("WHERE ");
                        whereCnt++;
                    }
                    else query.Append("AND ");

                    query.Append("TAG_DESC LIKE '%").Append(cond["DESCRIPTION"].ToString()).AppendLine("%'");
                }

                manager.FillDataSetScript(result, tableName, query.ToString(), null);
            }
            catch (Exception e)
            {
                Logger.Error(query.ToString());
                Logger.Error(e.ToString());
            }
        }

        /// <summary>
        /// 태그 목록을 Water-NET에 저장
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="updatedGrid"></param>
        /// <returns></returns>
        internal int CommitTagList(EMapper manager, DataTable updatedGrid)
        {
            int returnValue = 0;
            StringBuilder query = new StringBuilder();
            
            query.AppendLine("INSERT INTO IF_IHTAGS (TAGNAME, DESCRIPTION) VALUES (:1, :2)");

            foreach (DataRow row in updatedGrid.Rows)
            {
                IDataParameter[] parameters = 
                    {
                             new OracleParameter("1", OracleDbType.Varchar2)
                            ,new OracleParameter("2", OracleDbType.Varchar2)
                    };

                for (int i = 0; i < updatedGrid.Columns.Count - 1; i++)
                {
                    parameters[i].Value = row[i].ToString();
                }
                returnValue += manager.ExecuteScript(query.ToString(), parameters);
            }

            return returnValue;
        }
        /// <summary>
        /// 테스트용 : Historian DB 접속 대용으로 임시 사용
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="result"></param>
        /// <param name="tableName"></param>
        /// <param name="cond"></param>
        internal void GetHistorianTagList(EMapper manager, DataSet result, string tableName, Hashtable cond)
        {
            StringBuilder query = new StringBuilder();

            query.Append("SELECT tagname, description from if_ihtags");

            manager.FillDataSetScript(result, tableName, query.ToString(), null);
        }
        /// <summary>
        /// Water-NET 태그 목록을 삭제
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal int DeleteSelectedTag(EMapper manager, DataTable dt)
        {
            int returnValue = 0;
            StringBuilder query = new StringBuilder();
            
            query.AppendLine("DELETE FROM IF_IHTAGS WHERE TAGNAME = :1");

            foreach (DataRow row in dt.Rows)
            {
                IDataParameter[] param = 
                    {
                        new OracleParameter("1", OracleDbType.Varchar2)
                    };

                param[0].Value = row[0].ToString();

                returnValue += manager.ExecuteScript(query.ToString(), param);
            }

            return returnValue;
        }
        /// <summary>
        /// 태그 구분
        /// Water-NET 태그 리스트 조회
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="result"></param>
        /// <param name="tableName"></param>
        /// <param name="cond"></param>
        internal void GetTagList(EMapper manager, DataSet result, string tableName, Hashtable cond)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("SELECT TAGNAME, DESCRIPTION, BR_CODE, FN_CODE FROM IF_IHTAGS2");
            query.AppendLine("SELECT TAGNAME, DESCRIPTION, BR_CODE, FN_CODE FROM IF_IHTAGS");
            query.AppendLine("WHERE 1=1");
            if (!"".Equals(cond["TAGNAME"].ToString()))
            {
                query.Append("AND TAGNAME like '%").Append(cond["TAGNAME"].ToString()).AppendLine("%'");
            }
            if (!"".Equals(cond["DESCRIPTION"].ToString()))
            {
                query.Append("AND DESCRIPTION like '%").Append(cond["DESCRIPTION"].ToString()).AppendLine("%'");
            }
            if (!"".Equals(cond["BR_CODE"].ToString()))
            {
                query.Append("AND BR_CODE = '").Append(cond["BR_CODE"].ToString()).AppendLine("'");
            }
            if (!"".Equals(cond["FN_CODE"].ToString()))
            {
                query.Append("AND FN_CODE = '").Append(cond["FN_CODE"].ToString()).AppendLine("'");
            }
            if (!"".Equals(cond["USE_YN"].ToString()))
            {
                query.Append("AND USE_YN = '").Append(cond["USE_YN"].ToString()).AppendLine("'");
            }
            if (!"".Equals(cond["VTAG"].ToString()))
            {
                if ("VTAG".Equals(cond["VTAG"].ToString()))
                {
                    query.Append("AND (TAGNAME LIKE '%_1H' OR TAGNAME LIKE '%_TD' OR TAGNAME LIKE '%_YD') ");
                }
                if ("RTAG".Equals(cond["VTAG"].ToString()))
                {
                    query.Append("AND (TAGNAME NOT LIKE '%_1H' AND TAGNAME NOT LIKE '%_TD' AND  TAGNAME NOT LIKE '%_YD') ");
                }
            }
            query.AppendLine("ORDER BY TAGNAME ASC");

            manager.FillDataSetScript(result, tableName, query.ToString(), null);
        }

        /// <summary>
        /// 태그가 존재하는지 검사
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <returns></returns>
        private int DuplicationCheck(EMapper manager, string tagname)
        {
            string query = "SELECT COUNT(*) CNT FROM IF_IHTAGS WHERE TAGNAME = :1 ";

            IDataParameter[] parameters = { new OracleParameter("1", OracleDbType.Varchar2) };

            parameters[0].Value = tagname;

            return Convert.ToInt16(manager.ExecuteScriptScalar(query.ToString(), parameters));
        }

        /// <summary>
        /// 가상태그 생성
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dt"></param>
        /// <param name="vtag_gbn">
        /// 1H : 시간적산태그
        /// TD : 금일적산태그
        /// YD : 전일적산태그
        /// </param>
        /// <returns></returns>
        internal int CreateVirtualTag(EMapper manager, DataTable dt, string vtag_gbn)
        {
            int returnValue = 0;
            string vtag_gbn_msg = string.Empty;
            StringBuilder query = new StringBuilder();

            //query.AppendLine("INSERT INTO IF_IHTAGS2");
            query.AppendLine("INSERT INTO IF_IHTAGS");
            query.AppendLine("SELECT :1,          BONBU,    SAMUSO,   SAUPJANG,   BR_CODE,     FN_CODE");
            query.AppendLine("     , GT_CODE,     GB_NO,    BESUJI,   TAG_GBN,    IO_GBN,      PTAGNAME");
            query.AppendLine("     , :2,          USE_YN,   FTR_IDN,  LOC_CODE,   COEFFICIENT, LOC_CODE2");
            //query.AppendLine("FROM   IF_IHTAGS2");
            query.AppendLine("FROM   IF_IHTAGS");
            query.AppendLine("WHERE  TAGNAME = :3");

            if ("1H".Equals(vtag_gbn))
            {
                vtag_gbn_msg = "(1시간적산)";
            }
            if ("TD".Equals(vtag_gbn))
            {
                vtag_gbn_msg = "(금일적산)";
            }
            if ("YD".Equals(vtag_gbn))
            {
                vtag_gbn_msg = "(전일적산)";
            }

            foreach (DataRow row in dt.Rows)
            {
                IDataParameter[] parameters = 
                    {
                             new OracleParameter("1", OracleDbType.Varchar2)
                            ,new OracleParameter("2", OracleDbType.Varchar2)
                            ,new OracleParameter("3", OracleDbType.Varchar2)
                    };

                parameters[0].Value = row[0].ToString() + "_" + vtag_gbn;
                parameters[1].Value = row[1].ToString() + vtag_gbn_msg;
                parameters[2].Value = row[0].ToString();

                // 이미 입력된것이 있다면 패스 
                if (!(DuplicationCheck(manager, parameters[0].Value.ToString()) > 0))
                {
                    returnValue += manager.ExecuteScript(query.ToString(), parameters);
                }
            }

            return returnValue;
        }
        /// <summary>
        /// 태그구분 조회
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="result"></param>
        /// <param name="tableName"></param>
        /// <param name="cond"></param>
        internal void GetTagGbn(EMapper manager, DataSet result, string tableName, Hashtable cond)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TAGNAME, TAG_GBN, TAG_DESC AS DESCRIPTION FROM IF_TAG_GBN");
            query.AppendLine("WHERE 1=1");
            if (!"".Equals(cond["TAGNAME"].ToString()))
            {
                query.Append("AND TAGNAME LIKE '%").Append(cond["TAGNAME"].ToString()).AppendLine("%'");
            }
            if (!"".Equals(cond["TAG_GBN"].ToString()))
            {
                query.Append("AND TAG_GBN = '").Append(cond["TAG_GBN"].ToString()).AppendLine("'");
            }
            if (!"".Equals(cond["DESCRIPTION"].ToString()))
            {
                query.Append("AND TAG_DESC LIKE '%").Append(cond["DESCRIPTION"].ToString()).AppendLine("%'");
            }

            manager.FillDataSetScript(result, tableName, query.ToString(), null);
        }
        /// <summary>
        /// 태그구분 삭제
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal int DeleteTagGbn(EMapper manager, DataTable dt)
        {
            int returnValue = 0;
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_TAG_GBN WHERE TAGNAME = :1 AND TAG_GBN = :2 ");

            foreach (DataRow row in dt.Rows)
            {
                IDataParameter[] param = 
                    {
                        new OracleParameter("1", OracleDbType.Varchar2)
                        ,new OracleParameter("2", OracleDbType.Varchar2)
                    };

                param[0].Value = row[0].ToString();
                param[1].Value = row[1].ToString();

                returnValue += manager.ExecuteScript(query.ToString(), param);
            }

            return returnValue;
        }
        /// <summary>
        /// 태그구분 입력
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal int ImportTagGbn(EMapper manager, DataTable dt, string vtag_gbn, Hashtable MessagePool)
        {
            int returnValue = 0;
            int dupCnt = 0;
            int vldCnt = 0;

            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_TAG_GBN (TAGNAME, TAG_GBN, TAG_DESC, DUMMY_GBN, DUMMY_RELATION) VALUES (:1, :2, :3, :4, :5)");

            foreach (DataRow row in dt.Rows)
            {
                #region Validation 체크

                // 중복 체크
                if (DuplicationCheckTagGbn(manager, row[0].ToString(), vtag_gbn) > 0)
                {
                    dupCnt++;
                    if (MessagePool.Contains("DUP"))
                    {
                        MessagePool.Remove("DUP");
                        MessagePool.Add("DUP", dupCnt);
                    }
                    else
                    {
                        MessagePool.Add("DUP", dupCnt);
                    }
                    continue;
                }

                if (ValidationCheckTagGbn(manager, row[0].ToString(), vtag_gbn) > 0)
                {
                    vldCnt++;
                    if (MessagePool.Contains("VLD"))
                    {
                        MessagePool.Remove("VLD");
                        MessagePool.Add("VLD", vldCnt);
                    }
                    else
                    {
                        MessagePool.Add("VLD", vldCnt);
                    }
                    continue;
                }

                #endregion

                string dummy_gbn = string.Empty;
                string dummy_relation = string.Empty;

                if (row[0].ToString().IndexOf("1H") > 0 || row[0].ToString().IndexOf("TD") > 0 || row[0].ToString().IndexOf("YD") > 0)
                {
                    dummy_gbn = "DUMMY";
                    dummy_relation = row[0].ToString().Replace("_1H", "").Replace("_TD", "").Replace("_YD", "");
                }

                IDataParameter[] parameters = 
                    {
                         new OracleParameter("1", OracleDbType.Varchar2)
                        ,new OracleParameter("2", OracleDbType.Varchar2)
                        ,new OracleParameter("3", OracleDbType.Varchar2)
                        ,new OracleParameter("4", OracleDbType.Varchar2)
                        ,new OracleParameter("5", OracleDbType.Varchar2)
                    };

                parameters[0].Value = row[0].ToString();
                parameters[1].Value = vtag_gbn;
                parameters[2].Value = row[1].ToString();
                parameters[3].Value = dummy_gbn;
                parameters[4].Value = dummy_relation;

                returnValue += manager.ExecuteScript(query.ToString(), parameters);
            }

            return returnValue;
        }
        /// <summary>
        /// 태그구분이 있는지 검사한다
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="taggbn"></param>
        /// <returns></returns>
        private int DuplicationCheckTagGbn(EMapper manager, string tagname, string taggbn)
        {
            string query = "SELECT COUNT(*) CNT FROM IF_TAG_GBN WHERE TAGNAME = :1 AND TAG_GBN = :2 ";

            IDataParameter[] parameters = 
                { 
                    new OracleParameter("1", OracleDbType.Varchar2) 
                    ,new OracleParameter("2", OracleDbType.Varchar2) 
                };

            parameters[0].Value = tagname;
            parameters[1].Value = taggbn;

            return Convert.ToInt16(manager.ExecuteScriptScalar(query.ToString(), parameters));
        }
        /// <summary>
        /// 태그구분 벨리데이션 체크 
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="tag_gbn"></param>
        /// <returns></returns>
        private int ValidationCheckTagGbn(EMapper manager, string tagname, string tag_gbn)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT COUNT(*) CNT");
            query.AppendLine("FROM   IF_IHTAGS A");
            query.AppendLine("     , IF_TAG_GBN B");
            query.AppendLine("WHERE  A.TAGNAME = B.TAGNAME");
            query.AppendLine("AND    A.LOC_CODE = ( SELECT A.LOC_CODE");
            query.AppendLine("                      FROM   IF_IHTAGS A");
            query.AppendLine("                           , CM_LOCATION B");
            query.AppendLine("                      WHERE  A.TAGNAME  = :1 ");
            query.AppendLine("                      AND    A.LOC_CODE = B.LOC_CODE");
            query.AppendLine("                    )");
            query.AppendLine("AND    B.TAG_GBN = :2 ");

            IDataParameter[] parameters = 
                { 
                    new OracleParameter("1", OracleDbType.Varchar2) 
                    ,new OracleParameter("2", OracleDbType.Varchar2) 
                };

            parameters[0].Value = tagname;
            parameters[1].Value = tag_gbn;

            return Convert.ToInt16(manager.ExecuteScriptScalar(query.ToString(), parameters));
        }
        /// <summary>
        /// 태그리스트 삭제
        /// ############################################################################################
        /// ##################### 테이블명 반드시 수정 필요 현재 테스트용 테이블  ######################
        /// ############################################################################################
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        internal int DeleteTagList(EMapper manager, DataTable dt)
        {
            int returnValue = 0;
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_IHTAGS WHERE TAGNAME = :1 ");

            foreach (DataRow row in dt.Rows)
            {
                IDataParameter[] param = 
                    {
                        new OracleParameter("1", OracleDbType.Varchar2)
                    };

                param[0].Value = row[0].ToString();
                returnValue += manager.ExecuteScript(query.ToString(), param);
            }

            return returnValue;
        }
    }
}