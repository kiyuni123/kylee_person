﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNETServer.Common.DB.HistorianOLE;
using System.Data;
using System.Collections;
using EMFrame.log;

namespace WaterNETServer.dao
{
    public class OleDBWork : OleDBBaseDao
    {
        private OleDBDao hDao = null;
        public OleDBWork()
        {
            hDao = new OleDBDao();
        }

        internal DataSet GetHistorianTagList(string tableName, Hashtable cond)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                hDao.GetHistorianTagList(oleDBManager, result, tableName, cond);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
            finally
            {
                ConnectionClose();
            }
            return result;
        }
    }
}
