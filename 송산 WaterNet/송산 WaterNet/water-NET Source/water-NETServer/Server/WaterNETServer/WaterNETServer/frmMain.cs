﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using WaterNETServer.Common;
using WaterNETServer.Common.DB.Oracle;
using WaterNETServer.Common.utils;
using Infragistics.Win.UltraWinTabControl;
using Oracle.DataAccess.Client;
using WaterNETServer.DF_Simulation;
using WaterNETServer.form;
using WaterNETServer.formList;
using WaterNETServer.formPopup;
using WaterNETServer.WH_AnalysisScheduleManage.etc;
using WaterNETServer.WH_AnalysisScheduleManage.form;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNETServer.Common.DB.HistorianOLE;
using EMFrame.dm;
using WaterNETServer.Warning.work;
using WaterNETServer.ScheduleManage.form;
using System.Xml;
using EMFrame.log;

namespace WaterNETServer
{
    public partial class frmMain : Form
    {
        #region 선언부
        /// <summary>
        /// 타이머 관리할 전역 풀 생성
        /// </summary>
        public static Hashtable threadPool = new Hashtable();

        /// <summary>
        /// 글로벌 환경설정 파일
        /// </summary>
        GlobalVariable gVar = new GlobalVariable();
       
        //경보처리 비즈니스 로직객체
        WarningWork work = null;

        //경보처리 타이머 및 delegation
        private System.Timers.Timer timer = null;
        private delegate void ApplyWarningListCallback(DataTable warningTable);

        public frmMain()
        {
            InitializeComponent();
            InitializeSetting();
        }

        private void InitializeSetting()
        {
            if (!gVar.isHome())
            {
                //Database Connection Test
                frmDatabase oForm = new frmDatabase();

                if (!ConnectionTest())
                {
                    oForm.parentForm = this;
                    oForm.ShowDialog();
                }

                EMapper.ConnectionString[AppStatic.WATERNET_DATABASE] = OracleDBFunctionManager.GetWaterNETConnectionString();
                EMapper.ConnectionString[AppStatic.RWIS_DATABASE] = OracleDBFunctionManager.GetRWISConnectionString();
            }

            if (gVar.isDeployFtp())
            {
                UpgradeToWaterNETUpdater();
            }

            #region 하단 경고 그리드 설정

            UltraGridColumn warningColumn;

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WARN_NO";
            warningColumn.Header.Caption = "WARN_NO";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "CREATE_DATE";
            warningColumn.Header.Caption = "경보일자";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "FIXED_DATE";
            warningColumn.Header.Caption = "확인(처리)일자";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WORK_GBN";
            warningColumn.Header.Caption = "작업구분코드";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "LOC_CODE";
            warningColumn.Header.Caption = "지역코드";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WAR_CODE";
            warningColumn.Header.Caption = "경보코드";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "LOC_NAME";
            warningColumn.Header.Caption = "지역";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 100;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WORK_GBN_NAME";
            warningColumn.Header.Caption = "작업구분";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 100;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "WAR_CODE_NAME";
            warningColumn.Header.Caption = "경보내용";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Left;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "CHECK_YN";
            warningColumn.Header.Caption = "확인여부";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 80;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "FIXED_YN";
            warningColumn.Header.Caption = "처리여부";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 80;
            warningColumn.Hidden = false;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "SERIAL_NO";
            warningColumn.Header.Caption = "SERIAL_NO";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Center;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 150;
            warningColumn.Hidden = true;   //필드 보이기

            warningColumn = griWarningList.DisplayLayout.Bands[0].Columns.Add();
            warningColumn.Key = "REMARK";
            warningColumn.Header.Caption = "비고";
            warningColumn.CellActivation = Activation.NoEdit;
            warningColumn.CellClickAction = CellClickAction.RowSelect;
            warningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            warningColumn.CellAppearance.TextHAlign = HAlign.Left;
            warningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            warningColumn.Width = 400;
            warningColumn.Hidden = false;   //필드 보이기

            #endregion
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // KeyPress 이벤트를 등록
            this.KeyPress += new KeyPressEventHandler(this.GetKeyPress);

            // 트레이 아이콘과 켄텍스트 메뉴 연결
            notifyIcon1.ContextMenuStrip = contextMenuStrip1;

            // 이벤트 콘솔 닫고 시작
            eventConsoleCollapsed = true;
            splitContainer1.Panel2Collapsed = eventConsoleCollapsed;

            // 실시간 수집 데이터 띄우기
            //Form oform = new frmHOleDB2WOracle(threadPool);
            //ShowForm(oform);

            Form oform = new frmScheduleManage();
            this.ShowForm(oform);

            //실시간 수리해석
            if (gVar.isEPA())
            {
                Form oform1 = new frmAnalysisScheduleManage();
                ShowForm(oform1);
                this.Cursor = Cursors.Default;
            }

            //수요예측
            if(gVar.isDF())
            {
                DF_Simulation.form.frmMain dfForm = new WaterNETServer.DF_Simulation.form.frmMain();
                ShowForm(dfForm);
            }

            // 보여줄 탭으로 전환
            ShowForm(oform);

            //if(!gVar.isHome())
            //{
            //    //실시간 경고 설정
            //    work = WarningWork.GetInstance();

            //    griWarningList.DataSource = work.SelectWarningList().Tables["WA_WARNING"];

            //    for (int i = 0; i < griWarningList.Rows.Count; i++)
            //    {
            //        if ("N".Equals(griWarningList.Rows[i].GetCellValue("CHECK_YN").ToString()))
            //        {
            //            griWarningList.Rows[i].Appearance.BackColor = Color.LightSalmon;
            //        }
            //        else
            //        {
            //            griWarningList.Rows[i].Appearance.BackColor = Color.Empty;
            //        }
            //    }

            //    timer = new System.Timers.Timer();
            //    timer.Interval = 1000 * 60;
            //    timer.Elapsed += new ElapsedEventHandler(ApplyWarningListHandler);
            //    timer.Start();
            //}

            this.contextMenu1.Opening += new System.ComponentModel.CancelEventHandler(contextMenu1_Opening);

            XmlDocument xmlDocument = null;

            //global_config.xml 을 불러온다.
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH))
            {
                xmlDocument = new XmlDocument();
                xmlDocument.Load(AppStatic.GLOBAL_CONFIG_FILE_PATH);
            }

            if (xmlDocument != null)
            {
                XmlNodeList nodes = null;
                
                nodes = xmlDocument.SelectNodes("/Root/application/MainFormName");
                if (nodes != null && nodes.Count > 0)
                {
                    this.Text = string.Format("{0} ({1})", nodes[0].InnerText, gVar.SGNAME);
                    this.Refresh();
                }

                nodes = xmlDocument.SelectNodes("/Root/DataBase/realtime_table");
                if (nodes != null && nodes.Count > 0 && nodes[0].InnerText != "")
                {
                    AppStatic.REALTIME_TABLE = nodes[0].InnerText;
                }

                nodes = xmlDocument.SelectNodes("/Root/DataBase/accumulation_hour_table");
                if (nodes != null && nodes.Count > 0 && nodes[0].InnerText != "")
                {
                    AppStatic.ACCUMULATION_HOUR_TABLE = nodes[0].InnerText;
                }

                nodes = xmlDocument.SelectNodes("/Root/EPANET/inp_del");
                if (nodes != null && nodes.Count > 0 && nodes[0].InnerText != "")
                {
                    AppStatic.EPANET_INP_DELETE = nodes[0].InnerText;
                }
            }
        }

        private void contextMenu1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.uTabContents.SelectedTab.TabPage.Controls[0] is frmScheduleManage)
            {
                this.ToolStripMenuthisClose.Visible = false;
                this.ToolStripMenuAllClose.Visible = false;
                this.ToolStripMenuNthisClose.Visible = true;
            }
            else
            {
                this.ToolStripMenuthisClose.Visible = true;
                this.ToolStripMenuAllClose.Visible = false;
                this.ToolStripMenuNthisClose.Visible = false;
            }
        }

        //실시간 경고 출력 delegate관련 메소드
        private void ApplyWarningListHandler(Object sender, EventArgs eArgs)
        {
            //ApplyWarningList(SelectWarningList());
        }

        //실시간 경고 출력 delegate관련 메소드
        private DataTable SelectWarningList()
        {
            return work.SelectWarningList().Tables["WA_WARNING"];
        }

        //실시간 경고 출력 delegate관련 메소드
        private void ApplyWarningList(DataTable warningTable)
        {
            if (griWarningList.InvokeRequired)
            {
                ApplyWarningListCallback applyWarningListCallback = new ApplyWarningListCallback(ApplyWarningList);
                this.Invoke(applyWarningListCallback, new object[] { warningTable });
            }
            else
            {
                try
                {
                    griWarningList.DataSource = warningTable;

                    for (int i = 0; i < griWarningList.Rows.Count; i++)
                    {
                        if ("N".Equals(griWarningList.Rows[i].GetCellValue("CHECK_YN").ToString()))
                        {
                            griWarningList.Rows[i].Appearance.BackColor = Color.LightSalmon;
                        }
                        else
                        {
                            griWarningList.Rows[i].Appearance.BackColor = Color.Empty;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Watcher 쓰기 오류..." + ex.StackTrace);

                    timer.Stop();
                    timer.Dispose();
                }
            }
        }

        #endregion

        #region 메뉴 관련 ########################################################

        #region 툴바         #################################################
        /// <summary>
        /// 태그정보관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Form oform = new frmTagInformation();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// 이벤트 콘솔 visible 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ToggleEventConsole();
        }

        /// <summary>
        /// 실시간 수집
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            //Form oform = new frmHOleDB2WOracle(threadPool);
            //ShowForm(oform);

            Form oform = new frmScheduleManage();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// 실시간 관망해석 관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            Form oform = new frmAnalysisScheduleManage();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region 풀다운 메뉴  #################################################

        #region 실시간수집관리
        /// <summary>
        /// 실시간수집관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            //Form oform = new frmHOleDB2WOracle(threadPool);
            //ShowForm(oform);

            Form oform = new frmScheduleManage();
            ShowForm(oform);
        }

        /// <summary>
        /// 태그정보관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            Form oform = new frmTagInformation();
            if (GetForm(oform) == null)
            {

            }
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// 실시간정보조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iwaterRealtimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form oform = new frmRealtime();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region 관망해석관리
        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            Form oform = new frmAnalysisScheduleManage();
            ShowForm(oform);
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region 수요예측관리
        private void dfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DF_Simulation.form.frmMain dfForm = new DF_Simulation.form.frmMain();
            ShowForm(dfForm);
        }
        #endregion

        #region 시스템관리 메뉴
        /// <summary>
        /// 태그 임포트
        /// </summary>
        private void tagImportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTagImport frmtagimport = new frmTagImport();
            ShowForm(frmtagimport);
        }

        /// <summary>
        /// 태그 구분
        /// </summary>
        private void tagDivisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTagDivision frmtagdivision = new frmTagDivision();
            ShowForm(frmtagdivision);
        }

        /// <summary>
        /// DB 설정파일 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dBConfigDeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("데이터베이스 접속 정보를 삭제하시겠습니까", "경고", MessageBoxButtons.YesNo))
            {
                string strConfigPath = Application.StartupPath + @"\Config\";
                string strConfigFile = "DB_Config.xml";

                if (File.Exists(strConfigPath + strConfigFile) == true)
                {
                    File.Delete(strConfigPath + strConfigFile);

                    MessageBox.Show("Water-NET 데이터베이스 환경설정파일을 삭제했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        /// <summary>
        /// 시스템관리메뉴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_SystemManage_Click(object sender, EventArgs e)
        {
            frmTimeStamp frmtimestamp = new frmTimeStamp();
            frmtimestamp.Show();
        }

        /// <summary>
        /// about
        /// </summary>
        private void toolStripMenuItem20_Click(object sender, EventArgs e)
        {
            Form oform = new AboutBox1();
            oform.Show();
        }

        private void SHPNetworkGenerationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.IO.Path.GetFileName(Application.StartupPath + "\\WaterNETNetwork.exe").Length == 0)
            {
                MessageBox.Show("관망 네트워크 분석 프로그램이 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            System.Text.StringBuilder oMessage = new System.Text.StringBuilder();
            oMessage.AppendLine("이 작업은");
            oMessage.AppendLine("상수관-급수관-계량기의 네트워크 구조를 생성합니다.");
            oMessage.AppendLine("상수관망 Shape 데이터가 변경되었을때, 1회만 실행하십시오!");
            oMessage.AppendLine("이 작업은 3~4시간이 소요됩니다.");
            oMessage.AppendLine("그래도, 실행하시겠습니까?");
            if (MessageBox.Show(oMessage.ToString(), "안내", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                return;

            System.Diagnostics.ProcessStartInfo procNetwork = new System.Diagnostics.ProcessStartInfo();
            procNetwork.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
            procNetwork.CreateNoWindow = false;
            procNetwork.UseShellExecute = false;
            procNetwork.WorkingDirectory = Application.StartupPath;
            procNetwork.FileName = Application.StartupPath + "\\WaterNETNetwork.exe";
            try
            {
                System.Diagnostics.Process.Start(procNetwork);
            }
            catch (Exception ex)
            {
                MessageBox.Show("관망 네트워크 분석 프로그램을 실행할수 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.Error(ex.ToString());
            }   
        }
        #endregion

        #region 보기
        /// <summary>
        /// 툴바 보이기 / 감추기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem24_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if (menuItem != null)
            {
                this.toolStrip1.Visible = menuItem.Checked;
            }
        }
        #endregion

        #endregion
        #endregion

        #region 컨트롤 모음 ######################################################

        /// <summary>
        /// 탭 변경될때 메인폼 사이즈를 각 탭에따른 사이즈로 변경 시킨다.
        /// </summary>
        private void SelectedTabChangedAction(object sender, SelectedTabChangedEventArgs e)
        {
            //Common.utils.Configuration conf = new Common.utils.Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);

            //int _default = Convert.ToInt32(conf.getProp("/Root/formsize/default"));
            //int _realtime = Convert.ToInt32(conf.getProp("/Root/formsize/realtime"));
            //int _pipenetwork = Convert.ToInt32(conf.getProp("/Root/formsize/pipenetwork"));
            //int _taginfo = Convert.ToInt32(conf.getProp("/Root/formsize/taginfo"));
            //int _tagImport = Convert.ToInt32(conf.getProp("/Root/formsize/tagimport"));
            //int _tagDvsn = Convert.ToInt32(conf.getProp("/Root/formsize/tagdvsn"));
            //int _df = Convert.ToInt32(conf.getProp("/Root/formsize/df"));

            //int _vertical = Convert.ToInt32(conf.getProp("/Root/formsize/vertical"));

            //switch (e.Tab.Text)
            //{
            //    case "실시간데이터":
            //        this.MaximumSize = new System.Drawing.Size(_realtime, _vertical);
            //        this.MinimumSize = new System.Drawing.Size(_realtime, _vertical);
            //        this.Width = _realtime;
            //        break;
            //    case "실시간 관망해석 관리":
            //        this.MaximumSize = new System.Drawing.Size(_pipenetwork, _vertical);
            //        this.MinimumSize = new System.Drawing.Size(_pipenetwork, _vertical);
            //        this.Width = _pipenetwork;
            //        break;
            //    case "Tag정보":
            //        this.MaximumSize = new System.Drawing.Size(_taginfo, _vertical);
            //        this.MinimumSize = new System.Drawing.Size(_taginfo, _vertical);
            //        this.Width = _taginfo;
            //        break;
            //    case "Tag정보임포트":
            //        this.MaximumSize = new System.Drawing.Size(_tagImport, _vertical);
            //        this.MinimumSize = new System.Drawing.Size(_tagImport, _vertical);
            //        this.Width = _tagImport;
            //        break;
            //    case "Tag구분":
            //        this.MaximumSize = new System.Drawing.Size(_tagDvsn+100, _vertical);
            //        this.MinimumSize = new System.Drawing.Size(_tagDvsn, _vertical);
            //        this.Width = _tagDvsn;
            //        break;
            //    case "수요예측 서버모듈":
            //        this.MaximumSize = new System.Drawing.Size(_df, _vertical);
            //        this.MinimumSize = new System.Drawing.Size(_df, _vertical);
            //        this.Width = _df;
            //        break;
            //    default:
            //        this.MaximumSize = new System.Drawing.Size(_default, _vertical);
            //        this.MinimumSize = new System.Drawing.Size(_default, _vertical);
            //        this.Width = _default;
            //        break;
            //}
        }

        /// <summary>
        /// 서브화면- 기능화면을 메인화면에 표시한다.
        /// </summary>
        /// <param name="oform"></param>
        private void ShowForm(Form oform)
        {
            try
            {
                if (oform is IForminterface)
                {
                    IForminterface obj = (IForminterface)oform;

                    Infragistics.Win.UltraWinTabControl.UltraTab tab = GetForm(oform);
                    if (tab == null)
                    {
                        tab = uTabContents.Tabs.Add(obj.FormKey.ToString() + ":" + obj.GetType().Name.ToString(), obj.FormID);
                        oform.FormBorderStyle = FormBorderStyle.None;
                        oform.TopLevel = false;
                        oform.Dock = DockStyle.Fill;

                        oform.Parent = tab.TabPage;
                        oform.Show();
                    }
                    uTabContents.SelectedTab = tab;
                }
            }
            catch (Exception Exce)
            {
                Logger.Error(Exce.Message + " : " + Exce.Source);
            }

            VisibledMainPanel();

        }

        /// <summary>
        /// 메인화면에 표시되어 있는 서브화면을 찾는다
        /// </summary>
        /// <param name="oform"></param>
        /// <returns></returns>
        private Infragistics.Win.UltraWinTabControl.UltraTab GetForm(Form oform)
        {
            if (oform is IForminterface)
            {
                IForminterface obj = (IForminterface)oform;

                foreach (Infragistics.Win.UltraWinTabControl.UltraTab item in uTabContents.Tabs)
                {
                    if (item.Key == obj.FormKey.ToString() + ":" + obj.GetType().Name.ToString())
                    {
                        return item;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// 메인 탭에 화면삭제 팝업메뉴를 표시한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTabContents_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.uTabContents.SelectedTab.Index == 0)
            {
            }

            if (uTabContents.Tabs.Count == 0) return;
            if (e.Button == MouseButtons.Right) contextMenu1.Show(uTabContents, e.Location);
        }

        /// <summary>
        /// 전체화면을 삭제하는 팝업메뉴 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuAllClose_Click(object sender, EventArgs e)
        {
            tsbtnAllClose_Click(this, new EventArgs());
        }

        /// <summary>
        /// 현재화면을 삭제하는 팝업메뉴 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuthisClose_Click(object sender, EventArgs e)
        {
            tsbtnClose_Click(this, new EventArgs());
        }

        /// <summary>
        /// 현재 보여지는 폼을 삭제하는 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbtnClose_Click(object sender, EventArgs e)
        {
            UltraTab tab = uTabContents.SelectedTab;
            if (tab == null) return;

            foreach (object item in tab.TabPage.Controls)
            {
                if (item is IForminterface)
                {
                    ((Form)item).Close();
                    ((Form)item).Dispose();

                    continue;
                }
            }

            ///Contain된 폼을 삭제하는 루틴추가
            tab.Close();
            tab.Dispose();

            VisibledMainPanel();
        }

        /// <summary>
        /// TabPage 삽입된 폼 전체를 삭제하는 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbtnAllClose_Click(object sender, EventArgs e)
        {
            foreach (Infragistics.Win.UltraWinTabControl.UltraTab tab in uTabContents.Tabs)
            {
                foreach (object item in tab.TabPage.Controls)
                {
                    if (item is IForminterface)
                    {
                        ((Form)item).Close();
                        ((Form)item).Dispose();
                        continue;
                    }
                }
            }

            uTabContents.Tabs.Clear();

            VisibledMainPanel();
        }

        /// <summary>
        /// TabPage 삽입된 폼 현재화면을 제외한 전체를 삭제하는 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuNthisClose_Click(object sender, EventArgs e)
        {
            for (int i = uTabContents.Tabs.Count - 1; i > -1; i--)
            {
                UltraTab tab = uTabContents.Tabs[i];

                if (tab.Equals(uTabContents.SelectedTab)) continue;

                foreach (object item in tab.TabPage.Controls)
                {
                    if (item is IForminterface)
                    {
                        ((Form)item).Close();
                        ((Form)item).Dispose();
                        continue;
                    }
                }
                tab.Close();
                tab.Dispose();
            }
        }

        /// <summary>
        /// 메인패널폼 보이기/감추기 메소드
        /// </summary>
        private void VisibledMainPanel()
        {
            //Form oform = spcContents.Panel1.Controls["frmMainPanel"] as Form;
            //if (uTabContents.Tabs.Count == 0)
            //{
            //    if (oform != null)
            //    {
            //        this.FormBorderStyle = FormBorderStyle.FixedSingle;
            //        this.MaximizeBox = false;
            //        this.MinimizeBox = true;
            //        oform.Visible = true;
            //        oform.BringToFront();
            //    }
            //}
            //else
            //{
            //    if (oform != null)
            //    {
            //        this.FormBorderStyle = FormBorderStyle.Sizable;
            //        this.MaximizeBox = true;
            //        this.MinimizeBox = true;

            //        oform.Visible = false;
            //    }
            //}
        }

        /// <summary>
        /// 프로그램 종료
        /// TODO : 프로그램 종료 루틴 추가 필요
        /// </summary>
        private void ExitApplication(object sender, EventArgs e)
        {
            System.Diagnostics.Process[] procs = System.Diagnostics.Process.GetProcessesByName("WaterNETNetwork");

            System.Diagnostics.Process proc = null;
            foreach (System.Diagnostics.Process item in procs)
            {
                proc = item;
            }

            if (proc != null)
            {
                System.Text.StringBuilder oMessage = new System.Text.StringBuilder();
                oMessage.AppendLine("네트워크 구조 생성 프로그램이 BackGround에서 실행중입니다.");
                oMessage.AppendLine("전체종료(YES), 시스템만종료(NO), 취소(Cancel)을 선택하십시오!");
                DialogResult result = MessageBox.Show(oMessage.ToString(), "안내", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                if (result == DialogResult.Cancel)
                    return;
                else if (result == DialogResult.Yes)
                    proc.Kill();
            }

            notifyIcon1.Visible = false;
            bNotClose = false;

            AnalysisScheduleManager.DestroyAnalysisJob();

            Application.ExitThread();
            Application.Exit();
        }

        /// <summary>
        /// 트레이 아이콘을 클릭시 최대화 하기
        /// </summary>
        private void Tray_DoubleClick(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                return;
            }
            try
            {
                frmPassword oForm = new frmPassword();
                oForm.parentForm = this;

                oForm.SetContextMenuStripName("SHOW");

                oForm.StartPosition = FormStartPosition.Manual;
                oForm.Location = new Point(Screen.PrimaryScreen.WorkingArea.Size.Width - oForm.Width, Screen.PrimaryScreen.WorkingArea.Size.Height - oForm.Height);

                oForm.TopMost = true;
                oForm.ShowDialog();

                //this.Visible = true;
            }
            catch (Exception e1)
            {
                Debug.WriteLine(e1.ToString());
                // 다음 에러는 무시
                // System.InvalidOperationException: 이미 표시된 폼은 모달 대화 상자로 표시할 수 없습니다. showDialog을(를) 호출하기 전에 폼의 visible 속성을 false로 설정하십시오.
            }
        }

        /// <summary>
        /// 트레이 아이콘에서 종료 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitApplicationTray(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                return;
            }

            frmPassword oForm = new frmPassword();
            oForm.parentForm = this;

            oForm.SetContextMenuStripName("EXIT");

            oForm.StartPosition = FormStartPosition.Manual;
            oForm.Location = new Point(Screen.PrimaryScreen.WorkingArea.Size.Width - oForm.Width, Screen.PrimaryScreen.WorkingArea.Size.Height - oForm.Height);

            oForm.TopMost = true;
            oForm.ShowDialog();

            bNotClose = false;

            AnalysisScheduleManager.DestroyAnalysisJob();

            Application.ExitThread();
            Application.Exit();
        }

        private bool bNotClose = true;
        /// <summary>
        /// 폼 플로징 이벤트 처리
        /// 트레이 아이콘으로 보내는 작업 분리
        /// </summary>
        private void FormClosingEvent(object sender, FormClosingEventArgs e)
        {
            if (bNotClose)
            {
                // 클로징 이벤트 취소
                e.Cancel = true;

                // alt+tab 해도 나오지 않게 처리
                this.Visible = false;
            }
            else
            {
                //MessageBox.Show("formclosingevent else");
            }
        }

        /// <summary>
        /// 이벤트 콘솔의 초기 상태 값 : 보여주기
        /// </summary>
        bool eventConsoleCollapsed = true;

        /// <summary>
        /// 이벤트 콘솔 상태값 변경하기
        /// </summary>
        private void ToggleEventConsole()
        {
            if (!eventConsoleCollapsed)
            {
                eventConsoleCollapsed = true;
            }
            else
            {
                eventConsoleCollapsed = false;
            }
            splitContainer1.Panel2Collapsed = eventConsoleCollapsed;
        }


        /// <summary>
        /// ESC 입력을 받을때 창을 숨긴다.
        /// </summary>
        private void GetKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Visible = false;
            }
        }
        #endregion

        #region 데이터베이스 연결 테스트

        /// <summary>
        /// 데이터베이스 연결 테스트
        /// </summary>
        /// <returns></returns>
        private bool ConnectionTest()
        {
            if (File.Exists(AppStatic.DB_CONFIG_FILE_PATH) == true)
            {
                try
                {
                    Configuration m_doc = new Configuration(AppStatic.DB_CONFIG_FILE_PATH);

                    if (m_doc == null)
                    {
                        m_doc = m_doc = new Configuration(AppStatic.DB_CONFIG_FILE_PATH);
                    }

                    string svcName = Security.DecryptKey(m_doc.getProp("/Root/waternet/servicename"));
                    string ip = Security.DecryptKey(m_doc.getProp("/Root/waternet/ip"));
                    string id = Security.DecryptKey(m_doc.getProp("/Root/waternet/id"));
                    string passwd = Security.DecryptKey(m_doc.getProp("/Root/waternet/password"));

                    string strConnString = string.Empty;

                    strConnString = "Data Source=(DESCRIPTION="
                                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=1521)))"
                                    + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
                                    + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";

                    OracleConnection oDBConn = new OracleConnection(strConnString);

                    oDBConn.Open();

                    return true;
                }
                catch (Exception e)
                {
                    Logger.Error(e.ToString());

                    MessageBox.Show("Water-NET 데이터베이스에 접속할 수 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            else
            {
                MessageBox.Show("Water-NET 데이터베이스 환경설정파일을 찾을 수 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }


        /// <summary>
        /// Water-NET.Updater.exe를 최신으로 유지하기 위한 함수
        /// </summary>
        private void UpgradeToWaterNETUpdater()
        {
            string strSRC_PATH = @"C:\Water-NET\Download";
            string strDST_PATH = @"C:\Water-NET";

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            OracleDBManager oDBManager = new OracleDBManager();

            oDBManager.ConnectionString = OracleDBFunctionManager.GetWaterNETConnectionString();

            oDBManager.Open();

            oStringBuilder.AppendLine("SELECT   FILE_RNAME AS FILENAME, SUBSTR(REG_DATE, 0, 8) AS REGDT");
            oStringBuilder.AppendLine("FROM     CM_VERSION");
            oStringBuilder.AppendLine("WHERE    FILE_NAME = 'Water-NET.Updater.exe'");

            pDS = oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_VERSION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strSRC_PATH = strSRC_PATH + @"\" + oDRow["REGDT"].ToString().Trim() + @"\" + oDRow["FILENAME"].ToString().Trim();
                    strDST_PATH = strDST_PATH + @"\Water-NET.Updater.exe";

                    if (File.Exists(strSRC_PATH) == true)
                    {
                        File.Copy(strSRC_PATH, strDST_PATH, true);
                    }
                }
            }

            pDS.Dispose();
            oDBManager.Close();
        }
        #endregion

        //하단 경고 리스트 Row를 더블클릭
        private void griWarningList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            string checkYn = griWarningList.Selected.Rows[0].GetCellValue("CHECK_YN").ToString();
            string warnNo = griWarningList.Selected.Rows[0].GetCellValue("WARN_NO").ToString();

            //미확인인 경우 확인처리
            if ("N".Equals(checkYn))
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("WARN_NO", warnNo);

              //  griWarningList.DataSource = work.UpdateWarningList(conditions);
            }

            //재조회
          //  griWarningList.DataSource = work.SelectWarningList().Tables["WA_WARNING"];

            //for (int i = 0; i < griWarningList.Rows.Count; i++)
            //{
            //    if ("N".Equals(griWarningList.Rows[i].GetCellValue("CHECK_YN").ToString()))
            //    {
            //        griWarningList.Rows[i].Appearance.BackColor = Color.LightSalmon;
            //    }
            //    else
            //    {
            //        griWarningList.Rows[i].Appearance.BackColor = Color.Empty;
            //    }
            //}
        }
    }
}
