﻿using System;
using System.Windows.Forms;
using WaterNETServer.BatchJobs.dao;

namespace WaterNETServer.formPopup
{
    public partial class frmTimeStamp : Form
    {
        #region 선언부
        public frmTimeStamp()
        {
            InitializeComponent();
        }

        BatchJobsWork jobs = new BatchJobsWork();
        WaterNETServer.dao.OracleWork work = new WaterNETServer.dao.OracleWork();
        #endregion

        private void frmTimeStamp_Load(object sender, EventArgs e)
        {
            this.Name = "관리 메뉴";

            DateTimePickerReload();
        }

        /// <summary>
        /// DateTimePicker 리로드
        /// </summary>
        private void DateTimePickerReload()
        {
            // 분단위 DateTimePicker Reload
            dateTimePicker_timestamp_minute_from.Value = work.SelectMaxTimestampMinute();
            dateTimePicker_timestamp_minute_to.Value = dateTimePicker_timestamp_minute_from.Value.AddYears(1);

            // 시간위 DateTimePicker Reload
            dateTimePicker_timestamp_hour_from.Value = work.SelectMaxTimestampHour();
            dateTimePicker_timestamp_hour_to.Value = dateTimePicker_timestamp_hour_from.Value.AddYears(1);
        }

        /// <summary>
        /// Timestamp 작성 (1분단위)
        /// </summary>
        private void btn_timestamp_minute_Click(object sender, EventArgs e)
        {
            jobs.TimestampMinute(dateTimePicker_timestamp_minute_from.Value, dateTimePicker_timestamp_minute_to.Value);

            DateTimePickerReload();
        }

        /// <summary>
        /// Timestamp 작성(1시간단위)
        /// </summary>
        private void btn_timestamp_hour_Click(object sender, EventArgs e)
        {
            jobs.TimestampHour(dateTimePicker_timestamp_hour_from.Value, dateTimePicker_timestamp_hour_to.Value);

            DateTimePickerReload();
        }
    }
}
