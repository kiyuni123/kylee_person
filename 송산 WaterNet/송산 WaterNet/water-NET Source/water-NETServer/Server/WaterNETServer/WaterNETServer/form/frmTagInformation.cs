﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNETServer.Common.utils;
using Infragistics.Win.UltraWinGrid;
using WaterNETServer.dao;
using WaterNETServer.form;
using WaterNETServer.formPopup;
using EMFrame.log;

namespace WaterNETServer
{
    public partial class frmTagInformation : Form, IForminterface
    {
        #region 선언부
        GlobalVariable gVar = new GlobalVariable();
        private string _formName = string.Empty;

        Common.utils.Common cm = new Common.utils.Common();
        OracleWork ow = new OracleWork();
        FormUtils fu = new FormUtils();

        int exeCnt = 0;

        public frmTagInformation()
        {
            _formName = gVar.getFormName(4);

            InitializeComponent();
            // 검색조건부분 사이즈 변경 이벤트 등록
            flowLayoutPanel1.SizeChanged += new EventHandler(flowLayoutPanel1_SizeChanged);
        }

        #endregion

        #region IForminterface 멤버
        string IForminterface.FormID
        {
            get { return _formName; }
        }
        string IForminterface.FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
#endregion

        #region 초기화 (콤보 박스 완성)
        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void selectCodeCondition()
        {
            SelectCodeList1();
            SelectCodeList2();
            SelectCodeList3();
            SelectCodeList4();
            SelectCodeList5();
            SelectCodeList6();
            SelectCodeList7();
            SelectCodeList8();
            SelectCodeList9();
            SelectCodeList10();
            textBox3.Text = string.Empty;
        }
        /// <summary>
        /// 1.지역본부 검색조건
        /// </summary>
        private void SelectCodeList1()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7001");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox1, tableName);
        }

        /// <summary>
        /// 2.사무소기호
        /// </summary>
        private void SelectCodeList2()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7002");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox2, tableName);
        }

        /// <summary>
        /// 3.사업장기호
        /// </summary>
        private void SelectCodeList3()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7003");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox3, tableName);
        }

        /// <summary>
        /// 4.변량및기기기호
        /// </summary>
        private void SelectCodeList4()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7004");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox4, tableName);
        }

        /// <summary>
        /// 5.기능기호
        /// </summary>
        private void SelectCodeList5()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7005");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox5, tableName);
        }

        /// <summary>
        /// 6.태그구분
        /// </summary>
        private void SelectCodeList6()
        {
            string tableName = "TagGbn";

            DataSet dSet = ow.SelectTagGbn(tableName, null);

            cm.SetComboBox(dSet, comboBox6, tableName);
        }

        /// <summary>
        /// 7.입출력구분(배수지 입력,출력 등)
        /// </summary>
        private void SelectCodeList7()
        {
            string tableName = "IOGbn";

            DataSet dSet = ow.SelectIOGbn(tableName, null);

            cm.SetComboBox(dSet, comboBox7, tableName);
        }

        /// <summary>
        /// 8.지역정보
        /// </summary>
        private void SelectCodeList8()
        {
            string tableName = "cm_location";

            DataSet dSet = ow.SelectCmLocation(tableName, null);

            cm.SetComboBox(dSet, comboBox8, tableName);
        }

        /// <summary>
        /// 9.사용여부
        /// </summary>
        private void SelectCodeList9()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7009");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox9, tableName);

            comboBox9.SelectedValue = "Y";
        }

        /// <summary>
        /// 10.지역정보2
        /// </summary>
        private void SelectCodeList10()
        {
            string tableName = "cm_location";

            DataSet dSet = ow.SelectCmLocation(tableName, null);

            cm.SetComboBox(dSet, comboBox10, tableName);
        }
        #endregion 검색 조건 함수 완료

        #region 초기화
        /// <summary>
        /// 폼 사이즈 변경시 검색조건부분 리사이징
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void flowLayoutPanel1_SizeChanged(object sender, EventArgs e)
        {
            this.panel1.Height = this.flowLayoutPanel1.Height + 36;
            this.groupBox1.Height = this.panel1.Height + 20;
            this.splitContainer1.SplitterDistance = this.groupBox1.Height;
        }

        /// <summary>
        /// 폼 로드시 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmTagInformation_Load(object sender, EventArgs e)
        {
            // 폼 로드시 검색 부분 사이즈 초기화
            flowLayoutPanel1_SizeChanged(sender, e);

            // 콤보박스 초기화
            selectCodeCondition();

            // 일단 검색 보여줘
            SelectTagInformation();
        }
        #endregion

        #region 이벤트

        /// <summary>
        /// 툴팁 메뉴 띄우기
        /// 마우스 우클릭 설정
        /// 툴팁스트립 마진 없애기 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_MouseClick(object sender, MouseEventArgs e)
        {
            if (ultraGrid1.ActiveRow == null)
                return;

            try
            {
                if (!ultraGrid1.ActiveRow.Cells[0].Selected)
                {
                    // 툴팁스트립을 마우스 위치에 띄운다
                    if (e.Button == MouseButtons.Right)
                    {
                        Point mousePoint = new Point(e.X, e.Y);
                        this.contextMenuStrip1.Show(ultraGrid1, mousePoint);
                    }
                }

                // 툴팁스트립의 마진을 없앤다
                ToolStripMenuItem noMargins = new ToolStripMenuItem("NoMargins");

                // This ToolStripMenuItem has no image and no check margin.
                noMargins.DropDown = contextMenuStrip1;
                ((ContextMenuStrip)noMargins.DropDown).ShowImageMargin = false;
                ((ContextMenuStrip)noMargins.DropDown).ShowCheckMargin = false;
            }
            catch (NullReferenceException nex)
            {
                Logger.Error(nex.ToString());
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }

        /// <summary>
        /// 툴팁 메뉴 클릭시 액션
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Form oform = new frmConditionPopup(ultraGrid1);
                // 폼의 위치를 마우스 위치에 띄운다
                oform.StartPosition = FormStartPosition.Manual;
                oform.Location = new System.Drawing.Point(Control.MousePosition.X, Control.MousePosition.Y);
                oform.Show();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        /// <summary>
        /// 태그리스트 클릭시 태그명, 태그설명 TextBox에 클릭 내용 표시
        /// </summary>
        private void TagList_Click(object sender, EventArgs e)
        {
            if (ultraGrid1.ActiveRow == null)
                return;

            try
            {
                if (ultraGrid1.ActiveRow.Cells[0].Text != null) textBox3.Text = ultraGrid1.ActiveRow.Cells[0].Text;
            }
            catch (NullReferenceException nex)
            {
                Logger.Error(nex.ToString());
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        /// <summary>
        /// 검색버튼 클릭
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            SelectTagInformation();
        }

        /// <summary>
        /// 적용 (수정된 내용 DB에 Update)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            UpdateTag();

            frmShowMessageBox msgBox = new frmShowMessageBox("수정되었습니다.");
            msgBox.StartPosition = FormStartPosition.CenterScreen;
            msgBox.Show();

        }
        #endregion

        #region 기능
        /// <summary>
        /// 태그정보 검색
        /// </summary>
        private void SelectTagInformation()
        {
            // 검색 조건 세팅
            Hashtable sCondition = new Hashtable();
            sCondition.Add("textbox1", textBox1.Text);
            sCondition.Add("textbox2", textBox2.Text);
            sCondition.Add("combobox1", comboBox1.SelectedValue.ToString());
            sCondition.Add("combobox2", comboBox2.SelectedValue.ToString());
            sCondition.Add("combobox3", comboBox3.SelectedValue.ToString());
            sCondition.Add("combobox4", comboBox4.SelectedValue.ToString());
            sCondition.Add("combobox5", comboBox5.SelectedValue.ToString());
            sCondition.Add("combobox6", comboBox6.SelectedValue.ToString());
            sCondition.Add("combobox7", comboBox7.SelectedValue.ToString());
            sCondition.Add("combobox8", comboBox8.SelectedValue.ToString());
            sCondition.Add("combobox9", comboBox9.SelectedValue.ToString());

            // 검색
            string tableName = "TagInformation";
            DataSet dSet = ow.SelectTagInformation(tableName, sCondition);
            DataTableCollection dtc = dSet.Tables;
            foreach (DataTable dt in dtc)
            {
                DataColumnCollection dcc = dSet.Tables[dt.TableName].Columns;
                // 첫 검색일 때 그리드 컬럼명을 세팅
                if (exeCnt == 0) fu.InitializeSettingTagColumns(ultraGrid1, dcc);
                // 검색 결과 그리드에 표시
                ultraGrid1.DataSource = dSet.Tables[dt.TableName];
            }

            // 그리드 스타일 설정
            fu.SetGridStyle(ultraGrid1);
            fu.SetGridStyle4(ultraGrid1);

            // 컬럼 사이즈 변경
            fu.SetGridStyle_PerformAutoResize(ultraGrid1);

            // 검색 카운트 증가 (첫 검색 판단에만 사용)
            exeCnt++;


            ultraGrid1.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
            // 수정 컬럼 설정
            //cm.ColumeAllowEdit(ultraGrid1, 1, 12);
            //cm.ColumeAllowEdit(ultraGrid1, 13, 16);
        }

        /// <summary>
        /// 태그정보 수정
        /// : 그리드내 수정된 모든 라인의 값을 DataTable로 넘긴다
        /// </summary>
        private void UpdateTag()
        {
            DataTable modifyGrid = ((DataTable)ultraGrid1.DataSource).GetChanges(DataRowState.Modified);
            ow.UpdateTags(modifyGrid);
            // Grid 의 상태를 변경시켜줌
            ((DataTable)ultraGrid1.DataSource).AcceptChanges();
        }
        #endregion
    }
}
