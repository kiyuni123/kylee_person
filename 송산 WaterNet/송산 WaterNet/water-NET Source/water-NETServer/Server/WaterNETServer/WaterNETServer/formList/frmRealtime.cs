﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNETServer.Common.utils;
using Infragistics.Win.UltraWinGrid;
using WaterNETServer.dao;
using WaterNETServer.form;
using WaterNETServer.IF_iwater.dao;

namespace WaterNETServer.formList
{
    public partial class frmRealtime : Form, IForminterface
    {
        GlobalVariable gVar = new GlobalVariable();
        private string _formName = "";

        /// <summary>
        /// 생성자
        /// </summary>
        public frmRealtime()
        {
            _formName = gVar.getFormName(5);

            InitializeComponent();
            // 검색조건부분 사이즈 변경 이벤트 등록
            flowLayoutPanel1.SizeChanged += new EventHandler(flowLayoutPanel1_SizeChanged);
        }

#region IForminterface 멤버
        string IForminterface.FormID
        {
            get { return _formName; }
        }
        string IForminterface.FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
#endregion

        /// <summary>
        /// 공통 클래스
        /// </summary>
        Common.utils.Common cm = new Common.utils.Common();
        /// <summary>
        /// DB 클래스
        /// </summary>
        ListWork lw = new ListWork();
        WaterNETServer.dao.OracleWork ow = new WaterNETServer.dao.OracleWork();
        /// <summary>
        /// FormUtils 클래스
        /// </summary>
        FormUtils fu = new FormUtils();

        int exeCnt = 0;

        /// <summary>
        /// 폼 사이즈 변경시 검색조건부분 리사이징
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void flowLayoutPanel1_SizeChanged(object sender, EventArgs e)
        {
            this.panel1.Height = this.flowLayoutPanel1.Height + 34;
            this.groupBox1.Height = this.panel1.Height + 20;
            this.splitContainer1.SplitterDistance = this.groupBox1.Height;
        }

        /// <summary>
        /// 폼 로드시 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmTagInformation_Load(object sender, EventArgs e)
        {
            // 폼 로드시 검색 부분 사이즈 초기화
            flowLayoutPanel1_SizeChanged(sender, e);

            // 콤보박스 초기화
            selectCodeCondition();

            // 일단 검색 보여줘
            Search();
        }

        /// <summary>
        /// 검색
        /// </summary>
        private void Search()
        {
            // 검색 조건 세팅
            Hashtable sCondition = new Hashtable();
            sCondition.Add("textbox1", textBox1.Text);
            sCondition.Add("textbox2", textBox2.Text);
            sCondition.Add("combobox4", comboBox4.SelectedValue.ToString());
            sCondition.Add("combobox5", comboBox5.SelectedValue.ToString());
            sCondition.Add("dateTimePicker1", dateTimePicker1.Value.ToString("yyyyMMdd"));
            sCondition.Add("dateTimePicker2", dateTimePicker2.Value.ToString("yyyyMMdd"));
            sCondition.Add("cnt", textBox3.Text);

            // 검색
            string tableName = "Realtime";
            DataSet dSet = lw.selectRealtime(tableName, sCondition);
            DataTableCollection dtc = dSet.Tables;
            foreach (DataTable dt in dtc)
            {
                DataColumnCollection dcc = dSet.Tables[dt.TableName].Columns;
                // 첫 검색일 때 그리드 컬럼명을 세팅
                if (exeCnt == 0) fu.InitializeSettingTagColumns(ultraGrid1, dcc);
                // 검색 결과 그리드에 표시
                ultraGrid1.DataSource = dSet.Tables[dt.TableName];
            }

            // 그리드 스타일 설정
            fu.SetGridStyle(ultraGrid1);
            fu.SetGridStyle4(ultraGrid1);

            // 컬럼 사이즈 변경
            fu.SetGridStyle_PerformAutoResize(ultraGrid1);

            // 검색 카운트 증가 (첫 검색 판단에만 사용)
            exeCnt++;


            ultraGrid1.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
            // 수정 컬럼 설정
            //cm.ColumeAllowEdit(ultraGrid1, 1, 12);
            //cm.ColumeAllowEdit(ultraGrid1, 13, 16);
        }


#region 검색 조건 함수 (콤보 박스 완성)
        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void selectCodeCondition()
        {
            SelectCodeList4();
            SelectCodeList5();

            dateTimePicker2.Value = DateTime.Today.AddDays(1);

        }

        /// <summary>
        /// 4.변량및기기기호
        /// </summary>
        private void SelectCodeList4()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7004");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox4, tableName);
        }

        /// <summary>
        /// 5.기능기호
        /// </summary>
        private void SelectCodeList5()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7005");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox5, tableName);
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

#endregion 검색 조건 함수 완료

#region 이벤트 모음 시작

        /// <summary>
        /// 검색버튼 클릭
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            Search();
        }

#endregion 이벤트 모음 종료

    }
}
