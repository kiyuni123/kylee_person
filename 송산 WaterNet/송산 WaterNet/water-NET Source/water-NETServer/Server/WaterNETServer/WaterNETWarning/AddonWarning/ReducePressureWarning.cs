﻿using System;
using System.Data;
using System.Collections;
using System.Text;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;
using WaterNETServer.Warning.work;

namespace WaterNETServer.Warning.AddonWarning
{
    public class ReducePressureWarning : BaseWork
    {
        /// <summary>
        /// 현재시간 - 15분 ~ 현재시간 까지의 감시대상인 감압밸브의 실시간 계측값(분데이터)를 취득한다.
        /// 실시간 계측값이 인터페이스를 통해 들어오는 시간이 현재 시간과 일치하지 않기 때문에
        /// 15분 동안의 데이터를 가져와 경보처리 대상인지 검증을 한다.
        /// 단, 이미 경보처리된 실측값인지 경보 히스토리를 체크해서 경보가 발생한 값이 아닌 경우만 체크
        /// </summary>
        public void Analysis_RTData()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                //비즈니스 로직 선언부
                WarningWork work = WarningWork.GetInstance();
                //argument 정의

                StringBuilder oStringBuilder = new StringBuilder();
                string strAnalysisResult = string.Empty;    //경보확인결과
                string strVALVE_NO = string.Empty;          //감압밸브일련번호
                string strVALVE_TYPE_CD = string.Empty;     //감압밸브타입
                string strDATE_TIME = string.Empty;         //실시간 측정값의 일시
                string strRT_TIME = string.Empty;           //실시간 측정값의 HH24MI
                string strBLOCK_CODE = string.Empty;		//감압밸브가 위치한 블록
                decimal dcCUR_VAL = 0;                      //실시간 측정값
                decimal dcALERT_LIMIT = 0;                  //경보 기준 (+, -)

                DataSet pDS = new DataSet();

                oStringBuilder.AppendLine("SELECT   PNT.VALVE_NO,");
                oStringBuilder.AppendLine("         PNT.VALVE_TYPE,");
                oStringBuilder.AppendLine("         TO_CHAR(RT.TIMESTAMP, 'RRRRMMDDHH24MI') AS CUR_DT,");
                oStringBuilder.AppendLine("         ROUND(RT.VALUE, 3) AS CUR_VALUE,");
                oStringBuilder.AppendLine("         PNT.ALERT_LIMIT,");
                oStringBuilder.AppendLine("         PNT.BLOCK_CODE");
                oStringBuilder.AppendLine("FROM     IF_GATHER_REALTIME RT,");
                oStringBuilder.AppendLine("         WP_RT_MONITOR_POINT PNT");
                oStringBuilder.AppendLine("WHERE    RT.TAGNAME = PNT.TAGNAME");
                oStringBuilder.AppendLine("         AND RT.TIMESTAMP BETWEEN SYSDATE - (15/(24*60)) AND SYSDATE");
                oStringBuilder.AppendLine("ORDER BY CUR_DT DESC, VALVE_NO");

                pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RT_GAMSI");

                if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
                {
                    foreach (DataRow oDRow in pDS.Tables[0].Rows)
                    {
                        strVALVE_NO = oDRow["VALVE_NO"].ToString().Trim();
                        strVALVE_TYPE_CD = oDRow["VALVE_TYPE"].ToString().Trim();
                        strDATE_TIME = oDRow["CUR_DT"].ToString().Trim();
                        strRT_TIME = strDATE_TIME.Substring(8);
                        strBLOCK_CODE = oDRow["BLOCK_CODE"].ToString().Trim();

                        dcCUR_VAL = 0;
                        dcCUR_VAL = Convert.ToDecimal(oDRow["CUR_VALUE"].ToString().Trim());
                        dcALERT_LIMIT = 0;
                        dcALERT_LIMIT = Convert.ToDecimal(oDRow["ALERT_LIMIT"].ToString().Trim());

                        if (this.IsAlertedData(manager, strVALVE_NO, strDATE_TIME) != true)
                        {
                            if (dcCUR_VAL > 0)
                            {
                                switch (strVALVE_TYPE_CD)
                                {
                                    case "1":   //고정유출제어감압밸브
                                        strAnalysisResult = this.Analysis_PRValveType1(manager, strVALVE_NO, dcCUR_VAL, dcALERT_LIMIT);
                                        break;
                                    case "2":   //시간제어감압밸브
                                        strAnalysisResult = this.Analysis_PRValveType2(manager, strVALVE_NO, strRT_TIME, dcCUR_VAL, dcALERT_LIMIT);
                                        break;
                                    case "3":   //유량비례제어감압밸브
                                        strAnalysisResult = this.Analysis_PRValveType3(manager, strVALVE_NO, strDATE_TIME, dcCUR_VAL, dcALERT_LIMIT);
                                        break;
                                }

                                //실시간 측정값을 검증한 결과가 1 or 2이면 경보가 발생한 것이므로 History에 Insert
                                if (strAnalysisResult.Substring(0, 1) != "0")
                                {
                                    this.Insert_AlertHistory(manager, strVALVE_NO, dcCUR_VAL.ToString(), Convert.ToDecimal(strAnalysisResult.Substring(2)), strDATE_TIME);

                                    Hashtable warningCondition = new Hashtable();

                                    warningCondition.Add("WORK_GBN", "0004");				//업무구분 : 감압밸브
                                    warningCondition.Add("LOC_CODE", strBLOCK_CODE);	    //지역코드 (CM_LOCATION의 블록코드 -LOC_CODE-)
                                    warningCondition.Add("WAR_CODE", "000001");				//경고구분 : 압력이상
                                    warningCondition.Add("SERIAL_NO", strVALVE_NO);		    //밸브일련번호
                                    warningCondition.Add("REMARK", "");		                //비고

                                    //비즈니스로직 호출
                                    work.InsertWarningData(warningCondition);
                                }
                            }
                        }
                    }
                }

                pDS.Dispose();
                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 해당 데이터가 이미 경보 처리된 데이터 인지 확인.
        /// </summary>
        /// <param name="strVALVE_NO">감압밸브일련번호</param>
        /// <param name="strDATE_TIME">실시간측정일시</param>
        /// <returns>true : 이미 경보 처리 됨, false : 신규 경보</returns>
        private bool IsAlertedData(EMapper manager, string strVALVE_NO, string strDATE_TIME)
        {
            bool blResult = true;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   VALVE_NO");
            oStringBuilder.AppendLine("FROM     WP_RT_ALERT_HISTORY");
            oStringBuilder.AppendLine("WHERE    VALVE_NO = '" + strVALVE_NO + "' AND ALERT_DATE = '" + strDATE_TIME + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IS_ALERT");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                blResult = true;
            }
            else
            {
                blResult = false;
            }

            pDS.Dispose();

            return blResult;
        }

        /// <summary>
        /// 고정유출제어감압밸브 Value 분석
        /// </summary>
        /// <param name="strVALVE_NO">감압밸브일련번호</param>
        /// <param name="dcCUR_VALUE">실시간측정값</param>
        /// <param name="dcALERT_LIMIT">경보범위</param>
        /// <returns> 0 : 정상 / 경보 - 1 : 기준 초과, 2 : 기준 미만 + | + PR_VALUE</returns>
        private string Analysis_PRValveType1(EMapper manager, string strVALVE_NO, decimal dcCUR_VALUE, decimal dcALERT_LIMIT)
        {
            int iResult = 0;
            string strResult = string.Empty;

            decimal dcPR_VALUE = 0;
            decimal dcPR_T_LIMIT = 0;
            decimal dcPR_L_LIMIT = 0;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   PR_VALUE ");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_OPTION");
            oStringBuilder.AppendLine("WHERE    VALVE_NO = '" + strVALVE_NO + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PR_VALUE");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    dcPR_VALUE = Convert.ToDecimal(oDRow["PR_VALUE"].ToString().Trim());
                    dcPR_T_LIMIT = dcPR_VALUE + dcALERT_LIMIT;
                    dcPR_L_LIMIT = dcPR_VALUE - dcALERT_LIMIT;

                    if (dcCUR_VALUE > dcPR_T_LIMIT)
                    {
                        iResult = 1;
                    }
                    else if (dcCUR_VALUE < dcPR_L_LIMIT)
                    {
                        iResult = 2;
                    }
                    break;
                }
            }

            pDS.Dispose();

            if (iResult > 0)
            {
                strResult = iResult.ToString() + "|" + dcPR_VALUE.ToString();
            }
            else
            {
                strResult = iResult.ToString();
            }

            return strResult; ;
        }

        /// <summary>
        /// 시간제어감압밸브 Value 분석
        /// </summary>
        /// <param name="strVALVE_NO">감압밸브일련번호</param>
        /// <param name="strRT_TIME">실시간측정일시 중 HH24MI</param>
        /// <param name="dcCUR_VALUE">실시간측정값</param>
        /// <param name="dcALERT_LIMIT">경보범위</param>
        /// <returns> 0 : 정상 / 경보 - 1 : 기준 초과, 2 : 기준 미만 + | + PR_VALUE</returns>
        private string Analysis_PRValveType2(EMapper manager, string strVALVE_NO, string strRT_TIME, decimal dcCUR_VALUE, decimal dcALERT_LIMIT)
        {
            int iResult = 0;
            string strResult = string.Empty;

            decimal dcPR_VALUE = 0;
            decimal dcPR_T_LIMIT = 0;
            decimal dcPR_L_LIMIT = 0;
            string strSTART_OPTION = string.Empty;
            string strEND_OPTION = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   START_OPTION, END_OPTION, PR_VALUE");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_OPTION");
            oStringBuilder.AppendLine("WHERE    VALVE_NO = '" + strVALVE_NO + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PR_VALUE");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strSTART_OPTION = oDRow["START_OPTION"].ToString().Trim();
                    strEND_OPTION = oDRow["END_OPTION"].ToString().Trim();

                    if (Convert.ToInt32(strRT_TIME) >= Convert.ToInt32(strSTART_OPTION) && Convert.ToInt32(strRT_TIME) <= Convert.ToInt32(strEND_OPTION))
                    {
                        dcPR_VALUE = Convert.ToDecimal(oDRow["PR_VALUE"].ToString().Trim());
                        dcPR_T_LIMIT = dcPR_VALUE + dcALERT_LIMIT;
                        dcPR_L_LIMIT = dcPR_VALUE - dcALERT_LIMIT;

                        if (dcCUR_VALUE > dcPR_T_LIMIT)
                        {
                            iResult = 1;
                        }
                        else if (dcCUR_VALUE < dcPR_L_LIMIT)
                        {
                            iResult = 2;
                        }
                        break;
                    }
                }
            }

            pDS.Dispose();

            if (iResult > 0)
            {
                strResult = iResult.ToString() + "|" + dcPR_VALUE.ToString();
            }
            else
            {
                strResult = iResult.ToString();
            }

            return strResult; ;
        }

        /// <summary>
        /// 유량비례제어감압밸브 Value 분석
        /// </summary>
        /// <param name="strVALVE_NO">감압밸브일련번호</param>
        /// <param name="strDATE_TIME">실시간측정일시</param>
        /// <param name="dcCUR_VALUE">실시간측정값</param>
        /// <param name="dcALERT_LIMIT">경보범위</param>
        /// <returns> 0 : 정상 / 경보 - 1 : 기준 초과, 2 : 기준 미만 + | + PR_VALUE</returns>
        private string Analysis_PRValveType3(EMapper manager, string strVALVE_NO, string strDATE_TIME, decimal dcCUR_VALUE, decimal dcALERT_LIMIT)
        {
            int iResult = 0;
            string strResult = string.Empty;

            decimal dcPR_VALUE = 0;
            decimal dcPR_T_LIMIT = 0;
            decimal dcPR_L_LIMIT = 0;
            string strSTART_OPTION = string.Empty;
            string strEND_OPTION = string.Empty;
            string strCUR_VALUE_F = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            strCUR_VALUE_F = this.GetFlowmeteringInstrumentValue(manager, strVALVE_NO, strDATE_TIME);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   START_OPTION, END_OPTION, PR_VALUE");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_OPTION");
            oStringBuilder.AppendLine("WHERE    VALVE_NO = '" + strVALVE_NO + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "PR_VALUE");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strSTART_OPTION = oDRow["START_OPTION"].ToString().Trim();
                    strEND_OPTION = oDRow["END_OPTION"].ToString().Trim();

                    if (Convert.ToDecimal(strCUR_VALUE_F) >= Convert.ToDecimal(strSTART_OPTION) && Convert.ToDecimal(strCUR_VALUE_F) <= Convert.ToDecimal(strEND_OPTION))
                    {
                        dcPR_VALUE = Convert.ToDecimal(oDRow["PR_VALUE"].ToString().Trim());
                        dcPR_T_LIMIT = dcPR_VALUE + dcALERT_LIMIT;
                        dcPR_L_LIMIT = dcPR_VALUE - dcALERT_LIMIT;

                        if (dcCUR_VALUE > dcPR_T_LIMIT)
                        {
                            iResult = 1;
                        }
                        else if (dcCUR_VALUE < dcPR_L_LIMIT)
                        {
                            iResult = 2;
                        }
                        break;
                    }
                }
            }

            pDS.Dispose();

            if (iResult > 0)
            {
                strResult = iResult.ToString() + "|" + dcPR_VALUE.ToString();
            }
            else
            {
                strResult = iResult.ToString();
            }

            return strResult; ;
        }

        /// <summary>
        /// 감압밸브일련번호로 유량 TAG를 찾은 다음 그 TAG로 현재 유량값을 가져온다.
        /// </summary>
        /// <param name="strVALVE_NO">감압밸브일련번호</param>
        /// <param name="strDATE_TIME">실시간측정일시</param>
        /// <returns></returns>
        private string GetFlowmeteringInstrumentValue(EMapper manager, string strVALVE_NO, string strDATE_TIME)
        {
            string strResult = string.Empty;
            string strTAGNAME = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            DataSet pDS2 = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   TAGNAME_F");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("WHERE    VALVE_NO = '" + strVALVE_NO + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "TAGNAME");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strTAGNAME = oDRow["TAGNAME_F"].ToString().Trim();

                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("SELECT   ROUND(VALUE, 3) AS CUR_VALUE");
                    oStringBuilder.AppendLine("FROM     IF_GATHER_REALTIME");
                    oStringBuilder.AppendLine("WHERE    TAGNAME = '" + strTAGNAME + "' AND TIMESTAMP = TO_DATE('" + strDATE_TIME + "00', 'RRRR-MM-DD HH24:MI:SS')");

                    pDS2 = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CUR_VAL");

                    if ((pDS2.Tables.Count > 0) && (pDS2.Tables[0].Rows.Count > 0))
                    {
                        foreach (DataRow oDRow2 in pDS2.Tables[0].Rows)
                        {
                            strResult = oDRow2["CUR_VALUE"].ToString().Trim();
                        }
                    }
                }
            }

            pDS.Dispose();

            return strResult;
        }

        /// <summary>
        /// 경보 히스토리를 INSERT함
        /// </summary>
        /// <param name="strVALVE_NO">감압밸브일련번호</param>
        /// <param name="strCUR_VALUE">실시간측정값</param>
        /// <param name="dcPR_VALUE">감압설정치</param>
        /// <param name="strDATE_TIME">실시간측정일시</param>
        private void Insert_AlertHistory(EMapper manager, string strVALVE_NO, string strCUR_VALUE, decimal dcPR_VALUE, string strDATE_TIME)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   FROM    WP_RT_ALERT_HISTORY");
            oStringBuilder.AppendLine("WHERE    VALVE_NO = '" + strVALVE_NO + "' AND  ALERT_DATE = '" + strDATE_TIME + "'");

            manager.ExecuteScript(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    WP_RT_ALERT_HISTORY");
            oStringBuilder.AppendLine("(VALVE_NO, CUR_VALUE, PR_VALUE, START_OPTION, END_OPTION, ALERT_DATE)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("('" + strVALVE_NO + "', '" + strCUR_VALUE + "', " + dcPR_VALUE + ", '', '', '" + strDATE_TIME + "')");

            manager.ExecuteScript(oStringBuilder.ToString(), null);
        }

    }
}
