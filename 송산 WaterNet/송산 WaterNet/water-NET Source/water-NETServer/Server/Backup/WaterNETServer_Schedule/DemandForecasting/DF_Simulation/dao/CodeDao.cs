﻿using System.Text;
using System.Data;
using EMFrame.dm;
using System.Collections;

namespace WaterNETServer.DF_Simulation.dao
{
    public class CodeDao
    {
        private static CodeDao dao = null;
        private CodeDao() { }
        public static CodeDao GetInstance()
        {
            if (dao == null)
            {
                dao = new CodeDao();
            }
            return dao;
        }

        public DataTable selectCodeList(EMapper manager, Hashtable parameter)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.AppendLine("select code                                                                     ");
            oStringBuilder.AppendLine("      ,code_name                                                                ");
            oStringBuilder.AppendLine("  from cm_code                                                                  ");
            oStringBuilder.AppendLine(" where 1 = 1                                                                    ");
            oStringBuilder.Append("   and pcode = :PCODE                                                               ");
            oStringBuilder.AppendLine("   and use_yn = 'Y'                                                             ");
            oStringBuilder.AppendLine(" order by to_number(orderby)                                                    ");

            return manager.ExecuteScriptDataTable(oStringBuilder.ToString(), parameter);
        }
    }
}
