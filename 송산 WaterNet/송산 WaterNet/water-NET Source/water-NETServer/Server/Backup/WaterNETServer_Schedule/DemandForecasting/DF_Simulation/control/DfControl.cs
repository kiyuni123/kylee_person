﻿using System;
using System.Collections.Generic;
using WaterNETServer.DF_Simulation.data;
using WaterNETServer.DF_Simulation.form;
using System.Windows.Forms;
using WaterNETServer.DF_Algorithm.Simulation;

namespace WaterNETServer.DF_Simulation.control
{
    public class DfControl
    {
        /// <summary>
        /// 기본생성자
        /// </summary>
        public DfControl() { }

        private IDictionary<string, IDfData> items = new Dictionary<string, IDfData>();
        private DateTime targetDate;
        private int targetTIme;
        private RichTextBox texDfLog = null;

        /// <summary>
        /// 수요예측 실행 날자를 설정한다.
        /// 날자를 설정하면 자동으로 유량계별 일 예측량을 산정한다.
        /// </summary>
        public DateTime TargetDate
        {
            set
            {
                this.targetDate = value;
            }
        }

        /// <summary>
        /// 수요예측 실행 시간을 설정한다.
        /// 시간을 설정하면 자동으로 유량계별 시간 예측량을 보정한다.
        /// </summary>
        public int TargetTime
        {
            set
            {
                this.targetTIme = value;
                this.Time_Predict();
            }
        }

        //로깅창
        public RichTextBox TexDfLog
        {
            set
            {
                this.texDfLog = value;
            }
        }

        /// <summary>
        /// 모든 유량계를 수요예측 실행 객체로 설정한다.
        /// </summary>
        /// <returns></returns>
        public DfControl AddItem()
        {
            //유량계를 찾는 로직.....
            return this;
        }

        /// <summary>
        /// 특정 유량계를 수요예측 실행 객체로 설정한다.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public DfControl AddItem(IDfData item)
        {
            items.Add(item.Key, item);
            return this;
        }

        /// <summary>
        /// 수요예측 실행 객체로 설정된 유량계를 삭제한다.
        /// </summary>
        /// <returns></returns>
        public DfControl ClearItem()
        {
            items.Clear();
            return this;
        }

        public void Predict_All()
        {
            foreach (KeyValuePair<string, IDfData> item in items)
            {
                item.Value.TargetDate = this.targetDate;

                if (item.Value.StudyMethod == 0 && !item.Value.IsDataError)
                {
                    Simulation_NN nn = new Simulation_NN();

                    nn.Set_NN(
                        item.Value.Training_Data,
                        2.0,
                        item.Value.LearningRate,
                        item.Value.Momentum,
                        item.Value.Neuron,
                        1,
                        item.Value.Iterations,
                        item.Value.InputNum,
                        item.Value.MaxValue,
                        item.Value.MinValue
                    );

                    nn.Network_Learning();
                    nn.Set_Prediction_Data(item.Value.Input_Data);
                    nn.Flow_Prediction();

                    item.Value.Prediction_Result = nn.Prediction_Result;
                    item.Value.Set_Hour_prediction();
                }
                else if (item.Value.StudyMethod == 1 && !item.Value.IsDataError)
                {
                    Simulation_MRM mrm = new Simulation_MRM();
                    mrm.Set_Data(item.Value.Training_Data, item.Value.Setting_MRM);
                    mrm.Set_Num_Input(item.Value.InputNum);
                    mrm.Set_Param_MinMax_Input(item.Value.MinValue, item.Value.MaxValue);
                    mrm.Set_Param_MinMax_Output(item.Value.MinValue[item.Value.InputNum], item.Value.MaxValue[item.Value.InputNum]);
                    mrm.Set_Expand_Data(item.Value.Training_Data);
                    mrm.Data_Normalize(mrm.data_dimension);
                    mrm.Initialize();
                    mrm.Weight_Calculate();
                    mrm.Set_Prediction_Data(item.Value.Input_Data);
                    mrm.Flow_Prediction();

                    item.Value.Prediction_Result = mrm.Prediction_Result;
                    item.Value.Set_Hour_prediction();
                }

                if (this.texDfLog != null)
                {
                    foreach (string message in item.Value.Message)
                    {
                        if (this.texDfLog != null)
                        {
                            this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.AppendText("\n"+message); });
                            this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.ScrollToCaret(); });
                        }
                    }

                    for (int x = item.Value.Message.Count - 1; x > 0; x--)
                    {
                        item.Value.Message.RemoveAt(x);
                    }
                }

                if (!item.Value.IsDataError)
                {
                    for (int i = 0; i < 24; i++)
                    {
                        item.Value.TargetTime = i;

                        foreach (string message in item.Value.Message)
                        {
                            if (this.texDfLog != null)
                            {
                                this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.AppendText("\n" + message); });
                                this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.ScrollToCaret(); });
                            }
                        }
                        
                        for (int x = item.Value.Message.Count - 1; x > 0; x--)
                        {
                            item.Value.Message.RemoveAt(x);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 수요예측 실행 객체로 설정된 유량계의 일 수요예측을 실행한다.
        /// 1. 일예측물량산정
        /// 2. 시간예측물량산정
        /// </summary>
        public void Day_Predict()
        {
            foreach (KeyValuePair<string, IDfData> item in items)
            {
                item.Value.TargetDate = this.targetDate;

                if (item.Value.StudyMethod == 0 && !item.Value.IsDataError)
                {
                    Simulation_NN nn = new Simulation_NN();

                    nn.Set_NN(
                        item.Value.Training_Data,
                        2.0,
                        item.Value.LearningRate,
                        item.Value.Momentum,
                        item.Value.Neuron,
                        1,
                        item.Value.Iterations,
                        item.Value.InputNum,
                        item.Value.MaxValue,
                        item.Value.MinValue
                    );

                    nn.Network_Learning();
                    nn.Set_Prediction_Data(item.Value.Input_Data);
                    nn.Flow_Prediction();

                    item.Value.Prediction_Result = nn.Prediction_Result;
                    item.Value.Set_Hour_prediction();
                }
                else if (item.Value.StudyMethod == 1 && !item.Value.IsDataError)
                {
                    Simulation_MRM mrm = new Simulation_MRM();
                    mrm.Set_Data(item.Value.Training_Data, item.Value.Setting_MRM);
                    mrm.Set_Num_Input(item.Value.InputNum);
                    mrm.Set_Param_MinMax_Input(item.Value.MinValue, item.Value.MaxValue);
                    mrm.Set_Param_MinMax_Output(item.Value.MinValue[item.Value.InputNum], item.Value.MaxValue[item.Value.InputNum]);
                    mrm.Set_Expand_Data(item.Value.Training_Data);
                    mrm.Data_Normalize(mrm.data_dimension);
                    mrm.Initialize();
                    mrm.Weight_Calculate();
                    mrm.Set_Prediction_Data(item.Value.Input_Data);
                    mrm.Flow_Prediction();

                    item.Value.Prediction_Result = mrm.Prediction_Result;
                    item.Value.Set_Hour_prediction();
                }

                if (this.texDfLog != null)
                {
                    foreach (string message in item.Value.Message)
                    {
                        this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.AppendText("\n" + message); });
                        this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.ScrollToCaret(); });
                    }

                    for (int x = item.Value.Message.Count - 1; x > 0; x--)
                    {
                        item.Value.Message.RemoveAt(x);
                    }
                }
            }
        }

        public void Time_Predict_AllHour()
        {
            if (this.targetDate == null)
            {
                throw new Exception("예측일자를 설정해야 합니다.");
            }

            foreach (KeyValuePair<string, IDfData> item in items)
            {
                if (!item.Value.IsDataError)
                {
                    for (int i = 0; i < 24; i++)
                    {
                        item.Value.TargetTime = i;
                        if (this.texDfLog != null)
                        {
                            foreach (string message in item.Value.Message)
                            {
                                if (this.texDfLog != null)
                                {
                                    this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.AppendText("\n" + message); });
                                    this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.ScrollToCaret(); });
                                }
                            }

                            for (int x = item.Value.Message.Count - 1; x > 0; x--)
                            {
                                item.Value.Message.RemoveAt(x);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 수요예측 실행 객체로 설정된 유량계의 시간 수요예측을 실행한다.
        /// </summary>
        private void Time_Predict()
        {
            if (this.targetDate == null)
            {
                throw new Exception("예측일자가 설정되지 않았습니다.");
            }

            foreach (KeyValuePair<string, IDfData> item in items)
            {
                if (!item.Value.IsDataError)
                {
                    item.Value.TargetTime = this.targetTIme;
                }
            }
        }

        public void UpdateDayPrediction()
        {
            foreach (KeyValuePair<string, IDfData> item in items)
            {
                if (!item.Value.IsDataError)
                {
                    item.Value.UpdateDayPrediction();
                }
            }
        }

        public void UpdateHourPrediction_CurrentHour()
        {
            foreach (KeyValuePair<string, IDfData> item in items)
            {
                if (!item.Value.IsDataError)
                {
                    item.Value.UpdateHourPrediction_CurrentHour();
                }
            }
        }

        public void UpdateHourPrediction_AllHour()
        {
            foreach (KeyValuePair<string, IDfData> item in items)
            {
                if (!item.Value.IsDataError)
                {
                    item.Value.UpdateHourPrediction_AllHour();
                }
            }
        }
    }
}