﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Collections;
using WaterNETServer.DF_Simulation.work;
using EMFrame.log;

namespace WaterNETServer.DF_Simulation.data
{
    public class DfDataReservoir : IDfData
    {
        public DfDataReservoir()
        {
        }

        public DfDataReservoir(string reservoirKey)
        {
            this.Key = reservoirKey;
        }

        //배수지ID
        private string reservoirKey = null;

        //데이터이상유무
        private bool isDataError = false;

        private List<string> message = new List<string>();

        //수요예측 기본설정값 (배수지별 다르다.)
        private Hashtable setting = null;

        //수요예측 MRM설정값
        private DataTable setting_mrm = null;

        //수요예측 NN설정값
        private Hashtable setting_nn = null;

        //수요예측 이상치설정값
        private DataTable setting_outlier = null;

        //수요예측 데이터
        private DataTable df_data = null;

        //학습을 위한 데이터
        private double[,] training_data = null;

        //예측일자
        private DateTime target_date;

        //예측시간
        private int target_time = -1;

        //일예측유량값
        private double prediction_Result = 0;

        //시간실시간값
        private double[] hour_flow = new double[24];

        //시간예측유량값
        private double[] hour_prediction = new double[24];

        //시간별평균값
        private double[] hour_mean = new double[24];

        //시간별표준편차
        private double[] hour_stdv = new double[24];

        /// <summary>
        /// 배수지 키설정
        ///  - 기본설정값을 DB에서 가져온다
        ///  - MRM설정값을 DB에서 가져온다
        ///  - NN설정값을 DB에서 가져온다
        ///  - 이상치설정값을 DB에서 가져온다
        ///  - 데이터를 DB에서 가져온다
        /// </summary>
        public string Key
        {
            set
            {
                this.message.Add("\n[ 배수지관리번호 : " + value.ToString() + " ]");

                this.reservoirKey = value;
                this.Initialize_Setting();
                this.Initialize_Setting_MRM();
                this.Initialize_Setting_NN();
                this.Initialize_Setting_Outlier();
            }
            get
            {
                return this.reservoirKey;
            }
        }

        /// <summary>
        /// 예측일자 설정
        /// </summary>
        public DateTime TargetDate
        {
            set
            {
                if (value.ToString("yyyy") == "1")
                {
                    this.isDataError = true;
                    return;
                }
                this.target_date = value;
                this.Initialize_Setting_Data();
                try
                {
                    this.Initialize_Training_Data();
                }
                catch (Exception ex)
                {
                    this.isDataError = true;
                    this.message.Add("Error - 학습데이터 생성중에 오류가 나타났습니다.");
                    Logger.Error(ex);
                }
            }
        }

        /// <summary>
        /// 예측시간 설정
        /// </summary>
        public int TargetTime
        {
            set
            {
                this.target_time = value;
                this.Initialize_Time_Data();
            }
        }

        /// <summary>
        /// 기본설정값을 반환한다.
        /// - STUDYMETHOD : 학습설정알고리즘방법(NN/MRM)
        /// - ABNORMALDATA : 이상치보정방법(평균유량보정/예측유량보정/미보정)
        /// - TIMEFORECASTING : 시간예측알고리즘방법(고정분배방식/오차가중분배방식/오차율보정분배방식)
        /// - DAYKIND : 요일정보사용여부
        /// - STUDYDAY : 학습데이터갯수
        /// - STUDYPATTERN : 학습날수
        /// </summary>
        public Hashtable Setting_Default
        {
            get
            {
                return this.setting;
            }
        }

        public int StudyMethod
        {
            get
            {
                return Convert.ToInt16(this.setting["STUDYMETHOD"]);
            }
        }

        public int InputNum
        {
            get
            {
                return Convert.ToInt16(this.setting["STUDYPATTERN"]) - 1;
            }
        }

        /// <summary>
        /// MRM설정값을 반환한다. (학습날수에 따른 order값)
        /// - ATTRIBUTE : 학습날수번호
        /// - VALUE : ORDER
        /// </summary>
        public int[] Setting_MRM
        {
            get
            {
                int[] result = null;
                if (this.setting_mrm == null)
                {
                    return result;
                }

                result = new int[this.setting_mrm.Rows.Count];
                foreach (DataRow row in this.setting_mrm.Rows)
                {
                    result[this.setting_mrm.Rows.IndexOf(row)] = Convert.ToInt16(row["VALUE"]);
                }
                return result;
            }
        }

        /// <summary>
        /// NN설정값을 반환한다.
        /// NEURON : 뉴런 레이어 갯수
        /// LEARNINGRATE : 학습계수
        /// MOMENTUM : 모멘텀계수
        /// ITERATIONS : 학습반복수
        /// </summary>
        public Hashtable Setting_NN
        {
            get
            {
                return this.setting_nn;
            }
        }

        /// <summary>
        /// 모멘텀계수
        /// </summary>
        public double Momentum
        {
            get
            {
                return Convert.ToDouble(this.setting_nn["MOMENTUM"]);
            }
        }

        /// <summary>
        /// 학습계수
        /// </summary>
        public double LearningRate
        {
            get
            {
                return Convert.ToDouble(this.setting_nn["LEARNINGRATE"]);
            }
        }

        /// <summary>
        /// 뉴런 레이어 갯수
        /// </summary>
        public int Neuron
        {
            get
            {
                return Convert.ToInt16(this.setting_nn["NEURON"]);
            }
        }

        /// <summary>
        /// 학습반복수
        /// </summary>
        public int Iterations
        {
            get
            {
                return Convert.ToInt16(this.setting_nn["ITERATIONS"]);
            }
        }

        /// <summary>
        /// 이상치설정값을 반환한다. (각시간대별 표준편차값으로 이상치보정방법에 따라 사용여부 결정)
        /// TIME : 시간
        /// ALPHA : 표준편차
        /// </summary>
        public double[] Setting_Outlier
        {
            get
            {
                double[] result = null;
                if (this.setting_outlier == null)
                {
                    return result;
                }

                result = new double[this.setting_outlier.Rows.Count];
                foreach (DataRow row in this.setting_outlier.Rows)
                {
                    int time = Convert.ToInt16(row["TIME"]);

                    if (time == 24)
                    {
                        time = 1;
                    }
                    else
                    {
                        time++;
                    }
                    result[time - 1] = this.CalCorAlpha(Convert.ToDouble(row["ALPHA"]));
                }
                return result;
            }
        }

        /// <summary>
        /// 데이터를 반환한다.
        ///DLOG_DATE  : 실측일자
        ///DLOG_VALU : 일실측값
        ///VH01 : 01시 실측값
        ///VH02 : 02시 실측값
        ///VH03 : 03시 실측값
        ///VH04 : 04시 실측값
        ///VH05 : 05시 실측값
        ///VH06 : 06시 실측값
        ///VH07 : 07시 실측값
        ///VH08 : 08시 실측값
        ///VH09 : 09시 실측값
        ///VH10 : 10시 실측값
        ///VH11 : 11시 실측값
        ///VH12 : 12시 실측값
        ///VH13 : 13시 실측값
        ///VH14 : 14시 실측값
        ///VH15 : 15시 실측값
        ///VH16 : 16시 실측값
        ///VH17 : 17시 실측값
        ///VH18 : 18시 실측값
        ///VH19 : 19시 실측값
        ///VH20 : 20시 실측값
        ///VH21 : 21시 실측값
        ///VH22 : 22시 실측값
        ///VH23 : 23시 실측값
        ///VH24 : 24시 실측값
        /// </summary>
        public DataTable Setting_Data
        {
            get
            {
                return this.df_data;
            }
        }

        /// <summary>
        /// 학습을 위한 데이터
        /// 수요예측 기본설정의 요일정보사용여부, 학습일수, 요일패턴일수등의 정보를 참고해서 생성된다.
        /// </summary>
        public double[,] Training_Data
        {
            get
            {
                return this.training_data;
            }
        }

        /// <summary>
        /// 예측을 위한 데이터
        /// </summary>
        public double[,] Input_Data
        {
            get
            {
                double[,] temp_input = new double[1, this.InputNum];
                temp_input[0, 0] = this.training_data[this.training_data.GetLength(0) - 1, this.InputNum];
                for (int i = 1; i < this.InputNum; i++)
                {
                    temp_input[0, i] = this.training_data[this.training_data.GetLength(0) - 1, i - 1];
                }

                return temp_input;
            }
        }

        /// <summary>
        /// 일유량 예측 유량값 (타겟일자)
        /// </summary>
        public double Prediction_Result
        {
            get
            {
                return this.prediction_Result;
            }
            set
            {
                if (Double.IsNaN(value))
                {
                    this.isDataError = true;
                    this.message.Add("Error - 일예측량 산정을 정상적으로 수행하지 못했습니다.");
                    this.message.Add("Error - 과거 유량 데이터를 확인해주세요.");
                }
                else
                {
                    this.message.Add(target_date.ToString("yyyy-MM-dd") + " 예측량 : " + value.ToString());
                }

                this.prediction_Result = value;
            }
        }

        /// <summary>
        /// 각시간대별 예측 유량값
        /// </summary>
        public double[] Hour_prediction
        {
            get
            {
                return this.hour_prediction;
            }
        }

        /// <summary>
        /// 학습데이터 기준 각 시간대별 평균값
        /// </summary>
        public double[] Hour_mean
        {
            get
            {
                return this.hour_mean;
            }
        }

        /// <summary>
        /// 학습데이터 기준 각 시간대별 표준편차
        /// </summary>
        public double[] Hour_stdv
        {
            get
            {
                return this.hour_stdv;
            }
        }

        public bool IsDataError
        {
            get
            {
                return this.isDataError;
            }
        }

        public List<string> Message
        {
            get
            {
                return this.message;
            }
        }

        /// <summary>
        /// 최대값 배열을 반환한다.
        /// </summary>
        public double[] MaxValue
        {
            get
            {
                double[] value = new double[this.training_data.GetLength(1)];

                for (int i = 0; i < this.training_data.GetLength(1); i++)
                {
                    double[] temp = new double[this.training_data.GetLength(0)];
                    for (int j = 0; j < this.training_data.GetLength(0); j++)
                    {
                        temp[j] = this.training_data[j, i];
                    }
                    value[i] = temp.Max();
                }
                return value;
            }
        }

        /// <summary>
        /// 최소값 배열을 반환한다.
        /// </summary>
        public double[] MinValue
        {
            get
            {
                double[] value = new double[this.training_data.GetLength(1)];

                for (int i = 0; i < this.training_data.GetLength(1); i++)
                {
                    double[] temp = new double[this.training_data.GetLength(0)];
                    for (int j = 0; j < this.training_data.GetLength(0); j++)
                    {
                        temp[j] = this.training_data[j, i];
                    }
                    value[i] = temp.Min();
                }
                return value;
            }
        }

        /// <summary>
        /// 배수지의 수요예측 기본설정값을 조회한다.
        /// 설정등록값이 존재하지 않으면 DfSetting의 기본 설정 내용을 사용한다.
        /// </summary>
        private void Initialize_Setting()
        {
            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.reservoirKey;
            this.setting = DfSimulationWork.GetInstance().SelectDfSimulationSettingReservoir(parameter);

            if (this.setting == null)
            {
                this.setting = DfSetting.DefaultSetting;
            }
        }

        /// <summary>
        /// 배수지의 수요예측 MRM설정값을 조회한다.
        /// 설정등록값이 존재하지 않으면 DfSetting의 기본 설정 내용을 사용한다.
        /// </summary>
        private void Initialize_Setting_MRM()
        {
            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.reservoirKey;
            this.setting_mrm = DfSimulationWork.GetInstance().SelectDfSimulationSettingMRM(parameter);

            if (this.setting_mrm == null)
            {
                DataTable mrm = new DataTable();
                mrm.Columns.Add("VALUE");

                for (int i = 0; i < this.InputNum; i++)
                {
                    mrm.Rows.Add(DfSetting.DefaultSettingMRM);
                }

                this.setting_mrm = mrm;
            }
        }

        /// <summary>
        /// 배수지의 수요예측 NN설정값을 조회한다.
        /// 설정등록값이 존재하지 않으면 DfSetting의 기본 설정 내용을 사용한다.
        /// </summary>
        private void Initialize_Setting_NN()
        {
            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.reservoirKey;
            this.setting_nn = DfSimulationWork.GetInstance().SelectDfSimulationSettingNN(parameter);

            if (this.setting_nn == null)
            {
                this.setting_nn = DfSetting.DefaultSettingNN;
            }
        }

        /// <summary>
        /// 배수지의 수요예측 Outlier설정값을 조회한다.
        /// 설정등록값이 존재하지 않으면 DfSetting의 기본 설정 내용을 사용한다.
        /// </summary>
        private void Initialize_Setting_Outlier()
        {
            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.reservoirKey;
            this.setting_outlier = DfSimulationWork.GetInstance().SelectDfSimulationSettingOutlier(parameter);

            if (this.setting_outlier == null)
            {
                DataTable outlier = new DataTable();
                outlier.Columns.Add("TIME");
                outlier.Columns.Add("ALPHA");

                for (int i = 0; i < 24; i++)
                {
                    outlier.Rows.Add((i + 1).ToString(), DfSetting.DefaultSettingOutlier);
                }

                this.setting_outlier = outlier;
            }
        }

        /// <summary>
        /// 배수지의 수요예측 학습에 사용될 데이터를 조회한다.
        /// </summary>
        private void Initialize_Setting_Data()
        {
            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.reservoirKey;
            parameter["TARGETDATE"] = this.target_date.ToString("yyyyMMdd");
            parameter["STUDYDAY"] = Convert.ToInt16(this.Setting_Default["STUDYDAY"]);

            if (this.setting["USE_YN"].ToString() == "N")
            {
                this.isDataError = true;
                this.message.Add("Error - 현재 배수지는 수요예측 실행이 설정된 배수지가 아닙니다.");
                return;
            }

            //태그여부를 조회 태그가 존재하지 않으면 바로 예외 메세지 완성
            this.isDataError = DfSimulationWork.GetInstance().IsTagErrorReservoir(parameter);

            if (this.isDataError)
            {
                this.message.Add("Error - 배수지에 적산차태그가 연결되어있지 않습니다.");
                this.message.Add("Error - 현재 배수지의 수요예측을 종료합니다.");
                return;
            }

            //표준규격여부를 조회 표준규격이 존재하지 않으면 바로 에외 메세지 완성
            //this.isDataError = DfSimulationWork.GetInstance().IsSpecErrorReservoir(parameter);

            //if (this.isDataError)
            //{
            //    this.message.Add("Error - 배수지에 표준규격 정보가 연결되어있지 않습니다.");
            //    this.message.Add("Error - 현재 배수지의 수요예측을 종료합니다.");
            //    return;
            //}

            this.df_data = DfSimulationWork.GetInstance().SelectDfSimulationSettingDataReservoir(parameter);
        }

        /// <summary>
        /// 데이터를 학습 데이터 형식으로 변환한다.
        /// </summary>
        private void Initialize_Training_Data()
        {
            if (this.isDataError)
            {
                return;
            }

            //요일정보사용유무
            bool day_kind_use = Convert.ToBoolean(this.Setting_Default["DAYKIND"]);
            int study_day = Convert.ToInt16(this.Setting_Default["STUDYDAY"]);
            int study_pattern = Convert.ToInt16(this.Setting_Default["STUDYPATTERN"]);
            DataTable s_data = this.Setting_Data;

            if (s_data.Rows.Count != study_day + 1)
            {
                study_day = s_data.Rows.Count - 1;
            }

            //학습데이터로 생성되는 일자의 각시간대별 평균값을 구하기위한 임시저장 변수
            double[,] hour_mean_temp = null;

            if (!day_kind_use)
            {
                List<int> ii = new List<int>();
                for (int i = s_data.Rows.Count - 1; i >= 0; i--)
                {
                    if (s_data.Rows[i]["RESULT"].ToString() != "F")
                    {
                        ii.Add(i);
                    }
                }

                int row_index = ii.Count - study_pattern + 1;

                if (row_index <= 1)
                {
                    this.isDataError = true;
                    this.message.Add("Error - 학습데이터 생성 일수가 부족하여 수요예측을 수행하지 못했습니다.");
                    this.message.Add("Error - 과거 유량 데이터를 확인해주세요.");
                    return;
                }

                this.training_data = new double[row_index, study_pattern];
                hour_mean_temp = new double[24, row_index];

                for (int i = 0; i < row_index; i++)
                {
                    this.training_data[i, study_pattern - 1] = Convert.ToDouble(s_data.Rows[ii[i]]["DLOG_VALU"]);
                }

                for (int i = 0; i < row_index; i++)
                {
                    for (int j = 0; j < study_pattern - 1; j++)
                    {
                        this.training_data[i, j] = Convert.ToDouble(s_data.Rows[ii[i + j + 1]]["DLOG_VALU"]);
                    }
                    hour_mean_temp[0, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH01"]);
                    hour_mean_temp[1, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH02"]);
                    hour_mean_temp[2, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH03"]);
                    hour_mean_temp[3, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH04"]);
                    hour_mean_temp[4, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH05"]);
                    hour_mean_temp[5, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH06"]);
                    hour_mean_temp[6, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH07"]);
                    hour_mean_temp[7, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH08"]);
                    hour_mean_temp[8, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH09"]);
                    hour_mean_temp[9, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH10"]);
                    hour_mean_temp[10, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH11"]);
                    hour_mean_temp[11, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH12"]);
                    hour_mean_temp[12, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH13"]);
                    hour_mean_temp[13, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH14"]);
                    hour_mean_temp[14, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH15"]);
                    hour_mean_temp[15, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH16"]);
                    hour_mean_temp[16, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH17"]);
                    hour_mean_temp[17, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH18"]);
                    hour_mean_temp[18, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH19"]);
                    hour_mean_temp[19, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH20"]);
                    hour_mean_temp[20, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH21"]);
                    hour_mean_temp[21, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH22"]);
                    hour_mean_temp[22, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH23"]);
                    hour_mean_temp[23, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH24"]);
                }
            }
            else if (day_kind_use)
            {
                List<int> ii = new List<int>();
                for (int i = s_data.Rows.Count - 1; i >= 0; i -= study_pattern)
                {
                    if (s_data.Rows[i]["RESULT"].ToString() != "F")
                    {
                        ii.Add(i);
                    }
                }

                int row_index = ii.Count - study_pattern + 1;

                if (row_index <= 1)
                {
                    this.isDataError = true;
                    this.message.Add("Error - 학습데이터 생성 일수가 부족하여 수요예측을 수행하지 못했습니다.");
                    this.message.Add("Error - 과거 유량 데이터를 확인해주세요.");
                    return;
                }

                this.training_data = new double[row_index, study_pattern];
                hour_mean_temp = new double[24, row_index];

                for (int i = 0; i < row_index; i++)
                {
                    this.training_data[i, study_pattern - 1] = Convert.ToDouble(s_data.Rows[ii[i]]["DLOG_VALU"]);
                }

                for (int i = 0; i < row_index; i++)
                {
                    for (int j = 0; j < study_pattern - 1; j++)
                    {
                        this.training_data[i, j] = Convert.ToDouble(s_data.Rows[ii[i + j + 1]]["DLOG_VALU"]);
                    }
                    hour_mean_temp[0, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH01"]);
                    hour_mean_temp[1, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH02"]);
                    hour_mean_temp[2, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH03"]);
                    hour_mean_temp[3, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH04"]);
                    hour_mean_temp[4, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH05"]);
                    hour_mean_temp[5, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH06"]);
                    hour_mean_temp[6, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH07"]);
                    hour_mean_temp[7, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH08"]);
                    hour_mean_temp[8, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH09"]);
                    hour_mean_temp[9, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH10"]);
                    hour_mean_temp[10, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH11"]);
                    hour_mean_temp[11, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH12"]);
                    hour_mean_temp[12, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH13"]);
                    hour_mean_temp[13, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH14"]);
                    hour_mean_temp[14, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH15"]);
                    hour_mean_temp[15, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH16"]);
                    hour_mean_temp[16, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH17"]);
                    hour_mean_temp[17, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH18"]);
                    hour_mean_temp[18, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH19"]);
                    hour_mean_temp[19, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH20"]);
                    hour_mean_temp[20, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH21"]);
                    hour_mean_temp[21, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH22"]);
                    hour_mean_temp[22, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH23"]);
                    hour_mean_temp[23, i] = Convert.ToDouble(s_data.Rows[ii[i]]["VH24"]);
                }
            }

            this.hour_mean = new double[24];
            this.hour_stdv = new double[24];
            this.hour_flow = new double[24];


            if (hour_mean_temp != null)
            {
                for (int i = 0; i < 24; i++)
                {
                    double[] temp_mean = new double[hour_mean_temp.GetLength(1)];
                    double[] temp_stdv = new double[hour_mean_temp.GetLength(1)];

                    for (int j = 0; j < temp_mean.Length; j++)
                    {
                        temp_mean[j] = hour_mean_temp[i, j];
                        temp_stdv[j] = Math.Pow(hour_mean_temp[i, j], 2.0);
                    }
                    this.hour_mean[i] = temp_mean.Average();
                    this.hour_stdv[i] = Math.Sqrt(temp_stdv.Average() - this.hour_mean[i] * this.hour_mean[i]);
                }
            }
        }

        /// <summary>
        /// 일예측결과로 시간별 예측량을 생성한다.
        /// </summary>
        public void Set_Hour_prediction()
        {
            if (this.isDataError)
            {
                return;
            }

            double sum_mean = 0;
            for (int i = 0; i < 24; i++)
            {
                sum_mean += this.Hour_mean[i];
            }
            for (int i = 0; i < 24; i++)
            {
                this.Hour_prediction[i] = this.Prediction_Result * (this.Hour_mean[i] / sum_mean);
            }

            this.target_time = -1;
        }

        /// <summary>
        /// 배수지의 수요예측 학습에 사용될 시간적산값을 조회한다.
        /// </summary>
        private void Initialize_Time_Data()
        {
            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.reservoirKey;
            parameter["TARGETDATE"] = this.target_date.ToString("yyyyMMdd");
            parameter["TARGERTIME"] = this.target_time.ToString("00");

            this.hour_flow[this.target_time] = DfSimulationWork.GetInstance().SelectDfSimulationTimeDataReservoir(parameter);

            //이상치 보정설정값
            int abnormaldata = Convert.ToInt16(this.Setting_Default["ABNORMALDATA"]);

            //시간예측 알고리즘값
            int timeforecasting = Convert.ToInt16(this.Setting_Default["TIMEFORECASTING"]);

            //현재시간유량이 이상데이터라면 보정한다.
            if (this.IsCorrection())
            {
                this.correctHourRealFlow();
            }

            //다음 시간예측값을 보정한다.
            this.correctHourPrediction();
        }

        /// <summary>
        /// 시간적산유량값의 이상유무를 판단한다.
        /// </summary>
        /// <returns></returns>
        private bool IsCorrection()
        {
            bool result = false;

            int time = this.target_time;

            if (this.hour_flow[time] < this.hour_mean[time] - this.Setting_Outlier[time] * this.hour_stdv[time]
                || this.hour_flow[time] > this.hour_mean[time] + this.Setting_Outlier[time] * this.hour_stdv[time])
            {
                result = true;
            }

            //if (result)
            //{
            //    Console.WriteLine(this.target_date.ToString("yyyyMMdd") + "일 " + time.ToString("00") + "시 의 현재 유량(" + this.hour_flow[time].ToString() + ") 은 이상유량으로 판단됨");
            //    Console.Write("최저 " + (this.hour_mean[time] - this.Setting_Outlier[time] * this.hour_stdv[time]).ToString() + " ~ ");
            //    Console.WriteLine("최고 " + (this.hour_mean[time] + this.Setting_Outlier[time] * this.hour_stdv[time]).ToString());
            //}
            //else
            //{
            //    Console.WriteLine(this.target_date.ToString("yyyyMMdd") + "일 " + time.ToString("00") + "시 의 현재 유량(" + this.hour_flow[time].ToString() + ") 은 정상유량으로 판단됨");
            //}

            return result;
        }

        /// <summary>
        /// 이상으로 판단된 시간적산유량값을 보정한다.
        /// </summary>
        private void correctHourRealFlow()
        {
            //이상치 보정설정값
            int abnormaldata = Convert.ToInt16(this.Setting_Default["ABNORMALDATA"]);

            //평균유량
            if (abnormaldata == 0)
            {
                this.hour_flow[this.target_time] = this.hour_mean[this.target_time];
            }
            //예측유량
            else if (abnormaldata == 1)
            {
                this.hour_flow[this.target_time] = this.hour_prediction[this.target_time];
            }
        }

        /// <summary>
        /// 다음시간의 예측시간적산유량값을 보정한다.
        /// </summary>
        private void correctHourPrediction()
        {
            if (this.isDataError)
            {
                return;
            }

            double errorRateBoundary = 5.0;
            double current_flow = this.hour_flow[this.target_time];

            //전시간의 유량 예측값 (00시라면 전일 23시의 값을 가져야한다.)
            double past_flow_prediction = 0;
            double pre_past_flow_prediction = 0;

            if (this.target_time == 0)
            {
                past_flow_prediction = this.hour_prediction[23];
                pre_past_flow_prediction = this.hour_prediction[22];
            }
            else if (this.target_time == 1)
            {
                pre_past_flow_prediction = this.hour_prediction[23];
            }
            else
            {
                past_flow_prediction = this.hour_prediction[this.target_time - 1];
                pre_past_flow_prediction = this.hour_prediction[this.target_time - 2];
            }

            double flow_error = this.hour_flow[this.target_time] - past_flow_prediction;

            if (past_flow_prediction == 0)
            {
                past_flow_prediction = 1.0e-10;
            }

            double flow_error_rate = 100.0 * Math.Abs(flow_error) / past_flow_prediction;

            //시간예측 알고리즘값
            int timeforecasting = Convert.ToInt16(this.Setting_Default["TIMEFORECASTING"]);

            double ttt = this.hour_prediction[this.target_time];

            //실시간 보정분배
            if (timeforecasting == 1)
            {
                double temp_sum = 0;

                for (int i = this.target_time; i < 24; i++)
                {
                    temp_sum += hour_prediction[i];
                }

                if (temp_sum != 0)
                {
                    for (int j = this.target_time; j < 24; j++)
                    {
                        this.hour_prediction[j] = (temp_sum + flow_error) * hour_prediction[j] / temp_sum;
                    }
                }
            }
            //오차비례보정분배
            else if (timeforecasting == 2)
            {
                double temp = hour_prediction[this.target_time];
                if (Math.Abs(current_flow - pre_past_flow_prediction) < Math.Abs(flow_error))
                {
                    pre_past_flow_prediction = -1;
                }
                if (pre_past_flow_prediction > 0 && flow_error_rate >= errorRateBoundary)
                {
                    hour_prediction[this.target_time] = hour_prediction[this.target_time] * (1.0 + (100.0 * flow_error / past_flow_prediction) / 200.0);
                }
            }

            this.message.Clear();
            this.message.Add(this.target_time.ToString("00") + "시 처음 예측량 :\t" + Math.Round(ttt, 3).ToString() + " \t보정 예측량 :\t" + Math.Round(this.hour_prediction[this.target_time], 3).ToString() + "\t실측량 :\t" + Math.Round(this.hour_flow[this.target_time], 3).ToString());
            //Console.WriteLine(this.target_time.ToString("00") + "시\t" + "초기 예측량 : " + ttt.ToString() + " \t보정 예측량 : " + this.hour_prediction[this.target_time].ToString() + "\t실측량 : " + this.hour_flow[this.target_time].ToString());
        }

        private double CalCorAlpha(double x)
        {
            return System.Math.Sqrt(2.0) * CalInvErf(2.0 * (1.0 - (100.0 - x) / 200.0) - 1.0);
        }

        private double CalInvErf(double x)
        {
            // Constants
            double a = 0.140012;

            // Save the sign of x
            double sign = 1;
            if (x < 0)
                sign = -1;

            double temp1 = 2.0 / (System.Math.PI * a);
            double temp2 = System.Math.Log(1 - x * x);

            return sign * System.Math.Sqrt(System.Math.Sqrt(System.Math.Pow(temp1 + temp2 / 2.0, 2) - temp2 / a) - (temp1 + temp2 / 2.0));
        }


        //일예측량을 등록 및 수정 한다.
        public void UpdateDayPrediction()
        {
            if (this.isDataError)
            {
                return;
            }

            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.reservoirKey;
            parameter["DF_RL_DT"] = this.target_date.ToString("yyyyMMdd");
            parameter["DF_TDFQ"] = ((int)this.prediction_Result).ToString();
            DfSimulationWork.GetInstance().UpdateDayPrediction(parameter);
        }

        //현재시간의 예측량을 등록 및 수정 한다.
        public void UpdateHourPrediction_CurrentHour()
        {
            if (this.isDataError)
            {
                return;
            }

            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.reservoirKey;
            parameter["DF_RL_DT"] = this.target_date.ToString("yyyyMMdd");

            parameter["DF_RL_HOUR"] = this.target_time.ToString("00");
            parameter["DF_THFQ"] = ((int)this.hour_prediction[this.target_time]).ToString();
            DfSimulationWork.GetInstance().UpdateHourPrediction(parameter);
        }

        //24시간의 예측량을 등록 및 수정 한다.
        public void UpdateHourPrediction_AllHour()
        {
            if (this.isDataError)
            {
                return;
            }

            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.reservoirKey;
            parameter["DF_RL_DT"] = this.target_date.ToString("yyyyMMdd");
            DfSimulationWork.GetInstance().UpdateHourPrediction(parameter, this.hour_prediction);
        }
    }
}
