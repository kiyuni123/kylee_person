﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Data;
using System.Windows.Forms;
using WaterNETServer.DF_Algorithm.NN_Core;

namespace WaterNETServer.DF_Algorithm.Simulation
{
    /// <summary>
    /// 신경 회로망 시뮬레이션 클래스
    /// </summary>
    /// 
    /// <remarks>신경 회로망을 시뮬레이션 하기 위한 객체이다.
    /// </remarks>
    public class Simulation_NN
    {
        /// <summary>
        /// 수요 예측을 위한 유량 및 기상 데이터 입력 값 (Normalize값)
        /// </summary>
        public double[,] normalize_prediction_data;
        /// <summary>
        /// 수요 예측을 위한 유량 및 기상 데이터 입력 값 (원시값)
        /// </summary>
        public double[,] prediction_data;

        /// <summary>
        /// 네트워크 학습을 위한 유량 및 기상 데이터 입력 값(Normalize값)
        /// 마지막 column은 원하는 유량 출력 값(Normalize값)
        /// </summary>
        public double[,] normalize_data;
        /// <summary>
        /// 네트워크 학습을 위한 유량 및 기상 데이터 입력 값 및 원하는 유량 출력(원시값)
        /// 마지막 column은 원하는 유량 출력 값(원시값)
        /// </summary>
        public double[,] data;

        /// <summary>
        /// Sigmoid Function의 Alpha 값
        /// </summary>
        public double f_sigmoidAlphaValue;
        /// <summary>
        /// 학습 계수
        /// </summary>
        public double f_learningRate;
        /// <summary>
        /// 모멘텀 계수
        /// </summary>
        public double f_momentum;
        /// <summary>
        /// 쓰레드 동작을 위한 flag
        /// </summary>
        public bool needToStop;
        //   public int n_neuronsInFirstLayer;
        //   public int n_neuronsInSecondLayer;

        /// <summary>
        /// 최대 반복 학습 시행 수 
        /// </summary>
        public int n_iterations;
        /// <summary>
        /// 입력 수
        /// </summary>
        public int n_input;
        /// <summary>
        /// 레이어 수
        /// </summary>
        public int n_layer;
        /// <summary>
        /// 각 레이어의 뉴런 수(배열 형태)
        /// </summary>
        public int[] n_neuron;

        /// <summary>
        /// 현재 반복 학습 시행 횟수
        /// </summary>
        public int iteration;//MOD
        /// <summary>
        /// 학습 완료 여부 플래그
        /// </summary>
        public bool learning_completed = false;

        /// <summary>
        /// 학습시키거나 또는 수요 예측에 이용할 네트워크
        /// </summary>
        NN_Core.ActivationNetwork network;


        public double[] xFactor;
        /// <summary>
        /// 입력 변수별 최소치(배열 형태)
        /// </summary>
        public double[] xMin;
        /// <summary>
        /// 입력 변수별 최대치(배열 형태)
        /// </summary>
        public double[] xMax;
        public double yFactor;
        /// <summary>
        /// 출력 유량 최소치
        /// </summary>
        public double yMin;
        /// <summary>
        /// 출력 유량 최대치
        /// </summary>
        public double yMax;

        /// <summary>
        /// 수요 예측 결과 일 유량 값
        /// </summary>
        public double Prediction_Result;

        /// <summary>
        /// Epoch별 학습 에러 평균값
        /// </summary>
        public double[] Error_Learning_Mean;

        /// <summary>
        /// 학습 정확도 평균값
        /// </summary>
        public double[] ACC_Learning_MAPE;

        /// <summary>
        ///  Epoch별, Training Data별 학습 정확도
        /// </summary>
        public double[][] Error_Learning;

        /// <summary>
        ///  테스트 에러 평균값
        /// </summary>
        public double Error_Test_Mean;
        /// <summary>
        ///  테스트 정확도 평균값
        /// </summary>
        public double ACC_Test_MAPE;
        /// <summary>
        ///  Test Data별 테스트 정확도
        /// </summary>
        public double[] Error_Test;
        /// <summary>
        ///  수요 예측 에러
        /// </summary>
        public double Error_Prediction;

        /// <summary>
        /// <see cref="Simulation_NN"/> 클래스의 객체 초기화하기
        /// </summary>
        /// 
        /// <remarks>각종 필요한 파라미터들은 디폴트 값으로 처리된다.
        /// 학습 계수 : 0.005
        /// 모멘텀 계수 : 0.0
        /// Sigmoid 함수 알파 값 : 2.0
        /// 히든 레이어 뉴런 수 : 25
        /// 출력 레이어 뉴런 수 : 1
        /// 최대 학습 반복 시행 수 : 40000
        /// 입력 수 : 7
        /// 레이어 수 : 2
        /// </remarks>

        public Simulation_NN()
        {
            f_learningRate = 0.005;
            f_momentum = 0.0;
            f_sigmoidAlphaValue = 2.0;
            n_neuron = new int[2];
            n_neuron[0] = 25;
            n_neuron[1] = 1;
            n_iterations = 40000;
            n_input = 7;
            n_layer = 2;

        }

        /// <summary>
        /// Simulation_NN 클래스의 파라미터 지정 함수
        /// </summary>
        /// <param name="_data">학습시킬 유량 및 기상데이터 입력과 원하는 유량 출력으로 이루어진 2차 배열</param>
        /// <param name="_sigmoidAlphaValue">Sigmoid 함수의 알파 값</param>
        /// <param name="_learningRate">학습 계수 값</param>
        /// <param name="_momentum">모멘텀 계수 값</param>
        /// <param name="_neuronsInFirstLayer">히든 레이어의 뉴런 수</param>
        /// <param name="_neuronsInSecondLayer">출력 레이어의 뉴런 수</param>
        /// <param name="_iterations">최대 학습 반복 시행 수</param>
        /// <param name="_input">입력 수</param>
        /// <param name="_maxX">입력들과 출력 변수의 최대값</param>
        /// <param name="_minX">입력들과 출력 변수의 최소값</param>
        /// 
        /// <remarks>시뮬레이션 클래스가 만들어지고, 네트워크 학습에 필요한 여러가지 파라미터 정보들을 입력해준다.</remarks>
        /// 
        /// 
        public void Set_NN(double[,] _data, double _sigmoidAlphaValue, double _learningRate
                 , double _momentum, int _neuronsInFirstLayer, int _neuronsInSecondLayer, int _iterations, int _input, double[] _maxX, double[] _minX)
        {
            data = _data;
            f_sigmoidAlphaValue = _sigmoidAlphaValue;
            f_learningRate = _learningRate;
            f_momentum = _momentum;
            n_neuron = new int[2];
            n_neuron[0] = _neuronsInFirstLayer;
            n_neuron[1] = _neuronsInSecondLayer;
            n_iterations = _iterations;
            n_input = _input;
            xMin = new double[n_input];
            xFactor = new double[n_input];

            // Bipolar Sigmoid Function의 특성에 맞게 최소치와 최대치를 더 넓게 잡아서 학습이 효율적으로 되도록 함.
            for (int i = 0; i < n_input; i++)
            {
                xMin[i] = _minX[i] * 0.8;
                xFactor[i] = 2.0 / (_maxX[i] * 1.2 - _minX[i] * 0.8);

            }
            yMin = _minX[n_input] * 0.8;
            yFactor = 1.7 / (_maxX[n_input] * 1.2 - _minX[n_input] * 0.8);
            //Prediction(1);//MOD

            Data_Normalize(data);
        }

        /// <summary>
        /// 디버깅용 코드(사용 하지 않음)
        /// </summary>
        public void Prediction(int mode)
        {
            if (mode == 1)
            {
                Data_Normalize(data);
                Network_Learning();//MOD
                Save_Network("xxx.xml");

            }
            else if (mode == 2)
            {
                Data_Normalize(data);
                Network_Learning();//MOD
                Save_Network("xxx.xml");
                Flow_Prediction();
                Save_Prediction_Result();
            }
            else if (mode == 3)
            {
                Flow_Prediction();
                Save_Prediction_Result();
            }
            else if (mode == 4)
            {
                Load_Network("xxx.xml");
                Flow_Prediction();
                Save_Prediction_Result();
            }
            else if (mode == 5)
            {
                Data_Min_Max(data);
                Data_Normalize(data);
                Network_Learning();//MOD
                Save_Network("xxx.xml");
                Load_Network("xxx.xml");
            }

        }

        /// <summary>
        /// 디버깅용 코드(사용 하지 않음)
        /// </summary>
        public void Data_Min_Max(double[,] data)
        {
            int n_sample = data.GetLength(0);
            int n_input = data.GetLength(1) - 1;

            double[] minX = new double[n_input];
            double[] maxX = new double[n_input];
            double minY, maxY;

            for (int i = 0; i < n_input; i++)
            {
                minX[i] = 1000000.0;
                maxX[i] = -1000000.0;
                for (int j = 0; j < n_sample; j++)
                {
                    if (data[j, i] < minX[i])
                        minX[i] = data[j, i];
                    if (data[j, i] > maxX[i])
                        maxX[i] = data[j, i];
                }
            }

            maxY = -1000000.0;
            minY = 1000000.0;
            for (int j = 0; j < n_sample; j++)
            {
                if (data[j, n_input] < minY)
                    minY = data[j, n_input];
                if (data[j, n_input] > maxY)
                    maxY = data[j, n_input];

            }

            for (int i = 0; i < n_input; i++)
            {
                maxX[i] *= 1.2;
                minX[i] *= 0.8;
            }

            maxY *= 1.2;
            minY *= 0.8;


            yFactor = 1.7 / (maxY - minY);
            yMin = minY;
            xFactor = new double[n_input];
            xMin = new double[n_input];
            for (int i = 0; i < n_input; i++)
            {
                xFactor[i] = 2.0 / (maxX[i] - minX[i]);
                xMin[i] = minX[i];
            }
        }

        /// <summary>
        /// 데이터들을 Normalize하는 함수
        /// </summary>
        /// <param name="_data">학습시킬 유량 및 기상데이터 입력과 원하는 유량 출력, 또는 수요 예측에 필요한 입력 데이터 </param>
        /// 
        /// <remarks>입력 개수와 _data의 크기가 동일하면 수요 예측에 필요한 Normalization 데이터로 변환
        /// 입력 개수보다 _data의 크기가 하나 더 크면 네트워크 학습에 필요한 Noramlization 데이터로 변환</remarks>
        /// 
        /// 
        public void Data_Normalize(double[,] _data)
        {
            // 입력 데이터가 이틀 이상 분량이라면 학습을 위한 데이터
            if (_data.GetLength(0) > 1)
            {

                normalize_data = new double[_data.GetLength(0), n_input + 1];
                // 데이터 전체 일수에 대해
                for (int i = 0; i < _data.GetLength(0); i++)
                {
                    // 데이터 입력 변수들에 대해
                    for (int j = 0; j < n_input; j++)
                    {
                        // -1 ~ 1 까지 노말라이즈
                        normalize_data[i, j] = (_data[i, j] - xMin[j]) * xFactor[j] - 1.0;
                    }
                    // 데이터 출력 변수에 대해
                    normalize_data[i, n_input] = (_data[i, n_input] - yMin) * yFactor - 0.85;


                }
            }

            // 입력 데이터가 하루 분량이면 수요 예측을 위한 데이터
            else if (_data.GetLength(0) == 1)
            {
                normalize_prediction_data = new double[_data.GetLength(0), n_input];
                for (int i = 0; i < _data.GetLength(0); i++)
                {
                    // 데이터 입력 변수들에 대해
                    for (int j = 0; j < n_input; j++)
                    {
                        // -1 ~ 1 까지 노말라이즈
                        normalize_prediction_data[i, j] = (_data[i, j] - xMin[j]) * xFactor[j] - 1.0;
                        //MessageBox.Show(_data[i, j].ToString() + " , " + xMin[j].ToString() + " , " + xFactor[j].ToString());
                    }
                    // 수요 예측을 위한 데이터에는 출력 변수가 존재하지 않음
                }
            }


        }

        /// <summary>
        /// 본격적으로 Neural Network Core 클래스들을 이용하여 주어진 파라미터와 학습 데이터로 네트워크를 학습.
        /// </summary>
        /// 
        /// <remarks>여러 날짜로 이루어진 학습데이터를 10등분하여 임의로 하나의 집합은 test set으로, 나머지 9개의 집합은
        /// training set로 쓴다. Test set은 10일 간격으로 배당된 데이터들로 이루어진다.</remarks>
        /// 
        /// 
        public void Network_Learning()//MOD
        {
            needToStop = false;//MOD

            // 네트워크 생성
            network = new ActivationNetwork(new BipolarSigmoidFunction(f_sigmoidAlphaValue),
               n_input, n_neuron[0], n_neuron[1]);

            // 학습을 수행하는 주체
            BackPropagationLearning teacher = new BackPropagationLearning(network);

            teacher.f_learningRate = f_learningRate;
            teacher.f_momentum = f_momentum;

            // 10-fold Cross Validation
            Random r = new Random();
            int randomidx = r.Next(0, 9);
            int n_test_data, n_train_data;

            // 전체 데이터의 1/10은 테스트용, 9/10은 학습용
            if (normalize_data.GetLength(0) % 10 > randomidx)
            {
                n_test_data = normalize_data.GetLength(0) / 10 + 1;
            }
            else
            {
                n_test_data = normalize_data.GetLength(0) / 10;
            }

            n_train_data = normalize_data.GetLength(0) - n_test_data;

            double[,] test_data = new double[n_test_data, n_input + 1];
            double[,] train_data = new double[n_train_data, n_input + 1];
            int test_data_idx = 0;
            int train_data_idx = 0;

            double[][] unnormalize_train_data_input = new double[n_train_data][];
            double[][] unnormalize_train_data_output = new double[n_train_data][];
            double[][] unnormalize_test_data_input = new double[n_test_data][];
            double[][] unnormalize_test_data_output = new double[n_test_data][];
            double[][] train_data_input = new double[n_train_data][];
            double[][] train_data_output = new double[n_train_data][];
            double[][] test_data_input = new double[n_test_data][];
            double[][] test_data_output = new double[n_test_data][];

            //string strSaveFilePath = Application.StartupPath + "\\DF_data\\" + "training.txt";
            //StreamWriter file = new StreamWriter(strSaveFilePath, false, System.Text.Encoding.ASCII);

            //string strSaveFilePath2 = Application.StartupPath + "\\DF_data\\" + "test.txt";
            //StreamWriter file2 = new StreamWriter(strSaveFilePath2, false, System.Text.Encoding.ASCII);


            // 데이터의 전체 일수에 대하여
            for (int i = 0, k = normalize_data.GetLength(0); i < k; i++)
            {
                // 테스트용 데이터에 대해 노말라이즈된 입력과 원시 입력 값들 저장
                if (i % 10 == randomidx)
                {
                    test_data_input[test_data_idx] = new double[n_input];
                    test_data_output[test_data_idx] = new double[1];
                    unnormalize_test_data_input[test_data_idx] = new double[n_input];
                    unnormalize_test_data_output[test_data_idx] = new double[1];
                    for (int j = 0; j < n_input; j++)
                    {
                        test_data_input[test_data_idx][j] = normalize_data[i, j];
                        unnormalize_test_data_input[test_data_idx][j] = data[i, j];
                    }
                    test_data_output[test_data_idx][0] = normalize_data[i, n_input];
                    unnormalize_test_data_output[test_data_idx][0] = data[i, n_input];
                    test_data_idx++;
                }
                // 학습용 데이터에 대해 노말라이즈된 입력과 원시 입력 값들 저장
                else
                {
                    train_data_input[train_data_idx] = new double[n_input];
                    train_data_output[train_data_idx] = new double[1];
                    unnormalize_train_data_input[train_data_idx] = new double[n_input];
                    unnormalize_train_data_output[train_data_idx] = new double[1];
                    for (int j = 0; j < n_input; j++)
                    {
                        train_data_input[train_data_idx][j] = normalize_data[i, j];
                        unnormalize_train_data_input[train_data_idx][j] = data[i, j];
                    }
                    train_data_output[train_data_idx][0] = normalize_data[i, n_input];
                    unnormalize_train_data_output[train_data_idx][0] = data[i, n_input];
                    train_data_idx++;
                }
            }

            iteration = 1;

            double[] solution_train = new double[n_train_data];
            double[] networkInput = new double[n_input];

            Error_Learning = new double[n_iterations][];
            Error_Learning_Mean = new double[n_iterations];
            ACC_Learning_MAPE = new double[n_iterations];
            //file.Write("Training Error");
            //file.WriteLine();

            // 중간에 정지 명령이나 정해놓은 최대 학습 시행 횟수에 다다르기 까지
            while (!needToStop)
            {
                // 학습용 입력 데이터와 출력 데이터로 네트워크 학습
                double error = teacher.RunEpoch(train_data_input, train_data_output) / n_train_data;

                Error_Learning_Mean[iteration - 1] = 0.0;
                Error_Learning[iteration - 1] = new double[train_data.GetLength(0)];
                for (int i = 0, k = train_data.GetLength(0); i < k; i++)
                {
                    for (int j = 0; j < n_input; j++)
                    {
                        // 학습된 네트워크에 집어 넣을 입력은 학습용 입력 데이터
                        networkInput[j] = train_data_input[i][j];
                    }
                    // 위의 입력으로 학습된 네트워크에 집어 넣어 얻은 솔루션(유량)
                    solution_train[i] = (network.Compute(networkInput)[0] + 0.85) / yFactor + yMin;
                    // 네트워크의 솔루션과 학습용 출력 데이터 간의 오차
                    Error_Learning[iteration - 1][i] = Math.Abs(unnormalize_train_data_output[i][0] - solution_train[i]);
                    Error_Learning_Mean[iteration - 1] += Math.Abs(unnormalize_train_data_output[i][0] - solution_train[i]);
                    ACC_Learning_MAPE[iteration - 1] += Math.Abs(unnormalize_train_data_output[i][0] - solution_train[i]) / unnormalize_train_data_output[i][0];
                }
                // 학습 오차 평균
                Error_Learning_Mean[iteration - 1] /= n_train_data;
                ACC_Learning_MAPE[iteration - 1] /= n_train_data;
                // 학습 정확도
                ACC_Learning_MAPE[iteration - 1] = 100.0 * (1.0 - ACC_Learning_MAPE[iteration - 1]);
                if (iteration % 250 == 0)
                {
                    for (int i = 0, k = train_data.GetLength(0); i < k; i++)
                    {
                        //file.Write("Error" + iteration.ToString() + "th:" + i.ToString() + "th data :" + Error_Learning[iteration - 1][i].ToString());
                        //file.WriteLine();
                    }
                    //file.Write("Mean Error" + iteration.ToString() + ":" + Error_Learning_Mean[iteration - 1] + System.Environment.NewLine);
                }

                iteration++;
                if ((iteration != 0) && (iteration > n_iterations))
                    break;
                //Console.Write(iteration.ToString());
                //Console.WriteLine();
            }



            //file.Close();


            double[] solution_test = new double[n_test_data];
            double[] networktestInput = new double[n_input];

            Error_Test = new double[n_test_data];
            Error_Test_Mean = 0.0;
            //file2.Write("Test Error");
            //file2.WriteLine();

            for (int i = 0, k = test_data.GetLength(0); i < k; i++)
            {
                // 학습된 네트워크에 집어 넣을 입력은 테스트용 입력 데이터
                for (int j = 0; j < n_input; j++)
                {
                    networktestInput[j] = test_data_input[i][j];
                }
                // 위의 입력으로 학습된 네트워크에 집어 넣어 얻은 솔루션(유량)
                solution_test[i] = (network.Compute(networktestInput)[0] + 0.85) / yFactor + yMin;
                // 네트워크의 솔루션과 테스트용 출력 데이터 간의 오차
                Error_Test[i] = Math.Abs(unnormalize_test_data_output[i][0] - solution_test[i]);
                Error_Test_Mean += Error_Test[i];
                ACC_Test_MAPE += Math.Abs(unnormalize_test_data_output[i][0] - solution_test[i]) / unnormalize_test_data_output[i][0];
                //file2.Write("Error" + i.ToString() + "th data :" + Error_Test[i].ToString());
                //file2.WriteLine();

            }
            // 테스트 오차 평균
            Error_Test_Mean /= n_test_data;
            // 테스트 정확도
            ACC_Test_MAPE = 100.0 * (1.0 - ACC_Test_MAPE / n_test_data);
            //file2.Write("Mean Error" + Error_Test_Mean.ToString() + System.Environment.NewLine);
            //file2.Write("Mean Per Error" + ACC_Test_MAPE.ToString() + System.Environment.NewLine);
            //file2.Close();
            learning_completed = true;
        }

        /// <summary>
        /// 학습된 네트워크와 수요 예측에 필요한 입력들을 이용하여 일 유량을 예측한다.
        /// </summary>
        /// 
        /// <remarks>Network_Learing 함수 또는 Load_Network 함수를 실행하여 network 정보를 먼저 불러온 뒤 시작해야한다.</remarks>
        /// 
        /// 
        public void Flow_Prediction()
        {
            double solution_prediction = 0;
            double[] networkpredictionInput = new double[n_input];

            //string strSaveFilePath3 = Application.StartupPath + "\\DF_data\\" + "prediction.txt";
            //StreamWriter file3 = new StreamWriter(strSaveFilePath3, false, System.Text.Encoding.ASCII);

            // 수요 예측 입력 데이터 정규화
            Data_Normalize(prediction_data);

            for (int j = 0; j < n_input; j++)
            {
                networkpredictionInput[j] = normalize_prediction_data[0, j];
            }

            // 수요 예측 입력 데이터를 학습된 네트워크에 집어 넣음
            solution_prediction = (network.Compute(networkpredictionInput)[0] + 0.85) / yFactor + yMin;

            // 얻어진 솔루션이 예측 수요량 
            Prediction_Result = solution_prediction;
            if (prediction_data.GetLength(0) == n_input + 1)
            {
                Error_Prediction = prediction_data[0, n_input + 1] - solution_prediction;
                //file3.Write("Error :" + Error_Prediction.ToString());
            }
            //file3.Close();
        }

        /// <summary>
        /// 학습된 네트워크와 수요 예측에 필요한 입력들을 이용하여 일 유량을 예측한다.
        /// </summary>
        /// 
        /// <param name="FilePath"> 네트워크 정보를 불러올 파일 이름 </param>
        /// 
        /// 
        /// 
        public void Load_Network(string FilePath)
        {
            DataSet NetworkData = new DataSet();

            NetworkData.ReadXml(FilePath, XmlReadMode.ReadSchema);

            DataTable NetworkInfo = new DataTable();
            NetworkInfo = NetworkData.Tables["NetworkInfo"];

            // 학습 계수
            f_learningRate = double.Parse(NetworkInfo.Rows[0]["LearningRate"].ToString());

            // 모멘텀 계수
            f_momentum = double.Parse(NetworkInfo.Rows[0]["Momentum"].ToString());

            // Sigmoid 함수의 기울기 값
            f_sigmoidAlphaValue = double.Parse(NetworkInfo.Rows[0]["AlphaForSigmoidFunction"].ToString());

            // 최대 반복 학습 시행 횟수
            n_iterations = int.Parse(NetworkInfo.Rows[0]["NumEpochs"].ToString());

            // 입력 변수 갯수
            n_input = int.Parse(NetworkInfo.Rows[0]["NumInputs"].ToString());

            // 레이어 갯수
            n_layer = int.Parse(NetworkInfo.Rows[0]["NumLayers"].ToString());

            // 레이어별 뉴런 갯수
            n_neuron = new int[n_layer];
            for (int i = 0; i < n_layer; i++)
            {
                n_neuron[i] = int.Parse(NetworkInfo.Rows[0]["NumNeuronsforLayer" + (i + 1).ToString()].ToString());
            }

            network = new ActivationNetwork(new BipolarSigmoidFunction(f_sigmoidAlphaValue),
               n_input, n_neuron[0], n_neuron[1]);
            network.n_Inputs = n_input;
            network.n_Layers = n_layer;

            for (int i = 0; i < n_layer; i++)
            {
                if (i == 0)
                {
                    network.layers[i].n_Inputs = network.n_Inputs;
                    network.layers[i].n_Neurons = n_neuron[i];
                }
                else
                {
                    network.layers[i].n_Inputs = network.layers[i - 1].n_Neurons;
                    network.layers[i].n_Neurons = n_neuron[i];
                }


            }

            for (int table_idx = 0; table_idx < network.n_Layers; table_idx++)
            {

                DataTable LayerData = new DataTable();
                LayerData = NetworkData.Tables["Layer_" + (table_idx + 1).ToString()];

                for (int i = 0; i < network.layers[table_idx].n_Neurons; i++)
                {
                    for (int j = 0; j < network.layers[table_idx].n_Inputs; j++)
                    {
                        network.layers[table_idx].neurons[i].f_weights[j] = double.Parse(LayerData.Rows[i]["Input_" + (j + 1).ToString()].ToString());
                        network.layers[table_idx].neurons[i].n_Inputs = network.layers[table_idx].n_Inputs;
                    }
                    network.layers[table_idx].neurons[i].f_output = double.Parse(LayerData.Rows[i]["Bias"].ToString());
                    network.layers[table_idx].f_Output[i] = network.layers[table_idx].neurons[i].f_output;
                }


            }
            network.f_Output = network.layers[n_layer - 1].f_Output;


        }

        /// <summary>
        ///  네트워크 정보를 xml파일로 저장한다.
        /// </summary>
        /// <param name="FilePath">네트워크 정보를 저장할 파일 이름</param>
        public void Save_Network(string FilePath)
        {
            DataRow temp_row;
            DataSet NetworkData = new DataSet();

            DataTable NetworkInfo = new DataTable("NetworkInfo");

            // 학습 계수
            NetworkInfo.Columns.Add("LearningRate", typeof(System.Double));

            // 모멘텀 계수
            NetworkInfo.Columns.Add("Momentum", typeof(System.Double));

            // Sigmoid 함수의 기울기 값
            NetworkInfo.Columns.Add("AlphaForSigmoidFunction", typeof(System.Double));

            // 최대 반복 시행 횟수
            NetworkInfo.Columns.Add("NumEpochs", typeof(System.Int32));

            // 레이어 개수
            NetworkInfo.Columns.Add("NumLayers", typeof(System.Int32));

            // 입력 개수
            NetworkInfo.Columns.Add("NumInputs", typeof(System.Int32));
            for (int i = 0; i < network.n_Layers; i++)
                NetworkInfo.Columns.Add("NumNeuronsforLayer" + (i + 1).ToString(), typeof(System.Int32));

            temp_row = NetworkInfo.NewRow();
            temp_row["LearningRate"] = f_learningRate;
            temp_row["Momentum"] = f_momentum;
            temp_row["AlphaForSigmoidFunction"] = f_sigmoidAlphaValue;
            temp_row["NumEpochs"] = n_iterations;
            temp_row["NumLayers"] = network.n_Layers;
            temp_row["NumInputs"] = network.n_Inputs;
            for (int i = 0; i < network.n_Layers; i++)
                temp_row["NumNeuronsforLayer" + (i + 1).ToString()] = network.layers[i].n_Neurons;
            NetworkInfo.Rows.Add(temp_row);

            NetworkData.Tables.Add(NetworkInfo);

            for (int table_idx = 0; table_idx < network.n_Layers; table_idx++)
            {
                DataTable LayerData = new DataTable("Layer_" + (table_idx + 1).ToString());

                int _nNumColumn = 0, _nNumRow = 0;
                if (table_idx == 0)
                {
                    _nNumColumn = network.n_Inputs + 1;
                    _nNumRow = network.layers[table_idx].n_Neurons;
                }
                else
                {
                    _nNumColumn = network.layers[table_idx - 1].n_Neurons + 1;
                    _nNumRow = network.layers[table_idx].n_Neurons;
                }

                for (int k = 0; k < _nNumColumn - 1; k++)
                {
                    LayerData.Columns.Add("Input_" + (k + 1).ToString(), typeof(System.Double));
                }
                LayerData.Columns.Add("Bias", typeof(System.Double));

                for (int j = 0; j < _nNumRow; j++)
                {
                    temp_row = LayerData.NewRow();
                    for (int i = 0; i < _nNumColumn - 1; i++)
                    {
                        temp_row["Input_" + (i + 1).ToString()] = network.layers[table_idx].neurons[j].f_weights[i];
                    }
                    temp_row["Bias"] = network.layers[table_idx].f_Output[j];
                    LayerData.Rows.Add(temp_row);
                }
                NetworkData.Tables.Add(LayerData);
            }

            NetworkData.WriteXml(FilePath, XmlWriteMode.WriteSchema);



        }

        /// <summary>
        ///  일 유량 예측 결과를 저장한다.
        /// </summary>
        public void Save_Prediction_Result()
        {
            //HSJ // DataRow temp_row;

            DataTable Prediction_Result = new DataTable("Prediction_Result");

            Prediction_Result.Columns.Add("DateTime", typeof(System.Int32));
            for (int i = 0; i < 10; i++)
            {
                Prediction_Result.Columns.Add("VH_0" + (i + 1).ToString(), typeof(System.Int32));
            }
            for (int i = 0; i < 24; i++)
            {
                Prediction_Result.Columns.Add("VH_" + (i + 1).ToString(), typeof(System.Int32));
            }

            Prediction_Result.Columns.Add(" ", typeof(System.Double));
            Prediction_Result.Columns.Add(" ", typeof(System.Double));
            Prediction_Result.Columns.Add(" ", typeof(System.Double));
        }

        // Sigmoid 함수 기울기 값 세팅
        public void Set_Param_Alpha(double _sigmoidaAlphaValue)
        {
            f_sigmoidAlphaValue = _sigmoidaAlphaValue;
        }
        // 학습 계수 세팅
        public void Set_Param_Learning_Rate(double _learningRate)
        {
            f_learningRate = _learningRate;
        }
        // 모멘텀 계수 세팅
        public void Set_Param_Momentum(double _momentum)
        {
            f_momentum = _momentum;
        }

        // 각 레이어의 뉴런 개수 세팅
        public void Set_Param_HiddenLayer_Neurons(int _neurons)
        {
            n_neuron = new int[2];
            n_neuron[0] = _neurons;
            n_neuron[1] = 1;
        }

        // 최대 반복 학습 시행 횟수 세팅
        public void Set_Param_Num_Epochs(int _iterations)
        {
            n_iterations = _iterations;
        }

        // 입력 변수 개수 세팅
        public void Set_Param_Num_Input(int _input)
        {
            n_input = _input;
        }

        // 입력 변수 별 최소치, 최대치 세팅
        public void Set_Param_MinMax_Input(double[] _minX, double[] _maxX)
        {

            for (int i = 0; i < n_input; i++)
            {
                xMin[i] = _minX[i] * 0.8;
                xMax[i] = _maxX[i] * 1.2;
                xFactor[i] = 2.0 / (xMax[i] - xMin[i]);
            }


        }

        // 출력 변수 최소치, 최대치 세팅
        public void Set_Param_MinMax_output(double _minY, double _maxY)
        {
            yMin = _minY * 0.8;
            yMax = _maxY * 1.2;
            yFactor = 1.7 / (yMax - yMin);

        }

        // 입력 데이터 세팅
        public void Set_Learning_Data(double[,] _data)
        {
            data = _data;
        }

        // 데이터 정규화
        public void Set_Normalize_Data()
        {
            Data_Normalize(data);
        }

        // 수요 예측용 입력 데이터 세팅
        public void Set_Prediction_Data(double[,] _prediction_data)
        {
            prediction_data = _prediction_data;
        }

    }
        
}
