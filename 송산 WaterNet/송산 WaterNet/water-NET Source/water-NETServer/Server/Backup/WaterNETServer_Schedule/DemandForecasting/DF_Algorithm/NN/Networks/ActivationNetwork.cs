﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNETServer.DF_Algorithm.NN_Core
{
    /// <summary>
    /// 활성 네트워크
    /// </summary>
    /// 
    /// <remarks>활성 네트워크는 활성 함수를 기진 다중 레이어 신경망 회로이다.
    /// <see cref="ActivationLayer">활성 레이어</see>.</remarks>
    ///
    public class ActivationNetwork : Network
    {
        // public ActivationLayer[] layers;
        /// <summary>
        /// 네트워크의 레이어 접근자
        /// </summary>
        /// 
        /// <param name="index">레이어 인덱스</param>
        /// 

        //HSJ// public new ActivationLayer this[int index]
        public ActivationLayer this[int index]
        {
            get { return ((ActivationLayer)layers[index]); }
        }
        /// <summary>
        /// <see cref="ActivationNetwork"/> 클래스의 객체 초기화하기
        /// </summary>
        /// <param name="function">네트워크의 뉴런의 활성 함수</param>
        /// <param name="inputsCount">네트워크의 입력 수</param>
        /// <param name="neuronsCount">네트워크의 각 레이어의 뉴런 개수를 저장하는 배열</param>
        /// 
        /// <remarks>네트워크는 생성된 뒤, 랜덤화 된다.(see <see cref="ActivationNeuron.Randomize"/>
        /// method)</remarks>
        /// 
        /// <example>ActivationNetwork 예제 <c>ActivationNetwork</c> class:
        /// <code>
        ///		// 활성 네트워크 생성
        ///		ActivationNetwork network = new ActivationNetwork(
        ///			new SigmoidFunction( ), // sigmoid activation function
        ///			3,                      // 3 inputs
        ///			4, 1 );                 // 2 layers:
        ///                                 // 4 neurons in the first layer
        ///                                 // 1 neuron in the second layer
        ///	</code>
        /// </example>
        /// 
        public ActivationNetwork(IActivationFunction function, int n_Inputs, params int[] n_Neurons)
            : base(n_Inputs, n_Neurons.Length)
        {
            // 각 레이어 생성
            for (int i = 0; i < n_Layers; i++)
            {
                layers[i] = new ActivationLayer(
                    //레이어의 뉴런 수
                    n_Neurons[i],
                    // 레이어의 입력 수
                    (i == 0) ? n_Inputs : n_Neurons[i - 1],
                    // 레이어의 활성 함수
                    function);
            }
        }

    }
}
