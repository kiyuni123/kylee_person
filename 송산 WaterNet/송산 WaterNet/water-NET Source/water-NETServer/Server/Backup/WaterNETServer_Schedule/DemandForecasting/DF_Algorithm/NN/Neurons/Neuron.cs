﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNETServer.DF_Algorithm.NN_Core
{
    /// <summary>
    /// 기본 뉴런 클래스
    /// </summary>
    /// 
    /// <remarks> 기본 뉴런 클래스로 뉴런의 입력, 출력, 계수들로 구성</remarks>
    /// 
    public abstract class Neuron
    {
        /// <summary>
        /// 뉴런 입력 수
        /// </summary>
        public int n_Inputs = 0;

        /// <summary>
        /// 뉴런 계수 
        /// </summary>
        public double[] f_weights = null;

        /// <summary>
        /// 뉴런 출력
        /// </summary>
        public double f_output = 0;

        /// <summary>
        /// 랜덤 숫자 생성
        /// </summary>
        public static Random rand = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// 랜덤 숫자 생성 최소값
        /// </summary>
        public double f_minRange = 0.0;

        /// <summary>
        /// 랜덤 숫자 생성 최대값
        /// </summary>
        public double f_maxRange = 1.0;

        /// <summary>
        /// 객체 초기화
        /// </summary>
        public Neuron(int inputs)
        {
            // 계수 할당
            n_Inputs = Math.Max(1, inputs);
            f_weights = new double[n_Inputs];

            // 뉴런 랜덤 생성하기
            Randomize();
        }

        public double this[int index]
        {
            get { return f_weights[index]; }
            set { f_weights[index] = value; }
        }

        public virtual void Randomize()
        {
            double d = f_maxRange - f_minRange;
            // 계수 랜덤 생성
            for (int i = 0; i < n_Inputs; i++)
            {
                f_weights[i] = rand.NextDouble() * d + f_minRange;
            }


        }

        ///// <summary>
        ///// threshold 값
        ///// </summary>
        //public double f_threshold = 0.0f;

        /// <summary>
        /// 뉴런의 활동 함수
        /// </summary>
        /// 
        /// <remarks> 이 함수는 weighted sum과 threshold 값의 합에 대해 인가된다. </remarks>
        //public |ActivationFunction fct_function = null;
        public abstract double Compute(double[] input);


    }
}
