﻿using System;
using WaterNETServer.DF_Simulation.dao;
using System.Data;
using System.Collections;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.work;
using EMFrame.log;

namespace WaterNETServer.DF_Simulation.work
{
    public class BlockWork : BaseWork
    {
        private static BlockWork work = null;
        private BlockDao dao = null;

        public static BlockWork GetInstance()
        {
            if (work == null)
            {
                work = new BlockWork();
            }
            return work;
        }

        private BlockWork()
        {
            dao = BlockDao.GetInstance();
        }

        //특정 지역의 태그종류별 태그를 조회한다.
        //면단위 지역의 소블록의 경우 순시유량, 적산, 압력등의 태그는 해당 블록 분기태그를 사용해야한다.
        public string SelectBlockTag(string loc_code, string ftr_cde, string tag_gbn)
        {
            string result = string.Empty;
            string relLocGbn = string.Empty;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                //중블록일 경우 배수지 유무에 따라서 조회 태그가 달라진다..
                if (ftr_cde == "BZ002")
                {
                    result = Convert.ToString(dao.SelectMiddleBlockTag(manager, loc_code, tag_gbn));
                }
                //소블록일 경우 해당 중블록의 배수지 유무에 따라서 조회 태그가 달라진다..
                if (ftr_cde == "BZ003")
                {
                    result = Convert.ToString(dao.SelectSmallBlockTag(manager, loc_code, tag_gbn));
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        public bool HasReservoir(string loc_code)
        {
            bool result = false;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = Convert.ToBoolean(dao.HasReservoir(manager, loc_code));

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        public DataSet SelectCenter(Hashtable parameter, string dataMember)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectCenter(manager, parameter, dataMember);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        public DataSet SelectJungSuJang(Hashtable parameter, string dataMember)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectJungSuJang(manager, parameter, dataMember);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        public DataSet SelectBlockListByLevel(Hashtable parameter, string dataMember)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectBlockListByLevel(manager, parameter, dataMember);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        public DataSet SelectSmallBlock(Hashtable parameter, string dataMember)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectSmallBlock(manager, parameter, dataMember);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        public DataSet SelectBlockInfoByBlockCode(Hashtable parameter, string dataMember)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectBlockInfoByBlockCode(manager, parameter, dataMember);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }
    }
}
