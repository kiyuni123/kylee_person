﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using System.Threading;
using WaterNETServer.DF_Simulation.work;
using WaterNETServer.DF_Simulation.control;
using WaterNETServer.DF_Simulation.data;
using WaterNETServer.DF_Simulation.util;
using System.Collections.Generic;
using WaterNETServer.Common.utils;
using WaterNETServer.DF_Simulation.enum1;
using EMFrame.form;
using EMFrame.log;

namespace WaterNETServer.DF_Simulation.form
{
    public partial class frmMain : BaseForm, IForminterface
    {
        private BlockComboboxControl blockControl = null;

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeGridColumn();
            this.InitializeValueList();
            this.InitializeEvent();
            this.InitializeForm();
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.InitializeGrid(this.ultraGrid1);
            this.InitializeGrid(this.ultraGrid2);
            this.InitializeGrid(this.ultraGrid3);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid3.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid1.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;

            this.ultraGrid1.DisplayLayout.Override.RowSizing = RowSizing.Fixed;
            this.ultraGrid1.DisplayLayout.Override.RowSizingArea = RowSizingArea.RowBordersOnly;

            this.ultraGrid1.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;
        }

        #region 그리드 스타일 설정

        private void InitializeGrid(UltraGrid item)
        {
            item.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            item.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            item.DisplayLayout.GroupByBox.BorderStyle = UIElementBorderStyle.Solid;
            item.DisplayLayout.GroupByBox.Hidden = true;
            item.DisplayLayout.MaxColScrollRegions = 1;
            item.DisplayLayout.MaxRowScrollRegions = 1;

            item.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Solid;
            item.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Solid;
            item.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
            item.DisplayLayout.Override.CellPadding = 0;
            item.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            item.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;
            item.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            item.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            item.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            item.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            item.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            item.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;

            item.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            item.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;
            item.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.Default;

            //DSOH 추가. Row Select는 무조건 Single만 되고, Drag가 되지 않게.
            item.DisplayLayout.Override.SelectTypeRow = SelectType.SingleAutoDrag;

            item.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;
            item.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            item.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            item.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            item.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            item.Font = new Font("굴림", 9f, FontStyle.Regular, GraphicsUnit.Point, 129);

            for (int I = 0; I <= item.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                item.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                item.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                item.DisplayLayout.Bands[0].Columns[I].Header.Appearance.BackGradientStyle = GradientStyle.None;
                item.DisplayLayout.Bands[0].Columns[I].CellActivation = Activation.NoEdit;
            }

            item.DisplayLayout.Appearance.BorderColor = SystemColors.InactiveCaption;
            item.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.ColumnChooserButton;
            item.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            item.Font = new Font("굴림", 9f);

            foreach (UltraGridColumn gridColumn in item.DisplayLayout.Bands[0].Columns)
            {
                gridColumn.Header.Appearance.TextVAlign = VAlign.Middle;
                gridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            }

            item.Select();
        }

        #endregion

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            #region GRID1 컬럼추가 내용

            UltraGridColumn column;

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_CODE") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_CODE";
                column.Header.Caption = "지역관리코드";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 90;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 0;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LBLOCK";
                column.Header.Caption = "대블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 1;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("MBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "MBLOCK";
                column.Header.Caption = "중블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 2;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("SBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "SBLOCK";
                column.Header.Caption = "소블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 3;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("OUT_FLOW_TAG") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "OUT_FLOW_TAG";
                column.Header.Caption = "유량시간적산차태그";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 250;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 4;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "USE_YN";
                column.Header.Caption = "수요예측\n실행여부";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.Edit;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 5;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            #endregion

            #region GRID2 컬럼추가 내용
            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ATTRIBUTE";
            column.Header.Caption = "Attribute";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.CellSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.Hidden = false;

            column = this.ultraGrid2.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "VALUE";
            column.Header.Caption = "Order";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.EditAndSelectText;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 80;
            column.Hidden = false;
            #endregion

            #region GRID3 컬럼추가 내용
            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TIME";
            column.Header.Caption = "시간";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.CellSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 60;
            column.Hidden = false;

            column = this.ultraGrid3.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "ALPHA";
            column.Header.Caption = "Alpha";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.EditAndSelectText;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;

            #endregion
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            #region 코드 설정
            ValueList valueList = null;

            //실행여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("USE_YN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("USE_YN");

                valueList.ValueListItems.Add("Y", "Y");
                valueList.ValueListItems.Add("N", "N");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["USE_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["USE_YN"];
            }

            //배수지, 블록 구분
            ComboBoxUtils.SetMember(this.gubun, "CODE", "CODE_NAME");
            DataTable gubun = new DataTable();
            gubun.Columns.Add("CODE");
            gubun.Columns.Add("CODE_NAME");
            gubun.Rows.Add("1", "블록");
            gubun.Rows.Add("2", "배수지");
            this.gubun.DataSource = gubun;

            Hashtable parameter = new Hashtable();

            //5001 코드
            this.studymethod.ValueMember = "CODE";
            this.studymethod.DisplayMember = "CODE_NAME";
            parameter["PCODE"] = "5001";
            this.studymethod.DataSource = CodeWork.GetInstance().SelectCodeList(parameter);

            //5002 코드
            this.abnormaldata.ValueMember = "CODE";
            this.abnormaldata.DisplayMember = "CODE_NAME";
            parameter["PCODE"] = "5002";
            this.abnormaldata.DataSource = CodeWork.GetInstance().SelectCodeList(parameter);

            //5003 코드
            this.timeforecasting.ValueMember = "CODE";
            this.timeforecasting.DisplayMember = "CODE_NAME";
            parameter["PCODE"] = "5003";
            this.timeforecasting.DataSource = CodeWork.GetInstance().SelectCodeList(parameter);

            #endregion
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.Disposed += new EventHandler(frmMain_Disposed);

            this.useupdateBtn.Click += new EventHandler(useupdateBtn_Click);
            this.checkBox1.CheckedChanged += new EventHandler(checkBox1_CheckedChanged);

            this.logicBtn.Click += new EventHandler(logicBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);

            this.studypattern.TextChanged += new EventHandler(studypattern_TextChanged);

            //수요예측자동실행
            this.autoRun.Click += new EventHandler(autoRun_Click);
        }

        private void frmMain_Disposed(object sender, EventArgs e)
        {
            try
            {
                if (this.currentThread.ContainsKey("currentThread"))
                {
                    ((Thread)(currentThread["currentThread"])).Abort();
                }
                if (this.currentThread.ContainsKey("manualThread"))
                {
                    ((Thread)(currentThread["manualThread"])).Abort();
                }
            }
            catch (ThreadAbortException ex)
            {
                Logger.Error("수요예측 프로세스가 정상적으로 종료되었습니다.");
            }
        }

        private void useupdateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                DfSimulationWork.GetInstance().UpdateDfSimulationUse(this.ultraGrid1);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        //자동실행여부
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (UltraGridRow row in this.ultraGrid1.Rows)
                {
                    row.Hidden = false;
                    if (this.checkBox1.Checked && row.Cells["USE_YN"].Value.ToString() == "N")
                    {
                        row.Hidden = true;
                    }
                }
                if (this.ultraGrid1.Rows.Count > 0)
                {
                    this.ultraGrid1.ActiveRowScrollRegion.ScrollRowIntoView(this.ultraGrid1.Rows[0]);
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        //수요에측재실행 버튼 이벤트 핸들러
        private void logicBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.parameter == null)
                {
                    return;
                }

                bool IsSelected = false;

                //그리드 컬럼이 bound 인지..
                foreach (UltraGridColumn column in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
                {
                    if (!column.IsBound)
                    {
                        foreach (UltraGridCell cell in this.ultraGrid1.Selected.Cells)
                        {
                            if (cell.Column.Equals(column))
                            {
                                IsSelected = true;
                            }
                        }
                    }
                }

                if (!IsSelected)
                {
                    if (this.parameter["GUBUN"].ToString() == "1")
                    {
                        MessageBox.Show("블록별 재실행 일자가 선택되어 있지 않습니다.");
                    }
                    if (this.parameter["GUBUN"].ToString() == "2")
                    {
                        MessageBox.Show("배수지별 재실행 일자가 선택되어 있지 않습니다.");
                    }
                    return;
                }
                else
                {
                    Thread manualThread = new Thread(new ThreadStart(UpdateDfSimulation));
                    this.currentThread["manualThread"] = manualThread;
                    manualThread.Start();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private bool IsManualRun = false;
        private void UpdateDfSimulation()
        {
            try
            {
                if (this.IsRun)
                {
                    DialogResult qe = MessageBox.Show("현재 수요예측 자동 프로세스가 가동중입니다.\n수요예측 재실행 로그를 남길수 없습니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (qe == DialogResult.No)
                    {
                        return;
                    }
                }

                this.IsManualRun = true;
                MessageBox.Show("수요예측 재실행 프로세스를 실행합니다.");

                if (!this.IsRun)
                {
                    this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.AppendText("\n\n수요예측 재실행 프로세스를 실행합니다.\n"); });
                    this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.ScrollToCaret(); });
                }

                foreach (UltraGridColumn column in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
                {
                    DfControl con = new DfControl();

                    if (!this.IsRun)
                    {
                        con.TexDfLog = this.texDfLog;
                    }

                    if (!column.IsBound)
                    {
                        DateTime targetDate = DateTime.ParseExact(column.Key, "yyyyMMdd", null);

                        foreach (UltraGridCell cell in this.ultraGrid1.Selected.Cells)
                        {
                            if (cell.Column.Equals(column))
                            {
                                if (this.parameter["GUBUN"].ToString() == "1")
                                {
                                    con.AddItem(new DfDataBlock(cell.Row.Cells["LOC_CODE"].Value.ToString()));
                                }
                                if (this.parameter["GUBUN"].ToString() == "2")
                                {
                                    con.AddItem(new DfDataReservoir(cell.Row.Cells["LOC_CODE"].Value.ToString()));
                                }
                            }
                        }

                        con.TargetDate = targetDate;
                        con.Predict_All();
                        con.UpdateDayPrediction();
                        con.UpdateHourPrediction_AllHour();
                    }
                }

                if (!this.IsRun)
                {
                    this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.AppendText("\n\n수요예측 재실행 프로세스를 종료합니다.\n"); });
                    this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.ScrollToCaret(); });
                }

                MessageBox.Show("수요예측 재실행 프로세스를 종료합니다.");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                Logger.Error(ex.StackTrace);
                MessageBox.Show("수요예측 재실행 프로세스를 실행하는 도중 예기치 않은 오류가 발생했습니다.\n시스템 로그를 참고해주세요.");
            }
            finally
            {
                this.IsManualRun = false;
                this.searchBtn.Invoke((MethodInvoker)delegate { this.searchBtn.PerformClick(); });
            }
        }

        private Hashtable parameter = null;

        //검색 버튼 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.parameter = Utils.ConverToHashtable(this.groupBox1);

                DataRowView selectedItem = null;

                if (this.sblock != null && this.sblock.SelectedIndex > 0)
                {
                    selectedItem = this.sblock.SelectedItem as DataRowView;
                }
                else if (this.mblock != null && this.mblock.SelectedIndex > 0)
                {
                    selectedItem = this.mblock.SelectedItem as DataRowView;
                }
                else
                {
                    selectedItem = this.lblock.SelectedItem as DataRowView;
                }

                if (selectedItem != null)
                {
                    this.parameter["LOC_CODE"] = selectedItem["LOC_CODE"];
                    this.parameter["FTR_IDN"] = selectedItem["FTR_IDN"];
                    this.parameter["FTR_CODE"] = selectedItem["FTR_CODE"];
                    this.parameter["LOC_NAME"] = selectedItem["LOC_NAME"];
                } 

                this.SelectDfSimulation();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        //수요예측관리 검색
        private void SelectDfSimulation()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ultraGrid1.DataSource = null;

                if (this.gubun.SelectedValue.ToString() == "1")
                {
                    this.InitializeGrid();
                    this.InitializeBlockGridColumn();
                    this.InitializeBlockValueList();

                    DataSet dataSet = new DataSet();
                    DataTable temp_table = DfSimulationWork.GetInstance().SelectDfSimulationBlock(this.parameter);

                    if (temp_table == null)
                    {
                        return;
                    }

                    DataTable result = new DataTable();

                    foreach (DataColumn column in temp_table.Columns)
                    {
                        result.Columns.Add(column.ColumnName, column.DataType);
                    }

                    foreach (DataRow row in temp_table.Rows)
                    {
                        result.Rows.Add(row.ItemArray);
                    }

                    DataTable dataTable = result.Copy();

                    dataSet.Tables.Add(dataTable);
                    dataSet.Tables.Add(result);

                    //유량계 정보 제외 나머지 컬럼은 삭제한다.
                    for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                    {
                        if (dataTable.Columns[i].ColumnName != "LOC_CODE" &&
                            dataTable.Columns[i].ColumnName != "LBLOCK" &&
                            dataTable.Columns[i].ColumnName != "MBLOCK" &&
                            dataTable.Columns[i].ColumnName != "SBLOCK" &&
                            dataTable.Columns[i].ColumnName != "OUT_FLOW_TAG" &&
                            dataTable.Columns[i].ColumnName != "USE_YN"
                            )
                        {
                            dataTable.Columns.Remove(dataTable.Columns[i]);
                        }
                    }

                    this.ultraGrid1.DataSource = dataSet;
                    this.InitGrid();
                    this.SetGridColumns();
                    this.SetGridData();
                }
                else if (this.gubun.SelectedValue.ToString() == "2")
                {
                    this.InitializeGrid();
                    this.InitializeReservoirGridColumn();
                    this.InitializeReservoirValueList();

                    DataSet dataSet = new DataSet();
                    DataTable temp_table = DfSimulationWork.GetInstance().SelectDfSimulationReservoir(this.parameter);

                    if (temp_table == null)
                    {
                        return;
                    }

                    DataTable result = new DataTable();

                    foreach (DataColumn column in temp_table.Columns)
                    {
                        result.Columns.Add(column.ColumnName, column.DataType);
                    }

                    foreach (DataRow row in temp_table.Rows)
                    {
                        result.Rows.Add(row.ItemArray);
                    }

                    DataTable dataTable = result.Copy();

                    dataSet.Tables.Add(dataTable);
                    dataSet.Tables.Add(result);

                    //배수지 정보 제외 나머지 컬럼은 삭제한다.
                    for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                    {
                        if (dataTable.Columns[i].ColumnName != "LOC_CODE" &&
                            dataTable.Columns[i].ColumnName != "FTR_IDN" &&
                            dataTable.Columns[i].ColumnName != "LOC_NAME" &&
                            dataTable.Columns[i].ColumnName != "OUT_FLOW_TAG" &&
                            dataTable.Columns[i].ColumnName != "USE_YN"
                            )
                        {
                            dataTable.Columns.Remove(dataTable.Columns[i]);
                        }
                    }

                    this.ultraGrid1.DataSource = dataSet;
                    this.InitGrid();
                    this.SetGridColumns();
                    this.SetGridData();
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void InitializeBlockGridColumn()
        {
            UltraGridColumn column;

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_CODE") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_CODE";
                column.Header.Caption = "지역관리코드";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 90;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 0;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LBLOCK";
                column.Header.Caption = "대블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 1;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("MBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "MBLOCK";
                column.Header.Caption = "중블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 2;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("SBLOCK") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "SBLOCK";
                column.Header.Caption = "소블록";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 3;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("OUT_FLOW_TAG") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "OUT_FLOW_TAG";
                column.Header.Caption = "유량시간적산차태그";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 250;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 4;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "USE_YN";
                column.Header.Caption = "수요예측\n실행여부";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.Edit;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 5;
                column.RowLayoutColumnInfo.OriginY = 0;
            }
        }

        private void InitializeBlockValueList()
        {
            this.ultraGrid1.DisplayLayout.ValueLists.Clear();
            ValueList valueList = null;

            //실행여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("USE_YN") &&
                this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") != -1)
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("USE_YN");

                valueList.ValueListItems.Add("Y", "Y");
                valueList.ValueListItems.Add("N", "N");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["USE_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["USE_YN"];
            }
        }

        private void InitializeReservoirGridColumn()
        {
            UltraGridColumn column;

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_CODE") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_CODE";
                column.Header.Caption = "지역관리코드";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 90;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 0;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("FTR_IDN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "FTR_IDN";
                column.Header.Caption = "배수지관리번호";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 100;
                column.Hidden = true;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 1;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("LOC_NAME") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "LOC_NAME";
                column.Header.Caption = "배수지명";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 130;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 2;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("OUT_FLOW_TAG") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "OUT_FLOW_TAG";
                column.Header.Caption = "유량시간적산차태그";
                column.CellActivation = Activation.NoEdit;
                column.CellClickAction = CellClickAction.RowSelect;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                column.CellAppearance.TextHAlign = HAlign.Left;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 250;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 3;
                column.RowLayoutColumnInfo.OriginY = 0;
            }

            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") == -1)
            {
                column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                column.Key = "USE_YN";
                column.Header.Caption = "수요예측\n실행여부";
                column.CellActivation = Activation.AllowEdit;
                column.CellClickAction = CellClickAction.Edit;
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                column.CellAppearance.TextHAlign = HAlign.Center;
                column.CellAppearance.TextVAlign = VAlign.Middle;
                column.Width = 70;
                column.Hidden = false;
                column.RowLayoutColumnInfo.SpanY = 2;
                column.RowLayoutColumnInfo.SpanX = 1;
                column.RowLayoutColumnInfo.OriginX = 4;
                column.RowLayoutColumnInfo.OriginY = 0;
            }
        }

        private void InitializeReservoirValueList()
        {
            this.ultraGrid1.DisplayLayout.ValueLists.Clear();
            ValueList valueList = null;

            //실행여부
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("USE_YN") &&
                this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("USE_YN") != -1)
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("USE_YN");

                valueList.ValueListItems.Add("Y", "Y");
                valueList.ValueListItems.Add("N", "N");

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["USE_YN"].ValueList =
                    this.ultraGrid1.DisplayLayout.ValueLists["USE_YN"];
            }
        }

        //수요예측설정 검색
        private void SelectDfSetting()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //환경설정영역을 초기화한다.
                Utils.ClearControls(this.setting);
                Utils.ClearControls(this.grpNN);

                Hashtable parameter = new Hashtable();
                parameter["LOC_CODE"] = this.key;

                //기본설정값조회
                Utils.SetControlDataSetting(DfSimulationWork.GetInstance().SelectDfSimulationSetting(parameter), this.setting);

                //NN설정값조회
                Utils.SetControlDataSetting(DfSimulationWork.GetInstance().SelectDfSimulationSettingNN(parameter), this.grpNN);

                //MRM설정값조회
                DataTable mrm = DfSimulationWork.GetInstance().SelectDfSimulationSettingMRM(parameter);

                if (mrm == null)
                {
                    mrm = new DataTable();
                    mrm.Columns.Add("ATTRIBUTE");
                    mrm.Columns.Add("VALUE");
                }

                int sp = Convert.ToInt32(this.studypattern.Text.Trim());

                if (mrm.Rows.Count < sp)
                {
                    for (int i = 1; i <= sp; i++)
                    {
                        mrm.Rows.Add(i.ToString(), DfSetting.DefaultSettingMRM);
                    }
                }

                this.ultraGrid2.DataSource = mrm;

                //이상치조회
                DataTable outlier = DfSimulationWork.GetInstance().SelectDfSimulationSettingOutlier(parameter);

                if (outlier == null)
                {
                    outlier = new DataTable();
                    outlier.Columns.Add("TIME");
                    outlier.Columns.Add("ALPHA");
                }

                if (outlier.Rows.Count == 0)
                {
                    for (int i = 1; i <= 24; i++)
                    {
                        outlier.Rows.Add(i.ToString(), DfSetting.DefaultSettingOutlier);
                    }
                }
                this.ultraGrid3.DataSource = outlier;

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        /// <summary>
        /// 동적으로 생성한 그룹을 삭제한다.
        /// </summary>
        private void InitGrid()
        {
            try
            {
                for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Count; i > 0; i--)
                {
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups.Remove(i - 1);
                }

                for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count; i > 0; i--)
                {
                    if (!this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].IsBound)
                    {
                        this.ultraGrid1.DisplayLayout.Bands[0].Columns.Remove(i - 1);
                    }
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
                Console.WriteLine(e1.StackTrace);
                logger.Error(e1.ToString());
            }
        }

        /// <summary>
        /// 그리드내 그룹 컬럼을 설정한다.
        /// </summary>
        private void SetGridColumns()
        {
            try
            {
                UltraGridColumn column = null;
                UltraGridGroup group = null;

                UltraGridLayout layout = this.ultraGrid1.DisplayLayout;

                DateTime st = DateTime.ParseExact(this.parameter["STARTDATE"].ToString(), "yyyyMMdd", null);
                DateTime et = DateTime.ParseExact(this.parameter["ENDDATE"].ToString(), "yyyyMMdd", null);

                int diff = (et - st).Days;

                for (; st <= et; st = st.AddDays(1))
                {
                    if (layout.Bands[0].Groups.IndexOf((st.Year + st.Month).ToString()) == -1)
                    {
                        group = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                        group.Key = (st.Year + st.Month).ToString();
                        group.Header.Caption = st.Year.ToString() + "년 " + st.Month + "월";
                        group.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        group.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        group.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
                        group.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                        group.RowLayoutGroupInfo.LabelSpan = 1;
                        group.RowLayoutGroupInfo.OriginX = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count;
                        group.RowLayoutGroupInfo.OriginY = 0;
                    }

                    column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
                    column.Key = st.ToString("yyyyMMdd");
                    column.Header.Caption = st.ToString("dd") + "일";
                    column.CellActivation = Activation.AllowEdit;
                    column.CellClickAction = CellClickAction.CellSelect;
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
                    column.CellAppearance.TextHAlign = HAlign.Right;
                    column.CellAppearance.TextVAlign = VAlign.Middle;
                    column.Width = 45;
                    column.Hidden = false;
                    column.RowLayoutColumnInfo.SpanX = 1;
                    column.RowLayoutColumnInfo.SpanY = 1;
                    column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
                    column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
                    column.RowLayoutColumnInfo.ParentGroup = group;
                    column.CellAppearance.BorderColor = Color.White;
                    column.CellAppearance.BorderColor2 = Color.White;
                    column.CellAppearance.BorderColor3DBase = Color.White;
                    column.CellAppearance.ForeColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        /// <summary>
        /// 그리드내 데이터 컬럼을 설정한다.
        /// </summary>
        private void SetGridData()
        {
            try
            {
                DataTable dataTable = ((DataSet)this.ultraGrid1.DataSource).Tables[1];

                foreach (DataRow row in dataTable.Rows)
                {
                    //유량계ID를 기준으로 첫 행을 찾는다.
                    int rowIndex = 0;

                    foreach (UltraGridRow grow in this.ultraGrid1.Rows)
                    {
                        if (grow.Cells["LOC_CODE"].Value.ToString() == row["LOC_CODE"].ToString())
                        {
                            rowIndex = grow.Index + 1;
                            break;
                        }
                    }

                    foreach (UltraGridColumn gcolumn in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
                    {
                        if (!gcolumn.IsBound)
                        {
                            if (row[gcolumn.Key].ToString() == "T")
                            {
                                this.ultraGrid1.Rows[dataTable.Rows.IndexOf(row)].Cells[gcolumn.Key].Appearance.BackColor = Color.Blue;
                            }

                            if (row[gcolumn.Key].ToString() == "F")
                            {
                                this.ultraGrid1.Rows[dataTable.Rows.IndexOf(row)].Cells[gcolumn.Key].Appearance.BackColor = Color.Red;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        //환경설정 저장
        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.key == null || this.key == string.Empty)
                {
                    return;
                }
                this.UpdateDfSimulationSetting();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void UpdateDfSimulationSetting()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = new Hashtable();
                parameter["LOC_CODE"] = this.key;

                Hashtable setting = Utils.ConverToHashtable(this.setting);
                Hashtable nn = Utils.ConverToHashtable(this.grpNN);

                DfSimulationWork.GetInstance().UpdateDfSimulationSetting(parameter, setting, nn, this.ultraGrid2, this.ultraGrid3);

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private bool isAfterSelectChange = false;
        private string key = string.Empty;

        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                if (this.isAfterSelectChange)
                {
                    return;
                }

                this.isAfterSelectChange = true;

                if (e.Type.ToString() == "Infragistics.Win.UltraWinGrid.UltraGridRow")
                {
                    this.ultraGrid1.Selected.Cells.Clear();
                    UltraGridRow row = null;
                    if (this.ultraGrid1.Selected.Rows.Count > 0)
                    {
                        row = this.ultraGrid1.Selected.Rows[this.ultraGrid1.Selected.Rows.Count - 1];
                    }

                    this.ultraGrid1.Selected.Rows.Clear();

                    if (row != null)
                    {
                        row.Selected = true;
                    }

                    if (this.ultraGrid1.Selected.Rows.Count > 0)
                    {
                        this.key = this.ultraGrid1.Selected.Rows[0].Cells["LOC_CODE"].Value.ToString();
                        this.SelectDfSetting();
                    }
                }

                if (e.Type.ToString() == "Infragistics.Win.UltraWinGrid.UltraGridCell")
                {
                    foreach (UltraGridCell cell in this.ultraGrid1.Selected.Cells)
                    {
                        if (cell.Column.Key == "USE_YN")
                        {
                            cell.Selected = false;
                        }
                    }
                }

                this.isAfterSelectChange = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            try
            {
                foreach (UltraGridRow gridRow in e.Layout.Rows)
                {
                    if (this.checkBox1.Checked)
                    {
                        if (gridRow.Cells["USE_YN"].Value.ToString() == "N")
                        {
                            gridRow.Hidden = true;
                        }
                    }

                    gridRow.Height = 25;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void studypattern_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.ultraGrid2.DataSource == null)
                {
                    return;
                }

                for (int i = this.ultraGrid2.Rows.Count - 1; i >= 0; i--)
                {
                    this.ultraGrid2.Rows[i].Delete(false);
                }

                if (this.studypattern.Text == string.Empty)
                {
                    return;
                }

                for (int i = 1; i <= Convert.ToInt32(this.studypattern.Text.Trim()); i++)
                {
                    UltraGridRow row = this.ultraGrid2.DisplayLayout.Bands[0].AddNew();
                    row.Cells["ATTRIBUTE"].Value = i.ToString();
                    row.Cells["VALUE"].Value = DfSetting.DefaultSettingMRM;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            try
            {
                this.blockControl = new BlockComboboxControl(this.lblock, this.mblock, this.sblock);
                this.blockControl.AddDefaultOption(LOCATION_TYPE.All, false);

                this.startdate.Value = DateTime.Now.AddDays(-6);
                this.enddate.Value = DateTime.Now;

                this.studyday.MaxLength = 3;
                this.studypattern.MaxLength = 3;

                //폼이 로드되면 스레드 가동.
                Thread thread = new Thread(AutoDemandForecasting);
                thread.Start(DateTime.Now);
                this.currentThread = new Hashtable();
                this.currentThread["currentThread"] = thread;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private bool IsRun = false;
        private DateTime Now = DateTime.MinValue;
        private DfControl Df = new DfControl();

        private void autoRun_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.IsRun)
                {
                    DialogResult qe = MessageBox.Show("현재 수요예측 자동실행 프로세스가 실행중입니다.\n실행을 중지하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (qe == DialogResult.Yes)
                    {
                        this.autoRun.Image = Common.Properties.Resources.NoPopup;
                        this.IsRun = false;
                        this.timer.Tick -= timer_Tick;
                        this.timer.Stop();
                        this.Now = DateTime.MinValue;
                        return;
                    }
                }

                if (!this.IsRun)
                {
                    if (this.IsManualRun)
                    {
                        MessageBox.Show("현재 수요예측 재실행 프로세스가 가동중입니다.\n재실행 프로세스 종료후에 시도해주세요.");
                        return;
                    }

                    MessageBox.Show("수요예측 자동실행 프로세스를 실행합니다.");
                    this.autoRun.Image = Common.Properties.Resources.OkPopup;
                    this.IsRun = true;
                    this.timer.Tick += new EventHandler(timer_Tick);
                    this.timer.Interval = 60000;
                    this.timer.Start();

                    this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.AppendText("\n\n수요예측 자동실행 프로세스를 실행합니다.\n"); });
                    this.texDfLog.Invoke((MethodInvoker)delegate { this.texDfLog.ScrollToCaret(); });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private IList<DateTime> workItems = new List<DateTime>();

        //[StructLayout(LayoutKind.Sequential)]
        //public struct SYSTEMTIME
        //{
        //    public short Year;
        //    public short Month;
        //    public short DayOfWeek;
        //    public short Day;
        //    public short Hour;
        //    public short Minute;
        //    public short Second;
        //    public short Milliseconds;
        //}

        //[DllImport("kernel32.dll")]
        //public extern static uint SetSystemTime(ref SYSTEMTIME lpSystemTime);

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                //DateTime time = DateTime.Now.AddMinutes(1).ToUniversalTime();
                //SYSTEMTIME time2 = new SYSTEMTIME();
                //time2.Day = Convert.ToInt16(time.Day);
                //time2.Month = Convert.ToInt16(time.Month);
                //time2.Year = Convert.ToInt16(time.Year);
                //time2.Hour = Convert.ToInt16(time.Hour);
                //time2.Minute = Convert.ToInt16(time.Minute);
                //SetSystemTime(ref time2);

                this.workItems.Add(DateTime.Now);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private Hashtable currentThread = null;

        private void AutoDemandForecasting(object o1)
        {
            while (true)
            {
                if (this.IsRun)
                {
                    if (this.workItems.Count != 0)
                    {
                        //첫실행 (버튼클릭)
                        if (this.Now.Year.ToString() == "1")
                        {
                            this.Now = this.workItems[0];
                            this.InitializeDfControl();

                            //해당일자에서 이전 시간예측유량을 보정한다.
                            if (this.Now.Hour > 0)
                            {
                                for (int i = 1; i < this.Now.Hour; i++)
                                {
                                    if (i < this.Now.Hour)
                                    {
                                        Df.TargetTime = i - 1;
                                        Df.UpdateHourPrediction_CurrentHour();
                                    }
                                }
                            }
                        }
                        else
                        {
                            //다음날
                            if (this.Now.AddDays(1).ToString("yyyyMMdd") == this.workItems[0].ToString("yyyyMMdd"))
                            {
                                //00시 50분이면 일유량을 산정한다.
                                if (this.workItems[0].Hour == 0 && this.workItems[0].Minute == 50)
                                {
                                    this.Now = this.workItems[0];
                                    this.InitializeDfControl();

                                    //해당일자에서 이전 시간예측유량을 보정한다.
                                    if (this.Now.Hour > 0)
                                    {
                                        for (int i = 1; i < this.Now.Hour; i++)
                                        {
                                            if (i < this.Now.Hour)
                                            {
                                                Df.TargetTime = i - 1;
                                                Df.UpdateHourPrediction_CurrentHour();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //해당시간을 조회후에 매시간 30분이면 시간유량을 보정한다.
                        if (this.workItems[0].Hour == 0 && this.workItems[0].Minute == 30)
                        {
                            Df.TargetTime = 23;
                            Df.UpdateHourPrediction_CurrentHour();
                        }
                        else if (this.workItems[0].Hour > 1 && this.workItems[0].Minute == 30)
                        {
                            Df.TargetTime = this.workItems[0].Hour - 1;
                            Df.UpdateHourPrediction_CurrentHour();
                        }

                        this.workItems.Remove(this.workItems[0]);
                    }
                }
            }
        }

        private void InitializeDfControl()
        {
            try
            {
                if (!this.IsRun)
                {
                    return;
                }

                DataTable dfList = DF_Simulation.work.DfSimulationWork.GetInstance().SelectDfSimulation();
                this.Df.ClearItem();
                this.Df.TexDfLog = this.texDfLog;
                foreach (DataRow row in dfList.Rows)
                {
                    if (row["TYPE"].ToString() == "F")
                    {
                        this.Df.AddItem(new DfDataBlock(row["LOC_CODE"].ToString()));
                    }
                    if (row["TYPE"].ToString() == "R")
                    {
                        this.Df.AddItem(new DfDataReservoir(row["LOC_CODE"].ToString()));
                    }
                }

                DateTime taget_date = this.Now;

                this.Df.TargetDate = taget_date.AddDays(1);
                this.Df.Day_Predict();
                this.Df.UpdateDayPrediction();
                this.Df.UpdateHourPrediction_AllHour();

                this.Df.TargetDate = taget_date;
                this.Df.Day_Predict();
                this.Df.UpdateDayPrediction();
                this.Df.UpdateHourPrediction_AllHour();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        #region 외부상속구현

        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        //메인폼에서 호출
        public void Open()
        {
        }

        #endregion
    }

    public class CustomMergedCellEvaluator : Infragistics.Win.UltraWinGrid.IMergedCellEvaluator
    {
        public bool ShouldCellsBeMerged(UltraGridRow row1, UltraGridRow row2, UltraGridColumn column)
        {
            if (row1.Cells["USE_YN"].Value.ToString() != row2.Cells["USE_YN"].Value.ToString())
            {
                return false;
            }

            if (row1.Cells["BZSPCD"].Value.ToString() != row2.Cells["BZSPCD"].Value.ToString())
            {
                return false;
            }

            if (row1.Cells["CNNCD"].Value.ToString() != row2.Cells["CNNCD"].Value.ToString())
            {
                return false;
            }

            if (row1.Cells["SCNCD"].Value.ToString() != row2.Cells["SCNCD"].Value.ToString())
            {
                return false;
            }

            if (row1.Cells["FTR_IDN"].Value.ToString() != row2.Cells["FTR_IDN"].Value.ToString())
            {
                return false;
            }

            return true;
        }
    }
}
