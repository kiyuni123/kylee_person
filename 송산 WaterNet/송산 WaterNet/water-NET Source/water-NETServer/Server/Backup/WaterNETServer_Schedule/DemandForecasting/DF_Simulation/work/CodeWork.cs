﻿using System;
using EMFrame.work;
using WaterNETServer.DF_Simulation.dao;
using System.Data;
using System.Collections;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;

namespace WaterNETServer.DF_Simulation.work
{
    public class CodeWork : BaseWork
    {
        private static CodeWork work = null;
        private CodeDao dao = null;

        public static CodeWork GetInstance()
        {
            if (work == null)
            {
                work = new CodeWork();
            }
            return work;
        }

        private CodeWork()
        {
            dao = CodeDao.GetInstance();
        }

        public DataTable SelectCodeList(Hashtable parameter)
        {
            DataTable data = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                data = dao.selectCodeList(manager, parameter);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return data;
        }
    }
}
