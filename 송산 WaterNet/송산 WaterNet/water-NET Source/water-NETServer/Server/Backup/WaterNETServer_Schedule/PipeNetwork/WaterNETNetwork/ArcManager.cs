﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using ESRI.ArcGIS.esriSystem;
//using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
//using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
//using ESRI.ArcGIS.DataSourcesGDB;
using ESRI.ArcGIS.DataSourcesFile;

namespace WaterNETServer.WaterNETNetwork
{
    public class ArcManager
    {
        /// <summary>
        /// ShapeWorkspace 구하기
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        public static IWorkspace getShapeWorkspace(string sPath)
        {
            System.Object obj = Activator.CreateInstance(Type.GetTypeFromProgID("esriDataSourcesFile.ShapefileWorkspaceFactory"));
            IWorkspaceFactory2 pWSFact = obj as IWorkspaceFactory2;
            IWorkspace pWS = null;
            IPropertySet pProp = new PropertySetClass();

            try
            {
                pProp.SetProperty("DATABASE", sPath);

                if (pWSFact.IsWorkspace(sPath))
                {
                    pWS = pWSFact.Open(pProp, 0);
                }
            }
            catch (COMException oComEx)
            {
                Console.WriteLine("getShapeWorkspace : " + oComEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("getShapeWorkspace : " + ex.Message);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(pWSFact);
            }

            return pWS;
        }

        /// <summary>
        /// Workspace에서 ShapeLayer구하기
        /// </summary>
        /// <param name="pWS"></param>
        /// <param name="sName"></param>
        /// <returns></returns>
        public static ILayer GetShapeLayer(IWorkspace pWS, string sName)
        {
            if (pWS == null) return null;
            if (!(pWS is IFeatureWorkspace)) return null;
            if (pWS.Type != esriWorkspaceType.esriFileSystemWorkspace) return null;
            if (!(pWS.WorkspaceFactory is ShapefileWorkspaceFactory)) return null;

            ESRI.ArcGIS.Carto.IFeatureLayer pFL = null;
            try
            {
                IEnumDataset pEnumDS = pWS.get_Datasets(esriDatasetType.esriDTFeatureClass);
                pEnumDS.Reset();
                IDataset pDS = pEnumDS.Next();
                while (pDS != null)
                {
                    if (pDS.Name.ToUpper() == sName.ToUpper())
                    {
                        pFL = new ESRI.ArcGIS.Carto.FeatureLayer();
                        pFL.FeatureClass = pDS as IFeatureClass;
                        pFL.Name = pDS.Name;
                        return pFL;
                    }

                    pDS = pEnumDS.Next();
                }
            }
            catch (System.Runtime.InteropServices.COMException eComExc)
            {
                MessageBox.Show(eComExc.ErrorCode.ToString() + " : " + eComExc.Message + " : " + eComExc.Source + " : " + eComExc.HelpLink + " : " + eComExc.StackTrace);
            }

            return pFL;
        }
    }
}
