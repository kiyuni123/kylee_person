﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNETServer.Common.DB.Oracle;

using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.esriSystem;
using EMFrame.log;
//using ESRI.ArcGIS.Controls;

//using WaterNet.WaterAOCore;

namespace WaterNETServer.WaterNETNetwork
{
    public partial class frmWUMain : Form
    {
        private WaterNETServer.Common.DB.Oracle.OracleDBManager m_oDBManager = null;
        private IWorkspace m_PipeWorkspace;
        //////////////////////////////////////////////////////// 변수 및 상수
        private IFeatureLayer m_WTL_BLBG_AS = null;     //대블록
        private IFeatureLayer m_WTL_BLBM_AS = null;     //중블록
        private IFeatureLayer m_WTL_BLSM_AS = null;     //소블록
        private IFeatureLayer m_WTL_PIPE_LS = null;     //상수관로
        private IFeatureLayer m_WTL_SPLY_LS = null;     //급수관로
        private IFeatureLayer m_WTL_META_PS = null;     //수도계량기

        private ArrayList m_ArrayNetworkLog = new ArrayList();  //관로 네트워크 : 상수관로-급수관로-수도계량기
        private ArrayList m_ArrayPipeLog = new ArrayList();     //관로 소속 블록 : 관로-소블록,중블록,대블록

        private string m_DeleteQuery = string.Empty;
        private string m_InsertQuery = string.Empty;

        public frmWUMain()
        {
            InitializeComponent();

            InitializeSetting();

            InitializeWorkspace();
        }

        #region 초기화설정
        /// <summary>
        /// 삭제, 추가 쿼리문 설정
        /// </summary>
        private void InitializeSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("DELETE FROM SI_PIPE_NETWORK");
            m_DeleteQuery = oStringBuilder.ToString();

            ////////////////////////////////////////////////////////////
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT INTO SI_PIPE_NETWORK (");
            oStringBuilder.AppendLine("   S_NAME       ,");
            oStringBuilder.AppendLine("   S_FTR_IDN    ,");
            oStringBuilder.AppendLine("   S_OID        ,");
            oStringBuilder.AppendLine("   L_NAME       ,");
            oStringBuilder.AppendLine("   L_FTR_IDN   ,");
            oStringBuilder.AppendLine("   L_OID        )");
            oStringBuilder.AppendLine("VALUES (");
            oStringBuilder.AppendLine("   :S_NAME       ,");
            oStringBuilder.AppendLine("   :S_FTR_IDN    ,");
            oStringBuilder.AppendLine("   :S_OID        ,");
            oStringBuilder.AppendLine("   :L_NAME       ,");
            oStringBuilder.AppendLine("   :L_FTR_IDN   ,");
            oStringBuilder.AppendLine("   :L_OID        )");
            m_InsertQuery = oStringBuilder.ToString();
        }

        private void InitializeWorkspace()
        {
            if (m_oDBManager == null) m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNETServer.Common.DB.Oracle.OracleDBFunctionManager.GetWaterNETConnectionString();
            try
            {
                m_oDBManager.Open();

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT F_DATABASE FROM SI_WORKSPACE WHERE UPPER(F_TYPE) = 'SHAPE' AND F_NAME = '관망도'");
                object o = m_oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null);

                m_PipeWorkspace = ArcManager.getShapeWorkspace((string)o);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                Application.Exit();
            }
        }
        #endregion


        #region Private Property ------------------------------------------------------------------------
        /// <summary>
        /// 대블록 Propertry
        /// </summary>
        private IFeatureLayer WTL_BLBG_AS
        {
            get
            {
                if (m_WTL_BLBG_AS == null)
                {
                    m_WTL_BLBG_AS = ArcManager.GetShapeLayer(m_PipeWorkspace, "WTL_BLBG_AS") as IFeatureLayer;
                }
                return m_WTL_BLBG_AS;
            }
        }

        /// <summary>
        /// 중블록 Propertry
        /// </summary>
        private IFeatureLayer WTL_BLBM_AS
        {
            get
            {
                if (m_WTL_BLBM_AS == null)
                {
                    m_WTL_BLBM_AS = ArcManager.GetShapeLayer(m_PipeWorkspace, "WTL_BLBM_AS") as IFeatureLayer;
                }
                return m_WTL_BLBM_AS;
            }
        }

        /// <summary>
        /// 소블록 Propertry
        /// </summary>
        private IFeatureLayer WTL_BLSM_AS
        {
            get
            {
                if (m_WTL_BLSM_AS == null)
                {
                    m_WTL_BLSM_AS = ArcManager.GetShapeLayer(m_PipeWorkspace, "WTL_BLSM_AS") as IFeatureLayer;
                }
                return m_WTL_BLSM_AS;
            }
        }

        /// <summary>
        /// 상수관로 Propertry
        /// </summary>
        private IFeatureLayer WTL_PIPE_LS
        {
            get
            {
                if (m_WTL_PIPE_LS == null)
                {
                    m_WTL_PIPE_LS = ArcManager.GetShapeLayer(m_PipeWorkspace, "WTL_PIPE_LS") as IFeatureLayer;
                }
                return m_WTL_PIPE_LS;
            }
        }

        /// <summary>
        /// 급수관로 Propertry
        /// </summary>
        private IFeatureLayer WTL_SPLY_LS
        {
            get
            {
                if (m_WTL_SPLY_LS == null)
                {
                    m_WTL_SPLY_LS = ArcManager.GetShapeLayer(m_PipeWorkspace, "WTL_SPLY_LS") as IFeatureLayer;
                }
                return m_WTL_SPLY_LS;
            }
        }

        /// <summary>
        /// 수도계량기 Propertry
        /// </summary>
        private IFeatureLayer WTL_META_PS
        {
            get
            {
                if (m_WTL_META_PS == null)
                {
                    m_WTL_META_PS = ArcManager.GetShapeLayer(m_PipeWorkspace, "WTL_META_PS") as IFeatureLayer;
                }
                return m_WTL_META_PS;
            }
        }
        #endregion Private Property ------------------------------------------------------------------------

        /// <summary>
        /// 상수관망 분석 모듈
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExecute_Click(object sender, EventArgs e)
        {            
            m_ArrayNetworkLog.Clear();
            m_ArrayPipeLog.Clear();

            Console.WriteLine("//----------------------------FeatureIndexing");
            IFeatureIndex2 SplyFeatureIndex = new FeatureIndexClass();
            IGeoDataset SplyGeoDataset = WTL_SPLY_LS.FeatureClass as IGeoDataset;
            SplyFeatureIndex.FeatureClass = WTL_SPLY_LS.FeatureClass;
            SplyFeatureIndex.set_OutputSpatialReference(WTL_SPLY_LS.FeatureClass.ShapeFieldName, SplyGeoDataset.SpatialReference);
            SplyFeatureIndex.Index(null, SplyGeoDataset.Extent);

            IFeatureIndex2 PipeFeatureIndex = new FeatureIndexClass();
            IGeoDataset PipeGeoDataset = WTL_PIPE_LS.FeatureClass as IGeoDataset;
            PipeFeatureIndex.FeatureClass = WTL_PIPE_LS.FeatureClass;
            PipeFeatureIndex.set_OutputSpatialReference(WTL_PIPE_LS.FeatureClass.ShapeFieldName, PipeGeoDataset.SpatialReference);
            PipeFeatureIndex.Index(null, PipeGeoDataset.Extent);

            IFeatureIndex2 MetaFeatureIndex = new FeatureIndexClass();
            IGeoDataset MetaGeoDataset = WTL_META_PS.FeatureClass as IGeoDataset;
            MetaFeatureIndex.FeatureClass = WTL_META_PS.FeatureClass;
            MetaFeatureIndex.set_OutputSpatialReference(WTL_META_PS.FeatureClass.ShapeFieldName, MetaGeoDataset.SpatialReference);
            MetaFeatureIndex.Index(null, MetaGeoDataset.Extent);

            this.statusStrip1.Items["toolStripStatusLabel1"].Text = " 전체 상수관로를 분석중입니다.";

            Console.WriteLine("//----------------------------Pipe Query");
            Console.WriteLine(DateTime.Now.ToString());
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureCursor pFeatureCursor = WTL_PIPE_LS.Search(null, true);
                comReleaser.ManageLifetime(pFeatureCursor);

                IFeature pFeature = pFeatureCursor.NextFeature();
                while (pFeature != null)
                {
                    Console.WriteLine(pFeature.OID.ToString());
                    m_ArrayNetworkLog.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                                         + "," + pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString());


                    ///-------------상수관로(시작점)이 위치한 블록-------------------------------------
                    IFeature theFeature = GetBlockIsContain(pFeature);

                    if (theFeature != null)
                        m_ArrayPipeLog.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                                           + "," + pFeature.get_Value(pFeature.Fields.FindField("SAA_CDE")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("MOP_CDE")).ToString()
                                           + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_LEN")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_DIP")).ToString()
                                           + "," + theFeature.Class.AliasName + "," + theFeature.get_Value(theFeature.Fields.FindField("FTR_IDN")).ToString() + "," + theFeature.OID.ToString()
                                           + "," + "");

                    ///-------------상수관로(시작점)이 위치한 블록-------------------------------------

                    pFeature = pFeatureCursor.NextFeature();
                }
            }

            Console.WriteLine("//----------------------------Sply Query");
            Console.WriteLine(DateTime.Now.ToString());
            TreeView SplytreeView = new TreeView();
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureCursor pFeatureCursor = WTL_SPLY_LS.Search(null, true);
                comReleaser.ManageLifetime(pFeatureCursor);

                IFeature pFeature = pFeatureCursor.NextFeature();
                while (pFeature != null)
                {
                    TreeNode pNode = new TreeNode();
                    pNode.Tag = pFeature.Class.AliasName;
                    pNode.Text = pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString();
                    pNode.Name = pFeature.OID.ToString();
                    SplytreeView.Nodes.Add(pNode);

                    Console.WriteLine(pFeature.OID.ToString());

                    IFeature theFeature = GetBlockIsContain(pFeature);
                    if (theFeature != null)
                        m_ArrayPipeLog.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                                           + "," + pFeature.get_Value(pFeature.Fields.FindField("SAA_CDE")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("MOP_CDE")).ToString()
                                           + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_LEN")).ToString() + "," + pFeature.get_Value(pFeature.Fields.FindField("PIP_DIP")).ToString()
                                           + "," + theFeature.Class.AliasName + "," + theFeature.get_Value(theFeature.Fields.FindField("FTR_IDN")).ToString() + "," + theFeature.OID.ToString()
                                           + "," + "");

                    pFeature = pFeatureCursor.NextFeature();
                }
            }

            Console.WriteLine("//----------------------------Meta Query");
            Console.WriteLine(DateTime.Now.ToString());
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureCursor pFeatureCursor = WTL_META_PS.Search(null, true);
                comReleaser.ManageLifetime(pFeatureCursor);

                IFeature pFeature = pFeatureCursor.NextFeature();
                while (pFeature != null)
                {
                    Console.WriteLine(pFeature.OID.ToString());

                    IFeature theFeature = GetBlockIsContain(pFeature);
                    if (theFeature != null)
                        m_ArrayPipeLog.Add(pFeature.Class.AliasName + "," + pFeature.get_Value(pFeature.Fields.FindField("FTR_IDN")).ToString() + "," + pFeature.OID.ToString()
                                           + "," + "" + "," + ""
                                           + "," + "" + "," + pFeature.get_Value(pFeature.Fields.FindField("MET_DIP")).ToString()
                                           + "," + theFeature.Class.AliasName + "," + theFeature.get_Value(theFeature.Fields.FindField("FTR_IDN")).ToString() + "," + theFeature.OID.ToString()
                                           + "," + pFeature.get_Value(pFeature.Fields.FindField("DMNO")).ToString());

                    pFeature = pFeatureCursor.NextFeature();
                }
            }
            Console.WriteLine(DateTime.Now.ToString());

            //int ProcessCount = Environment.ProcessorCount;
            //int size = SplytreeView.Nodes.Count;
            //int range = size / ProcessCount;
            //int start = 0;
            //int end = 0;

            //Console.WriteLine("//----------------------------threed");
            //ArrayList startlist = new ArrayList();
            //ArrayList endlist = new ArrayList();
            //for (int i = 0; i < size; i += range + 1)
            //{
            //    start = i;
            //    end = (i + range) > size ? size : (i + range);

            //    startlist.Add(start);
            //    endlist.Add(end);
            //}

            //Console.WriteLine("//----------------------------tree 분할");
            //ArrayList treeCount = new ArrayList();
            //for (int i = 0; i < startlist.Count; i++)
            //{
            //    TreeView tree = new TreeView();
            //    foreach (TreeNode node in SplytreeView.Nodes)
            //    {
            //        if (node.Index > Convert.ToInt32(endlist[i])) continue;
            //        if (node.Index >= Convert.ToInt32(startlist[i]) && node.Index <= Convert.ToInt32(endlist[i]))
            //        {
            //            TreeNode nodeClone = new TreeNode();
            //            nodeClone = node.Clone() as TreeNode;
            //            tree.Nodes.Add(nodeClone);
            //        }
            //    }

            //    treeCount.Add(tree);
            //}

            /////병렬 실행
            //Parallel.For(0, treeCount.Count, delegate(int i)
            //{
                //Console.WriteLine("//----------------------------Parallel 실행" + i.ToString());
                //Console.WriteLine(DateTime.Now.ToString());
                //TreeView tree = treeCount[i] as TreeView;
                NetworkOperator oNetWorkOperator = new NetworkOperator();
                oNetWorkOperator.treeview = SplytreeView;
                oNetWorkOperator.log = m_ArrayNetworkLog;
                oNetWorkOperator.MetaFeatureIndex = MetaFeatureIndex;
                oNetWorkOperator.PipeFeatureIndex = PipeFeatureIndex;
                oNetWorkOperator.SplyFeatureIndex = SplyFeatureIndex;

                oNetWorkOperator.WTL_META_PS = this.m_WTL_META_PS;
                oNetWorkOperator.WTL_PIPE_LS = this.m_WTL_PIPE_LS;
                oNetWorkOperator.WTL_SPLY_LS = this.m_WTL_SPLY_LS;
                oNetWorkOperator.Execute();
            //});
        }

        /// <summary>
        /// 폼 종료
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        /// <summary>
        /// 상수관로, 급수관로의 시작점이 있는 블록을 가져온다.
        /// 소블록, 중블록, 대블록의 순서로 검색한다.
        /// </summary>
        /// <param name="pFeature"></param>
        private IFeature GetBlockIsContain(IFeature pFeature)
        {
            IPoint pPoint = getStartPoint(pFeature);
            if (pPoint == null) return null;

            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.Geometry = pPoint as IGeometry;
            pSpatialFilter.GeometryField = "Shape";
            pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;
            pSpatialFilter.WhereClause = string.Empty;

            IFeature theFeature = null;
            try
            {
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureCursor pCursor = WTL_BLSM_AS.FeatureClass.Search(pSpatialFilter, true);
                    comReleaser.ManageLifetime(pCursor);

                    theFeature = pCursor.NextFeature();
                    if (theFeature == null)
                    {
                        pCursor = WTL_BLBM_AS.FeatureClass.Search(pSpatialFilter, true);
                        theFeature = pCursor.NextFeature();
                        if (theFeature == null)
                        {
                            pCursor = WTL_BLBG_AS.FeatureClass.Search(pSpatialFilter, true);
                            theFeature = pCursor.NextFeature();
                        }
                    }
                }
            }
            catch (Exception)
            {
                return null;
                //throw;
            }


            return theFeature;
        }

        /// <summary>
        /// 관로의 시작점을 반환한다.
        /// </summary>
        /// <param name="pFeature"></param>
        /// <returns></returns>
        private IPoint getStartPoint(IFeature pFeature)
        {
            IPoint pPoint = null;
            try
            {
                if (pFeature.Shape.GeometryType == esriGeometryType.esriGeometryPolyline)
                {
                    IPolyline pPolyline = pFeature.Shape as IPolyline;
                    pPoint = new PointClass();
                    pPoint = pPolyline.FromPoint;
                }
                else if (pFeature.Shape.GeometryType == esriGeometryType.esriGeometryPoint)
                {
                    pPoint = new PointClass();
                    pPoint = pFeature.ShapeCopy as IPoint;
                }
            }
            catch (Exception)
            {

                return null;
            }

            

            return pPoint;
        }

        /// <summary>
        /// 상수관로, 급수관로의 블록정보를 데이터베이스에 저장한다.
        /// </summary>
        private void SavePipeBlockQuery()
        {
            if (this.m_ArrayPipeLog.Count == 0)
            {
                //MessageManager.ShowInformationMessage("상수관망 네트워크 분석을 실행하십시오.");
                return;
            }

            WaterNETServer.Common.DB.Oracle.OracleDBManager oDBManager = new OracleDBManager();
            try
            {
                oDBManager.ConnectionString = WaterNETServer.Common.DB.Oracle.OracleDBFunctionManager.GetWaterNETConnectionString();
                oDBManager.Open();

                oDBManager.BeginTransaction();
                ///기존 데이터 삭제
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("DELETE FROM SI_PIPE_BLOCK");
                oDBManager.ExecuteScript(oStringBuilder.ToString(), null);

                ////////////////////////////////////////////////////////////
                oStringBuilder.Remove(0, oStringBuilder.Length);
                oStringBuilder.AppendLine("INSERT INTO SI_PIPE_BLOCK (");
                oStringBuilder.AppendLine("   S_NAME       ,");
                oStringBuilder.AppendLine("   S_FTR_IDN    ,");
                oStringBuilder.AppendLine("   S_OID        ,");
                oStringBuilder.AppendLine("   SAA_CDE      ,");
                oStringBuilder.AppendLine("   MOP_CDE      ,");
                oStringBuilder.AppendLine("   PIP_LEN      ,");
                oStringBuilder.AppendLine("   PIP_DIP      ,");
                oStringBuilder.AppendLine("   B_NAME       ,");
                oStringBuilder.AppendLine("   B_FTR_IDN    ,");
                oStringBuilder.AppendLine("   B_OID        ,");
                oStringBuilder.AppendLine("   DMNO         )");
                oStringBuilder.AppendLine("VALUES (");
                oStringBuilder.AppendLine("   :S_NAME       ,");
                oStringBuilder.AppendLine("   :S_FTR_IDN    ,");
                oStringBuilder.AppendLine("   :S_OID        ,");
                oStringBuilder.AppendLine("   :SAA_CDE      ,");
                oStringBuilder.AppendLine("   :MOP_CDE      ,");
                oStringBuilder.AppendLine("   :PIP_LEN      ,");
                oStringBuilder.AppendLine("   :PIP_DIP      ,");
                oStringBuilder.AppendLine("   :B_NAME       ,");
                oStringBuilder.AppendLine("   :B_FTR_IDN    ,");
                oStringBuilder.AppendLine("   :B_OID        ,");
                oStringBuilder.AppendLine("   :DMNO        )");

                foreach (string item in this.m_ArrayPipeLog)
                {
                    string[] splitedstring = item.Split(',');
                    if (splitedstring.Length < 11) continue;

                    IDataParameter[] oParams = {
                        new OracleParameter(":S_NAME",      OracleDbType.Varchar2,50),
                        new OracleParameter(":S_FTR_IDN",   OracleDbType.Varchar2,10),
                        new OracleParameter(":S_OID",       OracleDbType.Int32,   4),
                        new OracleParameter(":SAA_CDE",     OracleDbType.Varchar2,50),
                        new OracleParameter(":MOP_CDE",     OracleDbType.Varchar2,10),
                        new OracleParameter(":PIP_LEN",     OracleDbType.Double,  8),
                        new OracleParameter(":PIP_DIP",     OracleDbType.Double,  8),
                        new OracleParameter(":B_NAME",      OracleDbType.Varchar2,50),
                        new OracleParameter(":B_FTR_IDN",   OracleDbType.Varchar2,10),        
                        new OracleParameter(":B_OID",       OracleDbType.Int32,   4),
                        new OracleParameter(":DMNO",        OracleDbType.Varchar2,20)
                    };

                    oParams[0].Value = splitedstring[0];
                    oParams[1].Value = splitedstring[1];
                    oParams[2].Value = Convert.ToInt32(splitedstring[2]);
                    oParams[3].Value = splitedstring[3];
                    oParams[4].Value = splitedstring[4];
                    oParams[5].Value = (splitedstring[5].Length > 0) ? Convert.ToDouble(splitedstring[5]) : 0;
                    oParams[6].Value = (splitedstring[6].Length > 0) ? Convert.ToDouble(splitedstring[6]) : 0;
                    oParams[7].Value = splitedstring[7];
                    oParams[8].Value = splitedstring[8];
                    oParams[9].Value = Convert.ToInt32(splitedstring[9]);
                    oParams[10].Value = splitedstring[10];

                    oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
                }

                oDBManager.CommitTransaction();
                //MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);
            }
            catch (Exception ex)
            {
                oDBManager.RollbackTransaction();
                //MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                throw ex;
            }
            finally
            {
                if (oDBManager != null)  oDBManager.Close();
            }
        }

        /// <summary>
        /// 상수관망 네트워크 구조를 데이터베이스에 저장한다.
        /// </summary>
        private void SavePipeNetworkQuery()
        {
            if (this.m_ArrayNetworkLog.Count == 0)
            {
                //MessageManager.ShowInformationMessage("상수관망 네트워크 분석을 실행하십시오.");
                return;
            }

            WaterNETServer.Common.DB.Oracle.OracleDBManager oDBManager = new OracleDBManager();
            try
            {
                oDBManager.ConnectionString = WaterNETServer.Common.DB.Oracle.OracleDBFunctionManager.GetWaterNETConnectionString();
                oDBManager.Open();

                oDBManager.BeginTransaction();
                ///기존 데이터 삭제
                oDBManager.ExecuteScript(m_DeleteQuery, null);

                foreach (string item in this.m_ArrayNetworkLog)
                {
                    string[] splitedstring = item.Split(',');
                    if (splitedstring.Length < 6) continue;

                    IDataParameter[] oParams = {
                        new OracleParameter(":S_NAME",      OracleDbType.Varchar2,50),
                        new OracleParameter(":S_FTR_IDN",   OracleDbType.Varchar2,10),
                        new OracleParameter(":S_OID",       OracleDbType.Int32,   4),
                        new OracleParameter(":L_NAME",      OracleDbType.Varchar2,50),
                        new OracleParameter(":L_FTR_IDN",   OracleDbType.Varchar2,10),        
                        new OracleParameter(":L_OID",       OracleDbType.Int32,  4)
                    };

                    oParams[0].Value = splitedstring[0];
                    oParams[1].Value = splitedstring[1];
                    oParams[2].Value = Convert.ToInt32(splitedstring[2]);
                    oParams[3].Value = splitedstring[3];
                    oParams[4].Value = splitedstring[4];
                    oParams[5].Value = Convert.ToInt32(splitedstring[5]);

                    oDBManager.ExecuteScript(m_InsertQuery, oParams);

                }

                oDBManager.CommitTransaction();
                //MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_COMPLETE);
            }
            catch (Exception ex)
            {
                oDBManager.RollbackTransaction();
                //MessageManager.ShowInformationMessage(MessageManager.MSG_SAVE_CANCEL);
                throw ex;
            }
            finally
            {
                if (oDBManager != null)   oDBManager.Close();
            }
        }
        /// <summary>
        /// 저장버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            SavePipeNetworkQuery();
            SavePipeBlockQuery();
        }

        /// <summary>
        /// 실행폼 로드
        /// 폼을 표시하지 않기 위해, Hide하고
        /// 분석 / 저장 / Destory 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmWUMain_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            this.Hide();

            btnExecute_Click(this, new EventArgs());

            btnSave_Click(this, new EventArgs());

            btnClose_Click(this, new EventArgs());
            
        }

    }
}
