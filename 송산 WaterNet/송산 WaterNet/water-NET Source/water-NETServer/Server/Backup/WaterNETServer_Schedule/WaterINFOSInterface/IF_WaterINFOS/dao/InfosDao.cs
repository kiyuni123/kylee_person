﻿using System;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using EMFrame.dm;
using EMFrame.log;

namespace IF_WaterINFOS.dao
{
    public class InfosDao
    {
        #region 수용가정보 #########################################
        /// <summary>
        /// 수용가정보 (임시테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dSet"></param>
        public void GetDMINFO(EMapper manager, DataSet dSet, DateTime dt)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" INSERT INTO IF_DMINFO (sgccd,dmno,umdcd,hydrntno,lftridn,mftridn,sftridn,dmclass,dmnm,dmaddr,regdt,if_date)");
            query.AppendLine(" 	 VALUES (");
            query.AppendLine(" 	 :1");
            query.AppendLine(" 	,:2");
            query.AppendLine(" 	,:3");
            query.AppendLine(" 	,:4");
            query.AppendLine(" 	,:5");
            query.AppendLine(" 	,:6");
            query.AppendLine(" 	,:7");
            query.AppendLine(" 	,:8");
            query.AppendLine(" 	,:9");
            query.AppendLine(" 	,:10");
            query.AppendLine(" 	,:11");
            query.AppendLine(" 	,:12");
            query.AppendLine(" )");

            DataRowCollection drc = dSet.Tables[0].Rows;
            foreach (DataRow row in drc)
            {
                IDataParameter[] parameters =  {
                     new OracleParameter("1", OracleDbType.Varchar2)
                    ,new OracleParameter("2", OracleDbType.Varchar2)
                    ,new OracleParameter("3", OracleDbType.Varchar2)
                    ,new OracleParameter("4", OracleDbType.Varchar2)
                    ,new OracleParameter("5", OracleDbType.Varchar2)
                    ,new OracleParameter("6", OracleDbType.Varchar2)
                    ,new OracleParameter("7", OracleDbType.Varchar2)
                    ,new OracleParameter("8", OracleDbType.Varchar2)
                    ,new OracleParameter("9", OracleDbType.Varchar2)
                    ,new OracleParameter("10", OracleDbType.Varchar2)
                    ,new OracleParameter("11", OracleDbType.Date)                    
                    ,new OracleParameter("12", OracleDbType.Date)               
                };

                try
                {
                    for (int i = 0; i < dSet.Tables[0].Columns.Count; i++)
                    {
                        parameters[i].Value = row[i].ToString();
                    }
                    parameters[11].Value = dt; //Convert.ToDateTime(dt);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }

                manager.ExecuteScript(query.ToString(), parameters);
            }
        }

        /// <summary>
        /// 수용가정보 (블록 코드 변환 후 본테이블에 넣기)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr">I/F Date</param>
        internal void MdfyDMINFO(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO WI_DMINFO A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT  A.DMNO,");
            query.AppendLine("                A.SGCCD,");
            query.AppendLine("                A.UMDCD,");
            query.AppendLine("                A.HYDRNTNO,");
            query.AppendLine("                B.DEST_CODE LFTRIDN,");
            query.AppendLine("                C.DEST_CODE MFTRIDN,");
            query.AppendLine("                D.DEST_CODE SFTRIDN,");
            query.AppendLine("                A.DMCLASS,");
            query.AppendLine("                A.DMNM,");
            query.AppendLine("                A.TRADENM,");
            query.AppendLine("                A.DMADDR,");
            query.AppendLine("                A.REGDT");
            query.AppendLine("          FROM IF_DMINFO A");
            query.AppendLine("             , IF_CODE_MAPPING B");
            query.AppendLine("             , IF_CODE_MAPPING C");
            query.AppendLine("             , IF_CODE_MAPPING D");
            query.AppendLine("         WHERE B.SOURCE_CODE = A.LFTRIDN");
            query.AppendLine("           AND B.MAPPING_GBN = '001'");
            query.AppendLine("           AND C.SOURCE_CODE = A.MFTRIDN");
            query.AppendLine("           AND C.MAPPING_GBN = '002'");
            query.AppendLine("           AND D.SOURCE_CODE = A.SFTRIDN");
            query.AppendLine("           AND D.MAPPING_GBN = '003'");
            query.AppendLine("           AND A.IF_DATE     = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.DMNO = B.DMNO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET A.SGCCD       = B.SGCCD");
            query.AppendLine("            , A.UMDCD       = B.UMDCD");
            query.AppendLine("            , A.HYDRNTNO    = B.HYDRNTNO");
            query.AppendLine("            , A.LFTRIDN     = B.LFTRIDN");
            query.AppendLine("            , A.MFTRIDN     = B.MFTRIDN");
            query.AppendLine("            , A.SFTRIDN     = B.SFTRIDN");
            query.AppendLine("            , A.DMCLASS     = B.DMCLASS");
            query.AppendLine("            , A.DMNM        = B.DMNM");
            query.AppendLine("            , A.TRADENM     = B.TRADENM");
            query.AppendLine("            , A.DMADDR      = B.DMADDR");
            query.AppendLine("            , A.REGDT       = B.REGDT");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT (A.DMNO, A.SGCCD, A.UMDCD, A.HYDRNTNO, A.LFTRIDN, A.MFTRIDN, ");
            query.AppendLine("           A.SFTRIDN, A.DMCLASS, A.DMNM, A.TRADENM, A.DMADDR, A.REGDT)");
            query.AppendLine("   VALUES (B.DMNO, B.SGCCD, B.UMDCD, B.HYDRNTNO, B.LFTRIDN, B.MFTRIDN, ");
            query.AppendLine("           B.SFTRIDN, B.DMCLASS, B.DMNM, B.TRADENM, B.DMADDR, B.REGDT)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 수용가정보2 (블록 코드 변환 후 본테이블에 넣기)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr">I/F Date</param>
        internal void MdfyDMINFO2(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO DMINFO A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT  A.DMNO,");
            query.AppendLine("                A.SGCCD,");
            query.AppendLine("                A.UMDCD,");
            query.AppendLine("                A.HYDRNTNO,");
            query.AppendLine("                B.DEST_CODE LFTRIDN,");
            query.AppendLine("                C.DEST_CODE MFTRIDN,");
            query.AppendLine("                D.DEST_CODE SFTRIDN,");
            query.AppendLine("                A.DMCLASS,");
            query.AppendLine("                A.DMNM,");
            query.AppendLine("                A.TRADENM,");
            query.AppendLine("                A.DMADDR,");
            query.AppendLine("                A.REGDT");
            query.AppendLine("          FROM IF_DMINFO A");
            query.AppendLine("             , IF_CODE_MAPPING B");
            query.AppendLine("             , IF_CODE_MAPPING C");
            query.AppendLine("             , IF_CODE_MAPPING D");
            query.AppendLine("         WHERE B.SOURCE_CODE = A.LFTRIDN");
            query.AppendLine("           AND B.MAPPING_GBN = '001'");
            query.AppendLine("           AND C.SOURCE_CODE = A.MFTRIDN");
            query.AppendLine("           AND C.MAPPING_GBN = '002'");
            query.AppendLine("           AND D.SOURCE_CODE = A.SFTRIDN");
            query.AppendLine("           AND D.MAPPING_GBN = '003'");
            query.AppendLine("           AND A.IF_DATE     = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.DMNO = B.DMNO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET A.SGCCD       = B.SGCCD");
            query.AppendLine("            , A.UMDCD       = B.UMDCD");
            query.AppendLine("            , A.HYDRNTNO    = B.HYDRNTNO");
            query.AppendLine("            , A.LFTRIDN     = B.LFTRIDN");
            query.AppendLine("            , A.MFTRIDN     = B.MFTRIDN");
            query.AppendLine("            , A.SFTRIDN     = B.SFTRIDN");
            query.AppendLine("            , A.DMCLASS     = B.DMCLASS");
            query.AppendLine("            , A.DMNM        = B.DMNM");
            query.AppendLine("            , A.TRADENM     = B.TRADENM");
            query.AppendLine("            , A.DMADDR      = B.DMADDR");
            query.AppendLine("            , A.REGDT       = B.REGDT");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT (A.DMNO, A.SGCCD, A.UMDCD, A.HYDRNTNO, A.LFTRIDN, A.MFTRIDN, ");
            query.AppendLine("           A.SFTRIDN, A.DMCLASS, A.DMNM, A.TRADENM, A.DMADDR, A.REGDT)");
            query.AppendLine("   VALUES (B.DMNO, B.SGCCD, B.UMDCD, B.HYDRNTNO, B.LFTRIDN, B.MFTRIDN, ");
            query.AppendLine("           B.SFTRIDN, B.DMCLASS, B.DMNM, B.TRADENM, B.DMADDR, B.REGDT)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 수용가정보 삭제
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void DeleteDMINFO(EMapper manager, string dtStr)
        {
            string cof = "2";   // 삭제는 2일전 데이터를 삭제

            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_DMINFO WHERE IF_DATE < TO_DATE('" + dtStr + "', 'YYYYMMDD') - " + cof);

            manager.ExecuteScript(query.ToString(), null);

            // 수용가정보 인덱스를 Rebuild한다.
            // 데이터가 쌓이고, 삭제되고를 반복하기 때문에 속도, 결과조회에 영향을 받기 때문
            manager.ExecuteScript("ALTER INDEX IF_DMINFO_PK REBUILD", null);
        }
        #endregion ###############################################

        #region 상하수도자원 #########################################
        /// <summary>
        /// 상하수도자원
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dSet"></param>
        public void GetDMWSRSRC(EMapper manager, DataSet dSet, DateTime dt)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" INSERT INTO IF_DMWSRSRC (dmno,hydrntstat,statappdt,strtdt,wspipeszcd,wsrepbizkndcd,wsnohshd,regdt,if_date)");
            query.AppendLine(" 	 VALUES (");
            query.AppendLine(" 	 :1");
            query.AppendLine(" 	,:2");
            query.AppendLine(" 	,:3");
            query.AppendLine(" 	,:4");
            query.AppendLine(" 	,:5");
            query.AppendLine(" 	,:6");
            query.AppendLine(" 	,:7");
            query.AppendLine(" 	,:8");
            query.AppendLine(" 	,:9");
            query.AppendLine(" )");

            DataRowCollection drc = dSet.Tables[0].Rows;
            foreach (DataRow row in drc)
            {
                IDataParameter[] parameters =  {
                     new OracleParameter("1", OracleDbType.Varchar2)
                    ,new OracleParameter("2", OracleDbType.Varchar2)
                    ,new OracleParameter("3", OracleDbType.Varchar2)
                    ,new OracleParameter("4", OracleDbType.Varchar2)
                    ,new OracleParameter("5", OracleDbType.Varchar2)
                    ,new OracleParameter("6", OracleDbType.Varchar2)
                    ,new OracleParameter("7", OracleDbType.Int32)
                    ,new OracleParameter("8", OracleDbType.Date)
                    ,new OracleParameter("9", OracleDbType.Date)
                };

                //i = 1 => sgccd필드 skip
                for (int i = 1; i < dSet.Tables[0].Columns.Count; i++)
                {
                    if (i == 7)
                    {
                        int result = 1;
                        int.TryParse(row[i].ToString(), out result);
                        parameters[i - 1].Value = result;
                    }
                    else if (i == 8)
                    {
                        DateTime result = DateTime.MinValue;
                        try
                        {
                            result = DateTime.ParseExact(row[i].ToString(), "yyyyMMdd", null, System.Globalization.DateTimeStyles.AssumeLocal);
                        }
                        catch { }
                        parameters[i - 1].Value = result;
                    }
                    else
                    {
                        parameters[i-1].Value = row[i].ToString();
                    }
                }
                parameters[8].Value = dt; // Convert.ToDateTime(dt);

                manager.ExecuteScript(query.ToString(), parameters);
            }
        }

        /// <summary>
        /// 상하수도자원 (사용테이블에 넣기)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void MdfyDMWSRSRC(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO WI_DMWSRSRC A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT");
            query.AppendLine("            DMNO,");
            query.AppendLine("            HYDRNTSTAT,");
            query.AppendLine("            STATAPPDT,");
            query.AppendLine("            STRTDT,");
            query.AppendLine("            WSPIPESZCD,");
            query.AppendLine("            WSREPBIZKNDCD,");
            query.AppendLine("            WSNOHSHD,");
            query.AppendLine("            REGDT");
            query.AppendLine("        FROM IF_DMWSRSRC");
            query.AppendLine("       WHERE IF_DATE = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.DMNO = B.DMNO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET");
            query.AppendLine("             A.HYDRNTSTAT       = B.HYDRNTSTAT");
            query.AppendLine("            ,A.STATAPPDT        = B.STATAPPDT");
            query.AppendLine("            ,A.STRTDT           = B.STRTDT");
            query.AppendLine("            ,A.WSPIPESZCD       = B.WSPIPESZCD");
            query.AppendLine("            ,A.WSREPBIZKNDCD    = B.WSREPBIZKNDCD");
            query.AppendLine("            ,A.WSNOHSHD         = B.WSNOHSHD");
            query.AppendLine("            ,A.REGDT            = B.REGDT");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT (A.DMNO, A.HYDRNTSTAT, A.STATAPPDT, A.STRTDT, A.WSPIPESZCD,            ");
            query.AppendLine("           A.WSREPBIZKNDCD, A.WSNOHSHD, A.REGDT)");
            query.AppendLine("   VALUES (B.DMNO, B.HYDRNTSTAT, B.STATAPPDT, B.STRTDT, B.WSPIPESZCD,            ");
            query.AppendLine("           B.WSREPBIZKNDCD, B.WSNOHSHD, B.REGDT)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 상하수도자원2 (사용테이블에 넣기)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void MdfyDMWSRSRC2(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO DMWSRSRC A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT");
            query.AppendLine("            DMNO,");
            query.AppendLine("            HYDRNTSTAT,");
            query.AppendLine("            STATAPPDT,");
            query.AppendLine("            STRTDT,");
            query.AppendLine("            WSPIPESZCD,");
            query.AppendLine("            WSREPBIZKNDCD,");
            query.AppendLine("            WSNOHSHD,");
            query.AppendLine("            REGDT");
            query.AppendLine("        FROM IF_DMWSRSRC");
            query.AppendLine("       WHERE IF_DATE = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.DMNO = B.DMNO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET");
            query.AppendLine("             A.HYDRNTSTAT       = B.HYDRNTSTAT");
            query.AppendLine("            ,A.STATAPPDT        = B.STATAPPDT");
            query.AppendLine("            ,A.STRTDT           = B.STRTDT");
            query.AppendLine("            ,A.WSPIPESZCD       = B.WSPIPESZCD");
            query.AppendLine("            ,A.WSREPBIZKNDCD    = B.WSREPBIZKNDCD");
            query.AppendLine("            ,A.WSNOHSHD         = B.WSNOHSHD");
            query.AppendLine("            ,A.REGDT            = B.REGDT");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT (A.DMNO, A.HYDRNTSTAT, A.STATAPPDT, A.STRTDT, A.WSPIPESZCD,            ");
            query.AppendLine("           A.WSREPBIZKNDCD, A.WSNOHSHD, A.REGDT)");
            query.AppendLine("   VALUES (B.DMNO, B.HYDRNTSTAT, B.STATAPPDT, B.STRTDT, B.WSPIPESZCD,            ");
            query.AppendLine("           B.WSREPBIZKNDCD, B.WSNOHSHD, B.REGDT)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 상하수도자원 삭제
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void DeleteDMWSRSRC(EMapper manager, string dtStr)
        {
            string cof = "2";   // 삭제는 2일전 데이터를 삭제

            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_DMWSRSRC WHERE IF_DATE < TO_DATE('" + dtStr + "', 'YYYYMMDD') - " + cof);

            manager.ExecuteScript(query.ToString(), null);

            // 상하수도자원 인덱스를 Rebuild한다.
            // 데이터가 쌓이고, 삭제되고를 반복하기 때문에 속도, 결과조회에 영향을 받기 때문
            manager.ExecuteScript("ALTER INDEX IF_DMWSRSRC_PK REBUILD", null);
        }
        #endregion ###############################################

        #region 민원기본정보 #########################################
        /// <summary>
        /// 민원기본정보 (임시테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dSet"></param>
        public void GetCAINFO(EMapper manager, DataSet dSet, DateTime dt)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" INSERT INTO IF_CAINFO (sgccd,cano,calrgcd,camidcd,caappldt,dmno,prcsdt,cacont,caprcsrslt,regdt,if_date)");
            query.AppendLine(" 	 VALUES (");
            query.AppendLine(" 	 :1");
            query.AppendLine(" 	,:2");
            query.AppendLine(" 	,:3");
            query.AppendLine(" 	,:4");
            query.AppendLine(" 	,:5");
            query.AppendLine(" 	,:6");
            query.AppendLine(" 	,:7");
            query.AppendLine(" 	,:8");
            query.AppendLine(" 	,:9");
            query.AppendLine(" 	,:10");
            query.AppendLine(" 	,:11)");
            
            DataRowCollection drc = dSet.Tables[0].Rows;
            //try
            //{
                foreach (DataRow row in drc)
                {
                    IDataParameter[] parameters =  {
                     new OracleParameter("1", OracleDbType.Varchar2)
                    ,new OracleParameter("2", OracleDbType.Varchar2)
                    ,new OracleParameter("3", OracleDbType.Varchar2)
                    ,new OracleParameter("4", OracleDbType.Varchar2)
                    ,new OracleParameter("5", OracleDbType.Varchar2)
                    ,new OracleParameter("6", OracleDbType.Varchar2)
                    ,new OracleParameter("7", OracleDbType.Varchar2)
                    ,new OracleParameter("8", OracleDbType.Varchar2)
                    ,new OracleParameter("9", OracleDbType.Varchar2)
                    ,new OracleParameter("10", OracleDbType.Date)
                    ,new OracleParameter("11", OracleDbType.Date)                    
                };

                    for (int i = 0; i < dSet.Tables[0].Columns.Count; i++)
                    {
                        parameters[i].Value = row[i].ToString();
                    }
                    parameters[10].Value = dt;// Convert.ToDateTime(dt);

                    manager.ExecuteScript(query.ToString(), parameters);
                }
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.ToString());
            //    throw;
            //}
          
        }

        /// <summary>
        /// 민원기본정보 (사용테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void MdfyCAINFO(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO WI_CAINFO A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT CANO, SGCCD, CAAPPLMETH, CAAPPLCLASS, FRMYN, VSTCAYN");
            query.AppendLine("             , APPRVYN, CALRGCD, CAMIDCD, CAAPPLDT, DMNO, CANM, CAADDR");
            query.AppendLine("             , PRCSDT, USERNM, DEPT, CACONT, CAPRCSRSLT, REGDT");
            query.AppendLine("        FROM IF_CAINFO");
            query.AppendLine("       WHERE IF_DATE = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.CANO = B.CANO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET");
            query.AppendLine("             A.SGCCD          = B.SGCCD");
            query.AppendLine("            ,A.CAAPPLMETH     = B.CAAPPLMETH");
            query.AppendLine("            ,A.CAAPPLCLASS    = B.CAAPPLCLASS");
            query.AppendLine("            ,A.FRMYN          = B.FRMYN");
            query.AppendLine("            ,A.VSTCAYN        = B.VSTCAYN");
            query.AppendLine("            ,A.APPRVYN        = B.APPRVYN");
            query.AppendLine("            ,A.CALRGCD        = B.CALRGCD");
            query.AppendLine("            ,A.CAMIDCD        = B.CAMIDCD");
            query.AppendLine("            ,A.CAAPPLDT       = B.CAAPPLDT");
            query.AppendLine("            ,A.DMNO           = B.DMNO");
            query.AppendLine("            ,A.CANM           = B.CANM");
            query.AppendLine("            ,A.CAADDR         = B.CAADDR");
            query.AppendLine("            ,A.PRCSDT         = B.PRCSDT");
            query.AppendLine("            ,A.USERNM         = B.USERNM");
            query.AppendLine("            ,A.DEPT           = B.DEPT");
            query.AppendLine("            ,A.CACONT         = B.CACONT");
            query.AppendLine("            ,A.CAPRCSRSLT     = B.CAPRCSRSLT");
            query.AppendLine("            ,A.REGDT          = B.REGDT");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT (A.CANO, A.SGCCD, A.CAAPPLMETH, A.CAAPPLCLASS, A.FRMYN, A.VSTCAYN");
            query.AppendLine("         , A.APPRVYN, A.CALRGCD, A.CAMIDCD, A.CAAPPLDT, A.DMNO, A.CANM, A.CAADDR");
            query.AppendLine("         , A.PRCSDT, A.USERNM, A.DEPT, A.CACONT, A.CAPRCSRSLT, A.REGDT)");
            query.AppendLine("   VALUES (B.CANO, B.SGCCD, B.CAAPPLMETH, B.CAAPPLCLASS, B.FRMYN, B.VSTCAYN");
            query.AppendLine("         , B.APPRVYN, B.CALRGCD, B.CAMIDCD, B.CAAPPLDT, B.DMNO, B.CANM, B.CAADDR");
            query.AppendLine("         , B.PRCSDT, B.USERNM, B.DEPT, B.CACONT, B.CAPRCSRSLT, B.REGDT)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 민원기본정보2 (사용테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void MdfyCAINFO2(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO CAINFO A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT CANO, SGCCD, CAAPPLMETH, CAAPPLCLASS, FRMYN, VSTCAYN");
            query.AppendLine("             , APPRVYN, CALRGCD, CAMIDCD, TO_CHAR(CAAPPLDT, 'yyyymmdd hh24miss') CAAPPLDT, DMNO, CANM, CAADDR");
            query.AppendLine("             , TO_CHAR(PRCSDT, 'yyyymmdd') PRCSDT, USERNM, DEPT, CACONT, CAPRCSRSLT, TO_CHAR(REGDT, 'yyyy/mm/dd hh24:mi:ss') REGDT");
            query.AppendLine("        FROM IF_CAINFO");
            query.AppendLine("       WHERE IF_DATE = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.CANO = B.CANO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET");
            query.AppendLine("             A.SGCCD          = B.SGCCD");
            query.AppendLine("            ,A.CAAPPLMETH     = B.CAAPPLMETH");
            query.AppendLine("            ,A.CAAPPLCLASS    = B.CAAPPLCLASS");
            query.AppendLine("            ,A.FRMYN          = B.FRMYN");
            query.AppendLine("            ,A.VSTCAYN        = B.VSTCAYN");
            query.AppendLine("            ,A.APPRVYN        = B.APPRVYN");
            query.AppendLine("            ,A.CALRGCD        = B.CALRGCD");
            query.AppendLine("            ,A.CAMIDCD        = B.CAMIDCD");
            query.AppendLine("            ,A.CAAPPLDT       = B.CAAPPLDT");
            query.AppendLine("            ,A.DMNO           = B.DMNO");
            query.AppendLine("            ,A.CANM           = B.CANM");
            query.AppendLine("            ,A.CAADDR         = B.CAADDR");
            query.AppendLine("            ,A.PRCSDT         = B.PRCSDT");
            query.AppendLine("            ,A.USERNM         = B.USERNM");
            query.AppendLine("            ,A.DEPT           = B.DEPT");
            query.AppendLine("            ,A.CACONT         = B.CACONT");
            query.AppendLine("            ,A.CAPRCSRSLT     = B.CAPRCSRSLT");
            query.AppendLine("            ,A.REGDT          = B.REGDT");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT (A.CANO, A.SGCCD, A.CAAPPLMETH, A.CAAPPLCLASS, A.FRMYN, A.VSTCAYN");
            query.AppendLine("         , A.APPRVYN, A.CALRGCD, A.CAMIDCD, A.CAAPPLDT, A.DMNO, A.CANM, A.CAADDR");
            query.AppendLine("         , A.PRCSDT, A.USERNM, A.DEPT, A.CACONT, A.CAPRCSRSLT, A.REGDT)");
            query.AppendLine("   VALUES (B.CANO, B.SGCCD, B.CAAPPLMETH, B.CAAPPLCLASS, B.FRMYN, B.VSTCAYN");
            query.AppendLine("         , B.APPRVYN, B.CALRGCD, B.CAMIDCD, B.CAAPPLDT, B.DMNO, B.CANM, B.CAADDR");
            query.AppendLine("         , B.PRCSDT, B.USERNM, B.DEPT, B.CACONT, B.CAPRCSRSLT, B.REGDT)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 민원기본정보 삭제
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void DeleteCAINFO(EMapper manager, string dtStr)
        {
            string cof = "2";   // 삭제는 2일전 데이터를 삭제

            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_CAINFO WHERE IF_DATE < TO_DATE('" + dtStr + "', 'YYYYMMDD') - " + cof);

            manager.ExecuteScript(query.ToString(), null);

            // 민원기본정보 인덱스를 Rebuild한다.
            // 데이터가 쌓이고, 삭제되고를 반복하기 때문에 속도, 결과조회에 영향을 받기 때문
            manager.ExecuteScript("ALTER INDEX IF_CAINFO_PK REBUILD", null);
        }
        #endregion ###############################################

        #region 계량기교체내역 #########################################
        /// <summary>
        /// 계량기교체내역(임시테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dSet"></param>
        public void GetMTRCHGINFO(EMapper manager, DataSet dSet, DateTime dt)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" INSERT INTO IF_MTRCHGINFO (dmno,mtrchgserno,transclass,mddt,nwspipeszcd,transrs,regdt,if_date)");
            query.AppendLine(" 	 VALUES (");
            query.AppendLine(" 	 :1");
            query.AppendLine(" 	,:2");
            query.AppendLine(" 	,:3");
            query.AppendLine(" 	,:4");
            query.AppendLine(" 	,:5");
            query.AppendLine(" 	,:6");
            query.AppendLine(" 	,:7");
            query.AppendLine(" 	,:8");                      
            query.AppendLine(" )");

            DataRowCollection drc = dSet.Tables[0].Rows;
            foreach (DataRow row in drc)
            {
                IDataParameter[] parameters =  {                    
                     new OracleParameter("1", OracleDbType.Varchar2)
                    ,new OracleParameter("2", OracleDbType.Int32)
                    ,new OracleParameter("3", OracleDbType.Varchar2)
                    ,new OracleParameter("4", OracleDbType.Varchar2)
                    ,new OracleParameter("5", OracleDbType.Varchar2)
                    ,new OracleParameter("6", OracleDbType.Varchar2)
                    ,new OracleParameter("7", OracleDbType.Date)
                    ,new OracleParameter("8", OracleDbType.Date)
                    
                };

                for (int i = 1; i < dSet.Tables[0].Columns.Count; i++)
                {
                    if (i == 2)
                    {
                        parameters[i-1].Value = Convert.ToInt32(row[i].ToString());
                    }
                    else
                    {
                        parameters[i-1].Value = row[i].ToString();
                    }
                }
                parameters[7].Value = dt; // Convert.ToDateTime(dt);

                manager.ExecuteScript(query.ToString(), parameters);
            }
        }

        /// <summary>
        /// 계량기교체내역(사용테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void MdfyMTRCHGINFO(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO WI_MTRCHGINFO A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT");
            query.AppendLine("            DMNO");
            query.AppendLine("            ,MTRCHGSERNO");
            query.AppendLine("            ,TRANSCLASS");
            query.AppendLine("            ,MRYM");
            query.AppendLine("            ,MDDT");
            query.AppendLine("            ,OMFRNO");
            query.AppendLine("            ,OWSPIPESZCD");
            query.AppendLine("            ,WSRMVNDL");
            query.AppendLine("            ,NMFRNO");
            query.AppendLine("            ,NWSPIPESZCD");
            query.AppendLine("            ,WSATTNDL");
            query.AppendLine("            ,TRANSRS");
            query.AppendLine("            ,REGDT");
            query.AppendLine("        FROM IF_MTRCHGINFO");
            query.AppendLine("       WHERE IF_DATE = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.DMNO = B.DMNO");
            query.AppendLine("        AND A.MTRCHGSERNO = B.MTRCHGSERNO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET");
            query.AppendLine("            A.TRANSCLASS    = B.TRANSCLASS");
            query.AppendLine("            ,A.MRYM         = B.MRYM");
            query.AppendLine("            ,A.MDDT         = B.MDDT");
            query.AppendLine("            ,A.OMFRNO       = B.OMFRNO");
            query.AppendLine("            ,A.OWSPIPESZCD  = B.OWSPIPESZCD");
            query.AppendLine("            ,A.WSRMVNDL     = B.WSRMVNDL");
            query.AppendLine("            ,A.NMFRNO       = B.NMFRNO");
            query.AppendLine("            ,A.NWSPIPESZCD  = B.NWSPIPESZCD");
            query.AppendLine("            ,A.WSATTNDL     = B.WSATTNDL");
            query.AppendLine("            ,A.TRANSRS      = B.TRANSRS");
            query.AppendLine("            ,A.REGDT        = B.REGDT");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT ( A.DMNO");
            query.AppendLine("            ,A.MTRCHGSERNO");
            query.AppendLine("            ,A.TRANSCLASS");
            query.AppendLine("            ,A.MRYM");
            query.AppendLine("            ,A.MDDT");
            query.AppendLine("            ,A.OMFRNO");
            query.AppendLine("            ,A.OWSPIPESZCD");
            query.AppendLine("            ,A.WSRMVNDL");
            query.AppendLine("            ,A.NMFRNO");
            query.AppendLine("            ,A.NWSPIPESZCD");
            query.AppendLine("            ,A.WSATTNDL");
            query.AppendLine("            ,A.TRANSRS");
            query.AppendLine("            ,A.REGDT)");
            query.AppendLine("   VALUES ( B.DMNO");
            query.AppendLine("            ,B.MTRCHGSERNO");
            query.AppendLine("            ,B.TRANSCLASS");
            query.AppendLine("            ,B.MRYM");
            query.AppendLine("            ,B.MDDT");
            query.AppendLine("            ,B.OMFRNO");
            query.AppendLine("            ,B.OWSPIPESZCD");
            query.AppendLine("            ,B.WSRMVNDL");
            query.AppendLine("            ,B.NMFRNO");
            query.AppendLine("            ,B.NWSPIPESZCD");
            query.AppendLine("            ,B.WSATTNDL");
            query.AppendLine("            ,B.TRANSRS");
            query.AppendLine("            ,B.REGDT)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 계량기교체내역2 (사용테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void MdfyMTRCHGINFO2(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO MTRCHGINFO A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT");
            query.AppendLine("            DMNO");
            query.AppendLine("            ,MTRCHGSERNO");
            query.AppendLine("            ,TRANSCLASS");
            query.AppendLine("            ,MRYM");
            query.AppendLine("            ,MDDT");
            query.AppendLine("            ,OMFRNO");
            query.AppendLine("            ,OWSPIPESZCD");
            query.AppendLine("            ,WSRMVNDL");
            query.AppendLine("            ,NMFRNO");
            query.AppendLine("            ,NWSPIPESZCD");
            query.AppendLine("            ,WSATTNDL");
            query.AppendLine("            ,TRANSRS");
            query.AppendLine("            ,REGDT");
            query.AppendLine("        FROM IF_MTRCHGINFO");
            query.AppendLine("       WHERE IF_DATE = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.DMNO = B.DMNO");
            query.AppendLine("        AND A.MTRCHGSERNO = B.MTRCHGSERNO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET");
            query.AppendLine("            A.TRANSCLASS    = B.TRANSCLASS");
            query.AppendLine("            ,A.MRYM         = B.MRYM");
            query.AppendLine("            ,A.MDDT         = B.MDDT");
            query.AppendLine("            ,A.OMFRNO       = B.OMFRNO");
            query.AppendLine("            ,A.OWSPIPESZCD  = B.OWSPIPESZCD");
            query.AppendLine("            ,A.WSRMVNDL     = B.WSRMVNDL");
            query.AppendLine("            ,A.NMFRNO       = B.NMFRNO");
            query.AppendLine("            ,A.NWSPIPESZCD  = B.NWSPIPESZCD");
            query.AppendLine("            ,A.WSATTNDL     = B.WSATTNDL");
            query.AppendLine("            ,A.TRANSRS      = B.TRANSRS");
            query.AppendLine("            ,A.REGDT        = B.REGDT");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT ( A.DMNO");
            query.AppendLine("            ,A.MTRCHGSERNO");
            query.AppendLine("            ,A.TRANSCLASS");
            query.AppendLine("            ,A.MRYM");
            query.AppendLine("            ,A.MDDT");
            query.AppendLine("            ,A.OMFRNO");
            query.AppendLine("            ,A.OWSPIPESZCD");
            query.AppendLine("            ,A.WSRMVNDL");
            query.AppendLine("            ,A.NMFRNO");
            query.AppendLine("            ,A.NWSPIPESZCD");
            query.AppendLine("            ,A.WSATTNDL");
            query.AppendLine("            ,A.TRANSRS");
            query.AppendLine("            ,A.REGDT)");
            query.AppendLine("   VALUES ( B.DMNO");
            query.AppendLine("            ,B.MTRCHGSERNO");
            query.AppendLine("            ,B.TRANSCLASS");
            query.AppendLine("            ,B.MRYM");
            query.AppendLine("            ,B.MDDT");
            query.AppendLine("            ,B.OMFRNO");
            query.AppendLine("            ,B.OWSPIPESZCD");
            query.AppendLine("            ,B.WSRMVNDL");
            query.AppendLine("            ,B.NMFRNO");
            query.AppendLine("            ,B.NWSPIPESZCD");
            query.AppendLine("            ,B.WSATTNDL");
            query.AppendLine("            ,B.TRANSRS");
            query.AppendLine("            ,B.REGDT)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 계량기교체내역 삭제
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void DeleteMTRCHGINFO(EMapper manager, string dtStr)
        {
            string cof = "2";   // 삭제는 2일전 데이터를 삭제

            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_MTRCHGINFO WHERE IF_DATE < TO_DATE('" + dtStr + "', 'YYYYMMDD') - " + cof);

            manager.ExecuteScript(query.ToString(), null);

            // 계량기교체내역 인덱스를 Rebuild한다.
            // 데이터가 쌓이고, 삭제되고를 반복하기 때문에 속도, 결과조회에 영향을 받기 때문
            manager.ExecuteScript("ALTER INDEX IF_MTRCHGINFO_PK REBUILD", null);
        }
        #endregion ###############################################

        #region 요금조정 #########################################
        /// <summary>
        /// 요금조정(임시테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dSet"></param>
        public void GetSTWCHRG(EMapper manager, DataSet dSet, DateTime dt)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" INSERT INTO IF_STWCHRG (stym,dmno,wsusevol,wsstvol,regdt,if_date)");
            query.AppendLine(" 	 VALUES (");
            query.AppendLine(" 	 :1");
            query.AppendLine(" 	,:2");
            query.AppendLine(" 	,:3");
            query.AppendLine(" 	,:4");
            query.AppendLine(" 	,:5");
            query.AppendLine(" 	,:6");
            query.AppendLine(" )");

            DataRowCollection drc = dSet.Tables[0].Rows;
            foreach (DataRow row in drc)
            {
                IDataParameter[] parameters =  {
                     new OracleParameter("1", OracleDbType.Varchar2)
                    ,new OracleParameter("2", OracleDbType.Varchar2)
                    ,new OracleParameter("3", OracleDbType.Int32)
                    ,new OracleParameter("4", OracleDbType.Int32)
                    ,new OracleParameter("5", OracleDbType.Date)
                    ,new OracleParameter("6", OracleDbType.Date)
                };

                for (int i = 1; i < dSet.Tables[0].Columns.Count; i++)
                {
                    if (i == 1)
                    {
                        DateTime stym = DateTime.ParseExact(row[i].ToString(), "yyyyMM", null).AddMonths(-1);
                        parameters[i - 1].Value = stym.ToString("yyyyMM");
                    }
                    else if (i == 3 || i == 4)
                    {
                        parameters[i-1].Value = Convert.ToInt32(row[i].ToString());
                    }                  
                    else
                    {
                        parameters[i-1].Value = row[i].ToString();
                    }
                }
                parameters[5].Value = dt; // Convert.ToDateTime(dt);

                manager.ExecuteScript(query.ToString(), parameters);
            }
        }

        /// <summary>
        /// 요금조정(사용테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void MdfySTWCHRG(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO WI_STWCHRG A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT");
            query.AppendLine("            STYM,");
            query.AppendLine("            DMNO,");
            query.AppendLine("            WSUSEVOL,");
            query.AppendLine("            WSSTVOL,");
            query.AppendLine("            REGDT");
            query.AppendLine("        FROM IF_STWCHRG");
            query.AppendLine("       WHERE IF_DATE = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.STYM = B.STYM");
            query.AppendLine("        AND A.DMNO = B.DMNO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET A.WSUSEVOL = B.WSUSEVOL");
            query.AppendLine("            , A.WSSTVOL = B.WSSTVOL");
            query.AppendLine("            , A.REGDT = B.REGDT");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT (A.STYM, A.DMNO, A.WSUSEVOL, A.WSSTVOL, A.REGDT)");
            query.AppendLine("   VALUES (B.STYM, B.DMNO, B.WSUSEVOL, B.WSSTVOL, B.REGDT)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 요금조정2(사용테이블)
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void MdfySTWCHRG2(EMapper manager, string dtStr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO STWCHRG A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT");
            query.AppendLine("            STYM,");
            query.AppendLine("            DMNO,");
            query.AppendLine("            WSUSEVOL,");
            query.AppendLine("            WSSTVOL,");
            query.AppendLine("            REGDT");
            query.AppendLine("        FROM IF_STWCHRG");
            query.AppendLine("       WHERE IF_DATE = TO_DATE('" + dtStr + "', 'YYYYMMDDHH24MISS')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON ( A.STYM = B.STYM");
            query.AppendLine("        AND A.DMNO = B.DMNO");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET A.WSUSEVOL = B.WSUSEVOL");
            query.AppendLine("            , A.WSSTVOL = B.WSSTVOL");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("   INSERT (A.STYM, A.DMNO, A.WSUSEVOL, A.WSSTVOL)");
            query.AppendLine("   VALUES (B.STYM, B.DMNO, B.WSUSEVOL, B.WSSTVOL)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 요금조정 삭제
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="dtStr"></param>
        internal void DeleteSTWCHRG(EMapper manager, string dtStr)
        {
            string cof = "2";   // 삭제는 2일전 데이터를 삭제

            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_STWCHRG WHERE IF_DATE < TO_DATE('" + dtStr + "', 'YYYYMMDD') - " + cof);

            manager.ExecuteScript(query.ToString(), null);

            // 요금조정 인덱스를 Rebuild한다.
            // 데이터가 쌓이고, 삭제되고를 반복하기 때문에 속도, 결과조회에 영향을 받기 때문
            manager.ExecuteScript("ALTER INDEX IF_STWCHRG_PK REBUILD", null);
        }
        #endregion ###############################################
    }
}
