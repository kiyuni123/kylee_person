﻿using System;
using System.Threading;

namespace WaterNETTest
{
    public class ThreadExam2
    {
        static int i = 0;

        public static void Print1(object obj)
        {
            for(i = 0 ; i < 3 ; i++)
            {
                Console.WriteLine("첫번째 Thread :{0} ***", i);
                Thread.Sleep(100);  // 0.1초 동안 쓰레드 정지
            }
        }

        public void Print2(object obj)
        {
            for(i = 0 ; i < 3 ; i++)
            {
                Console.WriteLine("두번째 Thread :{0} ***", i);
                Thread.Sleep(100);  // 0.1초 동안 쓰레드 정지
            }
        }


        public void myMain()
        {
            // static method를 이용한 ThreadPool에 대한 작업 요청
            ThreadPool.QueueUserWorkItem(new WaitCallback(Print1), null);

            // ThreadPool에 대한 작업 요청
            ThreadPool.QueueUserWorkItem(new WaitCallback((new ThreadExam2()).Print2), null);


            for(i = 0 ; i < 3 ; i++)
            {
                Console.WriteLine("Main :{0} ", i);
                Thread.Sleep(100);  // 0.1초 동안 쓰레드 정지
            }
        }
    }
}
