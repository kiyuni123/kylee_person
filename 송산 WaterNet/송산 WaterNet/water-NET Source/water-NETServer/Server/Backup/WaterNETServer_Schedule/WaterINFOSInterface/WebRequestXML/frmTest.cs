﻿using System.Windows.Forms;

namespace WaterNETTest
{
    public partial class frmTest : Form
    {
        public frmTest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            ThreadExam1 te = new ThreadExam1();
            te.myMain();
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            ThreadExam2 te = new ThreadExam2();
            te.myMain();
        }
      


    }
}