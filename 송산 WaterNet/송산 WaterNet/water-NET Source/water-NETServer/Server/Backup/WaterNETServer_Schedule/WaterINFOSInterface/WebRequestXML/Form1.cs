﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using System.IO;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Text.RegularExpressions;
using WaterNETServer.Common.utils;
using WaterNETServer.Common;

namespace WaterNETTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            
            InitializeComponent();
        }


       

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            xmlParse4(textBox3.Text);
        }

        private void myStringToken()
        {
            string str = "실시간 데이터 미수집(2011년04월05일 20시02분):1 건:->복구::0 건:";

            Console.WriteLine("# {0}", str.IndexOf(":", StringComparison.Ordinal));
            Console.WriteLine(str.Substring(0, 31));

            for (int i = 0; i < str.Length; i++)
            {

            }

            int j = 0;
            while ((j = str.IndexOf(':', j)) > -1)
            {
                Console.WriteLine(str.Substring(j));

                j++;
            }

            Console.WriteLine("########## ");


            ArrayList tmp = new ArrayList();
            int idx = 0;
            int next = 0;

            while ((next = str.IndexOf(':', idx)) > -1)
            {
                Console.WriteLine("->" + str.Substring(idx, next - idx));
                //tmp.Add(str.Substring(idx, next));

                idx = next + 1;
            }
            if (idx < str.Length)
            {
                Console.WriteLine("->" + str.Substring(idx));
                //tmp.Add(str.Substring(idx));
            }

            for (int i = 0; i < tmp.Count; i++)
            {
                Console.WriteLine(tmp[i]);
            }
        }

        private void Test_log()
        {
            //log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(Application.StartupPath + @"\Config\log4net.xml"));

            //log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            //logger.Debug("Debug : Hello Log4Net");
            //logger.Info("Info : Hello Log4Net");
            //logger.Warn("Warn : Hello Log4Net");
            //logger.Error("Error : Hello Log4Net");
            //logger.Fatal("Fatal : Hello Log4Net");
        }

        private void Test_DateTime()
        {
            // yyyyMMddHHmm
            Console.WriteLine(String.Format("{0:yyyy}", DateTime.Now));
            Console.WriteLine(String.Format("{0:MM}", DateTime.Now));
            Console.WriteLine(String.Format("{0:dd}", DateTime.Now));
        }

        /// <summary>
        /// Hashtable 연습
        /// </summary>
        private void Test_Hashtable()
        {
            Hashtable htable = new Hashtable();

            htable.Remove("_ModifyRealtime");
            htable.Add("_ModifyRealtime", "RUN");

            //Console.WriteLine("1 : {0} ", htable["_ModifyRealtime"].ToString());

            foreach (DictionaryEntry de in htable)
            {
                Console.WriteLine("1 : Key = {0}, Value = {1}", de.Key, de.Value);
            }

            htable.Remove("_ModifyRealtime");
            htable.Add("_ModifyRealtime", "STOP");
            foreach (DictionaryEntry de in htable)
            {
                Console.WriteLine("2 : Key = {0}, Value = {1}", de.Key, de.Value);
            }

            Console.WriteLine("1 : {0} ", htable["_ModifyRealtime"].ToString());
        }

        public void xmlParse4(string cond)
        {
            try
            {
                XmlDocument m_doc = new XmlDocument();
                m_doc.Load(AppStatic.GLOBAL_CONFIG_FILE_PATH);

                string key = "/Root/schedule/job";

                XmlNodeList nodes = m_doc.SelectNodes(key);

                Hashtable rtnHash = new Hashtable();

                foreach (XmlNode node in nodes)
                {
                    if (cond.Equals(node["number"].InnerText))
                    {
                        rtnHash["type"] = node["type"].InnerText;
                        rtnHash["exectime"] = node["exectime"].InnerText;
                    }
                    //Console.WriteLine("1" + node["number"].InnerText);
                    //Console.WriteLine("2" + node["type"].InnerText);
                    //Console.WriteLine("3" + node["exectime"].InnerText);
                }

                Console.WriteLine(rtnHash["type"].ToString());
                Console.WriteLine(rtnHash["exectime"].ToString());
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.ToString());
            }
        }

        public void xmlParse3(string cond)
        {
            string xmlfile = "D:\\WaterNET.Server\\ScheduleManager\\bin\\Debug\\Config\\a.xml";
            XmlTextReader reader = new XmlTextReader(xmlfile);

            Hashtable h_node = new Hashtable();


            bool aa = false;

            while (reader.Read())
            {

                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        h_node[reader.Name] = true;
                        break;
                    case XmlNodeType.Text:
                        if (Convert.ToBoolean(h_node["Root"]) && Convert.ToBoolean(h_node["schedule"]) && Convert.ToBoolean(h_node["job"]) && Convert.ToBoolean(h_node["number"]))
                        {
                            if (reader.Value.Equals(cond))
                            {
                                Console.WriteLine("같아욤");
                                aa = true;
                                
                            }

                        }
                        if (aa)
                        {
                            Console.WriteLine(reader.Value);
                        }
                        break;
                    case XmlNodeType.EndElement:
                        h_node[reader.Name] = false;
                        if (Convert.ToBoolean(h_node["Root"]) && Convert.ToBoolean(h_node["schedule"]) && !Convert.ToBoolean(h_node["job"]))
                        {
                            aa = false;
                        }
                        break;
                }

            }
        }

        public void xmlParse2()
        {
            XmlDocument m_doc = new XmlDocument();
            m_doc.Load(AppStatic.GLOBAL_CONFIG_FILE_PATH);

            string key = "/Root/schedule/job/number";

            XmlNodeList nodes = m_doc.SelectNodes(key);

            string value = string.Empty;

            foreach (XmlNode node in nodes)
            {
                if ("0001".Equals(node.InnerText))
                {
                    Console.WriteLine(node.InnerText);
                    Console.WriteLine(node.Attributes);
                    Console.WriteLine(node.ChildNodes);
                    Console.WriteLine(node.Name);
                    Console.WriteLine(node.Value);


                    Console.WriteLine(node.ChildNodes.Count);
                }
            }

        }

        public void xmlParse()
        {
            try
            {
                string xmlfile = "D:\\WaterNET.Server\\ScheduleManager\\bin\\Debug\\Config\\Global_Config.xml";
                XmlTextReader reader = new XmlTextReader(xmlfile);

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            Console.WriteLine("Element Name: {0}", reader.Name);
                            Console.WriteLine("Element Value: {0}", reader.Value);
                            break;
                        case XmlNodeType.EndElement:
                            Console.WriteLine("EndElement Name: {0}", reader.Name);
                            Console.WriteLine("EndElement Value: {0}", reader.Value);
                            break;
                        case XmlNodeType.Text:
                            Console.WriteLine("Text Name: {0}", reader.Name);
                            Console.WriteLine("Text Value: {0}", reader.Value);
                            break;
                        case XmlNodeType.Attribute:
                            Console.WriteLine("Attribute Name: {0}", reader.Name);
                            Console.WriteLine("Attribute Value: {0}", reader.Value);
                            break;
                        case XmlNodeType.Comment:
                            Console.WriteLine("Comment Name: {0}", reader.Name);
                            Console.WriteLine("Comment Value: {0}", reader.Value);
                            break;
                        default:
                            Console.WriteLine("default Name: {0}", reader.Name);
                            Console.WriteLine("default Value: {0}", reader.Value);
                            break;
                    }
                }

                Console.WriteLine("###########################################");

                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.Load(xmlfile);
                //XmlElement root = xmldoc.DocumentElement;

                //XmlNodeList nodes = root.ChildNodes;

                //foreach (XmlNode node in nodes)
                //{
                //    Console.WriteLine("'{0}', '{1}'", node.Name, node.InnerXml);
                //}
            }
            catch (XmlException e)
            {
                Console.WriteLine("{0}", e.Message);
            }
        }


        /// <summary>
        /// XML 파일에서 xpath에 의한 값을 리턴한다.
        /// </summary>
        /// <param name="key">xpath</param>
        /// <returns>노드 값(여기서는 설정 값)</returns>
        public ArrayList getProp(string key)
        {
            string xmlfile = "E:\\Download\\2010-12-15_10_10_11.xml";
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(xmlfile);

            ArrayList rtValue = new ArrayList();

            XmlNodeList nodes = xdoc.SelectNodes(key);

            foreach (XmlNode node in nodes)
            {
                rtValue.Add(node.InnerText);
            }
            return rtValue;
        }

        public void XPathRead()
        {
            //ArrayList arr = getProp("/Root/ID/주_보구분");
            //foreach (Object a in arr)
            //{
            //    Console.WriteLine(a.ToString());
            //}

            string[] field_list = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"};

            ReadXML(field_list);
        }

        private ArrayList ReadXML(string[] field_list)
        {
            ArrayList result = new ArrayList();
            string xmlfile = "E:\\Download\\2010-12-15_10_10_11.xml";

            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(xmlfile);
                XmlElement root = xmldoc.DocumentElement;

                XmlNodeList nodes = root.ChildNodes;

                //foreach (XmlNode node in nodes)
                //{
                //    if(node.Name == "ID")
                //    {
                //        Console.WriteLine("ka");
                //        string[] row = new string[field_list.Length];
                //        for(int i = 0; i < field_list.Length; i++)
                //        {
                //            row[i] = node[field_list[i]].InnerText;

                //            Console.WriteLine(row[i]);
                //        }
                //        result.Add(row);
                //    }
                //}

                foreach (XmlNode node in nodes)
                {
                    Console.WriteLine("'{0}', '{1}'", node.Name, node.InnerXml);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            return result;
        }

        public ArrayList ReadXML2ArrayList()
        {
            ArrayList returnValue = new ArrayList();

            XmlTextReader reader = new XmlTextReader("E:\\Download\\2010-12-15_14_58_36.xml");

            int idcnt = 0;
            int arrcnt = 0;
            bool idstart = false;
            string columnName = "";

            string [] columnList = new string [19];

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: 
                        if (reader.Name == "ID")
                        {
                            idcnt++;
                            idstart = true;
                        }
                        columnName = reader.Name;

                        break;
                    case XmlNodeType.Text: // The node is a Text
                        if(idstart)
                        {
                            columnList[arrcnt++] = reader.Value;
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if(reader.Name == "ID")
                        {
                            arrcnt = 0;
                            idstart = false;

                            returnValue.Add(columnList);
                            columnList = new string [19];
                        }
                        break;
                }

            }

            reader.Close();

            return returnValue;
        }

        public void xml1()
        {
            XmlTextReader reader = new XmlTextReader("E:\\Download\\2010-12-15_10_10_11.xml");

            int idcnt = 0;
            bool idstart = false;

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an Element
                        //Console.Write("<" + reader.Name);

                        if (reader.Name == "ID")
                        {
                            idcnt++;
                            idstart = true;
                        }

                        // Read Attribute
                        //while (reader.MoveToNextAttribute()) // Read attributes
                        //    Console.Write(" " + reader.Name + " = '" + reader.Value + "'");
                        Console.Write(">");
                        break;
                    case XmlNodeType.Text: // The node is a Text
                        if(idstart)
                        {
                            
                        }
                        Console.Write(reader.Value);
                        break;
                    case XmlNodeType.Comment:
                        // Comment : reader.value
                        break;
                    case XmlNodeType.EndElement:
                        // EndElement : reader.Name
                        // Console.WriteLine("</" + reader.Name + ">");
                        if(reader.Name == "ID")
                        {
                            idstart = false;
                        }
                        break;
                }
            }

            reader.Close();
        }

        /// <summary>
        /// 그냥 파일 다운로드
        /// </summary>
        public void simpledownload()
        {
            try
            {
                string dstr = DateTime.Now.Year.ToString() + "-"+DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "_" +DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString()+"_"+DateTime.Now.Second.ToString()+".xml";
                string url = "http://localhost:8080/suyoungga.xml";
                string downfile = "E:\\download\\" + dstr;

                //Console.WriteLine(downfile);

                DateTime startTime = DateTime.Now;

                WebClient webclient = new WebClient();
                webclient.DownloadFile(url, downfile);

                DateTime endTime = DateTime.Now;


                Console.WriteLine("프로그램 수행시간 : {0}/ms", (double)(endTime - startTime).Ticks / 1000000.0F);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message.ToString());
            }

        }

        /// <summary>
        /// HTTP Request 스트림 다운로드1
        /// </summary>
        public void WebFetch()
        {
            StringBuilder sb = new StringBuilder();

            byte[] buf = new byte[8192];

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:8080/suyoungga.xml");

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream resStream = response.GetResponseStream();

            string tempString = null;

            int count = 0;

            DateTime startTime = DateTime.Now;

            do
            {
                count = resStream.Read(buf, 0, buf.Length);

                if(count != 0)
                {
                    //tempString = Encoding.ASCII.GetString(buf, 0, count);
                    //tempString = Encoding.Unicode.GetString(buf, 0, count);
                    //tempString = Encoding.UTF8.GetString(buf, 0, count);
                    //tempString = Encoding.BigEndianUnicode.GetString(buf, 0, count);
                    tempString = Encoding.Default.GetString(buf, 0, count);

                    sb.Append(tempString);
                }
            } while (count > 0);

            DateTime endTime = DateTime.Now;

            Console.WriteLine("프로그램 수행시간 : {0}/ms", (double)(endTime - startTime).Ticks / 1000000.0F);

            Console.WriteLine(sb.ToString());
        }

        /// <summary>
        /// 인코딩을 아직 못맞춤
        /// post 방식 요청은 이렇게 하면될듯?
        /// </summary>
        public void WebSend()
        {
            String[] messages = new String[6];
            messages[0] = "행정과";         // 아이디
            messages[1] = "암호";           // 암호
            messages[2] = "111111";         // 보내는 사람 번호
            messages[3] = "222222";         // 받는 사람 번호
            messages[4] = "테스트메시지";   // 메시지
            messages[5] = "홍길동";         // 받는 사람 이름

            String postData = String.Format("id={0}&pw={1}&sender={2}&receiver={3}&msg={4}&rename={5}"
                , messages[0], messages[1], messages[2], messages[3], messages[4], messages[5]);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:8080/suyoungga.xml");

            // 인코딩 1 UTF-8
            byte[] sendData = UTF8Encoding.UTF8.GetBytes(postData);
            httpWebRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";

            // 인코딩 2 EUC-KR 
            //byte[] sendData = Encoding.GetEncoding("EUC-KR").GetBytes(postData);
            //httpWebRequest.ContentType = "application/x-www-form-urlencoded; charset=EUC-KR";

            httpWebRequest.Method = "POST";

            // Set the content length of the string being posted.
            httpWebRequest.ContentLength = sendData.Length;

            Stream requestStream = httpWebRequest.GetRequestStream();
            requestStream.Write(sendData, 0, sendData.Length);
            requestStream.Close();


            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
            //Encoding.GetEncoding("EUC-KR");
            Encoding.GetEncoding("utf-8");

            string html = streamReader.ReadToEnd();
            streamReader.Close();
            httpWebResponse.Close();

            // text.Value = html;

            Console.WriteLine(html);
        }


        /// <summary>
        /// HttpWebResponse를 Stream에 담은뒤 DataSet에 바로 담아서 사용한다
        /// </summary>
        public void WebXmlDataSet()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:8080/test.xml");

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream resStream = response.GetResponseStream();

            DataSet dSet = new DataSet();

            dSet.ReadXml(resStream);

            /*
             * 무한 Depth의 XML 을 가져온다
            DataTableCollection dta = dSet.Tables;
            foreach (DataTable dt in dta)
            {
                DataRowCollection drc = dSet.Tables[dt.TableName].Rows;

                foreach (DataRow row in drc)
                {
                    // 그때는 컬럼 수가 다른데 어떻게 가져오지?
                    for (int i = 0; i < 5; i++)
                    {
                        Console.WriteLine("## {0} : {1} : ", i, row[i]);
                    }
                }
            }
            */
            /*
             * Table[0] 은 1Depth의 XML을 가져온다
             * Table[1] 부터는 2 Depth의 XML을 가져온다
            DataRowCollection drc = dSet.Tables[1].Rows;
            foreach (DataRow row in drc)
            {
                for (int i = 0; i < 2; i++ )
                {
                    Console.WriteLine("## {0} : {1} : ", i, row[i]);
                }
            }
            */
        }

        public string GetFirstIPv4()
        {
            Regex regex = new Regex(@"^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$");
            foreach (System.Net.IPAddress ip in System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList)
            {
                if (regex.IsMatch(ip.ToString()))
                {
                    return ip.ToString();
                }
            }
            return null;
        }

        private static string _strSGCCD = "";  // 지자체코드
        GlobalVariable gVar = new GlobalVariable();


        private void WebServiceTest()
        {
            _strSGCCD = gVar.getSgccdCode();

            textBox1.Text = _strSGCCD;

            DataSet dSet = null;

//            DataSet dSet = infos.GetDMINFO(utils.GetFirstIPv4(), textBox1.Text, dateTimePicker1.Value.ToString("yyyyMMdd"), dateTimePicker2.Value.ToString("yyyyMMdd"), textBox2.Text);


            try
            {
                   

               // Console.WriteLine("############################ " + GetFirstIPv4());

                //using (WaterNETTest.WaterINFOS.Service1SoapClient client = new WaterNETTest.WaterINFOS.Service1SoapClient())
                //{
                //    Console.WriteLine("#1#"+GetFirstIPv4());
                //    Console.WriteLine("#2#"+textBox1.Text);
                //    Console.WriteLine("#3#"+dateTimePicker1.Value.ToString("yyyyMMdd"));
                //    Console.WriteLine("#4#"+dateTimePicker2.Value.ToString("yyyyMMdd"));
                //    Console.WriteLine("#5#" + textBox2.Text);
                //    dSet = client.GetDMINFO(GetFirstIPv4(), textBox1.Text, dateTimePicker1.Value.ToString("yyyyMMdd"), dateTimePicker2.Value.ToString("yyyyMMdd"), textBox2.Text);
                //}


                DataTableCollection dta = dSet.Tables;
                foreach (DataTable dt in dta)
                {
                    DataRowCollection drc = dSet.Tables[dt.TableName].Rows;

                    foreach (DataRow row in drc)
                    {
                        // 그때는 컬럼 수가 다른데 어떻게 가져오지?
                        for (int i = 0; i < 11; i++)
                        {
                            //Console.WriteLine("## {0} : {1} : ", i, row[i]);
                            Console.Write("##{0}:{1}", i, row[i]);
                        }
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception ee)
            {
                Debug.WriteLine(ee.ToString());
            }



        }

        private void button7_Click(object sender, EventArgs e)
        {
            WebServiceTest();
        }





    }
}
