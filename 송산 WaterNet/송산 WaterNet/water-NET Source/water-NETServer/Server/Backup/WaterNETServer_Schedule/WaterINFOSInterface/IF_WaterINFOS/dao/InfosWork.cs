﻿using System;
using System.Data;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;

namespace IF_WaterINFOS.dao
{
    public class InfosWork : BaseWork
    {
        private InfosDao iDao = null;
        public InfosWork()
        {
            iDao = new InfosDao();
        }

        #region 수용가정보 ################################
        /// <summary>
        /// 수용가정보(임시테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void GetDMINFO(DataSet dSet, DateTime dt)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.GetDMINFO(manager, dSet, dt);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 수용가정보 블록 코드 변환(사용테이블)
        /// </summary>
        /// <param name="dtStr">I/F Date</param>
        public void MdfyDMINFO(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfyDMINFO(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 수용가정보2 블록 코드 변환(사용테이블)
        /// </summary>
        /// <param name="dtStr">I/F Date</param>
        public void MdfyDMINFO2(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfyDMINFO2(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 수용가정보 임시테이블 삭제
        /// </summary>
        /// <param name="dtStr"></param>
        public void DeleteDMINFO(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.DeleteDMINFO(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        #endregion #####################################

        #region 상하수도자원 ################################
        /// <summary>
        /// 상하수도자원(임시테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void GetDMWSRSRC(DataSet dSet, DateTime dt)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.GetDMWSRSRC(manager, dSet, dt);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 상하수도자원 (사용테이블)
        /// </summary>
        /// <param name="dtStr">I/F Date</param>
        public void MdfyDMWSRSRC(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfyDMWSRSRC(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 상하수도자원2 (사용테이블)
        /// </summary>
        /// <param name="dtStr">I/F Date</param>
        public void MdfyDMWSRSRC2(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfyDMWSRSRC2(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 상하수도자원 임시테이블 삭제
        /// </summary>
        /// <param name="dtStr"></param>
        public void DeleteDMWSRSRC(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.DeleteDMWSRSRC(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        #endregion #####################################

        #region 민원기본정보 ################################
        /// <summary>
        /// 민원기본정보(임시테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void GetCAINFO(DataSet dSet, DateTime dt)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.GetCAINFO(manager, dSet, dt);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 민원기본정보(사용테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void MdfyCAINFO(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfyCAINFO(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 민원기본정보2(사용테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void MdfyCAINFO2(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfyCAINFO2(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 민원기본정보 임시테이블 삭제
        /// </summary>
        /// <param name="dtStr"></param>
        public void DeleteCAINFO(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.DeleteCAINFO(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }
        #endregion #####################################

        #region 계량기교체정보 ################################
        /// <summary>
        /// 계량기교체정보(임시테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void GetMTRCHGINFO(DataSet dSet, DateTime dt)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.GetMTRCHGINFO(manager, dSet, dt);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 계량기교체정보(사용테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void MdfyMTRCHGINFO(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfyMTRCHGINFO(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 계량기교체정보2(사용테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void MdfyMTRCHGINFO2(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfyMTRCHGINFO2(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 계량기교체정보 임시테이블 삭제
        /// </summary>
        /// <param name="dtStr"></param>
        public void DeleteMTRCHGINFO(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.DeleteMTRCHGINFO(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }
        #endregion #####################################

        #region 요금조정 ################################
        /// <summary>
        /// 요금조정(임시테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void GetSTWCHRG(DataSet dSet, DateTime dt)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.GetSTWCHRG(manager, dSet, dt);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 요금조정(사용테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void MdfySTWCHRG(String dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfySTWCHRG(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 요금조정2(사용테이블)
        /// </summary>
        /// <param name="dSet"></param>
        public void MdfySTWCHRG2(String dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.MdfySTWCHRG2(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 요금조정 임시테이블 삭제
        /// </summary>
        /// <param name="dtStr"></param>
        public void DeleteSTWCHRG(string dtStr)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                iDao.DeleteSTWCHRG(manager, dtStr);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }
        #endregion #####################################
    }
}