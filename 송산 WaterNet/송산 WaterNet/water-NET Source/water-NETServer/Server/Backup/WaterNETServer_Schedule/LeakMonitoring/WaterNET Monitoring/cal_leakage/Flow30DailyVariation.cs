﻿using System;
using System.Data;

using WaterNETServer.LeakMonitoring.utils;
using WaterNETServer.LeakMonitoring.interface1;

namespace WaterNETServer.LeakMonitoring.cal_leakage
{
    public class Flow30DailyVariation : ICal
    {
        private string type = "1120";
        private int count = 0;
        private string result = "N";
        private int count_max = 25;

        public Flow30DailyVariation(DateTime dateTime, DataTable thisYearFlow)
        {
            this.Cal(dateTime, thisYearFlow);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearFlow)
        {
            if (thisYearFlow == null || thisYearFlow.Rows.Count == 0)
            {
                return;
            }

            double todayFlow = 0;
            double minusday01Flow = 0;
            double minusday02Flow = 0;
            double minusday03Flow = 0;
            double minusday04Flow = 0;
            double minusday05Flow = 0;
            double minusday06Flow = 0;
            double minusday07Flow = 0;
            double minusday08Flow = 0;
            double minusday09Flow = 0;
            double minusday10Flow = 0;
            double minusday11Flow = 0;
            double minusday12Flow = 0;
            double minusday13Flow = 0;
            double minusday14Flow = 0;
            double minusday15Flow = 0;
            double minusday16Flow = 0;
            double minusday17Flow = 0;
            double minusday18Flow = 0;
            double minusday19Flow = 0;
            double minusday20Flow = 0;
            double minusday21Flow = 0;
            double minusday22Flow = 0;
            double minusday23Flow = 0;
            double minusday24Flow = 0;
            double minusday25Flow = 0;
            double minusday26Flow = 0;
            double minusday27Flow = 0;
            double minusday28Flow = 0;
            double minusday29Flow = 0;
            double minusday30Flow = 0;

            foreach (DataRow row in thisYearFlow.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayFlow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday01Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday02Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday03Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday04Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday05Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday06Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday07Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday08Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday09Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday10Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday11Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday12Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday13Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday14Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-15).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday15Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-16).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday16Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-17).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday17Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-18).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday18Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-19).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday19Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-20).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday20Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-21).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday21Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-22).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday22Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-23).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday23Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-24).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday24Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-25).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday25Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-26).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday26Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-27).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday27Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-28).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday28Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-29).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday29Flow = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-30).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday30Flow = Utils.ToDouble(row["VALUE"]);
                }
            }

            int count = 0;

            if (minusday01Flow < todayFlow)
            {
                count++;
            }
            if (minusday02Flow < todayFlow)
            {
                count++;
            }
            if (minusday03Flow < todayFlow)
            {
                count++;
            }
            if (minusday04Flow < todayFlow)
            {
                count++;
            }
            if (minusday05Flow < todayFlow)
            {
                count++;
            }
            if (minusday06Flow < todayFlow)
            {
                count++;
            }
            if (minusday07Flow < todayFlow)
            {
                count++;
            }
            if (minusday08Flow < todayFlow)
            {
                count++;
            }
            if (minusday09Flow < todayFlow)
            {
                count++;
            }
            if (minusday10Flow < todayFlow)
            {
                count++;
            }
            if (minusday11Flow < todayFlow)
            {
                count++;
            }
            if (minusday12Flow < todayFlow)
            {
                count++;
            }
            if (minusday13Flow < todayFlow)
            {
                count++;
            }
            if (minusday14Flow < todayFlow)
            {
                count++;
            }
            if (minusday15Flow < todayFlow)
            {
                count++;
            }
            if (minusday16Flow < todayFlow)
            {
                count++;
            }
            if (minusday17Flow < todayFlow)
            {
                count++;
            }
            if (minusday18Flow < todayFlow)
            {
                count++;
            }
            if (minusday19Flow < todayFlow)
            {
                count++;
            }
            if (minusday20Flow < todayFlow)
            {
                count++;
            }
            if (minusday21Flow < todayFlow)
            {
                count++;
            }
            if (minusday22Flow < todayFlow)
            {
                count++;
            }
            if (minusday23Flow < todayFlow)
            {
                count++;
            }
            if (minusday24Flow < todayFlow)
            {
                count++;
            }
            if (minusday25Flow < todayFlow)
            {
                count++;
            }
            if (minusday26Flow < todayFlow)
            {
                count++;
            }
            if (minusday27Flow < todayFlow)
            {
                count++;
            }
            if (minusday28Flow < todayFlow)
            {
                count++;
            }
            if (minusday29Flow < todayFlow)
            {
                count++;
            }
            if (minusday30Flow < todayFlow)
            {
                count++;
            }
            if (count >= 25)
            {
                this.result = "T";
            }
            else
            {
                this.result = "F";
            }

            this.count = count;
        }
    }
}
