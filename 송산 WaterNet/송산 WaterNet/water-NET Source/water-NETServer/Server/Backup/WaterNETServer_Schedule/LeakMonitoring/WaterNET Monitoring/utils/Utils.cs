﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNETServer.LeakMonitoring.utils
{
    public class Utils
    {
        public static double ToDouble(object value)
        {
            double result = 0;

            if (value != DBNull.Value && !Double.IsInfinity(Convert.ToDouble(value)) && !Double.IsNaN(Convert.ToDouble(value)))
            {
                result = Convert.ToDouble(value);
            }

            return result;
        }

        //일련번호 생성
        public static String GetSerialNumber(string workFlag)
        {
            Random random = new Random();

            return workFlag
                    + DateTime.Now.Year.ToString()
                    + (DateTime.Now.Month.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Day.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Second.ToString()).PadLeft(2, '0')
                    + (DateTime.Now.Millisecond.ToString()).PadLeft(3, '0')
                    + (random.Next(1, 9999).ToString()).PadLeft(4, '0');
        }
    }
}
