﻿using System;
using System.Data;

using WaterNETServer.LeakMonitoring.utils;
using WaterNETServer.LeakMonitoring.interface1;

namespace WaterNETServer.LeakMonitoring.cal_leakage
{
    //30일전 Data와의 최고 및 최저사이 비교(최저는?)
    public class MNF30DailyMax : ICal
    {
        private string type = "1030";
        private int count = 0;
        private string result = "N";
        private int count_max = 1;

        public MNF30DailyMax(DateTime dateTime, DataTable thisYearMNF)
        {
            this.Cal(dateTime, thisYearMNF);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearMNF)
        {
            if (thisYearMNF == null || thisYearMNF.Rows.Count == 0)
            {
                return;
            }

            double todayMNF = 0;
            double minusday30MaxValue = 0;

            foreach (DataRow row in thisYearMNF.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayMNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday30MaxValue = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-2).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-3).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-4).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-5).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-6).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-7).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-8).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-9).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-10).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-11).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-12).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-13).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-14).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-15).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-16).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-17).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-18).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-19).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-20).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-21).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-22).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-23).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-24).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-25).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-26).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-27).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-28).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-29).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
                else if (dateTime.AddDays(-30).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    double mnf = Utils.ToDouble(row["VALUE"]);
                    if (minusday30MaxValue < mnf)
                    {
                        minusday30MaxValue = mnf;
                    }
                }
            }

            int count = 0;

            if (minusday30MaxValue < todayMNF)
            {
                count++;
                this.result = "T";
            }
            else
            {
                this.result = "F";
            }

            this.count = count;
        }
    }
}
