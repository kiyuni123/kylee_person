﻿using System;
using System.Data;

using WaterNETServer.LeakMonitoring.interface1;
using WaterNETServer.LeakMonitoring.utils;

namespace WaterNETServer.LeakMonitoring.cal_leakage
{
    //MNF유량 Data 확인
    public class MNFCheck : ICal
    {
        private string type = "1090";
        private int count = 0;
        private string result = "T";
        private int count_max = 0;

        public MNFCheck(DateTime dateTime, DataTable thisYearMNF)
        {
            this.Cal(dateTime, thisYearMNF);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        private void Cal(DateTime dateTime, DataTable thisYearMNF)
        {
            if (thisYearMNF == null || thisYearMNF.Rows.Count == 0)
            {
                return;
            }

            double todayMNF = 0;
            double minusday01MNF = 0;

            foreach (DataRow row in thisYearMNF.Rows)
            {
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayMNF = Utils.ToDouble(row["VALUE"]);
                }
                else if (dateTime.AddDays(-1).ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    minusday01MNF = Utils.ToDouble(row["VALUE"]);
                }
            }

            if (((minusday01MNF * 2) < todayMNF) || (todayMNF * 2) < minusday01MNF)
            {
                this.result = "T";
            }
            else
            {
                this.result = "F";
            }
        }
    }
}
