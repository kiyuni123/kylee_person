﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Collections;

using WaterNETServer.LeakMonitoring.enum1;
using WaterNETServer.LeakMonitoring.vo;
using WaterNETServer.LeakMonitoring.work;
using WaterNETServer.Warning.work;

namespace WaterNETServer.LeakMonitoring.Control
{
    public class Monitering
    {
        private List<string> locationList = new List<string>();
        private List<LeakageVO> leakageList = new List<LeakageVO>();

        public Monitering() { }


        public List<LeakageVO> LeakaegList
        {
            get
            {
                return this.leakageList;
            }
        }

        public int SelectLocationCount()
        {
            return MoniteringWork.GetInstance().SelectLocationCount();
        }

        //하나의 지역을 감시대상에 포함한다.
        public Monitering Add(string loc_code)
        {
            this.locationList.Add(loc_code);

            //중복제거필요
            this.locationList = this.locationList.Distinct().ToList();
            return this;
        }

        //모든 지역(중,소블록)을 조회후 감시대상에 포함한다.
        public Monitering AddAll()
        {
            DataSet locList = MoniteringWork.GetInstance().SelectLocation();
            foreach (DataRow row in locList.Tables[0].Rows)
            {
                this.Add(row["LOC_CODE"].ToString());
            }

            //중복제거필요
            this.locationList = this.locationList.Distinct().ToList();
            return this;
        }

        //등록된 지역의 감시구분별 감시를 시작한다.
        public void Start(DateTime dateTime, MONITOR_TYPE type)
        {
            if (type == MONITOR_TYPE.LEAKAGE)
            {
                this.DoLeakageMonitering(dateTime);
            }
            else if (type == MONITOR_TYPE.BIG_DM_RECEIVING)
            {
                this.DoBigDMReceivingMonitering(dateTime);
            }
        }

        //등록된 지역별 누수 감시를 시작한다.
        private void DoLeakageMonitering(DateTime dateTime)
        {
            for (int i = 0; i < this.locationList.Count; i++)
            {
                leakageList.Add(new LeakageVO(this.locationList[i], dateTime));
            }
        }

        //등록된 지역별 대수용가수수 감시를 시작한다.
        private void DoBigDMReceivingMonitering(DateTime dateTime)
        {
            for (int i = 0; i < this.locationList.Count; i++)
            {

            }
        }

        public void WriteWarning()
        {
            //비즈니스 로직 선언부

            for (int i = 0; i < leakageList.Count; i++)
            {
                for (int j = 0; j < leakageList[i].Cal.Count; j++)
                {
                    if (leakageList[i].Cal[j].MonitorResult == "T")
                    {
                        Hashtable parameter = new Hashtable();
                        parameter["WORK_GBN"] = "0002";
                        parameter["LOC_CODE"] = leakageList[i].LOC_CODE;
                        parameter["WAR_CODE"] = "000001";
                        parameter["SERIAL_NO"] = leakageList[i].DAY.ToString("yyyyMMdd");
                        parameter["REMARK"] = string.Empty;
                        WarningWork.GetInstance().InsertWarningData(parameter);
                        break;
                    }
                }
            }
        }

        //테스트
        private void AddAll_TEST()
        {
            DataSet locList = MoniteringWork.GetInstance().SelectLocation();

            foreach (DataRow row in locList.Tables[0].Rows)
            {
                Console.WriteLine(row["LOC_CODE"].ToString());
            }
        }
    }
}
