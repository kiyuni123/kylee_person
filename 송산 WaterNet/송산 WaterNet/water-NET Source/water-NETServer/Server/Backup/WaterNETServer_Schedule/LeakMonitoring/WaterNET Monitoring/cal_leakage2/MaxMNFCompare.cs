﻿using System;
using System.Data;

using WaterNETServer.LeakMonitoring.utils;
using WaterNETServer.LeakMonitoring.work;
using WaterNETServer.LeakMonitoring.interface1;

namespace WaterNETServer.LeakMonitoring.cal_leakage2
{
    public class MaxMNFCompare : ICal
    {
        private string type = "1240";
        private int count = 0;
        private string result = "F";
        private int count_max = 1;
        private double mnf_max = 0;

        //금일기준 감시
        public MaxMNFCompare(string loc_code, DateTime dateTime, DataTable value)
        {
            //야간최소유량 한계치를 조회한다.
            mnf_max = Utils.ToDouble(MoniteringWork.GetInstance().SelectMNFMax(loc_code));
            this.Cal(dateTime, value);
        }

        public string MonitorType
        {
            get
            {
                return this.type;
            }
        }

        public int MonitorCount
        {
            get
            {
                return this.count;
            }
        }

        public int MonitorCountMax
        {
            get
            {
                return this.count_max;
            }
        }

        //누수결과
        //F : 누수아님(False)
        //T : 누수임 (True)
        public string MonitorResult
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        //계산식 적용
        private void Cal(DateTime dateTime, DataTable value)
        {
            if (value == null || value.Rows.Count == 0)
            {
                return;
            }

            double todayValue = 0;

            foreach (DataRow row in value.Rows)
            {
                //금일값
                if (dateTime.ToString("yyyyMMdd") == Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMdd"))
                {
                    todayValue = Utils.ToDouble(row["VALUE"]);
                }
            }

            //조건을 충족못하면 카운트를 초기화하고 로직을 중단한다.
            if (todayValue < this.mnf_max)
            {
                this.count = 0;
            }
            else
            {
                //카운트를 1단계 상승시킨다.
                this.count = 1;
                this.result = "T";
            }
        }
    }
}
