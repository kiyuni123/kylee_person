﻿using System.Collections.Generic;
using System.Text;
using System.Data;

using Oracle.DataAccess.Client;

using EMFrame.dm;
using WaterNETServer.LeakMonitoring.interface1;
using System;

namespace WaterNETServer.LeakMonitoring.dao
{
    public class MoniteringDao
    {
        private static MoniteringDao dao = null;
        private MoniteringDao() { }

        public static MoniteringDao GetInstance()
        {
            if (dao == null)
            {
                dao = new MoniteringDao();
            }
            return dao;
        }

        //누수감시 결과를 삭제한다.
        public void DeleteLakageMonitoring(EMapper manager, string loc_code, string datee)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete wv_monitering where loc_code = :LOC_CODE and datee = to_date(:DATEE,'yyyymmdd')           ");

            IDataParameter[] parameters =  {
                        new OracleParameter("LOC_CLODE", OracleDbType.Varchar2)
                        ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;
            parameters[1].Value = datee;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //블럭코드목록을 가져온다.
        public int SelectLocationCount(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT COUNT(*)");
            query.AppendLine("  FROM cm_location");
            query.AppendLine(" WHERE ftr_code = 'BZ003'");

            return Convert.ToInt32(manager.ExecuteScriptScalar(query.ToString(), null));
        }

        //블럭코드목록을 가져온다.
        public void SelectLocation(EMapper manager, DataSet dataset, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc_code                                                                  ");
            query.AppendLine("  from cm_location                                                               ");
            query.AppendLine(" where ftr_code = 'BZ003'                                                        ");

            manager.FillDataSetScript(dataset, dataMember, query.ToString(), null);
        }

        //블록지역코드를 기준으로 중블록일경우 관련배수지 코드를 리턴한다.
        public object SelectReservoir(EMapper manager, string loc_code)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select b.loc_code loc_code                                                       ");
            query.AppendLine("  from cm_location a                                                             ");
            query.AppendLine("      ,cm_location b                                                             ");
            query.AppendLine(" where a.loc_code = :LOC_CODE                                                    ");
            query.AppendLine("   and a.ftr_code = 'BZ002'                                                      ");
            query.AppendLine("   and b.loc_name = a.rel_loc_name                                               ");

            IDataParameter[] parameters =  {
	                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //누수감시 결과를 등록한다.
        //insert 또는 update 가능,
        public void UpdatetLakageMonitoring(EMapper manager, string loc_code, string datee, List<ICal> calList)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_monitering wmo                                                                  ");
            query.AppendLine("using (select :LOC_CODE loc_code, to_date(:DATEE,'yyyymmdd') datee, :TYPE typee from dual) a  ");
            query.AppendLine("   on (wmo.loc_code = a.loc_code and wmo.datee = a.datee and wmo.monitor_type = a.typee)      ");
            query.AppendLine(" when MATCHED then                                                                            ");
            query.AppendLine("      update set wmo.monitor_count = :COUNT                                                   ");
            query.AppendLine("                ,wmo.monitor_count_max = :COUNT_MAX                                           ");
            query.AppendLine("                ,wmo.monitor_result = :RESULT                                                 ");
            query.AppendLine(" when not MATCHED then                                                                        ");
            query.AppendLine("      insert (loc_code, datee, monitor_type, monitor_count, monitor_count_max, monitor_result, monitor_alarm)");
            query.AppendLine("      values (:LOC_CODE, to_date(:DATEE,'yyyymmdd'), :TYPE, :COUNT, :COUNT_MAX, :RESULT, decode(:RESULT, 'F', null, 'T'))      ");

            foreach (ICal cal in calList.ToArray())
            {
                IDataParameter[] parameters =  {
	                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                        ,new OracleParameter("DATEE", OracleDbType.Varchar2)
	                    ,new OracleParameter("TYPE", OracleDbType.Varchar2)
	                    ,new OracleParameter("COUNT", OracleDbType.Varchar2)
                        ,new OracleParameter("COUNT_MAX", OracleDbType.Varchar2)
                        ,new OracleParameter("RESULT", OracleDbType.Varchar2)
                        ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                        ,new OracleParameter("DATEE", OracleDbType.Varchar2)
	                    ,new OracleParameter("TYPE", OracleDbType.Varchar2)
	                    ,new OracleParameter("COUNT", OracleDbType.Varchar2)
                        ,new OracleParameter("COUNT_MAX", OracleDbType.Varchar2)
                        ,new OracleParameter("RESULT", OracleDbType.Varchar2)
                        ,new OracleParameter("RESULT", OracleDbType.Varchar2)
                    };

                parameters[0].Value = loc_code;
                parameters[1].Value = datee;
                parameters[2].Value = cal.MonitorType;
                parameters[3].Value = cal.MonitorCount.ToString();
                parameters[4].Value = cal.MonitorCountMax.ToString();
                parameters[5].Value = cal.MonitorResult;
                parameters[6].Value = loc_code;
                parameters[7].Value = datee;
                parameters[8].Value = cal.MonitorType;
                parameters[9].Value = cal.MonitorCount.ToString();
                parameters[10].Value = cal.MonitorCountMax.ToString();
                parameters[11].Value = cal.MonitorResult;
                parameters[12].Value = cal.MonitorResult;

                manager.ExecuteScript(query.ToString(), parameters);
            }
        }

        ////유수율을 조회한다. (지역관리코드, 기간)
        //public object SelectWaterRatio(EMapper manager, string loc_code, string year_mon)
        //{
        //    StringBuilder query = new StringBuilder();

        //    query.AppendLine("select revenue_ratio                                                             ");
        //    query.AppendLine("  from wv_revenue_ratio                                                          ");
        //    query.AppendLine(" where loc_code = :LOC_CODE                                                      ");
        //    query.AppendLine("   and year_mon = :YEAR_MON                                                      ");

        //    IDataParameter[] parameters =  {
        //                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
        //                 ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
        //            };

        //    parameters[0].Value = loc_code;
        //    parameters[1].Value = year_mon;

        //    return manager.ExecuteScriptScalar(query.ToString(), parameters);
        //}

        //야간최소유량을 조회한다. (지역관리코드, 기간)
        public void SelectMNF(EMapper manager, DataSet dataset, string dataMember, string loc_code, string io_gbn, string startdate, string enddate)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" select to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd') timestamp                                                                                             ");
            query.AppendLine("       ,round(sum(iwm.filtering),2) value                                                                                                                           ");
            query.AppendLine("   from if_ihtags iih                                                                                                                                               ");
            query.AppendLine("       ,if_wv_mnf iwm                                                                                                                                               ");
            query.AppendLine("       ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                               ");
            query.AppendLine("  where 1 = 1                                                                                                                                                       ");
            query.AppendLine("    and iih.loc_code = :LOC_CODE                                                                                                                                    ");
            query.AppendLine("    and iih.tagname = itg.tagname                                                                                                                                   ");
            query.AppendLine("    and itg.tag_gbn = 'MNF'                                                                                                                                         ");
            query.AppendLine("    and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                        ");
            query.AppendLine("    and iwm.tagname = itg.tagname                                                                                                                                   ");
            query.AppendLine("    and iwm.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')                                           ");
            query.AppendLine("  group by to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd')                                                                                                   ");

            IDataParameter[] parameters =  {
	                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                         ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;
            parameters[1].Value = startdate;
            parameters[2].Value = enddate;

            manager.FillDataSetScript(dataset, dataMember, query.ToString(), parameters);
        }

        //유입/유출 유량을 조회한다. (지역관리코드, 기간) (전일적산)
        public void SelectFlow(EMapper manager, DataSet dataset, string dataMember, string loc_code, string io_gbn, string startdate, string enddate)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select iay.timestamp  timestamp                                                                                                                                     ");
            query.AppendLine("      ,sum(value) value                                                                                                                                                        ");
            query.AppendLine("  from if_ihtags iih                                                                                                                                                ");
            query.AppendLine("      ,if_accumulation_yesterday iay                                                                                                                                ");
            query.AppendLine("      ,(select tagname, tag_gbn, dummy_relation from if_tag_gbn) itg                                                                                                ");
            query.AppendLine(" where 1 = 1                                                                                                                                                        ");
            query.AppendLine("   and iih.loc_code = :LOC_CODE                                                                                                                                     ");
            query.AppendLine("   and iih.tagname = itg.tagname                                                                                                                                    ");
            query.AppendLine("   and itg.tag_gbn = 'YD'                                                                                                                                           ");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                        ");
            query.AppendLine("   and iay.tagname = itg.tagname                                                                                                                                    ");
            query.AppendLine("   and iay.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')                                            ");
            query.AppendLine(" group by iay.timestamp ");

            IDataParameter[] parameters =  {
	                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                         ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;
            parameters[1].Value = startdate;
            parameters[2].Value = enddate;

            manager.FillDataSetScript(dataset, dataMember, query.ToString(), parameters);
        }

        public object SelectLeakageCount(EMapper manager, string loc_code, string datee, string type)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select monitor_count                                                             ");
            query.AppendLine("  from wv_monitering                                                             ");
            query.AppendLine(" where 1 = 1                                                                     ");
            query.AppendLine("   and loc_code = :LOC_CODE                                                      ");
            query.AppendLine("   and datee = to_date(:DATEE,'yyyymmdd')                                        ");
            query.AppendLine("   and monitor_type = :TYPE                                                      ");

            IDataParameter[] parameters =  {
	                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                         ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                         ,new OracleParameter("TYPE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;
            parameters[1].Value = datee;
            parameters[2].Value = type;
            
            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectMNFMax(EMapper manager, string loc_code)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select max_leakage from wv_block_default_option where loc_code = :LOC_CODE       ");

            IDataParameter[] parameters =  {
	                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = loc_code;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }
    }
}
