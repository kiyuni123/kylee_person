﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Collections;

using EMFrame.work;
using EMFrame.dm;
using EMFrame.log;
using WaterNETServer.Common;
using WaterNETServer.LeakMonitoring.dao;
using WaterNETServer.LeakMonitoring.interface1;

namespace WaterNETServer.LeakMonitoring.work
{
    public class MoniteringWork : BaseWork
    {
        private static MoniteringWork work = null;
        private MoniteringDao dao = null;

        private MoniteringWork()
        {
            dao = MoniteringDao.GetInstance();
        }

        public static MoniteringWork GetInstance()
        {
            if (work == null)
            {
                work = new MoniteringWork();
            }

            return work;
        }

        //블럭코드목록을 가져온다.
        public int SelectLocationCount()
        {
            EMapper manager = null;
            int result = 0;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectLocationCount(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //블럭코드목록을 가져온다.
        public DataSet SelectLocation()
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                dao.SelectLocation(manager, result, "RESULT");

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //누수감시대상 필요 데이터를 조회한다.
        //기준년 기준일 이전 30일의 유입유량
        //기준년 기준일 이전 30일의 야간최소유량

        //전년 기준일 이전 (14일 이후) ~ (14일 이전) 유입유량
        //전년 기준일 이전 (14일 이후) ~ ( 4일 이전) 야간최소유량
        //기준년 전월의 유수율
        //전년 기준월의 유수율

        //배수지코드를 조회해서 태그조회에 활용해야한다.
        //유수율은 일반 지역코드를 활용한다.
        public Hashtable SelectMonitering(Hashtable parameter)
        {
            Hashtable result = new Hashtable();
            DataSet dataSet = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                string loc_code = parameter["LOC_CODE"].ToString();
                DateTime datetime = Convert.ToDateTime(parameter["DATETIME"]);
                string reservoir = Convert.ToString(dao.SelectReservoir(manager, loc_code));
                string io_gbn = string.Empty;
                string tag_loc_code = string.Empty;

                //해당 블록이 소블록이거나... 중블록이지만 배수지가 아닌경우,
                if (reservoir != string.Empty)
                {
                    io_gbn = "OUT";
                    tag_loc_code = reservoir;

                }
                else
                {
                    io_gbn = "IN";
                    tag_loc_code = loc_code;
                }

                //기준년 기준일 이전 30일의 유입유량
                dao.SelectFlow(manager, dataSet, "thisYearFlow", tag_loc_code, io_gbn,
                    datetime.AddDays(-60).ToString("yyyyMMdd"), datetime.ToString("yyyyMMdd"));

                //기준년 기준일 이전 30일의 야간최소유량
                dao.SelectMNF(manager, dataSet, "thisYearMNF", tag_loc_code, io_gbn,
                    datetime.AddDays(-60).ToString("yyyyMMdd"), datetime.ToString("yyyyMMdd"));

                result.Add("thisYearFlow", dataSet.Tables["thisYearFlow"]);
                result.Add("thisYearMNF", dataSet.Tables["thisYearMNF"]);


                //------------------- 2011년 1월 5일 : 회의 결과 로직삭제로인한 데이터 불필요

                //전년 기준일 이전 (14일 이후) ~ (14일 이전) 유입유량
                //dao.SelectFlow(obase.oracleDBManager, dataSet, "lastYearFlow", tag_loc_code, io_gbn,
                //    datetime.AddYears(-1).AddDays(-14).ToString("yyyyMMdd"), datetime.AddYears(-1).AddDays(14).ToString("yyyyMMdd"));

                //전년 기준일 이전 (14일 이후) ~ (14일 이전) 야간최소유량
                //dao.SelectMNF(obase.oracleDBManager, dataSet, "lastYearMNF", tag_loc_code, io_gbn,
                //    datetime.AddYears(-1).AddDays(-14).ToString("yyyyMMdd"), datetime.AddYears(-1).AddDays(14).ToString("yyyyMMdd"));

                //double thisYearFlowRatio = Utils.ToDouble(dao.SelectWaterRatio(obase.oracleDBManager, loc_code, datetime.AddMonths(-1).ToString("yyyyMM")));
                //double lastYearFlowRatio = Utils.ToDouble(dao.SelectWaterRatio(obase.oracleDBManager, loc_code, datetime.AddYears(-1).ToString("yyyyMM")));

                //result.Add("lastYearFlow", dataSet.Tables["lastYearFlow"]);
                //result.Add("lastYearMNF", dataSet.Tables["lastYearMNF"]);
                //result.Add("thisYearFlowRatio", thisYearFlowRatio);
                //result.Add("lastYearFlowRatio", lastYearFlowRatio);

                //----------------------------------------------------------------------------

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //누수감시대상별 누수경보 카운트를 조회한다.
        public object SelectLeakageCount(string loc_code, string dateTime, string type)
        {
            object result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectLeakageCount(manager, loc_code, dateTime, type);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //누수감시대상별 야간최소유량 한계치를 조회한다.
        public object SelectMNFMax(string loc_code)
        {
            object result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.SelectMNFMax(manager, loc_code);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //누수감시 결과를 등록한다.
        //insert 또는 update 가능,
        public void UpdatetLakageMonitoring(Hashtable parameter, List<ICal> calList)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                string loc_code = parameter["LOC_CODE"].ToString();
                string datee = Convert.ToDateTime(parameter["DATETIME"]).ToString("yyyyMMdd");
                dao.DeleteLakageMonitoring(manager, loc_code, datee);
                dao.UpdatetLakageMonitoring(manager, loc_code, datee, calList);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        public void GetRatio_TEST()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                //double thisYearFlowRatio = Utils.ToDouble(dao.SelectWaterRatio(oracleDBManager, "000035", "200912"));
                //double lastYearFlowRatio = Utils.ToDouble(dao.SelectWaterRatio(oracleDBManager, "000035", "201112"));
                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }
    }
}
