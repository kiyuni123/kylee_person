﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNETServer.ScheduleManage.dao;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;
using System.Data;
using System.Collections;
using WaterNETServer.IF_RWIS.dao;

namespace WaterNETServer.ScheduleManage.work
{
    public class ScheduleWork
    {
        private static ScheduleWork work = null;
        private ScheduleDao dao = null;

        private ScheduleWork()
        {
            dao = ScheduleDao.GetInstance();
        }

        public static ScheduleWork GetInstance()
        {
            if (work == null)
            {
                work = new ScheduleWork();
            }

            return work;
        }

        public DataTable SelectTagTargetList(string [] kinds)
        {
            EMapper manager = null;
            DataTable data = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                data = WaterNETDao.GetInstance().SelectTagList(manager, kinds);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return data;
        }

        //태그 구분 목록 조회
        public DataTable SelectTagGbnList()
        {
            EMapper manager = null;
            DataTable data = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                data = dao.SelectTagGbnList(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return data;
        }

        //태그 목록 조회
        public DataTable SelectTagList(Hashtable parameters)
        {
            EMapper manager = null;
            DataTable data = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                data = dao.SelectTagList(manager, parameters);

                data.Columns.Add("SELECTED", typeof(bool));
                foreach (DataRow row in data.Rows)
                {
                    row["SELECTED"] = false;
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return data;
        }

        //태그  트렌드 조회
        public DataSet SelectTagTrend(IList<string> trendTagList, string tag_gubun, string tag_count)
        {
            EMapper manager = null;
            DataSet set = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                set = new DataSet();

                foreach (string tagname in trendTagList)
                {
                    Hashtable parameters = new Hashtable();
                    parameters["TAGNAME"] = tagname;
                    parameters["TAG_COUNT"] = tag_count;

                    //일단위
                    if (tag_gubun == "TD")
                    {
                        dao.SELECT_ACCUMULATION_TODAY(manager, set, parameters);
                    }

                    //일단위
                    else if (tag_gubun == "YD" ||
                        tag_gubun == "YD_D" ||
                        tag_gubun == "YD_R")
                    {
                        dao.SELECT_ACCUMULATION_YESTERDAY(manager, set, parameters);
                    }

                    //시간단위
                    else if (tag_gubun == "FRQ" ||
                        tag_gubun == "FRQ_I" ||
                        tag_gubun == "FRQ_O" ||
                        tag_gubun == "JU_O" ||
                        tag_gubun == "SPL_D" ||
                        tag_gubun == "SPL_I" ||
                        tag_gubun == "SPL_O")
                    {
                        dao.SELECT_ACCUMULATION_HOUR(manager, set, parameters);
                    }

                    //야간최소유량
                    else if (tag_gubun == "MNF")
                    {
                        dao.SELECT_WV_MNF(manager, set, parameters);
                    }

                    //분단위
                    else
                    {
                        dao.SELECT_GATHER_REALTIME(manager, set, parameters);
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return set;
        }
    }
}
