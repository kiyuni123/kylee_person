﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;
using System.Text;
using WaterNETServer.IF_iwater.dao;
using WaterNETServer.Common.utils;
using WaterNETServer.Warning.AddonWarning;
using WaterNETServer.BatchJobs.dao;
using WaterNETServer.LeakMonitoring.Control;
using WaterNETServer.LeakMonitoring.enum1;
using EMFrame.log;

namespace WaterNETServer.ThreadManager.runThread
{
    public class HistorianRunThread
    {
        private Hashtable _threadPool = null;
        public HistorianRunThread() { }
        public HistorianRunThread(Hashtable threadPool)
        {
            this._threadPool = threadPool;
        }

        private WaterNETServer.Common.utils.Common utils = new WaterNETServer.Common.utils.Common();
        BatchJobsWork jobs = new BatchJobsWork();
        Monitering monitering = new Monitering();

#region 화면 제어 부분 ###########################################
        /// <summary>
        /// 쓰레드 상태 표시 버튼
        /// </summary>
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Button _button4;
        private Button _button5;

        public void setButton1(Button button) { this._button1 = button; }
        public void setButton2(Button button) { this._button2 = button; }
        public void setButton3(Button button) { this._button3 = button; }
        public void setButton4(Button button) { this._button4 = button; }
        public void setButton5(Button button) { this._button5 = button; }

        /// <summary>
        /// 쓰레드 상태 표시 프로그래스바
        /// </summary>
        private ProgressBar _progressbar1;
        private ProgressBar _progressbar2;
        private ProgressBar _progressbar3;
        private ProgressBar _progressbar4;
        private ProgressBar _progressbar5;
        public void setProgressBar1(ProgressBar progressbar) { this._progressbar1 = progressbar; }
        public void setProgressBar2(ProgressBar progressbar) { this._progressbar2 = progressbar; }
        public void setProgressBar3(ProgressBar progressbar) { this._progressbar3 = progressbar; }
        public void setProgressBar4(ProgressBar progressbar) { this._progressbar4 = progressbar; }
        public void setProgressBar5(ProgressBar progressbar) { this._progressbar5 = progressbar; }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback1(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage1(int i)
        {
            if (_button1.InvokeRequired)
            {
                SetButtonImageCallback1 d = new SetButtonImageCallback1(SetButtonImage1);
                this._button1.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button1.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback2(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage2(int i)
        {
            if (_button2.InvokeRequired)
            {
                SetButtonImageCallback2 d = new SetButtonImageCallback2(SetButtonImage2);
                this._button2.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button2.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback3(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage3(int i)
        {
            if (_button3.InvokeRequired)
            {
                SetButtonImageCallback3 d = new SetButtonImageCallback3(SetButtonImage3);
                this._button3.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button3.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback4(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage4(int i)
        {
            if (_button4.InvokeRequired)
            {
                SetButtonImageCallback4 d = new SetButtonImageCallback4(SetButtonImage4);
                this._button4.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button4.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">이미지 번호</param>
        private delegate void SetButtonImageCallback5(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">이미지 번호</param>
        public void SetButtonImage5(int i)
        {
            if (_button5.InvokeRequired)
            {
                SetButtonImageCallback5 d = new SetButtonImageCallback5(SetButtonImage5);
                this._button5.Invoke(d, new object[] { i });
            }
            else
            {
                if (i == 1)
                {
                    // 버튼 이미지 (녹색)
                    _button5.Image = WaterNETServer.Common.Properties.Resources.OkPopup;
                }
                if (i == 2)
                {
                    // 버튼 이미지 (파랑)
                    _button5.Image = WaterNETServer.Common.Properties.Resources.YesPopup;
                }
                if (i == 3)
                {
                    // 버튼 이미지 (빨강)
                    _button5.Image = WaterNETServer.Common.Properties.Resources.NoPopup;
                }
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback1(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar1(int i)
        {
            if (_progressbar1.InvokeRequired)
            {
                SetProgressBarCallback1 d = new SetProgressBarCallback1(SetProgressBar1);
                this._progressbar1.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar1.Value = i;
            }
        }
        public void SetProgressBarMaximun1(int i)
        {
            if (_progressbar1.InvokeRequired)
            {
                SetProgressBarCallback1 d = new SetProgressBarCallback1(SetProgressBarMaximun1);
                this._progressbar1.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar1.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback2(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar2(int i)
        {
            if (_progressbar2.InvokeRequired)
            {
                SetProgressBarCallback2 d = new SetProgressBarCallback2(SetProgressBar2);
                this._progressbar2.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar2.Value = i;
            }
        }

        public void SetProgressBarMaximun2(int i)
        {
            if (_progressbar2.InvokeRequired)
            {
                SetProgressBarCallback2 d = new SetProgressBarCallback2(SetProgressBarMaximun2);
                this._progressbar2.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar2.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback3(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar3(int i)
        {
            if (_progressbar3.InvokeRequired)
            {
                SetProgressBarCallback3 d = new SetProgressBarCallback3(SetProgressBar3);
                this._progressbar3.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar3.Value = i;
            }
        }

        public void SetProgressBarMaximun3(int i)
        {
            if (_progressbar3.InvokeRequired)
            {
                SetProgressBarCallback3 d = new SetProgressBarCallback3(SetProgressBarMaximun3);
                this._progressbar3.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar3.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback4(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar4(int i)
        {
            if (_progressbar4.InvokeRequired)
            {
                SetProgressBarCallback4 d = new SetProgressBarCallback4(SetProgressBar4);
                this._progressbar4.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar4.Value = i;
            }
        }

        public void SetProgressBarMaximun4(int i)
        {
            if (_progressbar4.InvokeRequired)
            {
                SetProgressBarCallback4 d = new SetProgressBarCallback4(SetProgressBarMaximun4);
                this._progressbar4.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar4.Maximum = i;
            }
        }

        /// <summary>
        /// 쓰레드에서 이미지 제어 콜백
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        private delegate void SetProgressBarCallback5(int i);
        /// <summary>
        /// 쓰레드에서 이미지 제어 함수
        /// </summary>
        /// <param name="i">프로그래스바 진행 퍼센트</param>
        public void SetProgressBar5(int i)
        {
            if (_progressbar5.InvokeRequired)
            {
                SetProgressBarCallback5 d = new SetProgressBarCallback5(SetProgressBar5);
                this._progressbar5.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar5.Value = i;
            }
        }

        public void SetProgressBarMaximun5(int i)
        {
            if (_progressbar5.InvokeRequired)
            {
                SetProgressBarCallback5 d = new SetProgressBarCallback5(SetProgressBarMaximun5);
                this._progressbar5.Invoke(d, new object[] { i });
            }
            else
            {
                _progressbar5.Maximum = i;
            }
        }
#endregion


        /// <summary>
        /// 실시간 데이터 수집
        /// </summary>
        public void runThread_GatherRealtime()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage1(1);

                OleDBWork hWork = new OleDBWork();
                OracleWork oWork = new OracleWork();

                string hTableName1 = "RealTimeTags";
                string hTableName2 = "RealTimeRawDatas";

                // 1. Historian 에서 최종 시간을 조회
                Hashtable cTime = hWork.selectCurrentTimeStr();

                // 2. 조회한 최종시간 데이터가 이미 수집 됐는지 확인
                bool bool_IsGatherRealtimeData = oWork.IsGatherRealtimeData(DateTime.ParseExact(cTime["<="].ToString(), "yyyy-M-d H:m", null).ToString("yyyyMMddHHmm"));

                // 이미 수집했던 시간대면 패스! (Exception 피하기 위함)
                if (!bool_IsGatherRealtimeData)
                {
                    // 3. Oracle에서 실시간 태그 목록을 조회
                    DataSet result1 = oWork.selectRealtimeTags(hTableName1);

                    // 4. Historian 에서 result1에 따른 rawdata 값을 조회
                    DataSet result2 = hWork.selectRawData(hTableName2, result1, cTime);

                    // 5. result2 값을 Oracle에 넣는다.
                    oWork.insertRawData(result1, result2, hTableName2);

                    /// 10분 주기로 수집 보정 모듈로 대체 (000002) 
                    // 6. 수집대상이지만 현재시간에 수집되지 않은 것들 기록
                    //int rtnCnt = oWork.MissingRawData(result1, result2, cTime);
                    /// 10분 주기로 수집 보정 모듈로 대체 (000002)
                    // 7. 실시간 데이터 수집 누락 자동 보정
                    //if (rtnCnt > 0) autoModify_GatherRealtime("000001");
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine();
                    sb.AppendLine("############################################################################");
                    sb.AppendLine("이미 수집 했던 데이터를 또 시도함 (히스토리안이 멈춰있을 확률이 높습니다)   ");
                    sb.AppendLine("----------------------------------------------------------------------------");
                    sb.AppendLine("Start iFix Collector 를 재실행 하시오 (개발 서버일 경우 자주 발생)          ");
                    sb.AppendLine("############################################################################");

                    Logger.Error(sb.ToString());
                }

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage1(2);

            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        /// <summary>
        /// 누락 데이터 검토
        /// </summary>
        internal void runThread_RecordPasttime()
        {
            try
            {
                OleDBWork hWork = new OleDBWork();
                OracleWork oWork = new OracleWork();

                string hTableName3 = "MissingRealTimeRawDatas";

                // 1. 이전 수집 데이터 있는지 확인
                DataSet result3 = oWork.selectPastRawData(hTableName3);

                int selCnt = result3.Tables[0].Rows.Count;
                if (selCnt > 0)
                {
                    // 2. result3 값을 if_missing_realtime에 넣는다
                    oWork.MissingRawData(result3);

                    #region 프로그램 실행중 표시
                    DateTime tmpDateTime = new DateTime();
                    tmpDateTime = utils.CheckExecTime("start", DateTime.Now, "실시간 데이터 누락 자동 보정", "Info");
                    // 시작표시
                    if (this._threadPool.ContainsKey("runThread_RecordPasttime"))
                    {
                        this._threadPool.Remove("runThread_RecordPasttime");
                    }
                    this._threadPool.Add("runThread_RecordPasttime", "RUN");
                    #endregion

                    // 3. 실시간 데이터 수집 누락 자동 보정
                    autoModify_GatherRealtime("000002");

                    #region 프로그램 종료 표시
                    if (this._threadPool.ContainsKey("runThread_RecordPasttime"))
                    {
                        this._threadPool.Remove("runThread_RecordPasttime");
                    }
                    this._threadPool.Add("runThread_RecordPasttime", "STOP");
                    utils.CheckExecTime("end", tmpDateTime, DateTime.Now, "실시간 데이터 누락 자동 보정", "Info");
                    #endregion
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        /// <summary>
        /// 실시간 데이터 수집 누락 자동 보정(재수집)
        /// </summary>
        private void autoModify_GatherRealtime(string code)
        {
            try
            {
                OracleWork  oWork               = new OracleWork();
                OleDBWork   hWork               = new OleDBWork();
                Hashtable   param               = new Hashtable();
                Hashtable   warnHashtable       = new Hashtable();

                string      missingTableName    = "MissingRawDatas";
                string      hTableName1         = "RealTimeTags";
                string      hTableName2         = "RealTimeRawDatas";


                // Oracle에서 실시간 태그 목록을 조회
                // cof(계수)값을 실시간 값에 곱해줘야하기때문에 실시간 태그 목록 조회가 필요함
                DataSet result1 = oWork.selectRealtimeTags(hTableName1);

                // 누락된 데이터 조회 (태그+시간)
                // 10분전 이전의 모든 데이터 조회
                DataSet missingResult = oWork.selectMissingRawData(missingTableName, code, -10);

                DataTable list = missingResult.Tables[missingTableName];
                foreach (DataRow row in list.Rows)
                {
                    // 히스토리안 날짜조건 만듬
                    param = utils.GetTimestampHistorianCondition(Convert.ToDateTime(row[1].ToString()), 0);
                    // 히스토리안에서 데이터를 수집
                    DataSet result2 = hWork.selectMissingData(missingTableName, row[0].ToString(), param);
                    // 수집된 데이터 입력
                    if (oWork.insertRawData(result1, result2, hTableName2) > 0)
                    {
                        // 미수집 기록에 수집체크
                        if (oWork.UpdateMissingData(row[0].ToString(), row[1].ToString()) > 0)
                        {
                            // 시간(키) + 카운트(수정횟수)를 Hashtable에 기록
                            // 경고메시지에 몇시 경고를 몇회 수정했는지 기록 하기 위함
                            if (warnHashtable.ContainsKey(row[1].ToString()))
                            {
                                // 기록된 시간이 있으면 카운트를 받아 1증가
                                int cnt = Convert.ToInt32(warnHashtable[row[1].ToString()]);
                                warnHashtable.Remove(row[1].ToString());
                                warnHashtable.Add(row[1].ToString(), cnt + 1);
                            }
                            else
                            {
                                // 처음 기록되는 시간이면 카운트를 1기록
                                warnHashtable.Add(row[1].ToString(), 1);
                            }
                        }
                    }

                    // 000001	실시간 수집 오류 누락(Historian데이터 누락)
                    if ("000001".Equals(code))
                    {
                        // 경고메시지 업데이트
                        // 기록된 시간별 카운트
                        foreach (DictionaryEntry de in warnHashtable)
                        {
                            oWork.UpdateWarning(Convert.ToDateTime(de.Key.ToString()), Convert.ToInt32(de.Value.ToString()));
                        }
                    }

                    // 000002	실시간 수집 오류 누락(타임 싱크 오류)
                    if ("000002".Equals(code))
                    {
                        // 이건 업데이트 할 방법이 없음 warning 관리가 공통으로 사용하는것이라 시간 범위 지정을 할 수가 없음
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        /// <summary>
        /// 실시간 데이터 보정
        /// </summary>
        /// <param name="threadKey">쓰레드 번호</param>
        public void runThread_ModifyRealtime(string cTime, string cTime2)
        {
            #region 시작 메시지
            
            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 데이터 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_ModifyRealtime"))
            {
                this._threadPool.Remove("runThread_ModifyRealtime");
            }
            this._threadPool.Add("runThread_ModifyRealtime", "RUN");

            #endregion

            GlobalVariable gVar = new GlobalVariable();

            // 수정할 전체 건수 (전체 분)
            // 시작일 00시 00분 부터 종료일 23:59분 데이터
            // (종료일 - 시작일 + 1) x 1440
            int subCnt = (DateTime.ParseExact(cTime2, "yyyyMMddHHmm", null).Subtract(DateTime.ParseExact(cTime, "yyyyMMddHHmm", null)).Days + 1) * 1440;

            int localDeleteCnt = subCnt;                                                // 데이터 삭제
            int selectTagCnt = 2;                                                       // 태그 정보 조회
            int indexRebuildCnt = 100;                                                  // 인덱스 리빌드 (대략할당 : 데이터가 많아지면 증가할 시간이긴 하나.. 측정은 불가)
            int accuHourCnt = subCnt;                                                   // 1시간 적산 카운트 (카운트는 그대로 사용하고 루프에서 시간을 걸러서 사용)
            int accuDayCnt = subCnt;                                                    // 1일 적산 카운트 (카운트는 그대로 사용하고 루프에서 시간을 걸러서 사용)

            int pgbarCnt = subCnt + localDeleteCnt + selectTagCnt + indexRebuildCnt     // 전체 프로그래스바 카운트
                         + accuHourCnt + accuDayCnt;

            // 진행중인 카운트
            int ingCnt = 0;

            OleDBWork hWork = new OleDBWork();
            OracleWork oWork = new OracleWork();

            string hTableName1 = "RealTimeTags";
            string hTableName2 = "RealTimeRawDatas";

            // 프로그램 실행 시간 측정용
            DateTime tmpDateTime = new DateTime();
            try
            {
                // 파랑 (작업 시작했음을 알림)
                SetButtonImage5(2);

                // 전체 프로그래스바 셋팅
                SetProgressBarMaximun2(pgbarCnt);

                //////////////////////////////////////////////
                // 1. Local에서 조회기간의 실시간 데이터를 삭제한다.
                #region 1. 조회기간의 실시간 데이터 삭제
                // 개별 프로그래스바 셋팅 (삭제)
                SetProgressBarMaximun1(localDeleteCnt);

                for (int i = 0; i < localDeleteCnt; i++)
                {
                    SetProgressBar1(i);
                    SetProgressBar2(ingCnt++);

                    // 삭제로직 (분단위)
                    oWork.DeleteRealtime(utils.IncreaseTimeString(cTime, i));
                }

                #endregion

                //////////////////////////////////////////////
                // 2. Oracle(Local)에서 태그 목록을 조회후 DataSet에 담는다.
                #region 2. 태그 목록 조회
                // 개별 프로그래스바 셋팅 (태그 조회)
                SetProgressBarMaximun1(selectTagCnt);
                ingCnt = ingCnt + selectTagCnt;

                DataSet result1 = null;
                result1 = oWork.selectRealtimeTags(hTableName1);

                SetProgressBar1(selectTagCnt);
                SetProgressBar2(ingCnt);
                #endregion

                // 녹색 (수집 시작임을 알림)
                SetButtonImage5(1);

                //////////////////////////////////////////////
                // 3. 실시간 데이터 수집
                #region 실시간 데이터 수집
                // 개별 프로그래스바 셋팅 (조회 및 인서트)
                SetProgressBarMaximun1(subCnt);

                tmpDateTime = utils.CheckExecTime("start", DateTime.Now, "실시간 데이터 수집", "Info");

                for (int i = 0; i < subCnt; i++)
                {
                    SetProgressBar1(i);
                    SetProgressBar2(ingCnt++);

                    // 3.1. Historian에서 1분 데이터를 조회한다.
                    DataSet result2 = null;
                    result2 = hWork.selectRawData(hTableName2, result1, utils.GetHistorianWhereTime(utils.IncreaseTimeString(cTime, i)));

                    // 3.2. Oracle에 데이터를 넣는다.
                    oWork.insertRawData(result1, result2, hTableName2);
                }

                utils.CheckExecTime("end", tmpDateTime, DateTime.Now, "실시간 데이터 수집", "Info");

                //////////////////////////////////////////////
                // 3.3. index rebuild 
                tmpDateTime = utils.CheckExecTime("start", DateTime.Now, "실시간 데이터 index rebuild", "Info");

                oWork.IndexRebuildRealtime();

                utils.CheckExecTime("end", tmpDateTime, DateTime.Now, "실시간 데이터 index rebuild", "Info");
                #endregion

                //////////////////////////////////////////////
                // 4. 적산데이터 보정 (1시간)
                #region 1시간 적산데이터 보정
                // 개별 프로그래스바 셋팅 (조회 및 인서트)
                SetProgressBarMaximun1(accuHourCnt);

                tmpDateTime = utils.CheckExecTime("start", DateTime.Now, "적산데이터 보정 (1시간)", "Info");

                for (int i = 0; i < accuHourCnt; i++)
                {
                    SetProgressBar1(i);
                    SetProgressBar2(ingCnt++);

                    if ("00".Equals(utils.IncreaseTimeString(cTime, i).Substring(10, 2)))
                    {
                        // 4.1 적산 데이터 삭제
                        oWork.deleteAccumulation_Hour(utils.IncreaseTimeString(cTime, i).Substring(0, 10));

                        // 4.2 적산데이터 입력
                        oWork.insertAccumulation_Hour(utils.IncreaseTimeString(cTime, i).Substring(0, 10));
                    }
                }
                // 4.3 index rebuild
                oWork.IndexRebuildAccumulationHour();

                utils.CheckExecTime("end", tmpDateTime, DateTime.Now, "적산데이터 보정 (1시간)", "Info");
                #endregion

                //////////////////////////////////////////////
                // 6. 적산데이터 보정 (1일)
                oWork.insertModification_Day(utils.IncreaseTimeString(cTime, 0).Substring(0, 8), utils.IncreaseTimeString(cTime2, 0).Substring(0, 8));

                //////////////////////////////////////////////
                // 7. 정시수압 데이터 보정 
                jobs.AveragePressure_Modify(utils.IncreaseTimeString(cTime, 0).Substring(0, 8), utils.IncreaseTimeString(cTime2, 0).Substring(0, 8));

                //////////////////////////////////////////////
                // 8. 야간최소유량 데이터 보정
                jobs.MinimumNightFlow_modify(utils.IncreaseTimeString(cTime, 0).Substring(0, 8), utils.IncreaseTimeString(cTime2, 0).Substring(0, 8));

                //////////////////////////////////////////////
                // 9. 유수율 분석
                jobs.WV_BlockRevenueRatioBatch_Month(DateTime.ParseExact(cTime, "yyyyMMddHHmm", null), DateTime.ParseExact(cTime2, "yyyyMMddHHmm", null));

                //////////////////////////////////////////////
                // 10. 누수감시
                monitering.AddAll();
                monitering.Start(DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null), MONITOR_TYPE.LEAKAGE);
                //for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null).AddDays(-2); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null).AddDays(2); )
                //{
                //    monitering.Start(d, WaterNETMonitoring.enum1.MONITOR_TYPE.LEAKAGE);
                //    d = d.AddDays(1);
                //}

                // 빨강 (작업 종료임을 알림)
                SetButtonImage5(3);

                SetProgressBar1(0);
                SetProgressBar2(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 데이터 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_ModifyRealtime"))
            {
                this._threadPool.Remove("runThread_ModifyRealtime");
            }
            this._threadPool.Add("runThread_ModifyRealtime", "STOP");

            #endregion
        }

        /// <summary>
        /// 1시간 적산 데이터 수집
        /// </summary>
        /// <param name="threadKey">쓰레드 번호</param>
        public void runThread_Accumulation_Hour()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage2(1);

                OleDBWork tWork = new OleDBWork();
                OracleWork oWork = new OracleWork();

                //Historian 에서 최종 시간을 조회
                DateTime cTime = tWork.selectCurrentTime();
                string cTimeStr = String.Format("{0:yyyyMMddHH}", cTime);

                oWork.insertAccumulation_Hour(cTimeStr);

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage2(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        /// <summary>
        /// 1시간 적산 데이터 보정
        /// </summary>
        /// <param name="modifyHourString"></param>
        /// <param name="modifyHourString2"></param>
        internal void runThread_Modification_Hour(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 1시간 적산 데이터 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_Modification_Hour"))
            {
                this._threadPool.Remove("runThread_Modification_Hour");
            }
            this._threadPool.Add("runThread_Modification_Hour", "RUN");

            #endregion

            try
            {
                OracleWork oWork = new OracleWork();

                int accuHourCnt = (DateTime.ParseExact(cTime2, "yyyyMMddHHmm", null).Subtract(DateTime.ParseExact(cTime, "yyyyMMddHHmm", null)).Days + 1) * 1440;

                SetProgressBarMaximun3(accuHourCnt);

                for (int i = 0; i < accuHourCnt; i++)
                {
                    SetProgressBar3(i);

                    if ("00".Equals(utils.IncreaseTimeString(cTime, i).Substring(10, 2)))
                    {
                        // 1. 적산 데이터 삭제
                        //oWork.deleteAccumulation_Hour(utils.IncreaseTimeString(cTime, i).Substring(0, 10));

                        // 2. 적산데이터 입력
                        oWork.insertAccumulation_Hour(utils.IncreaseTimeString(cTime, i).Substring(0, 10));
                    }
                }
                // 3. index rebuild
                oWork.IndexRebuildAccumulationHour();

                SetProgressBar3(0);
            }
            catch (Exception e)
            {
                throw e;
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 1시간 적산 데이터 보정 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_Modification_Hour"))
            {
                this._threadPool.Remove("runThread_Modification_Hour");
            }
            this._threadPool.Add("runThread_Modification_Hour", "STOP");

            #endregion
        }

        /// <summary>
        /// 1일 적산 데이터 수집 (전일 적산)
        /// </summary>
        /// <param name="threadKey">쓰레드 번호</param>
        public void runThread_Accumulation_Day()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage3(1);

                OleDBWork tWork = new OleDBWork();
                OracleWork oWork = new OracleWork();

                //Historian 에서 최종 시간을 조회
                DateTime cTime = tWork.selectCurrentTime();
                string cTimeStr = String.Format("{0:yyyyMMdd}", cTime);

                oWork.insertAccumulation_Day(cTimeStr);

                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage3(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        /// <summary>
        /// 1일 적산 데이터 보정 (전일 적산)
        /// </summary>
        /// <param name="threadKey">쓰레드 번호</param>
        public void runThread_Modification_Day(string cTime, string cTime2)
        {
            #region 시작 메시지

            MessageBox.Show("데이터 보정 시작");

            // 전역변수에 데이터 보정중임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_Modification_Day"))
            {
                this._threadPool.Remove("runThread_Modification_Day");
            }
            this._threadPool.Add("runThread_Modification_Day", "RUN");

            #endregion

            try
            {
                OracleWork oWork = new OracleWork();

                int accuHourCnt = (DateTime.ParseExact(cTime2, "yyyyMMddHHmm", null).Subtract(DateTime.ParseExact(cTime, "yyyyMMddHHmm", null)).Days + 1);

                SetProgressBarMaximun4(accuHourCnt);

                int i = 1;

                for (DateTime d = DateTime.ParseExact(cTime.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(cTime2.Substring(0, 8), "yyyyMMdd", null); i++)
                {
                    SetProgressBar4(i);

                    oWork.insertModification_Day(d.ToString("yyyyMMdd"), d.ToString("yyyyMMdd"));

                    d = d.AddDays(1);
                }
                SetProgressBar4(0);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }

            #region 종료 메시지

            MessageBox.Show("데이터 보정 종료");

            // 전역변수에 보정 종료 STOP 임을 표시 (쓰레드 풀을 임시로 사용)
            if (this._threadPool.ContainsKey("runThread_Modification_Day"))
            {
                this._threadPool.Remove("runThread_Modification_Day");
            }
            this._threadPool.Add("runThread_Modification_Day", "STOP");

            #endregion
        }

        /// <summary>
        /// 금일 적산
        /// </summary>
        /// <param name="param1"></param>
        public void runThread_Accumulation_ToDay()
        {
            try
            {
                // 쓰레드 시작 이미지 표시(녹색)
                SetButtonImage4(1);
                OleDBWork tWork = new OleDBWork();
                OracleWork oWork = new OracleWork();

                //Historian 에서 최종 시간을 조회
                DateTime cTime = tWork.selectCurrentTime();
                string cTimeStr = String.Format("{0:yyyyMMddHHmm}", cTime);

                // 금일적산 수집 
                oWork.InsertAccumulation_ToDay(cTimeStr);

                // 10분 이전 데이터 삭제 (금일적산은 보관 불필요)
                oWork.DeleteAccumulation_Today(cTimeStr);
                
                // 쓰레드 대기 이미지 표시(파랑)
                SetButtonImage4(2);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        /// <summary>
        /// 실시간 경고
        /// </summary>
        internal void runThread_RealtimeWarning()
        {
            try
            {
                // 수질경보처리
                QualityWarning qWarning = new QualityWarning();
                qWarning.Analysis_RTData();

                // 감압밸브경고처리
                ReducePressureWarning rWarning = new ReducePressureWarning();
                rWarning.Analysis_RTData();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
    }
}
