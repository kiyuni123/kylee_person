﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading;
using EMFrame.log;
using WaterNETServer.ScheduleManage.interface1;
using IF_WaterINFOS.IF;
using WaterNETServer.Common.utils;
using IF_WaterINFOS.dao;
using System.Data;
using System.Windows.Forms;



/// 계량기교체정보

namespace WaterNETServer.ScheduleManage.schedule
{
    public class MTRCHGINFOSchedule : BaseSchedule
    {
        private string _strSVCKEY = string.Empty;  // 서비스코드
        private string _strSGCCD = string.Empty;   // 지자체코드
        private string _regmngrip = string.Empty;   // 데이터 받는 IP (Water-NET) 

        GlobalVariable gVar = new GlobalVariable();

        public MTRCHGINFOSchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
            this._strSVCKEY = gVar.SVCKEY;//.get_strSVCKEY();
            this._strSGCCD = gVar.SGCCD;//.getSgccdCode();
            this._regmngrip = gVar.IPADDR;//.getRegmngrip();
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                GetInfosWebService infos = new GetInfosWebService();
                InfosWork infosWork = new InfosWork();

                DataSet dSet = infos.GetMTRCHGINFO
                    (
                    _strSVCKEY,
                    _regmngrip, 
                    _strSGCCD,
                    string.Empty,
                    TARGETDATE.ToString("yyyyMMdd"), 
                    TARGETDATE.ToString("yyyyMMdd")                    
                    );

                this.autorun_target_count = dSet.Tables[0].Rows.Count;
                DateTime if_date = DateTime.Now;
                // IF_MTRCHGINFO에 계량기교체정보 넣기
                infosWork.GetMTRCHGINFO(dSet, if_date);

                // WI_MTRCHGINFO에 계량기교체정보 넣기
                infosWork.MdfyMTRCHGINFO(if_date.ToString("yyyyMMddHHmmss"));

                //infosWork.MdfyMTRCHGINFO2(dtStr);

                // IF_MTRCHGINFO의 2일전 데이터 삭제 (임시테이블 데이터 보관 불필요)
                infosWork.DeleteMTRCHGINFO(if_date.ToString("yyyyMMdd"));

                this.autorun_value = 100;
                this.autorun_complete_count = this.autorun_target_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                EMFrame.log.Logger.Error(ex.ToString());
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                GetInfosWebService infos = new GetInfosWebService();
                InfosWork infosWork = new InfosWork();
                Common.utils.Common utils = new Common.utils.Common();
                TimeSpan span = ENDDATE - STARTDATE;
                double progress = 0;

                for (int i = 0; i <= span.TotalDays; i++)
                {
                    DataSet dSet = infos.GetMTRCHGINFO
                        (
                        _strSVCKEY,
                        _regmngrip,
                        _strSGCCD,
                        string.Empty,
                        STARTDATE.AddDays(i).ToString("yyyyMMdd"),
                        STARTDATE.AddDays(i).ToString("yyyyMMdd")
                        );

                    DateTime if_date = DateTime.Now;
                    infosWork.GetMTRCHGINFO(dSet, if_date);
                    infosWork.MdfyMTRCHGINFO(if_date.ToString("yyyyMMddHHmmss"));
                    progress += 100.0 / (span.TotalDays + 1.0);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                }

                infosWork.DeleteMTRCHGINFO(DateTime.Today.ToString("yyyyMMdd"));
            }
            catch (Exception ex)
            {
                EMFrame.log.Logger.Error(ex.ToString());
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }
    }
}
