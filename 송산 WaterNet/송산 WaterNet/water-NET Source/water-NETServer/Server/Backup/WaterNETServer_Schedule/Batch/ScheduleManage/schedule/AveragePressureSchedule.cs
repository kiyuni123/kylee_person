﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading;
using EMFrame.log;
using WaterNETServer.ScheduleManage.interface1;
using WaterNETServer.BatchJobs.dao;
using WaterNETServer.IF_iwater.dao;
using System.Windows.Forms;
using WaterNETServer.ScheduleManage.formPopup;
using WaterNETServer.IF_RWIS.work;
using WaterNETServer.Common;



/// 1시간 정시 수압

namespace WaterNETServer.ScheduleManage.schedule
{
    public class AveragePressureSchedule : BaseSchedule
    {        
        string[] kinds = WaterNET.GetInstance().SelectTagGbn(AppStatic.PRESSURECODE);

        public AveragePressureSchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
            base.manualTargetForm = new frmTags(this, this.kinds);
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                IList<string> tagList = WaterNET.GetInstance().SelectTagList(this.kinds);
                this.autorun_target_count = tagList.Count;
                WaterNET.GetInstance().InsertAveragePressure(tagList, TARGETDATE);
                this.autorun_value = 100;
                this.autorun_complete_count = this.autorun_target_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                IList<string> tagList = null;

                if (base.manual_all_target)
                {
                    tagList = WaterNET.GetInstance().SelectTagList(this.kinds);
                }
                else if (!base.manual_all_target)
                {
                    tagList = base.manual_target as List<string>;
                }

                if (tagList == null)
                {
                    throw new Exception("water-NET 1시간정시수압 태그 목록이 존재하지 않습니다.");
                }

                TimeSpan span = ENDDATE - STARTDATE;
                double progress = 0;

                for (int i = 0; i <= span.TotalHours; i++)
                {
                    WaterNET.GetInstance().InsertAveragePressure(tagList, STARTDATE.AddDays(i).AddHours(i));
                    progress += 100.0 / (span.TotalHours + 1.0);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }
    }
}