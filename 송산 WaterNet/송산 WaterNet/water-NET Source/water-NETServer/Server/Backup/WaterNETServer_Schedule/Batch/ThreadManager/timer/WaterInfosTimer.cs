﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using WaterNETServer.ThreadManager.runThread;
using WaterNETServer.Common;
using EMFrame.log;

namespace WaterNETServer.ThreadManager.timer
{
    public class WaterInfosTimer
    {
        private Hashtable _threadPool = null;
        public WaterInfosTimer() {}
        public WaterInfosTimer(Hashtable threadPool)
        {
            this._threadPool = threadPool;
        }

        WaterInfosRunThread tExecute = null;
        WaterNETServer.Common.utils.Common utils = new WaterNETServer.Common.utils.Common();
        WaterNETServer.Common.utils.Configuration conf = new WaterNETServer.Common.utils.Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
        string _path = "/Root/schedule/job";

#region 화면 제어 부분 #######################################
        /// <summary>
        /// 쓰레드 상태 표시 버튼
        /// </summary>
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Button _button4;
        private Button _button5;

        public void setButton1(Button button) { this._button1 = button; }
        public void setButton2(Button button) { this._button2 = button; }
        public void setButton3(Button button) { this._button3 = button; }
        public void setButton4(Button button) { this._button4 = button; }
        public void setButton5(Button button) { this._button5 = button; }

        /// <summary>
        /// 쓰레드 상태 표시 프로그래스바
        /// </summary>
        private ProgressBar _progressbar_DMINFO_Modify;
        private ProgressBar _progressbar_DMWSRSRC_Modify;
        private ProgressBar _progressbar_CAINFO_Modify;
        private ProgressBar _progressbar_MTRCHGINFO_Modify;
        private ProgressBar _progressbar_STWCHRG_Modify;
        public void setProgressBar_DMINFO_Modify(ProgressBar progressbar) { this._progressbar_DMINFO_Modify = progressbar; }
        public void setProgressBar_DMWSRSRC_Modify(ProgressBar progressbar) { this._progressbar_DMWSRSRC_Modify = progressbar; }
        public void setProgressBar_CAINFO_Modify(ProgressBar progressbar) { this._progressbar_CAINFO_Modify = progressbar; }
        public void setProgressBar_MTRCHGINFO_Modify(ProgressBar progressbar) { this._progressbar_MTRCHGINFO_Modify = progressbar; }
        public void setProgressBar_STWCHRG_Modify(ProgressBar progressbar) { this._progressbar_STWCHRG_Modify = progressbar; }

#endregion ##########################################

#region Water-INFOS ######################################################

        #region 수용가정보
        private System.Timers.Timer timer_DMINFO = null;
        private Thread thread_DMINFO = null;
        private Hashtable hash_DMINFO = null;
        string time_DMINFO = string.Empty;
        string type_DMINFO = string.Empty;
        public void DMINFO_ThreadStart()
        {
            try
            {
                hash_DMINFO = conf.getSchedule(_path, "DMINFO_Execute");
                time_DMINFO = hash_DMINFO["time"].ToString();
                type_DMINFO = hash_DMINFO["type"].ToString();
                thread_DMINFO = new Thread(new ThreadStart(DMINFO_Timer));
                thread_DMINFO.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void DMINFO_Timer()
        {
            try
            {
                timer_DMINFO            = new System.Timers.Timer();
                timer_DMINFO.Interval   = 1000;
                timer_DMINFO.Elapsed   += new ElapsedEventHandler(DMINFO_Execute);
                timer_DMINFO.Start();

                if (this._threadPool.ContainsKey(timer_DMINFO.GetHashCode()))
                {
                    this._threadPool.Remove(timer_DMINFO.GetHashCode());
                }
                _threadPool.Add(timer_DMINFO.GetHashCode(), timer_DMINFO);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer DMINFO_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_DMINFO.GetHashCode()];
        }
        public void DMINFO_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_DMINFO).Equals((string)(utils.GetTime()[type_DMINFO])))
                {
                    tExecute = new WaterInfosRunThread(_threadPool);
                    tExecute.setButton1(this._button1);
                    tExecute.runThread_DMINFO();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 수용가정보 보정
        private System.Timers.Timer timer_DMINFO_Modify = null;
        private Thread thread_DMINFO_Modify = null;

        private string cTime_DMINFO_Modify = string.Empty;
        private string cTime2_DMINFO_Modify = string.Empty;

        public void DMINFO_Modify_ThreadStart(string cTime, string cTime2)
        {
            cTime_DMINFO_Modify = cTime;
            cTime2_DMINFO_Modify = cTime2;
            thread_DMINFO_Modify = new Thread(new ThreadStart(DMINFO_Modify_Timer));
            thread_DMINFO_Modify.Start();
        }

        public void DMINFO_Modify_Timer()
        {
            timer_DMINFO_Modify = new System.Timers.Timer();
            timer_DMINFO_Modify.Interval = 1000;
            timer_DMINFO_Modify.Start();
            timer_DMINFO_Modify.Elapsed += new ElapsedEventHandler(DMINFO_Modify_Execute);

            Thread.Sleep(1500);
            timer_DMINFO_Modify.Stop();
        }

        public void DMINFO_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new WaterInfosRunThread(_threadPool);
                tExecute.setProgressBar_DMINFO_Modify(this._progressbar_DMINFO_Modify);
                tExecute.runThread_DMINFO_Modify(cTime_DMINFO_Modify, cTime2_DMINFO_Modify);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 상하수도자원
        private System.Timers.Timer timer_DMWSRSRC = null;
        private Thread thread_DMWSRSRC = null;
        private Hashtable hash_DMWSRSRC = null;
        string time_DMWSRSRC = string.Empty;
        string type_DMWSRSRC = string.Empty;
        public void DMWSRSRC_ThreadStart()
        {
            try
            {
                hash_DMWSRSRC = conf.getSchedule(_path, "DMWSRSRC_Execute");
                time_DMWSRSRC = hash_DMWSRSRC["time"].ToString();
                type_DMWSRSRC = hash_DMWSRSRC["type"].ToString();
                thread_DMWSRSRC = new Thread(new ThreadStart(DMWSRSRC_Timer));
                thread_DMWSRSRC.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void DMWSRSRC_Timer()
        {
            try
            {
                timer_DMWSRSRC            = new System.Timers.Timer();
                timer_DMWSRSRC.Interval   = 1000;
                timer_DMWSRSRC.Elapsed   += new ElapsedEventHandler(DMWSRSRC_Execute);
                timer_DMWSRSRC.Start();

                if (this._threadPool.ContainsKey(timer_DMWSRSRC.GetHashCode()))
                {
                    this._threadPool.Remove(timer_DMWSRSRC.GetHashCode());
                }
                _threadPool.Add(timer_DMWSRSRC.GetHashCode(), timer_DMWSRSRC);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer DMWSRSRC_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_DMWSRSRC.GetHashCode()];
        }
        public void DMWSRSRC_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_DMWSRSRC).Equals((string)(utils.GetTime()[type_DMWSRSRC])))
                {
                    tExecute = new WaterInfosRunThread(_threadPool);
                    tExecute.setButton2(this._button2);
                    tExecute.runThread_DMWSRSRC();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 상하수도자원 보정
        private System.Timers.Timer timer_DMWSRSRC_Modify = null;
        private Thread thread_DMWSRSRC_Modify = null;

        private string cTime_DMWSRSRC_Modify = string.Empty;
        private string cTime2_DMWSRSRC_Modify = string.Empty;

        public void DMWSRSRC_Modify_ThreadStart(string cTime, string cTime2)
        {
            cTime_DMWSRSRC_Modify = cTime;
            cTime2_DMWSRSRC_Modify = cTime2;
            thread_DMWSRSRC_Modify = new Thread(new ThreadStart(DMWSRSRC_Modify_Timer));
            thread_DMWSRSRC_Modify.Start();
        }

        public void DMWSRSRC_Modify_Timer()
        {
            timer_DMWSRSRC_Modify = new System.Timers.Timer();
            timer_DMWSRSRC_Modify.Interval = 1000;
            timer_DMWSRSRC_Modify.Start();
            timer_DMWSRSRC_Modify.Elapsed += new ElapsedEventHandler(DMWSRSRC_Modify_Execute);

            Thread.Sleep(1500);
            timer_DMWSRSRC_Modify.Stop();
        }

        public void DMWSRSRC_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new WaterInfosRunThread(_threadPool);
                tExecute.setProgressBar_DMWSRSRC_Modify(this._progressbar_DMWSRSRC_Modify);
                tExecute.runThread_DMWSRSRC_Modify(cTime_DMWSRSRC_Modify, cTime2_DMWSRSRC_Modify);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 민원정보
        private System.Timers.Timer timer_CAINFO = null;
        private Thread thread_CAINFO = null;
        private Hashtable hash_CAINFO = null;
        string time_CAINFO = string.Empty;
        string type_CAINFO = string.Empty;
        public void CAINFO_ThreadStart()
        {
            try
            {
                hash_CAINFO = conf.getSchedule(_path, "CAINFO_Execute");
                time_CAINFO = hash_CAINFO["time"].ToString();
                type_CAINFO = hash_CAINFO["type"].ToString();
                thread_CAINFO = new Thread(new ThreadStart(CAINFO_Timer));
                thread_CAINFO.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void CAINFO_Timer()
        {
            try
            {
                timer_CAINFO = new System.Timers.Timer();
                timer_CAINFO.Interval = 1000;
                timer_CAINFO.Elapsed += new ElapsedEventHandler(CAINFO_Execute);
                timer_CAINFO.Start();

                if (this._threadPool.ContainsKey(timer_CAINFO.GetHashCode()))
                {
                    this._threadPool.Remove(timer_CAINFO.GetHashCode());
                }
                _threadPool.Add(timer_CAINFO.GetHashCode(), timer_CAINFO);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer CAINFO_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_CAINFO.GetHashCode()];
        }
        public void CAINFO_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_CAINFO).Equals((string)(utils.GetTime()[type_CAINFO])))
                {
                    tExecute = new WaterInfosRunThread(_threadPool);
                    tExecute.setButton3(this._button3);
                    tExecute.runThread_CAINFO();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 민원정보 보정
        private System.Timers.Timer timer_CAINFO_Modify = null;
        private Thread thread_CAINFO_Modify = null;

        private string cTime_CAINFO_Modify = string.Empty;
        private string cTime2_CAINFO_Modify = string.Empty;

        public void CAINFO_Modify_ThreadStart(string cTime, string cTime2)
        {
            cTime_CAINFO_Modify = cTime;
            cTime2_CAINFO_Modify = cTime2;
            thread_CAINFO_Modify = new Thread(new ThreadStart(CAINFO_Modify_Timer));
            thread_CAINFO_Modify.Start();
        }

        public void CAINFO_Modify_Timer()
        {
            timer_CAINFO_Modify = new System.Timers.Timer();
            timer_CAINFO_Modify.Interval = 1000;
            timer_CAINFO_Modify.Start();
            timer_CAINFO_Modify.Elapsed += new ElapsedEventHandler(CAINFO_Modify_Execute);

            Thread.Sleep(1500);
            timer_CAINFO_Modify.Stop();
        }

        public void CAINFO_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new WaterInfosRunThread(_threadPool);
                tExecute.setProgressBar_CAINFO_Modify(this._progressbar_CAINFO_Modify);
                tExecute.runThread_CAINFO_Modify(cTime_CAINFO_Modify, cTime2_CAINFO_Modify);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 계량기교체정보
        private System.Timers.Timer timer_MTRCHGINFO = null;
        private Thread thread_MTRCHGINFO = null;
        private Hashtable hash_MTRCHGINFO = null;
        string time_MTRCHGINFO = string.Empty;
        string type_MTRCHGINFO = string.Empty;
        public void MTRCHGINFO_ThreadStart()
        {
            try
            {
                hash_MTRCHGINFO = conf.getSchedule(_path, "MTRCHGINFO_Execute");
                time_MTRCHGINFO = hash_MTRCHGINFO["time"].ToString();
                type_MTRCHGINFO = hash_MTRCHGINFO["type"].ToString();
                thread_MTRCHGINFO = new Thread(new ThreadStart(MTRCHGINFO_Timer));
                thread_MTRCHGINFO.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void MTRCHGINFO_Timer()
        {
            try
            {
                timer_MTRCHGINFO = new System.Timers.Timer();
                timer_MTRCHGINFO.Interval = 1000;
                timer_MTRCHGINFO.Elapsed += new ElapsedEventHandler(MTRCHGINFO_Execute);
                timer_MTRCHGINFO.Start();

                if (this._threadPool.ContainsKey(timer_MTRCHGINFO.GetHashCode()))
                {
                    this._threadPool.Remove(timer_MTRCHGINFO.GetHashCode());
                }
                _threadPool.Add(timer_MTRCHGINFO.GetHashCode(), timer_MTRCHGINFO);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer MTRCHGINFO_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_MTRCHGINFO.GetHashCode()];
        }
        public void MTRCHGINFO_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_MTRCHGINFO).Equals((string)(utils.GetTime()[type_MTRCHGINFO])))
                {
                    tExecute = new WaterInfosRunThread(_threadPool);
                    tExecute.setButton4(this._button4);
                    tExecute.runThread_MTRCHGINFO();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }

        #endregion

        #region 계량기교체정보 보정
        private System.Timers.Timer timer_MTRCHGINFO_Modify = null;
        private Thread thread_MTRCHGINFO_Modify = null;

        private string cTime_MTRCHGINFO_Modify = string.Empty;
        private string cTime2_MTRCHGINFO_Modify = string.Empty;

        public void MTRCHGINFO_Modify_ThreadStart(string cTime, string cTime2)
        {
            cTime_MTRCHGINFO_Modify = cTime;
            cTime2_MTRCHGINFO_Modify = cTime2;
            thread_MTRCHGINFO_Modify = new Thread(new ThreadStart(MTRCHGINFO_Modify_Timer));
            thread_MTRCHGINFO_Modify.Start();
        }

        public void MTRCHGINFO_Modify_Timer()
        {
            timer_MTRCHGINFO_Modify = new System.Timers.Timer();
            timer_MTRCHGINFO_Modify.Interval = 1000;
            timer_MTRCHGINFO_Modify.Start();
            timer_MTRCHGINFO_Modify.Elapsed += new ElapsedEventHandler(MTRCHGINFO_Modify_Execute);

            Thread.Sleep(1500);
            timer_MTRCHGINFO_Modify.Stop();
        }

        public void MTRCHGINFO_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new WaterInfosRunThread(_threadPool);
                tExecute.setProgressBar_MTRCHGINFO_Modify(this._progressbar_MTRCHGINFO_Modify);
                tExecute.runThread_MTRCHGINFO_Modify(cTime_MTRCHGINFO_Modify, cTime2_MTRCHGINFO_Modify);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 요금조정
        private System.Timers.Timer timer_STWCHRG = null;
        private Thread thread_STWCHRG = null;
        private Hashtable hash_STWCHRG = null;
        string time_STWCHRG = string.Empty;
        string type_STWCHRG = string.Empty;
        public void STWCHRG_ThreadStart()
        {
            try
            {
                hash_STWCHRG = conf.getSchedule(_path, "STWCHRG_Execute");
                time_STWCHRG = hash_STWCHRG["time"].ToString();
                type_STWCHRG = hash_STWCHRG["type"].ToString();
                thread_STWCHRG = new Thread(new ThreadStart(STWCHRG_Timer));
                thread_STWCHRG.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void STWCHRG_Timer()
        {
            try
            {
                timer_STWCHRG = new System.Timers.Timer();
                timer_STWCHRG.Interval = 1000;
                timer_STWCHRG.Elapsed += new ElapsedEventHandler(STWCHRG_Execute);
                timer_STWCHRG.Start();

                if (this._threadPool.ContainsKey(timer_STWCHRG.GetHashCode()))
                {
                    this._threadPool.Remove(timer_STWCHRG.GetHashCode());
                }
                _threadPool.Add(timer_STWCHRG.GetHashCode(), timer_STWCHRG);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer STWCHRG_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_STWCHRG.GetHashCode()];
        }
        public void STWCHRG_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_STWCHRG).Equals((string)(utils.GetTime()[type_STWCHRG])))
                {
                    tExecute = new WaterInfosRunThread(_threadPool);
                    tExecute.setButton5(this._button5);
                    tExecute.runThread_STWCHRG();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 요금조정 보정
        private System.Timers.Timer timer_STWCHRG_Modify = null;
        private Thread thread_STWCHRG_Modify = null;

        private string cTime_STWCHRG_Modify = string.Empty;
        private string cTime2_STWCHRG_Modify = string.Empty;

        public void STWCHRG_Modify_ThreadStart(string cTime, string cTime2)
        {
            cTime_STWCHRG_Modify = cTime;
            cTime2_STWCHRG_Modify = cTime2;
            thread_STWCHRG_Modify = new Thread(new ThreadStart(STWCHRG_Modify_Timer));
            thread_STWCHRG_Modify.Start();
        }

        public void STWCHRG_Modify_Timer()
        {
            timer_STWCHRG_Modify = new System.Timers.Timer();
            timer_STWCHRG_Modify.Interval = 1000;
            timer_STWCHRG_Modify.Start();
            timer_STWCHRG_Modify.Elapsed += new ElapsedEventHandler(STWCHRG_Modify_Execute);

            Thread.Sleep(1500);
            timer_STWCHRG_Modify.Stop();
        }

        public void STWCHRG_Modify_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new WaterInfosRunThread(_threadPool);
                tExecute.setProgressBar_STWCHRG_Modify(this._progressbar_STWCHRG_Modify);
                tExecute.runThread_STWCHRG_Modify(cTime_STWCHRG_Modify, cTime2_STWCHRG_Modify);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion
#endregion
    }
}
