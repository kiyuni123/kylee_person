﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNETServer.ScheduleManage.interface1;
using System.Xml;
using EMFrame.log;
using WaterNETServer.Common;
using System.Threading;
using System.Timers;
using WaterNETServer.ScheduleManage.formPopup;
using System.Windows.Forms;

namespace WaterNETServer.ScheduleManage.schedule
{
    public enum ScheduleType {AUTORUN, MANUAL };
    public delegate void ScheduleCallBackEventHandler(ScheduleType TYPE);
    public delegate void NotifyEventHandler(string scheduleLog);

    /// <summary>
    /// 스케줄 정의
    /// 스케줄감시와 스케줄관리의 데이터 형식을 정의하며 스레드를 관리한다.
    /// 모든 스케줄은 해당 클래스를 상속받아서 구현한다.
    /// </summary>
    public class BaseSchedule : IJobMonitoring, IJobManage, IDisposable
    {
        #region 전역 멤버 ###########################################################

        public event ScheduleCallBackEventHandler ScheduleCallBack;
        public event NotifyEventHandler ScheduleNotify;

        /// <summary>
        /// 스케줄 작업 스레드
        /// </summary>
        protected Thread autorunScheduleThread = null;
        protected Thread manualScheduleThread = null;

        Common.utils.Common utils = new Common.utils.Common();
        protected System.Timers.Timer scheduleWorkTimer = null;
        protected IList<DateTime> scheduleWorkItem = new List<DateTime>();

        /// <summary>
        /// 스케줄 설정 XML형식
        /// </summary>
        protected XmlElement scheduleConfig = null;

        /// <summary>
        /// 스케줄 이름
        /// string
        /// </summary>
        protected object schedule_name = DBNull.Value;
        
        /// <summary>
        /// 스케줄 키
        /// string
        /// </summary>
        protected object schedule_key = DBNull.Value;
        
        /// <summary>
        /// 자동 스케줄 최종실행시간
        /// DateTIme
        /// </summary>
        protected object autorun_last_time = DBNull.Value;
        
        /// <summary>
        /// 자동 스케줄 대상건수
        /// int
        /// </summary>
        protected object autorun_target_count = DBNull.Value;
        
        /// <summary>
        /// 자동 스케줄 완료건수
        /// int
        /// </summary>
        protected object autorun_complete_count = DBNull.Value;
        
        /// <summary>
        /// 자동 스케줄 현재상태
        /// stop  : 스레드가 정지상태
        /// ready : 스레드가 구동중이나 작업이 없는 상태
        /// run   : 스레드가 구동중이고 작업중인 상태
        /// string
        /// </summary>
        protected object autorun_state = "stop";

        /// <summary>
        /// 자동 스케줄 작업 진행상태
        /// 프로그레스바의 최대값
        /// int
        /// </summary>
        protected int autorun_max_value = 100;
        
        /// <summary>
        /// 자동 스케줄 작업 진행상태
        /// 프로그레스바의 현재값
        /// int
        /// </summary>
        protected int autorun_value = 0;
        
        /// <summary>
        /// 자동 스케줄 작업 간격
        /// ss     : 1분 단위 작업
        /// mss    : 10분 단위 작업
        /// mmss   : 1시간 단위 작업
        /// hhmmss : 1일 단위 작업
        /// string
        /// </summary>
        protected object autorun_interval = DBNull.Value;
        
        /// <summary>
        /// 자동 스케줄 작업 시작 시간 (자동 스케줄 작업 간격에 따라서 시간단위가 틀려짐)
        /// 값체크 필요
        /// ss     : 매1분 OO 초에 작업 시작
        /// mss    : 매10분 0분 00초에 작업 시작
        /// mmss   : 매1시간 00분 00초에 작업시작
        /// hhmmss : 매1일 00시 00분 00초에 작업시작
        /// string
        /// </summary>
        protected object autorun_time = DBNull.Value;
        
        /// <summary>
        /// 자동 스케줄 일괄시작 포함여부
        /// Y  : 포함
        /// N  : 미포함
        /// string
        /// </summary>
        protected object batch_yn = DBNull.Value;

        /// <summary>
        /// 수동 스케줄 시작 시간
        /// DateTime
        /// </summary>
        protected object manual_startdate;

        /// <summary>
        /// 수동 스케줄 종료 시간
        /// DateTIme
        /// </summary>
        protected object manual_enddate;

        /// <summary>
        /// 수동 스케줄 현재상태
        /// stop  : 스레드가 정지상태
        /// run   : 스레드가 구동중이고 작업중인 상태
        /// string
        /// </summary>
        protected object manual_state = "stop";

        /// <summary>
        /// 수동 스케줄 작업 진행상태
        /// 프로그레스바의 최대값
        /// int
        /// </summary>
        protected int manual_max_value = 100;

        /// <summary>
        /// 수동 스케줄 작업 진행상태
        /// 프로그레스바의 현재값
        /// int
        /// </summary>
        protected int manual_value = 0;

        /// <summary>
        /// 수동 스케줄 대상목록
        /// ILIST<object>
        /// </summary>
        /// 
        protected Form manualTargetForm = null;
        protected object manual_target = null;
        protected bool manual_all_target = true;

        #endregion

        #region 생성자 ##############################################################
        /// <summary>
        /// 스케줄 설정 XML을 인자로 받아 각 스케줄에 값을 설정한다.
        /// </summary>
        /// <param name="scheduleConfig"></param>
        public BaseSchedule(XmlElement scheduleConfig)
        {
            this.scheduleConfig = scheduleConfig;

            this.autorunScheduleThread = new Thread(new ThreadStart(AutorunThread));
            this.autorunScheduleThread.Start();

            this.manualScheduleThread = new Thread(new ThreadStart(ManualThread));
            this.manualScheduleThread.Start();

            try
            {
                //스케줄 이름
                this.schedule_name = scheduleConfig.GetAttribute("name").ToString();
                
                //스케줄 키
                this.schedule_key = scheduleConfig.GetElementsByTagName("key")[0].InnerText;
                
                //자동 스케줄 작업 간격
                this.autorun_interval = scheduleConfig.GetElementsByTagName("type")[0].InnerText;
                
                //자동 스케줄 작업 시간
                this.autorun_time = scheduleConfig.GetElementsByTagName("time")[0].InnerText;
                
                //자동 스케줄 일괄 포함 여부
                this.batch_yn = scheduleConfig.GetElementsByTagName("batch")[0].InnerText;

                //수동 스케줄 작업 시작 시간, 작업 종료 시간
                manual_startdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                manual_enddate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 0);

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        #endregion

        #region 스케줄 설정 저장 ####################################################

        private void SetAUTORUN_INTERVAL(object value)
        {
            try
            {
                this.scheduleConfig.GetElementsByTagName("type")[0].InnerText = value.ToString();
                this.scheduleConfig.OwnerDocument.Save(AppStatic.GLOBAL_CONFIG_FILE_PATH);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void SetAUTORUN_TIME(object value)
        {
            try
            {
                this.scheduleConfig.GetElementsByTagName("time")[0].InnerText = value.ToString();
                this.scheduleConfig.OwnerDocument.Save(AppStatic.GLOBAL_CONFIG_FILE_PATH);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void SetBATCH_YN(object value)
        {
            try
            {
                this.scheduleConfig.GetElementsByTagName("batch")[0].InnerText = value.ToString();
                this.scheduleConfig.OwnerDocument.Save(AppStatic.GLOBAL_CONFIG_FILE_PATH);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        #endregion

        #region 스케줄 기타 #########################################################

        //스케줄 정지/ 시작 이벤트 정의
        private AutoResetEvent autoWait = new AutoResetEvent(false);
        private AutoResetEvent manualWait = new AutoResetEvent(false);

        //자동 스케줄 작업시간 체크 및 저장
        private void scheduleWorkTimer_Tick(object sender, EventArgs e)
        {
            if ("ready".Equals(this.autorun_state.ToString()) || "run".Equals(this.autorun_state.ToString()))
            {
                DateTime Now = DateTime.Now;

                if ("mss".Equals(this.autorun_interval.ToString()))
                {
                    if (this.autorun_time.ToString().Equals(Now.ToString("mmss").Substring(1, 3)))
                    {
                        this.scheduleWorkItem.Add(Now);
                        autoWait.Set();
                    }
                }
                else if ("hhmmss".Equals(this.autorun_interval.ToString()))
                {
                    if (this.autorun_time.ToString().Equals(Now.ToString("HHmmss")))
                    {
                        this.scheduleWorkItem.Add(Now);
                        autoWait.Set();
                    }
                }
                else
                {
                    if (this.autorun_time.ToString().Equals(Now.ToString(this.autorun_interval.ToString())))
                    {
                        this.scheduleWorkItem.Add(Now);
                        autoWait.Set();
                    }
                }
            }
        }

        /// <summary>
        /// 스케줄 자동
        /// </summary>
        private void AutorunThread()
        {
            //자동 스케줄 작업시간 체크 및 저장을 위한 타이머 생성 및 선언
            this.scheduleWorkTimer = new System.Timers.Timer();
            this.scheduleWorkTimer.Interval = 1000;
            this.scheduleWorkTimer.Elapsed += new ElapsedEventHandler(scheduleWorkTimer_Tick);
            this.scheduleWorkTimer.Start();

            while (true)
            {
                if (this.scheduleWorkItem.Count != 0)
                {
                    for (int i = 0; i < this.scheduleWorkItem.Count; i++)
                    {
                        bool isError = false;
                        StringBuilder sb = new StringBuilder();
                        string errllog = null;

                        try
                        {
                            this.autorun_state = "run";
                            this.autorun_value = 0;
                            this.CallScheduleCallBack(ScheduleType.AUTORUN);

                            this.autorun_last_time = this.scheduleWorkItem[0];
                            if (!this.AutorunThread_Execute(this.scheduleWorkItem[0]))
                            {
                                Logger.Error("[[Auto Schedule Error " + this.schedule_name + "]]");
                                Logger.Error(" - 알수없는에러");
                            }
                        }
                        catch (Exception ex)
                        {
                            isError = true;
                            this.autorun_complete_count = 0;
                            Logger.Error(ex);
                            errllog = ex.Message;
                        }
                        finally
                        {
                            sb.Append(((DateTime)this.autorun_last_time).ToString("yyyy-MM-dd HH:mm:ss  "));

                            if (isError)
                            {
                                sb.Append("Auto Schedule Error - [");
                            }
                            else
                            {
                                sb.Append("Auto Schedule Nomal - [");
                            }

                            sb.Append(this.schedule_name);
                            sb.Append("] : ");

                            if (isError)
                            {
                                sb.AppendLine("자동 스케줄을 수행 중 오류가 발생 했습니다.");
                            }
                            else
                            {
                                sb.AppendLine("자동 스케줄을 정상적으로 수행 했습니다.");
                            }


                            if (errllog != null)
                            {
                                sb.Append("    원인: ");
                                sb.AppendLine(errllog);
                            }

                            this.scheduleWorkItem.RemoveAt(0);
                            this.autorun_state = "ready";
                            this.autorun_value = 0;
                            this.CallScheduleCallBack(ScheduleType.AUTORUN);
                            this.ScheduleNotify(sb.ToString());

                            sb.Remove(0, sb.Length);
                        }
                    }
                }

                autoWait.WaitOne();
            }
        }

        /// <summary>
        /// 스케줄 수동
        /// </summary>
        private void ManualThread()
        {
            while (true)
            {
                if ("run".Equals(this.manual_state.ToString()))
                {
                    bool isError = false;
                    StringBuilder sb = new StringBuilder();
                    string errllog = null;

                    try
                    {
                        this.manual_state = "run";
                        this.manual_value = 0;
                        this.CallScheduleCallBack(ScheduleType.MANUAL);

                        //스케줄작업 구현
                        if (!this.ManualThread_Execute((DateTime)this.manual_startdate, (DateTime)this.manual_enddate))
                        {
                            Logger.Error("[[Manual Schedule Error " + this.schedule_name + "]]");
                            Logger.Error(" - 알수없는오류");
                        }
                    }
                    catch (Exception ex)
                    {
                        isError = true;
                        Logger.Error("[[Manual Schedule Error " + this.schedule_name + "]]");
                        Logger.Error(ex);
                        errllog = ex.Message;
                    }
                    finally
                    {
                        sb.Append(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss  "));

                        if (isError)
                        {
                            sb.Append("Manual Schedule Error - [");
                        }
                        else
                        {
                            sb.Append("Manual Schedule Nomal - [");
                        }

                        sb.Append(this.schedule_name);
                        sb.Append("] : ");

                        if (isError)
                        {
                            sb.AppendLine("수동 스케줄을 수행 중 오류가 발생 했습니다.");
                        }
                        else
                        {
                            sb.AppendLine("수동 스케줄을 정상적으로 수행 했습니다.");
                        }

                        if (errllog != null)
                        {
                            sb.Append("    원인: ");
                            sb.AppendLine(errllog);
                        }

                        this.manual_state = "stop";
                        this.manual_value = 0;
                        this.CallScheduleCallBack(ScheduleType.MANUAL);
                        this.ScheduleNotify(sb.ToString());

                        sb.Remove(0, sb.Length);
                    }
                }

                this.manualWait.WaitOne();
            }
        }

        /// <summary>
        /// 수동 스케줄 작업 대상 건수를 반환한다.
        /// </summary>
        /// <returns></returns>
        private object GetManualTargetCount()
        {
            IList<string> targets = this.manual_target as IList<string>;

            if (targets != null && !this.manual_all_target)
            {
                return targets.Count;
            }
            else if (targets != null && this.manual_all_target)
            {
                return null;
            }

            if (this.manual_target != null)
            {
                return this.manual_target;
            }

            return targets;
        }

        /// <summary>
        /// 자동 스케줄 작업 진행상태
        /// 프로그레스바의 최대값
        /// int
        /// </summary>
        public int GetAutorunMaxValue()
        {
            return this.autorun_max_value;
        }

        /// <summary>
        /// 수동 스케줄 작업 진행상태
        /// 프로그레스바의 최대값
        /// int
        /// </summary>
        public int GetManualMaxValue()
        {
            return this.manual_max_value;
        }

        protected void CallScheduleCallBack(ScheduleType Type)
        {
            this.ScheduleCallBack(Type);
            Thread.Sleep(100);
        }

        /// <summary>
        /// 스케줄 수동일경우 대상의 전체 선택 여부를 반환
        /// </summary>
        /// <param name="value"></param>
        public void SetIsManualAllTarget(bool value)
        {
            this.manual_all_target = value;
        }

        /// <summary>
        /// 스케줄 수동일경우 대상의 선택 목록을 반환
        /// </summary>
        /// <param name="value"></param>
        public void SetManualTarget(IList<string> value)
        {
            this.manual_target = value;
        }

        #endregion

        #region 스케줄 구현 #########################################################


        public virtual bool AutorunThread_Execute(DateTime targetDate)
        {
            return true;
        }

        public virtual bool ManualThread_Execute(DateTime startDate, DateTime endDate)
        {
            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public virtual Form GetManualTargetForm()
        {
            return null;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public virtual bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public virtual bool CanManualSchedule()
        {
            return true;
        }

        #endregion

        #region IJobMonitoring 멤버 #################################################

        object IJobMonitoring.SCHEDULE_NAME
        {
            get
            {
                return this.schedule_name;
            }
        }

        object IJobMonitoring.SCHEDULE_KEY
        {
            get
            {
                return this.schedule_key;
            }
        }

        object IJobMonitoring.AUTORUN_LAST_TIME
        {
            get
            {
                return this.autorun_last_time;
            }
        }

        object IJobMonitoring.AUTORUN_TARGET_COUNT
        {
            get
            {
                return this.autorun_target_count;
            }
        }

        object IJobMonitoring.AUTORUN_COMPLETE_COUNT
        {
            get
            {
                return this.autorun_complete_count;
            }
        }


        /// <summary>
        /// 자동 스케줄 작업 간격이 수정되면 XML파일에 값을 저장한다.
        /// </summary>
        object IJobMonitoring.AUTORUN_INTERVAL
        {
            get
            {
                return this.autorun_interval;
            }
            set
            {
                this.autorun_interval = value;
                this.SetAUTORUN_INTERVAL(value);
            }
        }

        /// <summary>
        /// 자동 스케줄 작업 시간이 수정되면 XML파일에 값을 저장한다.
        /// </summary>
        object IJobMonitoring.AUTORUN_TIME
        {
            get
            {
                return this.autorun_time;
            }
            set
            {
                this.autorun_time = value;
                this.SetAUTORUN_TIME(value);
            }
        }

        object IJobMonitoring.AUTORUN_STATE
        {
            get
            {
                return this.autorun_state;
            }
            set
            {
                this.autorun_state = value;
            }
        }

        /// <summary>
        /// 자동 스케줄 일괄 포함 여부가 수정되면 XML파일에 값을 저장한다.
        /// </summary>
        object IJobMonitoring.BATCH_YN
        {
            get
            {
                return this.batch_yn;
            }
            set
            {
                this.batch_yn = value;
                this.SetBATCH_YN(value);
            }
        }

        object IJobMonitoring.AUTORUN_VALUE
        {
            get
            {
                return this.autorun_value;
            }
        }

        #endregion

        #region IJobManage 멤버 #####################################################

        object IJobManage.SCHEDULE_NAME
        {
            get
            {
                return this.schedule_name;
            }
        }

        object IJobManage.SCHEDULE_KEY
        {
            get
            {
                return this.schedule_key;
            }
        }

        object IJobManage.MANUAL_STARTDATE
        {
            get
            {
                return this.manual_startdate;
            }
            set
            {
                this.manual_startdate = value;
            }
        }

        object IJobManage.MANUAL_ENDDATE
        {
            get
            {
                return this.manual_enddate;
            }
            set
            {
                this.manual_enddate = value;
            }
        }

        object IJobManage.MANUAL_STATE
        {
            get
            {
                return this.manual_state;
            }
            set
            {
                this.manual_state = value;

                if ("run".Equals(value.ToString()))
                {
                    this.manualWait.Set();
                }
            }
        }

        object IJobManage.MANUAL_VALUE
        {
            get
            {
                return this.manual_value;
            }
        }

        object IJobManage.MANUAL_TARGET
        {
            get
            {
                return this.GetManualTargetCount();
            }
            set
            {
                this.manual_target = value;
            }
        }

        #endregion

        #region IDisposable 멤버

        public void Dispose()
        {

            if (this.scheduleWorkTimer != null)
            {
                this.scheduleWorkTimer.Stop();
                this.scheduleWorkTimer.Dispose();
                this.scheduleWorkTimer = null;
            }

            if (this.autorunScheduleThread != null)
            {
                this.autorunScheduleThread.Abort();
                this.autorunScheduleThread = null;
            }

            if (this.manualScheduleThread != null)
            {
                this.manualScheduleThread.Abort();
                this.manualScheduleThread = null;
            }
        }

        #endregion
    }
}
