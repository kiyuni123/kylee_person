﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using EMFrame.log;
using System.Threading;
using WaterNETServer.ScheduleManage.interface1;
using WaterNETServer.Common.utils;
using IF_WaterINFOS.IF;
using System.Data;
using IF_WaterINFOS.dao;
using System.Windows.Forms;



/// 요금조정

namespace WaterNETServer.ScheduleManage.schedule
{
    public class STWCHRGSchedule : BaseSchedule
    {
        private string _strSVCKEY = string.Empty;  // 서비스코드
        private string _strSGCCD = string.Empty;   // 지자체코드
        private string _regmngrip = string.Empty;   // 데이터 받는 IP (Water-NET) 
        
        GlobalVariable gVar = new GlobalVariable();
        
        public STWCHRGSchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
            this._strSVCKEY = gVar.SVCKEY;//.get_strSVCKEY();
            this._strSGCCD = gVar.SGCCD;//.getSgccdCode();
            this._regmngrip = gVar.IPADDR;//.getRegmngrip();
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                GetInfosWebService infos = new GetInfosWebService();
                InfosWork infosWork = new InfosWork();

                // Water-INFOS에서 요금조정 받아오기
                DataSet dSet = infos.GetSTWCHRG
                    (
                    this._strSVCKEY,
                    this._regmngrip, 
                    this._strSGCCD,
                    TARGETDATE.ToString("yyyyMM"),
                    TARGETDATE.ToString("yyyyMM")                    
                    );

                this.autorun_target_count = dSet.Tables[0].Rows.Count;
                DateTime if_date = DateTime.Now;
                // IF_STWCHRG에 요금조정 넣기
                infosWork.GetSTWCHRG(dSet, if_date);

                // WI_STWCHRG에 요금조정 넣기
                infosWork.MdfySTWCHRG(if_date.ToString("yyyyMMddHHmmss"));

                // IF_STWCHRG의 2일전 데이터 삭제 (임시테이블 데이터 보관 불필요)
                infosWork.DeleteSTWCHRG(if_date.ToString("yyyyMMdd"));

                this.autorun_value = 100;
                this.autorun_complete_count = this.autorun_target_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                EMFrame.log.Logger.Error(ex.ToString());
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                GetInfosWebService infos = new GetInfosWebService();
                InfosWork infosWork = new InfosWork();
                Common.utils.Common utils = new Common.utils.Common();

                int gatherCnt = utils.MonthBetween(STARTDATE, ENDDATE) + 1;
                double progress = 0;

                for (DateTime d = STARTDATE; d <= ENDDATE; d = d.AddMonths(1))
                {
                    DataSet dSet = infos.GetSTWCHRG(this._strSVCKEY, this._regmngrip, this._strSGCCD, d.ToString("yyyyMM"), d.ToString("yyyyMM"));

                    DateTime if_date = DateTime.Now;
                    infosWork.GetSTWCHRG(dSet, if_date);
                    infosWork.MdfySTWCHRG(if_date.ToString("yyyyMMddHHmmss"));
                    //infosWork.MdfySTWCHRG2(dtStr);

                    progress += 100 / (gatherCnt + 1);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                }

                infosWork.DeleteSTWCHRG(DateTime.Today.ToString("yyyyMMdd"));
            }
            catch (Exception ex)
            {
                EMFrame.log.Logger.Error(ex.ToString());
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }
    }
}
