﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading;
using System.Collections;
using System.Data;
using EMFrame.log;
using WaterNETServer.IF_iwater.dao;
using WaterNETServer.ScheduleManage.interface1;
using System.Windows.Forms;
using WaterNETServer.ScheduleManage.formPopup;
using WaterNETServer.IF_RWIS.dao;
using WaterNETServer.IF_RWIS.work;
using WaterNETServer.Common;



/// 1일 적산(전일 적산)

namespace WaterNETServer.ScheduleManage.schedule
{
    public class AccumulationDaySchedule : BaseSchedule
    {
        string[] kinds = WaterNET.GetInstance().SelectTagGbn(AppStatic.HOURCODE);

        public AccumulationDaySchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
            base.manualTargetForm = new frmTags(this, this.kinds);
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                IList<string> tagList = WaterNET.GetInstance().SelectTagList(kinds);
                this.autorun_target_count = tagList.Count;
                WaterNET.GetInstance().InsertAccumulationDayData(tagList, TARGETDATE.AddDays(-1));
                //1일 누락 발생으로 -2일전 데이터 재수집
                WaterNET.GetInstance().InsertAccumulationDayData(tagList, TARGETDATE.AddDays(-2));
                
                this.autorun_value = 100;
                this.autorun_complete_count = this.autorun_target_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                IList<string> tagList = null;

                if (base.manual_all_target)
                {
                    tagList = WaterNET.GetInstance().SelectTagList(this.kinds);
                }
                else if (!base.manual_all_target)
                {
                    tagList = base.manual_target as List<string>;
                }

                if (tagList == null)
                {
                    throw new Exception("water-NET 전일 적산 태그 목록이 존재하지 않습니다.");
                }

                TimeSpan span = ENDDATE - STARTDATE;
                double progress = 0;

                for (int i = 0; i <= span.TotalDays; i++)
                {
                    if (!"run".Equals(this.manual_state.ToString()))
                    {
                        throw new Exception("사용자가 작업을 중단 했습니다.");
                    }
                    WaterNET.GetInstance().InsertAccumulationDayData(tagList, STARTDATE.AddDays(i));
                    progress += 100.0 / (span.TotalDays + 1.0);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                    Thread.Sleep(500);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }
    }
}
