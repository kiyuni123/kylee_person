﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNETServer.ScheduleManage.interface1;
using EMFrame.form;
using WaterNETServer.ScheduleManage.schedule;
using WaterNETServer.ScheduleManage.work;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace WaterNETServer.ScheduleManage.formPopup
{
    public partial class frmTags : BaseForm
    {
        /// <summary>
        /// 태그구분
        /// </summary>
        private string[] kinds = null;
        
        /// <summary>
        /// 스케줄
        /// </summary>
        private BaseSchedule schedule = null;

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="schedule"></param>
        /// <param name="kinds"></param>
        public frmTags(BaseSchedule schedule, params string[] kinds)
        {
            InitializeComponent();
            this.schedule = schedule;
            this.kinds = kinds;

            Load += new EventHandler(frmTags_Load);
        }

        /// <summary>
        /// 현재 폼이 닫히면 숨긴다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmTags_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();

            if (!this.Owner.IsDisposed)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// 폼 로드 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmTags_Load(object sender, EventArgs e)
        {
            this.InitializeEvent();
            this.InitializeGridColumn();
            this.InitializeGridSetting();
        }

        /// <summary>
        /// 이벤트 선언
        /// </summary>
        private void InitializeEvent()
        {
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.butApply.Click += new EventHandler(butApply_Click);
            this.butSelect.Click += new EventHandler(butSelect_Click);
        }

        /// <summary>
        /// 적용 버튼 클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butApply_Click(object sender, EventArgs e)
        {
            bool isAll = true;
            IList<string> value = new List<string>();

            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                if (!row.Hidden)
                {
                    if (!Convert.ToBoolean(row.Cells["SELECTED"].Value))
                    {
                        isAll = false;
                    }
                    else
                    {
                        value.Add( row.Cells["TAGNAME"].Value.ToString());
                    }
                }
            }

            this.schedule.SetIsManualAllTarget(isAll);

            if (isAll)
            {
                value = null;
            }

            this.schedule.SetManualTarget(value);
            this.Hide();
        }

        /// <summary>
        /// 태그 조회 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butSelect_Click(object sender, EventArgs e)
        {
            this.ultraGrid1.BeginUpdate();
            this.ultraGrid1.Selected.Rows.Clear();
            
            int index = 0;
            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                row.Appearance.ForeColor = Color.DarkGray;

                if (row.Cells["TAGNAME"].Value.ToString().IndexOf(this.texTagname.Text) != -1 &&
                    row.Cells["DESCRIPTION"].Value.ToString().IndexOf(this.texDescription.Text) != -1)
                {
                    row.Appearance.ForeColor = Color.Black;
                    this.ultraGrid1.Rows.Move(row, index);
                    index++;
                }
            }

            if (this.ultraGrid1.Rows.Count != 0)
            {
                this.ultraGrid1.ActiveRowScrollRegion.FirstRow = this.ultraGrid1.Rows[0];
            }
            
            this.ultraGrid1.EndUpdate();
        }

        /// <summary>
        /// 태그 목록 그리드 초기 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            if (this.ultraGrid1.DataSource == null)
            {
                return;
            }

            this.ultraGrid1.BeginUpdate();

            foreach (UltraGridRow row in e.Layout.Rows)
            {
                row.Hidden = false;
                row.Cells["SELECTED"].Value = true;

                //if (this.kinds != null)
                //{
                //    foreach (string kind in this.kinds)
                //    {
                //        if (row.Cells["TAG_GBN"].Value.ToString() == kind)
                //        {
                //            row.Hidden = false;
                //            row.Cells["SELECTED"].Value = true;
                //        }
                //    }
                //}
                //else
                //{
                //    row.Hidden = false;
                //    row.Cells["SELECTED"].Value = true;
                //}
            }

            e.Layout.Bands[0].Columns["SELECTED"].SetHeaderCheckedState(this.ultraGrid1.Rows, true);
            this.ultraGrid1.EndUpdate();
            this.ultraGrid1.UpdateData();
        }

        //그리드 컬럼을 설정한다.
        private void InitializeGridColumn()
        {
            this.ultraGrid1.DisplayLayout.Override.SelectTypeRow = SelectType.ExtendedAutoDrag;

            UltraGridColumn column;

            //태그그리드
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SELECTED";
            column.Header.Caption = "";

            column.SortIndicator = SortIndicator.Disabled;
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            column.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            column.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            column.Width = 30;
            column.Hidden = false;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TAGNAME";
            column.Header.Caption = "태그명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 245;
            column.Hidden = false;
            column.DefaultCellValue = DBNull.Value;

            //column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            //column.Key = "TAG_GBN";
            //column.Header.Caption = "태그구분";
            //column.CellActivation = Activation.NoEdit;
            //column.CellClickAction = CellClickAction.RowSelect;
            //column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //column.CellAppearance.TextHAlign = HAlign.Left;
            //column.CellAppearance.TextVAlign = VAlign.Middle;
            //column.Width = 80;
            //column.Hidden = false;
            //column.DefaultCellValue = DBNull.Value;

            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DESCRIPTION";
            column.Header.Caption = "태그설명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 400;
            column.Hidden = false;
            column.DefaultCellValue = DBNull.Value;
        }

        /// <summary>
        /// 그리드 초기설정, 태그구분이 없는 경우 실시간 태그 조회
        /// </summary>
        private void InitializeGridSetting()
        {
            this.ultraGrid1.DataSource = ScheduleWork.GetInstance().SelectTagTargetList(this.kinds);

            //if (this.kinds == null)
            //{
            //    this.ultraGrid1.DataSource = ScheduleWork.GetInstance().SelectRealtimeTagTargetList();
            //}
            //else
            //{
            //    this.ultraGrid1.DataSource = ScheduleWork.GetInstance().SelectTagTargetList();
            //}
        }
    }
}
