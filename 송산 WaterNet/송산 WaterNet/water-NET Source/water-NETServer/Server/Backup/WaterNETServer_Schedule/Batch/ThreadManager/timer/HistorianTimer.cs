﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using WaterNETServer.ThreadManager.runThread;
using WaterNETServer.Common.utils;
using WaterNETServer.Common;
using EMFrame.log;

namespace WaterNETServer.ThreadManager.timer
{
    public class HistorianTimer
    {
        private Hashtable _threadPool = null;
        HistorianRunThread tExecute = null;
        WaterNETServer.Common.utils.Common utils = new WaterNETServer.Common.utils.Common();
        WaterNETServer.Common.utils.Configuration conf = new WaterNETServer.Common.utils.Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
        string _path = "/Root/schedule/job";

        public HistorianTimer(){}
        public HistorianTimer(Hashtable threadPool)
        {
            this._threadPool = threadPool;
        }

#region 화면 제어 부분 #######################################
        /// <summary>
        /// 쓰레드 상태 표시 버튼
        /// </summary>
        private Button _button1;
        private Button _button2;
        private Button _button3;
        private Button _button4;
        private Button _button5;

        public void setButton1(Button button) { this._button1 = button; }
        public void setButton2(Button button) { this._button2 = button; }
        public void setButton3(Button button) { this._button3 = button; }
        public void setButton4(Button button) { this._button4 = button; }
        public void setButton5(Button button) { this._button5 = button; }

        /// <summary>
        /// 쓰레드 상태 표시 프로그래스바
        /// </summary>
        private ProgressBar _progressbar1;
        private ProgressBar _progressbar2;
        private ProgressBar _progressbar3;
        private ProgressBar _progressbar4;
        private ProgressBar _progressbar5;
        public void setProgressBar1(ProgressBar progressbar) { this._progressbar1 = progressbar; }
        public void setProgressBar2(ProgressBar progressbar) { this._progressbar2 = progressbar; }
        public void setProgressBar3(ProgressBar progressbar) { this._progressbar3 = progressbar; }
        public void setProgressBar4(ProgressBar progressbar) { this._progressbar4 = progressbar; }
        public void setProgressBar5(ProgressBar progressbar) { this._progressbar5 = progressbar; }

#endregion ##########################################

#region Historian 데이터 수집 ###############################################

        #region 실시간 데이터
        private System.Timers.Timer timer_GatherRealtime = null;
        private Thread thread_GatherRealtime = null;
        private Hashtable hash_GatherRealtime = null;
        string time_GatherRealtime = string.Empty;
        string type_GatherRealtime = string.Empty;
        public void GatherRealtime_ThreadStart()
        {
            try
            {
                hash_GatherRealtime = conf.getSchedule(_path, "GatherRealtime_Execute");
                time_GatherRealtime = hash_GatherRealtime["time"].ToString();
                type_GatherRealtime = hash_GatherRealtime["type"].ToString();
                thread_GatherRealtime = new Thread(new ThreadStart(GatherRealtime_Timer));
                thread_GatherRealtime.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void GatherRealtime_Timer()
        {
            try
            {
                timer_GatherRealtime = new System.Timers.Timer();
                timer_GatherRealtime.Interval = 1000;
                timer_GatherRealtime.Elapsed += new ElapsedEventHandler(GatherRealtime_Execute);
                timer_GatherRealtime.Start();

                if (this._threadPool.ContainsKey(timer_GatherRealtime.GetHashCode()))
                {
                    this._threadPool.Remove(timer_GatherRealtime.GetHashCode());
                }
                _threadPool.Add(timer_GatherRealtime.GetHashCode(), timer_GatherRealtime);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer GatherRealtime_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_GatherRealtime.GetHashCode()];
        }
        public void GatherRealtime_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_GatherRealtime).Equals((string)(utils.GetTime()[type_GatherRealtime])))
                {
                    tExecute = new HistorianRunThread(_threadPool);
                    // 쓰레드 상태 표시버튼을 제어하기 위한 객체 넘김
                    tExecute.setButton1(this._button1);

                    tExecute.runThread_GatherRealtime();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 실시간 데이터 누락 체크
        private System.Timers.Timer timer_RecordPasttime = null;
        private Thread thread_RecordPasttime = null;
        private Hashtable hash_RecordPasttime = null;
        string time_RecordPasttime = string.Empty;
        string type_RecordPasttime = string.Empty;
        public void RecordPasttime_ThreadStart()
        {
            try
            {
                hash_RecordPasttime = conf.getSchedule(_path, "RecordPasttime_Execute");
                time_RecordPasttime = hash_RecordPasttime["time"].ToString();
                type_RecordPasttime = hash_RecordPasttime["type"].ToString();

                thread_RecordPasttime = new Thread(new ThreadStart(RecordPasttime_Timer));
                thread_RecordPasttime.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void RecordPasttime_Timer()
        {
            try
            {
                timer_RecordPasttime = new System.Timers.Timer();
                timer_RecordPasttime.Interval = 1000;
                timer_RecordPasttime.Elapsed += new ElapsedEventHandler(RecordPasttime_Execute);
                timer_RecordPasttime.Start();

                if (this._threadPool.ContainsKey(timer_RecordPasttime.GetHashCode()))
                {
                    this._threadPool.Remove(timer_RecordPasttime.GetHashCode());
                }
                _threadPool.Add(timer_RecordPasttime.GetHashCode(), timer_RecordPasttime);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer RecordPasttime_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_RecordPasttime.GetHashCode()];
        }
        public void RecordPasttime_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                // 10분마다 가동 
                // 매 7분 9초마다 : 7분9초, 17분9초, 27분9초, 37분9초, 47분9초, 57분9초
                if ((time_RecordPasttime).Equals((string)(utils.GetTime()[type_RecordPasttime])) && AppStatic.RT_AUTO_MODIFY)
                {
                    if (this._threadPool.ContainsKey("runThread_RecordPasttime"))
                    {
                        if ("RUN".Equals(this._threadPool["runThread_RecordPasttime"].ToString()))
                        {
                            Logger.Error("{0}runThread_RecordPasttime 이미 가동중 : Pass");
                        }
                        else
                        {
                            tExecute = new HistorianRunThread(_threadPool);
                            tExecute.runThread_RecordPasttime();
                        }
                    }
                    else
                    {
                        tExecute = new HistorianRunThread(_threadPool);
                        tExecute.runThread_RecordPasttime();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 실시간 경고
        private System.Timers.Timer timer_RealtimeWarning = null;
        private Thread thread_RealtimeWarning = null;
        private Hashtable hash_RealtimeWarning = null;
        string time_RealtimeWarning = string.Empty;
        string type_RealtimeWarning = string.Empty;
        public void RealtimeWarning_ThreadStart()
        {
            try
            {
                hash_RealtimeWarning = conf.getSchedule(_path, "RealtimeWarning_Execute");
                time_RealtimeWarning = hash_RealtimeWarning["time"].ToString();
                type_RealtimeWarning = hash_RealtimeWarning["type"].ToString();
                thread_RealtimeWarning = new Thread(new ThreadStart(RealtimeWarning_Timer));
                thread_RealtimeWarning.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void RealtimeWarning_Timer()
        {
            try
            {
                timer_RealtimeWarning = new System.Timers.Timer();
                timer_RealtimeWarning.Interval = 1000;
                timer_RealtimeWarning.Elapsed += new ElapsedEventHandler(RealtimeWarning_Execute);
                timer_RealtimeWarning.Start();

                if (this._threadPool.ContainsKey(timer_RealtimeWarning.GetHashCode()))
                {
                    this._threadPool.Remove(timer_RealtimeWarning.GetHashCode());
                }
                _threadPool.Add(timer_RealtimeWarning.GetHashCode(), timer_RealtimeWarning);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer RealtimeWarning_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_RealtimeWarning.GetHashCode()];
        }
        public void RealtimeWarning_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_RealtimeWarning).Equals((string)(utils.GetTime()[type_RealtimeWarning])))
                {
                    tExecute = new HistorianRunThread();
                    tExecute.runThread_RealtimeWarning();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 실시간 데이터 보정

        private System.Timers.Timer timer_ModifyRealtime = null;
        private Thread thread_ModifyRealtime = null;

        private string cTime_ModifyRealtime = string.Empty;
        private string cTime2_ModifyRealtime = string.Empty;

        public void ModifyRealtime_ThreadStart(string cTime, string cTime2)
        {
            cTime_ModifyRealtime = cTime;
            cTime2_ModifyRealtime = cTime2;
            thread_ModifyRealtime = new Thread(new ThreadStart(ModifyRealtime_Timer));
            thread_ModifyRealtime.Start();
        }

        public void ModifyRealtime_Timer()
        {
            timer_ModifyRealtime = new System.Timers.Timer();
            timer_ModifyRealtime.Interval = 1000;
            timer_ModifyRealtime.Start();
            timer_ModifyRealtime.Elapsed += new ElapsedEventHandler(ModifyRealtime_Execute);

            Thread.Sleep(1500);
            timer_ModifyRealtime.Stop();
        }

        /// <summary>
        /// 실시간 값 전체를 1분 단위 보정
        /// </summary>
        public void ModifyRealtime_Execute(Object sender, EventArgs eArgs)
        {
            object threadNum = 1;
            try
            {
                tExecute = new HistorianRunThread(_threadPool);
                // 쓰레드 상태 표시버튼을 제어하기 위한 객체 넘김
                tExecute.setButton5(this._button5);
                tExecute.setProgressBar1(this._progressbar1);
                tExecute.setProgressBar2(this._progressbar2);

                tExecute.runThread_ModifyRealtime(cTime_ModifyRealtime, cTime2_ModifyRealtime);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 1시간 적산
        private System.Timers.Timer timer_Accumulation_Hour = null;
        private Thread thread_Accumulation_Hour = null;
        private Hashtable hash_Accumulation_Hour = null;
        string time_Accumulation_Hour = string.Empty;
        string type_Accumulation_Hour = string.Empty;
        public void Accumulation_Hour_ThreadStart()
        {
            try
            {
                hash_Accumulation_Hour = conf.getSchedule(_path, "Accumulation_Hour_Execute");
                time_Accumulation_Hour = hash_Accumulation_Hour["time"].ToString();
                type_Accumulation_Hour = hash_Accumulation_Hour["type"].ToString();
                thread_Accumulation_Hour = new Thread(new ThreadStart(Accumulation_Hour_Timer));
                thread_Accumulation_Hour.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void Accumulation_Hour_Timer()
        {
            try
            {
                timer_Accumulation_Hour = new System.Timers.Timer();
                timer_Accumulation_Hour.Interval = 1000;
                timer_Accumulation_Hour.Elapsed += new ElapsedEventHandler(Accumulation_Hour_Execute);
                timer_Accumulation_Hour.Start();

                if (this._threadPool.ContainsKey(timer_Accumulation_Hour.GetHashCode()))
                {
                    this._threadPool.Remove(timer_Accumulation_Hour.GetHashCode());
                }
                _threadPool.Add(timer_Accumulation_Hour.GetHashCode(), timer_Accumulation_Hour);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer Accumulation_Hour_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_Accumulation_Hour.GetHashCode()];
        }
        public void Accumulation_Hour_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_Accumulation_Hour).Equals((string)(utils.GetTime()[type_Accumulation_Hour])))
                {
                    tExecute = new HistorianRunThread(_threadPool);
                    // 쓰레드 상태 표시버튼을 제어하기 위한 객체 넘김
                    tExecute.setButton2(this._button2);

                    tExecute.runThread_Accumulation_Hour();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 1시간 적산 보정

        private System.Timers.Timer timer_Modification_Hour = null;
        private Thread thread_Modification_Hour = null;

        private string cTime_Modification_Hour = string.Empty;
        private string cTime2_Modification_Hour = string.Empty;

        public void Modification_Hour_ThreadStart(string cTime, string cTime2)
        {
            cTime_Modification_Hour = cTime;
            cTime2_Modification_Hour = cTime2;
            thread_Modification_Hour = new Thread(new ThreadStart(Modification_Hour_Timer));
            thread_Modification_Hour.Start();
        }

        public void Modification_Hour_Timer()
        {
            timer_Modification_Hour = new System.Timers.Timer();
            timer_Modification_Hour.Interval = 1000;
            timer_Modification_Hour.Start();
            timer_Modification_Hour.Elapsed += new ElapsedEventHandler(Modification_Hour_Execute);

            Thread.Sleep(1500);
            timer_Modification_Hour.Stop();
        }

        public void Modification_Hour_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new HistorianRunThread(_threadPool);
                tExecute.setProgressBar3(this._progressbar3);
                tExecute.runThread_Modification_Hour(cTime_Modification_Hour, cTime2_Modification_Hour);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 1일 적산(전일 적산)

        private System.Timers.Timer timer_Accumulation_Day = null;
        private Thread thread_Accumulation_Day = null;
        private Hashtable hash_Accumulation_Day = null;
        string time_Accumulation_Day = string.Empty;
        string type_Accumulation_Day = string.Empty;
        public void Accumulation_Day_ThreadStart()
        {
            try
            {
                hash_Accumulation_Day = conf.getSchedule(_path, "Accumulation_Day_Execute");
                time_Accumulation_Day = hash_Accumulation_Day["time"].ToString();
                type_Accumulation_Day = hash_Accumulation_Day["type"].ToString();
                thread_Accumulation_Day = new Thread(new ThreadStart(Accumulation_Day_Timer));
                thread_Accumulation_Day.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void Accumulation_Day_Timer()
        {
            try
            {
                timer_Accumulation_Day = new System.Timers.Timer();
                timer_Accumulation_Day.Interval = 1000;
                timer_Accumulation_Day.Elapsed += new ElapsedEventHandler(Accumulation_Day_Execute);
                timer_Accumulation_Day.Start();

                if (this._threadPool.ContainsKey(timer_Accumulation_Day.GetHashCode()))
                {
                    this._threadPool.Remove(timer_Accumulation_Day.GetHashCode());
                }
                _threadPool.Add(timer_Accumulation_Day.GetHashCode(), timer_Accumulation_Day);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer Accumulation_Day_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_Accumulation_Day.GetHashCode()];
        }
        public void Accumulation_Day_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_Accumulation_Day).Equals((string)(utils.GetTime()[type_Accumulation_Day])))
                {
                    tExecute = new HistorianRunThread(_threadPool);
                    tExecute.setButton3(this._button3);

                    tExecute.runThread_Accumulation_Day();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 1일 적산 보정(전일 적산 보정)
        private System.Timers.Timer timer_Modification_Day = null;
        private Thread thread_Modification_Day = null;

        private string cTime_Modification_Day = string.Empty;
        private string cTime2_Modification_Day = string.Empty;

        public void Modification_Day_ThreadStart(string cTime, string cTime2)
        {
            cTime_Modification_Day = cTime;
            cTime2_Modification_Day = cTime2;
            thread_Modification_Day = new Thread(new ThreadStart(Modification_Day_Timer));
            thread_Modification_Day.Start();
        }

        public void Modification_Day_Timer()
        {
            timer_Modification_Day = new System.Timers.Timer();
            timer_Modification_Day.Interval = 1000;
            timer_Modification_Day.Start();
            timer_Modification_Day.Elapsed += new ElapsedEventHandler(Modification_Day_Execute);

            Thread.Sleep(1500);
            timer_Modification_Day.Stop();
        }

        public void Modification_Day_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                tExecute = new HistorianRunThread(_threadPool);
                tExecute.setProgressBar4(this._progressbar4);
                tExecute.runThread_Modification_Day(cTime_Modification_Day, cTime2_Modification_Day);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

        #region 금일적산 (1분)
        private System.Timers.Timer timer_Accumulation_ToDay = null;
        private Thread thread_Accumulation_ToDay = null;
        private Hashtable hash_Accumulation_ToDay = null;
        string time_Accumulation_ToDay = string.Empty;
        string type_Accumulation_ToDay = string.Empty;
        public void Accumulation_ToDay_ThreadStart()
        {

            try
            {
                hash_Accumulation_ToDay = conf.getSchedule(_path, "Accumulation_ToDay_Execute");
                time_Accumulation_ToDay = hash_Accumulation_ToDay["time"].ToString();
                type_Accumulation_ToDay = hash_Accumulation_ToDay["type"].ToString();
                thread_Accumulation_ToDay = new Thread(new ThreadStart(Accumulation_ToDay_Timer));
                thread_Accumulation_ToDay.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        public void Accumulation_ToDay_Timer()
        {
            try
            {
                timer_Accumulation_ToDay = new System.Timers.Timer();
                timer_Accumulation_ToDay.Interval = 1000;
                timer_Accumulation_ToDay.Elapsed += new ElapsedEventHandler(Accumulation_ToDay_Execute);
                timer_Accumulation_ToDay.Start();

                if (this._threadPool.ContainsKey(timer_Accumulation_ToDay.GetHashCode()))
                {
                    this._threadPool.Remove(timer_Accumulation_ToDay.GetHashCode());
                }
                _threadPool.Add(timer_Accumulation_ToDay.GetHashCode(), timer_Accumulation_ToDay);
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }
        public System.Timers.Timer Accumulation_ToDay_Pool()
        {
            return (System.Timers.Timer)_threadPool[timer_Accumulation_ToDay.GetHashCode()];
        }
        public void Accumulation_ToDay_Execute(Object sender, EventArgs eArgs)
        {
            try
            {
                if ((time_Accumulation_ToDay).Equals((string)(utils.GetTime()[type_Accumulation_ToDay])))
                {
                    tExecute = new HistorianRunThread();

                    tExecute.setButton4(this._button4);

                    tExecute.runThread_Accumulation_ToDay();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
        #endregion

#endregion
    }
}
