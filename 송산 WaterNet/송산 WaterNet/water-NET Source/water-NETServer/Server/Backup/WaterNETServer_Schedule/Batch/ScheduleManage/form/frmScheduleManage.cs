﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using EMFrame.form;
using WaterNETServer.Common.utils;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinProgressBar;
using System.Xml;
using System.IO;
using WaterNETServer.Common;
using Infragistics.Win;
using WaterNETServer.ScheduleManage.interface1;
using WaterNETServer.ScheduleManage.schedule;
using WaterNETServer.ScheduleManage.manager;
using EMFrame.log;
using WaterNETServer.ScheduleManage.formPopup;

namespace WaterNETServer.ScheduleManage.form
{
    public partial class frmScheduleManage : BaseForm, IForminterface
    {
        //전역 변수
        private GlobalVariable gVar = new GlobalVariable();

        /// <summary>
        /// 생성자
        /// </summary>
        public frmScheduleManage()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmScheduleManage_Load);
            this.Disposed += new EventHandler(frmScheduleManage_Disposed);
            this.FormClosing += new FormClosingEventHandler(frmScheduleManage_FormClosing);
        }

        /// <summary>
        /// 폼로드 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmScheduleManage_Load(object sender, EventArgs e)
        {
            this.InitializeEvent();
            this.InitializeScheduleMonitoringGrid();
            this.InitializeScheduleMonitoringGridValueList();
            this.InitializeScheduleManageGrid();
            this.InitializeScheduleManageGridValueList();
            this.InitializeSchedule();
        }

        /// <summary>
        /// 스케줄 초기화
        /// global_config.xml의 스케줄 목록을 불러와서 각 스케줄을 설정한다.
        /// </summary>
        private void InitializeSchedule()
        {
            XmlDocument xmlDocument = null;

            //global_config.xml 을 불러온다.
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH))
            {
                xmlDocument = new XmlDocument();
                xmlDocument.Load(AppStatic.GLOBAL_CONFIG_FILE_PATH);
            }

            if (xmlDocument != null)
            {
                XmlNodeList scheduleList = xmlDocument.SelectNodes("/Root/schedule/job");

                IList<IJobMonitoring> monitoring = new List<IJobMonitoring>();
                IList<IJobManage> manage = new List<IJobManage>();

                foreach (XmlElement scheduleConfig in scheduleList)
                {
                    try
                    {
                        //global_config.xml내에 스케줄 key를 클래스명으로 인스턴스한다.
                        string type = scheduleConfig.GetElementsByTagName("key")[0].InnerText;
                        BaseSchedule schedule = ScheduleManager.GetSchedule(scheduleConfig);
                        
                        //스케줄 진행 이벤트 핸들러 (스케줄목록 그리드, 프로그레스바 화면 수정)
                        schedule.ScheduleCallBack += new ScheduleCallBackEventHandler(schedule_ScheduleCallBack);
                        
                        //스케줄 로그 이벤트 핸들러
                        schedule.ScheduleNotify += new NotifyEventHandler(schedule_ScheduleNotify);
                        
                        monitoring.Add(schedule);
                        manage.Add(schedule);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex);
                        Console.WriteLine(ex.StackTrace);
                    }
                }

                //스케줄 자동실행 그리드에 스케줄 설정
                this.griScheduleAuto.DataSource = monitoring;
                
                //스케줄 수동실행 그리드에 스케줄 설정
                this.griScheduleManual.DataSource = manage;
            }
        }

        /// <summary>
        /// 스케줄 진행 이벤트 핸들러 (스케줄목록 그리드, 프로그레스바 화면 수정)
        /// </summary>
        /// <param name="TYPE">스케줄 타입 (자동, 수동)</param>
        private void schedule_ScheduleCallBack(ScheduleType TYPE)
        {
            try
            {
                if (TYPE == ScheduleType.AUTORUN)
                {
                    this.griScheduleAuto.Invoke(new MethodInvoker(this.griScheduleAuto.Refresh));
                }
                if (TYPE == ScheduleType.MANUAL)
                {
                    this.griScheduleManual.Invoke(new MethodInvoker(this.griScheduleManual.Refresh));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        /// 스케줄 로그 이벤트 핸들러
        /// </summary>
        /// <param name="schduleLog">스케줄 로그</param>
        private void schedule_ScheduleNotify(string schduleLog)
        {
            if (this.texScheduleLog.IsHandleCreated)
            {
                this.texScheduleLog.Invoke((MethodInvoker)delegate
                {
                    if (this.texScheduleLog.Lines.Length > 500)
                    {
                        int line = 1;
                        int s_char = this.texScheduleLog.GetFirstCharIndexFromLine(line - 1);
                        int e_char = this.texScheduleLog.GetFirstCharIndexFromLine(line);
                        this.texScheduleLog.Select(s_char, e_char - s_char);
                        this.texScheduleLog.SelectedText = "";
                    }
                    if (schduleLog != null)
                    {
                        this.texScheduleLog.AppendText(schduleLog);
                        this.texScheduleLog.ScrollToCaret();
                    }
                });
            }
        }

        /// <summary>
        /// 프로그램 종료 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmScheduleManage_Disposed(object sender, EventArgs e)
        {
            if (this.griScheduleManual.DataSource != null)
            {
                foreach (BaseSchedule schedule in (IList<IJobManage>)this.griScheduleManual.DataSource)
                {
                    schedule.Dispose();
                }
            }
        }

        /// <summary>
        /// 프로그램 종료 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmScheduleManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.griScheduleManual.DataSource != null)
            {
                foreach (BaseSchedule schedule in (IList<IJobManage>)this.griScheduleManual.DataSource)
                {
                    schedule.Dispose();
                }
            }
        }

        private void InitializeEvent()
        {
            this.griScheduleManual.SizeChanged += new EventHandler(griSchedule_SizeChanged);
            this.griScheduleManual.ClickCellButton += new CellEventHandler(griScheduleManual_ClickCellButton);
            this.griScheduleManual.InitializeLayout += new InitializeLayoutEventHandler(griScheduleManual_InitializeLayout);

            this.griScheduleAuto.SizeChanged += new EventHandler(griSchedule_SizeChanged);
            this.griScheduleAuto.ClickCellButton += new CellEventHandler(griScheduleAuto_ClickCellButton);
            this.griScheduleAuto.InitializeLayout += new InitializeLayoutEventHandler(griScheduleAuto_InitializeLayout);
            this.griScheduleAuto.BeforeCellUpdate += new BeforeCellUpdateEventHandler(griScheduleAuto_BeforeCellUpdate);
            this.griScheduleAuto.AfterCellListCloseUp += new CellEventHandler(griScheduleAuto_AfterCellListCloseUp);
            this.griScheduleAuto.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(griScheduleAuto_BeforeExitEditMode);

            this.butScheduleTrend.Click += new EventHandler(butScheduleTrend_Click);
            this.butScheduleAllStart.Click += new EventHandler(butScheduleAllStart_Click);
            this.butScheduleAllEnd.Click += new EventHandler(butScheduleAllEnd_Click);
        }

        #region 스케줄 감시 그리드 ############################

        /// <summary>
        /// 스케줄 자동 그리드 초기화
        /// </summary>
        private void InitializeScheduleMonitoringGrid()
        {
            this.griScheduleAuto.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.griScheduleAuto.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridColumn column = null;

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SCHEDULE_NAME";
            column.Header.Caption = "스케줄 이름";
            column.Width = 150;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "AUTORUN_LAST_TIME";
            column.Header.Caption = "최종실행시간";
            column.Width = 150;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.Format = "yyyy-MM-dd HH:mm:ss";

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "AUTORUN_TARGET_COUNT";
            column.Header.Caption = "대상건수";
            column.Width = 80;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "AUTORUN_COMPLETE_COUNT";
            column.Header.Caption = "완료건수";
            column.Width = 80;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "AUTORUN_INTERVAL";
            column.Header.Caption = "실행주기";
            column.Width = 100;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.CellClickAction = CellClickAction.Edit;

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "AUTORUN_TIME";
            column.Header.Caption = "실행시간";
            column.Width = 80;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.CellClickAction = CellClickAction.Edit;

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "BATCH_YN";
            column.Header.Caption = "일괄포함";
            column.Width = 70;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "AUTORUN_STATE";
            column.Header.Caption = "현재상태";
            column.Width = 70;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "AUTORUN_VALUE";
            column.Header.Caption = "작업 진행률";
            column.EditorComponent = this.GetProgressBar();
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;

            column = this.griScheduleAuto.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SCHEDULE_KEY";
            column.Hidden = true;
        }

        private void InitializeScheduleMonitoringGridValueList()
        {
            ValueList valueList = null;

            //자동 실행주기
            if (!this.griScheduleAuto.DisplayLayout.ValueLists.Exists("AUTORUN_INTERVAL") &&
                this.griScheduleAuto.DisplayLayout.Bands[0].Columns.IndexOf("AUTORUN_INTERVAL") != -1)
            {
                valueList = this.griScheduleAuto.DisplayLayout.ValueLists.Add("AUTORUN_INTERVAL");

                valueList.ValueListItems.Add("ss", "1분 (ss)");
                valueList.ValueListItems.Add("mss", "10분 (mss)");
                valueList.ValueListItems.Add("mmss", "1시간 (mmss)");
                valueList.ValueListItems.Add("hhmmss", "1일 (hhmmss)");

                this.griScheduleAuto.DisplayLayout.Bands[0].Columns["AUTORUN_INTERVAL"].ValueList =
                    this.griScheduleAuto.DisplayLayout.ValueLists["AUTORUN_INTERVAL"];
            }

            //자동 현재상태
            if (!this.griScheduleAuto.DisplayLayout.ValueLists.Exists("AUTORUN_STATE") &&
                this.griScheduleAuto.DisplayLayout.Bands[0].Columns.IndexOf("AUTORUN_STATE") != -1)
            {
                valueList = this.griScheduleAuto.DisplayLayout.ValueLists.Add("AUTORUN_STATE");

                valueList.ValueListItems.Add("run", "실행");
                valueList.ValueListItems.Add("ready", "준비");
                valueList.ValueListItems.Add("stop", "정지");

                this.griScheduleAuto.DisplayLayout.Bands[0].Columns["AUTORUN_STATE"].ValueList =
                    this.griScheduleAuto.DisplayLayout.ValueLists["AUTORUN_STATE"];
            }

            //일괄포함
            if (!this.griScheduleAuto.DisplayLayout.ValueLists.Exists("BATCH_YN") &&
                this.griScheduleAuto.DisplayLayout.Bands[0].Columns.IndexOf("BATCH_YN") != -1)
            {
                valueList = this.griScheduleAuto.DisplayLayout.ValueLists.Add("BATCH_YN");

                valueList.ValueListItems.Add("Y", "Y");
                valueList.ValueListItems.Add("N", "N");

                this.griScheduleAuto.DisplayLayout.Bands[0].Columns["BATCH_YN"].ValueList =
                    this.griScheduleAuto.DisplayLayout.ValueLists["BATCH_YN"];
            }
        }

        private void griScheduleAuto_ClickCellButton(object sender, CellEventArgs e)
        {
            if ("AUTORUN_STATE".Equals(e.Cell.Column.Key))
            {
                if (e.Cell.Value == null)
                {
                    return;
                }

                if (e.Cell.Value.ToString() == "run" || e.Cell.Value.ToString() == "ready")
                {
                    DialogResult qe = MessageBox.Show("현재 스케줄이 실행 상태입니다.\n스케줄을 중지 하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (qe == DialogResult.Yes)
                    {
                        e.Cell.Value = "stop";
                        e.Cell.Row.Cells["AUTORUN_VALUE"].Value = 0;
                    }
                }
                else if (e.Cell.Value.ToString() == "stop")
                {
                    e.Cell.Value = "ready";
                    e.Cell.Row.Cells["AUTORUN_VALUE"].Value = 0;
                }
            }

            if ("BATCH_YN".Equals(e.Cell.Column.Key))
            {
                if ("Y".Equals(e.Cell.Value.ToString()))
                {
                    e.Cell.Value = "N";
                }
                else if ("N".Equals(e.Cell.Value.ToString()))
                {
                    e.Cell.Value = "Y";
                }
            }
        }

        private void griScheduleAuto_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            if (this.griScheduleAuto.DataSource == null)
            {
                return;
            }

            if (this.griScheduleAuto.Rows.Count == 0)
            {
                return;
            }

            IList<IJobMonitoring> data = this.griScheduleAuto.DataSource as IList<IJobMonitoring>;

            foreach (IJobMonitoring row in data)
            {
                BaseSchedule schedule = row as BaseSchedule;
                if (!schedule.CanAutoSchedule())
                {
                    e.Layout.Rows[data.IndexOf(row)].Hidden = true;
                }
            }
        }

        private void griScheduleAuto_BeforeCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            if ("AUTORUN_TIME".Equals(e.Cell.Column.Key))
            {
                e.Cancel = this.AutoRunTimeCheck(e.Cell);
            }
        }

        private void griScheduleAuto_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            if ("AUTORUN_TIME".Equals(this.griScheduleAuto.ActiveCell.Column.Key))
            {
                e.Cancel = this.AutoRunTimeCheck(this.griScheduleAuto.ActiveCell);
            }
        }

        private void griScheduleAuto_AfterCellListCloseUp(object sender, CellEventArgs e)
        {
            if ("AUTORUN_INTERVAL".Equals(e.Cell.Column.Key))
            {

                e.Cell.Row.Cells["AUTORUN_TIME"].Activate();
                this.griScheduleAuto.PerformAction(UltraGridAction.EnterEditMode, true, true);
            }
        }

        private bool AutoRunTimeCheck(UltraGridCell Cell)
        {
            bool IsError = false;

            if ("ss".Equals(Cell.Row.Cells["AUTORUN_INTERVAL"].Value.ToString()))
            {
                if (Cell.Row.Cells["AUTORUN_TIME"].Text.Length != 2)
                {
                    MessageBox.Show("선택하신 작업은 매 " + Cell.Row.Cells["AUTORUN_INTERVAL"].Text + " 마다 스케줄을 구동합니다. \n00초 단위로 다시 입력해주세요.");
                    IsError = true;
                }
            }
            else if ("mss".Equals(Cell.Row.Cells["AUTORUN_INTERVAL"].Value.ToString()))
            {
                if (Cell.Row.Cells["AUTORUN_TIME"].Text.Length != 3)
                {
                    MessageBox.Show("선택하신 작업은 매 " + Cell.Row.Cells["AUTORUN_INTERVAL"].Text + " 마다 스케줄을 구동합니다. \n0분 00초 단위로 다시 입력해주세요.");
                    IsError = true;
                }
            }
            else if ("mmss".Equals(Cell.Row.Cells["AUTORUN_INTERVAL"].Value.ToString()))
            {
                if (Cell.Row.Cells["AUTORUN_TIME"].Text.Length != 4)
                {
                    MessageBox.Show("선택하신 작업은 매 " + Cell.Row.Cells["AUTORUN_INTERVAL"].Text + " 마다 스케줄을 구동합니다. \n00분 00초 단위로 다시 입력해주세요.");
                    IsError = true;
                }
            }
            else if ("hhmmss".Equals(Cell.Row.Cells["AUTORUN_INTERVAL"].Value.ToString()))
            {
                if (Cell.Row.Cells["AUTORUN_TIME"].Text.Length != 6)
                {
                    MessageBox.Show("선택하신 작업은 매 " + Cell.Row.Cells["AUTORUN_INTERVAL"].Text + " 마다 단위로 스케줄을 구동합니다. \n00시 00분 00초 단위로 다시 입력해주세요.");
                    IsError = true;
                }
            }

            int result = 0;
            if (!Int32.TryParse(Cell.Row.Cells["AUTORUN_TIME"].Text, out result))
            {
                MessageBox.Show("입력하신 시간 형식이 올바르지 않습니다.\n다시 입력해주세요.");
                IsError = true;
            }

            return IsError;
        }

        #endregion


        #region 스케줄 수동 그리드 ############################

        /// <summary>
        /// 스케줄 감시 그리드 초기화
        /// </summary>
        private void InitializeScheduleManageGrid()
        {
            this.griScheduleManual.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.griScheduleManual.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridColumn column = null;

            column = this.griScheduleManual.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SCHEDULE_NAME";
            column.Header.Caption = "스케줄 이름";
            column.Width = 150;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;

            column = this.griScheduleManual.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MANUAL_STARTDATE";
            column.Header.Caption = "시작일자";
            column.Width = 130;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DateTimeWithSpin;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.CellClickAction = CellClickAction.Edit;
            column.Format = "yyyy-MM-dd HH:mm";
            column.MaskInput = "{date} hh:mm";

            column = this.griScheduleManual.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MANUAL_ENDDATE";
            column.Header.Caption = "종료일자";
            column.Width = 130;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DateTimeWithSpin;
            column.Format = "yyyy-MM-dd 23:59";
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.CellClickAction = CellClickAction.Edit;
            column.Format = "yyyy-MM-dd HH:mm";
            column.MaskInput = "{date} hh:mm";

            column = this.griScheduleManual.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MANUAL_TARGET";
            column.Header.Caption = "대상선택";
            column.Width = 70;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.NullText = "전체";

            column = this.griScheduleManual.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MANUAL_STATE";
            column.Header.Caption = "현재상태";
            column.Width = 70;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Header.Appearance.TextVAlign = VAlign.Middle;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;

            column = this.griScheduleManual.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "MANUAL_VALUE";
            column.Header.Caption = "작업 진행률";
            column.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            column.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            column.EditorComponent = this.GetProgressBar();

            column = this.griScheduleManual.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SCHEDULE_KEY";
            column.Hidden = true;
        }

        private void InitializeScheduleManageGridValueList()
        {
            ValueList valueList = null;

            //수동 현재상태
            if (!this.griScheduleManual.DisplayLayout.ValueLists.Exists("MANUAL_STATE") &&
                this.griScheduleManual.DisplayLayout.Bands[0].Columns.IndexOf("MANUAL_STATE") != -1)
            {
                valueList = this.griScheduleManual.DisplayLayout.ValueLists.Add("MANUAL_STATE");

                valueList.ValueListItems.Add("run", "실행");
                valueList.ValueListItems.Add("stop", "정지");

                this.griScheduleManual.DisplayLayout.Bands[0].Columns["MANUAL_STATE"].ValueList =
                    this.griScheduleManual.DisplayLayout.ValueLists["MANUAL_STATE"];
            }
        }

        private void griScheduleManual_ClickCellButton(object sender, CellEventArgs e)
        {
            if ("MANUAL_TARGET".Equals(e.Cell.Column.Key))
            {
                IList<IJobManage> data = this.griScheduleManual.DataSource as IList<IJobManage>;

                if (data != null)
                {

                    BaseSchedule schedule = data[e.Cell.Row.ListIndex] as BaseSchedule;

                    if (schedule != null)
                    {
                        Form form = schedule.GetManualTargetForm();

                        if (form != null)
                        {
                            form.Show(this);
                        }
                    }
                }
            }

            if ("MANUAL_STATE".Equals(e.Cell.Column.Key))
            {
                if (e.Cell.Value == null)
                {
                    return;
                }

                if (e.Cell.Value.ToString() == "run")
                {
                    DialogResult qe = MessageBox.Show("현재 스케줄이 실행 상태입니다.\n스케줄을 중지 하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (qe == DialogResult.Yes)
                    {
                        e.Cell.Value = "stop";
                        e.Cell.Row.Cells["MANUAL_VALUE"].Value = 0;
                    }
                }

                else if (e.Cell.Value.ToString() == "stop")
                {
                    DialogResult qe = MessageBox.Show(e.Cell.Row.Cells["SCHEDULE_NAME"].Text + " 보정 스케줄을 수동으로 실행 하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (qe == DialogResult.Yes)
                    {
                        e.Cell.Value = "run";
                        e.Cell.Row.Cells["MANUAL_VALUE"].Value = 0;
                    }
                }
            }
        }

        private void griScheduleManual_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            if (this.griScheduleManual.DataSource == null)
            {
                return;
            }

            if (this.griScheduleManual.Rows.Count == 0)
            {
                return;
            }

            IList<IJobManage> data = this.griScheduleManual.DataSource as IList<IJobManage>;

            foreach (IJobManage row in data)
            {
                BaseSchedule schedule = row as BaseSchedule;
                if (!schedule.CanManualSchedule())
                {
                    e.Layout.Rows[data.IndexOf(row)].Hidden = true;
                }

                if (schedule.GetManualTargetForm() == null)
                {
                    e.Layout.Rows[data.IndexOf(row)].Cells["MANUAL_TARGET"].Value = "-";
                }
            }
        }

        #endregion


        #region 기타 이벤트 핸들러 ############################

        private void griSchedule_SizeChanged(object sender, EventArgs e)
        {
            UltraGrid target = sender as UltraGrid;

            if (target == null)
            {
                return;
            }

            int totalSize = target.Width;
            int minusSize = 20;
            UltraGridColumn targetColumn = null;

            foreach (UltraGridColumn column in target.DisplayLayout.Bands[0].Columns)
            {
                if (column.EditorComponent is UltraProgressBar)
                {
                    targetColumn = column;
                }
                else
                {
                    if (!column.Hidden)
                    {
                        minusSize += column.Width;
                    }
                }
            }

            if (targetColumn == null)
            {
                return;
            }

            if (totalSize - minusSize < 0)
            {
                return;
            }

            targetColumn.Width = totalSize - minusSize;
        }

        private void butScheduleAllStart_Click(object sender, EventArgs e)
        {
            foreach (UltraGridRow row in this.griScheduleAuto.Rows)
            {
                if ("Y".Equals(row.Cells["BATCH_YN"].Value.ToString()) && !row.Hidden)
                {
                    row.Cells["AUTORUN_STATE"].Value = "ready";
                }
            }
        }

        private void butScheduleAllEnd_Click(object sender, EventArgs e)
        {
            foreach (UltraGridRow row in this.griScheduleAuto.Rows)
            {
                if ("Y".Equals(row.Cells["BATCH_YN"].Value.ToString()) && !row.Hidden)
                {
                    row.Cells["AUTORUN_STATE"].Value = "stop";
                }
            }
        }

        private void butScheduleTrend_Click(object sender, EventArgs e)
        {
            frmTrends oform = new frmTrends();
            oform.Show(this);
        }

        #endregion


        #region 공통 기타 #####################################

        private UltraProgressBar GetProgressBar()
        {
            UltraProgressBar progress = new UltraProgressBar();
            progress.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.SegmentedPartial;
            progress.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            progress.TextVisible = false;
            progress.FillAppearance.BackColor = Color.GreenYellow;
            return progress;
        }

        #endregion


        #region IForminterface 멤버 ###########################

        string IForminterface.FormID
        {
            get { return this.gVar.getFormName(1); }
        }

        string IForminterface.FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion
    }
}
