﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using EMFrame.dm;
using System.Collections;

namespace WaterNETServer.ScheduleManage.dao
{
    public class ScheduleDao
    {
        private static ScheduleDao dao = null;
        private ScheduleDao() { }

        public static ScheduleDao GetInstance()
        {
            if (dao == null)
            {
                dao = new ScheduleDao();
            }
            return dao;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////// 트랜드 조회에서 사용
        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        //태그 구분 목록 조회
        public DataTable SelectTagGbnList(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT A.CODE");
            query.AppendLine("      ,A.CODE_NAME");
            query.AppendLine("  FROM CM_CODE A");
            query.AppendLine(" WHERE A.PCODE = '7010'");
            query.AppendLine("   AND A.USE_YN = 'Y'");

            return manager.ExecuteScriptDataTable(query.ToString(), null);
        }

        //태그 목록 조회
        public DataTable SelectTagList(EMapper manager, Hashtable parameters)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT A.TAGNAME");
            query.AppendLine("      ,A.TAG_DESC DESCRIPTION");
            query.AppendLine("  FROM IF_TAG_GBN A");
            query.AppendLine(" WHERE A.TAG_GBN = :TAG_GBN");

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        //실시간태그
        public void SELECT_GATHER_REALTIME(EMapper manager, DataSet set, Hashtable parameters)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TIMESTAMP");
            query.AppendLine("      ,VALUE");
            query.AppendLine("  FROM (");
            query.AppendLine("        SELECT /*+ INDEX_DESC(A IF_GATHER_REALTIME_PK) */ *");
            query.AppendLine("          FROM IF_GATHER_REALTIME A");
            query.AppendLine("         WHERE A.TAGNAME = :TAGNAME ");
            query.AppendLine("           AND ROWNUM <= TO_NUMBER(:TAG_COUNT)");
            query.AppendLine("       ) ");
            query.AppendLine("  ORDER BY TIMESTAMP ASC");

            manager.FillDataSetScript(set, parameters["TAGNAME"].ToString(), query.ToString(), parameters);
        }

        //1시간태그
        public void SELECT_ACCUMULATION_HOUR(EMapper manager, DataSet set, Hashtable parameters)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TIMESTAMP");
            query.AppendLine("      ,VALUE");
            query.AppendLine("  FROM (");
            query.AppendLine("        SELECT /*+ INDEX_DESC(A IF_ACCUMULATION_HOUR_PK) */ *");
            query.AppendLine("          FROM IF_ACCUMULATION_HOUR A");
            query.AppendLine("         WHERE A.TAGNAME = :TAGNAME ");
            query.AppendLine("           AND ROWNUM <= TO_NUMBER(:TAG_COUNT)");
            query.AppendLine("       ) ");
            query.AppendLine("  ORDER BY TIMESTAMP ASC");

            manager.FillDataSetScript(set, parameters["TAGNAME"].ToString(), query.ToString(), parameters);
        }

        //1일태그
        public void SELECT_ACCUMULATION_YESTERDAY(EMapper manager, DataSet set, Hashtable parameters)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TIMESTAMP");
            query.AppendLine("      ,VALUE");
            query.AppendLine("  FROM (");
            query.AppendLine("        SELECT /*+ INDEX_DESC(A IF_ACCUMULATION_YESTERDAY_PK) */ *");
            query.AppendLine("          FROM IF_ACCUMULATION_YESTERDAY A");
            query.AppendLine("         WHERE A.tagname = :TAGNAME ");
            query.AppendLine("           AND ROWNUM <= TO_NUMBER(:TAG_COUNT)");
            query.AppendLine("       ) ");
            query.AppendLine("  ORDER BY TIMESTAMP ASC");

            manager.FillDataSetScript(set, parameters["TAGNAME"].ToString(), query.ToString(), parameters);
        }

        //금일태그
        public void SELECT_ACCUMULATION_TODAY(EMapper manager, DataSet set, Hashtable parameters)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TIMESTAMP");
            query.AppendLine("      ,VALUE");
            query.AppendLine("  FROM (");
            query.AppendLine("        SELECT /*+ INDEX_DESC(A IF_ACCUMULATION_TODAY_PK) */ *");
            query.AppendLine("          FROM IF_ACCUMULATION_TODAY A");
            query.AppendLine("         WHERE A.TAGNAME = :TAGNAME ");
            query.AppendLine("           AND ROWNUM <= TO_NUMBER(:TAG_COUNT)");
            query.AppendLine("       ) ");
            query.AppendLine("  ORDER BY TIMESTAMP ASC");

            manager.FillDataSetScript(set, parameters["TAGNAME"].ToString(), query.ToString(), parameters);
        }

        //야간최소유량
        public void SELECT_WV_MNF(EMapper manager, DataSet set, Hashtable parameters)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TIMESTAMP");
            query.AppendLine("      ,FILTERING VALUE");
            query.AppendLine("  FROM (");
            query.AppendLine("        SELECT /*+ INDEX_DESC(A IF_WV_MNF_PK) */ *");
            query.AppendLine("          FROM IF_WV_MNF A");
            query.AppendLine("         WHERE A.TAGNAME = :TAGNAME ");
            query.AppendLine("           AND ROWNUM <= TO_NUMBER(:TAG_COUNT)");
            query.AppendLine("       ) ");
            query.AppendLine("  ORDER BY TIMESTAMP ASC");

            manager.FillDataSetScript(set, parameters["TAGNAME"].ToString(), query.ToString(), parameters);
        }
    }
}
