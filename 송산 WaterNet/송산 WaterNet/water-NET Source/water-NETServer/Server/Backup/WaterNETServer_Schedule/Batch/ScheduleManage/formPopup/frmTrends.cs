﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EMFrame.form;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNETServer.ScheduleManage.work;
using System.Collections;
using ChartFX.WinForms;
using System.Threading;

namespace WaterNETServer.ScheduleManage.formPopup
{
    public partial class frmTrends : BaseForm
    {
        private IList<string> trendTagList = new List<string>();
        private System.Windows.Forms.Timer trendTimer = new System.Windows.Forms.Timer();

        /// <summary>
        /// 생성자
        /// </summary>
        public frmTrends()
        {
            InitializeComponent();
            
            //폼 로드완료 이벤트 핸들러
            Load += new EventHandler(frmTrends_Load);
        }

        /// <summary>
        /// 폼 로드완료 이벤트 핸들러 구현
        /// 이벤트 추가, 그리드 초기화, 폼 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmTrends_Load(object sender, EventArgs e)
        {
            this.InitializeEvent();
            this.InitializeGrid();
            this.InitializeChart();
            this.InitializeForm();
            this.InitialzeTimer();
        }

        /// <summary>
        /// 타이머 선언
        /// </summary>
        private void InitialzeTimer()
        {
            //1분
            this.trendTimer.Interval = 60000;
            this.trendTimer.Tick += new EventHandler(trendTimer_Tick);
            this.trendTimer.Start();
        }

        private void trendTimer_Tick(object sender, EventArgs e)
        {
            this.InitialzeChartSetting();
        }

        /// <summary>
        /// 이벤트를 추가한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.comTagGbn.SelectedIndexChanged += new EventHandler(comTagGbn_SelectedIndexChanged);
            this.comDataCount.SelectedIndexChanged += new EventHandler(comDataCount_SelectedIndexChanged);

            this.griTagList.CellChange += new CellEventHandler(griTagList_CellChange);

            this.chaTagTrend.MouseUp += new HitTestEventHandler(chaTagTrend_MouseUp);
            this.chaTagTrend.MouseDown += new HitTestEventHandler(chaTagTrend_MouseDown);
            this.chaTagTrend.MouseMove += new HitTestEventHandler(chaTagTrend_MouseMove);
        }

        /// <summary>
        /// 폼을 초기화 한다.
        /// </summary>
        private void InitializeForm()
        {
            this.comTagGbn.ValueMember = "CODE";
            this.comTagGbn.DisplayMember = "CODE_NAME";

            this.comDataCount.ValueMember = "CODE";
            this.comDataCount.DisplayMember = "CODE_NAME";

            this.comTagGbn.DataSource = ScheduleWork.GetInstance().SelectTagGbnList();

            DataTable data = new DataTable();
            data.Columns.Add("CODE");
            data.Columns.Add("CODE_NAME");
            data.Rows.Add(10, "10");
            data.Rows.Add(50, "50");
            data.Rows.Add(100, "100");
            data.Rows.Add(200, "200");
            data.Rows.Add(400, "400");
            this.comDataCount.DataSource = data;
        }

        /// <summary>
        /// 그리드를 초기화한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.griTagList.DisplayLayout.Override.SelectTypeRow = SelectType.ExtendedAutoDrag;

            UltraGridColumn column;

            column = this.griTagList.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "SELECTED";
            column.Header.Caption = "";

            column.SortIndicator = SortIndicator.Disabled;
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.DataType = typeof(bool);
            column.Width = 30;
            column.Hidden = false;

            column = this.griTagList.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "TAGNAME";
            column.Header.Caption = "태그명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 100;
            column.Hidden = false;
            column.DefaultCellValue = DBNull.Value;

            column = this.griTagList.DisplayLayout.Bands[0].Columns.Add();
            column.Key = "DESCRIPTION";
            column.Header.Caption = "태그설명";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Left;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Width = 200;
            column.Hidden = false;
            column.DefaultCellValue = DBNull.Value;
        }

        /// <summary>
        /// 차트를 초기화한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chaTagTrend.LegendBox.Dock = ChartFX.WinForms.DockArea.Right;
        }

        /// <summary>
        /// 차트 데이터를 세팅한다.
        /// </summary>
        /// <param name="set"></param>
        private void InitialzeChartSetting()
        {
            this.trendTimer.Stop();

            //차트를 초기화
            base.ComponentManager.InitializeLayout(this.chaTagTrend);

            if (this.trendTagList.Count == 0)
            {
                return;
            }

            if (this.comTagGbn.SelectedValue == null || this.comDataCount.SelectedValue == null)
            {
                return;
            }

            string tag_gubun = this.comTagGbn.SelectedValue.ToString();
            string tag_count = this.comDataCount.SelectedValue.ToString(); ;

            //데이터를 받아온다.
            DataSet set = ScheduleWork.GetInstance().SelectTagTrend(this.trendTagList, tag_gubun, tag_count);

            if (set == null || set.Tables.Count == 0)
            {
                return;
            }

            //차트 라인 갯수설정 (DataSet에서 Table갯수만큼 계열을 생성)
            this.chaTagTrend.Data.Series = set.Tables.Count;

            //Y축 설정
            this.chaTagTrend.AxesY[0].LabelsFormat.Format = AxisFormat.Number;
            this.chaTagTrend.AxesY[0].DataFormat.CustomFormat = "###,###.##";
            this.chaTagTrend.AxesY[0].Position = AxisPosition.Near;

            //차트 라인 기본설정 및 계열내 데이터 설정
            foreach (DataTable data in set.Tables)
            {
                int series = set.Tables.IndexOf(data);

                //라인명 설정 (라인명은 태그명으로 하며 태그명은 태이블명과 같다)
                this.chaTagTrend.Series[series].Text = data.TableName;
                this.chaTagTrend.Series[series].MarkerShape = MarkerShape.None;

                foreach (DataRow row in data.Rows)
                {
                    int point = data.Rows.IndexOf(row);

                    //X축 라벨 설정 (차트내 한번만 설정하면 되기때문에 첫번째 라인에서만 설정)
                    if (series == 0)
                    {
                        if (Convert.ToDateTime(row["TIMESTAMP"]).Hour == 0 && Convert.ToDateTime(row["TIMESTAMP"]).Minute == 0 && Convert.ToDateTime(row["TIMESTAMP"]).Second == 0)
                        {
                            this.chaTagTrend.AxisX.Labels[point] = Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyy-MM-dd");
                        }
                        else if (Convert.ToDateTime(row["TIMESTAMP"]).Hour != 0 && Convert.ToDateTime(row["TIMESTAMP"]).Minute == 0 && Convert.ToDateTime(row["TIMESTAMP"]).Second == 0)
                        {
                            this.chaTagTrend.AxisX.Labels[point] = Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyy-MM-dd HH");
                        }
                        else if (Convert.ToDateTime(row["TIMESTAMP"]).Hour != 0 && Convert.ToDateTime(row["TIMESTAMP"]).Minute != 0 && Convert.ToDateTime(row["TIMESTAMP"]).Second == 0)
                        {
                            this.chaTagTrend.AxisX.Labels[point] = Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyy-MM-dd HH:mm");
                        }
                    }

                    //각 라인의 데이터 설정
                    if (row["VALUE"] != null)
                    {
                        this.chaTagTrend.Data[series, point] = Convert.ToDouble(row["VALUE"]);
                    }
                }
            }

            this.trendTimer.Start();
        }

        /// <summary>
        /// 태그구분 콤보박스 이벤트 (선택한 태그구분 태그 목록을 조회한다)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comTagGbn_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SelectTagList();
            this.trendTagList.Clear();
            this.InitialzeChartSetting();
        }

        /// <summary>
        /// 검색건수 콤보박스 이벤트 (검색건수에 따라 데이터 목록을 조회한다)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comDataCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.InitialzeChartSetting();
        }

        /// <summary>
        /// 태그가 선택되거나 해제되면 차트를 변경함
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void griTagList_CellChange(object sender, CellEventArgs e)
        {
            if ("SELECTED".Equals(e.Cell.Column.Key))
            {
                bool check = Convert.ToBoolean(e.Cell.Text);
                if (check)
                {
                    this.trendTagList.Add(e.Cell.Row.Cells["TAGNAME"].Value.ToString());
                }
                else
                {
                    this.trendTagList.Remove(e.Cell.Row.Cells["TAGNAME"].Value.ToString());
                }

                this.InitialzeChartSetting();
            }
        }

        private bool IsMouseDown = false;
        private double MouseCurrentPoint = -1;
        private double MouseClickPoint = -1;

        //마우스 눌리면.. 이벤트핸들러
        private void chaTagTrend_MouseDown(object sender, ChartFX.WinForms.HitTestEventArgs e)
        {
            if (e.Button == MouseButtons.Left && (e.Object is Pane || e.Object is AxisSection))
            {
                this.IsMouseDown = true;
                this.MouseClickPoint = this.MouseCurrentPoint;

                this.chaTagTrend.ConditionalAttributes.Clear();
                this.chaTagTrend.AxisX.Sections.Clear();

                AxisSection section = new AxisSection();
                section.BackColor = Color.DarkGray;
                section.From = this.MouseCurrentPoint;
                section.To = this.MouseCurrentPoint;
                this.chaTagTrend.AxisX.Sections.Add(section);

                this.Cursor = Cursors.VSplit;
            }
        }

        //마우스를 떼면.. 이벤트핸들러
        private void chaTagTrend_MouseUp(object sender, ChartFX.WinForms.HitTestEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.IsMouseDown = false;
            }

            this.IsMouseDown = false;
            this.Cursor = Cursors.Default;
        }

        //마우스를 누르고 드레그할때 이벤트핸들러
        private void chaTagTrend_MouseMove(object sender, HitTestEventArgs e)
        {
            this.MouseCurrentPoint = this.chaTagTrend.AxisX.PixelToValue(e.X);

            if (this.IsMouseDown)
            {
                this.Cursor = Cursors.VSplit;
                this.chaTagTrend.AxisX.Sections[0].To = this.MouseCurrentPoint;

                this.chaTagTrend.ConditionalAttributes.Clear();
                ConditionalAttributes conditionalAttributes = new ConditionalAttributes();
                conditionalAttributes.Condition.DataElement = ChartFX.WinForms.Internal.DataElement.XValue;

                if (this.MouseClickPoint <= this.MouseCurrentPoint)
                {
                    conditionalAttributes.Condition.From = this.MouseClickPoint;
                    conditionalAttributes.Condition.To = this.MouseCurrentPoint;
                }
                else
                {
                    conditionalAttributes.Condition.From = this.MouseCurrentPoint;
                    conditionalAttributes.Condition.To = this.MouseClickPoint;
                }

                conditionalAttributes.MarkerShape = MarkerShape.Rect;
                conditionalAttributes.PointLabels.Visible = true;
                this.chaTagTrend.ConditionalAttributes.Add(conditionalAttributes);
            }
        }

        /// <summary>
        /// 그리드를 초기화한다.
        /// </summary>
        private void SelectTagList()
        {
            Hashtable parameters = new Hashtable();
            parameters.Add("TAG_GBN", this.comTagGbn.SelectedValue);
            this.griTagList.DataSource = ScheduleWork.GetInstance().SelectTagList(parameters);
        }
    }
}
