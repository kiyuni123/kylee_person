﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using WaterNETServer.ScheduleManage.interface1;
using System.Threading;
using WaterNETServer.BatchJobs.dao;
using EMFrame.log;
using System.Windows.Forms;
using WaterNETServer.ScheduleManage.formPopup;



/// 급수전수계산

namespace WaterNETServer.ScheduleManage.schedule
{
    public class DMCountSchedule : BaseSchedule
    {
        public DMCountSchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                BatchJobsWork jobs = new BatchJobsWork();
                this.autorun_target_count = jobs.WV_BlockCount();
                jobs.WV_BlockDMBatch_Day();
                this.autorun_value = 100;
                this.autorun_complete_count = this.autorun_target_count;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                TimeSpan span = ENDDATE - STARTDATE;
                BatchJobsWork jobs = new BatchJobsWork();
                double progress = 0;

                for (int i = 0; i <= span.TotalDays; i++)
                {
                    jobs.WV_BlockDMBatch_Day(STARTDATE.AddDays(i), STARTDATE.AddDays(i));
                    progress += 100.0 / (span.TotalDays + 1.0);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }
    }
}
