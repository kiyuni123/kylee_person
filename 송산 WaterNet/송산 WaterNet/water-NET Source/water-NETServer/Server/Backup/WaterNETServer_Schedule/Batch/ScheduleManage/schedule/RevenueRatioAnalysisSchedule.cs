﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using WaterNETServer.BatchJobs.dao;
using System.Threading;
using EMFrame.log;
using WaterNETServer.ScheduleManage.interface1;
using System.Windows.Forms;



/// 유수율분석

namespace WaterNETServer.ScheduleManage.schedule
{
    public class RevenueRatioAnalysisSchedule : BaseSchedule
    {
        public RevenueRatioAnalysisSchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                BatchJobsWork jobs = new BatchJobsWork();
                this.autorun_target_count = jobs.WV_BlockCount();                  //대상건수
                jobs.WV_BlockRevenueRatioBatch_Month();
                this.autorun_complete_count = this.autorun_target_count;
                this.autorun_value = 100;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                BatchJobsWork jobs = new BatchJobsWork();
                Common.utils.Common utils = new Common.utils.Common();

                int gatherCnt = utils.MonthBetween(STARTDATE, ENDDATE) + 1;
                double progress = 0;

                for (DateTime d = STARTDATE; d <= ENDDATE; d = d.AddMonths(1))
                {
                    jobs.WV_BlockRevenueRatioBatch_Month(d, d);
                    progress += 100 / (gatherCnt + 1);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }
    }
}