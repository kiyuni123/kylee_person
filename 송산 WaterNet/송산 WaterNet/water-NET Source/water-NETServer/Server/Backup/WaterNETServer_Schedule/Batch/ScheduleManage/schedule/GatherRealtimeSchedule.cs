﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading;
using EMFrame.log;
using WaterNETServer.ScheduleManage.interface1;
using System.Data;
using System.Collections;
using WaterNETServer.IF_iwater.dao;
using System.Windows.Forms;
using WaterNETServer.ScheduleManage.formPopup;
using WaterNETServer.IF_RWIS.work;
using WaterNETServer.Common;



/// 실시간 데이터
/// 수동보정 ??

namespace WaterNETServer.ScheduleManage.schedule
{
    public class GatherRealtimeSchedule : BaseSchedule
    {
        string[] kinds = WaterNET.GetInstance().SelectTagGbn(AppStatic.REALTIMECODE);

        public GatherRealtimeSchedule(XmlElement scheduleConfig)
            : base(scheduleConfig)
        {
            
            base.manualTargetForm = new frmTags(this, this.kinds);
        }

        public override bool AutorunThread_Execute(DateTime TARGETDATE)
        {
            try
            {
                IList<string> tagList = WaterNET.GetInstance().SelectTagList(this.kinds);
                this.autorun_target_count = tagList.Count;
                this.autorun_complete_count = WaterNET.GetInstance().InsertGatherRealtimeData(tagList, TARGETDATE.AddMinutes(-4));
                this.autorun_value = 100;
                base.CallScheduleCallBack(ScheduleType.AUTORUN);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }

            return true;
        }

        public override bool ManualThread_Execute(DateTime STARTDATE, DateTime ENDDATE)
        {
            try
            {
                IList<string> tagList = null;

                if (base.manual_all_target)
                {
                    tagList = WaterNET.GetInstance().SelectTagList(this.kinds);
                }
                else if (!base.manual_all_target)
                {
                    tagList = base.manual_target as List<string>;
                }

                if (tagList == null)
                {
                    throw new Exception("water-NET 실시간 데이터 태그 목록이 존재하지 않습니다.");
                }

                if (ENDDATE >= DateTime.Now.AddMinutes(-4))
                {
                    ENDDATE = DateTime.Now.AddMinutes(-5);
                }

                TimeSpan span = ENDDATE - STARTDATE;
                double progress = 0;

                for (int i = 0; i <= span.TotalMinutes; i++)
                {
                    if (!"run".Equals(this.manual_state.ToString()))
                    {
                        throw new Exception("사용자가 작업을 중단 했습니다.");
                    }

                    WaterNET.GetInstance().InsertGatherRealtimeData(tagList, STARTDATE.AddMinutes(i));
                    progress += 100.0 / (span.TotalMinutes + 1.0);
                    this.manual_value = Convert.ToInt32(Math.Floor(progress));
                    base.CallScheduleCallBack(ScheduleType.MANUAL);
                    Thread.Sleep(1);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }

            return true;
        }

        /// <summary>
        /// 수동타겟 설정 폼 반환
        /// </summary>
        /// <returns></returns>
        public override Form GetManualTargetForm()
        {
            return base.manualTargetForm;
        }

        /// <summary>
        /// 자동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanAutoSchedule()
        {
            return true;
        }

        /// <summary>
        /// 수동스케줄에표시여부
        /// </summary>
        /// <returns></returns>
        public override bool CanManualSchedule()
        {
            return true;
        }
    }
}
