﻿using System;
using System.Data;
using System.Collections;
using WaterNETServer.WH_Common.dao;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;

namespace WaterNETServer.WH_Common.work
{
    public class CommonWork : BaseWork
    {
        private static CommonWork work = null;
        private CommonDao dao = null;

        private CommonWork()
        {
            dao = CommonDao.GetInstance();
        }

        public static CommonWork GetInstance()
        {
            if (work == null)
            {
                work = new CommonWork();
            }

            return work;
        }

        //대블록 정보 리스트들을 조회한다. (폼 최초로딩 시)
        public DataSet GetLblockDataList(Hashtable conditions)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                //대블록 정보 조회
                result = dao.GetLblockDataList(manager, conditions);

                //최상위에 Blank 추가
                if (result != null)
                {
                    DataRow row = result.Tables["CM_LOCATION"].NewRow();
                    row["LOC_CODE"] = "";
                    row["LOC_NAME"] = "선택";

                    result.Tables["CM_LOCATION"].Rows.InsertAt(row, 0);
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }


            return result;
        }

        //상위블록코드로 하위블록리스트를 조회
        public DataSet GetBlockDataList(Hashtable conditions)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.GetBlockDataList(manager, conditions);

                //최상위에 Blank 추가
                if (result != null)
                {
                    DataRow row = result.Tables["CM_LOCATION"].NewRow();
                    row["LOC_CODE"] = "";
                    row["LOC_NAME"] = "선택";

                    result.Tables["CM_LOCATION"].Rows.InsertAt(row, 0);
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //Shape 소블록 리스트 조회
        public DataSet GetShapeSblockDataList(Hashtable conditions)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.GetShapeSblockDataList(manager, conditions);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //공통코드 조회
        public DataSet GetCodeList(Hashtable conditions)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = dao.GetCodeList(manager, conditions);

                string removeAllSelect = "N";

                if (conditions["removeAllSelect"] != null)
                {
                    removeAllSelect = (string)conditions["removeAllSelect"];
                }

                if (result != null && "N".Equals(removeAllSelect))
                {
                    DataRow row = result.Tables["CM_CODE"].NewRow();

                    row["CODE"] = "";
                    row["CODE_NAME"] = "전체";

                    result.Tables["CM_CODE"].Rows.InsertAt(row, 0);
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //계통(중블록)코드를 조회한다
        public DataSet GetMblockDataList(Hashtable conditions)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                //계통(중블록) 데이터 조회
                result = dao.GetMblockList(manager, conditions);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //해당 모델의 소블록코드 조회
        public DataSet SelectSblockInModel(Hashtable conditions)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                //계통(중블록) 데이터 조회
                result = dao.SelectSblockInModel(manager, conditions);

                DataRow row = result.Tables["WH_TITLE"].NewRow();

                row["LOC_CODE"] = "";
                row["FTR_IDN"] = "";
                row["LOC_NAME"] = "전체";

                result.Tables["WH_TITLE"].Rows.InsertAt(row, 0);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }
    }
}
