﻿using System.Data;
using System.Collections;
using System.Text;

using EMFrame.dm;

namespace WaterNETServer.WH_Common.dao
{
    public class CommonDao
    {
        private static CommonDao dao = null;

        private CommonDao()
        {
        }

        public static CommonDao GetInstance()
        {
            if (dao == null)
            {
                dao = new CommonDao();
            }

            return dao;
        }

        //대블록정보 조회
        public DataSet GetLblockDataList(EMapper manager, Hashtable conditons)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          LOC_CODE			    ");
            queryString.AppendLine("                ,PLOC_CODE			    ");
            queryString.AppendLine("                ,LOC_GBN			    ");
            queryString.AppendLine("                ,LOC_NAME			    ");
            queryString.AppendLine("from            CM_LOCATION			    ");
            queryString.AppendLine("where           1 = 1			        ");
            queryString.AppendLine("and             LOC_CODE = '000012'		");
            queryString.AppendLine("order by        LOC_NAME                ");

            return manager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_LOCATION");
        }

        //중,소블록정보 조회
        public DataSet GetBlockDataList(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          LOC_CODE			                            ");
            queryString.AppendLine("                ,PLOC_CODE			                            ");
            queryString.AppendLine("                ,LOC_GBN			                            ");
            queryString.AppendLine("                ,LOC_NAME			                            ");
            queryString.AppendLine("from            CM_LOCATION			                            ");
            queryString.AppendLine("where           1 = 1                                           ");
            queryString.AppendLine("and             PLOC_CODE   = '" + conditions["PLOC_CODE"] + "' ");
            queryString.AppendLine("order by        LOC_NAME                                        ");

            return manager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_LOCATION");
        }

        //계통(중블록)코드 조회
        public DataSet GetMblockList(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          LOC_CODE            ");
            queryString.AppendLine("                ,LOC_NAME           ");
            queryString.AppendLine("from            CM_LOCATION         ");
            queryString.AppendLine("where           FTR_CODE = 'BZ002'  ");
            queryString.AppendLine("order by        LOC_NAME            ");

            return manager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_LOCATION");
        }

        //소블록 Shape Code List 조회
        public DataSet GetShapeSblockDataList(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          LOC_CODE                ");
            queryString.AppendLine("                ,PLOC_CODE              ");
            queryString.AppendLine("                ,LOC_NAME               ");
            queryString.AppendLine("                ,FTR_IDN                ");
            queryString.AppendLine("from            CM_LOCATION             ");
            queryString.AppendLine("where           1 = 1                   ");
            queryString.AppendLine("and             FTR_CODE    = 'BZ003'   ");
            queryString.AppendLine("order by        LOC_NAME                ");

            return manager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_LOCATION");
        }

        //해당 모델의 소블록코드 조회
        public DataSet SelectSblockInModel(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      LOC_CODE                                                ");
            queryString.AppendLine("            ,LOC_NAME                                               ");
            queryString.AppendLine("            ,FTR_IDN                                                ");
            queryString.AppendLine("from        WH_TITLE        a                                       ");
            queryString.AppendLine("            ,CM_LOCATION    b                                       ");
            queryString.AppendLine("where       a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and         a.MFTRIDN       = b.PLOC_CODE                           ");
            queryString.AppendLine("order by    b.LOC_NAME                                              ");

            return manager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //공통코드 조회
        public DataSet GetCodeList(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          CODE                                        ");
            queryString.AppendLine("                ,CODE_NAME                                  ");
            queryString.AppendLine("from            CM_CODE                                     ");
            queryString.AppendLine("where           PCODE   = '" + conditions["PCODE"] + "'     ");
            queryString.AppendLine("order by        CODE                                        ");

            return manager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_CODE");
        }

        //JUNCTION/NODE ID로 지역정보 조회
        public DataSet GetLocationInfo(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      b.LOC_CODE 			                                                                                                                                        ");
            queryString.AppendLine("from        WH_TAGS         a			                                                                                                                                ");
            queryString.AppendLine("            ,CM_LOCATION    b			                                                                                                                                ");
            queryString.AppendLine("where       a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                            ");
            queryString.AppendLine("and         a.ID            = '" + conditions["ID"] + "'			                                                                                                    ");

            if ("NODE".Equals(conditions["TYPE"].ToString()))
            {
                queryString.AppendLine("and     a.TYPE          = 'NODE'			");
                queryString.AppendLine("and     substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 1)+1,instr(a.POSITION_INFO, '|', 1, 2)-1 - instr(a.POSITION_INFO, '|', 1, 1)) = b.FTR_CODE			");
                queryString.AppendLine("and     substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 2)+1,length(a.POSITION_INFO) - instr(a.POSITION_INFO, '|', 1, 2))             = b.FTR_IDN             ");
            }
            else if ("LINK".Equals(conditions["TYPE"].ToString()))
            {
                queryString.AppendLine("and     a.TYPE          = 'LINK'			");
                queryString.AppendLine("and     substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 5)+1,instr(a.POSITION_INFO, '|', 1, 6)-1 - instr(a.POSITION_INFO, '|', 1, 5)) = b.FTR_CODE			");
                queryString.AppendLine("and     substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 6)+1,length(a.POSITION_INFO) - instr(a.POSITION_INFO, '|', 1, 6))             = b.FTR_IDN             ");
            }

            return manager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TAGS");
        }
    }
}
