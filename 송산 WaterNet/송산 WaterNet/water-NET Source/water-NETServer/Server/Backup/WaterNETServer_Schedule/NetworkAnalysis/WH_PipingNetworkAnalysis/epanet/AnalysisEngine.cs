﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using WaterNETServer.WH_PipingNetworkAnalysis.dao;
using WaterNETServer.WH_PipingNetworkAnalysis.tmpUtils;
using WaterNETServer.WH_Common.utils;
using WaterNETServer.Common.utils;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;
using WaterNETServer.Warning.work;

namespace WaterNETServer.WH_PipingNetworkAnalysis.epanet
{
    public class AnalysisEngine : BaseWork
    {
        private const int EN_ELEVATION = 0;    ///Node parameters
        private const int EN_BASEDEMAND = 1;
        private const int EN_PATTERN = 2;
        private const int EN_EMITTER = 3;
        private const int EN_INITQUAL = 4;
        private const int EN_SOURCEQUAL = 5;
        private const int EN_SOURCEPAT = 6;
        private const int EN_SOURCETYPE = 7;
        private const int EN_TANKLEVEL = 8;
        private const int EN_DEMAND = 9;
        private const int EN_HEAD = 10;
        private const int EN_PRESSURE = 11;
        private const int EN_QUALITY = 12;
        private const int EN_SOURCEMASS = 13;
        private const int EN_INITVOLUME = 14;
        private const int EN_MIXMODEL = 15;
        private const int EN_MIXZONEVOL = 16;

        private const int EN_TANKDIAM = 17;
        private const int EN_MINVOLUME = 18;
        private const int EN_VOLCURVE = 19;
        private const int EN_MINLEVEL = 20;
        private const int EN_MAXLEVEL = 21;
        private const int EN_MIXFRACTION = 22;
        private const int EN_TANK_KBULK = 23;

        private const int EN_DIAMETER = 0;    ///Link parameters
        private const int EN_LENGTH = 1;
        private const int EN_ROUGHNESS = 2;
        private const int EN_MINORLOSS = 3;
        private const int EN_INITSTATUS = 4;
        private const int EN_INITSETTING = 5;
        private const int EN_KBULK = 6;
        private const int EN_KWALL = 7;
        private const int EN_FLOW = 8;
        private const int EN_VELOCITY = 9;
        private const int EN_HEADLOSS = 10;
        private const int EN_STATUS = 11;
        private const int EN_SETTING = 12;
        private const int EN_ENERGY = 13;

        private const int EN_DURATION = 0;    ///Time parameters
        private const int EN_HYDSTEP = 1;
        private const int EN_QUALSTEP = 2;
        private const int EN_PATTERNSTEP = 3;
        private const int EN_PATTERNSTART = 4;
        private const int EN_REPORTSTEP = 5;
        private const int EN_REPORTSTART = 6;
        private const int EN_RULESTEP = 7;
        private const int EN_STATISTIC = 8;
        private const int EN_PERIODS = 9;

        private const int EN_NODECOUNT = 0;    ///Component counts
        private const int EN_TANKCOUNT = 1;
        private const int EN_LINKCOUNT = 2;
        private const int EN_PATCOUNT = 3;
        private const int EN_CURVECOUNT = 4;
        private const int EN_CONTROLCOUNT = 5;

        private const int EN_JUNCTION = 0;    ///Node types
        private const int EN_RESERVOIR = 1;
        private const int EN_TANK = 2;

        private const int EN_CVPIPE = 0;    ///Link types
        private const int EN_PIPE = 1;
        private const int EN_PUMP = 2;
        private const int EN_PRV = 3;
        private const int EN_PSV = 4;
        private const int EN_PBV = 5;
        private const int EN_FCV = 6;
        private const int EN_TCV = 7;
        private const int EN_GPV = 8;

        private const int EN_NONE = 0;   ///Quality analysis types
        private const int EN_CHEM = 1;
        private const int EN_AGE = 2;
        private const int EN_TRACE = 3;

        private const int EN_CONCEN = 0;    ///Source quality types
        private const int EN_MASS = 1;
        private const int EN_SETPOINT = 2;
        private const int EN_FLOWPACED = 3;

        private const int EN_CFS = 0;    ///Flow units types
        private const int EN_GPM = 1;
        private const int EN_MGD = 2;
        private const int EN_IMGD = 3;
        private const int EN_AFD = 4;
        private const int EN_LPS = 5;
        private const int EN_LPM = 6;
        private const int EN_MLD = 7;
        private const int EN_CMH = 8;
        private const int EN_CMD = 9;

        private const int EN_TRIALS = 0;   ///Misc. options             
        private const int EN_ACCURACY = 1;
        private const int EN_TOLERANCE = 2;
        private const int EN_EMITEXPON = 3;
        private const int EN_DEMANDMULT = 4;

        private const int EN_LOWLEVEL = 0;   ///Control types             
        private const int EN_HILEVEL = 1;
        private const int EN_TIMER = 2;
        private const int EN_TIMEOFDAY = 3;

        private const int EN_AVERAGE = 1;   ///Time statistic types.     
        private const int EN_MINIMUM = 2;
        private const int EN_MAXIMUM = 3;
        private const int EN_RANGE = 4;

        private const int EN_MIX1 = 0;   ///Tank mixing models        
        private const int EN_MIX2 = 1;
        private const int EN_FIFO = 2;
        private const int EN_LIFO = 3;

        private const int EN_NOSAVE = 0;   ///Save-results-to-file flag 
        private const int EN_SAVE = 1;
        private const int EN_INITFLOW = 10;  ///Re-initialize flow flag

        private INPManageDao dao = null;
        private AnalysisDao aDao = null;
        private WarningWork wWork = null;

        //해석 오류 시 원래 해석정보를 기록하기 위해 argument를 담아둔다.
        Hashtable tmpConditions = null;

        //해석오류시 경고저장을 위한 변수
        bool isSaveError = false;
        float lowPressure = 0;
        string locCode = "";
        string inpNumber = "";

        //해석오류의 순서
        int errorIdx = 0;

        public AnalysisEngine()
        {
            dao = INPManageDao.GetInstance();
            aDao = AnalysisDao.GetInstance();
            wWork = WarningWork.GetInstance();
        }

        //관망해석 실행 - 외부노출
        public Hashtable Execute(Hashtable conditions)
        {

            DataSet settingDataSet = SelectAnalysisSetting(conditions);
            DataTable settingDataTable = settingDataSet.Tables["WH_ANALYSIS_SETTING"];

            if (settingDataTable.Rows.Count != 0)
            {
                if ("Y".Equals(settingDataTable.Rows[0]["ERROR_SAVE_YN"].ToString()))
                {
                    isSaveError = true;                                                                 //에러저장여부
                    lowPressure = float.Parse(settingDataTable.Rows[0]["LOW_PRESSURE"].ToString());     //설정수압
                }
                else
                {
                    isSaveError = false;
                }
            }

            //관망해석 결과 저장용
            Hashtable result = null;

            tmpConditions = conditions;

            try
            {
                //수질/수리해석 구분 (기본은 수리해석)
                string analysisFlag = "H";

                //자동실행/수동실행 구분 (기본은 수동실행)
                string autoManual = "M";

                //해석결과 DB저장 여부
                bool saveReport = false;

                if (conditions["analysisFlag"] != null)
                {
                    analysisFlag = (string)conditions["analysisFlag"];
                }

                if (conditions["AUTO_MANUAL"] != null)
                {
                    autoManual = (string)conditions["AUTO_MANUAL"];
                }

                if (conditions["saveReport"] != null)
                {
                    saveReport = (bool)conditions["saveReport"];
                }

                Console.WriteLine("파일생성..." + conditions["INP_NUMBER"]);

                //파일이 중복되는 경우를 막기위해 파일명을 랜덤하게 재생성한다.
                conditions.Add("FILE_NAME", Utils.GetSerialNumber("IN") + ".inp");
                conditions.Add("rptFileName", Utils.GetSerialNumber("RP") + ".rpt");

                //파일생성
                DataSet InpMasterData = GenerateINPFile(conditions);

                Console.WriteLine("파일생성 완료..." + conditions["INP_NUMBER"]);

                Console.WriteLine("해석시작..." + conditions["INP_NUMBER"]);

                if ("H".Equals(analysisFlag))
                {
                    //수리해석
                    result = RunHydraulicAnalysis(conditions);
                }
                else if ("Q".Equals(analysisFlag))
                {
                    //수질해석
                    result = RunQualityAnalysis(conditions);
                }

                Console.WriteLine("해석완료..." + conditions["INP_NUMBER"]);

                if (result == null)
                {
                    Console.WriteLine("결과가 널임...");
                }

                //해석결과 저장
                if (result != null && saveReport)
                {
                    Console.WriteLine("저장시작...");
                    //Rpt Master 정보 생성
                    Hashtable rptMasterData = new Hashtable();

                    //에너지 관련 결과 저장 : wjs추가 2012.07.19
                    string rptNumber = Utils.GetSerialNumber("RP");

                    rptMasterData.Add("RPT_NUMBER", rptNumber);
                    rptMasterData.Add("WSP_NAM", "");
                    rptMasterData.Add("LFTRIDN", Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["LFTRIDN"]));
                    rptMasterData.Add("MFTRIDN", Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["MFTRIDN"]));
                    rptMasterData.Add("SFTRIDN", Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["SFTRIDN"]));
                    rptMasterData.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                    rptMasterData.Add("TITLE", Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["TITLE"]));
                    rptMasterData.Add("AUTO_MANUAL", autoManual);
                    rptMasterData.Add("TARGET_DATE", conditions["TARGET_DATE"]);
                   
                    /////wjs
                    InsertAnalysisResultData(rptMasterData, result);

                    //에너지 관련 결과 저장 : wjs추가 2012.07.19
                    if (GetEnergyModel((string)conditions["INP_NUMBER"]).ToUpper().Equals("Y"))
                    {
                        UpdateEnergyHydrodynamicResult(rptNumber);    
                    }
                    

                    //경고 시 저장될 지역코드 (소블록부터 확인한다.)
                    if(!"".Equals(Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["SFTRIDN"])))
                    {
                        locCode = Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["SFTRIDN"]);
                    }
                    else if(!"".Equals(Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["MFTRIDN"])))
                    {
                        locCode = Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["MFTRIDN"]);
                    }
                    else if(!"".Equals(Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["LFTRIDN"])))
                    {
                        locCode = Utils.nts(InpMasterData.Tables["WH_TITLE"].Rows[0]["LFTRIDN"]);
                    }

                    //경고 시 저장될 inp 번호
                    inpNumber = (string)conditions["INP_NUMBER"];

                    Console.WriteLine("저장완료...");
                }

                //관망해석에 사용된 파일 삭제
                FileInfo inpFileInfo = new FileInfo(TmpVariables.INP_FILE_PATH + conditions["FILE_NAME"]);
                FileInfo rptFileInfo = new FileInfo(TmpVariables.RPT_FILE_PATH + conditions["rptFileName"]);

                if (inpFileInfo.Exists && "Y".Equals(Common.AppStatic.EPANET_INP_DELETE))
                {
                    inpFileInfo.Delete();
                }

                if (rptFileInfo.Exists)
                {
                    rptFileInfo.Delete();
                }
            }
            catch (IOException ie)
            {
                Logger.Error(ie);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return result;
        }

        //관망해석 설정 조회
        public DataSet SelectAnalysisSetting(Hashtable conditions)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                result = aDao.SelectAnalysisSetting(manager, conditions);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //관망해석 실행 
        //
        //현재 기본경로는 C:\Inetpub\ftproot\로 설정되어 있으며 해당 폴더를 생성하고 파일을 넣은다음 파일명을 argument로 던져야 함
        //rpt파일은 C:\Inetpub\ftproot\Test.rpt에 자동생성되나 관망해석 로깅만 남음 (이후 필요에 의해 내용 추가)
        //
        //analisysType : (0 : 설정된 시간/횟수만큼 실행, 1 : 1회만 실행)
        //resetValues : Hashtable형태이며 key는 다음과 같다
        //              demands - 사용량 데이터
        //              pressurs - 압력데이터
        //              elevations - 배수지 수위
        //              *필요없는경우 argument로 null을 넘김*
        //saveReport : 해석결과를 DB에 저장
        public Hashtable RunHydraulicAnalysis(Hashtable conditions)
        {
            string inpFileName = (string)conditions["FILE_NAME"];
            string rptFileName = (string)conditions["rptFileName"];

            int analysisType = (int)conditions["analysisType"];
            Hashtable resetValues = (Hashtable)conditions["resetValues"];
            Hashtable analysisResult = new Hashtable();

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENopen(new StringBuilder(TmpVariables.INP_FILE_PATH + inpFileName), new StringBuilder(TmpVariables.RPT_FILE_PATH + rptFileName), new StringBuilder(""))))
            {
                //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.               
                //AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                return null;
            }

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENopenH()))
            {
                //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                //AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                return null;
            }

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENinitH(0)))
            {
                //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                return null;
            }

            long t = 0;

            if (analysisType == 0)
            {
                //주어진 시간만큼 관망해석 실행

                long tstep = 0;
                long hydStep = 0;
                long duration = 0;

                long reportStep = 0;

                if (!AnalysisErrorHandling(EPANETMethodDefinition.ENgettimeparam(EN_HYDSTEP, ref hydStep)))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                    AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                    return null;
                }

                if (!AnalysisErrorHandling(EPANETMethodDefinition.ENgettimeparam(EN_DURATION, ref duration)))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                    AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                    return null;
                }

                if (!AnalysisErrorHandling(EPANETMethodDefinition.ENgettimeparam(EN_REPORTSTEP, ref reportStep)))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                    AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                    return null;
                }

                do
                {
                    if (!AnalysisErrorHandling(EPANETMethodDefinition.ENrunH(ref t)))
                    {
                        //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                        AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                        AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                        return null;
                    }

                    //간혹 step에 맞지않는 시간이 도출되는 경우가 있을때를 필터링 (이유는 불명)
                    //if ((t % hydStep) == 0)
                    if ((t % reportStep) == 0)
                    {
                        Hashtable timeResult = new Hashtable();
                        //Console.WriteLine("******************************* 시간 : " + (Convert.ToString(t / 60 / 60)).PadLeft(2, '0') + ":" + Convert.ToString((t - (t / 60 / 60 * 60 * 60)) / 60).PadLeft(2, '0'));

                        timeResult.Add("node", ExtractNodeAnalysisResult());
                        timeResult.Add("link", ExtractLinkAnalysisResult());

                        if ((t/60/60) == 1)  //확장해석시 01:00 결과만 저장하고, 00:00시로 변경하는 로직 추가됨(실시간해석에서만사용)
                        {
                            analysisResult.Add("00:00", timeResult);
                            //해석시간을 key로 하여 node,junction 해석결과를 저장
                            //analysisResult.Add((Convert.ToString(t / 60 / 60)).PadLeft(2, '0') + ":" + Convert.ToString((t - (t / 60 / 60 * 60 * 60)) / 60).PadLeft(2, '0'), timeResult);
                        }
                    }

                    if (!AnalysisErrorHandling(EPANETMethodDefinition.ENnextH(ref tstep)))
                    {
                        //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                        AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                        AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                        return null;
                    }
                }
                while (tstep > 0);
            }
            else if (analysisType == 1)
            {
                //관망해석 1회 실행
                if (!AnalysisErrorHandling(EPANETMethodDefinition.ENrunH(ref t)))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                    AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                    return null;
                }

                Hashtable timeResult = new Hashtable();

                timeResult.Add("node", ExtractNodeAnalysisResult());
                timeResult.Add("link", ExtractLinkAnalysisResult());

                analysisResult.Add("00:00", timeResult);
            }

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH()))
            {
                //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                AnalysisErrorHandling(EPANETMethodDefinition.ENclose());
                return null;
            }

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENclose()))
            {
                return null;
            }

            return analysisResult;
        }

        //수질해석 실행
        private Hashtable RunQualityAnalysis(Hashtable conditions)
        {
            string inpFileName = (string)conditions["FILE_NAME"];
            string rptFileName = (string)conditions["rptFileName"];
            int analysisType = (int)conditions["analysisType"];
            Hashtable resetValues = (Hashtable)conditions["resetValues"];
            Hashtable analysisResult = new Hashtable();

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENopen(new StringBuilder(TmpVariables.INP_FILE_PATH + inpFileName), new StringBuilder(TmpVariables.RPT_FILE_PATH + rptFileName), new StringBuilder(""))))
            {
                //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                //AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                return null;
            }

            //수질해석은 필히 관망해석을 수행하고 나서 실행해야한다.
            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENsolveH()))
            {
                //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                //AnalysisErrorHandling(EPANETMethodDefinition.ENcloseH());
                AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                return null;
            }

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENopenQ()))
            {
                //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                //AnalysisErrorHandling(EPANETMethodDefinition.ENcloseQ());
                AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                return null;
            }

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENinitQ(0)))
            {
                //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                AnalysisErrorHandling(EPANETMethodDefinition.ENcloseQ());
                AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                return null;
            }

            long t = 0;

            if (analysisType == 0)
            {
                //주어진 시간만큼 관망해석 실행

                long tstep = 0;
                long qualStep = 0;
                long duration = 0;

                long reportStep = 0;

                if (!AnalysisErrorHandling(EPANETMethodDefinition.ENgettimeparam(EN_QUALSTEP, ref qualStep)))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(EPANETMethodDefinition.ENcloseQ());
                    AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                    return null;
                }

                if (!AnalysisErrorHandling(EPANETMethodDefinition.ENgettimeparam(EN_DURATION, ref duration)))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(EPANETMethodDefinition.ENcloseQ());
                    AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                    return null;
                }

                if (!AnalysisErrorHandling(EPANETMethodDefinition.ENgettimeparam(EN_REPORTSTEP, ref reportStep)))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(EPANETMethodDefinition.ENcloseQ());
                    AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                    return null;
                }

                do
                {
                    if (!AnalysisErrorHandling(EPANETMethodDefinition.ENrunQ(ref t)))
                    {
                        //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                        AnalysisErrorHandling(EPANETMethodDefinition.ENcloseQ());
                        AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                        return null;
                    }

                    //간혹 step에 맞지않는 시간이 도출되는 경우가 있을때를 필터링 (이유는 불명)
                    //if ((t % qualStep) == 0)
                    if ((t % reportStep) == 0)
                    {
                        Hashtable timeResult = new Hashtable();
                        //Console.WriteLine("******************************* 시간 : " + (Convert.ToString(t / 60 / 60)).PadLeft(2, '0') + ":" + Convert.ToString((t - (t / 60 / 60 * 60 * 60)) / 60).PadLeft(2, '0'));

                        timeResult.Add("node", ExtractNodeAnalysisResult());
                        timeResult.Add("link", ExtractLinkAnalysisResult());

                        //해석시간을 key로 하여 node,junction 해석결과를 저장
                        analysisResult.Add((Convert.ToString(t / 60 / 60)).PadLeft(2, '0') + ":" + Convert.ToString((t - (t / 60 / 60 * 60 * 60)) / 60).PadLeft(2, '0'), timeResult);
                    }

                    if (!AnalysisErrorHandling(EPANETMethodDefinition.ENnextQ(ref tstep)))
                    {
                        //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                        AnalysisErrorHandling(EPANETMethodDefinition.ENcloseQ());
                        AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                        return null;
                    }
                }
                while (tstep > 0);
            }
            else if (analysisType == 1)
            {
                //관망해석 1회 실행
                if (!AnalysisErrorHandling(EPANETMethodDefinition.ENrunQ(ref t)))
                {
                    //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                    AnalysisErrorHandling(EPANETMethodDefinition.ENcloseQ());
                    AnalysisErrorHandling(EPANETMethodDefinition.ENclose());

                    return null;
                }

                Hashtable timeResult = new Hashtable();

                timeResult.Add("node", ExtractNodeAnalysisResult());
                timeResult.Add("link", ExtractLinkAnalysisResult());

                analysisResult.Add("00:00", timeResult);
            }

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENcloseQ()))
            {
                //오류발생으로 인해 해석이 중간종료가 되는 경우 파일을 닫아준다.
                AnalysisErrorHandling(EPANETMethodDefinition.ENclose());
                return null;
            }

            if (!AnalysisErrorHandling(EPANETMethodDefinition.ENclose()))
            {
                return null;
            }

            return analysisResult;
        }

        //DB에서 정보를 읽어 INP파일 조합
        private DataSet GenerateINPFile(Hashtable conditions)
        {
            DataSet inpMasterData = null;
            StreamWriter writer = null;
            EMapper manager = null;

            try
            {
                //파일경로 생성
                Configuration.CreateDirectory(TmpVariables.RPT_FILE_PATH);

                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                //Master 정보로부터 타이틀 발췌
                inpMasterData = dao.SelectINPMasterData(manager, conditions);

                //string fileName = (string)masterData.Tables["WH_TITLE"].Rows[0]["FILE_NAME"];
                string fileName = (string)conditions["FILE_NAME"];
                string title = (string)inpMasterData.Tables["WH_TITLE"].Rows[0]["TITLE"];
                string inpNumber = (string)conditions["INP_NUMBER"];

                writer = new StreamWriter(TmpVariables.INP_FILE_PATH + fileName, true, Encoding.GetEncoding(51949));
                //writer.AutoFlush = true;

                //Section별 데이터 조회
                DataSet junctionList = dao.SelectJunctionList(manager, conditions);
                DataSet reservoirList = dao.SelectReservoirList(manager, conditions);
                DataSet tankList = dao.SelectTankList(manager, conditions);
                DataSet pipeList = dao.SelectPipeList(manager, conditions);
                DataSet pumpList = dao.SelectPumpList(manager, conditions);
                DataSet valveList = dao.SelectValveList(manager, conditions);
                DataSet demandList = dao.SelectDemandList(manager, conditions);
                DataSet statusList = dao.SelectStatusList(manager, conditions);
                DataSet curveList = dao.SelectCurveList(manager, conditions);
                DataSet patternList = dao.SelectPatternList(manager, conditions);
                DataSet controlList = dao.SelectControlList(manager, conditions);
                DataSet ruleList = dao.SelectRuleList(manager, conditions);
                DataSet energyList = dao.SelectEnergyList(manager, conditions);
                DataSet emitterList = dao.SelectEmitterList(manager, conditions);
                DataSet qualityList = dao.SelectQualityList(manager, conditions);
                DataSet sourceList = dao.SelectSourceList(manager, conditions);
                DataSet reactionList = dao.SelectReactionList(manager, conditions);
                DataSet mixingList = dao.SelectMixingList(manager, conditions);
                DataSet timeList = dao.SelectTimeOptionList(manager, conditions);
                DataSet reportList = dao.SelectReportOptionList(manager, conditions);
                DataSet optionList = dao.SelectOptionList(manager, conditions);
                DataSet coordinateList = dao.SelectCoordinateList(manager, conditions);
                DataSet verticesList = dao.SelectVerticesList(manager, conditions);
                DataSet labelList = dao.SelectLabelList(manager, conditions);

                //넘겨받은 재정의 데이터가 있는지 확인
                Hashtable junctionPatternData = null;
                Hashtable demandData = null;
                Hashtable largeConsumer = null;
                Hashtable reservoirData = null;
                Hashtable reservoirPatternData = null;
                Hashtable valveData = null;
                Hashtable pipeData = null;
                Hashtable pumpData = null;
                Hashtable statusData = null;
                ArrayList curveDataList = null;
                Hashtable patternData = null;
                Hashtable qualityData = null;
                ArrayList controlData = null;
                Hashtable timeData = null;
                Hashtable optionData = null;
                ArrayList energyData = null;
                ArrayList tankData = null;
                ArrayList ruleData = null;

                if (conditions["resetValues"] != null)
                {
                    junctionPatternData = (Hashtable)((Hashtable)conditions["resetValues"])["junctionPattern"];
                    demandData = (Hashtable)((Hashtable)conditions["resetValues"])["demand"];
                    largeConsumer = (Hashtable)((Hashtable)conditions["resetValues"])["largeConsumer"];
                    reservoirData = (Hashtable)((Hashtable)conditions["resetValues"])["reservoir"];
                    reservoirPatternData = (Hashtable)((Hashtable)conditions["resetValues"])["reservoirPattern"];
                    valveData = (Hashtable)((Hashtable)conditions["resetValues"])["valve"];
                    pipeData = (Hashtable)((Hashtable)conditions["resetValues"])["pipe"];
                    pumpData = (Hashtable)((Hashtable)conditions["resetValues"])["pump"];
                    statusData = (Hashtable)((Hashtable)conditions["resetValues"])["status"];
                    curveDataList = (ArrayList)((Hashtable)conditions["resetValues"])["curve"];
                    patternData = (Hashtable)((Hashtable)conditions["resetValues"])["pattern"];
                    qualityData = (Hashtable)((Hashtable)conditions["resetValues"])["quality"];
                    controlData = (ArrayList)((Hashtable)conditions["resetValues"])["control"];
                    timeData = (Hashtable)((Hashtable)conditions["resetValues"])["time"];
                    optionData = (Hashtable)((Hashtable)conditions["resetValues"])["option"];
                    energyData = (ArrayList)((Hashtable)conditions["resetValues"])["energy"];
                    tankData = (ArrayList)((Hashtable)conditions["resetValues"])["tank"];
                    ruleData = (ArrayList)((Hashtable)conditions["resetValues"])["rule"];
                }

                string unitOption = "";

                //옵션데이터를 확인하여 단위환산
                foreach (DataRow optionRow in optionList.Tables["WH_OPTIONS"].Rows)
                {
                    string statement = (optionRow["OPTIONS_STATEMENT"].ToString()).ToUpper();

                    if (statement.Contains("UNITS"))
                    {
                        if (statement.Contains("CFS"))
                        {
                            //Cubic feet per second
                            unitOption = "1";
                        }
                        else if (statement.Contains("GPM"))
                        {
                            //Gallons per minute
                            unitOption = "2";
                        }
                        else if (statement.Contains("MGD"))
                        {
                            //Million Gallons per day
                            unitOption = "3";
                        }
                        else if (statement.Contains("IMGD"))
                        {
                            //Imperial MGD
                            unitOption = "4";
                        }
                        else if (statement.Contains("AFD"))
                        {
                            //Acre-feet per day
                            unitOption = "5";
                        }
                        else if (statement.Contains("LPS"))
                        {
                            //Liter per second\
                            unitOption = "6";
                        }
                        else if (statement.Contains("LPM"))
                        {
                            //Liter per minute
                            unitOption = "7";
                        }
                        else if (statement.Contains("MLD"))
                        {
                            //Million Liter per day
                            unitOption = "8";
                        }
                        else if (statement.Contains("CMH"))
                        {
                            //Cubic meters per hour
                            unitOption = "9";
                        }
                        else if (statement.Contains("CMD "))
                        {
                            //Cubic meters per day
                            unitOption = "10";
                        }

                        break;
                    }
                }

                //Section별로 조회된 데이터를 파일로 쓰기
                writer.WriteLine("[TITLE]");
                writer.WriteLine(title);

                writer.WriteLine("");
                writer.WriteLine("[JUNCTIONS]");

                string demand = "";

                foreach (DataRow junctionRow in junctionList.Tables["WH_JUNCTIONS"].Rows)
                {
                    demand = "";

                    if (demandData != null)
                    {
                        if (demandData[(string)junctionRow["ID"]] != null)
                        {
                            demand = (string)demandData[(string)junctionRow["ID"]];
                        }
                        else
                        {
                            if (largeConsumer != null)
                            {
                                if (largeConsumer[(string)junctionRow["ID"]] != null)
                                {
                                    demand = (string)largeConsumer[(string)junctionRow["ID"]];
                                }
                                else
                                {
                                    //블럭정보가 없는 junction은 임의로 Demand를 0으로 세팅한다.
                                    if ("WH".Equals(inpNumber.Substring(0, 2)))
                                    {
                                        demand = "0";
                                    }
                                    else
                                    {
                                        demand = (string)junctionRow["DEMAND"];
                                    }
                                }
                            }
                            else
                            {
                                //블럭정보가 없는 junction은 임의로 Demand를 0으로 세팅한다.
                                if ("WH".Equals(inpNumber.Substring(0, 2)))
                                {
                                    demand = "0";
                                }
                                else
                                {
                                    demand = (string)junctionRow["DEMAND"];
                                }
                            }
                        }
                    }
                    //else if (largeConsumer != null)
                    //{
                    //    if (largeConsumer[(string)junctionRow["ID"]] != null)
                    //    {
                    //        demand = (string)largeConsumer[(string)junctionRow["ID"]];
                    //    }
                    //    else
                    //    {
                    //        //블럭정보가 없는 junction은 임의로 Demand를 0으로 세팅한다.
                    //        if ("WH".Equals(inpNumber.Substring(0, 2)))
                    //        {
                    //            demand = "0";
                    //        }
                    //        else
                    //        {
                    //            demand = (string)junctionRow["DEMAND"];
                    //        }
                    //    }
                    //}
                    else
                    {
                        demand = (string)junctionRow["DEMAND"];
                    }

                    string patternString = "";

                    if ("WH".Equals(inpNumber.Substring(0, 2)))
                    {
                        //실시간 관망해석인 경우는 패턴을 삭제한다.
                        patternString = "\t";
                    }
                    else
                    {
                        if (junctionPatternData != null)
                        {
                            if (junctionPatternData[(string)junctionRow["ID"]] != null)
                            {
                                patternString = (string)junctionPatternData[(string)junctionRow["ID"]];
                            }
                            else
                            {
                                patternString = junctionRow["PATTERN_ID"].ToString();
                            }
                        }
                        else
                        {
                            patternString = junctionRow["PATTERN_ID"].ToString();
                        }
                    }

                    writer.WriteLine(junctionRow["ID"] + "\t" + junctionRow["ELEV"] + "\t" + demand + "\t" + patternString + ";" + junctionRow["REMARK"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[RESERVOIRS]");

                string head = "";

                foreach (DataRow reservoirRow in reservoirList.Tables["WH_RESERVOIRS"].Rows)
                {
                    head = "";

                    if (reservoirData != null)
                    {
                        if (reservoirData[(string)reservoirRow["ID"]] != null)
                        {
                            head = (double.Parse((string)reservoirRow["HEAD"]) + double.Parse((string)reservoirData[(string)reservoirRow["ID"]])).ToString();
                        }
                        else
                        {
                            head = (string)reservoirRow["HEAD"];
                        }
                    }
                    else
                    {
                        head = (string)reservoirRow["HEAD"];
                    }

                    string patternString = "";

                    if ("WH".Equals(inpNumber.Substring(0, 2)))
                    {
                        //실시간 관망해석인 경우는 패턴을 삭제한다.
                        patternString = "\t";
                    }
                    else
                    {
                        if (reservoirPatternData != null)
                        {
                            if (reservoirPatternData[(string)reservoirRow["ID"]] != null)
                            {
                                patternString = (string)reservoirPatternData[reservoirRow["ID"].ToString()];
                            }
                            else
                            {
                                patternString = (string)reservoirRow["PATTERN_ID"];
                            }
                        }
                        else
                        {
                            patternString = reservoirRow["PATTERN_ID"].ToString();
                        }
                    }

                    writer.WriteLine(reservoirRow["ID"] + "\t" + head + "\t" + patternString);
                }

                writer.WriteLine("");
                writer.WriteLine("[TANKS]");

                if (tankData != null)
                {
                    for (int i = 0; i < tankData.Count; i++ )
                    {
                        Hashtable tmpTable = (Hashtable)tankData[i];

                        writer.WriteLine(tmpTable["ID"] + "\t" + tmpTable["ELEV"] + "\t" + tmpTable["INITLVL"] + "\t" + tmpTable["MINLVL"] + "\t" + tmpTable["MAXLVL"] + "\t" + tmpTable["DIAM"] + "\t" + tmpTable["MINVOL"] + "\t" + tmpTable["VOLCURVE_ID"]);
                    }
                }
                else
                {
                    foreach (DataRow tankRow in tankList.Tables["WH_TANK"].Rows)
                    {
                        writer.WriteLine(tankRow["ID"] + "\t" + tankRow["ELEV"] + "\t" + tankRow["INITLVL"] + "\t" + tankRow["MINLVL"] + "\t" + tankRow["MAXLVL"] + "\t" + tankRow["DIAM"] + "\t" + tankRow["MINVOL"] + "\t" + tankRow["VOLCURVE_ID"]);
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("[PIPES]");

                string pipeState = "";

                foreach (DataRow pipeRow in pipeList.Tables["WH_PIPES"].Rows)
                {
                    pipeState = "";

                    if (pipeData != null)
                    {
                        if (pipeData[(string)pipeRow["ID"]] != null)
                        {
                            pipeState = (string)pipeData[(string)pipeRow["ID"]];
                        }
                        else
                        {
                            pipeState = (string)pipeRow["STATUS"];
                        }
                    }
                    else
                    {
                        pipeState = (string)pipeRow["STATUS"];
                    }

                    writer.WriteLine(pipeRow["ID"] + "\t" + pipeRow["NODE1"] + "\t" + pipeRow["NODE2"] + "\t" + pipeRow["LENGTH"] + "\t" + pipeRow["DIAM"] + "\t" + pipeRow["LEAKAGE_COEFFICIENT"] + "\t" + pipeRow["ROUGHNESS"] + "\t" + pipeRow["MLOSS"] + "\t" + pipeState + ";" + pipeRow["REMARK"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[PUMPS]");

                string pumpProperties = "";

                foreach (DataRow pumpRow in pumpList.Tables["WH_PUMPS"].Rows)
                {
                    pumpProperties = "";

                    if (pumpData != null)
                    {
                        if (pumpData[(string)pumpRow["ID"]] != null)
                        {
                            pumpProperties = (string)pumpData[(string)pumpRow["ID"]];
                        }
                        else
                        {
                            pumpProperties = (string)pumpRow["PROPERTIES"];
                        }
                    }
                    else
                    {
                        pumpProperties = (string)pumpRow["PROPERTIES"];
                    }

                    writer.WriteLine(pumpRow["ID"] + "\t" + pumpRow["NODE1"] + "\t" + pumpRow["NODE2"] + "\t" + pumpProperties);
                }

                writer.WriteLine("");
                writer.WriteLine("[VALVES]");

                string pressure = "";

                foreach (DataRow valveRow in valveList.Tables["WH_VALVES"].Rows)
                {
                    pressure = "";

                    if (valveData != null)
                    {
                        if (valveData[(string)valveRow["ID"]] != null)
                        {
                            //단위가 kgf/cm2이며 epanet은 m로 표시되기에 10을 곱해준다.
                            pressure = (double.Parse(((string)valveData[(string)valveRow["ID"]])) * 10).ToString();
                        }
                        else
                        {
                            pressure = (string)valveRow["SETTING"];
                        }
                    }
                    else
                    {
                        pressure = (string)valveRow["SETTING"];
                    }

                    writer.WriteLine(valveRow["ID"] + "\t" + valveRow["NODE1"] + "\t" + valveRow["NODE2"] + "\t" + valveRow["DIAMETER"] + "\t" + valveRow["TYPE"] + "\t" + pressure + "\t" + valveRow["MINORLOSS"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[DEMANDS]");

                foreach (DataRow demandRow in demandList.Tables["WH_DEMANDS"].Rows)
                {
                    writer.WriteLine(demandRow["ID"] + "\t" + demandRow["DEMAND"] + "\t" + demandRow["PATTERN_ID"] + "\t" + demandRow["CATEGORY"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[STATUS]");

                string statusSetting = "";

                //모델의 Status에 실시간 Status가 없을 수도 있고 실시간 Status에 모델의 Status가 없을 수도 있다.
                Hashtable tmpStatusTable = new Hashtable();
                //일단 실시간 Status값을 밀어넣는다. (실시간 값이 최우선)
                if (statusData != null)
                {
                    foreach (string key in statusData.Keys)
                    {
                        tmpStatusTable.Add(key, statusData[key].ToString());
                    }
                }

                //원래 Status값을 밀어넣되 이미 들어간 status가 있다면(실시간으로 받았다면) 패스한다.
                foreach (DataRow statusRow in statusList.Tables["WH_STATUS"].Rows)
                {
                    if (tmpStatusTable[statusRow["ID"].ToString()] == null)
                    {
                        //실시간 값이 없다면 추가.
                        tmpStatusTable.Add(statusRow["ID"].ToString(), statusRow["STATUS_SETTING"].ToString());
                    }
                }

                foreach (string key in tmpStatusTable.Keys)
                {
                    if (!"DELETE".Equals(tmpStatusTable[key].ToString()))
                    {
                        //개도값이 0%나 100%가 아닌 값이라 status에서 지워야하는 경우가 아니면 출력
                        writer.WriteLine(key + "\t" + tmpStatusTable[key].ToString());
                    }
                }

                //foreach (DataRow statusRow in statusList.Tables["WH_STATUS"].Rows)
                //{
                //    statusSetting = "";

                //    if (statusData != null)
                //    {
                //        if (statusData[(string)statusRow["ID"]] != null)
                //        {
                //            statusSetting = (string)statusData[(string)statusRow["ID"]];
                //        }
                //        else
                //        {
                //            statusSetting = (string)statusRow["STATUS_SETTING"];
                //        }
                //    }
                //    else
                //    {
                //        statusSetting = (string)statusRow["STATUS_SETTING"];
                //    }

                //    writer.WriteLine(statusRow["ID"] + "\t" + statusSetting);
                //}

                writer.WriteLine("");
                writer.WriteLine("[CURVES]");

                if (curveDataList != null)
                {
                    foreach (Hashtable curveData in curveDataList)
                    {
                        writer.WriteLine(curveData["ID"] + "\t" + curveData["X"] + "\t" + curveData["Y"]);
                    }
                }
                else
                {
                    foreach (DataRow curveRow in curveList.Tables["WH_CURVES"].Rows)
                    {
                        writer.WriteLine(curveRow["ID"] + "\t" + curveRow["X"] + "\t" + curveRow["Y"]);
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("[PATTERNS]");

                if (patternData != null)
                {
                    foreach (string key in patternData.Keys)
                    {
                        ArrayList multiplierList = (ArrayList)patternData[key];

                        for (int i = 0; i < multiplierList.Count; i++)
                        {
                            writer.WriteLine(key + "\t" + multiplierList[i]);
                        }
                    }
                }
                else
                {
                    foreach (DataRow patternRow in patternList.Tables["WH_PATTERNS"].Rows)
                    {
                        writer.WriteLine(patternRow["PATTERN_ID"] + "\t" + patternRow["MULTIPLIER"]);
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("[CONTROLS]");
                if (controlData != null)
                {
                    for (int i = 0; i < controlData.Count; i++)
                    {
                        writer.WriteLine(controlData[i].ToString());
                    }
                }
                else
                {
                    foreach (DataRow controlRow in controlList.Tables["WH_CONTROLS"].Rows)
                    {
                        writer.WriteLine(controlRow["CONTROLS_STATEMENT"]);
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("[RULES]");
                
                if (ruleData != null)
                {
                    for (int i = 0; i < ruleData.Count; i++)
                    {
                        writer.WriteLine(ruleData[i].ToString());
                    }
                }
                else
                {
                    foreach (DataRow ruleRow in ruleList.Tables["WH_RULES"].Rows)
                    {
                        writer.WriteLine(ruleRow["RULES_STATEMENT"]);
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("[ENERGY]");

                if (energyData != null)
                {
                    for (int i = 0; i < energyData.Count; i ++ )
                    {
                        writer.WriteLine(energyData[i].ToString());
                    }
                }
                else
                {
                    foreach (DataRow energyRow in energyList.Tables["WH_ENERGY"].Rows)
                    {
                        writer.WriteLine(energyRow["ENERGY_STATEMENT"]);
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("[EMITTERS]");
                foreach (DataRow emitterRow in emitterList.Tables["WH_EMITTERS"].Rows)
                {
                    writer.WriteLine(emitterRow["ID"] + "\t" + emitterRow["FLOW_COFFICIENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[QUALITY]");

                string initQual = "";

                foreach (DataRow qualityRow in qualityList.Tables["WH_QUALITY"].Rows)
                {
                    initQual = "";

                    if (qualityData != null)
                    {
                        if (qualityData[(string)qualityRow["ID"]] != null)
                        {
                            initQual = (string)qualityData[(string)qualityRow["ID"]];
                        }
                        else
                        {
                            initQual = (string)qualityRow["INITQUAL"];
                        }
                    }
                    else
                    {
                        initQual = (string)qualityRow["INITQUAL"];
                    }

                    writer.WriteLine(qualityRow["ID"] + "\t" + initQual);
                }

                writer.WriteLine("");
                writer.WriteLine("[SOURCES]");
                foreach (DataRow sourceRow in sourceList.Tables["WH_SOURCE"].Rows)
                {
                    writer.WriteLine(sourceRow["ID"] + "\t" + sourceRow["TYPE"] + "\t" + sourceRow["STRENGTH"] + "\t" + sourceRow["PATTERN_ID"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[REACTIONS]");
                foreach (DataRow reactionRow in reactionList.Tables["WH_REACTIONS"].Rows)
                {
                    writer.WriteLine(reactionRow["REACTION_STATEMENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[MIXING]");
                foreach (DataRow mixingRow in mixingList.Tables["WH_MIXING"].Rows)
                {
                    writer.WriteLine(mixingRow["ID"] + "\t" + mixingRow["MODEL"] + "\t" + mixingRow["FLACTION"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[TIMES]");
                if (timeData != null)
                {
                    foreach (string key in timeData.Keys)
                    {
                        writer.WriteLine(key + "\t" + timeData[key]);
                    }
                }

                foreach (DataRow timeRow in timeList.Tables["WH_TIMES"].Rows)
                {
                    string timeSetting = timeRow["TIMES_STATEMENT"].ToString();
                    bool isExist = false;

                    if (timeData != null)
                    {
                        foreach (string key in timeData.Keys)
                        {
                            if ((timeSetting.ToUpper()).Contains(key.ToUpper()))
                            {
                                isExist = true;
                                break;
                            }
                        }
                    }

                    if (!isExist)
                    {
                        writer.WriteLine(timeSetting);
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("[REPORT]");
                foreach (DataRow reportRow in reportList.Tables["WH_RPT_OPTIONS"].Rows)
                {
                    writer.WriteLine(reportRow["REPORT_STATEMENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[OPTIONS]");
                if (optionData != null)
                {
                    foreach (string key in optionData.Keys)
                    {
                        writer.WriteLine(key + "\t" + optionData[key]);
                    }
                }

                foreach (DataRow optionRow in optionList.Tables["WH_OPTIONS"].Rows)
                {
                    string optionSetting = optionRow["OPTIONS_STATEMENT"].ToString();
                    bool isExist = false;

                    if (optionData != null)
                    {
                        foreach (string key in optionData.Keys)
                        {
                            if ((optionSetting.ToUpper()).Contains(key.ToUpper()))
                            {
                                isExist = true;
                                break;
                            }
                        }
                    }

                    if (!isExist)
                    {
                        writer.WriteLine(optionSetting);
                    }
                }
                writer.WriteLine("");
                writer.WriteLine("[COORDINATES]");
                foreach (DataRow coordinateRow in coordinateList.Tables["WH_COORDINATES"].Rows)
                {
                    writer.WriteLine(coordinateRow["ID"] + "\t" + coordinateRow["X"] + "\t" + coordinateRow["Y"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[VERTICES]");
                foreach (DataRow verticesRow in verticesList.Tables["WH_VERTICES"].Rows)
                {
                    writer.WriteLine(verticesRow["ID"] + "\t" + verticesRow["X"] + "\t" + verticesRow["Y"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[LABELS]");
                foreach (DataRow labelRow in labelList.Tables["WH_LABELS"].Rows)
                {
                    writer.WriteLine(labelRow["X"] + "\t" + labelRow["Y"] + "\t" + labelRow["REMARK"]);
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
                writer.Flush();
                writer.Close();
            }

            return inpMasterData;
        }

        //INP파일을 넘겨받은 정보를 이용하여 재편성
        private void ResettingValues(string inpFileName, Hashtable resetValues)
        {
            string line;

            bool replaceActivation = false;
            string section = "";
            string replacedString = "";

            StreamReader reader = null;
            StreamWriter writer = null;

            Hashtable demand = (Hashtable)resetValues["demand"];            //사용량
            Hashtable reservoir = (Hashtable)resetValues["reservoir"];      //배수지수위
            Hashtable valve = (Hashtable)resetValues["valve"];              //PRV 2차측 압력

            try
            {
                reader = new System.IO.StreamReader(TmpVariables.INP_FILE_PATH + inpFileName);             //원본 파일
                writer = new System.IO.StreamWriter(TmpVariables.REAL_INP_FILE_PATH + inpFileName);        //재편성한 파일

                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains("["))
                    {
                        if ((line.ToUpper()).Contains("[JUNCTION"))
                        {
                            //Junction 시작
                            replaceActivation = true;
                            section = "junction";

                            writer.WriteLine(line);
                        }
                        else if ((line.ToUpper()).Contains("[RESERVOIR"))
                        {
                            //Reservoir 시작
                            replaceActivation = true;
                            section = "reservoir";

                            writer.WriteLine(line);
                        }
                        else if ((line.ToUpper()).Contains("[VALVE"))
                        {
                            //valve 시작
                            replaceActivation = true;
                            section = "valve";

                            writer.WriteLine(line);
                        }
                        else
                        {
                            //변경대상외 Section 시작
                            replaceActivation = false;
                            section = "";
                            writer.WriteLine(line);
                        }
                    }
                    else
                    {
                        if (replaceActivation)
                        {
                            if ("junction".Equals(section))
                            {
                                if (line.StartsWith(";"))
                                {
                                    //주석문인경우 그대로 써준다.
                                    writer.WriteLine(line);
                                }
                                else
                                {
                                    //처리시작
                                    line.Trim();
                                    string[] dataArray = line.Split('\t');

                                    if (dataArray.Length > 2)
                                    {
                                        //Demand 할당
                                        if (demand[(string)dataArray[0]] != null)
                                        {
                                            dataArray[2] = (string)demand[(string)dataArray[0]];
                                        }
                                    }

                                    replacedString = "";

                                    for (int i = 0; i < dataArray.Length; i++)
                                    {
                                        if (replacedString.Length != 0)
                                        {
                                            replacedString = replacedString + "\t" + dataArray[i];
                                        }
                                        else
                                        {
                                            replacedString = dataArray[i];
                                        }
                                    }

                                    writer.WriteLine(replacedString);
                                }
                            }
                            else if ("reservoir".Equals(section))
                            {
                                if (line.StartsWith(";"))
                                {
                                    //주석문인경우 그대로 써준다.
                                    writer.WriteLine(line);
                                }
                                else
                                {
                                    //처리시작
                                    line.Trim();
                                    string[] dataArray = line.Split('\t');

                                    if (dataArray.Length > 2)
                                    {
                                        //Head 할당
                                        if (reservoir[(string)dataArray[0]] != null)
                                        {
                                            dataArray[1] = (string)reservoir[(string)dataArray[0]];
                                        }
                                    }

                                    replacedString = "";

                                    for (int i = 0; i < dataArray.Length; i++)
                                    {
                                        if (replacedString.Length != 0)
                                        {
                                            replacedString = replacedString + "\t" + dataArray[i];
                                        }
                                        else
                                        {
                                            replacedString = dataArray[i];
                                        }
                                    }

                                    writer.WriteLine(replacedString);
                                }
                            }
                            else if ("valve".Equals(section))
                            {
                                if (line.StartsWith(";"))
                                {
                                    //주석문인경우 그대로 써준다.
                                    writer.WriteLine(line);
                                }
                                else
                                {
                                    //처리시작
                                    line.Trim();
                                    string[] dataArray = line.Split('\t');

                                    if (dataArray.Length > 2)
                                    {
                                        //압력 할당
                                        if ("PRV".Equals(((string)dataArray[3]).ToUpper()))
                                        {
                                            if (valve[(string)dataArray[0]] != null)
                                            {
                                                dataArray[5] = (string)valve[(string)dataArray[0]];
                                            }
                                        }
                                    }

                                    replacedString = "";

                                    for (int i = 0; i < dataArray.Length; i++)
                                    {
                                        if (replacedString.Length != 0)
                                        {
                                            replacedString = replacedString + "\t" + dataArray[i];
                                        }
                                        else
                                        {
                                            replacedString = dataArray[i];
                                        }
                                    }

                                    writer.WriteLine(replacedString);
                                }
                            }
                        }
                        else
                        {
                            //조작할 행이 아니면 그냥 쓴다.
                            writer.WriteLine(line);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                reader.Close();
                writer.Close();
            }
        }

        //Node해석결과 발췌
        private Hashtable ExtractNodeAnalysisResult()
        {
            //Node 해석결과 발췌

            //전체 node 해석결과를 저장할 Hashtable
            Hashtable nodeResult = new Hashtable();

            ArrayList junctionResult = new ArrayList();            //절점 해석결과
            ArrayList reserviorResult = new ArrayList();           //배수지 해석결과
            ArrayList tankResult = new ArrayList();                //tank 해석결과

            //Node Count 발췌
            int nodeCount = 0;
            EPANETMethodDefinition.ENgetcount(EN_NODECOUNT, ref nodeCount);

            //출수불량 발생
            bool isOccurLowPressure = false;

            for (int i = 1; i <= nodeCount; i++)
            {
                StringBuilder nodeId = new StringBuilder();

                //Node 해석결과 변수
                float elevationValue = 0;
                float basedemandValue = 0;
                float patternValue = 0;
                float emitterValue = 0;
                float initqualValue = 0;
                float sourcequalValue = 0;
                float sourcepatValue = 0;
                float sourcetypeValue = 0;
                float tanklevelValue = 0;
                float demandValue = 0;
                float headValue = 0;
                float pressureValue = 0;
                float qualityValue = 0;
                float sourcemassValue = 0;
                float initvolumeValue = 0;
                float mixmodelValue = 0;
                float mixzonevolValue = 0;
                float tankdiamValue = 0;
                float minvolumeValue = 0;
                float volcurveValue = 0;
                float minlevelValue = 0;
                float maxlevelValue = 0;
                float mixfractionValue = 0;
                float tankKbulkValue = 0;

                int nodeType = 0;

                EPANETMethodDefinition.ENgetnodeid(i, nodeId);
                EPANETMethodDefinition.ENgetnodetype(i, ref nodeType);

                EPANETMethodDefinition.ENgetnodevalue(i, EN_ELEVATION, ref elevationValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_BASEDEMAND, ref	basedemandValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_PATTERN, ref patternValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_EMITTER, ref emitterValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_INITQUAL, ref initqualValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_SOURCEQUAL, ref	sourcequalValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_SOURCEPAT, ref sourcepatValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_SOURCETYPE, ref	sourcetypeValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_TANKLEVEL, ref tanklevelValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_DEMAND, ref	demandValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_HEAD, ref headValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_PRESSURE, ref pressureValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_QUALITY, ref qualityValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_SOURCEMASS, ref	sourcemassValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_INITVOLUME, ref	initvolumeValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_MIXMODEL, ref mixmodelValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_MIXZONEVOL, ref	mixzonevolValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_TANKDIAM, ref tankdiamValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_MINVOLUME, ref minvolumeValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_VOLCURVE, ref volcurveValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_MINLEVEL, ref minlevelValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_MAXLEVEL, ref maxlevelValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_MIXFRACTION, ref mixfractionValue);
                EPANETMethodDefinition.ENgetnodevalue(i, EN_TANK_KBULK, ref	tankKbulkValue);

                //출수불량 발생 판단
                if(pressureValue < lowPressure)
                {
                    isOccurLowPressure = true;
                }

                Hashtable tmpResult = new Hashtable();

                tmpResult.Add("NODE_ID", nodeId.ToString());
                tmpResult.Add("EN_ELEVATION", elevationValue);
                tmpResult.Add("EN_BASEDEMAND", basedemandValue);
                tmpResult.Add("EN_PATTERN", patternValue);
                tmpResult.Add("EN_EMITTER", emitterValue);
                tmpResult.Add("EN_INITQUAL", initqualValue);
                tmpResult.Add("EN_SOURCEQUAL", sourcequalValue);
                tmpResult.Add("EN_SOURCEPAT", sourcepatValue);
                tmpResult.Add("EN_SOURCETYPE", sourcetypeValue);
                tmpResult.Add("EN_TANKLEVEL", tanklevelValue);
                tmpResult.Add("EN_DEMAND", demandValue);
                tmpResult.Add("EN_HEAD", headValue);
                tmpResult.Add("EN_PRESSURE", pressureValue);
                tmpResult.Add("EN_QUALITY", qualityValue);
                tmpResult.Add("EN_SOURCEMASS", sourcemassValue);
                tmpResult.Add("EN_INITVOLUME", initvolumeValue);
                tmpResult.Add("EN_MIXMODEL", mixmodelValue);
                tmpResult.Add("EN_MIXZONEVOL", mixzonevolValue);
                tmpResult.Add("EN_TANKDIAM", tankdiamValue);
                tmpResult.Add("EN_MINVOLUME", minvolumeValue);
                tmpResult.Add("EN_VOLCURVE", volcurveValue);
                tmpResult.Add("EN_MINLEVEL", minlevelValue);
                tmpResult.Add("EN_MAXLEVEL", maxlevelValue);
                tmpResult.Add("EN_MIXFRACTION", mixfractionValue);
                tmpResult.Add("EN_TANK_KBULK", tankKbulkValue);

                //Console.WriteLine("node : " + nodeId + " : " + tankKbulkValue);

                //Type에 따라 구분해서 저장한다.
                if (nodeType == EN_JUNCTION)
                {
                    //절점해석결과
                    junctionResult.Add(tmpResult);
                }
                else if (nodeType == EN_RESERVOIR)
                {
                    //배수지해석결과
                    reserviorResult.Add(tmpResult);
                }
                else if (nodeType == EN_TANK)
                {
                    //탱크해석결과
                    tankResult.Add(tmpResult);
                }
            }

            //에러발생/저장여부에 따라 저장
            if(isOccurLowPressure && isSaveError)
            {
                Hashtable warningConditions = new Hashtable();

                warningConditions.Add("WORK_GBN","0001");
                warningConditions.Add("LOC_CODE",locCode);
                warningConditions.Add("WAR_CODE","000002");
                warningConditions.Add("SERIAL_NO", inpNumber);
                warningConditions.Add("REMARK", "");

                wWork.InsertWarningData(warningConditions);
            }

            nodeResult.Add("junction", junctionResult);
            nodeResult.Add("reservior", reserviorResult);
            nodeResult.Add("tank", tankResult);

            return nodeResult;
        }

        //Link해석결과 발췌
        private Hashtable ExtractLinkAnalysisResult()
        {
            //Node 해석결과 발췌

            //전체 Link 해석결과를 저장할 Hashtable
            Hashtable linkResult = new Hashtable();

            ArrayList cvpipeResult = new ArrayList();              //Pipe with Check Valve
            ArrayList pipeResult = new ArrayList();                //pipe
            ArrayList pumpResult = new ArrayList();                //pump
            ArrayList prvResult = new ArrayList();                 //Pressure Reducing Valve
            ArrayList psvResult = new ArrayList();                 //Pressure Sustaining Valve
            ArrayList pbvResult = new ArrayList();                 //Pressure Breaker Valve
            ArrayList fcvResult = new ArrayList();                 //Flow Control Valve
            ArrayList tcvResult = new ArrayList();                 //Throttle Control Valve
            ArrayList gpvResult = new ArrayList();                 //General Purpose Valve

            //Link Count 발췌
            int linkCount = 0;
            EPANETMethodDefinition.ENgetcount(EN_LINKCOUNT, ref linkCount);

            for (int i = 1; i <= linkCount; i++)
            {
                StringBuilder linkId = new StringBuilder();

                //Link 해석결과 변수
                float diameterValue = 0;
                float lengthValue = 0;
                float roughnessValue = 0;
                float minorlossValue = 0;
                float initstatusValue = 0;
                float initsettingValue = 0;
                float kbulkValue = 0;
                float kwallValue = 0;
                float flowValue = 0;
                float velocityValue = 0;
                float headlossValue = 0;
                float statusValue = 0;
                float settingValue = 0;
                float energyValue = 0;

                int linkType = 0;

                EPANETMethodDefinition.ENgetlinkid(i, linkId);
                EPANETMethodDefinition.ENgetlinktype(i, ref linkType);

                EPANETMethodDefinition.ENgetlinkvalue(i, EN_DIAMETER, ref diameterValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_LENGTH, ref lengthValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_ROUGHNESS, ref roughnessValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_MINORLOSS, ref minorlossValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_INITSTATUS, ref initstatusValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_INITSETTING, ref initsettingValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_KBULK, ref kbulkValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_KWALL, ref kwallValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_FLOW, ref flowValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_VELOCITY, ref velocityValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_HEADLOSS, ref headlossValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_STATUS, ref	statusValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_SETTING, ref settingValue);
                EPANETMethodDefinition.ENgetlinkvalue(i, EN_ENERGY, ref energyValue);


                Hashtable tmpResult = new Hashtable();

                tmpResult.Add("LINK_ID", linkId.ToString());
                tmpResult.Add("EN_DIAMETER", diameterValue);
                tmpResult.Add("EN_LENGTH", lengthValue);
                tmpResult.Add("EN_ROUGHNESS", roughnessValue);
                tmpResult.Add("EN_MINORLOSS", minorlossValue);
                tmpResult.Add("EN_INITSTATUS", initstatusValue);
                tmpResult.Add("EN_INITSETTING", initsettingValue);
                tmpResult.Add("EN_KBULK", kbulkValue);
                tmpResult.Add("EN_KWALL", kwallValue);
                //tmpResult.Add("EN_FLOW", Math.Abs(flowValue));
                tmpResult.Add("EN_FLOW", flowValue);
                tmpResult.Add("EN_VELOCITY", velocityValue);
                tmpResult.Add("EN_HEADLOSS", headlossValue);
                tmpResult.Add("EN_STATUS", statusValue);
                tmpResult.Add("EN_SETTING", settingValue);
                tmpResult.Add("EN_ENERGY", energyValue);

                //Console.WriteLine(linkId + "\t\t" + flowValue + "\t\t" + velocityValue + "\t\t" + headlossValue + "\t\t" + statusValue + "\t\t" + energyValue);

                //Type에 따라 구분해서 저장한다.
                if (linkType == EN_CVPIPE)
                {
                    cvpipeResult.Add(tmpResult);
                }
                else if (linkType == EN_PIPE)
                {
                    pipeResult.Add(tmpResult);
                }
                else if (linkType == EN_PUMP)
                {
                    pumpResult.Add(tmpResult);
                }
                else if (linkType == EN_PRV)
                {
                    prvResult.Add(tmpResult);
                }
                else if (linkType == EN_PSV)
                {
                    psvResult.Add(tmpResult);
                }
                else if (linkType == EN_PBV)
                {
                    pbvResult.Add(tmpResult);
                }
                else if (linkType == EN_FCV)
                {
                    fcvResult.Add(tmpResult);
                }
                else if (linkType == EN_TCV)
                {
                    tcvResult.Add(tmpResult);
                }
                else if (linkType == EN_GPV)
                {
                    gpvResult.Add(tmpResult);
                }
            }

            linkResult.Add("cvPipe", cvpipeResult);
            linkResult.Add("pipe", pipeResult);
            linkResult.Add("pump", pumpResult);
            linkResult.Add("prv", prvResult);
            linkResult.Add("psv", psvResult);
            linkResult.Add("pbv", pbvResult);
            linkResult.Add("fcv", fcvResult);
            linkResult.Add("tcv", tcvResult);
            linkResult.Add("gpv", gpvResult);

            return linkResult;
        }

        //해석결과 DB 저장
        private void InsertAnalysisResultData(Hashtable masterData, Hashtable analysisResultData)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();
                
                //마스터 데이터 입력
                aDao.InsertReportMasterData(manager, masterData);

                //Bulk insert를 위한 Collection 선언
                List<string> nodeRptNumberList = new List<string>();
                List<string> nodeInpNumberList = new List<string>();
                List<string> nodeNodeIdList = new List<string>();
                List<string> nodeAnalysisTimeList = new List<string>();
                List<string> nodeElevationList = new List<string>();
                List<string> nodeBasedemandList = new List<string>();
                List<string> nodePatternList = new List<string>();
                List<string> nodeEmitterList = new List<string>();
                List<string> nodeInitqualList = new List<string>();
                List<string> nodeSourcequalList = new List<string>();
                List<string> nodeSourcepatList = new List<string>();
                List<string> nodeSourcetypeList = new List<string>();
                List<string> nodeTanklevelList = new List<string>();
                List<string> nodeDemandList = new List<string>();
                List<string> nodeHeadList = new List<string>();
                List<string> nodePressureList = new List<string>();
                List<string> nodeQualityList = new List<string>();
                List<string> nodeSourcemassList = new List<string>();
                List<string> nodeInitvolumeList = new List<string>();
                List<string> nodeMixmodelList = new List<string>();
                List<string> nodeMixzonevolList = new List<string>();
                List<string> nodeTankdiamList = new List<string>();
                List<string> nodeMinvolumeList = new List<string>();
                List<string> nodeVolcurveList = new List<string>();
                List<string> nodeMinlevelList = new List<string>();
                List<string> nodeMaxlevelList = new List<string>();
                List<string> nodeMixfractionList = new List<string>();
                List<string> nodeTankKbulkList = new List<string>();
                List<string> nodeAnalysisTypeList = new List<string>();

                List<string> linkRptNumber = new List<string>();
                List<string> linkInpNumber = new List<string>();
                List<string> linkLinkId = new List<string>();
                List<string> linkAnalysisTime = new List<string>();
                List<string> linkAnalysisType = new List<string>();
                List<string> linkDiameter = new List<string>();
                List<string> linkLength = new List<string>();
                List<string> linkRoughness = new List<string>();
                List<string> linkMinorloss = new List<string>();
                List<string> linkInitstatus = new List<string>();
                List<string> linkInitsetting = new List<string>();
                List<string> linkKbulk = new List<string>();
                List<string> linkKwall = new List<string>();
                List<string> linkFlow = new List<string>();
                List<string> linkVelocity = new List<string>();
                List<string> linkHeadloss = new List<string>();
                List<string> linkStatus = new List<string>();
                List<string> linkSetting = new List<string>();
                List<string> linkEnergy = new List<string>();

                //Node, Link별 해석결과 저장
                foreach (string time in analysisResultData.Keys)
                {
                    //time은 시간 - 실시간 관망해석은 00:00밖에 없다.
                    Hashtable resultByTime = (Hashtable)analysisResultData[time];

                    foreach (string type in resultByTime.Keys)
                    {
                        //type은 node,link
                        Hashtable resultByType = (Hashtable)resultByTime[type];

                        foreach (string subtype in resultByType.Keys)
                        {
                            //subtype은 junction,reservior,tank,cvPipe,pipe,pump,prv,psv,pbv,fcv,tcv,gpv
                            ArrayList resultList = (ArrayList)resultByType[subtype];

                            for (int i = 0; i < resultList.Count; i++)
                            {
                                //ID별 해석결과 리스트
                                Hashtable resultData = (Hashtable)resultList[i];

                                if ("node".Equals(type))
                                {
                                    //노드해석결과인 경우
                                    Hashtable conditions = new Hashtable();

                                    nodeRptNumberList.Add((string)masterData["RPT_NUMBER"]);
                                    nodeInpNumberList.Add((string)masterData["INP_NUMBER"]);
                                    nodeNodeIdList.Add((string)resultData["NODE_ID"]);
                                    nodeAnalysisTimeList.Add(time);
                                    nodeElevationList.Add(Math.Round((float)resultData["EN_ELEVATION"], 3).ToString());
                                    nodeBasedemandList.Add(Math.Round((float)resultData["EN_BASEDEMAND"], 3).ToString());
                                    nodePatternList.Add(Math.Round((float)resultData["EN_PATTERN"], 3).ToString());
                                    nodeEmitterList.Add(Math.Round((float)resultData["EN_EMITTER"], 3).ToString());
                                    nodeInitqualList.Add(Math.Round((float)resultData["EN_INITQUAL"], 3).ToString());
                                    nodeSourcequalList.Add(Math.Round((float)resultData["EN_SOURCEQUAL"], 3).ToString());
                                    nodeSourcepatList.Add(Math.Round((float)resultData["EN_SOURCEPAT"], 3).ToString());
                                    nodeSourcetypeList.Add(Math.Round((float)resultData["EN_SOURCETYPE"], 3).ToString());
                                    nodeTanklevelList.Add(Math.Round((float)resultData["EN_TANKLEVEL"], 3).ToString());
                                    nodeDemandList.Add(Math.Round((float)resultData["EN_DEMAND"], 3).ToString());
                                    nodeHeadList.Add(Math.Round((float)resultData["EN_HEAD"], 3).ToString());
                                    nodePressureList.Add(Math.Round((float)resultData["EN_PRESSURE"], 3).ToString());
                                    nodeQualityList.Add(Math.Round((float)resultData["EN_QUALITY"], 3).ToString());
                                    nodeSourcemassList.Add(Math.Round((float)resultData["EN_SOURCEMASS"], 3).ToString());
                                    nodeInitvolumeList.Add(Math.Round((float)resultData["EN_INITVOLUME"], 3).ToString());
                                    nodeMixmodelList.Add(Math.Round((float)resultData["EN_MIXMODEL"], 3).ToString());
                                    nodeMixzonevolList.Add(Math.Round((float)resultData["EN_MIXZONEVOL"], 3).ToString());
                                    nodeTankdiamList.Add(Math.Round((float)resultData["EN_TANKDIAM"], 3).ToString());
                                    nodeMinvolumeList.Add(Math.Round((float)resultData["EN_MINVOLUME"], 3).ToString());
                                    nodeVolcurveList.Add(Math.Round((float)resultData["EN_VOLCURVE"], 3).ToString());
                                    nodeMinlevelList.Add(Math.Round((float)resultData["EN_MINLEVEL"], 3).ToString());
                                    nodeMaxlevelList.Add(Math.Round((float)resultData["EN_MAXLEVEL"], 3).ToString());
                                    nodeMixfractionList.Add(Math.Round((float)resultData["EN_MIXFRACTION"], 3).ToString());
                                    nodeTankKbulkList.Add(Math.Round((float)resultData["EN_TANK_KBULK"], 3).ToString());
                                    nodeAnalysisTypeList.Add(subtype);
                                }
                                else if ("link".Equals(type))
                                {
                                    //링크해석결과인 경우
                                    Hashtable conditions = new Hashtable();

                                    linkRptNumber.Add((string)masterData["RPT_NUMBER"]);
                                    linkInpNumber.Add((string)masterData["INP_NUMBER"]);
                                    linkLinkId.Add(resultData["LINK_ID"].ToString());
                                    linkAnalysisTime.Add(time);
                                    linkAnalysisType.Add(subtype);
                                    linkDiameter.Add((Math.Round((float)resultData["EN_DIAMETER"], 3)).ToString());
                                    linkLength.Add((Math.Round((float)resultData["EN_LENGTH"], 3)).ToString());
                                    linkRoughness.Add((Math.Round((float)resultData["EN_ROUGHNESS"], 3)).ToString());
                                    linkMinorloss.Add((Math.Round((float)resultData["EN_MINORLOSS"], 3)).ToString());
                                    linkInitstatus.Add((Math.Round((float)resultData["EN_INITSTATUS"], 3)).ToString());
                                    linkInitsetting.Add((Math.Round((float)resultData["EN_INITSETTING"], 3)).ToString());
                                    linkKbulk.Add((Math.Round((float)resultData["EN_KBULK"], 3)).ToString());
                                    linkKwall.Add((Math.Round((float)resultData["EN_KWALL"], 3)).ToString());
                                    linkFlow.Add((Math.Round((float)resultData["EN_FLOW"], 3)).ToString());
                                    linkVelocity.Add((Math.Round((float)resultData["EN_VELOCITY"], 3)).ToString());
                                    linkHeadloss.Add((Math.Round((float)resultData["EN_HEADLOSS"], 3)).ToString());
                                    linkStatus.Add((Math.Round((float)resultData["EN_STATUS"], 3)).ToString());
                                    linkSetting.Add((Math.Round((float)resultData["EN_SETTING"], 3)).ToString());
                                    linkEnergy.Add((Math.Round((float)resultData["EN_ENERGY"], 3)).ToString());
                                }
                            }
                        }
                    }
                }

                Hashtable nodeConditions = new Hashtable();
                Hashtable linkConditions = new Hashtable();

                nodeConditions.Add("nodeRptNumberList", nodeRptNumberList);
                nodeConditions.Add("nodeInpNumberList", nodeInpNumberList);
                nodeConditions.Add("nodeNodeIdList", nodeNodeIdList);
                nodeConditions.Add("nodeAnalysisTimeList", nodeAnalysisTimeList);
                nodeConditions.Add("nodeElevationList", nodeElevationList);
                nodeConditions.Add("nodeBasedemandList", nodeBasedemandList);
                nodeConditions.Add("nodePatternList", nodePatternList);
                nodeConditions.Add("nodeEmitterList", nodeEmitterList);
                nodeConditions.Add("nodeInitqualList", nodeInitqualList);
                nodeConditions.Add("nodeSourcequalList", nodeSourcequalList);
                nodeConditions.Add("nodeSourcepatList", nodeSourcepatList);
                nodeConditions.Add("nodeSourcetypeList", nodeSourcetypeList);
                nodeConditions.Add("nodeTanklevelList", nodeTanklevelList);
                nodeConditions.Add("nodeDemandList", nodeDemandList);
                nodeConditions.Add("nodeHeadList", nodeHeadList);
                nodeConditions.Add("nodePressureList", nodePressureList);
                nodeConditions.Add("nodeQualityList", nodeQualityList);
                nodeConditions.Add("nodeSourcemassList", nodeSourcemassList);
                nodeConditions.Add("nodeInitvolumeList", nodeInitvolumeList);
                nodeConditions.Add("nodeMixmodelList", nodeMixmodelList);
                nodeConditions.Add("nodeMixzonevolList", nodeMixzonevolList);
                nodeConditions.Add("nodeTankdiamList", nodeTankdiamList);
                nodeConditions.Add("nodeMinvolumeList", nodeMinvolumeList);
                nodeConditions.Add("nodeVolcurveList", nodeVolcurveList);
                nodeConditions.Add("nodeMinlevelList", nodeMinlevelList);
                nodeConditions.Add("nodeMaxlevelList", nodeMaxlevelList);
                nodeConditions.Add("nodeMixfractionList", nodeMixfractionList);
                nodeConditions.Add("nodeTankKbulkList", nodeTankKbulkList);
                nodeConditions.Add("nodeAnalysisTypeList", nodeAnalysisTypeList);

                aDao.InsertNodeAnalysisResultBulk(manager, nodeConditions);

                linkConditions.Add("linkRptNumber", linkRptNumber);
                linkConditions.Add("linkInpNumber", linkInpNumber);
                linkConditions.Add("linkLinkId", linkLinkId);
                linkConditions.Add("linkAnalysisTime", linkAnalysisTime);
                linkConditions.Add("linkAnalysisType", linkAnalysisType);
                linkConditions.Add("linkDiameter", linkDiameter);
                linkConditions.Add("linkLength", linkLength);
                linkConditions.Add("linkRoughness", linkRoughness);
                linkConditions.Add("linkMinorloss", linkMinorloss);
                linkConditions.Add("linkInitstatus", linkInitstatus);
                linkConditions.Add("linkInitsetting", linkInitsetting);
                linkConditions.Add("linkKbulk", linkKbulk);
                linkConditions.Add("linkKwall", linkKwall);
                linkConditions.Add("linkFlow", linkFlow);
                linkConditions.Add("linkVelocity", linkVelocity);
                linkConditions.Add("linkHeadloss", linkHeadloss);
                linkConditions.Add("linkStatus", linkStatus);
                linkConditions.Add("linkSetting", linkSetting);
                linkConditions.Add("linkEnergy", linkEnergy);

                aDao.InsertLinkAnalysisResultBulk(manager, linkConditions);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }
        //해석결과 저장 중 오류 시 모든 해석정보를 삭제한다.
        private void DeleteAllReportData(string rptNumber)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                Hashtable conditions = new Hashtable();

                conditions.Add("RPT_NUMBER", rptNumber);

                aDao.DeleteReportMaster(manager, conditions);
                aDao.DeleteNodeReport(manager, conditions);
                aDao.DeleteLinkReport(manager, conditions);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        //Epanet관련 오류코드 처리
        private bool AnalysisErrorHandling(int errorCode)
        {
            bool result = true;

            if (errorCode > 10)
            {
                result = false;
                Console.WriteLine("해석 오류 : " + errorCode);

                DataTable settingDataTable = null;
                EMapper manager = null;

                try
                {
                    manager = new EMapper(AppStatic.WATERNET_DATABASE);
                    manager.BeginTransaction();

                    DataSet settingDataSet = aDao.SelectAnalysisSetting(manager, tmpConditions);
                    settingDataTable = settingDataSet.Tables["WH_ANALYSIS_SETTING"];

                    //해석오류 저장여부
                    if (isSaveError)
                    {
                        Hashtable errorConditions = new Hashtable();
                        errorConditions.Add("IDX", errorIdx.ToString());
                        errorConditions.Add("INP_NUMBER", tmpConditions["INP_NUMBER"]);
                        errorConditions.Add("ERROR_CODE", errorCode.ToString());
                        errorConditions.Add("INCREASE_DATE", tmpConditions["TARGET_DATE"]);

                        aDao.InsertRealtimeAnalysisError(manager, errorConditions);

                        errorIdx++;
                    }

                    manager.CommitTransaction();

                }
                catch (Exception ex)
                {
                    manager.RollbackTransaction();
                    Logger.Error(ex);
                }
                finally
                {
                    manager.Dispose();
                }

                //경고 입력 (별도 트랜잭션)
                if (settingDataTable != null)
                {
                    if ("Y".Equals(settingDataTable.Rows[0]["ERROR_SAVE_YN"].ToString()))
                    {
                        //경고 입력 (별도 트랜잭션)
                        Hashtable warningConditions = new Hashtable();

                        warningConditions.Add("WORK_GBN", "0001");
                        warningConditions.Add("LOC_CODE", locCode);
                        warningConditions.Add("WAR_CODE", "000001");
                        warningConditions.Add("SERIAL_NO", inpNumber);
                        warningConditions.Add("REMARK", "");

                        wWork.InsertWarningData(warningConditions);
                    }
                }
            }

            return result;
        }

        //Epanet관련 오류코드 처리
        public void ExternalAnalysisErrorHandling(string inpNumber, string targetDate, int errorCode)
        {
            if (errorCode > 10)
            {
                Console.WriteLine("해석 오류 : " + errorCode);

                DataTable settingDataTable = null;
                EMapper manager = null;

                try
                {
                    manager = new EMapper(AppStatic.WATERNET_DATABASE);
                    manager.BeginTransaction();

                    Hashtable settingCondition = new Hashtable();
                    settingCondition.Add("INP_NUMBER", inpNumber);

                    DataSet settingDataSet = aDao.SelectAnalysisSetting(manager, settingCondition);
                    settingDataTable = settingDataSet.Tables["WH_ANALYSIS_SETTING"];

                    if (settingDataTable.Rows.Count != 0)
                    {
                        if ("Y".Equals(settingDataTable.Rows[0]["ERROR_SAVE_YN"].ToString()))
                        {
                            Hashtable errorConditions = new Hashtable();
                            errorConditions.Add("IDX", "0");
                            errorConditions.Add("INP_NUMBER", inpNumber);
                            errorConditions.Add("ERROR_CODE", errorCode.ToString());
                            errorConditions.Add("INCREASE_DATE", targetDate);

                            aDao.InsertRealtimeAnalysisError(manager, errorConditions);
                        }
                    }

                    manager.CommitTransaction();
                }
                catch (Exception ex)
                {
                    manager.RollbackTransaction();
                    Logger.Error(ex);
                }
                finally
                {
                    manager.Dispose();
                }

                //경고 입력 (별도 트랜잭션)
                if (settingDataTable != null)
                {
                    if ("Y".Equals(settingDataTable.Rows[0]["ERROR_SAVE_YN"].ToString()))
                    {
                        //경고 시 저장될 지역코드 (소블록부터 확인한다.)
                        if (!"".Equals(Utils.nts(settingDataTable.Rows[0]["SFTRIDN"])))
                        {
                            locCode = Utils.nts(settingDataTable.Rows[0]["SFTRIDN"]);
                        }
                        else if (!"".Equals(Utils.nts(settingDataTable.Rows[0]["MFTRIDN"])))
                        {
                            locCode = Utils.nts(settingDataTable.Rows[0]["MFTRIDN"]);
                        }
                        else if (!"".Equals(Utils.nts(settingDataTable.Rows[0]["LFTRIDN"])))
                        {
                            locCode = Utils.nts(settingDataTable.Rows[0]["LFTRIDN"]);
                        }

                        Hashtable warningConditions = new Hashtable();

                        warningConditions.Add("WORK_GBN", "0001");
                        warningConditions.Add("LOC_CODE", locCode);
                        warningConditions.Add("WAR_CODE", "000001");
                        warningConditions.Add("SERIAL_NO", inpNumber);
                        warningConditions.Add("REMARK", "");

                        wWork.InsertWarningData(warningConditions);
                    }
                }
            }
        }

        //에너지 해석결과조합 저장 : wjs추가 2012.07.19
        public void UpdateEnergyHydrodynamicResult(string rptNumber)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                aDao.UpdateEnergyHydrodynamicResult(manager, rptNumber);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        //에너지 관련 결과 저장 : wjs추가 2012.07.19
        public string GetEnergyModel(string inpNumber)
        {
            EMapper manager = null;
            string result = "N";
            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);

                result = aDao.GetEnergyModel(manager, inpNumber);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }
    }
}
