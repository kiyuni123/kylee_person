﻿using System;
using System.Collections;
using System.Threading;
using System.Text;

using WaterNETServer.WH_Common.utils;
using EMFrame.work;
using EMFrame.log;

namespace WaterNETServer.WH_AnalysisScheduleManage.etc
{
    public class AnalysisScheduleManager : BaseWork
    {
        //Scheduling job thread를 저장하는 풀
        private static Hashtable threadPool = new Hashtable();
        private static WatcherThread watcher = null;
        private static Thread watcherThread = null;
        private static AnalysisScheduleManager scheduleManager = null;

        private AnalysisScheduleManager()
        {
            //Watcher 활성화
            watcher = WatcherThread.GetInstance();
            watcherThread = new Thread(watcher.RunThread);
            watcherThread.Start();
        }

        public static AnalysisScheduleManager GetInstance()
        {
            if (scheduleManager == null)
            {
                scheduleManager = new AnalysisScheduleManager();
            }

            return scheduleManager;
        }

        //실시간 관망해석 작업 시작 및 재시작
        public void RunSchedulingJob(Hashtable jobInfo)
        {
            if (jobInfo["IDX"] != null && jobInfo["HYDRA_CYCLE"] != null)
            {
                //Pool 내부에 등록된 Thread가 존재하는지 확인
                if (threadPool[(string)jobInfo["IDX"]] != null)
                {
                    //*****************************
                    //존재하는 경우는 Abolt 시키고 풀에서 제거 후 새로 만들어 넣는다.
                    ((Thread)(threadPool[(string)jobInfo["IDX"]])).Abort();
                    threadPool.Remove((string)jobInfo["IDX"]);
                }


                AnalysisThread job = new AnalysisThread();
                Thread thread = new Thread(job.RunThread);

                thread.Start(jobInfo);
                threadPool.Add((string)jobInfo["IDX"], thread);
            }
            else
            {
                throw new Exception("실행할 작업정보를 넘겨받지 못했습니다.");
            }
        }

        //실시간 관망해석 작업 정지
        public void StopSchedulingJob(Hashtable jobInfo)
        {
            if (jobInfo["IDX"] != null && jobInfo["HYDRA_CYCLE"] != null)
            {
                if (threadPool[(string)jobInfo["IDX"]] != null)
                {
                    //*******************
                    //정지요청인 경우 Abolt시키고 pool에서 제거한다.
                    ((Thread)(threadPool[(string)jobInfo["IDX"]])).Abort();
                    threadPool.Remove((string)jobInfo["IDX"]);
                }
            }
            else
            {
                throw new Exception("정지할 작업정보를 넘겨받지 못했습니다.");
            }
        }

        //Threadpool의 상태를 Watcher의 logString에 기록
        public static void SetThreadpoolStatus()
        {
            try
            {
                StringBuilder statusString = new StringBuilder();

                statusString.AppendLine("==================== Thread Pool Monitor Start====================");
                statusString.AppendLine("Time : " + Utils.GetTime()["yyyymmddhhmmss"]);

                foreach (string key in threadPool.Keys)
                {
                    statusString.AppendLine(key + " : " + ((Thread)threadPool[key]).ThreadState);

                    if (ThreadState.Stopped == ((Thread)threadPool[key]).ThreadState)
                    {
                        ((Thread)threadPool[key]).Start();
                    }
                }

                statusString.AppendLine("==================== Thread Pool Monitor End ====================");

                WatcherThread.SetStatus(statusString.ToString());
            }
            catch(Exception e)
            {
                Console.WriteLine("check2 : " + e.ToString());
            }
        }

        public static void DestroyAnalysisJob()
        {
            try
            {
                if (watcher != null)
                {
                    watcher.stopThread();
                    watcher = null;
                }

                if (watcherThread != null)
                {
                    watcherThread.Abort();
                    watcherThread = null;
                }

                foreach (string key in threadPool.Keys)
                {
                    Thread thread = (Thread)threadPool[key];
                    thread.Abort();
                    thread = null;
                }

                threadPool.Clear();
                threadPool = null;
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
                //Console.WriteLine("최종종료...");
            }
        }
    }
}
