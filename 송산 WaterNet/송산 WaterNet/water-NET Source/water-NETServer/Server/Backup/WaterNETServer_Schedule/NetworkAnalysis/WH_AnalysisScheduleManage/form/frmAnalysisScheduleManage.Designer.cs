﻿namespace WaterNETServer.WH_AnalysisScheduleManage.form
{
    partial class frmAnalysisScheduleManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butAllStop = new System.Windows.Forms.Button();
            this.butAllStart = new System.Windows.Forms.Button();
            this.butAddSchedule = new System.Windows.Forms.Button();
            this.butDeleteSchedule = new System.Windows.Forms.Button();
            this.butSaveSchedule = new System.Windows.Forms.Button();
            this.cheIsRemainResult = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.texHydraCycle = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.griJobList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ricJobStatus = new System.Windows.Forms.RichTextBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griJobList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(889, 9);
            this.pictureBox2.TabIndex = 113;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 601);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(889, 9);
            this.pictureBox1.TabIndex = 114;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 592);
            this.picFrLeftM1.TabIndex = 115;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(879, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 592);
            this.pictureBox3.TabIndex = 116;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pictureBox5);
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 100);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(3, 17);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(194, 9);
            this.pictureBox5.TabIndex = 129;
            this.pictureBox5.TabStop = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 7;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 107F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 511F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 301F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.griJobList);
            this.groupBox1.Controls.Add(this.pictureBox10);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.pictureBox7);
            this.groupBox1.Controls.Add(this.pictureBox8);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Controls.Add(this.pictureBox6);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(869, 307);
            this.groupBox1.TabIndex = 117;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "실시간 관망해석 모델";
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox10.Location = new System.Drawing.Point(13, 255);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(843, 9);
            this.pictureBox10.TabIndex = 128;
            this.pictureBox10.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butAllStop);
            this.panel1.Controls.Add(this.butAllStart);
            this.panel1.Controls.Add(this.butAddSchedule);
            this.panel1.Controls.Add(this.butDeleteSchedule);
            this.panel1.Controls.Add(this.butSaveSchedule);
            this.panel1.Controls.Add(this.cheIsRemainResult);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.texHydraCycle);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(13, 264);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(843, 31);
            this.panel1.TabIndex = 126;
            // 
            // butAllStop
            // 
            this.butAllStop.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butAllStop.Font = new System.Drawing.Font("굴림", 9F);
            this.butAllStop.Location = new System.Drawing.Point(582, 5);
            this.butAllStop.Name = "butAllStop";
            this.butAllStop.Size = new System.Drawing.Size(75, 23);
            this.butAllStop.TabIndex = 53;
            this.butAllStop.Text = "일괄종료";
            this.butAllStop.UseVisualStyleBackColor = true;
            this.butAllStop.Click += new System.EventHandler(this.butAllStop_Click);
            // 
            // butAllStart
            // 
            this.butAllStart.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butAllStart.Font = new System.Drawing.Font("굴림", 9F);
            this.butAllStart.Location = new System.Drawing.Point(504, 5);
            this.butAllStart.Name = "butAllStart";
            this.butAllStart.Size = new System.Drawing.Size(75, 23);
            this.butAllStart.TabIndex = 52;
            this.butAllStart.Text = "일괄시작";
            this.butAllStart.UseVisualStyleBackColor = true;
            this.butAllStart.Click += new System.EventHandler(this.butAllStart_Click);
            // 
            // butAddSchedule
            // 
            this.butAddSchedule.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butAddSchedule.Font = new System.Drawing.Font("굴림", 9F);
            this.butAddSchedule.Location = new System.Drawing.Point(765, 5);
            this.butAddSchedule.Name = "butAddSchedule";
            this.butAddSchedule.Size = new System.Drawing.Size(72, 23);
            this.butAddSchedule.TabIndex = 51;
            this.butAddSchedule.Text = "신규등록";
            this.butAddSchedule.UseVisualStyleBackColor = true;
            this.butAddSchedule.Click += new System.EventHandler(this.butAddSchedule_Click);
            // 
            // butDeleteSchedule
            // 
            this.butDeleteSchedule.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butDeleteSchedule.Location = new System.Drawing.Point(713, 5);
            this.butDeleteSchedule.Name = "butDeleteSchedule";
            this.butDeleteSchedule.Size = new System.Drawing.Size(49, 23);
            this.butDeleteSchedule.TabIndex = 50;
            this.butDeleteSchedule.Text = "삭제";
            this.butDeleteSchedule.UseVisualStyleBackColor = true;
            this.butDeleteSchedule.Click += new System.EventHandler(this.butDeleteSchedule_Click);
            // 
            // butSaveSchedule
            // 
            this.butSaveSchedule.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSaveSchedule.Font = new System.Drawing.Font("굴림", 9F);
            this.butSaveSchedule.Location = new System.Drawing.Point(660, 5);
            this.butSaveSchedule.Name = "butSaveSchedule";
            this.butSaveSchedule.Size = new System.Drawing.Size(49, 23);
            this.butSaveSchedule.TabIndex = 49;
            this.butSaveSchedule.Text = "저장";
            this.butSaveSchedule.UseVisualStyleBackColor = true;
            this.butSaveSchedule.Click += new System.EventHandler(this.butSaveSchedule_Click);
            // 
            // cheIsRemainResult
            // 
            this.cheIsRemainResult.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cheIsRemainResult.AutoSize = true;
            this.cheIsRemainResult.Location = new System.Drawing.Point(228, 8);
            this.cheIsRemainResult.Name = "cheIsRemainResult";
            this.cheIsRemainResult.Size = new System.Drawing.Size(100, 16);
            this.cheIsRemainResult.TabIndex = 27;
            this.cheIsRemainResult.Text = "해석결과 저장";
            this.cheIsRemainResult.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("굴림", 9F);
            this.label30.Location = new System.Drawing.Point(204, 9);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(17, 12);
            this.label30.TabIndex = 5;
            this.label30.Text = "분";
            // 
            // texHydraCycle
            // 
            this.texHydraCycle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texHydraCycle.Location = new System.Drawing.Point(96, 5);
            this.texHydraCycle.Name = "texHydraCycle";
            this.texHydraCycle.Size = new System.Drawing.Size(100, 21);
            this.texHydraCycle.TabIndex = 4;
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("굴림", 9F);
            this.label29.Location = new System.Drawing.Point(9, 10);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(81, 12);
            this.label29.TabIndex = 3;
            this.label29.Text = "관망해석 주기";
            // 
            // griJobList
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.griJobList.DisplayLayout.Appearance = appearance1;
            this.griJobList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griJobList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.griJobList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griJobList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.griJobList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.griJobList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.griJobList.DisplayLayout.MaxColScrollRegions = 1;
            this.griJobList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.griJobList.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.griJobList.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.griJobList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.griJobList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.griJobList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.griJobList.DisplayLayout.Override.CellAppearance = appearance8;
            this.griJobList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.griJobList.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.griJobList.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.griJobList.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.griJobList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griJobList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.griJobList.DisplayLayout.Override.RowAppearance = appearance11;
            this.griJobList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.griJobList.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.griJobList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griJobList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griJobList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.griJobList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griJobList.Location = new System.Drawing.Point(13, 26);
            this.griJobList.Name = "griJobList";
            this.griJobList.Size = new System.Drawing.Size(843, 229);
            this.griJobList.TabIndex = 125;
            this.griJobList.Text = "ultraGrid1";
            this.griJobList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.griJobList_ClickCellButton);
            this.griJobList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griJobList_AfterSelectChange);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox7.Location = new System.Drawing.Point(856, 26);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 269);
            this.pictureBox7.TabIndex = 124;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox8.Location = new System.Drawing.Point(3, 26);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(10, 269);
            this.pictureBox8.TabIndex = 123;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(3, 295);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(863, 9);
            this.pictureBox4.TabIndex = 122;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox6.Location = new System.Drawing.Point(3, 17);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(863, 9);
            this.pictureBox6.TabIndex = 121;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox11.Location = new System.Drawing.Point(10, 316);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(869, 9);
            this.pictureBox11.TabIndex = 129;
            this.pictureBox11.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ricJobStatus);
            this.groupBox2.Controls.Add(this.pictureBox15);
            this.groupBox2.Controls.Add(this.pictureBox14);
            this.groupBox2.Controls.Add(this.pictureBox13);
            this.groupBox2.Controls.Add(this.pictureBox12);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(10, 325);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(869, 276);
            this.groupBox2.TabIndex = 130;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "실행 로그";
            // 
            // ricJobStatus
            // 
            this.ricJobStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ricJobStatus.Location = new System.Drawing.Point(13, 26);
            this.ricJobStatus.Name = "ricJobStatus";
            this.ricJobStatus.Size = new System.Drawing.Size(843, 238);
            this.ricJobStatus.TabIndex = 133;
            this.ricJobStatus.Text = "";
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox15.Location = new System.Drawing.Point(856, 26);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(10, 238);
            this.pictureBox15.TabIndex = 132;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox14.Location = new System.Drawing.Point(3, 26);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(10, 238);
            this.pictureBox14.TabIndex = 131;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox13.Location = new System.Drawing.Point(3, 17);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(863, 9);
            this.pictureBox13.TabIndex = 130;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox12.Location = new System.Drawing.Point(3, 264);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(863, 9);
            this.pictureBox12.TabIndex = 129;
            this.pictureBox12.TabStop = false;
            // 
            // frmAnalysisScheduleManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 610);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Name = "frmAnalysisScheduleManage";
            this.Text = "실시간 관망해석 관리";
            this.Load += new System.EventHandler(this.frmAnalysisScheduleManage_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAnalysisScheduleManage_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griJobList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private Infragistics.Win.UltraWinGrid.UltraGrid griJobList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Button butAddSchedule;
        private System.Windows.Forms.Button butDeleteSchedule;
        private System.Windows.Forms.Button butSaveSchedule;
        private System.Windows.Forms.CheckBox cheIsRemainResult;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox texHydraCycle;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.RichTextBox ricJobStatus;
        private System.Windows.Forms.Button butAllStop;
        private System.Windows.Forms.Button butAllStart;
    }
}