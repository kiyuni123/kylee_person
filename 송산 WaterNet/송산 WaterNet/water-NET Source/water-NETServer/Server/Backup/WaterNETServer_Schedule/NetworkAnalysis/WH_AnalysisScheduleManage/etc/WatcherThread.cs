﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WaterNETServer.WH_AnalysisScheduleManage.work;
using WaterNETServer.WH_Common.utils;
using EMFrame.work;
using WaterNETServer.Warning.work;
using EMFrame.log;

namespace WaterNETServer.WH_AnalysisScheduleManage.etc
{
    public class WatcherThread : BaseWork
    {
        private static StringBuilder logString = new StringBuilder();
        private static WatcherThread watcher = null;

        private AnalysisScheduleManageWork work = null;
        private WarningWork wWork = null;

        private string executeDeleteDate = "";
        private string executeIndexRebuildDate = "";
        private bool isStop = false;

        private WatcherThread()
        {
            //work = AnalysisScheduleManageWork.GetInstance();
            work = new AnalysisScheduleManageWork();
            wWork = WarningWork.GetInstance();
        }

        public static WatcherThread GetInstance()
        {
            if(watcher == null)
            {
                watcher = new WatcherThread();
            }

            return watcher;
        }

        public void RunThread(Object obj)
        {
            while(true)
            {
                try
                {
                    if (!isStop)
                    {
                        Hashtable timeData = Utils.GetTime();

                        //매일 01시와 04시에 삭제작업 수행
                        if ("01:02".Equals(timeData["hhmm"].ToString()) || "04:21".Equals(timeData["hhmm"].ToString()))
                        {
                            if ("".Equals(executeDeleteDate) || !executeDeleteDate.Equals(timeData["deleteCheck"].ToString()))
                            {
                                executeDeleteDate = timeData["deleteCheck"].ToString();
                                work.DeleteOldAnalysisResult();

                                wWork.DeleteWarningData();
                            }
                        }

                        //매일 오후11시에 인덱스 리빌드 시작
                        if("23:41".Equals(timeData["hhmm"].ToString()))
                        {
                            if ("".Equals(executeIndexRebuildDate) || !executeIndexRebuildDate.Equals(timeData["deleteCheck"].ToString()))
                            {
                                executeIndexRebuildDate = timeData["deleteCheck"].ToString();

                                work.AnalysisResultIndexTableRebuild();
                            }
                        }

                        //10초에 한번씩 메니져로 부터 Thread의 상태를 받아온다.
                        AnalysisScheduleManager.SetThreadpoolStatus();
                        Thread.Sleep(10 * 1000);
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }

        public void stopThread()
        {
            isStop = true;
        }

        //누적된 메세지를 반환
        public static string GetThreadStatus()
        {
            return logString.ToString();
        }

        //외부에서 로그스트링에 메세지를 추가
        public static void SetStatus(string message)
        {
            logString.Remove(0, logString.Length);
            logString.AppendLine(message);
        }
    }
}
