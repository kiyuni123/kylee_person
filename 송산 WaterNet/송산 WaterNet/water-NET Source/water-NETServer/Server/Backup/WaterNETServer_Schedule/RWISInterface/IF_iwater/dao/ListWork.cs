﻿using System;
using System.Data;
using System.Collections;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;

namespace WaterNETServer.IF_iwater.dao
{
    public class ListWork : BaseWork
    {
        private ListDao wDao = null;
        public ListWork()
        {
            wDao = new ListDao();
        }

        /// <summary>
        /// 실시간 수집 데이터를 조회한다
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public DataSet selectRealtime(string tableName, Hashtable param)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.selectRealtimeDao(manager, result, tableName, param);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        /// <summary>
        /// 금일적산 데이터를 조회한다
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public DataSet selectAccumulationToday(string tableName, Hashtable param)
        {
            DataSet result = new DataSet();
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                wDao.selectAccumulationTodayDao(manager, result, tableName, param);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }
    }
}
