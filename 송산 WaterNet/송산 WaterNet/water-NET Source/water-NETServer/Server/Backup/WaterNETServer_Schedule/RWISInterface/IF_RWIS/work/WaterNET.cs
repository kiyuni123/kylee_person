﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNETServer.IF_RWIS.dao;
using System.Collections;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;
using System.Data;

namespace WaterNETServer.IF_RWIS.work
{
    public class WaterNET
    {
        private static WaterNET work = null;
        private WaterNETDao dao = null;

        private WaterNET()
        {
            dao = WaterNETDao.GetInstance();
        }

        public static WaterNET GetInstance()
        {
            if (work == null)
            {
                work = new WaterNET();
            }

            return work;
        }

        /// <summary>
        /// 태그구분 조회
        /// </summary>
        /// <param name="gbn"></param>
        /// <returns></returns>
        public string[] SelectTagGbn(string gbn)
        {
            EMapper waterNET_mapper = null;

            IList<string> resultValue = new List<string>();

            string[] kind = null;

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                DataTable data = dao.SelectTagGbn(waterNET_mapper, gbn);

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        resultValue.Add(row["CODE"].ToString());
                    }
                }
                
                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                waterNET_mapper.RollbackTransaction();

            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
            }

            kind = resultValue.ToArray(); 

            return kind;
        }


        /// <summary>
        /// 태그목록 조회
        /// </summary>
        /// <param name="kinds"></param>
        /// <returns></returns>
        public IList<string> SelectTagList(string[] kinds)
        {
            EMapper waterNET_mapper = null;

            IList<string> resultValue = new List<string>();

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                DataTable data = dao.SelectTagList(waterNET_mapper, kinds);

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        resultValue.Add(row["TAGNAME"].ToString());
                    }
                }

                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                waterNET_mapper.RollbackTransaction();

            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
            }

            return resultValue;
        }

        /// <summary>
        /// 실시간데이터 자동, 보정
        /// 미수집데이터 자동, 보정
        /// </summary>
        /// <param name="tagList"></param>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        public int InsertGatherRealtimeData(IList<string> tagList, DateTime timestamp)
        {
            EMapper rwis_mapper = null;
            EMapper waterNET_mapper = null;

            int returnValue = 0;

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                try
                {
                    rwis_mapper = new EMapper(AppStatic.RWIS_DATABASE);
                    rwis_mapper.BeginTransaction();
                }
                catch (Exception ex)
                {
                    foreach (string tagname in tagList)
                    {
                        Hashtable missing = new Hashtable();
                        missing["TAGNAME"] = tagname;
                        missing["TIMESTAMP"] = timestamp.ToString("yyyyMMddHHmm");
                        missing["GATHER_YN"] = "N";
                        missing["MISSING_GUBUN"] = "000002";

                        dao.InsertMissingRealtimeData(waterNET_mapper, missing);
                    }

                    Logger.Error(ex);
                    waterNET_mapper.CommitTransaction();
                    throw new Exception("RWIS 서버에 접속할수 없습니다.");
                }

                if (tagList == null || tagList.Count == 0)
                {
                    throw new Exception("water-NET 실시간 데이터 태그 목록이 존재하지 않습니다.");
                }

                //일반태그, 계산태그 분류
                IList<string> rwisTagList = new List<string>();
                IList<string> calTagList = new List<string>();

                foreach (string tagname in tagList)
                {
                    if (tagname.IndexOf("DUMMY") == -1)
                    {
                        rwisTagList.Add(tagname);
                    }
                    else
                    {
                        calTagList.Add(tagname);
                    }
                }

                DataTable tagData = null;

                if (rwisTagList.Count > 0)
                {
                    try
                    {
                        tagData = RWIS.GetInstance().SelectGatherRealtimeData(rwis_mapper, rwisTagList, timestamp);
                    }
                    catch (Exception ex)
                    {
                        foreach (string tagname in tagList)
                        {
                            Hashtable missing = new Hashtable();
                            missing["TAGNAME"] = tagname;
                            missing["TIMESTAMP"] = timestamp.ToString("yyyyMMddHHmm");
                            missing["GATHER_YN"] = "N";
                            missing["MISSING_GUBUN"] = "000002";

                            dao.InsertMissingRealtimeData(waterNET_mapper, missing);
                        }

                        Logger.Error(ex);
                        waterNET_mapper.CommitTransaction();
                        throw new Exception("RWIS 서버에 접속할수 없습니다.");
                    }
                }

                if (tagData != null)
                {
                    foreach (string tagname in rwisTagList)
                    {
                        object val = null;

                        foreach (DataRow row in tagData.Rows)
                        {
                            if (tagname.Equals(row["TAGSN"].ToString()))
                            {
                                //계수 조회
                                object confficient = dao.SelectConfficient(waterNET_mapper, tagname);

                                //계수로 값 계산
                                val = Convert.ToDouble(row["VAL"]) * Convert.ToDouble(confficient);

                                try
                                {
                                    Hashtable parameter = new Hashtable();
                                    parameter["TAGNAME"] = tagname;
                                    parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHHmm");
                                    parameter["VALUE"] = val;
                                    parameter["MISSING_GUBUN"] = "000002";

                                    //기존태그값삭제
                                    dao.DeleteGatherRealtimeData(waterNET_mapper, parameter);

                                    //태그값등록
                                    returnValue += dao.InsertGatherRealtimeData(waterNET_mapper, parameter);

                                    if (dao.CheckMissingRealtimeData(waterNET_mapper, parameter) == 1)
                                    {
                                        parameter["GATHER_YN"] = "Y";
                                        parameter["GATHER_DATE"] = DateTime.Now.ToString("yyyyMMddHHmm");

                                        dao.DeleteMissingRealtimeData(waterNET_mapper, parameter);
                                        //dao.UpdateMissingRealtimeData(waterNET_mapper, parameter);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex);
                                }

                                break;
                            }
                        }

                        if (val == DBNull.Value || val == null)
                        {
                            try
                            {
                                Hashtable parameter = new Hashtable();
                                parameter["TAGNAME"] = tagname;
                                parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHHmm");
                                parameter["GATHER_YN"] = "N";
                                parameter["MISSING_GUBUN"] = "000002";

                                if (dao.CheckMissingRealtimeData(waterNET_mapper, parameter) == 0)
                                {
                                    dao.InsertMissingRealtimeData(waterNET_mapper, parameter);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex);
                            }
                        }
                    }
                }

                if (calTagList.Count > 0)
                {
                    //계산태그에 사용되는 원태그를 한번에 조회
                    DataTable ctagnames = dao.SelectCalTagList(waterNET_mapper, calTagList);
                    IList<string> cTagList = new List<string>();
                    foreach (DataRow row in ctagnames.Rows)
                    {
                        cTagList.Add(row["CTAGNAME"].ToString());
                    }

                    cTagList = cTagList.ToArray().Distinct().ToList();

                    //계산태그에 사용되는 원태그값
                    DataTable cTagData = RWIS.GetInstance().SelectGatherRealtimeData(rwis_mapper, cTagList, timestamp);

                    foreach (string tagname in calTagList)
                    {
                        object val = null;

                        //계산식목록 조회
                        DataTable calList = dao.SelectCalTagList(waterNET_mapper, tagname);
                        if (calList != null && calList.Rows.Count != 0)
                        {
                            double dou_cal_val = 0;

                            foreach (DataRow row in calList.Rows)
                            {
                                object obj_cal_val = null;
                                //RWIS.GetInstance().SelectGatherRealtimeData(rwis_mapper, row["CTAGNAME"].ToString(), timestamp);

                                if (cTagData != null)
                                {
                                    foreach (DataRow cal in cTagData.Rows)
                                    {
                                        if (cal["TAGSN"].ToString().Equals(row["CTAGNAME"].ToString()))
                                        {
                                            //계수 조회
                                            object confficient = dao.SelectConfficient(waterNET_mapper, row["CTAGNAME"].ToString());

                                            //계수로 값 계산
                                            obj_cal_val = Convert.ToDouble(cal["VAL"]) * Convert.ToDouble(confficient);

                                            Hashtable parameter = new Hashtable();
                                            parameter["TAGNAME"] = row["CTAGNAME"].ToString();
                                            parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHHmm");
                                            parameter["VALUE"] = obj_cal_val;
                                            
                                            //기존태그값삭제
                                            dao.DeleteGatherRealtimeData(waterNET_mapper, parameter);

                                            //태그값등록
                                            returnValue += dao.InsertGatherRealtimeData(waterNET_mapper, parameter);

                                            break;
                                        }
                                    }
                                }

                                if (obj_cal_val == DBNull.Value || obj_cal_val == null)
                                {
                                    val = DBNull.Value;
                                    break;
                                }

                                if ("P".Equals(row["CAL_GBN"].ToString()))
                                {
                                    dou_cal_val += Convert.ToDouble(obj_cal_val);
                                }
                                if ("M".Equals(row["CAL_GBN"].ToString()))
                                {
                                    dou_cal_val -= Convert.ToDouble(obj_cal_val);
                                }

                                val = dou_cal_val;
                            }

                            if (val != DBNull.Value && val != null)
                            {
                                try
                                {
                                    //계수 조회
                                    object confficient = dao.SelectConfficient(waterNET_mapper, tagname);

                                    //계수로 값 계산
                                    val = Convert.ToDouble(val) * Convert.ToDouble(confficient);

                                    Hashtable parameter = new Hashtable();
                                    parameter["TAGNAME"] = tagname;
                                    parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHHmm");
                                    parameter["VALUE"] = val;
                                    parameter["MISSING_GUBUN"] = "000002";

                                    //기존태그값삭제
                                    dao.DeleteGatherRealtimeData(waterNET_mapper, parameter);

                                    //태그값등록
                                    returnValue += dao.InsertGatherRealtimeData(waterNET_mapper, parameter);

                                    if (dao.CheckMissingRealtimeData(waterNET_mapper, parameter) == 1)
                                    {
                                        parameter["GATHER_YN"] = "Y";
                                        parameter["GATHER_DATE"] = DateTime.Now.ToString("yyyyMMddHHmm");

                                        dao.DeleteMissingRealtimeData(waterNET_mapper, parameter);
                                        //dao.UpdateMissingRealtimeData(waterNET_mapper, parameter);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex);
                                }
                            }
                            else
                            {
                                try
                                {
                                    Hashtable parameter = new Hashtable();
                                    parameter["TAGNAME"] = tagname;
                                    parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHHmm");
                                    parameter["GATHER_YN"] = "N";
                                    parameter["MISSING_GUBUN"] = "000002";

                                    if (dao.CheckMissingRealtimeData(waterNET_mapper, parameter) == 0)
                                    {
                                        dao.InsertMissingRealtimeData(waterNET_mapper, parameter);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex);
                                }
                            }
                        }
                    }
                }

                rwis_mapper.CommitTransaction();
                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                rwis_mapper.RollbackTransaction();
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
                if (rwis_mapper != null)
                {
                    rwis_mapper.Dispose();
                }
            }

            return returnValue;
        }

        /// <summary>
        /// 금일적산 자동, 보정
        /// </summary>
        /// <param name="timestamp"></param>
        public void InsertAccumulationToDayData(DateTime timestamp)
        {
            EMapper waterNET_mapper = null;

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();
                dao.InsertAccumulationToDayData(waterNET_mapper, timestamp);
                dao.DeleteAccumulationToDayData(waterNET_mapper, timestamp);
                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
            }
        }

        /// <summary>
        /// 전일적산 자동, 보정
        /// </summary>
        /// <param name="timestamp"></param>
        public void InsertAccumulationDayData(IList<string> tagList, DateTime timestamp)
        {
            EMapper waterNET_mapper = null;

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                foreach (string tagname in tagList)
                {
                    dao.InsertAccumulationDayData(waterNET_mapper, tagname, timestamp);
                }

                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
            }
        }

        /// <summary>
        /// 적산 보정 로그 업데이트
        /// </summary>
        /// <param name="timestamp"></param>
        public int InsertAccumulationHourLogData(IList<string> tagList)
        {
            EMapper rwis_mapper = null;
            EMapper waterNET_mapper = null;

            int returnValue = 0;

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                try
                {
                    rwis_mapper = new EMapper(AppStatic.RWIS_DATABASE);
                    rwis_mapper.BeginTransaction();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    waterNET_mapper.CommitTransaction();
                    throw new Exception("RWIS 서버에 접속할수 없습니다.");
                }

                if (tagList == null || tagList.Count == 0)
                {
                    throw new Exception("water-NET 실시간 데이터 태그 목록이 존재하지 않습니다.");
                }

                //일반태그, 계산태그 분류
                IList<string> rwisTagList = new List<string>();
                IList<string> calTagList = new List<string>();

                foreach (string tagname in tagList)
                {
                    if (tagname.IndexOf("DUMMY") == -1)
                    {
                        rwisTagList.Add(tagname);
                    }
                    else
                    {
                        calTagList.Add(tagname);
                    }
                }

                DataTable tagData = null;

                if (rwisTagList.Count > 0)
                {
                    tagData = RWIS.GetInstance().SelectAccumulationHourLogData(rwis_mapper, rwisTagList);
                }

                if (tagData != null)
                {
                    foreach (DataRow row in tagData.Rows)
                    {
                        object val = row["CUR_VAL"];
                        string tagname = row["TAGSN"].ToString();

                        if (row["CUR_VAL"] != DBNull.Value && row["CUR_VAL"] != null)
                        {
                            try
                            {
                                Hashtable parameter = new Hashtable();
                                parameter["TAGNAME"] = tagname;
                                parameter["TIMESTAMP"] = row["LOG_TIME"].ToString();
                                parameter["VALUE"] = val;

                                //기존태그값삭제
                                dao.DeleteAccumulationHourData(waterNET_mapper, parameter);

                                //태그값등록
                                returnValue += dao.InsertAccumulationHourData(waterNET_mapper, parameter);

                                RWIS.GetInstance().UpdateLogData(rwis_mapper, parameter);
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex);
                            }
                        }
                    }
                }

                if (calTagList.Count > 0)
                {
                    foreach (string tagname in calTagList)
                    {
                        object val = null;
                        bool isCal = true;

                        //계산식목록 조회
                        DataTable calList = dao.SelectCalTagList(waterNET_mapper, tagname);
                        if (calList != null && calList.Rows.Count != 0)
                        {
                            double dou_cal_val = 0;

                            foreach (DataRow row in calList.Rows)
                            {
                                DataTable caldata = RWIS.GetInstance().SelectAccumulationHourLogData(rwis_mapper, row["CTAGNAME"].ToString());


                                if (caldata != null && calList.Rows.Count != 0)
                                {
                                    foreach (DataRow row2 in caldata.Rows)
                                    {
                                        Hashtable parameter = new Hashtable();
                                        parameter["TAGNAME"] = row2["TAGSN"].ToString();
                                        parameter["TIMESTAMP"] = row2["LOG_TIME"].ToString();
                                        parameter["VALUE"] = row2["CUR_VAL"].ToString();
                                        parameter["CALTAGNAME"] = tagname;
                                        //계산태그값삭제
                                        dao.DeleteAccumulationHourData(waterNET_mapper, parameter);

                                        //계산태그값등록
                                        returnValue += dao.InsertAccumulationHourData(waterNET_mapper, parameter);

                                        DataTable calList2 = dao.SelectCalTagList2(waterNET_mapper, parameter);

                                        if (calList2 != null && calList2.Rows.Count != 0)
                                        {
                                            foreach (DataRow row3 in calList2.Rows)
                                            {
                                                if ("P".Equals(row3["CAL_GBN"].ToString()))
                                                {
                                                    dou_cal_val += Convert.ToDouble(row3["VALUE"].ToString());
                                                }
                                                if ("M".Equals(row3["CAL_GBN"].ToString()))
                                                {
                                                    dou_cal_val -= Convert.ToDouble(row3["VALUE"].ToString());
                                                }
                                            }
                                        }

                                        if ("P".Equals(row["CAL_GBN"].ToString()))
                                        {
                                            val = Convert.ToDouble(row2["CUR_VAL"].ToString());
                                            dou_cal_val += Convert.ToDouble(val.ToString());
                                        }
                                        if ("M".Equals(row["CAL_GBN"].ToString()))
                                        {
                                            val = Convert.ToDouble(row2["CUR_VAL"].ToString());
                                            dou_cal_val -= Convert.ToDouble(val.ToString());
                                        }

                                        val = dou_cal_val;

                                        Hashtable parameter2 = new Hashtable();
                                        parameter2["TAGNAME"] = tagname;
                                        parameter2["VALUE"] = val;
                                        parameter2["TIMESTAMP"] = row2["LOG_TIME"].ToString();

                                        //기존태그값삭제
                                        dao.DeleteAccumulationHourData(waterNET_mapper, parameter2);

                                        //태그값등록
                                        returnValue += dao.InsertAccumulationHourData(waterNET_mapper, parameter2);

                                        RWIS.GetInstance().UpdateLogData(rwis_mapper, parameter);
                                    }
                                }
                            }
                        }
                    }
                }

                rwis_mapper.CommitTransaction();
                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                rwis_mapper.RollbackTransaction();
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
                if (rwis_mapper != null)
                {
                    rwis_mapper.Dispose();
                }
            }

            return returnValue;
        }

        /// <summary>
        /// 1시간적산차 자동, 보정
        /// </summary>
        /// <param name="tagList"></param>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        public int InsertAccumulationHourData(IList<string> tagList, DateTime timestamp)
        {
            EMapper rwis_mapper = null;
            EMapper waterNET_mapper = null;

            int returnValue = 0;

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                try
                {
                    rwis_mapper = new EMapper(AppStatic.RWIS_DATABASE);
                    rwis_mapper.BeginTransaction();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    waterNET_mapper.CommitTransaction();
                    throw new Exception("RWIS 서버에 접속할수 없습니다.");
                }

                if (tagList == null || tagList.Count == 0)
                {
                    throw new Exception("water-NET 실시간 데이터 태그 목록이 존재하지 않습니다.");
                }

                //일반태그, 계산태그 분류
                IList<string> rwisTagList = new List<string>();
                IList<string> calTagList = new List<string>();

                foreach (string tagname in tagList)
                {
                    if (tagname.IndexOf("DUMMY") == -1)
                    {
                        rwisTagList.Add(tagname);
                    }
                    else
                    {
                        calTagList.Add(tagname);
                    }
                }

                DataTable tagData = null;

                if (rwisTagList.Count > 0)
                {
                    tagData = RWIS.GetInstance().SelectAccumulationHourData(rwis_mapper, rwisTagList, timestamp);
                }

                if (tagData != null)
                {
                    foreach (DataRow row in tagData.Rows)
                    {
                        object val = row["VAL"];
                        string tagname = row["TAGSN"].ToString();

                        if (row["VAL"] != DBNull.Value && row["VAL"] != null)
                        {
                            try
                            {
                                //계수 조회
                                object confficient = dao.SelectConfficient(waterNET_mapper, tagname);

                                //계수로 값 계산
                                val = Convert.ToDouble(row["VAL"]) * Convert.ToDouble(confficient);

                                Hashtable parameter = new Hashtable();
                                parameter["TAGNAME"] = tagname;
                                parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHH");
                                parameter["VALUE"] = val;

                                //기존태그값삭제
                                dao.DeleteAccumulationHourData(waterNET_mapper, parameter);

                                //태그값등록
                                returnValue += dao.InsertAccumulationHourData(waterNET_mapper, parameter);
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex);
                            }
                        }
                    }
                }

                if (calTagList.Count > 0)
                {
                    foreach (string tagname in calTagList)
                    {
                        object val = null;
                        bool isCal = true;

                        //계산식목록 조회
                        DataTable calList = dao.SelectCalTagList(waterNET_mapper, tagname);
                        if (calList != null && calList.Rows.Count != 0)
                        {
                            double dou_cal_val = 0;

                            foreach (DataRow row in calList.Rows)
                            {
                                object obj_cal_val = RWIS.GetInstance().SelectAccumulationHourData(rwis_mapper, row["CTAGNAME"].ToString(), timestamp);

                                if (obj_cal_val == DBNull.Value || obj_cal_val == null)
                                {
                                    val = DBNull.Value;
                                    isCal = false;
                                }
                                else
                                {
                                    //계수 조회
                                    object confficient = dao.SelectConfficient(waterNET_mapper, row["CTAGNAME"].ToString());

                                    //계수로 값 계산
                                    obj_cal_val = Convert.ToDouble(obj_cal_val) * Convert.ToDouble(confficient);

                                    Hashtable parameter = new Hashtable();
                                    parameter["TAGNAME"] = row["CTAGNAME"].ToString();
                                    parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHH");
                                    parameter["VALUE"] = obj_cal_val;

                                    //계산태그값삭제
                                    dao.DeleteAccumulationHourData(waterNET_mapper, parameter);

                                    //계산태그값등록
                                    returnValue += dao.InsertAccumulationHourData(waterNET_mapper, parameter);

                                    if ("P".Equals(row["CAL_GBN"].ToString()))
                                    {
                                        dou_cal_val += Convert.ToDouble(obj_cal_val);
                                    }
                                    if ("M".Equals(row["CAL_GBN"].ToString()))
                                    {
                                        dou_cal_val -= Convert.ToDouble(obj_cal_val);
                                    }

                                    val = dou_cal_val;
                                }
                            }

                            if (val != DBNull.Value && val != null && isCal)
                            {
                                try
                                {
                                    //계수 조회
                                    object confficient = dao.SelectConfficient(waterNET_mapper, tagname);

                                    //계수로 값 계산
                                    val = Convert.ToDouble(val) * Convert.ToDouble(confficient);

                                    Hashtable parameter = new Hashtable();
                                    parameter["TAGNAME"] = tagname;
                                    parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHH");
                                    parameter["VALUE"] = val;

                                    //기존태그값삭제
                                    dao.DeleteAccumulationHourData(waterNET_mapper, parameter);

                                    //태그값등록
                                    returnValue += dao.InsertAccumulationHourData(waterNET_mapper, parameter);
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex);
                                }
                            }
                        }
                    }
                }

                rwis_mapper.CommitTransaction();
                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                rwis_mapper.RollbackTransaction();
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
                if (rwis_mapper != null)
                {
                    rwis_mapper.Dispose();
                }
            }

            return returnValue;
        }

        /// <summary>
        /// 야간최소유량 자동, 보정
        /// </summary>
        /// <param name="timestamp"></param>
        public void InsertMinimumNightFlowData(IList<string> tagList, DateTime timestamp)
        {
            EMapper waterNET_mapper = null;

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                foreach (string tagname in tagList)
                {
                    Hashtable parameter = new Hashtable();
                    parameter["TAGNAME"] = tagname;
                    parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMdd");
                    dao.InsertMinimumNightFlowData(waterNET_mapper, parameter);
                }

                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
            }
        }

        /// <summary>
        /// 정시평균수압 자동, 보정
        /// </summary>
        /// <param name="timestamp"></param>
        public void InsertAveragePressure(IList<string> tagList, DateTime timestamp)
        {
            EMapper waterNET_mapper = null;

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                foreach (string tagname in tagList)
                {
                    Hashtable parameter = new Hashtable();
                    parameter["TAGNAME"] = tagname;
                    parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHH");
                    dao.InsertAveragePressure(waterNET_mapper, parameter);
                }

                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
            }
        }

        /// <summary>
        /// 미수집데이터 목록조회
        /// </summary>
        /// <param name="dateTIme"></param>
        /// <returns></returns>
        public IList<string> SelectMissingRealtimeTagList(DateTime timestamp)
        {
            EMapper waterNET_mapper = null;

            IList<string> resultValue = new List<string>();

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                Hashtable parameter = new Hashtable();
                parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHHmm");
                parameter["MISSING_GUBUN"] = "000002";

                DataTable data = dao.SelectMissingRealtimeTagList(waterNET_mapper, parameter);

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        resultValue.Add(row["TAGNAME"].ToString());
                    }
                }

                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
            }

            return resultValue;
        }

        /// <summary>
        /// 미수집데이터 목록조회
        /// </summary>
        /// <param name="dateTIme"></param>
        /// <returns></returns>
        public IList<string> SelectMissingRealtimeTagListAll(DateTime timestamp)
        {
            EMapper waterNET_mapper = null;

            IList<string> resultValue = new List<string>();

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                Hashtable parameter = new Hashtable();
                parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHHmm");

                DataTable data = dao.SelectMissingRealtimeTagListAll(waterNET_mapper, parameter);

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        resultValue.Add(row["TAGNAME"].ToString());
                    }
                }

                foreach (string tagname in resultValue)
                {
                    parameter["TAGNAME"] = tagname;
                    parameter["GATHER_YN"] = "N";
                    parameter["MISSING_GUBUN"] = "000002";

                    if (dao.CheckMissingRealtimeData(waterNET_mapper, parameter) == 0)
                    {
                        dao.InsertMissingRealtimeData(waterNET_mapper, parameter);
                    }
                }

                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
            }

            return resultValue;
        }

        /// <summary>
        /// 미수집데이터 목록조회
        /// </summary>
        /// <param name="dateTIme"></param>
        /// <returns></returns>
        public IList<string> SelectMissingHourTagListAll(DateTime timestamp)
        {
            EMapper waterNET_mapper = null;

            IList<string> resultValue = new List<string>();

            try
            {
                waterNET_mapper = new EMapper(AppStatic.WATERNET_DATABASE);
                waterNET_mapper.BeginTransaction();

                Hashtable parameter = new Hashtable();
                parameter["TIMESTAMP"] = timestamp.ToString("yyyyMMddHH");

                DataTable data = dao.SelectMissingHourTagListAll(waterNET_mapper, parameter);

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        resultValue.Add(row["TAGNAME"].ToString());
                    }
                }

                waterNET_mapper.CommitTransaction();
            }
            catch (Exception ex)
            {
                waterNET_mapper.RollbackTransaction();
                throw ex;
            }
            finally
            {
                if (waterNET_mapper != null)
                {
                    waterNET_mapper.Dispose();
                }
            }

            return resultValue;
        }
    }
}
