﻿using System.Text;
using System.Data;
using System.Collections;
using EMFrame.dm;

namespace WaterNETServer.IF_iwater.dao
{
    public class ListDao
    {
        public ListDao() { }

        /// <summary>
        /// 실시간 수집 태그 목록 전체를 가져온다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="tableName"></param>
        internal void selectRealtimeDao(EMapper manager, DataSet dataset, string tableName, Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select /*+ index_desc( a if_gather_realtime_pk ) index_desc( b xpkif_ihtags )*/");
            query.AppendLine("       a.tagname");
            query.AppendLine("     , to_char(a.timestamp, 'yyyy/mm/dd hh24:mi:ss') timestamp");
            query.AppendLine("     , b.description");
            query.AppendLine("     , round(a.value, 2) value");
            query.AppendLine("  from if_gather_realtime a");
            query.AppendLine("     , (select *");
            query.AppendLine("          from if_ihtags");
            query.AppendLine("         where 1 = 1");

            if (!param["combobox4"].ToString().Equals(""))
            {
                query.AppendLine("           and br_code = '" + param["combobox4"].ToString() + "'");
            }
            if (!param["combobox5"].ToString().Equals(""))
            {
                query.AppendLine("           and fn_code = '" + param["combobox5"].ToString() + "'");
            }
            if (!param["textbox1"].ToString().Equals(""))
            {
                query.AppendLine("           and tagname like '%" + param["textbox1"].ToString() + "%'");
            }
            if (!param["textbox2"].ToString().Equals(""))
            {
                query.AppendLine("           and description like '%" + param["textbox2"].ToString() + "%'");
            }

            query.AppendLine("       ) b");
            query.AppendLine(" where a.tagname = b.tagname");

            if (!param["dateTimePicker1"].ToString().Equals(""))
            {
                query.Append("   and a.timestamp between to_date('" + param["dateTimePicker1"].ToString() + "', 'yyyymmdd')");
            }
            if (!param["dateTimePicker2"].ToString().Equals(""))
            {
                query.AppendLine(" and to_date('" + param["dateTimePicker2"].ToString() + "', 'yyyymmdd')");
            }

            query.AppendLine("   and rownum < " + param["cnt"].ToString() + "");

            manager.FillDataSetScript(dataset, tableName, query.ToString(), null);
        }

        /// <summary>
        /// 실시간 수집 태그 목록 전체를 가져온다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="tableName"></param>
        internal void selectAccumulationTodayDao(EMapper manager, DataSet dataset, string tableName, Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select /*+ index_desc( a if_accu_today_pk ) index_desc ( b xpkif_ihtags ) */");
            query.AppendLine("       a.tagname");
            query.AppendLine("     , to_char(a.timestamp, 'yyyy/mm/dd hh24:mi:ss') timestamp");
            query.AppendLine("     , b.description");
            query.AppendLine("     , round(a.value, 2) value");
            query.AppendLine("  from if_accumulation_today a");
            query.AppendLine("     , (select *");
            query.AppendLine("          from if_ihtags");
            query.AppendLine("         where 1 = 1");

            if (!param["combobox4"].ToString().Equals(""))
            {
                query.AppendLine("           and br_code = '" + param["combobox4"].ToString() + "'");
            }
            if (!param["combobox5"].ToString().Equals(""))
            {
                query.AppendLine("           and fn_code = '" + param["combobox5"].ToString() + "'");
            }
            if (!param["textbox1"].ToString().Equals(""))
            {
                query.AppendLine("           and tagname like '%" + param["textbox1"].ToString() + "%'");
            }
            if (!param["textbox2"].ToString().Equals(""))
            {
                query.AppendLine("           and description like '%" + param["textbox2"].ToString() + "%'");
            }
            query.AppendLine("       ) b");
            query.AppendLine(" where a.tagname = b.tagname");

            if (!param["dateTimePicker1"].ToString().Equals(""))
            {
                query.Append("   and a.timestamp between to_date('" + param["dateTimePicker1"].ToString() + "', 'yyyymmdd')");
            }
            if (!param["dateTimePicker2"].ToString().Equals(""))
            {
                query.AppendLine(" and to_date('" + param["dateTimePicker2"].ToString() + "', 'yyyymmdd')");
            }
            query.AppendLine("   and rownum < 100");

            manager.FillDataSetScript(dataset, tableName, query.ToString(), null);
        }
    }
}
