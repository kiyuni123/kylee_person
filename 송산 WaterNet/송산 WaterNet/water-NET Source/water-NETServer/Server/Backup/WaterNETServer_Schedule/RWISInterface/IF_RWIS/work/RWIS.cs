﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNETServer.IF_RWIS.dao;
using EMFrame.dm;
using System.Collections;
using System.Data;

namespace WaterNETServer.IF_RWIS.work
{
    public class RWIS
    {
        private static RWIS work = null;
        private RWISDao dao = null;

        private RWIS()
        {
            dao = RWISDao.GetInstance();
        }

        public static RWIS GetInstance()
        {
            if (work == null)
            {
                work = new RWIS();
            }

            return work;
        }

        public object SelectGatherRealtimeData(EMapper manager, string tagname, DateTime timestamp)
        {
            object returnValue = null;

            try
            {
                returnValue = dao.SelectGatherRealtimeData(manager, tagname, timestamp);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnValue;
        }

        public DataTable SelectGatherRealtimeData(EMapper manager, IList<string> tagList, DateTime timestamp)
        {
            DataTable returnValue = null;

            try
            {
                returnValue = dao.SelectGatherRealtimeData(manager, tagList, timestamp);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnValue;
        }

        public object SelectAccumulationHourData(EMapper manager, string tagname, DateTime timestamp)
        {
            object returnValue = null;

            try
            {
                returnValue = dao.SelectAccumulationHourData(manager, tagname, timestamp);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnValue;
        }

        public DataTable SelectAccumulationHourData(EMapper manager, IList<string> tagList, DateTime timestamp)
        {
            DataTable returnValue = null;

            try
            {
                returnValue = dao.SelectAccumulationHourData(manager, tagList, timestamp);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnValue;
        }

        public DataTable SelectAccumulationHourLogData(EMapper manager, string tagname)
        {
            DataTable returnValue = null;

            try
            {
                returnValue = dao.SelectAccumulationHourLogData(manager, tagname);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnValue;
        }

        public DataTable SelectAccumulationHourLogData(EMapper manager, IList<string> tagList)
        {
            DataTable returnValue = null;

            try
            {
                returnValue = dao.SelectAccumulationHourLogData(manager, tagList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnValue;
        }

        public void UpdateLogData(EMapper manager, Hashtable parameter)
        {
            try
            {
                dao.UpdateLogData(manager, parameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
