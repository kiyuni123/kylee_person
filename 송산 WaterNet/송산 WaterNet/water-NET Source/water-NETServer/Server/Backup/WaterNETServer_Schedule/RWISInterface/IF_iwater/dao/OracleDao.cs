﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using EMFrame.dm;
using WaterNETServer.Warning.work;

namespace WaterNETServer.IF_iwater.dao
{
    public class OracleDao
    {
        public OracleDao() { }

        /// <summary>
        /// 태그구분별 카운트를 가져온다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="tableName"></param>
        internal int selectTagCount_Dao(EMapper manager, string taggbn)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" SELECT COUNT(*) ");
            query.AppendLine("   FROM IF_TAG_GBN ");
            query.AppendLine("  WHERE TAG_GBN = '"+ taggbn + "'");

            return Convert.ToInt32(manager.ExecuteScriptScalar(query.ToString(), null));
        }

        /// <summary>
        /// 태그구분별 카운트를 가져온다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="tableName"></param>
        internal int selectTagCount_Dao(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" SELECT COUNT(*)");
            query.AppendLine("   FROM IF_IHTAGS ");
            query.AppendLine("  WHERE USE_YN = 'Y' ");

            return Convert.ToInt32(manager.ExecuteScriptScalar(query.ToString(), null));
        }

        /// <summary>
        /// 실시간 수집 태그 목록 전체를 가져온다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="tableName"></param>
        internal void selectRealtimeTags_Dao(EMapper manager, DataSet dataset, string tableName)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" SELECT TAGNAME ");
            query.AppendLine("      , COEFFICIENT");
            query.AppendLine("      , DESCRIPTION");
            query.AppendLine("   FROM IF_IHTAGS ");
            query.AppendLine("  WHERE USE_YN = 'Y' ");

            manager.FillDataSetScript(dataset, tableName, query.ToString(), null);
        }

        /// <summary>
        /// 1시간 적산 데이터 수집
        /// </summary>
        internal void insertAccumulation_Hour_Dao(EMapper manager, string cTime)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_ACCUMULATION_HOUR A");
            query.AppendLine("USING ( SELECT A.TAGNAME");
            query.AppendLine("             , A.TIMESTAMP");
            query.AppendLine("             , A.VALUE - B.VALUE AS VALUE");
            query.AppendLine("          FROM (SELECT DISTINCT C.TAGNAME");
            query.AppendLine("                     , A.TIMESTAMP");
            query.AppendLine("                     , A.VALUE");
            query.AppendLine("                  FROM IF_GATHER_REALTIME A");
            query.AppendLine("                     , IF_IHTAGS B");
            query.AppendLine("                     , IF_TAG_GBN C");
            query.AppendLine("                     , IF_TIMESTAMP_YYYYMMDDHH24 D");
            query.AppendLine("                 WHERE A.TAGNAME   = B.TAGNAME");
            query.AppendLine("                   AND A.TIMESTAMP = TO_DATE('" + cTime + "', 'YYYYMMDDHH24')");
            query.AppendLine("                   AND A.TIMESTAMP = TO_DATE(D.YYYYMMDDHH24, 'YYYYMMDDHH24')");
            query.AppendLine("                   AND B.TAGNAME   = C.DUMMY_RELATION");
            query.AppendLine("                   AND B.USE_YN    = 'Y'");
            query.AppendLine("                   AND C.TAG_GBN   IN ('FRQ', 'FRQ_I', 'FRQ_O')");
            query.AppendLine("               ) A");
            query.AppendLine("             , (SELECT DISTINCT C.TAGNAME");
            query.AppendLine("                     , A.TIMESTAMP + 1/24 AS TIMESTAMP");
            query.AppendLine("                     , A.VALUE");
            query.AppendLine("                  FROM IF_GATHER_REALTIME A");
            query.AppendLine("                     , IF_IHTAGS B");
            query.AppendLine("                     , IF_TAG_GBN C");
            query.AppendLine("                     , IF_TIMESTAMP_YYYYMMDDHH24 D");
            query.AppendLine("                 WHERE A.TAGNAME   = B.TAGNAME");
            query.AppendLine("                   AND A.TIMESTAMP = TO_DATE('" + cTime + "', 'YYYYMMDDHH24') - 1/24");
            query.AppendLine("                   AND A.TIMESTAMP = TO_DATE(D.YYYYMMDDHH24, 'YYYYMMDDHH24')");
            query.AppendLine("                   AND B.TAGNAME   = C.DUMMY_RELATION");
            query.AppendLine("                   AND B.USE_YN    = 'Y'");
            query.AppendLine("                   AND C.TAG_GBN   IN ('FRQ', 'FRQ_I', 'FRQ_O')");
            query.AppendLine("               ) B");
            query.AppendLine("         WHERE A.TAGNAME   = B.TAGNAME");
            query.AppendLine("           AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("       ) B");
            query.AppendLine("   ON (        A.TAGNAME = B.TAGNAME");
            query.AppendLine("           AND A.TIMESTAMP = B.TIMESTAMP ");
            query.AppendLine("      )");
            query.AppendLine(" WHEN MATCHED THEN");
            query.AppendLine("      UPDATE SET A.VALUE = B.VALUE");
            query.AppendLine(" WHEN NOT MATCHED THEN");
            query.AppendLine("      INSERT (A.TAGNAME, A.TIMESTAMP, A.VALUE)");
            query.AppendLine("      VALUES (B.TAGNAME, B.TIMESTAMP, B.VALUE)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 1시간 적산 데이터 삭제
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="cTime"></param>
        internal void deleteAccumulation_Hour_Dao(EMapper manager, string cTime)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_ACCUMULATION_HOUR WHERE TIMESTAMP = TO_DATE('" + cTime + "', 'YYYYMMDDHH24')");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 1일 적산 데이터 수집 (전일적산)
        /// </summary>
        internal void insertAccumulation_Day_Dao(EMapper manager, string cTime)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_ACCUMULATION_YESTERDAY A");
            query.AppendLine("USING ( SELECT REPLACE(B.TAGNAME, '_1H', '_YD') AS TAGNAME");
            query.AppendLine("             , TO_DATE(TO_CHAR(A.TIMESTAMP, 'YYYYMMDD'), 'YYYYMMDD') AS TIMESTAMP");
            query.AppendLine("             , SUM(A.VALUE) AS VALUE");
            query.AppendLine("          FROM IF_ACCUMULATION_HOUR A");
            query.AppendLine("             , IF_TAG_GBN B");
            query.AppendLine("         WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("           AND B.TAG_GBN IN ('FRQ', 'FRQ_O')");
            query.AppendLine("         GROUP BY B.TAGNAME, TO_DATE(TO_CHAR(A.TIMESTAMP, 'YYYYMMDD'), 'YYYYMMDD')");
            query.AppendLine("        HAVING TO_DATE(TO_CHAR(A.TIMESTAMP, 'YYYYMMDD'), 'YYYYMMDD') = TO_DATE('" + cTime + "', 'YYYYMMDD') - 1");
            query.AppendLine("      ) B");
            query.AppendLine("   ON (       A.TAGNAME   = B.TAGNAME");
            query.AppendLine("          AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("      UPDATE SET A.VALUE = B.VALUE");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("      INSERT (A.TAGNAME, A.TIMESTAMP, A.VALUE)");
            query.AppendLine("      VALUES (B.TAGNAME, B.TIMESTAMP, B.VALUE)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 1일 적산 데이터 보정 (전일적산)
        /// </summary>
        internal void insertModification_Day_Dao(EMapper manager, string cTime, string cTime2)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_ACCUMULATION_YESTERDAY A");
            query.AppendLine("USING (SELECT REPLACE(B.TAGNAME, '_1H', '_YD') TAGNAME");
            query.AppendLine("            , TO_DATE(TO_CHAR(A.TIMESTAMP, 'YYYYMMDD'), 'YYYYMMDD') TIMESTAMP");
            query.AppendLine("            , SUM(A.VALUE) VALUE");
            query.AppendLine("         FROM IF_ACCUMULATION_HOUR A");
            query.AppendLine("            , IF_TAG_GBN B");
            query.AppendLine("        WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("          AND B.TAG_GBN IN ('FRQ', 'FRQ_O')");
            query.AppendLine("        GROUP BY B.TAGNAME, TO_DATE(TO_CHAR(A.TIMESTAMP, 'YYYYMMDD'), 'YYYYMMDD')");
            query.AppendLine("       HAVING TO_DATE(TO_CHAR(A.TIMESTAMP, 'YYYYMMDD'), 'YYYYMMDD') BETWEEN TO_DATE('" + cTime + "', 'YYYYMMDD') AND ((TO_DATE('" + cTime2 + "', 'YYYYMMDD') + 1) - 1/24/60)"); // 입력받은 날짜
            query.AppendLine("      ) B");
            query.AppendLine("   ON (       A.TAGNAME   = B.TAGNAME");
            query.AppendLine("          AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("   UPDATE SET A.VALUE = B.VALUE");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("      INSERT (A.TAGNAME, A.TIMESTAMP, A.VALUE)");
            query.AppendLine("      VALUES (B.TAGNAME, B.TIMESTAMP, B.VALUE)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 금일 적산 (계산)
        /// </summary>
        internal void InsertAccumulation_ToDay_Dao(EMapper manager, string cTime)
        {
            //string cof = "4";   // 금일적산 계산은 4분 전꺼를 계산한다.

            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_ACCUMULATION_TODAY A");
            query.AppendLine("USING ( SELECT A.TAGNAME || '_TD' AS TAGNAME");
            query.AppendLine("             , A.TIMESTAMP");
            query.AppendLine("             , A.VALUE - B.VALUE AS VALUE");
            query.AppendLine("          FROM (SELECT A.TAGNAME");
            query.AppendLine("                     , A.TIMESTAMP");
            query.AppendLine("                     , A.VALUE");
            query.AppendLine("                  FROM IF_GATHER_REALTIME A");
            query.AppendLine("                     , IF_TAG_GBN B");
            query.AppendLine("                 WHERE A.TAGNAME   = B.TAGNAME");
            query.AppendLine("                   AND A.TIMESTAMP = TO_DATE('" + cTime + "', 'YYYYMMDDHH24MI')  - (1/24/60) * 4");
            query.AppendLine("                   AND B.TAG_GBN   = 'TT'");
            query.AppendLine("               ) A");
            query.AppendLine("             , (SELECT A.TAGNAME");
            query.AppendLine("                     , A.VALUE");
            query.AppendLine("                  FROM IF_GATHER_REALTIME A");
            query.AppendLine("                     , IF_TAG_GBN B");
            query.AppendLine("                 WHERE A.TAGNAME   = B.TAGNAME");
            query.AppendLine("                   AND A.TIMESTAMP = TRUNC(TO_DATE('" + cTime + "', 'YYYYMMDDHH24MI') - (1/24/60) * 4, 'DD') ");
            query.AppendLine("                   AND B.TAG_GBN   = 'TT' ");
            query.AppendLine("               ) B");
            query.AppendLine("         WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("      ) B");
            query.AppendLine("ON    (        A.TAGNAME = B.TAGNAME");
            query.AppendLine("         AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("  UPDATE SET A.VALUE = B.VALUE");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("  INSERT (A.TAGNAME, A.TIMESTAMP, A.VALUE)");
            query.AppendLine("  VALUES (B.TAGNAME, B.TIMESTAMP, B.VALUE)");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 금일 적산 삭제 (10분전 데이터 삭제)
        /// </summary>
        internal void DeleteAccumulation_ToDay_Dao(EMapper manager, string cTime)
        {
            string cof = "14";   // 삭제는 14분전 데이터를 삭제

            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_ACCUMULATION_TODAY WHERE TIMESTAMP < TO_DATE('" + cTime + "', 'YYYYMMDDHH24MI') - (1/24/60) * " + cof);

            manager.ExecuteScript(query.ToString(), null);

            // 금일적산 인덱스를 Rebuild한다.
            // 데이터가 쌓이고, 삭제되고를 반복하기 때문에 속도, 결과조회에 영향을 받기 때문
            manager.ExecuteScript("ALTER INDEX IF_ACCU_TODAY_PK REBUILD", null);
        }

        /// <summary>
        /// ihRawData 에서 가져온 데이터를 IF_GATHER_REALTIME 테이블에 넣는다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="dataMember"></param>
        internal int insertRawData_Dao(EMapper manager, DataSet tagList, DataSet dataset, string dataMember)
        {
            int insertCnt = 0;

            Hashtable _errorMessageBox = new Hashtable();
            StringBuilder query = new StringBuilder();

            // 태그목록과 계수 값을 해쉬테이블에 저장 (태그별로 수집 값에 계수를 곱한다.) 
            // 유량계별로(i-water) 수집값을 일정비율로 줄여서 저장하는데 표현할 때는 그것을 곱해서 표현해야함
            Hashtable hTagList = new Hashtable();
            DataTable list = tagList.Tables[0];

            foreach (DataRow row in list.Rows)
            {
                hTagList[row[0].ToString()] = row[1].ToString();
            }

            query.AppendLine(" INSERT INTO IF_GATHER_REALTIME (");
            query.AppendLine(" 	 TAGNAME");         //
            query.AppendLine(" 	,TIMESTAMP");       // 
            query.AppendLine(" 	,VALUE");           // 
            query.AppendLine(" 	,VALUE_QUALITY");   //
            query.AppendLine(" ) VALUES (");
            query.AppendLine(" 	 :1");
            query.AppendLine(" 	,:2");
            query.AppendLine(" 	,:3");
            query.AppendLine(" 	,:4");
            query.AppendLine(" )");

            DataTableCollection dta = dataset.Tables;

            foreach (DataTable dt in dta)
            {
                DataRowCollection drc = dataset.Tables[dt.TableName].Rows;

                foreach (DataRow row in drc)
                {
                    IDataParameter[] parameters =  {
	                                                     new OracleParameter("1", OracleDbType.Varchar2)
                                                        ,new OracleParameter("2", OracleDbType.Date)
	                                                    ,new OracleParameter("3", OracleDbType.Varchar2)
	                                                    ,new OracleParameter("4", OracleDbType.Varchar2)
                                                       };

                    // 수집 값
                    double value1 = Convert.ToDouble(row[2].ToString());
                    // 계수 값 (Coefficient)
                    int value2 = Convert.ToInt32(hTagList[row[0].ToString()].ToString());

                    parameters[0].Value = row[0].ToString();
                    parameters[1].Value = Convert.ToDateTime(row[1].ToString());
                    parameters[2].Value = (value1 * value2).ToString();                 // 수집 값 * 계수 값
                    parameters[3].Value = row[3].ToString();

                    // 실시간 수집값 입력 오류시 Exception처리 출력
                    // TODO : 다시 만들어야함 .. 오동작
                    if (_errorMessageBox.ContainsKey("insertRawData_Dao"))
                    {
                        _errorMessageBox.Remove("insertRawData_Dao");
                        _errorMessageBox.Add("insertRawData_Dao", row[0].ToString() + row[1].ToString());
                    }
                    else
                    {
                        _errorMessageBox.Add("insertRawData_Dao", row[0].ToString() + row[1].ToString());
                    }

                    insertCnt += manager.ExecuteScript(query.ToString(), parameters);
                }
            }

            return insertCnt;
        }

        /// <summary>
        /// 수집대상이지만 현재시간에 수집되지 않은 것들 기록
        /// </summary>
        /// <param name="result1">실시간 태그목록</param>
        /// <param name="result2">수집해온 실시간 태그값</param>
        /// <param name="cTime">수집 시간</param>
        [Obsolete("Not used anymore", true)]
        internal int MissingRawData_Dao(EMapper manager, DataSet result1, DataSet result2, Hashtable cTime)
        {
            int returnValue = 0;
            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_MISSING_REALTIME (TAGNAME, TIMESTAMP, GATHER_YN, MISSING_GUBUN)VALUES (:1, :2, 'N', '000001')");

            // 수집할 태그 목록
            Hashtable hTagList = new Hashtable();
            DataTable list = result1.Tables[0];
            foreach (DataRow row in list.Rows)
            {
                hTagList.Add(row[0].ToString(), row[2].ToString());
            }

            // 수집된 태그 목록
            Hashtable resultTagList = new Hashtable();
            list = result2.Tables[0];
            foreach (DataRow row in list.Rows)
            {
                resultTagList.Add(row[0].ToString(), Convert.ToDateTime(row[1].ToString()));
            }

            // 미수집 된 건수
            int missingCnt = 0;
            // 미수집 태그 처리
            foreach (DictionaryEntry hList in hTagList)
            {
                int matchCnt = 0;
                bool virtualTag = false;
                foreach (DictionaryEntry rList in resultTagList)
                {
                    if (hList.Key.ToString().IndexOf("_YD", StringComparison.Ordinal) >= 0) { virtualTag = true; break; }
                    if (hList.Key.ToString().IndexOf("_1H", StringComparison.Ordinal) >= 0) { virtualTag = true; break; }
                    if (hList.Key.ToString().IndexOf("_TD", StringComparison.Ordinal) >= 0) { virtualTag = true; break; }

                    if (hList.Key.Equals(rList.Key))
                    {
                        matchCnt++;
                        break;
                    }

                }

                // 미수집 태그 기록
                if (matchCnt == 0 && !virtualTag)
                {
                    IDataParameter[] parameters =  {
                                                           new OracleParameter("1", OracleDbType.Varchar2)
                                                          ,new OracleParameter("2", OracleDbType.Date)
                                                       };

                    parameters[0].Value = hList.Key.ToString();
                    parameters[1].Value = Convert.ToDateTime(cTime["<="].ToString());

                    returnValue += manager.ExecuteScript(query.ToString(), parameters);

                    missingCnt++;
                }
            }

            // 경고관리에 기록
            if (missingCnt > 0)
            {
                InsertWarningData_Dao(manager, DateTime.ParseExact(cTime["<="].ToString(), "yyyy-M-d H:m", null).ToString("yyyyMMddHHmm"), missingCnt, "0005", "000001");
            }

            return returnValue;
        }

        /// <summary>
        /// 이전 수집 데이터 있는지 확인값을 if_missing_realtime에 넣는다
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="result3">최종시간 -1분의 데이터</param>
        /// <param name="cTime">현재 수집시간 -1</param>
        internal int MissingRawData_Dao(EMapper manager, DataSet result3)
        {
            int returnValue = 0;
            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_MISSING_REALTIME (TAGNAME, TIMESTAMP, GATHER_YN, MISSING_GUBUN)VALUES (:1, :2, 'N', '000002')");

            int totalCnt = 0;
            List<string> listMissingTag = new List<string>();
            List<DateTime> listMissingTime = new List<DateTime>();

            Hashtable Condition = new Hashtable();

            // 누락 데이터의 시작, 종료 변수
            DateTime timeFrom = DateTime.Now;               // 시작시간은 현재 시간으로 (이전데이터는 무조건 현재 시간보다 작은 시간임)
            DateTime timeTo = DateTime.Now.AddYears(-10);  // 종료시간을 강제로 -10년 시켜놨음 (이전데이터는 무조건 -10년보다 큰 시간임)

            DataTable list = result3.Tables[0];
            foreach (DataRow row in list.Rows)
            {
                listMissingTag.Add(row[0].ToString());
                listMissingTime.Add(Convert.ToDateTime(row[1].ToString()));

                // 왼쪽 더 크면 시작시간에 대입 > 0
                if (DateTime.Compare(timeFrom, Convert.ToDateTime(row[1].ToString())) > 0)
                {
                    timeFrom = Convert.ToDateTime(row[1].ToString());
                }

                // 오른쪽이 더 크면 종료시간에 대입 < 0
                if (DateTime.Compare(timeTo, Convert.ToDateTime(row[1].ToString())) < 0)
                {
                    timeTo = Convert.ToDateTime(row[1].ToString());
                }

                totalCnt++;
            }

            if (totalCnt > 0)
            {
                string cTime = timeFrom.ToString("yyyyMMddHHmm") + timeTo.ToString("yyyyMMddHHmm");

                Condition.Add("TAGNAME", listMissingTag);
                Condition.Add("TIMESTAMP", listMissingTime);

                OracleCommand oracleCommand = new OracleCommand();
                oracleCommand.CommandText = query.ToString();
                oracleCommand.Connection = manager.Connection;
                oracleCommand.ArrayBindCount = ((List<string>)Condition["TAGNAME"]).Count;

                OracleParameter parmTagList = new OracleParameter("1", OracleDbType.Varchar2);
                parmTagList.Direction = ParameterDirection.Input;
                parmTagList.Value = ((List<string>)Condition["TAGNAME"]).ToArray();
                oracleCommand.Parameters.Add(parmTagList);

                OracleParameter paramTimestampList = new OracleParameter("2", OracleDbType.Date);
                paramTimestampList.Direction = ParameterDirection.Input;
                paramTimestampList.Value = ((List<DateTime>)Condition["TIMESTAMP"]).ToArray();
                oracleCommand.Parameters.Add(paramTimestampList);

                returnValue = oracleCommand.ExecuteNonQuery();

                // 경고관리에 기록
                if (((List<string>)Condition["TAGNAME"]).Count > 0)
                {
                    InsertWarningData_Dao(manager, cTime, ((List<string>)Condition["TAGNAME"]).Count, "0005", "000002");
                }
            }

            return returnValue;
        }

        /// <summary>
        /// 공통코드에서 코드명을 반환
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="pcode"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private string getCodeName(EMapper manager, string pcode, string code)
        {
            string codeName = string.Empty;

            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT CODE_NAME FROM CM_CODE WHERE PCODE = :1 AND CODE = :2 ");
            IDataParameter[] param = { 
                                             new OracleParameter("1", OracleDbType.Varchar2) 
                                            ,new OracleParameter("2", OracleDbType.Varchar2)
                                         };
            param[0].Value = pcode;
            param[1].Value = code;

            return manager.ExecuteScriptScalar(query.ToString(), param).ToString();
        }

        /// <summary>
        /// 경고관리에 데이터 입력
        /// </summary>
        /// <param name="cTime"></param>
        /// <param name="missingCnt"></param>
        private void InsertWarningData_Dao(EMapper manager, string cTime, int missingCnt, string pcode, string code)
        {
            /// 경고관리가 특별히 키가 존재하는것도 아니고 공용으로 사용하는 것이므로
            /// SERIAL_NO 를 사용해서 경고 메시지를 찾고 (키로 사용함)
            /// 경고 메시지를 구분자(delimeter)를 이용해서 관리함
            /////////////////////////////////////////////////////////////////////////////////////////////////
            // #############################################:#########:******:************************:$$$$$
            // 실시간 데이터 미수집(2011년04월06일 11시15분):1 건 발생:->복구:2011년04월06일 12시 15분:1 건
            // Token1                                       :Token2   :Token3:Token4                  :Token5
            // -->                   Warn Message                  <--:-->        Recover Message         <--
            // -->         경고관리에서 메시지 입력                <--:-->      복구 메시지 입력          <--
            /////////////////////////////////////////////////////////////////////////////////////////////////

            Hashtable warningCondition = new Hashtable();

            // 에러구분
            string _gatherWarnMessage = "MISS_RT_";
            // 에러발생시간 키
            string _serialTime = cTime;
            // 구분자
            string _deli = ":";

            // 메시지
            string _msg_pre = string.Empty;
            // 에러발생시간 메시지
            string _warnTime = string.Empty;
            // 메시지
            string _msg_post = string.Empty;
            // 메시지
            string _msg_end = string.Empty;
            // 복구구분 메시지
            string _recoverGbn = string.Empty;

            if ("000001".Equals(code))
            {
                _msg_pre = getCodeName(manager, pcode, code) + "(";
                _warnTime = Convert.ToDateTime(DateTime.ParseExact(cTime, "yyyyMMddHHmm", null)).ToString("yyyy년MM월dd일 HH시mm분");

                _msg_post = ")";
                _msg_end = " 건";
                _recoverGbn = "->복구";
            }
            if ("000002".Equals(code))
            {
                //_msg_pre = getCodeName(oracleDBManager, pcode, code);
                _warnTime = Convert.ToDateTime(DateTime.ParseExact(cTime.Substring(0, 12), "yyyyMMddHHmm", null)).ToString("yyyy년MM월dd일 HH시mm분")
                          + "~"
                          + Convert.ToDateTime(DateTime.ParseExact(cTime.Substring(12, 12), "yyyyMMddHHmm", null)).ToString("yyyy년MM월dd일 HH시mm분");

                _recoverGbn = "->자동복구";
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // 경고관리메시지
            string _WarnMessage = _msg_pre + _warnTime + _msg_post + _deli + missingCnt + _msg_end;

            // 복구시간 (에러 발생시는 해결시간이 없음)
            string _recoverTime = string.Empty;
            // 복구건수
            int _recoverCnt = 0;

            // 복구메시지
            string _recoverMessage = string.Empty;

            if ("000001".Equals(code))
            {
                _recoverMessage = _recoverGbn + _deli + _recoverTime + _deli + _recoverCnt + _msg_end;
            }
            if ("000002".Equals(code))
            {
                _recoverMessage = _recoverGbn;
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            WarningWork work = WarningWork.GetInstance();
            // 업무구분
            warningCondition.Add("WORK_GBN", "0005");
            // 지역코드
            warningCondition.Add("LOC_CODE", "");
            // 경고구분
            warningCondition.Add("WAR_CODE", code);
            // 에러발생 키
            warningCondition.Add("SERIAL_NO", _gatherWarnMessage + _serialTime);
            // 에러발생 메시지 
            warningCondition.Add("REMARK", _WarnMessage + _deli + _recoverMessage);

            if ("000002".Equals(code))
            {
                // 수정여부
                warningCondition.Add("FIXED_YN", "Y");
            }

            work.InsertWarningData(warningCondition);
        }

        /// <summary>
        /// ihTags에서 가져온 태그를 임시 테이블(IF_IHTAGS_ORIGIN)에 넣는다. 
        /// 
        /// 수정필요 : 1회만 실행 (중복 체크 및 업데이트)
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="dataMember"></param>
        [Obsolete("Not used anymore", true)]
        public void insertTagsAllDao(EMapper manager, DataSet dataset, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            #region 쿼리문
            query.AppendLine(" INSERT INTO IF_IHTAGS_ORIGIN (");
            query.AppendLine(" 	 TAGNAME");
            query.AppendLine(" 	,DESCRIPTION");
            query.AppendLine(" 	,ENGUNITS");
            query.AppendLine(" 	,COMMENTS");
            query.AppendLine(" 	,DATATYPE");
            query.AppendLine(" 	,FIXEDSTRINGLENGTH");
            query.AppendLine(" 	,COLLECTORNAME");
            query.AppendLine(" 	,SOURCEADDRESS");
            query.AppendLine(" 	,COLLECTIONTYPE");
            query.AppendLine(" 	,COLLECTIONINTERVAL");
            query.AppendLine(" 	,COLLECTIONOFFSET");
            query.AppendLine(" 	,LOADBALANCING");
            query.AppendLine(" 	,TIMESTAMPTYPE");
            query.AppendLine(" 	,HIENGINEERINGUNITS");
            query.AppendLine(" 	,LOENGINEERINGUNITS");
            query.AppendLine(" 	,INPUTSCALING");
            query.AppendLine(" 	,HISCALE");
            query.AppendLine(" 	,LOSCALE");
            query.AppendLine(" 	,COLLECTORCOMPRESSION");
            query.AppendLine(" 	,COLLECTORDEADBANDPERCENTRANGE");
            query.AppendLine(" 	,ARCHIVECOMPRESSION");
            query.AppendLine(" 	,ARCHIVEDEADBANDPERCENTRANGE");
            query.AppendLine(" 	,COLLECTORGENERAL1");
            query.AppendLine(" 	,COLLECTORGENERAL2");
            query.AppendLine(" 	,COLLECTORGENERAL3");
            query.AppendLine(" 	,COLLECTORGENERAL4");
            query.AppendLine(" 	,COLLECTORGENERAL5");
            query.AppendLine(" 	,READSECURITYGROUP");
            query.AppendLine(" 	,WRITESECURITYGROUP");
            query.AppendLine(" 	,ADMINISTRATORSECURITYGROUP");
            query.AppendLine(" 	,CALCULATION");
            query.AppendLine(" 	,LASTMODIFIED");
            query.AppendLine(" 	,LASTMODIFIEDUSER");
            query.AppendLine(" 	,COLLECTORTYPE");
            query.AppendLine(" 	,STOREMILLISECONDS");
            query.AppendLine(" 	,UTCBIAS");
            query.AppendLine(" 	,AVERAGECOLLECTIONTIME");
            query.AppendLine(" 	,COLLECTIONDISABLED");
            query.AppendLine(" 	,COLLECTORCOMPRESSIONTIMEOUT");
            query.AppendLine(" 	,ARCHIVECOMPRESSIONTIMEOUT");
            query.AppendLine(" 	,TIMEZONE");
            query.AppendLine(" 	,DAYLIGHTSAVINGTIME");
            query.AppendLine(" 	,ROWCOUNT");
            query.AppendLine(" 	,INTERFACEABSOLUTEDEADBANDING");
            query.AppendLine(" 	,INTERFACEABSOLUTEDEADBAND");
            query.AppendLine(" 	,ARCHIVEABSOLUTEDEADBANDING");
            query.AppendLine(" 	,ARCHIVEABSOLUTEDEADBAND");
            query.AppendLine(" 	,SPIKELOGIC");
            query.AppendLine(" 	,SPIKELOGICOVERRIDE");
            query.AppendLine(" 	,STEPVALUE");
            query.AppendLine(" ) VALUES (");
            query.AppendLine(" 	 :1");
            query.AppendLine(" 	,:2");
            query.AppendLine(" 	,:3");
            query.AppendLine(" 	,:4");
            query.AppendLine(" 	,:5");
            query.AppendLine(" 	,:6");
            query.AppendLine(" 	,:7");
            query.AppendLine(" 	,:8");
            query.AppendLine(" 	,:9");
            query.AppendLine(" 	,:10");
            query.AppendLine(" 	,:11");
            query.AppendLine(" 	,:12");
            query.AppendLine(" 	,:13");
            query.AppendLine(" 	,:14");
            query.AppendLine(" 	,:15");
            query.AppendLine(" 	,:16");
            query.AppendLine(" 	,:17");
            query.AppendLine(" 	,:18");
            query.AppendLine(" 	,:19");
            query.AppendLine(" 	,:20");
            query.AppendLine(" 	,:21");
            query.AppendLine(" 	,:22");
            query.AppendLine(" 	,:23");
            query.AppendLine(" 	,:24");
            query.AppendLine(" 	,:25");
            query.AppendLine(" 	,:26");
            query.AppendLine(" 	,:27");
            query.AppendLine(" 	,:28");
            query.AppendLine(" 	,:29");
            query.AppendLine(" 	,:30");
            query.AppendLine(" 	,:31");
            query.AppendLine(" 	,:32");
            query.AppendLine(" 	,:33");
            query.AppendLine(" 	,:34");
            query.AppendLine(" 	,:35");
            query.AppendLine(" 	,:36");
            query.AppendLine(" 	,:37");
            query.AppendLine(" 	,:38");
            query.AppendLine(" 	,:39");
            query.AppendLine(" 	,:40");
            query.AppendLine(" 	,:41");
            query.AppendLine(" 	,:42");
            query.AppendLine(" 	,:43");
            query.AppendLine(" 	,:44");
            query.AppendLine(" 	,:45");
            query.AppendLine(" 	,:46");
            query.AppendLine(" 	,:47");
            query.AppendLine(" 	,:48");
            query.AppendLine(" 	,:49");
            query.AppendLine(" 	,:50");
            query.AppendLine(" )");
            #endregion

            DataTableCollection dta = dataset.Tables;

            foreach (DataTable dt in dta)
            {
                DataRowCollection drc = dataset.Tables[dt.TableName].Rows;

                foreach (DataRow row in drc)
                {
                    #region 파라미터
                    IDataParameter[] parameters =  {
	                     new OracleParameter("1", OracleDbType.Varchar2)
	                    ,new OracleParameter("2", OracleDbType.Varchar2)
	                    ,new OracleParameter("3", OracleDbType.Varchar2)
	                    ,new OracleParameter("4", OracleDbType.Varchar2)
	                    ,new OracleParameter("5", OracleDbType.Varchar2)
	                    ,new OracleParameter("6", OracleDbType.Varchar2)
	                    ,new OracleParameter("7", OracleDbType.Varchar2)
	                    ,new OracleParameter("8", OracleDbType.Varchar2)
	                    ,new OracleParameter("9", OracleDbType.Varchar2)
	                    ,new OracleParameter("10", OracleDbType.Varchar2)
	                    ,new OracleParameter("11", OracleDbType.Varchar2)
	                    ,new OracleParameter("12", OracleDbType.Varchar2)
	                    ,new OracleParameter("13", OracleDbType.Varchar2)
	                    ,new OracleParameter("14", OracleDbType.Varchar2)
	                    ,new OracleParameter("15", OracleDbType.Varchar2)
	                    ,new OracleParameter("16", OracleDbType.Varchar2)
	                    ,new OracleParameter("17", OracleDbType.Varchar2)
	                    ,new OracleParameter("18", OracleDbType.Varchar2)
	                    ,new OracleParameter("19", OracleDbType.Varchar2)
	                    ,new OracleParameter("20", OracleDbType.Varchar2)
	                    ,new OracleParameter("21", OracleDbType.Varchar2)
	                    ,new OracleParameter("22", OracleDbType.Varchar2)
	                    ,new OracleParameter("23", OracleDbType.Varchar2)
	                    ,new OracleParameter("24", OracleDbType.Varchar2)
	                    ,new OracleParameter("25", OracleDbType.Varchar2)
	                    ,new OracleParameter("26", OracleDbType.Varchar2)
	                    ,new OracleParameter("27", OracleDbType.Varchar2)
	                    ,new OracleParameter("28", OracleDbType.Varchar2)
	                    ,new OracleParameter("29", OracleDbType.Varchar2)
	                    ,new OracleParameter("30", OracleDbType.Varchar2)
	                    ,new OracleParameter("31", OracleDbType.Varchar2)
	                    ,new OracleParameter("32", OracleDbType.Varchar2)
	                    ,new OracleParameter("33", OracleDbType.Varchar2)
	                    ,new OracleParameter("34", OracleDbType.Varchar2)
	                    ,new OracleParameter("35", OracleDbType.Varchar2)
	                    ,new OracleParameter("36", OracleDbType.Varchar2)
	                    ,new OracleParameter("37", OracleDbType.Varchar2)
	                    ,new OracleParameter("38", OracleDbType.Varchar2)
	                    ,new OracleParameter("39", OracleDbType.Varchar2)
	                    ,new OracleParameter("40", OracleDbType.Varchar2)
	                    ,new OracleParameter("41", OracleDbType.Varchar2)
	                    ,new OracleParameter("42", OracleDbType.Varchar2)
	                    ,new OracleParameter("43", OracleDbType.Varchar2)
	                    ,new OracleParameter("44", OracleDbType.Varchar2)
	                    ,new OracleParameter("45", OracleDbType.Varchar2)
	                    ,new OracleParameter("46", OracleDbType.Varchar2)
	                    ,new OracleParameter("47", OracleDbType.Varchar2)
	                    ,new OracleParameter("48", OracleDbType.Varchar2)
	                    ,new OracleParameter("49", OracleDbType.Varchar2)
	                    ,new OracleParameter("50", OracleDbType.Varchar2)
                    };
                    #endregion

                    for (int i = 0; i < dataset.Tables[dt.TableName].Columns.Count; i++)
                    {
                        parameters[i].Value = row[i].ToString();
                    }
                    manager.ExecuteScript(query.ToString(), parameters);
                }
            }
        }

        /// <summary>
        /// 실시간 데이터 삭제
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="time"></param>
        internal void DeleteRealtime_Dao(EMapper manager, string time)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_GATHER_REALTIME WHERE TIMESTAMP = TO_DATE('" + time + "', 'YYYYMMDDHH24MI')");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 실시간 데이터 인덱스 리빌드
        /// </summary>
        /// <param name="oracleDBManager"></param>
        internal void IndexRebuildRealtime_Dao(EMapper manager)
        {
            // 실시간데이터 인덱스를 Rebuild한다.
            // 데이터가 쌓이고, 삭제되고를 반복하기 때문에 속도, 결과조회에 영향을 받기 때문
            manager.ExecuteScript("ALTER INDEX IF_GATHER_REALTIME_PK REBUILD", null);

            manager.ExecuteScript("ALTER INDEX IF_GATHER_REALTIME_PK2 REBUILD", null);
        }

        /// <summary>
        /// 1시간 적산 데이터 인덱스 리빌드
        /// </summary>
        /// <param name="oracleDBManager"></param>
        internal void IndexRebuildAccumulationHour_Dao(EMapper manager)
        {
            // 1시간 적산 데이터 인덱스를 Rebuild한다.
            // 데이터가 쌓이고, 삭제되고를 반복하기 때문에 속도, 결과조회에 영향을 받기 때문
            manager.ExecuteScript("ALTER INDEX IF_ACCUMULATION_HOUR_PK REBUILD", null);
        }

        /// <summary>
        /// 미수집된 데이터를 반환
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="result"></param>
        /// <param name="tableName"></param>
        internal void selectMissingRawData_Dao(EMapper manager, DataSet result, string tableName, string code, int cof)
        {
            StringBuilder query = new StringBuilder();

            query.Append(" SELECT TAGNAME, TIMESTAMP FROM IF_MISSING_REALTIME WHERE MISSING_GUBUN = '").Append(code).AppendLine("'");
            query.Append(" AND GATHER_YN = 'N' AND TIMESTAMP < SYSDATE - (1/24/60) * ").AppendLine(Convert.ToString(cof));

            manager.FillDataSetScript(result, tableName, query.ToString(), null);
        }

        /// <summary>
        /// 미수집 목록에서 수집한 목록은 수집체크를 한다.
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestr"></param>
        internal int UpdateMissingData_Dao(EMapper manager, string tagname, string timestr)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("UPDATE IF_MISSING_REALTIME SET GATHER_YN = 'Y', GATHER_DATE = SYSDATE ");
            query.AppendLine("WHERE TAGNAME = :1");
            query.AppendLine("  AND TIMESTAMP = :2");

            IDataParameter[] parameters =  {
                     new OracleParameter("1", OracleDbType.Varchar2)
                    ,new OracleParameter("2", OracleDbType.Date)
                };

            parameters[0].Value = tagname;
            parameters[1].Value = Convert.ToDateTime(timestr);

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 미수집 목록에서 수집한 목록에 대한 경고메시지 업데이트를 한다.
        /// 
        /// MISS_RT_201104052002', '', '', '실시간 데이터 미수집(2011년04월05일 20시02분):1 건:->복구::0 건'
        ///                                 실시간 데이터 미수집(2011년04월05일 20시03분):1 건:->복구:2011년04월07일 01시31분:1 건
        /// MISS_RT_201104052002201104052005
        /// </summary>
        internal void UpdateWarning_Dao(EMapper manager, DateTime cTime, int cnt)
        {
            string key = "MISS_RT_" + cTime.ToString("yyyyMMddHHmm");
            string remark = selectWarningMessage(manager, key);
            StringBuilder recoverMsg = new StringBuilder();

            string delim = ":";
            int ingCnt = 0;
            int idx = 0;
            int next = 0;
            while ((next = remark.IndexOf(delim, idx)) > -1)
            {
                recoverMsg.Append(remark.Substring(idx, next - idx));
                recoverMsg.Append(delim);

                idx = next + 1;
                ingCnt++;

                if (ingCnt == 3)
                {
                    recoverMsg.Append(DateTime.Now.ToString("yyyy년MM월dd일 HH시mm분"));
                    recoverMsg.Append(delim);
                    recoverMsg.Append(Convert.ToString(cnt));
                    recoverMsg.Append(" 건");
                    break;
                }
            }

            StringBuilder query = new StringBuilder();

            query.AppendLine("UPDATE WA_WARNING SET REMARK = :1 ");
            query.AppendLine("                    , FIXED_DATE = :2 ");
            query.AppendLine("                    , FIXED_YN = :3 ");
            query.AppendLine(" WHERE SERIAL_NO = :4");

            IDataParameter[] parameters =
                {
                     new OracleParameter("1", OracleDbType.Varchar2)
                    ,new OracleParameter("2", OracleDbType.Varchar2)
                    ,new OracleParameter("3", OracleDbType.Varchar2)
                    ,new OracleParameter("4", OracleDbType.Varchar2)
                };

            parameters[0].Value = recoverMsg.ToString();
            parameters[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            parameters[2].Value = "Y";
            parameters[3].Value = key;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 경고처리 테이블에서 SERIAL_NO에 따른 REMARK를 반환
        /// </summary>
        private string selectWarningMessage(EMapper manager, string key)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT REMARK FROM WA_WARNING WHERE SERIAL_NO = :1");

            IDataParameter[] param = { new OracleParameter("1", OracleDbType.Varchar2) };
            param[0].Value = key;

            return manager.ExecuteScriptScalar(query.ToString(), param).ToString();
        }

        /// <summary>
        /// 조회한 최종시간 데이터가 이미 수집 됐는지 확인
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="cTime"></param>
        /// <returns></returns>
        internal int IsGatherRealtimeData_Dao(EMapper manager, string cTime)
        {
            string returnValue = string.Empty;
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT count(*) FROM IF_GATHER_REALTIME WHERE TIMESTAMP = :1");

            IDataParameter[] param = { new OracleParameter("1", OracleDbType.Date) };
            param[0].Value = DateTime.ParseExact(cTime, "yyyyMMddHHmm", null);

            returnValue = manager.ExecuteScriptScalar(query.ToString(), param).ToString();

            return Convert.ToInt16(returnValue);
        }

        /// <summary>
        /// 이전 수집 데이터 있는지 확인
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="result"></param>
        /// <param name="cTime"></param>
        internal void selectPastRawData_Dao(EMapper manager, DataSet result, string tableName)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TAGNAME, TIMESTAMP, COEFFICIENT");
            query.AppendLine("FROM   (SELECT T.TAGNAME, T.TIMESTAMP, T.COEFFICIENT, I.TAGNAME TAG");
            query.AppendLine("        FROM   (SELECT TAGNAME, TIMESTAMP");
            query.AppendLine("                FROM   IF_GATHER_REALTIME");
            query.AppendLine("                WHERE  TIMESTAMP BETWEEN (SELECT /*+ index_desc(i if_missing_realtime_pk ) */");
            query.AppendLine("                                                 DECODE(TIMESTAMP, NULL, SYSDATE, TIMESTAMP + 1/24/60) TIMESTAMP");
            query.AppendLine("                                          FROM   IF_MISSING_REALTIME I");
            query.AppendLine("                                          WHERE  ROWNUM = 1");
            query.AppendLine("                                         )");
            query.AppendLine("                                     AND (SELECT /*+ index_desc( i if_gather_realtime_pk ) */");
            query.AppendLine("                                                 DECODE(TIMESTAMP, NULL, SYSDATE, TIMESTAMP - 1/24/60) TIMESTAMP");
            query.AppendLine("                                          FROM   IF_GATHER_REALTIME I");
            query.AppendLine("                                          WHERE  ROWNUM = 1");
            query.AppendLine("                                         )");
            query.AppendLine("               )I");
            query.AppendLine("               RIGHT OUTER JOIN (SELECT A.TAGNAME");
            query.AppendLine("                                      , TO_DATE(B.YYYYMMDDHH24MI, 'yyyymmddhh24mi') TIMESTAMP");
            query.AppendLine("                                      , COEFFICIENT");
            query.AppendLine("                                 FROM   IF_IHTAGS A");
            query.AppendLine("                                      , IF_TIMESTAMP_YYYYMMDDHH24MI B");
            query.AppendLine("                                 WHERE  A.USE_YN = 'Y'");
            query.AppendLine("                                 AND    B.YYYYMMDDHH24MI BETWEEN (SELECT /*+ index_desc(i if_missing_realtime_pk ) */");
            query.AppendLine("                                                                         TO_CHAR(DECODE(TIMESTAMP, NULL, SYSDATE, TIMESTAMP) + 1/24/60, 'yyyymmddhh24mi')");
            query.AppendLine("                                                                  FROM   IF_MISSING_REALTIME I");
            query.AppendLine("                                                                  WHERE  ROWNUM = 1");
            query.AppendLine("                                                                 )");
            query.AppendLine("                                                             AND (SELECT /*+ index_desc( i if_gather_realtime_pk ) */");
            query.AppendLine("                                                                         TO_CHAR(DECODE(TIMESTAMP, NULL, SYSDATE, TIMESTAMP) - 1/24/60, 'yyyymmddhh24mi')");
            query.AppendLine("                                                                  FROM   IF_GATHER_REALTIME I");
            query.AppendLine("                                                                  WHERE  ROWNUM = 1");
            query.AppendLine("                                                                 )");
            query.AppendLine("                                ) T");
            query.AppendLine("        ON     I.TAGNAME = T.TAGNAME AND I.TIMESTAMP =  T.TIMESTAMP");
            query.AppendLine("       )");
            query.AppendLine("WHERE  TAGNAME NOT LIKE '%1H'");
            query.AppendLine("AND    TAGNAME NOT LIKE '%YD'");
            query.AppendLine("AND    TAGNAME NOT LIKE '%TD'");
            query.AppendLine("AND    TAG IS NULL");

            manager.FillDataSetScript(result, tableName, query.ToString(), null);
        }

        /// <summary>
        /// 자동보정 시간 설정
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <param name="cTime"></param>
        internal int checkAutoModify_dao(EMapper manager, string cTime)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_MISSING_REALTIME VALUES('TMP_TAG_FOR_MISSINGDATA', :1, 'Y', :2, '000002') ");
            deleteAutoModify(manager);

            IDataParameter[] parameters =  {
                     new OracleParameter("1", OracleDbType.Date)
                    ,new OracleParameter("2", OracleDbType.Date)
                };

            parameters[0].Value = DateTime.ParseExact(cTime, "yyyyMMddHHmm", null);
            parameters[1].Value = DateTime.ParseExact(cTime, "yyyyMMddHHmm", null);

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 자동보정 이전 데이터 삭제
        /// </summary>
        /// <param name="oracleDBManager"></param>
        internal void deleteAutoModify(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE FROM IF_MISSING_REALTIME");

            manager.ExecuteScript(query.ToString(), null);
        }

        /// <summary>
        /// 자동보정 시간 반환
        /// </summary>
        /// <param name="oracleDBManager"></param>
        /// <returns></returns>
        internal string getTimeAutoModify(EMapper manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT MAX(TIMESTAMP) FROM IF_MISSING_REALTIME");

            return manager.ExecuteScriptScalar(query.ToString(), null).ToString();
        }
    }
}