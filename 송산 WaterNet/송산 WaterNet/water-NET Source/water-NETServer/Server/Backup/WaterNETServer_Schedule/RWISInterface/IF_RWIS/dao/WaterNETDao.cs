﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMFrame.dm;
using System.Collections;
using System.Data;
using Oracle.DataAccess.Client;

namespace WaterNETServer.IF_RWIS.dao
{
    public class WaterNETDao
    {
        private static WaterNETDao dao = null;
        private WaterNETDao() { }

        public static WaterNETDao GetInstance()
        {
            if (dao == null)
            {
                dao = new WaterNETDao();
            }

            return dao;
        }

        public DataTable SelectTagGbn(EMapper manager, string gbn)
        {
            StringBuilder query = new StringBuilder();

            query.Append("SELECT CODE FROM CM_CODE WHERE PCODE = :TAGGBN");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGGBN", OracleDbType.Varchar2)
                    };

            parameters[0].Value = gbn;

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        /// <summary>
        /// 태그 목록조회
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="kinds">태그 구분 배열</param>
        /// <returns></returns>
        public DataTable SelectTagList(EMapper manager, string[] kinds)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT DISTINCT B.TAGNAME");
            query.AppendLine("      ,A.DESCRIPTION");
            query.AppendLine("  FROM IF_IHTAGS A");
            query.AppendLine("      ,IF_TAG_GBN B");
            query.AppendLine(" WHERE A.USE_YN = 'Y'");
            query.AppendLine("   AND B.TAGNAME = A.TAGNAME");

            if (kinds != null && kinds.Length != 0)
            {
                query.Append("       AND B.TAG_GBN IN (");

                foreach (string kind in kinds)
                {
                    if (kind.Equals(kinds[0]))
                    {
                        query.Append("'").Append(kind).Append("'");
                    }
                    else
                    {
                        query.Append(",'").Append(kind).Append("'");
                    }
                }

                query.AppendLine(")");
            }

            return manager.ExecuteScriptDataTable(query.ToString(), null);
        }

        public DataTable SelectCalTagList(EMapper manager, string tagname)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TAGNAME");
            query.AppendLine("      ,CTAGNAME");
            query.AppendLine("      ,CAL_GBN");
            query.AppendLine("  FROM IF_TAG_CAL");
            query.AppendLine(" WHERE TAGNAME = :TAGNAME");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                    };

            parameters[0].Value = tagname;

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        public DataTable SelectCalTagList2(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select a.tagname                      ");
            query.AppendLine("        , value                       ");
            query.AppendLine("        ,b.cal_gbn                    ");
            query.AppendLine("  from if_accumulation_hour a         ");
            query.AppendLine("       ,(select * from if_tag_cal     ");
            query.AppendLine("          where tagname = :CALTAGNAME ");
            query.AppendLine("          and ctagname != :TAGNAME    ");
            query.AppendLine("        ) b                           ");
            query.AppendLine("  where a.tagname = b.ctagname        ");
            query.AppendLine("  and timestamp = TO_DATE(:TIMESTAMP,'yyyyMMddHH24') ");

            IDataParameter[] parameters =  {
                        new OracleParameter("CALTAGNAME", OracleDbType.Varchar2)
                        , new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                        , new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["CALTAGNAME"];
            parameters[1].Value = parameter["TAGNAME"];
            parameters[2].Value = parameter["TIMESTAMP"];

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        public object SelectConfficient(EMapper manager, string tagname)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT NVL(COEFFICIENT, 1)");
            query.AppendLine("  FROM IF_IHTAGS");
            query.AppendLine(" WHERE TAGNAME = :TAGNAME");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                    };

            parameters[0].Value = tagname;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public DataTable SelectCalTagList(EMapper manager, IList<string> tagnames)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TAGNAME");
            query.AppendLine("      ,CTAGNAME");
            query.AppendLine("      ,CAL_GBN");
            query.AppendLine("  FROM IF_TAG_CAL");

            if (tagnames != null && tagnames.Count != 0)
            {
                query.Append("     WHERE TAGNAME IN (");

                foreach (string tagname in tagnames)
                {
                    if (tagname.Equals(tagnames[0]))
                    {
                        query.Append("'").Append(tagname).Append("'");
                    }
                    else
                    {
                        query.Append(",'").Append(tagname).Append("'");
                    }
                }

                query.AppendLine(")");
            }

            return manager.ExecuteScriptDataTable(query.ToString(), null);
        }

        /// <summary>
        /// 실시간(1분) 수집 데이터 삭제
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int DeleteGatherRealtimeData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE IF_GATHER_REALTIME");
            query.AppendLine(" WHERE TAGNAME = :TAGNAME");
            query.AppendLine("   AND TIMESTAMP = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI')");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2),
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 실시간(1분) 수집 데이터 입력
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int InsertGatherRealtimeData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_GATHER_REALTIME");
            query.AppendLine("           (");
            query.AppendLine("            TAGNAME,");
            query.AppendLine("            TIMESTAMP,");
            query.AppendLine("            VALUE");
            query.AppendLine("           )");
            query.AppendLine("           VALUES");
            query.AppendLine("           (");
            query.AppendLine("            :TAGNAME,");
            query.AppendLine("            TO_DATE(:TIMESTAMP,'YYYYMMDDHH24MI'),");
            query.AppendLine("            :VALUE");
            query.AppendLine("           )");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2),
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2),
                        new OracleParameter("VALUE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];
            parameters[2].Value = parameter["VALUE"];

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 금일적산데이터 등록
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestmap"></param>
        /// <returns></returns>
        public void InsertAccumulationToDayData(EMapper manager, DateTime timestamp)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_ACCUMULATION_TODAY A");
            query.AppendLine("USING ( SELECT A.TAGNAME AS TAGNAME");
            query.AppendLine("             , A.TIMESTAMP");
            query.AppendLine("             , A.VALUE - B.VALUE AS VALUE");
            query.AppendLine("          FROM (SELECT A.TAGNAME");
            query.AppendLine("                     , A.TIMESTAMP");
            query.AppendLine("                     , (SELECT NVL(COEFFICIENT, 1) * A.VALUE FROM IF_IHTAGS WHERE TAGNAME = A.TAGNAME) VALUE");
            query.AppendLine("                  FROM IF_GATHER_REALTIME A");
            query.AppendLine("                     , IF_TAG_GBN B");
            query.AppendLine("                 WHERE A.TAGNAME   = B.TAGNAME");
            query.AppendLine("                   AND A.TIMESTAMP = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI')  - (1/24/60) * 4");
            query.AppendLine("                   AND B.TAG_GBN   = 'TT'");
            query.AppendLine("               ) A");
            query.AppendLine("             , (SELECT A.TAGNAME");
            query.AppendLine("                     , (SELECT NVL(COEFFICIENT, 1) * A.VALUE FROM IF_IHTAGS WHERE TAGNAME = A.TAGNAME) VALUE");
            query.AppendLine("                  FROM IF_GATHER_REALTIME A");
            query.AppendLine("                     , IF_TAG_GBN B");
            query.AppendLine("                 WHERE A.TAGNAME   = B.TAGNAME");
            query.AppendLine("                   AND A.TIMESTAMP = TRUNC(TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI') - (1/24/60) * 4, 'DD') ");
            query.AppendLine("                   AND B.TAG_GBN   = 'TT' ");
            query.AppendLine("               ) B");
            query.AppendLine("         WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("      ) B");
            query.AppendLine("ON    (        A.TAGNAME = B.TAGNAME");
            query.AppendLine("         AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("  UPDATE SET A.VALUE = B.VALUE");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("  INSERT (A.TAGNAME, A.TIMESTAMP, A.VALUE)");
            query.AppendLine("  VALUES (B.TAGNAME, B.TIMESTAMP, B.VALUE)");

            IDataParameter[] parameters =  {
                       new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = timestamp.ToString("yyyyMMddHHmm");
            parameters[1].Value = timestamp.ToString("yyyyMMddHHmm");

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 금일적산데이터 삭제
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="tagname"></param>
        /// <param name="timestmap"></param>
        /// <returns></returns>
        public void DeleteAccumulationToDayData(EMapper manager, DateTime timestamp)
        {
            string cof = "14";   // 삭제는 14분전 데이터를 삭제

            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE");
            query.AppendLine("  FROM IF_ACCUMULATION_TODAY");
            query.AppendLine(" WHERE TIMESTAMP < TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI') - (1/24/60) * :COF");

            IDataParameter[] parameters =  {
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        ,new OracleParameter("COF", OracleDbType.Varchar2)
                    };

            parameters[0].Value = timestamp.ToString("yyyyMMddHHmm");
            parameters[1].Value = cof;

            manager.ExecuteScript(query.ToString(), parameters);

            // 금일적산 인덱스를 Rebuild한다.
            // 데이터가 쌓이고, 삭제되고를 반복하기 때문에 속도, 결과조회에 영향을 받기 때문
            manager.ExecuteScript("ALTER INDEX IF_ACCU_TODAY_PK REBUILD", null);
        }

        /// <summary>
        /// 1일 적산 데이터 수집 (전일적산)
        /// </summary>
        public void InsertAccumulationDayData(EMapper manager, string tagname, DateTime timestamp)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_ACCUMULATION_YESTERDAY A");
            query.AppendLine("USING ( SELECT B.TAGNAME AS TAGNAME");
            query.AppendLine("             , TO_DATE(TO_CHAR(A.TIMESTAMP - 1/24, 'YYYYMMDD'), 'YYYYMMDD') AS TIMESTAMP");
            query.AppendLine("             , SUM(A.VALUE) AS VALUE");
            query.AppendLine("          FROM IF_ACCUMULATION_HOUR A");
            query.AppendLine("             , (SELECT DISTINCT TAGNAME FROM IF_TAG_GBN WHERE TAGNAME = :TAGNAME AND TAG_GBN IN ('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_I', 'SPL_O')) B");
            query.AppendLine("         WHERE A.TAGNAME = B.TAGNAME");
            query.AppendLine("         GROUP BY B.TAGNAME, TO_DATE(TO_CHAR(A.TIMESTAMP - 1/24, 'YYYYMMDD'), 'YYYYMMDD')");
            query.AppendLine("        HAVING TO_DATE(TO_CHAR(A.TIMESTAMP - 1/24, 'YYYYMMDD'), 'YYYYMMDD') = TO_DATE(:TIMESTAMP, 'YYYYMMDD')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON (       A.TAGNAME   = B.TAGNAME");
            query.AppendLine("          AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("      UPDATE SET A.VALUE = B.VALUE");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("      INSERT (A.TAGNAME, A.TIMESTAMP, A.VALUE)");
            query.AppendLine("      VALUES (B.TAGNAME, B.TIMESTAMP, B.VALUE)");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = tagname;
            parameters[1].Value = timestamp.ToString("yyyyMMdd");

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 1시간 적산차 데이터 삭제
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int DeleteAccumulationHourData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE IF_ACCUMULATION_HOUR");
            query.AppendLine(" WHERE TAGNAME = :TAGNAME");
            query.AppendLine("   AND TIMESTAMP = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24')");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2),
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 1시간 적산차 데이터 입력
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int InsertAccumulationHourData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_ACCUMULATION_HOUR");
            query.AppendLine("           (");
            query.AppendLine("            TAGNAME,");
            query.AppendLine("            TIMESTAMP,");
            query.AppendLine("            VALUE");
            query.AppendLine("           )");
            query.AppendLine("           VALUES");
            query.AppendLine("           (");
            query.AppendLine("            :TAGNAME,");
            query.AppendLine("            TO_DATE(:TIMESTAMP,'YYYYMMDDHH24'),");
            query.AppendLine("            :VALUE");
            query.AppendLine("           )");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2),
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2),
                        new OracleParameter("VALUE", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];
            parameters[2].Value = parameter["VALUE"];

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 일별 야간 최소 유량 (매일 오전(8시30분)에 00~08시까지의 야간 최소유량을 구함)
        /// </summary>
        public void InsertMinimumNightFlowData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_WV_MNF A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT /*+ rule */ A.TAGNAME");
            query.AppendLine("             , TO_DATE(TO_CHAR(A.TIMESTAMP, 'YYYYMMDD'), 'YYYYMMDD') TIMESTAMP");
            query.AppendLine("             , ROUND(AVG(B.VALUE), 2) MIN");
            query.AppendLine("             , ROUND(MIN(A.MAVG), 2) MVAVG_MIN");
            query.AppendLine("             , MIN(TO_CHAR(A.TIMESTAMP, 'YYYYMMDDHH24MI')) KEEP(DENSE_RANK FIRST ORDER BY A.MAVG ASC) MVAVG_MINTIME  -- 이동평균이 최소일때의 시간");
            query.AppendLine("         FROM ( ");
            query.AppendLine("               SELECT TAGNAME, TIMESTAMP, VALUE, MAVG");
            query.AppendLine("                 FROM (");
            query.AppendLine("                       SELECT A.TAGNAME");
            query.AppendLine("                            , A.TIMESTAMP");
            query.AppendLine("                            , TO_NUMBER(A.VALUE) VALUE");
            query.AppendLine("                            , AVG(TO_NUMBER(A.VALUE)) OVER (PARTITION BY A.TAGNAME ORDER BY A.TAGNAME, TIMESTAMP ROWS 60 - 1 PRECEDING) AS MAVG    -- 1시간 이동평균");
            query.AppendLine("                         FROM IF_GATHER_REALTIME A");
            query.AppendLine("                            , IF_TAG_GBN B");
            query.AppendLine("                        WHERE B.TAGNAME = :TAGNAME");
            query.AppendLine("                          AND A.TAGNAME = B.TAGNAME");
            query.AppendLine("                          AND A.TIMESTAMP BETWEEN TO_DATE(:TIMESTAMP,'YYYYMMDD') AND TO_DATE(:TIMESTAMP,'YYYYMMDD') + 1 - 1/24/60 ");
            query.AppendLine("                          AND B.TAG_GBN = 'MNF'");
            query.AppendLine("                          AND TO_NUMBER(A.VALUE) >= 0");
            query.AppendLine("                      )");
            query.AppendLine("                WHERE TIMESTAMP BETWEEN TO_DATE(:TIMESTAMP,'YYYYMMDD') AND TO_DATE(:TIMESTAMP,'YYYYMMDD') + 7/24");
            query.AppendLine("              ) A");
            query.AppendLine("            , (");
            query.AppendLine("                SELECT TAGNAME");
            query.AppendLine("                     , TIMESTAMP");
            query.AppendLine("                     , VALUE");
            query.AppendLine("                     , NU");
            query.AppendLine("                  FROM (");
            query.AppendLine("                        SELECT A.TAGNAME");
            query.AppendLine("                             , A.TIMESTAMP");
            query.AppendLine("                             , TO_NUMBER(A.VALUE) AS VALUE");
            query.AppendLine("                             , ROW_NUMBER() OVER ");
            query.AppendLine("                                   (PARTITION BY A.TAGNAME, TRUNC(A.TIMESTAMP, 'DD')");
            query.AppendLine("                                        ORDER BY A.TAGNAME, TRUNC(A.TIMESTAMP, 'DD'), TO_NUMBER(A.VALUE))");
            query.AppendLine("                               NU -- 순시값 순위 ");
            query.AppendLine("                          FROM IF_GATHER_REALTIME A");
            query.AppendLine("                             , IF_TAG_GBN B");
            query.AppendLine("                         WHERE B.TAGNAME = :TAGNAME");
            query.AppendLine("                           AND A.TAGNAME = B.TAGNAME");
            query.AppendLine("                           AND A.TIMESTAMP BETWEEN TO_DATE(:TIMESTAMP,'YYYYMMDD') AND TO_DATE(:TIMESTAMP,'YYYYMMDD') + 1 - 1/24/60 ");
            query.AppendLine("                           AND TO_CHAR(A.TIMESTAMP, 'HH24MI') BETWEEN '0000'AND '0700'");
            query.AppendLine("                           AND B.TAG_GBN = 'MNF'");
            query.AppendLine("                           AND TO_NUMBER(A.VALUE) >= 0");
            query.AppendLine("                       ) A");
            query.AppendLine("                 WHERE NU BETWEEN 3 AND 8");
            query.AppendLine("              ) B");
            query.AppendLine("         WHERE A.TAGNAME = B.TAGNAME (+)");
            query.AppendLine("           AND A.TIMESTAMP = B.TIMESTAMP (+)");
            query.AppendLine("         GROUP BY A.TAGNAME, TO_CHAR(A.TIMESTAMP, 'YYYYMMDD')");
            query.AppendLine("      ) B");
            query.AppendLine("   ON (     A.TAGNAME   = B.TAGNAME");
            query.AppendLine("        AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("    UPDATE SET A.MIN           = B.MIN");
            query.AppendLine("             , A.MVAVG_MIN     = B.MVAVG_MIN");
            query.AppendLine("             , A.MVAVG_MINTIME = B.MVAVG_MINTIME");
            query.AppendLine("             , A.FILTERING     = B.MVAVG_MIN");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("    INSERT (A.TAGNAME, A.TIMESTAMP, A.MIN, A.MVAVG_MIN, A.MVAVG_MINTIME, A.FILTERING)");
            query.AppendLine("    VALUES (B.TAGNAME, B.TIMESTAMP, B.MIN, B.MVAVG_MIN, B.MVAVG_MINTIME, B.MVAVG_MIN)");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        ,new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];
            parameters[2].Value = parameter["TIMESTAMP"];
            parameters[3].Value = parameter["TIMESTAMP"];
            parameters[4].Value = parameter["TIMESTAMP"];
            parameters[5].Value = parameter["TAGNAME"];
            parameters[6].Value = parameter["TIMESTAMP"];
            parameters[7].Value = parameter["TIMESTAMP"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 1시간 수압(정시의 수압) (지정일) 보정
        /// </summary>
        /// <param name="cTime2">보정 시작 날짜</param>
        /// <param name="cTime3">보정 끝 날짜</param>
        public void InsertAveragePressure(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_WV_PRAVG A");
            query.AppendLine("USING (");
            query.AppendLine("        SELECT A.TAGNAME, A.TIMESTAMP, ROUND(TO_NUMBER(A.VALUE), 2) TAVG");
            query.AppendLine("          FROM IF_GATHER_REALTIME A");
            query.AppendLine("             , IF_TAG_GBN B");
            query.AppendLine("         WHERE B.TAGNAME = :TAGNAME");
            query.AppendLine("           AND A.TAGNAME = B.TAGNAME");
            query.AppendLine("           AND A.TIMESTAMP = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24')");
            query.AppendLine("           AND B.TAG_GBN = 'PRI'");
            query.AppendLine("      ) B");
            query.AppendLine("   ON (     A.TAGNAME   = B.TAGNAME");
            query.AppendLine("        AND A.TIMESTAMP = B.TIMESTAMP");
            query.AppendLine("      )");
            query.AppendLine("WHEN MATCHED THEN");
            query.AppendLine("    UPDATE SET A.TAVG = B.TAVG");
            query.AppendLine("WHEN NOT MATCHED THEN");
            query.AppendLine("    INSERT (A.TAGNAME, A.TIMESTAMP, A.TAVG)");
            query.AppendLine("    VALUES (B.TAGNAME, B.TIMESTAMP, B.TAVG)");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                        ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            string time = Convert.ToString(parameter["TIMESTAMP"]);
            parameters[1].Value = time;

            manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 실시간(1분) 미수집 태그 입력
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int InsertMissingRealtimeData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("INSERT INTO IF_MISSING_REALTIME");
            query.AppendLine("           (");
            query.AppendLine("            TAGNAME,");
            query.AppendLine("            TIMESTAMP,");
            query.AppendLine("            GATHER_YN,");
            query.AppendLine("            MISSING_GUBUN");
            query.AppendLine("           )");
            query.AppendLine("           VALUES");
            query.AppendLine("           (");
            query.AppendLine("            :TAGNAME,");
            query.AppendLine("            TO_DATE(:TIMESTAMP,'YYYYMMDDHH24MI'),");
            query.AppendLine("            :GATHER_YN,");
            query.AppendLine("            :MISSING_GUBUN");
            query.AppendLine("           )");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2),
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2),
                        new OracleParameter("GATHER_YN", OracleDbType.Varchar2),
                        new OracleParameter("MISSING_GUBUN", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];
            parameters[2].Value = parameter["GATHER_YN"];
            parameters[3].Value = parameter["MISSING_GUBUN"];

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 실시간(1분) 미수집 태그 조회 (카운트)
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int CheckMissingRealtimeData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT COUNT(*)");
            query.AppendLine("  FROM IF_MISSING_REALTIME");
            query.AppendLine(" WHERE TAGNAME = :TAGNAME");
            query.AppendLine("   AND TIMESTAMP = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI')");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2),
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];

            return Convert.ToInt32(manager.ExecuteScriptScalar(query.ToString(), parameters).ToString());
        }

        /// <summary>
        /// 실시간(1분) 미수집 태그 조회
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public DataTable SelectMissingRealtimeTagList(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT TAGNAME");
            query.AppendLine("  FROM IF_MISSING_REALTIME");
            query.AppendLine(" WHERE TIMESTAMP = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI')");
            query.AppendLine("   AND GATHER_YN != 'Y'");
            query.AppendLine("   AND MISSING_GUBUN = :MISSING_GUBUN");

            IDataParameter[] parameters =  {
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2),
                        new OracleParameter("MISSING_GUBUN", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TIMESTAMP"];
            parameters[1].Value = parameter["MISSING_GUBUN"];

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        /// <summary>
        /// 실시간(1분) 미수집 태그 조회 (realtime, missing 포함)
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public DataTable SelectMissingRealtimeTagListAll(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT DISTINCT  A.TAGNAME");
            query.AppendLine("  FROM (");
            query.AppendLine("		    SELECT A.TAGNAME");
            query.AppendLine("		      FROM IF_IHTAGS A");
            query.AppendLine("			      ,IF_TAG_GBN B");
            query.AppendLine("	    	 WHERE A.USE_YN = 'Y'");
            query.AppendLine("		       AND B.TAGNAME = A.TAGNAME");
            query.AppendLine("		       AND B.TAG_GBN NOT IN ('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_I', 'SPL_O')");
            //query.AppendLine("		       AND SUBSTR(B.TAGNAME, 0, 5) != 'DUMMY'");
            query.AppendLine("       ) A");
            query.AppendLine("	    ,IF_GATHER_REALTIME B");
            query.AppendLine(" WHERE B.TAGNAME(+) = A.TAGNAME");
            query.AppendLine("   AND B.TIMESTAMP(+) = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI')");
            query.AppendLine("   AND B.VALUE IS NULL");
            //query.AppendLine("UNION ALL");
            //query.AppendLine("SELECT A.TAGNAME");
            //query.AppendLine("  FROM IF_TAG_CAL A");
            //query.AppendLine("      ,(");
            //query.AppendLine("        SELECT B.TAGNAME");
            //query.AppendLine("          FROM (");
            //query.AppendLine("               SELECT DISTINCT A.TAGNAME");
            //query.AppendLine("                 FROM IF_IHTAGS A");
            //query.AppendLine("                     ,IF_TAG_GBN B");
            //query.AppendLine("                WHERE A.USE_YN = 'Y'");
            //query.AppendLine("		              AND B.TAGNAME = A.TAGNAME");
            //query.AppendLine("		              AND B.TAG_GBN IN ('CLI', 'FRI', 'LC', 'LEI', 'MC', 'PHI', 'POI_IN', 'POI_OU', 'PRI', 'RT', 'TBI', 'TT', 'MNF', 'TD', 'PMB')");
            //query.AppendLine("		              AND SUBSTR(A.TAGNAME, 0, 5) != 'DUMMY'");
            //query.AppendLine("              ) A");
            //query.AppendLine("	            ,IF_GATHER_REALTIME B");
            //query.AppendLine("         WHERE B.TAGNAME(+) = A.TAGNAME");
            //query.AppendLine("           AND B.TIMESTAMP(+) = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI')");
            //query.AppendLine("           AND B.VALUE IS NULL");
            //query.AppendLine("              ) B");
            //query.AppendLine("WHERE B.TAGNAME = A.CTAGNAME");

            IDataParameter[] parameters =  {
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        //,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        //,new OracleParameter("MISSING_GUBUN", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TIMESTAMP"];
            //parameters[1].Value = parameter["TIMESTAMP"];
            //parameters[1].Value = parameter["MISSING_GUBUN"];

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        /// <summary>
        /// 실시간(1분) 미수집 태그 조회 (realtime, missing 포함)
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public DataTable SelectMissingHourTagListAll(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT DISTINCT  A.TAGNAME");
            query.AppendLine("  FROM (");
            query.AppendLine("		    SELECT A.TAGNAME");
            query.AppendLine("		      FROM IF_IHTAGS A");
            query.AppendLine("			       ,IF_TAG_GBN B");
            query.AppendLine("	    	 WHERE A.USE_YN = 'Y'");
            query.AppendLine("		       AND B.TAGNAME = A.TAGNAME");
            query.AppendLine("		       AND B.TAG_GBN IN ('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_I', 'SPL_O')");
            //query.AppendLine("		       AND SUBSTR(B.TAGNAME, 0, 5) != 'DUMMY'");
            query.AppendLine("       ) A");
            query.AppendLine("	    ,IF_ACCUMULATION_HOUR B");
            query.AppendLine(" WHERE B.TAGNAME(+) = A.TAGNAME");
            query.AppendLine("   AND B.TIMESTAMP(+) = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24')");
            query.AppendLine("   AND B.VALUE IS NULL");
            //query.AppendLine("UNION ALL");
            //query.AppendLine("SELECT A.TAGNAME");
            //query.AppendLine("  FROM IF_TAG_CAL A");
            //query.AppendLine("      ,(");
            //query.AppendLine("        SELECT A.TAGNAME");
            //query.AppendLine("          FROM (");
            //query.AppendLine("               SELECT DISTINCT B.TAGNAME");
            //query.AppendLine("                 FROM IF_IHTAGS A");
            //query.AppendLine("                     ,IF_TAG_GBN B");
            //query.AppendLine("                WHERE A.USE_YN = 'Y'");
            //query.AppendLine("		              AND B.TAGNAME = A.TAGNAME");
            //query.AppendLine("		              AND B.TAG_GBN IN ('FRQ', 'FRQ_O', 'FRQ_I', 'SPL_D', 'SPL_I', 'SPL_O')");
            //query.AppendLine("		              AND SUBSTR(B.TAGNAME, 0, 5) != 'DUMMY'");
            //query.AppendLine("              ) A");
            //query.AppendLine("	            ,IF_ACCUMULATION_HOUR B");
            //query.AppendLine("         WHERE B.TAGNAME(+) = A.TAGNAME");
            //query.AppendLine("           AND B.TIMESTAMP(+) = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24')");
            //query.AppendLine("           AND B.VALUE IS NULL");
            //query.AppendLine("              ) B");
            //query.AppendLine("WHERE B.TAGNAME = A.CTAGNAME");

            IDataParameter[] parameters =  {
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        //,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                        //,new OracleParameter("MISSING_GUBUN", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TIMESTAMP"];
            //parameters[1].Value = parameter["TIMESTAMP"];
            //parameters[1].Value = parameter["MISSING_GUBUN"];

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        /// <summary>
        /// 실시간(1분) 미수집 태그 수정
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int UpdateMissingRealtimeData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("UPDATE IF_MISSING_REALTIME");
            query.AppendLine("   SET GATHER_YN = :GATHER_YN");
            query.AppendLine("      ,GATHER_DATE = TO_DATE(:GATHER_DATE, 'YYYYMMDDHH24MI')");
            query.AppendLine(" WHERE TAGNAME = :TAGNAME");
            query.AppendLine("   AND TIMESTAMP = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI')");
            query.AppendLine("   AND MISSING_GUBUN = :MISSING_GUBUN");

            IDataParameter[] parameters =  {
                        new OracleParameter("GATHER_YN", OracleDbType.Varchar2),
                        new OracleParameter("GATHER_DATE", OracleDbType.Varchar2),
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2),
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2),
                        new OracleParameter("MISSING_GUBUN", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["GATHER_YN"];
            parameters[1].Value = parameter["GATHER_DATE"];
            parameters[2].Value = parameter["TAGNAME"];
            parameters[3].Value = parameter["TIMESTAMP"];
            parameters[4].Value = parameter["MISSING_GUBUN"];

            return manager.ExecuteScript(query.ToString(), parameters);
        }

        /// <summary>
        /// 실시간(1분) 미수집 태그 삭제
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int DeleteMissingRealtimeData(EMapper manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("DELETE IF_MISSING_REALTIME");
            query.AppendLine(" WHERE TAGNAME = :TAGNAME");
            query.AppendLine("   AND TIMESTAMP = TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24MI')");
            query.AppendLine("   AND MISSING_GUBUN = :MISSING_GUBUN");

            IDataParameter[] parameters =  {
                        new OracleParameter("TAGNAME", OracleDbType.Varchar2),
                        new OracleParameter("TIMESTAMP", OracleDbType.Varchar2),
                        new OracleParameter("MISSING_GUBUN", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];
            parameters[2].Value = parameter["MISSING_GUBUN"];

            return manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}