﻿using System;
using System.Data;
using System.Diagnostics;
using Oracle.DataAccess.Client;

namespace WaterNETServer.Common.DB.Oracle
{
    public class OracleDBManager
    {
        private OracleConnection con = null;
        private string conStr = null;
        private OracleTransaction transaction = null;

        public OracleConnection Connection
        {
            get { return con; }
            set { con = value; }
        }

        public string ConnectionString
        {
            get { return conStr; }
            set { conStr = value; }
        }

        public OracleTransaction Transaction
        {
            get { return transaction; }
            set { transaction = value; }
        }

        public void Open()
        {
            if (con == null)
            {
                try
                {
                    con = new OracleConnection();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            if (con.State == ConnectionState.Closed)
            {
                try
                {
                    con.ConnectionString = conStr;
                    con.Open();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public void Close()
        {
            if (con != null)
            {
                if (con.State != ConnectionState.Closed)
                {
                    try
                    {
                        con.Close();
                        con.Dispose();
                        con = null;
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
        }

        public void BeginTransaction()
        {
            if (transaction == null)
            {
                try
                {
                    transaction = con.BeginTransaction();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public void CommitTransaction()
        {
            if (transaction != null)
            {
                try
                {
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public void RollbackTransaction()
        {
            if (transaction != null)
            {
                try
                {
                    transaction.Rollback();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public DataSet ExecuteScriptDataSet(string strScript, IDataParameter[] param, string strTableName)
        {
            DataSet dataSet = null;

            if (string.IsNullOrEmpty(strScript))
                return null;

            try
            {
                dataSet = new DataSet();
                OracleDataAdapter adapter = new OracleDataAdapter();

                adapter.SelectCommand = CreateScriptCommand(strScript, param);
                adapter.Fill(dataSet, strTableName);
            }
            catch (Exception e)
            {
                throw e;
            }
            return dataSet;
        }

        public OracleCommand CreateScriptCommand(string strScript, IDataParameter[] param)
        {
            OracleCommand cmd = new OracleCommand(strScript, con);
            cmd.CommandType = CommandType.Text;

            if (param != null)
            {
                foreach (OracleParameter p in param)
                {
                    cmd.Parameters.Add(p);
                }
            }
            return cmd;
        }


        public void FillDataSetScript(DataSet oDataSet, string strTableName, string strScript, IDataParameter[] aoDataParameter)
        {
            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();

                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int ExecuteScript(string strScript, IDataParameter[] aoDataParameter)
        {
            int nRowCount = 0;

            ExecuteScript(strScript, aoDataParameter, ref nRowCount);

            return nRowCount;
        }


        public void ExecuteScript(string strScript, IDataParameter[] aoDataParameter, ref int nRowCount)
        {
            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

                nRowCount = oCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }



        public object ExecuteScriptScalar(string strScript, IDataParameter[] aoDataParameter)
        {
            object oScalar = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

                oScalar = oCommand.ExecuteScalar();

            }
            catch (Exception e)
            {
                throw e;
            }
            return oScalar;
        }

        #region "데이터 리더를 돌려주는 스크립트 실행하기 - ExecuteScriptDataReader(strScript, aoDataParameter)"

        /// <summary>
        /// 데이터 리더를 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>데이터 리더</returns>
        /// <remarks></remarks>
        public OracleDataReader ExecuteScriptDataReader(string strScript, IDataParameter[] aoDataParameter)
        {
            OracleDataReader oDataReader = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

                oDataReader = oCommand.ExecuteReader();
            }
            catch (Exception oException)
            {
                throw oException;
            }

            return oDataReader;
        }

        #endregion
    }
}
