﻿using WaterNETServer.Common.utils;

namespace WaterNETServer.Common.DB.HistorianOLE
{
    public class OleDBFunctionManager
    {
        public static string GetConnectionString()
        {
            Configuration m_doc = new Configuration(AppStatic.DB_CONFIG_FILE_PATH);
            if (m_doc == null)
            {
                m_doc = m_doc = new Configuration(AppStatic.DB_CONFIG_FILE_PATH);
            }

            string serverName = Security.DecryptKey(m_doc.getProp("/Root/historian/servername"));
            string id = Security.DecryptKey(m_doc.getProp("/Root/historian/id"));
            string passwd = Security.DecryptKey(m_doc.getProp("/Root/historian/password"));

            string conStr = "Provider=ihOLEDB.iHistorian.1;Data Source=" + serverName
                                + ";User Id=" + id
                                + ";Password=" + passwd;
            return conStr;
        }
    }
}
