﻿
namespace WaterNETServer.Common.DB.HistorianOLE
{
    public class OleDBBaseDao
    {
        private OleDBManager databaseManager = null;

        public OleDBBaseDao()
        {
            databaseManager = new OleDBManager();
            databaseManager.ConnectionString = OleDBFunctionManager.GetConnectionString();
        }

        public OleDBManager oleDBManager
        {
            get { return databaseManager; }
        }

        public void ConnectionOpen()
        {
            databaseManager.Open();
        }

        public void ConnectionClose()
        {
            databaseManager.Close();
        }

        public void BeginTransaction()
        {
            databaseManager.BeginTransaction();
        }

        public void CommitTransaction()
        {
            databaseManager.CommitTransaction();
        }

        public void RollbackTransaction()
        {
            databaseManager.RollbackTransaction();
        }

        public void CloseTransaction()
        {
            if (databaseManager.Transaction != null)
            {
                databaseManager.Transaction.Dispose();
                databaseManager.Transaction = null;
            }
        }
    }
}
