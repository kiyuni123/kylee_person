﻿using System.Data;
using System.Windows.Forms;
using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Text;
using EMFrame.log;
namespace WaterNETServer.Common.utils
{
    public class Common
    {
        /// <summary>
        /// 콤보박스 설정 공통 함수
        /// </summary>
        /// <param name="dSet">DataSet</param>
        /// <param name="cBox">저장할 ComboBox</param>
        /// <param name="tableName">TableName</param>
        public void SetComboBox(DataSet dSet, ComboBox cBox, string tableName)
        {
            DataTableCollection dtc = dSet.Tables;
            foreach (DataTable dt in dtc)
            {
                // value, text 컬럼명 설정
                DataColumnCollection dcc = dSet.Tables[dt.TableName].Columns;
                cBox.ValueMember = dcc[0].ColumnName.ToString();
                cBox.DisplayMember = dcc[1].ColumnName.ToString();

                // 최상위에 Blank 추가
                DataRow row = dSet.Tables[tableName].NewRow();
                row[dcc[0].ColumnName.ToString()] = "";
                row[dcc[1].ColumnName.ToString()] = "선택";
                dSet.Tables[tableName].Rows.InsertAt(row, 0);
            }
            cBox.DataSource = dSet.Tables[tableName];
        }

        /// <summary>
        /// ComboBox 내용 출력 (테스트용)
        /// </summary>
        /// <param name="cBox"></param>
        public void printComboBox(ComboBox cBox)
        {
        }


        // 타이머 세팅
        public Hashtable GetTime()
        {
            Hashtable result = new Hashtable();

            string deli = ":";
            StringBuilder sb = new StringBuilder();

            // Get current time
            int year    = DateTime.Now.Year;
            int month   = DateTime.Now.Month;
            int day     = DateTime.Now.Day;
            int hour    = DateTime.Now.Hour;
            int min     = DateTime.Now.Minute;
            int sec     = DateTime.Now.Second;

            string yyyy = year.ToString();
            string mon  = (month < 10)  ? "0" + month.ToString()    : month.ToString();
            string dd   = (day < 10)    ? "0" + day.ToString()      : day.ToString();

            string hh   = (hour < 10)   ? "0" + hour.ToString()     : hour.ToString();
            string mm   = (min < 10)    ? "0" + min.ToString()      : min.ToString();
            string ss   = (sec < 10)    ? "0" + sec.ToString()      : sec.ToString();

            // Format current time into string
            result.Add("hh", hh);
            result.Add("mm", mm);
            result.Add("ss", ss);
            result.Add("s", ss.Substring(1, 1));

            // 매시 mm분ss초 mmss 형식
            result.Add("mmss", sb.Append(mm).Append(ss).ToString());
            // 매시 m분ss초 mss 형식 (10분마다)
            result.Add("mss", sb.ToString().Substring(1, 3));
            sb.Remove(0, sb.Length);

            // 매시 mm분ss초 mm:ss 형식
            result.Add("mm:ss", sb.Append(mm).Append(deli).Append(ss).ToString());
            sb.Remove(0, sb.Length);

            // 매일 hh시mm분ss초 hhmmss 형식
            result.Add("hhmmss", sb.Append(hh).Append(mm).Append(ss).ToString());
            sb.Remove(0, sb.Length);

            // 매일 hh시mm분ss초 hh:mm:ss 형식
            result.Add("hh:mm:ss", sb.Append(hh).Append(deli).Append(mm).Append(deli).Append(ss).ToString());
            sb.Remove(0, sb.Length);

            // 매달 dd일 hh:mm:ss
            result.Add("dd hh:mm:ss", sb.Append(dd).Append(" ").Append(hh).Append(deli).Append(mm).Append(deli).Append(ss).ToString());
            sb.Remove(0, sb.Length);

            result.Add("yyyymmddhhmiss", sb.Append(yyyy).Append(mon).Append(dd).Append(hh).Append(mm).Append(ss).ToString());
            sb.Remove(0, sb.Length);

            return result;
        }

        // DateTime을 스트링으로 반환
        public string GetTimeStr(DateTime dt)
        {
            return dt.ToString("yyyyMMddHHmmss");
        }

        /// <summary>
        /// DateTime을 Truncate 시켜반환
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        /// TODO : 버그 있음 수정해야함
        public DateTime DateTimeTrunc(DateTime dt, string option)
        {
            DateTime returnDateTime = new DateTime(); ;

            if ("yyyy".Equals(option)) returnDateTime = DateTime.ParseExact(dt.Year.ToString(), "yyyy", null);
            if ("yyyyMM".Equals(option)) returnDateTime = DateTime.ParseExact(dt.Year.ToString(), "yyyyMM", null);
            if ("yyyyMMdd".Equals(option)) returnDateTime = DateTime.ParseExact(dt.Year.ToString(), "yyyyMMdd", null);
            if ("yyyyMMddHH".Equals(option)) returnDateTime = DateTime.ParseExact(dt.Year.ToString(), "yyyyMMddHH", null);
            if ("yyyyMMddHHmm".Equals(option)) returnDateTime = DateTime.ParseExact(dt.Year.ToString(), "yyyyMMddHHmm", null);
            if ("yyyyMMddHHmmss".Equals(option)) returnDateTime = DateTime.ParseExact(dt.Year.ToString(), "yyyyMMddHHmmss", null);

            return returnDateTime;
        }

        /// <summary>
        /// 프로그램 실행시간을 알아봄
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public DateTime CheckExecTime(string mode, DateTime startTime, DateTime endTime, string message, string logLevel)
        {
            DateTime _startTime = startTime;
            DateTime _endTime = endTime;

            if ("start".Equals(mode))
            {
            }

            if ("end".Equals(mode))
            {
            }

            return _startTime;
        }


        public DateTime CheckExecTime(string mode, DateTime startTime, string message, string logLevel)
        {
            return CheckExecTime(mode, startTime, DateTime.Now, message, logLevel);
        }

        /// <summary>
        /// 첫번째 IPv4 값을 가져온다.
        /// </summary>
        /// <returns>못찾으면 null</returns>

        public string GetFirstIPv4()
        {
            Regex regex = new Regex(@"^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$");
            foreach (System.Net.IPAddress ip in System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList)
            {
                if (regex.IsMatch(ip.ToString()))
                {
                    return ip.ToString();
                }
            }
            return null;
        }


        /// <summary>
        /// 타이머를 중지 시킨다.
        /// </summary>
        /// <param name="timer"></param>
        public void TimerStop(System.Timers.Timer timer)
        {
            try
            {
                timer.Stop();
            }
            catch (ObjectDisposedException de)
            {
                Logger.Error(de.ToString());
            }
        }


        /// <summary>
        /// 입력 받은 두 날짜를 비교 함
        /// </summary>
        /// <param name="from">시작날짜</param>
        /// <param name="to">종료날짜</param>
        /// <returns>
        /// GT(Greater Than) : 시작날짜가 종료날짜보다 클 경우
        /// LT(Less Than) : 시작날짜보다 종료날짜가 작을 경우
        /// EQ(Equal) : 시작날짜와 종료날짜가 같은 경우
        /// </returns>
        public string CompareDateTime(DateTimePicker from, DateTimePicker to)
        {
            string rtn = string.Empty;

            string strFrom = from.Value.ToString("yyyyMMddHHmm");
            string strTo = to.Value.ToString("yyyyMMddHHmm");

            // 시작날짜가 종료날짜보다 크다.
            if (DateTime.ParseExact(strTo, "yyyyMMddHHmm", null) < DateTime.ParseExact(strFrom, "yyyyMMddHHmm", null))
            {
                rtn = "GT";
            }

            // 시작날짜가 종료날짜보다 작다.
            if (DateTime.ParseExact(strTo, "yyyyMMddHHmm", null) > DateTime.ParseExact(strFrom, "yyyyMMddHHmm", null))
            {
                rtn = "LT";
            }

            // 시작날짜와 종료날짜가 같다.
            if (DateTime.ParseExact(strTo, "yyyyMMddHHmm", null) == DateTime.ParseExact(strFrom, "yyyyMMddHHmm", null))
            {
                rtn = "EQ";
            }

            return rtn;
        }


        /// <summary>
        /// 입력받은 두 날짜 사이에 금일이 포함되어 있는지 확인함
        /// </summary>
        /// <param name="from">시작날짜</param>
        /// <param name="to">종료날짜</param>
        /// <returns></returns>
        public string IsContainToday(DateTimePicker from, DateTimePicker to)
        {
            string rtn = string.Empty;

            string strFrom = from.Value.ToString("yyyyMMddHHmm");
            string strTo = to.Value.ToString("yyyyMMddHHmm");

            for (DateTime d = DateTime.ParseExact(strFrom.Substring(0, 8), "yyyyMMdd", null); d <= DateTime.ParseExact(strTo.Substring(0, 8), "yyyyMMdd", null); )
            {
                if (d == DateTime.ParseExact(String.Format("{0:yyyy}", DateTime.Now) + String.Format("{0:MM}", DateTime.Now) + String.Format("{0:dd}", DateTime.Now), "yyyyMMdd", null))
                {
                    rtn = "YES";

                    return rtn;
                }
                d = d.AddDays(1);
            }

            return rtn;
        }


        /// <summary>
        /// 히스토리안에서 사용할 날짜 조건을 만들어서 반환
        /// </summary>
        /// <param name="cTime"></param>
        /// <returns></returns>
        public Hashtable GetHistorianWhereTime(string cTime)
        {
            Hashtable param = new Hashtable();

            DateTime d = new DateTime(Convert.ToInt16(cTime.Substring(0, 4))
                        , Convert.ToInt16(cTime.Substring(4, 2))
                        , Convert.ToInt16(cTime.Substring(6, 2))
                        , Convert.ToInt16(cTime.Substring(8, 2))
                        , Convert.ToInt16(cTime.Substring(10, 2))
                        , 0);

            //d = d.AddMinutes(-1);
            param["<="] = d.Year.ToString() + "-" + d.Month.ToString() + "-" + d.Day.ToString() + " " + d.Hour.ToString() + ":" + d.Minute.ToString();

            d = d.AddMinutes(-1);
            param[">"] = d.Year.ToString() + "-" + d.Month.ToString() + "-" + d.Day.ToString() + " " + d.Hour.ToString() + ":" + d.Minute.ToString();

            return param;
        }

        /// <summary>
        /// 스트링형 날짜에 입력받은 분데이터만큼 증가시켜서 스트링으로 반환
        /// </summary>
        /// <param name="cTime"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public string IncreaseTimeString(string cTime, int i)
        {
            DateTime d = new DateTime(Convert.ToInt16(cTime.Substring(0, 4))
                                    , Convert.ToInt16(cTime.Substring(4, 2))
                                    , Convert.ToInt16(cTime.Substring(6, 2))
                                    , Convert.ToInt16(cTime.Substring(8, 2))
                                    , Convert.ToInt16(cTime.Substring(10, 2))
                                    , 0);

            return String.Format("{0:yyyyMMddHHmm}", d.AddMinutes(i));
        }

        /// <summary>
        /// 오늘날짜(DateTime.Now)를 금일 00시 00분으로 리턴
        /// </summary>
        /// <returns></returns>
        public DateTime DateTimeTrunc(DateTime dt)
        {
            return DateTime.ParseExact(String.Format("{0:yyyy}", dt) + String.Format("{0:MM}", dt) + String.Format("{0:dd}", dt), "yyyyMMdd", null);
        }

        /// <summary>
        /// Historian의 SQL 조건절에 사용할 조건을 반환
        /// CurrentValue의 시간보다 약간 이전의 시간을 반환한다.
        /// 실시간 데이터가 누락되는 경우가 자주 발생함(i-water에서 반응이 늦은듯)
        /// </summary>
        /// <param name="cTime">CurrentValue의 시간</param>
        /// <param name="i">분</param>
        /// <returns></returns>
        public Hashtable GetTimestampHistorianCondition(DateTime cTime, int i)
        {
            Hashtable param = new Hashtable();

            cTime = cTime.AddMinutes(i);
            param["<="] = cTime.Year.ToString()
                    + "-" + cTime.Month.ToString()
                    + "-" + cTime.Day.ToString()
                    + " " + cTime.Hour.ToString()
                    + ":" + cTime.Minute.ToString();

            cTime = cTime.AddMinutes(-1);
            param[">"] = cTime.Year.ToString()
                    + "-" + cTime.Month.ToString()
                    + "-" + cTime.Day.ToString()
                    + " " + cTime.Hour.ToString()
                    + ":" + cTime.Minute.ToString();

            return param;
        }

        /// <summary>
        /// 두날짜를 입력받아 차이의 절대값을 월로 반환한다.
        /// 입력 인자는 DateTime
        /// </summary>
        /// <param name="from">DateTime from</param>
        /// <param name="to">DateTime to</param>
        /// <returns>차이 월</returns>
        public int MonthBetween(DateTime from, DateTime to)
        {
            DateTime    _from   = from;
            DateTime    _to     = to;

            int         M       = Math.Abs(_to.Year - _from.Year);
            int         months  = (M * 12) + Math.Abs(_to.Month - _from.Month);

            return months;
        }

        /// <summary>
        /// 두날짜를 입력받아 차이의 절대값을 월로 반환한다.
        /// 입력 인자는 string "yyyyMM" 형식
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public int MonthBetween(string from, string to)
        {
            DateTime    _from   = DateTime.ParseExact(from.Substring(0, 6), "yyyyMM", null);
            DateTime    _to     = DateTime.ParseExact(to.Substring(0, 6), "yyyyMM", null);

            int         M       = Math.Abs(_to.Year - _from.Year);
            int         months  = (M * 12) + Math.Abs(_to.Month - _from.Month);

            return months;
        }

        /// <summary>
        /// DUMMY 히스토리안 타임(string)을 DateTime으로 변환
        /// yyyy-M-d H:m
        /// </summary>
        /// <param name="strTime"></param>
        /// <returns></returns>
        public DateTime ConvertToDateTime(string strTime)
        {
            DateTime dt = new DateTime();

            dt = DateTime.ParseExact(strTime, "yyyy-M-d H:m", null);

            return dt;
        }

        /// <summary>
        /// 입력받은 문자가 "" 이면 null로 변환
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string nullStr(string str)
        {
            return str.Equals("") ? null : str;
        }
    }
}
