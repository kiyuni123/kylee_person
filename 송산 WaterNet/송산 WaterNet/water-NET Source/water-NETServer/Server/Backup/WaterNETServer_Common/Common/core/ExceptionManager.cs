﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using WaterNETServer.core;

namespace WaterNETServer.Common.core
{
    /// <summary>
    /// 예외 관리자
    /// </summary>
    public class ExceptionManager : ApplicationException
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////// Field

        ////////////////////////////////////////////////////////////////////////////////////////// Protected

        #region Field/Protected

        protected string m_strMachineName = Environment.MachineName;     // 장치명
        protected string m_strProgramName = "Canavian";                         // Program명
        protected string m_strNamespaceName = "";                                  // Namespace명
        protected string m_strClassName = "";                         // Class명
        protected string m_strMethodName = "";                         // Method명
        protected DateTime m_dtCreationTime = DateTime.Now;               // 생성 시각
        protected Exception m_pInnerException = null;                       // 내부 예외자
        protected string m_strDescription = "";                         // 설명

        protected string m_strInnerException = "";
        protected string m_strErrorMessage = "";
        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////// Private

        #region Field/Private

        private const int m_nResultHandle = unchecked((int)0x81234567); // 결과 핸들
        private static LogMediaType m_eLogMediaType = LogMediaType.File;        // Log Media 종류

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// 생성자

        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region 생성자 - ExceptionManager(strNamespaceName, strClassName, strMethodName, pInnerException, strDescription)
        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="strNamespaceName">Namespace명</param>
        /// <param name="strClassName">Class명</param>
        /// <param name="strMethodName">Method명</param>
        /// <param name="pInnerException">내부 예외자</param>
        /// <param name="strDescription">설명</param>
        public ExceptionManager(string strNamespaceName, string strClassName, string strMethodName, Exception pInnerException, string strDescription)
            : base(string.Format("{0}.{1}.{2}\r\n{3}\r\n{4}\r\n{5}", strNamespaceName, strClassName, strMethodName, pInnerException.GetType().Name, pInnerException.Message, strDescription))
        {
            HResult = m_nResultHandle;

            m_strNamespaceName = strNamespaceName;
            m_strClassName = strClassName;
            m_strMethodName = strMethodName;
            m_pInnerException = pInnerException;

            try
            {
                LogManager.Write(this, m_eLogMediaType);
            }
            catch (Exception oException)
            {
                throw oException;
            }
        }

        #endregion

        #region "생성자 - New(strNameSpaceName, strClassName, strMethodName, strErrorMessage, strInnerException)"
        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="strNamespaceName"></param>
        /// <param name="strClassName"></param>
        /// <param name="strMethodName"></param>
        /// <param name="strErrorMessage"></param>
        /// <param name="strInnerException"></param>
        /// <remarks></remarks>
        public ExceptionManager(string strNamespaceName, string strClassName, string strMethodName, string strErrorMessage, string strInnerException)
            : base(string.Format("{0}\r\n{1}.{2}.{3} ▶ {4}", strErrorMessage, strNamespaceName, strClassName, strMethodName, strInnerException))
        {

            HResult = m_nResultHandle;

            m_strNamespaceName = strNamespaceName;
            m_strClassName = strClassName;
            m_strMethodName = strMethodName;
            m_strErrorMessage = strErrorMessage;
            m_strInnerException = strInnerException;

            try
            {
                LogManager.Write(this, m_eLogMediaType);
            }
            catch (Exception oException)
            {
                throw oException;
            }
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////// Protected

        #region "생성자 - New(oSerializationInfo, oStreamingContext)"

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="oSerializationInfo">직렬화 정보</param>
        /// <param name="oStreamingContext">스트리밍 컨텍스트</param>
        /// <remarks></remarks>
        protected ExceptionManager(SerializationInfo oSerializationInfo, StreamingContext oStreamingContext)
            : base(oSerializationInfo, oStreamingContext)
        {
            HResult = m_nResultHandle;
        }

        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////// 속성

        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region 장치명 - MachineName

        /// <summary>
        /// 장치명
        /// </summary>
        public string MachineName
        {
            get
            {
                return m_strMachineName;
            }
            set
            {
                m_strMachineName = value;
            }
        }

        #endregion

        #region Program명 - ProgramName

        /// <summary>
        /// Program명
        /// </summary>
        public string ProgramName
        {
            get
            {
                return m_strProgramName;
            }
            set
            {
                m_strProgramName = value;
            }
        }

        #endregion

        #region Namespace명 - NamespaceName

        /// <summary>
        /// Namespace명
        /// </summary>
        public string NamespaceName
        {
            get
            {
                return m_strNamespaceName;
            }
            set
            {
                m_strNamespaceName = value;
            }
        }

        #endregion

        #region Class명 - ClassName

        /// <summary>
        /// Class명
        /// </summary>
        public string ClassName
        {
            get
            {
                return m_strClassName;
            }
            set
            {
                m_strClassName = value;
            }
        }

        #endregion

        #region Method명 - MethodName

        /// <summary>
        /// Method명
        /// </summary>
        public string MethodName
        {
            get
            {
                return m_strMethodName;
            }
            set
            {
                m_strMethodName = value;
            }
        }

        #endregion

        #region 생성 시각 - CreationTime

        /// <summary>
        /// 생성 시각
        /// </summary>
        public DateTime CreationTime
        {
            get
            {
                return m_dtCreationTime;
            }
            set
            {
                m_dtCreationTime = value;
            }
        }

        #endregion

        #region 내부 예외자 - InnerException

        /// <summary>
        /// 내부 예외자
        /// </summary>
        public new Exception InnerException
        {
            get
            {
                return m_pInnerException;
            }
            set
            {
                m_pInnerException = value;
            }
        }

        #endregion

        #region 설명 - Description

        /// <summary>
        /// 설명
        /// </summary>
        public string Description
        {
            get
            {
                return m_strDescription;
            }
            set
            {
                m_strDescription = value;
            }
        }

        #endregion
    }
}
