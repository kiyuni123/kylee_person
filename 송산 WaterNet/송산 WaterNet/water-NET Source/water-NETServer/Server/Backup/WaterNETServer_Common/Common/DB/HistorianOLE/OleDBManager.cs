﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;

namespace WaterNETServer.Common.DB.HistorianOLE
{
    public class OleDBManager
    {
        private OleDbConnection con = null;
        private string conStr = null;
        private OleDbTransaction transaction = null;

        public OleDbConnection Connection
        {
            get { return con; }
            set { con = value; }
        }

        public string ConnectionString
        {
            get { return conStr; }
            set { conStr = value; }
        }

        public OleDbTransaction Transaction
        {
            get { return transaction; }
            set { transaction = value; }
        }

        public void Open()
        {
            if (con == null)
            {
                try
                {
                    con = new OleDbConnection();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            if (con.State == ConnectionState.Closed)
            {
                try
                {
                    con.ConnectionString = conStr;
                    con.Open();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public void Close()
        {
            if (con != null)
            {
                if (con.State != ConnectionState.Closed)
                {
                    try
                    {
                        con.Close();
                        con.Dispose();
                        con = null;
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
        }

        public void BeginTransaction()
        {
            if (transaction == null)
            {
                try
                {
                    transaction = con.BeginTransaction();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public void CommitTransaction()
        {
            if (transaction != null)
            {
                try
                {
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public void RollbackTransaction()
        {
            if (transaction != null)
            {
                try
                {
                    transaction.Rollback();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public DataSet ExecuteScriptDataSet(string strScript, IDataParameter[] param, string strTableName)
        {
            DataSet dataSet = null;

            if (string.IsNullOrEmpty(strScript))
                return null;

            try
            {
                dataSet = new DataSet();
                OleDbDataAdapter adapter = new OleDbDataAdapter();

                adapter.SelectCommand = CreateScriptCommand(strScript, param);
                adapter.Fill(dataSet, strTableName);
            }
            catch (Exception e)
            {
                throw e;
            }
            return dataSet;
        }

        public OleDbCommand CreateScriptCommand(string strScript, IDataParameter[] param)
        {
            OleDbCommand cmd = new OleDbCommand(strScript, con);
            try
            {
                cmd.CommandType = CommandType.Text;
                if (param != null)
                {
                    foreach (OleDbParameter p in param)
                    {
                        cmd.Parameters.Add(p);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return cmd;
        }

        public void FillDataSetScript(DataSet oDataSet, string strTableName, string strScript, IDataParameter[] aoDataParameter)
        {
            try
            {
                OleDbDataAdapter oDataAdapter = new OleDbDataAdapter();

                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int ExecuteScript(string strScript, IDataParameter[] aoDataParameter)
        {
            int nRowCount = 0;

            ExecuteScript(strScript, aoDataParameter, ref nRowCount);

            return nRowCount;
        }


        public void ExecuteScript(string strScript, IDataParameter[] aoDataParameter, ref int nRowCount)
        {
            try
            {
                OleDbCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

                nRowCount = oCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public object ExecuteScriptScalar(string strScript, IDataParameter[] aoDataParameter)
        {
            object oScalar = null;

            try
            {
                OleDbCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

                oScalar = oCommand.ExecuteScalar();

            }
            catch (Exception e)
            {
                throw e;
            }
            return oScalar;
        }

    }
}
