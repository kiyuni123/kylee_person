﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace WaterNETServer.Common.utils
{
    public class GlobalVariable
    {
        private Configuration m_doc = null;
        private string sgccd = string.Empty;
        private string svckey = string.Empty;
        private string ipaddress = string.Empty;
        private string sgname = string.Empty;
        private string url = string.Empty;

        public GlobalVariable()
        {
            this.sgccd = getSgccdCode();
            this.svckey = get_strSVCKEY(this.sgccd);
            this.ipaddress = get_strIP(this.sgccd);
            this.sgname = get_strSgName(this.sgccd);
            this.url = get_strURL(this.sgccd);
        }
        public string SGCCD
        {
            get { return this.sgccd; }
        }
        public string SVCKEY
        {
            get { return this.svckey; }
        }
        public string IPADDR
        {
            get { return this.ipaddress; }
        }
        public string SGNAME
        {
            get { return this.sgname; }
        }
        public string URL
        {
            get { return this.url; }
        }
        /// <summary>
        /// 지자체코드를 불러온다.
        /// </summary>
        /// <returns></returns>
        public string getSgccdCode()
        {
            string sgccd = string.Empty;
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                sgccd = m_doc.getProp("/Root/Global/sgccd");
            }
            return sgccd;
        }

        /// <summary>
        /// Water-NET 서비스키를 반환한다 (설정 파일에 있는)
        /// </summary>
        /// <returns></returns>
        public string get_strSVCKEY()
        {
            string _sgccd = this.getSgccdCode();

            string _strSVCKEY = string.Empty;
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                XmlNodeList nodelist = m_doc.doc.SelectNodes("/Root/sgccd/sgcode");
                foreach (XmlElement nodeConfig in nodelist)
                {
                    if (nodeConfig.Attributes[0].Value.Equals(_sgccd))
                    {
                        _strSVCKEY = nodeConfig.GetElementsByTagName("svckey")[0].InnerText;
                        break;
                    }
                }
                //_strSVCKEY = m_doc.getProp("/Root/Global/svckey");
            }
            return _strSVCKEY;
        }

        private string get_strSVCKEY(string _sgccd)
        {
            string _strSVCKEY = string.Empty;
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                XmlNodeList nodelist = m_doc.doc.SelectNodes("/Root/sgccd/sgcode");
                foreach (XmlElement nodeConfig in nodelist)
                {
                    if (nodeConfig.Attributes[0].Value.Equals(_sgccd))
                    {
                        _strSVCKEY = nodeConfig.GetElementsByTagName("svckey")[0].InnerText;
                        break;
                    }
                }
                //_strSVCKEY = m_doc.getProp("/Root/Global/svckey");
            }
            return _strSVCKEY;
        }

        private string get_strSgName(string _sgccd)
        {
            string _strSVCKEY = string.Empty;
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                XmlNodeList nodelist = m_doc.doc.SelectNodes("/Root/sgccd/sgcode");
                foreach (XmlElement nodeConfig in nodelist)
                {
                    if (nodeConfig.Attributes[0].Value.Equals(_sgccd))
                    {
                        _strSVCKEY = nodeConfig.GetElementsByTagName("sgname")[0].InnerText;
                        break;
                    }
                }
                //_strSVCKEY = m_doc.getProp("/Root/Global/svckey");
            }
            return _strSVCKEY;
        }

        private string get_strURL(string _sgccd)
        {
            string _strURL = string.Empty;
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                XmlNodeList nodelist = m_doc.doc.SelectNodes("/Root/sgccd/sgcode");
                foreach (XmlElement nodeConfig in nodelist)
                {
                    if (nodeConfig.Attributes[0].Value.Equals(_sgccd))
                    {
                        _strURL = nodeConfig.GetElementsByTagName("url")[0].InnerText;
                        break;
                    }
                }
                //_strSVCKEY = m_doc.getProp("/Root/Global/svckey");
            }
            return _strURL;
        }

        private string get_strIP(string _sgccd)
        {
            string ipsetting = string.Empty;
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                XmlNodeList nodelist = m_doc.doc.SelectNodes("/Root/sgccd/sgcode");
                foreach (XmlElement nodeConfig in nodelist)
                {
                    if (nodeConfig.Attributes[0].Value.Equals(_sgccd))
                    {
                        ipsetting = nodeConfig.GetElementsByTagName("ipsetting")[0].InnerText;
                        break;
                    }
                }
                //_strSVCKEY = m_doc.getProp("/Root/Global/svckey");
            }
            return ipsetting;
        }

        /// <summary>
        /// Service Reference 명을 가져온다. 
        /// </summary>
        /// <returns></returns>
        public string get_strService()
        {
            string _strService = string.Empty;
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                _strService = m_doc.getProp("/Root/Global/service");
            }
            return _strService;
        }


        /// <summary>
        /// Water-NET IP를 반환한다 (설정 파일에 있는)
        /// </summary>
        /// <returns></returns>
        public string getRegmngrip()
        {
            string regmngrip = string.Empty;
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                if ("fix".Equals(m_doc.getProp("/Root/Global/ipsetting")))
                {
                    regmngrip = m_doc.getProp("/Root/Global/regmngrip");
                }
                else
                {
                    Common utils   = new Common();
                    regmngrip = utils.GetFirstIPv4();
                }
            }
            return regmngrip;
        }

        /// <summary>
        /// Application Name을 가져온다. (Form Window의 타이틀 부분에 해당)
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public string getFormName(int i)
        {
            string str = "";
            string _xmlPath = "/Root/application/title" + i;
            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }

                if (i == 0)
                {
                    str = m_doc.getProp("/Root/Application/MainFormName");
                }
                else
                {
                    str = m_doc.getProp(_xmlPath);
                }
            }
            return str;
        }


        /// <summary>
        /// 배포된 프로그램일 경우 ftp서버를 사용할 것인지에 대한 값을 가져온다.
        /// true : FTP 서버를 사용해 프로그램을 업데이트하게 함
        /// false : FTP 서버를 사용하는 부분을 패스함
        /// </summary>
        /// <returns></returns>
        public bool isDeployFtp()
        {
            bool _isDeployFtp = false;

            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                if ("true".Equals(m_doc.getProp("/Root/DeployMode/ftp")))
                {
                    _isDeployFtp = true;
                }
                else
                {
                    _isDeployFtp = false;
                }
            }
            return _isDeployFtp;
        }

        /// <summary>
        /// #### 임시 ####
        /// 숙소에서 작업할때 DB체크를 안하고 메인 폼 띄우게 함
        /// 개발 시에만 필요
        /// </summary>
        /// <returns></returns>
        public bool isHome()
        {
            bool _isHome = false;

            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                if ("true".Equals(m_doc.getProp("/Root/DeployMode/home")))
                {
                    _isHome = true;
                }
                else
                {
                    _isHome = false;
                }
            }

            return _isHome;
        }

        /// <summary>
        /// #### 임시 ####
        /// 관망해석 안돌리게 할때 필요
        /// 개발 시에만 필요
        /// </summary>
        /// <returns></returns>
        public bool isEPA()
        {
            bool _isHome = false;

            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                if ("true".Equals(m_doc.getProp("/Root/DeployMode/epa")))
                {
                    _isHome = true;
                }
                else
                {
                    _isHome = false;
                }
            }

            return _isHome;
        }

        /// <summary>
        /// #### 임시 ####
        /// 수요예측 안돌리게할 때 필요
        /// 개발 시에만 필요
        /// </summary>
        /// <returns></returns>
        public bool isDF()
        {
            bool _isDF = false;

            if (File.Exists(AppStatic.GLOBAL_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.GLOBAL_CONFIG_FILE_PATH);
                }
                if ("true".Equals(m_doc.getProp("/Root/DeployMode/df")))
                {
                    _isDF = true;
                }
                else
                {
                    _isDF = false;
                }
            }

            return _isDF;
        }
    }
}
