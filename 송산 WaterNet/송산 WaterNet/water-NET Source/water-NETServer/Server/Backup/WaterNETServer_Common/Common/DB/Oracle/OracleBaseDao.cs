﻿
namespace WaterNETServer.Common.DB.Oracle
{
    public class OracleBaseDao
    {
        private OracleDBManager databaseManager = null;

        public OracleBaseDao()
        {
            databaseManager = new OracleDBManager();
            databaseManager.ConnectionString = OracleDBFunctionManager.GetWaterNETConnectionString();
        }

        public OracleDBManager oracleDBManager
        {
            get { return databaseManager; }
        }

        public void ConnectionOpen()
        {
            databaseManager.Open();
        }

        public void ConnectionClose()
        {
            databaseManager.Close();
        }

        public void BeginTransaction()
        {
            if (databaseManager.Transaction != null)
            {
                databaseManager.Transaction.Dispose();
                databaseManager.Transaction = null;
            }
            databaseManager.BeginTransaction();
        }

        public void CommitTransaction()
        {
            databaseManager.CommitTransaction();
        }

        public void RollbackTransaction()
        {
            databaseManager.RollbackTransaction();
        }

        public void CloseTransaction()
        {
            if (databaseManager.Transaction != null)
            {
                databaseManager.Transaction.Dispose();
                databaseManager.Transaction = null;
            }
        }
    }
}
