﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace WaterNETServer.Common.utils
{
    public class Security
    {
        /// <summary>
        /// 현재는 8자리의 암호키를 설정해야함.
        /// 추후 기능개선 필요
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string EncryptKey(string strToEncrypt)
        {
            try
            {
                byte[] key = { };
                byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
                byte[] inputByteArray; //Convert.ToByte(stringToEncrypt.Length) 

                key = Encoding.UTF8.GetBytes("WATERNET");
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Encoding.UTF8.GetBytes(strToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        /// <summary>
        /// 현재는 8자리의 암호키를 설정해야함.[추후 기능개선 필요]
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string DecryptKey(string strToDecrypt)
        {
            try
            {
                byte[] key = { };
                byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
                byte[] inputByteArray = new byte[strToDecrypt.Length];
                key = Encoding.UTF8.GetBytes("WATERNET");
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();

                inputByteArray = Convert.FromBase64String(strToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}
