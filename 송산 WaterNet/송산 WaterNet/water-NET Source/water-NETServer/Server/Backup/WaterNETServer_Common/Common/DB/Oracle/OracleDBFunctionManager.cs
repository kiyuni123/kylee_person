﻿using WaterNETServer.Common.utils;

namespace WaterNETServer.Common.DB.Oracle
{
    public class OracleDBFunctionManager
    {
        public static string GetWaterNETConnectionString()
        {
            Configuration m_doc = new Configuration(AppStatic.DB_CONFIG_FILE_PATH);
            //Security sec = new Security();

            if (m_doc == null)
            {
                m_doc = new Configuration(AppStatic.DB_CONFIG_FILE_PATH);
            }
 
            string svcName = Security.DecryptKey(m_doc.getProp("/Root/waternet/servicename"));
            string ip = Security.DecryptKey(m_doc.getProp("/Root/waternet/ip"));
            string id = Security.DecryptKey(m_doc.getProp("/Root/waternet/id"));
            string passwd = Security.DecryptKey(m_doc.getProp("/Root/waternet/password"));

            string conStr = "Data Source=(DESCRIPTION="
                            + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=1521)))"
                            + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
                            + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";
            return conStr;
        }

        public static string GetRWISConnectionString()
        {
            Configuration m_doc = new Configuration(AppStatic.DB_CONFIG_FILE_PATH);
            //Security sec = new Security();

            if (m_doc == null)
            {
                m_doc = new Configuration(AppStatic.DB_CONFIG_FILE_PATH);
            }

            string svcName = Security.DecryptKey(m_doc.getProp("/Root/rwis/servicename"));
            string ip = Security.DecryptKey(m_doc.getProp("/Root/rwis/ip"));
            string id = Security.DecryptKey(m_doc.getProp("/Root/rwis/id"));
            string passwd = Security.DecryptKey(m_doc.getProp("/Root/rwis/password"));

            string conStr = "Data Source=(DESCRIPTION="
                            + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=1521)))"
                            + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
                            + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";
            return conStr;
        }
    }
}
