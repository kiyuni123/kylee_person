﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EMFrame.statics
{
	public class AppStatic
	{
        #region 로그온한 사용자 정보
        public static string ZONE_GBN = string.Empty;       //지방, 광역 구분자
        public static string USER_ID = string.Empty;        //로그온한 사용자 ID
        public static string USER_NAME = string.Empty;      //로그온한 사용자 성명
        public static string USER_DIVISION = string.Empty;  //로그온한 사용자 부서
        public static string USER_RIGHT = string.Empty;     //로그온한 사용자 권한
        public static string USER_IP = string.Empty;        //로그온한 사용자의 IP
        public static string EXT_YN = string.Empty;         //타부서 조회권한(본부,본사)
        public static string USER_GROUP = string.Empty;     //사용자권한그룹
        public static string USER_HEADQUARTECD = string.Empty;//로그온한 사용자의 지역본부 코드
        public static string USER_HEADQUARTENM = string.Empty;//로그온한 사용자의 지역본부 명
        public static string USER_SGCCD = string.Empty;     //로그온한 사용자의 지자체 코드
        public static string USER_SGCNM = string.Empty;     //로그온한 사용자의 지자체 명
        public static string LOGIN_DT = string.Empty;       //로그온한 시각

        public static System.Collections.Hashtable USER_MENU = new System.Collections.Hashtable(); //로그온한 사용자의 메뉴권한
        #endregion

        #region 로그인한 사업장의 fullExtent정보
        public static double MIN_X = 0.0;
        public static double MAX_X = 0.0;
        public static double MIN_Y = 0.0;
        public static double MAX_Y = 0.0;
        #endregion 로그인한 사업장의 fullExtent정보

        #region 등록된 관리부서에서 접속가능한 라이센스 수
        public static Dictionary<string, int> LICENSE_ACCOUNT = new Dictionary<string, int>();
        #endregion 등록된 관리부서에서 접속가능한 라이센스 수

        #region Config File 정보
        public static string DB_CONFIG_FILE_PATH = Application.StartupPath + @"\Config\DB_Config.xml";        //Database Config File 경로 및 파일 명
        #endregion

        public static string DATA_DIR = Application.StartupPath + @"\WaterNET-Data";

	}
}

