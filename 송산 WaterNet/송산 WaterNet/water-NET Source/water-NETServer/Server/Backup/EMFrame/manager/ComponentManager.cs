﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChartFX.WinForms;
using System.Drawing;
using ChartFX.WinForms.Adornments;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;
using Infragistics.Win;
using System.Collections;
using System.Data;

namespace EMFrame.manager
{
    public class ComponentManager
    {
        public void InitializeComponent(Control control)
        {
            //Chart chart = control as Chart;
            //if (chart != null)
            //{
            //    this.InitializeComponent(chart);
            //    return;
            //}
            UltraGrid grid = control as UltraGrid;
            if (grid != null)
            {
                this.InitializeComponent(grid);
                return;
            }
        }

        public void InitializeComponent(UltraGrid grid)
        {
            grid.KeyDown += new KeyEventHandler(grid_KeyDown);
            this.InitializeStyle(grid);
        }



        private void grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ((UltraGrid)sender).PerformAction(UltraGridAction.ExitEditMode);
            }
        }

        private void InitializeStyle(UltraGrid grid)
        {
            grid.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            grid.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            
            grid.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            grid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            grid.DisplayLayout.GroupByBox.BorderStyle = UIElementBorderStyle.Solid;
            grid.DisplayLayout.GroupByBox.Hidden = true;
            grid.DisplayLayout.MaxColScrollRegions = 1;
            grid.DisplayLayout.MaxRowScrollRegions = 1;

            grid.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Solid;
            grid.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Solid;
            grid.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
            grid.DisplayLayout.Override.CellPadding = 0;
            grid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            grid.DisplayLayout.Override.HeaderStyle = HeaderStyle.Standard;
            grid.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            grid.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            grid.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            grid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            grid.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            grid.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;

            grid.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;
            grid.DisplayLayout.Override.RowSelectorAppearance.TextVAlign = VAlign.Middle;
            grid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.Default;
            //DSOH 추가. Row Select는 무조건 Single만 되고, Drag가 되지 않게.
            grid.DisplayLayout.Override.SelectTypeRow = SelectType.SingleAutoDrag;

            grid.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;
            grid.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            grid.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            grid.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            grid.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            grid.Font = new Font("굴림", 9f, FontStyle.Regular, GraphicsUnit.Point, 129);

            for (int I = 0; I <= grid.DisplayLayout.Bands[0].Columns.Count - 1; I++)
            {
                grid.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                grid.DisplayLayout.Bands[0].Columns[I].Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                grid.DisplayLayout.Bands[0].Columns[I].Header.Appearance.BackGradientStyle = GradientStyle.None;
                grid.DisplayLayout.Bands[0].Columns[I].CellActivation = Activation.NoEdit;
            }

            grid.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            grid.DisplayLayout.Override.HeaderAppearance.TextVAlign = VAlign.Middle;

            grid.DisplayLayout.Override.GroupByColumnHeaderAppearance.TextHAlign = HAlign.Center;
            grid.DisplayLayout.Override.GroupByColumnHeaderAppearance.TextVAlign = VAlign.Middle;

            grid.DisplayLayout.Appearance.BorderColor = SystemColors.InactiveCaption;
            grid.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.ColumnChooserButton;
            grid.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            grid.Font = new Font("굴림", 9f);

            grid.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            grid.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            foreach (UltraGridColumn gridColumn in grid.DisplayLayout.Bands[0].Columns)
            {
                gridColumn.Header.Appearance.TextVAlign = VAlign.Middle;
                gridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            }

            grid.Select();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chart"></param>
        public void InitializeLayout(UltraGrid grid)
        {
            if (grid.DataSource != null)
            {
                if (grid.DataSource is BindingSource)
                {
                    BindingSource source = grid.DataSource as BindingSource;
                    source.Clear();
                }

                if (grid.DataSource is IList)
                {
                    IList source = grid.DataSource as IList;
                    source.Clear();
                }

                if (grid.DataSource is DataTable)
                {
                    DataTable source = grid.DataSource as DataTable;
                    source.Clear();
                }
            }
        }

        //public void InitializeComponent(Chart chart)
        //{
        //    chart.MouseClick += new HitTestEventHandler(chart_MouseClick);
        //    this.InitializeStyle(chart);
        //}

        //private Dictionary<SeriesAttributes, List<Double>> hiddenSeries = new Dictionary<SeriesAttributes, List<double>>();
        //private void chart_MouseClick(object sender, HitTestEventArgs e)
        //{
        //    Chart chart = (Chart)sender;

        //    e = chart.HitTest(e.AbsoluteLocation.X, e.AbsoluteLocation.Y, true);

        //    if (e.HitType.ToString() == "LegendBox")
        //    {
        //        if (e.Series != -1)
        //        {
        //            SeriesAttributes series = chart.Series[e.Series];

        //            //숨겨진 목록에 있음 -> 목록에 제거하고 보임
        //            if (this.hiddenSeries.ContainsKey(series))
        //            {
        //                List<double> data = hiddenSeries[series];

        //                for (int i = 0; i < chart.Data.Points; i++)
        //                {
        //                    chart.Data[e.Series, i] = data[i];
        //                }

        //                this.hiddenSeries.Remove(series);

        //                chart.LegendBox.ItemAttributes[chart.Series, e.Series].TextColor = Color.Black;

        //            }
        //            //숨겨진 목록에 없음 -> 목록에 추가하고 숨김
        //            else if (!this.hiddenSeries.ContainsKey(series))
        //            {
        //                List<double> data = new List<double>();
        //                for (int i = 0; i < chart.Data.Points; i++)
        //                {
        //                    data.Add(chart.Data[e.Series, i]);
        //                    chart.Data[e.Series, i] = Chart.Hidden;
        //                }

        //                this.hiddenSeries.Add(series, data);

        //                chart.LegendBox.ItemAttributes[chart.Series, e.Series].TextColor = Color.DarkGray;
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// 초기 스타일 설정
        /// </summary>
        /// <param name="chart"></param>
        //private void InitializeStyle(Chart chart)
        //{
        //    chart.LegendBox.Dock = DockArea.Bottom;
        //    chart.LegendBox.MarginY = 1;
        //    chart.LegendBox.ContentLayout = ContentLayout.Spread;
        //    chart.LegendBox.Visible = false;

        //    chart.AxisX.Staggered = true;
        //    chart.BackColor = Color.White;

        //    GradientBackground gradient = new GradientBackground(GradientType.Vertical);
        //    gradient.ColorTo = Color.White;
        //    gradient.ColorFrom = Color.White;
        //    chart.Background = gradient;

        //    chart.Series.Clear();
        //    chart.AxesX.Clear();
        //    chart.AxesY.Clear();
        //    chart.DataSourceSettings.Fields.Clear();

        //    foreach (SeriesAttributes series in chart.Series)
        //    {
        //        series.Visible = false;
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chart"></param>
        public void InitializeLayout(Chart chart)
        {
            foreach (SeriesAttributes series in chart.Series)
            {
                series.Visible = true;
                chart.LegendBox.ItemAttributes[chart.Series, chart.Series.IndexOf(series)].TextColor = Color.Black;
            }

            chart.LegendBox.Visible = true;
            chart.Series.Clear();
            chart.AxesX.Clear();
            chart.AxesY.Clear();
            chart.DataSourceSettings.Fields.Clear();
            chart.Data.Clear();
            chart.LegendBox.CustomItems.Clear();
            chart.Extensions.Clear();
        }
    }
}
