﻿using System;
using System.Collections;
using System.Data;
using WaterNETServer.WH_Common.utils;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;
using WaterNETServer.Warning.dao;

namespace WaterNETServer.Warning.work
{
    public class WarningWork : BaseWork
    {
        private static WarningWork work = null;
        private WarningDao dao = null;

        private WarningWork()
        {
            dao = WarningDao.GetInstance();
        }

        public static WarningWork GetInstance()
        {
            if (work == null)
            {
                work = new WarningWork();
            }

            return work;
        }

        //경고내역 입력 (경고 일련번호를 반환한다.)
        public string InsertWarningData(Hashtable conditions)
        {
            string serialNo = "";
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                serialNo = Utils.GetSerialNumber("WA");

                conditions.Add("WARN_NO", serialNo);

                dao.InsertWarningData(manager, conditions);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                //throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return serialNo;
        }

        //경고 처리
        public void UpdateWarningFix(Hashtable conditions)
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                dao.UpdateWarningFix(manager, conditions);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                //throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        //경고내역 삭제
        public void DeleteWarningData()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                dao.DeleteWarningData(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                //throw ex;
            }
            finally
            {
                manager.Dispose();
            }
        }

        //경고리스트 조회
        public DataSet SelectWarningList()
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                result = dao.SelectWarningList(manager);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }

        //경고확인
        public DataSet UpdateWarningList(Hashtable conditions)
        {
            DataSet result = null;
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                dao.UpdateCheckYn(manager, conditions);
                result = dao.SelectWarningList(manager);

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
                //throw ex;
            }
            finally
            {
                manager.Dispose();
            }

            return result;
        }
    }
}
