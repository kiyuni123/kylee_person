﻿using System.Collections;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using EMFrame.dm;

namespace WaterNETServer.Warning.dao
{
    public class WarningDao
    {
        private static WarningDao dao = null;

        private WarningDao()
        {
        }

        public static WarningDao GetInstance()
        {
            if(dao == null)
            {
                dao = new WarningDao();
            }

            return dao;
        }

        //경고내역 입력
        public void InsertWarningData(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WA_WARNING  (                                               ");
            queryString.AppendLine("                            WARN_NO                                     ");
            queryString.AppendLine("                            ,CREATE_DATE                                ");
            queryString.AppendLine("                            ,WORK_GBN                                   ");
            queryString.AppendLine("                            ,LOC_CODE                                   ");
            queryString.AppendLine("                            ,WAR_CODE                                   ");
            queryString.AppendLine("                            ,CHECK_YN                                   ");
            queryString.AppendLine("                            ,SERIAL_NO                                  ");
            queryString.AppendLine("                            ,REMARK                                     ");
            
            if (conditions["FIXED_YN"] != null)
            {
                queryString.AppendLine("                        ,FIXED_DATE                                 ");
                queryString.AppendLine("                        ,FIXED_YN                                   ");
            }
            
            queryString.AppendLine("                        ) values (                                      ");
            queryString.AppendLine("                            :1                                          ");
            queryString.AppendLine("                            ,to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss')  ");
            queryString.AppendLine("                            ,:2                                         ");
            queryString.AppendLine("                            ,:3                                         ");
            queryString.AppendLine("                            ,:4                                         ");
            queryString.AppendLine("                            ,'N'                                        ");
            queryString.AppendLine("                            ,:5                                         ");
            queryString.AppendLine("                            ,:6                                         ");

            if (conditions["FIXED_YN"] != null)
            {
                queryString.AppendLine("                        ,to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss')  ");
                queryString.AppendLine("                        ,'Y'                                        ");
            }

            queryString.AppendLine("                        )                                               ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["WARN_NO"];
            parameters[1].Value = (string)conditions["WORK_GBN"];
            parameters[2].Value = (string)conditions["LOC_CODE"];
            parameters[3].Value = (string)conditions["WAR_CODE"];
            parameters[4].Value = (string)conditions["SERIAL_NO"];
            parameters[5].Value = (string)conditions["REMARK"];

            manager.ExecuteScript(queryString.ToString(), parameters);
        }

        //경고내역 확인
        public void UpdateWarningState(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WA_WARNING  set                                 ");
            queryString.AppendLine("        CHECK_YN    = 'Y'                               ");
            queryString.AppendLine("where   WARN_NO     = '" + conditions["WARN_NO"] + "'   ");

            manager.ExecuteScript(queryString.ToString(), null);
        }

        //경고내역 처리
        public void UpdateWarningFix(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WA_WARNING      set         ");
            queryString.AppendLine("        FIXED_YN        = '" + conditions["FIXED_YN"] + "'      ");
            queryString.AppendLine("where   SERIAL_NO       = '" + conditions["SERIAL_NO"] + "'     ");

            manager.ExecuteScript(queryString.ToString(), null);
        }

        //경고삭제
        public void DeleteWarningData(EMapper manager)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WA_WARNING                                                                  ");
            queryString.AppendLine("where       to_date(CREATE_DATE, 'yyyy-mm-dd hh24:mi:ss') < add_months(sysdate,-1)      ");

            manager.ExecuteScript(queryString.ToString(), null);
        }

        //경고리스트 조회 (메인화면)
        public DataSet SelectWarningList(EMapper manager)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      CREATE_DATE			                                                            ");
            queryString.AppendLine("            ,WORK_GBN			                                                            ");
            queryString.AppendLine("            ,LOC_CODE			                                                            ");
            queryString.AppendLine("            ,WAR_CODE			                                                            ");
            queryString.AppendLine("            ,CHECK_YN			                                                            ");
            queryString.AppendLine("            ,SERIAL_NO			                                                            ");
            queryString.AppendLine("            ,FIXED_DATE			                                                            ");
            queryString.AppendLine("            ,FIXED_YN			                                                            ");
            queryString.AppendLine("            ,REMARK			                                                                ");
            queryString.AppendLine("            ,PCODE_NAME     as WORK_GBN_NAME			                                    ");
            queryString.AppendLine("            ,CODE_NAME		as WAR_CODE_NAME	                                            ");
            queryString.AppendLine("            ,LOC_NAME			                                                            ");
            queryString.AppendLine("            ,WARN_NO			                                                            ");
            queryString.AppendLine("from        (            			                                                        ");
            queryString.AppendLine("                select      a.CREATE_DATE			                                        ");
            queryString.AppendLine("                            ,a.WORK_GBN			                                            ");
            queryString.AppendLine("                            ,a.LOC_CODE			                                            ");
            queryString.AppendLine("                            ,a.WAR_CODE			                                            ");
            queryString.AppendLine("                            ,a.CHECK_YN			                                            ");
            queryString.AppendLine("                            ,a.SERIAL_NO			                                        ");
            queryString.AppendLine("                            ,a.FIXED_DATE			                                        ");
            queryString.AppendLine("                            ,a.FIXED_YN			                                            ");
            queryString.AppendLine("                            ,a.REMARK			                                            ");
            queryString.AppendLine("                            ,a.WARN_NO			                                            ");
            queryString.AppendLine("                            ,b.PCODE_NAME			                                        ");
            queryString.AppendLine("                            ,c.CODE_NAME			                                        ");
            queryString.AppendLine("                            ,d.LOC_NAME			                                            ");
            queryString.AppendLine("                from        WA_WARNING      a			                                    ");
            queryString.AppendLine("                            ,CM_CODE_MASTER b			                                    ");
            queryString.AppendLine("                            ,CM_CODE        c			                                    ");
            queryString.AppendLine("                            ,CM_LOCATION    d			                                    ");
            queryString.AppendLine("                where       b.PCODE                     = '0005'                            ");
            queryString.AppendLine("                and         b.PCODE                     = a.WORK_GBN			            ");
            queryString.AppendLine("                and         c.PCODE                     = a.WORK_GBN			            ");
            queryString.AppendLine("                and         c.CODE                      = a.WAR_CODE			            ");
            queryString.AppendLine("                and         a.LOC_CODE                  = d.LOC_CODE(+)				        ");
            queryString.AppendLine("                and         to_date(a.CREATE_DATE,'yyyy-mm-dd hh24:mi:ss') > sysdate - 4    ");
            queryString.AppendLine("             )											                                    ");
            queryString.AppendLine("order by     to_date(CREATE_DATE,'yyyy-mm-dd hh24:mi:ss') desc					            ");

            return manager.ExecuteScriptDataSet(queryString.ToString(), null, "WA_WARNING");
        }

        //확인여부 update
        public void UpdateCheckYn(EMapper manager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WA_WARNING set                                              ");
            queryString.AppendLine("        CHECK_YN       = 'Y'                                        ");
            queryString.AppendLine("        ,FIXED_DATE     = to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')  ");
            queryString.AppendLine("where   WARN_NO         = '" + conditions["WARN_NO"] + "'           ");

            manager.ExecuteScript(queryString.ToString(), null);
        }
    }
}
