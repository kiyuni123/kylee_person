﻿using System;
using System.Data;
using System.Collections;
using System.Text;
using EMFrame.work;
using EMFrame.dm;
using WaterNETServer.Common;
using EMFrame.log;
using WaterNETServer.Warning.work;

namespace WaterNETServer.Warning.AddonWarning
{
    public class QualityWarning : BaseWork
    {
        /// <summary>
        /// 현재시간 - 15분 ~ 현재시간 까지의 감시대상인 수질감시지점의 실시간 계측값(분데이터)를 취득한다.
        /// 실시간 계측값이 인터페이스를 통해 들어오는 시간이 현재 시간과 일치하지 않기 때문에
        /// 15분 동안의 데이터를 가져와 경보처리 대상인지 검증을 한다.
        /// 단, 이미 경보처리된 실측값인지 경보 히스토리를 체크해서 경보가 발생한 값이 아닌 경우만 체크
        /// </summary>
        public void Analysis_RTData()
        {
            EMapper manager = null;

            try
            {
                manager = new EMapper(AppStatic.WATERNET_DATABASE);
                manager.BeginTransaction();

                //비즈니스 로직 선언부
                WarningWork work = WarningWork.GetInstance();

                decimal dcTOP_LIMIT = 0;                //경보 기준 상한값
                decimal dcLOW_LIMIT = 0;                //경보 기준 하한값
                decimal dcCUR_VAL = 0;                  //실시간수질감시 측정값

                int iAnalysisResult = 0;                //경보확인결과
                string strMONITOR_NO = string.Empty;    //실시간수질감시지점일련번호
                string strRT_OPTION = string.Empty;     //실시간수질감시 옵션
                string strRT_ITEM_CD = string.Empty;    //실시간수질감시 수질 아이템 CODE
                string strDATE_TIME = string.Empty;     //실시간측정일시
                string strRT_TIME = string.Empty;       //실시간측정시간 HH24MI
                string strBLOCK_CODE = string.Empty;		//실시간수질감시지점이 위치한 블록

                StringBuilder oStringBuilderPoint = new StringBuilder();
                StringBuilder oStringBuilder = new StringBuilder();

                DataSet pDSPoint = new DataSet();
                DataSet pDS = new DataSet();

                oStringBuilderPoint.Remove(0, oStringBuilderPoint.Length);
                oStringBuilderPoint.AppendLine("SELECT      MONITOR_NO, SBLOCK_CODE");
                oStringBuilderPoint.AppendLine("FROM        WQ_RT_MONITOR_POINT");
                oStringBuilderPoint.AppendLine("WHERE       MONPNT_GBN = '0'"); //실시간수질감시 화면에서 등록한 감시지점
                oStringBuilderPoint.AppendLine("ORDER BY    MONITOR_NO");

                pDSPoint = manager.ExecuteScriptDataSet(oStringBuilderPoint.ToString(), null, "RT_POINT");

                if ((pDSPoint.Tables.Count > 0) && (pDSPoint.Tables[0].Rows.Count > 0))
                {
                    int i = 0;

                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    foreach (DataRow oDRowPoint in pDSPoint.Tables[0].Rows)
                    {
                        if (i != 0)
                        {
                            oStringBuilder.AppendLine("UNION ALL");
                        }

                        oStringBuilder.AppendLine("SELECT   PNT.MONITOR_NO AS MONITOR_NO,");
                        oStringBuilder.AppendLine("         TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI') AS CUR_DT,");
                        oStringBuilder.AppendLine("         DECODE(CL.CUR_VALUE, '', 0, CL.CUR_VALUE) AS CL_VAL,");
                        oStringBuilder.AppendLine("         DECODE(TB.CUR_VALUE, '', 0, TB.CUR_VALUE) AS TB_VAL,");
                        oStringBuilder.AppendLine("         DECODE(PH.CUR_VALUE, '', 0, PH.CUR_VALUE) AS PH_VAL,");
                        oStringBuilder.AppendLine("         DECODE(TE.CUR_VALUE, '', 0, TE.CUR_VALUE) AS TE_VAL,");
                        oStringBuilder.AppendLine("         DECODE(CU.CUR_VALUE, '', 0, CU.CUR_VALUE) AS CU_VAL");
                        oStringBuilder.AppendLine("FROM     (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CL FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowPoint["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (15/(24*60)) AND SYSDATE) CL,");
                        oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TB FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowPoint["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (15/(24*60)) AND SYSDATE) TB,");
                        oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_PH FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowPoint["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (15/(24*60)) AND SYSDATE) PH,");
                        oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_TE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowPoint["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (15/(24*60)) AND SYSDATE) TE,");
                        oStringBuilder.AppendLine("         (SELECT TIMESTAMP, ROUND(TO_NUMBER(VALUE), 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TAGNAME = (SELECT TAG_ID_CU FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowPoint["MONITOR_NO"].ToString().Trim() + "') AND TIMESTAMP BETWEEN SYSDATE - (15/(24*60)) AND SYSDATE) CU,");
                        oStringBuilder.AppendLine("         (SELECT MONITOR_NO, MONPNT, SBLOCK_CODE FROM WQ_RT_MONITOR_POINT WHERE MONITOR_NO = '" + oDRowPoint["MONITOR_NO"].ToString().Trim() + "' AND MONPNT_GBN = '0') PNT,");
                        oStringBuilder.AppendLine("         (SELECT SYSDATE - ((1/24/60) * (ROWNUM)) AS DT FROM DUAL CONNECT BY ROWNUM  <= (((SYSDATE + ( 1 / 24 )) - SYSDATE) * 24 * 15)) TMP");
                        oStringBuilder.AppendLine("WHERE    CL.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                        oStringBuilder.AppendLine("         AND PH.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                        oStringBuilder.AppendLine("         AND TB.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                        oStringBuilder.AppendLine("         AND TE.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
                        oStringBuilder.AppendLine("         AND CU.TIMESTAMP(+) = TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");

                        i++;
                    }

                    oStringBuilder.AppendLine("ORDER BY CUR_DT DESC, MONITOR_NO");

                    pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RT_GAMSI");

                    if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
                    {
                        foreach (DataRow oDRow in pDS.Tables[0].Rows)
                        {
                            strMONITOR_NO = oDRow["MONITOR_NO"].ToString().Trim();
                            strRT_OPTION = this.GetMonitorOption(manager, strMONITOR_NO);
                            strDATE_TIME = oDRow["CUR_DT"].ToString().Trim();
                            strRT_TIME = strDATE_TIME.Substring(8);
                            strBLOCK_CODE = this.GetBlockByMonitorPoint(manager, strMONITOR_NO);

                            #region - S 잔류염소 경보처리

                            strRT_ITEM_CD = "CL";
                            dcCUR_VAL = 0;
                            dcTOP_LIMIT = 0;
                            dcLOW_LIMIT = 0;
                            dcCUR_VAL = Convert.ToDecimal(oDRow["CL_VAL"].ToString().Trim());

                            if (dcCUR_VAL != 0 && this.IsAlertedData(manager, strMONITOR_NO, strDATE_TIME, strRT_ITEM_CD) != true)
                            {
                                this.GetMappingData(manager, strMONITOR_NO, strRT_ITEM_CD, strRT_OPTION, ref dcTOP_LIMIT, ref dcLOW_LIMIT);

                                //경보옵션에 따라 경보처리
                                switch (strRT_OPTION)
                                {
                                    case "1":
                                        iAnalysisResult = this.Analysis_AlertOption1(dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                    case "2":
                                        iAnalysisResult = this.Analysis_AlertOption2(manager, strMONITOR_NO, strRT_TIME, strRT_ITEM_CD, dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                }

                                //실시간 측정값을 검증한 결과가 1 or 2이면 경보가 발생한 것이므로 History에 Insert
                                if (iAnalysisResult != 0)
                                {
                                    this.Insert_AlertHistory(manager, strMONITOR_NO, strRT_ITEM_CD, strDATE_TIME, dcCUR_VAL.ToString());

                                    Hashtable warningCondition = new Hashtable();
                                    
                                    warningCondition.Add("WORK_GBN", "0003");				//업무구분 : 실시간 수질 감시
                                    warningCondition.Add("LOC_CODE", strBLOCK_CODE);				//지역코드 (CM_LOCATION의 블록코드 -LOC_CODE-)
                                    warningCondition.Add("WAR_CODE", "000001");				//경고구분 : 잔류염소이상
                                    warningCondition.Add("SERIAL_NO", strMONITOR_NO);		//실시간 수질 감시 지점 일련번호
                                    warningCondition.Add("REMARK", "");		                //비고

                                    //비즈니스로직 호출
                                    work.InsertWarningData(warningCondition);
                                }
                            }

                            #endregion - E 잔류염소 경보처리

                            #region - S 탁도 경보처리

                            strRT_ITEM_CD = "TB";
                            dcCUR_VAL = 0;
                            dcTOP_LIMIT = 0;
                            dcLOW_LIMIT = 0;
                            dcCUR_VAL = Convert.ToDecimal(oDRow["TB_VAL"].ToString().Trim());

                            if (dcCUR_VAL != 0 && this.IsAlertedData(manager, strMONITOR_NO, strDATE_TIME, strRT_ITEM_CD) != true)
                            {
                                this.GetMappingData(manager, strMONITOR_NO, strRT_ITEM_CD, strRT_OPTION, ref dcTOP_LIMIT, ref dcLOW_LIMIT);

                                //경보옵션에 따라 경보처리
                                switch (strRT_OPTION)
                                {
                                    case "1":
                                        iAnalysisResult = this.Analysis_AlertOption1(dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                    case "2":
                                        iAnalysisResult = this.Analysis_AlertOption2(manager, strMONITOR_NO, strRT_TIME, strRT_ITEM_CD, dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                }

                                //실시간 측정값을 검증한 결과가 1 or 2이면 경보가 발생한 것이므로 History에 Insert
                                if (iAnalysisResult != 0)
                                {
                                    this.Insert_AlertHistory(manager, strMONITOR_NO, strRT_ITEM_CD, strDATE_TIME, dcCUR_VAL.ToString());
                                    
                                    Hashtable warningCondition = new Hashtable();

                                    warningCondition.Add("WORK_GBN", "0003");				//업무구분 : 실시간 수질 감시
                                    warningCondition.Add("LOC_CODE", strBLOCK_CODE);				//지역코드 (CM_LOCATION의 블록코드 -LOC_CODE-)
                                    warningCondition.Add("WAR_CODE", "000002");				//경고구분 : 탁도이상
                                    warningCondition.Add("SERIAL_NO", strMONITOR_NO);		//실시간 수질 감시 지점 일련번호
                                    warningCondition.Add("REMARK", "");		                //비고

                                    //비즈니스로직 호출
                                    work.InsertWarningData(warningCondition);
                                }
                            }

                            #endregion - E 탁도 경보처리

                            #region - S 수소이온농도 경보처리

                            strRT_ITEM_CD = "PH";
                            dcCUR_VAL = 0;
                            dcTOP_LIMIT = 0;
                            dcLOW_LIMIT = 0;
                            dcCUR_VAL = Convert.ToDecimal(oDRow["PH_VAL"].ToString().Trim());

                            if (dcCUR_VAL != 0 && this.IsAlertedData(manager, strMONITOR_NO, strDATE_TIME, strRT_ITEM_CD) != true)
                            {
                                this.GetMappingData(manager, strMONITOR_NO, strRT_ITEM_CD, strRT_OPTION, ref dcTOP_LIMIT, ref dcLOW_LIMIT);

                                //경보옵션에 따라 경보처리
                                switch (strRT_OPTION)
                                {
                                    case "1":
                                        iAnalysisResult = this.Analysis_AlertOption1(dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                    case "2":
                                        iAnalysisResult = this.Analysis_AlertOption2(manager, strMONITOR_NO, strRT_TIME, strRT_ITEM_CD, dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                }

                                //실시간 측정값을 검증한 결과가 1 or 2이면 경보가 발생한 것이므로 History에 Insert
                                if (iAnalysisResult != 0)
                                {
                                    this.Insert_AlertHistory(manager, strMONITOR_NO, strRT_ITEM_CD, strDATE_TIME, dcCUR_VAL.ToString());

                                    Hashtable warningCondition = new Hashtable();

                                    warningCondition.Add("WORK_GBN", "0003");				//업무구분 : 실시간 수질 감시
                                    warningCondition.Add("LOC_CODE", strBLOCK_CODE);				//지역코드 (CM_LOCATION의 블록코드 -LOC_CODE-)
                                    warningCondition.Add("WAR_CODE", "000003");				//경고구분 : 수소이온농도이상
                                    warningCondition.Add("SERIAL_NO", strMONITOR_NO);		//실시간 수질 감시 지점 일련번호
                                    warningCondition.Add("REMARK", "");		                //비고

                                    //비즈니스로직 호출
                                    work.InsertWarningData(warningCondition);
                                }
                            }

                            #endregion - E 수소이온농도 경보처리

                            #region - S 온도 경보처리

                            strRT_ITEM_CD = "TE";
                            dcCUR_VAL = 0;
                            dcTOP_LIMIT = 0;
                            dcLOW_LIMIT = 0;
                            dcCUR_VAL = Convert.ToDecimal(oDRow["TE_VAL"].ToString().Trim());

                            if (dcCUR_VAL != 0 && this.IsAlertedData(manager, strMONITOR_NO, strDATE_TIME, strRT_ITEM_CD) != true)
                            {
                                this.GetMappingData(manager, strMONITOR_NO, strRT_ITEM_CD, strRT_OPTION, ref dcTOP_LIMIT, ref dcLOW_LIMIT);

                                //경보옵션에 따라 경보처리
                                switch (strRT_OPTION)
                                {
                                    case "1":
                                        iAnalysisResult = this.Analysis_AlertOption1(dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                    case "2":
                                        iAnalysisResult = this.Analysis_AlertOption2(manager, strMONITOR_NO, strRT_TIME, strRT_ITEM_CD, dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                }

                                //실시간 측정값을 검증한 결과가 1 or 2이면 경보가 발생한 것이므로 History에 Insert
                                if (iAnalysisResult != 0)
                                {
                                    this.Insert_AlertHistory(manager, strMONITOR_NO, strRT_ITEM_CD, strDATE_TIME, dcCUR_VAL.ToString());

                                    Hashtable warningCondition = new Hashtable();

                                    warningCondition.Add("WORK_GBN", "0003");				//업무구분 : 실시간 수질 감시
                                    warningCondition.Add("LOC_CODE", strBLOCK_CODE);				//지역코드 (CM_LOCATION의 블록코드 -LOC_CODE-)
                                    warningCondition.Add("WAR_CODE", "000004");				//경고구분 : 온도이상
                                    warningCondition.Add("SERIAL_NO", strMONITOR_NO);		//실시간 수질 감시 지점 일련번호
                                    warningCondition.Add("REMARK", "");		                //비고

                                    //비즈니스로직 호출
                                    work.InsertWarningData(warningCondition);
                                }
                            }

                            #endregion - E 온도 경보처리

                            #region - S 전기전도도 경보처리

                            strRT_ITEM_CD = "CU";
                            dcCUR_VAL = 0;
                            dcTOP_LIMIT = 0;
                            dcLOW_LIMIT = 0;
                            dcCUR_VAL = Convert.ToDecimal(oDRow["CU_VAL"].ToString().Trim());

                            if (dcCUR_VAL != 0 && this.IsAlertedData(manager, strMONITOR_NO, strDATE_TIME, strRT_ITEM_CD) != true)
                            {
                                this.GetMappingData(manager, strMONITOR_NO, strRT_ITEM_CD, strRT_OPTION, ref dcTOP_LIMIT, ref dcLOW_LIMIT);

                                //경보옵션에 따라 경보처리
                                switch (strRT_OPTION)
                                {
                                    case "1":
                                        iAnalysisResult = this.Analysis_AlertOption1(dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                    case "2":
                                        iAnalysisResult = this.Analysis_AlertOption2(manager, strMONITOR_NO, strRT_TIME, strRT_ITEM_CD, dcCUR_VAL, dcTOP_LIMIT, dcLOW_LIMIT);
                                        break;
                                }

                                //실시간 측정값을 검증한 결과가 1 or 2이면 경보가 발생한 것이므로 History에 Insert
                                if (iAnalysisResult != 0)
                                {
                                    this.Insert_AlertHistory(manager, strMONITOR_NO, strRT_ITEM_CD, strDATE_TIME, dcCUR_VAL.ToString());

                                    Hashtable warningCondition = new Hashtable();

                                    warningCondition.Add("WORK_GBN", "0003");				//업무구분 : 실시간 수질 감시
                                    warningCondition.Add("LOC_CODE", strBLOCK_CODE);				//지역코드 (CM_LOCATION의 블록코드 -LOC_CODE-)
                                    warningCondition.Add("WAR_CODE", "000005");				//경고구분 : 전기전도도이상
                                    warningCondition.Add("SERIAL_NO", strMONITOR_NO);		//실시간 수질 감시 지점 일련번호
                                    warningCondition.Add("REMARK", "");		                //비고

                                    //비즈니스로직 호출
                                    work.InsertWarningData(warningCondition);
                                }
                            }

                            #endregion - E 전기전도도 경보처리
                        }
                    }
                }

                manager.CommitTransaction();
            }
            catch (Exception ex)
            {
                manager.RollbackTransaction();
                Logger.Error(ex);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// 해당 데이터가 이미 경보 처리된 데이터 인지 확인.
        /// </summary>
        /// <param name="strMONITOR_NO">실시간수질감시지점일련번호</param>
        /// <param name="strDATE_TIME">실시간측정일시</param>
        /// <param name="strRT_ITEM_CD">실시간수질감시 수질 아이템 CODE</param>
        /// <returns>true : 이미 경보 처리 됨, false : 신규 경보</returns>
        private bool IsAlertedData(EMapper manager, string strMONITOR_NO, string strDATE_TIME, string strRT_ITEM_CD)
        {
            bool blResult = true;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   MONITOR_NO");
            oStringBuilder.AppendLine("FROM     WQ_ALARM_HIST");
            oStringBuilder.AppendLine("WHERE    MONITOR_NO = '" + strMONITOR_NO + "'");
            oStringBuilder.AppendLine("         AND DATES = '" + strDATE_TIME + "'");
            oStringBuilder.AppendLine("         AND WQ_ITEM_CODE = '" + strRT_ITEM_CD + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IS_ALERT");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                blResult = true;
            }
            else
            {
                blResult = false;
            }

            pDS.Dispose();

            return blResult;
        }

        /// <summary>
        /// 감시지점이 속한 블록의 코드를 반환한다.
        /// </summary>
        /// <param name="strMONITOR_NO">실시간수질감시지점일련번호</param>
        /// <returns></returns>
        private string GetBlockByMonitorPoint(EMapper manager, string strMONITOR_NO)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strRTN = string.Empty;

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    SBLOCK_CODE");
            oStringBuilder.AppendLine("FROM      WQ_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("WHERE     MONITOR_NO = '" + strMONITOR_NO + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RT_BLOCK");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRTN = oDRow["SBLOCK_CODE"].ToString().Trim();
                }
            }

            pDS.Dispose();

            return strRTN;
        }

        /// <summary>
        /// 경보옵션을 취득해 반환한다.
        /// 1 : 기준 상/하한치로 경보처리
        /// 2 : 패턴 상/하한치(%)로 경보처리
        /// </summary>
        /// <param name="strMONITOR_NO">실시간수질감시지점일련번호</param>
        /// <returns></returns>
        private string GetMonitorOption(EMapper manager, string strMONITOR_NO)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strRTN = string.Empty;

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    OPTIONS");
            oStringBuilder.AppendLine("FROM      WQ_MONITOR_OPTION");
            oStringBuilder.AppendLine("WHERE     MONITOR_NO = '" + strMONITOR_NO + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RT_OPTION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strRTN = oDRow["OPTIONS"].ToString().Trim();
                }
            }

            pDS.Dispose();

            return strRTN;
        }

        /// <summary>
        /// 실시간수질감시일련번호, 감시 아이템, 감시옵션으로 경보 상/하한 기준값을 찾아 반환한다.
        /// </summary>
        /// <param name="strMONITOR_NO">실시간수질감시일련번호</param>
        /// <param name="strRT_ITEM_CD">실시간수질감시 수질 아이템 CODE</param>
        /// <param name="strRT_OPTION">실시간수질감시옵션</param>
        /// <param name="dcTOP_LIMIT">반환값 : 경보 기준 상한값</param>
        /// <param name="dcLOW_LIMIT">반환값 : 경보 기준 하한값</param>
        private void GetMappingData(EMapper manager, string strMONITOR_NO, string strRT_ITEM_CD, string strRT_OPTION, ref decimal dcTOP_LIMIT, ref decimal dcLOW_LIMIT)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    DECODE(N1_TOP_LIMIT, NULL, 0, N1_TOP_LIMIT) AS T1_LIMIT,");
            oStringBuilder.AppendLine("          DECODE(N1_LOW_LIMIT, NULL, 0, N1_LOW_LIMIT) AS L1_LIMIT,");
            oStringBuilder.AppendLine("          DECODE(N2_TOP_LIMIT, NULL, 0, N2_TOP_LIMIT) AS T2_LIMIT,");
            oStringBuilder.AppendLine("          DECODE(N2_LOW_LIMIT, NULL, 0, N2_LOW_LIMIT) AS L2_LIMIT");
            oStringBuilder.AppendLine("FROM      WQ_MONPNT_MAPPING");
            oStringBuilder.AppendLine("WHERE     MONITOR_NO = '" + strMONITOR_NO + "' AND WQ_ITEM_CODE = '" + strRT_ITEM_CD + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RT_MAPPING");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    switch (strRT_OPTION)
                    {
                        case "1":   //기준 상/하한치로 경보처리
                            dcTOP_LIMIT = Convert.ToDecimal(oDRow["T1_LIMIT"].ToString().Trim());
                            dcLOW_LIMIT = Convert.ToDecimal(oDRow["L1_LIMIT"].ToString().Trim());
                            break;
                        case "2":   //패턴 상/하한치(%)로 경보처리
                            dcTOP_LIMIT = Convert.ToDecimal(oDRow["T2_LIMIT"].ToString().Trim());
                            dcLOW_LIMIT = Convert.ToDecimal(oDRow["L2_LIMIT"].ToString().Trim());
                            break;
                    }
                }
            }

            pDS.Dispose();
        }

        /// <summary>
        /// 실시간 수질 감시 기준 상/하한값으로 분석.
        /// 대상 : 실시간데이터
        /// 기준 : 항목별 기준치
        /// </summary>
        /// <param name="dcCUR_VAL">실시간수질감시 측정값</param>
        /// <param name="dcTOP_LIMIT">경보 기준 상한값</param>
        /// <param name="dcLOW_LIMIT">경보 기준 하한값</param>
        /// <returns>0 : 경보 미발생, 1 : 경보 발생 (초과), 2 : 경보 발생 (미만)</returns>
        private int Analysis_AlertOption1(decimal dcCUR_VAL, decimal dcTOP_LIMIT, decimal dcLOW_LIMIT)
        {
            int iResult = 0;

            if (dcTOP_LIMIT == 0 || dcLOW_LIMIT == 0) return iResult;

            if (dcCUR_VAL > dcTOP_LIMIT)
            {
                iResult = 1;
            }
            else if (dcCUR_VAL < dcLOW_LIMIT)
            {
                iResult = 2;
            }

            return iResult;
        }

        /// <summary>
        /// 실시간 수질 감시 패턴 상/하한값으로 분석.
        /// 대상 : 실시간데이터
        /// 기준 : 항목별 패턴에 +- %
        /// </summary>
        /// <param name="strMONITOR_NO">실시간수질감시일련번호</param>
        /// <param name="strRT_TIME">실시간측정일시 중 HH24MI</param>
        /// <param name="strRT_ITEM_CD">실시간수질감시 항목</param>
        /// <param name="dcCUR_VAL">실시간수질감시 측정값</param>
        /// <param name="dcTOP_LIMIT">경보 기준 상한값 %</param>
        /// <param name="dcLOW_LIMIT">경보 기준 하한값 %</param>
        /// <returns>0 : 경보 미발생, 1 : 경보 발생 (초과), 2 : 경보 발생 (미만)</returns>
        private int Analysis_AlertOption2(EMapper manager, string strMONITOR_NO, string strRT_TIME, string strRT_ITEM_CD, decimal dcCUR_VAL, decimal dcTOP_LIMIT, decimal dcLOW_LIMIT)
        {
            int iResult = 0;

            if (dcTOP_LIMIT == 0 || dcLOW_LIMIT == 0) return iResult;

            StringBuilder oStringBuilder = new StringBuilder();

            decimal dcGT_LIMIT = 0;     //패턴 상한값
            decimal dcGL_LIMIT = 0;     //패턴 하한값
            decimal dcCAL_T_LIMIT = 0;
            decimal dcCAL_L_LIMIT = 0;
            decimal dcCAL_T_VALUE = 0;  //패턴 상한값에 경보 기준 상한값(%) 적용한 값
            decimal dcCAL_L_VALUE = 0;  //패턴 상한값에 경보 기준 하한값(%) 적용한 값

            DataSet pDS = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   TOP_LIMIT AS GT_LIMIT, LOWER_LIMIT AS GL_LIMIT");
            oStringBuilder.AppendLine("FROM     WQ_PATTERN");
            oStringBuilder.AppendLine("WHERE    MONITOR_NO = '" + strMONITOR_NO + "' AND WQ_ITEM_CODE = '" + strRT_ITEM_CD + "' AND TIME = '" + strRT_TIME.Substring(0, 2) + ":" + strRT_TIME.Substring(2, 2) + "'");

            pDS = manager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RT_PATTERN");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    dcGT_LIMIT = Convert.ToDecimal(oDRow["GT_LIMIT"].ToString().Trim());
                    dcGL_LIMIT = Convert.ToDecimal(oDRow["GL_LIMIT"].ToString().Trim());

                    dcCAL_T_LIMIT = dcGT_LIMIT * (dcTOP_LIMIT / 100);
                    dcCAL_L_LIMIT = dcGL_LIMIT * (dcLOW_LIMIT / 100);
                    dcCAL_T_VALUE = dcGT_LIMIT + dcCAL_T_LIMIT;
                    dcCAL_L_VALUE = dcGL_LIMIT - dcCAL_L_LIMIT;

                    if (dcCUR_VAL > dcCAL_T_VALUE)
                    {
                        iResult = 1;
                    }
                    else if (dcCUR_VAL < dcCAL_L_VALUE)
                    {
                        iResult = 2;
                    }
                }
            }

            return iResult;
        }

        /// <summary>
        /// 경보 히스토리를 INSERT함
        /// </summary>
        /// <param name="strMONITOR_NO">실시간수질감시일련번호</param>
        /// <param name="strRT_ITEM_CD">실시간수질감시 항목</param>
        /// <param name="strDATE_TIME">실시간측정일시</param>
        /// <param name="strCUR_VAL">실시간측정값</param>
        private void Insert_AlertHistory(EMapper manager, string strMONITOR_NO, string strRT_ITEM_CD, string strDATE_TIME, string strCUR_VAL)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   FROM    WQ_ALARM_HIST");
            oStringBuilder.AppendLine("WHERE    MONITOR_NO = '" + strMONITOR_NO + "' AND  WQ_ITEM_CODE = '" + strRT_ITEM_CD + "' AND DATES = '" + strDATE_TIME + "'");

            manager.ExecuteScript(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    WQ_ALARM_HIST");
            oStringBuilder.AppendLine("(MONITOR_NO, WQ_ITEM_CODE, DATES, ALARM_LEVEL, RPT_NUMBER)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine("('" + strMONITOR_NO + "', '" + strRT_ITEM_CD + "', '" + strDATE_TIME + "', '" + strCUR_VAL + "', '')");

            manager.ExecuteScript(oStringBuilder.ToString(), null);
        }
    }
}
