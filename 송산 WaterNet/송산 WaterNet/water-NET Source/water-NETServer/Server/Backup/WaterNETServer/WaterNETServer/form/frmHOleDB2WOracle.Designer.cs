﻿namespace WaterNETServer.form
{
    partial class frmHOleDB2WOracle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHOleDB2WOracle));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button23 = new System.Windows.Forms.Button();
            this.progressBar4 = new System.Windows.Forms.ProgressBar();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.button19 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.button11 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.dateTimePicker8 = new System.Windows.Forms.DateTimePicker();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.dateTimePicker7 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.button18 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button12 = new System.Windows.Forms.Button();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.button9 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_auto_mdodify = new System.Windows.Forms.Button();
            this.dateTimePicker_auto_modify = new System.Windows.Forms.DateTimePicker();
            this.checkBox_auto_modify = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.progressBar_WHHpress_Result_Modify = new System.Windows.Forms.ProgressBar();
            this.button70 = new System.Windows.Forms.Button();
            this.dateTimePicker31 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker32 = new System.Windows.Forms.DateTimePicker();
            this.progressBar_RevenueRatio_Analysis_Modify = new System.Windows.Forms.ProgressBar();
            this.button69 = new System.Windows.Forms.Button();
            this.dateTimePicker29 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker30 = new System.Windows.Forms.DateTimePicker();
            this.progressBar_Pipe_Info_Modify = new System.Windows.Forms.ProgressBar();
            this.button68 = new System.Windows.Forms.Button();
            this.dateTimePicker27 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker28 = new System.Windows.Forms.DateTimePicker();
            this.progressBar_DM_Used_Modify = new System.Windows.Forms.ProgressBar();
            this.button67 = new System.Windows.Forms.Button();
            this.dateTimePicker25 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker26 = new System.Windows.Forms.DateTimePicker();
            this.progressBar_DM_Info_Modify = new System.Windows.Forms.ProgressBar();
            this.button66 = new System.Windows.Forms.Button();
            this.dateTimePicker23 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker24 = new System.Windows.Forms.DateTimePicker();
            this.progressBar_STWCHRG_Modify = new System.Windows.Forms.ProgressBar();
            this.button65 = new System.Windows.Forms.Button();
            this.dateTimePicker21 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker22 = new System.Windows.Forms.DateTimePicker();
            this.progressBar_MTRCHGINFO_Modify = new System.Windows.Forms.ProgressBar();
            this.button64 = new System.Windows.Forms.Button();
            this.dateTimePicker19 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker20 = new System.Windows.Forms.DateTimePicker();
            this.progressBar_CAINFO_Modify = new System.Windows.Forms.ProgressBar();
            this.button63 = new System.Windows.Forms.Button();
            this.dateTimePicker17 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker18 = new System.Windows.Forms.DateTimePicker();
            this.progressBar_DMWSRSRC_Modify = new System.Windows.Forms.ProgressBar();
            this.button62 = new System.Windows.Forms.Button();
            this.dateTimePicker15 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker16 = new System.Windows.Forms.DateTimePicker();
            this.progressBar_DMINFO_Modify = new System.Windows.Forms.ProgressBar();
            this.button4 = new System.Windows.Forms.Button();
            this.dateTimePicker13 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker14 = new System.Windows.Forms.DateTimePicker();
            this.progressBar9 = new System.Windows.Forms.ProgressBar();
            this.button3 = new System.Windows.Forms.Button();
            this.dateTimePicker11 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker12 = new System.Windows.Forms.DateTimePicker();
            this.progressBar8 = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker10 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker9 = new System.Windows.Forms.DateTimePicker();
            this.progressBar7 = new System.Windows.Forms.ProgressBar();
            this.progressBar6 = new System.Windows.Forms.ProgressBar();
            this.progressBar5 = new System.Windows.Forms.ProgressBar();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button61 = new System.Windows.Forms.Button();
            this.button60 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.button25 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button24 = new System.Windows.Forms.Button();
            this.label_auto_modify = new System.Windows.Forms.Label();
            this.label_auto_modify_status = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "가동중";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "대기중";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "정지중";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(6, 64);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(6, 42);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(4, 657);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(53, 145);
            this.button23.TabIndex = 3;
            this.button23.Text = "프로세스보기";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.CheckRunningTimer);
            // 
            // progressBar4
            // 
            this.progressBar4.Location = new System.Drawing.Point(111, 48);
            this.progressBar4.Name = "progressBar4";
            this.progressBar4.Size = new System.Drawing.Size(271, 23);
            this.progressBar4.TabIndex = 2;
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(111, 18);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(271, 23);
            this.progressBar3.TabIndex = 1;
            // 
            // button19
            // 
            this.button19.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button19.Location = new System.Drawing.Point(6, 18);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(99, 53);
            this.button19.TabIndex = 0;
            this.button19.Text = "시작";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.BatchStart);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(61, 642);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 12);
            this.label4.TabIndex = 60;
            this.label4.Text = "프로세스 정보";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(63, 657);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(739, 145);
            this.textBox1.TabIndex = 59;
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(190, 42);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(336, 23);
            this.progressBar2.TabIndex = 56;
            // 
            // button11
            // 
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.Location = new System.Drawing.Point(126, 13);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(25, 23);
            this.button11.TabIndex = 16;
            this.button11.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(190, 14);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(336, 23);
            this.progressBar1.TabIndex = 52;
            // 
            // dateTimePicker8
            // 
            this.dateTimePicker8.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker8.Location = new System.Drawing.Point(98, 14);
            this.dateTimePicker8.Name = "dateTimePicker8";
            this.dateTimePicker8.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker8.TabIndex = 54;
            // 
            // button13
            // 
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(6, 42);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(178, 23);
            this.button13.TabIndex = 51;
            this.button13.Text = "실시간데이터수집";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.ModifyRealtime);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(206, 13);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(43, 23);
            this.button14.TabIndex = 55;
            this.button14.Text = "정지";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.GatherRealtime_stop);
            // 
            // dateTimePicker7
            // 
            this.dateTimePicker7.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker7.Location = new System.Drawing.Point(6, 14);
            this.dateTimePicker7.Name = "dateTimePicker7";
            this.dateTimePicker7.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker7.TabIndex = 53;
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker6.Location = new System.Drawing.Point(98, 101);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker6.TabIndex = 50;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(206, 100);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(43, 23);
            this.button18.TabIndex = 43;
            this.button18.Text = "정지";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.Accumulation_Day_stop);
            // 
            // button8
            // 
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(190, 100);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(106, 23);
            this.button8.TabIndex = 42;
            this.button8.Text = "일 적산데이터";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Modification_Day);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(206, 42);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(43, 23);
            this.button17.TabIndex = 42;
            this.button17.Text = "정지";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.Accumulation_ToDay_stop);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(206, 71);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(43, 23);
            this.button16.TabIndex = 41;
            this.button16.Text = "정지";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.Accumulation_Hour_stop);
            // 
            // button15
            // 
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button15.Image = ((System.Drawing.Image)(resources.GetObject("button15.Image")));
            this.button15.Location = new System.Drawing.Point(126, 71);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(25, 23);
            this.button15.TabIndex = 40;
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.Location = new System.Drawing.Point(126, 100);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(25, 23);
            this.button7.TabIndex = 41;
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button10.Image = ((System.Drawing.Image)(resources.GetObject("button10.Image")));
            this.button10.Location = new System.Drawing.Point(126, 42);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(25, 23);
            this.button10.TabIndex = 33;
            this.button10.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(6, 101);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker1.TabIndex = 43;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(190, 158);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(106, 23);
            this.button12.TabIndex = 47;
            this.button12.Text = "정시수압";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.AveragePressure_Modify);
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker4.Location = new System.Drawing.Point(6, 159);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker4.TabIndex = 49;
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker5.Location = new System.Drawing.Point(98, 159);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker5.TabIndex = 48;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(206, 129);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(43, 23);
            this.button20.TabIndex = 42;
            this.button20.Text = "정지";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.MinimumNightFlow_stop);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(206, 158);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(43, 23);
            this.button21.TabIndex = 60;
            this.button21.Text = "정지";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.AveragePressure_stop);
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(126, 129);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(25, 23);
            this.button5.TabIndex = 24;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker3.Location = new System.Drawing.Point(98, 130);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker3.TabIndex = 46;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(190, 129);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(106, 23);
            this.button9.TabIndex = 44;
            this.button9.Text = "야간최소유량";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.MinimumNightFlow_modify);
            // 
            // button6
            // 
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.Location = new System.Drawing.Point(126, 158);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(25, 23);
            this.button6.TabIndex = 25;
            this.button6.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(6, 130);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker2.TabIndex = 45;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(126, 187);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(25, 23);
            this.button2.TabIndex = 39;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(206, 187);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(43, 23);
            this.button22.TabIndex = 61;
            this.button22.Text = "정지";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.MonitoringLeakage_stop);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.groupBox9);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.button23);
            this.panel1.Controls.Add(this.groupBox8);
            this.panel1.Controls.Add(this.groupBox7);
            this.panel1.Controls.Add(this.groupBox6);
            this.panel1.Location = new System.Drawing.Point(9, 9);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(815, 805);
            this.panel1.TabIndex = 67;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_auto_modify_status);
            this.groupBox1.Controls.Add(this.label_auto_modify);
            this.groupBox1.Controls.Add(this.button_auto_mdodify);
            this.groupBox1.Controls.Add(this.dateTimePicker_auto_modify);
            this.groupBox1.Controls.Add(this.checkBox_auto_modify);
            this.groupBox1.Location = new System.Drawing.Point(517, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 107);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "옵션";
            // 
            // button_auto_mdodify
            // 
            this.button_auto_mdodify.Location = new System.Drawing.Point(139, 53);
            this.button_auto_mdodify.Name = "button_auto_mdodify";
            this.button_auto_mdodify.Size = new System.Drawing.Size(43, 23);
            this.button_auto_mdodify.TabIndex = 7;
            this.button_auto_mdodify.Text = "적용";
            this.button_auto_mdodify.UseVisualStyleBackColor = true;
            this.button_auto_mdodify.Click += new System.EventHandler(this.button_auto_mdodify_Click);
            // 
            // dateTimePicker_auto_modify
            // 
            this.dateTimePicker_auto_modify.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePicker_auto_modify.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_auto_modify.Location = new System.Drawing.Point(12, 54);
            this.dateTimePicker_auto_modify.Name = "dateTimePicker_auto_modify";
            this.dateTimePicker_auto_modify.Size = new System.Drawing.Size(120, 21);
            this.dateTimePicker_auto_modify.TabIndex = 6;
            // 
            // checkBox_auto_modify
            // 
            this.checkBox_auto_modify.AutoSize = true;
            this.checkBox_auto_modify.Location = new System.Drawing.Point(12, 29);
            this.checkBox_auto_modify.Name = "checkBox_auto_modify";
            this.checkBox_auto_modify.Size = new System.Drawing.Size(72, 16);
            this.checkBox_auto_modify.TabIndex = 5;
            this.checkBox_auto_modify.Text = "자동보정";
            this.checkBox_auto_modify.UseVisualStyleBackColor = true;
            this.checkBox_auto_modify.CheckedChanged += new System.EventHandler(this.checkBox_auto_modify_CheckedChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.progressBar_WHHpress_Result_Modify);
            this.groupBox9.Controls.Add(this.button70);
            this.groupBox9.Controls.Add(this.dateTimePicker31);
            this.groupBox9.Controls.Add(this.dateTimePicker32);
            this.groupBox9.Controls.Add(this.progressBar_RevenueRatio_Analysis_Modify);
            this.groupBox9.Controls.Add(this.button69);
            this.groupBox9.Controls.Add(this.dateTimePicker29);
            this.groupBox9.Controls.Add(this.dateTimePicker30);
            this.groupBox9.Controls.Add(this.progressBar_Pipe_Info_Modify);
            this.groupBox9.Controls.Add(this.button68);
            this.groupBox9.Controls.Add(this.dateTimePicker27);
            this.groupBox9.Controls.Add(this.dateTimePicker28);
            this.groupBox9.Controls.Add(this.progressBar_DM_Used_Modify);
            this.groupBox9.Controls.Add(this.button67);
            this.groupBox9.Controls.Add(this.dateTimePicker25);
            this.groupBox9.Controls.Add(this.dateTimePicker26);
            this.groupBox9.Controls.Add(this.progressBar_DM_Info_Modify);
            this.groupBox9.Controls.Add(this.button66);
            this.groupBox9.Controls.Add(this.dateTimePicker23);
            this.groupBox9.Controls.Add(this.dateTimePicker24);
            this.groupBox9.Controls.Add(this.progressBar_STWCHRG_Modify);
            this.groupBox9.Controls.Add(this.button65);
            this.groupBox9.Controls.Add(this.dateTimePicker21);
            this.groupBox9.Controls.Add(this.dateTimePicker22);
            this.groupBox9.Controls.Add(this.progressBar_MTRCHGINFO_Modify);
            this.groupBox9.Controls.Add(this.button64);
            this.groupBox9.Controls.Add(this.dateTimePicker19);
            this.groupBox9.Controls.Add(this.dateTimePicker20);
            this.groupBox9.Controls.Add(this.progressBar_CAINFO_Modify);
            this.groupBox9.Controls.Add(this.button63);
            this.groupBox9.Controls.Add(this.dateTimePicker17);
            this.groupBox9.Controls.Add(this.dateTimePicker18);
            this.groupBox9.Controls.Add(this.progressBar_DMWSRSRC_Modify);
            this.groupBox9.Controls.Add(this.button62);
            this.groupBox9.Controls.Add(this.dateTimePicker15);
            this.groupBox9.Controls.Add(this.dateTimePicker16);
            this.groupBox9.Controls.Add(this.progressBar_DMINFO_Modify);
            this.groupBox9.Controls.Add(this.button4);
            this.groupBox9.Controls.Add(this.dateTimePicker13);
            this.groupBox9.Controls.Add(this.dateTimePicker14);
            this.groupBox9.Controls.Add(this.progressBar9);
            this.groupBox9.Controls.Add(this.button3);
            this.groupBox9.Controls.Add(this.dateTimePicker11);
            this.groupBox9.Controls.Add(this.dateTimePicker12);
            this.groupBox9.Controls.Add(this.progressBar8);
            this.groupBox9.Controls.Add(this.button1);
            this.groupBox9.Controls.Add(this.dateTimePicker10);
            this.groupBox9.Controls.Add(this.dateTimePicker9);
            this.groupBox9.Controls.Add(this.progressBar7);
            this.groupBox9.Controls.Add(this.progressBar6);
            this.groupBox9.Controls.Add(this.progressBar5);
            this.groupBox9.Controls.Add(this.button12);
            this.groupBox9.Controls.Add(this.dateTimePicker6);
            this.groupBox9.Controls.Add(this.dateTimePicker5);
            this.groupBox9.Controls.Add(this.dateTimePicker4);
            this.groupBox9.Controls.Add(this.progressBar2);
            this.groupBox9.Controls.Add(this.button8);
            this.groupBox9.Controls.Add(this.dateTimePicker3);
            this.groupBox9.Controls.Add(this.dateTimePicker1);
            this.groupBox9.Controls.Add(this.button9);
            this.groupBox9.Controls.Add(this.dateTimePicker2);
            this.groupBox9.Controls.Add(this.dateTimePicker7);
            this.groupBox9.Controls.Add(this.progressBar1);
            this.groupBox9.Controls.Add(this.dateTimePicker8);
            this.groupBox9.Controls.Add(this.button13);
            this.groupBox9.Location = new System.Drawing.Point(270, 114);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(532, 517);
            this.groupBox9.TabIndex = 4;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "보정";
            // 
            // progressBar_WHHpress_Result_Modify
            // 
            this.progressBar_WHHpress_Result_Modify.Location = new System.Drawing.Point(303, 476);
            this.progressBar_WHHpress_Result_Modify.Name = "progressBar_WHHpress_Result_Modify";
            this.progressBar_WHHpress_Result_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_WHHpress_Result_Modify.TabIndex = 107;
            // 
            // button70
            // 
            this.button70.Location = new System.Drawing.Point(190, 476);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(106, 23);
            this.button70.TabIndex = 104;
            this.button70.Text = "평균수압지점계산";
            this.button70.UseVisualStyleBackColor = true;
            this.button70.Click += new System.EventHandler(this.WHHpress_Result_Modify);
            // 
            // dateTimePicker31
            // 
            this.dateTimePicker31.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker31.Location = new System.Drawing.Point(98, 477);
            this.dateTimePicker31.Name = "dateTimePicker31";
            this.dateTimePicker31.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker31.TabIndex = 105;
            // 
            // dateTimePicker32
            // 
            this.dateTimePicker32.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker32.Location = new System.Drawing.Point(6, 477);
            this.dateTimePicker32.Name = "dateTimePicker32";
            this.dateTimePicker32.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker32.TabIndex = 106;
            // 
            // progressBar_RevenueRatio_Analysis_Modify
            // 
            this.progressBar_RevenueRatio_Analysis_Modify.Location = new System.Drawing.Point(303, 447);
            this.progressBar_RevenueRatio_Analysis_Modify.Name = "progressBar_RevenueRatio_Analysis_Modify";
            this.progressBar_RevenueRatio_Analysis_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_RevenueRatio_Analysis_Modify.TabIndex = 103;
            // 
            // button69
            // 
            this.button69.Location = new System.Drawing.Point(190, 447);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(106, 23);
            this.button69.TabIndex = 100;
            this.button69.Text = "유수율분석";
            this.button69.UseVisualStyleBackColor = true;
            this.button69.Click += new System.EventHandler(this.RevenueRatio_Analysis_Modify);
            // 
            // dateTimePicker29
            // 
            this.dateTimePicker29.CustomFormat = "yyyy년MM월";
            this.dateTimePicker29.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker29.Location = new System.Drawing.Point(98, 448);
            this.dateTimePicker29.Name = "dateTimePicker29";
            this.dateTimePicker29.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker29.TabIndex = 101;
            // 
            // dateTimePicker30
            // 
            this.dateTimePicker30.CustomFormat = "yyyy년MM월";
            this.dateTimePicker30.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker30.Location = new System.Drawing.Point(6, 448);
            this.dateTimePicker30.Name = "dateTimePicker30";
            this.dateTimePicker30.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker30.TabIndex = 102;
            // 
            // progressBar_Pipe_Info_Modify
            // 
            this.progressBar_Pipe_Info_Modify.Location = new System.Drawing.Point(303, 418);
            this.progressBar_Pipe_Info_Modify.Name = "progressBar_Pipe_Info_Modify";
            this.progressBar_Pipe_Info_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_Pipe_Info_Modify.TabIndex = 99;
            // 
            // button68
            // 
            this.button68.Location = new System.Drawing.Point(190, 418);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(106, 23);
            this.button68.TabIndex = 96;
            this.button68.Text = "관로길이계산";
            this.button68.UseVisualStyleBackColor = true;
            this.button68.Click += new System.EventHandler(this.Pipe_Info_Modify);
            // 
            // dateTimePicker27
            // 
            this.dateTimePicker27.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker27.Location = new System.Drawing.Point(98, 419);
            this.dateTimePicker27.Name = "dateTimePicker27";
            this.dateTimePicker27.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker27.TabIndex = 97;
            // 
            // dateTimePicker28
            // 
            this.dateTimePicker28.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker28.Location = new System.Drawing.Point(6, 419);
            this.dateTimePicker28.Name = "dateTimePicker28";
            this.dateTimePicker28.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker28.TabIndex = 98;
            // 
            // progressBar_DM_Used_Modify
            // 
            this.progressBar_DM_Used_Modify.Location = new System.Drawing.Point(303, 390);
            this.progressBar_DM_Used_Modify.Name = "progressBar_DM_Used_Modify";
            this.progressBar_DM_Used_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_DM_Used_Modify.TabIndex = 95;
            // 
            // button67
            // 
            this.button67.Location = new System.Drawing.Point(190, 390);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(106, 23);
            this.button67.TabIndex = 92;
            this.button67.Text = "블록별사용량계산";
            this.button67.UseVisualStyleBackColor = true;
            this.button67.Click += new System.EventHandler(this.DM_Used_Modify);
            // 
            // dateTimePicker25
            // 
            this.dateTimePicker25.CustomFormat = "yyyy년MM월";
            this.dateTimePicker25.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker25.Location = new System.Drawing.Point(98, 391);
            this.dateTimePicker25.Name = "dateTimePicker25";
            this.dateTimePicker25.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker25.TabIndex = 93;
            // 
            // dateTimePicker26
            // 
            this.dateTimePicker26.CustomFormat = "yyyy년MM월";
            this.dateTimePicker26.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker26.Location = new System.Drawing.Point(6, 391);
            this.dateTimePicker26.Name = "dateTimePicker26";
            this.dateTimePicker26.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker26.TabIndex = 94;
            // 
            // progressBar_DM_Info_Modify
            // 
            this.progressBar_DM_Info_Modify.Location = new System.Drawing.Point(303, 361);
            this.progressBar_DM_Info_Modify.Name = "progressBar_DM_Info_Modify";
            this.progressBar_DM_Info_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_DM_Info_Modify.TabIndex = 91;
            // 
            // button66
            // 
            this.button66.Location = new System.Drawing.Point(190, 361);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(106, 23);
            this.button66.TabIndex = 88;
            this.button66.Text = "급수전수계산";
            this.button66.UseVisualStyleBackColor = true;
            this.button66.Click += new System.EventHandler(this.DM_Info_Modify);
            // 
            // dateTimePicker23
            // 
            this.dateTimePicker23.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker23.Location = new System.Drawing.Point(98, 362);
            this.dateTimePicker23.Name = "dateTimePicker23";
            this.dateTimePicker23.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker23.TabIndex = 89;
            // 
            // dateTimePicker24
            // 
            this.dateTimePicker24.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker24.Location = new System.Drawing.Point(6, 362);
            this.dateTimePicker24.Name = "dateTimePicker24";
            this.dateTimePicker24.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker24.TabIndex = 90;
            // 
            // progressBar_STWCHRG_Modify
            // 
            this.progressBar_STWCHRG_Modify.Location = new System.Drawing.Point(303, 332);
            this.progressBar_STWCHRG_Modify.Name = "progressBar_STWCHRG_Modify";
            this.progressBar_STWCHRG_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_STWCHRG_Modify.TabIndex = 87;
            // 
            // button65
            // 
            this.button65.Location = new System.Drawing.Point(190, 332);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(106, 23);
            this.button65.TabIndex = 84;
            this.button65.Text = "요금정보";
            this.button65.UseVisualStyleBackColor = true;
            this.button65.Click += new System.EventHandler(this.STWCHRG_Modify);
            // 
            // dateTimePicker21
            // 
            this.dateTimePicker21.CustomFormat = "yyyy년MM월";
            this.dateTimePicker21.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker21.Location = new System.Drawing.Point(98, 333);
            this.dateTimePicker21.Name = "dateTimePicker21";
            this.dateTimePicker21.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker21.TabIndex = 85;
            // 
            // dateTimePicker22
            // 
            this.dateTimePicker22.CustomFormat = "yyyy년MM월";
            this.dateTimePicker22.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker22.Location = new System.Drawing.Point(6, 333);
            this.dateTimePicker22.Name = "dateTimePicker22";
            this.dateTimePicker22.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker22.TabIndex = 86;
            // 
            // progressBar_MTRCHGINFO_Modify
            // 
            this.progressBar_MTRCHGINFO_Modify.Location = new System.Drawing.Point(303, 303);
            this.progressBar_MTRCHGINFO_Modify.Name = "progressBar_MTRCHGINFO_Modify";
            this.progressBar_MTRCHGINFO_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_MTRCHGINFO_Modify.TabIndex = 83;
            // 
            // button64
            // 
            this.button64.Location = new System.Drawing.Point(190, 303);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(106, 23);
            this.button64.TabIndex = 80;
            this.button64.Text = "계량기교체";
            this.button64.UseVisualStyleBackColor = true;
            this.button64.Click += new System.EventHandler(this.MTRCHGINFO_Modify);
            // 
            // dateTimePicker19
            // 
            this.dateTimePicker19.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker19.Location = new System.Drawing.Point(98, 304);
            this.dateTimePicker19.Name = "dateTimePicker19";
            this.dateTimePicker19.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker19.TabIndex = 81;
            // 
            // dateTimePicker20
            // 
            this.dateTimePicker20.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker20.Location = new System.Drawing.Point(6, 304);
            this.dateTimePicker20.Name = "dateTimePicker20";
            this.dateTimePicker20.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker20.TabIndex = 82;
            // 
            // progressBar_CAINFO_Modify
            // 
            this.progressBar_CAINFO_Modify.Location = new System.Drawing.Point(303, 274);
            this.progressBar_CAINFO_Modify.Name = "progressBar_CAINFO_Modify";
            this.progressBar_CAINFO_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_CAINFO_Modify.TabIndex = 79;
            // 
            // button63
            // 
            this.button63.Location = new System.Drawing.Point(190, 274);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(106, 23);
            this.button63.TabIndex = 76;
            this.button63.Text = "민원정보";
            this.button63.UseVisualStyleBackColor = true;
            this.button63.Click += new System.EventHandler(this.CAINFO_Modify);
            // 
            // dateTimePicker17
            // 
            this.dateTimePicker17.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker17.Location = new System.Drawing.Point(98, 275);
            this.dateTimePicker17.Name = "dateTimePicker17";
            this.dateTimePicker17.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker17.TabIndex = 77;
            // 
            // dateTimePicker18
            // 
            this.dateTimePicker18.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker18.Location = new System.Drawing.Point(6, 275);
            this.dateTimePicker18.Name = "dateTimePicker18";
            this.dateTimePicker18.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker18.TabIndex = 78;
            // 
            // progressBar_DMWSRSRC_Modify
            // 
            this.progressBar_DMWSRSRC_Modify.Location = new System.Drawing.Point(303, 245);
            this.progressBar_DMWSRSRC_Modify.Name = "progressBar_DMWSRSRC_Modify";
            this.progressBar_DMWSRSRC_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_DMWSRSRC_Modify.TabIndex = 75;
            // 
            // button62
            // 
            this.button62.Location = new System.Drawing.Point(190, 245);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(106, 23);
            this.button62.TabIndex = 72;
            this.button62.Text = "상하수도자원";
            this.button62.UseVisualStyleBackColor = true;
            this.button62.Click += new System.EventHandler(this.DMWSRSRC_Modify);
            // 
            // dateTimePicker15
            // 
            this.dateTimePicker15.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker15.Location = new System.Drawing.Point(98, 246);
            this.dateTimePicker15.Name = "dateTimePicker15";
            this.dateTimePicker15.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker15.TabIndex = 73;
            // 
            // dateTimePicker16
            // 
            this.dateTimePicker16.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker16.Location = new System.Drawing.Point(6, 246);
            this.dateTimePicker16.Name = "dateTimePicker16";
            this.dateTimePicker16.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker16.TabIndex = 74;
            // 
            // progressBar_DMINFO_Modify
            // 
            this.progressBar_DMINFO_Modify.Location = new System.Drawing.Point(303, 216);
            this.progressBar_DMINFO_Modify.Name = "progressBar_DMINFO_Modify";
            this.progressBar_DMINFO_Modify.Size = new System.Drawing.Size(223, 23);
            this.progressBar_DMINFO_Modify.TabIndex = 71;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(190, 216);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 23);
            this.button4.TabIndex = 68;
            this.button4.Text = "수용가정보";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.DMINFO_Modify);
            // 
            // dateTimePicker13
            // 
            this.dateTimePicker13.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker13.Location = new System.Drawing.Point(98, 217);
            this.dateTimePicker13.Name = "dateTimePicker13";
            this.dateTimePicker13.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker13.TabIndex = 69;
            // 
            // dateTimePicker14
            // 
            this.dateTimePicker14.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker14.Location = new System.Drawing.Point(6, 217);
            this.dateTimePicker14.Name = "dateTimePicker14";
            this.dateTimePicker14.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker14.TabIndex = 70;
            // 
            // progressBar9
            // 
            this.progressBar9.Location = new System.Drawing.Point(303, 187);
            this.progressBar9.Name = "progressBar9";
            this.progressBar9.Size = new System.Drawing.Size(223, 23);
            this.progressBar9.TabIndex = 67;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(190, 187);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 23);
            this.button3.TabIndex = 64;
            this.button3.Text = "누수감시";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.MonitoringLeakage_Modify);
            // 
            // dateTimePicker11
            // 
            this.dateTimePicker11.Enabled = false;
            this.dateTimePicker11.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker11.Location = new System.Drawing.Point(98, 188);
            this.dateTimePicker11.Name = "dateTimePicker11";
            this.dateTimePicker11.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker11.TabIndex = 65;
            // 
            // dateTimePicker12
            // 
            this.dateTimePicker12.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker12.Location = new System.Drawing.Point(6, 188);
            this.dateTimePicker12.Name = "dateTimePicker12";
            this.dateTimePicker12.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker12.TabIndex = 66;
            // 
            // progressBar8
            // 
            this.progressBar8.Location = new System.Drawing.Point(303, 73);
            this.progressBar8.Name = "progressBar8";
            this.progressBar8.Size = new System.Drawing.Size(223, 23);
            this.progressBar8.TabIndex = 63;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(190, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 23);
            this.button1.TabIndex = 62;
            this.button1.Text = "1시간 적산";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Modification_Hour);
            // 
            // dateTimePicker10
            // 
            this.dateTimePicker10.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker10.Location = new System.Drawing.Point(98, 74);
            this.dateTimePicker10.Name = "dateTimePicker10";
            this.dateTimePicker10.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker10.TabIndex = 61;
            // 
            // dateTimePicker9
            // 
            this.dateTimePicker9.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker9.Location = new System.Drawing.Point(6, 74);
            this.dateTimePicker9.Name = "dateTimePicker9";
            this.dateTimePicker9.Size = new System.Drawing.Size(86, 21);
            this.dateTimePicker9.TabIndex = 60;
            // 
            // progressBar7
            // 
            this.progressBar7.Location = new System.Drawing.Point(303, 158);
            this.progressBar7.Name = "progressBar7";
            this.progressBar7.Size = new System.Drawing.Size(223, 23);
            this.progressBar7.TabIndex = 59;
            // 
            // progressBar6
            // 
            this.progressBar6.Location = new System.Drawing.Point(303, 129);
            this.progressBar6.Name = "progressBar6";
            this.progressBar6.Size = new System.Drawing.Size(223, 23);
            this.progressBar6.TabIndex = 58;
            // 
            // progressBar5
            // 
            this.progressBar5.Location = new System.Drawing.Point(303, 100);
            this.progressBar5.Name = "progressBar5";
            this.progressBar5.Size = new System.Drawing.Size(223, 23);
            this.progressBar5.TabIndex = 57;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.button61);
            this.groupBox8.Controls.Add(this.button60);
            this.groupBox8.Controls.Add(this.button59);
            this.groupBox8.Controls.Add(this.button58);
            this.groupBox8.Controls.Add(this.button57);
            this.groupBox8.Controls.Add(this.button56);
            this.groupBox8.Controls.Add(this.button55);
            this.groupBox8.Controls.Add(this.button54);
            this.groupBox8.Controls.Add(this.button53);
            this.groupBox8.Controls.Add(this.button52);
            this.groupBox8.Controls.Add(this.button51);
            this.groupBox8.Controls.Add(this.button50);
            this.groupBox8.Controls.Add(this.button49);
            this.groupBox8.Controls.Add(this.button48);
            this.groupBox8.Controls.Add(this.button47);
            this.groupBox8.Controls.Add(this.button46);
            this.groupBox8.Controls.Add(this.button45);
            this.groupBox8.Controls.Add(this.button44);
            this.groupBox8.Controls.Add(this.button43);
            this.groupBox8.Controls.Add(this.button42);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.label20);
            this.groupBox8.Controls.Add(this.button39);
            this.groupBox8.Controls.Add(this.button40);
            this.groupBox8.Controls.Add(this.button41);
            this.groupBox8.Controls.Add(this.label19);
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.label17);
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Controls.Add(this.label14);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.button2);
            this.groupBox8.Controls.Add(this.button22);
            this.groupBox8.Controls.Add(this.button21);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.label10);
            this.groupBox8.Controls.Add(this.button20);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Controls.Add(this.label8);
            this.groupBox8.Controls.Add(this.button18);
            this.groupBox8.Controls.Add(this.button6);
            this.groupBox8.Controls.Add(this.button37);
            this.groupBox8.Controls.Add(this.button5);
            this.groupBox8.Controls.Add(this.button38);
            this.groupBox8.Controls.Add(this.button35);
            this.groupBox8.Controls.Add(this.button36);
            this.groupBox8.Controls.Add(this.button33);
            this.groupBox8.Controls.Add(this.button7);
            this.groupBox8.Controls.Add(this.button34);
            this.groupBox8.Controls.Add(this.button31);
            this.groupBox8.Controls.Add(this.button32);
            this.groupBox8.Controls.Add(this.button29);
            this.groupBox8.Controls.Add(this.button30);
            this.groupBox8.Controls.Add(this.button16);
            this.groupBox8.Controls.Add(this.button27);
            this.groupBox8.Controls.Add(this.button15);
            this.groupBox8.Controls.Add(this.button28);
            this.groupBox8.Controls.Add(this.button26);
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Controls.Add(this.button25);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this.button17);
            this.groupBox8.Controls.Add(this.button11);
            this.groupBox8.Controls.Add(this.button14);
            this.groupBox8.Controls.Add(this.button10);
            this.groupBox8.Location = new System.Drawing.Point(4, 114);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(260, 517);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "수집";
            // 
            // button61
            // 
            this.button61.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button61.Image = ((System.Drawing.Image)(resources.GetObject("button61.Image")));
            this.button61.Location = new System.Drawing.Point(126, 245);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(25, 23);
            this.button61.TabIndex = 106;
            this.button61.UseVisualStyleBackColor = true;
            // 
            // button60
            // 
            this.button60.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button60.Image = ((System.Drawing.Image)(resources.GetObject("button60.Image")));
            this.button60.Location = new System.Drawing.Point(126, 476);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(25, 23);
            this.button60.TabIndex = 105;
            this.button60.UseVisualStyleBackColor = true;
            // 
            // button59
            // 
            this.button59.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button59.Image = ((System.Drawing.Image)(resources.GetObject("button59.Image")));
            this.button59.Location = new System.Drawing.Point(126, 447);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(25, 23);
            this.button59.TabIndex = 104;
            this.button59.UseVisualStyleBackColor = true;
            // 
            // button58
            // 
            this.button58.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button58.Image = ((System.Drawing.Image)(resources.GetObject("button58.Image")));
            this.button58.Location = new System.Drawing.Point(126, 418);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(25, 23);
            this.button58.TabIndex = 103;
            this.button58.UseVisualStyleBackColor = true;
            // 
            // button57
            // 
            this.button57.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button57.Image = ((System.Drawing.Image)(resources.GetObject("button57.Image")));
            this.button57.Location = new System.Drawing.Point(126, 390);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(25, 23);
            this.button57.TabIndex = 102;
            this.button57.UseVisualStyleBackColor = true;
            // 
            // button56
            // 
            this.button56.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button56.Image = ((System.Drawing.Image)(resources.GetObject("button56.Image")));
            this.button56.Location = new System.Drawing.Point(126, 361);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(25, 23);
            this.button56.TabIndex = 101;
            this.button56.UseVisualStyleBackColor = true;
            // 
            // button55
            // 
            this.button55.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button55.Image = ((System.Drawing.Image)(resources.GetObject("button55.Image")));
            this.button55.Location = new System.Drawing.Point(126, 274);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(25, 23);
            this.button55.TabIndex = 100;
            this.button55.UseVisualStyleBackColor = true;
            // 
            // button54
            // 
            this.button54.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button54.Image = ((System.Drawing.Image)(resources.GetObject("button54.Image")));
            this.button54.Location = new System.Drawing.Point(126, 332);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(25, 23);
            this.button54.TabIndex = 99;
            this.button54.UseVisualStyleBackColor = true;
            // 
            // button53
            // 
            this.button53.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button53.Image = ((System.Drawing.Image)(resources.GetObject("button53.Image")));
            this.button53.Location = new System.Drawing.Point(126, 303);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(25, 23);
            this.button53.TabIndex = 98;
            this.button53.UseVisualStyleBackColor = true;
            // 
            // button52
            // 
            this.button52.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button52.Image = ((System.Drawing.Image)(resources.GetObject("button52.Image")));
            this.button52.Location = new System.Drawing.Point(126, 216);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(25, 23);
            this.button52.TabIndex = 4;
            this.button52.UseVisualStyleBackColor = true;
            // 
            // button51
            // 
            this.button51.Location = new System.Drawing.Point(206, 476);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(43, 23);
            this.button51.TabIndex = 97;
            this.button51.Text = "정지";
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.WHHpress_Result_stop);
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(206, 447);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(43, 23);
            this.button50.TabIndex = 96;
            this.button50.Text = "정지";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.RevenueRatio_Analysis_stop);
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(206, 418);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(43, 23);
            this.button49.TabIndex = 95;
            this.button49.Text = "정지";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.Pipe_Info_stop);
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(206, 390);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(43, 23);
            this.button48.TabIndex = 94;
            this.button48.Text = "정지";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.DM_Used_stop);
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(206, 361);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(43, 23);
            this.button47.TabIndex = 93;
            this.button47.Text = "정지";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.DM_Info_stop);
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(206, 332);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(43, 23);
            this.button46.TabIndex = 92;
            this.button46.Text = "정지";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.STWCHRG_stop);
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(206, 303);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(43, 23);
            this.button45.TabIndex = 91;
            this.button45.Text = "정지";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.MTRCHGINFO_stop);
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(206, 274);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(43, 23);
            this.button44.TabIndex = 90;
            this.button44.Text = "정지";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.CAINFO_stop);
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(206, 245);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(43, 23);
            this.button43.TabIndex = 89;
            this.button43.Text = "정지";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.DMWSRSRC_stop);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(206, 216);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(43, 23);
            this.button42.TabIndex = 65;
            this.button42.Text = "정지";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.DMINFO_stop);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 481);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(101, 12);
            this.label22.TabIndex = 88;
            this.label22.Text = "평균수압지점계산";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 452);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 12);
            this.label21.TabIndex = 87;
            this.label21.Text = "유수율분석";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 423);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 12);
            this.label20.TabIndex = 86;
            this.label20.Text = "관로길이계산";
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(157, 476);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(43, 23);
            this.button39.TabIndex = 85;
            this.button39.Text = "시작";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.WHHpress_Result);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(157, 447);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(43, 23);
            this.button40.TabIndex = 84;
            this.button40.Text = "시작";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.RevenueRatio_Analysis);
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(157, 418);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(43, 23);
            this.button41.TabIndex = 83;
            this.button41.Text = "시작";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.Pipe_Info);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 395);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(101, 12);
            this.label19.TabIndex = 82;
            this.label19.Text = "블록별사용량계산";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 366);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 12);
            this.label18.TabIndex = 81;
            this.label18.Text = "급수전수계산";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 337);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 80;
            this.label17.Text = "요금정보";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 308);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 12);
            this.label16.TabIndex = 79;
            this.label16.Text = "계량기교체";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 279);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 78;
            this.label15.Text = "민원정보";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 250);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 12);
            this.label14.TabIndex = 77;
            this.label14.Text = "상하수도자원";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 221);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 76;
            this.label13.Text = "수용가정보";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 192);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 75;
            this.label12.Text = "누수감시";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 163);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 74;
            this.label11.Text = "정시수압";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 134);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 73;
            this.label10.Text = "야간최소유량";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 12);
            this.label9.TabIndex = 72;
            this.label9.Text = "1일 적산데이터";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 12);
            this.label8.TabIndex = 71;
            this.label8.Text = "1시간적산데이터";
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(157, 390);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(43, 23);
            this.button37.TabIndex = 70;
            this.button37.Text = "시작";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.DM_Used);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(157, 361);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(43, 23);
            this.button38.TabIndex = 69;
            this.button38.Text = "시작";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.DM_Info);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(157, 332);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(43, 23);
            this.button35.TabIndex = 68;
            this.button35.Text = "시작";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.STWCHRG);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(157, 303);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(43, 23);
            this.button36.TabIndex = 67;
            this.button36.Text = "시작";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.MTRCHGINFO);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(157, 274);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(43, 23);
            this.button33.TabIndex = 66;
            this.button33.Text = "시작";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.CAINFO);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(157, 245);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(43, 23);
            this.button34.TabIndex = 65;
            this.button34.Text = "시작";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.DMWSRSRC);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(157, 216);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(43, 23);
            this.button31.TabIndex = 64;
            this.button31.Text = "시작";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.DMINFO);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(157, 187);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(43, 23);
            this.button32.TabIndex = 63;
            this.button32.Text = "시작";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.MonitoringLeakage);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(157, 158);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(43, 23);
            this.button29.TabIndex = 62;
            this.button29.Text = "시작";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.AveragePressure);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(157, 129);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(43, 23);
            this.button30.TabIndex = 61;
            this.button30.Text = "시작";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.MinimumNightFlow);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(157, 100);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(43, 23);
            this.button27.TabIndex = 60;
            this.button27.Text = "시작";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.Accumulation_Day);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(157, 71);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(43, 23);
            this.button28.TabIndex = 59;
            this.button28.Text = "시작";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.Accumulation_Hour);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(157, 42);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(43, 23);
            this.button26.TabIndex = 58;
            this.button26.Text = "시작";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.Accumulation_ToDay);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 57;
            this.label7.Text = "금일적산데이터";
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(157, 13);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(43, 23);
            this.button25.TabIndex = 56;
            this.button25.Text = "시작";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.GatherRealtime);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "실시간 데이터";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.pictureBox3);
            this.groupBox7.Controls.Add(this.label3);
            this.groupBox7.Controls.Add(this.pictureBox1);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.pictureBox2);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Location = new System.Drawing.Point(711, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(91, 106);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "범례";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button24);
            this.groupBox6.Controls.Add(this.button19);
            this.groupBox6.Controls.Add(this.progressBar4);
            this.groupBox6.Controls.Add(this.progressBar3);
            this.groupBox6.Location = new System.Drawing.Point(10, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(501, 106);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "프로세스관리";
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(389, 18);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(99, 53);
            this.button24.TabIndex = 3;
            this.button24.Text = "종료";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.BatchEnd);
            // 
            // label_auto_modify
            // 
            this.label_auto_modify.AutoSize = true;
            this.label_auto_modify.Location = new System.Drawing.Point(13, 86);
            this.label_auto_modify.Name = "label_auto_modify";
            this.label_auto_modify.Size = new System.Drawing.Size(81, 12);
            this.label_auto_modify.TabIndex = 8;
            this.label_auto_modify.Text = "자동보정 시간";
            // 
            // label_auto_modify_status
            // 
            this.label_auto_modify_status.AutoSize = true;
            this.label_auto_modify_status.Location = new System.Drawing.Point(80, 31);
            this.label_auto_modify_status.Name = "label_auto_modify_status";
            this.label_auto_modify_status.Size = new System.Drawing.Size(89, 12);
            this.label_auto_modify_status.TabIndex = 9;
            this.label_auto_modify_status.Text = ": 자동보정 정지";
            // 
            // frmHOleDB2WOracle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(836, 823);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(842, 848);
            this.Name = "frmHOleDB2WOracle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmHOleDB2WOracle";
            this.Load += new System.EventHandler(this.frmHOleDB2WOracle_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.DateTimePicker dateTimePicker8;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.DateTimePicker dateTimePicker7;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.ProgressBar progressBar4;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ProgressBar progressBar7;
        private System.Windows.Forms.ProgressBar progressBar6;
        private System.Windows.Forms.ProgressBar progressBar5;
        private System.Windows.Forms.ProgressBar progressBar9;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker dateTimePicker11;
        private System.Windows.Forms.DateTimePicker dateTimePicker12;
        private System.Windows.Forms.ProgressBar progressBar8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker10;
        private System.Windows.Forms.DateTimePicker dateTimePicker9;
        private System.Windows.Forms.ProgressBar progressBar_WHHpress_Result_Modify;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.DateTimePicker dateTimePicker31;
        private System.Windows.Forms.DateTimePicker dateTimePicker32;
        private System.Windows.Forms.ProgressBar progressBar_RevenueRatio_Analysis_Modify;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.DateTimePicker dateTimePicker29;
        private System.Windows.Forms.DateTimePicker dateTimePicker30;
        private System.Windows.Forms.ProgressBar progressBar_Pipe_Info_Modify;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.DateTimePicker dateTimePicker27;
        private System.Windows.Forms.DateTimePicker dateTimePicker28;
        private System.Windows.Forms.ProgressBar progressBar_DM_Used_Modify;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.DateTimePicker dateTimePicker25;
        private System.Windows.Forms.DateTimePicker dateTimePicker26;
        private System.Windows.Forms.ProgressBar progressBar_DM_Info_Modify;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.DateTimePicker dateTimePicker23;
        private System.Windows.Forms.DateTimePicker dateTimePicker24;
        private System.Windows.Forms.ProgressBar progressBar_STWCHRG_Modify;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.DateTimePicker dateTimePicker21;
        private System.Windows.Forms.DateTimePicker dateTimePicker22;
        private System.Windows.Forms.ProgressBar progressBar_MTRCHGINFO_Modify;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.DateTimePicker dateTimePicker19;
        private System.Windows.Forms.DateTimePicker dateTimePicker20;
        private System.Windows.Forms.ProgressBar progressBar_CAINFO_Modify;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.DateTimePicker dateTimePicker17;
        private System.Windows.Forms.DateTimePicker dateTimePicker18;
        private System.Windows.Forms.ProgressBar progressBar_DMWSRSRC_Modify;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.DateTimePicker dateTimePicker15;
        private System.Windows.Forms.DateTimePicker dateTimePicker16;
        private System.Windows.Forms.ProgressBar progressBar_DMINFO_Modify;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DateTimePicker dateTimePicker13;
        private System.Windows.Forms.DateTimePicker dateTimePicker14;
        private System.Windows.Forms.DateTimePicker dateTimePicker_auto_modify;
        private System.Windows.Forms.CheckBox checkBox_auto_modify;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_auto_mdodify;
        private System.Windows.Forms.Label label_auto_modify;
        private System.Windows.Forms.Label label_auto_modify_status;

    }
}