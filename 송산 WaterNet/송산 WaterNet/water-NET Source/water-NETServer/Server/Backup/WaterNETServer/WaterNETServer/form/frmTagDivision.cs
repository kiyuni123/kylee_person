﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WaterNETServer.Common.utils;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using WaterNETServer.dao;
using EMFrame.log;

namespace WaterNETServer.form
{
    public partial class frmTagDivision : Form, IForminterface
    {
        #region 선언부
        
        public static Hashtable MessagePool = new Hashtable();              // 태그구분의 Validation결과를 담을 전역 Message Pool

        GlobalVariable gVar = new GlobalVariable();
        private string _formName = string.Empty;
        
        public frmTagDivision()
        {
            _formName = gVar.getFormName(11);
            InitializeComponent();
            InitializeSetting();
        }

        OracleWork oWork = new OracleWork();
        //OleDBWork hWork = new OleDBWork();
        FormUtils fu = new FormUtils();
        Common.utils.Common cm = new Common.utils.Common();

        DataTable taglistDataTable;
        DataTable taggbnDataTable;

        #endregion

        #region IForminterface 멤버
        string IForminterface.FormID
        {
            get { return this.Text; }
        }
        string IForminterface.FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion

        #region 초기화
        private void InitializeSetting()
        {
            InitializeTagList();
            InitializeTagGbn();

            fu.SetGridStyle_TagImport(ultraGrid_TagList);
            fu.SetGridStyle_TagImport(ultraGrid_TagGbn);

            InitComboBrCode();
            InitComboFnCode();
            InitComboUseYn();
            InitComboVirtualTag();
            InitComboTagGbn();
            InitComboTagGbnImpot();
        }
        private void InitializeTagList()
        {
            UltraGridColumn tagListColumn;
            tagListColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            tagListColumn.CellActivation = Activation.AllowEdit;
            tagListColumn.CellClickAction = CellClickAction.Edit;
            tagListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            tagListColumn.Key = "IDX";
            tagListColumn.Header.Caption = "";
            tagListColumn.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            tagListColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListColumn.Width = 50;
            tagListColumn.Hidden = false;

            tagListColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            tagListColumn.Key = "TAGNAME";
            tagListColumn.Header.Caption = "태그명";
            tagListColumn.CellActivation = Activation.NoEdit;
            tagListColumn.CellClickAction = CellClickAction.RowSelect;
            tagListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListColumn.Width = 200;
            tagListColumn.Hidden = false;

            tagListColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            tagListColumn.Key = "DESCRIPTION";
            tagListColumn.Header.Caption = "태그설명";
            tagListColumn.CellActivation = Activation.NoEdit;
            tagListColumn.CellClickAction = CellClickAction.RowSelect;
            tagListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListColumn.Width = 200;
            tagListColumn.Hidden = false;

            tagListColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            tagListColumn.Key = "BR_CODE";
            tagListColumn.Header.Caption = "변량기호";
            tagListColumn.CellActivation = Activation.NoEdit;
            tagListColumn.CellClickAction = CellClickAction.RowSelect;
            tagListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListColumn.Width = 50;
            tagListColumn.Hidden = false;

            tagListColumn = ultraGrid_TagList.DisplayLayout.Bands[0].Columns.Add();
            tagListColumn.Key = "FN_CODE";
            tagListColumn.Header.Caption = "기능기호";
            tagListColumn.CellActivation = Activation.NoEdit;
            tagListColumn.CellClickAction = CellClickAction.RowSelect;
            tagListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListColumn.Width = 50;
            tagListColumn.Hidden = false;
        }
        private void InitializeTagGbn()
        {
            UltraGridColumn tagGbnColumn;
            tagGbnColumn = ultraGrid_TagGbn.DisplayLayout.Bands[0].Columns.Add();
            tagGbnColumn.CellActivation = Activation.AllowEdit;
            tagGbnColumn.CellClickAction = CellClickAction.Edit;
            tagGbnColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            tagGbnColumn.Key = "IDX";
            tagGbnColumn.Header.Caption = "";
            tagGbnColumn.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            tagGbnColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagGbnColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagGbnColumn.Width = 50;
            tagGbnColumn.Hidden = false;

            tagGbnColumn = ultraGrid_TagGbn.DisplayLayout.Bands[0].Columns.Add();
            tagGbnColumn.Key = "TAGNAME";
            tagGbnColumn.Header.Caption = "태그명";
            tagGbnColumn.CellActivation = Activation.NoEdit;
            tagGbnColumn.CellClickAction = CellClickAction.RowSelect;
            tagGbnColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagGbnColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagGbnColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagGbnColumn.Width = 200;
            tagGbnColumn.Hidden = false;

            tagGbnColumn = ultraGrid_TagGbn.DisplayLayout.Bands[0].Columns.Add();
            tagGbnColumn.Key = "TAG_GBN";
            tagGbnColumn.Header.Caption = "태그구분";
            tagGbnColumn.CellActivation = Activation.NoEdit;
            tagGbnColumn.CellClickAction = CellClickAction.RowSelect;
            tagGbnColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagGbnColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagGbnColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagGbnColumn.Width = 70;
            tagGbnColumn.Hidden = false;

            tagGbnColumn = ultraGrid_TagGbn.DisplayLayout.Bands[0].Columns.Add();
            tagGbnColumn.Key = "DESCRIPTION";
            tagGbnColumn.Header.Caption = "태그설명";
            tagGbnColumn.CellActivation = Activation.NoEdit;
            tagGbnColumn.CellClickAction = CellClickAction.RowSelect;
            tagGbnColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagGbnColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagGbnColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagGbnColumn.Width = 200;
            tagGbnColumn.Hidden = false;

        }
        #region 콤보박스
        /// <summary>
        /// 변량및기기기호
        /// </summary>
        private void InitComboBrCode()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7004");

            DataSet dSet = oWork.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox_br_code, tableName);
        }

        /// <summary>
        /// 기능기호
        /// </summary>
        private void InitComboFnCode()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7005");

            DataSet dSet = oWork.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox_fn_code, tableName);
        }
        /// <summary>
        /// 사용여부
        /// </summary>
        private void InitComboUseYn()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7009");

            DataSet dSet = oWork.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox_use_yn, tableName);
        }
        /// <summary>
        /// 가상태그구분
        /// </summary>
        private void InitComboVirtualTag()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7011");

            DataSet dSet = oWork.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox_virtualTag, tableName);
        }
        /// <summary>
        /// 태그구분
        /// </summary>
        private void InitComboTagGbn()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7010");

            DataSet dSet = oWork.SelectCodeWithValueList(tableName, param);

            cm.SetComboBox(dSet, comboBox_tag_gbn, tableName);
        }
        /// <summary>
        /// 태그구분
        /// </summary>
        private void InitComboTagGbnImpot()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7010");

            DataSet dSet = oWork.SelectCodeWithValueList(tableName, param);

            cm.SetComboBox(dSet, comboBox_tag_gbn_import, tableName);
        }
        #endregion
        #endregion

        #region  버튼엑션
        #region 태그리스트 쪽
        private void btn_GetTagList_Click(object sender, EventArgs e)
        {
            GetTagList();
        }
        private void btn_virtualTag_Click(object sender, EventArgs e)
        {
            if (CountSelectedTagList() > 0)
            {
                if(!cb_hr.Checked && !cb_td.Checked && !cb_yd.Checked)
                {
                    MessageBox.Show("선택된 태그구분이 없습니다.", "경고");
                }
                StringBuilder msg = new StringBuilder();
                if (cb_hr.Checked) msg.AppendLine("==시간적산가상태그==").AppendLine(Create_virtualTag("1H"));
                if (cb_td.Checked) msg.AppendLine("==금일적산가상태그==").AppendLine(Create_virtualTag("TD"));
                if (cb_yd.Checked) msg.AppendLine("==전일적산가상태그==").AppendLine(Create_virtualTag("YD"));
                if (msg.Length > 0)
                {
                    MessageBox.Show(msg.ToString(), "정보");
                    GetTagList();
                }
            }
            else
            {
                MessageBox.Show("선택된 레코드가 없습니다.", "경고");
            }
        }
        /// <summary>
        /// 태그리스트 삭제
        /// </summary>
        private void btn_DeleteTagList_Click(object sender, EventArgs e)
        {
            int selectedtaglistCnt = CountSelectedTagList();
            if(selectedtaglistCnt > 0)
            {
                int cnt = DeleteTagList();
                if(cnt > 0)
                {
                    MessageBox.Show(cnt + "건의 데이터가 삭제되었습니다.", "정보");
                    GetTagList();
                }
                else
                {
                    MessageBox.Show("삭제된 데이터가 없습니다.", "정보");
                }
            }
            else
            {
                MessageBox.Show("선택된 레코드가 없습니다.", "경고");
            }
        }


        #endregion

        #region 태그구분 쪽
        /// <summary>
        /// 태그구분 조회
        /// </summary>
        private void btn_GetTagGbn_Click(object sender, EventArgs e)
        {
            GetTagGbn();
        }
        /// <summary>
        /// 태그구분 삭제
        /// </summary>
        private void btn_DeleteTagGbn_Click(object sender, EventArgs e)
        {
            int selectedtaggbnCnt = CountSelectedTagGbn();
            if (selectedtaggbnCnt > 0)
            {
                int cnt = DeleteTagGbn();
                if (cnt > 0)
                {
                    MessageBox.Show(cnt + "건의 데이터가 삭제되었습니다.", "정보");
                    GetTagGbn();
                }
                else
                {
                    MessageBox.Show("삭제된 데이터가 없습니다.", "정보");
                }
            }
            else
            {
                MessageBox.Show("선택된 태그구분 레코드가 없습니다.", "경고");
            }
        }
        /// <summary>
        /// 태그구분 입력
        /// </summary>
        private void btn_taggbnImport_Click(object sender, EventArgs e)
        {
            int selectedtaglistCnt = CountSelectedTagList();
            if (selectedtaglistCnt > 0)
            {
                int cnt = TagGbnImport();

                StringBuilder msg = new StringBuilder();

                if (cnt > 0)
                {
                    msg.AppendLine(cnt + "건의 태그구분이 작성되었습니다.");
                    GetTagGbn();
                }
                else
                {
                    msg.AppendLine(selectedtaglistCnt + "건의 레코드는 중복으로 생략되었습니다.");
                }

                foreach (DictionaryEntry de in MessagePool)
                {
                    if ("DUP".Equals(de.Key))
                    {
                        msg.Append("태그구분 중복 생략 : ").AppendLine(de.Value.ToString());
                    }
                    if ("VLD".Equals(de.Key))
                    {
                        msg.Append("동일지역, 동일태그구분 중복 생략 : ").AppendLine(de.Value.ToString());
                    }
                }

                MessageBox.Show(msg.ToString(), "정보");
            }
            else
            {
                MessageBox.Show("태그리스트에서 레코드를 선택해야합니다.", "경고");
            }
        }
        #endregion

        #endregion

        #region  기능함수

        #region 태그리스트 쪽
        /// <summary>
        /// 태그리스트 검색
        /// </summary>
        private void GetTagList()
        {
            // 검색조건
            Hashtable cond = new Hashtable();
            cond.Add("TAGNAME", textBox_tagname.Text);
            cond.Add("DESCRIPTION", textBox_description.Text);
            cond.Add("BR_CODE", comboBox_br_code.SelectedValue.ToString());
            cond.Add("FN_CODE", comboBox_fn_code.SelectedValue.ToString());
            cond.Add("USE_YN", comboBox_use_yn.SelectedValue.ToString());
            cond.Add("VTAG", comboBox_virtualTag.SelectedValue.ToString());

            if (taglistDataTable != null)
            {
                taglistDataTable = null;
                ultraGrid_TagList.DataSource = null;
                InitializeTagList();
            }

            string tableName = "TagList";
            taglistDataTable = oWork.GetTagList(tableName, cond).Tables[0];

            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.Int16");
            col.ColumnName = "IDX";
            col.DefaultValue = 0;
            taglistDataTable.Columns.Add(col);

            ultraGrid_TagList.DataSource = taglistDataTable;

            fu.SetGridStyle_PerformAutoResize(ultraGrid_TagList);
        }

        /// <summary>
        /// 가상태그 만들 데이터 테이블 셋팅
        /// </summary>
        /// <param name="dt"></param>
        private void SetTagListDataTable(DataTable dt)
        {
            // 데이터 테이블 컬럼 셋팅
            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "TAGNAME";
            col.AutoIncrement = false;
            col.DefaultValue = 0;
            dt.Columns.Add(col);

            DataColumn col2 = new DataColumn();
            col2.DataType = System.Type.GetType("System.String");
            col2.ColumnName = "DESCRIPTION";
            col2.AutoIncrement = false;
            col2.DefaultValue = 0;
            dt.Columns.Add(col2);

            DataColumn col3 = new DataColumn();
            col3.DataType = System.Type.GetType("System.String");
            col3.ColumnName = "BR_CODE";
            col3.AutoIncrement = false;
            col3.DefaultValue = 0;
            dt.Columns.Add(col3);

            DataColumn col4 = new DataColumn();
            col4.DataType = System.Type.GetType("System.String");
            col4.ColumnName = "FN_CODE";
            col4.AutoIncrement = false;
            col4.DefaultValue = 0;
            dt.Columns.Add(col4);

            // 데이터 테이블 프라이머리 키 셋팅
            // dt.Rows.Contains를 사용하려면 프라이머리키가 셋팅되어 있어야 함
            dt.PrimaryKey = new DataColumn[] { col };
        }
        /// <summary>
        /// 태그리스트의 선택된 레코드수를 반환한다.
        /// </summary>
        /// <returns></returns>
        private int CountSelectedTagList()
        {
            int returnValue = 0;
            foreach (UltraGridRow row in ultraGrid_TagList.Rows)
            {
                if ("1".Equals(row.Cells[4].Text)) returnValue++;
            }
            return returnValue;
        }

        /// <summary>
        /// 적산가상태그 만들기
        /// </summary>
        private string Create_virtualTag(string vtag_gbn)
        {
            string rtnValue = string.Empty;
            try
            {
                // 성공한 가상태그 만들기 갯수 
                int makeCnt = 0;
                // 실패한 가상태그 만들기 갯수
                int passCnt = 0;
                // 가상태그 구분
                string _vtag_gbn = vtag_gbn;
                // 생성할 가상태그 담을 데이터테이블
                DataTable dt = new DataTable();
                // 데이터 테이블 셋팅
                SetTagListDataTable(dt);

                foreach (UltraGridRow row in ultraGrid_TagList.Rows)
                {
                    // 체크된 그리드만 선택
                    if ("1".Equals(row.Cells[4].Text))
                    {
                        DataRow dr = dt.NewRow();

                        dr["TAGNAME"] = row.Cells[0].Text;
                        dr["DESCRIPTION"] = row.Cells[1].Text;
                        dr["BR_CODE"] = row.Cells[2].Text;
                        dr["FN_CODE"] = row.Cells[3].Text;

                        // 선택된것중 가상태그는 패스
                        if (row.Cells[0].Text.IndexOf("1H") > 0 || row.Cells[0].Text.IndexOf("TD") > 0 || row.Cells[0].Text.IndexOf("YD") > 0)
                        {
                            passCnt++;
                        }
                        else
                        {
                            dt.Rows.Add(dr);
                            makeCnt++;
                        }
                    }
                }
                if (makeCnt > 0 || passCnt > 0)
                {
                    StringBuilder msg = new StringBuilder();

                    // 가상태그 입력
                    int insertCnt = oWork.CreateVirtualTag(dt, _vtag_gbn);


                    if (insertCnt > 0)
                    {
                        msg.Append(insertCnt).AppendLine("건의 태그가 가상태그로 작성되었습니다.");
                    }
                    if ((makeCnt - insertCnt) == 0)
                    {

                    }
                    else
                    {
                        msg.Append(makeCnt - insertCnt).AppendLine("건의 태그는 중복태그로 생략되었습니다.");
                    }

                    if (passCnt > 0)
                    {
                        msg.Append(passCnt).AppendLine("건의 태그는 생략되었습니다.(현재 가상태그임)");
                    }

                    rtnValue = msg.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }

            return rtnValue;
        }
        /// <summary>
        /// 태그리스트를 삭제
        /// </summary>
        private int DeleteTagList()
        {
            int cnt = 0;
            DataTable dt = new DataTable();                  // 생성할 가상태그 담을 데이터테이블
            SetTagListDataTable(dt);                         // 데이터 테이블 셋팅

            // 삭제할 레코드를 담는다.
            foreach (UltraGridRow row in ultraGrid_TagList.Rows)
            {
                if ("1".Equals(row.Cells[4].Text))
                {
                    DataRow dr = dt.NewRow();

                    dr["TAGNAME"] = row.Cells[0].Text;

                    dt.Rows.Add(dr);
                }
            }
            // 담겨진 레코드를 삭제
            if (dt.Rows.Count > 0)
            {
                cnt = oWork.DeleteTagList(dt);
            }
            return cnt;
        }
        #endregion

        #region 태그구분 쪽
        /// <summary>
        /// 태그구분의 선택된 레코드수를 반환한다.
        /// </summary>
        /// <returns></returns>
        private int CountSelectedTagGbn()
        {
            int returnValue = 0;
            foreach (UltraGridRow row in ultraGrid_TagGbn.Rows)
            {
                if ("1".Equals(row.Cells[3].Text)) returnValue++;
            }
            return returnValue;
        }
        /// <summary>
        /// 태그구분 조회
        /// </summary>
        private void GetTagGbn()
        {
            // 검색조건
            Hashtable cond = new Hashtable();
            cond.Add("TAGNAME", textBox_tagname_gbn.Text);
            cond.Add("TAG_GBN", comboBox_tag_gbn.SelectedValue.ToString());
            cond.Add("DESCRIPTION", textBox_description_gbn.Text);

            if (taggbnDataTable != null)
            {
                taggbnDataTable = null;
                ultraGrid_TagGbn.DataSource = null;
                InitializeTagGbn();
            }

            string tableName = "TagGbn";
            taggbnDataTable = oWork.GetTagGbn(tableName, cond).Tables[0];

            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.Int16");
            col.ColumnName = "IDX";
            col.DefaultValue = 0;
            taggbnDataTable.Columns.Add(col);

            ultraGrid_TagGbn.DataSource = taggbnDataTable;

            fu.SetGridStyle_PerformAutoResize(ultraGrid_TagGbn);
        }
        /// <summary>
        /// 가상태그 만들 데이터 테이블 셋팅
        /// </summary>
        /// <param name="dt"></param>
        private void SetTagGbnDataTable(DataTable dt)
        {
            // 데이터 테이블 컬럼 셋팅
            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "TAGNAME";
            col.AutoIncrement = false;
            col.DefaultValue = 0;
            dt.Columns.Add(col);

            DataColumn col2 = new DataColumn();
            col2.DataType = System.Type.GetType("System.String");
            col2.ColumnName = "TAG_GBN";
            col2.AutoIncrement = false;
            col2.DefaultValue = 0;
            dt.Columns.Add(col2);

            // 데이터 테이블 프라이머리 키 셋팅
            // dt.Rows.Contains를 사용하려면 프라이머리키가 셋팅되어 있어야 함
            dt.PrimaryKey = new DataColumn[] { col, col2 };
        }
        /// <summary>
        /// 태그구분 삭제
        /// </summary>
        private int DeleteTagGbn()
        {
            int cnt = 0;
            DataTable dt = new DataTable();                 // 생성할 가상태그 담을 데이터테이블
            SetTagGbnDataTable(dt);                         // 데이터 테이블 셋팅
            
            // 삭제할 레코드를 담는다.
            foreach (UltraGridRow row in ultraGrid_TagGbn.Rows)
            {
                if ("1".Equals(row.Cells[3].Text))
                {
                    DataRow dr = dt.NewRow();

                    dr["TAGNAME"] = row.Cells[0].Text;
                    dr["TAG_GBN"] = row.Cells[1].Text;

                    dt.Rows.Add(dr);
                }
            }
            // 담겨진 레코드를 삭제
            if(dt.Rows.Count > 0)
            {
                cnt = oWork.DeleteTagGbn(dt);
            }
            return cnt;
        }
        /// <summary>
        /// 태그구분 임포트
        /// </summary>
        private int TagGbnImport()
        {
            int cnt = 0;
            string vtag_gbn = comboBox_tag_gbn_import.SelectedValue.ToString();
            DataTable dt = new DataTable();                 // 생성할 가상태그 담을 데이터테이블
            SetTagListDataTable(dt);                        // 데이터 테이블 셋팅

            // 임포트할 레코드를 담는다.
            foreach (UltraGridRow row in ultraGrid_TagList.Rows)
            {
                if ("1".Equals(row.Cells[4].Text))
                {
                    DataRow dr = dt.NewRow();

                    dr["TAGNAME"] = row.Cells[0].Text;
                    dr["DESCRIPTION"] = row.Cells[1].Text;
                    dr["BR_CODE"] = row.Cells[2].Text;
                    dr["FN_CODE"] = row.Cells[3].Text;

                    dt.Rows.Add(dr);
                }
            }

            // 담겨진 레코드를 임포트
            if (dt.Rows.Count > 0)
            {
                cnt = oWork.ImportTagGbn(dt, vtag_gbn, MessagePool);

                foreach (DictionaryEntry de in MessagePool)
                {
                    Console.Write(de.Key);
                    Console.Write(":");
                    Console.WriteLine(de.Value);
                }
            }
            return cnt;
        }
        #endregion

        #endregion

    }
}
