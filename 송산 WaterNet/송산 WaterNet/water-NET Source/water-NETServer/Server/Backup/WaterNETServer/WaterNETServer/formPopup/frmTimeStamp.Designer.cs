﻿namespace WaterNETServer.formPopup
{
    partial class frmTimeStamp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker_timestamp_minute_from = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_timestamp_minute_to = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_timestamp_hour = new System.Windows.Forms.Button();
            this.dateTimePicker_timestamp_hour_to = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_timestamp_hour_from = new System.Windows.Forms.DateTimePicker();
            this.btn_timestamp_minute = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateTimePicker_timestamp_minute_from
            // 
            this.dateTimePicker_timestamp_minute_from.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePicker_timestamp_minute_from.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_timestamp_minute_from.Location = new System.Drawing.Point(6, 20);
            this.dateTimePicker_timestamp_minute_from.Name = "dateTimePicker_timestamp_minute_from";
            this.dateTimePicker_timestamp_minute_from.Size = new System.Drawing.Size(121, 21);
            this.dateTimePicker_timestamp_minute_from.TabIndex = 0;
            // 
            // dateTimePicker_timestamp_minute_to
            // 
            this.dateTimePicker_timestamp_minute_to.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePicker_timestamp_minute_to.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_timestamp_minute_to.Location = new System.Drawing.Point(134, 20);
            this.dateTimePicker_timestamp_minute_to.Name = "dateTimePicker_timestamp_minute_to";
            this.dateTimePicker_timestamp_minute_to.Size = new System.Drawing.Size(121, 21);
            this.dateTimePicker_timestamp_minute_to.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_timestamp_hour);
            this.groupBox1.Controls.Add(this.dateTimePicker_timestamp_hour_to);
            this.groupBox1.Controls.Add(this.dateTimePicker_timestamp_hour_from);
            this.groupBox1.Controls.Add(this.btn_timestamp_minute);
            this.groupBox1.Controls.Add(this.dateTimePicker_timestamp_minute_to);
            this.groupBox1.Controls.Add(this.dateTimePicker_timestamp_minute_from);
            this.groupBox1.Location = new System.Drawing.Point(12, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(415, 91);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "타임스템프 생성";
            // 
            // btn_timestamp_hour
            // 
            this.btn_timestamp_hour.Location = new System.Drawing.Point(262, 48);
            this.btn_timestamp_hour.Name = "btn_timestamp_hour";
            this.btn_timestamp_hour.Size = new System.Drawing.Size(143, 23);
            this.btn_timestamp_hour.TabIndex = 5;
            this.btn_timestamp_hour.Text = "시단위";
            this.btn_timestamp_hour.UseVisualStyleBackColor = true;
            this.btn_timestamp_hour.Click += new System.EventHandler(this.btn_timestamp_hour_Click);
            // 
            // dateTimePicker_timestamp_hour_to
            // 
            this.dateTimePicker_timestamp_hour_to.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePicker_timestamp_hour_to.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_timestamp_hour_to.Location = new System.Drawing.Point(134, 48);
            this.dateTimePicker_timestamp_hour_to.Name = "dateTimePicker_timestamp_hour_to";
            this.dateTimePicker_timestamp_hour_to.Size = new System.Drawing.Size(121, 21);
            this.dateTimePicker_timestamp_hour_to.TabIndex = 4;
            // 
            // dateTimePicker_timestamp_hour_from
            // 
            this.dateTimePicker_timestamp_hour_from.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePicker_timestamp_hour_from.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_timestamp_hour_from.Location = new System.Drawing.Point(7, 48);
            this.dateTimePicker_timestamp_hour_from.Name = "dateTimePicker_timestamp_hour_from";
            this.dateTimePicker_timestamp_hour_from.Size = new System.Drawing.Size(120, 21);
            this.dateTimePicker_timestamp_hour_from.TabIndex = 3;
            // 
            // btn_timestamp_minute
            // 
            this.btn_timestamp_minute.Location = new System.Drawing.Point(262, 20);
            this.btn_timestamp_minute.Name = "btn_timestamp_minute";
            this.btn_timestamp_minute.Size = new System.Drawing.Size(143, 23);
            this.btn_timestamp_minute.TabIndex = 2;
            this.btn_timestamp_minute.Text = "분단위";
            this.btn_timestamp_minute.UseVisualStyleBackColor = true;
            this.btn_timestamp_minute.Click += new System.EventHandler(this.btn_timestamp_minute_Click);
            // 
            // frmTimeStamp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 114);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmTimeStamp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmTimeStamp";
            this.Load += new System.EventHandler(this.frmTimeStamp_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker_timestamp_minute_from;
        private System.Windows.Forms.DateTimePicker dateTimePicker_timestamp_minute_to;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_timestamp_minute;
        private System.Windows.Forms.Button btn_timestamp_hour;
        private System.Windows.Forms.DateTimePicker dateTimePicker_timestamp_hour_to;
        private System.Windows.Forms.DateTimePicker dateTimePicker_timestamp_hour_from;
    }
}