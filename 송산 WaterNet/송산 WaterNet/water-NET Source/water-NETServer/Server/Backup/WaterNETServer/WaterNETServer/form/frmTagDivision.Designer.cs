﻿namespace WaterNETServer.form
{
    partial class frmTagDivision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_DeleteTagList = new System.Windows.Forms.Button();
            this.cb_yd = new System.Windows.Forms.CheckBox();
            this.cb_td = new System.Windows.Forms.CheckBox();
            this.cb_hr = new System.Windows.Forms.CheckBox();
            this.btn_virtualTag_hour = new System.Windows.Forms.Button();
            this.btn_GetTagList = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_tagname = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_description = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.comboBox_br_code = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.comboBox_fn_code = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.comboBox_use_yn = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.comboBox_virtualTag = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_taggbnImport = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox_tag_gbn_import = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_DeleteTagGbn = new System.Windows.Forms.Button();
            this.btn_GetTagGbn = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_tagname_gbn = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.textBox_description_gbn = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.comboBox_tag_gbn = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ultraGrid_TagList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ultraGrid_TagGbn = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_TagList)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_TagGbn)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(1192, 625);
            this.splitContainer1.SplitterDistance = 163;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Size = new System.Drawing.Size(1192, 163);
            this.splitContainer2.SplitterDistance = 659;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btn_DeleteTagList);
            this.groupBox1.Controls.Add(this.cb_yd);
            this.groupBox1.Controls.Add(this.cb_td);
            this.groupBox1.Controls.Add(this.cb_hr);
            this.groupBox1.Controls.Add(this.btn_virtualTag_hour);
            this.groupBox1.Controls.Add(this.panel5);
            this.groupBox1.Controls.Add(this.btn_GetTagList);
            this.groupBox1.Controls.Add(this.flowLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(644, 148);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "태그 검색조건";
            // 
            // btn_DeleteTagList
            // 
            this.btn_DeleteTagList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_DeleteTagList.Location = new System.Drawing.Point(483, 117);
            this.btn_DeleteTagList.Name = "btn_DeleteTagList";
            this.btn_DeleteTagList.Size = new System.Drawing.Size(75, 25);
            this.btn_DeleteTagList.TabIndex = 18;
            this.btn_DeleteTagList.Text = "삭 제";
            this.btn_DeleteTagList.UseVisualStyleBackColor = true;
            this.btn_DeleteTagList.Visible = false;
            this.btn_DeleteTagList.Click += new System.EventHandler(this.btn_DeleteTagList_Click);
            // 
            // cb_yd
            // 
            this.cb_yd.AutoSize = true;
            this.cb_yd.Location = new System.Drawing.Point(240, 122);
            this.cb_yd.Name = "cb_yd";
            this.cb_yd.Size = new System.Drawing.Size(48, 16);
            this.cb_yd.TabIndex = 17;
            this.cb_yd.Text = "전일";
            this.cb_yd.UseVisualStyleBackColor = true;
            this.cb_yd.Visible = false;
            // 
            // cb_td
            // 
            this.cb_td.AutoSize = true;
            this.cb_td.Location = new System.Drawing.Point(186, 122);
            this.cb_td.Name = "cb_td";
            this.cb_td.Size = new System.Drawing.Size(48, 16);
            this.cb_td.TabIndex = 16;
            this.cb_td.Text = "금일";
            this.cb_td.UseVisualStyleBackColor = true;
            this.cb_td.Visible = false;
            // 
            // cb_hr
            // 
            this.cb_hr.AutoSize = true;
            this.cb_hr.Location = new System.Drawing.Point(132, 122);
            this.cb_hr.Name = "cb_hr";
            this.cb_hr.Size = new System.Drawing.Size(48, 16);
            this.cb_hr.TabIndex = 15;
            this.cb_hr.Text = "시간";
            this.cb_hr.UseVisualStyleBackColor = true;
            this.cb_hr.Visible = false;
            // 
            // btn_virtualTag_hour
            // 
            this.btn_virtualTag_hour.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_virtualTag_hour.Location = new System.Drawing.Point(7, 117);
            this.btn_virtualTag_hour.Name = "btn_virtualTag_hour";
            this.btn_virtualTag_hour.Size = new System.Drawing.Size(116, 25);
            this.btn_virtualTag_hour.TabIndex = 14;
            this.btn_virtualTag_hour.Text = "가상태그작성";
            this.btn_virtualTag_hour.UseVisualStyleBackColor = true;
            this.btn_virtualTag_hour.Visible = false;
            this.btn_virtualTag_hour.Click += new System.EventHandler(this.btn_virtualTag_Click);
            // 
            // btn_GetTagList
            // 
            this.btn_GetTagList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_GetTagList.Location = new System.Drawing.Point(566, 117);
            this.btn_GetTagList.Name = "btn_GetTagList";
            this.btn_GetTagList.Size = new System.Drawing.Size(75, 25);
            this.btn_GetTagList.TabIndex = 11;
            this.btn_GetTagList.Text = "검 색";
            this.btn_GetTagList.UseVisualStyleBackColor = true;
            this.btn_GetTagList.Click += new System.EventHandler(this.btn_GetTagList_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanel1.Controls.Add(this.panel4);
            this.flowLayoutPanel1.Controls.Add(this.panel6);
            this.flowLayoutPanel1.Controls.Add(this.panel10);
            this.flowLayoutPanel1.Controls.Add(this.panel11);
            this.flowLayoutPanel1.Controls.Add(this.panel15);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 17);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(638, 64);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.textBox_tagname);
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(309, 26);
            this.panel4.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "태그명";
            // 
            // textBox_tagname
            // 
            this.textBox_tagname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_tagname.Location = new System.Drawing.Point(60, 2);
            this.textBox_tagname.Name = "textBox_tagname";
            this.textBox_tagname.Size = new System.Drawing.Size(246, 21);
            this.textBox_tagname.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.textBox_description);
            this.panel6.Location = new System.Drawing.Point(318, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(315, 26);
            this.panel6.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "태그설명";
            // 
            // textBox_description
            // 
            this.textBox_description.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_description.Location = new System.Drawing.Point(62, 2);
            this.textBox_description.Name = "textBox_description";
            this.textBox_description.Size = new System.Drawing.Size(247, 21);
            this.textBox_description.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.comboBox_br_code);
            this.panel10.Controls.Add(this.label6);
            this.panel10.Location = new System.Drawing.Point(3, 35);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(186, 26);
            this.panel10.TabIndex = 7;
            // 
            // comboBox_br_code
            // 
            this.comboBox_br_code.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_br_code.FormattingEnabled = true;
            this.comboBox_br_code.Location = new System.Drawing.Point(60, 4);
            this.comboBox_br_code.Name = "comboBox_br_code";
            this.comboBox_br_code.Size = new System.Drawing.Size(123, 20);
            this.comboBox_br_code.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 1;
            this.label6.Text = "변량기호";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.comboBox_fn_code);
            this.panel11.Controls.Add(this.label7);
            this.panel11.Location = new System.Drawing.Point(195, 35);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(139, 26);
            this.panel11.TabIndex = 8;
            // 
            // comboBox_fn_code
            // 
            this.comboBox_fn_code.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_fn_code.FormattingEnabled = true;
            this.comboBox_fn_code.Location = new System.Drawing.Point(61, 4);
            this.comboBox_fn_code.Name = "comboBox_fn_code";
            this.comboBox_fn_code.Size = new System.Drawing.Size(75, 20);
            this.comboBox_fn_code.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 1;
            this.label7.Text = "기능기호";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.comboBox_use_yn);
            this.panel15.Controls.Add(this.label11);
            this.panel15.Location = new System.Drawing.Point(340, 35);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(129, 26);
            this.panel15.TabIndex = 12;
            // 
            // comboBox_use_yn
            // 
            this.comboBox_use_yn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_use_yn.FormattingEnabled = true;
            this.comboBox_use_yn.Location = new System.Drawing.Point(61, 4);
            this.comboBox_use_yn.Name = "comboBox_use_yn";
            this.comboBox_use_yn.Size = new System.Drawing.Size(57, 20);
            this.comboBox_use_yn.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 1;
            this.label11.Text = "사용여부";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.comboBox_virtualTag);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Location = new System.Drawing.Point(7, 87);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(158, 26);
            this.panel5.TabIndex = 13;
            this.panel5.Visible = false;
            // 
            // comboBox_virtualTag
            // 
            this.comboBox_virtualTag.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_virtualTag.FormattingEnabled = true;
            this.comboBox_virtualTag.Location = new System.Drawing.Point(61, 4);
            this.comboBox_virtualTag.Name = "comboBox_virtualTag";
            this.comboBox_virtualTag.Size = new System.Drawing.Size(89, 20);
            this.comboBox_virtualTag.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "가상태그";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btn_taggbnImport);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.btn_DeleteTagGbn);
            this.groupBox2.Controls.Add(this.btn_GetTagGbn);
            this.groupBox2.Controls.Add(this.flowLayoutPanel3);
            this.groupBox2.Location = new System.Drawing.Point(3, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(523, 148);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "태그구분 검색조건";
            // 
            // btn_taggbnImport
            // 
            this.btn_taggbnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_taggbnImport.Location = new System.Drawing.Point(262, 117);
            this.btn_taggbnImport.Name = "btn_taggbnImport";
            this.btn_taggbnImport.Size = new System.Drawing.Size(75, 25);
            this.btn_taggbnImport.TabIndex = 16;
            this.btn_taggbnImport.Text = "임포트";
            this.btn_taggbnImport.UseVisualStyleBackColor = true;
            this.btn_taggbnImport.Click += new System.EventHandler(this.btn_taggbnImport_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox_tag_gbn_import);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Location = new System.Drawing.Point(6, 116);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 26);
            this.panel1.TabIndex = 15;
            // 
            // comboBox_tag_gbn_import
            // 
            this.comboBox_tag_gbn_import.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_tag_gbn_import.FormattingEnabled = true;
            this.comboBox_tag_gbn_import.Location = new System.Drawing.Point(56, 4);
            this.comboBox_tag_gbn_import.Name = "comboBox_tag_gbn_import";
            this.comboBox_tag_gbn_import.Size = new System.Drawing.Size(188, 20);
            this.comboBox_tag_gbn_import.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 1;
            this.label9.Text = "태그구분";
            // 
            // btn_DeleteTagGbn
            // 
            this.btn_DeleteTagGbn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_DeleteTagGbn.Location = new System.Drawing.Point(361, 117);
            this.btn_DeleteTagGbn.Name = "btn_DeleteTagGbn";
            this.btn_DeleteTagGbn.Size = new System.Drawing.Size(75, 25);
            this.btn_DeleteTagGbn.TabIndex = 14;
            this.btn_DeleteTagGbn.Text = "삭 제";
            this.btn_DeleteTagGbn.UseVisualStyleBackColor = true;
            this.btn_DeleteTagGbn.Click += new System.EventHandler(this.btn_DeleteTagGbn_Click);
            // 
            // btn_GetTagGbn
            // 
            this.btn_GetTagGbn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_GetTagGbn.Location = new System.Drawing.Point(442, 117);
            this.btn_GetTagGbn.Name = "btn_GetTagGbn";
            this.btn_GetTagGbn.Size = new System.Drawing.Size(75, 25);
            this.btn_GetTagGbn.TabIndex = 13;
            this.btn_GetTagGbn.Text = "검 색";
            this.btn_GetTagGbn.UseVisualStyleBackColor = true;
            this.btn_GetTagGbn.Click += new System.EventHandler(this.btn_GetTagGbn_Click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.AutoSize = true;
            this.flowLayoutPanel3.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanel3.Controls.Add(this.panel2);
            this.flowLayoutPanel3.Controls.Add(this.panel7);
            this.flowLayoutPanel3.Controls.Add(this.panel3);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 17);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(517, 64);
            this.flowLayoutPanel3.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.textBox_tagname_gbn);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 26);
            this.panel2.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "태그명";
            // 
            // textBox_tagname_gbn
            // 
            this.textBox_tagname_gbn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_tagname_gbn.Location = new System.Drawing.Point(54, 2);
            this.textBox_tagname_gbn.Name = "textBox_tagname_gbn";
            this.textBox_tagname_gbn.Size = new System.Drawing.Size(190, 21);
            this.textBox_tagname_gbn.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.textBox_description_gbn);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Location = new System.Drawing.Point(259, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(249, 26);
            this.panel7.TabIndex = 13;
            // 
            // textBox_description_gbn
            // 
            this.textBox_description_gbn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_description_gbn.Location = new System.Drawing.Point(55, 2);
            this.textBox_description_gbn.Name = "textBox_description_gbn";
            this.textBox_description_gbn.Size = new System.Drawing.Size(189, 21);
            this.textBox_description_gbn.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "태그설명";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.comboBox_tag_gbn);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Location = new System.Drawing.Point(3, 35);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(250, 26);
            this.panel3.TabIndex = 12;
            // 
            // comboBox_tag_gbn
            // 
            this.comboBox_tag_gbn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_tag_gbn.FormattingEnabled = true;
            this.comboBox_tag_gbn.Location = new System.Drawing.Point(56, 4);
            this.comboBox_tag_gbn.Name = "comboBox_tag_gbn";
            this.comboBox_tag_gbn.Size = new System.Drawing.Size(188, 20);
            this.comboBox_tag_gbn.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "태그구분";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.groupBox3);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox4);
            this.splitContainer3.Size = new System.Drawing.Size(1192, 458);
            this.splitContainer3.SplitterDistance = 659;
            this.splitContainer3.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.ultraGrid_TagList);
            this.groupBox3.Location = new System.Drawing.Point(12, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(644, 443);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "태그 리스트";
            // 
            // ultraGrid_TagList
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_TagList.DisplayLayout.Appearance = appearance4;
            this.ultraGrid_TagList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_TagList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagList.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_TagList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.ultraGrid_TagList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_TagList.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.ultraGrid_TagList.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_TagList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_TagList.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_TagList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.ultraGrid_TagList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_TagList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagList.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_TagList.DisplayLayout.Override.CellAppearance = appearance5;
            this.ultraGrid_TagList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_TagList.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagList.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.ultraGrid_TagList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.ultraGrid_TagList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_TagList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_TagList.DisplayLayout.Override.RowAppearance = appearance10;
            this.ultraGrid_TagList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_TagList.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.ultraGrid_TagList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_TagList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_TagList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_TagList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_TagList.Location = new System.Drawing.Point(3, 17);
            this.ultraGrid_TagList.Name = "ultraGrid_TagList";
            this.ultraGrid_TagList.Size = new System.Drawing.Size(638, 423);
            this.ultraGrid_TagList.TabIndex = 0;
            this.ultraGrid_TagList.Text = "ultraGrid1";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.ultraGrid_TagGbn);
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(514, 443);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "태그구분 리스트";
            // 
            // ultraGrid_TagGbn
            // 
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_TagGbn.DisplayLayout.Appearance = appearance16;
            this.ultraGrid_TagGbn.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_TagGbn.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagGbn.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_TagGbn.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.ultraGrid_TagGbn.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_TagGbn.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.ultraGrid_TagGbn.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_TagGbn.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_TagGbn.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_TagGbn.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.ultraGrid_TagGbn.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_TagGbn.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagGbn.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_TagGbn.DisplayLayout.Override.CellAppearance = appearance17;
            this.ultraGrid_TagGbn.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_TagGbn.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_TagGbn.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance23.TextHAlignAsString = "Left";
            this.ultraGrid_TagGbn.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.ultraGrid_TagGbn.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_TagGbn.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_TagGbn.DisplayLayout.Override.RowAppearance = appearance22;
            this.ultraGrid_TagGbn.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_TagGbn.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.ultraGrid_TagGbn.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_TagGbn.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_TagGbn.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_TagGbn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_TagGbn.Location = new System.Drawing.Point(3, 17);
            this.ultraGrid_TagGbn.Name = "ultraGrid_TagGbn";
            this.ultraGrid_TagGbn.Size = new System.Drawing.Size(508, 423);
            this.ultraGrid_TagGbn.TabIndex = 0;
            this.ultraGrid_TagGbn.Text = "ultraGrid2";
            // 
            // frmTagDivision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 625);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmTagDivision";
            this.Text = "Tag구분";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_TagList)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_TagGbn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_tagname;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_description;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.ComboBox comboBox_br_code;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.ComboBox comboBox_fn_code;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.ComboBox comboBox_use_yn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_virtualTag_hour;
        private System.Windows.Forms.Button btn_GetTagList;
        private System.Windows.Forms.Button btn_GetTagGbn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_tagname_gbn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox comboBox_tag_gbn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_TagList;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_TagGbn;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox comboBox_virtualTag;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_description_gbn;
        private System.Windows.Forms.Button btn_DeleteTagGbn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox_tag_gbn_import;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cb_yd;
        private System.Windows.Forms.CheckBox cb_td;
        private System.Windows.Forms.CheckBox cb_hr;
        private System.Windows.Forms.Button btn_taggbnImport;
        private System.Windows.Forms.Button btn_DeleteTagList;
    }
}