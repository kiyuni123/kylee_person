﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNETServer.Common.utils;
using WaterNETServer.dao;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using EMFrame.log;

namespace WaterNETServer.form
{
    public partial class frmTagImport : Form, IForminterface
    {
        #region 선언부
        GlobalVariable gVar = new GlobalVariable();

        public frmTagImport()
        {
            InitializeComponent();
            InitializeSetting();
        }

        OracleWork oWork = new OracleWork();
        //OleDBWork hWork = new OleDBWork();
        FormUtils fu = new FormUtils();

        //OleDBWork hWork = null;
        //FormUtils fu = null;

        DataTable historianDataTable;               // 히스토리안 태그 목록
        DataTable waternetDataTable;                // 워터넷 태그 목록
        DataTable copyDataTable;                    // 임포트용 데이터 테이블



        private bool firstActionGetHistorianTagList = true;
        private bool firstActionGetWaternetTagList = true;

        #endregion

        #region IForminterface 멤버
        string IForminterface.FormID
        {
            get { return this.Text; }
        }
        string IForminterface.FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion

        #region 초기화
        private void InitializeHistorianGrid()
        {
            // 히스토리안 컬럼 셋팅
            UltraGridColumn historianColumn;
            historianColumn = ultraGrid_HistorianTagList.DisplayLayout.Bands[0].Columns.Add();
            historianColumn.CellActivation = Activation.AllowEdit;
            historianColumn.CellClickAction = CellClickAction.Edit;
            historianColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            historianColumn.Key = "IDX";
            historianColumn.Header.Caption = "";
            historianColumn.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            historianColumn.CellAppearance.TextHAlign = HAlign.Center;
            historianColumn.CellAppearance.TextVAlign = VAlign.Middle;
            historianColumn.Width = 50;
            historianColumn.Hidden = false;

            historianColumn = ultraGrid_HistorianTagList.DisplayLayout.Bands[0].Columns.Add();
            historianColumn.Key = "tagname";
            historianColumn.Header.Caption = "태그명";
            historianColumn.CellActivation = Activation.NoEdit;
            historianColumn.CellClickAction = CellClickAction.RowSelect;
            historianColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            historianColumn.CellAppearance.TextHAlign = HAlign.Left;
            historianColumn.CellAppearance.TextVAlign = VAlign.Middle;
            historianColumn.Width = 200;
            historianColumn.Hidden = false;

            historianColumn = ultraGrid_HistorianTagList.DisplayLayout.Bands[0].Columns.Add();
            historianColumn.Key = "description";
            historianColumn.Header.Caption = "태그설명";
            historianColumn.CellActivation = Activation.NoEdit;
            historianColumn.CellClickAction = CellClickAction.RowSelect;
            historianColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            historianColumn.CellAppearance.TextHAlign = HAlign.Left;
            historianColumn.CellAppearance.TextVAlign = VAlign.Middle;
            historianColumn.Width = 200;
            historianColumn.Hidden = false;
        }
        private void InitializeWaternetGrid()
        {
            // 워터넷 컬럼 셋팅
            UltraGridColumn waternetColumn;
            waternetColumn = ultraGrid_WaternetTagList.DisplayLayout.Bands[0].Columns.Add();
            waternetColumn.CellActivation = Activation.AllowEdit;
            waternetColumn.CellClickAction = CellClickAction.Edit;
            waternetColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            waternetColumn.Key = "IDX";
            waternetColumn.Header.Caption = "";
            waternetColumn.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            waternetColumn.CellAppearance.TextHAlign = HAlign.Center;
            waternetColumn.CellAppearance.TextVAlign = VAlign.Middle;
            waternetColumn.Width = 50;
            waternetColumn.Hidden = false;

            waternetColumn = ultraGrid_WaternetTagList.DisplayLayout.Bands[0].Columns.Add();
            waternetColumn.Key = "TAGNAME";
            waternetColumn.Header.Caption = "태그명";
            waternetColumn.CellActivation = Activation.NoEdit;
            waternetColumn.CellClickAction = CellClickAction.RowSelect;
            waternetColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            waternetColumn.CellAppearance.TextHAlign = HAlign.Left;
            waternetColumn.CellAppearance.TextVAlign = VAlign.Middle;
            waternetColumn.Width = 200;
            waternetColumn.Hidden = false;

            waternetColumn = ultraGrid_WaternetTagList.DisplayLayout.Bands[0].Columns.Add();
            waternetColumn.Key = "DESCRIPTION";
            waternetColumn.Header.Caption = "태그설명";
            waternetColumn.CellActivation = Activation.NoEdit;
            waternetColumn.CellClickAction = CellClickAction.RowSelect;
            waternetColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            waternetColumn.CellAppearance.TextHAlign = HAlign.Left;
            waternetColumn.CellAppearance.TextVAlign = VAlign.Middle;
            waternetColumn.Width = 200;
            waternetColumn.Hidden = false;
        }
        private void InitializeSetting()
        {
            InitializeHistorianGrid();
            InitializeWaternetGrid();
            fu.SetGridStyle_TagImport(ultraGrid_HistorianTagList);
            fu.SetGridStyle_TagImport(ultraGrid_WaternetTagList);
        }
        #endregion

        #region 버튼엑션
        /// <summary>
        /// 히스토리안 태그목록
        /// </summary>
        private void GetHistorianTagListBtn(object sender, EventArgs e)
        {
            GetHistorianTagList();
        }
        /// <summary>
        /// 히스토리안 태그목록을 우측으로(WaterNET) 보냄
        /// </summary>
        private void ImportTagListBtn(object sender, EventArgs e)
        {
            ImportTabList();
        }
        /// <summary>
        /// 임포트된 태그목록을 WaterNET에 저장함
        /// </summary>
        private void CommitTagListBtn(object sender, EventArgs e)
        {
            CommitTagList();
        }
        /// <summary>
        /// 선택된 WaterNET 태그목록을 삭제함
        /// </summary>
        private void DeleteSelectedTagBtn(object sender, EventArgs e)
        {
            DeleteSelectedTag();
        }
        /// <summary>
        /// WaterNET 태그목록
        /// </summary>
        private void GetWaternetTagListBtn(object sender, EventArgs e)
        {
            GetWaternetTagList();
        }
        #endregion

        #region 기능함수
        /// <summary>
        /// 히스토리안 테이블 조회
        /// </summary>
        private void GetHistorianTagList()
        {
            Hashtable cond = new Hashtable();
            cond.Add("TAGNAME", textBox1.Text);
            cond.Add("DESCRIPTION", textBox2.Text);

            string tableName = "HistorianTagList";

            if (historianDataTable != null)
            {
                historianDataTable = null;
                ultraGrid_HistorianTagList.DataSource = null;
                InitializeHistorianGrid();
            }
            historianDataTable = oWork.GetRWISTagList(tableName, cond).Tables[0];

            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.Int16");
            col.ColumnName = "IDX";
            col.DefaultValue = 0;
            historianDataTable.Columns.Add(col);

            ultraGrid_HistorianTagList.DataSource = historianDataTable;

            // 컬럼 사이즈 변경
            fu.SetGridStyle_PerformAutoResize(ultraGrid_HistorianTagList);

            if (ultraGrid_WaternetTagList.Rows.Count > 0)
            {
                SetHistorianGridStatus();
            }
            if (firstActionGetHistorianTagList)
            {
                firstActionGetHistorianTagList = false;
                btn_GetHistorianTagList.Text = "RWIS 태그 재조회";
            }
        }

        /// <summary>
        /// 워터넷 테이블 조회
        /// </summary>
        private void GetWaternetTagList()
        {
            Hashtable cond = new Hashtable();

            string tableName = "WaternetTabList";
            if (waternetDataTable != null)
            {
                waternetDataTable = null;
                ultraGrid_WaternetTagList.DataSource = null;
                InitializeWaternetGrid();
            }
            waternetDataTable = oWork.GetWaternetTagList(tableName, cond).Tables[0];

            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.Int16");
            col.ColumnName = "IDX";
            col.AutoIncrement = false;
            col.DefaultValue = 0;
            waternetDataTable.Columns.Add(col);

            ultraGrid_WaternetTagList.DataSource = waternetDataTable;

            fu.SetGridStyle_PerformAutoResize(ultraGrid_WaternetTagList);

            if (ultraGrid_HistorianTagList.Rows.Count > 0)
            {
                SetHistorianGridStatus();
            }
            if (firstActionGetWaternetTagList)
            {
                firstActionGetWaternetTagList = false;
                btn_GetWaternetTagList.Text = "워터넷태그 재조회";
            }
        }
        private void SetHistorianGridStatus()
        {
            foreach (UltraGridRow wRow in ultraGrid_WaternetTagList.Rows)
            {
                foreach (UltraGridRow hRow in ultraGrid_HistorianTagList.Rows)
                {
                    if (wRow.Cells[0].Text.Equals(hRow.Cells[0].Text))
                    {
                        hRow.Activation = Activation.Disabled;
                        hRow.Cells[2].Value = "1";
                    }
                    else
                    {
                        //hRow.Activation = Activation.NoEdit;
                        hRow.Cells[2].Activation = Activation.AllowEdit;
                        hRow.Cells[2].Value = "0";
                    }
                }
            }
        }

        /// <summary>
        /// 복제용 데이터테이블 초기화
        /// </summary>
        private void SetCopyDataTable()
        {
            // 임포트용 데이터 테이블 컬럼 셋팅
            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "TAGNAME";
            col.AutoIncrement = false;
            col.DefaultValue = 0;
            copyDataTable.Columns.Add(col);

            DataColumn col2 = new DataColumn();
            col2.DataType = System.Type.GetType("System.String");
            col2.ColumnName = "DESCRIPTION";
            col2.AutoIncrement = false;
            col2.DefaultValue = 0;
            copyDataTable.Columns.Add(col2);

            // 임포트용 데이터 테이블 프라이머리 키 셋팅
            // copyDataTable.Rows.Contains를 사용하려면 프라이머리키가 셋팅되어 있어야 함
            copyDataTable.PrimaryKey = new DataColumn[] { col };
        }

        /// <summary>
        /// 히스토리안쪽 그리드에서 선택된 내용을 워터넷 그리드로 이동시킴
        /// </summary>
        private void ImportTabList()
        {
            int cnt = 0;
            try
            {
                copyDataTable = new DataTable();
                SetCopyDataTable();

                foreach (UltraGridRow row in ultraGrid_HistorianTagList.Rows)
                {
                    // Disabled가 아니면 (즉 워터넷에 없는 컬럼만)
                    if (row.Activation != Activation.Disabled)
                    {
                        // 체크된 것만
                        if ("1".Equals(row.Cells[2].Text))
                        {
                            // 데이터 테이블에 추가 되지 않은것만
                            if (copyDataTable.Rows.Contains(row.Cells[0].Text))
                            {
                                Console.WriteLine(" 이미 키가 있음 패스 " + row.Cells[0].Text);
                            }
                            else
                            {
                                DataRow dr = copyDataTable.NewRow();

                                dr["TAGNAME"] = row.Cells[0].Text;
                                dr["DESCRIPTION"] = row.Cells[1].Text;

                                copyDataTable.Rows.Add(dr);

                                cnt++;
                            }
                        }
                    }
                }

                if (cnt > 0)
                {
                    if (waternetDataTable == null)
                    {
                        // 워터넷 테이블을 조회하지 않은 상태
                        StringBuilder msg = new StringBuilder();
                        msg.AppendLine("워터넷 데이블을 조회하지 않았습니다");
                        msg.AppendLine("이 작업은 심각한 오류를 발생시킬 수 있습니다");
                        msg.AppendLine("진행하시겠습니까?");

                        if (DialogResult.Yes == MessageBox.Show(msg.ToString(), "경고", MessageBoxButtons.YesNo))
                        {
                            // 워터넷 그리드에 반영시킴
                            ultraGrid_WaternetTagList.DataSource = copyDataTable;

                            MessageBox.Show(cnt + "건의 레코드가 추가 되었습니다", "정보");
                        }

                    }
                    else
                    {
                        // 추가한 데이터 테이블과 워터넷 데이터 테이블과 Merge
                        copyDataTable.Merge(waternetDataTable);

                        // 워터넷 그리드에 반영시킴
                        ultraGrid_WaternetTagList.DataSource = copyDataTable;

                        MessageBox.Show(cnt + "건의 레코드가 추가 되었습니다", "정보");
                    }
                }
                else
                {
                    MessageBox.Show("선택된 레코드가 없습니다", "정보");
                }
            }
            catch (System.ArgumentException ae)
            {
                Logger.Error(ae.ToString());
            }
        }

        /// <summary>
        /// 임포트된 태그 리스트를 워터넷 DB에 반영 시키고 양쪽 그리드를 적용함
        /// </summary>
        private void CommitTagList()
        {
            try
            {
                if (ultraGrid_WaternetTagList.DataSource == null)
                {
                    // 워터쪽 그리드에 값이 하나도 없을때
                    MessageBox.Show("워터넷 레코드가 없습니가 확인하시고 재시도하십시오", "경고");
                }
                else
                {
                    // 테이터가 추가됐을때는 추가된 그리드만 추가
                    DataTable updatedGrid = ((DataTable)ultraGrid_WaternetTagList.DataSource).GetChanges(DataRowState.Added);

                    if (updatedGrid == null)
                    {
                        MessageBox.Show("추가된 레코드가 없습니다.", "정보");
                    }
                    else
                    {
                        StringBuilder msg = new StringBuilder();
                        msg.AppendLine("히스토리안의 태그 목록을 워터넷에 추가하시겠습니까");
                        msg.AppendLine("이 작업은 되돌릴 수 없습니다.");

                        if (DialogResult.Yes == MessageBox.Show(msg.ToString(), "경고", MessageBoxButtons.YesNo))
                        {
                            int commitCount = oWork.CommitTagList(updatedGrid);
                            if(commitCount > 0)
                            {
                                GetWaternetTagList();
                                SetHistorianGridStatus();

                                MessageBox.Show(commitCount + "건의 태그가 입력되었습니다.", "정보");
                            }
                        }
                    }
                }
            }
            catch (NullReferenceException ne)
            {
                Logger.Error(ne.ToString());
            }
        }

        /// <summary>
        /// 선택된 워터넷 태그를 삭제한다
        /// </summary>
        private void DeleteSelectedTag()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TAGNAME", System.Type.GetType("System.String"));
            dt.Columns.Add("DESCRIPTION", System.Type.GetType("System.String"));

            int deleteCount = 0;

            foreach (UltraGridRow row in ultraGrid_WaternetTagList.Rows)
            {
                if("1".Equals(row.Cells[2].Text))
                {
                    DataRow dr = dt.NewRow();
                    dr["TAGNAME"] = row.Cells[0].Text;
                    dr["DESCRIPTION"] = row.Cells[1].Text;

                    dt.Rows.Add(dr);
                    deleteCount++;
                }
            }

            if(deleteCount > 0)
            {
                int deleteReturnCount = 0;

                // 워터넷 테이블을 조회하지 않은 상태
                StringBuilder msg = new StringBuilder();
                msg.AppendLine("워터넷 태그를 삭제합니다");
                msg.AppendLine("선택한 작업은 되돌릴 수 없습니다");
                msg.AppendLine("진행하시겠습니까?");

                if (DialogResult.Yes == MessageBox.Show(msg.ToString(), "경고", MessageBoxButtons.YesNo))
                {
                    deleteReturnCount = oWork.DeleteSelectedTag(dt);
                }

                if (deleteReturnCount > 0)
                {
                    GetWaternetTagList();
                    GetHistorianTagList();
                    MessageBox.Show(deleteReturnCount + "건의 태그를 삭제했습니다", "정보");
                }
            }
        }
        #endregion
    }
}
