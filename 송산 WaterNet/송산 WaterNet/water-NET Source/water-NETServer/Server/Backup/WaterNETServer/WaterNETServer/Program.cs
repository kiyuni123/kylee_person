﻿using System;
using System.Windows.Forms;

using System.Diagnostics;
using System.Runtime.InteropServices;
using WaterNETServer.Common.utils;
using System.Threading;
using WaterNETServer.formPopup;
using EMFrame.config;
using EMFrame.log;

namespace WaterNETServer
{
    static class Program
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void BringWindowToTop(IntPtr hWnd);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        //static void Main(string[] args)
        static void Main()
        {
            //오라클 환경변수 특성을 타서 환경변수 등록함.
            Environment.SetEnvironmentVariable("NLS_LANG", "KOREAN_KOREA.KO16MSWIN949");
            EConfig.INITIALIZE_LOG();
           
            #region 자동으로 업데이트
            try
            {
                GlobalVariable gVar = new GlobalVariable();
  
                // FTP를 이용해서 다운받도록 실행 옵션
                if (gVar.isDeployFtp())
                {
                    Application.Exit();
                    ProcessStartInfo pStartInfo = new ProcessStartInfo();
                    pStartInfo.FileName = Application.StartupPath + @"\Water-NET.Updater.exe";
                    pStartInfo.Arguments = string.Format(" S{0}", "");

                    Process Proc = new Process();

                    Proc.StartInfo = pStartInfo;

                    Proc.Start();
                    Proc.Dispose();

                    return;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                Logger.Error(e.ToString());
            }
            #endregion

            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmMain());
            }
            catch (ObjectDisposedException e)
            {
                MessageBox.Show(e.ToString());
                Logger.Error("DB정보 입력창에서 닫기 버튼을 눌렀을때 발생하는 오류");
                Logger.Error(e.ToString());
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(e.ToString());
                Logger.Error(e.ToString());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                Logger.Error("그 외 에러");
                Logger.Error(e.ToString());
            }
            finally
            {
                Application.Exit();
            }

            #region 프로그램 중복 실행 방지
            //try
            //{
            //    bool createdNew;
            //    Mutex mutex = new Mutex(true, "WaterNET.Server2", out createdNew);
            //    if (createdNew)
            //    {
            //        Application.EnableVisualStyles();
            //        Application.SetCompatibleTextRenderingDefault(false);
            //        Application.Run(new frmMain());
            //        mutex.ReleaseMutex();
            //    }
            //    else
            //    {
            //        MessageBox.Show("서버프로그램이 이미 실행중입니다.", "중복실행");

            //        IntPtr wHandle = FindWindow(null, "WaterNET.Server2");
            //        if (wHandle != IntPtr.Zero)
            //        {
            //            ShowWindow(wHandle, 1);
            //            BringWindowToTop(wHandle);
            //            SetForegroundWindow(wHandle);
            //        }
            //        Application.Exit();
            //    }
            //}
            //catch (ObjectDisposedException e)
            //{
            //    MessageBox.Show(e.ToString());
            //    Logger.Error("DB정보 입력창에서 닫기 버튼을 눌렀을때 발생하는 오류");
            //    Logger.Error(e.ToString());
            //}
            //catch (InvalidOperationException e)
            //{
            //    MessageBox.Show(e.ToString());
            //    Logger.Error(e.ToString());
            //}
            //catch (Exception e)
            //{
            //    MessageBox.Show(e.ToString());
            //    Logger.Error("그 외 에러");
            //    Logger.Error(e.ToString());
            //}
            //finally
            //{
            //    Application.Exit();
            //}

            #endregion
        }
    }
}
