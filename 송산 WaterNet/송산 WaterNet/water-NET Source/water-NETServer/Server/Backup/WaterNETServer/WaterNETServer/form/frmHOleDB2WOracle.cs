﻿using System;
using System.Collections;
using System.Threading;
using System.Windows.Forms;
using WaterNETServer.Common;
using WaterNETServer.Common.utils;
using IF_WaterINFOS.IF;
using WaterNETServer.IF_iwater.dao;
using WaterNETServer.ThreadManager.timer;
using WaterNETServer.formPopup;
using EMFrame.log;

namespace WaterNETServer.form
{
    public partial class frmHOleDB2WOracle : Form, IForminterface
    {
        #region 선언부
        private Hashtable _threadPool       = null;
        WaterNETServer.Common.utils.Common utils = new WaterNETServer.Common.utils.Common();
        GlobalVariable gVar                 = new GlobalVariable();

        private string _formName            = string.Empty;
        private static string _strSGCCD     = string.Empty;
        
        // IF_iwater의 Timer 클래스
        HistorianTimer hTimer               = null;

        // WaterNETMonitoring의 Timer 클래스
        WvTimer wvTimer                     = null;

        // IF_WaterINFOS 의 서비스 클래스
        GetInfosWebService infos            = null;

        // IF_WaterINFOS의 Timer 클래스
        WaterInfosTimer infosTimer          = null;

        // VolumeBatch Timer 클래스
        VolumeBatchTimer volumeTimer        = null;

        // IF_iwater의 oracle work 클래스
        IF_iwater.dao.OracleWork iwater     = new IF_iwater.dao.OracleWork();
        
        // 생성자
        public frmHOleDB2WOracle(Hashtable threadPool)
        {
            // 전역 풀
            _threadPool = threadPool;

            // 탭 이름
            _formName = gVar.getFormName(1);

            // 지역코드
            _strSGCCD = gVar.getSgccdCode();

            InitializeComponent();
        }
        
        /// <summary>
        /// 폼 로드시 실행할 이벤트
        /// </summary>
        private void frmHOleDB2WOracle_Load(object sender, EventArgs e)
        {
            // IF_iwater의 Timer 객체 생성
            hTimer = new HistorianTimer(_threadPool);

            // WaterNETMonitoring의 Timer 객체 생성
            wvTimer = new WvTimer(_threadPool);

            // IF_WaterINFOS의 서비스 객체 생성
            infos = new GetInfosWebService();

            // IF_Water_INFOS의 Timer 객체 생성
            infosTimer = new WaterInfosTimer(_threadPool);

            // VolumeBatch의 Timer 객체 생성
            volumeTimer = new VolumeBatchTimer(_threadPool);

            dateTimePicker2.Value = DateTime.Today.AddDays(-1);
            dateTimePicker4.Value = DateTime.Today.AddDays(-1);

            dateTimePicker9.Value = DateTime.Today.AddDays(-1);
            dateTimePicker10.Value = DateTime.Today.AddDays(-1);

            dateTimePicker7.Value = DateTime.Today.AddDays(-1);
            dateTimePicker8.Value = DateTime.Today.AddDays(-1);

            dateTimePicker1.Value = DateTime.Today.AddDays(-1);
            dateTimePicker6.Value = DateTime.Today.AddDays(-1);

            dateTimePicker2.Value = DateTime.Today.AddDays(-1);
            dateTimePicker3.Value = DateTime.Today.AddDays(-1);

            dateTimePicker4.Value = DateTime.Today.AddDays(-1);
            dateTimePicker5.Value = DateTime.Today.AddDays(-1);

            dateTimePicker12.Value = DateTime.Today.AddDays(-1);
            dateTimePicker11.Value = DateTime.Today.AddDays(-1);

            dateTimePicker14.Value = DateTime.Today.AddDays(-1);
            dateTimePicker13.Value = DateTime.Today.AddDays(-1);

            dateTimePicker16.Value = DateTime.Today.AddDays(-1);
            dateTimePicker15.Value = DateTime.Today.AddDays(-1);

            dateTimePicker18.Value = DateTime.Today.AddDays(-1);
            dateTimePicker17.Value = DateTime.Today.AddDays(-1);

            dateTimePicker20.Value = DateTime.Today.AddDays(-1);
            dateTimePicker19.Value = DateTime.Today.AddDays(-1);

            dateTimePicker22.Value = DateTime.Today.AddDays(-1);
            dateTimePicker21.Value = DateTime.Today.AddDays(-1);

            dateTimePicker24.Value = DateTime.Today.AddDays(-1);
            dateTimePicker23.Value = DateTime.Today.AddDays(-1);

            dateTimePicker26.Value = DateTime.Today.AddDays(-1);
            dateTimePicker25.Value = DateTime.Today.AddDays(-1);

            dateTimePicker28.Value = DateTime.Today.AddDays(-1);
            dateTimePicker27.Value = DateTime.Today.AddDays(-1);

            dateTimePicker30.Value = DateTime.Today.AddDays(-1);
            dateTimePicker29.Value = DateTime.Today.AddDays(-1);

            dateTimePicker32.Value = DateTime.Today.AddDays(-1);
            dateTimePicker31.Value = DateTime.Today.AddDays(-1);

            label_getTimeAutoModify();
        }

        #endregion

        #region IForminterface 멤버 ############################
        string IForminterface.FormID
        {
            get { return _formName; }
        }
        string IForminterface.FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion

        #region 버튼/프로그래스바 컨트롤 #######################
        /// <summary>
        /// 버튼 이미지 색 변경
        /// </summary>
        /// <param name="_button"></param>
        /// <param name="i"></param>
        private void SetButtonImage(Button _button, int i)
        {
            if (i == 1)
            {
                // 버튼 이미지 (녹색)
                _button.Image = Common.Properties.Resources.OkPopup;
            }
            if (i == 2)
            {
                // 버튼 이미지 (파랑)
                _button.Image = Common.Properties.Resources.YesPopup;
            }
            if (i == 3)
            {
                // 버튼 이미지 (빨강)
                _button.Image = Common.Properties.Resources.NoPopup;
            }
        }

        private void CheckRunningTimer(object sender, EventArgs e)
        {
            textBox1.Text = "";
            int i = 1;

            textBox1.Text = "실행중 작업 ("+DateTime.Now+")\r\n";
            foreach (DictionaryEntry de in _threadPool)
            {
                textBox1.Text += i++ + " : Key = "+de.Key+", Value = " + de.Value +"\r\n";
                //Console.WriteLine("Key = {0}, Value = {1}", de.Key, de.Value);
            }
            if (i == 1)
            {
                textBox1.Text += "실행중인 작업이 없습니다.";
            }
        }
        #endregion

        #region 일괄 수집 ######################################

        // 가짜 진행 상태 보여줌
        private void increaseProgressBar1(ProgressBar progressbar)
        {
            for (int i = 0; i <= 100; i++)
            {
                progressbar.Value = i;
                Thread.Sleep(1);
            }
        }

        // 단계 진행 상태 보여줌
        private void increaseProgressBar2(ProgressBar progressbar, int i)
        {
            progressbar.Value = i;
            progressbar.Refresh();
            Application.DoEvents();
        }

        // 일괄 시작/종료 상태 플래그
        private bool _batchStart = false;
        // 시작중/종료중/완료 여부 플래그
        private string _ing = "COMPLETE";   // COMPLETE, STARTING, STOPING
        /// <summary>
        /// 일괄 시작
        /// </summary>
        private void BatchStart(object sender, EventArgs e)
        {
            if ("STARTING".Equals(_ing))
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 시작중인 일괄 프로세스가 있습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                _ing = "STARTING";
                return;
            }
            if ("STOPING".Equals(_ing))
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 종료중인 일괄 프로세스가 있습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                _ing = "STOPING";
                return;
            }
            try
            {
                progressBar3.Maximum = 100;
                progressBar3.Step = 1;

                progressBar4.Maximum = 17;
                progressBar4.Step = 1;

                int i = 1;
                if (!_batchStart)
                {
                    _ing = "STARTING";
                    progressBar3.Value = 0;
                    progressBar4.Value = 0;

                    // 실시간 수집
                    if (!bool_GatherRealtime)
                    {
                        hTimer.setButton1(this.button11);
                        SetButtonImage(this.button11, 2);
                        // 실시간 수집
                        hTimer.GatherRealtime_ThreadStart();
                        // 데이터 보정
                        hTimer.RecordPasttime_ThreadStart();
                        // 실시간 경고
                        hTimer.RealtimeWarning_ThreadStart();
                        bool_GatherRealtime = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 금일 적산데이터 수집
                    if (!bool_Accumulation_ToDay)
                    {
                        hTimer.setButton4(this.button10);
                        SetButtonImage(this.button10, 2);
                        hTimer.Accumulation_ToDay_ThreadStart();
                        bool_Accumulation_ToDay = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 1시간 적산데이터 수집
                    if (!bool_Accumulation_Hour)
                    {
                        hTimer.setButton2(this.button15);
                        SetButtonImage(this.button15, 2);
                        hTimer.Accumulation_Hour_ThreadStart();
                        bool_Accumulation_Hour = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 1일 적산데이터 수집
                    if (!bool_Accumulation_Day)
                    {
                        hTimer.setButton3(this.button7);
                        SetButtonImage(this.button7, 2);
                        hTimer.Accumulation_Day_ThreadStart();
                        bool_Accumulation_Day = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 일별 야간최소유량
                    if (!bool_MinimumNightFlow)
                    {
                        wvTimer.setButton1(this.button5);
                        SetButtonImage(this.button5, 2);
                        wvTimer.MinimumNightFlow_ThreadStart();
                        bool_MinimumNightFlow = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 1시간 정시 수압
                    if (!bool_AveragePressure)
                    {
                        wvTimer.setButton2(this.button6);
                        SetButtonImage(this.button6, 2);
                        wvTimer.AveragePressure_ThreadStart();
                        bool_AveragePressure = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 누수감시
                    if (!bool_MonitoringLeakage)
                    {
                        wvTimer.setButton3(this.button2);
                        SetButtonImage(this.button2, 2);
                        wvTimer.MonitoringLeakage_ThreadStart();
                        bool_MonitoringLeakage = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 수용가정보
                    if (!bool_DMINFO)
                    {
                        infosTimer.setButton1(this.button52);
                        SetButtonImage(this.button52, 2);
                        infosTimer.DMINFO_ThreadStart();
                        bool_DMINFO = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 상하수도자원
                    if (!bool_DMWSRSRC)
                    {
                        infosTimer.setButton2(this.button61);
                        SetButtonImage(this.button61, 2);
                        infosTimer.DMWSRSRC_ThreadStart();
                        bool_DMWSRSRC = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 민원정보
                    if (!bool_CAINFO)
                    {
                        infosTimer.setButton3(this.button55);
                        SetButtonImage(this.button55, 2);
                        infosTimer.CAINFO_ThreadStart();
                        bool_CAINFO = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 계량기교체
                    if (!bool_MTRCHGINFO)
                    {
                        infosTimer.setButton4(this.button53);
                        SetButtonImage(this.button53, 2);
                        infosTimer.MTRCHGINFO_ThreadStart();
                        bool_MTRCHGINFO = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 요금정보
                    if (!bool_STWCHRG)
                    {
                        infosTimer.setButton5(this.button54);
                        SetButtonImage(this.button54, 2);
                        infosTimer.STWCHRG_ThreadStart();
                        bool_STWCHRG = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 수용가기본
                    if (!bool_DM_Info)
                    {
                        SetButtonImage(this.button56, 2);
                        volumeTimer.setButton1(this.button56);
                        volumeTimer.DM_Info_ThreadStart();
                        bool_DM_Info = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 사용량정보
                    if (!bool_DM_Used)
                    {
                        SetButtonImage(this.button57, 2);
                        volumeTimer.setButton2(this.button57);
                        volumeTimer.DM_Used_ThreadStart();
                        bool_DM_Used = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 관망정보
                    if (!bool_Pipe_Info)
                    {
                        SetButtonImage(this.button58, 2);
                        volumeTimer.setButton3(this.button58);
                        volumeTimer.Pipe_Info_ThreadStart();
                        bool_Pipe_Info = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 유수율분석
                    if (!bool_RevenueRatio_Analysis)
                    {
                        SetButtonImage(this.button59, 2);
                        volumeTimer.setButton4(this.button59);
                        volumeTimer.RevenueRatio_Analysis_ThreadStart();
                        bool_RevenueRatio_Analysis = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 수리해석결과
                    if (!bool_WHHpress_Result)
                    {
                        SetButtonImage(this.button60, 2);
                        volumeTimer.setButton5(this.button60);
                        volumeTimer.WHHpress_Result_ThreadStart();
                        bool_WHHpress_Result = true;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    _batchStart = true;
                    progressBar3.Value = 0;
                    progressBar4.Value = 0;
                    SetButtonImage(this.button19, 2);

                    _ing = "COMPLETE";
                }
                else
                {
                    frmShowMessageBox msgBox = new frmShowMessageBox("일괄 시작된 프로세스가 실행중입니다.");
                    msgBox.StartPosition = FormStartPosition.CenterScreen;
                    msgBox.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }

        /// <summary>
        /// 일괄 종료
        /// </summary>
        private void BatchEnd(object sender, EventArgs e)
        {
            if ("STARTING".Equals(_ing))
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 시작중인 일괄 프로세스가 있습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                _ing = "STARTING";
                return;
            }
            if ("STOPING".Equals(_ing))
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 종료중인 일괄 프로세스가 있습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                _ing = "STOPING";
                return;
            }
            try
            {
                progressBar3.Maximum = 100;
                progressBar3.Step = 1;

                progressBar4.Maximum = 17;
                progressBar4.Step = 1;

                int i = 1;
                if (_batchStart)
                {
                    _ing = "STOPING";
                    progressBar3.Value = 0;
                    progressBar4.Value = 0;

                    // 실시간 수집 
                    if (bool_GatherRealtime)
                    {
                        // 실시간 수집
                        utils.TimerStop(hTimer.GatherRealtime_Pool());
                        this._threadPool.Remove(hTimer.GatherRealtime_Pool().GetHashCode());

                        // 데이터 보정
                        utils.TimerStop(hTimer.RecordPasttime_Pool());
                        this._threadPool.Remove(hTimer.RecordPasttime_Pool().GetHashCode());

                        // 실시간 경고
                        utils.TimerStop(hTimer.RealtimeWarning_Pool());
                        this._threadPool.Remove(hTimer.RealtimeWarning_Pool().GetHashCode());

                        SetButtonImage(this.button11, 3);
                        bool_GatherRealtime = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 금일 적산데이터 수집
                    if (bool_Accumulation_ToDay)
                    {
                        utils.TimerStop(hTimer.Accumulation_ToDay_Pool());
                        this._threadPool.Remove(hTimer.Accumulation_ToDay_Pool().GetHashCode());
                        SetButtonImage(this.button10, 3);
                        bool_Accumulation_ToDay = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 1시간 적산데이터 수집
                    if (bool_Accumulation_Hour)
                    {
                        utils.TimerStop(hTimer.Accumulation_Hour_Pool());
                        this._threadPool.Remove(hTimer.Accumulation_Hour_Pool().GetHashCode());
                        SetButtonImage(this.button15, 3);
                        bool_Accumulation_Hour = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 1일 적산데이터 수집
                    if (bool_Accumulation_Day)
                    {
                        utils.TimerStop(hTimer.Accumulation_Day_Pool());
                        this._threadPool.Remove(hTimer.Accumulation_Day_Pool().GetHashCode());
                        SetButtonImage(this.button7, 3);
                        bool_Accumulation_Day = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 일별 야간최소유량
                    if (bool_MinimumNightFlow)
                    {
                        utils.TimerStop(wvTimer.MinimumNightFlow_Pool());
                        this._threadPool.Remove(wvTimer.MinimumNightFlow_Pool().GetHashCode());
                        SetButtonImage(this.button5, 3);
                        bool_MinimumNightFlow = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 1시간 정시 수압
                    if (bool_AveragePressure)
                    {
                        utils.TimerStop(wvTimer.AveragePressure_Pool());
                        this._threadPool.Remove(wvTimer.AveragePressure_Pool().GetHashCode());
                        SetButtonImage(this.button6, 3);
                        bool_AveragePressure = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 누수감시
                    if (bool_MonitoringLeakage)
                    {
                        utils.TimerStop(wvTimer.MonitoringLeakage_Pool());
                        this._threadPool.Remove(wvTimer.MonitoringLeakage_Pool().GetHashCode());
                        SetButtonImage(this.button2, 3);
                        bool_MonitoringLeakage = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 수용가정보
                    if (bool_DMINFO)
                    {
                        utils.TimerStop(infosTimer.DMINFO_Pool());
                        this._threadPool.Remove(infosTimer.DMINFO_Pool().GetHashCode());
                        SetButtonImage(this.button52, 3);
                        bool_DMINFO = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 상하수도자원
                    if (bool_DMWSRSRC)
                    {
                        utils.TimerStop(infosTimer.DMWSRSRC_Pool());
                        this._threadPool.Remove(infosTimer.DMWSRSRC_Pool().GetHashCode());
                        SetButtonImage(this.button61, 3);
                        bool_DMWSRSRC = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 민원정보
                    if (bool_CAINFO)
                    {
                        utils.TimerStop(infosTimer.CAINFO_Pool());
                        this._threadPool.Remove(infosTimer.CAINFO_Pool().GetHashCode());
                        SetButtonImage(this.button55, 3);
                        bool_CAINFO = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 계량기교체
                    if (bool_MTRCHGINFO)
                    {
                        utils.TimerStop(infosTimer.MTRCHGINFO_Pool());
                        this._threadPool.Remove(infosTimer.MTRCHGINFO_Pool().GetHashCode());
                        SetButtonImage(this.button53, 3);
                        bool_MTRCHGINFO = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 요금정보
                    if (bool_STWCHRG)
                    {
                        utils.TimerStop(infosTimer.STWCHRG_Pool());
                        this._threadPool.Remove(infosTimer.STWCHRG_Pool().GetHashCode());
                        SetButtonImage(this.button54, 3);
                        bool_STWCHRG = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 수용가기본
                    if (bool_DM_Info)
                    {
                        utils.TimerStop(volumeTimer.DM_Info_Pool());
                        this._threadPool.Remove(volumeTimer.DM_Info_Pool().GetHashCode());
                        SetButtonImage(this.button56, 3);
                        bool_DM_Info = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 사용량정보
                    if (bool_DM_Used)
                    {
                        utils.TimerStop(volumeTimer.DM_Used_Pool());
                        this._threadPool.Remove(volumeTimer.DM_Used_Pool().GetHashCode());
                        SetButtonImage(this.button57, 3);
                        bool_DM_Used = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 관망정보
                    if (bool_Pipe_Info)
                    {
                        utils.TimerStop(volumeTimer.Pipe_Info_Pool());
                        this._threadPool.Remove(volumeTimer.Pipe_Info_Pool().GetHashCode());
                        SetButtonImage(this.button58, 3);
                        bool_Pipe_Info = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 유수율분석
                    if (bool_RevenueRatio_Analysis)
                    {
                        utils.TimerStop(volumeTimer.RevenueRatio_Analysis_Pool());
                        this._threadPool.Remove(volumeTimer.RevenueRatio_Analysis_Pool().GetHashCode());
                        SetButtonImage(this.button59, 3);
                        bool_RevenueRatio_Analysis = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    // 수리해석결과
                    if (bool_WHHpress_Result)
                    {
                        utils.TimerStop(volumeTimer.WHHpress_Result_Pool());
                        this._threadPool.Remove(volumeTimer.WHHpress_Result_Pool().GetHashCode());
                        SetButtonImage(this.button60, 3);
                        bool_WHHpress_Result = false;
                    }
                    increaseProgressBar1(progressBar3);
                    increaseProgressBar2(progressBar4, i); i++;

                    _batchStart = false;
                    progressBar3.Value = 0;
                    progressBar4.Value = 0;
                    SetButtonImage(this.button19, 3);

                    _ing = "COMPLETE";
                }
                else
                {
                    frmShowMessageBox msgBox = new frmShowMessageBox("일괄 시작된 프로세스가 없습니다.");
                    msgBox.StartPosition = FormStartPosition.CenterScreen;
                    msgBox.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 자동수집 옵션
        /// <summary>
        /// 자동보정 체크
        /// </summary>
        private void checkBox_auto_modify_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox_auto_modify.Checked)
            {
                AppStatic.RT_AUTO_MODIFY = true;
                label_auto_modify_status.Text = ": 자동보정중";
            }
            else
            {
                AppStatic.RT_AUTO_MODIFY = false;
                label_auto_modify_status.Text = ": 자동보정 정지";
            }

            label_getTimeAutoModify();
        }
        /// <summary>
        /// 자동보정 시간 셋팅
        /// </summary>
        private void button_auto_mdodify_Click(object sender, EventArgs e)
        {
            iwater.checkAutoModify(dateTimePicker_auto_modify.Value.ToString("yyyyMMddHHmm"));

            label_getTimeAutoModify();
        }
        /// <summary>
        /// 자동보정 시간 반환
        /// </summary>
        private void label_getTimeAutoModify()
        {
            string time = iwater.getTimeAutoModify();
            label_auto_modify.Text = time;
        }
        #endregion

#region Historian 데이터 #######################################
        #region 실시간 데이터 수집
        /// <summary>
        /// 실시간 데이터 수집
        /// </summary>
        bool bool_GatherRealtime = false;
        private void GatherRealtime(object sender, EventArgs e)
        {
            if (bool_GatherRealtime)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 실시간 데이터 수집이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("실시간 데이터 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_GatherRealtime = true;
                try
                {
                    hTimer.setButton1(this.button11);
                    SetButtonImage(this.button11, 2);
                    // 실시간 데이터 수집
                    hTimer.GatherRealtime_ThreadStart();

                    // 데이터 보정
                    hTimer.RecordPasttime_ThreadStart();

                    // 실시간 경고
                    hTimer.RealtimeWarning_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void GatherRealtime_stop(object sender, EventArgs e)
        {
            if (bool_GatherRealtime)
            {
                bool_GatherRealtime = false;
                if (this._threadPool.ContainsKey(hTimer.GatherRealtime_Pool().GetHashCode()))
                {
                    utils.TimerStop(hTimer.GatherRealtime_Pool());
                    this._threadPool.Remove(hTimer.GatherRealtime_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button11, 3);
        }

        #endregion

        #region 실시간 데이터 보정
        /// <summary>
        /// 실시간 데이터 보정
        /// </summary>
        private void ModifyRealtime(object sender, EventArgs e)
        {
            try
            {
                progressBar1.Step = 1;
                progressBar2.Step = 1;

                // from
                string cTime = dateTimePicker7.Value.ToString("yyyyMMddHHmm");
                // to
                string cTime2 = dateTimePicker8.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 실행중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_ModifyRealtime"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_ModifyRealtime"].ToString()))
                    {
                        frmShowMessageBox msgBox = new frmShowMessageBox("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        msgBox.StartPosition = FormStartPosition.CenterScreen;
                        msgBox.Show();
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker7, dateTimePicker8)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker7, dateTimePicker8)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    hTimer.setButton5(this.button13);
                    hTimer.setProgressBar1(this.progressBar1);
                    hTimer.setProgressBar2(this.progressBar2);
                    hTimer.ModifyRealtime_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ee)
            {
                Logger.Error(ee.ToString());
            }
        }

        #endregion

        #region 1시간 적산데이터 수집
        /// <summary>
        /// 1시간 적산데이터 수집
        /// </summary>
        bool bool_Accumulation_Hour = false;
        private void Accumulation_Hour(object sender, EventArgs e)
        {
            if (bool_Accumulation_Hour)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 1시간 적산 데이터 수집이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("1시간 적산 데이터 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_Accumulation_Hour = true;
                try
                {
                    hTimer.setButton2(this.button15);
                    SetButtonImage(this.button15, 2);
                    hTimer.Accumulation_Hour_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void Accumulation_Hour_stop(object sender, EventArgs e)
        {
            if (bool_Accumulation_Hour)
            {
                bool_Accumulation_Hour = false;
                if (this._threadPool.ContainsKey(hTimer.Accumulation_Hour_Pool().GetHashCode()))
                {
                    utils.TimerStop(hTimer.Accumulation_Hour_Pool());
                    this._threadPool.Remove(hTimer.Accumulation_Hour_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button15, 3);
        }
        #endregion

        #region 1시간 적산데이터 보정
        /// <summary>
        /// 1시간 적산데이터 보정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Modification_Hour(object sender, EventArgs e)
        {
            try
            {
                progressBar8.Step = 1;

                string cTime = dateTimePicker9.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker10.Value.ToString("yyyyMMddHHmm");

                // 1시간 적산데이터 보정이 실행중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_Modification_Hour"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_Modification_Hour"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker9, dateTimePicker10)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker9, dateTimePicker10)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    hTimer.setProgressBar3(this.progressBar8);
                    hTimer.Modification_Hour_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception e1)
            {
                Logger.Error(e1.ToString());
            }
        }
        #endregion

        #region 일 적산 데이터 수집
        /// <summary>
        /// 일 적산 데이터 수집
        /// </summary>
        bool bool_Accumulation_Day = false;
        private void Accumulation_Day(object sender, EventArgs e)
        {
            if (bool_Accumulation_Day)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 1일 적산 데이터 수집이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("1일 적산 데이터 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_Accumulation_Day = true;
                try
                {
                    hTimer.setButton3(this.button7);
                    SetButtonImage(this.button7, 2);
                    hTimer.Accumulation_Day_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void Accumulation_Day_stop(object sender, EventArgs e)
        {
            if (bool_Accumulation_Day)
            {
                bool_Accumulation_Day = false;
                if (this._threadPool.ContainsKey(hTimer.Accumulation_Day_Pool().GetHashCode()))
                {
                    utils.TimerStop(hTimer.Accumulation_Day_Pool());
                    this._threadPool.Remove(hTimer.Accumulation_Day_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button7, 3);
        }
        #endregion

        #region 일 적산 데이터 보정
        /// <summary>
        /// 일 적산 데이터 보정
        /// </summary>
        private void Modification_Day(object sender, EventArgs e)
        {
            try
            {
                progressBar8.Step = 1;

                string cTime = dateTimePicker1.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker6.Value.ToString("yyyyMMddHHmm");

                // 일 적산 데이터 보정이 실행중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_Modification_Day"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_Modification_Day"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker1, dateTimePicker6)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker1, dateTimePicker6)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    hTimer.setProgressBar4(this.progressBar5);

                    hTimer.Modification_Day_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception e1)
            {
                Logger.Error(e1.ToString());
            }
        }
        #endregion

        #region 금일 적산 데이터 수집
        /// <summary>
        /// 금일 적산 데이터 수집
        /// </summary>
        bool bool_Accumulation_ToDay = false;
        private void Accumulation_ToDay(object sender, EventArgs e)
        {
            if (bool_Accumulation_ToDay)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 금일 적산 데이터 수집이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("금일 적산 데이터 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_Accumulation_ToDay = true;
                try
                {
                    hTimer.setButton4(this.button10);
                    SetButtonImage(this.button10, 2);
                    hTimer.Accumulation_ToDay_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void Accumulation_ToDay_stop(object sender, EventArgs e)
        {
            if (bool_Accumulation_ToDay)
            {
                bool_Accumulation_ToDay = false;
                if (this._threadPool.ContainsKey(hTimer.Accumulation_ToDay_Pool().GetHashCode()))
                {
                    utils.TimerStop(hTimer.Accumulation_ToDay_Pool());
                    this._threadPool.Remove(hTimer.Accumulation_ToDay_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button10, 3);
        }
        #endregion 
#endregion

#region 배치작업 ###############################################

        #region 야간최소유량
        /// <summary>
        /// 야간 최소유량 일별
        /// </summary>
        bool bool_MinimumNightFlow = false;
        private void MinimumNightFlow(object sender, EventArgs e)
        {
            frmShowMessageBox msgBox = new frmShowMessageBox("일별 야간 최소유량 수집 시작");
            msgBox.StartPosition = FormStartPosition.CenterScreen;
            msgBox.Show();
            if (bool_MinimumNightFlow)
            {
                frmShowMessageBox msgBox2 = new frmShowMessageBox("이미 일별 야간 최소유량 수집이 시작되었습니다.");
                msgBox2.StartPosition = FormStartPosition.CenterScreen;
                msgBox2.Show();
            }
            else
            {
                bool_MinimumNightFlow = true;
                try
                {
                    wvTimer.setButton1(this.button5);
                    SetButtonImage(this.button5, 2);
                    wvTimer.MinimumNightFlow_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void MinimumNightFlow_stop(object sender, EventArgs e)
        {
            if (bool_MinimumNightFlow)
            {
                bool_MinimumNightFlow = false;
                if (this._threadPool.ContainsKey(wvTimer.MinimumNightFlow_Pool().GetHashCode()))
                {
                    utils.TimerStop(wvTimer.MinimumNightFlow_Pool());
                    this._threadPool.Remove(wvTimer.MinimumNightFlow_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button5, 3);
        }
        #endregion

        #region 야간최소유량 보정
        /// <summary>
        /// 야간 최소유량 보정
        /// </summary>
        private void MinimumNightFlow_modify(object sender, EventArgs e)
        {
            try
            {
                progressBar9.Step = 1;

                string cTime = dateTimePicker2.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker3.Value.ToString("yyyyMMddHHmm");

                // 야간 최소유량 보정이 실행중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_MinimumNightFlow_modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_MinimumNightFlow_modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker2, dateTimePicker3)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                //if ("YES".Equals(utils.IsContainToday(dateTimePicker2, dateTimePicker3)))
                //{
                //    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                //    logger.Debug("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                //    return;
                //}

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    wvTimer.setProgressBar2(this.progressBar6);
                    wvTimer.MinimumNightFlow_modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 정시수압
        /// <summary>
        /// 1시간 정시 수압 시간별
        /// </summary>
        bool bool_AveragePressure = false;
        private void AveragePressure(object sender, EventArgs e)
        {
            frmShowMessageBox msgBox = new frmShowMessageBox("정시 수압 수집 시작");
            msgBox.StartPosition = FormStartPosition.CenterScreen;
            msgBox.Show();
            if (bool_AveragePressure)
            {
                frmShowMessageBox msgBox2 = new frmShowMessageBox("이미 정시 수압 수집이 시작되었습니다.");
                msgBox2.StartPosition = FormStartPosition.CenterScreen;
                msgBox2.Show();
            }
            else
            {
                bool_AveragePressure = true;
                try
                {
                    wvTimer.setButton2(this.button6);
                    SetButtonImage(this.button6, 2);
                    wvTimer.AveragePressure_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void AveragePressure_stop(object sender, EventArgs e)
        {
            if (bool_AveragePressure)
            {
                bool_AveragePressure = false;
                if (this._threadPool.ContainsKey(wvTimer.AveragePressure_Pool().GetHashCode()))
                {
                    utils.TimerStop(wvTimer.AveragePressure_Pool());
                    this._threadPool.Remove(wvTimer.AveragePressure_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button6, 3);
        }
        #endregion

        #region 정시수압 보정
        /// <summary>
        /// 1시간 정시 수압 보정
        /// </summary>
        private void AveragePressure_Modify(object sender, EventArgs e)
        {
            progressBar7.Step = 1;

            string cTime = dateTimePicker4.Value.ToString("yyyyMMddHHmm");
            string cTime2 = dateTimePicker5.Value.ToString("yyyyMMddHHmm");

            // 정시수압 보정이 실행중일경우 실행하지 않는다.
            if (this._threadPool.ContainsKey("runThread_AveragePressure_Modify"))
            {
                if ("RUN".Equals(this._threadPool["runThread_AveragePressure_Modify"].ToString()))
                {
                    MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                    return;
                }
            }

            // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
            if ("GT".Equals(utils.CompareDateTime(dateTimePicker4, dateTimePicker5)))
            {
                MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                return;
            }

            // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
            // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
            if ("YES".Equals(utils.IsContainToday(dateTimePicker4, dateTimePicker5)))
            {
                MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                return;
            }

            // 마지막 확인
            if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
            {
                wvTimer.setProgressBar3(this.progressBar7);

                wvTimer.AveragePressure_Modify_ThreadStart(cTime, cTime2);
            }
        }
        #endregion

        #region 누수감시
        /// <summary>
        /// 누수감시
        /// </summary>
        bool bool_MonitoringLeakage = false;
        private void MonitoringLeakage(object sender, EventArgs e)
        {
            frmShowMessageBox msgBox = new frmShowMessageBox("누수감시 시작");
            msgBox.StartPosition = FormStartPosition.CenterScreen;
            msgBox.Show();
            if (bool_MonitoringLeakage)
            {
                frmShowMessageBox msgBox2 = new frmShowMessageBox("이미 누수감시가 시작되었습니다.");
                msgBox2.StartPosition = FormStartPosition.CenterScreen;
                msgBox2.Show();
            }
            else
            {
                bool_MonitoringLeakage = true;
                try
                {
                    wvTimer.setButton3(this.button2);
                    SetButtonImage(this.button2, 2);
                    wvTimer.MonitoringLeakage_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void MonitoringLeakage_stop(object sender, EventArgs e)
        {
            if (bool_MonitoringLeakage)
            {
                bool_MonitoringLeakage = false;
                if (this._threadPool.ContainsKey(wvTimer.MonitoringLeakage_Pool().GetHashCode()))
                {
                    utils.TimerStop(wvTimer.MonitoringLeakage_Pool());
                    this._threadPool.Remove(wvTimer.MonitoringLeakage_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button2, 3);
        }
        #endregion

        #region 누수감시 보정
        /// <summary>
        /// 누수감시 보정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MonitoringLeakage_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar9.Step = 1;

                string cTime = dateTimePicker12.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker11.Value.ToString("yyyyMMddHHmm");

                // 실시간 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_MonitoringLeakage_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_MonitoringLeakage_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker12, dateTimePicker11)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker12, dateTimePicker11)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    wvTimer.setProgressBar1(this.progressBar9);
                    wvTimer.MonitoringLeakage_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion
#endregion

#region Water-INFOS 수집 #######################################
        #region 수용가정보
        /// <summary>
        /// 수용가정보
        /// </summary>
        bool bool_DMINFO = false;
        private void DMINFO(object sender, EventArgs e)
        {
            if (bool_DMINFO)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 수용가정보 수집이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("수용가정보 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_DMINFO = true;
                try
                {
                    infosTimer.setButton1(this.button52);
                    SetButtonImage(this.button52, 2);
                    infosTimer.DMINFO_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void DMINFO_stop(object sender, EventArgs e)
        {
            if (bool_DMINFO)
            {
                bool_DMINFO = false;
                if (this._threadPool.ContainsKey(infosTimer.DMINFO_Pool().GetHashCode()))
                {
                    utils.TimerStop(infosTimer.DMINFO_Pool());
                    this._threadPool.Remove(infosTimer.DMINFO_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button52, 3);
        }
        #endregion

        #region 수용가정보 보정
        /// <summary>
        /// 수용가정보 보정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DMINFO_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_DMINFO_Modify.Step = 1;

                string cTime = dateTimePicker14.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker13.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_DMINFO_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_DMINFO_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker14, dateTimePicker13)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker14, dateTimePicker13)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    infosTimer.setProgressBar_DMINFO_Modify(this.progressBar_DMINFO_Modify);
                    infosTimer.DMINFO_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 상하수도자원
        /// <summary>
        /// 상하수도자원
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        bool bool_DMWSRSRC = false;
        private void DMWSRSRC(object sender, EventArgs e)
        {

            if (bool_DMWSRSRC)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 상하수도자원 수집이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("상하수도자원 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_DMWSRSRC = true;
                try
                {
                    infosTimer.setButton2(this.button61);
                    SetButtonImage(this.button61, 2);
                    infosTimer.DMWSRSRC_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void DMWSRSRC_stop(object sender, EventArgs e)
        {
            if (bool_DMWSRSRC)
            {
                bool_DMWSRSRC = false;
                if (this._threadPool.ContainsKey(infosTimer.DMWSRSRC_Pool().GetHashCode()))
                {
                    utils.TimerStop(infosTimer.DMWSRSRC_Pool());
                    this._threadPool.Remove(infosTimer.DMWSRSRC_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button61, 3);
        }
        #endregion

        #region 상하수도자원 보정
        /// <summary>
        /// 상하수도자원 보정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DMWSRSRC_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_DMWSRSRC_Modify.Step = 1;

                string cTime = dateTimePicker16.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker15.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_DMWSRSRC_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_DMWSRSRC_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker16, dateTimePicker15)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker16, dateTimePicker15)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    infosTimer.setProgressBar_DMWSRSRC_Modify(this.progressBar_DMWSRSRC_Modify);
                    infosTimer.DMWSRSRC_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 민원정보
        /// <summary>
        /// 민원기본정보
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        bool bool_CAINFO = false;
        private void CAINFO(object sender, EventArgs e)
        {
            if (bool_CAINFO)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 민원기본정보 수집이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("민원기본정보 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_CAINFO = true;
                try
                {
                    infosTimer.setButton3(this.button55);
                    SetButtonImage(this.button55, 2);
                    infosTimer.CAINFO_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void CAINFO_stop(object sender, EventArgs e)
        {
            if (bool_CAINFO)
            {
                bool_CAINFO = false;
                if (this._threadPool.ContainsKey(infosTimer.CAINFO_Pool().GetHashCode()))
                {
                    utils.TimerStop(infosTimer.CAINFO_Pool());
                    this._threadPool.Remove(infosTimer.CAINFO_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button55, 3);
        }
        #endregion

        #region 민원기본정보 보정
        /// <summary>
        /// 민원기본정보 보정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CAINFO_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_CAINFO_Modify.Step = 1;

                string cTime = dateTimePicker18.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker17.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_CAINFO_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_CAINFO_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker18, dateTimePicker17)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker18, dateTimePicker17)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    infosTimer.setProgressBar_CAINFO_Modify(this.progressBar_CAINFO_Modify);
                    infosTimer.CAINFO_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 계량기교체정보
        /// <summary>
        /// 계량기교체정보
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        bool bool_MTRCHGINFO = false;
        private void MTRCHGINFO(object sender, EventArgs e)
        {
            if (bool_MTRCHGINFO)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 계량기교체정보 수집이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("계량기교체정보 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_MTRCHGINFO = true;
                try
                {
                    infosTimer.setButton4(this.button53);
                    SetButtonImage(this.button53, 2);
                    infosTimer.MTRCHGINFO_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void MTRCHGINFO_stop(object sender, EventArgs e)
        {
            if (bool_MTRCHGINFO)
            {
                bool_MTRCHGINFO = false;
                if (this._threadPool.ContainsKey(infosTimer.MTRCHGINFO_Pool().GetHashCode()))
                {
                    utils.TimerStop(infosTimer.MTRCHGINFO_Pool());
                    this._threadPool.Remove(infosTimer.MTRCHGINFO_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button53, 3);
        }
        #endregion

        #region 계량기교체정보 보정
        /// <summary>
        /// 계량기교체정보 보정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MTRCHGINFO_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_MTRCHGINFO_Modify.Step = 1;

                string cTime = dateTimePicker20.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker19.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_MTRCHGINFO_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_MTRCHGINFO_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker20, dateTimePicker19)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker20, dateTimePicker19)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    infosTimer.setProgressBar_MTRCHGINFO_Modify(this.progressBar_MTRCHGINFO_Modify);
                    infosTimer.MTRCHGINFO_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 요금조정
        /// <summary>
        /// 요금조정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        bool bool_STWCHRG = false;
        private void STWCHRG(object sender, EventArgs e)
        {
            if (bool_STWCHRG)
            {
                frmShowMessageBox msgBox2 = new frmShowMessageBox("이미 요금조정 수집이 시작되었습니다.");
                msgBox2.StartPosition = FormStartPosition.CenterScreen;
                msgBox2.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("요금조정 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();


                bool_STWCHRG = true;
                try
                {
                    infosTimer.setButton5(this.button54);
                    SetButtonImage(this.button54, 2);
                    infosTimer.STWCHRG_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void STWCHRG_stop(object sender, EventArgs e)
        {
            if (bool_STWCHRG)
            {
                bool_STWCHRG = false;
                if (this._threadPool.ContainsKey(infosTimer.STWCHRG_Pool().GetHashCode()))
                {
                    utils.TimerStop(infosTimer.STWCHRG_Pool());
                    this._threadPool.Remove(infosTimer.STWCHRG_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button54, 3);
        }
        #endregion

        #region 요금조정 보정
        /// <summary>
        /// 요금조정 보정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void STWCHRG_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_STWCHRG_Modify.Step = 1;

                string cTime = dateTimePicker22.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker21.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_STWCHRG_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_STWCHRG_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker22, dateTimePicker21)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker22, dateTimePicker21)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    infosTimer.setProgressBar_STWCHRG_Modify(this.progressBar_STWCHRG_Modify);
                    infosTimer.STWCHRG_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion
#endregion

#region 수량 작업 ##############################################
        #region 급수전수계산
        /// <summary>
        /// 급수전수계산 (1일 1회)
        /// </summary>
        bool bool_DM_Info = false;
        private void DM_Info(object sender, EventArgs e)
        {
            if (bool_DM_Info)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 급수전수계산이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("수량관리-급수전수계산 (1일 1회) 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_DM_Info = true;
                try
                {
                    SetButtonImage(this.button56, 2);
                    volumeTimer.setButton1(this.button56);
                    volumeTimer.DM_Info_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void DM_Info_stop(object sender, EventArgs e)
        {
            if (bool_DM_Info)
            {
                bool_DM_Info = false;
                if (this._threadPool.ContainsKey(volumeTimer.DM_Info_Pool().GetHashCode()))
                {
                    utils.TimerStop(volumeTimer.DM_Info_Pool());
                    this._threadPool.Remove(volumeTimer.DM_Info_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button56, 3);
        }
        #endregion

        #region 급수전수계산 보정 DM_Info_Modify
        private void DM_Info_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_DM_Info_Modify.Step = 1;

                string cTime = dateTimePicker24.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker23.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_DM_Info_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_DM_Info_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker24, dateTimePicker23)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker24, dateTimePicker23)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    volumeTimer.setProgressBar_DM_Info_Modify(this.progressBar_DM_Info_Modify);
                    volumeTimer.DM_Info_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 블록별사용량계산
        /// <summary>
        /// 블록별사용량계산 (1일 1회)
        /// </summary>
        bool bool_DM_Used = false;
        private void DM_Used(object sender, EventArgs e)
        {
            if (bool_DM_Used)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 블록별사용량계산이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("수량관리-블록별사용량계산 (1일 1회) 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_DM_Used = true;
                try
                {
                    SetButtonImage(this.button57, 2);
                    volumeTimer.setButton2(this.button57);
                    volumeTimer.DM_Used_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void DM_Used_stop(object sender, EventArgs e)
        {
            if (bool_DM_Used)
            {
                bool_DM_Used = false;
                if (this._threadPool.ContainsKey(volumeTimer.DM_Used_Pool().GetHashCode()))
                {
                    utils.TimerStop(volumeTimer.DM_Used_Pool());
                    this._threadPool.Remove(volumeTimer.DM_Used_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button57, 3);
        }
        #endregion

        #region 블록별사용량계산 보정 DM_Used_Modify
        private void DM_Used_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_DM_Used_Modify.Step = 1;

                string cTime = dateTimePicker26.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker25.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_DM_Used_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_DM_Used_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker26, dateTimePicker25)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker26, dateTimePicker25)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    volumeTimer.setProgressBar_DM_Used_Modify(this.progressBar_DM_Used_Modify);
                    volumeTimer.DM_Used_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 관로길이계산
        /// <summary>
        /// 관로길이계산 (1일 1회)
        /// </summary>
        bool bool_Pipe_Info = false;
        private void Pipe_Info(object sender, EventArgs e)
        {
            if (bool_Pipe_Info)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 관로길이계산이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("수량관리-관로길이계산 (1일 1회) 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_Pipe_Info = true;
                try
                {
                    SetButtonImage(this.button58, 2);
                    volumeTimer.setButton3(this.button58);
                    volumeTimer.Pipe_Info_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void Pipe_Info_stop(object sender, EventArgs e)
        {
            if (bool_Pipe_Info)
            {
                bool_Pipe_Info = false;
                if (this._threadPool.ContainsKey(volumeTimer.Pipe_Info_Pool().GetHashCode()))
                {
                    utils.TimerStop(volumeTimer.Pipe_Info_Pool());
                    this._threadPool.Remove(volumeTimer.Pipe_Info_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button58, 3);
        }
        #endregion

        #region 관로길이계산 보정 Pipe_Info_Modify
        private void Pipe_Info_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_Pipe_Info_Modify.Step = 1;

                string cTime = dateTimePicker28.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker27.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_Pipe_Info_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_Pipe_Info_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker28, dateTimePicker27)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker28, dateTimePicker27)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    volumeTimer.setProgressBar_Pipe_Info_Modify(this.progressBar_Pipe_Info_Modify);
                    volumeTimer.Pipe_Info_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 유수율분석
        /// <summary>
        /// 유수율분석 (1일 1회)
        /// </summary>
        bool bool_RevenueRatio_Analysis = false;
        private void RevenueRatio_Analysis(object sender, EventArgs e)
        {
            if (bool_RevenueRatio_Analysis)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 유수율분석이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("수량관리-유수율분석 (1일 1회) 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_RevenueRatio_Analysis = true;
                try
                {
                    SetButtonImage(this.button59, 2);
                    volumeTimer.setButton4(this.button59);
                    volumeTimer.RevenueRatio_Analysis_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void RevenueRatio_Analysis_stop(object sender, EventArgs e)
        {
            if (bool_RevenueRatio_Analysis)
            {
                bool_RevenueRatio_Analysis = false;
                if (this._threadPool.ContainsKey(volumeTimer.RevenueRatio_Analysis_Pool().GetHashCode()))
                {
                    utils.TimerStop(volumeTimer.RevenueRatio_Analysis_Pool());
                    this._threadPool.Remove(volumeTimer.RevenueRatio_Analysis_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button59, 3);
        }
        #endregion

        #region 유수율분석 보정 RevenueRatio_Analysis_Modify
        private void RevenueRatio_Analysis_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_RevenueRatio_Analysis_Modify.Step = 1;

                string cTime = dateTimePicker30.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker29.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_RevenueRatio_Analysis_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_RevenueRatio_Analysis_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker30, dateTimePicker29)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker30, dateTimePicker29)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    volumeTimer.setProgressBar_RevenueRatio_Analysis_Modify(this.progressBar_RevenueRatio_Analysis_Modify);
                    volumeTimer.RevenueRatio_Analysis_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion

        #region 평균수압지점계산
        /// <summary>
        /// 평균수압지점계산 (1일 1회)
        /// </summary>

        bool bool_WHHpress_Result = false;
        private void WHHpress_Result(object sender, EventArgs e)
        {
            if (bool_WHHpress_Result)
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("이미 평균수압지점계산이 시작되었습니다.");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();
            }
            else
            {
                frmShowMessageBox msgBox = new frmShowMessageBox("수량관리-평균수압지점계산 (1일 1회) 수집 시작");
                msgBox.StartPosition = FormStartPosition.CenterScreen;
                msgBox.Show();

                bool_WHHpress_Result = true;
                try
                {
                    SetButtonImage(this.button60, 2);
                    volumeTimer.setButton5(this.button60);
                    volumeTimer.WHHpress_Result_ThreadStart();
                }
                catch (Exception e1)
                {
                    Logger.Error(e1.ToString());
                }
            }
        }
        private void WHHpress_Result_stop(object sender, EventArgs e)
        {
            if (bool_WHHpress_Result)
            {
                bool_WHHpress_Result = false;
                if (this._threadPool.ContainsKey(volumeTimer.WHHpress_Result_Pool().GetHashCode()))
                {
                    utils.TimerStop(volumeTimer.WHHpress_Result_Pool());
                    this._threadPool.Remove(volumeTimer.WHHpress_Result_Pool().GetHashCode());
                }
            }
            SetButtonImage(this.button60, 3);
        }
        #endregion

        #region 평균수압지점계산 보정 WHHpress_Result_Modify
        private void WHHpress_Result_Modify(object sender, EventArgs e)
        {
            try
            {
                progressBar_WHHpress_Result_Modify.Step = 1;

                string cTime = dateTimePicker32.Value.ToString("yyyyMMddHHmm");
                string cTime2 = dateTimePicker31.Value.ToString("yyyyMMddHHmm");

                // 데이터 보정이 수집중일경우 실행하지 않는다.
                if (this._threadPool.ContainsKey("runThread_WHHpress_Result_Modify"))
                {
                    if ("RUN".Equals(this._threadPool["runThread_WHHpress_Result_Modify"].ToString()))
                    {
                        MessageBox.Show("데이터 보정중입니다.\n작업이 끝난후 실행하십시오.");
                        return;
                    }
                }

                // 시작날짜가 종료날짜보다 크면 실행하지 않는다. 
                if ("GT".Equals(utils.CompareDateTime(dateTimePicker32, dateTimePicker31)))
                {
                    MessageBox.Show("시작 날짜가 종료날자보다 큽니다.");
                    return;
                }

                // 보정기간에서 금일이 포함되어 있으면 실행하지 않는다. 
                // 금일은 보정을 하게 되면 문제가 생김 (실시간 데이터 수집과 충돌나서 DeadLock이 걸림)
                if ("YES".Equals(utils.IsContainToday(dateTimePicker32, dateTimePicker31)))
                {
                    MessageBox.Show("데이터 보정은 금일을 포함할 수 없습니다.\n금일을 제외하고 실행하십시오.");
                    return;
                }

                // 마지막 확인
                if (DialogResult.Yes == MessageBox.Show("데이터 보정은 작업 중간 종료할 수 없습니다.\n진행 하시겠습니까?\n(진행동안 데이터 조회가 원할하지 않습니다.)", "경고", MessageBoxButtons.YesNo))
                {
                    volumeTimer.setProgressBar_WHHpress_Result_Modify(this.progressBar_WHHpress_Result_Modify);
                    volumeTimer.WHHpress_Result_Modify_ThreadStart(cTime, cTime2);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
        #endregion
#endregion

    }
}