﻿using System;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using WaterNETServer.Common;
using WaterNETServer.Common.utils;
using Oracle.DataAccess.Client;
using EMFrame.log;

namespace WaterNETServer.formPopup
{
    public partial class frmDatabase : Form
    {
        public static Configuration m_doc = null;

        private bool success_oracle = false;
        private bool success_rwis = false;
        private bool isPassRWIS = false;

        public frmMain parentForm = null;       //부모폼

        public frmDatabase()
        {
            InitializeComponent();
        }
        
        private void frmDatabase_Load(object sender, EventArgs e)
        {
            if (File.Exists(AppStatic.DB_CONFIG_FILE_PATH) == true)
            {
                if (m_doc == null)
                {
                    m_doc = new Configuration(AppStatic.DB_CONFIG_FILE_PATH);
                }

                this.txtWaterNETDataSource.Text = Security.DecryptKey(m_doc.getProp("/Root/waternet/servicename"));
                this.txtWaterNETDataBase.Text = Security.DecryptKey(m_doc.getProp("/Root/waternet/ip"));
                this.txtWaterNETUserID.Text = Security.DecryptKey(m_doc.getProp("/Root/waternet/id"));
                this.txtWaterNETPw.Text = Security.DecryptKey(m_doc.getProp("/Root/waternet/password"));
            }

            this.btnSave.Enabled = false;
        }

        #region Button Events

        private void btnExit_Click(object sender, EventArgs e)
        {
            parentForm.Close();
            this.Close();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            if (this.ConnectionTest() == true)
            {
                label4.Text = "Oracle 연결 성공";
            }
            else
            {
                MessageBox.Show("Water-NET 데이터베이스 연결 실패. 데이터베이스 정보를 다시 입력후 데스트 하십시오", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.HistorianConnectionTest() == true)
            {
                label5.Text = "RWIS 연결 성공";
            }
            else
            {
                MessageBox.Show("RWIS 데이터베이스 연결 실패. 데이터베이스 정보를 다시 입력후 데스트 하십시오", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.SaveDatabaseConfigXML() == true)
            {
                MessageBox.Show("Water-NET 데이터베이스 환경설정파일을 저장했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Water-NET 데이터베이스 환경설정파일을 저장에 실패했습니다. 다시 시도하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                isPassRWIS = true;
            }
            else
            {
                isPassRWIS = false;
            }
            if (success_oracle && (success_rwis || isPassRWIS))
            {
                btnSave.Enabled = true;
            }
            else
            {
                btnSave.Enabled = false;
            }
        }

        #endregion


        #region Control Events

        private void txtDataBase_MouseDown(object sender, MouseEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (tb.Text == "Database IP Address")
            {
                tb.Text = "";
            }
        }

        private void txtDataSource_MouseDown(object sender, MouseEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (tb.Text == "SID")
            {
                tb.Text = "";
            }
        }
        
        private void txtUserID_MouseDown(object sender, MouseEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (tb.Text == "User ID")
            {
                tb.Text = "";
            }
        }
        
        private void txtPw_MouseDown(object sender, MouseEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (tb.Text == "User Password")
            {
                tb.Text = "";
            }
        }


        private void txtDataBase_KeyPreass(object sender, KeyPressEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (tb.Text == "Database IP Address")
            {
                tb.Text = "";
            }
            if (e.KeyChar == 13)
            {
                if (this.txtWaterNETDataBase.Equals(tb))
                {
                    this.txtWaterNETDataSource.Focus();
                }
                else
                {
                    this.txtRWISDataSource.Focus();
                }
            }
        }

        private void txtDataSource_KeyPreass(object sender, KeyPressEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (tb.Text == "SID")
            {
                tb.Text = "";
            }
            if (e.KeyChar == 13)
            {
                if (this.txtWaterNETDataSource.Equals(tb))
                {
                    this.txtWaterNETUserID.Focus();
                }
                else
                {
                    this.txtRWISUserID.Focus();
                }
            }
        }

        private void txtUserID_KeyPreass(object sender, KeyPressEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (tb.Text == "User ID")
            {
                tb.Text = "";
            }
            if (e.KeyChar == 13)
            {
                if (this.txtWaterNETUserID.Equals(tb))
                {
                    this.txtWaterNETPw.Focus();
                }
                else
                {
                    this.txtRWISPw.Focus();
                }
            }
        }

        private void txtPw_KeyPreass(object sender, KeyPressEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (tb.Text == "User Password")
            {
                tb.Text = "";
            }
            if (e.KeyChar == 13)
            {
                if (this.txtWaterNETPw.Equals(tb))
                {
                    this.btnTest.Focus();
                }
                else
                {
                    this.btnRWIS.Focus();
                }
            }
        }
        #endregion


        #region User Function

        /// <summary>
        /// 데이터베이스 연결 테스트
        /// </summary>
        /// <returns></returns>
        private bool ConnectionTest()
        {
            try
            {

                string strConnString = string.Empty;
                
                strConnString = "Data Source=(DESCRIPTION="
                              + "(ADDRESS=(PROTOCOL=TCP)(HOST=" + txtWaterNETDataBase.Text + ")(PORT=1521))"
                              + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + txtWaterNETDataSource.Text + ")));"
                              + "User Id=" + txtWaterNETUserID.Text + ";Password=" + txtWaterNETPw.Text + ";";

                OracleConnection oDBConn = new OracleConnection(strConnString);

                oDBConn.Open();

                success_oracle = true;

                if (success_oracle && (success_rwis || isPassRWIS))
                {
                    btnSave.Enabled = true;
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                btnSave.Enabled = false;
                txtWaterNETDataBase.Focus();

                return false;
            }
        }

        /// <summary>
        /// Historian 연결 테스트
        /// </summary>
        /// <returns></returns>
        private bool HistorianConnectionTest()
        {
            try
            {
                string strConnString = string.Empty;


                strConnString = "Data Source=(DESCRIPTION="
                              + "(ADDRESS=(PROTOCOL=TCP)(HOST=" + txtRWISDataBase.Text + ")(PORT=1521))"
                              + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + txtRWISDataSource.Text + ")));"
                              + "User Id=" + txtRWISUserID.Text + ";Password=" + txtRWISPw.Text + ";";

                OracleConnection oDBConn = new OracleConnection(strConnString);

                oDBConn.Open();

                success_rwis = true;
                if (success_oracle && (success_rwis || isPassRWIS))
                {
                    btnSave.Enabled = true;
                }

                return true;
            }
            catch
            {
                if (success_oracle && isPassRWIS)
                {
                    btnSave.Enabled = true;
                }
                else
                {
                    btnSave.Enabled = false;
                }
                txtWaterNETDataBase.Focus();

                return false;
            }
        }
        /// <summary>
        /// 데이터베이스 연결 정보를 XML로 Save
        /// </summary>
        private bool SaveDatabaseConfigXML()
        {
            try
            {
                string[] strDbValue = new string[8];

                string strConfigPath = Application.StartupPath + @"\Config\";
                string strConfigFile = "DB_Config.xml";

                if (this.ConnectionTest() != true)
                {
                    btnSave.Enabled = false;
                    success_oracle = false;
                    txtWaterNETDataBase.Focus();

                    MessageBox.Show("Water-NET 데이터베이스 연결 실패. 데이터베이스 정보를 다시 입력후 데스트 하십시오", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return false;
                }

                if(!isPassRWIS)
                {
                    if ((this.HistorianConnectionTest() != true))
                    {
                        btnSave.Enabled = false;
                        success_rwis = false;
                        txtRWISUserID.Focus();

                        MessageBox.Show("Historian 데이터베이스 연결 실패. 데이터베이스 정보를 다시 입력후 데스트 하십시오", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        return false;
                    }
                }
                

                if (Configuration.IsDirectoryExists(strConfigPath) != true)
                {
                    Configuration.CreateDirectory(strConfigPath);
                }

                if (File.Exists(strConfigPath + strConfigFile) == true)
                {
                    File.Delete(strConfigPath + strConfigFile);
                }

                strDbValue[0] = txtWaterNETDataBase.Text.Trim();    //IP
                strDbValue[1] = txtWaterNETDataSource.Text.Trim();  //SID
                strDbValue[2] = txtWaterNETUserID.Text.Trim();      //ID
                strDbValue[3] = txtWaterNETPw.Text.Trim();          //Password

                if (txtRWISDataBase == null)
                {
                    strDbValue[4] = "";
                }
                else
                {
                    strDbValue[4] = txtRWISDataBase.Text.Trim();
                }

                if (txtRWISDataSource == null)
                {
                    strDbValue[5] = "";
                }
                else
                {
                    strDbValue[5] = txtRWISDataSource.Text.Trim();
                }

                if (txtRWISUserID == null)
                {
                    strDbValue[6] = "";
                }
                else
                {
                    strDbValue[6] = txtRWISUserID.Text.Trim();
                }

                if (txtRWISPw == null)
                {
                    strDbValue[7] = "";
                }
                else
                {
                    strDbValue[7] = txtRWISPw.Text.Trim();
                }

                if (this.CreateDatabaseConfigFile(strDbValue, strConfigPath + strConfigFile) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }                
            }
            catch (Exception ex)
            {
                btnSave.Enabled = false;
                txtWaterNETDataBase.Focus();
                return false;
            }
        }

        /// <summary>
        /// Database Connection Information 정보를 XML파일로 생성한다.
        /// </summary>
        /// <param name="strValue">서브로 만들어질 목록</param>
        /// <param name="XMLfile">파일경로</param>
        public bool CreateDatabaseConfigFile(string[] strValue, string XMLfile)
        {
            try
            {
                XmlTextWriter textWriter = new XmlTextWriter(XMLfile, System.Text.Encoding.Default);
                textWriter.WriteStartDocument();
                textWriter.WriteStartElement("Root");
                textWriter.WriteEndElement();
                textWriter.Close();

                XmlDocument doc = new XmlDocument();
                doc.Load(XMLfile);

                XmlNode insertNode;
                XmlDocumentFragment docInsert = doc.CreateDocumentFragment();

                docInsert.InnerXml = "<waternet>"
                                   + "  <servicename>" + Security.EncryptKey(strValue[1].Trim()) + "</servicename>"
                                   + "  <ip>" + Security.EncryptKey(strValue[0].Trim()) + "</ip>"
                                   + "  <id>" + Security.EncryptKey(strValue[2].Trim()) + "</id>"
                                   + "  <password>" + Security.EncryptKey(strValue[3].Trim()) + "</password>"
                                   + "</waternet>"
                                   + "<rwis>"
                                   + "  <servicename>" + Security.EncryptKey(strValue[5].Trim()) + "</servicename>"
                                   + "  <ip>" + Security.EncryptKey(strValue[4].Trim()) + "</ip>"
                                   + "  <id>" + Security.EncryptKey(strValue[6].Trim()) + "</id>"
                                   + "  <password>" + Security.EncryptKey(strValue[7].Trim()) + "</password>"
                                   + "</rwis>"
                                   ;

                insertNode = doc.DocumentElement;
                insertNode.InsertAfter(docInsert, insertNode.LastChild);
                doc.Save(XMLfile);

                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

    }
}
