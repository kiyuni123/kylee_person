﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNETServer.WH_AnalysisScheduleManage.etc;

namespace WaterNETServer.formPopup
{
    public partial class frmPassword : Form
    {
        public frmPassword()
        {
            InitializeComponent();
        }

        public frmMain parentForm = null;                           //부모폼
        private string _contextMenuStripName = string.Empty;      //트레이의 어떤 메뉴에서 실행했는지.

        private void frmPassword_Load(object sender, EventArgs e)
        {
            this.KeyPress += new KeyPressEventHandler(this.GetKeyPress);

            this.textBox1.Text = string.Empty;
            this.textBox1.Focus();
        }

        public bool ComparePassword(string passwd)
        {
            // TODO : DB에서 Admin의 아이디와 비밀번호를 가져와서 로그인하도록 해야할듯..
            //   1. 현재 로그인한 ID를 저장
            //   2. 로그인한 ID의 비밀번호를 비교

            bool isEqual = false;

            if ("waternet!@#".Equals(passwd)) isEqual = true;

            return isEqual;
        }

        public void ControlPassword()
        {
            if (!ComparePassword(textBox1.Text))
            {
                MessageBox.Show("비밀번호가 틀렸습니다.");

                parentForm.Visible = false;

                return;
            }
            else
            {
                if ("EXIT".Equals(_contextMenuStripName))
                {
                    AnalysisScheduleManager.DestroyAnalysisJob();

                    Application.ExitThread();
                    Application.Exit();
                    this.Close();
                }
                if ("SHOW".Equals(_contextMenuStripName))
                {
                    parentForm.Visible = true;
                    this.Close();
                }
            }
        }
        internal void SetContextMenuStripName(string menuName)
        {
            if ("EXIT".Equals(menuName))
            {
                this._contextMenuStripName = "EXIT";
            }
            if ("SHOW".Equals(menuName))
            {
                this._contextMenuStripName = "SHOW";
            }
        }

        private void GetKeyPress(object sender, KeyPressEventArgs e)
        {
            // ESC 입력시 창 닫음
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Close();
                this.Dispose();

                return;
            }

            // Enter 입력시 비밀번호 비교
            if (e.KeyChar == (char)Keys.Enter)
            {
                ControlPassword();

                return;
            }
        }

        /// <summary>
        /// 비밀번호 비교
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            ControlPassword();
        }
    }
}
