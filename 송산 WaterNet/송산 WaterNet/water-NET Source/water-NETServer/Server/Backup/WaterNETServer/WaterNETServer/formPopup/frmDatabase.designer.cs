﻿namespace WaterNETServer.formPopup
{
    partial class frmDatabase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDatabase));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.lblDataSource = new System.Windows.Forms.Label();
            this.txtWaterNETDataSource = new System.Windows.Forms.TextBox();
            this.txtWaterNETDataBase = new System.Windows.Forms.TextBox();
            this.lblUserID = new System.Windows.Forms.Label();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.txtWaterNETUserID = new System.Windows.Forms.TextBox();
            this.txtWaterNETPw = new System.Windows.Forms.TextBox();
            this.lblPw = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtRWISUserID = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtRWISPw = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRWIS = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRWISDataBase = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRWISDataSource = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(229, 265);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 26);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "저장";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnTest
            // 
            this.btnTest.Image = ((System.Drawing.Image)(resources.GetObject("btnTest.Image")));
            this.btnTest.Location = new System.Drawing.Point(13, 113);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(258, 26);
            this.btnTest.TabIndex = 4;
            this.btnTest.Text = "테스트";
            this.btnTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // lblDataSource
            // 
            this.lblDataSource.AutoSize = true;
            this.lblDataSource.BackColor = System.Drawing.Color.Transparent;
            this.lblDataSource.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataSource.Location = new System.Drawing.Point(25, 40);
            this.lblDataSource.Name = "lblDataSource";
            this.lblDataSource.Size = new System.Drawing.Size(90, 14);
            this.lblDataSource.TabIndex = 501;
            this.lblDataSource.Text = "Data Source :";
            this.lblDataSource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWaterNETDataSource
            // 
            this.txtWaterNETDataSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWaterNETDataSource.Location = new System.Drawing.Point(117, 37);
            this.txtWaterNETDataSource.MaxLength = 16;
            this.txtWaterNETDataSource.Name = "txtWaterNETDataSource";
            this.txtWaterNETDataSource.Size = new System.Drawing.Size(154, 22);
            this.txtWaterNETDataSource.TabIndex = 1;
            this.txtWaterNETDataSource.Text = "SID";
            this.txtWaterNETDataSource.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtDataSource_MouseDown);
            this.txtWaterNETDataSource.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDataSource_KeyPreass);
            // 
            // txtWaterNETDataBase
            // 
            this.txtWaterNETDataBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWaterNETDataBase.Location = new System.Drawing.Point(117, 13);
            this.txtWaterNETDataBase.MaxLength = 16;
            this.txtWaterNETDataBase.Name = "txtWaterNETDataBase";
            this.txtWaterNETDataBase.Size = new System.Drawing.Size(154, 22);
            this.txtWaterNETDataBase.TabIndex = 0;
            this.txtWaterNETDataBase.Text = "Database IP Address";
            this.txtWaterNETDataBase.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtDataBase_MouseDown);
            this.txtWaterNETDataBase.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDataBase_KeyPreass);
            // 
            // lblUserID
            // 
            this.lblUserID.AutoSize = true;
            this.lblUserID.BackColor = System.Drawing.Color.Transparent;
            this.lblUserID.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserID.Location = new System.Drawing.Point(56, 64);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(59, 14);
            this.lblUserID.TabIndex = 502;
            this.lblUserID.Text = "User ID :";
            this.lblUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.BackColor = System.Drawing.Color.Transparent;
            this.lblDatabase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabase.Location = new System.Drawing.Point(10, 16);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(105, 14);
            this.lblDatabase.TabIndex = 504;
            this.lblDatabase.Text = "Database Host :";
            this.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWaterNETUserID
            // 
            this.txtWaterNETUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWaterNETUserID.Location = new System.Drawing.Point(117, 61);
            this.txtWaterNETUserID.MaxLength = 16;
            this.txtWaterNETUserID.Name = "txtWaterNETUserID";
            this.txtWaterNETUserID.Size = new System.Drawing.Size(154, 22);
            this.txtWaterNETUserID.TabIndex = 2;
            this.txtWaterNETUserID.Text = "User ID";
            this.txtWaterNETUserID.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtUserID_MouseDown);
            this.txtWaterNETUserID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserID_KeyPreass);
            // 
            // txtWaterNETPw
            // 
            this.txtWaterNETPw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWaterNETPw.Location = new System.Drawing.Point(117, 85);
            this.txtWaterNETPw.MaxLength = 16;
            this.txtWaterNETPw.Name = "txtWaterNETPw";
            this.txtWaterNETPw.Size = new System.Drawing.Size(154, 22);
            this.txtWaterNETPw.TabIndex = 3;
            this.txtWaterNETPw.Text = "User Password";
            this.txtWaterNETPw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtPw_MouseDown);
            this.txtWaterNETPw.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPw_KeyPreass);
            // 
            // lblPw
            // 
            this.lblPw.AutoSize = true;
            this.lblPw.BackColor = System.Drawing.Color.Transparent;
            this.lblPw.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPw.Location = new System.Drawing.Point(41, 88);
            this.lblPw.Name = "lblPw";
            this.lblPw.Size = new System.Drawing.Size(74, 14);
            this.lblPw.TabIndex = 503;
            this.lblPw.Text = "Password :";
            this.lblPw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnExit
            // 
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.Location = new System.Drawing.Point(306, 265);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(70, 26);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "닫기";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(604, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 518;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnTest);
            this.groupBox1.Controls.Add(this.txtWaterNETUserID);
            this.groupBox1.Controls.Add(this.txtWaterNETPw);
            this.groupBox1.Controls.Add(this.lblDatabase);
            this.groupBox1.Controls.Add(this.lblDataSource);
            this.groupBox1.Controls.Add(this.lblPw);
            this.groupBox1.Controls.Add(this.lblUserID);
            this.groupBox1.Controls.Add(this.txtWaterNETDataSource);
            this.groupBox1.Controls.Add(this.txtWaterNETDataBase);
            this.groupBox1.Location = new System.Drawing.Point(12, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(287, 164);
            this.groupBox1.TabIndex = 519;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Oracle";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 14);
            this.label4.TabIndex = 505;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtRWISUserID);
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Controls.Add(this.txtRWISPw);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnRWIS);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtRWISDataBase);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtRWISDataSource);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(306, 96);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(287, 163);
            this.groupBox2.TabIndex = 520;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RWIS";
            // 
            // txtRWISUserID
            // 
            this.txtRWISUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRWISUserID.Location = new System.Drawing.Point(116, 61);
            this.txtRWISUserID.MaxLength = 16;
            this.txtRWISUserID.Name = "txtRWISUserID";
            this.txtRWISUserID.Size = new System.Drawing.Size(154, 22);
            this.txtRWISUserID.TabIndex = 508;
            this.txtRWISUserID.Text = "User ID";
            this.txtRWISUserID.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtUserID_MouseDown);
            this.txtRWISUserID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserID_KeyPreass);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(162, 140);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(108, 18);
            this.checkBox1.TabIndex = 511;
            this.checkBox1.Text = "RWIS 사용안함";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtRWISPw
            // 
            this.txtRWISPw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRWISPw.Location = new System.Drawing.Point(116, 85);
            this.txtRWISPw.MaxLength = 16;
            this.txtRWISPw.Name = "txtRWISPw";
            this.txtRWISPw.Size = new System.Drawing.Size(154, 22);
            this.txtRWISPw.TabIndex = 509;
            this.txtRWISPw.Text = "User Password";
            this.txtRWISPw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtPw_MouseDown);
            this.txtRWISPw.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPw_KeyPreass);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 14);
            this.label5.TabIndex = 506;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 14);
            this.label1.TabIndex = 513;
            this.label1.Text = "Database Host :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnRWIS
            // 
            this.btnRWIS.Image = ((System.Drawing.Image)(resources.GetObject("btnRWIS.Image")));
            this.btnRWIS.Location = new System.Drawing.Point(12, 113);
            this.btnRWIS.Name = "btnRWIS";
            this.btnRWIS.Size = new System.Drawing.Size(258, 26);
            this.btnRWIS.TabIndex = 510;
            this.btnRWIS.Text = "테스트";
            this.btnRWIS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRWIS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRWIS.UseVisualStyleBackColor = true;
            this.btnRWIS.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 14);
            this.label2.TabIndex = 510;
            this.label2.Text = "Data Source :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRWISDataBase
            // 
            this.txtRWISDataBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRWISDataBase.Location = new System.Drawing.Point(116, 13);
            this.txtRWISDataBase.MaxLength = 16;
            this.txtRWISDataBase.Name = "txtRWISDataBase";
            this.txtRWISDataBase.Size = new System.Drawing.Size(154, 22);
            this.txtRWISDataBase.TabIndex = 506;
            this.txtRWISDataBase.Text = "Database IP Address";
            this.txtRWISDataBase.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtDataBase_MouseDown);
            this.txtRWISDataBase.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDataBase_KeyPreass);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 14);
            this.label3.TabIndex = 512;
            this.label3.Text = "Password :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRWISDataSource
            // 
            this.txtRWISDataSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRWISDataSource.Location = new System.Drawing.Point(116, 37);
            this.txtRWISDataSource.MaxLength = 16;
            this.txtRWISDataSource.Name = "txtRWISDataSource";
            this.txtRWISDataSource.Size = new System.Drawing.Size(154, 22);
            this.txtRWISDataSource.TabIndex = 507;
            this.txtRWISDataSource.Text = "SID";
            this.txtRWISDataSource.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtDataSource_MouseDown);
            this.txtRWISDataSource.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDataSource_KeyPreass);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(55, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 14);
            this.label6.TabIndex = 511;
            this.label6.Text = "User ID :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 298);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDatabase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Water-NET 데이터베이스 연결 정보";
            this.Load += new System.EventHandler(this.frmDatabase_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Label lblDataSource;
        private System.Windows.Forms.TextBox txtWaterNETDataSource;
        private System.Windows.Forms.TextBox txtWaterNETDataBase;
        private System.Windows.Forms.Label lblUserID;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.TextBox txtWaterNETUserID;
        private System.Windows.Forms.TextBox txtWaterNETPw;
        private System.Windows.Forms.Label lblPw;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnRWIS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtRWISUserID;
        private System.Windows.Forms.TextBox txtRWISPw;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRWISDataBase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRWISDataSource;
        private System.Windows.Forms.Label label6;

    }
}