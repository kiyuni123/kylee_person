﻿using System;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using System.Data;
using WaterNETServer.dao;

namespace WaterNETServer.form
{
    public partial class frmConditionPopup : Form
    {
        #region 선언부
        Common.utils.Common cm = new Common.utils.Common();
        OracleWork ow = new OracleWork();

        // 수정할 Grid
        UltraGrid uGrid;
        // 수정할 Cells
        SelectedCellsCollection columnName;

        // 일괄 수정으로 할것이면 참
        bool isOneColumn = true;
        // 적용할 값
        string editValue = string.Empty;

        /// <summary>
        /// 생성자
        /// </summary>
        public frmConditionPopup()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="col">수정할 CellsCollection</param>
        public frmConditionPopup(SelectedCellsCollection col)
        {
            InitializeComponent();

            // 선택된 Cell 들을 받음
            this.columnName = col;
        }
        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="col">수정할 CellsCollection</param>
        public frmConditionPopup(UltraGrid grid)
        {
            InitializeComponent();

            // Form Size 설정
            System.Drawing.Size fSize = new System.Drawing.Size(322, 59);
            this.Size = fSize;

            this.uGrid = grid;

            // 선택된 Cell 들을 받음
            this.columnName = grid.Selected.Cells;
        }

        #endregion

        #region 초기화

        private void frmConditionPopup_Load(object sender, EventArgs e)
        {
            this.KeyPress += new KeyPressEventHandler(this.GetKeyPress);
            // 콤보박스 초기화
            selectCodeCondition();
            // 보여줄 컨트롤 설정
            SetControlVisual();

            this.Focus();
        }

        #region 사용 컨트롤 검색조건 초기화
        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void selectCodeCondition()
        {
            SelectCodeList1();
            SelectCodeList2();
            SelectCodeList3();
            SelectCodeList4();
            SelectCodeList5();
            SelectCodeList7();
            SelectCodeList8();
            SelectCodeList9();
            SelectCodeListCmLocation();
        }
        /// <summary>
        /// 1.지역본부 검색조건
        /// </summary>
        private void SelectCodeList1()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7001");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox1, tableName);
        }
        /// <summary>
        /// 2.사무소기호
        /// </summary>
        private void SelectCodeList2()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7002");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox2, tableName);
        }
        /// <summary>
        /// 3.사업장기호
        /// </summary>
        private void SelectCodeList3()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7003");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox3, tableName);
        }
        /// <summary>
        /// 4.변량및기기기호
        /// </summary>
        private void SelectCodeList4()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7004");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox4, tableName);
        }
        /// <summary>
        /// 5.기능기호
        /// </summary>
        private void SelectCodeList5()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7005");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox5, tableName);
        }
        /// <summary>
        /// 7.태그구분
        /// </summary>
        private void SelectCodeList7()
        {
            string tableName = "TagGbn";

            DataSet dSet = ow.SelectTagGbn(tableName, null);

            cm.SetComboBox(dSet, comboBox7, tableName);
        }
        /// <summary>
        /// 8.입출력구분(배수지 입력,출력 등)
        /// </summary>
        private void SelectCodeList8()
        {
            string tableName = "IOGbn";

            DataSet dSet = ow.SelectIOGbn(tableName, null);

            cm.SetComboBox(dSet, comboBox8, tableName);
        }
        /// <summary>
        /// 9.사용여부
        /// </summary>
        private void SelectCodeList9()
        {
            string tableName = "CodeList";
            Hashtable param = new Hashtable();
            param.Add("PCODE", "7009");

            DataSet dSet = ow.SelectCodeList(tableName, param);

            cm.SetComboBox(dSet, comboBox9, tableName);
        }

        /// <summary>
        /// 10.지역정보
        /// </summary>
        private void SelectCodeListCmLocation()
        {
            string tableName = "cm_location";

            DataSet dSet = ow.SelectCmLocation(tableName, null);

            cm.SetComboBox(dSet, comboBox6, tableName);

            cm.SetComboBox(dSet, comboBox10, tableName);
        }
        #endregion

        /// <summary>
        /// 사용할 컨트롤을 Visible True로 바꾸어준다
        /// </summary>
        private void SetControlVisual()
        {
            // 컬럼 값을 저장할 임시 변수
            string tmpColumn = "";

            // 컬럼 갯수가 "0" 이상일때 (선택된 셀수가 있을때) 실행
            if (columnName.Count > 0)
            {
                // 일단 첫번 째 컬럼명을 임시 변수에 넣는다
                tmpColumn = columnName[0].Column.ToString();

                for (int i = 1; i < columnName.Count; i++)
                {
                    // 두번째 부터 첫번째 컬럼명이랑 비교해서 다르면 무조건 false
                    if (!tmpColumn.Equals(columnName[i].Column.ToString()))
                    {
                        isOneColumn = false;
                    }
                }
            }

            // 일단 모든 컨트롤을 숨긴다
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
            panel4.Visible = false;
            panel5.Visible = false;
            panel6.Visible = false;
            panel7.Visible = false;
            panel8.Visible = false;
            panel9.Visible = false;
            panel10.Visible = false;
            panel11.Visible = false;
            panel12.Visible = false;
            panel13.Visible = false;
            panel14.Visible = false;
            panel15.Visible = false;
            panel17.Visible = false;
            panel18.Visible = false;
            panel19.Visible = false;

            // 선택된 컬럼명이 1개 이상일때는 1번째 컨트롤을 보이게 한다.
            if (!isOneColumn)
            {
                panel1.Visible = true;
            }
            else
            {
                // 보여줄 컨트롤 셋팅
                switch (columnName[0].Column.ToString())
                {
                    case "본부":
                        panel2.Visible = true;
                        break;
                    case "사무소":
                        panel3.Visible = true;
                        break;
                    case "사업장":
                        panel4.Visible = true;
                        break;
                    case "변량기호":
                        panel5.Visible = true;
                        break;
                    case "기능기호":
                        panel6.Visible = true;
                        break;
                    case "계통코드":
                        panel7.Visible = true;
                        break;
                    case "구분번호":
                        panel8.Visible = true;
                        break;
                    case "배수지구분":
                        panel9.Visible = true;
                        break;
                    case "태그구분":
                        panel10.Visible = true;
                        break;
                    case "입출구분":
                        panel11.Visible = true;
                        break;
                    case "관련태그":
                        panel12.Visible = true;
                        break;
                    case "사용여부":
                        panel13.Visible = true;
                        break;
                    case "GIS코드":
                        panel14.Visible = true;
                        break;
                    case "블록정보":
                        panel15.Visible = true;
                        break;
                    case "블록정보2":
                        panel18.Visible = true;
                        break;
                    case "승수":
                        panel19.Visible = true;
                        break;
                    case "태그설명":
                        panel17.Visible = true;
                        break;
                }
            }
        }

        #endregion 

        #region 이벤트
        private void button1_Click(object sender, EventArgs e)
        {
            SaveChange();
        }

        private void GetKeyPress(object sender, KeyPressEventArgs e)
        {
            // ESC 입력시 창 닫음
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Close();
                this.Dispose();

                return;
            }

            // Enter 입력시 
            if (e.KeyChar == (char)Keys.Enter)
            {
                SaveChange();

                return;
            }
        }

        #endregion

        #region 기능

        /// <summary>
        /// 적용 : UltraGrid에 선택한(수정한) 값을 적용한다
        /// </summary>
        private void SaveChange()
        {
            // 선택된 컬럼명이 1개 이상일때는 1번째 값으로 일괄 수정
            if (!isOneColumn)
            {
                editValue = textBox1.Text;
            }
            else
            {
                // 적용할 컨트롤 값
                switch (columnName[0].Column.ToString())
                {
                    case "본부":
                        editValue = comboBox1.SelectedValue.ToString();
                        break;
                    case "사무소":
                        editValue = comboBox2.SelectedValue.ToString();
                        break;
                    case "사업장":
                        editValue = comboBox3.SelectedValue.ToString();
                        break;
                    case "변량기호":
                        editValue = comboBox4.SelectedValue.ToString();
                        break;
                    case "기능기호":
                        editValue = comboBox5.SelectedValue.ToString();
                        break;
                    case "계통코드":
                        editValue = textBox6.Text;
                        break;
                    case "구분번호":
                        editValue = textBox2.Text;
                        break;
                    case "배수지구분":
                        editValue = textBox3.Text;
                        break;
                    case "태그구분":
                        editValue = comboBox7.SelectedValue.ToString();
                        break;
                    case "입출구분":
                        editValue = comboBox8.SelectedValue.ToString();
                        break;
                    case "관련태그":
                        editValue = textBox4.Text;
                        break;
                    case "사용여부":
                        editValue = comboBox9.SelectedValue.ToString();
                        break;
                    case "GIS코드":
                        editValue = textBox5.Text;
                        break;
                    case "태그설명":
                        editValue = textBox7.Text;
                        break;
                    case "블록정보":
                        editValue = comboBox6.SelectedValue.ToString();
                        break;
                    case "블록정보2":
                        editValue = comboBox10.SelectedValue.ToString();
                        break;
                    case "승수":
                        editValue = textBox8.Text;
                        break;
                }
            }
            uGrid.BeginUpdate();
            // 선택된 그리드의 셀 값을 변경
            if (uGrid.Selected.Cells.Count > 0)
            {
                for (int i = 0; i < uGrid.Selected.Cells.Count; i++)
                {
                    uGrid.Selected.Cells[i].SetValue(editValue, false);
                }
            }
            uGrid.EndUpdate();

            uGrid.UpdateData();

            this.Close();
        }
        #endregion
    }
}
