﻿using System;
using System.Windows.Forms;

namespace WaterNETServer.formPopup
{
    public partial class frmShowMessageBox : Form
    {
        Timer stimer = new Timer();


        public frmShowMessageBox()
        {
            InitializeComponent();
        }

        public frmShowMessageBox(string msg)
        {
            InitializeComponent();

            this.label1.Text = msg;
        }


        private int cnt = 0;

        private void TimerStart()
        {
            stimer.Interval = 1000;
            stimer.Start();

            stimer.Tick += new EventHandler(thisClose);
        }

        private void thisClose(Object sender, EventArgs eArgs)
        {
            cnt++;
            if (cnt > 1)
            {
                stimer.Stop();
                this.Close();
            }
        }
        
        private void ThreadSleepStop()
        {
            System.Threading.Thread.Sleep(2000);

            this.Close();
        }

        private void frmShowMessageBox_Load(object sender, EventArgs e)
        {
            this.Name = "정보";

            this.Width = label1.Width + 20;
            this.Height = label1.Height + (95 - 12);

            TimerStart();
            //ThreadSleepStop();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
