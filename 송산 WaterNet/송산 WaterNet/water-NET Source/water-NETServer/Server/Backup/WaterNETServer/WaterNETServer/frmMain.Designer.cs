﻿namespace WaterNETServer
{
    partial class frmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.파일ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.uTabContents = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.iwaterRealtimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.dfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.tagImportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tagDivisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SHPNetworkGenerationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_SystemManage = new System.Windows.Forms.ToolStripMenuItem();
            this.dB설정파일삭제ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.contextMenu1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuthisClose = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuNthisClose = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuAllClose = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.griWarningList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabContents)).BeginInit();
            this.uTabContents.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.contextMenu1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griWarningList)).BeginInit();
            this.SuspendLayout();
            // 
            // 파일ToolStripMenuItem
            // 
            this.파일ToolStripMenuItem.Name = "파일ToolStripMenuItem";
            this.파일ToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Water-NET Schedule Manager";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.Tray_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem21,
            this.toolStripMenuItem22});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(105, 48);
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(104, 22);
            this.toolStripMenuItem21.Text = "Show";
            this.toolStripMenuItem21.Click += new System.EventHandler(this.Tray_DoubleClick);
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            this.toolStripMenuItem22.Size = new System.Drawing.Size(104, 22);
            this.toolStripMenuItem22.Text = "Exit";
            this.toolStripMenuItem22.Click += new System.EventHandler(this.ExitApplicationTray);
            // 
            // uTabContents
            // 
            this.uTabContents.CloseButtonLocation = Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None;
            this.uTabContents.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabContents.Location = new System.Drawing.Point(0, 0);
            this.uTabContents.Margin = new System.Windows.Forms.Padding(0);
            this.uTabContents.Name = "uTabContents";
            this.uTabContents.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabContents.Size = new System.Drawing.Size(1023, 576);
            this.uTabContents.TabButtonStyle = Infragistics.Win.UIElementButtonStyle.Flat;
            this.uTabContents.TabCloseButtonVisibility = Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never;
            this.uTabContents.TabIndex = 0;
            this.uTabContents.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.VisualStudio2005;
            this.uTabContents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTabContents_MouseDown);
            this.uTabContents.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.SelectedTabChangedAction);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(2, 21);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1019, 553);
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 86);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1023, 10);
            this.panel5.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripButton1,
            this.toolStripButton7,
            this.toolStripSeparator3,
            this.toolStripButton2,
            this.toolStripSeparator2,
            this.toolStripButton5});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1023, 62);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.toolStripButton3.Size = new System.Drawing.Size(75, 59);
            this.toolStripButton3.Text = "실시간수집";
            this.toolStripButton3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.toolStripButton1.Size = new System.Drawing.Size(63, 59);
            this.toolStripButton1.Text = "태그정보";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(83, 59);
            this.toolStripButton7.Text = "관망해석관리";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 62);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.toolStripButton2.Size = new System.Drawing.Size(63, 59);
            this.toolStripButton2.Text = "경고정보";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 62);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.toolStripButton5.Size = new System.Drawing.Size(63, 59);
            this.toolStripButton5.Text = "서버종료";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.ExitApplication);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem23,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.ToolStripMenuItem5,
            this.toolStripMenuItem6});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1023, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(43, 20);
            this.toolStripMenuItem1.Text = "파일";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem2.Text = "서버종료";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.ExitApplication);
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem24,
            this.toolStripMenuItem25});
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(43, 20);
            this.toolStripMenuItem23.Text = "보기";
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.Checked = true;
            this.toolStripMenuItem24.CheckOnClick = true;
            this.toolStripMenuItem24.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(202, 22);
            this.toolStripMenuItem24.Text = "바로가기메뉴";
            this.toolStripMenuItem24.Click += new System.EventHandler(this.toolStripMenuItem24_Click);
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.CheckOnClick = true;
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(202, 22);
            this.toolStripMenuItem25.Text = "데이터수집 제외 숨기기";
            this.toolStripMenuItem25.Visible = false;
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem13,
            this.toolStripMenuItem12,
            this.iwaterRealtimeToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(103, 20);
            this.toolStripMenuItem3.Text = "실시간수집관리";
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem13.Text = "실시간데이터수집";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.toolStripMenuItem13_Click);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem12.Text = "수집태그정보";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.toolStripMenuItem12_Click);
            // 
            // iwaterRealtimeToolStripMenuItem
            // 
            this.iwaterRealtimeToolStripMenuItem.Name = "iwaterRealtimeToolStripMenuItem";
            this.iwaterRealtimeToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.iwaterRealtimeToolStripMenuItem.Text = "수집데이터조회";
            this.iwaterRealtimeToolStripMenuItem.Click += new System.EventHandler(this.iwaterRealtimeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem16});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(91, 20);
            this.toolStripMenuItem4.Text = "관망해석관리";
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(190, 22);
            this.toolStripMenuItem16.Text = "실시간 관망해석 관리";
            this.toolStripMenuItem16.Click += new System.EventHandler(this.toolStripMenuItem16_Click);
            // 
            // ToolStripMenuItem5
            // 
            this.ToolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dfToolStripMenuItem});
            this.ToolStripMenuItem5.Name = "ToolStripMenuItem5";
            this.ToolStripMenuItem5.Size = new System.Drawing.Size(91, 20);
            this.ToolStripMenuItem5.Text = "수요예측관리";
            // 
            // dfToolStripMenuItem
            // 
            this.dfToolStripMenuItem.Name = "dfToolStripMenuItem";
            this.dfToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.dfToolStripMenuItem.Text = "실시간수요예측관리";
            this.dfToolStripMenuItem.Click += new System.EventHandler(this.dfToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tagImportToolStripMenuItem,
            this.tagDivisionToolStripMenuItem,
            this.SHPNetworkGenerationToolStripMenuItem,
            this.ToolStripMenuItem_SystemManage,
            this.dB설정파일삭제ToolStripMenuItem,
            this.toolStripMenuItem20});
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem6.Text = "시스템관리";
            // 
            // tagImportToolStripMenuItem
            // 
            this.tagImportToolStripMenuItem.Name = "tagImportToolStripMenuItem";
            this.tagImportToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.tagImportToolStripMenuItem.Text = "태그 임포트";
            this.tagImportToolStripMenuItem.Click += new System.EventHandler(this.tagImportToolStripMenuItem_Click);
            // 
            // tagDivisionToolStripMenuItem
            // 
            this.tagDivisionToolStripMenuItem.Name = "tagDivisionToolStripMenuItem";
            this.tagDivisionToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.tagDivisionToolStripMenuItem.Text = "태그 구분 관리";
            this.tagDivisionToolStripMenuItem.Click += new System.EventHandler(this.tagDivisionToolStripMenuItem_Click);
            // 
            // SHPNetworkGenerationToolStripMenuItem
            // 
            this.SHPNetworkGenerationToolStripMenuItem.Name = "SHPNetworkGenerationToolStripMenuItem";
            this.SHPNetworkGenerationToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.SHPNetworkGenerationToolStripMenuItem.Text = "관망네트워크생성";
            this.SHPNetworkGenerationToolStripMenuItem.Click += new System.EventHandler(this.SHPNetworkGenerationToolStripMenuItem_Click);
            // 
            // ToolStripMenuItem_SystemManage
            // 
            this.ToolStripMenuItem_SystemManage.Name = "ToolStripMenuItem_SystemManage";
            this.ToolStripMenuItem_SystemManage.Size = new System.Drawing.Size(170, 22);
            this.ToolStripMenuItem_SystemManage.Text = "시스템관리";
            this.ToolStripMenuItem_SystemManage.Visible = false;
            this.ToolStripMenuItem_SystemManage.Click += new System.EventHandler(this.ToolStripMenuItem_SystemManage_Click);
            // 
            // dB설정파일삭제ToolStripMenuItem
            // 
            this.dB설정파일삭제ToolStripMenuItem.Name = "dB설정파일삭제ToolStripMenuItem";
            this.dB설정파일삭제ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.dB설정파일삭제ToolStripMenuItem.Text = "DB 설정파일 삭제";
            this.dB설정파일삭제ToolStripMenuItem.Click += new System.EventHandler(this.dBConfigDeleteToolStripMenuItem_Click);
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem20.Text = "about";
            this.toolStripMenuItem20.Click += new System.EventHandler(this.toolStripMenuItem20_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 852);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1023, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // contextMenu1
            // 
            this.contextMenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuthisClose,
            this.ToolStripMenuNthisClose,
            this.ToolStripMenuAllClose});
            this.contextMenu1.Name = "contextMenu1";
            this.contextMenu1.Size = new System.Drawing.Size(227, 70);
            // 
            // ToolStripMenuthisClose
            // 
            this.ToolStripMenuthisClose.Name = "ToolStripMenuthisClose";
            this.ToolStripMenuthisClose.Size = new System.Drawing.Size(226, 22);
            this.ToolStripMenuthisClose.Text = "닫기";
            this.ToolStripMenuthisClose.Click += new System.EventHandler(this.ToolStripMenuthisClose_Click);
            // 
            // ToolStripMenuNthisClose
            // 
            this.ToolStripMenuNthisClose.Name = "ToolStripMenuNthisClose";
            this.ToolStripMenuNthisClose.Size = new System.Drawing.Size(226, 22);
            this.ToolStripMenuNthisClose.Text = "이 창을 제외 하고 모두 닫기";
            this.ToolStripMenuNthisClose.Click += new System.EventHandler(this.ToolStripMenuNthisClose_Click);
            // 
            // ToolStripMenuAllClose
            // 
            this.ToolStripMenuAllClose.Name = "ToolStripMenuAllClose";
            this.ToolStripMenuAllClose.Size = new System.Drawing.Size(226, 22);
            this.ToolStripMenuAllClose.Text = "열려있는 창 모두 닫기";
            this.ToolStripMenuAllClose.Click += new System.EventHandler(this.ToolStripMenuAllClose_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 96);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.uTabContents);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(1023, 756);
            this.splitContainer1.SplitterDistance = 576;
            this.splitContainer1.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.griWarningList);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1023, 176);
            this.panel3.TabIndex = 1;
            // 
            // griWarningList
            // 
            this.griWarningList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griWarningList.Location = new System.Drawing.Point(0, 0);
            this.griWarningList.Name = "griWarningList";
            this.griWarningList.Size = new System.Drawing.Size(1023, 176);
            this.griWarningList.TabIndex = 1;
            this.griWarningList.Text = "System Console";
            this.griWarningList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griWarningList_DoubleClickRow);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 874);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "Water-NET Schedule Manager";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GetKeyPress);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClosingEvent);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTabContents)).EndInit();
            this.uTabContents.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenu1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griWarningList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem 파일ToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabContents;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenu1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuthisClose;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuNthisClose;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuAllClose;
        private System.Windows.Forms.ToolStripMenuItem dB설정파일삭제ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem SHPNetworkGenerationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iwaterRealtimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_SystemManage;
        private System.Windows.Forms.ToolStripMenuItem tagImportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tagDivisionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem dfToolStripMenuItem;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid griWarningList;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;




    }
}

