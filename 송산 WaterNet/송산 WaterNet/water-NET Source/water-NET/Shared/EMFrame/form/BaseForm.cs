﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Infragistics.Win.UltraWinGrid;
using EMFrame.manager;
//using ChartFX.WinForms;
using log4net;

namespace EMFrame.form
{
    public partial class BaseForm : Form
    {
        protected static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected bool CloseEnable = true;

        /// <summary>
        /// 차트매니저
        /// </summary>
        //private ChartManager chartManager = null;
        //protected ChartManager ChartManager
        //{
        //    get
        //    {
        //        return this.chartManager;
        //    }
        //}
        public bool isCloseEnable
        {
            get
            {
                return CloseEnable;
            }

            set
            {
                CloseEnable = value;
            }
        }

        /// <summary>
        /// 그리드매니저
        /// </summary>
        private GridManager gridManager = null;
        protected GridManager GridManager
        {
            get
            {
                return this.gridManager;
            }
        }


        private ComponentManager componentManager = new ComponentManager();
        protected ComponentManager ComponentManager
        {
            get
            {
                return this.componentManager;
            }
        }

        public BaseForm()
        {
            InitializeComponent();
            this.Load += new EventHandler(FormLoad_EventHandler);
            this.FormClosed += new FormClosedEventHandler(FormClosed_EventHandler);
        }

        /// <summary>
        /// 폼 로드가 완료 되었을때 이벤트 핸들러
        /// 자식의 모든 컨트롤 객체를 찾아서 공통 작업을 해준다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormLoad_EventHandler(object sender, EventArgs e)
        {
            this.InitializeControl(this.Controls);
        }

        /// <summary>
        /// 폼이 닫혔을떄 이벤트 핸들러
        /// 자식의 모든 컨트롤 객체를 찾아서 공통 작업을 해준다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormClosed_EventHandler(object sender, FormClosedEventArgs e)
        {
            this.RemoveAllEventHandler(this.Controls);
        }

        /// <summary>
        /// 재귀함수임
        /// 모든 컨트롤 객체에서 차트와 그리드를 찾아서 스타일 설정을 해준다
        /// </summary>
        /// <param name="controls"></param>
        private void InitializeControl(Control.ControlCollection controls)
        {
            Control control = null;

            if (controls.Count > 0)
            {
                foreach (Control c in controls)
                {
                    this.InitializeControl(c.Controls);
                }
            }

            control = controls.Owner;
            this.componentManager.InitializeComponent(control);
        }

        /// <summary>
        /// 재귀함수임
        /// 모든 컨트롤 객체에서 이벤트를 삭제해준다
        /// 컨트롤를 상속 받아서 이벤트를 재 구현한 컴포넌트의 경우 해당 타입의 처리가 필요함
        /// </summary>
        /// <param name="controls"></param>
        private void RemoveAllEventHandler(Control.ControlCollection controls)
        {
            Control control = null;

            if (controls.Count > 0)
            {
                foreach (Control c in controls)
                {
                    this.RemoveAllEventHandler(c.Controls);
                }
            }

            control = controls.Owner;

            if (control != null)
            {
                control.DataBindings.Clear();

                FieldInfo[] ff = null;

                if (control is Form)
                {
                    ff = typeof(Form).GetFields(BindingFlags.Static | BindingFlags.NonPublic);
                }

                else
                {
                    ff = typeof(Control).GetFields(BindingFlags.Static | BindingFlags.NonPublic);
                }

                PropertyInfo events = control.GetType().GetProperty("Events", BindingFlags.NonPublic | BindingFlags.Instance);
                EventHandlerList eventHandlerList = (EventHandlerList)events.GetValue(control, null);

                if (ff != null)
                {
                    foreach (FieldInfo fp in ff)
                    {
                        if (fp.FieldType.Equals(typeof(object)) && (fp.Name.IndexOf("Event") != -1 || fp.Name.IndexOf("EVENT") != -1))
                        {
                            if (fp.GetValue(control) != null && eventHandlerList[fp.GetValue(control)] != null)
                            {
                                eventHandlerList.RemoveHandler(fp.GetValue(control), eventHandlerList[fp.GetValue(control)]);
                            }
                        }
                    }
                }
            }
        }
    }
}
