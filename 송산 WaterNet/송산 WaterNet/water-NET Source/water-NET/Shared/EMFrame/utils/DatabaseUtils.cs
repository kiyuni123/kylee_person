﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMFrame.utils
{
    public class DatabaseUtils
    {
        private static DatabaseUtils work = null;
        private static EMFrame.dm.EMapper mapper = null;

        public static DatabaseUtils GetInstance()
        {
            if (work == null)
            {
                work = new DatabaseUtils();
            }
            return work;
        }

        private DatabaseUtils()
        {
            mapper = new EMFrame.dm.EMapper("SVR");
        }

        #region 로그인 관련 : 통합DB서버에 로그인 정보를 저장한다.
        /// <summary>
        /// login정보를 insert한다.
        /// </summary>
        /// <param name="param"></param>
        public void LoginInfo(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("INSERT INTO LOG_INFO (               ");
            oStringBuilder.AppendLine("       USER_ID, CON_MGDPCD, LOGIN_DT, LOGOUT_DT, LOGIN_IP, LOGOUT_GBN, SESSION_DT ) ");
            oStringBuilder.AppendLine("VALUES (");
            oStringBuilder.AppendLine("       :USER_ID, :SGCCD, TO_DATE(:LOGIN_DT,'yyyymmddhh24miss'), TO_DATE('99991231','YYYYMMDD'), :LOGIN_IP, '1', SYSDATE )");

            //EMFrame.dm.EMapper mapper = null;
            try
            {
                //mapper = new EMFrame.dm.EMapper("SVR");

                int i = mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.log.Logger.Error("에러 : " + oException);
                //throw;
            }
            finally
            {
                //mapper.Close();
            }
        }

        /// <summary>
        /// 정상적으로 로그아웃 : logout_dt, logout_gbn을 update한다.
        /// </summary>
        /// <param name="param"></param>
        public void SuccessLogoutInfo(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE LOG_INFO SET                         ");
            oStringBuilder.AppendLine("       LOGOUT_DT = SYSDATE, LOGOUT_GBN = :LOGOUT_GBN ");
            oStringBuilder.AppendLine(" WHERE USER_ID = :USER_ID                   ");
            oStringBuilder.AppendLine("   AND LOGIN_DT = TO_DATE(:LOGIN_DT,'yyyymmddhh24miss')     ");

            //EMFrame.dm.EMapper mapper = null;
            try
            {
                //mapper = new EMFrame.dm.EMapper("SVR");

                mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.log.Logger.Error("에러 : " + oException);
            }
            finally
            {
                //mapper.Close();
            }
        }

        /// <summary>
        /// 클라이언트에서 주기적으로 동작중임을 표시하는 기능으로
        /// Session_dt에 현재 시간을 Update한다.
        /// </summary>
        /// <param name="param"></param>
        public void SessionInfo(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE LOG_INFO SET                         ");
            oStringBuilder.AppendLine("       SESSION_DT = SYSDATE                 ");
            oStringBuilder.AppendLine(" WHERE USER_ID = :USER_ID                   ");
            oStringBuilder.AppendLine("   AND LOGIN_DT = TO_DATE(:LOGIN_DT,'yyyymmddhh24miss')     ");
            oStringBuilder.AppendLine("   AND LOGIN_IP = :LOGIN_IP     ");

            //EMFrame.dm.EMapper mapper = null;
            try
            {
                //mapper = new EMFrame.dm.EMapper("SVR");

                mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.log.Logger.Error("에러 : " + oException);
            }
            finally
            {
                //mapper.Close();
            }
        }

        public void forceLogout_info(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE LOG_INFO SET                         ");
            oStringBuilder.AppendLine("       LOGOUT_DT = SYSDATE                 ");
            oStringBuilder.AppendLine(" WHERE USER_ID = :USER_ID                   ");
            oStringBuilder.AppendLine("   AND LOGIN_DT = TO_DATE(:LOGIN_DT,'yyyymmddhh24miss')     ");
            oStringBuilder.AppendLine("   AND LOGIN_IP = :LOGIN_IP     ");

            //EMFrame.dm.EMapper mapper = null;
            try
            {
                //mapper = new EMFrame.dm.EMapper("SVR");

                mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.log.Logger.Error("에러 : " + oException);
            }
            finally
            {
                //mapper.Close();
            }
        }

        /// <summary>
        /// 로그인ID : 로그정보에 LOGOUT_DATA = 9999-12-31인 레코드를 찾는다.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public System.Data.DataTable IsBeforeLogin(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT LOGIN_DT, LOGOUT_DT, LOGIN_IP        ");
            oStringBuilder.AppendLine("  FROM  LOG_INFO                            ");
            oStringBuilder.AppendLine(" WHERE USER_ID = :USER_ID                   ");
            oStringBuilder.AppendLine("   AND LOGOUT_DT = TO_DATE('99991231','yyyymmdd')     ");

            System.Data.DataTable result = null;
            //EMFrame.dm.EMapper mapper = null;
            try
            {
                //mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.log.Logger.Error("에러 : " + oException);
            }
            finally
            {
                //mapper.Close();
            }

            return result;
        }

        public System.Data.DataTable GetUserMenu(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            System.Data.DataTable result = null;
            //EMFrame.dm.EMapper mapper = null;
            try
            {
                //mapper = new EMFrame.dm.EMapper("SVR");

                oStringBuilder.AppendLine("WITH MENU AS                                                     ");
                oStringBuilder.AppendLine("(SELECT A.MU_ID, A.MU_NM, A.TOP_YN, A.USE_YN, A.REMARK           ");
                oStringBuilder.AppendLine("       ,A.P_MU_ID, A.ORDERNO, B.MENU_AUTH, B.USER_GROUP_ID       ");
                oStringBuilder.AppendLine("  FROM MENU_MASTER A                                             ");
                oStringBuilder.AppendLine("      ,CM_GROUP_MENU B                                           ");
                oStringBuilder.AppendLine(" WHERE A.MU_ID = B.MU_ID AND A.ZONE_GBN = B.ZONE_GBN AND A.ZONE_GBN = :ZONE_GBN ");
                oStringBuilder.AppendLine("   AND B.USER_GROUP_ID = (                 ");
                switch ((string)param["ZONE_GBN"])
                {
                    case "지방":
                        oStringBuilder.AppendLine("    SELECT L_USER_GROUP_ID FROM CM_USER WHERE USER_ID = :USER_ID)  ");
                        break;
                    case "광역":
                        oStringBuilder.AppendLine("    SELECT G_USER_GROUP_ID FROM CM_USER WHERE USER_ID = :USER_ID)  ");
                        break;
                }
                
                oStringBuilder.AppendLine(")                                                                ");
                oStringBuilder.AppendLine("SELECT A.MU_ID                                                   ");
                oStringBuilder.AppendLine("      ,DECODE(LEVEL, 1, A.MU_NM, NULL) MENU1                     ");
                oStringBuilder.AppendLine("      ,DECODE(LEVEL, 2, A.MU_NM, NULL) MENU2                     ");
                oStringBuilder.AppendLine("      ,DECODE(LEVEL, 3, A.MU_NM, NULL) MENU3                     ");
                oStringBuilder.AppendLine("      ,A.TOP_YN                                                  ");
                oStringBuilder.AppendLine("      ,A.USE_YN                                                  ");
                oStringBuilder.AppendLine("      ,A.P_MU_ID                                                 ");
                oStringBuilder.AppendLine("      ,A.ORDERNO                                                 ");
                oStringBuilder.AppendLine("      ,A.MENU_AUTH                                               ");
                oStringBuilder.AppendLine("      ,A.REMARK                                                  ");
                oStringBuilder.AppendLine("      ,LEVEL                                                     ");
                oStringBuilder.AppendLine("  FROM MENU A                                                    ");
                oStringBuilder.AppendLine("  START WITH TOP_YN = '1'                                        ");
                oStringBuilder.AppendLine("  CONNECT BY PRIOR MU_ID = P_MU_ID AND USE_YN = '1'              ");
                oStringBuilder.AppendLine(" ORDER SIBLINGS BY ORDERNO                                       ");


                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
                
            }
            catch (Exception oException)
            {
                EMFrame.log.Logger.Error("에러 : " + oException);
            }
            finally
            {
                //mapper.Close();
            }

            return result;
        }

        /// <summary>
        /// 로그인ID : 로그정보에 LOGOUT_DATA = 9999-12-31인 레코드를 sysdate로 변경한다.
        /// </summary>
        /// <param name="param"></param>
        public void DisableBeforeLogin(System.Collections.Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE LOG_INFO SET                         ");
            oStringBuilder.AppendLine("       LOGOUT_DT = SYSDATE                  ");
            oStringBuilder.AppendLine(" WHERE USER_ID = :USER_ID                   ");
            oStringBuilder.AppendLine("   AND LOGOUT_DT = TO_DATE('99991231','yyyymmdd')     ");

            //EMFrame.dm.EMapper mapper = null;
            try
            {
                //mapper = new EMFrame.dm.EMapper("SVR");

                mapper.ExecuteScript(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.log.Logger.Error("에러 : " + oException);
            }
            finally
            {
                //mapper.Close();
            }
        }

        #endregion 로그인 관련
    }

}
