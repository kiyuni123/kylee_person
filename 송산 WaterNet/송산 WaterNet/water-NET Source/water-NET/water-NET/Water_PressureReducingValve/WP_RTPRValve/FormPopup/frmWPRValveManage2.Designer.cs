﻿namespace WaterNet.WP_RTPRValve.FormPopup
{
    partial class frmWPRValveManage2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.PicFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cboMANOMETER_NO_B = new System.Windows.Forms.ComboBox();
            this.cboTAGNAME_B = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.cboMANOMETER_NO = new System.Windows.Forms.ComboBox();
            this.cboTAGNAME = new System.Windows.Forms.ComboBox();
            this.cboVALVE_NO = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label28 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtPR_VALUE10 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE3 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE8 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE9 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE2 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE7 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE6 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE5 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE4 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE1 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtALERT_LIMIT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboDELAY_TIME = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cboVALVE_TYPE = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboBLOCK_CODE = new System.Windows.Forms.ComboBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.uGridPopup = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            this.ucSH10 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM10 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH10 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM10 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSH9 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM9 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH9 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM9 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSH8 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM8 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH8 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM8 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSH7 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM7 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH7 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM7 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSH6 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM6 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH6 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM6 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSH5 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM5 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH5 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM5 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSH4 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM4 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH4 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM4 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSH3 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM3 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH3 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM3 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSH2 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM2 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH2 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM2 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEH1 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucEM1 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSM1 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            this.ucSH1 = new WaterNet.WP_RTPRValve.ucTimeCombo();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 676);
            this.picFrLeft.TabIndex = 106;
            this.picFrLeft.TabStop = false;
            // 
            // PicFrRight
            // 
            this.PicFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.PicFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicFrRight.Location = new System.Drawing.Point(675, 4);
            this.PicFrRight.Name = "PicFrRight";
            this.PicFrRight.Size = new System.Drawing.Size(4, 676);
            this.PicFrRight.TabIndex = 107;
            this.PicFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 680);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(679, 4);
            this.picFrBottom.TabIndex = 105;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(679, 4);
            this.picFrTop.TabIndex = 104;
            this.picFrTop.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(671, 676);
            this.panel1.TabIndex = 131;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.uGridPopup);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 56);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(671, 620);
            this.panel4.TabIndex = 106;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.Controls.Add(this.cboMANOMETER_NO_B);
            this.panel7.Controls.Add(this.cboTAGNAME_B);
            this.panel7.Controls.Add(this.label29);
            this.panel7.Controls.Add(this.label30);
            this.panel7.Controls.Add(this.cboMANOMETER_NO);
            this.panel7.Controls.Add(this.cboTAGNAME);
            this.panel7.Controls.Add(this.cboVALVE_NO);
            this.panel7.Controls.Add(this.panel3);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.txtALERT_LIMIT);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.cboDELAY_TIME);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.cboVALVE_TYPE);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Controls.Add(this.cboBLOCK_CODE);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 176);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(671, 444);
            this.panel7.TabIndex = 148;
            // 
            // cboMANOMETER_NO_B
            // 
            this.cboMANOMETER_NO_B.FormattingEnabled = true;
            this.cboMANOMETER_NO_B.Location = new System.Drawing.Point(98, 53);
            this.cboMANOMETER_NO_B.Name = "cboMANOMETER_NO_B";
            this.cboMANOMETER_NO_B.Size = new System.Drawing.Size(140, 19);
            this.cboMANOMETER_NO_B.TabIndex = 198;
            // 
            // cboTAGNAME_B
            // 
            this.cboTAGNAME_B.FormattingEnabled = true;
            this.cboTAGNAME_B.Location = new System.Drawing.Point(394, 53);
            this.cboTAGNAME_B.Name = "cboTAGNAME_B";
            this.cboTAGNAME_B.Size = new System.Drawing.Size(239, 19);
            this.cboTAGNAME_B.TabIndex = 197;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.Location = new System.Drawing.Point(300, 57);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(92, 12);
            this.label29.TabIndex = 196;
            this.label29.Text = "태그명(전단) :";
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.Location = new System.Drawing.Point(1, 57);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(92, 12);
            this.label30.TabIndex = 195;
            this.label30.Text = "계측기(전단) :";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboMANOMETER_NO
            // 
            this.cboMANOMETER_NO.FormattingEnabled = true;
            this.cboMANOMETER_NO.Location = new System.Drawing.Point(98, 78);
            this.cboMANOMETER_NO.Name = "cboMANOMETER_NO";
            this.cboMANOMETER_NO.Size = new System.Drawing.Size(140, 19);
            this.cboMANOMETER_NO.TabIndex = 194;
            // 
            // cboTAGNAME
            // 
            this.cboTAGNAME.FormattingEnabled = true;
            this.cboTAGNAME.Location = new System.Drawing.Point(394, 77);
            this.cboTAGNAME.Name = "cboTAGNAME";
            this.cboTAGNAME.Size = new System.Drawing.Size(239, 19);
            this.cboTAGNAME.TabIndex = 193;
            // 
            // cboVALVE_NO
            // 
            this.cboVALVE_NO.FormattingEnabled = true;
            this.cboVALVE_NO.Location = new System.Drawing.Point(98, 28);
            this.cboVALVE_NO.Name = "cboVALVE_NO";
            this.cboVALVE_NO.Size = new System.Drawing.Size(140, 19);
            this.cboVALVE_NO.TabIndex = 192;
            this.cboVALVE_NO.SelectedIndexChanged += new System.EventHandler(this.cboVALVE_NO_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 102);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(671, 342);
            this.panel3.TabIndex = 186;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 198F));
            this.tableLayoutPanel2.Controls.Add(this.label28, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1, 31);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(664, 26);
            this.tableLayoutPanel2.TabIndex = 207;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.Location = new System.Drawing.Point(271, 2);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(196, 22);
            this.label28.TabIndex = 214;
            this.label28.Text = "종료시간(시:분)";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(475, 2);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(192, 22);
            this.label15.TabIndex = 213;
            this.label15.Text = "감압설정치(㎏/㎠)";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(5, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 22);
            this.label5.TabIndex = 212;
            this.label5.Text = "NO";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(67, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 22);
            this.label2.TabIndex = 211;
            this.label2.Text = "시작시간(시:분)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayoutPanel1.Controls.Add(this.ucSH10, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.ucSM10, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.ucEH10, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.ucEM10, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.ucSH9, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.ucSM9, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.ucEH9, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.ucEM9, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.ucSH8, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.ucSM8, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.ucEH8, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.ucEM8, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.ucSH7, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.ucSM7, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.ucEH7, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.ucEM7, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.ucSH6, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.ucSM6, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.ucEH6, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.ucEM6, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.ucSH5, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.ucSM5, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.ucEH5, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.ucEM5, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.ucSH4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.ucSM4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.ucEH4, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.ucEM4, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.ucSH3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.ucSM3, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.ucEH3, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.ucEM3, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.ucSH2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ucSM2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.ucEH2, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.ucEM2, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.ucEH1, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.ucEM1, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.ucSM1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.ucSH1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE10, 5, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE3, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE8, 5, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE9, 5, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE2, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE7, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE6, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE5, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE4, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE1, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label27, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label23, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label21, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label20, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label19, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 58);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(667, 281);
            this.tableLayoutPanel1.TabIndex = 206;
            // 
            // txtPR_VALUE10
            // 
            this.txtPR_VALUE10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE10.Location = new System.Drawing.Point(475, 257);
            this.txtPR_VALUE10.Name = "txtPR_VALUE10";
            this.txtPR_VALUE10.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE10.TabIndex = 229;
            this.txtPR_VALUE10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE3
            // 
            this.txtPR_VALUE3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE3.Location = new System.Drawing.Point(475, 61);
            this.txtPR_VALUE3.Name = "txtPR_VALUE3";
            this.txtPR_VALUE3.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE3.TabIndex = 228;
            this.txtPR_VALUE3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE8
            // 
            this.txtPR_VALUE8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE8.Location = new System.Drawing.Point(475, 201);
            this.txtPR_VALUE8.Name = "txtPR_VALUE8";
            this.txtPR_VALUE8.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE8.TabIndex = 227;
            this.txtPR_VALUE8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE9
            // 
            this.txtPR_VALUE9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE9.Location = new System.Drawing.Point(475, 229);
            this.txtPR_VALUE9.Name = "txtPR_VALUE9";
            this.txtPR_VALUE9.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE9.TabIndex = 226;
            this.txtPR_VALUE9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE2
            // 
            this.txtPR_VALUE2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE2.Location = new System.Drawing.Point(475, 33);
            this.txtPR_VALUE2.Name = "txtPR_VALUE2";
            this.txtPR_VALUE2.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE2.TabIndex = 225;
            this.txtPR_VALUE2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE7
            // 
            this.txtPR_VALUE7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE7.Location = new System.Drawing.Point(475, 173);
            this.txtPR_VALUE7.Name = "txtPR_VALUE7";
            this.txtPR_VALUE7.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE7.TabIndex = 224;
            this.txtPR_VALUE7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE6
            // 
            this.txtPR_VALUE6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE6.Location = new System.Drawing.Point(475, 145);
            this.txtPR_VALUE6.Name = "txtPR_VALUE6";
            this.txtPR_VALUE6.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE6.TabIndex = 223;
            this.txtPR_VALUE6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE5
            // 
            this.txtPR_VALUE5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE5.Location = new System.Drawing.Point(475, 117);
            this.txtPR_VALUE5.Name = "txtPR_VALUE5";
            this.txtPR_VALUE5.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE5.TabIndex = 222;
            this.txtPR_VALUE5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE4
            // 
            this.txtPR_VALUE4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE4.Location = new System.Drawing.Point(475, 89);
            this.txtPR_VALUE4.Name = "txtPR_VALUE4";
            this.txtPR_VALUE4.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE4.TabIndex = 221;
            this.txtPR_VALUE4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE1
            // 
            this.txtPR_VALUE1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE1.Location = new System.Drawing.Point(475, 5);
            this.txtPR_VALUE1.Name = "txtPR_VALUE1";
            this.txtPR_VALUE1.Size = new System.Drawing.Size(187, 20);
            this.txtPR_VALUE1.TabIndex = 220;
            this.txtPR_VALUE1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(5, 198);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 26);
            this.label27.TabIndex = 219;
            this.label27.Text = "8";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(5, 58);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 26);
            this.label26.TabIndex = 218;
            this.label26.Text = "3";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(5, 254);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 26);
            this.label25.TabIndex = 217;
            this.label25.Text = "10";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(5, 226);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 26);
            this.label24.TabIndex = 216;
            this.label24.Text = "9";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(5, 170);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 26);
            this.label23.TabIndex = 215;
            this.label23.Text = "7";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(5, 30);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 26);
            this.label22.TabIndex = 214;
            this.label22.Text = "2";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(5, 142);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 26);
            this.label21.TabIndex = 213;
            this.label21.Text = "6";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(5, 114);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 26);
            this.label20.TabIndex = 212;
            this.label20.Text = "5";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(5, 86);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 26);
            this.label19.TabIndex = 211;
            this.label19.Text = "4";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(5, 2);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 26);
            this.label18.TabIndex = 210;
            this.label18.Text = "1";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label13);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(671, 28);
            this.panel5.TabIndex = 183;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(4, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(465, 12);
            this.label13.TabIndex = 164;
            this.label13.Text = "시간제어 감압밸브 운영 정보 (1일 기준 00시 00분 부터 24시 00분 까지 설정)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(615, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 12);
            this.label12.TabIndex = 185;
            this.label12.Text = "(분)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(382, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 12);
            this.label11.TabIndex = 184;
            this.label11.Text = "±";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(300, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 12);
            this.label6.TabIndex = 182;
            this.label6.Text = "태그명(후단) :";
            // 
            // txtALERT_LIMIT
            // 
            this.txtALERT_LIMIT.Location = new System.Drawing.Point(399, 25);
            this.txtALERT_LIMIT.Name = "txtALERT_LIMIT";
            this.txtALERT_LIMIT.Size = new System.Drawing.Size(43, 20);
            this.txtALERT_LIMIT.TabIndex = 180;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(300, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 12);
            this.label4.TabIndex = 179;
            this.label4.Text = "경보범위 :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(481, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 12);
            this.label1.TabIndex = 176;
            this.label1.Text = "시간제어 :";
            // 
            // cboDELAY_TIME
            // 
            this.cboDELAY_TIME.FormattingEnabled = true;
            this.cboDELAY_TIME.Location = new System.Drawing.Point(554, 25);
            this.cboDELAY_TIME.Name = "cboDELAY_TIME";
            this.cboDELAY_TIME.Size = new System.Drawing.Size(61, 19);
            this.cboDELAY_TIME.TabIndex = 175;
            this.cboDELAY_TIME.SelectedIndexChanged += new System.EventHandler(this.cboDELAY_TIME_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(12, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 12);
            this.label10.TabIndex = 173;
            this.label10.Text = "감압밸브ID :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(1, 82);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 12);
            this.label14.TabIndex = 166;
            this.label14.Text = "계측기(후단) :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(300, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 12);
            this.label16.TabIndex = 165;
            this.label16.Text = "감압밸브타입 :";
            // 
            // cboVALVE_TYPE
            // 
            this.cboVALVE_TYPE.FormattingEnabled = true;
            this.cboVALVE_TYPE.Location = new System.Drawing.Point(399, 4);
            this.cboVALVE_TYPE.Name = "cboVALVE_TYPE";
            this.cboVALVE_TYPE.Size = new System.Drawing.Size(140, 19);
            this.cboVALVE_TYPE.TabIndex = 164;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(38, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 12);
            this.label17.TabIndex = 163;
            this.label17.Text = "소블록 :";
            // 
            // cboBLOCK_CODE
            // 
            this.cboBLOCK_CODE.FormattingEnabled = true;
            this.cboBLOCK_CODE.Location = new System.Drawing.Point(98, 4);
            this.cboBLOCK_CODE.Name = "cboBLOCK_CODE";
            this.cboBLOCK_CODE.Size = new System.Drawing.Size(140, 19);
            this.cboBLOCK_CODE.TabIndex = 162;
            this.cboBLOCK_CODE.SelectedIndexChanged += new System.EventHandler(this.cboBLOCK_CODE_SelectedIndexChanged);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Gold;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Location = new System.Drawing.Point(0, 172);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(671, 4);
            this.pictureBox3.TabIndex = 149;
            this.pictureBox3.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.btnNew);
            this.panel6.Controls.Add(this.btnSave);
            this.panel6.Controls.Add(this.btnDelete);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 144);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(671, 28);
            this.panel6.TabIndex = 147;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(4, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 12);
            this.label3.TabIndex = 164;
            this.label3.Text = "시간제어 감압밸브 감시 관리";
            // 
            // btnNew
            // 
            this.btnNew.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnNew.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Clear;
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNew.Location = new System.Drawing.Point(387, 0);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 26);
            this.btnNew.TabIndex = 159;
            this.btnNew.Text = "초기화";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSave.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(477, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 26);
            this.btnSave.TabIndex = 158;
            this.btnSave.Text = "지점 등록";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Delete;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.Location = new System.Drawing.Point(567, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 26);
            this.btnDelete.TabIndex = 157;
            this.btnDelete.Text = "지점 삭제";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 140);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(671, 4);
            this.pictureBox2.TabIndex = 146;
            this.pictureBox2.TabStop = false;
            // 
            // uGridPopup
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPopup.DisplayLayout.Appearance = appearance1;
            this.uGridPopup.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPopup.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPopup.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridPopup.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPopup.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridPopup.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPopup.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPopup.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPopup.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridPopup.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPopup.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPopup.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridPopup.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPopup.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridPopup.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridPopup.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPopup.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridPopup.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridPopup.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPopup.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridPopup.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPopup.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPopup.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPopup.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridPopup.Location = new System.Drawing.Point(0, 0);
            this.uGridPopup.Name = "uGridPopup";
            this.uGridPopup.Size = new System.Drawing.Size(671, 140);
            this.uGridPopup.TabIndex = 144;
            this.uGridPopup.Text = "ultraGrid1";
            this.uGridPopup.Click += new System.EventHandler(this.uGridPopup_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(671, 4);
            this.pictureBox1.TabIndex = 105;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnQuery);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.cboSBlock);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cboMBlock);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cboLBlock);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(671, 52);
            this.panel2.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Close2;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.Location = new System.Drawing.Point(564, 22);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 26);
            this.btnClose.TabIndex = 154;
            this.btnClose.Text = "닫기";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Query;
            this.btnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery.Location = new System.Drawing.Point(473, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(90, 26);
            this.btnQuery.TabIndex = 153;
            this.btnQuery.Text = "지점 조회";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(416, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 12);
            this.label9.TabIndex = 148;
            this.label9.Text = "소블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(476, 3);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(140, 19);
            this.cboSBlock.TabIndex = 147;
            this.cboSBlock.SelectedIndexChanged += new System.EventHandler(this.cboSBlock_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(210, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 146;
            this.label8.Text = "중블록 :";
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(270, 3);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 19);
            this.cboMBlock.TabIndex = 145;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(4, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 144;
            this.label7.Text = "대블록 :";
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(64, 3);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 19);
            this.cboLBlock.TabIndex = 143;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // ucSH10
            // 
            this.ucSH10.Location = new System.Drawing.Point(67, 257);
            this.ucSH10.Name = "ucSH10";
            this.ucSH10.Size = new System.Drawing.Size(94, 20);
            this.ucSH10.TabIndex = 307;
            // 
            // ucSM10
            // 
            this.ucSM10.Location = new System.Drawing.Point(169, 257);
            this.ucSM10.Name = "ucSM10";
            this.ucSM10.Size = new System.Drawing.Size(94, 20);
            this.ucSM10.TabIndex = 306;
            this.ucSM10.ucDataType = "분";
            this.ucSM10.ucText = "00";
            // 
            // ucEH10
            // 
            this.ucEH10.Location = new System.Drawing.Point(271, 257);
            this.ucEH10.Name = "ucEH10";
            this.ucEH10.Size = new System.Drawing.Size(94, 20);
            this.ucEH10.TabIndex = 305;
            // 
            // ucEM10
            // 
            this.ucEM10.Location = new System.Drawing.Point(373, 257);
            this.ucEM10.Name = "ucEM10";
            this.ucEM10.Size = new System.Drawing.Size(94, 20);
            this.ucEM10.TabIndex = 304;
            this.ucEM10.ucDataType = "분";
            this.ucEM10.ucText = "00";
            // 
            // ucSH9
            // 
            this.ucSH9.Location = new System.Drawing.Point(67, 229);
            this.ucSH9.Name = "ucSH9";
            this.ucSH9.Size = new System.Drawing.Size(94, 20);
            this.ucSH9.TabIndex = 303;
            // 
            // ucSM9
            // 
            this.ucSM9.Location = new System.Drawing.Point(169, 229);
            this.ucSM9.Name = "ucSM9";
            this.ucSM9.Size = new System.Drawing.Size(94, 20);
            this.ucSM9.TabIndex = 302;
            this.ucSM9.ucDataType = "분";
            this.ucSM9.ucText = "00";
            // 
            // ucEH9
            // 
            this.ucEH9.Location = new System.Drawing.Point(271, 229);
            this.ucEH9.Name = "ucEH9";
            this.ucEH9.Size = new System.Drawing.Size(94, 20);
            this.ucEH9.TabIndex = 301;
            // 
            // ucEM9
            // 
            this.ucEM9.Location = new System.Drawing.Point(373, 229);
            this.ucEM9.Name = "ucEM9";
            this.ucEM9.Size = new System.Drawing.Size(94, 20);
            this.ucEM9.TabIndex = 300;
            this.ucEM9.ucDataType = "분";
            this.ucEM9.ucText = "00";
            // 
            // ucSH8
            // 
            this.ucSH8.Location = new System.Drawing.Point(67, 201);
            this.ucSH8.Name = "ucSH8";
            this.ucSH8.Size = new System.Drawing.Size(94, 20);
            this.ucSH8.TabIndex = 299;
            // 
            // ucSM8
            // 
            this.ucSM8.Location = new System.Drawing.Point(169, 201);
            this.ucSM8.Name = "ucSM8";
            this.ucSM8.Size = new System.Drawing.Size(94, 20);
            this.ucSM8.TabIndex = 298;
            this.ucSM8.ucDataType = "분";
            this.ucSM8.ucText = "00";
            // 
            // ucEH8
            // 
            this.ucEH8.Location = new System.Drawing.Point(271, 201);
            this.ucEH8.Name = "ucEH8";
            this.ucEH8.Size = new System.Drawing.Size(94, 20);
            this.ucEH8.TabIndex = 297;
            // 
            // ucEM8
            // 
            this.ucEM8.Location = new System.Drawing.Point(373, 201);
            this.ucEM8.Name = "ucEM8";
            this.ucEM8.Size = new System.Drawing.Size(94, 20);
            this.ucEM8.TabIndex = 296;
            this.ucEM8.ucDataType = "분";
            this.ucEM8.ucText = "00";
            // 
            // ucSH7
            // 
            this.ucSH7.Location = new System.Drawing.Point(67, 173);
            this.ucSH7.Name = "ucSH7";
            this.ucSH7.Size = new System.Drawing.Size(94, 20);
            this.ucSH7.TabIndex = 295;
            // 
            // ucSM7
            // 
            this.ucSM7.Location = new System.Drawing.Point(169, 173);
            this.ucSM7.Name = "ucSM7";
            this.ucSM7.Size = new System.Drawing.Size(94, 20);
            this.ucSM7.TabIndex = 294;
            this.ucSM7.ucDataType = "분";
            this.ucSM7.ucText = "00";
            // 
            // ucEH7
            // 
            this.ucEH7.Location = new System.Drawing.Point(271, 173);
            this.ucEH7.Name = "ucEH7";
            this.ucEH7.Size = new System.Drawing.Size(94, 20);
            this.ucEH7.TabIndex = 293;
            // 
            // ucEM7
            // 
            this.ucEM7.Location = new System.Drawing.Point(373, 173);
            this.ucEM7.Name = "ucEM7";
            this.ucEM7.Size = new System.Drawing.Size(94, 20);
            this.ucEM7.TabIndex = 292;
            this.ucEM7.ucDataType = "분";
            this.ucEM7.ucText = "00";
            // 
            // ucSH6
            // 
            this.ucSH6.Location = new System.Drawing.Point(67, 145);
            this.ucSH6.Name = "ucSH6";
            this.ucSH6.Size = new System.Drawing.Size(94, 20);
            this.ucSH6.TabIndex = 291;
            // 
            // ucSM6
            // 
            this.ucSM6.Location = new System.Drawing.Point(169, 145);
            this.ucSM6.Name = "ucSM6";
            this.ucSM6.Size = new System.Drawing.Size(94, 20);
            this.ucSM6.TabIndex = 290;
            this.ucSM6.ucDataType = "분";
            this.ucSM6.ucText = "00";
            // 
            // ucEH6
            // 
            this.ucEH6.Location = new System.Drawing.Point(271, 145);
            this.ucEH6.Name = "ucEH6";
            this.ucEH6.Size = new System.Drawing.Size(94, 20);
            this.ucEH6.TabIndex = 289;
            // 
            // ucEM6
            // 
            this.ucEM6.Location = new System.Drawing.Point(373, 145);
            this.ucEM6.Name = "ucEM6";
            this.ucEM6.Size = new System.Drawing.Size(94, 20);
            this.ucEM6.TabIndex = 288;
            this.ucEM6.ucDataType = "분";
            this.ucEM6.ucText = "00";
            // 
            // ucSH5
            // 
            this.ucSH5.Location = new System.Drawing.Point(67, 117);
            this.ucSH5.Name = "ucSH5";
            this.ucSH5.Size = new System.Drawing.Size(94, 20);
            this.ucSH5.TabIndex = 287;
            // 
            // ucSM5
            // 
            this.ucSM5.Location = new System.Drawing.Point(169, 117);
            this.ucSM5.Name = "ucSM5";
            this.ucSM5.Size = new System.Drawing.Size(94, 20);
            this.ucSM5.TabIndex = 286;
            this.ucSM5.ucDataType = "분";
            this.ucSM5.ucText = "00";
            // 
            // ucEH5
            // 
            this.ucEH5.Location = new System.Drawing.Point(271, 117);
            this.ucEH5.Name = "ucEH5";
            this.ucEH5.Size = new System.Drawing.Size(94, 20);
            this.ucEH5.TabIndex = 285;
            // 
            // ucEM5
            // 
            this.ucEM5.Location = new System.Drawing.Point(373, 117);
            this.ucEM5.Name = "ucEM5";
            this.ucEM5.Size = new System.Drawing.Size(94, 20);
            this.ucEM5.TabIndex = 284;
            this.ucEM5.ucDataType = "분";
            this.ucEM5.ucText = "00";
            // 
            // ucSH4
            // 
            this.ucSH4.Location = new System.Drawing.Point(67, 89);
            this.ucSH4.Name = "ucSH4";
            this.ucSH4.Size = new System.Drawing.Size(94, 20);
            this.ucSH4.TabIndex = 283;
            // 
            // ucSM4
            // 
            this.ucSM4.Location = new System.Drawing.Point(169, 89);
            this.ucSM4.Name = "ucSM4";
            this.ucSM4.Size = new System.Drawing.Size(94, 20);
            this.ucSM4.TabIndex = 282;
            this.ucSM4.ucDataType = "분";
            this.ucSM4.ucText = "00";
            // 
            // ucEH4
            // 
            this.ucEH4.Location = new System.Drawing.Point(271, 89);
            this.ucEH4.Name = "ucEH4";
            this.ucEH4.Size = new System.Drawing.Size(94, 20);
            this.ucEH4.TabIndex = 281;
            // 
            // ucEM4
            // 
            this.ucEM4.Location = new System.Drawing.Point(373, 89);
            this.ucEM4.Name = "ucEM4";
            this.ucEM4.Size = new System.Drawing.Size(94, 20);
            this.ucEM4.TabIndex = 280;
            this.ucEM4.ucDataType = "분";
            this.ucEM4.ucText = "00";
            // 
            // ucSH3
            // 
            this.ucSH3.Location = new System.Drawing.Point(67, 61);
            this.ucSH3.Name = "ucSH3";
            this.ucSH3.Size = new System.Drawing.Size(94, 20);
            this.ucSH3.TabIndex = 279;
            // 
            // ucSM3
            // 
            this.ucSM3.Location = new System.Drawing.Point(169, 61);
            this.ucSM3.Name = "ucSM3";
            this.ucSM3.Size = new System.Drawing.Size(94, 20);
            this.ucSM3.TabIndex = 278;
            this.ucSM3.ucDataType = "분";
            this.ucSM3.ucText = "00";
            // 
            // ucEH3
            // 
            this.ucEH3.Location = new System.Drawing.Point(271, 61);
            this.ucEH3.Name = "ucEH3";
            this.ucEH3.Size = new System.Drawing.Size(94, 20);
            this.ucEH3.TabIndex = 277;
            // 
            // ucEM3
            // 
            this.ucEM3.Location = new System.Drawing.Point(373, 61);
            this.ucEM3.Name = "ucEM3";
            this.ucEM3.Size = new System.Drawing.Size(94, 20);
            this.ucEM3.TabIndex = 276;
            this.ucEM3.ucDataType = "분";
            this.ucEM3.ucText = "00";
            // 
            // ucSH2
            // 
            this.ucSH2.Location = new System.Drawing.Point(67, 33);
            this.ucSH2.Name = "ucSH2";
            this.ucSH2.Size = new System.Drawing.Size(94, 20);
            this.ucSH2.TabIndex = 275;
            // 
            // ucSM2
            // 
            this.ucSM2.Location = new System.Drawing.Point(169, 33);
            this.ucSM2.Name = "ucSM2";
            this.ucSM2.Size = new System.Drawing.Size(94, 20);
            this.ucSM2.TabIndex = 274;
            this.ucSM2.ucDataType = "분";
            this.ucSM2.ucText = "00";
            // 
            // ucEH2
            // 
            this.ucEH2.Location = new System.Drawing.Point(271, 33);
            this.ucEH2.Name = "ucEH2";
            this.ucEH2.Size = new System.Drawing.Size(94, 20);
            this.ucEH2.TabIndex = 273;
            // 
            // ucEM2
            // 
            this.ucEM2.Location = new System.Drawing.Point(373, 33);
            this.ucEM2.Name = "ucEM2";
            this.ucEM2.Size = new System.Drawing.Size(94, 20);
            this.ucEM2.TabIndex = 272;
            this.ucEM2.ucDataType = "분";
            this.ucEM2.ucText = "00";
            // 
            // ucEH1
            // 
            this.ucEH1.Location = new System.Drawing.Point(271, 5);
            this.ucEH1.Name = "ucEH1";
            this.ucEH1.Size = new System.Drawing.Size(94, 20);
            this.ucEH1.TabIndex = 271;
            // 
            // ucEM1
            // 
            this.ucEM1.Location = new System.Drawing.Point(373, 5);
            this.ucEM1.Name = "ucEM1";
            this.ucEM1.Size = new System.Drawing.Size(94, 20);
            this.ucEM1.TabIndex = 270;
            this.ucEM1.ucDataType = "분";
            this.ucEM1.ucText = "00";
            // 
            // ucSM1
            // 
            this.ucSM1.Location = new System.Drawing.Point(169, 5);
            this.ucSM1.Name = "ucSM1";
            this.ucSM1.Size = new System.Drawing.Size(94, 20);
            this.ucSM1.TabIndex = 166;
            this.ucSM1.ucDataType = "분";
            this.ucSM1.ucText = "00";
            // 
            // ucSH1
            // 
            this.ucSH1.Location = new System.Drawing.Point(67, 5);
            this.ucSH1.Name = "ucSH1";
            this.ucSH1.Size = new System.Drawing.Size(94, 20);
            this.ucSH1.TabIndex = 165;
            // 
            // frmWPRValveManage2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 11F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 684);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.PicFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmWPRValveManage2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "시간제어 감압밸브 감시 관리";
            this.Load += new System.EventHandler(this.frmWPRValveManage2_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWPRValveManage2_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox PicFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboLBlock;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnClose;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPopup;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboVALVE_TYPE;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cboBLOCK_CODE;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboDELAY_TIME;
        private System.Windows.Forms.TextBox txtALERT_LIMIT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPR_VALUE10;
        private System.Windows.Forms.TextBox txtPR_VALUE3;
        private System.Windows.Forms.TextBox txtPR_VALUE8;
        private System.Windows.Forms.TextBox txtPR_VALUE9;
        private System.Windows.Forms.TextBox txtPR_VALUE2;
        private System.Windows.Forms.TextBox txtPR_VALUE7;
        private System.Windows.Forms.TextBox txtPR_VALUE6;
        private System.Windows.Forms.TextBox txtPR_VALUE5;
        private System.Windows.Forms.TextBox txtPR_VALUE4;
        private System.Windows.Forms.TextBox txtPR_VALUE1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboMANOMETER_NO;
        private System.Windows.Forms.ComboBox cboTAGNAME;
        private System.Windows.Forms.ComboBox cboVALVE_NO;
        private ucTimeCombo ucSH1;
        private ucTimeCombo ucSM1;
        private ucTimeCombo ucEH1;
        private ucTimeCombo ucEM1;
        private ucTimeCombo ucSH10;
        private ucTimeCombo ucSM10;
        private ucTimeCombo ucEH10;
        private ucTimeCombo ucEM10;
        private ucTimeCombo ucSH9;
        private ucTimeCombo ucSM9;
        private ucTimeCombo ucEH9;
        private ucTimeCombo ucEM9;
        private ucTimeCombo ucSH8;
        private ucTimeCombo ucSM8;
        private ucTimeCombo ucEH8;
        private ucTimeCombo ucEM8;
        private ucTimeCombo ucSH7;
        private ucTimeCombo ucSM7;
        private ucTimeCombo ucEH7;
        private ucTimeCombo ucEM7;
        private ucTimeCombo ucSH6;
        private ucTimeCombo ucSM6;
        private ucTimeCombo ucEH6;
        private ucTimeCombo ucEM6;
        private ucTimeCombo ucSH5;
        private ucTimeCombo ucSM5;
        private ucTimeCombo ucEH5;
        private ucTimeCombo ucEM5;
        private ucTimeCombo ucSH4;
        private ucTimeCombo ucSM4;
        private ucTimeCombo ucEH4;
        private ucTimeCombo ucEM4;
        private ucTimeCombo ucSH3;
        private ucTimeCombo ucSM3;
        private ucTimeCombo ucEH3;
        private ucTimeCombo ucEM3;
        private ucTimeCombo ucSH2;
        private ucTimeCombo ucSM2;
        private ucTimeCombo ucEH2;
        private ucTimeCombo ucEM2;
        private System.Windows.Forms.ComboBox cboMANOMETER_NO_B;
        private System.Windows.Forms.ComboBox cboTAGNAME_B;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
    }
}