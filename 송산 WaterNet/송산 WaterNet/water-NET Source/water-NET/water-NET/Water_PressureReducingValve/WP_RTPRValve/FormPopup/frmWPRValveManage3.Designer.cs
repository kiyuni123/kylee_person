﻿namespace WaterNet.WP_RTPRValve.FormPopup
{
    partial class frmWPRValveManage3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cboMANOMETER_NO_B = new System.Windows.Forms.ComboBox();
            this.cboTAGNAME_B = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.cboMANOMETER_NO_F = new System.Windows.Forms.ComboBox();
            this.cboTAGNAME_F = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.cboMANOMETER_NO = new System.Windows.Forms.ComboBox();
            this.cboTAGNAME = new System.Windows.Forms.ComboBox();
            this.cboVALVE_NO = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label28 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtEND_OPTION10 = new System.Windows.Forms.TextBox();
            this.txtEND_OPTION9 = new System.Windows.Forms.TextBox();
            this.txtEND_OPTION8 = new System.Windows.Forms.TextBox();
            this.txtEND_OPTION7 = new System.Windows.Forms.TextBox();
            this.txtEND_OPTION6 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION10 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION9 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION8 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION7 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION6 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION5 = new System.Windows.Forms.TextBox();
            this.txtEND_OPTION5 = new System.Windows.Forms.TextBox();
            this.txtEND_OPTION4 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION4 = new System.Windows.Forms.TextBox();
            this.txtEND_OPTION3 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION3 = new System.Windows.Forms.TextBox();
            this.txtEND_OPTION2 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION2 = new System.Windows.Forms.TextBox();
            this.txtSTART_OPTION1 = new System.Windows.Forms.TextBox();
            this.txtEND_OPTION1 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE10 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE3 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE8 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE9 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE2 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE7 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE6 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE5 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE4 = new System.Windows.Forms.TextBox();
            this.txtPR_VALUE1 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtALERT_LIMIT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboDELAY_TIME = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cboVALVE_TYPE = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboBLOCK_CODE = new System.Windows.Forms.ComboBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.uGridPopup = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.PicFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(675, 698);
            this.panel1.TabIndex = 131;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.uGridPopup);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 56);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(675, 642);
            this.panel4.TabIndex = 106;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.Controls.Add(this.cboMANOMETER_NO_B);
            this.panel7.Controls.Add(this.cboTAGNAME_B);
            this.panel7.Controls.Add(this.label31);
            this.panel7.Controls.Add(this.label32);
            this.panel7.Controls.Add(this.cboMANOMETER_NO_F);
            this.panel7.Controls.Add(this.cboTAGNAME_F);
            this.panel7.Controls.Add(this.label29);
            this.panel7.Controls.Add(this.label30);
            this.panel7.Controls.Add(this.cboMANOMETER_NO);
            this.panel7.Controls.Add(this.cboTAGNAME);
            this.panel7.Controls.Add(this.cboVALVE_NO);
            this.panel7.Controls.Add(this.panel3);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.txtALERT_LIMIT);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.cboDELAY_TIME);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.cboVALVE_TYPE);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Controls.Add(this.cboBLOCK_CODE);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 194);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(675, 448);
            this.panel7.TabIndex = 148;
            // 
            // cboMANOMETER_NO_B
            // 
            this.cboMANOMETER_NO_B.FormattingEnabled = true;
            this.cboMANOMETER_NO_B.Location = new System.Drawing.Point(98, 49);
            this.cboMANOMETER_NO_B.Name = "cboMANOMETER_NO_B";
            this.cboMANOMETER_NO_B.Size = new System.Drawing.Size(140, 19);
            this.cboMANOMETER_NO_B.TabIndex = 202;
            // 
            // cboTAGNAME_B
            // 
            this.cboTAGNAME_B.FormattingEnabled = true;
            this.cboTAGNAME_B.Location = new System.Drawing.Point(404, 49);
            this.cboTAGNAME_B.Name = "cboTAGNAME_B";
            this.cboTAGNAME_B.Size = new System.Drawing.Size(236, 19);
            this.cboTAGNAME_B.TabIndex = 201;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label31.Location = new System.Drawing.Point(307, 53);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(92, 12);
            this.label31.TabIndex = 200;
            this.label31.Text = "태그명(전단) :";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.Location = new System.Drawing.Point(4, 53);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(92, 12);
            this.label32.TabIndex = 199;
            this.label32.Text = "계측기(전단) :";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboMANOMETER_NO_F
            // 
            this.cboMANOMETER_NO_F.FormattingEnabled = true;
            this.cboMANOMETER_NO_F.Location = new System.Drawing.Point(98, 97);
            this.cboMANOMETER_NO_F.Name = "cboMANOMETER_NO_F";
            this.cboMANOMETER_NO_F.Size = new System.Drawing.Size(140, 19);
            this.cboMANOMETER_NO_F.TabIndex = 195;
            this.cboMANOMETER_NO_F.SelectedIndexChanged += new System.EventHandler(this.cboMANOMETER_NO_F_SelectedIndexChanged);
            // 
            // cboTAGNAME_F
            // 
            this.cboTAGNAME_F.FormattingEnabled = true;
            this.cboTAGNAME_F.Location = new System.Drawing.Point(403, 96);
            this.cboTAGNAME_F.Name = "cboTAGNAME_F";
            this.cboTAGNAME_F.Size = new System.Drawing.Size(239, 19);
            this.cboTAGNAME_F.TabIndex = 194;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.Location = new System.Drawing.Point(299, 101);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(99, 12);
            this.label29.TabIndex = 193;
            this.label29.Text = "유량계 태그명 :";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.Location = new System.Drawing.Point(4, 101);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(92, 12);
            this.label30.TabIndex = 192;
            this.label30.Text = "유량계번호 :";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboMANOMETER_NO
            // 
            this.cboMANOMETER_NO.FormattingEnabled = true;
            this.cboMANOMETER_NO.Location = new System.Drawing.Point(98, 74);
            this.cboMANOMETER_NO.Name = "cboMANOMETER_NO";
            this.cboMANOMETER_NO.Size = new System.Drawing.Size(140, 19);
            this.cboMANOMETER_NO.TabIndex = 191;
            // 
            // cboTAGNAME
            // 
            this.cboTAGNAME.FormattingEnabled = true;
            this.cboTAGNAME.Location = new System.Drawing.Point(403, 73);
            this.cboTAGNAME.Name = "cboTAGNAME";
            this.cboTAGNAME.Size = new System.Drawing.Size(239, 19);
            this.cboTAGNAME.TabIndex = 190;
            // 
            // cboVALVE_NO
            // 
            this.cboVALVE_NO.FormattingEnabled = true;
            this.cboVALVE_NO.Location = new System.Drawing.Point(348, 25);
            this.cboVALVE_NO.Name = "cboVALVE_NO";
            this.cboVALVE_NO.Size = new System.Drawing.Size(140, 19);
            this.cboVALVE_NO.TabIndex = 189;
            this.cboVALVE_NO.SelectedIndexChanged += new System.EventHandler(this.cboVALVE_NO_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 123);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(675, 325);
            this.panel3.TabIndex = 186;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.Controls.Add(this.label28, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1, 31);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 26);
            this.tableLayoutPanel2.TabIndex = 207;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.Location = new System.Drawing.Point(269, 2);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(194, 22);
            this.label28.TabIndex = 214;
            this.label28.Text = "종료유량(㎥/h)";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(471, 2);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(194, 22);
            this.label15.TabIndex = 213;
            this.label15.Text = "감압설정치(㎏/㎠)";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(5, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 22);
            this.label5.TabIndex = 212;
            this.label5.Text = "NO";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(67, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(194, 22);
            this.label2.TabIndex = 211;
            this.label2.Text = "시작유량(㎥/h)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION10, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION9, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION8, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION7, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION6, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION10, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION9, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION8, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION7, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION6, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION5, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION5, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION3, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtSTART_OPTION1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtEND_OPTION1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE10, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE3, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE8, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE9, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE2, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE7, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE6, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE5, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE4, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtPR_VALUE1, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label27, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label23, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label21, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label20, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label19, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 58);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(668, 262);
            this.tableLayoutPanel1.TabIndex = 206;
            // 
            // txtEND_OPTION10
            // 
            this.txtEND_OPTION10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION10.Location = new System.Drawing.Point(269, 239);
            this.txtEND_OPTION10.Name = "txtEND_OPTION10";
            this.txtEND_OPTION10.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION10.TabIndex = 249;
            this.txtEND_OPTION10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEND_OPTION9
            // 
            this.txtEND_OPTION9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION9.Location = new System.Drawing.Point(269, 213);
            this.txtEND_OPTION9.Name = "txtEND_OPTION9";
            this.txtEND_OPTION9.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION9.TabIndex = 248;
            this.txtEND_OPTION9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEND_OPTION8
            // 
            this.txtEND_OPTION8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION8.Location = new System.Drawing.Point(269, 187);
            this.txtEND_OPTION8.Name = "txtEND_OPTION8";
            this.txtEND_OPTION8.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION8.TabIndex = 247;
            this.txtEND_OPTION8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEND_OPTION7
            // 
            this.txtEND_OPTION7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION7.Location = new System.Drawing.Point(269, 161);
            this.txtEND_OPTION7.Name = "txtEND_OPTION7";
            this.txtEND_OPTION7.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION7.TabIndex = 246;
            this.txtEND_OPTION7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEND_OPTION6
            // 
            this.txtEND_OPTION6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION6.Location = new System.Drawing.Point(269, 135);
            this.txtEND_OPTION6.Name = "txtEND_OPTION6";
            this.txtEND_OPTION6.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION6.TabIndex = 245;
            this.txtEND_OPTION6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION10
            // 
            this.txtSTART_OPTION10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION10.Location = new System.Drawing.Point(67, 239);
            this.txtSTART_OPTION10.Name = "txtSTART_OPTION10";
            this.txtSTART_OPTION10.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION10.TabIndex = 244;
            this.txtSTART_OPTION10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION9
            // 
            this.txtSTART_OPTION9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION9.Location = new System.Drawing.Point(67, 213);
            this.txtSTART_OPTION9.Name = "txtSTART_OPTION9";
            this.txtSTART_OPTION9.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION9.TabIndex = 243;
            this.txtSTART_OPTION9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION8
            // 
            this.txtSTART_OPTION8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION8.Location = new System.Drawing.Point(67, 187);
            this.txtSTART_OPTION8.Name = "txtSTART_OPTION8";
            this.txtSTART_OPTION8.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION8.TabIndex = 242;
            this.txtSTART_OPTION8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION7
            // 
            this.txtSTART_OPTION7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION7.Location = new System.Drawing.Point(67, 161);
            this.txtSTART_OPTION7.Name = "txtSTART_OPTION7";
            this.txtSTART_OPTION7.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION7.TabIndex = 241;
            this.txtSTART_OPTION7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION6
            // 
            this.txtSTART_OPTION6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION6.Location = new System.Drawing.Point(67, 135);
            this.txtSTART_OPTION6.Name = "txtSTART_OPTION6";
            this.txtSTART_OPTION6.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION6.TabIndex = 240;
            this.txtSTART_OPTION6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION5
            // 
            this.txtSTART_OPTION5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION5.Location = new System.Drawing.Point(67, 109);
            this.txtSTART_OPTION5.Name = "txtSTART_OPTION5";
            this.txtSTART_OPTION5.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION5.TabIndex = 239;
            this.txtSTART_OPTION5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEND_OPTION5
            // 
            this.txtEND_OPTION5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION5.Location = new System.Drawing.Point(269, 109);
            this.txtEND_OPTION5.Name = "txtEND_OPTION5";
            this.txtEND_OPTION5.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION5.TabIndex = 238;
            this.txtEND_OPTION5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEND_OPTION4
            // 
            this.txtEND_OPTION4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION4.Location = new System.Drawing.Point(269, 83);
            this.txtEND_OPTION4.Name = "txtEND_OPTION4";
            this.txtEND_OPTION4.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION4.TabIndex = 237;
            this.txtEND_OPTION4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION4
            // 
            this.txtSTART_OPTION4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION4.Location = new System.Drawing.Point(67, 83);
            this.txtSTART_OPTION4.Name = "txtSTART_OPTION4";
            this.txtSTART_OPTION4.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION4.TabIndex = 236;
            this.txtSTART_OPTION4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEND_OPTION3
            // 
            this.txtEND_OPTION3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION3.Location = new System.Drawing.Point(269, 57);
            this.txtEND_OPTION3.Name = "txtEND_OPTION3";
            this.txtEND_OPTION3.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION3.TabIndex = 235;
            this.txtEND_OPTION3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION3
            // 
            this.txtSTART_OPTION3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION3.Location = new System.Drawing.Point(67, 57);
            this.txtSTART_OPTION3.Name = "txtSTART_OPTION3";
            this.txtSTART_OPTION3.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION3.TabIndex = 234;
            this.txtSTART_OPTION3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEND_OPTION2
            // 
            this.txtEND_OPTION2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION2.Location = new System.Drawing.Point(269, 31);
            this.txtEND_OPTION2.Name = "txtEND_OPTION2";
            this.txtEND_OPTION2.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION2.TabIndex = 233;
            this.txtEND_OPTION2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION2
            // 
            this.txtSTART_OPTION2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION2.Location = new System.Drawing.Point(67, 31);
            this.txtSTART_OPTION2.Name = "txtSTART_OPTION2";
            this.txtSTART_OPTION2.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION2.TabIndex = 232;
            this.txtSTART_OPTION2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSTART_OPTION1
            // 
            this.txtSTART_OPTION1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTART_OPTION1.Location = new System.Drawing.Point(67, 5);
            this.txtSTART_OPTION1.Name = "txtSTART_OPTION1";
            this.txtSTART_OPTION1.Size = new System.Drawing.Size(194, 20);
            this.txtSTART_OPTION1.TabIndex = 231;
            this.txtSTART_OPTION1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEND_OPTION1
            // 
            this.txtEND_OPTION1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEND_OPTION1.Location = new System.Drawing.Point(269, 5);
            this.txtEND_OPTION1.Name = "txtEND_OPTION1";
            this.txtEND_OPTION1.Size = new System.Drawing.Size(194, 20);
            this.txtEND_OPTION1.TabIndex = 230;
            this.txtEND_OPTION1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPR_VALUE10
            // 
            this.txtPR_VALUE10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE10.Location = new System.Drawing.Point(471, 239);
            this.txtPR_VALUE10.Name = "txtPR_VALUE10";
            this.txtPR_VALUE10.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE10.TabIndex = 229;
            this.txtPR_VALUE10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE3
            // 
            this.txtPR_VALUE3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE3.Location = new System.Drawing.Point(471, 57);
            this.txtPR_VALUE3.Name = "txtPR_VALUE3";
            this.txtPR_VALUE3.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE3.TabIndex = 228;
            this.txtPR_VALUE3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE8
            // 
            this.txtPR_VALUE8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE8.Location = new System.Drawing.Point(471, 187);
            this.txtPR_VALUE8.Name = "txtPR_VALUE8";
            this.txtPR_VALUE8.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE8.TabIndex = 227;
            this.txtPR_VALUE8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE9
            // 
            this.txtPR_VALUE9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE9.Location = new System.Drawing.Point(471, 213);
            this.txtPR_VALUE9.Name = "txtPR_VALUE9";
            this.txtPR_VALUE9.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE9.TabIndex = 226;
            this.txtPR_VALUE9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE2
            // 
            this.txtPR_VALUE2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE2.Location = new System.Drawing.Point(471, 31);
            this.txtPR_VALUE2.Name = "txtPR_VALUE2";
            this.txtPR_VALUE2.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE2.TabIndex = 225;
            this.txtPR_VALUE2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE7
            // 
            this.txtPR_VALUE7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE7.Location = new System.Drawing.Point(471, 161);
            this.txtPR_VALUE7.Name = "txtPR_VALUE7";
            this.txtPR_VALUE7.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE7.TabIndex = 224;
            this.txtPR_VALUE7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE6
            // 
            this.txtPR_VALUE6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE6.Location = new System.Drawing.Point(471, 135);
            this.txtPR_VALUE6.Name = "txtPR_VALUE6";
            this.txtPR_VALUE6.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE6.TabIndex = 223;
            this.txtPR_VALUE6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE5
            // 
            this.txtPR_VALUE5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE5.Location = new System.Drawing.Point(471, 109);
            this.txtPR_VALUE5.Name = "txtPR_VALUE5";
            this.txtPR_VALUE5.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE5.TabIndex = 222;
            this.txtPR_VALUE5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE4
            // 
            this.txtPR_VALUE4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE4.Location = new System.Drawing.Point(471, 83);
            this.txtPR_VALUE4.Name = "txtPR_VALUE4";
            this.txtPR_VALUE4.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE4.TabIndex = 221;
            this.txtPR_VALUE4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPR_VALUE1
            // 
            this.txtPR_VALUE1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPR_VALUE1.Location = new System.Drawing.Point(471, 5);
            this.txtPR_VALUE1.Name = "txtPR_VALUE1";
            this.txtPR_VALUE1.Size = new System.Drawing.Size(194, 20);
            this.txtPR_VALUE1.TabIndex = 220;
            this.txtPR_VALUE1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(5, 184);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 24);
            this.label27.TabIndex = 219;
            this.label27.Text = "8";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(5, 54);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 24);
            this.label26.TabIndex = 218;
            this.label26.Text = "3";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(5, 236);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 24);
            this.label25.TabIndex = 217;
            this.label25.Text = "10";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(5, 210);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 24);
            this.label24.TabIndex = 216;
            this.label24.Text = "9";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(5, 158);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 24);
            this.label23.TabIndex = 215;
            this.label23.Text = "7";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(5, 28);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 24);
            this.label22.TabIndex = 214;
            this.label22.Text = "2";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(5, 132);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 24);
            this.label21.TabIndex = 213;
            this.label21.Text = "6";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(5, 106);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 24);
            this.label20.TabIndex = 212;
            this.label20.Text = "5";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(5, 80);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 24);
            this.label19.TabIndex = 211;
            this.label19.Text = "4";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(5, 2);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 24);
            this.label18.TabIndex = 210;
            this.label18.Text = "1";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label13);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(675, 28);
            this.panel5.TabIndex = 183;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(4, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(202, 12);
            this.label13.TabIndex = 164;
            this.label13.Text = "유량비례제어 감압밸브 운영 정보";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(638, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 12);
            this.label12.TabIndex = 185;
            this.label12.Text = "(분)";
            this.label12.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(94, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 12);
            this.label11.TabIndex = 184;
            this.label11.Text = "±";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(307, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 12);
            this.label6.TabIndex = 182;
            this.label6.Text = "태그명(후단) :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtALERT_LIMIT
            // 
            this.txtALERT_LIMIT.Location = new System.Drawing.Point(111, 25);
            this.txtALERT_LIMIT.Name = "txtALERT_LIMIT";
            this.txtALERT_LIMIT.Size = new System.Drawing.Size(43, 20);
            this.txtALERT_LIMIT.TabIndex = 180;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(25, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 12);
            this.label4.TabIndex = 179;
            this.label4.Text = "경보범위 :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(504, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 12);
            this.label1.TabIndex = 176;
            this.label1.Text = "시간제어 :";
            this.label1.Visible = false;
            // 
            // cboDELAY_TIME
            // 
            this.cboDELAY_TIME.FormattingEnabled = true;
            this.cboDELAY_TIME.Location = new System.Drawing.Point(577, 25);
            this.cboDELAY_TIME.Name = "cboDELAY_TIME";
            this.cboDELAY_TIME.Size = new System.Drawing.Size(61, 19);
            this.cboDELAY_TIME.TabIndex = 175;
            this.cboDELAY_TIME.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(264, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 12);
            this.label10.TabIndex = 173;
            this.label10.Text = "감압밸브ID :";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(4, 78);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 12);
            this.label14.TabIndex = 166;
            this.label14.Text = "계측기(후단) :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(305, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 12);
            this.label16.TabIndex = 165;
            this.label16.Text = "감압밸브타입 :";
            // 
            // cboVALVE_TYPE
            // 
            this.cboVALVE_TYPE.FormattingEnabled = true;
            this.cboVALVE_TYPE.Location = new System.Drawing.Point(404, 4);
            this.cboVALVE_TYPE.Name = "cboVALVE_TYPE";
            this.cboVALVE_TYPE.Size = new System.Drawing.Size(234, 19);
            this.cboVALVE_TYPE.TabIndex = 164;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(38, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 12);
            this.label17.TabIndex = 163;
            this.label17.Text = "소블록 :";
            // 
            // cboBLOCK_CODE
            // 
            this.cboBLOCK_CODE.FormattingEnabled = true;
            this.cboBLOCK_CODE.Location = new System.Drawing.Point(98, 4);
            this.cboBLOCK_CODE.Name = "cboBLOCK_CODE";
            this.cboBLOCK_CODE.Size = new System.Drawing.Size(140, 19);
            this.cboBLOCK_CODE.TabIndex = 162;
            this.cboBLOCK_CODE.SelectedIndexChanged += new System.EventHandler(this.cboBLOCK_CODE_SelectedIndexChanged);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Gold;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Location = new System.Drawing.Point(0, 190);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(675, 4);
            this.pictureBox3.TabIndex = 149;
            this.pictureBox3.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.btnNew);
            this.panel6.Controls.Add(this.btnSave);
            this.panel6.Controls.Add(this.btnDelete);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 162);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(675, 28);
            this.panel6.TabIndex = 147;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(4, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 12);
            this.label3.TabIndex = 164;
            this.label3.Text = "유량비례제어 감압밸브 감시 관리";
            // 
            // btnNew
            // 
            this.btnNew.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnNew.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Clear;
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNew.Location = new System.Drawing.Point(393, 0);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 26);
            this.btnNew.TabIndex = 159;
            this.btnNew.Text = "초기화";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSave.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(483, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 26);
            this.btnSave.TabIndex = 158;
            this.btnSave.Text = "지점 등록";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Delete;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.Location = new System.Drawing.Point(573, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 26);
            this.btnDelete.TabIndex = 157;
            this.btnDelete.Text = "지점 삭제";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 158);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(675, 4);
            this.pictureBox2.TabIndex = 146;
            this.pictureBox2.TabStop = false;
            // 
            // uGridPopup
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPopup.DisplayLayout.Appearance = appearance1;
            this.uGridPopup.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPopup.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPopup.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridPopup.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPopup.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridPopup.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPopup.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPopup.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPopup.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridPopup.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPopup.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPopup.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridPopup.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPopup.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridPopup.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridPopup.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPopup.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridPopup.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridPopup.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPopup.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridPopup.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPopup.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPopup.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPopup.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridPopup.Location = new System.Drawing.Point(0, 0);
            this.uGridPopup.Name = "uGridPopup";
            this.uGridPopup.Size = new System.Drawing.Size(675, 158);
            this.uGridPopup.TabIndex = 144;
            this.uGridPopup.Text = "ultraGrid1";
            this.uGridPopup.Click += new System.EventHandler(this.uGridPopup_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(675, 4);
            this.pictureBox1.TabIndex = 105;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnQuery);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.cboSBlock);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cboMBlock);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cboLBlock);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(675, 52);
            this.panel2.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Close2;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.Location = new System.Drawing.Point(573, 22);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 26);
            this.btnClose.TabIndex = 154;
            this.btnClose.Text = "닫기";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Query;
            this.btnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery.Location = new System.Drawing.Point(483, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(90, 26);
            this.btnQuery.TabIndex = 153;
            this.btnQuery.Text = "지점 조회";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(416, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 12);
            this.label9.TabIndex = 148;
            this.label9.Text = "소블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(476, 3);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(183, 19);
            this.cboSBlock.TabIndex = 147;
            this.cboSBlock.SelectedIndexChanged += new System.EventHandler(this.cboSBlock_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(210, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 146;
            this.label8.Text = "중블록 :";
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(270, 3);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 19);
            this.cboMBlock.TabIndex = 145;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(4, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 144;
            this.label7.Text = "대블록 :";
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(64, 3);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 19);
            this.cboLBlock.TabIndex = 143;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 698);
            this.picFrLeft.TabIndex = 106;
            this.picFrLeft.TabStop = false;
            // 
            // PicFrRight
            // 
            this.PicFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.PicFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicFrRight.Location = new System.Drawing.Point(679, 4);
            this.PicFrRight.Name = "PicFrRight";
            this.PicFrRight.Size = new System.Drawing.Size(4, 698);
            this.PicFrRight.TabIndex = 107;
            this.PicFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 702);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(683, 4);
            this.picFrBottom.TabIndex = 105;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(683, 4);
            this.picFrTop.TabIndex = 104;
            this.picFrTop.TabStop = false;
            // 
            // frmWPRValveManage3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 11F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 706);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.PicFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmWPRValveManage3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "유량비례제어 감압밸브 감시 관리";
            this.Load += new System.EventHandler(this.frmWPRValveManage3_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWPRValveManage3_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox PicFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboLBlock;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnClose;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPopup;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboVALVE_TYPE;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cboBLOCK_CODE;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboDELAY_TIME;
        private System.Windows.Forms.TextBox txtALERT_LIMIT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPR_VALUE10;
        private System.Windows.Forms.TextBox txtPR_VALUE3;
        private System.Windows.Forms.TextBox txtPR_VALUE8;
        private System.Windows.Forms.TextBox txtPR_VALUE9;
        private System.Windows.Forms.TextBox txtPR_VALUE2;
        private System.Windows.Forms.TextBox txtPR_VALUE7;
        private System.Windows.Forms.TextBox txtPR_VALUE6;
        private System.Windows.Forms.TextBox txtPR_VALUE5;
        private System.Windows.Forms.TextBox txtPR_VALUE4;
        private System.Windows.Forms.TextBox txtPR_VALUE1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSTART_OPTION1;
        private System.Windows.Forms.TextBox txtEND_OPTION1;
        private System.Windows.Forms.TextBox txtEND_OPTION9;
        private System.Windows.Forms.TextBox txtEND_OPTION8;
        private System.Windows.Forms.TextBox txtEND_OPTION7;
        private System.Windows.Forms.TextBox txtEND_OPTION6;
        private System.Windows.Forms.TextBox txtSTART_OPTION10;
        private System.Windows.Forms.TextBox txtSTART_OPTION9;
        private System.Windows.Forms.TextBox txtSTART_OPTION8;
        private System.Windows.Forms.TextBox txtSTART_OPTION7;
        private System.Windows.Forms.TextBox txtSTART_OPTION6;
        private System.Windows.Forms.TextBox txtSTART_OPTION5;
        private System.Windows.Forms.TextBox txtEND_OPTION5;
        private System.Windows.Forms.TextBox txtEND_OPTION4;
        private System.Windows.Forms.TextBox txtSTART_OPTION4;
        private System.Windows.Forms.TextBox txtEND_OPTION3;
        private System.Windows.Forms.TextBox txtSTART_OPTION3;
        private System.Windows.Forms.TextBox txtEND_OPTION2;
        private System.Windows.Forms.TextBox txtSTART_OPTION2;
        private System.Windows.Forms.TextBox txtEND_OPTION10;
        private System.Windows.Forms.ComboBox cboMANOMETER_NO;
        private System.Windows.Forms.ComboBox cboTAGNAME;
        private System.Windows.Forms.ComboBox cboVALVE_NO;
        private System.Windows.Forms.ComboBox cboMANOMETER_NO_F;
        private System.Windows.Forms.ComboBox cboTAGNAME_F;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cboMANOMETER_NO_B;
        private System.Windows.Forms.ComboBox cboTAGNAME_B;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
    }
}