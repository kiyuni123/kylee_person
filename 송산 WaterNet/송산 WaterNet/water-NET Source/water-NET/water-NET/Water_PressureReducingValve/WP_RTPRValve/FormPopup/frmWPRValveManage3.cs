﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WP_RTPRValve.FormPopup
{
    /// <summary>
    /// Project ID : WN_WP_A01
    /// Project Explain : 감압밸브감시
    /// Project Developer : 오두석
    /// Project Create Date : 2011.02.09
    /// Form Explain : 유량비례제어감압밸브감시관리 Popup Form
    /// </summary>
    public partial class frmWPRValveManage3 : Form
    {
        public frmWPRValveManage3()
        {
            InitializeComponent();

            this.InitializeGridSetting();

            this.InitializeComboSetting();

            this.InitializeControlSetting();
        }

        private void frmWPRValveManage3_Load(object sender, EventArgs e)
        {
            //=======================================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=======================================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["감압밸브감시ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
                this.btnDelete.Enabled = false;
            }

            //==========================================================================================
            this.cboMANOMETER_NO.SelectedIndexChanged += new EventHandler(cboMANOMETER_NO_SelectedIndexChanged);
            this.cboMANOMETER_NO_B.SelectedIndexChanged += new EventHandler(cboMANOMETER_NO_SelectedIndexChanged);

            WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE3 = true;
            this.GetQueryData();
        }

        private void frmWPRValveManage3_FormClosing(object sender, FormClosingEventArgs e)
        {
            WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE3 = false;
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetQueryData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.InitializeControlSetting();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValidation() == false) return;

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            this.SavePRValveMonitorPointData();

            this.SavePRValveMonitorOptionData();

            this.InitializeControlSetting();

            this.GetQueryData();

            this.Cursor = System.Windows.Forms.Cursors.Default;

            MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.IsValidation() == false) return;

            DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oDRtn == DialogResult.Yes)
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                this.DeletePRValveMonitorPointData();

                this.DeletePRValveMonitorOptionData();

                this.InitializeControlSetting();

                this.GetQueryData();

                this.Cursor = System.Windows.Forms.Cursors.Default;

                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        
        #endregion


        #region Control Events

        #region - UltraGrid Events

        private void uGridPopup_Click(object sender, EventArgs e)
        {
            if (this.uGridPopup.Rows.Count <= 0) return;

            this.InitializeControlSetting();

            this.SetDataBySelectedRecordOnGrid();
        }

        #endregion

        #region - Combo Events

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.InitializeControlSetting();

            string strLOC_CODE = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLOC_CODE, 1);
            WQ_Function.SetCombo_SmallBlock(this.cboBLOCK_CODE, EMFrame.statics.AppStatic.USER_SGCCD, strLOC_CODE, 0);

            if (strLOC_CODE.Trim() == "") return;
            string strFTR_IDN = WQ_Function.GetAllSmallBlockFTRIDNInMediumBlock(strLOC_CODE);
            WP_AppStatic.SetCombo_NoOfPressureReducingValveOnSmallBlockByLayer(this.cboVALVE_NO, strFTR_IDN, "MOF013", 0);

            //string strFTR_IDN = WQ_Function.GetMediumBlockFTR_IDN(strLOC_CODE);
            //WP_AppStatic.Viewmap_SelectedBlock(strFTR_IDN, 0);
        }
        
        private void cboSBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLOC_CODE = WQ_Function.SplitToCode(this.cboSBlock.Text);
            if (this.cboBLOCK_CODE.Items.Count > 0)
            {
                this.cboBLOCK_CODE.SelectedIndex = this.cboSBlock.SelectedIndex - 1;
            }
            //string strFTR_IDN = WQ_Function.GetSmallBlockFTR_IDN(strLOC_CODE);
            //WP_AppStatic.Viewmap_SelectedBlock(strFTR_IDN, 1);
        }

        private void cboBLOCK_CODE_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLOC_CODE = WQ_Function.SplitToCode(this.cboBLOCK_CODE.Text);
            //string strFTR_IDN = WQ_Function.GetSmallBlockFTR_IDN(strLOC_CODE);
            //WP_AppStatic.SetCombo_NoOfPressureReducingValveOnSmallBlockByLayer(this.cboVALVE_NO, strFTR_IDN, 0);
            WP_AppStatic.SetCombo_NoOfMeasuringInstrumentOnSmallBlock(this.cboMANOMETER_NO, strLOC_CODE, 0);
            WP_AppStatic.SetCombo_NoOfMeasuringInstrumentOnSmallBlock(this.cboMANOMETER_NO_B, strLOC_CODE, 0);
            WP_AppStatic.SetCombo_NoOfFlowmeteringInstrumentOnSmallBlock(this.cboMANOMETER_NO_F, strLOC_CODE, 0);
        }

        private void cboMANOMETER_NO_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strFTR_IDN = ((ComboBox)sender).Text;
            if (((ComboBox)sender).Name.Equals("cboMANOMETER_NO"))
            {
                WP_AppStatic.SetCombo_TAGOfMeasuringInstrumentOnSmallBlock(this.cboTAGNAME, strFTR_IDN, 0);
            }
            else if (((ComboBox)sender).Name.Equals("cboMANOMETER_NO_B"))
            {
                WP_AppStatic.SetCombo_TAGOfMeasuringInstrumentOnSmallBlock2(this.cboTAGNAME_B, strFTR_IDN, 0);
            }

            WP_AppStatic.Viewmap_SelectedManometer(strFTR_IDN);

        }

        private void cboMANOMETER_NO_F_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strFTR_IDN = this.cboMANOMETER_NO_F.Text;
            WP_AppStatic.SetCombo_TAGOfFlowmeteringInstrumentOnSmallBlock(this.cboTAGNAME_F, strFTR_IDN, 0);
            WP_AppStatic.Viewmap_SelectedManometer(strFTR_IDN);
        }

        private void cboVALVE_NO_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strFTR_IDN = this.cboVALVE_NO.Text;
            WP_AppStatic.Viewmap_SelectedValve(strFTR_IDN);
        }

        #endregion

        #endregion


        #region User Function

        #region - Initialize Function

        /// <summary>
        /// 초기 실행시 그리드 설정
        /// </summary>
        private void InitializeGridSetting()
        {
            #region uGridPopup : 유량비례제어감압밸브감시지점 Grid
            
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VALVE_NO";
            oUltraGridColumn.Header.Caption = "감압밸브ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ALERT_LIMIT";
            oUltraGridColumn.Header.Caption = "경보범위(±)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DELAY_TIME";
            oUltraGridColumn.Header.Caption = "시간제어";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MANOMETER_NO_B";
            oUltraGridColumn.Header.Caption = "계측기(전단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME_B";
            oUltraGridColumn.Header.Caption = "태그명(전단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MANOMETER_NO";
            oUltraGridColumn.Header.Caption = "계측기(후단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "태그명(후단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MANOMETER_NO_F";
            oUltraGridColumn.Header.Caption = "유량계번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME_F";
            oUltraGridColumn.Header.Caption = "유량계태그명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPopup);

            #endregion
        }

        /// <summary>
        /// 초기 실행시 콤보 설정
        /// </summary>
        private void InitializeComboSetting()
        {
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);
            WP_AppStatic.SetCombo_PRValveType(this.cboVALVE_TYPE, 2, false);
            this.cboVALVE_TYPE.Enabled = false;
        }

        /// <summary>
        /// 초기 이후에도 초기화가 필요한 TextBox 등 Control을 초기화한다.
        /// </summary>
        private void InitializeControlSetting()
        {
            WP_AppStatic.SetCombo_DelayTime(this.cboDELAY_TIME, 2);

            this.txtALERT_LIMIT.Text = "0.5";
            if (this.cboVALVE_NO.Items.Count > 0) this.cboVALVE_NO.SelectedIndex = 0;
            if (this.cboMANOMETER_NO.Items.Count > 0) this.cboMANOMETER_NO.SelectedIndex = 0;
            if (this.cboTAGNAME.Items.Count > 0) this.cboTAGNAME.SelectedIndex = 0;
            if (this.cboMANOMETER_NO_B.Items.Count > 0) this.cboMANOMETER_NO_B.SelectedIndex = 0;
            if (this.cboTAGNAME_B.Items.Count > 0) this.cboTAGNAME_B.SelectedIndex = 0;
            if (this.cboMANOMETER_NO_F.Items.Count > 0) this.cboMANOMETER_NO_F.SelectedIndex = 0;
            if (this.cboTAGNAME_F.Items.Count > 0) this.cboTAGNAME_F.SelectedIndex = 0;

            this.txtSTART_OPTION1.Text = "";
            this.txtSTART_OPTION2.Text = "";
            this.txtSTART_OPTION3.Text = "";
            this.txtSTART_OPTION4.Text = "";
            this.txtSTART_OPTION5.Text = "";
            this.txtSTART_OPTION6.Text = "";
            this.txtSTART_OPTION7.Text = "";
            this.txtSTART_OPTION8.Text = "";
            this.txtSTART_OPTION9.Text = "";
            this.txtSTART_OPTION10.Text = "";

            this.txtEND_OPTION1.Text = "";
            this.txtEND_OPTION2.Text = "";
            this.txtEND_OPTION3.Text = "";
            this.txtEND_OPTION4.Text = "";
            this.txtEND_OPTION5.Text = "";
            this.txtEND_OPTION6.Text = "";
            this.txtEND_OPTION7.Text = "";
            this.txtEND_OPTION8.Text = "";
            this.txtEND_OPTION9.Text = "";
            this.txtEND_OPTION10.Text = "";

            this.txtPR_VALUE1.Text = "";
            this.txtPR_VALUE2.Text = "";
            this.txtPR_VALUE3.Text = "";
            this.txtPR_VALUE4.Text = "";
            this.txtPR_VALUE5.Text = "";
            this.txtPR_VALUE6.Text = "";
            this.txtPR_VALUE7.Text = "";
            this.txtPR_VALUE8.Text = "";
            this.txtPR_VALUE9.Text = "";
            this.txtPR_VALUE10.Text = "";
        }

        #endregion

        #region - SQL Function

        /// <summary>
        /// 조회 조건으로 Query를 실행하여 Grid에 Set한다.
        /// </summary>
        private void GetQueryData()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();
            
            DataSet pDS = new DataSet();

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;
            string strVALVE_TYPE = string.Empty;

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = " AND A.BLOCK_CODE IN (" + strBLOCK_CD_S + ")";
                }
                else
                {
                    strBLOCK_SQL = " AND A.BLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }
            strVALVE_TYPE = WQ_Function.SplitToCode(this.cboVALVE_TYPE.Text);
            
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   (SELECT LOC_NAME || ' (' || LOC_CODE || ')' FROM CM_LOCATION WHERE FTR_CODE = 'BZ003' AND LOC_CODE = A.BLOCK_CODE) AS BLOCK, A.VALVE_NO, A.ALERT_LIMIT, A.DELAY_TIME,A.MANOMETER_NO_B, A.TAGNAME_B, A.MANOMETER_NO, A.TAGNAME, A.MANOMETER_NO_F, A.TAGNAME_F");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_POINT A");
            oStringBuilder.AppendLine("WHERE    A.VALVE_TYPE = '" + strVALVE_TYPE + "'" + strBLOCK_SQL);
            oStringBuilder.AppendLine("ORDER BY A.VALVE_NO");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WP_POINT");

            this.uGridPopup.DataSource = pDS.Tables["WP_POINT"].DefaultView;

            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 감압밸브감시지점 Data를 저장 한다.
        /// </summary>
        private void SavePRValveMonitorPointData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strBLOCK_CODE = WQ_Function.SplitToCode(this.cboBLOCK_CODE.Text);
            string strVALVE_NO = this.cboVALVE_NO.Text;
            string strVALVE_TYPE = WQ_Function.SplitToCode(this.cboVALVE_TYPE.Text);
            string strALERT_LIMIT = this.txtALERT_LIMIT.Text;
            string strDELAY_TIME = this.cboDELAY_TIME.Text;
            string strMANOMETER_NO = this.cboMANOMETER_NO.Text;
            string strTAGNAME = this.cboTAGNAME.Text;
            string strMANOMETER_NO_F = this.cboMANOMETER_NO_F.Text;
            string strTAGNAME_F = this.cboTAGNAME_F.Text;
            string strMANOMETER_NO_B = this.cboMANOMETER_NO_B.Text;
            string strTAGNAME_B = this.cboTAGNAME_B.Text;
            string strREG_DATE = WQ_Function.StringToDateTime(DateTime.Now);

            string strParam = string.Empty;

            this.DeletePRValveMonitorPointData();

            strParam = "('" + strVALVE_NO + "', '" + strVALVE_TYPE + "', '" + strALERT_LIMIT + "', '" + strDELAY_TIME + "', '" + strMANOMETER_NO_B + "', '" + strTAGNAME_B + "', '"
                     + strMANOMETER_NO + "', '" + strTAGNAME + "', '" + strMANOMETER_NO_F + "', '" + strTAGNAME_F + "', '"
                     + strBLOCK_CODE + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + strREG_DATE + "')";

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    WP_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("(VALVE_NO, VALVE_TYPE, ALERT_LIMIT, DELAY_TIME,MANOMETER_NO_B, TAGNAME_B, MANOMETER_NO, TAGNAME, MANOMETER_NO_F, TAGNAME_F, BLOCK_CODE, REG_ID, REG_DATE)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine(strParam);

            WP_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 감압밸브감시옵션 Data를 저장 한다.
        /// </summary>
        private void SavePRValveMonitorOptionData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strOPTION_NO = string.Empty;
            string strSTART_OPTION = string.Empty;
            string strEND_OPTION = string.Empty;
            string strPR_VALUE = string.Empty;

            string strVALVE_NO = this.cboVALVE_NO.Text;
            string strREG_DATE = WQ_Function.StringToDateTime(DateTime.Now);
            
            string strParam = string.Empty;

            this.DeletePRValveMonitorOptionData();

            for (int i = 1; i <= 10; i++)
            {
                strOPTION_NO = i.ToString();
                strSTART_OPTION = string.Empty;
                strEND_OPTION = string.Empty;

                switch (strOPTION_NO)
                {
                    case "1":                        
                        strSTART_OPTION = this.txtSTART_OPTION1.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION1.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE1.Text;
                        break;
                    case "2":
                        strSTART_OPTION = this.txtSTART_OPTION2.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION2.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE2.Text;
                        break;
                    case "3":
                        strSTART_OPTION = this.txtSTART_OPTION3.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION3.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE3.Text;
                        break;
                    case "4":
                        strSTART_OPTION = this.txtSTART_OPTION4.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION4.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE4.Text;
                        break;
                    case "5":
                        strSTART_OPTION = this.txtSTART_OPTION5.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION5.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE5.Text;
                        break;
                    case "6":
                        strSTART_OPTION = this.txtSTART_OPTION6.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION6.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE6.Text;
                        break;
                    case "7":
                        strSTART_OPTION = this.txtSTART_OPTION7.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION7.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE7.Text;
                        break;
                    case "8":
                        strSTART_OPTION = this.txtSTART_OPTION8.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION8.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE8.Text;
                        break;
                    case "9":
                        strSTART_OPTION = this.txtSTART_OPTION9.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION9.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE9.Text;
                        break;
                    case "10":
                        strSTART_OPTION = this.txtSTART_OPTION10.Text.Trim();
                        strEND_OPTION = this.txtEND_OPTION10.Text.Trim();
                        strPR_VALUE = this.txtPR_VALUE10.Text;
                        break;
                }

                if (strSTART_OPTION.Length > 0 && strEND_OPTION.Length > 0 && strPR_VALUE.Length > 0)
                {
                    strParam = "('" + strVALVE_NO + "', '" + strOPTION_NO + "', '" + strSTART_OPTION + "', '" + strEND_OPTION + "', '" + strPR_VALUE + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + strREG_DATE + "')";
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("INSERT   INTO    WP_RT_MONITOR_OPTION");
                    oStringBuilder.AppendLine("(VALVE_NO, OPTION_NO, START_OPTION, END_OPTION, PR_VALUE, REG_ID, REG_DATE)");
                    oStringBuilder.AppendLine("VALUES");
                    oStringBuilder.AppendLine(strParam);

                    WP_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
                }
            }
        }

        /// <summary>
        /// 감압밸브감시지점 Data를 삭제 한다.
        /// </summary>
        private void DeletePRValveMonitorPointData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strVALVE_NO = this.cboVALVE_NO.Text;

            string strParam = string.Empty;

            strParam = "WHERE    VALVE_NO = '" + strVALVE_NO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WP_RT_MONITOR_POINT");
            oStringBuilder.AppendLine(strParam);

            WP_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 감압밸브감시옵션 Data를 삭제 한다.
        /// </summary>
        private void DeletePRValveMonitorOptionData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strVALVE_NO = this.cboVALVE_NO.Text;

            string strParam = string.Empty;

            strParam = "WHERE    VALVE_NO = '" + strVALVE_NO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WP_RT_MONITOR_OPTION");
            oStringBuilder.AppendLine(strParam);

            WP_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        #endregion

        #region - ETC Function

        /// <summary>
        /// 그리드에서 선택된 Data를 감시관리 필드의 Control에 Set
        /// </summary>
        private void SetDataBySelectedRecordOnGrid()
        {
            this.cboBLOCK_CODE.Items.Clear();
            this.cboBLOCK_CODE.Items.Add(this.uGridPopup.ActiveRow.Cells[0].Text.Trim());
            this.cboBLOCK_CODE.SelectedIndex = 0;

            this.cboVALVE_NO.Items.Clear();
            this.cboVALVE_NO.Items.Add(this.uGridPopup.ActiveRow.Cells[1].Text.Trim());
            this.cboVALVE_NO.SelectedIndex = 0; 
            
            this.txtALERT_LIMIT.Text = this.uGridPopup.ActiveRow.Cells[2].Text;
            WQ_Function.SetCombo_SelectFindData(this.cboVALVE_NO, this.uGridPopup.ActiveRow.Cells[1].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboDELAY_TIME, this.uGridPopup.ActiveRow.Cells[3].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboMANOMETER_NO_B, this.uGridPopup.ActiveRow.Cells["MANOMETER_NO_B"].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboTAGNAME_B, this.uGridPopup.ActiveRow.Cells["TAGNAME_B"].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboMANOMETER_NO, this.uGridPopup.ActiveRow.Cells["MANOMETER_NO"].Text);            
            WQ_Function.SetCombo_SelectFindData(this.cboTAGNAME, this.uGridPopup.ActiveRow.Cells["TAGNAME"].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboMANOMETER_NO_F, this.uGridPopup.ActiveRow.Cells["MANOMETER_NO_F"].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboTAGNAME_F, this.uGridPopup.ActiveRow.Cells["TAGNAME_F"].Text);

            //운영정보 Control에 Data Set
            string strSTART_OPTION = string.Empty;
            string strEND_OPTION = string.Empty;
            string strPR_VALUE = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT   OPTION_NO, START_OPTION, END_OPTION, PR_VALUE");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_OPTION");
            oStringBuilder.AppendLine("WHERE    VALVE_NO = '" + this.cboVALVE_NO.Text + "'");
            oStringBuilder.AppendLine("ORDER BY TO_NUMBER(OPTION_NO)");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WP_OPTION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strSTART_OPTION = string.Empty;
                    strEND_OPTION = string.Empty;
                    strPR_VALUE = string.Empty;

                    strSTART_OPTION = oDRow["START_OPTION"].ToString().Trim();
                    strEND_OPTION = oDRow["END_OPTION"].ToString().Trim();
                    strPR_VALUE = oDRow["PR_VALUE"].ToString().Trim();

                    switch (oDRow["OPTION_NO"].ToString().Trim())
                    {
                        case "1":
                            this.txtSTART_OPTION1.Text = strSTART_OPTION;
                            this.txtEND_OPTION1.Text = strEND_OPTION;    
                            this.txtPR_VALUE1.Text = strPR_VALUE;
                            break;
                        case "2":
                            this.txtSTART_OPTION2.Text = strSTART_OPTION;
                            this.txtEND_OPTION2.Text = strEND_OPTION;   
                            this.txtPR_VALUE2.Text = strPR_VALUE;
                            break;
                        case "3":
                            this.txtSTART_OPTION3.Text = strSTART_OPTION;
                            this.txtEND_OPTION3.Text = strEND_OPTION;   
                            this.txtPR_VALUE3.Text = strPR_VALUE;
                            break;
                        case "4":
                            this.txtSTART_OPTION4.Text = strSTART_OPTION;
                            this.txtEND_OPTION4.Text = strEND_OPTION;   
                            this.txtPR_VALUE4.Text = strPR_VALUE;
                            break;
                        case "5":
                            this.txtSTART_OPTION5.Text = strSTART_OPTION;
                            this.txtEND_OPTION5.Text = strEND_OPTION;   
                            this.txtPR_VALUE5.Text = strPR_VALUE;
                            break;
                        case "6":
                            this.txtSTART_OPTION6.Text = strSTART_OPTION;
                            this.txtEND_OPTION6.Text = strEND_OPTION;   
                            this.txtPR_VALUE6.Text = strPR_VALUE;
                            break;
                        case "7":
                            this.txtSTART_OPTION7.Text = strSTART_OPTION;
                            this.txtEND_OPTION7.Text = strEND_OPTION;   
                            this.txtPR_VALUE7.Text = strPR_VALUE;
                            break;
                        case "8":
                            this.txtSTART_OPTION8.Text = strSTART_OPTION;
                            this.txtEND_OPTION8.Text = strEND_OPTION;   
                            this.txtPR_VALUE8.Text = strPR_VALUE;
                            break;
                        case "9":
                            this.txtSTART_OPTION9.Text = strSTART_OPTION;
                            this.txtEND_OPTION9.Text = strEND_OPTION;   
                            this.txtPR_VALUE9.Text = strPR_VALUE;
                            break;
                        case "10":
                            this.txtSTART_OPTION10.Text = strSTART_OPTION;
                            this.txtEND_OPTION10.Text = strEND_OPTION;   
                            this.txtPR_VALUE10.Text = strPR_VALUE;
                            break;
                    }
                }
            }

            pDS.Dispose();
        }

        /// <summary>
        /// 저장하는 Data를 검증한다.
        /// </summary>
        /// <returns>True / False</returns>
        private bool IsValidation()
        {
            bool blRtn = true;

            if (this.cboBLOCK_CODE.Text == "") blRtn = false;
            if (this.cboVALVE_TYPE.Text == "") blRtn = false;
            if (this.cboDELAY_TIME.Text == "") blRtn = false;
            if (this.txtALERT_LIMIT.Text.Trim() == "") blRtn = false;
            if (this.cboMANOMETER_NO.Text.Trim() == "") blRtn = false;
            if (this.cboVALVE_NO.Text.Trim() == "") blRtn = false;
            if (this.cboTAGNAME.Text.Trim() == "") blRtn = false;
            if (this.cboMANOMETER_NO_F.Text.Trim() == "") blRtn = false;
            if (this.cboTAGNAME_F.Text.Trim() == "") blRtn = false;

            if (this.txtSTART_OPTION1.Text.Trim() == "") blRtn = false;
            if (this.txtEND_OPTION1.Text.Trim() == "") blRtn = false;
            if (this.txtPR_VALUE1.Text.Trim() == "") blRtn = false;

            if (blRtn == false)
            {
                MessageBox.Show("유량비례제어감압밸브 감시정보를 입력(또는 선택)해 주십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return blRtn;
        }

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE3 = false;
            this.Dispose();
            this.Close();
        }

        #endregion

        #endregion
    }
}
