﻿namespace WaterNet.WP_RTPRValve.FormPopup
{
    partial class frmWPRValveManage1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.picFrLeft = new System.Windows.Forms.PictureBox();
            this.PicFrRight = new System.Windows.Forms.PictureBox();
            this.picFrBottom = new System.Windows.Forms.PictureBox();
            this.picFrTop = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cboMANOMETER_NO_B = new System.Windows.Forms.ComboBox();
            this.cboTAGNAME_B = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cboMANOMETER_NO = new System.Windows.Forms.ComboBox();
            this.cboTAGNAME = new System.Windows.Forms.ComboBox();
            this.cboVALVE_NO = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtALERT_LIMIT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPR_VALUE = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboDELAY_TIME = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cboVALVE_TYPE = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboBLOCK_CODE = new System.Windows.Forms.ComboBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.uGridPopup = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cboSBlock = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboMBlock = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboLBlock = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // picFrLeft
            // 
            this.picFrLeft.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeft.Location = new System.Drawing.Point(0, 4);
            this.picFrLeft.Name = "picFrLeft";
            this.picFrLeft.Size = new System.Drawing.Size(4, 380);
            this.picFrLeft.TabIndex = 106;
            this.picFrLeft.TabStop = false;
            // 
            // PicFrRight
            // 
            this.PicFrRight.BackColor = System.Drawing.SystemColors.Control;
            this.PicFrRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicFrRight.Location = new System.Drawing.Point(627, 4);
            this.PicFrRight.Name = "PicFrRight";
            this.PicFrRight.Size = new System.Drawing.Size(4, 380);
            this.PicFrRight.TabIndex = 107;
            this.PicFrRight.TabStop = false;
            // 
            // picFrBottom
            // 
            this.picFrBottom.BackColor = System.Drawing.SystemColors.Control;
            this.picFrBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picFrBottom.Location = new System.Drawing.Point(0, 384);
            this.picFrBottom.Name = "picFrBottom";
            this.picFrBottom.Size = new System.Drawing.Size(631, 4);
            this.picFrBottom.TabIndex = 105;
            this.picFrBottom.TabStop = false;
            // 
            // picFrTop
            // 
            this.picFrTop.BackColor = System.Drawing.SystemColors.Control;
            this.picFrTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFrTop.Location = new System.Drawing.Point(0, 0);
            this.picFrTop.Name = "picFrTop";
            this.picFrTop.Size = new System.Drawing.Size(631, 4);
            this.picFrTop.TabIndex = 104;
            this.picFrTop.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(623, 380);
            this.panel1.TabIndex = 131;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.uGridPopup);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 56);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(623, 324);
            this.panel4.TabIndex = 106;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.Controls.Add(this.cboMANOMETER_NO_B);
            this.panel7.Controls.Add(this.cboTAGNAME_B);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Controls.Add(this.cboMANOMETER_NO);
            this.panel7.Controls.Add(this.cboTAGNAME);
            this.panel7.Controls.Add(this.cboVALVE_NO);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.txtALERT_LIMIT);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.txtPR_VALUE);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.cboDELAY_TIME);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.cboVALVE_TYPE);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Controls.Add(this.cboBLOCK_CODE);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 194);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(623, 130);
            this.panel7.TabIndex = 148;
            // 
            // cboMANOMETER_NO_B
            // 
            this.cboMANOMETER_NO_B.FormattingEnabled = true;
            this.cboMANOMETER_NO_B.Location = new System.Drawing.Point(104, 75);
            this.cboMANOMETER_NO_B.Name = "cboMANOMETER_NO_B";
            this.cboMANOMETER_NO_B.Size = new System.Drawing.Size(136, 19);
            this.cboMANOMETER_NO_B.TabIndex = 192;
            // 
            // cboTAGNAME_B
            // 
            this.cboTAGNAME_B.FormattingEnabled = true;
            this.cboTAGNAME_B.Location = new System.Drawing.Point(370, 75);
            this.cboTAGNAME_B.Name = "cboTAGNAME_B";
            this.cboTAGNAME_B.Size = new System.Drawing.Size(239, 19);
            this.cboTAGNAME_B.TabIndex = 191;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(276, 79);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 12);
            this.label13.TabIndex = 190;
            this.label13.Text = "태그명(전단) :";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(7, 79);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 12);
            this.label15.TabIndex = 189;
            this.label15.Text = "계측기(전단) :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboMANOMETER_NO
            // 
            this.cboMANOMETER_NO.FormattingEnabled = true;
            this.cboMANOMETER_NO.Location = new System.Drawing.Point(103, 100);
            this.cboMANOMETER_NO.Name = "cboMANOMETER_NO";
            this.cboMANOMETER_NO.Size = new System.Drawing.Size(136, 19);
            this.cboMANOMETER_NO.TabIndex = 188;
            // 
            // cboTAGNAME
            // 
            this.cboTAGNAME.FormattingEnabled = true;
            this.cboTAGNAME.Location = new System.Drawing.Point(369, 100);
            this.cboTAGNAME.Name = "cboTAGNAME";
            this.cboTAGNAME.Size = new System.Drawing.Size(239, 19);
            this.cboTAGNAME.TabIndex = 187;
            // 
            // cboVALVE_NO
            // 
            this.cboVALVE_NO.FormattingEnabled = true;
            this.cboVALVE_NO.Location = new System.Drawing.Point(103, 51);
            this.cboVALVE_NO.Name = "cboVALVE_NO";
            this.cboVALVE_NO.Size = new System.Drawing.Size(136, 19);
            this.cboVALVE_NO.TabIndex = 186;
            this.cboVALVE_NO.SelectedIndexChanged += new System.EventHandler(this.cboVALVE_NO_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(585, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 12);
            this.label12.TabIndex = 185;
            this.label12.Text = "(분)";
            this.label12.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(352, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 12);
            this.label11.TabIndex = 184;
            this.label11.Text = "±";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(276, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 12);
            this.label6.TabIndex = 182;
            this.label6.Text = "태그명(후단) :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(166, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 12);
            this.label5.TabIndex = 181;
            this.label5.Text = "(㎏/㎠)";
            // 
            // txtALERT_LIMIT
            // 
            this.txtALERT_LIMIT.Location = new System.Drawing.Point(369, 25);
            this.txtALERT_LIMIT.Name = "txtALERT_LIMIT";
            this.txtALERT_LIMIT.Size = new System.Drawing.Size(43, 20);
            this.txtALERT_LIMIT.TabIndex = 180;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(270, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 12);
            this.label4.TabIndex = 179;
            this.label4.Text = "경보범위 :";
            // 
            // txtPR_VALUE
            // 
            this.txtPR_VALUE.Location = new System.Drawing.Point(105, 25);
            this.txtPR_VALUE.Name = "txtPR_VALUE";
            this.txtPR_VALUE.Size = new System.Drawing.Size(61, 20);
            this.txtPR_VALUE.TabIndex = 178;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(7, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 12);
            this.label2.TabIndex = 177;
            this.label2.Text = "감압설정치 :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(451, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 12);
            this.label1.TabIndex = 176;
            this.label1.Text = "시간제어 :";
            this.label1.Visible = false;
            // 
            // cboDELAY_TIME
            // 
            this.cboDELAY_TIME.FormattingEnabled = true;
            this.cboDELAY_TIME.Location = new System.Drawing.Point(524, 25);
            this.cboDELAY_TIME.Name = "cboDELAY_TIME";
            this.cboDELAY_TIME.Size = new System.Drawing.Size(61, 19);
            this.cboDELAY_TIME.TabIndex = 175;
            this.cboDELAY_TIME.Visible = false;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(7, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 12);
            this.label10.TabIndex = 173;
            this.label10.Text = "감압밸브ID :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(7, 104);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 12);
            this.label14.TabIndex = 166;
            this.label14.Text = "계측기(후단) :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(270, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 12);
            this.label16.TabIndex = 165;
            this.label16.Text = "감압밸브타입 :";
            // 
            // cboVALVE_TYPE
            // 
            this.cboVALVE_TYPE.FormattingEnabled = true;
            this.cboVALVE_TYPE.Location = new System.Drawing.Point(369, 4);
            this.cboVALVE_TYPE.Name = "cboVALVE_TYPE";
            this.cboVALVE_TYPE.Size = new System.Drawing.Size(140, 19);
            this.cboVALVE_TYPE.TabIndex = 164;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(7, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(92, 12);
            this.label17.TabIndex = 163;
            this.label17.Text = "소블록 :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboBLOCK_CODE
            // 
            this.cboBLOCK_CODE.FormattingEnabled = true;
            this.cboBLOCK_CODE.Location = new System.Drawing.Point(103, 4);
            this.cboBLOCK_CODE.Name = "cboBLOCK_CODE";
            this.cboBLOCK_CODE.Size = new System.Drawing.Size(136, 19);
            this.cboBLOCK_CODE.TabIndex = 162;
            this.cboBLOCK_CODE.SelectedIndexChanged += new System.EventHandler(this.cboBLOCK_CODE_SelectedIndexChanged);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Gold;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Location = new System.Drawing.Point(0, 190);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(623, 4);
            this.pictureBox3.TabIndex = 149;
            this.pictureBox3.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.btnNew);
            this.panel6.Controls.Add(this.btnSave);
            this.panel6.Controls.Add(this.btnDelete);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 162);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(623, 28);
            this.panel6.TabIndex = 147;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(4, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 12);
            this.label3.TabIndex = 164;
            this.label3.Text = "고정유출 감압밸브 감시 관리";
            // 
            // btnNew
            // 
            this.btnNew.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnNew.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Clear;
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNew.Location = new System.Drawing.Point(351, 0);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 26);
            this.btnNew.TabIndex = 159;
            this.btnNew.Text = "초기화";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSave.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(441, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 26);
            this.btnSave.TabIndex = 158;
            this.btnSave.Text = "지점 등록";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnDelete.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Delete;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.Location = new System.Drawing.Point(531, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 26);
            this.btnDelete.TabIndex = 157;
            this.btnDelete.Text = "지점 삭제";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 158);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(623, 4);
            this.pictureBox2.TabIndex = 146;
            this.pictureBox2.TabStop = false;
            // 
            // uGridPopup
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPopup.DisplayLayout.Appearance = appearance1;
            this.uGridPopup.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPopup.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPopup.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridPopup.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPopup.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridPopup.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPopup.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPopup.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPopup.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridPopup.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPopup.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPopup.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridPopup.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPopup.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPopup.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridPopup.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridPopup.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPopup.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridPopup.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridPopup.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPopup.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridPopup.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPopup.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPopup.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPopup.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridPopup.Location = new System.Drawing.Point(0, 0);
            this.uGridPopup.Name = "uGridPopup";
            this.uGridPopup.Size = new System.Drawing.Size(623, 158);
            this.uGridPopup.TabIndex = 144;
            this.uGridPopup.Text = "ultraGrid1";
            this.uGridPopup.Click += new System.EventHandler(this.uGridPopup_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(623, 4);
            this.pictureBox1.TabIndex = 105;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnQuery);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.cboSBlock);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cboMBlock);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cboLBlock);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(623, 52);
            this.panel2.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Close2;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.Location = new System.Drawing.Point(526, 22);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 26);
            this.btnClose.TabIndex = 154;
            this.btnClose.Text = "닫기";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Image = global::WaterNet.WP_RTPRValve.Properties.Resources.Query;
            this.btnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuery.Location = new System.Drawing.Point(435, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(90, 26);
            this.btnQuery.TabIndex = 153;
            this.btnQuery.Text = "지점 조회";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(416, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 12);
            this.label9.TabIndex = 148;
            this.label9.Text = "소블록 :";
            // 
            // cboSBlock
            // 
            this.cboSBlock.FormattingEnabled = true;
            this.cboSBlock.Location = new System.Drawing.Point(476, 3);
            this.cboSBlock.Name = "cboSBlock";
            this.cboSBlock.Size = new System.Drawing.Size(140, 19);
            this.cboSBlock.TabIndex = 147;
            this.cboSBlock.SelectedIndexChanged += new System.EventHandler(this.cboSBlock_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(210, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 146;
            this.label8.Text = "중블록 :";
            // 
            // cboMBlock
            // 
            this.cboMBlock.FormattingEnabled = true;
            this.cboMBlock.Location = new System.Drawing.Point(270, 3);
            this.cboMBlock.Name = "cboMBlock";
            this.cboMBlock.Size = new System.Drawing.Size(140, 19);
            this.cboMBlock.TabIndex = 145;
            this.cboMBlock.SelectedIndexChanged += new System.EventHandler(this.cboMBlock_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(4, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 144;
            this.label7.Text = "대블록 :";
            // 
            // cboLBlock
            // 
            this.cboLBlock.FormattingEnabled = true;
            this.cboLBlock.Location = new System.Drawing.Point(64, 3);
            this.cboLBlock.Name = "cboLBlock";
            this.cboLBlock.Size = new System.Drawing.Size(140, 19);
            this.cboLBlock.TabIndex = 143;
            this.cboLBlock.SelectedIndexChanged += new System.EventHandler(this.cboLBlock_SelectedIndexChanged);
            // 
            // frmWPRValveManage1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 11F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 388);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picFrLeft);
            this.Controls.Add(this.PicFrRight);
            this.Controls.Add(this.picFrBottom);
            this.Controls.Add(this.picFrTop);
            this.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmWPRValveManage1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "고정유출 감압밸브 감시 관리";
            this.Load += new System.EventHandler(this.frmWPRValveManage1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWPRValveManage1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFrRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrTop)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeft;
        private System.Windows.Forms.PictureBox PicFrRight;
        private System.Windows.Forms.PictureBox picFrBottom;
        private System.Windows.Forms.PictureBox picFrTop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboSBlock;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboMBlock;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboLBlock;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnClose;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPopup;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboVALVE_TYPE;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cboBLOCK_CODE;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboDELAY_TIME;
        private System.Windows.Forms.TextBox txtPR_VALUE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtALERT_LIMIT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboMANOMETER_NO;
        private System.Windows.Forms.ComboBox cboTAGNAME;
        private System.Windows.Forms.ComboBox cboVALVE_NO;
        private System.Windows.Forms.ComboBox cboMANOMETER_NO_B;
        private System.Windows.Forms.ComboBox cboTAGNAME_B;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
    }
}