﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WP_RTPRValve.FormPopup
{
    /// <summary>
    /// Project ID : WN_WP_A01
    /// Project Explain : 감압밸브감시
    /// Project Developer : 오두석
    /// Project Create Date : 2011.02.09
    /// Form Explain : 시간제어감압밸브감시관리 Popup Form
    /// </summary>
    public partial class frmWPRValveManage2 : Form
    {
        int m_DelayTime = 0;

        public frmWPRValveManage2()
        {
            InitializeComponent();

            this.InitializeGridSetting();

            this.InitializeComboSetting();

            this.InitializeControlSetting();
        }

        private void frmWPRValveManage2_Load(object sender, EventArgs e)
        {
            //=======================================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=======================================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["감압밸브감시ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnSave.Enabled = false;
                this.btnDelete.Enabled = false;
            }

            //==========================================================================================

            this.cboMANOMETER_NO.SelectedIndexChanged += new EventHandler(cboMANOMETER_NO_SelectedIndexChanged);
            this.cboMANOMETER_NO_B.SelectedIndexChanged += new EventHandler(cboMANOMETER_NO_SelectedIndexChanged);

            WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE2 = true;
            this.GetQueryData();
        }

        private void frmWPRValveManage2_FormClosing(object sender, FormClosingEventArgs e)
        {
            WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE2 = false;
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                
                this.GetQueryData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.InitializeControlSetting();

            this.InitializeComboSelectedIndex();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValidation() == false) return;

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            this.SavePRValveMonitorPointData();

            this.SavePRValveMonitorOptionData();

            this.InitializeControlSetting();

            this.InitializeComboSelectedIndex();

            this.GetQueryData();

            this.Cursor = System.Windows.Forms.Cursors.Default;

            MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.IsValidation() == false) return;

            DialogResult oDRtn = MessageBox.Show("선택한 항목을 삭제하겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oDRtn == DialogResult.Yes)
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                this.DeletePRValveMonitorPointData();

                this.DeletePRValveMonitorOptionData();

                this.InitializeControlSetting();

                this.GetQueryData();

                this.Cursor = System.Windows.Forms.Cursors.Default;

                MessageBox.Show("정상적으로 처리했습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        
        #endregion


        #region Control Events

        #region - UltraGrid Events

        private void uGridPopup_Click(object sender, EventArgs e)
        {
            if (this.uGridPopup.Rows.Count <= 0) return;

            this.InitializeControlSetting();

            this.SetDataBySelectedRecordOnGrid();
        }

        #endregion

        #region - Combo Events

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.InitializeControlSetting();

            string strLOC_CODE = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLOC_CODE, 1);
            WQ_Function.SetCombo_SmallBlock(this.cboBLOCK_CODE, EMFrame.statics.AppStatic.USER_SGCCD, strLOC_CODE, 0);

            if (strLOC_CODE.Trim() == "") return;
            string strFTR_IDN = WQ_Function.GetAllSmallBlockFTRIDNInMediumBlock(strLOC_CODE);
            WP_AppStatic.SetCombo_NoOfPressureReducingValveOnSmallBlockByLayer(this.cboVALVE_NO, strFTR_IDN, "MOF012", 0);

            //string strFTR_IDN = WQ_Function.GetMediumBlockFTR_IDN(strLOC_CODE);
            //WP_AppStatic.Viewmap_SelectedBlock(strFTR_IDN, 0);
        }
        
        private void cboSBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLOC_CODE = WQ_Function.SplitToCode(this.cboSBlock.Text);
            if (this.cboBLOCK_CODE.Items.Count > 0)
            {
                this.cboBLOCK_CODE.SelectedIndex = this.cboSBlock.SelectedIndex - 1;
            }
            //string strFTR_IDN = WQ_Function.GetSmallBlockFTR_IDN(strLOC_CODE);
            //WP_AppStatic.Viewmap_SelectedBlock(strFTR_IDN, 1);
        }

        private void cboBLOCK_CODE_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLOC_CODE = WQ_Function.SplitToCode(this.cboBLOCK_CODE.Text);
            //string strFTR_IDN = WQ_Function.GetSmallBlockFTR_IDN(strLOC_CODE);
            //WP_AppStatic.SetCombo_NoOfPressureReducingValveOnSmallBlockByLayer(this.cboVALVE_NO, strFTR_IDN, 0);
            WP_AppStatic.SetCombo_NoOfMeasuringInstrumentOnSmallBlock(this.cboMANOMETER_NO, strLOC_CODE, 0);
            WP_AppStatic.SetCombo_NoOfMeasuringInstrumentOnSmallBlock(this.cboMANOMETER_NO_B, strLOC_CODE, 0);
        }

        private void cboMANOMETER_NO_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strFTR_IDN = ((ComboBox)sender).Text;
            if (((ComboBox)sender).Name.Equals("cboMANOMETER_NO"))
            {
                WP_AppStatic.SetCombo_TAGOfMeasuringInstrumentOnSmallBlock(this.cboTAGNAME, strFTR_IDN, 0);
            }
            else if (((ComboBox)sender).Name.Equals("cboMANOMETER_NO_B"))
            {
                WP_AppStatic.SetCombo_TAGOfMeasuringInstrumentOnSmallBlock2(this.cboTAGNAME_B, strFTR_IDN, 0);
            }

            WP_AppStatic.Viewmap_SelectedManometer(strFTR_IDN);
        }

        private void cboVALVE_NO_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strFTR_IDN = this.cboVALVE_NO.Text;
            WP_AppStatic.Viewmap_SelectedValve(strFTR_IDN);
        }

        private void cboDELAY_TIME_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_DelayTime = cboDELAY_TIME.SelectedIndex;
            this.InitializeComboSelectedIndex();
        }

        #endregion

        #endregion


        #region User Function

        #region - Initialize Function

        /// <summary>
        /// 초기 실행시 그리드 설정
        /// </summary>
        private void InitializeGridSetting()
        {
            #region uGridPopup : 시간제어감압밸브감시지점 Grid
            
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VALVE_NO";
            oUltraGridColumn.Header.Caption = "감압밸브ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ALERT_LIMIT";
            oUltraGridColumn.Header.Caption = "경보범위(±)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DELAY_TIME";
            oUltraGridColumn.Header.Caption = "시간제어";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MANOMETER_NO_B";
            oUltraGridColumn.Header.Caption = "계측기ID(전단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME_B";
            oUltraGridColumn.Header.Caption = "태그명(전단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MANOMETER_NO";
            oUltraGridColumn.Header.Caption = "계측기ID(후단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridPopup.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "태그명(후단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(uGridPopup);

            #endregion
        }

        /// <summary>
        /// 초기 실행시 콤보 설정
        /// </summary>
        private void InitializeComboSetting()
        {
            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);
            WP_AppStatic.SetCombo_PRValveType(this.cboVALVE_TYPE, 1, false);
            this.cboVALVE_TYPE.Enabled = false;

            //옵션 1 시작 시/분
            this.ucSH1.ucDataType = "시"; this.ucSH1.ucEnabled = false; this.ucSM1.ucDataType = "분"; this.ucSM1.ucEnabled = false;
            //옵션 1 종료 시/분
            this.ucEH1.ucDataType = "시"; this.ucEH1.ucEnabled = true; this.ucEM1.ucDataType = "분"; this.ucEM1.ucEnabled = true;
            //옵션 2 시작 시/분
            this.ucSH2.ucDataType = "시"; this.ucSH2.ucEnabled = true; this.ucSM2.ucDataType = "분"; this.ucSM2.ucEnabled = true;
            //옵션 2 종료 시/분
            this.ucEH2.ucDataType = "시"; this.ucEH2.ucEnabled = true; this.ucEM2.ucDataType = "분"; this.ucEM2.ucEnabled = true;
            //옵션 3 시작 시/분
            this.ucSH3.ucDataType = "시"; this.ucSH3.ucEnabled = true; this.ucSM3.ucDataType = "분"; this.ucSM3.ucEnabled = true;
            //옵션 3 종료 시/
            this.ucEH3.ucDataType = "시"; this.ucEH3.ucEnabled = true; this.ucEM3.ucDataType = "분"; this.ucEM3.ucEnabled = true;
            //옵션 4 시작 시/분
            this.ucSH4.ucDataType = "시"; this.ucSH4.ucEnabled = true; this.ucSM4.ucDataType = "분"; this.ucSM4.ucEnabled = true;
            //옵션 4 종료 시/분
            this.ucEH4.ucDataType = "시"; this.ucEH4.ucEnabled = true; this.ucEM4.ucDataType = "분"; this.ucEM4.ucEnabled = true;
            //옵션 5 시작 시/분
            this.ucSH5.ucDataType = "시"; this.ucSH5.ucEnabled = true; this.ucSM5.ucDataType = "분"; this.ucSM5.ucEnabled = true;
            //옵션 5 종료 시/분
            this.ucEH5.ucDataType = "시"; this.ucEH5.ucEnabled = true; this.ucEM5.ucDataType = "분"; this.ucEM5.ucEnabled = true;
            //옵션 6 시작 시/분
            this.ucSH6.ucDataType = "시"; this.ucSH6.ucEnabled = true; this.ucSM6.ucDataType = "분"; this.ucSM6.ucEnabled = true;
            //옵션 6 종료 시/분
            this.ucEH6.ucDataType = "시"; this.ucEH6.ucEnabled = true; this.ucEM6.ucDataType = "분"; this.ucEM6.ucEnabled = true;
            //옵션 7 시작 시/분
            this.ucSH7.ucDataType = "시"; this.ucSH7.ucEnabled = true; this.ucSM7.ucDataType = "분"; this.ucSM7.ucEnabled = true;
            //옵션 7 종료 시/분
            this.ucEH7.ucDataType = "시"; this.ucEH7.ucEnabled = true; this.ucEM7.ucDataType = "분"; this.ucEM7.ucEnabled = true;
            //옵션 8 시작 시/분
            this.ucSH8.ucDataType = "시"; this.ucSH8.ucEnabled = true; this.ucSM8.ucDataType = "분"; this.ucSM8.ucEnabled = true;
            //옵션 8 종료 시/분
            this.ucEH8.ucDataType = "시"; this.ucEH8.ucEnabled = true; this.ucEM8.ucDataType = "분"; this.ucEM8.ucEnabled = true;
            //옵션 9 시작 시/분
            this.ucSH9.ucDataType = "시"; this.ucSH9.ucEnabled = true; this.ucSM9.ucDataType = "분"; this.ucSM9.ucEnabled = true;
            //옵션 9 종료 시/분
            this.ucEH9.ucDataType = "시"; this.ucEH9.ucEnabled = true; this.ucEM9.ucDataType = "분"; this.ucEM9.ucEnabled = true;
            //옵션 10 시작 시/분
            this.ucSH10.ucDataType = "시"; this.ucSH10.ucEnabled = true; this.ucSM10.ucDataType = "분"; this.ucSM10.ucEnabled = true;
            //옵션 10 종료 시/분
            this.ucEH10.ucDataType = "시"; this.ucEH10.ucEnabled = true; this.ucEM10.ucDataType = "분"; this.ucEM10.ucEnabled = true;
        }

        /// <summary>
        /// 초기 이후에도 초기화가 필요한 TextBox 등 Control을 초기화한다.
        /// </summary>
        private void InitializeControlSetting()
        {
            WP_AppStatic.SetCombo_DelayTime(this.cboDELAY_TIME, 2);

            this.txtALERT_LIMIT.Text = "0.5";
            if (this.cboMANOMETER_NO.Items.Count > 0) this.cboMANOMETER_NO.SelectedIndex = 0;
            if (this.cboMANOMETER_NO_B.Items.Count > 0) this.cboMANOMETER_NO_B.SelectedIndex = 0;
            if (this.cboVALVE_NO.Items.Count > 0) this.cboVALVE_NO.SelectedIndex = 0;
            if (this.cboTAGNAME.Items.Count > 0) this.cboTAGNAME.SelectedIndex = 0;
            if (this.cboTAGNAME_B.Items.Count > 0) this.cboTAGNAME_B.SelectedIndex = 0;

            this.txtPR_VALUE1.Text = "";
            this.txtPR_VALUE2.Text = "";
            this.txtPR_VALUE3.Text = "";
            this.txtPR_VALUE4.Text = "";
            this.txtPR_VALUE5.Text = "";
            this.txtPR_VALUE6.Text = "";
            this.txtPR_VALUE7.Text = "";
            this.txtPR_VALUE8.Text = "";
            this.txtPR_VALUE9.Text = "";
            this.txtPR_VALUE10.Text = "";
        }
        
        /// <summary>
        /// 특정 콤보들의 SelectedIndex를 특정 값으로 변경
        /// </summary>
        /// <returns></returns>
        private void InitializeComboSelectedIndex()
        {
            this.ucSH1.ucSelectedIndex = 1; this.ucSM1.ucSelectedIndex = 0;
            this.ucEH1.ucSelectedIndex = 7; this.ucEM1.ucSelectedIndex = 0;
            this.ucSH2.ucSelectedIndex = 0; this.ucSM2.ucSelectedIndex = m_DelayTime;
            this.ucEH2.ucSelectedIndex = 0; this.ucEM2.ucSelectedIndex = 0;
            this.ucSH3.ucSelectedIndex = 0; this.ucSM3.ucSelectedIndex = m_DelayTime;
            this.ucEH3.ucSelectedIndex = 0; this.ucEM3.ucSelectedIndex = 0;
            this.ucSH4.ucSelectedIndex = 0; this.ucSM4.ucSelectedIndex = m_DelayTime;
            this.ucEH4.ucSelectedIndex = 0; this.ucEM4.ucSelectedIndex = 0;
            this.ucSH5.ucSelectedIndex = 0; this.ucSM5.ucSelectedIndex = m_DelayTime;
            this.ucEH5.ucSelectedIndex = 0; this.ucEM5.ucSelectedIndex = 0;
            this.ucSH6.ucSelectedIndex = 0; this.ucSM6.ucSelectedIndex = m_DelayTime;
            this.ucEH6.ucSelectedIndex = 0; this.ucEM6.ucSelectedIndex = 0;
            this.ucSH7.ucSelectedIndex = 0; this.ucSM7.ucSelectedIndex = m_DelayTime;
            this.ucEH7.ucSelectedIndex = 0; this.ucEM7.ucSelectedIndex = 0;
            this.ucSH8.ucSelectedIndex = 0; this.ucSM8.ucSelectedIndex = m_DelayTime;
            this.ucEH8.ucSelectedIndex = 0; this.ucEM8.ucSelectedIndex = 0;
            this.ucSH9.ucSelectedIndex = 0; this.ucSM9.ucSelectedIndex = m_DelayTime;
            this.ucEH9.ucSelectedIndex = 0; this.ucEM9.ucSelectedIndex = 0;
            this.ucSH10.ucSelectedIndex = 0; this.ucSM10.ucSelectedIndex = m_DelayTime;
            this.ucEH10.ucSelectedIndex = 0; this.ucEM10.ucSelectedIndex = 0;
        }

        #endregion

        #region - SQL Function

        /// <summary>
        /// 조회 조건으로 Query를 실행하여 Grid에 Set한다.
        /// </summary>
        private void GetQueryData()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;
            string strVALVE_TYPE = string.Empty;

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = " AND A.BLOCK_CODE IN (" + strBLOCK_CD_S + ")";
                }
                else
                {
                    strBLOCK_SQL = " AND A.BLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }

            strVALVE_TYPE = WQ_Function.SplitToCode(this.cboVALVE_TYPE.Text);
            
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   (SELECT LOC_NAME || ' (' || LOC_CODE || ')' FROM CM_LOCATION WHERE FTR_CODE = 'BZ003' AND LOC_CODE = A.BLOCK_CODE) AS BLOCK,");
            oStringBuilder.AppendLine("A.VALVE_NO, A.ALERT_LIMIT, A.DELAY_TIME, A.MANOMETER_NO, A.TAGNAME,  A.MANOMETER_NO_B, A.TAGNAME_B");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_POINT A");
            oStringBuilder.AppendLine("WHERE    A.VALVE_TYPE = '" + strVALVE_TYPE + "'" + strBLOCK_SQL);
            oStringBuilder.AppendLine("ORDER BY A.VALVE_NO");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WP_POINT");

            this.uGridPopup.DataSource = pDS.Tables["WP_POINT"].DefaultView;

            FormManager.SetGridStyle_PerformAutoResize(this.uGridPopup);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 감압밸브감시지점 Data를 저장 한다.
        /// </summary>
        private void SavePRValveMonitorPointData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strBLOCK_CODE = WQ_Function.SplitToCode(this.cboBLOCK_CODE.Text);
            string strVALVE_NO = this.cboVALVE_NO.Text;
            string strVALVE_TYPE = WQ_Function.SplitToCode(this.cboVALVE_TYPE.Text);
            string strALERT_LIMIT = this.txtALERT_LIMIT.Text;
            string strDELAY_TIME = this.cboDELAY_TIME.Text;
            string strMANOMETER_NO = this.cboMANOMETER_NO.Text;
            string strTAGNAME = this.cboTAGNAME.Text;
            string strMANOMETER_NO_B = this.cboMANOMETER_NO_B.Text;
            string strTAGNAME_B = this.cboTAGNAME_B.Text;
            string strREG_DATE = WQ_Function.StringToDateTime(DateTime.Now);

            string strParam = string.Empty;

            this.DeletePRValveMonitorPointData();

            strParam = "('" + strVALVE_NO + "', '" + strVALVE_TYPE + "', '" + strALERT_LIMIT + "', '" + strDELAY_TIME + "', '" + strMANOMETER_NO_B + "', '" + strTAGNAME_B + "', '" + strMANOMETER_NO
                     + "', '" + strTAGNAME + "', '" + strBLOCK_CODE + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + strREG_DATE + "')";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("INSERT   INTO    WP_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("(VALVE_NO, VALVE_TYPE, ALERT_LIMIT, DELAY_TIME, MANOMETER_NO_B, TAGNAME_B, MANOMETER_NO, TAGNAME, BLOCK_CODE, REG_ID, REG_DATE)");
            oStringBuilder.AppendLine("VALUES");
            oStringBuilder.AppendLine(strParam);

            WP_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 감압밸브감시옵션 Data를 저장 한다.
        /// </summary>
        private void SavePRValveMonitorOptionData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strOPTION_NO = string.Empty;
            string strSTART_OPTION = string.Empty;
            string strEND_OPTION = string.Empty;
            string strPR_VALUE = string.Empty;

            string strVALVE_NO = this.cboVALVE_NO.Text;
            string strREG_DATE = WQ_Function.StringToDateTime(DateTime.Now);
            
            string strParam = string.Empty;

            this.DeletePRValveMonitorOptionData();

            for (int i = 1; i <= 10; i++)
            {
                strOPTION_NO = i.ToString();
                strSTART_OPTION = string.Empty;
                strEND_OPTION = string.Empty;
                switch (strOPTION_NO)
                {
                    case "1":
                        strSTART_OPTION = this.ucSH1.ucText.Trim() + this.ucSH1.ucText.Trim();
                        strEND_OPTION = this.ucEH1.ucText.Trim() + this.ucEM1.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE1.Text;
                        break;
                    case "2":
                        strSTART_OPTION = this.ucSH2.ucText.Trim() + this.ucSM2.ucText.Trim();
                        strEND_OPTION = this.ucEH2.ucText.Trim() + this.ucEM2.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE2.Text;
                        break;
                    case "3":
                        strSTART_OPTION = this.ucSH3.ucText.Trim() + this.ucSM3.ucText.Trim();
                        strEND_OPTION = this.ucEH3.ucText.Trim() + this.ucEM3.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE3.Text;
                        break;
                    case "4":
                        strSTART_OPTION = this.ucSH4.ucText.Trim() + this.ucSM4.ucText.Trim();
                        strEND_OPTION = this.ucEH4.ucText.Trim() + this.ucEM4.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE4.Text;
                        break;
                    case "5":
                        strSTART_OPTION = this.ucSH5.ucText.Trim() + this.ucSM5.ucText.Trim();
                        strEND_OPTION = this.ucEH5.ucText.Trim() + this.ucEM5.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE5.Text;
                        break;
                    case "6":
                        strSTART_OPTION = this.ucSH6.ucText.Trim() + this.ucSM6.ucText.Trim();
                        strEND_OPTION = this.ucEH6.ucText.Trim() + this.ucEM6.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE6.Text;
                        break;
                    case "7":
                        strSTART_OPTION = this.ucSH7.ucText.Trim() + this.ucSM7.ucText.Trim();
                        strEND_OPTION = this.ucEH7.ucText.Trim() + this.ucEM7.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE7.Text;
                        break;
                    case "8":
                        strSTART_OPTION = this.ucSH8.ucText.Trim() + this.ucSM8.ucText.Trim();
                        strEND_OPTION = this.ucEH8.ucText.Trim() + this.ucEM8.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE8.Text;
                        break;
                    case "9":
                        strSTART_OPTION = this.ucSH9.ucText.Trim() + this.ucSM9.ucText.Trim();
                        strEND_OPTION = this.ucEH9.ucText.Trim() + this.ucEM9.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE9.Text;
                        break;
                    case "10":
                        strSTART_OPTION = this.ucSH10.ucText.Trim() + this.ucSM10.ucText.Trim();
                        strEND_OPTION = this.ucEH10.ucText.Trim() + this.ucEM10.ucText.Trim();
                        strPR_VALUE = this.txtPR_VALUE10.Text;
                        break;
                }

                if (strEND_OPTION == "2400") strEND_OPTION = "0000";

                if (strSTART_OPTION.Length == 4 && strEND_OPTION.Length == 4 && strPR_VALUE.Length > 0)
                {
                    strParam = "('" + strVALVE_NO + "', '" + strOPTION_NO + "', '" + strSTART_OPTION + "', '" + strEND_OPTION + "', '" + strPR_VALUE + "', '" + EMFrame.statics.AppStatic.USER_ID + "', '" + strREG_DATE + "')";
                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("INSERT   INTO    WP_RT_MONITOR_OPTION");
                    oStringBuilder.AppendLine("(VALVE_NO, OPTION_NO, START_OPTION, END_OPTION, PR_VALUE, REG_ID, REG_DATE)");
                    oStringBuilder.AppendLine("VALUES");
                    oStringBuilder.AppendLine(strParam);

                    WP_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
                }
            }
        }

        /// <summary>
        /// 감압밸브감시지점 Data를 삭제 한다.
        /// </summary>
        private void DeletePRValveMonitorPointData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strVALVE_NO = this.cboVALVE_NO.Text;

            string strParam = string.Empty;

            strParam = "WHERE    VALVE_NO = '" + strVALVE_NO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WP_RT_MONITOR_POINT");
            oStringBuilder.AppendLine(strParam);

            WP_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        /// <summary>
        /// 감압밸브감시옵션 Data를 삭제 한다.
        /// </summary>
        private void DeletePRValveMonitorOptionData()
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strVALVE_NO = this.cboVALVE_NO.Text;

            string strParam = string.Empty;

            strParam = "WHERE    VALVE_NO = '" + strVALVE_NO + "'";
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("DELETE   WP_RT_MONITOR_OPTION");
            oStringBuilder.AppendLine(strParam);

            WP_AppStatic.ORACLE_MANAGER.ExecuteScript(oStringBuilder.ToString(), null);
        }

        #endregion

        #region - ETC Function

        /// <summary>
        /// 그리드에서 선택된 Data를 감시관리 필드의 Control에 Set
        /// </summary>
        private void SetDataBySelectedRecordOnGrid()
        {
            this.cboBLOCK_CODE.Items.Clear();
            this.cboBLOCK_CODE.Items.Add(this.uGridPopup.ActiveRow.Cells[0].Text.Trim());
            this.cboBLOCK_CODE.SelectedIndex = 0;

            this.cboVALVE_NO.Items.Clear();
            this.cboVALVE_NO.Items.Add(this.uGridPopup.ActiveRow.Cells[1].Text.Trim());
            this.cboVALVE_NO.SelectedIndex = 0; 

            this.txtALERT_LIMIT.Text = this.uGridPopup.ActiveRow.Cells[2].Text;
            WQ_Function.SetCombo_SelectFindData(this.cboDELAY_TIME, this.uGridPopup.ActiveRow.Cells[3].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboMANOMETER_NO_B, this.uGridPopup.ActiveRow.Cells[6].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboMANOMETER_NO, this.uGridPopup.ActiveRow.Cells[4].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboVALVE_NO, this.uGridPopup.ActiveRow.Cells[1].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboTAGNAME_B, this.uGridPopup.ActiveRow.Cells[7].Text);
            WQ_Function.SetCombo_SelectFindData(this.cboTAGNAME, this.uGridPopup.ActiveRow.Cells[5].Text);

            //운영정보 Control에 Data Set
            string strSHH = string.Empty;
            string strSMM = string.Empty;
            string strEHH = string.Empty;
            string strEMM = string.Empty;
            string strPR_VALUE = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT   OPTION_NO, START_OPTION, END_OPTION, PR_VALUE");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_OPTION");
            oStringBuilder.AppendLine("WHERE    VALVE_NO = '" + this.cboVALVE_NO.Text + "'");
            oStringBuilder.AppendLine("ORDER BY TO_NUMBER(OPTION_NO)");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WP_OPTION");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strSHH = string.Empty;
                    strSMM = string.Empty;
                    strEHH = string.Empty;
                    strEMM = string.Empty;
                    strPR_VALUE = string.Empty;

                    strSHH = oDRow["START_OPTION"].ToString().Trim().Substring(0, 2);
                    strSMM = oDRow["START_OPTION"].ToString().Trim().Substring(2, 2);
                    strEHH = oDRow["END_OPTION"].ToString().Trim().Substring(0, 2);
                    strEMM = oDRow["END_OPTION"].ToString().Trim().Substring(2, 2);
                    strPR_VALUE = oDRow["PR_VALUE"].ToString().Trim();

                    if (strEHH == "00") strEHH = "24";

                    switch (oDRow["OPTION_NO"].ToString().Trim())
                    {
                        case "1":
                            this.ucSH1.ucText = strSHH; this.ucSM1.ucText = strSMM; this.ucEH1.ucText = strEHH; this.ucEM1.ucText = strEMM;
                            this.txtPR_VALUE1.Text = strPR_VALUE;
                            break;
                        case "2":
                            this.ucSH2.ucText = strSHH; this.ucSM2.ucText = strSMM; this.ucEH2.ucText = strEHH; this.ucEM2.ucText = strEMM;
                            this.txtPR_VALUE2.Text = strPR_VALUE;
                            break;
                        case "3":
                            this.ucSH3.ucText = strSHH; this.ucSM3.ucText = strSMM; this.ucEH3.ucText = strEHH; this.ucEM3.ucText = strEMM;
                            this.txtPR_VALUE3.Text = strPR_VALUE;
                            break;
                        case "4":
                            this.ucSH4.ucText = strSHH; this.ucSM4.ucText = strSMM; this.ucEH4.ucText = strEHH; this.ucEM4.ucText = strEMM;
                            this.txtPR_VALUE4.Text = strPR_VALUE;
                            break;
                        case "5":
                            this.ucSH5.ucText = strSHH; this.ucSM5.ucText = strSMM; this.ucEH5.ucText = strEHH; this.ucEM5.ucText = strEMM;
                            this.txtPR_VALUE5.Text = strPR_VALUE;
                            break;
                        case "6":
                            this.ucSH6.ucText = strSHH; this.ucSM6.ucText = strSMM; this.ucEH6.ucText = strEHH; this.ucEM6.ucText = strEMM;
                            this.txtPR_VALUE6.Text = strPR_VALUE;
                            break;
                        case "7":
                            this.ucSH7.ucText = strSHH; this.ucSM7.ucText = strSMM; this.ucEH7.ucText = strEHH; this.ucEM7.ucText = strEMM;
                            this.txtPR_VALUE7.Text = strPR_VALUE;
                            break;
                        case "8":
                            this.ucSH8.ucText = strSHH; this.ucSM8.ucText = strSMM; this.ucEH8.ucText = strEHH; this.ucEM8.ucText = strEMM;
                            this.txtPR_VALUE8.Text = strPR_VALUE;
                            break;
                        case "9":
                            this.ucSH9.ucText = strSHH; this.ucSM9.ucText = strSMM; this.ucEH9.ucText = strEHH; this.ucEM9.ucText = strEMM;
                            this.txtPR_VALUE9.Text = strPR_VALUE;
                            break;
                        case "10":
                            this.ucSH10.ucText = strSHH; this.ucSM10.ucText = strSMM; this.ucEH10.ucText = strEHH; this.ucEM10.ucText = strEMM;
                            this.txtPR_VALUE10.Text = strPR_VALUE;
                            break;
                    }
                }
            }

            pDS.Dispose();
        }

        /// <summary>
        /// 저장하는 Data를 검증한다.
        /// </summary>
        /// <returns>True / False</returns>
        private bool IsValidation()
        {
            bool blRtn = true;

            if (this.cboBLOCK_CODE.Text == "") blRtn = false;
            if (this.cboVALVE_TYPE.Text == "") blRtn = false;
            if (this.cboDELAY_TIME.Text == "") blRtn = false;
            if (this.txtALERT_LIMIT.Text.Trim() == "") blRtn = false;
            if (this.cboMANOMETER_NO.Text.Trim() == "") blRtn = false;
            if (this.cboVALVE_NO.Text.Trim() == "") blRtn = false;
            if (this.cboTAGNAME.Text.Trim() == "") blRtn = false;

            if (this.txtPR_VALUE1.Text.Trim() == "") blRtn = false;

            if (blRtn == false)
            {
                MessageBox.Show("시간제어감압밸브 감시정보를 입력(또는 선택)해 주십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return blRtn;
        }
        
        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE2 = false;
            this.Dispose();
            this.Close();
        }

        #endregion

        #endregion
    }
}
