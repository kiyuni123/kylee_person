﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

#region UltraGrid를 사용=>namespace선언

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

#endregion

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WP_RTPRValve.FormPopup
{
    /// <summary>
    /// Project ID : WN_WP_A01
    /// Project Explain : 감압밸브감시
    /// Project Developer : 오두석
    /// Project Create Date : 2011.02.16
    /// Form Explain : 감압밸브 경보이력 Popup Form
    /// </summary>
    public partial class frmRTAlertHistory : Form
    {
        public frmRTAlertHistory()
        {
            InitializeComponent();

            this.InitializeGridSetting();
        }

        private void frmRTAlertHistory_Load(object sender, EventArgs e)
        {
            WP_AppStatic.IS_SHOW_FORM_ALERT_HIS = true;

            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);

            WP_AppStatic.SetCombo_PRValveType(this.cboVALVE_TYPE, 0, true);

            WQ_Function.SetUDateTime_MaskInput(this.uDTAlertS, 0, 0);
            WQ_Function.SetUDateTime_ModifyDay(this.uDTAlertS, -7);
            WQ_Function.SetUDateTime_MaskInput(this.uDTAlertE, 0, 0);
        }

        private void frmRTAlertHistory_FormClosing(object sender, FormClosingEventArgs e)
        {
            WP_AppStatic.IS_SHOW_FORM_ALERT_HIS = false;
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.GetAlertHistoryData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.ClosePopup();
        }

        #endregion


        #region Control Events

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMFTRIDN = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strMFTRIDN, 1);
        }

        private void uGridHistory_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.uGridHistory.Rows.Count <= 0) return;

            string strFTR_IDN = this.uGridHistory.ActiveRow.Cells[1].Text;
            WP_AppStatic.Viewmap_SelectedValve(strFTR_IDN);
        }

        #endregion


        #region User Function

        #region - Initialize Control

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitializeGridSetting()
        {
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VALVE_NO";
            oUltraGridColumn.Header.Caption = "감압밸브일련번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VALVE_TYPE";
            oUltraGridColumn.Header.Caption = "감압밸브타입";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CUR_VALUE";
            oUltraGridColumn.Header.Caption = "측정압력";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PR_VALUE";
            oUltraGridColumn.Header.Caption = "감압설정치";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ALERT_LIMIT";
            oUltraGridColumn.Header.Caption = "경보범위(±)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = uGridHistory.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ALERT_DATE";
            oUltraGridColumn.Header.Caption = "측정일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.uGridHistory);
        }

        #endregion

        #region - SQL Function
        /// <summary>
        /// 조회 조건으로 Query를 실행하여 Grid에 Set한다.
        /// </summary>
        private void GetAlertHistoryData()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            StringBuilder oStringBuilder = new StringBuilder();

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;
            string strSDate = string.Empty;
            string strEDate = string.Empty;
            string strVALVE_TYPE = string.Empty;
            string strVALVE_TYPE_CD = string.Empty;

            DataSet pDS = new DataSet();

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = "AND PNT.BLOCK_CODE IN (" + strBLOCK_CD_S + ")";
                }
                else
                {
                    strBLOCK_SQL = "AND PNT.BLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }

            strSDate = WQ_Function.StringToDateTime(this.uDTAlertS.DateTime);
            strEDate = WQ_Function.StringToDateTime(this.uDTAlertE.DateTime);
            strVALVE_TYPE = this.cboVALVE_TYPE.Text;
            strVALVE_TYPE_CD = WQ_Function.SplitToCode(strVALVE_TYPE).Trim();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   (SELECT LOC_NAME || ' (' || LOC_CODE || ')' FROM CM_LOCATION WHERE FTR_CODE = 'BZ003' AND LOC_CODE = PNT.BLOCK_CODE) AS BLOCK, ");
            oStringBuilder.AppendLine("         HIS.VALVE_NO, (SELECT CODE_NAME || ' (' || CODE || ')' FROM CM_CODE WHERE PCODE = '6001' AND CODE = PNT.VALVE_TYPE) AS VALVE_TYPE,");
            oStringBuilder.AppendLine("         HIS.CUR_VALUE, HIS.PR_VALUE, PNT.ALERT_LIMIT, TO_CHAR(TO_DATE(HIS.ALERT_DATE, 'RRRRMMDDHH24MISS'), 'RRRR-MM-DD HH24:MI') AS ALERT_DATE");
            oStringBuilder.AppendLine("FROM     WP_RT_ALERT_HISTORY HIS, WP_RT_MONITOR_POINT PNT");
            oStringBuilder.AppendLine("WHERE    PNT.VALVE_TYPE LIKE '%" + strVALVE_TYPE_CD + "%'");
            oStringBuilder.AppendLine("         " + strBLOCK_SQL);
            oStringBuilder.AppendLine("         AND HIS.VALVE_NO = PNT.VALVE_NO");
            oStringBuilder.AppendLine("         AND TO_DATE(SUBSTR(HIS.ALERT_DATE, 0, 8), 'RRRR-MM-DD') BETWEEN TO_DATE('" + strSDate + "', 'RRRR-MM-DD') AND TO_DATE('" + strEDate + "', 'RRRR-MM-DD')");
            oStringBuilder.AppendLine("ORDER BY BLOCK, ALERT_DATE DESC");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "WP_RT_ALERT_HISTORY");

            this.uGridHistory.DataSource = pDS.Tables["WP_RT_ALERT_HISTORY"].DefaultView;

            if (this.uGridHistory.Rows.Count > 0) this.uGridHistory.Rows[0].Activated = true;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.uGridHistory);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #region - ETC Function

        /// <summary>
        /// 팝업 Form Close
        /// </summary>
        private void ClosePopup()
        {
            WP_AppStatic.IS_SHOW_FORM_ALERT_HIS = false;
            this.Dispose();
            this.Close();
        }

        #endregion

        #endregion

    }
}
