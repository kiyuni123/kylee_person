﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

using ChartFX.WinForms;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WP_RTPRValve.FormPopup
{
    /// <summary>
    /// Project ID : WN_WP_A01
    /// Project Explain : 감압밸브감시
    /// Project Developer : 오두석
    /// Project Create Date : 2011.02.10
    /// Form Explain : 감압밸브실시간데이터 감시 및 경보처리 Popup Form
    /// </summary>
    public partial class frmRTData : Form
    {
        public frmRTData()
        {
            InitializeComponent();

            this.InitializeGridSetting();
            this.InitializeChartSetting();

            WQ_Function.SetUDateTime_ModifyDay(this.uDTAlertS, -7);

            DateTime now = DateTime.Now;
            int iMinutes = -30;
            uDTAlertS.Value = now.AddMinutes(iMinutes).Year + "-" + now.AddMinutes(iMinutes).Month + "-" + now.AddMinutes(iMinutes).Day
                + " " + now.AddMinutes(iMinutes).Hour + ":" + now.AddMinutes(iMinutes).Minute;
            uDTAlertE.Value = now;
        }

        private void frmRTData_Load(object sender, EventArgs e)
        {
            WP_AppStatic.IS_SHOW_FORM_RT_DATA = true;

            WQ_Function.SetCombo_LargeBlock(this.cboLBlock, EMFrame.statics.AppStatic.USER_SGCCD);
            WP_AppStatic.SetCombo_PRValveType(this.cboVALVE_TYPE, 0, true);

            string stTime = string.Format("{0:yyyyMMddhhmm}", uDTAlertS.Value);
            string edTime = string.Format("{0:yyyyMMddhhmm}", uDTAlertE.Value);

            this.GetRTMonitorData(stTime, edTime);

            if (this.ugRTData.Rows.Count <= 0) return;

            switch (WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[2].Text))
            {
                case "1":
                case "2":
                    this.SetChartByValveType1To2(stTime, edTime);
                    break;
                case "3":
                    this.SetChartByValveType3(stTime, edTime);
                    break;
            }
        }

        private void frmRTData_FormClosing(object sender, FormClosingEventArgs e)
        {
            WP_AppStatic.IS_SHOW_FORM_RT_DATA = false;
        }


        #region Button Events

        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string stTime = string.Format("{0:yyyyMMddhhmm}", uDTAlertS.Value);
                string edTime = string.Format("{0:yyyyMMddhhmm}", uDTAlertE.Value);

                this.GetRTMonitorData(stTime, edTime);

                if (this.ugRTData.Rows.Count <= 0) return;

                switch (WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[2].Text))
                {
                    case "1":
                    case "2":
                        this.SetChartByValveType1To2(stTime, edTime);
                        break;
                    case "3":
                        this.SetChartByValveType3(stTime, edTime);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            WP_AppStatic.IS_SHOW_FORM_RT_DATA = false;
            this.Close();
        }

        #endregion


        #region Control Events

        private void ugRTData_Click(object sender, EventArgs e)
        {
            if (this.ugRTData.Rows.Count <= 0) return;

            string stTime = string.Format("{0:yyyyMMddhhmm}", uDTAlertS.Value);
            string edTime = string.Format("{0:yyyyMMddhhmm}", DateTime.Parse(ugRTData.ActiveRow.Cells["CUR_DT"].Value.ToString()).ToString("yyyyMMddhhmm"));

            switch (WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[2].Text))
            {
                case "1":
                case "2":
                    this.SetChartByValveType1To2(stTime, edTime);
                    break;
                case "3":
                    this.SetChartByValveType3(stTime, edTime);
                    break;
            }
        }

        private void ugRTData_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (this.ugRTData.Rows.Count <= 0) return;

            WP_AppStatic.Viewmap_SelectedValve(this.ugRTData.ActiveRow.Cells[1].Text);
        }

        private void cboLBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLFTRIDN = WQ_Function.SplitToCode(this.cboLBlock.Text);
            WQ_Function.SetCombo_MediumBlock(this.cboMBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLFTRIDN, 1);
        }

        private void cboMBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strLOC_CODE = WQ_Function.SplitToCode(this.cboMBlock.Text);
            WQ_Function.SetCombo_SmallBlock(this.cboSBlock, EMFrame.statics.AppStatic.USER_SGCCD, strLOC_CODE, 1);
        }
        
        #endregion

        
        #region User Function

        #region - Initialize Control

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitializeGridSetting()
        {
            UltraGridColumn oUltraGridColumn;

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BLOCK";
            oUltraGridColumn.Header.Caption = "블록";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VALVE_NO";
            oUltraGridColumn.Header.Caption = "감압밸브ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VALVE_TYPE";
            oUltraGridColumn.Header.Caption = "감압밸브타입";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MANOMETER_NO";
            oUltraGridColumn.Header.Caption = "계측기번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CUR_DT";
            oUltraGridColumn.Header.Caption = "측정일시";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CUR_VALUE_B";
            oUltraGridColumn.Header.Caption = "수압(전단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CUR_VALUE";
            oUltraGridColumn.Header.Caption = "수압(후단)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ALERT_LIMIT";
            oUltraGridColumn.Header.Caption = "경보범위(±)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = false;   //필드 보이기

            oUltraGridColumn = ugRTData.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DELAY_TIME";
            oUltraGridColumn.Header.Caption = "제어량산출지연시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Width = 120;
            oUltraGridColumn.Hidden = true;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(this.ugRTData);

            //ugRTData.DisplayLayout.CaptionVisible = DefaultableBoolean.True;
        }

        /// <summary>
        /// Chart 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            //Data를 클리어 해야 초기 Chart에 아무것도 안나옴
            this.cFXRT.Data.Clear();
            this.cFXRT.Series.Clear();
            this.cFXRT.DataSourceSettings.Fields.Clear();
            this.cFXRT.AxisX.Staggered = true;
            this.cFXRT.AxisY.Staggered = true;
            this.cFXRT.LegendBox.Dock = DockArea.Top;

            if (this.ugRTData.Rows.Count > 0)
            {
                TitleDockable td = new TitleDockable();
                td.Font = new Font("굴림", 10, FontStyle.Bold);
                td.TextColor = Color.DarkBlue;
                td.Text = WQ_Function.SplitToCodeName(this.ugRTData.ActiveRow.Cells[2].Text) + " (" + this.ugRTData.ActiveRow.Cells[1].Text + ")";
                this.cFXRT.Titles.Clear();
                this.cFXRT.Titles.Add(td);

                switch (WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[2].Text))
                {
                    case "1":
                    case "2":
                        this.cFXRT.Panes[1].Visible = false;
                        break;
                    case "3":
                        this.cFXRT.Panes[1].Visible = true;
                        break;
                }
            }

            //유량비례제어감압밸브인 경우 유량을 별도로 표시하기 위해 Panes를 2개로 유지
            this.cFXRT.Panes[0].AxisY.LabelsFormat.Format = AxisFormat.Number;
            this.cFXRT.Panes[0].AxisY.LabelsFormat.CustomFormat = "##0.000";
            this.cFXRT.Panes[1].AxisY.LabelsFormat.Format = AxisFormat.Number;
            this.cFXRT.Panes[1].AxisY.LabelsFormat.CustomFormat = "##0.000";
        }

        #endregion

        #region - RT Monitor Function

        /// <summary>
        /// 조회 조건에 맞는 실시간 감압밸브 감시지점의 실시간계측값을 그리드에 SET
        /// </summary>
        private void GetRTMonitorData(string startTime, string EndTime)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            string strBLOCK_CD_M = string.Empty;
            string strBLOCK_CD_S = string.Empty;
            string strBLOCK_SQL = string.Empty;
            string strVALVE_TYPE = string.Empty;
            string strVALVE_SQL = string.Empty;

            strBLOCK_CD_M = WQ_Function.SplitToCode(this.cboMBlock.Text);
            strBLOCK_CD_S = WQ_Function.SplitToCode(this.cboSBlock.Text);

            if (strBLOCK_CD_M.Trim() != "")
            {
                if (strBLOCK_CD_S.Trim() == "")
                {
                    strBLOCK_CD_S = WQ_Function.GetAllSmallBlockInMediumBlock(strBLOCK_CD_M);
                    strBLOCK_SQL = "AND PNT.BLOCK_CODE IN (" + strBLOCK_CD_S + ")";
                }
                else
                {
                    strBLOCK_SQL = "AND PNT.BLOCK_CODE = '" + strBLOCK_CD_S + "'";
                }
            }

            strVALVE_TYPE = WQ_Function.SplitToCode(this.cboVALVE_TYPE.Text);
           
            if (strVALVE_TYPE.Trim() != "")
            {
                strVALVE_SQL = "AND PNT.VALVE_TYPE = '" + strVALVE_TYPE + "'";
            }

            DataSet pDS = new DataSet();
            
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   DISTINCT(BLK.LOC_NAME || ' (' || BLK.LOC_CODE || ')') AS BLOCK, ");
            oStringBuilder.AppendLine("         PNT.VALVE_NO, CM.CODE_NAME || ' (' || PNT.VALVE_TYPE || ')' AS VALVE_TYPE, PNT.MANOMETER_NO,");
            oStringBuilder.AppendLine("         TO_CHAR(RT.TIMESTAMP, 'RRRR-MM-DD HH24:MI') AS CUR_DT, DECODE(PNT.TAGNAME_B,NULL,NULL, ROUND(RT_B.VALUE, 3)) AS CUR_VALUE_B, ROUND(RT.VALUE, 3) AS CUR_VALUE, ");
            oStringBuilder.AppendLine("         ALERT_LIMIT AS ALERT_LIMIT, DELAY_TIME AS DELAY_TIME");
            oStringBuilder.AppendLine("FROM     IF_GATHER_REALTIME RT, IF_GATHER_REALTIME RT_B,");
            oStringBuilder.AppendLine("         WP_RT_MONITOR_POINT PNT,");
            oStringBuilder.AppendLine("         CM_LOCATION BLK, ");
            oStringBuilder.AppendLine("         CM_CODE CM");
            oStringBuilder.AppendLine("WHERE    RT.TAGNAME = PNT.TAGNAME AND (RT_B.TAGNAME = PNT.TAGNAME_B OR PNT.TAGNAME_B IS NULL)");
            oStringBuilder.AppendLine("         AND RT.TIMESTAMP = rt_b.timestamp");
            oStringBuilder.AppendLine("         AND RT.TIMESTAMP BETWEEN TO_DATE('" + startTime + "','YYYYMMDDHH24MI') AND TO_DATE('" + EndTime + "','YYYYMMDDHH24MI')");
            oStringBuilder.AppendLine("         " + strBLOCK_SQL);
            oStringBuilder.AppendLine("         " + strVALVE_SQL);
            oStringBuilder.AppendLine("         AND BLK.FTR_CODE = 'BZ003' AND BLK.LOC_CODE = PNT.BLOCK_CODE");
            oStringBuilder.AppendLine("         AND CM.PCODE = '6001' AND CM.CODE = PNT.VALVE_TYPE");
            oStringBuilder.AppendLine("ORDER BY CUR_DT DESC, BLOCK, VALVE_NO");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RTDATA_GRID");

            this.ugRTData.DataSource = pDS.Tables["RTDATA_GRID"];

            if (this.ugRTData.Rows.Count > 0) this.ugRTData.Rows[0].Activated = true;

            //AutoResizeColumes
            FormManager.SetGridStyle_PerformAutoResize(this.ugRTData);

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #region - Chart Function

        /// <summary>
        /// 조회 조건에 맞는 실시간 감압밸브 감시지점의 실시간계측값 중 고정유출제어감압밸브, 시간제어감압밸브 차트
        /// </summary>
        private void SetChartByValveType1To2(string startTime, string EndTime)
        {
            //Data를 클리어 해야 초기 Chart에 아무것도 안나옴
            this.InitializeChartSetting();

            string strVALVE_NO = WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[1].Text);
            string strVALVE_TYPE = WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[2].Text);

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT distinct TO_CHAR(TMP.DT, 'RRRR-MM-DD HH24:MI') AS CUR_DT, decode(pnt.tagname_b, null, null, rt_b.value) cur_value_b, RT.VALUE CUR_VALUE, OPT.PR_VALUE,");
            oStringBuilder.AppendLine("         TO_NUMBER(DECODE(OPT.PR_VALUE, '', 0, OPT.PR_VALUE)) + TO_NUMBER(PNT.ALERT_LIMIT) AS T_LIMIT, ");
            oStringBuilder.AppendLine("         TO_NUMBER(DECODE(OPT.PR_VALUE, '', 0, OPT.PR_VALUE)) - TO_NUMBER(PNT.ALERT_LIMIT) AS L_LIMIT ");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_POINT PNT, WP_RT_MONITOR_OPTION OPT,");
            oStringBuilder.AppendLine("         IF_GATHER_REALTIME RT, IF_GATHER_REALTIME RT_B,");
            oStringBuilder.AppendLine("         (SELECT TO_DATE('" + EndTime + "', 'YYYYMMDDHH24MI') - ((1/24/60) * (ROWNUM) - (1/24/60)) AS DT ");
            oStringBuilder.AppendLine("            FROM DUAL  CONNECT BY ROWNUM  <=  ((TO_DATE('" + EndTime + "','YYYYMMDDHH24MI') - TO_DATE('" + startTime + "','YYYYMMDDHH24MI')) *  ( 24* 60)) + 1) TMP ");
            oStringBuilder.AppendLine("WHERE    PNT.VALVE_NO = '" + strVALVE_NO + "' AND PNT.VALVE_TYPE = '" + strVALVE_TYPE + "' ");
            oStringBuilder.AppendLine("         AND OPT.VALVE_NO = PNT.VALVE_NO");
            oStringBuilder.AppendLine("         AND RT.TAGNAME = PNT.TAGNAME and (rt_b.tagname = pnt.tagname_b or pnt.tagname_b is null)");
            oStringBuilder.AppendLine("         AND RT.TIMESTAMP(+) = TMP.DT AND RT_B.TIMESTAMP(+) = TMP.DT"); // TO_DATE(TO_CHAR(TMP.DT, 'RRRRMMDDHH24MI'), 'RRRRMMDDHH24MI')");
            oStringBuilder.AppendLine("         AND TO_CHAR(RT.TIMESTAMP, 'HH24MI') BETWEEN OPT.START_OPTION AND OPT.END_OPTION");
            oStringBuilder.AppendLine("ORDER BY CUR_DT");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CHART_MAIN");

            if (pDS.Tables.Count > 0 && pDS.Tables[0].Rows.Count > 0)
            {
                this.cFXRT.Data.Series = 5;

                #region -- Series Set

                DataTable oDT = pDS.Tables["CHART_MAIN"];

                this.cFXRT.Series[0].Gallery = Gallery.Lines;
                this.cFXRT.Series[0].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[0].Text = "압력(전단)";
                this.cFXRT.Series[0].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[0].Line.Style = System.Drawing.Drawing2D.DashStyle.Solid;

                this.cFXRT.Series[1].Gallery = Gallery.Lines;
                this.cFXRT.Series[1].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[1].Text = "압력(후단)";
                this.cFXRT.Series[1].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[1].Line.Style = System.Drawing.Drawing2D.DashStyle.Solid;

                this.cFXRT.Series[2].Gallery = Gallery.Step;
                this.cFXRT.Series[2].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[2].Text = "감압밸브 설정값";
                this.cFXRT.Series[2].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[2].Line.Style = System.Drawing.Drawing2D.DashStyle.Dash;

                this.cFXRT.Series[3].Gallery = Gallery.Step;
                this.cFXRT.Series[3].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[3].Text = "감압오차허용최대";
                this.cFXRT.Series[3].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[3].Line.Style = System.Drawing.Drawing2D.DashStyle.DashDot;

                this.cFXRT.Series[4].Gallery = Gallery.Step;
                this.cFXRT.Series[4].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[4].Text = "감압오차허용최소";
                this.cFXRT.Series[4].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[4].Line.Style = System.Drawing.Drawing2D.DashStyle.DashDot;


                int i = 0;
                foreach (DataRow oDRow in oDT.Rows)
                {
                    this.cFXRT.AxisX.Labels[i] = oDRow["CUR_DT"].ToString().Substring(11);

                    this.cFXRT.Data[1, i] = Convert.ToDouble(oDRow["CUR_VALUE"].ToString());
                    this.cFXRT.Data[2, i] = Convert.ToDouble(oDRow["PR_VALUE"].ToString());
                    this.cFXRT.Data[3, i] = Convert.ToDouble(oDRow["T_LIMIT"].ToString());
                    this.cFXRT.Data[4, i] = Convert.ToDouble(oDRow["L_LIMIT"].ToString());
                    if (oDRow["CUR_VALUE_B"] != DBNull.Value)
                    {
                        this.cFXRT.Data[0, i] = Convert.ToDouble(oDRow["CUR_VALUE_B"].ToString());    
                    }
                    
                    i++;
                }

                #endregion
            }

            pDS.Dispose();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        /// <summary>
        /// 조회 조건에 맞는 실시간 감압밸브 감시지점의 실시간계측값 중 유량비례제어감압밸브 차트
        /// </summary>
        private void SetChartByValveType3(string startTime, string EndTime)
        {
            //Data를 클리어 해야 초기 Chart에 아무것도 안나옴
            this.InitializeChartSetting();

            string strVALVE_NO = WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[1].Text);
            string strVALVE_TYPE = WQ_Function.SplitToCode(this.ugRTData.ActiveRow.Cells[2].Text);

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT  DISTINCT  TO_CHAR(TMP.DT, 'RRRR-MM-DD HH24:MI') AS CUR_DT, decode(PNT.TAGNAME_B, NULL, NULL, RT_B.VALUE) CUR_VALUE_B, RT.VALUE CUR_VALUE, RT_F.VALUE CUR_VALUE_F, OPT.PR_VALUE,");
            oStringBuilder.AppendLine("         TO_NUMBER(DECODE(OPT.PR_VALUE, '', 0, OPT.PR_VALUE)) + TO_NUMBER(PNT.ALERT_LIMIT) AS T_LIMIT, ");
            oStringBuilder.AppendLine("         TO_NUMBER(DECODE(OPT.PR_VALUE, '', 0, OPT.PR_VALUE)) - TO_NUMBER(PNT.ALERT_LIMIT) AS L_LIMIT ");
            oStringBuilder.AppendLine("FROM     (SELECT TAGNAME, TAGNAME_B, TAGNAME_F, ALERT_LIMIT, VALVE_NO, VALVE_TYPE FROM WP_RT_MONITOR_POINT) PNT,");
            oStringBuilder.AppendLine("         IF_GATHER_REALTIME RT, IF_GATHER_REALTIME RT_F, IF_GATHER_REALTIME RT_B,WP_RT_MONITOR_OPTION OPT, ");
            //oStringBuilder.AppendLine("         (SELECT TAGNAME, TIMESTAMP, ROUND(VALUE, 3) AS CUR_VALUE FROM IF_GATHER_REALTIME WHERE TIMESTAMP BETWEEN SYSDATE - (30/(24*60)) AND SYSDATE) RT,");
            //oStringBuilder.AppendLine("         (SELECT TAGNAME, TIMESTAMP, ROUND(VALUE, 3) AS CUR_VALUE_F FROM IF_GATHER_REALTIME WHERE TIMESTAMP BETWEEN SYSDATE - (30/(24*60)) AND SYSDATE) RT_F,");
            //oStringBuilder.AppendLine("         (SELECT VALVE_NO, START_OPTION, END_OPTION, PR_VALUE FROM WP_RT_MONITOR_OPTION) OPT,");
            //oStringBuilder.AppendLine("         (SELECT SYSDATE - ((1/24/60) * (ROWNUM)) AS DT FROM DUAL CONNECT BY ROWNUM  <= (((SYSDATE + ( 1 / 24 )) - SYSDATE) * 24 * 30)) TMP");
            oStringBuilder.AppendLine("         (SELECT TO_DATE('" + EndTime + "', 'YYYYMMDDHH24MI') - ((1/24/60) * (ROWNUM) - (1/24/60)) AS DT ");
            oStringBuilder.AppendLine("            FROM DUAL  CONNECT BY ROWNUM  <=  ((TO_DATE('" + EndTime + "','YYYYMMDDHH24MI') - TO_DATE('" + startTime + "','YYYYMMDDHH24MI')) *  ( 24* 60)) + 1) TMP ");
            oStringBuilder.AppendLine("WHERE    PNT.VALVE_NO = '" + strVALVE_NO + "' AND PNT.VALVE_TYPE = '" + strVALVE_TYPE + "' ");
            oStringBuilder.AppendLine("         AND OPT.VALVE_NO = PNT.VALVE_NO");
            oStringBuilder.AppendLine("         AND RT.TAGNAME = PNT.TAGNAME and (RT_B.tagname = pnt.tagname_b or pnt.tagname_b is null)");
            oStringBuilder.AppendLine("         AND RT.TIMESTAMP(+) = TMP.DT ");
            oStringBuilder.AppendLine("         AND RT_F.TAGNAME = PNT.TAGNAME_F");
            oStringBuilder.AppendLine("         AND RT_F.TIMESTAMP(+) = TMP.DT AND RT_B.TIMESTAMP(+) = TMP.DT");
            oStringBuilder.AppendLine("         AND TO_NUMBER(RT_F.VALUE) BETWEEN TO_NUMBER(OPT.START_OPTION) AND TO_NUMBER(OPT.END_OPTION)");
            oStringBuilder.AppendLine("ORDER BY CUR_DT");

      
            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CHART_MAIN");

            if (pDS.Tables.Count > 0 && pDS.Tables[0].Rows.Count > 0)
            {
                this.cFXRT.Data.Series = 6;
 
                #region -- Series Set

                DataTable oDT = pDS.Tables["CHART_MAIN"];


                this.cFXRT.Series[0].Gallery = Gallery.Lines;
                this.cFXRT.Series[0].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[0].Text = "압력(전단)";
                this.cFXRT.Series[0].AxisY = this.cFXRT.Panes[1].AxisY;
                this.cFXRT.Series[0].Line.Style = System.Drawing.Drawing2D.DashStyle.Solid;
                
                this.cFXRT.Series[1].Gallery = Gallery.Lines;
                this.cFXRT.Series[1].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[1].Text = "압력(후단)";
                this.cFXRT.Series[1].AxisY = this.cFXRT.Panes[1].AxisY;
                this.cFXRT.Series[1].Line.Style = System.Drawing.Drawing2D.DashStyle.Solid;

                this.cFXRT.Series[2].Gallery = Gallery.Step;
                this.cFXRT.Series[2].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[2].Text = "감압밸브 설정값";
                this.cFXRT.Series[2].AxisY = this.cFXRT.Panes[1].AxisY;
                this.cFXRT.Series[2].Line.Style = System.Drawing.Drawing2D.DashStyle.Dash;

                this.cFXRT.Series[3].Gallery = Gallery.Step;
                this.cFXRT.Series[3].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[3].Text = "감압오차허용최대";
                this.cFXRT.Series[3].AxisY = this.cFXRT.Panes[1].AxisY;
                this.cFXRT.Series[3].Line.Style = System.Drawing.Drawing2D.DashStyle.DashDot;

                this.cFXRT.Series[4].Gallery = Gallery.Step;
                this.cFXRT.Series[4].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[4].Text = "감압오차허용최소";
                this.cFXRT.Series[4].AxisY = this.cFXRT.Panes[1].AxisY;
                this.cFXRT.Series[4].Line.Style = System.Drawing.Drawing2D.DashStyle.DashDot;

                this.cFXRT.Series[5].Gallery = Gallery.Curve;
                this.cFXRT.Series[5].MarkerShape = MarkerShape.None;
                this.cFXRT.Series[5].Text = "유량";
                this.cFXRT.Series[5].AxisY = this.cFXRT.Panes[0].AxisY;
                this.cFXRT.Series[5].Line.Style = System.Drawing.Drawing2D.DashStyle.Solid;


                int i = 0;
                foreach (DataRow oDRow in oDT.Rows)
                {
                    this.cFXRT.AxisX.Labels[i] = oDRow["CUR_DT"].ToString().Substring(11);
                    this.cFXRT.Data[1, i] = Convert.ToDouble(oDRow["CUR_VALUE"].ToString());
                    this.cFXRT.Data[2, i] = Convert.ToDouble(oDRow["PR_VALUE"].ToString());
                    this.cFXRT.Data[3, i] = Convert.ToDouble(oDRow["T_LIMIT"].ToString());
                    this.cFXRT.Data[4, i] = Convert.ToDouble(oDRow["L_LIMIT"].ToString());
                    this.cFXRT.Data[5, i] = Convert.ToDouble(oDRow["CUR_VALUE_F"].ToString());

                    if (oDRow["CUR_VALUE_B"] != DBNull.Value)
                    {
                        this.cFXRT.Data[0, i] = Convert.ToDouble(oDRow["CUR_VALUE_B"].ToString());
                    }

                    i++;
                }

                #endregion
            }

            pDS.Dispose();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        #endregion

        #endregion
    }
}
