﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WP_RTPRValve
{
    public partial class ucTimeCombo : UserControl
    {
        bool _Enabled = true;

        /// <summary>
        /// 사용가능여부
        /// </summary>
        [Category("데이터")]
        [DefaultValue(true)]
        [Description("Control Enabled")]
        public bool ucEnabled
        {
            get
            {
                return _Enabled;
            }
            set
            {
                _Enabled = value;
                this.cboTime.Enabled = _Enabled;
            }
        }

        string _DataType = "시";

        /// <summary>
        /// Combo에 Set할 데이터 타입
        /// </summary>
        [Category("데이터")]
        [DefaultValue("시")]
        [Description("시(HH24), 분(MI)")]
        public string ucDataType
        {
            get
            {
                return _DataType;
            }
            set
            {
                _DataType = value;
                switch (_DataType)
                {
                    case "시":
                        WP_AppStatic.SetCombo_Hour(this.cboTime, 0);
                        break;
                    case "분":
                        WP_AppStatic.SetCombo_Minute(this.cboTime, 0);
                        break;
                }
                if (this.cboTime.Items.Count > 0) this.cboTime.SelectedIndex = _cboIndex;
            }
        }

        int _cboIndex = 0;

        /// <summary>
        /// Combo에 Set할 기본 Index
        /// </summary>
        [Category("데이터")]
        [DefaultValue(0)]
        [Description("Index")]
        public int ucSelectedIndex
        {
            get
            {
                if (this.cboTime.Items.Count > 0) _cboIndex = this.cboTime.SelectedIndex;   
                return _cboIndex;
            }
            set
            {
                _cboIndex = value;
                if (this.cboTime.Items.Count > 0) this.cboTime.SelectedIndex = _cboIndex;                
            }
        }

        string _Text = string.Empty;

        /// <summary>
        /// Combo의 현재 Text
        /// </summary>
        [Category("데이터")]
        [DefaultValue("")]
        [Description("Index")]
        public string ucText
        {
            get
            {
                _Text = this.cboTime.Text;
                return _Text;
            }
            set
            {
                _Text = value;
                _cboIndex = this.cboTime.FindString(_Text);
                if (this.cboTime.Items.Count > 0) this.cboTime.SelectedIndex = _cboIndex;    
            }
        }

        public ucTimeCombo()
        {
            InitializeComponent();
        }

        private void ucTimeCombo_Load(object sender, EventArgs e)
        {
            switch (_DataType)
            {
                case "시":
                    WP_AppStatic.SetCombo_Hour(this.cboTime, 0);
                    break;
                case "분":
                    WP_AppStatic.SetCombo_Minute(this.cboTime, 0);
                    break;
            }
            
            //_cboIndex = this.cboTime.FindString(_Text);

            if (this.cboTime.Items.Count > 0) this.cboTime.SelectedIndex = _cboIndex;
        }
    }
}
