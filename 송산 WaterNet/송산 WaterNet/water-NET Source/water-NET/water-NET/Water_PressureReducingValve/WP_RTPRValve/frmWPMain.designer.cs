﻿namespace WaterNet.WP_RTPRValve
{
    partial class frmWPMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWPMain));
            this.btnPRValveManage1 = new System.Windows.Forms.Button();
            this.btnAlertHis = new System.Windows.Forms.Button();
            this.btnPRValveManage2 = new System.Windows.Forms.Button();
            this.btnPRValveManage3 = new System.Windows.Forms.Button();
            this.btnRTMonitor = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).BeginInit();
            this.panelCommand.SuspendLayout();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmMapAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.tabTOC.SuspendLayout();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            this.tabPageTOC.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnRTMonitor);
            this.panelCommand.Controls.Add(this.btnPRValveManage3);
            this.panelCommand.Controls.Add(this.btnPRValveManage2);
            this.panelCommand.Controls.Add(this.btnAlertHis);
            this.panelCommand.Controls.Add(this.btnPRValveManage1);
            // 
            // spcContents
            // 
            // 
            // axToolbar
            // 
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.DefaultGroupSettings.ForceSerialization = true;
            this.DockManagerCommand.DefaultPaneSettings.ForceSerialization = true;
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.TabIndex = 11;
            // 
            // spcTOC
            // 
            this.spcTOC.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            // 
            // tvBlock
            // 
            this.tvBlock.LineColor = System.Drawing.Color.Black;
            // 
            // spcIndexMap
            // 
            // 
            // axTOC
            // 
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            // 
            // axMap
            // 
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Size = new System.Drawing.Size(661, 524);
            this.axMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(this.axMap_OnMouseDown);
            // 
            // btnPRValveManage1
            // 
            this.btnPRValveManage1.Location = new System.Drawing.Point(3, 2);
            this.btnPRValveManage1.Name = "btnPRValveManage1";
            this.btnPRValveManage1.Size = new System.Drawing.Size(62, 54);
            this.btnPRValveManage1.TabIndex = 14;
            this.btnPRValveManage1.Text = "고정유출감시관리";
            this.btnPRValveManage1.UseVisualStyleBackColor = true;
            this.btnPRValveManage1.Click += new System.EventHandler(this.btnPRValveManage1_Click);
            // 
            // btnAlertHis
            // 
            this.btnAlertHis.Location = new System.Drawing.Point(2, 222);
            this.btnAlertHis.Name = "btnAlertHis";
            this.btnAlertHis.Size = new System.Drawing.Size(62, 54);
            this.btnAlertHis.TabIndex = 20;
            this.btnAlertHis.Text = "경보이력 조회";
            this.btnAlertHis.UseVisualStyleBackColor = true;
            this.btnAlertHis.Click += new System.EventHandler(this.btnAlertHis_Click);
            // 
            // btnPRValveManage2
            // 
            this.btnPRValveManage2.Location = new System.Drawing.Point(3, 57);
            this.btnPRValveManage2.Name = "btnPRValveManage2";
            this.btnPRValveManage2.Size = new System.Drawing.Size(62, 54);
            this.btnPRValveManage2.TabIndex = 21;
            this.btnPRValveManage2.Text = "시간제어감시관리";
            this.btnPRValveManage2.UseVisualStyleBackColor = true;
            this.btnPRValveManage2.Click += new System.EventHandler(this.btnPRValveManage2_Click);
            // 
            // btnPRValveManage3
            // 
            this.btnPRValveManage3.Location = new System.Drawing.Point(2, 112);
            this.btnPRValveManage3.Name = "btnPRValveManage3";
            this.btnPRValveManage3.Size = new System.Drawing.Size(62, 54);
            this.btnPRValveManage3.TabIndex = 22;
            this.btnPRValveManage3.Text = "유량비례제어감시관리";
            this.btnPRValveManage3.UseVisualStyleBackColor = true;
            this.btnPRValveManage3.Click += new System.EventHandler(this.btnPRValveManage3_Click);
            // 
            // btnRTMonitor
            // 
            this.btnRTMonitor.Location = new System.Drawing.Point(2, 167);
            this.btnRTMonitor.Name = "btnRTMonitor";
            this.btnRTMonitor.Size = new System.Drawing.Size(62, 54);
            this.btnRTMonitor.TabIndex = 23;
            this.btnRTMonitor.Text = "실시간 감압밸브 감시조회";
            this.btnRTMonitor.UseVisualStyleBackColor = true;
            this.btnRTMonitor.Click += new System.EventHandler(this.btnRTMonitor_Click);
            // 
            // frmWPMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.ClientSize = new System.Drawing.Size(948, 583);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "frmWPMain";
            this.Text = "감압밸브 감시";
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaLeft, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaRight, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaBottom, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaTop, 0);
            this.Controls.SetChildIndex(this.windowDockingArea1, 0);
            this.Controls.SetChildIndex(this.spcContents, 0);
            this.Controls.SetChildIndex(this._frmMapAutoHideControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).EndInit();
            this.panelCommand.ResumeLayout(false);
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.Panel2.PerformLayout();
            this.spcContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmMapAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.tabTOC.ResumeLayout(false);
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            this.tabPageTOC.ResumeLayout(false);
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPRValveManage1;
        private System.Windows.Forms.Button btnAlertHis;
        private System.Windows.Forms.Button btnPRValveManage3;
        private System.Windows.Forms.Button btnPRValveManage2;
        private System.Windows.Forms.Button btnRTMonitor;



    }
}
