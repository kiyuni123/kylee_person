﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WQ_Common;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using EMFrame.log;

namespace WaterNet.WP_RTPRValve
{
    /// <summary>
    /// Project ID : WN_WP_A01
    /// Project Explain : 감압밸브감시
    /// Project Developer : 오두석
    /// Project Create Date : 2011.02.08
    /// Form Explain : 감압밸브감시 Main Form 
    /// </summary>
    public partial class frmWPMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        public frmWPMain()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버 (메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.)

        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            get { return this.Text.ToString(); }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //------------------------------------------------------------------
        //기본적으로 맵제어 기능은 이미 적용되어 있음.
        //추가로 초기 설정이 필요하면, 다음을 적용해야 함.
        //------------------------------------------------------------------
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
        }

        //------------------------------------------------------------------
        //기본적으로 IndexMap 기능은 이미 적용되어 있음.
        //지도맵 관련 레이어 로드기능은 추가로 구현해야함.
        //------------------------------------------------------------------
        public override void Open()
        {
            base.Open();

            #region - WP_AppStatic에 필요한 Object Set
            //Database Setting
            WP_AppStatic.ORACLE_MANAGER = new OracleDBManager();
            WP_AppStatic.ORACLE_MANAGER.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();

            WP_AppStatic.ORACLE_MANAGER.Open();
            if (WP_AppStatic.ORACLE_MANAGER == null)
            {
                MessageBox.Show("Water-Net Database와 연결이 끊겼습니다. 확인 후 다시 시작하십시오.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            //IMAP Set
            WP_AppStatic.IMAP = (IMapControl3)axMap.Object;
            WP_AppStatic.ITOC = (ITOCControl2)axTOC.Object;

            WP_AppStatic.IS_SHOW_FORM_ALERT_HIS = false;
            WP_AppStatic.IS_SHOW_FORM_RT_DATA = false;
            WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE1 = false;
            WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE2 = false;
            WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE3 = false;

            #endregion

            if (WP_AppStatic.IS_SHOW_FORM_RT_DATA == false)
            {
                WaterNet.WP_RTPRValve.FormPopup.frmRTData oForm = new WaterNet.WP_RTPRValve.FormPopup.frmRTData();
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }


        #region Button Events

        /// <summary>
        /// 고정유출감압밸브감시관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPRValveManage1_Click(object sender, EventArgs e)
        {
            if (WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE1 == false)
            {
                WaterNet.WP_RTPRValve.FormPopup.frmWPRValveManage1 oForm = new WaterNet.WP_RTPRValve.FormPopup.frmWPRValveManage1();
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        /// <summary>
        /// 시간제어감압밸브감시관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPRValveManage2_Click(object sender, EventArgs e)
        {
            if (WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE2 == false)
            {
                WaterNet.WP_RTPRValve.FormPopup.frmWPRValveManage2 oForm = new WaterNet.WP_RTPRValve.FormPopup.frmWPRValveManage2();
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        /// <summary>
        /// 유량비례제어감압밸브감시관리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPRValveManage3_Click(object sender, EventArgs e)
        {
            if (WP_AppStatic.IS_SHOW_PR_VALVE_MANAGE3 == false)
            {
                WaterNet.WP_RTPRValve.FormPopup.frmWPRValveManage3 oForm = new WaterNet.WP_RTPRValve.FormPopup.frmWPRValveManage3();
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        /// <summary>
        /// 경보이력
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAlertHis_Click(object sender, EventArgs e)
        {
            if (WP_AppStatic.IS_SHOW_FORM_ALERT_HIS == false)
            {
                WaterNet.WP_RTPRValve.FormPopup.frmRTAlertHistory oForm = new WaterNet.WP_RTPRValve.FormPopup.frmRTAlertHistory();
                oForm.TopLevel = false;
                oForm.Parent = this;
                oForm.StartPosition = FormStartPosition.Manual;
                oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                oForm.BringToFront();
                oForm.Show();
            }
        }

        private void btnRTMonitor_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (WP_AppStatic.IS_SHOW_FORM_RT_DATA == false)
                {
                    WaterNet.WP_RTPRValve.FormPopup.frmRTData oForm = new WaterNet.WP_RTPRValve.FormPopup.frmRTData();
                    oForm.TopLevel = false;
                    oForm.Parent = this;
                    oForm.StartPosition = FormStartPosition.Manual;
                    oForm.SetBounds(this.Width - (this.axMap.Width + 2), this.axToolbar.Height + 6, oForm.Width, oForm.Height);
                    oForm.BringToFront();
                    oForm.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        #endregion


        #region Control Events

        /// <summary>
        /// Map에서 오른쪽 마우스 버튼 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            ///GIS 상 좌표
            WP_AppStatic.IMAP_X = e.mapX;
            WP_AppStatic.IMAP_Y = e.mapY;

            WP_AppStatic.CURRENT_IGEOMETRY = null;

            if (!((axMap.CurrentTool == null) & (toolActionCommand.Checked))) return;

            IPoint pPoint = new PointClass();
            pPoint.X = WP_AppStatic.IMAP_X;
            pPoint.Y = WP_AppStatic.IMAP_Y;
            pPoint.Z = 0;

            IGeometry pGeom = (IGeometry)pPoint;

            WP_AppStatic.CURRENT_IGEOMETRY = pGeom;
            
            switch (e.button)
            {
                case 1:    //왼쪽버튼
                    break;

                case 2:    //오른쪽버튼
                    //ContextMenuStripPopup.Show(axMap, m_X, m_Y);
                    break;
            }
        }
        
        #endregion

    }
}
