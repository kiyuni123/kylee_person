﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;
using WaterNet.WQ_Common;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WP_RTPRValve
{
    class WP_AppStatic
    {
        #region 선언부

        /// <summary>
        /// Oracle Database Manager
        /// </summary>
        public static OracleDBManager ORACLE_MANAGER = null;

        /// <summary>
        /// 밸브 중 갑압밸브
        /// </summary>
        private static string FTR_CDE = "SA204";

        private static double SCALE = 1000;

        /// <summary>
        /// frmWPMain의 (IMapControl3)axMap.Object
        /// </summary>
        public static IMapControl3 IMAP;

        /// <summary>
        /// frmWPMain의 (ITOCControl2)axTOC.Object
        /// </summary>
        public static ITOCControl2 ITOC;

        /// <summary>
        /// frmWPMain의 axMap_OnMouseDown Event 또는 User의 동작에 의한 GIS상의 X 좌표
        /// 이후 GetCurrentIGeometry()에서 활용해 IGeometry를 생성하는데 사용
        /// </summary>
        public static double IMAP_X = 0;

        /// <summary>
        /// frmWPMain의 axMap_OnMouseDown Event 또는 User의 동작에 의한 GIS상의 X 좌표
        /// 이후 GetCurrentIGeometry()에서 활용해 IGeometry를 생성하는데 사용
        /// </summary>
        public static double IMAP_Y = 0;

        /// <summary>
        /// frmWPMain의 axMap_OnMouseDown Event에서 취득한 Window상의 X 좌표
        /// 이후 Control을 배치시킬때 사용
        /// </summary>
        public static int WIN_X = 0;

        /// <summary>
        /// frmWPMain의 axMap_OnMouseDown Event에서 취득한 Window상의 Y 좌표
        /// 이후 Control을 배치시킬때 사용
        /// </summary>
        public static int WIN_Y = 0;

        /// <summary>
        /// 그리드 또는 MAP에서 선택한 지점의 IGeometry
        /// </summary>
        public static IGeometry CURRENT_IGEOMETRY;

        #endregion

        #region 팝업 폼 오픈여부 선언부

        /// <summary>
        /// frmRTData 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_RT_DATA = false;
        /// <summary>
        /// frmRTAlertHistory 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_FORM_ALERT_HIS = false;
        /// <summary>
        /// frmWPRValveManage1 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_PR_VALVE_MANAGE1 = false;
        /// <summary>
        /// frmWPRValveManage2 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_PR_VALVE_MANAGE2 = false;
        /// <summary>
        /// frmWPRValveManage3 의 Show 여부
        /// </summary>
        public static bool IS_SHOW_PR_VALVE_MANAGE3 = false;

        #endregion

        #region User Function

        #region - GIS Function

        /// <summary>
        /// Combo에서 선택된 블록 GIS에서 View
        /// </summary>
        /// <param name="strFTR_IDN">중, 소블록 FTR_IDN</param>
        /// <param name="iType">0 : 중블록, 1 : 소블록</param>
        public static void Viewmap_SelectedBlock(string strFTR_IDN, int iType)
        {
            IFeatureLayer pFeatureLayer = default(IFeatureLayer);

            switch (iType)
            {
                case 0:
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(WP_AppStatic.IMAP, "중블록");
                    break;
                case 1:
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(WP_AppStatic.IMAP, "소블록");
                    break;
            }

            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '" + strFTR_IDN + "'");
            if (pFeature == null) return;

            WP_AppStatic.IMAP.Extent = pFeature.Shape.Envelope;
            WP_AppStatic.IMAP.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);
        }

        /// <summary>
        /// 그리드 or 콤보에서 선택된 밸브 GIS에서 View
        /// </summary>
        /// <param name="strFTR_IDN">밸브 FTR_IDN</param>
        public static void Viewmap_SelectedValve(string strFTR_IDN)
        {
            IFeatureLayer pFeatureLayer = default(IFeatureLayer);

            pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(WP_AppStatic.IMAP, "밸브");

            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_CDE = '" + WP_AppStatic.FTR_CDE + "' AND FTR_IDN = '" + strFTR_IDN + "'");
            if (pFeature == null) return;

            if (ArcManager.GetMapScale(WP_AppStatic.IMAP) > WP_AppStatic.SCALE)
            {
                ArcManager.SetMapScale(WP_AppStatic.IMAP, WP_AppStatic.SCALE);
            }

            ArcManager.MoveCenterAt(WP_AppStatic.IMAP, pFeature);

            ArcManager.PartialRefresh(WP_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();
        }

        /// <summary>
        /// 콤보에서 선택된 계측기 GIS에서 View
        /// </summary>
        /// <param name="strFTR_IDN">계측기 FTR_IDN</param>
        public static void Viewmap_SelectedManometer(string strFTR_IDN)
        {
            IFeatureLayer pFeatureLayer = default(IFeatureLayer);

            pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(WP_AppStatic.IMAP, "유량계");

            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '" + strFTR_IDN + "'");
            if (pFeature == null) return;

            if (ArcManager.GetMapScale(WP_AppStatic.IMAP) > WP_AppStatic.SCALE)
            {
                ArcManager.SetMapScale(WP_AppStatic.IMAP, WP_AppStatic.SCALE);
            }

            ArcManager.MoveCenterAt(WP_AppStatic.IMAP, pFeature);

            ArcManager.PartialRefresh(WP_AppStatic.IMAP.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
            Application.DoEvents();
        }

        /// <summary>
        /// 가장 최근 axMap_OnMouseDown Event에서 취득한 GIS 상의 X, Y 좌표로 IGeometry를 반환한다.
        /// </summary>
        /// <returns></returns>
        public static IGeometry GetCurrentIGeometryFromXY()
        {
            IPoint pPoint = new PointClass();

            pPoint.X = WP_AppStatic.IMAP_X;
            pPoint.Y = WP_AppStatic.IMAP_Y;
            pPoint.Z = 0;

            IGeometry pGeom = (IGeometry)pPoint;

            return pGeom;
        }

        #endregion
        
        #region - Combo Function

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 대상 Data : 감압밸브타입
        /// 대상 Table : CM_CODE
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="iDefaultIndex">Combo Item 중 Default Index</param>
        /// <param name="IsUseAll">전체 사용여부 (true 전체 항목 추가)</param>
        public static void SetCombo_PRValveType(ComboBox oCombo, int iDefaultIndex, bool IsUseAll)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT   CODE_NAME || ' (' || CODE || ')' AS RTNDATA");
            oStringBuilder.AppendLine("FROM     CM_CODE");
            oStringBuilder.AppendLine("WHERE    PCODE = '6001'");
            oStringBuilder.AppendLine("ORDER BY CODE");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CM_CODE");

            oCombo.Items.Clear();

            if (IsUseAll == true)
            {
                oCombo.Items.Add("전체");
            }

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
        }

        /// <summary>
        /// 소블록 CODE로 IF_IHTAGS에서 수압계의 FTR_IDN(계측기번호)를 Combo에 SET
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strLOC_CODE">소블록 코드</param>
        /// <param name="iDefaultIndex">Combo의 기본 선택 Index</param>
        public static void SetCombo_NoOfMeasuringInstrumentOnSmallBlock(ComboBox oCombo, string strLOC_CODE, int iDefaultIndex)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT DISTINCT  FTR_IDN AS RTNDATA");
            //oStringBuilder.AppendLine("FROM     CM_LOCATION");
            oStringBuilder.AppendLine("FROM     IF_IHTAGS");
            oStringBuilder.AppendLine("WHERE    LOC_CODE = '" + strLOC_CODE + "'" + "AND FTR_IDN IS NOT NULL" );
            oStringBuilder.AppendLine("ORDER BY FTR_IDN");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IF_IHTAGS");

            oCombo.Items.Clear();

            oCombo.Items.Add("");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
        }

        /// <summary>
        /// 소블록 CODE로 IF_IHTAGS에서 유량계 FTR_IDN(유량계번호)를 Combo에 SET
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strLOC_CODE">소블록 코드</param>
        /// <param name="iDefaultIndex">Combo의 기본 선택 Index</param>
        public static void SetCombo_NoOfFlowmeteringInstrumentOnSmallBlock(ComboBox oCombo, string strLOC_CODE, int iDefaultIndex)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT DISTINCT  FTR_IDN AS RTNDATA");
            oStringBuilder.AppendLine("FROM     IF_IHTAGS");
            oStringBuilder.AppendLine("WHERE    LOC_CODE = '" + strLOC_CODE + "'" + "AND FTR_IDN IS NOT NULL");
            //oStringBuilder.AppendLine("FROM     CM_LOCATION");
            //oStringBuilder.AppendLine("WHERE    LOC_CODE = '" + strLOC_CODE + "'");
            oStringBuilder.AppendLine("ORDER BY FTR_IDN");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IF_IHTAGS");

            oCombo.Items.Clear();

            oCombo.Items.Add("");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
        }

        /// <summary>
        /// 계측기번호로 IF_IHTAGS에서 수압계의 TAG(계측기 TAGNAME)를 Combo에 SET
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strFTR_IDN">계측기번호</param>
        /// <param name="iDefaultIndex">Combo의 기본 선택 Index</param>
        public static void SetCombo_TAGOfMeasuringInstrumentOnSmallBlock(ComboBox oCombo, string strFTR_IDN, int iDefaultIndex)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT   TAGNAME AS RTNDATA");
            oStringBuilder.AppendLine("FROM     IF_IHTAGS");
            //oStringBuilder.AppendLine("WHERE    FTR_IDN = '" + strFTR_IDN + "' AND BR_CODE = 'PR' AND FN_CODE = 'I'");
            oStringBuilder.AppendLine("WHERE    FTR_IDN = '" + strFTR_IDN + "' " + "AND TAGNAME IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'PRI')");
            oStringBuilder.AppendLine("AND TAG_GBN <> '수압계(전단)'");
            oStringBuilder.AppendLine("ORDER BY FTR_IDN");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IF_IHTAGS");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
        }

        /// <summary>
        /// 계측기번호로 IF_IHTAGS에서 수압계의 TAG(계측기 TAGNAME)를 Combo에 SET
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strFTR_IDN">계측기번호</param>
        /// <param name="iDefaultIndex">Combo의 기본 선택 Index</param>
        public static void SetCombo_TAGOfMeasuringInstrumentOnSmallBlock2(ComboBox oCombo, string strFTR_IDN, int iDefaultIndex)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT   TAGNAME AS RTNDATA");
            oStringBuilder.AppendLine("FROM     IF_IHTAGS");
            //oStringBuilder.AppendLine("WHERE    FTR_IDN = '" + strFTR_IDN + "' AND BR_CODE = 'PR' AND FN_CODE = 'I'");
            oStringBuilder.AppendLine("WHERE    FTR_IDN = '" + strFTR_IDN + "' " + "AND TAGNAME IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'PRI')");
            oStringBuilder.AppendLine("AND TAG_GBN = '수압계(전단)'");
            oStringBuilder.AppendLine("ORDER BY FTR_IDN");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IF_IHTAGS");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
        }

        /// <summary>
        /// 계측기번호로 IF_IHTAGS에서 유량계 TAG(유량계 TAGNAME)를 Combo에 SET
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strFTR_IDN">계측기번호</param>
        /// <param name="iDefaultIndex">Combo의 기본 선택 Index</param>
        public static void SetCombo_TAGOfFlowmeteringInstrumentOnSmallBlock(ComboBox oCombo, string strFTR_IDN, int iDefaultIndex)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT   TAGNAME AS RTNDATA");
            oStringBuilder.AppendLine("FROM     IF_IHTAGS");
            //oStringBuilder.AppendLine("WHERE    FTR_IDN = '" + strFTR_IDN + "'");
            oStringBuilder.AppendLine("WHERE    FTR_IDN = '" + strFTR_IDN + "' " + "AND TAGNAME IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'FRI')");
            oStringBuilder.AppendLine("ORDER BY FTR_IDN");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "IF_IHTAGS");

            oCombo.Items.Clear();

            //oCombo.Items.Add("");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
        }

        /// <summary>
        /// 밸브 레이어에서 FTR_CDE = 'SA204'(감압밸브) 이고 BSM_IDN이 소블록 FTR_IDN인
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strBSM_IDN">밸브 레이어 중 블록 코드(소블록 FTR_IDN)</param>
        /// <param name="iDefaultIndex">Combo의 기본 선택 Index</param>
        public static void SetCombo_NoOfPressureReducingValveOnSmallBlockByLayer(ComboBox oCombo, string strBSM_IDN, string strMOF, int iDefaultIndex)
        {
            IFeatureLayer pFeatureLayer = default(IFeatureLayer);

            pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer(WP_AppStatic.IMAP, "밸브");

            if (pFeatureLayer == null) return;

            IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureLayer.FeatureClass, 
                "FTR_CDE = '" + WP_AppStatic.FTR_CDE + "' AND BSM_IDN IN (" + strBSM_IDN + ")" + " AND VAL_MOF = '" + strMOF + "'") as IFeatureCursor;

            IFeature pFeature = pCursor.NextFeature();

            oCombo.Items.Clear();

            oCombo.Items.Add("");

            while (pFeature != null)
            {
                object oFTR_IDN = WaterAOCore.ArcManager.GetValue(pFeature, "FTR_IDN");
                if (oFTR_IDN != null)
                {
                    oCombo.Items.Add(oFTR_IDN.ToString());
                }
                pFeature = pCursor.NextFeature();
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
   
        }

        /// <summary>
        /// VALVE TYPE으로 이미 등록된 갑압밸브 감시지점을 SELECT해서 Combo에 SET
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="strLOC_CODE">소블록 코드</param>
        /// <param name="iDefaultIndex">Combo의 기본 선택 Index</param>
        public static void SetCombo_NoOfPressureReducingValveByRTPointDB(ComboBox oCombo, string strVALVE_TYPE, int iDefaultIndex)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();

            oStringBuilder.AppendLine("SELECT   VALVE_NO AS RTNDATA");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("WHERE    VALVE_TYPE = '" + strVALVE_TYPE + "'");
            oStringBuilder.AppendLine("ORDER BY VALVE_NO");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "RT_POINT");

            oCombo.Items.Clear();

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    oCombo.Items.Add(oDRow["RTNDATA"].ToString().Trim());
                }
            }
            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            pDS.Dispose();
        }
        
        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 0 ~ 5 까지
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="iDefaultIndex">Combo Item 중 Default Index</param>
        public static void SetCombo_DelayTime(ComboBox oCombo, int iDefaultIndex)
        {
            oCombo.Items.Clear();

            for (int i = 0; i < 6; i++)
            {
                oCombo.Items.Add(i.ToString());
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// '' + 00 ~ 24 까지
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="iDefaultIndex">Combo Item 중 Default Index</param>
        public static void SetCombo_Hour(ComboBox oCombo, int iDefaultIndex)
        {
            oCombo.Items.Clear();

            oCombo.Items.Add("");
            
            for (int i = 0; i < 25; i++)
            {
                oCombo.Items.Add(WQ_Function.FillZero(i.ToString(), 2, 1));
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// ComboBox에 Data를 Set 한다.
        /// 00 ~ 59 까지
        /// </summary>
        /// <param name="oCombo"></param>
        /// <param name="iDefaultIndex">Combo Item 중 Default Index</param>
        public static void SetCombo_Minute(ComboBox oCombo, int iDefaultIndex)
        {
            oCombo.Items.Clear();

            for (int i = 0; i < 60; i++)
            {
                oCombo.Items.Add(WQ_Function.FillZero(i.ToString(), 2, 1));
            }

            if (oCombo.Items.Count > 0)
            {
                oCombo.SelectedIndex = iDefaultIndex;
            }
            oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        #endregion

        #region - ETC Function

        /// <summary>
        /// 감압밸브 NO로 유량 TAG를 찾은 다음 그 TAG로 현재 유량값을 가져온다.
        /// </summary>
        /// <param name="strVALVE_NO"></param>
        /// <param name="strDATE_TIME"></param>
        /// <returns></returns>
        public static string GetFlowmeteringInstrumentValue(string strVALVE_NO, string strDATE_TIME)
        {
            string strResult = string.Empty;
            string strTAGNAME = string.Empty;

            StringBuilder oStringBuilder = new StringBuilder();

            DataSet pDS = new DataSet();
            DataSet pDS2 = new DataSet();

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   TAGNAME_F");
            oStringBuilder.AppendLine("FROM     WP_RT_MONITOR_POINT");
            oStringBuilder.AppendLine("WHERE    VALVE_NO = '" + strVALVE_NO + "'");

            pDS = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "TAGNAME");

            if ((pDS.Tables.Count > 0) && (pDS.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow oDRow in pDS.Tables[0].Rows)
                {
                    strTAGNAME = oDRow["TAGNAME_F"].ToString().Trim();

                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("SELECT   ROUND(VALUE, 3) AS CUR_VALUE");
                    oStringBuilder.AppendLine("FROM     IF_GATHER_REALTIME");
                    oStringBuilder.AppendLine("WHERE    TAGNAME = '" + strTAGNAME + "' AND TIMESTAMP = TO_DATE('" + strDATE_TIME + "', 'RRRR-MM-DD HH24:MI')");

                    pDS2 = WP_AppStatic.ORACLE_MANAGER.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "CUR_VAL");

                    if ((pDS2.Tables.Count > 0) && (pDS2.Tables[0].Rows.Count > 0))
                    {
                        foreach (DataRow oDRow2 in pDS2.Tables[0].Rows)
                        {
                            strResult = oDRow2["CUR_VALUE"].ToString().Trim();
                        }
                    }
                }
            }

            return strResult;
        }

        #endregion

        #endregion

        #region
        public static DataTable IncrementTime(System.DateTime _sDate, System.DateTime _eDate, AggregateTime aggregate)
        {
            DataTable t = new DataTable();
            t.Columns.Add("TIMESTAMP", typeof(System.String));
            DateTime tmp = _sDate;
            while (tmp < _eDate)
            {
                DataRow r = t.NewRow();
                r[0] = tmp.ToString("yyyy-MM-dd HH:mm:ss");
                t.Rows.Add(r);
                switch (aggregate)
                {
                    case AggregateTime.Second:
                        tmp = tmp.AddSeconds(1);
                        break;
                    case AggregateTime.Min:
                        tmp = tmp.AddMinutes(1);
                        break;
                    case AggregateTime.Hour:
                        tmp = tmp.AddHours(1);
                        break;
                    case AggregateTime.Day:
                        tmp = tmp.AddDays(1);
                        break;
                    case AggregateTime.Month:
                        tmp = tmp.AddMonths(1);
                        break;
                    case AggregateTime.Year:
                        tmp = tmp.AddYears(1);
                        break;
                }
            }
            return t;
        }
        #endregion
    }

    public enum AggregateTime
    {
        Second = 1,
        Min = 2,
        Hour = 3,
        Day = 4,
        Month = 5,
        Year = 6
    }
}
