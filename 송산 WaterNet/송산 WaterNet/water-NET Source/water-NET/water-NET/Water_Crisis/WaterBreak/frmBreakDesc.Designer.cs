﻿namespace WaterNet.WC_CrisisTransfer
{
    partial class frmBreakDesc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnChange = new System.Windows.Forms.Button();
            this.btnExcel = new System.Windows.Forms.Button();
            this.label_PP_CAT = new System.Windows.Forms.Label();
            this.comboBox_Scale = new System.Windows.Forms.ComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.splitContainer_Image = new System.Windows.Forms.SplitContainer();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.PagePipe = new System.Windows.Forms.TabPage();
            this.ultraGrid_Pipe = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.PageSply = new System.Windows.Forms.TabPage();
            this.ultraGrid_Sply = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.PageMeta = new System.Windows.Forms.TabPage();
            this.ultraGrid_Meta = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label_Count = new System.Windows.Forms.Label();
            this.pictureMap = new System.Windows.Forms.PictureBox();
            this.panelCommand.SuspendLayout();
            this.splitContainer_Image.Panel1.SuspendLayout();
            this.splitContainer_Image.Panel2.SuspendLayout();
            this.splitContainer_Image.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.PagePipe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Pipe)).BeginInit();
            this.PageSply.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Sply)).BeginInit();
            this.PageMeta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Meta)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureMap)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnChange);
            this.panelCommand.Controls.Add(this.btnExcel);
            this.panelCommand.Controls.Add(this.label_PP_CAT);
            this.panelCommand.Controls.Add(this.comboBox_Scale);
            this.panelCommand.Controls.Add(this.btnClose);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(556, 31);
            this.panelCommand.TabIndex = 16;
            // 
            // btnChange
            // 
            this.btnChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChange.Location = new System.Drawing.Point(310, 3);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(75, 25);
            this.btnChange.TabIndex = 45;
            this.btnChange.Text = "지도변경";
            this.btnChange.UseVisualStyleBackColor = true;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.Location = new System.Drawing.Point(391, 3);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(75, 25);
            this.btnExcel.TabIndex = 44;
            this.btnExcel.Text = "엑셀";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // label_PP_CAT
            // 
            this.label_PP_CAT.Location = new System.Drawing.Point(6, 10);
            this.label_PP_CAT.Name = "label_PP_CAT";
            this.label_PP_CAT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_CAT.Size = new System.Drawing.Size(67, 17);
            this.label_PP_CAT.TabIndex = 43;
            this.label_PP_CAT.Text = "축척";
            // 
            // comboBox_Scale
            // 
            this.comboBox_Scale.FormattingEnabled = true;
            this.comboBox_Scale.Location = new System.Drawing.Point(81, 6);
            this.comboBox_Scale.Name = "comboBox_Scale";
            this.comboBox_Scale.Size = new System.Drawing.Size(106, 20);
            this.comboBox_Scale.TabIndex = 42;
            this.comboBox_Scale.Tag = "";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(472, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 25);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // splitContainer_Image
            // 
            this.splitContainer_Image.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_Image.Location = new System.Drawing.Point(0, 31);
            this.splitContainer_Image.Name = "splitContainer_Image";
            this.splitContainer_Image.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_Image.Panel1
            // 
            this.splitContainer_Image.Panel1.Controls.Add(this.tabControl);
            this.splitContainer_Image.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer_Image.Panel2
            // 
            this.splitContainer_Image.Panel2.Controls.Add(this.pictureMap);
            this.splitContainer_Image.Size = new System.Drawing.Size(556, 537);
            this.splitContainer_Image.SplitterDistance = 271;
            this.splitContainer_Image.TabIndex = 17;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.PagePipe);
            this.tabControl.Controls.Add(this.PageSply);
            this.tabControl.Controls.Add(this.PageMeta);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 28);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(556, 243);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 20;
            // 
            // PagePipe
            // 
            this.PagePipe.Controls.Add(this.ultraGrid_Pipe);
            this.PagePipe.Location = new System.Drawing.Point(4, 22);
            this.PagePipe.Name = "PagePipe";
            this.PagePipe.Padding = new System.Windows.Forms.Padding(3);
            this.PagePipe.Size = new System.Drawing.Size(548, 217);
            this.PagePipe.TabIndex = 1;
            this.PagePipe.Text = "상수관로";
            this.PagePipe.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_Pipe
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Pipe.DisplayLayout.Appearance = appearance37;
            this.ultraGrid_Pipe.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Pipe.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pipe.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Pipe.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.ultraGrid_Pipe.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Pipe.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.ultraGrid_Pipe.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Pipe.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_Pipe.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_Pipe.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.ultraGrid_Pipe.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Pipe.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pipe.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Pipe.DisplayLayout.Override.CellAppearance = appearance44;
            this.ultraGrid_Pipe.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Pipe.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Pipe.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.ultraGrid_Pipe.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.ultraGrid_Pipe.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Pipe.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Pipe.DisplayLayout.Override.RowAppearance = appearance47;
            this.ultraGrid_Pipe.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Pipe.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.ultraGrid_Pipe.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Pipe.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Pipe.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_Pipe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Pipe.Location = new System.Drawing.Point(3, 3);
            this.ultraGrid_Pipe.Name = "ultraGrid_Pipe";
            this.ultraGrid_Pipe.Size = new System.Drawing.Size(542, 211);
            this.ultraGrid_Pipe.TabIndex = 2;
            this.ultraGrid_Pipe.Text = "상수관로";
            // 
            // PageSply
            // 
            this.PageSply.Controls.Add(this.ultraGrid_Sply);
            this.PageSply.Location = new System.Drawing.Point(4, 22);
            this.PageSply.Name = "PageSply";
            this.PageSply.Padding = new System.Windows.Forms.Padding(3);
            this.PageSply.Size = new System.Drawing.Size(548, 217);
            this.PageSply.TabIndex = 2;
            this.PageSply.Text = "급수관로";
            this.PageSply.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_Sply
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Sply.DisplayLayout.Appearance = appearance25;
            this.ultraGrid_Sply.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Sply.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Sply.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Sply.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.ultraGrid_Sply.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Sply.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.ultraGrid_Sply.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Sply.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_Sply.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_Sply.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.ultraGrid_Sply.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Sply.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Sply.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Sply.DisplayLayout.Override.CellAppearance = appearance32;
            this.ultraGrid_Sply.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Sply.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Sply.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.ultraGrid_Sply.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.ultraGrid_Sply.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Sply.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Sply.DisplayLayout.Override.RowAppearance = appearance35;
            this.ultraGrid_Sply.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Sply.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.ultraGrid_Sply.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Sply.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Sply.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_Sply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Sply.Location = new System.Drawing.Point(3, 3);
            this.ultraGrid_Sply.Name = "ultraGrid_Sply";
            this.ultraGrid_Sply.Size = new System.Drawing.Size(542, 211);
            this.ultraGrid_Sply.TabIndex = 3;
            this.ultraGrid_Sply.Text = "급수관로";
            // 
            // PageMeta
            // 
            this.PageMeta.Controls.Add(this.ultraGrid_Meta);
            this.PageMeta.Location = new System.Drawing.Point(4, 22);
            this.PageMeta.Name = "PageMeta";
            this.PageMeta.Padding = new System.Windows.Forms.Padding(3);
            this.PageMeta.Size = new System.Drawing.Size(548, 217);
            this.PageMeta.TabIndex = 3;
            this.PageMeta.Text = "수도계량기";
            this.PageMeta.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_Meta
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Meta.DisplayLayout.Appearance = appearance4;
            this.ultraGrid_Meta.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Meta.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Meta.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Meta.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.ultraGrid_Meta.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Meta.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.ultraGrid_Meta.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Meta.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_Meta.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_Meta.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.ultraGrid_Meta.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Meta.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Meta.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Meta.DisplayLayout.Override.CellAppearance = appearance5;
            this.ultraGrid_Meta.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Meta.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Meta.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.ultraGrid_Meta.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.ultraGrid_Meta.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Meta.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Meta.DisplayLayout.Override.RowAppearance = appearance10;
            this.ultraGrid_Meta.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Meta.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.ultraGrid_Meta.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Meta.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Meta.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_Meta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Meta.Location = new System.Drawing.Point(3, 3);
            this.ultraGrid_Meta.Name = "ultraGrid_Meta";
            this.ultraGrid_Meta.Size = new System.Drawing.Size(542, 211);
            this.ultraGrid_Meta.TabIndex = 3;
            this.ultraGrid_Meta.Text = "수도계량기";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label_Count);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(556, 28);
            this.panel3.TabIndex = 19;
            // 
            // label_Count
            // 
            this.label_Count.AutoSize = true;
            this.label_Count.Location = new System.Drawing.Point(7, 9);
            this.label_Count.Name = "label_Count";
            this.label_Count.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_Count.Size = new System.Drawing.Size(267, 12);
            this.label_Count.TabIndex = 39;
            this.label_Count.Text = "상수관로 : 0개, 급수관로 : 0개, 수도계량기 : 0개";
            // 
            // pictureMap
            // 
            this.pictureMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureMap.Location = new System.Drawing.Point(0, 0);
            this.pictureMap.Name = "pictureMap";
            this.pictureMap.Size = new System.Drawing.Size(556, 262);
            this.pictureMap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureMap.TabIndex = 0;
            this.pictureMap.TabStop = false;
            // 
            // frmBreakDesc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 568);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer_Image);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmBreakDesc";
            this.Text = "초기 단수지역 분석결과";
            this.Load += new System.EventHandler(this.frmBreakDesc_Load);
            this.panelCommand.ResumeLayout(false);
            this.splitContainer_Image.Panel1.ResumeLayout(false);
            this.splitContainer_Image.Panel2.ResumeLayout(false);
            this.splitContainer_Image.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.PagePipe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Pipe)).EndInit();
            this.PageSply.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Sply)).EndInit();
            this.PageMeta.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Meta)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureMap)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.SplitContainer splitContainer_Image;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage PagePipe;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Pipe;
        private System.Windows.Forms.TabPage PageSply;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Sply;
        private System.Windows.Forms.TabPage PageMeta;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Meta;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label_Count;
        private System.Windows.Forms.PictureBox pictureMap;
        private System.Windows.Forms.Label label_PP_CAT;
        private System.Windows.Forms.ComboBox comboBox_Scale;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Button btnChange;

    }
}