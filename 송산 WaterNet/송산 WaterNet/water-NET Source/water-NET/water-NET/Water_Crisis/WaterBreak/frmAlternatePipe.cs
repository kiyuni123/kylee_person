﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;

using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

using WaterNet.WC_Common;
using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WC_CrisisTransfer
{
    public partial class frmAlternatePipe : Form
    {
        /// <summary>
        /// 폼을 최소화하기 위한 멤버선언
        /// </summary>
        private System.Drawing.Size m_formsize = new System.Drawing.Size(); //폼사이즈
        private System.Drawing.Point m_formLocation = new System.Drawing.Point(); //폼위치
        private IntPtr m_formParam = new IntPtr();                                //최종 윈도우 파라메터
        //--------------------------------------------------------------------------------------------------

        //private IMapControl3 m_mapcontrol;
        //private ESRI.ArcGIS.Carto.IMap m_map;
        private EAGL.Explore.MapExplorer _mapExplorer;
        private Dictionary<string, EpaNetwork.xLink> _alternateValve;

        public frmAlternatePipe(Dictionary<string, EpaNetwork.xLink> alternateValve)
        {
            InitializeComponent();
            this._alternateValve = alternateValve;
            InitializeSetting();
        }

        /// <summary>
        /// 윈도우 메세지 후킹 : 화면 최소화, 복구 이벤트 기능
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                if (m.WParam == new IntPtr(0xF020)) // SC_MINIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    m_formsize = this.Size;
                    m_formLocation = this.Location;
                    m_formParam = m.WParam;

                    this.Size = new System.Drawing.Size(150, SystemInformation.CaptionHeight);
                    if (this.ParentForm != null)
                        this.Location = new System.Drawing.Point(this.ParentForm.Width - 170, this.ParentForm.Height - 40);
                    else
                        this.Location = new System.Drawing.Point(0, 0);

                    this.BringToFront();
                    m.Result = new IntPtr(0);
                    return;
                }
                else if (m.WParam == new IntPtr(0xF030))  // SC_MAXIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    if (m_formsize.Height == 0 | m_formsize.Width == 0) return;

                    this.Size = m_formsize;
                    this.Location = m_formLocation;
                    this.BringToFront();
                    m_formParam = m.WParam;

                    m.Result = new IntPtr(0);
                    return;
                }
                else if ((m.WParam.ToInt32() >= 61441) && (m.WParam.ToInt32() <= 61448))  // SC_SIZE변경
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF032))  //최대화(타이틀바 더블클릭)시.
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF060))  //SC_CLOSE버튼 실행안함.
                {
                    return;
                }
                m.Result = new IntPtr(0);
            }

            base.WndProc(ref m);
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            ///대체공급관 PIPE
            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHECKED";
            oUltraGridColumn.Header.Caption = "선택";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Center;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 200;

            FormManager.SetGridStyle(ultraGrid_Pipe);
            FormManager.SetGridStyle_ColumeAllowEdit(ultraGrid_Pipe, 0);

            #endregion

            
        }
        #endregion

        /// <summary>
        /// Focus Map 설정
        /// </summary>
        public EAGL.Explore.MapExplorer MapExplorer
        {
            set { this._mapExplorer = value; }
        }

        public Boolean Open()
        {
            ///모델에서 status = Closed로 초기설정된 (INP) PIPE객체
            //Dictionary<string, string> statusPIPE = setQueryStatment();
            if (this._alternateValve.Count < 1)
            {
                MessageManager.ShowInformationMessage("탐색된 비상공급관이 없습니다.");
                this.Close();
                return false;
            }

            ///광역관로사고일경우 광역PIPE만 남기도록
            if (WC_VariableManager.m_CrisisParams["PIPE_GBN"].Equals("G"))
            {
                this._alternateValve = this._alternateValve.Where(entry => entry.Value.getTagstoken('|', 7).Equals("G"))
                             .Select(entry => entry).ToDictionary(key => key.Key, key => key.Value);
                
                if (this._alternateValve.Count < 1)
                {
                    MessageManager.ShowInformationMessage("수리모델에서 비상공급관으로 설정된 PIPE정보가 없습니다.");
                    this.Close();
                    return false;
                }
            }

            /*
            ///구현 알고리즘 
            /// 1. 1차해석 - 수압이 0 인 Junction에 인접한 (INP) PIPE 를 찾는다 => m_CLOSED_pipeline_1st
            /// 2. 모델 데이터베이스에서 초기 Close로 설정된 관로를 찾는다 => m_Datatable
            /// 3. 1과 2를 비교하여, 2에 없는 객체를 삭제한다.
            /// 4. 앞에서 찾은 차단관로와 3을 비교하여, 다른 경우는 비상공급관으로 설정한다.
            ///remove a list of feature (OID's) from the selection set        
            
            System.Collections.Generic.List<int> constructOIDDeleteList = new System.Collections.Generic.List<int>();
            //1차 해석의 결과(Junction)에 인접한 (INP PIPE) 객체
            IEnumIDs enumIDS = this.m_CLOSED_pipe_1st.IDs;
            enumIDS.Reset();
            int iD = -1;
            IRow pRow = null;
            while ((iD = enumIDS.Next()) != -1)
            {
                //1차해석에서 찾은 PIPE 객체중 모델초기설정(STATUS)이 Open인 경우는 추가하고, Closed인 경우는 skip한다.
                if ((pRow = this.m_CLOSED_pipe_1st.Target.GetRow(iD)) == null) continue;
                //Console.WriteLine(Convert.ToString(pRow.get_Value(pRow.Fields.FindField("ID"))));
                if (!(statusPIPE.ContainsKey(Convert.ToString(pRow.get_Value(pRow.Fields.FindField("ID"))))))
                {
                    Console.WriteLine(string.Format("{0}-{1}", iD, pRow.get_Value(pRow.Fields.FindField("ID"))));
                    constructOIDDeleteList.Add(iD);
                }
            }

            //Close된 PIPE만 남기기 위해서 나머지 삭제
            int[] oidDeleteList = constructOIDDeleteList.ToArray();
            Console.WriteLine(string.Format("org 1st : {0} - {1}", this.m_CLOSED_pipe_1st.Count, oidDeleteList.Length));
            if (oidDeleteList.Length > 0)
            {
                this.m_CLOSED_pipe_1st.RemoveList(oidDeleteList.Length, ref oidDeleteList[0]);    
            }
            Console.WriteLine(string.Format("remove 1st : {0} - {1}", this.m_CLOSED_pipe_1st.Count, oidDeleteList.Length));

            //Console.WriteLine(((IFeatureSelection)InpPipeLayer).SelectionSet.Count);
            ///기존 검색된 관로에 포함된 비상공급관은 제외하기 위해 Combine메소드 사용
            ISelectionSet resultSet;
            Console.WriteLine(string.Format("pipeSelction : {0} - {1}", this.m_CLOSED_pipe_1st.Count, ((IFeatureSelection)InpPipeLayer).SelectionSet.Count));
            this.m_CLOSED_pipe_1st.Combine(((IFeatureSelection)InpPipeLayer).SelectionSet, esriSetOperation.esriSetDifference, out resultSet);
            */

            //ultraGrid_Pipe.DataSource = (convertCursorToDataTable(pCursor)).DefaultView ;

            this.Show();

            DataTable table = new DataTable();
            table.Columns.Add("CHECKED", typeof(string));
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("REMARK", typeof(string));
            foreach (var v in this._alternateValve)
            {
                DataRow pDataRow = table.NewRow();
                pDataRow[0] = "false";
                pDataRow[1] = v.Key;
                pDataRow[2] = v.Value.Description;
                table.Rows.Add(pDataRow);
            }

            ultraGrid_Pipe.DataSource = table;

            return true;
        }


        /// <summary>
        /// 그리드 행을 더블클릭시 위치이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row == null) return;
            UltraGrid Grid = (UltraGrid)sender;
            ILayer layer = this._mapExplorer.GetLayer("PIPE");
            IFeature pFeature = ArcManager.GetFeature(layer, string.Format("ID='{0}'", e.Row.Cells["ID"].Value));
            if (pFeature != null)
            {
                IRelationalOperator pRelOp = pFeature.Shape as IRelationalOperator;
                if (!pRelOp.Within(((IActiveView)this._mapExplorer.Map).Extent.Envelope as IGeometry))
                {
                    ArcManager.MoveCenterAt((IActiveView)this._mapExplorer.Map, pFeature.Shape);
                }
                ArcManager.FlashShape((IActiveView)this._mapExplorer.Map, (IGeometry)pFeature.Shape, 6, 100);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void frmAlternatePipe_Load(object sender, EventArgs e)
        {
            ultraGrid_Pipe.DoubleClickRow += new DoubleClickRowEventHandler(GridDoubleClickRow);
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            Dictionary<EpaNetwork.xPipe, string> altValve = new Dictionary<EpaNetwork.xPipe, string>();
            #region validation check
            foreach (UltraGridRow row in ultraGrid_Pipe.Rows)
            {
                if (bool.Parse(row.Cells["CHECKED"].Value.ToString()))
                {
                    EpaNetwork.xPipe valve = WC_Global.crisisAnalysisOp.WorkModel.NETWORK.FindLink(EMFrame.utils.ConvertUtils.ToString(row.Cells["ID"].Value)) as EpaNetwork.xPipe;
                    if (valve != null)
                        altValve.Add(valve, valve.Status);
                }
            }
            if (altValve.Count == 0)
            {
                MessageManager.ShowInformationMessage("비상공급관을 선택하지 않았습니다.");
                return;
            }
            #endregion validation check

            try
            {
                foreach (var valve in altValve)
                {
                    valve.Key.Status = "OPEN";
                }
                ////단수구역 분석 실행
                ((frmTransferMain)this.ParentForm).ExecuteExtendAnalysis(altValve);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ///비상공급관 선택후 수리해석완료 -> 원래대로 복원(Closed)
                foreach (var valve in altValve) valve.Key.Status = valve.Value;
            }

            //List<string> ADD_PIPE = new List<string>();
            //// 단수구역 분석 재실행
            //foreach (UltraGridRow item in ultraGrid_Pipe.Rows)
            //{
            //    if (Convert.ToBoolean(item.Cells["CHECKED"].Value))
            //    {
            //        ADD_PIPE.Add(Convert.ToString(item.Cells["ID"].Value));
            //    }
            //}


            //if (ADD_PIPE.Count == 0)
            //{
            //    MessageManager.ShowInformationMessage("Open으로 선택한 비상공급관이 없습니다.");
            //    return;
            //}
            ////단수구역 분석 실행
            //frmTransferMain oform = this.ParentForm as frmTransferMain;
            //if (oform == null) return;
            //oform.ExecuteExtendAnalysis(ADD_PIPE);
        }
    }
}
