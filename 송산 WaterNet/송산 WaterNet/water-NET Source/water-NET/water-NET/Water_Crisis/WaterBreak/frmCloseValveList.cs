﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

using WaterNet.WC_Common;
using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;
using QuickGraph;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WC_CrisisTransfer
{
    public partial class frmCloseValveList : Form
    {
        /// <summary>
        /// 폼을 최소화하기 위한 멤버선언
        /// </summary>
        private System.Drawing.Size m_formsize = new System.Drawing.Size(); //폼사이즈
        private System.Drawing.Point m_formLocation = new System.Drawing.Point(); //폼위치
        private IntPtr m_formParam = new IntPtr();                                //최종 윈도우 파라메터
        //-------------------------------------------------------------------------------------------------
        private IMapControl3 m_mapcontrol;
        private ESRI.ArcGIS.Carto.IMap m_map;
        private BidirectionalGraph<Vertex, Edge<Vertex>> m_ClosedGraph;

        public frmCloseValveList()
        {
            InitializeComponent();
            InitializeSetting();
        }

        /// <summary>
        /// 윈도우 메세지 후킹 : 화면 최소화, 복구 이벤트 기능
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                if (m.WParam == new IntPtr(0xF020)) // SC_MINIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    m_formsize = this.Size;
                    m_formLocation = this.Location;
                    m_formParam = m.WParam;

                    this.Size = new System.Drawing.Size(150, SystemInformation.CaptionHeight);
                    if (this.ParentForm != null)
                        this.Location = new System.Drawing.Point(this.ParentForm.Width - 170, this.ParentForm.Height - 40);
                    else
                        this.Location = new System.Drawing.Point(0, 0);

                    this.BringToFront();
                    m.Result = new IntPtr(0);
                    return;
                }
                else if (m.WParam == new IntPtr(0xF030))  // SC_MAXIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    if (m_formsize.Height == 0 | m_formsize.Width == 0) return;

                    this.Size = m_formsize;
                    this.Location = m_formLocation;
                    this.BringToFront();
                    m_formParam = m.WParam;

                    m.Result = new IntPtr(0);
                    return;
                }
                else if ((m.WParam.ToInt32() >= 61441) && (m.WParam.ToInt32() <= 61448))  // SC_SIZE변경
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF032))  //최대화(타이틀바 더블클릭)시.
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF060))  //SC_CLOSE버튼 실행안함.
                {
                    return;
                }
                m.Result = new IntPtr(0);
            }

            base.WndProc(ref m);
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            InitializeGridSetting();
        }

        private void InitializeGridSetting()
        {
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            ///차단제수밸브
            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "CHECKED";
            oUltraGridColumn.Header.Caption = "선택";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FID";
            oUltraGridColumn.Header.Caption = "OBJECTID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "Shape";
            oUltraGridColumn.Header.Caption = "Shape";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_CDE";
            oUltraGridColumn.Header.Caption = "FTR_CDE";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_IDN";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MNG_CDE";
            oUltraGridColumn.Header.Caption = "관리기관";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VAL_MOF";
            oUltraGridColumn.Header.Caption = "형식";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VAL_MOP";
            oUltraGridColumn.Header.Caption = "재질";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VAL_DIP";
            oUltraGridColumn.Header.Caption = "구경";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Valve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WID_CDE";
            oUltraGridColumn.Header.Caption = "지방/광역";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;


            ///이토밸브
            oUltraGridColumn = ultraGrid_OutValve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FID";
            oUltraGridColumn.Header.Caption = "OBJECTID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_OutValve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "Shape";
            oUltraGridColumn.Header.Caption = "Shape";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_OutValve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_CDE";
            oUltraGridColumn.Header.Caption = "FTR_CDE";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_OutValve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_IDN";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_OutValve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MNG_CDE";
            oUltraGridColumn.Header.Caption = "관리기관";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_OutValve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VAL_MOF";
            oUltraGridColumn.Header.Caption = "형식";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_OutValve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VAL_MOP";
            oUltraGridColumn.Header.Caption = "재질";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_OutValve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "VAL_DIP";
            oUltraGridColumn.Header.Caption = "구경";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_OutValve.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WID_CDE";
            oUltraGridColumn.Header.Caption = "지방/광역";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;


            ///상수관로
            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FID";
            oUltraGridColumn.Header.Caption = "OBJECTID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "Shape";
            oUltraGridColumn.Header.Caption = "Shape";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_CDE";
            oUltraGridColumn.Header.Caption = "FTR_CDE";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_IDN";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MNG_CDE";
            oUltraGridColumn.Header.Caption = "관리기관";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SAA_CDE";
            oUltraGridColumn.Header.Caption = "관로종류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MOP_CDE";
            oUltraGridColumn.Header.Caption = "관재질";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIP_DIP";
            oUltraGridColumn.Header.Caption = "관경";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIP_LEN";
            oUltraGridColumn.Header.Caption = "관연장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WID_CDE";
            oUltraGridColumn.Header.Caption = "지방/광역";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_PipeLs.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIP_LBL";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            FormManager.SetGridStyle(ultraGrid_OutValve);
            FormManager.SetGridStyle(ultraGrid_Valve);
            FormManager.SetGridStyle(ultraGrid_PipeLs);
            FormManager.SetGridStyle_ColumeAllowEdit(ultraGrid_Valve, 0); //CHECKED

            #endregion
        }
        #endregion 초기화설정

        #region Public Property
        /// <summary>
        /// Focus Map 설정
        /// </summary>
        public IMapControl3 mapControl
        {
            set
            {
                m_mapcontrol = value;
                m_map = m_mapcontrol.ActiveView.FocusMap;
            }
        }

        public BidirectionalGraph<Vertex, Edge<Vertex>> ClosedGraph
        {
            set
            {
                this.m_ClosedGraph = value;
            }
        }
        #endregion Public Property

        private void DisplayGridData()
        {
            EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.m_map);
            {   ///상수관로
                var value = this.m_ClosedGraph.Edges.Where(entry => (entry != null && (new string[] { "상수관로", "WTL_PIPE_LS" }).Contains(entry.CLASSNAME)))
                                           .GroupBy(entry => entry.OBJECTID)
                                           .Select(grp => string.Format("{0}", grp.First().OBJECTID)).ToArray();

                string fieldsClause = "FID,FTR_IDN, FTR_CDE, MNG_CDE, SAA_CDE, MOP_CDE, PIP_DIP, PIP_LEN, WID_CDE, PIP_LBL";
                string whereClause = value.Length > 0 ? string.Format("FID IN ({0})", string.Join(",", value)) : string.Format("FID IN (null)", string.Empty);
                EAGL.Search.AttributeSearcher at1 = new EAGL.Search.AttributeSearcher((IFeatureLayer)mapExplorer.GetLayer("상수관로"));
                at1.FieldsClause = fieldsClause;
                at1.WhereClause = whereClause;
                System.Data.DataTable t1 = at1.Search();
                this.ultraGrid_PipeLs.DataSource = t1;
            }

            {   ///차단제수밸브
                var value = this.m_ClosedGraph.Vertices.Where(entry => (entry != null && (new string[] { "밸브", "WTL_VALV_PS" }).Contains(entry.CLASSNAME))
                                          && (entry.HashData["FTR_CDE"]).Equals("SA200"))
                                         .GroupBy(entry => entry.OBJECTID)
                                         .Select(grp => string.Format("{0}", grp.First().OBJECTID)).ToArray();

                string fieldsClause = "FID,FTR_IDN, FTR_CDE, MNG_CDE, VAL_MOF, VAL_MOP, VAL_DIP, WID_CDE";
                string whereClause = value.Length > 0 ? string.Format("FID IN ({0})", string.Join(",", value)) : string.Format("FID IN (null)", string.Empty);
                EAGL.Search.AttributeSearcher at1 = new EAGL.Search.AttributeSearcher((IFeatureLayer)mapExplorer.GetLayer("밸브"));
                at1.FieldsClause = fieldsClause;
                at1.WhereClause = whereClause;
                System.Data.DataTable t1 = at1.Search();
                this.ultraGrid_Valve.DataSource = t1;

                foreach (UltraGridRow row in ultraGrid_Valve.Rows)
                    row.Cells["CHECKED"].Value = "false";
            }
            {   ///이토밸브
                var value = this.m_ClosedGraph.Vertices.Where(entry => (entry != null && (new string[] { "밸브", "WTL_VALV_PS" }).Contains(entry.CLASSNAME))
                                          && (entry.HashData["FTR_CDE"]).Equals("SA202"))
                                         .GroupBy(entry => entry.OBJECTID)
                                         .Select(grp => string.Format("{0}", grp.First().OBJECTID)).ToArray();

                string fieldsClause = "FID,FTR_IDN, FTR_CDE, MNG_CDE, VAL_MOF, VAL_MOP, VAL_DIP, WID_CDE";
                string whereClause = value.Length > 0 ? string.Format("FID IN ({0})", string.Join(",", value)) : string.Format("FID IN (null)", string.Empty);
                EAGL.Search.AttributeSearcher at1 = new EAGL.Search.AttributeSearcher((IFeatureLayer)mapExplorer.GetLayer("밸브"));
                at1.FieldsClause = fieldsClause;
                at1.WhereClause = whereClause;
                System.Data.DataTable t1 = at1.Search();
                this.ultraGrid_OutValve.DataSource = t1;
            }
        }
        #region Public Method
        /// <summary>
        /// 실행
        /// </summary>
        public Boolean Open()
        {
            this.DisplayGridData();

            this.Show();

            return true;
        }

        #endregion Public Method


        #region Private Method
        /// <summary>
        /// 닫기 버튼실행시 : Form.Hide()
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        /// <summary>
        /// 그리드 행을 더블클릭시 위치이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row == null) return;
            UltraGrid Grid = (UltraGrid)sender;
            IFeature pFeature = null;
            switch (Grid.Name)
            {
                case "ultraGrid_Valve":
                case "ultraGrid_OutValve":
                    pFeature = ArcManager.GetFeature(ArcManager.GetMapLayer(m_map, "밸브") as IFeatureLayer, Convert.ToInt32(e.Row.Cells["FID"].Value));
                    break;
                //case "ultraGrid_Fire":
                //    pFeature = ArcManager.GetFeature(ArcManager.GetMapLayer(m_map, "소방시설") as IFeatureLayer, Convert.ToInt32(e.Row.Cells["FID"].Value));
                //    break;
                case "ultraGrid_PipeLs":
                    pFeature = ArcManager.GetFeature(ArcManager.GetMapLayer(m_map, "상수관로") as IFeatureLayer, Convert.ToInt32(e.Row.Cells["FID"].Value));
                    break;
                case "ultraGrid_SplyLs":
                    pFeature = ArcManager.GetFeature(ArcManager.GetMapLayer(m_map, "급수관로") as IFeatureLayer, Convert.ToInt32(e.Row.Cells["FID"].Value));
                    break;
            }


            if (pFeature != null)
            {
                //if (Int32.TryParse(comboBox_Scale.Text, out nscale)) ArcManager.SetMapScale(m_mapcontrol, (double)nscale);
                IRelationalOperator pRelOp = pFeature.Shape as IRelationalOperator;
                if (!pRelOp.Within(m_mapcontrol.ActiveView.Extent.Envelope as IGeometry))
                {
                    ArcManager.MoveCenterAt(m_mapcontrol, pFeature);
                    Application.DoEvents();
                }

                ArcManager.FlashShape(m_mapcontrol.ActiveView, (IGeometry)pFeature.Shape, 5, 100);
            }

        }

        /// <summary>
        /// 폼 로드시 그리드 이벤트 처리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmCloseValveList_Load(object sender, EventArgs e)
        {
            this.ultraGrid_Valve.DoubleClickRow += new DoubleClickRowEventHandler(GridDoubleClickRow);
            this.ultraGrid_OutValve.DoubleClickRow += new DoubleClickRowEventHandler(GridDoubleClickRow);
            this.ultraGrid_PipeLs.DoubleClickRow += new DoubleClickRowEventHandler(GridDoubleClickRow);
        }
        #endregion Private Method

        /// <summary>
        /// 차단제수밸브 검색-확장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExecute_Click(object sender, EventArgs e)
        {
            List<string> ignorevalve = new List<string>();
            foreach (UltraGridRow row in ultraGrid_Valve.Rows)
            {
                if (bool.Parse(row.Cells["CHECKED"].Value.ToString()))
                    ignorevalve.Add(Convert.ToString(row.Cells["FID"].Value));
            }

            if (ignorevalve.Count == 0)
            {
                MessageManager.ShowInformationMessage("제외할 제수밸브가 선택되지 않았습니다.");
                return;
            }

            ///제수밸브(차단)
            var Valves = this.m_ClosedGraph.Vertices.Where(entry => (entry != null
                                                      && (entry.CLASSNAME.Equals("밸브") || entry.CLASSNAME.Equals("WTL_VALV_PS")))
                                                      && (entry.HashData["FTR_CDE"]).Equals("SA200")
                                                      && (ignorevalve.ToArray().Contains(entry.OBJECTID.ToString())))
                                       .Select(grp => grp).ToList();
            Form oform = this.ParentForm;
            ((frmTransferMain)oform).ExecuteExtendCloseAnalysis(ref this.m_ClosedGraph, Valves as List<Vertex>);

            this.DisplayGridData();
        }
    }
}
