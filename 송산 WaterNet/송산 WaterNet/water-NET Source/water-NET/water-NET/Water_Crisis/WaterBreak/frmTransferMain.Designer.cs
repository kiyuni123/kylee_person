﻿namespace WaterNet.WC_CrisisTransfer
{
    partial class frmTransferMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTransferMain));
            this.btnBrokePoint = new System.Windows.Forms.Button();
            this.btnSelModel = new System.Windows.Forms.Button();
            this.btnAnalysis = new System.Windows.Forms.Button();
            this.btnReplacePipe = new System.Windows.Forms.Button();
            this.btnAnalysisView = new System.Windows.Forms.Button();
            this.contextMenuPopup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.chk_pres = new System.Windows.Forms.CheckBox();
            this.btnGraph2Shape = new System.Windows.Forms.Button();
            this.contextMenuSelect = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsLocalPoint = new System.Windows.Forms.ToolStripMenuItem();
            this.tsGlobalPoint = new System.Windows.Forms.ToolStripMenuItem();
            this.panelCommand.SuspendLayout();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmMapAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.tabTOC.SuspendLayout();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            this.tabPageTOC.SuspendLayout();
            this.pnlLayerFunc.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).BeginInit();
            this.contextMenuSelect.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnGraph2Shape);
            this.panelCommand.Controls.Add(this.btnAnalysisView);
            this.panelCommand.Controls.Add(this.btnReplacePipe);
            this.panelCommand.Controls.Add(this.btnAnalysis);
            this.panelCommand.Controls.Add(this.btnSelModel);
            this.panelCommand.Controls.Add(this.btnBrokePoint);
            // 
            // spcContents
            // 
            this.spcContents.Location = new System.Drawing.Point(24, 24);
            this.spcContents.Size = new System.Drawing.Size(943, 559);
            // 
            // axToolbar
            // 
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            this.axToolbar.Size = new System.Drawing.Size(680, 28);
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.DefaultGroupSettings.ForceSerialization = true;
            this.DockManagerCommand.DefaultPaneSettings.ForceSerialization = true;
            // 
            // _frmMapUnpinnedTabAreaLeft
            // 
            this._frmMapUnpinnedTabAreaLeft.Location = new System.Drawing.Point(0, 24);
            this._frmMapUnpinnedTabAreaLeft.Size = new System.Drawing.Size(24, 559);
            // 
            // _frmMapUnpinnedTabAreaRight
            // 
            this._frmMapUnpinnedTabAreaRight.Location = new System.Drawing.Point(967, 24);
            this._frmMapUnpinnedTabAreaRight.Size = new System.Drawing.Size(0, 559);
            // 
            // _frmMapUnpinnedTabAreaTop
            // 
            this._frmMapUnpinnedTabAreaTop.Location = new System.Drawing.Point(24, 24);
            this._frmMapUnpinnedTabAreaTop.Size = new System.Drawing.Size(943, 0);
            // 
            // _frmMapUnpinnedTabAreaBottom
            // 
            this._frmMapUnpinnedTabAreaBottom.Size = new System.Drawing.Size(943, 0);
            // 
            // _frmMapAutoHideControl
            // 
            this._frmMapAutoHideControl.Size = new System.Drawing.Size(11, 583);
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.TabIndex = 13;
            // 
            // tabTOC
            // 
            this.tabTOC.Size = new System.Drawing.Size(251, 378);
            // 
            // spcTOC
            // 
            this.spcTOC.Size = new System.Drawing.Size(251, 378);
            // 
            // tabPageAddr
            // 
            this.tabPageAddr.Size = new System.Drawing.Size(243, 376);
            // 
            // tvBlock
            // 
            this.tvBlock.LineColor = System.Drawing.Color.Black;
            this.tvBlock.Size = new System.Drawing.Size(243, 376);
            // 
            // tabPageTOC
            // 
            this.tabPageTOC.Size = new System.Drawing.Size(243, 352);
            // 
            // pnlLayerFunc
            // 
            this.pnlLayerFunc.Controls.Add(this.chk_pres);
            this.pnlLayerFunc.Location = new System.Drawing.Point(0, 331);
            // 
            // spcIndexMap
            // 
            this.spcIndexMap.Size = new System.Drawing.Size(257, 559);
            // 
            // axTOC
            // 
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            this.axTOC.Size = new System.Drawing.Size(243, 331);
            // 
            // axMap
            // 
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Size = new System.Drawing.Size(680, 500);
            this.axMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(this.axMap_OnMouseDown);
            // 
            // btnBrokePoint
            // 
            this.btnBrokePoint.Location = new System.Drawing.Point(3, 66);
            this.btnBrokePoint.Name = "btnBrokePoint";
            this.btnBrokePoint.Size = new System.Drawing.Size(62, 54);
            this.btnBrokePoint.TabIndex = 2;
            this.btnBrokePoint.Text = "사고지점 선정";
            this.btnBrokePoint.UseVisualStyleBackColor = true;
            // 
            // btnSelModel
            // 
            this.btnSelModel.Location = new System.Drawing.Point(3, 6);
            this.btnSelModel.Name = "btnSelModel";
            this.btnSelModel.Size = new System.Drawing.Size(62, 54);
            this.btnSelModel.TabIndex = 3;
            this.btnSelModel.Text = "해석모델선택";
            this.btnSelModel.UseVisualStyleBackColor = true;
            this.btnSelModel.Click += new System.EventHandler(this.btnSelModel_Click);
            // 
            // btnAnalysis
            // 
            this.btnAnalysis.Location = new System.Drawing.Point(3, 126);
            this.btnAnalysis.Name = "btnAnalysis";
            this.btnAnalysis.Size = new System.Drawing.Size(62, 54);
            this.btnAnalysis.TabIndex = 4;
            this.btnAnalysis.Text = "단수지역 분석";
            this.btnAnalysis.UseVisualStyleBackColor = true;
            this.btnAnalysis.Click += new System.EventHandler(this.btnAnalysis_Click);
            // 
            // btnReplacePipe
            // 
            this.btnReplacePipe.Location = new System.Drawing.Point(3, 186);
            this.btnReplacePipe.Name = "btnReplacePipe";
            this.btnReplacePipe.Size = new System.Drawing.Size(62, 54);
            this.btnReplacePipe.TabIndex = 5;
            this.btnReplacePipe.Text = "대체관로 탐색";
            this.btnReplacePipe.UseVisualStyleBackColor = true;
            this.btnReplacePipe.Click += new System.EventHandler(this.btnReplacePipe_Click);
            // 
            // btnAnalysisView
            // 
            this.btnAnalysisView.Location = new System.Drawing.Point(3, 246);
            this.btnAnalysisView.Name = "btnAnalysisView";
            this.btnAnalysisView.Size = new System.Drawing.Size(62, 54);
            this.btnAnalysisView.TabIndex = 6;
            this.btnAnalysisView.Text = "분석결과조회";
            this.btnAnalysisView.UseVisualStyleBackColor = true;
            this.btnAnalysisView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnAnalysisView_MouseDown);
            // 
            // contextMenuPopup
            // 
            this.contextMenuPopup.Name = "contextMenuPopup";
            this.contextMenuPopup.Size = new System.Drawing.Size(61, 4);
            // 
            // chk_pres
            // 
            this.chk_pres.AutoSize = true;
            this.chk_pres.Location = new System.Drawing.Point(3, 2);
            this.chk_pres.Name = "chk_pres";
            this.chk_pres.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chk_pres.Size = new System.Drawing.Size(88, 16);
            this.chk_pres.TabIndex = 0;
            this.chk_pres.Text = "수압값 보기";
            this.chk_pres.UseVisualStyleBackColor = true;
            // 
            // btnGraph2Shape
            // 
            this.btnGraph2Shape.Location = new System.Drawing.Point(4, 389);
            this.btnGraph2Shape.Name = "btnGraph2Shape";
            this.btnGraph2Shape.Size = new System.Drawing.Size(62, 54);
            this.btnGraph2Shape.TabIndex = 7;
            this.btnGraph2Shape.Text = "Graph->Shape";
            this.btnGraph2Shape.UseVisualStyleBackColor = true;
            this.btnGraph2Shape.Click += new System.EventHandler(this.btnGraph2Shape_Click);
            // 
            // contextMenuSelect
            // 
            this.contextMenuSelect.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLocalPoint,
            this.tsGlobalPoint});
            this.contextMenuSelect.Name = "contextMenuPopup";
            this.contextMenuSelect.Size = new System.Drawing.Size(153, 70);
            // 
            // tsLocalPoint
            // 
            this.tsLocalPoint.Name = "tsLocalPoint";
            this.tsLocalPoint.Size = new System.Drawing.Size(152, 22);
            this.tsLocalPoint.Text = "지방관로";
            // 
            // tsGlobalPoint
            // 
            this.tsGlobalPoint.Name = "tsGlobalPoint";
            this.tsGlobalPoint.Size = new System.Drawing.Size(152, 22);
            this.tsGlobalPoint.Text = "광역관로";
            // 
            // frmTransferMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.ClientSize = new System.Drawing.Size(967, 583);
            this.Name = "frmTransferMain";
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaLeft, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaRight, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaBottom, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaTop, 0);
            this.Controls.SetChildIndex(this.windowDockingArea1, 0);
            this.Controls.SetChildIndex(this.spcContents, 0);
            this.Controls.SetChildIndex(this._frmMapAutoHideControl, 0);
            this.panelCommand.ResumeLayout(false);
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.Panel2.PerformLayout();
            this.spcContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmMapAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.tabTOC.ResumeLayout(false);
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            this.tabPageTOC.ResumeLayout(false);
            this.pnlLayerFunc.ResumeLayout(false);
            this.pnlLayerFunc.PerformLayout();
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).EndInit();
            this.contextMenuSelect.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBrokePoint;
        private System.Windows.Forms.Button btnSelModel;
        private System.Windows.Forms.Button btnReplacePipe;
        private System.Windows.Forms.Button btnAnalysis;
        private System.Windows.Forms.Button btnAnalysisView;
        private System.Windows.Forms.ContextMenuStrip contextMenuPopup;
        private System.Windows.Forms.CheckBox chk_pres;
        private System.Windows.Forms.Button btnGraph2Shape;
        private System.Windows.Forms.ContextMenuStrip contextMenuSelect;
        private System.Windows.Forms.ToolStripMenuItem tsLocalPoint;
        private System.Windows.Forms.ToolStripMenuItem tsGlobalPoint;
    }
}
