﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Output;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

using WaterNet.WC_Common;
using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;
using QuickGraph;
#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion
namespace WaterNet.WC_CrisisTransfer
{
    public partial class frmBreakDesc : Form
    {
        /// <summary>
        /// 폼을 최소화하기 위한 멤버선언
        /// </summary>
        private System.Drawing.Size m_formsize = new System.Drawing.Size(); //폼사이즈
        private System.Drawing.Point m_formLocation = new System.Drawing.Point(); //폼위치
        private IntPtr m_formParam = new IntPtr();                                //최종 윈도우 파라메터
        //-------------------------------------------------------------------------------------------------
        //private QuickGraph.BidirectionalGraph<QuickGraph.Vertex, Edge<QuickGraph.Vertex>> graph = null; //geometry graph

        private IMapControl3 m_mapcontrol;
        private ESRI.ArcGIS.Carto.IMap m_map;
        private Dictionary<string, string> m_dicDmcell = new Dictionary<string,string>(); //수용가정보(WI_DMINFO)의 전화번호정보
        private List<string> _minus_pipe;

        public frmBreakDesc()
        {
            InitializeComponent();
            InitializeSetting();
            InitializeGridSetting();
        }

        /// <summary>
        /// 윈도우 메세지 후킹 : 화면 최소화, 복구 이벤트 기능
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                if (m.WParam == new IntPtr(0xF020)) // SC_MINIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    m_formsize = this.Size;
                    m_formLocation = this.Location;
                    m_formParam = m.WParam;

                    this.Size = new System.Drawing.Size(150, SystemInformation.CaptionHeight);
                    if (this.ParentForm != null)
                        this.Location = new System.Drawing.Point(this.ParentForm.Width - 170, this.ParentForm.Height - 40);
                    else
                        this.Location = new System.Drawing.Point(0, 0);

                    this.BringToFront();
                    m.Result = new IntPtr(0);
                    return;
                }
                else if (m.WParam == new IntPtr(0xF030))  // SC_MAXIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    if (m_formsize.Height == 0 | m_formsize.Width == 0) return;

                    this.Size = m_formsize;
                    this.Location = m_formLocation;
                    this.BringToFront();
                    m_formParam = m.WParam;

                    m.Result = new IntPtr(0);
                    return;
                }
                else if ((m.WParam.ToInt32() >= 61441) && (m.WParam.ToInt32() <= 61448))  // SC_SIZE변경
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF032))  //최대화(타이틀바 더블클릭)시.
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF060))  //SC_CLOSE버튼 실행안함.
                {
                    return;
                }
                m.Result = new IntPtr(0);
            }

            base.WndProc(ref m);
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            ///DataBase Connect
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);
                DataTable table = mapper.ExecuteScriptDataTable("SELECT DMNO, DMCELL FROM WI_DMINFO WHERE DMNO IS NOT NULL", null);

                this.m_dicDmcell = table.AsEnumerable().Select(entry => entry).ToDictionary(key => string.Format("{0}", key["DMNO"]), key => string.Format("{0}", key["DMCELL"]));
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
            }
            finally
            {
                if (mapper != null) mapper.Close();
            }

            comboBox_Scale.Items.Add("1000");
            comboBox_Scale.Items.Add("2000");
            comboBox_Scale.Items.Add("3000");
            comboBox_Scale.Items.Add("5000");
            comboBox_Scale.Items.Add("10000");
            comboBox_Scale.Items.Add("15000");
            comboBox_Scale.Items.Add("20000");
            comboBox_Scale.Items.Add("30000");
            comboBox_Scale.Items.Add("50000");
            comboBox_Scale.Items.Add("100000");

        }

        private void InitializeGridSetting()
        {
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;
            ///상수관로
            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FID";
            oUltraGridColumn.Header.Caption = "OBJECTID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "Shape";
            oUltraGridColumn.Header.Caption = "Shape";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;


            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_CDE";
            oUltraGridColumn.Header.Caption = "FTR_CDE";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_IDN";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MNG_CDE";
            oUltraGridColumn.Header.Caption = "관리기관";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SAA_CDE";
            oUltraGridColumn.Header.Caption = "관로종류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MOP_CDE";
            oUltraGridColumn.Header.Caption = "관재질";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIP_DIP";
            oUltraGridColumn.Header.Caption = "관경";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIP_LEN";
            oUltraGridColumn.Header.Caption = "관연장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WID_CDE";
            oUltraGridColumn.Header.Caption = "지방/광역";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Pipe.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIP_LBL";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            ///급수관로
            ///
            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FID";
            oUltraGridColumn.Header.Caption = "OBJECTID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "Shape";
            oUltraGridColumn.Header.Caption = "Shape";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            //oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "SGCCD";
            //oUltraGridColumn.Header.Caption = "SGCCD";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = true;
            //oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_CDE";
            oUltraGridColumn.Header.Caption = "FTR_CDE";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_IDN";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MNG_CDE";
            oUltraGridColumn.Header.Caption = "관리기관";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SAA_CDE";
            oUltraGridColumn.Header.Caption = "관로종류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MOP_CDE";
            oUltraGridColumn.Header.Caption = "관재질";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIP_DIP";
            oUltraGridColumn.Header.Caption = "관경";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIP_LEN";
            oUltraGridColumn.Header.Caption = "관연장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WID_CDE";
            oUltraGridColumn.Header.Caption = "지방/광역";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Sply.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PIP_LBL";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;           

            ///수도계량기
            ///
            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FID";
            oUltraGridColumn.Header.Caption = "OBJECTID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "Shape";
            oUltraGridColumn.Header.Caption = "Shape";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            //oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "SGCCD";
            //oUltraGridColumn.Header.Caption = "SGCCD";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = true;
            //oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_CDE";
            oUltraGridColumn.Header.Caption = "FTR_CDE";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FTR_IDN";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNO";
            oUltraGridColumn.Header.Caption = "수용가번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.MaskDataMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            oUltraGridColumn.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            oUltraGridColumn.MaskInput = "#####-##-#####";
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 120;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMNM";
            oUltraGridColumn.Header.Caption = "수용가명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "DMCELL";
            oUltraGridColumn.Header.Caption = "전화번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MET_DIP";
            oUltraGridColumn.Header.Caption = "구경";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MET_MOF";
            oUltraGridColumn.Header.Caption = "형식";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MET_CST";
            oUltraGridColumn.Header.Caption = "상태";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "IST_YMD";
            oUltraGridColumn.Header.Caption = "설치일";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = ultraGrid_Meta.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MET_NUM";
            oUltraGridColumn.Header.Caption = "계량기번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            FormManager.SetGridStyle(ultraGrid_Meta);
            FormManager.SetGridStyle(ultraGrid_Sply);
            FormManager.SetGridStyle(ultraGrid_Pipe);

            #endregion

        }
        #endregion 초기화설정

        /// <summary>
        /// 행정구역 코드 ValueList
        /// </summary>
        private void SetValuelist_HjdCde()
        {
            Infragistics.Win.ValueList objValue = new Infragistics.Win.ValueList();
            ILayer pLayer = ArcManager.GetMapLayer(m_map, "행정읍면동");
            if (pLayer != null)
            {
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    ICursor pCursor = ArcManager.GetCursor(pLayer,  string.Empty);
                    comReleaser.ManageLifetime(pCursor);

                    IRow pRow = pCursor.NextRow();
                    while (pRow != null)
                    {
                        objValue.ValueListItems.Add(Convert.ToString(pRow.get_Value(pRow.Fields.FindField("HJD_CDE"))).Substring(0,8),Convert.ToString(pRow.get_Value(pRow.Fields.FindField("HJD_NAM"))));
                        pRow = pCursor.NextRow();
                    }

                }
            }
            
            this.ultraGrid_Meta.DisplayLayout.Bands[0].Columns["HJD_CDE"].ValueList = objValue.Clone();
            this.ultraGrid_Pipe.DisplayLayout.Bands[0].Columns["HJD_CDE"].ValueList = objValue.Clone();
            this.ultraGrid_Sply.DisplayLayout.Bands[0].Columns["HJD_CDE"].ValueList = objValue.Clone();

        }

        #region Public Property
        public List<string> minus_pipe
        {
            set { this._minus_pipe = value; }
        }
        
        /// <summary>
        /// Focus Map 설정
        /// </summary>
        public IMapControl3 mapControl
        {
            set
            {
                m_mapcontrol = value;
                m_map = m_mapcontrol.ActiveView.FocusMap;
            }
        }

        #endregion Public Property

        private void ShowMessage()
        {
            this.tabControl.Visible = false;
            Label message = new Label();
            message.Parent = this.splitContainer_Image.Panel1;
            message.Dock = DockStyle.Fill;
            message.TextAlign = ContentAlignment.MiddleCenter;
            message.Text = "단수지역이 없습니다.";
            message.Show();
        }

        #region Public Method
        public Boolean Open()
        {

            WaterNetCore.frmSplashMsg oSplash = new frmSplashMsg();
            oSplash.Message = "단수 시설물정보를 검색중입니다. 잠시만 기다리십시오!";
            oSplash.Show();
            Application.DoEvents();

            this.CreateMapImage();
            if (this._minus_pipe == null || this._minus_pipe.Count == 0)
            {
                this.ShowMessage();
                this.Show();
                oSplash.Close();
                return true;
            }

            //this.SetValuelist_HjdCde(); //그리드에 행정읍면동을 valvulist로 설정한다.

            try
            {
                //#region 단수지역의 상수관로, 급수관로, 수도계량기의 시설물정보를 검색한다.(그래프검색)
                ///1. 수압 <= 0.0 인 junction객체에 인접한 상수관로(wtl_pipe_ls)를 공간검색(Buffer = 0.01)한다.
                ///2. 인접한 상수관로 목록으로 그래프에서 검색한다.
                ///3. 그래프에서 검색된 edgeDic, metaDic에서 각 시설물을 분리한다.
                try
                {
                    EAGL.Data.Wrapper.ITableWrapper twr = VariableManager.LAYER_DIC["상수관로"];
                    var t1 = (from row in twr.DATATABLE.AsEnumerable()
                              where this._minus_pipe.Contains(string.Format("{0}", row["FTR_IDN"]))
                              select row).CopyToDataTable();

                    string[] cols = { "FID", "FTR_IDN", "FTR_CDE", "MNG_CDE", "SAA_CDE", "MOP_CDE", "PIP_DIP", "PIP_LEN", "WID_CDE", "PIP_LBL" };
                    for (int i = t1.Columns.Count - 1; i >= 0; i--)
                    {
                        if (!cols.Contains(t1.Columns[i].ColumnName))
                            t1.Columns.RemoveAt(i);
                    }

                    this.ultraGrid_Pipe.DataSource = t1;
                }
                catch {}

                try
                {
                    EAGL.Data.Wrapper.ITableWrapper twr = VariableManager.LAYER_DIC["급수관로"];
                    var t1 = (from row in twr.DATATABLE.AsEnumerable()
                              where this._minus_pipe.Contains(string.Format("{0}", row["PIP_IDN"]))
                              select row).CopyToDataTable();
                    string[] cols = { "FID", "FTR_IDN", "FTR_CDE", "MNG_CDE", "SAA_CDE", "MOP_CDE", "PIP_DIP", "PIP_LEN", "WID_CDE", "PIP_LBL" };
                    for (int i = t1.Columns.Count - 1; i >= 0; i--)
                    {
                        if (!cols.Contains(t1.Columns[i].ColumnName))
                            t1.Columns.RemoveAt(i);
                    }

                    this.ultraGrid_Sply.DataSource = t1;
                }
                catch {}

                try
                {
                    var q1 = (from row in ((DataTable)this.ultraGrid_Sply.DataSource).AsEnumerable()
                              select new { IDN = string.Format("{0}", row["FTR_IDN"]) }).Distinct();
                    List<string> minus_sply = q1.Select(entry => entry.IDN).ToList();

                    EAGL.Data.Wrapper.ITableWrapper twr = VariableManager.LAYER_DIC["수도계량기"];
                    var t1 = (from row in twr.DATATABLE.AsEnumerable()
                              where minus_sply.Contains(string.Format("{0}", row["PIP_IDN"]))
                              select row).CopyToDataTable();

                    string[] cols = { "FID", "DMNO", "DMNM", "FTR_IDN", "FTR_CDE", "IST_YMD", "MET_NUM", "MET_CST", "MET_DIP", "MET_MOF" };
                    for (int i = t1.Columns.Count - 1; i >= 0; i--)
                    {
                        if (!cols.Contains(t1.Columns[i].ColumnName))
                            t1.Columns.RemoveAt(i);
                    }

                    this.ultraGrid_Meta.DataSource = t1;
                }
                catch { }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                oSplash.Close();

                string strCount = "상수관로 : " + ultraGrid_Pipe.Rows.Count.ToString() + " 건";
                strCount += " , 급수관로 : " + ultraGrid_Sply.Rows.Count.ToString() + " 건";
                strCount += " , 수도계량기 : " + ultraGrid_Meta.Rows.Count.ToString() + " 건";
                label_Count.Text = strCount;
            }

            this.Show();

            return true;
        }
        #endregion Public Method

        /// <summary>
        /// 지도를 그림파일로 저장한다.
        /// </summary>
        private void CreateMapImage()
        {
            try
            {
                Console.WriteLine("Image start : " + DateTime.Now.ToString());
                IActiveView pActiveView = m_map as IActiveView;
                int iOutputResolution = 96;
                double dScreenResolution = pActiveView.ScreenDisplay.DisplayTransformation.Resolution;

                IExport export = new ExportJPEGClass();
                export.ExportFileName = System.IO.Path.GetTempFileName();
                // Microsoft Windows default DPI resolution
                export.Resolution = iOutputResolution;

                ESRI.ArcGIS.esriSystem.tagRECT exportRECT = new ESRI.ArcGIS.esriSystem.tagRECT();
                exportRECT.left = 0;
                exportRECT.top = 0;
                exportRECT.right = pActiveView.ExportFrame.right * (iOutputResolution / (int)dScreenResolution);
                exportRECT.bottom = pActiveView.ExportFrame.bottom * (iOutputResolution / (int)dScreenResolution);

                ESRI.ArcGIS.Geometry.IEnvelope pPixelBoundsEnv = new ESRI.ArcGIS.Geometry.EnvelopeClass();
                pPixelBoundsEnv.PutCoords(exportRECT.left, exportRECT.top, exportRECT.right, exportRECT.bottom);
                export.PixelBounds = pPixelBoundsEnv;

                System.Int32 hDC = export.StartExporting();
                ((IActiveView)m_map).Output(hDC, (System.Int16)export.Resolution, ref exportRECT, null, null);

                export.FinishExporting();
                export.Cleanup();

                pictureMap.Image = Image.FromFile(export.ExportFileName);
                Console.WriteLine("Image end : " + DateTime.Now.ToString());
            }
            catch (Exception)
            {   
                throw;
            }


        }


        /// <summary>
        /// 그리드 행을 더블클릭시 위치이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row == null) return;
            UltraGrid Grid = (UltraGrid)sender;
            IFeature pFeature = null;
            EAGL.Explore.MapExplorer mapexplorer = new EAGL.Explore.MapExplorer(this.m_map);
            switch (Grid.Name)
            {
                case "ultraGrid_Pipe":
                    pFeature = ArcManager.GetFeature(mapexplorer.GetLayer("상수관로"), Convert.ToInt32(e.Row.Cells["FID"].Value));
                    break;
                case "ultraGrid_Sply":
                    pFeature = ArcManager.GetFeature(mapexplorer.GetLayer("급수관로"), Convert.ToInt32(e.Row.Cells["FID"].Value));
                    break;
                case "ultraGrid_Meta":
                    pFeature = ArcManager.GetFeature(mapexplorer.GetLayer("수도계량기"), Convert.ToInt32(e.Row.Cells["FID"].Value));
                    break;
                default:
                    break;
            }
            if (pFeature != null)
            {
                int nscale = 0;
                if (Int32.TryParse(comboBox_Scale.Text, out nscale))     ArcManager.SetMapScale(m_mapcontrol, (double)nscale);

                IRelationalOperator pRelOp = pFeature.Shape as IRelationalOperator;
                if (!pRelOp.Within(m_mapcontrol.ActiveView.Extent.Envelope as IGeometry))
                {
                    ArcManager.MoveCenterAt(m_mapcontrol, pFeature);
                    Application.DoEvents();
                }

                ArcManager.FlashShape(m_mapcontrol.ActiveView, (IGeometry)pFeature.Shape, 8, 100);
            }

        }

        /// <summary>
        /// 폼 로드 : 그리드 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmBreakDesc_Load(object sender, EventArgs e)
        {
            //this.SetValuelist_HjdCde(); //그리드에 행정읍면동을 valvulist로 설정한다.

            this.ultraGrid_Pipe.DoubleClickRow += new DoubleClickRowEventHandler(GridDoubleClickRow);
            this.ultraGrid_Sply.DoubleClickRow += new DoubleClickRowEventHandler(GridDoubleClickRow);
            this.ultraGrid_Meta.DoubleClickRow += new DoubleClickRowEventHandler(GridDoubleClickRow);

            this.ultraGrid_Meta.InitializeRow +=new InitializeRowEventHandler(ultraGrid_Meta_InitializeRow);
        }

        #region 버튼클릭
        /// <summary>
        /// 닫기버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        /// <summary>
        /// 지도변경 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChange_Click(object sender, EventArgs e)
        {
            CreateMapImage();
        }

        /// <summary>
        /// 엑셀출력 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter oExcelExport = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter();
                Infragistics.Excel.Workbook workbook = Infragistics.Excel.Workbook.Load(Application.StartupPath + "\\CloseWater_style.xls");

                if (workbook == null)
                {
                    MessageManager.ShowInformationMessage(Application.StartupPath + " 경로에 CloseWater_style.xls 을 찾을수 없습니다.");
                    return;
                }
                Infragistics.Excel.Worksheet worksheet = null;

                switch (EMFrame.statics.AppStatic.USER_SGCCD)
                {
                    case "45180": //정읍
                        workbook.Worksheets[0].GetCell("B21").Value = (workbook.Worksheets[0].GetCell("B21").Value.ToString()).Replace("수도관리단", "정읍수도관리단");
                        workbook.Worksheets[0].GetCell("B18").Value = (workbook.Worksheets[0].GetCell("B18").Value.ToString()).Replace("수도관리단", "정읍수도관리단");
                        workbook.Worksheets[0].GetCell("B18").Value = (workbook.Worksheets[0].GetCell("B18").Value.ToString()).Replace("전화번호1", "063-530-0246");
                        workbook.Worksheets[0].GetCell("B19").Value = (workbook.Worksheets[0].GetCell("B19").Value.ToString()).Replace("전화번호2", "011-9439-0451");
                        break;
                    case "44230": //논산
                        workbook.Worksheets[0].GetCell("B21").Value = (workbook.Worksheets[0].GetCell("B21").Value.ToString()).Replace("수도관리단", "논산수도관리단");
                        workbook.Worksheets[0].GetCell("B18").Value = (workbook.Worksheets[0].GetCell("B18").Value.ToString()).Replace("수도관리단", "논산수도관리단");
                        break;
                    case "48310": //거제
                        workbook.Worksheets[0].GetCell("B21").Value = (workbook.Worksheets[0].GetCell("B21").Value.ToString()).Replace("수도관리단", "거제수도관리단");
                        workbook.Worksheets[0].GetCell("B18").Value = (workbook.Worksheets[0].GetCell("B18").Value.ToString()).Replace("수도관리단", "거제수도관리단");
                        break;
                    default:
                        break;
                }

                worksheet = workbook.Worksheets.Add("상수관로");
                oExcelExport.Export(ultraGrid_Pipe, worksheet);
                worksheet = workbook.Worksheets.Add("급수관로");
                oExcelExport.Export(ultraGrid_Sply, worksheet);
                worksheet = workbook.Worksheets.Add("수도계량기");
                oExcelExport.Export(ultraGrid_Meta, worksheet);
                worksheet = workbook.Worksheets.Add("단수구역");

                try
                {
                    using (System.IO.MemoryStream MemStream = new System.IO.MemoryStream())
                    {
                        Image pic = pictureMap.Image as Image;
                        pic.Save(MemStream, System.Drawing.Imaging.ImageFormat.Bmp);
                        MemStream.Flush();

                        Image image = Image.FromStream(MemStream);

                        Infragistics.Excel.WorksheetImage imageShape = new Infragistics.Excel.WorksheetImage(image);
                        imageShape.PositioningMode = Infragistics.Excel.ShapePositioningMode.MoveAndSizeWithCells;
                        imageShape.TopLeftCornerCell = worksheet.Rows[1].Cells[1];
                        imageShape.BottomRightCornerCell = worksheet.Rows[26].Cells[12];

                        worksheet.Shapes.Add(imageShape);

                        string tempfile = System.IO.Path.GetTempFileName() + ".xls";
                        workbook.Save(tempfile);

                        System.Diagnostics.Process oProc = new System.Diagnostics.Process();
                        System.Diagnostics.ProcessStartInfo oProcStartInfor = new System.Diagnostics.ProcessStartInfo("Excel.exe");

                        oProcStartInfor.FileName = tempfile;
                        oProcStartInfor.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden; // 콘솔 윈도우가 보이지 않게 됩니다.

                        oProc.StartInfo = oProcStartInfor;
                        oProc.Start();

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    //throw ex;
                }
            }
            catch (Exception eex)
            {
                Logger.Error(eex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }
        #endregion 버튼클릭

        private void ultraGrid_Meta_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            string value = EMFrame.utils.ConvertUtils.ToString(e.Row.Cells["DMNO"].Value).Replace("-",string.Empty);
            if (this.m_dicDmcell.ContainsKey(value))
                e.Row.Cells["DMCELL"].Value = value;

        }
    }
}
