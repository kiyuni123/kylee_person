﻿namespace WaterNet.WC_CrisisTransfer
{
    partial class frmCloseValveList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnExtend = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.PageCloseValve = new System.Windows.Forms.TabPage();
            this.ultraGrid_Valve = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.PageOutValve = new System.Windows.Forms.TabPage();
            this.ultraGrid_OutValve = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pagePipeLs = new System.Windows.Forms.TabPage();
            this.ultraGrid_PipeLs = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelCommand.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.PageCloseValve.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Valve)).BeginInit();
            this.PageOutValve.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_OutValve)).BeginInit();
            this.pagePipeLs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_PipeLs)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnExtend);
            this.panelCommand.Controls.Add(this.btnClose);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(513, 37);
            this.panelCommand.TabIndex = 17;
            // 
            // btnExtend
            // 
            this.btnExtend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExtend.Location = new System.Drawing.Point(380, 6);
            this.btnExtend.Name = "btnExtend";
            this.btnExtend.Size = new System.Drawing.Size(61, 25);
            this.btnExtend.TabIndex = 39;
            this.btnExtend.Text = "확장검색";
            this.btnExtend.UseVisualStyleBackColor = true;
            this.btnExtend.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(445, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(61, 25);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.PageCloseValve);
            this.tabControl1.Controls.Add(this.PageOutValve);
            this.tabControl1.Controls.Add(this.pagePipeLs);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 37);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(513, 382);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 19;
            // 
            // PageCloseValve
            // 
            this.PageCloseValve.Controls.Add(this.ultraGrid_Valve);
            this.PageCloseValve.Location = new System.Drawing.Point(4, 22);
            this.PageCloseValve.Name = "PageCloseValve";
            this.PageCloseValve.Padding = new System.Windows.Forms.Padding(3);
            this.PageCloseValve.Size = new System.Drawing.Size(505, 356);
            this.PageCloseValve.TabIndex = 0;
            this.PageCloseValve.Text = "차단제수밸브";
            this.PageCloseValve.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_Valve
            // 
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Valve.DisplayLayout.Appearance = appearance85;
            this.ultraGrid_Valve.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Valve.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance86.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance86.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance86.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Valve.DisplayLayout.GroupByBox.Appearance = appearance86;
            appearance87.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Valve.DisplayLayout.GroupByBox.BandLabelAppearance = appearance87;
            this.ultraGrid_Valve.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance88.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance88.BackColor2 = System.Drawing.SystemColors.Control;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance88.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Valve.DisplayLayout.GroupByBox.PromptAppearance = appearance88;
            this.ultraGrid_Valve.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Valve.DisplayLayout.MaxRowScrollRegions = 1;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            appearance89.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_Valve.DisplayLayout.Override.ActiveCellAppearance = appearance89;
            appearance90.BackColor = System.Drawing.SystemColors.Highlight;
            appearance90.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_Valve.DisplayLayout.Override.ActiveRowAppearance = appearance90;
            this.ultraGrid_Valve.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Valve.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Valve.DisplayLayout.Override.CardAreaAppearance = appearance91;
            appearance92.BorderColor = System.Drawing.Color.Silver;
            appearance92.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Valve.DisplayLayout.Override.CellAppearance = appearance92;
            this.ultraGrid_Valve.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Valve.DisplayLayout.Override.CellPadding = 0;
            appearance93.BackColor = System.Drawing.SystemColors.Control;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Valve.DisplayLayout.Override.GroupByRowAppearance = appearance93;
            appearance94.TextHAlignAsString = "Left";
            this.ultraGrid_Valve.DisplayLayout.Override.HeaderAppearance = appearance94;
            this.ultraGrid_Valve.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Valve.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Valve.DisplayLayout.Override.RowAppearance = appearance95;
            this.ultraGrid_Valve.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance96.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Valve.DisplayLayout.Override.TemplateAddRowAppearance = appearance96;
            this.ultraGrid_Valve.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Valve.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Valve.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_Valve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Valve.Location = new System.Drawing.Point(3, 3);
            this.ultraGrid_Valve.Name = "ultraGrid_Valve";
            this.ultraGrid_Valve.Size = new System.Drawing.Size(499, 350);
            this.ultraGrid_Valve.TabIndex = 1;
            this.ultraGrid_Valve.Text = "ultraGrid1";
            // 
            // PageOutValve
            // 
            this.PageOutValve.Controls.Add(this.ultraGrid_OutValve);
            this.PageOutValve.Location = new System.Drawing.Point(4, 22);
            this.PageOutValve.Name = "PageOutValve";
            this.PageOutValve.Size = new System.Drawing.Size(505, 356);
            this.PageOutValve.TabIndex = 4;
            this.PageOutValve.Text = "이토밸브";
            this.PageOutValve.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_OutValve
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_OutValve.DisplayLayout.Appearance = appearance25;
            this.ultraGrid_OutValve.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_OutValve.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_OutValve.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_OutValve.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.ultraGrid_OutValve.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_OutValve.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.ultraGrid_OutValve.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_OutValve.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_OutValve.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_OutValve.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.ultraGrid_OutValve.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_OutValve.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_OutValve.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_OutValve.DisplayLayout.Override.CellAppearance = appearance32;
            this.ultraGrid_OutValve.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_OutValve.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_OutValve.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.ultraGrid_OutValve.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.ultraGrid_OutValve.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_OutValve.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_OutValve.DisplayLayout.Override.RowAppearance = appearance35;
            this.ultraGrid_OutValve.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_OutValve.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.ultraGrid_OutValve.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_OutValve.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_OutValve.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_OutValve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_OutValve.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_OutValve.Name = "ultraGrid_OutValve";
            this.ultraGrid_OutValve.Size = new System.Drawing.Size(505, 356);
            this.ultraGrid_OutValve.TabIndex = 2;
            this.ultraGrid_OutValve.Text = "ultraGrid1";
            // 
            // pagePipeLs
            // 
            this.pagePipeLs.Controls.Add(this.ultraGrid_PipeLs);
            this.pagePipeLs.Location = new System.Drawing.Point(4, 22);
            this.pagePipeLs.Name = "pagePipeLs";
            this.pagePipeLs.Size = new System.Drawing.Size(505, 356);
            this.pagePipeLs.TabIndex = 6;
            this.pagePipeLs.Text = "상수관로";
            this.pagePipeLs.UseVisualStyleBackColor = true;
            // 
            // ultraGrid_PipeLs
            // 
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_PipeLs.DisplayLayout.Appearance = appearance97;
            this.ultraGrid_PipeLs.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_PipeLs.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance98.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance98.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance98.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance98.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_PipeLs.DisplayLayout.GroupByBox.Appearance = appearance98;
            appearance99.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_PipeLs.DisplayLayout.GroupByBox.BandLabelAppearance = appearance99;
            this.ultraGrid_PipeLs.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance100.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance100.BackColor2 = System.Drawing.SystemColors.Control;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance100.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_PipeLs.DisplayLayout.GroupByBox.PromptAppearance = appearance100;
            this.ultraGrid_PipeLs.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_PipeLs.DisplayLayout.MaxRowScrollRegions = 1;
            appearance101.BackColor = System.Drawing.SystemColors.Window;
            appearance101.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_PipeLs.DisplayLayout.Override.ActiveCellAppearance = appearance101;
            appearance102.BackColor = System.Drawing.SystemColors.Highlight;
            appearance102.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_PipeLs.DisplayLayout.Override.ActiveRowAppearance = appearance102;
            this.ultraGrid_PipeLs.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_PipeLs.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance103.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_PipeLs.DisplayLayout.Override.CardAreaAppearance = appearance103;
            appearance104.BorderColor = System.Drawing.Color.Silver;
            appearance104.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_PipeLs.DisplayLayout.Override.CellAppearance = appearance104;
            this.ultraGrid_PipeLs.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_PipeLs.DisplayLayout.Override.CellPadding = 0;
            appearance105.BackColor = System.Drawing.SystemColors.Control;
            appearance105.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance105.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance105.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance105.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_PipeLs.DisplayLayout.Override.GroupByRowAppearance = appearance105;
            appearance106.TextHAlignAsString = "Left";
            this.ultraGrid_PipeLs.DisplayLayout.Override.HeaderAppearance = appearance106;
            this.ultraGrid_PipeLs.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_PipeLs.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance107.BackColor = System.Drawing.SystemColors.Window;
            appearance107.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_PipeLs.DisplayLayout.Override.RowAppearance = appearance107;
            this.ultraGrid_PipeLs.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance108.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_PipeLs.DisplayLayout.Override.TemplateAddRowAppearance = appearance108;
            this.ultraGrid_PipeLs.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_PipeLs.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_PipeLs.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_PipeLs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_PipeLs.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid_PipeLs.Name = "ultraGrid_PipeLs";
            this.ultraGrid_PipeLs.Size = new System.Drawing.Size(505, 356);
            this.ultraGrid_PipeLs.TabIndex = 2;
            this.ultraGrid_PipeLs.Text = "ultraGrid1";
            // 
            // frmCloseValveList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 419);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panelCommand);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "frmCloseValveList";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "차단 밸브 및 관로";
            this.Load += new System.EventHandler(this.frmCloseValveList_Load);
            this.panelCommand.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.PageCloseValve.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Valve)).EndInit();
            this.PageOutValve.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_OutValve)).EndInit();
            this.pagePipeLs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_PipeLs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExtend;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage PageCloseValve;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Valve;
        private System.Windows.Forms.TabPage PageOutValve;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_OutValve;
        private System.Windows.Forms.TabPage pagePipeLs;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_PipeLs;
    }
}