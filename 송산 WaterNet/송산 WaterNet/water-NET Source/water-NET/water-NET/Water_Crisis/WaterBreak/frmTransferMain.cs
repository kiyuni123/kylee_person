﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Data;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.esriSystem;
using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;
using QuickGraph;
using System.Text;
using System.Security.Cryptography;


namespace WaterNet.WC_CrisisTransfer
{
    public partial class frmTransferMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        private QuickGraph.BidirectionalGraph<Vertex, Edge<Vertex>> m_ClosedGraph; //차단밸브 및 관로저장 그래프

        //private Hashtable m_Pipes = new Hashtable();    ///Closed Pipe's                                     
        private IEnvelope m_Extent = new EnvelopeClass(); //1차해석의 결과 지도범위
        //private ISelectionSet m_CLOSED_pipeline_1st = null;  //1차해석 차단된 상수관로 목록

        private QuickGraph.BidirectionalGraph<QuickGraph.Vertex, Edge<QuickGraph.Vertex>> graph = null;
        private QuickGraph.Geometries.Index.RTree<Edge<Vertex>> rtreeEdge = new QuickGraph.Geometries.Index.RTree<Edge<Vertex>>();

        private IMapEvents_Event mapEvent;

        //public IMultiLayerLineSymbol negativeSymbol = null;
        //public IMultiLayerLineSymbol positiveSymbol = null;

        /// <summary>
        /// 생성자
        /// </summary>
        public frmTransferMain()
        {
            InitializeComponent();
            InitializeSetting();
            this.btnGraph2Shape.Visible = false;
            //this.getArrorLineSymbol();
            this.mapEvent = axMap.Map as IMapEvents_Event;
            this.Load +=new EventHandler(frmTransferMain_Load);
            this.FormClosed += new FormClosedEventHandler(frmTransferMain_FormClosed);
        }

    

        #region IForminterface 멤버

        public string FormID
        {
            get { return "단수관리"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion



        /// <summary>
        /// 초기화 설정
        /// </summary>
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
        
            pnlLayerFunc.Visible = false;
        }

        #region Private Property

        #endregion Private Property

        /// <summary>
        /// Open
        /// </summary>
        public override void Open()
        {
            base.Open();
            //추가적인 구현은 여기에....

            InitializeMap();

            #region 상수관망 네트워크 구조 생성
            frmSplashMsg oSplash = new frmSplashMsg();
            oSplash.Message = "상수관망 네트워크 구조를 생성하고 있습니다.\n\r잠시만 기다리세요!";
            oSplash.Open();
            Application.DoEvents();
            try
            {
                if (this.graph == null)
                {
                    this.graph = new QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>>(true);
                    bool flag = false;
                    string hashcode = this.CheckMd5HashCode();
                    try
                    {
                        if (!string.IsNullOrEmpty(hashcode))
                        {
                            string md5 = IniReadValue("MD5", EMFrame.statics.AppStatic.USER_SGCCD);
                            if (hashcode.Equals(md5))
                            {
                                this.graph = BinaryFormatterDeserialize(System.IO.Path.Combine(Application.StartupPath + @"\Config\",
                                                                                   EMFrame.statics.AppStatic.USER_SGCCD + ".md5"))
                                                        as QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>>;
                                flag = this.graph != null ? true : false;
                            }
                        }
                    }
                    catch { }

                    if (!flag) //위에서 성공하지 못했다면, 네트워크 구조를 새로이 생성한다.
                    {
                        QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>> g =
                             new QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>>();

                        this.Createnetworktopology(ref g);

                        BinaryFormatterSerialize(System.IO.Path.Combine(Application.StartupPath + @"\Config\", EMFrame.statics.AppStatic.USER_SGCCD + ".md5"), g);
                        IniWriteValue("MD5", EMFrame.statics.AppStatic.USER_SGCCD, hashcode);

                        this.graph = g;
                    }
                }
            #endregion 상수관망 네트워크 구조 생성
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                oSplash.Close();
            }
        }

        /// <summary>
        /// 지도상에 레이어를 표시하지 않도록 설정
        /// </summary>
        private void InitializeMap()
        {
            IEnumLayer pEnumLayer = this.axMap.ActiveView.FocusMap.get_Layers(null, false);
            pEnumLayer.Reset();

            ILayer pLayer = (ILayer)pEnumLayer.Next();

            do
            {
                if (pLayer is IFeatureLayer)
                {
                    switch (pLayer.Name)
                    {
                        case "재염소처리지점":
                        case "중요시설":
                        case "감시지점":
                        case "누수지점":
                        case "민원지점":
                        case "관세척구간":
                        //case "밸브":
                        //case "유량계":
                        case "수압계":
                        case "소방시설":
                        case "스탠드파이프":
                        case "수도계량기":
                        case "표고점":
                        //case "급수관로":
                        //case "상수관로":
                        //case "정수장":
                        //case "가압장":
                        //case "취수장":
                        //case "수원지":
                        //case "배수지":
                        case "밸브실":
                        //case "철도":
                        case "도로경계선":
                        case "등고선":
                        case "담장":
                        case "법정읍면동":
                        //case "행정읍면동":
                        case "건물":
                        case "하천":
                        case "지방도":
                        case "국도":
                        case "고속도로":
                        case "지형지번":
                            //case "소블록":
                            //case "중블록":
                            //case "대블록":
                            pLayer.Visible = false;
                            break;
                        default:
                            break;
                    }
                }
                pLayer = pEnumLayer.Next();
            }
            while (pLayer != null);

        }


        #region 네트워크 MD5 사용
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        /// <summary>
        /// BinaryFormatter를 이용해 Serialize합니다.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="graph"></param>
        private object BinaryFormatterDeserialize(string filePath)
        {
            System.IO.Stream stream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Read);
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            object obj = bf.Deserialize(stream);
            return obj;
            //System.IO.FileStream fs = null;
            //try
            //{
            //    fs = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            //    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            //    object obj = bf.Deserialize(fs);
            //    //fs.Close();
            //    return obj;
            //}
            //catch
            //{
            //    return null;
            //}
            //finally
            //{

            //    if (fs != null)
            //        fs.Close();
            //}
        }

        /// <summary>
        /// BinaryFormatter를 이용해 Serialize합니다.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="graph"></param>
        private void BinaryFormatterSerialize(string filePath, object graph)
        {
            System.IO.Stream stream = System.IO.File.Open(filePath, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            bf.Serialize(stream, graph);
            stream.Close();
            //System.IO.FileStream fs = null;
            //try
            //{
            //    fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            //    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            //    bf.Serialize(fs, graph);
            //    fs.Close();
            //}
            //catch { }
            //finally
            //{
            //    if (fs != null)
            //        fs.Close();
            //}
        }

        private string IniReadValue(string Section, string Key)
        {
            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "Revision.ini";

            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, strPath + strFile);
            return temp.ToString();
        }

        private void IniWriteValue(string Section, string Key, string Value)
        {
            string strPath = Application.StartupPath + @"\Config\";
            string strFile = "Revision.ini";

            WritePrivateProfileString(Section, Key, Value, strPath + strFile);
        }

        private string CheckMd5HashCode()
        {
            List<string> files = new List<string>();
            string folder = string.Empty;

            List<string> lname = new List<string>() { "밸브", "소방시설", "수도계량기", "상수관로" ,"급수관로"};
            EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);

            foreach (string l in lname)
            {
                ILayer layer = mapExplorer.GetLayer(l) as IFeatureLayer;
                if (layer == null) continue;

                IDataset dataset = ((IFeatureLayer)layer).FeatureClass as IDataset;
                if (string.IsNullOrEmpty(folder))
                    folder = dataset.Workspace.PathName;
                files.Add(string.Format("{0}.dbf", dataset.BrowseName));
                files.Add(string.Format("{0}.shp", dataset.BrowseName));
                files.Add(string.Format("{0}.shx", dataset.BrowseName));
            }

            return this.CreateMd5ForFiles(folder, files.ToArray());
        }

        private string CreateMd5ForFiles(string folder, string[] _files)
        {
            string hashcode = string.Empty;
            // assuming you want to include nested folders
            var files = System.IO.Directory.GetFiles(folder, "*.???", System.IO.SearchOption.TopDirectoryOnly)
                                 .Where(entry => _files.Contains(entry.Replace(folder + "\\", string.Empty)))
                                 .OrderBy(p => p).ToList();

            MD5 md5 = MD5.Create();
            for (int i = 0; i < files.Count; i++)
            {
                string file = files[i];

                // hash path
                string relativePath = file.Substring(folder.Length + 1);
                byte[] pathBytes = Encoding.UTF8.GetBytes(relativePath.ToLower());
                md5.TransformBlock(pathBytes, 0, pathBytes.Length, pathBytes, 0);

                // hash contents
                byte[] contentBytes = System.IO.File.ReadAllBytes(file);
                if (i == files.Count - 1)
                    md5.TransformFinalBlock(contentBytes, 0, contentBytes.Length);
                else
                    md5.TransformBlock(contentBytes, 0, contentBytes.Length, contentBytes, 0);
            }
            if (md5.HashSize > 0)
            {
                hashcode = BitConverter.ToString(md5.Hash).Replace("-", "").ToLower();
            }
            return hashcode;
        }
        #endregion 네트워크 MD5 사용

        #region 네트워크 구조 생성
        private int vertexid = 0; private int edgeid = 0;
        private const double m_Tolerence = (0.1 / 7);
        private double SAME_LEN = 0.0000001;
        private readonly string[] hData = { "FTR_IDN", "FTR_CDE", "VAL_MOF", "VAL_MOP", "VAL_DIP", "MNG_CDE", "SAA_CDE", "MOP_CDE", "PIP_DIP", "PIP_LEN", "WID_CDE", "PIP_LBL" };

        private Hashtable ConvertIRow2HashTable(IRow row, string[] fields)
        {
            Hashtable table = new Hashtable();
            for (int i = 0; i < row.Fields.FieldCount; i++)
            {
                IField field = row.Fields.get_Field(i);
                if (fields.Contains(field.Name))
                {
                    table.Add(row.Fields.get_Field(i).Name, row.get_Value(i));
                }
            }
            return table;
        }

        private QuickGraph.Vertex GetNode(ref QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex> rtree, IPoint pt)
        {
            return GetNode(ref rtree, pt, 0.0);
        }

        private QuickGraph.Vertex GetNode(ref QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex> rtree, IPoint pt, double tolerence)
        {
            List<QuickGraph.Vertex> vs = rtree.Contains(get_Rectangle(pt, tolerence));
            if (vs != null && vs.Count > 0) return vs[0];

            return null;
        }

        private QuickGraph.Vertex GetOrCreateNode(ref QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>> g,
                            ref QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex> rtree, IPoint pt, IFeature feature)
        {
            IList<QuickGraph.Vertex> vs = rtree.Contains(get_Rectangle(pt, m_Tolerence));
            if (vs != null && vs.Count > 0) return vs[0];

            QuickGraph.Vertex v = new QuickGraph.Vertex(pt.X, pt.Y, pt.Z);
            //v.Id = string.Format("{0}", ++vertexid);
            v.OBJECTID = feature.OID;
            v.CLASSNAME = feature.Class.AliasName;
            v.HashData = ConvertIRow2HashTable((IRow)feature, hData);
            g.AddVertex(v);
            rtree.Add(get_Rectangle(pt), v);

            return v;
        }


        private void CreateEdgeNetwork(ref QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>> g,
                                           ref QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex> rtree)
        {
            EAGL.Explore.MapExplorer map = new EAGL.Explore.MapExplorer(this.axMap.Map);
            List<string> lname = new List<string>();
            lname.Add("상수관로");
            //lname.Add("급수관로");
            Console.WriteLine("Edge Start: " + DateTime.Now.ToString());
            foreach (string l in lname)
            {
                ILayer layer = map.GetLayer(l) as IFeatureLayer;
                if (layer == null) continue;

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureCursor FCursor = ((IFeatureLayer)layer).Search(null, false);
                    comReleaser.ManageLifetime(FCursor);

                    IFeature feature;
                    while ((feature = FCursor.NextFeature()) != null)
                    {
                        IPolyline line = feature.Shape as IPolyline;
                        line.SimplifyNetwork();
                        if (line.IsEmpty || ((IPointCollection)line).PointCount == 0) continue;
                        if (line.Length < 0.01) continue;

                        //IPoint pt1 = line.FromPoint; IPoint pt2 = line.ToPoint;
                        QuickGraph.Vertex v1 = this.GetOrCreateNode(ref g, ref rtree, line.FromPoint, feature);
                        QuickGraph.Vertex v2 = this.GetOrCreateNode(ref g, ref rtree, line.ToPoint, feature);
                        if (v1.Equals(v2)) continue;

                        Edge<Vertex> edge = new Edge<Vertex>(v1, v2);
                        edge.HashData = ConvertIRow2HashTable((IRow)feature, hData);
                        edge.OBJECTID = feature.OID;
                        edge.CLASSNAME = feature.Class.AliasName;
                        //edge.Id = string.Format("{0}", ++edgeid);
                        edge.VTS = EpaNetwork.xUtils.makeVertices(line);
                        g.AddVerticesAndEdge(edge);
                    }
                }
            }
            Console.WriteLine("Edge End: " + DateTime.Now.ToString());
        }

        private void CreateVertexNetwork(ref QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>> g,
                                         ref QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex> rtree)
        {
            EAGL.Explore.MapExplorer map = new EAGL.Explore.MapExplorer(this.axMap.Map);
            Console.WriteLine("Vertex Start: " + DateTime.Now.ToString());
            List<string> lname = new List<string>();
            lname.Add("밸브");
            //lname.Add("소방시설");
            //lname.Add("수도계량기");
            foreach (string l in lname)
            {
                ILayer layer = map.GetLayer(l) as IFeatureLayer;
                if (layer == null) continue;

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureCursor FCursor = ((IFeatureLayer)layer).Search(null, false);
                    comReleaser.ManageLifetime(FCursor);

                    IFeature feature;
                    while ((feature = FCursor.NextFeature()) != null)
                    {
                        IPoint pt = feature.ShapeCopy as IPoint;
                        QuickGraph.Vertex node = this.GetNode(ref rtree, pt, m_Tolerence);
                        if (node != null) continue;

                        node = new QuickGraph.Vertex(pt.X, pt.Y, pt.Z);
                        node.HashData = ConvertIRow2HashTable((IRow)feature, hData);
                        node.OBJECTID = feature.OID;
                        node.CLASSNAME = layer.Name;
                        //node.Id = Convert.ToString(++vertexid);
                        g.AddVertex(node);
                        rtree.Add(get_Rectangle(pt, SAME_LEN), node);
                    }

                }
            } 
            Console.WriteLine("Vertex End: " + DateTime.Now.ToString());
        }

        private void DivideNetworkPipes(ref QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>> g, ref QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex> rtree)
        {
            Console.WriteLine("Devide Edge Start: " + DateTime.Now.ToString());
            List<Edge<Vertex>> edges = g.Edges.ToList();
            foreach (Edge<Vertex> edge in edges)
            {
                IGeometry line = EpaNetwork.xUtils.getGeometryVertexEdge(edge);
                if (line.IsEmpty) continue;

                List<Vertex> vts = IsDivide(ref rtree, edge, line);
                if (vts.Count > 0)
                {
                    DoDivide(ref g, ref rtree, edge, vts, line);
                }
            }
            Console.WriteLine("Devide Edge End: " + DateTime.Now.ToString());
        }

        /// <summary>
        /// Edge를 분할하여, Edge를 생성한다.
        /// </summary>
        /// <param name="edge"></param>
        /// <param name="vs"></param>
        private void DoDivide(ref QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>> g,
                            ref QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex> rtree, Edge<Vertex> edge, List<Vertex> vts, IGeometry line)
        {
            Vertex s1 = edge.Source as Vertex;
            Vertex s2 = edge.Target as Vertex;
            IPolycurve2 polycurve = line as IPolycurve2;

            bool isSplitted;
            int newPartIndex;
            int newSegmentIndex;

            object missing = Type.Missing;
            IPointCollection points = new MultipointClass();
            //Console.WriteLine(string.Format("{0},{1}", edge.CLASSNAME, edge.OBJECTID));
            foreach (Vertex v in vts)
            {
                //Console.Write(string.Format("{0},{1}\t", v.CLASSNAME, v.OBJECTID));
                points.AddPoint((IPoint)EpaNetwork.xUtils.getGeometryVertexEdge(v), ref missing, ref missing);

                //IPoint splitpoint = GetNearestPoint(polycurve, EpaNetwork.xUtils.getGeometryVertexEdge(v) as IPoint);
                //polycurve.SplitAtPoint(splitpoint, true, true, out isSplitted, out newPartIndex, out newSegmentIndex);
            }

            IEnumSplitPoint enumSplitPoint = polycurve.SplitAtPoints(points.EnumVertices, true, true, m_Tolerence);
            IGeometryCollection geometryCollection = polycurve as IGeometryCollection;

            if (geometryCollection.GeometryCount != vts.Count + 1)
            {
                Console.WriteLine("Error");
            }

            try
            {
                for (int i = 0; i < geometryCollection.GeometryCount; i++)
                {
                    IGeometry _geometry = geometryCollection.get_Geometry(i);
                    IGeometryCollection geoms = new PolylineClass();
                    geoms.AddGeometry(_geometry, ref missing, ref missing);
                    if (_geometry.IsEmpty)
                    {
                        Console.WriteLine("Empty : " + i.ToString());
                        continue;
                    }

                    IPolyline ln = geoms as IPolyline;
                    if (i == 0)
                    {
                        edge.Source = s1;
                        edge.Target = vts[i];
                        edge.VTS = EpaNetwork.xUtils.makeVertices(ln);
                    }
                    else if (i == geometryCollection.GeometryCount - 1)
                    {
                        //Console.WriteLine(string.Format("{0},{1},{2},{3}", i, vts[i - 1].OBJECTID, s2.OBJECTID, ln.Length));
                        Edge<Vertex> temp = new Edge<Vertex>(vts[i - 1], s2);
                        temp.Assign(edge);
                        temp.VTS = EpaNetwork.xUtils.makeVertices(ln);
                        g.AddVerticesAndEdge(temp);
                    }
                    else
                    {
                        Edge<Vertex> temp = new Edge<Vertex>(vts[i - 1], vts[i]);
                        temp.Assign(edge);
                        temp.VTS = EpaNetwork.xUtils.makeVertices(ln);
                        g.AddVerticesAndEdge(temp);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// 분할이 필요한 Edge인지 확인한다.
        /// 분할이 필요하면, Edge에 인접한 Vertex목록을 반환한다.
        /// </summary>
        /// <param name="edge"></param>
        /// <returns></returns>
        private List<Vertex> IsDivide(ref QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex> rtree, Edge<Vertex> edge, IGeometry line)
        {
            List<Vertex> tempvts = new List<Vertex>();
            List<Vertex> vts = rtree.Contains(get_Rectangle(line));
            ICurve3 curve = line as ICurve3;
            Dictionary<double, Vertex> vtss = new Dictionary<double, Vertex>();
            foreach (Vertex v in vts)
            {
                if (v.Equals(edge.Source) || v.Equals(edge.Target)) continue;

                IPoint p = EpaNetwork.xUtils.getGeometryVertexEdge(v) as IPoint;
                double d = (GetDistancePoint(line, p));
                if (d <= (float)m_Tolerence)
                {
                    if (!(edge.Source.Equals(v) || edge.Target.Equals(v)))
                    {
                        double alon = 0.0; double dist = 0.0; bool r = false;
                        IPoint tmp = new PointClass();
                        curve.QueryPointAndDistance(esriSegmentExtension.esriNoExtension, p, false, tmp, ref alon, ref dist, ref r);
                        vtss.Add(alon, v);
                    }
                }
            }

            return vtss.OrderBy(entry => entry.Key).Select(entry => entry.Value).ToList();
        }

        private enum direction { Source, Target, Nothing };
        private direction get_direction(Edge<Vertex> edge, Vertex v)
        {
            direction result = direction.Nothing;
            if (edge.Source.Equals(v)) result = direction.Source;
            else if (edge.Target.Equals(v)) result = direction.Target;
            return result;
        }

        /// <summary>
        /// 급수관로의 방향전환
        /// </summary>
        /// <param name="g"></param>
        /// <param name="v"></param>
        private void SetDirection(ref QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>> g, Vertex v)
        {
            if (v == null) return;

            List<Edge<Vertex>> edges = g.AdjacentEdges(v).ToList();
            foreach (var edge in edges)
            {
                if (!(edge.CLASSNAME.Equals("WTL_SPLY_LS") || edge.CLASSNAME.Equals("급수관로"))) continue;
                if (edge.Visited) continue;

                edge.Visited = true;

                direction di = get_direction(edge, v);
                //di == Source : 순방향, di == Target : 역방향
                Vertex tmp = null;
                if (di == direction.Source)
                {
                    //tmp = edge.Target.CloneID() as Vertex;
                }
                else if (di == direction.Target)
                {
                    edge.Reverse();

                    //tmp = edge.Source.CloneID() as Vertex;
                    //Vertex v2 = edge.Target.CloneID() as Vertex;
                    //edge.Source = v2; edge.Target = tmp;

                    //IPolyline line =  edge.GEOMETRY as IPolyline;
                    //line.ReverseOrientation();
                    //edge.GEOMETRY = line;
                }
                //순방향이든 역방향이든 재귀함수 호출
                SetDirection(ref g, tmp);
            }
        }

        /// <summary>
        /// edge와 연결되지 않은 독립된 vertex반환
        /// </summary>
        /// <returns></returns>
        private List<Vertex> get_AloneVertexList(QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>> g)
        {
            var dic = from entry in g.Vertices
                      where g.AdjacentDegree(entry) == 0
                      select entry;

            return dic.ToList();
        }

        /// <summary>
        /// 상수관로와 밸브로 네트워크 구조를 생성한다.
        /// logi
        /// </summary>
        private void Createnetworktopology(ref QuickGraph.BidirectionalGraph<QuickGraph.Vertex, QuickGraph.Edge<QuickGraph.Vertex>> g)
        {
            vertexid = 0; edgeid = 0;
            if (g != null) g.Clear();
            QuickGraph.Geometries.Index.RTree<Vertex> rtree = new QuickGraph.Geometries.Index.RTree<Vertex>();

            try
            {
                this.CreateVertexNetwork(ref g, ref rtree);
                this.CreateEdgeNetwork(ref g, ref rtree);
                this.DivideNetworkPipes(ref g, ref rtree);
               
                #region 급수관로 방향전환
                //Console.WriteLine("Edge redirect Start: " + DateTime.Now.ToString());
                //QuickGraph.UndirectedBidirectionalGraph<Vertex, Edge<Vertex>> g = new UndirectedBidirectionalGraph<Vertex, Edge<Vertex>>(graph);
                //foreach (var edge in g.Edges) edge.Visited = false;

                //var dic = g.Edges.Where(entry => ((new string[] { "상수관로", "WTL_PIPE_LS" }).Contains(entry.CLASSNAME)))
                //          .Select(grp => grp);

                //foreach (var edge in dic)
                //{
                //    if (edge.Target == null) continue;
                //    SetDirection(ref g, edge.Target);
                //}
                //Console.WriteLine("Edge redirect End: " + DateTime.Now.ToString());
                #endregion 급수관로 방향전환

                #region 외톨이 Vertex 삭제
                g.ResetVertexInOutEdges();
                List<Vertex> vs = get_AloneVertexList(g);
                for (int i = vs.Count() - 1; i >= 0; i--)
                {
                    g.RemoveVertex(vs.ElementAt(i));
                }
                g.ResetVertexInOutEdges();
                #endregion 외톨이 Vertex 삭제

                #region ID재구성
                int vid = 0; int eid = 0;
                foreach (var item in g.Edges) item.Id = string.Format("{0}", ++eid);
                foreach (var item in g.Vertices) item.Id = string.Format("{0}", ++vid);
                #endregion ID재구성
            }
            catch { }
        }

        #endregion 네트워크 구조 생성

        #region 공통함수
        private IGeometry GetBuffer(IGeometry pGeometry, double distance)
        {
            ESRI.ArcGIS.Geometry.ITopologicalOperator topoOpt = pGeometry as ESRI.ArcGIS.Geometry.ITopologicalOperator;
            topoOpt.Simplify();
            return topoOpt.Buffer(distance);
        }
        private IPoint GetNearestPoint(IGeometry pGeometry, IPoint pt)
        {
            ESRI.ArcGIS.Geometry.IProximityOperator proxOpt = (ESRI.ArcGIS.Geometry.IProximityOperator)pGeometry;


            return proxOpt.ReturnNearestPoint(pt, ESRI.ArcGIS.Geometry.esriSegmentExtension.esriNoExtension);
        }

        private double GetDistancePoint(IGeometry pGeometry, IPoint pt)
        {
            ESRI.ArcGIS.Geometry.IProximityOperator proxOpt = (ESRI.ArcGIS.Geometry.IProximityOperator)pGeometry;

            return proxOpt.ReturnDistance(pt);
        }

        private bool GetOverlap(IGeometry pGeometry, IGeometry newGeometry)
        {
            ESRI.ArcGIS.Geometry.IRelationalOperator relOpt = pGeometry as ESRI.ArcGIS.Geometry.IRelationalOperator;

            return relOpt.Overlaps(newGeometry);
        }

        private IGeometry GetInsertsect(IGeometry pGeometry, IGeometry newGeometry)
        {
            ESRI.ArcGIS.Geometry.ITopologicalOperator pTopoOp = pGeometry as ESRI.ArcGIS.Geometry.ITopologicalOperator;
            pTopoOp.Simplify();

            return pTopoOp.Intersect(newGeometry, ESRI.ArcGIS.Geometry.esriGeometryDimension.esriGeometry1Dimension);
        }

        private IGeometry GetDifference(IGeometry pGeometry, IGeometry newGeometry)
        {
            ESRI.ArcGIS.Geometry.ITopologicalOperator pTopoOp = pGeometry as ESRI.ArcGIS.Geometry.ITopologicalOperator;
            pTopoOp.Simplify();

            return pTopoOp.Difference(newGeometry);
        }

        private QuickGraph.Geometries.Rectangle get_Rectangle(IGeometry geometry)
        {
            return get_Rectangle(geometry, 0.0);
        }

        private QuickGraph.Geometries.Coordinate get_RTreePoint(IPoint pt)
        {
            QuickGraph.Geometries.Coordinate p = new QuickGraph.Geometries.Coordinate((float)pt.X, (float)pt.Y, (float)pt.Z);
            return p;
        }

        private QuickGraph.Geometries.Rectangle get_Rectangle(IGeometry geometry, double dist)
        {
            ESRI.ArcGIS.Geometry.IEnvelope envelope = geometry.Envelope;
            envelope.Expand(dist, dist, false);
            QuickGraph.Geometries.Rectangle rect = new QuickGraph.Geometries.Rectangle((float)envelope.XMin, (float)envelope.YMin, (float)envelope.XMax, (float)envelope.YMax, (float)0.0, (float)0.0);
            return rect;
        }
        #endregion 공통함수

        void frmTransferMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            WC_Global.crisisAnalysisOp.Clear();
        }

        /// <summary>
        /// 화면 로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmTransferMain_Load(object sender, EventArgs e)
        {
            chk_pres.CheckedChanged += new EventHandler(ShowLabelPressure);
            this.btnBrokePoint.MouseDown += new MouseEventHandler(btnBrokePoint_MouseDown);
            this.tsLocalPoint.Click += new EventHandler(tsLocalPoint_Click);
            this.tsGlobalPoint.Click += new EventHandler(tsGlobalPoint_Click);

            #region RTree Edge 생성 - 차단밸브 검색시 - 누수지점 선택할때 상수관로만 선택가능하도록 하기위해서
            //차단밸브 검색시 - 누수지점 선택할때 상수관로만 선택가능하도록 하기위해서
            var edges = this.graph.Edges.Where(entry => ((new string[] { "상수관로", "WTL_PIPE_LS" }).Contains(entry.CLASSNAME)))
                        .Select(grp => grp);
            foreach (var edge in edges)
            {
                this.rtreeEdge.Add(get_Rectangle(EpaNetwork.xUtils.getGeometryVertexEdge(edge), SAME_LEN), edge);
            }
            #endregion RTree Edge 생성 - 차단밸브 검색시 - 누수지점 선택할때 상수관로만 선택가능하도록 하기위해서
        }

        /// <summary>
        /// 사고지점 선택(광역관로)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tsGlobalPoint_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("광역관로상에 사고지점을 선택하십시오.", "안내");
            toolActionCommand.Checked = true;
            base.toolActionCommand_Click(this, new EventArgs());
            //WC_Common.WC_VariableManager.m_CrisisParams["PIPE_GBN"] = "G";
        }

        /// <summary>
        /// 사고지점 선택(지방관로)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tsLocalPoint_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("지방관로상에 사고지점을 선택하십시오.", "안내");
            toolActionCommand.Checked = true;
            base.toolActionCommand_Click(this, new EventArgs());
            //WC_Common.WC_VariableManager.m_CrisisParams["PIPE_GBN"] = "L";
        }

        void btnBrokePoint_MouseDown(object sender, MouseEventArgs e)
        {
            toolActionCommand.Checked = true;
            base.toolActionCommand_Click(this, new EventArgs());

            //if (ArcManager.GetMapLayer((IMapControl3)axMap.Object, "PIPE") == null)
            //{
            //    WaterNetCore.MessageManager.ShowInformationMessage("수리(위기)모델을 로드하지 않았습니다.");
            //    return;
            //}
            //foreach (ToolStripItem item in this.contextMenuSelect.Items)
            //{
            //    if ((new string[] { "tsLocalPoint", "tsGlobalPoint" }).Contains(item.Name))
            //    {
            //        item.Visible = true; item.Enabled = true;
            //    }
            //    else
            //    {
            //        item.Visible = false; item.Enabled = false;
            //    }
            //}
            //this.contextMenuSelect.Show((Button)sender, new System.Drawing.Point(e.Location.X, e.Location.Y));

            ModifyButton(sender);

        }


        private ESRI.ArcGIS.esriSystem.IPropertySet LoadRendererStream(byte[] pData)
        {
            if (pData == null) return null;
            if (pData.Length == 0) return null;

            object objectData = pData as object;

            ESRI.ArcGIS.esriSystem.IMemoryBlobStream ipMemBlobStream = new ESRI.ArcGIS.esriSystem.MemoryBlobStreamClass();
            ESRI.ArcGIS.esriSystem.IMemoryBlobStreamVariant ipMemBlobStreamVar = (ESRI.ArcGIS.esriSystem.IMemoryBlobStreamVariant)ipMemBlobStream;

            ipMemBlobStreamVar.ImportFromVariant(objectData);

            ESRI.ArcGIS.esriSystem.IObjectStream ipObjectStream = new ESRI.ArcGIS.esriSystem.ObjectStreamClass();
            ipObjectStream.Stream = ipMemBlobStreamVar as ESRI.ArcGIS.esriSystem.IStream;

            ESRI.ArcGIS.esriSystem.IPropertySet ipPropertySet = new ESRI.ArcGIS.esriSystem.PropertySetClass();

            ESRI.ArcGIS.esriSystem.IPersistStream oPersistStream = (ESRI.ArcGIS.esriSystem.IPersistStream)ipPropertySet;

            oPersistStream.Load(ipObjectStream);
            return ipPropertySet;
        }


        #region 버튼 처리
        /// <summary>
        /// 해석모델 선택버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelModel_Click(object sender, EventArgs e)
        {
            #region 레이어렌더링 초기설정
            //WaterNetCore.OracleDBManager oDBManager = new OracleDBManager();
            //oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            //try
            //{
            //    oDBManager.Open();
            //    DataTable table = oDBManager.ExecuteScriptDataTable("select * from si_layer", null);
            //    foreach (DataRow row in table.Rows)
            //    {
            //        if (Convert.IsDBNull(row["F_OBJECT"])) continue;
            //        ESRI.ArcGIS.esriSystem.IPropertySet ipPropertySet = LoadRendererStream(row["F_OBJECT"] as byte[]);
            //        if (ipPropertySet == null) continue;

            //        System.Text.StringBuilder oStringBuilder = new System.Text.StringBuilder();
            //        oStringBuilder.AppendLine("UPDATE SI_LAYER SET ");
            //        oStringBuilder.AppendLine("F_RENDERE = :F_RENDERE");
            //        oStringBuilder.AppendLine(",F_ANNOPROP = :F_ANNOPROP");
            //        oStringBuilder.AppendLine(",F_MINSCALE = :F_MINSCALE");
            //        oStringBuilder.AppendLine(",F_MAXSCALE = :F_MAXSCALE");
            //        oStringBuilder.AppendLine(",F_FIELDDISP = :F_FIELDDISP");
            //        oStringBuilder.AppendLine(",F_ANNODISP = :F_ANNODISP");
            //        oStringBuilder.AppendLine(",F_TIPDISP = :F_TIPDISP");
            //        oStringBuilder.AppendLine("WHERE F_NAME = :F_NAME");

            //        IDataParameter[] oParams = {
            //            new Oracle.DataAccess.Client.OracleParameter("F_RENDERE",Oracle.DataAccess.Client.OracleDbType.Blob),
            //            new Oracle.DataAccess.Client.OracleParameter("F_ANNOPROP",Oracle.DataAccess.Client.OracleDbType.Blob),
            //            new Oracle.DataAccess.Client.OracleParameter("F_MINSCALE",Oracle.DataAccess.Client.OracleDbType.Double),
            //            new Oracle.DataAccess.Client.OracleParameter("F_MAXSCALE",Oracle.DataAccess.Client.OracleDbType.Double),
            //            new Oracle.DataAccess.Client.OracleParameter("F_FIELDDISP",Oracle.DataAccess.Client.OracleDbType.Varchar2, 30),
            //            new Oracle.DataAccess.Client.OracleParameter("F_ANNODISP",Oracle.DataAccess.Client.OracleDbType.Varchar2, 10),
            //            new Oracle.DataAccess.Client.OracleParameter("F_TIPDISP",Oracle.DataAccess.Client.OracleDbType.Varchar2, 10),
            //            new Oracle.DataAccess.Client.OracleParameter("F_NAME",Oracle.DataAccess.Client.OracleDbType.Varchar2,30)
            //        };

            //        oParams[0].Value = EAGL.Core.SerializationManager.Serialize(ipPropertySet.GetProperty("Renderer"));
            //        oParams[1].Value = EAGL.Core.SerializationManager.Serialize(ipPropertySet.GetProperty("Annotation"));
            //        oParams[2].Value = ipPropertySet.GetProperty("MinScale") == null ? DBNull.Value : ipPropertySet.GetProperty("MinScale");
            //        oParams[3].Value = ipPropertySet.GetProperty("MaxScale") == null ? DBNull.Value : ipPropertySet.GetProperty("MaxScale");
            //        oParams[4].Value = ipPropertySet.GetProperty("DisplayField") == null ? DBNull.Value : ipPropertySet.GetProperty("DisplayField");
            //        oParams[5].Value = ipPropertySet.GetProperty("DisplayAnno") == null ? DBNull.Value : ipPropertySet.GetProperty("DisplayAnno");
            //        oParams[6].Value = ipPropertySet.GetProperty("Tooltip") == null ? DBNull.Value : ipPropertySet.GetProperty("Tooltip");
            //        oParams[7].Value = Convert.ToString(row["F_NAME"]);

            //        oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);

            //    }
            //}
            //catch (Exception)
            //{

            //    throw;
            //}
            //finally
            //{
            //    oDBManager.Close();
            //}

            //return;
            #endregion 레이어렌더링 초기설정
            ///-------------------------------------------------------------------------
            //해석모델 선택화면
            WaterNetCore.frmINP_Title form = new WaterNetCore.frmINP_Title("RI");
            if (form.Open() == DialogResult.Cancel) return;
            if (WC_Global.crisisAnalysisOp.BaseModel != null)
            {
                if (((EpaSimulator.DBNetwork)WC_Global.crisisAnalysisOp.BaseModel).inpNUMBER.Equals(form.m_INP_NUMBER))
                {
                    MessageBox.Show(string.Format("({0}) 수리모델은 현재 실행중인 모델과 동일합니다.", form.m_INP_NUMBER));
                    return;
                }
            }
            //WC_Common.WC_VariableManager.m_INP_NUMBER = form.m_INP_NUMBER;

            frmSplashMsg oSplash = new frmSplashMsg();
            oSplash.Open();
            oSplash.Message = "수리해석모델 레이어를 불러오는 중입니다.\n\r잠시만 기다리십시오.";
            Application.DoEvents();

            try
            {
                #region 수리모델 구성(DB에 저장된 원본모델)
                //일차적으로 DB에 있는모델을 그대로 수리해석하여 결과를 저장한다.(비상공급관을 찾을때 사용됨)
                WC_Global.crisisAnalysisOp.BaseModel = new EpaSimulator.DBNetwork(form.m_INP_NUMBER);
                WC_Global.crisisAnalysisOp.BaseModel.makeNetwork();
                WC_Global.crisisAnalysisOp.BaseModel.NETWORK.Options.Data[EpaNetwork.xGlobal.DURATION_INDEX] = "0:00";
                WC_Global.crisisAnalysisOp.BaseModel.NETWORK.Options.Data[EpaNetwork.xGlobal.HYD_TSTEP_INDEX] = "1:00";
                WC_Global.crisisAnalysisOp.BaseModel.NETWORK.Options.Data[EpaNetwork.xGlobal.QUAL_TSTEP_INDEX] = "1:00";
                WC_Global.crisisAnalysisOp.BaseModel.NETWORK.Options.Data[EpaNetwork.xGlobal.RPT_TSTEP_INDEX] = "0:00";
                WC_Global.crisisAnalysisOp.BaseModel.NETWORK.Options.Data[EpaNetwork.xGlobal.PAT_TSTEP_INDEX] = "1:00";
                //int err = WC_Global.crisisAnalysisOp.BaseModel.RunAnalysis();
                //if (err != 0)
                //{
                //    throw new Exception(EpaNetwork.xGlobal.ErrorCodes[err]);
                //}
                #endregion

                #region 모델레이어 생성 및 맵로드
                CreateINPLayerManager2 inpManager = new CreateINPLayerManager2(WC_Global.crisisAnalysisOp.BaseModel);
                int flag = inpManager.Execute();
                if (flag == 0)
                {
                    MessageBox.Show("Error");
                    return;
                }
                else if (flag == -1 || flag == 1) //기존의 workspace 있거나, 정상적으로 생성됨
                {
                    this.Remove_INP_Layer(new string[] { "TANK", "RESERVOIR", "JUNCTION", "PIPE", "PUMP", "VALVE" });
                    Dictionary<string, IFeatureLayer> modelLayers = inpManager.ModelLayers;
                    this.Load_INP_Layer(modelLayers, new string[] { "TANK", "RESERVOIR", "JUNCTION", "PIPE", "PUMP", "VALVE" });
                }
                #endregion 모델레이어 생성 및 맵로드

                #region Extent범위로 지도확대/축소
                //QuickGraph.Geometries.Index.RTree<int> t = new QuickGraph.Geometries.Index.RTree<int>();
                EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
                ILayer layer = mapExplorer.GetLayer("JUNCTION");

                //QuickGraph.Geometries.Rectangle env = t.getBounds();
                //IEnvelope rect = new EnvelopeClass();
                //rect.PutCoords(env.XMin, env.YMin, env.XMax, env.YMax);
                //rect.Expand(1.1, 1.1, true);
                this.axMap.ActiveView.Extent = ((IFeatureLayer)layer).AreaOfInterest;
                #endregion Extent범위로 지도확대/축소
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                oSplash.Close();
            }

            ///초기화 및 분석조회화면, 팝업메뉴 삭제
            this.ClearVariable();
            this.ModifyButton(sender);
        }

        /// <summary>
        /// 기능버튼 BackColor 설정
        /// </summary>
        /// <param name="sender"></param>
        private void ModifyButton(object sender)
        {
            if (sender == null) return;
            Button button = sender as Button;
            if (button == null) return;

            switch (button.Name)
            {
                case "btnSelModel":
                    btnSelModel.BackColor = Color.FromArgb(14, 158, 193);
                    btnBrokePoint.BackColor = Color.FromArgb(220, 232, 244);
                    btnAnalysis.BackColor = Color.FromArgb(220, 232, 244);
                    btnReplacePipe.BackColor = Color.FromArgb(220, 232, 244);
                    break;
                case "btnBrokePoint":
                    btnBrokePoint.BackColor = Color.FromArgb(14, 158, 193);
                    btnAnalysis.BackColor = Color.FromArgb(220, 232, 244);
                    btnReplacePipe.BackColor = Color.FromArgb(220, 232, 244);
                    break;
                case "btnAnalysis":
                    btnAnalysis.BackColor = Color.FromArgb(14, 158, 193);
                    btnReplacePipe.BackColor = Color.FromArgb(220, 232, 244);
                    break;
                case "btnReplacePipe":
                    btnReplacePipe.BackColor = Color.FromArgb(14, 158, 193);
                    break;
                default:
                    break;
            }

        }

        /// <summary>
        /// 누수지점선택 버튼 클릭-차단제수밸브 탐색
        /// 지도화면에서 누수지점을 선택하여, 분석실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBrokePoint_Click(object sender, EventArgs e)
        {
            if (WC_Global.crisisAnalysisOp.BaseModel == null)
            {
                WaterNetCore.MessageManager.ShowInformationMessage("해석모델을 로드하지 않았습니다.");
                return;
            }

            this.ModifyButton(sender);
            toolActionCommand.Checked = true;
            base.toolActionCommand_Click(this, new EventArgs());

        }

        /// <summary>
        /// 기존의 팝업메뉴, 조회화면, 그래픽 객체를 삭제한다.
        /// </summary>
        private void ClearVariable()
        {
            //분석결과 조회화면 및 팝업메뉴 삭제
            removeContextmenu();
            m_Extent.SetEmpty();
            if (this.m_ClosedGraph != null) this.m_ClosedGraph.Clear();
            //this.m_CrackPoint = null;
            //기존의 지도화면 선택객체 및 그래픽객체 초기화
            ArcManager.ClearSelection(axMap.ActiveView.FocusMap);
            IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
            pGraphicsContainer.DeleteAllElements();
            this.axMap.ActiveView.Refresh();
        }

        private void DrawFlowAnnotateRenderer(DataTable table, ILayer layer)
        {
            Dictionary<string, string> values = (from t in table.AsEnumerable()
                                                 where t.Field<double>("FLOW") > 0.0
                                                  select new
                                                  {
                                                      ID = t.Field<string>("LINK_ID"),
                                                      VALUE = Convert.ToString(t.Field<double>("FLOW"))
                                                  }).ToDictionary(k => k.ID, k => k.VALUE);
            if (values.Count == 0) return;

            EAGL.Display.LabelRenderingMap LRM = new EAGL.Display.LabelRenderingMap();
            LRM.Extent = this.axMap.ActiveView.Extent;
            LRM.valueMap = values;

            //렌더링
            var ids = values.Keys.Select(v => string.Format("'{0}'", v)).ToList();
            if (ids.Count == 0) return;

            string wheres = string.Format("{0} in (", "ID");
            wheres += string.Join(",", ids.ToArray());
            wheres += string.Format(")", string.Empty);

            IFormattedTextSymbol pText = new TextSymbolClass();
            pText.Font.Bold = false;
            pText.Font.Name = "굴림";
            pText.Size = 8;
            pText.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Blue);

            LRM.Add("0", wheres, "ID", (ITextSymbol)pText);

            if (layer != null)
            {
                LRM.FeatureLayer = (IFeatureLayer)layer;
                IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                for (int i = 0; i < ac.Count; i++)
                {
                    IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                    ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                }
                ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = this.chk_pres.Checked;
            }

        }

        /// <summary>
        /// 라벨표시 : Junction=수압, Pipe = 유량
        /// </summary>
        /// <param name="values"></param>
        private void DrawPressureAnnotateRenderer(DataTable table, ILayer layer)
        {
            Dictionary<string, string> values = (from t in table.AsEnumerable()
                                                 select new
                                                 {
                                                     ID = t.Field<string>("NODE_ID"),
                                                     VALUE = Convert.ToString(t.Field<double>("PRESSURE"))
                                                 }).ToDictionary(k => k.ID, k => k.VALUE);

            EAGL.Display.LabelRenderingMap LRM = new EAGL.Display.LabelRenderingMap();
            LRM.Extent = this.axMap.ActiveView.Extent;
            LRM.valueMap = values;

            double[] scope = { 0.0 };
            Color[] colors = { Color.Red, Color.Black };
            int[] size = { 8, 8 };

            for (int i = 0; i <= scope.Length; i++)
            {
                double min = double.MinValue; double max = double.MaxValue;
                if (i == 0)
                    max = scope[i];
                else if (i == scope.Length)
                    min = scope[i - 1];
                else
                {
                    min = scope[i - 1];
                    max = scope[i];
                }

                //렌더링
                var ids = values.Where(entry => (double.Parse(entry.Value) > min && double.Parse(entry.Value) <= max))
                                .Select(v => string.Format("'{0}'", v.Key)).ToList();
                if (ids.Count == 0) continue;

                string wheres = string.Format("{0} in (", "ID");
                wheres += string.Join(",", ids.ToArray());
                wheres += string.Format(")", string.Empty);

                IFormattedTextSymbol pText = new TextSymbolClass();
                pText.Font.Bold = false;
                pText.Font.Name = "굴림";
                pText.Size = size[i];
                pText.Color = EAGL.Display.ColorManager.GetESRIColor(colors[i]);

                LRM.Add(i.ToString(), wheres, "ID", (ITextSymbol)pText);
            }
            if (layer != null)
            {
                LRM.FeatureLayer = (IFeatureLayer)layer;
                IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                for (int i = 0; i < ac.Count; i++)
                {
                    IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                    ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                }
                ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = this.chk_pres.Checked;
            }
        }

        /// <summary>
        /// 수리해석 실행하여, 단수지역 탐색 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAnalysis_Click(object sender, EventArgs e)
        {
            #region Validation Check
            if (WC_Global.crisisAnalysisOp.BaseModel == null)
            {
                WaterNetCore.MessageManager.ShowInformationMessage("해석모델을 로드하지 않았습니다.");
                return;
            }

            if (this.m_ClosedGraph == null || this.m_ClosedGraph.EdgeCount == 0)
            {
                MessageManager.ShowInformationMessage("사고지점을 선택하여, 차단밸브검색이 실행되지 않았습니다\n\r먼저 사고지점 선정을 하십시오!");
                return;
            }

            frmSplashMsg oSplash = new frmSplashMsg();
            oSplash.Message = "단수지역 분석을 위한 수리해석을 실행중입니다.\n\r잠시만 기다리세요!";
            oSplash.Open();
            Application.DoEvents();

            ///workmodel이 변경되었으므로, basemodel로 복사하여 초기화
            WC_Global.crisisAnalysisOp.WorkModel = WC_Global.crisisAnalysisOp.BaseModel.Clone();

            ///차단구간내 상수관로 = 차단밸브를 찾는 과정에서 선택된 Edge의 상수관로
            ///상수관로 <-> 모델(PIPE) 맵핑
            ///중복된 상수관로를 걸러내어, INP의 PIPE를 찾기위함
            var closepipes = this.m_ClosedGraph.Edges.Where(entry => (entry != null
                                 && ((new string[] { "상수관로", "WTL_PIPE_LS" }).Contains(entry.CLASSNAME))))
                                 .GroupBy(entry => entry.HashData["FTR_IDN"])
                                 .Select(grp => grp.First().HashData["FTR_IDN"].ToString()).ToList();

            ///모델에서 상수관로에 해당하는 LINK(PIPE)를 찾는다.
            var closelinks = WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Links
                            .Where(entry => closepipes.Contains(entry.Value.getTagstoken('|', 1)) &&
                                            entry.Value.getTagstoken('|', 0).Equals("상수관로"))
                            .Select(entry => entry).ToDictionary(entry => entry.Value, entry => entry.Key);

            if (closelinks.Count() == 0)
            {
                MessageBox.Show("상수관로에 해당하는 해석모델의 LINK 객체를 찾을수 없습니다.\n 수리모델의 tags 정보를 확인하십시오.");
                oSplash.Close();
                return;
            }

            #endregion Validation Check
            ///---------------------------------------------------------------------------------

            try
            {
                string[] type = { "JUNCTION", "제수밸브", "이토밸브", "상수관로", "WTL_PIPE_LS", "급수관로", "WTL_SPLY_LS", "소방시설", "WTL_FIRE_PS" };
                for (int i = 0; i < type.Length; i++)
                {
                    WC_Common.WC_FunctionManager.removeGraphics(type[i]);
                }

                foreach (var item in closelinks)
                {
                    ((EpaNetwork.xPipe)item.Key).Status = "CLOSED";
                }

                var minus_nodes = WC_Global.crisisAnalysisOp.BatchMinusPressure();
                ///물흐름 방향은 여기에서 출력
                this.DrawArrowRenderer(WC_Global.crisisAnalysisOp.WorkModel);

                if (minus_nodes == null)
                {
                    return;
                }
                QuickGraph.Geometries.Index.RTree<IGeometry> t = new QuickGraph.Geometries.Index.RTree<IGeometry>();
                foreach (var item in minus_nodes)
                {
                    IGeometry geometry = EpaNetwork.xUtils.getGeometryVertexEdge(item.Value);
                    Add_GraphicsDic(geometry, "JUNCTION");
                    t.Add(this.get_Rectangle(geometry), geometry);
                }

                IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
                for (int i = 0; i < type.Length; i++)
                {
                    WC_Common.WC_FunctionManager.displayGraphics(pGraphicsContainer, type[i]);
                }

                if (t.Count > 0)
                {
                    //찾은 edge객체의 Extent범위로 지도확대/축소
                    QuickGraph.Geometries.Rectangle env = t.getBounds();
                    this.m_Extent.PutCoords(env.XMin, env.YMin, env.XMax, env.YMax);
                    this.m_Extent.Expand(1.1, 1.1, true);
                    this.axMap.ActiveView.Extent = this.m_Extent;
                    this.axMap.ActiveView.Refresh();
                    Application.DoEvents();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oSplash.Close();
            }


            #region 단수지역의 상수관로 FTR_IDN
            var minus = WC_Global.crisisAnalysisOp.GetMinusPressure();
            var q1 = (from entry in WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Links.Values
                      where minus.Keys.Contains(entry.Target.Id) && entry is EpaNetwork.xPipe &&
                      "상수관로".Equals(entry.getTagstoken('|', 0).Trim()) &&
                      !string.IsNullOrEmpty(entry.getTagstoken('|', 1).Trim())
                      select new { IDN = string.Format("{0}", entry.getTagstoken('|', 1)) }).Distinct();
            List<string> minus_pipe = q1.Select(entry => entry.IDN).ToList();
            #endregion 단수지역의 상수관로 FTR_IDN

            Form oform = new frmBreakDesc();
            ((frmBreakDesc)oform).mapControl = (IMapControl3)axMap.Object;
            ((frmBreakDesc)oform).minus_pipe = minus_pipe;
            ((frmBreakDesc)oform).TopLevel = false;
            ((frmBreakDesc)oform).Parent = this;
            ((frmBreakDesc)oform).TopMost = true;
            ((frmBreakDesc)oform).Text = "단수지역 분석결과";
            ((frmBreakDesc)oform).BringToFront();
            if (((frmBreakDesc)oform).Open()) AddContextmenu(oform);

            this.ModifyButton(sender);
        }

        /// <summary>
        /// 비상공급관 적용 수리 해석 실행
        /// </summary>
        /// <param name="oList"></param>
        public void ExecuteExtendAnalysis(Dictionary<EpaNetwork.xPipe, string> altValve)
        {
            WC_Common.WC_VariableManager.m_SplashForm.Open();
            WC_Common.WC_VariableManager.m_SplashForm.Message = "단수지역 분석을 위한 수리해석을 실행중입니다.\n\r잠시만 기다리세요!";
            Application.DoEvents();

            try
            {
                int err = WC_Global.crisisAnalysisOp.WorkModel.RunAnalysis();
                if (err != 0)
                {
                    throw new Exception(EpaNetwork.xGlobal.ErrorCodes[err]);
                }
                var minus_nodes = WC_Global.crisisAnalysisOp.GetMinusPressure();

                ///물흐름 방향은 여기에서 출력
                this.DrawArrowRenderer(WC_Global.crisisAnalysisOp.WorkModel);

                WC_Common.WC_FunctionManager.removeGraphics("JUNCTION");
                QuickGraph.Geometries.Index.RTree<IGeometry> t = new QuickGraph.Geometries.Index.RTree<IGeometry>();
                foreach (var item in minus_nodes)
                {
                    IGeometry geometry = EpaNetwork.xUtils.getGeometryVertexEdge(item.Value);
                    Add_GraphicsDic(geometry, "JUNCTION");
                    t.Add(this.get_Rectangle(geometry), geometry);
                }

                IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
                pGraphicsContainer.DeleteAllElements();

                string[] type = { "JUNCTION","사고지점","제수밸브", "이토밸브", "상수관로", "WTL_PIPE_LS", "급수관로", "WTL_SPLY_LS", "소방시설", "WTL_FIRE_PS" };
                for (int i = 0; i < type.Length; i++)
                {
                    WC_Common.WC_FunctionManager.displayGraphics(pGraphicsContainer, type[i]);
                }
                this.axMap.ActiveView.Extent = this.m_Extent;
                this.axMap.ActiveView.Refresh();
                Application.DoEvents();
            }
            catch (Exception)
            {
                throw new Exception("수리해석 실행중 오류가 발생하였습니다.");
            }
            finally
            {
                WC_Common.WC_VariableManager.m_SplashForm.Hide();
            }

            #region 단수지역의 상수관로 FTR_IDN
            var minus = WC_Global.crisisAnalysisOp.GetMinusPressure();
            var q1 = (from entry in WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Links.Values
                      where minus.Keys.Contains(entry.Target.Id) && entry is EpaNetwork.xPipe &&
                      "상수관로".Equals(entry.getTagstoken('|', 0).Trim()) &&
                      !string.IsNullOrEmpty(entry.getTagstoken('|', 1).Trim())
                      select new { IDN = string.Format("{0}", entry.getTagstoken('|', 1)) }).Distinct();
            List<string> minus_pipe = q1.Select(entry => entry.IDN).ToList();
            #endregion 단수지역의 상수관로 FTR_IDN

            string title = string.Join("|", altValve.Keys.Select(entry => string.Format("{0}", entry.Id)).ToArray());
            Form oform = new frmBreakDesc();
            ((frmBreakDesc)oform).mapControl = (IMapControl3)axMap.Object;
            ((frmBreakDesc)oform).minus_pipe = minus_pipe;
            ((frmBreakDesc)oform).TopLevel = false;
            ((frmBreakDesc)oform).Parent = this;
            ((frmBreakDesc)oform).TopMost = true;
            oform.Text = "2차단수지역분석결과(" + title.ToString() + ")";
            ((frmBreakDesc)oform).BringToFront();
            if (((frmBreakDesc)oform).Open()) AddContextmenu(oform);

            
        }

        #region 팝업메뉴
        /// <summary>
        /// 분석결과 화면 보기버튼 클릭시 팝업메뉴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAnalysisView_MouseDown(object sender, MouseEventArgs e)
        {
            contextMenuPopup.Show((Button)sender, new System.Drawing.Point(e.Location.X, e.Location.Y));
        }

        /// <summary>
        /// 팝업메뉴 추가
        /// </summary>
        /// <param name="oform"></param>
        private void AddContextmenu(Form oform)
        {
            for (int i = 0; i < contextMenuPopup.Items.Count; i++)
            {
                Form form = contextMenuPopup.Items[i].Tag as Form;
                if (form.Name.Equals(oform.Name))
                {
                    if (form.Text.Equals(oform.Text))
                    {
                        removeAtContextmenu(i);
                        break;
                    }
                }
            }

            ToolStripItem item = new ToolStripMenuItem();
            item.Text = oform.Text;
            item.Tag = oform;
            item.ToolTipText = oform.Text;
            contextMenuPopup.Items.Add(item);

            item.Click += new EventHandler(toolStripMenuItemClick);
        }

        /// <summary>
        /// 팝업메뉴 Clear
        /// </summary>
        private void removeContextmenu()
        {
            if (contextMenuPopup.Items.Count == 0) return;
            for (int i = contextMenuPopup.Items.Count - 1; i >= 0; i--)
            {
                Form oform = contextMenuPopup.Items[i].Tag as Form;
                if (oform != null) oform.Close();

                contextMenuPopup.Items.RemoveAt(i);
            }
        }

        /// <summary>
        /// 팝업메뉴 Index 삭제 및 조회폼 삭제
        /// </summary>
        /// <param name="index"></param>
        private void removeAtContextmenu(int index)
        {
            if (contextMenuPopup.Items.Count == 0) return;

            Form oform = contextMenuPopup.Items[index].Tag as Form;
            if (oform != null) oform.Close();

            contextMenuPopup.Items.RemoveAt(index);

        }

        /// <summary>
        /// 팝업메뉴 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemClick(object sender, EventArgs e)
        {
            ToolStripItem item = sender as ToolStripItem;
            Form oform = item.Tag as Form;
            try
            {
                if (oform != null)
                {
                    oform.BringToFront();
                    oform.Activate();
                    oform.Show();
                }
            }
            catch (Exception)
            {

            }

        }
        #endregion 팝업메뉴

       

        private List<string> getValueMapRow(ref Dictionary<string, string> values, double min, double max, out int direction)
        {
            List<string> ids = null;
            if (min == max) //0
            {
                ids = values.Where(entry => (double.Parse(entry.Value) == min && double.Parse(entry.Value) == max))
                                                   .Select(v => string.Format("{0}", v.Key)).ToList();
                direction = 0;
            }
            else if (min < 0 && max <= 0) //-
            {
                ids = values.Where(entry => (double.Parse(entry.Value) >= min && double.Parse(entry.Value) < max))
                                                   .Select(v => string.Format("{0}", v.Key)).ToList();
                direction = -1;
            }
            else if (min >= 0 && max > 0)
            {
                ids = values.Where(entry => (double.Parse(entry.Value) > min && double.Parse(entry.Value) <= max))
                                                   .Select(v => string.Format("{0}", v.Key)).ToList();
                direction = 1;
            }
            else
            {
                ids = values.Select(v => string.Format("{0}", v.Key)).ToList();
                direction = 2;
            }
            return ids;
        }

        /// <summary>
        /// 해석결과:유량의 흐름방향을 지도화면에 표시한다.
        /// </summary>
        private void DrawRenderer()
        {
            EAGL.Explore.MapExplorer map = new EAGL.Explore.MapExplorer(this.axMap.Map);
            ILayer layer = map.GetLayer("PIPE");
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            Dictionary<string, string> values = (from t in WC_Common.WC_VariableManager.analysis_Data.Tables["LINK"].AsEnumerable()
                                                 select new
                                                 {
                                                     ID = t.Field<string>("LINK_ID"),
                                                     VALUE = Convert.ToString(t.Field<double>("FLOW"))
                                                 }).ToDictionary(k => k.ID, k => k.VALUE);
            if (values.Count == 0) return;

            EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
            vrm.Fields = new string[] { "ID" };

            double[] scope = { 0, 0 };
            Color[] colors = { Color.Gray, Color.Gray, Color.Gray };
            int[] width = { 2, 1, 2 };

            for (int i = 0; i <= scope.Length; i++)
            {
                double min = double.MinValue; double max = double.MaxValue;
                if (i == 0)
                    max = scope[i];
                else if (i == scope.Length)
                    min = scope[i - 1];
                else
                {
                    min = scope[i - 1];
                    max = scope[i];
                }

                int direction;
                List<string> ids = getValueMapRow(ref values, min, max, out direction);
                if (ids == null || ids.Count == 0) continue;

                ISimpleLineDecorationElement simpleLineDecorationElement = new SimpleLineDecorationElementClass();
                simpleLineDecorationElement.AddPosition(0.5);
                simpleLineDecorationElement.PositionAsRatio = true;

                ICartographicLineSymbol cartoGraphicLineSymbol = new CartographicLineSymbolClass();
                cartoGraphicLineSymbol.Color = EAGL.Display.ColorManager.GetESRIColor(colors[i]);
                cartoGraphicLineSymbol.Width = width[i];

                ILineProperties lineProperties = cartoGraphicLineSymbol as ILineProperties;
                ILineDecoration lineDecoration = new LineDecorationClass();
                lineDecoration.AddElement(simpleLineDecorationElement);
                lineProperties.LineDecoration = lineDecoration;
                ISymbol sym = cartoGraphicLineSymbol as ISymbol;

                switch (direction)
                {
                    case -1:
                        simpleLineDecorationElement.MarkerSymbol = ArcManager.MakeArrowSymbol(Color.Black, 8, 6, 180);
                        break;
                    case 2:
                    case 0:
                        EAGL.Display.Rendering.SimpleLineRenderer slr = new EAGL.Display.Rendering.SimpleLineRenderer();
                        slr.Color = colors[i];
                        slr.Style = esriSimpleLineStyle.esriSLSSolid;
                        slr.Width = width[i];
                        sym = slr.Symbol;
                        break;
                    case 1:
                        simpleLineDecorationElement.MarkerSymbol = ArcManager.MakeArrowSymbol(Color.Black, 8, 6, 0);
                        break;
                }

                object key = ids[0];
                List<object> refValues = new List<object>();
                for (int j = 1; j < ids.Count(); j++)
                {
                    refValues.Add(ids[j]);
                }
                if (min == max)
                    vrm.Add(key, sym, string.Format("{0}", "0"));
                else if (min == double.MinValue || max == double.MaxValue)
                    vrm.Add(key, sym, string.Format("{0}{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));
                else
                    vrm.Add(key, sym, string.Format("{0}-{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));

                vrm.AddReferanceValue2(key, refValues);
            }
            ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
        }

       

        /// <summary>
        /// 대체관로 탐색 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReplacePipe_Click(object sender, EventArgs e)
        {
            #region Validation Check
            if (this.m_ClosedGraph == null || this.m_ClosedGraph.EdgeCount == 0)
            {
                MessageBox.Show("사고지점을 선택하여, 차단밸브검색이 실행되지 않았습니다\n\r먼저 사고지점 선정을 하십시오!");
                return;
            }

            if (WC_Global.crisisAnalysisOp.WorkModel.Simulator == null)
            {
                MessageBox.Show("1차수리해석을 실행하지 않았습니다.\n\r먼저 1차수리해석을 실행하십시오!");
                return;
            }
            #endregion Validation Check

            Form oform = new frmAlternatePipe(WC_Global.crisisAnalysisOp.GetAlternateValve2());
            ((frmAlternatePipe)oform).MapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
            ((frmAlternatePipe)oform).TopLevel = false;
            ((frmAlternatePipe)oform).Parent = this;
            ((frmAlternatePipe)oform).TopMost = true;
            ((frmAlternatePipe)oform).BringToFront();
            if (((frmAlternatePipe)oform).Open()) AddContextmenu(oform);

            ModifyButton(sender);
        }

        #endregion 버튼 처리


        #region 네트워크 구조 양방향 검색
        /// <summary>
        /// 무방향 그래프 검색하여 그래프로 반환
        /// </summary>
        /// <param name="e"></param>
        /// <param name="v"></param>
        /// <param name="g"></param>
        /// <param name="closedgraph"></param>
        private void BiDirectionSearch(Edge<Vertex> e, Vertex v,
                    ref QuickGraph.BidirectionalGraph<Vertex, Edge<Vertex>> closedgraph, ISymbol pointsymbol, ISymbol linesymbol)
        {
            if (!v.Visited)
            {
                v.Visited = true;
                this.axMap.FlashShape(EpaNetwork.xUtils.getGeometryVertexEdge(v), 1, 100, pointsymbol);
            }
            
            if ((new string[] { "밸브", "WTL_VALV_PS" }).Contains(v.CLASSNAME) && (Convert.ToString(v.HashData["FTR_CDE"])).Equals("SA200"))
            {
                if (!closedgraph.ContainsEdge(e)) closedgraph.AddVerticesAndEdge(e);
                return;
            }

            if (!closedgraph.ContainsEdge(e)) closedgraph.AddVerticesAndEdge(e);
            if (!e.Visited)
            {
                e.Visited = true;
                this.axMap.FlashShape(EpaNetwork.xUtils.getGeometryVertexEdge(e), 1, 100, linesymbol);
            }
            

            List<Edge<Vertex>> edges = this.graph.AdjacentEdges(v).ToList();
            foreach (Edge<Vertex> edge in edges)
            {
                if (!((new string[] { "상수관로", "WTL_PIPE_LS" }).Contains(edge.CLASSNAME))) continue; //상수관로만 찾기위해서 삽입됨                
                if (edge.Visited) continue;

                BiDirectionSearch(edge, edge.Source, ref closedgraph, pointsymbol, linesymbol);
                BiDirectionSearch(edge, edge.Target, ref closedgraph, pointsymbol, linesymbol);
            }
        }

       
        #endregion 네트워크 구조 양방향 검색

        #region 차단밸브 찾기-1차 및 확장검색
        /// <summary>
        /// 1차 차단밸브 및 관로 찾기-양방향 검색
        /// </summary>
        /// <param name="nearestedge">선택지점의 edge</param>
        /// <param name="nearestPoint">선택지점 위치</param>        
        private QuickGraph.BidirectionalGraph<Vertex, Edge<Vertex>> ExecuteClosedAnalysis(Edge<Vertex> nearestedge, IPoint nearestPoint)
        {
            ISymbol linesymbol = ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 5, ArcManager.GetColor(Color.Red));
            ISymbol pointsymbol = ArcManager.MakeSimpleMarkerSymbol(10, esriSimpleMarkerStyle.esriSMSCircle, ArcManager.GetColor(Color.Blue), false, 1);

            foreach (var edge in this.graph.Edges)
                edge.Visited = false;
            foreach (var v in this.graph.Vertices)
                v.Visited = false;

            //Vertex root = nearestedge.Source;
            Vertex root = (GetDistancePoint(EpaNetwork.xUtils.getGeometryVertexEdge(nearestedge.Source), nearestPoint) >
                           GetDistancePoint(EpaNetwork.xUtils.getGeometryVertexEdge(nearestedge.Target), nearestPoint)) ?
                          nearestedge.Target : nearestedge.Source;

            QuickGraph.BidirectionalGraph<Vertex, Edge<Vertex>> closedgraph = new BidirectionalGraph<Vertex, Edge<Vertex>>();
            BiDirectionSearch(nearestedge, nearestedge.Source, ref closedgraph, pointsymbol, linesymbol);
            BiDirectionSearch(nearestedge, nearestedge.Target, ref closedgraph, pointsymbol, linesymbol);

            return closedgraph;
        }

        public void ExecuteExtendCloseAnalysis(ref QuickGraph.BidirectionalGraph<Vertex, Edge<Vertex>> closedgraph, List<Vertex> valves)
        {
            ISymbol linesymbol = ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 5, ArcManager.GetColor(Color.Red));
            ISymbol pointsymbol = ArcManager.MakeSimpleMarkerSymbol(10, esriSimpleMarkerStyle.esriSMSCircle, ArcManager.GetColor(Color.Blue), false, 1);

            ///1차 검색에서 찾은 곳은 다시 검색하지 않도록
            ///1차 검색과 동일한 edge, vertex visited = true로 설정
            foreach (var edge in this.graph.Edges.Intersect(closedgraph.Edges))
                edge.Visited = true;
            foreach (var vertex in this.graph.Vertices.Intersect(closedgraph.Vertices))
                vertex.Visited = true;

 
            foreach (Vertex root in valves)
            {
                List<QuickGraph.Edge<Vertex>> edges = this.graph.AdjacentEdges(root).ToList();
                foreach (var edge in edges)
                {
                    Vertex v = edge.Source == root ? edge.Target : edge.Source;
                    BiDirectionSearch(edge, v, ref closedgraph, pointsymbol, linesymbol);
                }
            }
            #region 차단밸브검색결과 - 지도화면에 렌더링
            QuickGraph.Geometries.Index.RTree<IGeometry> t = new QuickGraph.Geometries.Index.RTree<IGeometry>();
            foreach (var edge in this.m_ClosedGraph.Edges)
            {
                IGeometry line = EpaNetwork.xUtils.getGeometryVertexEdge(edge);
                t.Add(this.get_Rectangle(line), line);
                Add_GraphicsDic(line, edge.CLASSNAME);
            }
            foreach (var vt in this.m_ClosedGraph.Vertices)
            {
                if ((new string[] { "밸브", "WTL_VALV_PS", "소방시설", "WTL_FIRE_PS" }).Contains(vt.CLASSNAME))
                {
                    if (Convert.ToString(vt.HashData["FTR_CDE"]).Equals("SA200"))
                        Add_GraphicsDic(EpaNetwork.xUtils.getGeometryVertexEdge(vt), "제수밸브");
                    else if (Convert.ToString(vt.HashData["FTR_CDE"]).Equals("SA202"))
                        Add_GraphicsDic(EpaNetwork.xUtils.getGeometryVertexEdge(vt), "이토밸브");
                    else
                        Add_GraphicsDic(EpaNetwork.xUtils.getGeometryVertexEdge(vt), vt.CLASSNAME);
                }
            }
            IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
            string[] type = { "제수밸브", "이토밸브", "상수관로", "WTL_PIPE_LS", "급수관로", "WTL_SPLY_LS", "소방시설", "WTL_FIRE_PS" };
            for (int i = 0; i < type.Length; i++)
            {
                WC_Common.WC_FunctionManager.displayGraphics(pGraphicsContainer, type[i]);
            }

            //찾은 edge객체의 Extent범위로 지도확대/축소
            QuickGraph.Geometries.Rectangle env = t.getBounds();
            IEnvelope rect = new EnvelopeClass();
            rect.PutCoords(env.XMin, env.YMin, env.XMax, env.YMax);
            rect.Expand(1.1, 1.1, true);
            this.axMap.ActiveView.Extent = rect;
            this.axMap.ActiveView.Refresh();
            #endregion 차단밸브검색결과 - 지도화면에 렌더링

        }
        /// <summary>
        /// 확장 차단밸브 및 관로 찾기
        /// </summary>
        //public void ExecuteExtendCloseAnalysis(ref Dictionary<Edge<Vertex>, Vertex> Closededge, List<Vertex> valves)
        //{
        //    ISymbol linesymbol = ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 5, ArcManager.GetColor(Color.Red));
        //    ISymbol pointsymbol = ArcManager.MakeSimpleMarkerSymbol(10, esriSimpleMarkerStyle.esriSMSCircle, ArcManager.GetColor(Color.Blue), false, 1);

        //    try
        //    {
        //        foreach (Vertex root in valves)
        //        {
        //            List<Edge<Vertex>> edges = null;
        //            edges = graph.OutEdges(root).ToList();
        //            foreach (var edge in edges)
        //            {
        //                BiDirectionSearch(edge, root, ref Closededge, pointsymbol, linesymbol);
        //            }

        //            edges = graph.InEdges(root).ToList();
        //            foreach (var edge in edges)
        //            {
        //                BiDirectionSearch(edge, root, ref Closededge, pointsymbol, linesymbol);
        //            }
        //        }

        //        //찾은 edge객체의 Extent범위로 지도확대/축소
        //        IGeometry pGeom = this.getConvexHull(Closededge);
        //        if ((pGeom == null) | (pGeom.IsEmpty)) return;

        //        IEnvelope rect = pGeom.Envelope;
        //        rect.Expand(1.2, 1.2, true);
        //        IActiveView pActiveView = axMap.ActiveView as IActiveView;
        //        pActiveView.Extent = rect;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        axMap.MousePointer = esriControlsMousePointer.esriPointerArrow;
        //        this.axMap.Refresh();
        //    }
        //}
        #endregion 차단밸브 찾기-1차 및 확장검색

        #region 지도 처리
        private void DisplayRenderer_CrackPoint(IGeometry nearestPoint)
        {
            IColor pColor = ArcManager.GetColor(255, 0, 0);
            ISymbol pSymbol = ArcManager.MakeCharacterMarkerSymbol(30, 94, 0.0, pColor);
            IElement pElement = new MarkerElementClass();
            pElement.Geometry = nearestPoint;
            IMarkerElement pMarkerElement = pElement as IMarkerElement;
            pMarkerElement.Symbol = pSymbol as ICharacterMarkerSymbol;

            IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
            pGraphicsContainer.AddElement(pElement, 0);
            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics);
            ArcManager.FlashShape(axMap.ActiveView, nearestPoint as IPoint, 3);
        }

        //private IGeometry m_CrackPoint; //사고지점

        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            if (!((axMap.CurrentTool == null) & (toolActionCommand.Checked))) return;
            if (WC_Global.crisisAnalysisOp.BaseModel == null)
            {
                MessageBox.Show("수리해석모델 불러오기를 실행하지 않았습니다", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            IActiveView activeView = axMap.ActiveView;

            #region 사고지점을 선택하고, 상수관로상의 최단거리 Point를 찾는다.
            IRubberBand pRubberBand = new RubberPointClass();
            IPoint pt = (IPoint)pRubberBand.TrackNew(activeView.ScreenDisplay, null);
            if (pt == null | pt.IsEmpty) return;

            //---------------------네트워크 구조사용
            List<Edge<Vertex>> list = rtreeEdge.Nearest(get_RTreePoint(pt), (float)15.0);
            if (list.Count == 0)
            {
                MessageManager.ShowInformationMessage("선택된 위치에는 상수관로가 없습니다. 다시 선택하십시오.");
                return;
            }
            double tmp = 0.0; double nearest = double.MaxValue; IPoint nearestPoint = null;
            Edge<Vertex> nearestedge = null;
            foreach (var edge in list)
            {
                IGeometry geometry = EpaNetwork.xUtils.getGeometryVertexEdge(edge);
                tmp = GetDistancePoint(geometry, pt);
                if (tmp < nearest)
                {
                    nearest = tmp;
                    nearestPoint = GetNearestPoint(geometry, pt);
                    nearestedge = edge;
                }
            }

            if (nearestedge.HashData["WID_CDE"].Equals("MGD001")) //광역관로
            {
                string message = "광역관로에서 사고가 발생되었습니다.\n\r비상공급관 분석에서는 광역비상관로만 표시됩니다.\n\r계속 진행하시겠습니까?";
                if ((MessageBox.Show(message, "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)) != DialogResult.Yes)
                {
                    WC_Common.WC_VariableManager.m_CrisisParams["PIPE_GBN"] = string.Empty;
                    return;
                }
                WC_Common.WC_VariableManager.m_CrisisParams["PIPE_GBN"] = "G";
            }
            //---------------------네트워크 구조
            #endregion 사고지점을 선택하고, 상수관로상의 최단거리 Point를 찾는다.

            ///기존 객체 멤버변수 및 환경 초기화
            ClearVariable();

            #region 위치선택 - 지도에 사고지점을 선택하고, 별표 표시
            IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
            IElementCollection elementCollection;
            if (WC_Common.WC_VariableManager.m_graphicsDic.ContainsKey("사고지점"))
            {
                elementCollection = WC_Common.WC_VariableManager.m_graphicsDic["사고지점"];
                IElement element = null; int id = -1;
                elementCollection.QueryItem(0, out element, out id);
                element.Geometry = nearestPoint as IGeometry;
            }
            else
            {
                elementCollection = new ElementCollectionClass();

                IColor pColor = ArcManager.GetColor(255, 0, 0);
                ISymbol pSymbol = ArcManager.MakeCharacterMarkerSymbol(30, 94, 0.0, pColor);
                IElement element = new MarkerElementClass();
                element.Geometry = nearestPoint as IGeometry;
                IMarkerElement pMarkerElement = element as IMarkerElement;
                pMarkerElement.Symbol = pSymbol as ICharacterMarkerSymbol;

                elementCollection.Add(element, -1);
                WC_Common.WC_VariableManager.m_graphicsDic.Add("사고지점", elementCollection);
            }
            WC_Common.WC_FunctionManager.displayGraphics(pGraphicsContainer, "사고지점");

            ArcManager.SetMapScale((IMapControl3)axMap.Object, 7000);
            ArcManager.MoveCenterAt((IMapControl3)axMap.Object, nearestPoint);
            Application.DoEvents();
            
            WC_Common.WC_VariableManager.m_CrisisParams["CRACKPOINT"] = (IGeometry)nearestPoint;
            
            #endregion 지도에 사고지점을 선택하고, 별표 표시 끝

            WC_Common.WC_SplashForm SplashForm = new WaterNet.WC_Common.WC_SplashForm();
            SplashForm.Message = "차단밸브 분석을 실행하고 있습니다. 잠시만 기다리세요!";
            SplashForm.Open();
            Application.DoEvents();

            try
            {
                //차단밸브 분석 실행
                string[] type = { "제수밸브", "이토밸브", "상수관로", "WTL_PIPE_LS", "급수관로", "WTL_SPLY_LS", "소방시설", "WTL_FIRE_PS" };
                for (int i = 0; i < type.Length; i++)
                {
                    WC_Common.WC_FunctionManager.removeGraphics(type[i]);
                }
                #region 차단관로를 네트워크 그래프에서 찾는다.(방향성없이)
                this.m_ClosedGraph = this.ExecuteClosedAnalysis(nearestedge, nearestPoint);
                #endregion 차단관로를 네트워크 그래프에서 찾는다.(방향성없이)

                #region 차단밸브검색결과 - 지도화면에 렌더링
                QuickGraph.Geometries.Index.RTree<IGeometry> t = new QuickGraph.Geometries.Index.RTree<IGeometry>();
                foreach (var edge in this.m_ClosedGraph.Edges)
                {
                    IGeometry line = EpaNetwork.xUtils.getGeometryVertexEdge(edge);
                    t.Add(this.get_Rectangle(line), line);
                    Add_GraphicsDic(line, edge.CLASSNAME);
                }
                foreach (var vt in this.m_ClosedGraph.Vertices)
                {
                    if ((new string[] { "밸브", "WTL_VALV_PS", "소방시설", "WTL_FIRE_PS" }).Contains(vt.CLASSNAME))
                    {
                        if (Convert.ToString(vt.HashData["FTR_CDE"]).Equals("SA200"))
                            Add_GraphicsDic(EpaNetwork.xUtils.getGeometryVertexEdge(vt), "제수밸브");
                        else if (Convert.ToString(vt.HashData["FTR_CDE"]).Equals("SA202"))
                            Add_GraphicsDic(EpaNetwork.xUtils.getGeometryVertexEdge(vt), "이토밸브");
                        else
                            Add_GraphicsDic(EpaNetwork.xUtils.getGeometryVertexEdge(vt), vt.CLASSNAME);
                    }
                }
                for (int i = 0; i < type.Length; i++)
                {
                    WC_Common.WC_FunctionManager.displayGraphics(pGraphicsContainer, type[i]);
                }

                //찾은 edge객체의 Extent범위로 지도확대/축소
                QuickGraph.Geometries.Rectangle env = t.getBounds();
                IEnvelope rect = new EnvelopeClass();
                rect.PutCoords(env.XMin, env.YMin, env.XMax, env.YMax);
                rect.Expand(1.1, 1.1, true);
                this.axMap.ActiveView.Extent = rect;
                this.axMap.ActiveView.Refresh();
                #endregion 차단밸브검색결과 - 지도화면에 렌더링

                ////차단제수밸브 보기화면 오픈
                ShowfrmCloseValveList(ref this.m_ClosedGraph);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                //throw ex;
            }
            finally
            {
                axMap.MousePointer = esriControlsMousePointer.esriPointerArrow;
                SplashForm.Close();
            }
        }

        /// <summary>
        /// 상수관망 네트워크:차단밸브검색시 검색된 객체를 렌더링한다.
        /// </summary>
        /// <param name="geometry"></param>
        /// <param name="gbn"></param>
        private void Add_GraphicsDic(IGeometry geometry, string type)
        {
            IElementCollection elementCollection;
            if (WC_Common.WC_VariableManager.m_graphicsDic.ContainsKey(type))
                elementCollection = WC_Common.WC_VariableManager.m_graphicsDic[type];
            else
            {
                elementCollection = new ElementCollectionClass();
                WC_Common.WC_VariableManager.m_graphicsDic.Add(type, elementCollection);
            }
            IColor pColor = null;
            switch (type)
            {
                case "제수밸브":
                    pColor = ArcManager.GetColor(System.Drawing.Color.Blue);
                    break;
                case "이토밸브":
                    pColor = ArcManager.GetColor(System.Drawing.Color.BlueViolet);
                    break;
                case "JUNCTION":
                    pColor = ArcManager.GetColor(System.Drawing.Color.Red);
                    break;
                case "상수관로":
                case "WTL_PIPE_LS":
                    pColor = ArcManager.GetColor(System.Drawing.Color.Red);
                    break;
                case "급수관로":
                case "WTL_SPLY_LS":
                    pColor = ArcManager.GetColor(System.Drawing.Color.Violet);
                    break;
                case "소방시설":
                case "WTL_FIRE_PS":
                    pColor = ArcManager.GetColor(System.Drawing.Color.Coral);
                    break;
                default:
                    return;
            }

            ESRI.ArcGIS.Display.ISimpleMarkerSymbol pMarkerSymbol = default(ESRI.ArcGIS.Display.ISimpleMarkerSymbol);
            ESRI.ArcGIS.Display.ISimpleLineSymbol pLineSymbol = default(ESRI.ArcGIS.Display.ISimpleLineSymbol);
            ESRI.ArcGIS.Display.ISimpleFillSymbol pFillSymbol = default(ESRI.ArcGIS.Display.ISimpleFillSymbol);
            ESRI.ArcGIS.Display.ISymbol pSymbol = null;

            IElement element = null;
            switch (geometry.GeometryType)
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    pMarkerSymbol = new ESRI.ArcGIS.Display.SimpleMarkerSymbolClass();
                    pMarkerSymbol.Style = ESRI.ArcGIS.Display.esriSimpleMarkerStyle.esriSMSCircle;
                    pMarkerSymbol.Color = pColor;
                    pMarkerSymbol.OutlineColor = pColor;
                    pMarkerSymbol.Size = 11;

                    IMarkerElement markerElement = new MarkerElementClass();
                    markerElement.Symbol = pMarkerSymbol;
                    element = (IElement)markerElement;
                    element.Geometry = geometry;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryLine:
                    pLineSymbol = new ESRI.ArcGIS.Display.SimpleLineSymbolClass();
                    pLineSymbol.Width = 3;
                    pLineSymbol.Style = esriSimpleLineStyle.esriSLSSolid;
                    pLineSymbol.Color = pColor;

                    ILineElement lineElement = new LineElementClass();
                    lineElement.Symbol = pLineSymbol;
                    element = (IElement)lineElement; // Explicit Cast
                    element.Geometry = geometry;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    pFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbolClass();
                    pFillSymbol.Outline = null;
                    pFillSymbol.Color = pColor;
                    pFillSymbol.Style = esriSimpleFillStyle.esriSFSCross;
                    pSymbol = pFillSymbol as ISymbol;

                    IFillShapeElement polygonElement = new PolygonElementClass();
                    polygonElement.Symbol = pFillSymbol;
                    element = (IElement)polygonElement; // Explicit Cast
                    element.Geometry = geometry;
                    break;
            }

            if (!(element == null))
            {
                elementCollection.Add(element, 0);
            }
        }


        protected override void axMap_OnMouseMove(object sender, IMapControlEvents2_OnMouseMoveEvent e)
        {
            base.axMap_OnMouseMove(sender, e);
        }

        #endregion 지도 처리

        /// <summary>
        /// 차단제수밸브 보기화면 오픈
        /// </summary>
        private void ShowfrmCloseValveList(ref BidirectionalGraph<Vertex, Edge<Vertex>> closedgraph)
        {
            Form oform = new frmCloseValveList();
            ((frmCloseValveList)oform).mapControl = (IMapControl3)axMap.Object;
            ((frmCloseValveList)oform).ClosedGraph = closedgraph;  //차단 edge목록

            ((frmCloseValveList)oform).TopLevel = false;
            ((frmCloseValveList)oform).Parent = this;
            ((frmCloseValveList)oform).TopMost = true;
            ((frmCloseValveList)oform).BringToFront();
            if (((frmCloseValveList)oform).Open()) AddContextmenu(oform);
        }

        

        #region 위기-단수관리 해석 ResetValue Hashtable
        /// <summary>
        /// 차단Pipe객체를 해석실행을 위하여 HashTable로 변환
        /// 차단Pipe객체의 상태를 Close로 변경한다.
        /// key = PIPE ID, status = Closed
        /// </summary>
        /// <returns></returns>
        private Hashtable CreateResetValues(System.Data.DataTable datatable, List<string> ADD_PIPE)
        {
            Hashtable resetValue = new Hashtable();  //최상위 hashtable
            Hashtable oPipeS = new Hashtable(); //서브 Pipe 파라메터

            foreach (System.Data.DataRow row in datatable.Rows)
                oPipeS.Add(row["ID"], "Closed");

            if (ADD_PIPE != null)
            {
                foreach (var id in ADD_PIPE)
                {
                    if (oPipeS.ContainsKey(id))
                    {
                        oPipeS[id] = "Open";
                    }
                    else
                    {
                        oPipeS.Add(id, "Open");
                    }
                }
            }

            resetValue.Add("pipe", oPipeS);

            ///INP Times Section
            CreateSectionTimes(ref resetValue);

            ///INP Options Section
            CreateSectionOptions(ref resetValue);

            return resetValue;
        }

        /// <summary>
        /// INP Times Section을 생성한다.
        /// resetValue : HashTable에 추가한다.
        /// </summary>
        /// <param name="resetValue"></param>
        private void CreateSectionTimes(ref Hashtable resetValue)
        {
            Hashtable times = new Hashtable(); //INP Times Section
            times.Add("Report Timestep", "1:00");
            times.Add("Duration", "1:00");
            //times.Add("Pattern Timestep", "1:00");
            //times.Add("Hydraulic Timestep", "1:00");
            ////times.Add("Quality Timestep", "1:00");
            //times.Add("Pattern Start", "00:00");
            //times.Add("Report Start", "00:00");
            //times.Add("Start ClockTime", "12:00 AM");
            //times.Add("Statistic", "None");
            resetValue.Add("time", times);
        }

        /// <summary>
        /// INP Options Section을 생성한다.
        /// resetValue : Hashtable에 추가한다.
        /// </summary>
        /// <param name="resetValue"></param>
        private void CreateSectionOptions(ref Hashtable resetValue)
        {
            Hashtable options = new Hashtable();   //INP Options Section
            options.Add("Quality", "None mg/L");
            //options.Add("Units", "CMD");
            //options.Add("Headloss", "H-W");
            //options.Add("Specific Gravity", "1");
            //options.Add("Viscosity", "1");
            //options.Add("Trials", "40");
            //options.Add("Accuracy", "0.001");
            //options.Add("Unbalanced Continue", "10");
            //options.Add("Pattern", "1");
            //options.Add("Demand Multiplier", "1");
            //options.Add("Emitter Exponent", "0.5");
            //options.Add("Diffusivity", "1");
            options.Add("Tolerance", "0.01");

            resetValue.Add("option", options);
        }

        #endregion 위기-단수관리 해석 ResetValue Hashtable

    

        /// <summary>
        /// 수압값을 라벨로 지도상에 표시하는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowLabelPressure(object sender, EventArgs e)
        {
            ILayer pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "JUNCTION");
            if (pLayer == null) return;
            IGeoFeatureLayer pGeoFeatureLayer = ((IFeatureLayer)pLayer) as IGeoFeatureLayer;
            if (pGeoFeatureLayer == null) return;
            pGeoFeatureLayer.DisplayAnnotation = ((CheckBox)sender).Checked;

            pLayer = ArcManager.GetMapLayer(axMap.ActiveView.FocusMap, "PIPE");
            if (pLayer == null) return;
            pGeoFeatureLayer = ((IFeatureLayer)pLayer) as IGeoFeatureLayer;
            if (pGeoFeatureLayer == null) return;
            pGeoFeatureLayer.DisplayAnnotation = ((CheckBox)sender).Checked;

            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeoSelection | esriViewDrawPhase.esriViewGraphics);
        }

        /// <summary>
        /// 수압값을 라벨로 지도상에 표시한다.
        /// </summary>
        /// <param name="pFeatureLayer"></param>
        private void SetPressureLabelProperty(IFeatureLayer pFeatureLayer)
        {
            if (!(pFeatureLayer is IGeoFeatureLayer)) return;

            IGeoFeatureLayer pGeoFeatureLayer = pFeatureLayer as IGeoFeatureLayer;
            IDisplayTable pDisTable = pGeoFeatureLayer as IDisplayTable;
            pGeoFeatureLayer.DisplayField = "NODE.PRESSURE";

            //라벨의 특성(위치, Conflict 등등)
            IBasicOverposterLayerProperties pBOLayerProps = new BasicOverposterLayerPropertiesClass();
            pBOLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPoint;
            pBOLayerProps.NumLabelsOption = esriBasicNumLabelsOption.esriOneLabelPerShape;
            pBOLayerProps.FeatureWeight = esriBasicOverposterWeight.esriNoWeight;
            pBOLayerProps.LabelWeight = esriBasicOverposterWeight.esriLowWeight;
            pBOLayerProps.GenerateUnplacedLabels = true;

            //라벨 폰트 & 칼라 정의
            stdole.StdFont pFont = new stdole.StdFontClass();
            pFont.Bold = false;
            pFont.Name = "굴림";    //굴림
            pFont.Size = 8;

            ////수압이 0이하 - 빨강색
            IFormattedTextSymbol pText = new TextSymbolClass();
            pText.Font = (stdole.IFontDisp)pFont;
            pText.Color = ArcManager.GetColor(Color.Red);

            IAnnotateLayerPropertiesCollection pAnnoLayerPropsColl = new AnnotateLayerPropertiesCollection();

            //스케일별 라벨 보이기/감추기
            IAnnotateLayerProperties pAnnoLayerProps;
            pAnnoLayerProps = new LabelEngineLayerPropertiesClass();
            pAnnoLayerProps.Class = "Lower";
            pAnnoLayerProps.AnnotationMinimumScale = 0;
            pAnnoLayerProps.AnnotationMaximumScale = 0;
            pAnnoLayerProps.DisplayAnnotation = true;
            //어느 Feature를 Labeling할 것인가?
            pAnnoLayerProps.WhereClause = "(NODE.PRESSURE <= -10000.0)";

            ILabelEngineLayerProperties pLabelEng = pAnnoLayerProps as ILabelEngineLayerProperties;
            pLabelEng.Symbol = pText;
            pLabelEng.Expression = "[NODE.PRESSURE]";
            pLabelEng.BasicOverposterLayerProperties = pBOLayerProps;

            pAnnoLayerPropsColl.Add(pLabelEng as IAnnotateLayerProperties);

            ////수압이 0이상 - 파란색
            IFormattedTextSymbol pText2 = new TextSymbolClass();
            pText2.Font = (stdole.IFontDisp)pFont;
            pText2.Color = ArcManager.GetColor(Color.Blue);

            pAnnoLayerProps = new LabelEngineLayerPropertiesClass();
            pAnnoLayerProps.Class = "Higher";
            pAnnoLayerProps.AnnotationMinimumScale = 0;
            pAnnoLayerProps.AnnotationMaximumScale = 0;
            pAnnoLayerProps.DisplayAnnotation = true;
            //어느 Feature를 Labeling할 것인가?
            pAnnoLayerProps.WhereClause = "(NODE.PRESSURE > 0.0)";

            ILabelEngineLayerProperties pLabelEng2 = pAnnoLayerProps as ILabelEngineLayerProperties;
            pLabelEng2.Symbol = pText2;
            pLabelEng2.Expression = "[NODE.PRESSURE]";
            pLabelEng2.BasicOverposterLayerProperties = pBOLayerProps;

            pAnnoLayerPropsColl.Add(pLabelEng2 as IAnnotateLayerProperties);

            pGeoFeatureLayer.AnnotationProperties = pAnnoLayerPropsColl;
            pGeoFeatureLayer.DisplayAnnotation = chk_pres.Checked;
        }

      

        #region QuickGraph->Shape로 변환
        //QuickGraph->Shape로 변환
        private void btnGraph2Shape_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            FBD.ShowNewFolderButton = true;
            if (FBD.ShowDialog() != DialogResult.OK) return;

            string dir = @"C:\waterNET(src)-Local\BaseMap\"; string dir2 = FBD.SelectedPath; // @"D:\Workset\BaseMap\Map\";
            string[] files = { "edge.*", "vertex.*" };
            foreach (string file in files)
            {
                string[] f = System.IO.Directory.GetFiles(dir, file, System.IO.SearchOption.TopDirectoryOnly);

                foreach (string f1 in f)
                {
                    string f2 = System.IO.Path.GetFileName(f1);
                    System.IO.File.Copy(f1, @dir2 + "\\" + f2, true);
                }
            }

            IWorkspace workspace = ArcManager.getShapeWorkspace(dir2);

            this.CreateNodeShape(workspace, graph);
            this.CreateLinkShape(workspace, graph);
        }

        #region Shape
        private void CreateNodeShape(IWorkspace workspace, QuickGraph.BidirectionalGraph<QuickGraph.Vertex, Edge<QuickGraph.Vertex>> graph)
        {

            IFeatureClass vertex = ArcManager.getShapeFeatureClass(workspace, "vertex");
            try
            {
                int tname = vertex.FindField("TNAME");
                int id = vertex.FindField("ID");
                int objid = vertex.FindField("OBJID");
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    StartEditor(workspace);
                    IFeatureBuffer pFeatureBuffer = vertex.CreateFeatureBuffer();
                    IFeatureCursor pFeatureCursor = vertex.Insert(true);
                    IFeature pFeature = pFeatureBuffer as IFeature;

                    foreach (Vertex v in graph.Vertices)
                    {
                        pFeature.set_Value(tname, v.CLASSNAME.ToString());
                        pFeature.set_Value(id, v.Id.ToString());
                        pFeature.set_Value(objid, v.OBJECTID.ToString());
                        pFeature.Shape =(IPoint)EpaNetwork.xUtils.getGeometryVertexEdge(v);

                        pFeatureCursor.InsertFeature(pFeatureBuffer);
                    }

                    pFeatureCursor.Flush();
                    StopEditor(workspace, true);
                }

            }
            catch (Exception)
            {

                AbortEditor(workspace);
            }

        }

        private void CreateLinkShape(IWorkspace workspace, QuickGraph.BidirectionalGraph<QuickGraph.Vertex, Edge<QuickGraph.Vertex>> graph)
        {
            IFeatureClass edge = ArcManager.getShapeFeatureClass(workspace, "edge");
            try
            {
                int tname = edge.FindField("TNAME");
                int id = edge.FindField("ID");
                int objid = edge.FindField("OBJID");
                int leng = edge.FindField("LENG");
                int sid = edge.FindField("SID");
                int tid = edge.FindField("TID");
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    StartEditor(workspace);
                    IFeatureBuffer pFeatureBuffer = edge.CreateFeatureBuffer();
                    IFeatureCursor pFeatureCursor = edge.Insert(true);
                    IFeature pFeature = pFeatureBuffer as IFeature;

                    foreach (Edge<Vertex> e in graph.Edges)
                    {
                        pFeature.set_Value(tname, e.CLASSNAME.ToString());
                        pFeature.set_Value(id, e.Id.ToString());
                        pFeature.set_Value(objid, e.OBJECTID.ToString());
                        //pFeature.set_Value(leng, e.LENG.ToString());
                        pFeature.set_Value(sid, Convert.ToString(e.Source.Id));
                        pFeature.set_Value(tid, Convert.ToString(e.Target.Id));
                        pFeature.Shape = (IPolyline)EpaNetwork.xUtils.getGeometryVertexEdge(e);

                        pFeatureCursor.InsertFeature(pFeatureBuffer);
                    }

                    pFeatureCursor.Flush();
                    StopEditor(workspace, true);
                }
            }
            catch (Exception)
            {

                AbortEditor(workspace);
            }
        }

        /// <summary>
        /// ArcEditor StartEdit
        /// </summary>
        /// <param name="pWorkSpace"></param>
        /// <remarks></remarks>
        public bool StartEditor(IWorkspace workspace)
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.StartEditOperation();
                return true;
            }
            else
            {
                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();
                return false;
            }

        }

        /// <summary>
        /// ArcEditor StopEdit
        /// </summary>
        /// <param name="pWorkSpace"></param>
        /// <param name="pSave"></param>
        /// <remarks></remarks>
        public void StopEditor(IWorkspace workspace, bool pSave)
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(pSave);
            }

        }

        /// <summary>
        /// ArcEditor AbortEditor
        /// </summary>
        public void AbortEditor(IWorkspace workspace)
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.AbortEditOperation();
            }
        }

        #endregion Shape

    
        #endregion QuickGraph->Shape로 변환
    }

    public static class WC_Global
    {
        public static WC_CrisisTransfer.CrisisAnalysisOperator crisisAnalysisOp = new WC_CrisisTransfer.CrisisAnalysisOperator();
    }


    public class CrisisAnalysisOperator
    {
        private EAGL.Explore.MapExplorer _mapExplorer;
        private EpaSimulator.ISimulator _basemodel; //DB원본모델
        private EpaSimulator.ISimulator _workmodel;

        public CrisisAnalysisOperator() { }
        public CrisisAnalysisOperator(EpaSimulator.ISimulator model)
        {
            this._basemodel = model;
        }

        public EAGL.Explore.MapExplorer MapExplorer
        {
            get { return this._mapExplorer; }
            set { this._mapExplorer = value; }
        }

        public EpaSimulator.ISimulator WorkModel
        {
            get { return this._workmodel; }
            set { this._workmodel = value; }
        }

        public EpaSimulator.ISimulator BaseModel
        {
            get { return this._basemodel; }
            set
            {
                this._basemodel = value;
            }
        }

        public void Clear()
        {
            this._mapExplorer = null;
            this._basemodel = null;
            this._workmodel = null;
        }

        /// <summary>
        /// 기본(DB model)모델과 현재모델의 수압비교하여 단수되는 junction을 찾는다.
        /// 기본(DB model) 수압>0 junction, 현재모델 수압<=0 junction 에서 양쪽다 있는 junction은 단수junction으로 설정
        /// </summary>
        public Dictionary<string, EpaNetwork.xNode> GetMinusPressure()
        {
            return GetMinusPressure(this._workmodel);
        }

        public Dictionary<string, EpaNetwork.xNode> GetMinusPressure(EpaSimulator.ISimulator _model)
        {
            //Dictionary<EpaNetwork.xNode, float> bpressure = this._basemodel.Output.GetJuncValues(EpaNetwork.xGlobal.OUT_PRESSURE, 0);
            Dictionary<EpaNetwork.xNode, float> cpressure = _model.Output.GetJuncValues(EpaNetwork.xGlobal.OUT_PRESSURE, 0);
            var minusnode = (from c in cpressure
                             where c.Value <= -10000
                             select new { ID = c.Key.Id, NODE = c.Key }).ToDictionary(key => key.ID, key => key.NODE);

            return minusnode;
        }

        private List<EpaNetwork.xJunc> tempJunctionList = new List<EpaNetwork.xJunc>();
        /// <summary>
        /// 임시 junc, Pipe 객체 생성
        /// </summary>
        public void CreateTempNodeAndLink(EpaNetwork.xLink link)
        {
            EpaNetwork.xJunc junc = link.Target as EpaNetwork.xJunc != null ? link.Target as EpaNetwork.xJunc : link.Source as EpaNetwork.xJunc;
            if (junc.BaseDemand == 0)
            {
                junc.BaseDemand = 1;
                this.tempJunctionList.Add(junc);
            }

            /////임시로 junc, pipe을 생성하여 수리해석
            //EpaNetwork.xNode node = link.Target as EpaNetwork.xNode;
            //string id = string.Format("__{0}__", node.Id);

            //EpaNetwork.xJunc junc = this._workmodel.NETWORK.FindNode(id) as EpaNetwork.xJunc;
            //EpaNetwork.xPipe pipe = this._workmodel.NETWORK.FindLink(id) as EpaNetwork.xPipe;
            //if (junc == null)
            //{
            //    junc = new EpaNetwork.xJunc(node.X + 1, node.Y + 1);
            //    junc.Id = id;
            //    junc.CLASSNAME = "ADDIN"; junc.BaseDemand = 1; junc.Elevation = ((EpaNetwork.xJunc)node).Elevation;
            //    this._workmodel.NETWORK.AddNode(junc);
            //}
            //if (pipe == null && junc != null)
            //{
            //    pipe = new EpaNetwork.xPipe(node, junc);
            //    pipe.Id = id;
            //    pipe.Length = "1"; pipe.CLASSNAME = "ADDIN";
            //    pipe.Diameter = ((EpaNetwork.xPipe)link).Diameter;
            //    this._workmodel.NETWORK.AddLink(pipe);    
            //}

        }

        /// <summary>
        /// 임시로 생성된 junc, pipe객체 삭제
        /// </summary>
        public void DeleteTempNodeAndLink()
        {
            foreach (var junc in this.tempJunctionList)
            {
                junc.BaseDemand = 0;
            }
            //this.DeleteTempNodeAndLink(this._workmodel);
        }

        public void DeleteTempNodeAndLink(EpaSimulator.ISimulator _model)
        {
            var nodes = (from b in _model.NETWORK.Graph.Vertices
                         from c in this.tempJunctionList
                         where b.Id == c.Id
                         select b);
            foreach (var junc in nodes)
            {
                ((EpaNetwork.xJunc)junc).BaseDemand = 0;
            }
            this.tempJunctionList.Clear();
            //var nodes = _model.NETWORK.Graph.Vertices.Where(entry => !string.IsNullOrEmpty(entry.CLASSNAME) &&
            //                                                entry.CLASSNAME.Equals("ADDIN"))
            //                                         .Select(entry => entry).ToList();
            //foreach (var node in nodes)
            //{
            //    _model.NETWORK.Graph.RemoveVertex(node);
            //}
            //_model.NETWORK.Graph.ResetVertexInOutEdges();
            //_model.NETWORK.ReSetNodeAndLink();
        }


        /// <summary>
        /// 기본(DB model)모델과 현재모델의 수압비교하여 단수되는 junction을 찾는다.
        /// 기본(DB model) 수압>0 junction, 현재모델 수압<=0 junction 에서 양쪽다 있는 junction은 단수junction으로 설정
        /// 사업장너머의 관로확인후 수리해석재실행(tags.value13 저장된 PIPE객체번호 closed)
        /// </summary>
        /// <param name="closelinks">차단 xPipe목록</param>
        /// <returns></returns>
        public Dictionary<string, EpaNetwork.xNode> BatchMinusPressure()
        {
            ///임시 junc, pipe객체 생성
            int err = this._workmodel.RunAnalysis();
            if (err != 0)
            {
                throw new Exception(EpaNetwork.xGlobal.ErrorCodes[err]);
            }
            ///해석결과 부압인 노드검색
            var minus_nodes = this.GetMinusPressure();

            /////사업장너머의 관로 확인 => minus_nodes : tags (Value13)
            //var crosspipe = minus_nodes.Values.Where(entry => !string.IsNullOrEmpty(entry.getTagstoken(':', 12).Trim()))
            //                           .Select(entry => entry.getTagstoken(':', 12));

            //bool flag = false;
            /////사업장너머의 관로가 있으면, 재해석실행
            //foreach (var ids in crosspipe)
            //{
            //    if (string.IsNullOrEmpty(ids)) continue;
            //    string[] split = ids.Split('|');
            //    foreach (var id in split)
            //    {
            //        EpaNetwork.xPipe p = this._workmodel.NETWORK.FindLink(id) as EpaNetwork.xPipe;
            //        if (p != null && !p.Status.Equals("CLOSED"))
            //        {
            //            p.Status = "CLOSED";
            //            CreateTempNodeAndLink(p);
            //            flag = true;
            //        }
            //    }
            //}

            //if (flag)
            //{
            //    return BatchMinusPressure();
            //}
            return minus_nodes;
        }

        /// <summary>
        /// 단수지역분석결과를 지도화면에 렌더링한다.
        /// </summary>
        private void DisplayClosingRenderer(List<string> FlowmeterList, ref Dictionary<string, EpaNetwork.xLink> minus_fcv)
        {
            #region 단수구역지도 렌더링
            IGraphicsContainer pGraphicsContainer = (IGraphicsContainer)this._mapExplorer.Map;
            ///pGraphicsContainer.DeleteAllElements();
            WC_Common.WC_FunctionManager.removeGraphics("유량계");
            WC_Common.WC_FunctionManager.removeGraphics("FCV");
            #region 유량계를 표시할때
            if (FlowmeterList.Count > 0)
            {
                ILayer layer = this._mapExplorer.GetLayer("유량계");
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    ICursor cursor = ArcManager.GetCursor(layer, string.Format("FTR_IDN IN ({0})", string.Join(",", FlowmeterList.ToArray())));
                    comReleaser.ManageLifetime(cursor);
                    List<IGeometry> geoms = new List<IGeometry>();
                    IFeature feature = cursor.NextRow() as IFeature;
                    while (feature != null)
                    {
                        geoms.Add(feature.ShapeCopy);
                        feature = cursor.NextRow() as IFeature;
                    }
                    this.DisplayMapRender(pGraphicsContainer, geoms, "유량계");
                }
            }
            #endregion 유량계를 표시할때

            #region FCV를 표시할때
            if (minus_fcv.Count > 0)
            {
                ILayer layer = this._mapExplorer.GetLayer("VALVE");
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    ICursor cursor = ArcManager.GetCursor(layer, string.Format("ID IN ({0})", string.Join(",", minus_fcv.Keys.ToArray())));
                    comReleaser.ManageLifetime(cursor);
                    List<IGeometry> geoms = new List<IGeometry>();
                    IFeature feature = cursor.NextRow() as IFeature;
                    while (feature != null)
                    {
                        geoms.Add(feature.ShapeCopy);
                        feature = cursor.NextRow() as IFeature;
                    }
                    this.DisplayMapRender(pGraphicsContainer, geoms, "FCV");
                }
            }
            #endregion FCV를 표시할때
            pGraphicsContainer.DeleteAllElements();
            string[] type = { "사고지점", "제수밸브", "이토밸브", "상수관로", "WTL_PIPE_LS", "급수관로", "WTL_SPLY_LS", "유량계", "FCV" };
            for (int i = 0; i < type.Length; i++)
            {
                WC_Common.WC_FunctionManager.displayGraphics(pGraphicsContainer, type[i]);
            }
            #endregion 단수구역지도 렌더링
        }

        private void DisplayMapRender(IGraphicsContainer pGraphicsContainer, List<IGeometry> geoms, string gbn)
        {
            foreach (IGeometry geom in geoms)
                Add_GraphicsDic(geom, gbn);
        }

        private void Add_GraphicsDic(IGeometry geometry, string type)
        {
            IElementCollection elementCollection;
            if (WC_Common.WC_VariableManager.m_graphicsDic.ContainsKey(type))
                elementCollection = WC_Common.WC_VariableManager.m_graphicsDic[type];
            else
            {
                elementCollection = new ElementCollectionClass();
                WC_Common.WC_VariableManager.m_graphicsDic.Add(type, elementCollection);
            }
            IColor pColor = null;
            switch (type)
            {
                case "제수밸브":
                    pColor = ArcManager.GetColor(System.Drawing.Color.Blue);
                    break;
                case "이토밸브":
                    pColor = ArcManager.GetColor(System.Drawing.Color.BlueViolet);
                    break;
                case "상수관로":
                case "WTL_PIPE_LS":
                    pColor = ArcManager.GetColor(System.Drawing.Color.Red);
                    break;
                case "급수관로":
                case "WTL_SPLY_LS":
                    pColor = ArcManager.GetColor(System.Drawing.Color.Violet);
                    break;
                case "유량계":
                case "FCV":
                    pColor = ArcManager.GetColor(System.Drawing.Color.Red);
                    break;
                default:
                    return;
            }

            ESRI.ArcGIS.Display.ISimpleMarkerSymbol pMarkerSymbol = default(ESRI.ArcGIS.Display.ISimpleMarkerSymbol);
            ESRI.ArcGIS.Display.ISimpleLineSymbol pLineSymbol = default(ESRI.ArcGIS.Display.ISimpleLineSymbol);
            ESRI.ArcGIS.Display.ISimpleFillSymbol pFillSymbol = default(ESRI.ArcGIS.Display.ISimpleFillSymbol);
            ESRI.ArcGIS.Display.ISymbol pSymbol = null;

            IElement element = null;
            switch (geometry.GeometryType)
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    pMarkerSymbol = new ESRI.ArcGIS.Display.SimpleMarkerSymbolClass();
                    pMarkerSymbol.Style = ESRI.ArcGIS.Display.esriSimpleMarkerStyle.esriSMSCircle;
                    pMarkerSymbol.Color = pColor;
                    pMarkerSymbol.OutlineColor = pColor;
                    pMarkerSymbol.Size = 11;

                    IMarkerElement markerElement = new MarkerElementClass();
                    markerElement.Symbol = pMarkerSymbol;
                    element = (IElement)markerElement;
                    element.Geometry = geometry;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryLine:
                    pLineSymbol = new ESRI.ArcGIS.Display.SimpleLineSymbolClass();
                    pLineSymbol.Width = 3;
                    pLineSymbol.Style = esriSimpleLineStyle.esriSLSSolid;
                    pLineSymbol.Color = pColor;

                    ILineElement lineElement = new LineElementClass();
                    lineElement.Symbol = pLineSymbol;
                    element = (IElement)lineElement; // Explicit Cast
                    element.Geometry = geometry;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    pFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbolClass();
                    pFillSymbol.Outline = null;
                    pFillSymbol.Color = pColor;
                    pFillSymbol.Style = esriSimpleFillStyle.esriSFSCross;
                    pSymbol = pFillSymbol as ISymbol;

                    IFillShapeElement polygonElement = new PolygonElementClass();
                    polygonElement.Symbol = pFillSymbol;
                    element = (IElement)polygonElement; // Explicit Cast
                    element.Geometry = geometry;
                    break;
            }

            if (!(element == null))
            {
                elementCollection.Add(element, 0);
            }
        }

        /// <summary>
        /// 단수되는 FCV 밸브를 검색한다.
        /// setting > flow , type = FCV, status != closed 인 밸브검색 -> 단수구역에 포함
        /// </summary>
        public List<string> GetClosingFcvValve(List<string> flowmeterlist)
        {
            EpaSimulator.ISimulator tmpModel = this._workmodel.Clone();

            ///임시생성된 junction, pipe 객체 삭제->모델에서 부압이 있을경우는 FCV의 해석결과가 다르게 나올수 있으므로 삭제
            this.DeleteTempNodeAndLink(tmpModel);
            int err = tmpModel.RunAnalysis();
            if (err != 0)
            {
                throw new Exception(EpaNetwork.xGlobal.ErrorCodes[err]);
            }
            Dictionary<EpaNetwork.xLink, float> linkflow = tmpModel.Output.GetLinkValues2(EpaNetwork.xGlobal.OUT_FLOW, 0);
            ///Link - flow 해석결과에서 setting > flow 인 FCV 밸브만 검출
            var minus_fcv = linkflow.Where(entry => entry.Key is EpaNetwork.xValve &&
                                                    ((EpaNetwork.xValve)entry.Key).Vtype.Equals("FCV") &&
                                                   !(((EpaNetwork.xValve)entry.Key).Status.ToUpper().Equals("CLOSED")) &&
                                                    EpaNetwork.xGlobal.ToSingle(((EpaNetwork.xValve)entry.Key).Setting) > entry.Value)
                                        .Select(entry => entry).ToDictionary(key => string.Format("'{0}'", key.Key.Id), key => key.Key);

            var minus_flowmeter = (from b in tmpModel.NETWORK.Nodes
                                   from c in minus_fcv
                                   where b.Value is EpaNetwork.xJunc &&
                                         !string.IsNullOrEmpty(b.Value.getTagstoken(':', 11).Trim()) &&
                                         b.Value.getTagstoken(':', 11).Trim().Split('|').Contains(c.Value.Id) &&
                                         !string.IsNullOrEmpty(b.Value.getTagstoken(':', 0).Trim())
                                   select new { IDN = string.Format("'{0}'", b.Value.getTagstoken(':', 0)) }).Distinct();

            List<string> FlowmeterList = flowmeterlist.Union(minus_flowmeter.Select(entry => entry.IDN)).ToList();
            this.DisplayClosingRenderer(FlowmeterList, ref minus_fcv);

            return FlowmeterList;
        }
        /// <summary>
        /// (Pipe.status = Closed)으로 설정된 PIPE전체를 비상공급관으로 정의한다.
        /// </summary>
        private Dictionary<string, EpaNetwork.xLink> GetAlternateValve()
        {
            var altvalve = this._basemodel.NETWORK.Links.Where(entry => entry.Value is EpaNetwork.xPipe &&
                                ((EpaNetwork.xPipe)entry.Value).Status.ToUpper().Equals("CLOSED"))
                                .Select(entry => entry).ToDictionary(key => key.Key, key => key.Value);
            return altvalve;
        }

        /// <summary>
        /// 기본(DB model)모델과 현재모델의 수압비교하여 단수되는 junction을 찾는다.
        /// 기본(DB model) 수압>0 junction, 현재모델 수압<=0 junction 에서 양쪽다 있는 junction은 단수junction으로 설정
        /// (Valve.type = TCV and Valve.status = CLOSED)으로 설정된 밸브를 비상공급관으로 정의한다.
        /// valve.souce == junction || valve.target == junction
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, EpaNetwork.xLink> GetAlternateValve2()
        {
            var minusnode = this.GetMinusPressure();
            var valve = this.GetAlternateValve();  ///원본 모델에서 Closed된 Link객체
            var temp = (from b in minusnode
                        from c in valve
                        where c.Value.Source.Id.Equals(b.Value.Id) || c.Value.Target.Id.Equals(b.Value.Id)
                        select new { ID = c.Key, NODE = c.Value }).Distinct().ToDictionary(key => key.ID, key => key.NODE);

            //basemodel-Link를 workmodel의 Link로 치환
            var altvalve = (from b in temp
                            from c in this._workmodel.NETWORK.Links
                            where b.Key.Equals(c.Key)
                            select new { ID = c.Key, NODE = c.Value }).Distinct().ToDictionary(key => key.ID, key => key.NODE);
            return altvalve;
        }
    }
}
