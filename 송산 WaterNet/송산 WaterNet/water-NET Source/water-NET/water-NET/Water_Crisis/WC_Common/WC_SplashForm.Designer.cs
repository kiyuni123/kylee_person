﻿namespace WaterNet.WC_Common
{
    partial class WC_SplashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_lblMsg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_lblMsg
            // 
            this.m_lblMsg.AutoSize = true;
            this.m_lblMsg.BackColor = System.Drawing.Color.Transparent;
            this.m_lblMsg.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.m_lblMsg.ForeColor = System.Drawing.Color.Red;
            this.m_lblMsg.Location = new System.Drawing.Point(12, 21);
            this.m_lblMsg.Name = "m_lblMsg";
            this.m_lblMsg.Size = new System.Drawing.Size(50, 19);
            this.m_lblMsg.TabIndex = 4;
            this.m_lblMsg.Text = "label1";
            this.m_lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.m_lblMsg.UseWaitCursor = true;
            // 
            // WC_SplashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 60);
            this.Controls.Add(this.m_lblMsg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WC_SplashForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "WC_SplashForm";
            this.TransparencyKey = System.Drawing.SystemColors.Control;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblMsg;
    }
}