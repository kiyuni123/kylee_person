﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
namespace WaterNet.WC_Common
{
    /// <summary>
    /// Project ID : WN_WC_A00
    /// Project Explain : 위기관리에서 공통으로 사용
    /// Project Developer : 전병록
    /// Project Create Date : 2010.10.13
    /// Class Explain : 위기관리에서 공통으로 사용하는 공통함수 Class
    /// </summary>
    public class WC_FunctionManager
    {
        public static void displayGraphics(IGraphicsContainer graphicsContainer, string type)
        {
            if (!WC_Common.WC_VariableManager.m_graphicsDic.ContainsKey(type)) return;
            IElementCollection elementCollection = WC_Common.WC_VariableManager.m_graphicsDic[type];
            graphicsContainer.AddElements(elementCollection, -1);
        }

        public static void removeGraphics(string type)
        {
            if (!WC_Common.WC_VariableManager.m_graphicsDic.ContainsKey(type)) return;
            WC_Common.WC_VariableManager.m_graphicsDic.Remove(type);
        }

        /// <summary>
        /// 숫자를 Double형으로 반환
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double convertToDouble(object value)
        {
            if (Convert.IsDBNull(value)) return 0.0;
            if (value == null) return 0.0;

            return Convert.ToDouble(value);
        }

        /// <summary>
        /// IFeatureCursor를 Parse하여 DataTable로 반환
        /// </summary>
        /// <param name="pCursor"></param>
        /// <returns></returns>
        public static DataTable convertCursorToDataTable(IFeatureCursor pCursor)
        {
            return convertCursorToDataTable(pCursor as ICursor);
        }

        /// <summary>
        /// IFeatureCursor를 Parse하여 DataTable로 반환
        /// </summary>
        /// <param name="pCursor"></param>
        /// <returns></returns>
        public static DataTable convertCursorToDataTable(ICursor pCursor)
        {
            DataTable pDataTable = new DataTable();
            
            try
            {
                IRow pRow = pCursor.NextRow();

                if (pRow != null)
                {
                    for (int i = 0; i < pRow.Fields.FieldCount; i++)
                    {
                        pDataTable.Columns.Add(pRow.Fields.get_Field(i).Name);
                    }

                    while (pRow != null)
                    {
                        DataRow pDataRow = pDataTable.NewRow();
                        for (int j = 0; j < pCursor.Fields.FieldCount; j++)
                            pDataRow[j] = pRow.get_Value(j);
                        pDataTable.Rows.Add(pDataRow);
                        pRow = pCursor.NextRow();
                    }
                }
            }
            catch (Exception ex)
            {   
                throw ex;
            }

            return pDataTable;
        }

        #region 시간을 문자열로 반환
        /// <summary>
        /// 현재시간을 반환한다.
        /// </summary>
        /// <returns></returns>
        public static string GetNowDateTimeToString()
        {
            return GetNowDateTimeToString(DateTime.Now);
        }

        public static string GetNowDateTimeToString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int year = oDateTime.Year;
            int month = oDateTime.Month;
            int day = oDateTime.Day;
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = year.ToString();
            nowtime += month.ToString().PadLeft(2, '0');
            nowtime += day.ToString().PadLeft(2, '0');
            nowtime += hour.ToString().PadLeft(2, '0');
            nowtime += min.ToString().PadLeft(2, '0');
            nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }

        public static string GetNowDateTimeToFormatString()
        {
            return GetNowDateTimeToFormatString(DateTime.Now);
        }

        public static string GetNowDateTimeToFormatString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int year = oDateTime.Year;
            int month = oDateTime.Month;
            int day = oDateTime.Day;
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = year.ToString() + "-";
            nowtime += month.ToString().PadLeft(2, '0') + "-";
            nowtime += day.ToString().PadLeft(2, '0') + " ";
            nowtime += hour.ToString().PadLeft(2, '0') + ":";
            nowtime += min.ToString().PadLeft(2, '0') + ":";
            nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }

        public static string GetNowTimeToString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = hour.ToString().PadLeft(2, '0');
            nowtime += min.ToString().PadLeft(2, '0');
            nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }

        public static string GetNowTimeToString()
        {
            return GetNowTimeToString(DateTime.Now);
        }

        public static string GetNowTimeToFormatString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = hour.ToString().PadLeft(2, '0') + ":";
            nowtime += min.ToString().PadLeft(2, '0') + ":";
            nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }

        public static string GetNowTimeToFormatString()
        {
            return GetNowTimeToFormatString(DateTime.Now);
        }

        /// <summary>
        /// 날짜 유효성 체크
        /// yyyy-mm-dd
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static bool ValidateCheckDate(string date)
        {
            if (date.Length != 10) return false;
            
            try
            {
                string[] dateParts = date.Split('-');

                DateTime vDate = new DateTime(Convert.ToInt32(dateParts[0]),
                                               Convert.ToInt32(dateParts[1]),
                                               Convert.ToInt32(dateParts[2]));
                return true;

            }
            catch (Exception)
            {   
                return false;
            }
        }

        /// <summary>
        /// 시간 유효성 체크
        /// hh:mm
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static bool ValidateCheckTime(string time)
        {
            if (time.Length != 5) return false;

            //Regex checktime = new Regex(@"^(20|21|22|23|[01]d|d)(([:][0-5]d){1,2})$");
            Regex checktime = new Regex(@"^(?:0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");

            return checktime.IsMatch(time);
        }

        #endregion 시간을 문자열로 반환

        /// <summary>
        /// 해석모델 Shape의 Position Parser
        /// position : 상수관로|8111||SAA004|SA001|BZ003|9
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public static Hashtable getParserPositionString(string position)
        {   
            Hashtable ohashtable = new Hashtable();
            if (string.IsNullOrEmpty(position)) return null;

            string[] Separator = {"[||]"};
            //szPairs = 상수관로|8111 , SAA004|SA001|BZ003|9
            string[] szPairs = position.Split(Separator, StringSplitOptions.None);
            if (szPairs == null) return null;

            string[] szTemp;
            szTemp = szPairs[0].Split('|');
            ohashtable.Add("LAYER", szTemp[0]);
            ohashtable.Add("OID", szTemp[1]);
            szTemp = szPairs[1].Split('|');
            ohashtable.Add("SAA", szTemp[0]);
            ohashtable.Add("CDE", szTemp[1]);
            ohashtable.Add("BLOCK", szTemp[2]);
            ohashtable.Add("BLOCK_OID", szTemp[3]);

            return ohashtable;
        }

        /// <summary>
        /// 새로운 Selected 객체 반환
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pGeometry"></param>
        /// <param name="eSpatialRel"></param>
        /// <param name="sWhereClause"></param>
        /// <returns></returns>
        public static IFeatureSelection FeatureSelect_New(ILayer pLayer, IGeometry pGeometry, esriSpatialRelEnum eSpatialRel, string sWhereClause)
        {
            IFeatureSelection pFeatureSelection = default(IFeatureSelection);

            IColor pColor = null;
            ESRI.ArcGIS.Display.ISimpleMarkerSymbol pMarkerSymbol = default(ESRI.ArcGIS.Display.ISimpleMarkerSymbol);
            ESRI.ArcGIS.Display.ISimpleLineSymbol pLineSymbol = default(ESRI.ArcGIS.Display.ISimpleLineSymbol);
            ESRI.ArcGIS.Display.ISimpleFillSymbol pFillSymbol = default(ESRI.ArcGIS.Display.ISimpleFillSymbol);
            ESRI.ArcGIS.Display.ISymbol pSymbol = null;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)pLayer;
            if (pFeatureLayer.FeatureClass.AliasName.Equals("밸브"))
                pColor = ArcManager.GetColor(47, 111, 255); //파란색
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("PIPE"))
                pColor = ArcManager.GetColor(239, 182, 0); //노란색
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("VALVE"))
                pColor = ArcManager.GetColor(239, 182, 0); //노란색
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("JUNCTION"))
                pColor = ArcManager.GetColor(255, 0, 0); //빨강색   
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("RESERVOIR"))
                pColor = ArcManager.GetColor(255, 0, 0); //빨강색 
            else
                pColor = ArcManager.GetRandomColor;

            switch (pFeatureLayer.FeatureClass.ShapeType)
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    pMarkerSymbol = new ESRI.ArcGIS.Display.SimpleMarkerSymbol();
                    pMarkerSymbol.Style = ESRI.ArcGIS.Display.esriSimpleMarkerStyle.esriSMSCircle;
                    if (pFeatureLayer.FeatureClass.AliasName.Equals("밸브"))
                    {
                        pMarkerSymbol.Size = (double)20.0;
                    }
                    else if (pFeatureLayer.FeatureClass.AliasName.Equals("JUNCTION"))
                    {
                        pMarkerSymbol.Size = (double)9.0;
                    }
                    else if (pFeatureLayer.FeatureClass.AliasName.Equals("RESERVOIR"))
                    {
                        pMarkerSymbol.Size = (double)10.0;
                    }
                    pMarkerSymbol.Color = pColor;
                    pSymbol = pMarkerSymbol as ISymbol;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryLine:
                    pLineSymbol = new ESRI.ArcGIS.Display.SimpleLineSymbol();
                    pLineSymbol.Width = 3;
                    pLineSymbol.Style = esriSimpleLineStyle.esriSLSDashDot;
                    pLineSymbol.Color = pColor;
                    pSymbol = pLineSymbol as ISymbol;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    pFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbol();
                    pFillSymbol.Outline = null;
                    pFillSymbol.Color = pColor;
                    pFillSymbol.Style = esriSimpleFillStyle.esriSFSCross;
                    pSymbol = pFillSymbol as ISymbol;
                    break;
            }


            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.Geometry = pGeometry;
            pSpatialFilter.GeometryField = pFeatureLayer.FeatureClass.ShapeFieldName;
            pSpatialFilter.SpatialRel = eSpatialRel;
            pSpatialFilter.WhereClause = sWhereClause;

            pFeatureSelection = (IFeatureSelection)pLayer;
            pFeatureSelection.Clear();

            pFeatureSelection.SetSelectionSymbol = true;
            pFeatureSelection.SelectionSymbol = pSymbol;
            pFeatureSelection.SelectFeatures((IQueryFilter)pSpatialFilter, esriSelectionResultEnum.esriSelectionResultNew, false);

            return pFeatureSelection;
        }

        /// <summary>
        ///  새로운 Selected 객체 반환 - None Geometry
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pGeometry"></param>
        /// <param name="eSpatialRel"></param>
        /// <param name="sWhereClause"></param>
        /// <returns></returns>
        public static IFeatureSelection FeatureSelect_New(ILayer pLayer, string sWhereClause)
        {
            IFeatureSelection pFeatureSelection = default(IFeatureSelection);

            IColor pColor = null;
            ESRI.ArcGIS.Display.ISimpleMarkerSymbol pMarkerSymbol = default(ESRI.ArcGIS.Display.ISimpleMarkerSymbol);
            ESRI.ArcGIS.Display.ISimpleLineSymbol pLineSymbol = default(ESRI.ArcGIS.Display.ISimpleLineSymbol);
            ESRI.ArcGIS.Display.ISimpleFillSymbol pFillSymbol = default(ESRI.ArcGIS.Display.ISimpleFillSymbol);
            ESRI.ArcGIS.Display.ISymbol pSymbol = null;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)pLayer;
            if (pFeatureLayer.FeatureClass.AliasName.Equals("밸브"))
                pColor = ArcManager.GetColor(47, 111, 255); //파란색
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("PIPE"))
                pColor = ArcManager.GetColor(239, 182, 0); //노란색
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("VALVE"))
                pColor = ArcManager.GetColor(239, 182, 0); //노란색
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("JUNCTION"))
                pColor = ArcManager.GetColor(255, 0, 0); //빨강색                
            else
                pColor = ArcManager.GetRandomColor;

            switch (pFeatureLayer.FeatureClass.ShapeType)
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    pMarkerSymbol = new ESRI.ArcGIS.Display.SimpleMarkerSymbol();
                    pMarkerSymbol.Style = ESRI.ArcGIS.Display.esriSimpleMarkerStyle.esriSMSCircle;
                    if (pFeatureLayer.FeatureClass.AliasName.Equals("밸브"))
                    {
                        pMarkerSymbol.Size = (double)20.0;
                    }
                    else if (pFeatureLayer.FeatureClass.AliasName.Equals("JUNCTION"))
                    {
                        pMarkerSymbol.Size = (double)9.0;
                    }
                    pMarkerSymbol.Color = pColor;
                    pSymbol = pMarkerSymbol as ISymbol;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryLine:
                    pLineSymbol = new ESRI.ArcGIS.Display.SimpleLineSymbol();
                    pLineSymbol.Width = 3;
                    pLineSymbol.Style = esriSimpleLineStyle.esriSLSDashDot;
                    pLineSymbol.Color = pColor;
                    pSymbol = pLineSymbol as ISymbol;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    pFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbol();
                    pFillSymbol.Outline = null;
                    pFillSymbol.Color = pColor;
                    pFillSymbol.Style = esriSimpleFillStyle.esriSFSCross;
                    pSymbol = pFillSymbol as ISymbol;
                    break;
            }

            pFeatureSelection = (IFeatureSelection)pLayer;
            pFeatureSelection.Clear();

            IQueryFilter pQueryFilter = new QueryFilterClass();
            pQueryFilter.WhereClause = sWhereClause;
            pFeatureSelection.SetSelectionSymbol = true;
            pFeatureSelection.SelectionSymbol = pSymbol;
            if (string.IsNullOrEmpty(sWhereClause)) //단수되는 지점이 없는경우 선택해제를 위한
                pFeatureSelection.SelectFeatures((IQueryFilter)pQueryFilter, esriSelectionResultEnum.esriSelectionResultAnd, false);    
            else
                pFeatureSelection.SelectFeatures((IQueryFilter)pQueryFilter, esriSelectionResultEnum.esriSelectionResultNew, false);

            return pFeatureSelection;
        }

        /// <summary>
        /// Selected객체에 Feature객체를 추가등록한다.
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pFeature"></param>
        public static void FeatureSelect_Added(ILayer pLayer, IFeature pFeature)
        {
            IColor pColor = null; //초록색
            ESRI.ArcGIS.Display.ISimpleMarkerSymbol pMarkerSymbol = default(ESRI.ArcGIS.Display.ISimpleMarkerSymbol);
            ESRI.ArcGIS.Display.ISimpleLineSymbol pLineSymbol = default(ESRI.ArcGIS.Display.ISimpleLineSymbol);
            ESRI.ArcGIS.Display.ISimpleFillSymbol pFillSymbol = default(ESRI.ArcGIS.Display.ISimpleFillSymbol);
            ESRI.ArcGIS.Display.ISymbol pSymbol = null;

            IFeatureLayer pFeatureLayer = (IFeatureLayer)pLayer;

            if (pFeatureLayer.FeatureClass.AliasName.Equals("밸브") | pFeatureLayer.FeatureClass.AliasName.Equals("WTL_VALV_PS"))
                pColor = ArcManager.GetColor(47, 111, 255); //파란색
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("PIPE"))
                pColor = ArcManager.GetColor(239, 182, 0); //노란색
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("VALVE"))
                pColor = ArcManager.GetColor(239, 182, 0); //노란색
            else if (pFeatureLayer.FeatureClass.AliasName.Equals("JUNCTION"))
                pColor = ArcManager.GetColor(255, 0, 0); //빨강색                
            else
                pColor = ArcManager.GetRandomColor;

            switch (pFeatureLayer.FeatureClass.ShapeType)
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    pMarkerSymbol = new ESRI.ArcGIS.Display.SimpleMarkerSymbol();
                    pMarkerSymbol.Style = ESRI.ArcGIS.Display.esriSimpleMarkerStyle.esriSMSCircle;
                    if (pFeatureLayer.FeatureClass.AliasName.Equals("밸브") | pFeatureLayer.FeatureClass.AliasName.Equals("WTL_VALV_PS"))
                    {
                        pMarkerSymbol.Size = (double)9.0;
                    }
                    else if (pFeatureLayer.FeatureClass.AliasName.Equals("JUNCTION"))
                    {
                        pMarkerSymbol.Size = (double)9.0;
                    }
                    pMarkerSymbol.Color = pColor;
                    pSymbol = pMarkerSymbol as ISymbol;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryLine:
                    pLineSymbol = new ESRI.ArcGIS.Display.SimpleLineSymbol();
                    pLineSymbol.Width = 3;
                    pLineSymbol.Style = esriSimpleLineStyle.esriSLSDashDot;
                    pLineSymbol.Color = pColor;
                    pSymbol = pLineSymbol as ISymbol;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    pFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbol();
                    pFillSymbol.Outline = null;
                    pFillSymbol.Color = pColor;
                    pFillSymbol.Style = esriSimpleFillStyle.esriSFSCross;
                    pSymbol = pFillSymbol as ISymbol;
                    break;
            }

            IFeatureSelection pFeatureSelection = pFeatureLayer as IFeatureSelection;
            pFeatureSelection.SetSelectionSymbol = true;
            pFeatureSelection.SelectionSymbol = pSymbol;
            pFeatureSelection.Add(pFeature);

        }

        /// <summary>
        /// 해당 레이어에 선택된 객체의 ConvexHull범위를 반환한다.
        /// </summary>
        /// <param name="pLayer"></param>
        /// <returns></returns>
        public static IGeometry getGeometryConvexHull(IFeatureLayer pLayer)
        {
            ISelectionSet pSelectionSet = ArcManager.getSelectedFeatures(pLayer) as ISelectionSet;

            IEnumIDs enumIDS = pSelectionSet.IDs;
            int iD = enumIDS.Next();

            IFeature feature;
            IGeometryBag geomBag = new GeometryBagClass();
            IGeometryCollection geomCol = geomBag as IGeometryCollection;
            object missing = Type.Missing;
            IGeometry geom;


            if (pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryPoint || pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryMultipoint)
            {
                geomCol = new MultipointClass() as IGeometryCollection;
                while (iD != -1)
                {
                    feature = pLayer.FeatureClass.GetFeature(iD);
                    geom = feature.ShapeCopy;
                    geomCol.AddGeometry(geom, ref missing, ref missing);
                    iD = enumIDS.Next();
                }
            }
            else if (pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryPolygon)
            {
                geomCol = new PolygonClass() as IGeometryCollection;
                ISegmentCollection ring;
                ISegmentCollection segCol;
                while (iD != -1)
                {
                    ring = new RingClass();
                    feature = pLayer.FeatureClass.GetFeature(iD);
                    segCol = feature.ShapeCopy as ISegmentCollection;
                    for (int x = 0; x < segCol.SegmentCount - 1; x++)
                    {
                        ring.AddSegment(segCol.get_Segment(x), ref missing, ref missing);
                    }

                    IRing ring2 = ring as IRing;
                    ring2.Close();
                    geomCol.AddGeometry((IGeometry)ring2, ref missing, ref missing);
                    iD = enumIDS.Next();
                }

            }
            else if (pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryPolyline || pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryLine)
            {
                geomCol = new PolylineClass() as IGeometryCollection;
                ISegmentCollection path;
                ISegmentCollection segCol;
                while (iD != -1)
                {
                    path = new PathClass();
                    feature = pLayer.FeatureClass.GetFeature(iD);
                    segCol = feature.ShapeCopy as ISegmentCollection;
                    for (int x = 0; x < segCol.SegmentCount - 1; x++)
                    {
                        path.AddSegment(segCol.get_Segment(x), ref missing, ref missing);
                    }

                    geomCol.AddGeometry((IGeometry)path, ref missing, ref missing);
                    iD = enumIDS.Next();
                }
            }
            else return null;

            
            ITopologicalOperator topoOP = geomCol as ITopologicalOperator;
            if (!topoOP.IsSimple) topoOP.Simplify();
            IGeometry convexHull = topoOP.ConvexHull();

            return convexHull;
            
        }

        /// <summary>
        /// 해당 레이어에 선택된 객체의 ConvexHull범위를 반환한다.
        /// </summary>
        /// <param name="pLayer"></param>
        /// <returns></returns>
        public static IGeometry getGeometryConvexHull(IFeatureLayer pLayer, string sWhereClause)
        {
            IGeometryBag geomBag = new GeometryBagClass();
            IGeometryCollection geomCol = geomBag as IGeometryCollection;

            try
            {
                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureCursor pFeatureCursor = ArcManager.GetCursor(pLayer, sWhereClause) as IFeatureCursor;
                    //comReleaser.ManageLifetime(pFeatureCursor);

                    IFeature pFeature = null;
                    object missing = Type.Missing;
                    IGeometry geom;

                    if (pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryPoint || pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryMultipoint)
                    {
                        geomCol = new MultipointClass() as IGeometryCollection;

                        while ((pFeature = pFeatureCursor.NextFeature()) != null)
                        {
                            geom = pFeature.ShapeCopy;
                            geomCol.AddGeometry(geom, ref missing, ref missing);
                        }
                    }
                    else if (pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryPolygon)
                    {
                        geomCol = new PolygonClass() as IGeometryCollection;
                        ISegmentCollection ring;
                        ISegmentCollection segCol;

                        while ((pFeature = pFeatureCursor.NextFeature()) != null)
                        {
                            ring = new RingClass();

                            segCol = pFeature.ShapeCopy as ISegmentCollection;
                            for (int x = 0; x < segCol.SegmentCount - 1; x++)
                            {
                                ring.AddSegment(segCol.get_Segment(x), ref missing, ref missing);
                            }

                            IRing ring2 = ring as IRing;
                            ring2.Close();
                            geomCol.AddGeometry((IGeometry)ring2, ref missing, ref missing);
                        }
                    }
                    else if (pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryPolyline || pLayer.FeatureClass.ShapeType == esriGeometryType.esriGeometryLine)
                    {
                        geomCol = new PolylineClass() as IGeometryCollection;
                        ISegmentCollection path;
                        ISegmentCollection segCol;

                        while ((pFeature = pFeatureCursor.NextFeature()) != null)
                        {
                            path = new PathClass();

                            segCol = pFeature.ShapeCopy as ISegmentCollection;
                            for (int x = 0; x < segCol.SegmentCount - 1; x++)
                            {
                                path.AddSegment(segCol.get_Segment(x), ref missing, ref missing);
                            }

                            geomCol.AddGeometry((IGeometry)path, ref missing, ref missing);
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }



            ITopologicalOperator topoOP = geomCol as ITopologicalOperator;
            if (!topoOP.IsSimple) topoOP.Simplify();
            IGeometry convexHull = topoOP.ConvexHull();

            return convexHull;

        }

        ///// <summary>
        ///// 해석결과의 Junction 수압 <= 0 junction을 hashtable 반환
        ///// </summary>
        ///// <param name="result"></param>
        ///// <returns></returns>
        //public static Hashtable getResult_Junction_Analysis(Hashtable result)
        //{
        //    //해석결과를 담기위해
        //    Hashtable Data = new Hashtable();

        //    //----------------------------------------------------
        //    Hashtable resultByTime = new Hashtable();
        //    Hashtable resultByType = new Hashtable();
        //    ArrayList resultList = new ArrayList();
        //    Hashtable resultData = new Hashtable();


        //    foreach (string time in result.Keys)    ///해석시간
        //    {
        //        if (!time.Equals("24:00")) continue;
        //        resultByTime = result[time] as Hashtable;  //24:00데이터만

        //        foreach (string type in resultByTime.Keys)  //노드 or 링크
        //        {
        //            //if (!type.ToUpper().Equals("NODE")) continue;  //NODE 데이터만
        //            resultByType = resultByTime[type] as Hashtable;
        //            foreach (string subtype in resultByType.Keys)  //junction, reservior...
        //            {
        //                Console.WriteLine(subtype);

        //                //if (!subtype.ToUpper().Equals("JUNCTION")) continue;  //junction 데이터만
        //                resultList = resultByType[subtype] as ArrayList;  //해석항목ID(복수)에 해당하는 해석값

        //                for (int i = 0; i < resultList.Count; i++)
        //                {
        //                    resultData = resultList[i] as Hashtable;  //개개의 해석항목ID 해석값
        //                    if (Convert.ToDouble(resultData["EN_PRESSURE"]) <= 0.0)
        //                    {
        //                        Data.Add(Convert.ToString(resultData["NODE_ID"]), Convert.ToString(resultData["EN_PRESSURE"]));
        //                        //oDataRow = oDataTable.NewRow();
        //                        Console.WriteLine(type + " : " + subtype + " : " + Convert.ToString(resultData["NODE_ID"]) + " : " + Convert.ToString(resultData["EN_PRESSURE"]));
        //                        //oDataTable.Rows.Add(oDataRow);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return Data;
        //}

        public static ISelectionSet QueryByLayer(IMap map, IFeatureLayer byLayer, IFeatureLayer fromLayer, esriLayerSelectionMethod selMethod, esriSelectionResultEnum selType, bool UseSelected)
        {
            IQueryByLayer pQueryByLayer = new QueryByLayerClass();
            pQueryByLayer.BufferDistance = 0.1;
            pQueryByLayer.LayerSelectionMethod = selMethod;
            pQueryByLayer.ResultType = selType;
            pQueryByLayer.BufferUnits = esriUnits.esriMeters;
            pQueryByLayer.UseSelectedFeatures = UseSelected;

            pQueryByLayer.ByLayer = byLayer;  //Where Layer
            pQueryByLayer.FromLayer = fromLayer; //Select Layer

            ISelectionSet2 pResultSet = pQueryByLayer.Select() as ISelectionSet2;

            return pResultSet;
        }

        public static ISelectionSet QueryByLayer(IMap map, string byLayer, string fromLayer, esriLayerSelectionMethod selMethod, esriSelectionResultEnum selType, bool UseSelected)
        {
            IFeatureLayer ByLayer = ArcManager.GetMapLayer(map, byLayer) as IFeatureLayer; //Where Layer
            IFeatureLayer FromLayer = ArcManager.GetMapLayer(map, fromLayer) as IFeatureLayer; //Select Layer

            return QueryByLayer(map, ByLayer, FromLayer, selMethod, selType, UseSelected);
        }

        public static ISelectionSet QueryByLayer(IMap map, IFeatureLayer byLayer, IFeatureLayer fromLayer, esriLayerSelectionMethod selMethod, esriSelectionResultEnum selType)
        {
            IQueryByLayer pQueryByLayer = new QueryByLayerClass();
            pQueryByLayer.BufferDistance = 0.1;
            pQueryByLayer.LayerSelectionMethod = selMethod;
            pQueryByLayer.ResultType = selType;
            pQueryByLayer.BufferUnits = esriUnits.esriMeters;
            pQueryByLayer.UseSelectedFeatures = true;

            pQueryByLayer.ByLayer = byLayer;  //Where Layer
            pQueryByLayer.FromLayer = fromLayer; //Select Layer

            ISelectionSet2 pResultSet = pQueryByLayer.Select() as ISelectionSet2;

            return pResultSet;
        }

        public static ISelectionSet QueryByLayer(IMap map, string byLayer, string fromLayer, esriLayerSelectionMethod selMethod, esriSelectionResultEnum selType)
        {
            IFeatureLayer ByLayer = ArcManager.GetMapLayer(map, byLayer) as IFeatureLayer; //Where Layer
            IFeatureLayer FromLayer = ArcManager.GetMapLayer(map, fromLayer) as IFeatureLayer; //Select Layer

            return QueryByLayer(map, ByLayer, FromLayer, selMethod, selType);
        }

        public static ISelectionSet QueryByLayer(IMap map, IFeatureLayer byLayer, IFeatureLayer fromLayer)
        {
            IQueryByLayer pQueryByLayer = new QueryByLayerClass();
            pQueryByLayer.BufferDistance = 0.01;
            pQueryByLayer.LayerSelectionMethod = esriLayerSelectionMethod.esriLayerSelectIntersect;
            pQueryByLayer.ResultType = esriSelectionResultEnum.esriSelectionResultNew;
            pQueryByLayer.BufferUnits = esriUnits.esriMeters;
            pQueryByLayer.UseSelectedFeatures = true;

            pQueryByLayer.ByLayer = byLayer;  //Where Layer
            pQueryByLayer.FromLayer = fromLayer; //Select Layer

            ISelectionSet2 pResultSet = pQueryByLayer.Select() as ISelectionSet2;

            return pResultSet;
        }

        public static ISelectionSet QueryByLayer(ESRI.ArcGIS.Carto.IMap map, string byLayer, string fromLayer)
        {
            IFeatureLayer ByLayer = ArcManager.GetMapLayer(map, byLayer) as IFeatureLayer; //Where Layer
            IFeatureLayer FromLayer = ArcManager.GetMapLayer(map, fromLayer) as IFeatureLayer; //Select Layer
 
            return QueryByLayer(map, ByLayer, FromLayer);

        }

        /// <summary>
        /// ISelectionSet의 OID를 배열로 반환한다.
        /// </summary>
        /// <param name="oselection"></param>
        /// <returns></returns>
        public static ArrayList getOIDarray_Selectionset(ISelectionSet2 oSelection)
        {
            ArrayList oList = new ArrayList();
            IEnumIDs enumIDS = oSelection.IDs;
            int iD = -1;
            while ((iD = enumIDS.Next()) != -1)
            {
                oList.Add(iD);
            }
            return oList;
        }

        /// <summary>
        /// ISelectionSet의 OID를 List로 반환한다.
        /// </summary>
        /// <param name="oselection"></param>
        /// <returns></returns>
        public static System.Collections.Generic.List<int> getOIDList_Selectionset(ISelectionSet2 oSelection)
        {
            System.Collections.Generic.List<int> oList = new List<int>();
            IEnumIDs enumIDS = oSelection.IDs;
            int iD = -1;
            while ((iD = enumIDS.Next()) != -1)
            {
                oList.Add(iD);
            }
            return oList;
        }

        /// <summary>
        /// FeatureLayer 복사하기
        /// FID가 변경되므로, 기존의 FID를 저장하기 위해
        /// 원본 필드에 OFID 필드를 추가하여 복사한다.
        /// </summary>
        /// <param name="pWorkspace"></param>
        /// <param name="pLayer"></param>
        /// <returns></returns>
        public static ILayer Copy(IWorkspace pWorkspace, ILayer pLayer)
        {
            IFeatureLayer srcFeatureLayer = pLayer as IFeatureLayer;

            IFeatureLayer CopyLayer = new FeatureLayerClass();

            /////Create Field
            IFields fields = srcFeatureLayer.FeatureClass.Fields;

            #region OFID 필드 추가
            IFieldsEdit fieldsEdit = (IFieldsEdit)fields;
            IField field = new FieldClass();
            IFieldEdit fieldEdit = (IFieldEdit)field;
            fieldEdit.Name_2 = "OFID";
            fieldEdit.AliasName_2 = "OFID";
            fieldEdit.Type_2 = esriFieldType.esriFieldTypeInteger;

            fieldsEdit.AddField(field);
            #endregion OFID 필드 추가

            IFeatureWorkspace pFeatureWorkspace = pWorkspace as IFeatureWorkspace;

            // Use IFieldChecker to create a validated fields collection.  
            IFieldChecker fieldChecker = new FieldCheckerClass();  
            IEnumFieldError enumFieldError = null;  
            IFields validatedFields = null;
            fieldChecker.ValidateWorkspace = (IWorkspace)pFeatureWorkspace; 
            fieldChecker.Validate(fields, out enumFieldError, out validatedFields);

            ///Create FeatureClass
            IFeatureClass pFeatureClass = pFeatureWorkspace.CreateFeatureClass(pLayer.Name, validatedFields, srcFeatureLayer.FeatureClass.CLSID,
                                                                               srcFeatureLayer.FeatureClass.EXTCLSID, srcFeatureLayer.FeatureClass.FeatureType,
                                                                               srcFeatureLayer.FeatureClass.ShapeFieldName, "");
            ///Insert Feature


            // Start an edit session and edit operation.
            IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)pWorkspace;
            workspaceEdit.StartEditing(true);
            workspaceEdit.StartEditOperation();

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureClass srcFeatureClass = srcFeatureLayer.FeatureClass;
                IFeatureCursor srcFeatureCursor = srcFeatureClass.Search(null, true);
                IFeature srcFeature = null;
                comReleaser.ManageLifetime(srcFeatureCursor);

                IFeatureCursor featureCursor = pFeatureClass.Insert(true);
                comReleaser.ManageLifetime(featureCursor);
                IFeatureBuffer featureBuffer = pFeatureClass.CreateFeatureBuffer();
                comReleaser.ManageLifetime(featureBuffer);
                IFeature pFeature = featureBuffer as IFeature;

                while ((srcFeature = srcFeatureCursor.NextFeature()) != null)
                {
                    for (int i = 0; i < srcFeature.Fields.FieldCount; i++)
                    {
                        if (srcFeature.Fields.get_Field(i).Name.Equals("OFID")) continue;

                        switch (srcFeature.Fields.get_Field(i).Type)
                        {
                            case esriFieldType.esriFieldTypeOID:
                                pFeature.set_Value(pFeature.Fields.FindField("OFID"), srcFeature.OID);
                                break;
                            case esriFieldType.esriFieldTypeGeometry:
                                pFeature.Shape = srcFeature.Shape;
                                pFeature.Shape.SpatialReference = srcFeature.Shape.SpatialReference;
                                break;
                            default:
                                pFeature.set_Value(srcFeature.Fields.FindField(srcFeature.Fields.get_Field(i).Name), srcFeature.get_Value(i));
                                break;
                        }

                    }
                    featureCursor.InsertFeature(featureBuffer);
                }

                featureCursor.Flush();
            }

            workspaceEdit.StopEditOperation();
            workspaceEdit.StopEditing(true);

            CopyLayer.FeatureClass = pFeatureClass;
            return CopyLayer as ILayer;
        }

        public static void RemoveAllLayerInMemoryWorkspace(IWorkspace pWorkspace)
        {
            IEnumDataset pEnumDS = pWorkspace.get_Datasets(esriDatasetType.esriDTFeatureClass);
            pEnumDS.Reset();
            IDataset pDS = pEnumDS.Next();
            while (pDS != null)
            {
                if (pDS.CanDelete())         pDS.Delete();
                
                pDS = pEnumDS.Next();
            }

        }
    }


    public class ExcelUtils
    {
        /// <summary>

        /// Builds the workbook.

        /// </summary>

        /// <param name="fileName">Name of the file.</param>

        /// <param name="inputData">The input data.</param>

        /// <param name="overwriteContents"></param>

        /// <param name="clearHeader"></param>

        public void SaveWorkbook(string fileName, DataSet inputData, bool overwriteContents, bool clearHeader)
        {

            SpreadsheetDocument spreadsheetDocument;

            bool isNewFile = false;

            try
            {

                if (!System.IO.File.Exists(fileName))
                {

                    spreadsheetDocument = SpreadsheetDocument.Create(fileName, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook); //Create

                    isNewFile = true;

                }

                else
                {

                    spreadsheetDocument = SpreadsheetDocument.Open(fileName, true); //Opening exisiting

                    isNewFile = false;

                }

                if (true)//using (spreadsheetDocument)
                {

                    WorkbookPart workbookPart;

                    //Instantiates workbookpart

                    if (isNewFile)

                        workbookPart = spreadsheetDocument.AddWorkbookPart(); //If file is new file

                    else

                        workbookPart = spreadsheetDocument.WorkbookPart;

                    //Creates Workbook if workbook not existed(For New Excel File), Existing excel file this is not necessary

                    if (workbookPart.Workbook == null)

                        workbookPart.Workbook = new Workbook();

                    Sheets sheets;

                    //

                    if (isNewFile)

                        sheets = workbookPart.Workbook.AppendChild(new Sheets()); //Adding new sheets to the file, if new file

                    else

                        sheets = workbookPart.Workbook.Sheets; //Retrieving existing sheets from the file

                    FileVersion fileVersion = new FileVersion { ApplicationName = "Microsoft Office Excel" };

                    //  workbook.Append(fileVersion);

                    uint tableIndex = 0;

                    //converting the sheets collection to a list of <Sheet>

                    List<Sheet> sheetsList = workbookPart.Workbook.Descendants<Sheet>().ToList();

                    // If the InputDataSet having 1 or more tables, looping through and

                    // creates new sheet for each table and dumps the data to the sheet

                    // and saves the excel workbook.

                    foreach (DataTable inputDataTable in inputData.Tables)
                    {

                        bool hasSheetExists = false;

                        int sheetIndex = 0;

                        string relId = "";

                        Sheet sheet;

                        //Checking sheet exists in the excel file.

                        sheetIndex = sheetsList.FindIndex(c => c.Name == inputDataTable.TableName);

                        if (sheetIndex >= 0)
                        {

                            hasSheetExists = true;

                        }

                        WorksheetPart worksheetPart;

                        if (!hasSheetExists) //If a new sheet
                        {

                            worksheetPart = workbookPart.AddNewPart<WorksheetPart>();

                            relId = workbookPart.GetIdOfPart(worksheetPart);

                            sheet = new Sheet { Name = inputDataTable.TableName, SheetId = tableIndex + 1, Id = relId };

                        }

                        else // if sheet already exists
                        {

                            sheet = sheetsList[sheetIndex];

                            List<WorksheetPart> partList = workbookPart.WorksheetParts.ToList();

                            // Take the existing the sheet reference from the workbook.

                            WorksheetPart worksheetPart1 = (WorksheetPart)(workbookPart.GetPartById(sheet.Id));

                            worksheetPart = worksheetPart1;

                            //Retriving RelationID form the workbookPart

                            relId = workbookPart.GetIdOfPart(worksheetPart);

                            // partList.FindAll( c=> c.)

                            // worksheetPart = workbookPart.AddNewPart<WorksheetPart>();

                        }

                        SheetData sheetData = new SheetData();

                        Worksheet workSheet = worksheetPart.Worksheet;

                        int lastRowIndex = 0;

                        if (workSheet != null)
                        {

                            //Retrieving existing sheet data from the worksheet

                            sheetData = workSheet.GetFirstChild<SheetData>();

                            if (overwriteContents) //Clearing the contents of the Sheet, except the header
                            {

                                int endIndex = 1;

                                //if value true clear the existing header

                                if (clearHeader)

                                    endIndex = 0;

                                //Deleting content row by row, starting from the bottom.

                                for (int childIndex = sheetData.ChildElements.Count - 1; childIndex >= endIndex; childIndex--)
                                {

                                    sheetData.RemoveChild(sheetData.ChildElements[childIndex]);

                                }

                            }

                            //Getting all existing record rows.

                            IEnumerable<DocumentFormat.OpenXml.Spreadsheet.Row> rows = sheetData.Descendants<DocumentFormat.OpenXml.Spreadsheet.Row>();

                            //Considering the last row index as total row count. append the records to the last index onwards.

                            lastRowIndex = rows.Count();

                        }

                        else //Creating new worksheet
                        {

                            workSheet = new Worksheet(sheetData);

                            worksheetPart.Worksheet = workSheet;

                        }

                        //If Data Table is not empty

                        if (inputDataTable != null && inputDataTable.Rows.Count > 0)
                        {

                            //If Not Sheet already exists(Based on Table Name) -- creating column headers for the excel sheet

                            if (!hasSheetExists || lastRowIndex < 1)
                            {

                                //Creating columns..(INDX = 1 , Header)

                                DocumentFormat.OpenXml.Spreadsheet.Row headerRow = CreateContentHeader(1, inputDataTable.Columns);

                                sheetData.Append(headerRow);

                                lastRowIndex = 1;

                            }

                            //Last Row index

                            lastRowIndex++;

                            //Worksheet Data Row Number.. (INDX = 2 onwards data)

                            uint currDataRowIndex = (uint)lastRowIndex; //From this index on data will get appended.

                            //Creating Row Data

                            for (int iterRowIndex = 0; iterRowIndex < inputDataTable.Rows.Count; iterRowIndex++)
                            {

                                //Retrieving current DataRow from DataTable

                                DataRow currentInputRow = inputDataTable.Rows[iterRowIndex];

                                //Creating insertble row for the openxml.

                                DocumentFormat.OpenXml.Spreadsheet.Row contentRow = CreateContentRow(currDataRowIndex, currentInputRow,

                                                                  inputDataTable.Columns);

                                currDataRowIndex++;

                                //Appending to sheet data

                                sheetData.AppendChild(contentRow);

                            }

                        }

                        //new Worksheet(sheetData);

                        //Saving worksheet contents

                        worksheetPart.Worksheet.Save();

                        //If sheet new, then appending to sheets collection

                        if (!hasSheetExists)

                            sheets.AppendChild(sheet);

                        tableIndex++;

                    }

                    //Saving the complete workbook to disk

                    spreadsheetDocument.WorkbookPart.Workbook.Save();

                    spreadsheetDocument.Close();

                }

            }

            catch (Exception)
            {

                // spreadsheetDocument.Close();

                throw;

            }

        }

        /// <summary>

        /// Creates the content header.

        /// </summary>

        /// <param name="rowDataIndex">Index of the row data.</param>

        /// <param name="dataColumns">The data columns.</param>

        /// <returns></returns>

        private DocumentFormat.OpenXml.Spreadsheet.Row CreateContentHeader(UInt32 rowDataIndex, DataColumnCollection dataColumns)
        {

            DocumentFormat.OpenXml.Spreadsheet.Row resultRow = new DocumentFormat.OpenXml.Spreadsheet.Row { RowIndex = rowDataIndex };

            for (int iterColIndex = 0; iterColIndex < dataColumns.Count; iterColIndex++)
            {

                Cell cell1 = CreateHeaderCell(Convert.ToString(Convert.ToChar(65 + iterColIndex)), rowDataIndex, dataColumns[iterColIndex].ColumnName);  //CreateTextCell("A", rowDataIndex, Convert.ToString(dataRow[iterColIndex]));
                //Cell cell1 = CreateHeaderCell(dataColumns[iterColIndex].ColumnName, rowDataIndex, dataColumns[iterColIndex].ColumnName);  //CreateTextCell("A", rowDataIndex, Convert.ToString(dataRow[iterColIndex]));

                resultRow.Append(cell1);

            }

            return resultRow;

        }

        /// <summary>

        /// Creates the content row.

        /// </summary>

        /// <param name="rowDataIndex">The rowDataIndex.</param>

        /// <param name="dataRow">DataRow</param>

        /// <param name="dataColumns">DataColumnCollection</param>

        /// <returns></returns>

        private DocumentFormat.OpenXml.Spreadsheet.Row CreateContentRow(UInt32 rowDataIndex, DataRow dataRow, DataColumnCollection dataColumns)
        {

            DocumentFormat.OpenXml.Spreadsheet.Row resultRow = new DocumentFormat.OpenXml.Spreadsheet.Row { RowIndex = rowDataIndex };

            for (int iterColIndex = 0; iterColIndex < dataColumns.Count; iterColIndex++)
            {
                Cell cell1 = CreateContentCell(Convert.ToString(Convert.ToChar(65 + iterColIndex)), rowDataIndex, dataRow[iterColIndex]);
                //Cell cell1 = CreateContentCell(dataColumns[iterColIndex].ColumnName, rowDataIndex, dataRow[iterColIndex]);  //CreateTextCell("A", rowDataIndex, Convert.ToString(dataRow[iterColIndex]));

                resultRow.Append(cell1);
            }

            return resultRow;

        }

        private bool EnumTryParse<T>(string strType, out T result)
        {
            string strTypeFixed = strType.Replace(' ', '_');
            if (Enum.IsDefined(typeof(T), strTypeFixed))
            {
                result = (T)Enum.Parse(typeof(T), strTypeFixed, true);
                return true;
            }
            else
            {
                foreach (string value in Enum.GetNames(typeof(T)))
                {
                    if (value.Equals(strTypeFixed, StringComparison.OrdinalIgnoreCase))
                    {
                        result = (T)Enum.Parse(typeof(T), value);
                        return true;
                    }
                }
                result = default(T);
                return false;
            }
        }


        /// <summary>

        /// Creates the content cell.

        /// </summary>

        /// <param name="header">The header.</param>

        /// <param name="index">The rowDataIndex.</param>

        /// <param name="inputValue">The input value.</param>

        /// <returns></returns>

        private Cell CreateContentCell(string header, UInt32 index, object inputValue)
        {

            Cell resultCell = null;

            Type objectType = inputValue.GetType();

            TypeCode objectTypeCode;
            bool parseSuccess = EnumTryParse<TypeCode>(objectType.Name, out objectTypeCode);

            //bool parseSuccess =  Enum.TryParse(objectType.Name, true, out objectTypeCode);

            if (parseSuccess)
            {

                switch (objectTypeCode)
                {

                    // Number Fields

                    case TypeCode.UInt64:

                    case TypeCode.UInt32:

                    case TypeCode.UInt16:

                    case TypeCode.Int64:

                    case TypeCode.Int32:

                    case TypeCode.Int16:

                    case TypeCode.Double:

                    case TypeCode.Decimal:

                        resultCell = CreateNumberCell(header, index, inputValue);

                        break;

                    // Date Time Field

                    case TypeCode.DateTime:

                        resultCell = CreateDateCell(header, index, inputValue);

                        break;

                    // Boolean Field

                    case TypeCode.Boolean:

                        resultCell = CreateBooleanCell(header, index, inputValue);

                        break;

                    default:

                        resultCell = CreateTextCell(header, index, inputValue);

                        break;

                    //case TypeCode.

                }

            }

            else

                resultCell = CreateTextCell(header, index, inputValue);

            return resultCell;

        }

        /// <summary>

        /// Creates the header cell.

        /// </summary>

        /// <param name="header">The header.</param>

        /// <param name="index">The index.</param>

        /// <param name="text">The text.</param>

        /// <returns></returns>

        private Cell CreateHeaderCell(string header, UInt32 index, object text)
        {
            Cell c = new Cell { DataType = CellValues.String, CellReference = header + index };

            CellValue cellValue = new CellValue

            {

                Text = Convert.ToString(text),

            };

            c.Append(cellValue);

            return c;

        }

        /// <summary>

        /// Creates the text cell.

        /// </summary>

        /// <param name="header">The header.</param>

        /// <param name="index">The rowDataIndex.</param>

        /// <param name="text">The text.</param>

        /// <returns></returns>

        private Cell CreateTextCell(string header, UInt32 index, object text)
        {
            Cell c = new Cell { DataType = CellValues.InlineString, CellReference = header + index };

            InlineString istring = new InlineString();

            Text t = new Text { Text = Convert.ToString(text) };

            istring.Append(t);

            c.Append(istring);

            return c;

        }

        /// <summary>

        /// Creates the number cell.

        /// </summary>

        /// <param name="header">The header.</param>

        /// <param name="index">The rowDataIndex.</param>

        /// <param name="number">The number.</param>

        /// <returns></returns>

        private Cell CreateNumberCell(string header, UInt32 index, object number)
        {

            Cell c = new Cell

            {

                CellReference = header + index,

                DataType = CellValues.Number

            };

            CellValue v = new CellValue

            {

                Text = Convert.ToString(number),

                // DataType = CellValues.Number,

            };

            c.Append(v);

            return c;

        }

        /// <summary>

        /// Creates the date cell.

        /// </summary>

        /// <param name="header">The header.</param>

        /// <param name="index">The rowDataIndex.</param>

        /// <param name="number">The date.</param>

        /// <returns></returns>

        private Cell CreateDateCell(string header, UInt32 index, object date)
        {

            Cell c = new Cell
            {
                CellReference = header + index,
                DataType = CellValues.Date
            };

            CellValue v = new CellValue
            {
                Text = Convert.ToDateTime(date).ToString() //.ToShortTimeString(), // Convert.ToString(date),
            };

            c.Append(v);

            return c;

        }

        /// <summary>

        /// Creates the date cell.

        /// </summary>

        /// <param name="header">The header.</param>

        /// <param name="index">The rowDataIndex.</param>

        /// <param name="number">The date.</param>

        /// <returns></returns>

        private Cell CreateBooleanCell(string header, UInt32 index, object boolVal)
        {

            Cell c = new Cell

            {

                CellReference = header + index,

                DataType = CellValues.Boolean

            };

            CellValue v = new CellValue

            {

                Text = Convert.ToString(boolVal),

                // DataType = CellValues.Number,

            };

            c.Append(v);

            return c;

        }

        #region DeleteWorksheet()

        /// <summary>

        /// Deletes the A work sheet.

        /// </summary>

        /// <param name="fileName">Name of the file.</param>

        /// <param name="sheetToDelete">The sheet to delete.</param>

        public void DeleteWorkSheet(string fileName, string sheetToDelete)
        {

            string Sheetid = "";

            //Open the workbook

            using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, true))
            {

                WorkbookPart wbPart = document.WorkbookPart;

                // Get the pivot Table Parts

                IEnumerable<PivotTableCacheDefinitionPart> pvtTableCacheParts = wbPart.PivotTableCacheDefinitionParts;

                Dictionary<PivotTableCacheDefinitionPart, string> pvtTableCacheDefinationPart = new Dictionary<PivotTableCacheDefinitionPart, string>();

                foreach (PivotTableCacheDefinitionPart Item in pvtTableCacheParts)
                {

                    PivotCacheDefinition pvtCacheDef = Item.PivotCacheDefinition;

                    //Check if this CacheSource is linked to SheetToDelete

                    var pvtCahce = pvtCacheDef.Descendants<CacheSource>().Where(s => s.WorksheetSource.Sheet == sheetToDelete);

                    if (pvtCahce.Count() > 0)
                    {

                        pvtTableCacheDefinationPart.Add(Item, Item.ToString());

                    }

                }

                foreach (var Item in pvtTableCacheDefinationPart)
                {

                    wbPart.DeletePart(Item.Key);

                }

                //Get the SheetToDelete from workbook.xml

                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetToDelete).FirstOrDefault();

                if (theSheet == null)
                {

                    // The specified sheet doesn't exist.

                }

                //Store the SheetID for the reference

                Sheetid = theSheet.SheetId;

                // Remove the sheet reference from the workbook.

                WorksheetPart worksheetPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                theSheet.Remove();

                // Delete the worksheet part.

                wbPart.DeletePart(worksheetPart);

                //Get the DefinedNames

                var definedNames = wbPart.Workbook.Descendants<DefinedNames>().FirstOrDefault();

                if (definedNames != null)
                {

                    foreach (DefinedName Item in definedNames)
                    {

                        // This condition checks to delete only those names which are part of Sheet in question

                        if (Item.Text.Contains(sheetToDelete + "!"))

                            Item.Remove();

                    }

                }

                // Get the CalculationChainPart

                //Note: An instance of this part type contains an ordered set of references to all cells in all worksheets in the

                //workbook whose value is calculated from any formula

                CalculationChainPart calChainPart;

                calChainPart = wbPart.CalculationChainPart;

                if (calChainPart != null)
                {

                    var calChainEntries = calChainPart.CalculationChain.Descendants<CalculationCell>().Where(c => c.SheetId == Sheetid);

                    foreach (CalculationCell Item in calChainEntries)
                    {

                        Item.Remove();

                    }

                    if (calChainPart.CalculationChain.Count() == 0)
                    {

                        wbPart.DeletePart(calChainPart);

                    }

                }

                // Save the workbook.

                wbPart.Workbook.Save();

            }

        }

        #endregion

        #region  ClearWorkSheetData

        /// <summary>

        /// Deletes the A work sheet.

        /// </summary>

        /// <param name="fileName">Name of the file.</param>

        /// <param name="sheetToDelete">The sheet to delete.</param>

        public void ClearWorkSheetData(string fileName, string sheetToClear)
        {

            string Sheetid = "";

            //Open the workbook

            using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, true))
            {

                WorkbookPart wbPart = document.WorkbookPart;

                // Get the pivot Table Parts

                IEnumerable<PivotTableCacheDefinitionPart> pvtTableCacheParts = wbPart.PivotTableCacheDefinitionParts;

                Dictionary<PivotTableCacheDefinitionPart, string> pvtTableCacheDefinationPart = new Dictionary<PivotTableCacheDefinitionPart, string>();

                foreach (PivotTableCacheDefinitionPart Item in pvtTableCacheParts)
                {

                    PivotCacheDefinition pvtCacheDef = Item.PivotCacheDefinition;

                    //Check if this CacheSource is linked to SheetToDelete

                    var pvtCahce = pvtCacheDef.Descendants<CacheSource>().Where(s => s.WorksheetSource.Sheet == sheetToClear);

                    if (pvtCahce.Count() > 0)
                    {

                        pvtTableCacheDefinationPart.Add(Item, Item.ToString());

                    }

                }

                foreach (var Item in pvtTableCacheDefinationPart)
                {

                    wbPart.DeletePart(Item.Key);

                }

                //Get the SheetToDelete from workbook.xml

                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetToClear).FirstOrDefault();

                if (theSheet == null)
                {

                    // The specified sheet doesn't exist.

                }

                //Store the SheetID for the reference

                Sheetid = theSheet.SheetId;

                // Remove the sheet reference from the workbook.

                WorksheetPart worksheetPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                Worksheet workSheet = worksheetPart.Worksheet;

                SheetData sheetData = workSheet.GetFirstChild<SheetData>();

                for (int childIndex = 1; childIndex < sheetData.ChildElements.Count; childIndex++)
                {

                    sheetData.RemoveChild(sheetData.ChildElements[childIndex]);

                }

                IEnumerable<DocumentFormat.OpenXml.Spreadsheet.Row> rows = sheetData.Descendants<DocumentFormat.OpenXml.Spreadsheet.Row>();

                List<DocumentFormat.OpenXml.Spreadsheet.Row> rowsList = rows.ToList();

                //rowsList.RemoveRange(1, rowsList.Count - 1);

                // Save the workbook.

                wbPart.Workbook.Save();

            }

        }

        #endregion

        #region Create(string fileSavePath, DataSet dataSet)

        /// <summary>

        /// Saves the specified file save path.

        /// </summary>

        /// <param name="fileSavePath">The file save path.</param>

        /// <param name="dataSet">The data set.</param>

        public void Create(string fileSavePath, DataSet dataSet)
        {

            Dictionary<string, List<DocumentFormat.OpenXml.OpenXmlElement>> inputDictionary = ToSheets(dataSet);

            this.Create(fileSavePath, inputDictionary);

            // inputDictionary

        }

        /// <summary>

        /// Creates the specified path.

        /// </summary>

        /// <param name="path">The path.</param>

        /// <param name="sets">The sets.</param>

        private void Create(string path, Dictionary<String, List<DocumentFormat.OpenXml.OpenXmlElement>> sets)
        {

            using (SpreadsheetDocument package = SpreadsheetDocument.Create(path, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
            {

                WorkbookPart workbookpart = package.AddWorkbookPart();

                workbookpart.Workbook = new Workbook();

                Sheets sheets = workbookpart.Workbook.AppendChild(new Sheets());

                foreach (KeyValuePair<String, List<DocumentFormat.OpenXml.OpenXmlElement>> set in sets)
                {

                    WorksheetPart worksheetpart = workbookpart.AddNewPart<WorksheetPart>();

                    worksheetpart.Worksheet = new Worksheet(new SheetData(set.Value));

                    worksheetpart.Worksheet.Save();

                    Sheet sheet = new Sheet()

                    {

                        Id = workbookpart.GetIdOfPart(worksheetpart),

                        SheetId = (uint)(sheets.Count() + 1),

                        Name = set.Key

                    };

                    sheets.AppendChild(sheet);

                }

                workbookpart.Workbook.Save();

            }

        }

        /// <summary>

        /// Toes the sheets.

        /// </summary>

        /// <param name="ds">The ds.</param>

        /// <returns></returns>

        private Dictionary<string, List<DocumentFormat.OpenXml.OpenXmlElement>> ToSheets(DataSet ds)
        {

            return

                (from dt in ds.Tables.OfType<DataTable>()

                 select new

                 {

                     // Sheet Name

                     Key = dt.TableName,

                     Value = (

                         // Sheet Columns

                     new List<DocumentFormat.OpenXml.OpenXmlElement>(new DocumentFormat.OpenXml.OpenXmlElement[]

                {

                    new DocumentFormat.OpenXml.Spreadsheet.Row(

                        from d in dt.Columns.OfType<DataColumn>()

                        select (DocumentFormat.OpenXml.OpenXmlElement)new Cell()

                        {

                            CellValue = new CellValue(d.ColumnName),

                            DataType = CellValues.String

                        })

                })).Union

                         // Sheet Rows

                     ((from dr in dt.Rows.OfType<DataRow>()

                       select ((DocumentFormat.OpenXml.OpenXmlElement)new DocumentFormat.OpenXml.Spreadsheet.Row(from dc in dr.ItemArray

                                                                                                                 select (DocumentFormat.OpenXml.OpenXmlElement)new Cell()

                                                                                                                 {

                                                                                                                     CellValue = new CellValue(dc.ToString()),

                                                                                                                     DataType = CellValues.String

                                                                                                                 })))).ToList()

                 }).ToDictionary(p => p.Key, p => p.Value);

        }

        #endregion

        #region Read

        /// <summary>

        /// Reads the specified file save path.

        /// </summary>

        /// <param name="fileSavePath">The file save path.</param>

        /// <returns></returns>

        public DataSet Read(string fileSavePath)
        {

            DataSet resultSet = new DataSet();

            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileSavePath, false))
            {

                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;

                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();

                foreach (Sheet sheet in sheets)
                {

                    DataTable dt = new DataTable();

                    string relationshipId = sheet.Id.Value;

                    string sheetName = sheet.SheetId;

                    dt.TableName = sheet.SheetId;

                    WorksheetPart worksheetPart =

                        (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);

                    Worksheet workSheet = worksheetPart.Worksheet;

                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();

                    IEnumerable<DocumentFormat.OpenXml.Spreadsheet.Row> rows = sheetData.Descendants<DocumentFormat.OpenXml.Spreadsheet.Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {

                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));

                    }

                    List<DocumentFormat.OpenXml.Spreadsheet.Row> rowsList = new List<DocumentFormat.OpenXml.Spreadsheet.Row>();

                    rowsList = rows.ToList();

                    //Start from 1, first row is header.

                    for (int iterRowIndex = 1; iterRowIndex < rowsList.Count; iterRowIndex++) //this will also include your header row...
                    {

                        DocumentFormat.OpenXml.Spreadsheet.Row row = rowsList[iterRowIndex];

                        DataRow tempRow = dt.NewRow();

                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {

                            tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));

                        }

                        dt.Rows.Add(tempRow);

                    }

                    resultSet.Tables.Add(dt);

                }

            }

            return resultSet;

        }

        /// <summary>

        /// Gets the cell value.

        /// </summary>

        /// <param name="document">The document.</param>

        /// <param name="cell">The cell.</param>

        /// <returns></returns>

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {

            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;

            string value = cell.CellValue != null ? cell.CellValue.InnerXml : string.Empty;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {

                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;

            }

            else
            {

                return value;

            }

        }

        #endregion

    }

    /// <summary>
    /// 수리해석결과-모델레이어 렌더링
    /// </summary>
    public class AnalysisDataRenderer
    {
        private DataSet Data;
        public AnalysisDataRenderer(DataSet data)
        {
            this.Data = data;
        }

        public void DrawLinkRendering()
        {
            if (this.Data == null) return;
            if (!this.Data.Tables.Contains("LINK")) return;   

        }

        public void DrawNodeRendering(string fieldName)
        {
            if (this.Data == null) return;
            if (!this.Data.Tables.Contains("NODE")) return;

            DataTable table = this.Data.Tables["NODE"];
            var values = (from t in table.AsEnumerable()
                          select new
                          {
                              ID = t.Field<string>("NODE_ID"),
                              VALUE = t.Field<string>(fieldName)
                          });

        }

        public void DrawLinkAnnotation()
        {
            if (this.Data == null) return;
            if (!this.Data.Tables.Contains("LINK")) return;   

        }

        public void DrawNodeAnnotation()
        {
            if (this.Data == null) return;
            if (!this.Data.Tables.Contains("NODE")) return;   

        }

        //public void DrawRendering()
        //{
        //    //링크, 노드 렌더링
        //    try
        //    {
        //        #region 노드
        //        if (settingData["NODE_RESULT_TYPE"] != null)  //node
        //        {
        //            if (!WH_Common.WH_VariableManager.analysis_Data.Tables.Contains("WH_RPT_NODES")) return;
        //            #region 노드-해석결과데이터
        //            string fieldName = string.Empty;
        //            switch ((string)settingData["NODE_RESULT_CODE"])
        //            {
        //                case "000001":
        //                    fieldName = "ELEVATION";
        //                    break;
        //                case "000002":
        //                    fieldName = "BASEDEMAND";
        //                    break;
        //                case "000003":
        //                    fieldName = "INITQUAL";
        //                    break;
        //                case "000004":
        //                    fieldName = "DEMAND";
        //                    break;
        //                case "000005":
        //                    fieldName = "HEAD";
        //                    break;
        //                case "000006":
        //                    fieldName = "PRESSURE";
        //                    break;
        //                case "000007":
        //                    fieldName = "QUALITY";
        //                    break;
        //                case "000008":
        //                    fieldName = "HEADLOSS";
        //                    break;
        //            }

        //            var values = (from table in WH_Common.WH_VariableManager.analysis_Data.Tables["WH_RPT_NODES"].AsEnumerable()
        //                          select new
        //                          {
        //                              ID = table.Field<string>("NODE_ID"),
        //                              VALUE = table.Field<string>(fieldName)
        //                          });
        //            #endregion 노드-해석결과데이터

        //            EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
        //            vrm.Fields = new string[] { "ID" };

        //            double[] scope = { double.Parse((string)settingData["nodeRange1"]), double.Parse((string)settingData["nodeRange2"]),
        //                               double.Parse((string)settingData["nodeRange3"]), double.Parse((string)settingData["nodeRange4"])};
        //            Color[] colors = { Color.Blue, Color.Cyan, Color.Green, Color.Yellow, Color.Red };
        //            int[] size = { 2, 4, 6, 8, 10 };

        //            for (int i = 0; i <= scope.Length; i++)
        //            {
        //                double min = double.MinValue; double max = double.MaxValue;
        //                if (i == 0)
        //                    max = scope[i];
        //                else if (i == scope.Length)
        //                    min = scope[i - 1];
        //                else
        //                {
        //                    min = scope[i - 1];
        //                    max = scope[i];
        //                }

        //                //렌더링
        //                var ids = values.Where(entry => (double.Parse(entry.VALUE) > min && double.Parse(entry.VALUE) <= max))
        //                                .Select(v => string.Format("{0}", v.ID)).ToList();
        //                if (ids.Count == 0) continue;

        //                EAGL.Display.Rendering.SimplePointRenderer spr = new EAGL.Display.Rendering.SimplePointRenderer();
        //                spr.Color = colors[i];
        //                spr.Style = esriSimpleMarkerStyle.esriSMSCircle;
        //                spr.Size = size[i];
        //                ISymbol sym = spr.Symbol;

        //                object key = ids[0];
        //                List<object> refValues = new List<object>();
        //                for (int j = 1; j < ids.Count(); j++)
        //                {
        //                    refValues.Add(ids[j]);
        //                }

        //                if (min == double.MinValue || max == double.MaxValue)
        //                    vrm.Add(key, sym, string.Format("{0}{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));
        //                else
        //                    vrm.Add(key, sym, string.Format("{0}-{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));

        //                vrm.AddReferanceValue2(key, refValues);
        //            }

        //            ILayer layer = null;
        //            layer = ArcManager.GetMapLayer(axMap.Map, "JUNCTION");
        //            if (layer != null)
        //            {
        //                ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
        //            }
        //            layer = ArcManager.GetMapLayer(axMap.Map, "RESERVOIR");
        //            if (layer != null)
        //            {
        //                ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
        //            }
        //            layer = ArcManager.GetMapLayer(axMap.Map, "TANK");
        //            if (layer != null)
        //            {
        //                ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
        //            }

        //        }
        //        #endregion 노드

        //        #region 링크
        //        if (settingData["LINK_RESULT_TYPE"] != null)  //link
        //        {
        //            if (!WH_Common.WH_VariableManager.analysis_Data.Tables.Contains("WH_RPT_LINKS")) return;

        //            #region 링크-해석결과데이터
        //            string fieldName = string.Empty;
        //            switch ((string)settingData["LINK_RESULT_CODE"])
        //            {
        //                case "000001":
        //                    fieldName = "LENGTH";
        //                    break;
        //                case "000002":
        //                    fieldName = "DIAMETER";
        //                    break;
        //                case "000003":
        //                    fieldName = "ROUGHNESS";
        //                    break;
        //                case "000004":
        //                    fieldName = "KBULK";
        //                    break;
        //                case "000005":
        //                    fieldName = "KWALL";
        //                    break;
        //                case "000006":
        //                    fieldName = "FLOW";
        //                    break;
        //                case "000007":
        //                    fieldName = "VELOCITY";
        //                    break;
        //                case "000008":
        //                    fieldName = "HEADLOSS";
        //                    break;
        //            }

        //            var values = (from table in WH_Common.WH_VariableManager.analysis_Data.Tables["WH_RPT_LINKS"].AsEnumerable()
        //                          select new
        //                          {
        //                              ID = table.Field<string>("LINK_ID"),
        //                              VALUE = table.Field<string>(fieldName)
        //                          });
        //            #endregion 링크-해석결과데이터

        //            EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
        //            vrm.Fields = new string[] { "ID" };

        //            double[] scope = { double.Parse((string)settingData["linkRange4"]) * -1, double.Parse((string)settingData["linkRange3"]) * -1,
        //                               double.Parse((string)settingData["linkRange2"]) * -1, double.Parse((string)settingData["linkRange1"]) * -1,
        //                               0,
        //                               double.Parse((string)settingData["linkRange1"]), double.Parse((string)settingData["linkRange2"]),
        //                               double.Parse((string)settingData["linkRange3"]), double.Parse((string)settingData["linkRange4"])};
        //            Color[] colors = { Color.Red, Color.Yellow, Color.Green, Color.Cyan, Color.Blue, Color.Blue, Color.Cyan, Color.Green, Color.Yellow, Color.Red };
        //            int[] width = { 5, 4, 3, 2, 1, 1, 2, 3, 4, 5 };

        //            for (int i = 0; i <= scope.Length; i++)
        //            {
        //                double min = double.MinValue; double max = double.MaxValue;
        //                if (i == 0)
        //                    max = scope[i];
        //                else if (i == scope.Length)
        //                    min = scope[i - 1];
        //                else
        //                {
        //                    min = scope[i - 1];
        //                    max = scope[i];
        //                }

        //                var ids = values.Where(entry => (double.Parse(entry.VALUE) > min && double.Parse(entry.VALUE) <= max))
        //                                .Select(v => string.Format("{0}", v.ID)).ToList();
        //                if (ids.Count == 0) continue;

        //                EAGL.Display.Rendering.SimpleLineRenderer slr = new EAGL.Display.Rendering.SimpleLineRenderer();
        //                slr.Color = colors[i];
        //                slr.Style = esriSimpleLineStyle.esriSLSSolid;
        //                slr.Width = width[i];
        //                ISymbol sym = slr.Symbol;

        //                object key = ids[0];
        //                List<object> refValues = new List<object>();
        //                for (int j = 1; j < ids.Count(); j++)
        //                {
        //                    refValues.Add(ids[j]);
        //                }

        //                if (min == double.MinValue || max == double.MaxValue)
        //                    vrm.Add(key, sym, string.Format("{0}{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));
        //                else
        //                    vrm.Add(key, sym, string.Format("{0}-{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));


        //                vrm.AddReferanceValue2(key, refValues);
        //            }

        //            ILayer layer = null;
        //            layer = ArcManager.GetMapLayer(axMap.Map, "PIPE");
        //            if (layer != null)
        //            {
        //                ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
        //            }
        //            layer = ArcManager.GetMapLayer(axMap.Map, "PUMP");
        //            if (layer != null)
        //            {
        //                ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
        //            }
        //            layer = ArcManager.GetMapLayer(axMap.Map, "VALVE");
        //            if (layer != null)
        //            {
        //                ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
        //            }
        //        }
        //        #endregion 링크
        //    }
        //    catch { }
        //}

        //public void DrawAnnotation()
        //{
        //    //링크, 노드 라벨 렌더링
        //    try
        //    {
        //        #region 노드
        //        if (settingData["NODE_RESULT_TYPE"] != null)  //node
        //        {
        //            if (!WH_Common.WH_VariableManager.analysis_Data.Tables.Contains("WH_RPT_NODES")) return;
        //            #region 노드-해석결과데이터
        //            string fieldName = string.Empty;
        //            switch ((string)settingData["NODE_RESULT_CODE"])
        //            {
        //                case "000001":
        //                    fieldName = "ELEVATION";
        //                    break;
        //                case "000002":
        //                    fieldName = "BASEDEMAND";
        //                    break;
        //                case "000003":
        //                    fieldName = "INITQUAL";
        //                    break;
        //                case "000004":
        //                    fieldName = "DEMAND";
        //                    break;
        //                case "000005":
        //                    fieldName = "HEAD";
        //                    break;
        //                case "000006":
        //                    fieldName = "PRESSURE";
        //                    break;
        //                case "000007":
        //                    fieldName = "QUALITY";
        //                    break;
        //                case "000008":
        //                    fieldName = "HEADLOSS";
        //                    break;
        //            }

        //            var q = (from table in WH_Common.WH_VariableManager.analysis_Data.Tables["WH_RPT_NODES"].AsEnumerable()
        //                     select new
        //                     {
        //                         ID = table.Field<string>("NODE_ID"),
        //                         VALUE = table.Field<string>(fieldName)
        //                     });
        //            Dictionary<string, string> values = q.ToDictionary(k => k.ID, k => k.VALUE);

        //            #endregion 노드-해석결과데이터

        //            EAGL.Display.LabelRenderingMap LRM = new EAGL.Display.LabelRenderingMap();
        //            //LRM.FeatureLayer = (IFeatureLayer)layer;
        //            LRM.Extent = this.axMap.Extent;
        //            LRM.valueMap = values;

        //            double[] scope = { double.Parse((string)settingData["nodeRange1"]), double.Parse((string)settingData["nodeRange2"]),
        //                               double.Parse((string)settingData["nodeRange3"]), double.Parse((string)settingData["nodeRange4"])};
        //            Color[] colors = { Color.Blue, Color.Cyan, Color.Green, Color.Yellow, Color.Red };
        //            int[] size = { 2, 4, 6, 8, 10 };

        //            for (int i = 0; i <= scope.Length; i++)
        //            {
        //                double min = double.MinValue; double max = double.MaxValue;
        //                if (i == 0)
        //                    max = scope[i];
        //                else if (i == scope.Length)
        //                    min = scope[i - 1];
        //                else
        //                {
        //                    min = scope[i - 1];
        //                    max = scope[i];
        //                }

        //                //렌더링
        //                var ids = values.Where(entry => (double.Parse(entry.Value) > min && double.Parse(entry.Value) <= max))
        //                                .Select(v => string.Format("'{0}'", v.Key)).ToList();
        //                if (ids.Count == 0) continue;

        //                string wheres = string.Format("{0} in (", "ID");
        //                wheres += string.Join(",", ids.ToArray());
        //                wheres += string.Format(")", string.Empty);

        //                IFormattedTextSymbol pText = new TextSymbolClass();
        //                pText.Font.Bold = false;
        //                pText.Font.Name = "굴림";
        //                pText.Size = 8;
        //                pText.Color = EAGL.Display.ColorManager.GetESRIColor(colors[i]);

        //                LRM.Add(i.ToString(), wheres, "ID", (ITextSymbol)pText);
        //            }

        //            ILayer layer = null;
        //            layer = ArcManager.GetMapLayer(axMap.Map, "JUNCTION");
        //            if (layer != null)
        //            {
        //                LRM.FeatureLayer = (IFeatureLayer)layer;
        //                IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
        //                for (int i = 0; i < ac.Count; i++)
        //                {
        //                    IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
        //                    ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
        //                    Console.WriteLine(string.Format("{0} : {1}, {2}", i, placedElements.Count, unplacedElements.Count));
        //                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
        //                }
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
        //            }
        //            layer = ArcManager.GetMapLayer(axMap.Map, "RESERVOIR");
        //            if (layer != null)
        //            {
        //                LRM.FeatureLayer = (IFeatureLayer)layer;
        //                IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
        //                for (int i = 0; i < ac.Count; i++)
        //                {
        //                    IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
        //                    ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
        //                    Console.WriteLine(string.Format("{0} : {1}, {2}", i, placedElements.Count, unplacedElements.Count));
        //                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
        //                }
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
        //            }
        //            layer = ArcManager.GetMapLayer(axMap.Map, "TANK");
        //            if (layer != null)
        //            {
        //                LRM.FeatureLayer = (IFeatureLayer)layer;
        //                IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
        //                for (int i = 0; i < ac.Count; i++)
        //                {
        //                    IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
        //                    ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
        //                    Console.WriteLine(string.Format("{0} : {1}, {2}", i, placedElements.Count, unplacedElements.Count));
        //                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
        //                }
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
        //            }

        //        }
        //        #endregion 노드

        //        #region 링크
        //        if (settingData["LINK_RESULT_TYPE"] != null)  //link
        //        {
        //            if (!WH_Common.WH_VariableManager.analysis_Data.Tables.Contains("WH_RPT_LINKS")) return;

        //            #region 링크-해석결과데이터
        //            string fieldName = string.Empty;
        //            switch ((string)settingData["LINK_RESULT_CODE"])
        //            {
        //                case "000001":
        //                    fieldName = "LENGTH";
        //                    break;
        //                case "000002":
        //                    fieldName = "DIAMETER";
        //                    break;
        //                case "000003":
        //                    fieldName = "ROUGHNESS";
        //                    break;
        //                case "000004":
        //                    fieldName = "KBULK";
        //                    break;
        //                case "000005":
        //                    fieldName = "KWALL";
        //                    break;
        //                case "000006":
        //                    fieldName = "FLOW";
        //                    break;
        //                case "000007":
        //                    fieldName = "VELOCITY";
        //                    break;
        //                case "000008":
        //                    fieldName = "HEADLOSS";
        //                    break;
        //            }

        //            var q = (from table in WH_Common.WH_VariableManager.analysis_Data.Tables["WH_RPT_LINKS"].AsEnumerable()
        //                     select new
        //                     {
        //                         ID = table.Field<string>("LINK_ID"),
        //                         VALUE = table.Field<string>(fieldName)
        //                     });
        //            Dictionary<string, string> values = q.ToDictionary(k => k.ID, k => k.VALUE);
        //            #endregion 링크-해석결과데이터

        //            EAGL.Display.LabelRenderingMap LRM = new EAGL.Display.LabelRenderingMap();
        //            //LRM.FeatureLayer = (IFeatureLayer)layer;
        //            LRM.Extent = this.axMap.Extent;
        //            LRM.valueMap = values;

        //            double[] scope = { double.Parse((string)settingData["linkRange4"]) * -1, double.Parse((string)settingData["linkRange3"]) * -1,
        //                               double.Parse((string)settingData["linkRange2"]) * -1, double.Parse((string)settingData["linkRange1"]) * -1,
        //                               0,
        //                               double.Parse((string)settingData["linkRange1"]), double.Parse((string)settingData["linkRange2"]),
        //                               double.Parse((string)settingData["linkRange3"]), double.Parse((string)settingData["linkRange4"])};
        //            Color[] colors = { Color.Red, Color.Yellow, Color.Green, Color.Cyan, Color.Blue, Color.Blue, Color.Cyan, Color.Green, Color.Yellow, Color.Red };
        //            int[] width = { 5, 4, 3, 2, 1, 1, 2, 3, 4, 5 };

        //            for (int i = 0; i <= scope.Length; i++)
        //            {
        //                double min = double.MinValue; double max = double.MaxValue;
        //                if (i == 0)
        //                    max = scope[i];
        //                else if (i == scope.Length)
        //                    min = scope[i - 1];
        //                else
        //                {
        //                    min = scope[i - 1];
        //                    max = scope[i];
        //                }

        //                //렌더링
        //                var ids = values.Where(entry => (double.Parse(entry.Value) > min && double.Parse(entry.Value) <= max))
        //                                .Select(v => string.Format("'{0}'", v.Key)).ToList();
        //                if (ids.Count == 0) continue;

        //                string wheres = string.Format("{0} in (", "ID");
        //                wheres += string.Join(",", ids.ToArray());
        //                wheres += string.Format(")", string.Empty);

        //                IFormattedTextSymbol pText = new TextSymbolClass();
        //                pText.Font.Bold = false;
        //                pText.Font.Name = "굴림";
        //                pText.Size = 9;
        //                pText.Color = EAGL.Display.ColorManager.GetESRIColor(colors[i]);

        //                LRM.Add(i.ToString(), wheres, "ID", (ITextSymbol)pText);
        //            }

        //            ILayer layer = null;
        //            layer = ArcManager.GetMapLayer(axMap.Map, "PIPE");
        //            if (layer != null)
        //            {
        //                LRM.FeatureLayer = (IFeatureLayer)layer;
        //                IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
        //                for (int i = 0; i < ac.Count; i++)
        //                {
        //                    IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
        //                    ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
        //                    Console.WriteLine(string.Format("{0} : {1}, {2}", i, placedElements.Count, unplacedElements.Count));
        //                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
        //                }
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
        //            }
        //            layer = ArcManager.GetMapLayer(axMap.Map, "PUMP");
        //            if (layer != null)
        //            {
        //                LRM.FeatureLayer = (IFeatureLayer)layer;
        //                IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
        //                for (int i = 0; i < ac.Count; i++)
        //                {
        //                    IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
        //                    ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
        //                    Console.WriteLine(string.Format("{0} : {1}, {2}", i, placedElements.Count, unplacedElements.Count));
        //                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
        //                }
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
        //            }
        //            layer = ArcManager.GetMapLayer(axMap.Map, "VALVE");
        //            if (layer != null)
        //            {
        //                LRM.FeatureLayer = (IFeatureLayer)layer;
        //                IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
        //                for (int i = 0; i < ac.Count; i++)
        //                {
        //                    IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
        //                    ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
        //                    Console.WriteLine(string.Format("{0} : {1}, {2}", i, placedElements.Count, unplacedElements.Count));
        //                    ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
        //                }
        //                ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
        //            }
        //        }
        //        #endregion 링크
        //    }
        //    catch { }
        //}
    }

}
