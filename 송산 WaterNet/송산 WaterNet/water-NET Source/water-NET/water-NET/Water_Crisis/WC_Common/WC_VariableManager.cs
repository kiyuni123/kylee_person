﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WC_Common
{
    public class WC_VariableManager
    {
        //public static string m_INP_NUMBER = string.Empty; // "WH20101116337182884";  //에너지 해석 모델번호 INP_NUMBER

        public static string[] m_shapes = { "TANK", "RESERVOIR", "JUNCTION", "PIPE", "PUMP", "VALVE" };

        #region Public Struct -------------------------------------------------------------------------
        /// <summary>
        /// INP : NODE 구조체
        /// </summary>
        public struct Struct_INP_Node
        {
            public string ID;
            public double EN_ELEVATI;
            public double EN_BASEDEM;
            public double EN_PATTERN;
            public double EN_EMITTER;
            public double EN_INITQUA;
            public double EN_SOURCEQ;
            public double EN_SOURCEP;
            public double EN_SOURCET;
            public double EN_TANKLEV;
            public double EN_DEMAND;
            public double EN_HEAD;
            public double EN_PRESSUR;
            public double EN_QUALITY;
            public double EN_SOURCEM;
            public double EN_INITVOL;
            public double EN_MIXMODE;
            public double EN_MIXZONE;
            public double EN_TANKDIA;
            public double EN_MINVOLU;
            public double EN_VOLCURV;
            public double EN_MINLEVE;
            public double EN_MAXLEVE;
            public double EN_MIXFRAC;
            public double EN_TANK_KB;
            public string POSITION;
        }

        /// <summary>
        /// INP : LINK 구조체
        /// </summary>
        public struct Struct_INP_Link
        {
            public string ID;
            public double EN_DIAMETE;
            public double EN_LENGTH;
            public double EN_ROUGHNE;
            public double EN_MINORLO;
            public double EN_INITSTA;
            public double EN_INITSET;
            public double EN_KBULK;
            public double EN_KWALL;
            public double EN_FLOW;
            public double EN_VELOCIT;
            public double EN_HEADLOS;
            public double EN_STATUS;
            public double EN_SETTING;
            public double EN_ENERGY;
            public string POSITION;
        }
        #endregion Public Struct -------------------------------------------------------------------------

        #region FeatureLayer 선언
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_PipeLayer; //상수관망 상수관로 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_SplyLayer; //상수관망 급수관로 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_ValveLayer; //상수관망 밸브 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_MetaLayer; //상수관망 수도계량기 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_FireLayer; //상수관망 소방시설 레이어

        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_ValveLayer;     //INP VALVE 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_JunctionLayer;  //INP JUNCTION 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_ReservoirLayer; //INP RESERVOIR 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_TankLayer;      //INP TANK 레이어
        public static ESRI.ArcGIS.Carto.IFeatureLayer m_INP_PipeLayer;      //INP PIPE 레이어
        #endregion FeatureLayer 선언

        //위기관리 상수
        public static System.Collections.Hashtable m_CrisisParams = new System.Collections.Hashtable()
        {
            {"PIPE_GBN", string.Empty}, //사고관로-G(광역관로), L(지방관로)
            {"CRACKPOINT", null} //사고관로-G(광역관로), L(지방관로)
        };

        public static WaterNetCore.frmSplashMsg m_SplashForm = new WaterNetCore.frmSplashMsg();  //Splash 폼

        public static System.Data.DataSet analysis_Data = new System.Data.DataSet();

        public static Dictionary<string, ESRI.ArcGIS.Carto.IElementCollection> m_graphicsDic = new Dictionary<string, ESRI.ArcGIS.Carto.IElementCollection>();
    }
}
