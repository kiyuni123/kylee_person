﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WC_Common
{
    public partial class WC_SplashForm : Form
    {
        private string MSG = "작업중입니다......";

        public WC_SplashForm()
        {
            InitializeComponent();

            m_lblMsg.BackColor = Color.Transparent;
            m_lblMsg.BringToFront();
        }

        public string Message
        {
            set
            {
                MSG = value;
                m_lblMsg.Text = MSG;
                System.Windows.Forms.Application.DoEvents();
            }
        }

        public void Open()
        {
            this.Opacity = 1.0;
            m_lblMsg.Text = MSG;
            this.BringToFront();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Show();
        }
    }
}
