﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WC_Common;
using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WC_WaterTrouble
{
    public partial class frmSelectTimes : Form
    {
        /// <summary>
        /// 폼을 최소화하기 위한 멤버선언
        /// </summary>
        private System.Drawing.Size m_formsize = new System.Drawing.Size(); //폼사이즈
        private System.Drawing.Point m_formLocation = new System.Drawing.Point(); //폼위치
        private IntPtr m_formParam = new IntPtr();                                //최종 윈도우 파라메터

        private IMapControl3 m_mapcontrol;
        private ESRI.ArcGIS.Carto.IMap m_map;

        public frmSelectTimes()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 윈도우 메세지 후킹 : 화면 최소화, 복구 이벤트 기능
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                if (m.WParam == new IntPtr(0xF020)) // SC_MINIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    m_formsize = this.Size;
                    m_formLocation = this.Location;
                    m_formParam = m.WParam;

                    int deskHeight = Screen.PrimaryScreen.Bounds.Height;
                    int deskWidth = Screen.PrimaryScreen.Bounds.Width;

                    this.Size = new System.Drawing.Size(150, SystemInformation.CaptionHeight);
                    if (this.ParentForm != null)
                        this.Location = new System.Drawing.Point(this.ParentForm.Width - 170, this.ParentForm.Height - 40);
                    else
                    {
                        if (this.Owner != null)
                            this.Location = new System.Drawing.Point(this.Owner.Width - 170, this.Owner.Height - 40);
                        else
                            this.Location = new System.Drawing.Point(0, 0);
                    }
                    this.BringToFront();
                    m.Result = new IntPtr(0);
                    return;
                }
                else if (m.WParam == new IntPtr(0xF030))  // SC_MAXIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    if (m_formsize.Height == 0 | m_formsize.Width == 0) return;

                    this.Size = m_formsize;
                    this.Location = m_formLocation;
                    this.BringToFront();
                    m_formParam = m.WParam;

                    m.Result = new IntPtr(0);
                    return;
                }
                else if ((m.WParam.ToInt32() >= 61441) && (m.WParam.ToInt32() <= 61448))  // SC_SIZE변경
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF032))  //최대화(타이틀바 더블클릭)시.
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF060))  //SC_CLOSE버튼 실행안함.
                {
                    return;
                }
                m.Result = new IntPtr(0);
            }

            base.WndProc(ref m);
        }

        #region Public Property
        /// <summary>
        /// Focus Map 설정
        /// </summary>
        public IMapControl3 mapControl
        {
            set
            {
                m_mapcontrol = value;
                m_map = m_mapcontrol.ActiveView.FocusMap;
            }
        }

        /// <summary>
        /// 사고시각 라벨 설정
        /// </summary>
        public string AccidentTime
        {
            set
            {
                lbl_AccientTime.Text = "사고시각 : " + value;
            }
        }

        #endregion Public Property

      
        /// <summary>
        /// 실행
        /// </summary>
        public void Open()
        {
            //int periods = WC_Global.crisisAnalysisOp.WorkModel.Output.Nperiods;
            DataTable times = new DataTable();
            times.Columns.Add("TIME", typeof(string));
            for (int i = 1; i <= 124; i++)
            {
                DataRow row = times.NewRow();
                row["TIME"] = i.ToString() + ":00";
                times.Rows.Add(row);
            }
            cbotime.DataSource = times.DefaultView;
            cbotime.DisplayMember = "TIME";
            cbotime.ValueMember = "TIME";

            lbl_AccientTime.Text = "사고시각 : " + localVariable.m_AccidentTime;
            this.Show();
        }

        private void frmSelectTimes_Load(object sender, EventArgs e)
        {
            cbotime.SelectedIndexChanged += new EventHandler(cbotime_SelectedIndexChanged);
            cbotime.SelectedIndex = 0;
            cbotime_SelectedIndexChanged(this.cbotime, new EventArgs());
        }

        private void cbotime_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbotime.SelectedIndex == -1) return;
            string strTime = cbotime.SelectedValue.ToString();
            ///시간에따라 변경해야 하는부분(1-124)
            WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.DURATION_INDEX] = strTime;
            WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.RPT_TSTEP_INDEX] = strTime;
            WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.RPT_START_INDEX] = strTime;

            try
            {
                int err = WC_Global.crisisAnalysisOp.WorkModel.RunAnalysis();
                if (err != 0)
                {
                    throw new Exception(EpaNetwork.xGlobal.ErrorCodes[err]);
                }

                Dictionary<EpaNetwork.xNode, float> values = WC_Global.crisisAnalysisOp.WorkModel.Output.GetJuncValues(EpaNetwork.xGlobal.OUT_NODEQUAL, 0);// cbotime.SelectedIndex + 1);
                var v = values.Where(entry => entry.Key is EpaNetwork.xJunc && entry.Value > 0).Select(entry => entry).ToDictionary(key => key.Key.Id, value => Math.Round(value.Value, 2).ToString());
                if (v.Count == 0)
                {
                    EAGL.Explore.MapExplorer mapexplorer = new EAGL.Explore.MapExplorer(this.m_map);
                    IFeatureLayer layer = mapexplorer.GetLayer("JUNCTION") as IFeatureLayer;
                    ((IGeoFeatureLayer)layer).DisplayAnnotation = false;
                    ((IGeoFeatureLayer)layer).Renderer = ArcManager.DeSerializeFeatureRenderer(WC_Global.defaultJunctionRenderer); ;
                }
                else
                {
                    this.DrawAnnotateRenderer(v);
                    this.DrawGraphicRenderer(v);
                    ///물흐름 방향은 여기에서 출력
                    ((frmTroubleMain)this.ParentForm).DrawArrowRenderer(WC_Global.crisisAnalysisOp.WorkModel);
                }
                
                ArcManager.PartialRefresh(this.m_map, esriViewDrawPhase.esriViewGraphics);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

       

        private void DrawGraphicRenderer(Dictionary<string, string> values)
        {
            EAGL.Explore.MapExplorer mapexplorer = new EAGL.Explore.MapExplorer(this.m_map);
            IFeatureLayer layer = mapexplorer.GetLayer("JUNCTION") as IFeatureLayer;
            EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
            vrm.Fields = new string[] { "ID" };

            {
                var ids = values.Where(entry => (double.Parse(entry.Value) > 0) && (double.Parse(entry.Value) < 100)).Select(v => string.Format("{0}", v.Key)).ToList();
                if (ids.Count > 0)
                {
                    EAGL.Display.SimplePointSymbol spr = new EAGL.Display.SimplePointSymbol();
                    spr.Color = Color.Green;
                    spr.Style = esriSimpleMarkerStyle.esriSMSCircle;
                    spr.size = 4;
                    ISymbol sym = spr.EsriSymbol;

                    object key = ids[0];
                    List<object> refValues = new List<object>();
                    for (int j = 1; j < ids.Count(); j++)
                    {
                        refValues.Add(ids[j]);
                    }
                    
                    vrm.Add(key, sym, " < 100");
                    vrm.AddReferanceValue2(key, refValues);
                }
            }
            {
                var ids = values.Where(entry => (double.Parse(entry.Value) >= 100)).Select(v => string.Format("{0}", v.Key)).ToList();
                if (ids.Count > 0)
                {
                    EAGL.Display.SimplePointSymbol spr = new EAGL.Display.SimplePointSymbol();
                    spr.Color = Color.Red;
                    spr.Style = esriSimpleMarkerStyle.esriSMSCircle;
                    spr.size = 4;
                    ISymbol sym = spr.EsriSymbol;

                    object key = ids[0];
                    List<object> refValues = new List<object>();
                    for (int j = 1; j < ids.Count(); j++)
                    {
                        refValues.Add(ids[j]);
                    }

                    vrm.Add(key, sym, " = 100");
                    vrm.AddReferanceValue2(key, refValues);
                }
            }
           
            if (layer != null || vrm.List.Count > 0)
            {   
                ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
            }
        }

        private void DrawAnnotateRenderer(Dictionary<string, string> values)
        {
            EAGL.Explore.MapExplorer mapexplorer = new EAGL.Explore.MapExplorer(this.m_map);
            IFeatureLayer layer = mapexplorer.GetLayer("JUNCTION") as IFeatureLayer;
            EAGL.Display.LabelRenderingMap LRM = new EAGL.Display.LabelRenderingMap();
            LRM.Extent = layer.AreaOfInterest;
            LRM.valueMap = values;

            //{
            //    var ids = values.Where(entry => (double.Parse(entry.Value) > 0) && (double.Parse(entry.Value) < 100)).Select(v => string.Format("'{0}'", v.Key)).ToList();
            //    if (ids.Count > 0)
            //    {
            //        IFormattedTextSymbol pText = new TextSymbolClass();
            //        pText.Font.Bold = false;
            //        pText.Font.Name = "굴림";
            //        pText.Size = 8;
            //        pText.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Green);

            //        string wheres = string.Format("{0} in (", "ID");
            //        wheres += string.Join(",", ids.ToArray());
            //        wheres += string.Format(")", string.Empty);

            //        LRM.Add("Mid", wheres, "ID", (ITextSymbol)pText);
            //    }
            //}
            {
                var ids = values.Where(entry => (double.Parse(entry.Value) > 0)).Select(v => string.Format("'{0}'", v.Key)).ToList();
                if (ids.Count > 0)
                {
                    IFormattedTextSymbol pText = new TextSymbolClass();
                    pText.Font.Bold = false;
                    pText.Font.Name = "굴림";
                    pText.Size = 8;
                    pText.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Black);

                    string wheres = string.Format("{0} in (", "ID");
                    wheres += string.Join(",", ids.ToArray());
                    wheres += string.Format(")", string.Empty);

                    LRM.Add("reach", wheres, "ID", (ITextSymbol)pText);
                }
            }
           
            if (layer != null || LRM.Count > 0)
            {
                LRM.FeatureLayer = layer;
                IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                ((IGeoFeatureLayer)layer).AnnotationProperties.Clear();
                for (int i = 0; i < ac.Count; i++)
                {
                    IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                    ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                    ((IGeoFeatureLayer)layer).AnnotationProperties.Add(alProps);
                }
                ((IGeoFeatureLayer)layer).DisplayAnnotation = chkLabel.Checked;
            }
        }

        private void chkLabel_CheckedChanged(object sender, EventArgs e)
        {
            EAGL.Explore.MapExplorer mapexplorer = new EAGL.Explore.MapExplorer(this.m_map);
            IFeatureLayer layer = mapexplorer.GetLayer("JUNCTION") as IFeatureLayer;
            ((IGeoFeatureLayer)layer).DisplayAnnotation = chkLabel.Checked;
            ArcManager.PartialRefresh(m_map, esriViewDrawPhase.esriViewBackground);
        }
    }
}
