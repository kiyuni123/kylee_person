﻿namespace WaterNet.WC_WaterTrouble
{
    partial class frmSelectNodes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.label_PP_CAT = new System.Windows.Forms.Label();
            this.comboBox_Scale = new System.Windows.Forms.ComboBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.ultraGrid_Node = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Node)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnClose);
            this.panelCommand.Controls.Add(this.label_PP_CAT);
            this.panelCommand.Controls.Add(this.comboBox_Scale);
            this.panelCommand.Controls.Add(this.btnSelect);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(303, 37);
            this.panelCommand.TabIndex = 18;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(247, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(53, 25);
            this.btnClose.TabIndex = 39;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label_PP_CAT
            // 
            this.label_PP_CAT.Location = new System.Drawing.Point(7, 12);
            this.label_PP_CAT.Name = "label_PP_CAT";
            this.label_PP_CAT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_PP_CAT.Size = new System.Drawing.Size(67, 17);
            this.label_PP_CAT.TabIndex = 38;
            this.label_PP_CAT.Text = "축척";
            // 
            // comboBox_Scale
            // 
            this.comboBox_Scale.FormattingEnabled = true;
            this.comboBox_Scale.Location = new System.Drawing.Point(82, 9);
            this.comboBox_Scale.Name = "comboBox_Scale";
            this.comboBox_Scale.Size = new System.Drawing.Size(106, 20);
            this.comboBox_Scale.TabIndex = 37;
            this.comboBox_Scale.Tag = "";
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelect.Location = new System.Drawing.Point(192, 6);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(53, 25);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "선택";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // ultraGrid_Node
            // 
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Node.DisplayLayout.Appearance = appearance61;
            this.ultraGrid_Node.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Node.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Node.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Node.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.ultraGrid_Node.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Node.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.ultraGrid_Node.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Node.DisplayLayout.MaxRowScrollRegions = 1;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_Node.DisplayLayout.Override.ActiveCellAppearance = appearance65;
            appearance66.BackColor = System.Drawing.SystemColors.Highlight;
            appearance66.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_Node.DisplayLayout.Override.ActiveRowAppearance = appearance66;
            this.ultraGrid_Node.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Node.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Node.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            appearance68.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Node.DisplayLayout.Override.CellAppearance = appearance68;
            this.ultraGrid_Node.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Node.DisplayLayout.Override.CellPadding = 0;
            appearance69.BackColor = System.Drawing.SystemColors.Control;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Node.DisplayLayout.Override.GroupByRowAppearance = appearance69;
            appearance70.TextHAlignAsString = "Left";
            this.ultraGrid_Node.DisplayLayout.Override.HeaderAppearance = appearance70;
            this.ultraGrid_Node.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Node.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Node.DisplayLayout.Override.RowAppearance = appearance71;
            this.ultraGrid_Node.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Node.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.ultraGrid_Node.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Node.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Node.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_Node.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid_Node.Location = new System.Drawing.Point(0, 37);
            this.ultraGrid_Node.Name = "ultraGrid_Node";
            this.ultraGrid_Node.Size = new System.Drawing.Size(303, 255);
            this.ultraGrid_Node.TabIndex = 19;
            this.ultraGrid_Node.Text = "ultraGrid1";
            // 
            // frmSelectNodes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 292);
            this.Controls.Add(this.ultraGrid_Node);
            this.Controls.Add(this.panelCommand);
            this.Name = "frmSelectNodes";
            this.Text = "선택객체";
            this.Load += new System.EventHandler(this.frmSelectNodes_Load);
            this.panelCommand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Node)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Label label_PP_CAT;
        private System.Windows.Forms.ComboBox comboBox_Scale;
        private System.Windows.Forms.Button btnSelect;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Node;
        private System.Windows.Forms.Button btnClose;
    }
}