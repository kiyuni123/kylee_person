﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WC_Common;
using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WC_WaterTrouble
{
    public partial class frmSelectNodes : Form
    {
        private IMapControl3 m_mapcontrol;
        private ESRI.ArcGIS.Carto.IMap m_map;

        private DataTable m_DataTable;

        private string m_iDNo = string.Empty;  //해석모델 ID

        public frmSelectNodes()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            ///차단제수밸브
            oUltraGridColumn = ultraGrid_Node.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FID";
            oUltraGridColumn.Header.Caption = "OBJECTID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = ultraGrid_Node.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE";
            oUltraGridColumn.Header.Caption = "NODE";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 120;

            oUltraGridColumn = ultraGrid_Node.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            FormManager.SetGridStyle(ultraGrid_Node);

            #endregion

            comboBox_Scale.Items.Add("100");
            comboBox_Scale.Items.Add("500");
            comboBox_Scale.Items.Add("1000");
            comboBox_Scale.Items.Add("2000");
            comboBox_Scale.Items.Add("3000");
            comboBox_Scale.Items.Add("5000");
            comboBox_Scale.Items.Add("10000");
            comboBox_Scale.Items.Add("15000");
            comboBox_Scale.Items.Add("20000");
            comboBox_Scale.Items.Add("30000");
            comboBox_Scale.Items.Add("50000");
            comboBox_Scale.Items.Add("100000");
        }
        #endregion 초기화설정

        #region Public Property
        /// <summary>
        /// Focus Map 설정
        /// </summary>
        public IMapControl3 mapControl
        {
            set
            {
                m_mapcontrol = value;
                m_map = m_mapcontrol.ActiveView.FocusMap;
            }
        }

        /// <summary>
        /// 선택 NODE 객체 테이블 설정
        /// </summary>
        public DataTable datatable
        {
            set
            {
                m_DataTable = value;
            }
        }
        #endregion Public Property

        /// <summary>
        /// 실행
        /// </summary>
        public string Open()
        {
            ultraGrid_Node.DataSource = m_DataTable.DefaultView;

            this.ShowDialog();

            return m_iDNo;
        }

        /// <summary>
        /// 그리드 행을 더블클릭시 위치이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row == null) return;
            ILayer pLayer = ArcManager.GetMapLayer(m_map, Convert.ToString(e.Row.Cells["NODE"].Value).ToUpper());
            if (pLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature(pLayer, Convert.ToInt32(e.Row.Cells["FID"].Value));
            if (pFeature != null)
            {
                int nscale = 0;
                if (Int32.TryParse(comboBox_Scale.Text, out nscale))
                {
                    ArcManager.SetMapScale(m_mapcontrol, (double)nscale);
                }

                ArcManager.MoveCenterAt(m_mapcontrol, pFeature);
                Application.DoEvents();
                ArcManager.FlashShape(m_mapcontrol.ActiveView, (IGeometry)pFeature.Shape, 10, 300);
            }

        }

        /// <summary>
        /// 화면 로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSelectNodes_Load(object sender, EventArgs e)
        {
            ultraGrid_Node.DoubleClickRow += new DoubleClickRowEventHandler(GridDoubleClickRow);
        }

        /// <summary>
        /// 화면 닫기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            m_iDNo = Convert.ToString(ultraGrid_Node.ActiveRow.Cells["ID"].Value);
            DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
