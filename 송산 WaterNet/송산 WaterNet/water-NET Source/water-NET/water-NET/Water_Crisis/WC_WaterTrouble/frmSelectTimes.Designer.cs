﻿namespace WaterNet.WC_WaterTrouble
{
    partial class frmSelectTimes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            this.panelCommand = new System.Windows.Forms.Panel();
            this.chkLabel = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.ultraGrid_Node = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_AccientTime = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.cbotime = new System.Windows.Forms.ComboBox();
            this.panelCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Node)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.chkLabel);
            this.panelCommand.Controls.Add(this.btnClose);
            this.panelCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCommand.Location = new System.Drawing.Point(0, 0);
            this.panelCommand.Name = "panelCommand";
            this.panelCommand.Size = new System.Drawing.Size(226, 37);
            this.panelCommand.TabIndex = 19;
            // 
            // chkLabel
            // 
            this.chkLabel.AutoSize = true;
            this.chkLabel.Location = new System.Drawing.Point(10, 11);
            this.chkLabel.Name = "chkLabel";
            this.chkLabel.Size = new System.Drawing.Size(72, 16);
            this.chkLabel.TabIndex = 40;
            this.chkLabel.Text = "라벨보기";
            this.chkLabel.UseVisualStyleBackColor = true;
            this.chkLabel.CheckedChanged += new System.EventHandler(this.chkLabel_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(149, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(74, 25);
            this.btnClose.TabIndex = 39;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Visible = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ultraGrid_Node
            // 
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid_Node.DisplayLayout.Appearance = appearance61;
            this.ultraGrid_Node.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid_Node.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Node.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Node.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.ultraGrid_Node.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid_Node.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.ultraGrid_Node.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid_Node.DisplayLayout.MaxRowScrollRegions = 1;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid_Node.DisplayLayout.Override.ActiveCellAppearance = appearance65;
            appearance66.BackColor = System.Drawing.SystemColors.Highlight;
            appearance66.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid_Node.DisplayLayout.Override.ActiveRowAppearance = appearance66;
            this.ultraGrid_Node.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid_Node.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Node.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            appearance68.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid_Node.DisplayLayout.Override.CellAppearance = appearance68;
            this.ultraGrid_Node.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid_Node.DisplayLayout.Override.CellPadding = 0;
            appearance69.BackColor = System.Drawing.SystemColors.Control;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid_Node.DisplayLayout.Override.GroupByRowAppearance = appearance69;
            appearance70.TextHAlignAsString = "Left";
            this.ultraGrid_Node.DisplayLayout.Override.HeaderAppearance = appearance70;
            this.ultraGrid_Node.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid_Node.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid_Node.DisplayLayout.Override.RowAppearance = appearance71;
            this.ultraGrid_Node.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid_Node.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.ultraGrid_Node.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid_Node.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid_Node.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid_Node.Location = new System.Drawing.Point(0, 156);
            this.ultraGrid_Node.Name = "ultraGrid_Node";
            this.ultraGrid_Node.Size = new System.Drawing.Size(222, 255);
            this.ultraGrid_Node.TabIndex = 20;
            this.ultraGrid_Node.Text = "ultraGrid1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbl_AccientTime);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.cbotime);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(226, 99);
            this.panel1.TabIndex = 24;
            // 
            // lbl_AccientTime
            // 
            this.lbl_AccientTime.AutoSize = true;
            this.lbl_AccientTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_AccientTime.ForeColor = System.Drawing.Color.Red;
            this.lbl_AccientTime.Location = new System.Drawing.Point(10, 16);
            this.lbl_AccientTime.Name = "lbl_AccientTime";
            this.lbl_AccientTime.Size = new System.Drawing.Size(67, 12);
            this.lbl_AccientTime.TabIndex = 26;
            this.lbl_AccientTime.Text = "사고시간 :";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(10, 44);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(61, 12);
            this.lblTime.TabIndex = 25;
            this.lblTime.Text = "경과시간 :";
            // 
            // cbotime
            // 
            this.cbotime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotime.FormattingEnabled = true;
            this.cbotime.Location = new System.Drawing.Point(77, 40);
            this.cbotime.Name = "cbotime";
            this.cbotime.Size = new System.Drawing.Size(136, 20);
            this.cbotime.TabIndex = 24;
            // 
            // frmSelectTimes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(226, 136);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ultraGrid_Node);
            this.Controls.Add(this.panelCommand);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmSelectTimes";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "해석결과 조회";
            this.Load += new System.EventHandler(this.frmSelectTimes_Load);
            this.panelCommand.ResumeLayout(false);
            this.panelCommand.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid_Node)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCommand;
        private System.Windows.Forms.Button btnClose;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid_Node;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_AccientTime;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.ComboBox cbotime;
        private System.Windows.Forms.CheckBox chkLabel;
    }
}