﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WC_Common;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using QuickGraph;

namespace WaterNet.WC_WaterTrouble
{
    public partial class frmTroubleMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        #region "변수 및 상수"
        private QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex> rtreeEdge = new QuickGraph.Geometries.Index.RTree<QuickGraph.Vertex>();
        private QuickGraph.Vertex node = null;

        #endregion "변수 및 상수"

        public frmTroubleMain()
        {
            InitializeComponent();
            InitializeSetting();
            this.Load +=new EventHandler(frmTroubleMain_Load);
            this.FormClosed += new FormClosedEventHandler(frmTroubleMain_FormClosed);
        }


        #region IForminterface 멤버

        public string FormID
        {
            get { return "수질사고관리"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        /// <summary>
        /// 초기화 설정
        /// </summary>
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
            pnlLayerFunc.Visible = false;
        }

        #region Private Property
      

        #endregion Private Property

        /// <summary>
        /// Open
        /// </summary>
        public override void Open()
        {
            base.Open();
            //추가적인 구현은 여기에....
            //초기 레이어 표시여부 설정
            InitializeMap();

        }

        /// <summary>
        /// 지도상에 레이어를 표시하지 않도록 설정
        /// </summary>
        private void InitializeMap()
        {
            IEnumLayer pEnumLayer = this.axMap.ActiveView.FocusMap.get_Layers(null, false);
            pEnumLayer.Reset();

            ILayer pLayer = (ILayer)pEnumLayer.Next();

            do
            {
                if (pLayer is IFeatureLayer)
                {
                    switch (pLayer.Name)
                    {
                        case "재염소처리지점":
                        case "중요시설":
                        case "감시지점":
                        case "누수지점":
                        case "민원지점":
                        case "관세척구간":
                        //case "밸브":
                        //case "유량계":
                        case "수압계":
                        case "소방시설":
                        case "스탠드파이프":
                        case "수도계량기":
                        case "표고점":
                        //case "급수관로":
                        //case "상수관로":
                        //case "정수장":
                        //case "가압장":
                        //case "취수장":
                        //case "수원지":
                        //case "배수지":
                        case "밸브실":
                        //case "철도":
                        case "도로경계선":
                        case "등고선":
                        case "담장":
                        case "법정읍면동":
                        //case "행정읍면동":
                        case "건물":
                        case "하천":
                        case "지방도":
                        case "국도":
                        case "고속도로":
                        case "지형지번":
                        //case "소블록":
                        //case "중블록":
                        //case "대블록":
                            pLayer.Visible = false;
                            break;
                        default:
                            break;
                    }
                }
                pLayer = pEnumLayer.Next();
            }
            while (pLayer != null);

        }
     
   

        /// <summary>
        /// 해석모델 불러오기 버튼 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelModel_Click(object sender, EventArgs e)
        {
            ///-------------------------------------------------------------------------
            //해석모델 선택화면
            WaterNetCore.frmINP_Title form = new WaterNetCore.frmINP_Title(new string[] { "RI", "ET", "WQ" });
            if (form.Open() == DialogResult.Cancel) return;
            if (WC_Global.crisisAnalysisOp.BaseModel != null)
            {
                if (((EpaSimulator.DBNetwork)WC_Global.crisisAnalysisOp.BaseModel).inpNUMBER.Equals(form.m_INP_NUMBER))
                {
                    MessageBox.Show(string.Format("({0}) 수리모델은 현재 실행중인 모델과 동일합니다.", form.m_INP_NUMBER));
                    return;
                }
            }

            frmSplashMsg oSplash = new frmSplashMsg();
            oSplash.Open();
            oSplash.Message = "수리해석모델 레이어를 불러오는 중입니다.\n\r잠시만 기다리십시오.";
            Application.DoEvents();
            try
            {
                #region 수리모델 구성(DB에 저장된 원본모델)
                //일차적으로 DB에 있는모델을 그대로 수리해석하여 결과를 저장한다.(비상공급관을 찾을때 사용됨)
                WC_Global.crisisAnalysisOp.BaseModel = new EpaSimulator.DBNetwork(form.m_INP_NUMBER);
                WC_Global.crisisAnalysisOp.BaseModel.makeNetwork();
                //WC_Global.crisisAnalysisOp.BaseModel.RunAnalysis();

                this.rtreeEdge.Clear();
                this.rtreeEdge.init();
                foreach (var v in WC_Global.crisisAnalysisOp.BaseModel.NETWORK.Nodes.Values)
                {
                    IGeometry geomtry = EpaNetwork.xUtils.getGeometryVertexEdge(v);
                    this.rtreeEdge.Add(EpaNetwork.xUtils.get_Rectangle(geomtry, 0.01), v);
                }
                #endregion

                #region 모델레이어 생성 및 맵로드
                CreateINPLayerManager2 inpManager = new CreateINPLayerManager2(WC_Global.crisisAnalysisOp.BaseModel);
                int flag = inpManager.Execute();
                if (flag == 0)
                {
                    MessageBox.Show("Error");
                    return;
                }
                else if (flag == -1 || flag == 1) //기존의 workspace 있거나, 정상적으로 생성됨
                {
                    this.Remove_INP_Layer(new string[] { "TANK", "RESERVOIR", "JUNCTION", "PIPE", "PUMP", "VALVE" });
                    Dictionary<string, IFeatureLayer> modelLayers = inpManager.ModelLayers;
                    this.Load_INP_Layer(modelLayers, new string[] { "TANK", "RESERVOIR", "JUNCTION", "PIPE", "PUMP", "VALVE" });
                }
                #endregion 모델레이어 생성 및 맵로드


                #region Extent범위로 지도확대/축소
                EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
                IFeatureLayer layer = mapExplorer.GetLayer("JUNCTION") as IFeatureLayer;
                ///Junction 렌더러 저장
                WC_Global.defaultJunctionRenderer = ArcManager.SerializeFeatureRenderer(((IGeoFeatureLayer)layer).Renderer);
                this.axMap.ActiveView.Extent = layer.AreaOfInterest;
                this.axMap.ActiveView.Refresh();
                #endregion Extent범위로 지도확대/축소
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oSplash.Close();
            }

            this.ClearVariable();  
            ModifyButton(sender);
        }

        /// <summary>
        /// 사고시간 설정 버튼 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAccident_Click(object sender, EventArgs e)
        {
            if (WC_Global.crisisAnalysisOp.BaseModel == null)
            {
                MessageBox.Show("수리모델불러오기를 실행하지 않았습니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            ModifyButton(sender);

            #region 사고시간 입력 화면
            Form form = Application.OpenForms["frmAccidentTime"];
            if (form == null) form = new frmAccidentTime();
            form.TopMost = true;
            form.StartPosition = FormStartPosition.CenterParent;
            form.Activate();
            string strTime = ((frmAccidentTime)form).Open();
            if (form.DialogResult == DialogResult.Yes) localVariable.m_AccidentTime = strTime;
            #endregion 사고시간 입력 화면
        }

        /// <summary>
        /// 수질사고 지점 선택 버튼 실행(레저버)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBrokePoint_Click(object sender, EventArgs e)
        {
            if (WC_Global.crisisAnalysisOp.BaseModel == null)
            {
                MessageBox.Show("수리모델 불러오기를 실행하지 않았습니다.", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.ModifyButton(sender);
            toolActionCommand.Checked = true;
            base.toolActionCommand_Click(this, new EventArgs());

            //QuickGraph.Geometries.Rectangle env = this.rtreeEdge.getBounds();
            //IEnvelope rect = new EnvelopeClass();
            //rect.PutCoords(env.XMin, env.YMin, env.XMax, env.YMax);
            //rect.Expand(1.1, 1.1, true);
            //this.axMap.ActiveView.Extent = rect;
            //this.axMap.ActiveView.Refresh();
        }

        #region 지도 처리
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            if (!((axMap.CurrentTool == null) & (toolActionCommand.Checked))) return;
            if (WC_Global.crisisAnalysisOp.BaseModel == null)
            {
                MessageBox.Show("수리해석모델 불러오기를 실행하지 않았습니다", "안내", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (String.IsNullOrEmpty(localVariable.m_AccidentTime))
            {
                MessageManager.ShowInformationMessage("사고시각이 설정되지 않았습니다.");
                return;
            }

            #region 위치선택 - 별표표시
            IRubberBand pRubberBand = new RubberPointClass();
            IPoint pt = (IPoint)pRubberBand.TrackNew(this.axMap.ActiveView.ScreenDisplay, null);
            if (pt == null | pt.IsEmpty) return;

            List<Vertex> vs = this.rtreeEdge.Contains(EpaNetwork.xUtils.get_Rectangle(pt, (double)15.0));
            if (vs.Count == 0)
            {
                MessageManager.ShowInformationMessage("선택된 node 객체가 없습니다. 다시 선택하십시오.");
                return;
            }
            var v = vs.Where(entry => entry is EpaNetwork.xReservoir).Select(entry => entry);
            this.node = v.Count() > 0 ? v.First() : vs.First();

            ///기존 생성 객체 초기화
            this.ClearVariable();  
          
            IColor pColor = ArcManager.GetColor(255, 0, 0);
            ISymbol pSymbol = ArcManager.MakeCharacterMarkerSymbol(30, 94, 0.0, pColor);
            IElement pElement = new MarkerElementClass();
            pElement.Geometry = EpaNetwork.xUtils.getGeometryVertexEdge(this.node) as IGeometry;
            IMarkerElement pMarkerElement = pElement as IMarkerElement;
            pMarkerElement.Symbol = pSymbol as ICharacterMarkerSymbol;
            this.axMap.ActiveView.GraphicsContainer.AddElement(pElement, 0);
            ArcManager.FlashShape(axMap.ActiveView, pt as IPoint, 6);
            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics);
            #endregion 지도에 사고지점을 선택하고, 별표 표시 끝

            this.btnAnalysis_Click(btnAnalysis, new EventArgs());
        }

        /// <summary>
        /// 지도상에 마우스 이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void axMap_OnMouseMove(object sender, IMapControlEvents2_OnMouseMoveEvent e)
        {
            base.axMap_OnMouseMove(sender, e);
        }
        #endregion 지도 처리

        #region 지도제어 툴바
        /// <summary>
        /// 사고시간 입력화면을 표시하고, 사고시간을 멤버변수에 저장한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void toolActionCommand_Click(object sender, EventArgs e)
        {
            //if (toolActionCommand.Checked)
            //{
            //    ///사고시간 입력 화면
            //    Form form = Application.OpenForms["frmAccidentTime"];
            //    if (form == null) form = new frmAccidentTime();
            //    //((frmAccidentTime)form).TopLevel = false;
            //    //((frmAccidentTime)form).Parent = this;
            //    ((frmAccidentTime)form).TopMost = true;
            //    //((frmAccidentTime)form).BringToFront();
            //    ((frmAccidentTime)form).Activate();
            //    string strTime = ((frmAccidentTime)form).Open();
            //    if (form.DialogResult == DialogResult.Yes)
            //    {
            //        m_AccidentTime = strTime;
            //    }
            //    else m_AccidentTime = string.Empty;

            //}

            base.toolActionCommand_Click(sender, e);
        }
        #endregion 지도제어 툴바


        /// <summary>
        /// 해석시 생성변수 및 메뉴 초기화
        /// 지도화면 선택객체 및 그래픽객체 초기화
        /// </summary>
        private void ClearVariable()
        {
            ////분석결과 조회화면 및 팝업메뉴 삭제
            removeContextmenu();

            //기존의 지도화면 선택객체 및 그래픽객체 초기화
            ArcManager.ClearSelection(axMap.ActiveView.FocusMap);
            IGraphicsContainer pGraphicsContainer = axMap.ActiveView as IGraphicsContainer;
            pGraphicsContainer.DeleteAllElements();
            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeoSelection | esriViewDrawPhase.esriViewGraphics);
        }

        /// <summary>
        /// 기능버튼 BackColor 설정
        /// </summary>
        /// <param name="sender"></param>
        private void ModifyButton(object sender)
        {
            if (sender == null) return;
            Button button = sender as Button;
            if (button == null) return;

            switch (button.Name)
            {
                case "btnSelModel":
                    btnSelModel.BackColor = Color.FromArgb(14, 158, 193);
                    btnAccident.BackColor = Color.FromArgb(220, 232, 244);
                    btnBrokePoint.BackColor = Color.FromArgb(220, 232, 244);
                    break;
                case "btnAccident":
                    btnAccident.BackColor = Color.FromArgb(14, 158, 193);
                    btnBrokePoint.BackColor = Color.FromArgb(220, 232, 244);
                    break;
                case "btnBrokePoint":
                    btnBrokePoint.BackColor = Color.FromArgb(14, 158, 193);
                    break;
            }

        }

        /// <summary>
        /// 시간 문자열을 DataTime으로 반환한다.
        /// yyyy-mm-dd hh24:mi
        /// </summary>
        /// <param name="date">yyyy-mm-dd hh24:mi</param>
        /// <returns></returns>
        private DateTime getStringToDateTime(string date)
        {
            int year = Convert.ToInt32(date.Substring(0, 4));
            int month = Convert.ToInt32(date.Substring(5, 2));
            int day = Convert.ToInt32(date.Substring(8, 2));
            int hour = Convert.ToInt32(date.Substring(11, 2));
            int min = Convert.ToInt32(date.Substring(14, 2));
            int sec = 0;

            return new DateTime(year,month,day,hour,min,sec);
        }

        private DataTable SelectRealtimeFlow()
        {
            string inpnumber = ((EpaSimulator.DBNetwork)WC_Global.crisisAnalysisOp.BaseModel).inpNUMBER;
            DateTime date = getStringToDateTime(localVariable.m_AccidentTime);

            StringBuilder queryString = new StringBuilder();
            queryString.AppendLine("WITH TEMP_1                                                                                           ");
            queryString.AppendLine("AS                                                                                                    ");
            queryString.AppendLine(" (                                                                                                    ");
            queryString.AppendLine("   SELECT   A.FTR_IDN	AS BSM_IDN		                                                                  ");
            queryString.AppendLine("           ,TO_CHAR(D.TIMESTAMP,'YYYY-MM-DD HH24') AS DATE1                                           ");
            queryString.AppendLine("           ,ROUND(AVG(D.VALUE),4) AS VALUE                                                            ");
            queryString.AppendLine("     FROM                                                                                             ");
            queryString.AppendLine("           CM_LOCATION A                                                                              ");
            queryString.AppendLine("          ,(select b.loc_code, b.tagname, c.tag_desc from if_ihtags b, if_tag_gbn c where b.tagname = c.tagname and c.tag_gbn = 'FRI') B  ");
            queryString.AppendLine("          ,IF_GATHER_REALTIME D                                                                       ");
            queryString.AppendLine("          ,WH_TITLE    E                                                                              ");
            queryString.AppendLine("    WHERE A.FTR_CODE = 'BZ003'                                                                        ");
            //queryString.AppendLine("      AND A.PLOC_CODE = E.MFTRIDN                                                                     ");
            queryString.AppendLine("      AND E.INP_NUMBER = '" + inpnumber + "'");
            queryString.AppendLine("      AND A.LOC_CODE = B.LOC_CODE                                                                     ");
            queryString.AppendLine("      AND B.TAGNAME = D.TAGNAME and d.value > 0                                                       ");
            queryString.AppendLine("      AND D.TIMESTAMP     BETWEEN TO_DATE('" + WC_Common.WC_FunctionManager.GetNowDateTimeToFormatString(date.AddHours(-24)) + "', 'yyyy-mm-dd hh24:mi:ss') ");
            queryString.AppendLine("      AND                         TO_DATE('" + WC_Common.WC_FunctionManager.GetNowDateTimeToFormatString(date.AddHours(-1)) + "', 'yyyy-mm-dd hh24:mi:ss') ");
            queryString.AppendLine("   GROUP BY        A.FTR_IDN			                                                              ");
            queryString.AppendLine("                  ,TO_CHAR(D.TIMESTAMP,'YYYY-MM-DD HH24')                                             ");
            queryString.AppendLine("   ORDER BY BSM_IDN, TO_DATE(DATE1, 'YYYY-MM-DD HH24') ASC                                   ");
            queryString.AppendLine(" )                                                                                                    ");
            queryString.AppendLine("SELECT                                                                                                ");
            queryString.AppendLine("       TEMP_1.BSM_IDN AS BSM_IDN,                                                                     ");
            queryString.AppendLine("       TEMP_1.DATE1 AS DATE1,                                                                         ");
            queryString.AppendLine("       CASE WHEN TEMP_1.VALUE = 0 THEN 0                                                              ");
            queryString.AppendLine("            WHEN TEMP_2.VALUE = 0 THEN 0                                                              ");
            queryString.AppendLine("            ELSE ROUND(TEMP_1.VALUE/TEMP_2.VALUE, 4)                                                  ");
            queryString.AppendLine("       END AS VALUE                                                                                   ");
            queryString.AppendLine("  FROM                                                                                                ");
            queryString.AppendLine("       TEMP_1,                                                                                        ");
            queryString.AppendLine("       ( SELECT BSM_IDN, AVG(VALUE) AS VALUE                                       ");
            queryString.AppendLine("           FROM TEMP_1                                                                                ");
            queryString.AppendLine("         GROUP BY BSM_IDN ) TEMP_2                                                 ");
            queryString.AppendLine(" WHERE TEMP_1.BSM_IDN = TEMP_2.BSM_IDN                                                                ");
            queryString.AppendLine("ORDER BY  bsm_idn, date1 asc                                                                          ");

            DataTable table = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD);
                table = mapper.ExecuteScriptDataTable(queryString.ToString(),new IDataParameter[] {});
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
            finally
            {
                if (mapper != null) mapper.Close();
            }
            return table;
        }

     
        
        #region 팝업메뉴
        /// <summary>
        /// 분석결과 화면 보기버튼 클릭시 팝업메뉴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAnalysisView_MouseDown(object sender, MouseEventArgs e)
        {
            contextMenuPopup.Show((Button)sender, new System.Drawing.Point(e.Location.X, e.Location.Y));
        }

        /// <summary>
        /// 팝업메뉴 추가
        /// </summary>
        /// <param name="oform"></param>
        private void AddContextmenu(Form oform)
        {
            for (int i = 0; i < contextMenuPopup.Items.Count; i++)
            {
                Form form = contextMenuPopup.Items[i].Tag as Form;
                if (form.Name.Equals(oform.Name))
                {
                    if (form.Text.Equals(oform.Text))
                    {
                        removeAtContextmenu(i);
                        break;
                    }
                }
            }

            ToolStripItem item = new ToolStripMenuItem();
            item.Text = oform.Text;
            item.Tag = oform;
            item.ToolTipText = oform.Text;
            contextMenuPopup.Items.Add(item);

            item.Click += new EventHandler(toolStripMenuItemClick);
        }

        /// <summary>
        /// 팝업메뉴 Clear
        /// </summary>
        private void removeContextmenu()
        {
            if (contextMenuPopup.Items.Count == 0) return;
            for (int i = contextMenuPopup.Items.Count - 1; i >= 0; i--)
            {
                Form oform = contextMenuPopup.Items[i].Tag as Form;
                if (oform != null) oform.Close();

                contextMenuPopup.Items.RemoveAt(i);
            }
        }

        /// <summary>
        /// 팝업메뉴 Index 삭제 및 조회폼 삭제
        /// </summary>
        /// <param name="index"></param>
        private void removeAtContextmenu(int index)
        {
            if (contextMenuPopup.Items.Count == 0) return;

            Form oform = contextMenuPopup.Items[index].Tag as Form;
            if (oform != null) oform.Close();

            contextMenuPopup.Items.RemoveAt(index);

        }

        /// <summary>
        /// 팝업메뉴 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemClick(object sender, EventArgs e)
        {
            ToolStripItem item = sender as ToolStripItem;
            Form oform = item.Tag as Form;
            if (oform != null)
            {
                oform.BringToFront();
                oform.Activate();
                oform.Show();
            }
        }
        #endregion 팝업메뉴



        void frmTroubleMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            WC_Global.crisisAnalysisOp.Clear();
        }

        private void frmTroubleMain_Load(object sender, EventArgs e)
        {
            EpaNetwork.xGlobal.defaultDelimiter = EMFrame.statics.AppStatic.ZONE_GBN.Equals("광역") ? ':' : '|';

            this.btnAccident.Click +=new EventHandler(btnAccident_Click);
            this.btnAnalysis.Click += new EventHandler(btnAnalysis_Click);
            this.btnBrokePoint.Click +=new EventHandler(btnBrokePoint_Click);
            this.btnSelModel.Click +=new EventHandler(btnSelModel_Click);
            this.btnAnalysisView.MouseDown +=new MouseEventHandler(btnAnalysisView_MouseDown);
        }

        void btnAnalysis_Click(object sender, EventArgs e)
        {
            WC_Common.WC_SplashForm SplashForm = new WaterNet.WC_Common.WC_SplashForm();
            SplashForm.Message = "Trace 수질해석을 실행하고 있습니다. 잠시만 기다리세요!";
            SplashForm.Open();
            Application.DoEvents();

            DataTable flowData = SelectRealtimeFlow();
            try
            {
                ///workmodel이 변경되었으므로, basemodel로 복사하여 초기화
                WC_Global.crisisAnalysisOp.WorkModel = WC_Global.crisisAnalysisOp.BaseModel.Clone();

                ///Time Option
                WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.HYD_TSTEP_INDEX] = "1:00";
                WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.QUAL_TSTEP_INDEX] = "1:00";
                WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.PAT_TSTEP_INDEX] = "1:00";
                WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.PAT_START_INDEX] = "1:00";

                ///시간에따라 변경해야 하는부분(1-124)
                WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.DURATION_INDEX] = "1:00";
                WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.RPT_TSTEP_INDEX] = "1:00";
                WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.RPT_START_INDEX] = "1:00";

                /////Time Option
                //WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.DURATION_INDEX] = "124:00";
                //WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.QUAL_TSTEP_INDEX] = "1:00";
                //WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.RPT_TSTEP_INDEX] = "1:00";
                //WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.PAT_TSTEP_INDEX] = "1:00";
                ///Quality Trace
                WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.QUAL_PARAM_INDEX] = "Trace";
                WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Options.Data[EpaNetwork.xGlobal.TRACE_NODE_INDEX] = this.node.Id;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                SplashForm.Close();
                return;
            }
                
            try
            {
                ///Pattern 생성
                var fList = from r in flowData.AsEnumerable()
                            orderby r.Field<string>("DATE1")
                            group r by r.Field<string>("BSM_IDN") into gr
                            select new { Id = gr.Key, Post = gr };

                {
                    EpaNetwork.xPattern pattern = WC_Global.crisisAnalysisOp.WorkModel.NETWORK.FindPattern("dummy");
                    if (pattern == null)
                    {
                        pattern = new EpaNetwork.xPattern();
                        pattern.Id = "dummy";
                        pattern.description = "dummy";
                        pattern.multipliers.AddRange((new double[24] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }));
                        WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Patterns.Add("dummy", pattern);
                    }
                }

                foreach (var p in fList)
                {
                    EpaNetwork.xPattern pattern = WC_Global.crisisAnalysisOp.WorkModel.NETWORK.FindPattern(p.Id);
                    if (pattern == null)
                    {
                        pattern = new EpaNetwork.xPattern();
                        pattern.Id = p.Id;
                        pattern.description = p.Id;
                        pattern.multipliers.AddRange(p.Post.Select(r => EMFrame.utils.ConvertUtils.ToDouble(r["VALUE"])));
                        WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Patterns.Add(p.Id, pattern);
                    }
                    else
                    {
                        pattern.multipliers.Clear();
                        pattern.multipliers.AddRange(p.Post.Select(r => EMFrame.utils.ConvertUtils.ToDouble(r["VALUE"])));
                    }
                }

                ///Junction Pattern 생성
                foreach (var v in WC_Global.crisisAnalysisOp.WorkModel.NETWORK.Nodes.Values)
                {
                    EpaNetwork.xJunc junc = v as EpaNetwork.xJunc;
                    if (junc == null) continue;
                    string idn = junc.getTagstoken(2);

                    EpaNetwork.xPattern pattern = WC_Global.crisisAnalysisOp.WorkModel.NETWORK.FindPattern(idn);
                    junc.Data[EpaNetwork.xGlobal.JUNC_PATTERN_INDEX] = pattern == null ? "dummy" : idn;
                }

                //int err = WC_Global.crisisAnalysisOp.WorkModel.RunAnalysis();
                //if (err != 0)
                //{
                //    throw new Exception(EpaNetwork.xGlobal.ErrorCodes[err]);
                //}
                //this.DrawArrowRenderer(WC_Global.crisisAnalysisOp.WorkModel);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }
            finally
            {
                SplashForm.Close();
            }


            #region 해석결과 선택화면 보기
            Form oform = new frmSelectTimes();
            ((frmSelectTimes)oform).mapControl = (IMapControl3)axMap.Object;
            oform.TopLevel = false;
            oform.Parent = this;
            oform.TopMost = true;
            oform.BringToFront();
            oform.StartPosition = FormStartPosition.CenterParent;
            ((frmSelectTimes)oform).Open();
            AddContextmenu(oform);
            #endregion 해석결과 선택화면 보기
        }
    }

    public static class WC_Global
    {
        public static CrisisAnalysisOperator crisisAnalysisOp = new CrisisAnalysisOperator();
        public static byte[] defaultJunctionRenderer = null;
    }


    public class CrisisAnalysisOperator
    {
        private EAGL.Explore.MapExplorer _mapExplorer;
        private EpaSimulator.ISimulator _basemodel; //DB원본모델
        private EpaSimulator.ISimulator _workmodel;

        public CrisisAnalysisOperator() { }
        public CrisisAnalysisOperator(EpaSimulator.ISimulator model)
        {
            this._basemodel = model;
        }

        public EAGL.Explore.MapExplorer MapExplorer
        {
            get { return this._mapExplorer; }
            set { this._mapExplorer = value; }
        }

        public EpaSimulator.ISimulator WorkModel
        {
            get { return this._workmodel; }
            set { this._workmodel = value; }
        }

        public EpaSimulator.ISimulator BaseModel
        {
            get { return this._basemodel; }
            set
            {
                this._basemodel = value;
            }
        }

        public void Clear()
        {
            this._mapExplorer = null;
            this._basemodel = null;
            this._workmodel = null;
        }
    }
}
