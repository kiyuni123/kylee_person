﻿namespace WaterNet.WC_WaterTrouble
{
    partial class frmTroubleMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTroubleMain));
            this.btnSelModel = new System.Windows.Forms.Button();
            this.btnBrokePoint = new System.Windows.Forms.Button();
            this.btnAnalysisView = new System.Windows.Forms.Button();
            this.contextMenuPopup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnAccident = new System.Windows.Forms.Button();
            this.btnAnalysis = new System.Windows.Forms.Button();
            this.panelCommand.SuspendLayout();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmMapAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.tabTOC.SuspendLayout();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            this.tabPageTOC.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCommand
            // 
            this.panelCommand.Controls.Add(this.btnAnalysis);
            this.panelCommand.Controls.Add(this.btnAccident);
            this.panelCommand.Controls.Add(this.btnAnalysisView);
            this.panelCommand.Controls.Add(this.btnSelModel);
            this.panelCommand.Controls.Add(this.btnBrokePoint);
            // 
            // spcContents
            // 
            // 
            // axToolbar
            // 
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.DefaultGroupSettings.ForceSerialization = true;
            this.DockManagerCommand.DefaultPaneSettings.ForceSerialization = true;
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.TabIndex = 12;
            // 
            // spcTOC
            // 
            // 
            // tvBlock
            // 
            this.tvBlock.LineColor = System.Drawing.Color.Black;
            // 
            // spcIndexMap
            // 
            // 
            // axTOC
            // 
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            // 
            // axMap
            // 
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Size = new System.Drawing.Size(661, 524);
            this.axMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(this.axMap_OnMouseDown);
            this.axMap.OnMouseMove += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseMoveEventHandler(this.axMap_OnMouseMove);
            // 
            // btnSelModel
            // 
            this.btnSelModel.Location = new System.Drawing.Point(3, 5);
            this.btnSelModel.Name = "btnSelModel";
            this.btnSelModel.Size = new System.Drawing.Size(62, 46);
            this.btnSelModel.TabIndex = 5;
            this.btnSelModel.Text = "해석모델선택";
            this.btnSelModel.UseVisualStyleBackColor = true;
            // 
            // btnBrokePoint
            // 
            this.btnBrokePoint.Location = new System.Drawing.Point(3, 113);
            this.btnBrokePoint.Name = "btnBrokePoint";
            this.btnBrokePoint.Size = new System.Drawing.Size(62, 46);
            this.btnBrokePoint.TabIndex = 4;
            this.btnBrokePoint.Text = "사고지점";
            this.btnBrokePoint.UseVisualStyleBackColor = true;
            // 
            // btnAnalysisView
            // 
            this.btnAnalysisView.Location = new System.Drawing.Point(3, 219);
            this.btnAnalysisView.Name = "btnAnalysisView";
            this.btnAnalysisView.Size = new System.Drawing.Size(62, 46);
            this.btnAnalysisView.TabIndex = 7;
            this.btnAnalysisView.Text = "분석결과조회";
            this.btnAnalysisView.UseVisualStyleBackColor = true;
            this.btnAnalysisView.Visible = false;
            // 
            // contextMenuPopup
            // 
            this.contextMenuPopup.Name = "contextMenuPopup";
            this.contextMenuPopup.Size = new System.Drawing.Size(61, 4);
            // 
            // btnAccident
            // 
            this.btnAccident.Location = new System.Drawing.Point(3, 59);
            this.btnAccident.Name = "btnAccident";
            this.btnAccident.Size = new System.Drawing.Size(62, 46);
            this.btnAccident.TabIndex = 8;
            this.btnAccident.Text = "사고시간 선정";
            this.btnAccident.UseVisualStyleBackColor = true;
            // 
            // btnAnalysis
            // 
            this.btnAnalysis.Location = new System.Drawing.Point(3, 166);
            this.btnAnalysis.Name = "btnAnalysis";
            this.btnAnalysis.Size = new System.Drawing.Size(62, 46);
            this.btnAnalysis.TabIndex = 9;
            this.btnAnalysis.Text = "Trace  수질해석";
            this.btnAnalysis.UseVisualStyleBackColor = true;
            this.btnAnalysis.Visible = false;
            // 
            // frmTroubleMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 583);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "frmTroubleMain";
            this.Text = "frmTroubleMain";
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaLeft, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaRight, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaBottom, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaTop, 0);
            this.Controls.SetChildIndex(this.windowDockingArea1, 0);
            this.Controls.SetChildIndex(this.spcContents, 0);
            this.Controls.SetChildIndex(this._frmMapAutoHideControl, 0);
            this.panelCommand.ResumeLayout(false);
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.Panel2.PerformLayout();
            this.spcContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmMapAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.tabTOC.ResumeLayout(false);
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            this.tabPageTOC.ResumeLayout(false);
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSelModel;
        private System.Windows.Forms.Button btnBrokePoint;
        private System.Windows.Forms.Button btnAnalysisView;
        private System.Windows.Forms.ContextMenuStrip contextMenuPopup;
        private System.Windows.Forms.Button btnAccident;
        private System.Windows.Forms.Button btnAnalysis;
    }
}