﻿namespace WaterNet.WE_CaseSenarioSimulate
{
    partial class frmCaseAnalysisData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.cboCASEID = new System.Windows.Forms.ComboBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblCASE = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabNode = new System.Windows.Forms.TabPage();
            this.tabLink = new System.Windows.Forms.TabPage();
            this.txtCaseName = new System.Windows.Forms.TextBox();
            this.ultraGridNode = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGridLink = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabNode.SuspendLayout();
            this.tabLink.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridNode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridLink)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.cboCASEID);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(968, 34);
            this.panel1.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 41;
            this.label10.Text = "번호";
            // 
            // cboCASEID
            // 
            this.cboCASEID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCASEID.FormattingEnabled = true;
            this.cboCASEID.Location = new System.Drawing.Point(54, 7);
            this.cboCASEID.Name = "cboCASEID";
            this.cboCASEID.Size = new System.Drawing.Size(142, 20);
            this.cboCASEID.TabIndex = 40;
            this.cboCASEID.SelectedValueChanged += new System.EventHandler(this.cboCASEID_SelectedValueChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 34);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtCaseName);
            this.splitContainer1.Panel1.Controls.Add(this.lblCASE);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(968, 428);
            this.splitContainer1.SplitterDistance = 63;
            this.splitContainer1.TabIndex = 43;
            // 
            // lblCASE
            // 
            this.lblCASE.AutoSize = true;
            this.lblCASE.Location = new System.Drawing.Point(12, 14);
            this.lblCASE.Name = "lblCASE";
            this.lblCASE.Size = new System.Drawing.Size(61, 12);
            this.lblCASE.TabIndex = 43;
            this.lblCASE.Text = "해석조건 :";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabNode);
            this.tabControl1.Controls.Add(this.tabLink);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(968, 361);
            this.tabControl1.TabIndex = 0;
            // 
            // tabNode
            // 
            this.tabNode.Controls.Add(this.ultraGridNode);
            this.tabNode.Location = new System.Drawing.Point(4, 22);
            this.tabNode.Name = "tabNode";
            this.tabNode.Padding = new System.Windows.Forms.Padding(3);
            this.tabNode.Size = new System.Drawing.Size(960, 335);
            this.tabNode.TabIndex = 0;
            this.tabNode.Text = "NODE";
            this.tabNode.UseVisualStyleBackColor = true;
            // 
            // tabLink
            // 
            this.tabLink.Controls.Add(this.ultraGridLink);
            this.tabLink.Location = new System.Drawing.Point(4, 22);
            this.tabLink.Name = "tabLink";
            this.tabLink.Padding = new System.Windows.Forms.Padding(3);
            this.tabLink.Size = new System.Drawing.Size(960, 335);
            this.tabLink.TabIndex = 1;
            this.tabLink.Text = "LINK";
            this.tabLink.UseVisualStyleBackColor = true;
            // 
            // txtCaseName
            // 
            this.txtCaseName.Location = new System.Drawing.Point(77, 9);
            this.txtCaseName.Multiline = true;
            this.txtCaseName.Name = "txtCaseName";
            this.txtCaseName.ReadOnly = true;
            this.txtCaseName.Size = new System.Drawing.Size(875, 46);
            this.txtCaseName.TabIndex = 45;
            // 
            // ultraGridNode
            // 
            this.ultraGridNode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridNode.Location = new System.Drawing.Point(3, 3);
            this.ultraGridNode.Name = "ultraGridNode";
            this.ultraGridNode.Size = new System.Drawing.Size(954, 329);
            this.ultraGridNode.TabIndex = 0;
            this.ultraGridNode.Text = "ultraGrid1";
            // 
            // ultraGridLink
            // 
            this.ultraGridLink.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridLink.Location = new System.Drawing.Point(3, 3);
            this.ultraGridLink.Name = "ultraGridLink";
            this.ultraGridLink.Size = new System.Drawing.Size(954, 329);
            this.ultraGridLink.TabIndex = 1;
            this.ultraGridLink.Text = "ultraGrid1";
            // 
            // frmCaseAnalysisData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 462);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Name = "frmCaseAnalysisData";
            this.Text = "제외 CASE 수리해석 결과 보기";
            this.Load += new System.EventHandler(this.frmCaseAnalysisData_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabNode.ResumeLayout(false);
            this.tabLink.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridNode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridLink)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboCASEID;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblCASE;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabNode;
        private System.Windows.Forms.TabPage tabLink;
        private System.Windows.Forms.TextBox txtCaseName;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridNode;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridLink;
    }
}