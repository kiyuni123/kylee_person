﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;
using WaterNet.WaterNetCore;
using WaterNet.WE_SimCommon;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion

namespace WaterNet.WE_CaseSenarioSimulate
{
    public partial class frmCaseSenario : Form, WaterNet.WaterNetCore.IForminterface
    {
        private WaterNetCore.OracleDBManager m_oDBManager = new OracleDBManager();

        private DataTable m_pumptable = null; //펌프그리드 테이블
        //private DataTable m_CaseTable = new DataTable(); //해석 CASE 테이블

        //해석결과를 DataSet 목록으로 저장한다. : DataSet Key = ID:Status, ID:Status,....
        private List<DataSet> m_AnalysisDataset = new List<DataSet>();
        private List<DataSet> m_AnalysisDeletedDataset = new List<DataSet>(); //수압이 0이하인 해석결과를 저장한다.

        private DataTable m_WH_PUMP = null;
        private DataTable m_WH_VALVE = null;
        private DataTable m_WH_PIPE = null;

        //private ArrayList m_junctions = new ArrayList(); ///수용가그리드 목록
        public frmCaseSenario()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            m_oDBManager.Open();

            StringBuilder oStringBuilder = new StringBuilder();
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            ///펌프조건 설정 그리드
            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "INP_NUMBER";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_NAME";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 150;

            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIM_GBN";
            oUltraGridColumn.Header.Caption = "설정";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT CODE AS CODE, CODE_NAME AS VALUE FROM CM_CODE WHERE PCODE = '6101' AND CODE NOT IN ('000002','000005')");
            oUltraGridColumn.ValueList = WaterNet.WaterNetCore.FormManager.SetValueList(oStringBuilder.ToString());

            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "GBN";
            oUltraGridColumn.Header.Caption = "GBN";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE1";
            oUltraGridColumn.Header.Caption = "전단";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE2";
            oUltraGridColumn.Header.Caption = "후단";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "태그명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridPumpCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_ID";
            oUltraGridColumn.Header.Caption = "PUMPSTATION_ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;


            ///밸브조건 설정 그리드
            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "INP_NUMBER";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_NAME";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_ID";
            oUltraGridColumn.Header.Caption = "PUMPSTATION_ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 120;

            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIM_GBN";
            oUltraGridColumn.Header.Caption = "설정";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT CODE AS CODE, CODE_NAME AS VALUE FROM CM_CODE WHERE PCODE = '6101' AND CODE NOT IN ('000003','000004')");
            oUltraGridColumn.ValueList = WaterNet.WaterNetCore.FormManager.SetValueList(oStringBuilder.ToString());

         
            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE1";
            oUltraGridColumn.Header.Caption = "전단";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE2";
            oUltraGridColumn.Header.Caption = "후단";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "GBN";
            oUltraGridColumn.Header.Caption = "GBN";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridValveCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "태그명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            ///사용량 조건 그리드
            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "INP_NUMBER";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_NAME";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_ID";
            oUltraGridColumn.Header.Caption = "PUMPSTATION_ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TAGNAME";
            oUltraGridColumn.Header.Caption = "태그명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 0;

            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 120;

            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 200;

            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIM_FLOW";
            oUltraGridColumn.Header.Caption = "유량(㎥/h)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MIN_PRESS";
            oUltraGridColumn.Header.Caption = "최저한계수압(kg/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PRESSURE";
            oUltraGridColumn.Header.Caption = "수압(kg/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridUsedCondition.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "GBN";
            oUltraGridColumn.Header.Caption = "GBN";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            ///펌프가동 최적 조합그리드  GridOptimum
            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "INP_NUMBER";
            oUltraGridColumn.Header.Caption = "INP_NUMBER";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "펌프/밸브명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 150;

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "GBN";
            oUltraGridColumn.Header.Caption = "GBN";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_ID";
            oUltraGridColumn.Header.Caption = "PUMPSTATION_ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_NAME";
            oUltraGridColumn.Header.Caption = "PUMPSTATION_NAME";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE1";
            oUltraGridColumn.Header.Caption = "전단";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "NODE2";
            oUltraGridColumn.Header.Caption = "후단";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "RANK1";
            oUltraGridColumn.Header.Caption = "가동";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group1";

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENERGY1";
            oUltraGridColumn.Header.Caption = "전력량(kWh)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group1";

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "UNITCOST1";
            oUltraGridColumn.Header.Caption = "원단위(kWh/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group1";

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "RANK2";
            oUltraGridColumn.Header.Caption = "가동";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group2";

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENERGY2";
            oUltraGridColumn.Header.Caption = "전력량(kWh)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group2";

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "UNITCOST2";
            oUltraGridColumn.Header.Caption = "원단위(kWh/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group2";

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "RANK3";
            oUltraGridColumn.Header.Caption = "가동";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group3";

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENERGY3";
            oUltraGridColumn.Header.Caption = "전력량(kWh)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group3";

            oUltraGridColumn = GridOptimum.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "UNITCOST3";
            oUltraGridColumn.Header.Caption = "원단위(kWh/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group3";

       
            //-----------------------------------------------------------

            FormManager.SetGridStyle(GridPumpCondition);
            FormManager.SetGridStyle(GridValveCondition);
            FormManager.SetGridStyle(GridUsedCondition);
            FormManager.SetGridStyle(GridCase);
            FormManager.SetGridStyle(GridOptimum);
            GridOptimum.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            GridOptimum.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            GridOptimum.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;

            WE_FunctionManager.SetGridStyle_ColumeAllowEdit(GridPumpCondition, 4); //CHECKED
            WE_FunctionManager.SetGridStyle_ColumeAllowEdit(GridValveCondition, 5); //CHECKED
            WE_FunctionManager.SetGridStyle_ColumeAllowEdit(GridUsedCondition, 6); //CHECKED
            WE_FunctionManager.SetGridStyle_ColumeAllowEdit(GridUsedCondition, 7); //CHECKED


            #endregion 그리드 설정

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT * FROM WH_PUMPS WHERE INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')");
            m_WH_PUMP = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT * FROM WH_VALVES WHERE INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')");
            m_WH_VALVE = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT * FROM WH_PIPES WHERE INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')");
            m_WH_PIPE = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            //m_CaseTable.Columns.Add("NAME", typeof(System.String));
            //m_CaseTable.Columns.Add("OBJECT", typeof(System.Object));
            //GridCase.DataSource = m_CaseTable;




            ///사업장 콤보박스 설정
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("select distinct a.pumpstation_id cd,  a.pumpstation_name cdnm ");
            oStringBuilder.AppendLine("  from we_pump_station a                                             ");
            oStringBuilder.AppendLine("      ,we_pump b                                                     ");
            oStringBuilder.AppendLine(" where a.pumpstation_id = b.pumpstation_id                           ");
            oStringBuilder.AppendLine(" order by pumpstation_name asc");
            FormManager.SetComboBoxEX(cboBzs, oStringBuilder.ToString(), false);

        }
        #endregion 초기화설정
        private void aa()
        {
            DisplaySetDatasourceGrid();
            ///해석결과 테이블
            SetGridGroup();
            SetSummaryRows();
            GridOptimum.DataSource = CreateOptimunData().DefaultView;
        }

        public void SetSummaryRows()
        {
            this.GridOptimum.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            foreach (UltraGridColumn column in GridOptimum.DisplayLayout.Bands[0].Columns)
            {
                SummarySettings summary = null;
                switch (column.Key)
                {
                    case "UNITCOST1":
                    case "UNITCOST2":
                    case "UNITCOST3":
                        summary = this.GridOptimum.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Average, column, SummaryPosition.UseSummaryPositionColumn);
                        summary.DisplayFormat = "{0:###,##0.00000}";
                        summary.SummaryDisplayArea = SummaryDisplayAreas.Top | SummaryDisplayAreas.GroupByRowsFooter;
                        summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                        summary.Appearance.BackColor = Color.YellowGreen;
                        summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        summary.Key = column.Key + "_AVG";
                        break;
                    case "ENERGY1":
                    case "ENERGY2":
                    case "ENERGY3":
                        summary = this.GridOptimum.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, column, SummaryPosition.UseSummaryPositionColumn);
                        summary.DisplayFormat = "{0:###,##0.00}";
                        summary.SummaryDisplayArea = SummaryDisplayAreas.Top | SummaryDisplayAreas.GroupByRowsFooter;
                        summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                        summary.Appearance.BackColor = Color.YellowGreen;
                        summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        summary.Key = column.Key + "_SUM";
                        //summary.DisplayFormat = "합계";

                        break;
                }
            }
        }
        /// <summary>
        /// 그리드 설정 : 그룹
        /// </summary>
        private void SetGridGroup()
        {
            ArrayList columns = new ArrayList();
            columns.Add("INP_NUMBER"); columns.Add("ID"); columns.Add("REMARK"); columns.Add("GBN"); columns.Add("TAGNAME"); 
            WE_SimCommon.WE_FunctionManager.AddColumnGroup(this.GridOptimum, "NO0", "기본정보", columns);

            columns.Clear();
            columns.Add("RANK1"); columns.Add("ENERGY1"); columns.Add("UNITCOST1");
            WE_SimCommon.WE_FunctionManager.AddColumnGroup(this.GridOptimum, "NO1", "1순위", columns);

            columns.Clear();
            columns.Add("RANK2"); columns.Add("ENERGY2"); columns.Add("UNITCOST2");
            WE_SimCommon.WE_FunctionManager.AddColumnGroup(this.GridOptimum, "NO2", "2순위", columns);

            columns.Clear();
            columns.Add("RANK3"); columns.Add("ENERGY3"); columns.Add("UNITCOST3");
            WE_SimCommon.WE_FunctionManager.AddColumnGroup(this.GridOptimum, "NO3", "3순위", columns);
        }

        //------------------------------------------------------------------
        //메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.
        //FormID : 탭에 보여지는 이름
        //FormKey : 현재 프로젝트 이름
        //------------------------------------------------------------------
        #region IForminterface 멤버

        public string FormID
        {
            get { return "펌프 최적조합 분석"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion IForminterface 멤버

        public bool Open()
        {
            this.Show();

            return true;
        }

        /// <summary>
        /// 그리드에 초기설정 값을 생성한다.
        /// </summary>
        private void DisplaySetDatasourceGrid()
        {
            Hashtable param = new Hashtable();
            param.Add("PUMPSTATION_ID", this.cboBzs.SelectedValue);
            this.GetPumpConditionData(param);
            this.GetValveConditionData(param);
            this.GetusedConditionData(param);
        }

        /// <summary>
        /// 펌프 그리드 데이터 설정하기
        /// </summary>
        public void GetPumpConditionData(Hashtable param)
        {
            ///펌프 조건 설정
            StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.AppendLine("SELECT B.INP_NUMBER, B.CHPDVICD, B.ID, B.TITLE, B.NODE1, B.NODE2, 'PUMP' AS GBN ");
            //oStringBuilder.AppendLine("            , decode(upper(b.status),'OPEN','000003','CLOSED','000004') AS SIM_GBN");
            //oStringBuilder.AppendLine("     ,C.VALUE1 FTR_IDN, A.PUMPSTATION_ID, A.PUMPSTATION_NAME                                   ");
            //oStringBuilder.AppendLine("FROM                                                                                           ");
            //oStringBuilder.AppendLine("    (SELECT A.PUMPSTATION_ID, A.PUMPSTATION_NAME, B.PUMP_FTR_IDN                               ");
            //oStringBuilder.AppendLine("       FROM WE_PUMP_STATION A                                                                  ");
            //oStringBuilder.AppendLine("           ,WE_PUMP B                                                                          ");
            //oStringBuilder.AppendLine("      WHERE A.PUMPSTATION_ID = B.PUMPSTATION_ID ) A                                            ");
            //oStringBuilder.AppendLine("    ,(SELECT A.INP_NUMBER AS INP_NUMBER, A.CHPDVICD, A.ID, A.TITLE, B.NODE1, B.NODE2, nvl(c.status_setting, 'OPEN') status           ");
            //oStringBuilder.AppendLine("       FROM WH_CHECKPOINT A, WH_PUMPS B, WH_STATUS C                                                        ");
            //oStringBuilder.AppendLine("      WHERE A.INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE USE_GBN = 'WH')             ");
            //oStringBuilder.AppendLine("        AND A.INP_NUMBER = B.INP_NUMBER                                                        ");
            //oStringBuilder.AppendLine("        AND b.INP_NUMBER = c.INP_NUMBER(+)                                                     ");
            //oStringBuilder.AppendLine("        AND A.CHPDVICD = '000004'                                                              ");
            //oStringBuilder.AppendLine("        AND A.ID = B.ID AND B.ID = C.ID(+)) B                                                                     ");
            //oStringBuilder.AppendLine("    ,WH_TAGS C                                                                                 ");
            //oStringBuilder.AppendLine("WHERE B.INP_NUMBER = C.INP_NUMBER                                                              ");
            //oStringBuilder.AppendLine("		AND B.ID = C.ID                                                                            ");
            //oStringBuilder.AppendLine("		AND C.TYPE = 'LINK'                                                                        ");
            //oStringBuilder.AppendLine("		AND A.PUMP_FTR_IDN = C.VALUE1                                                              ");
            oStringBuilder.AppendLine("SELECT B.INP_NUMBER, A.ID, A.REMARK, B.NODE1, B.NODE2, 'PUMP' AS GBN , A.TAGNAME                                   ");
            oStringBuilder.AppendLine("       , DECODE(UPPER(B.STATUS),'OPEN','000003','CLOSED','000004') AS SIM_GBN                                      ");
            oStringBuilder.AppendLine("       , A.PUMPSTATION_ID, A.PUMPSTATION_NAME                                                                      ");
            oStringBuilder.AppendLine("FROM                                                                                                               ");
            oStringBuilder.AppendLine("    (SELECT A.PUMPSTATION_ID, A.PUMPSTATION_NAME, B.ID, B.REMARK, B.TAGNAME                                        ");
            oStringBuilder.AppendLine("       FROM WE_PUMP_STATION A                                                                                      ");
            oStringBuilder.AppendLine("           ,WE_PUMP B                                                                                              ");
            oStringBuilder.AppendLine("      WHERE A.PUMPSTATION_ID = B.PUMPSTATION_ID                                                                    ");
            oStringBuilder.AppendLine("        AND A.PUMPSTATION_ID = '" + param["PUMPSTATION_ID"] + "') A                                                ");
            oStringBuilder.AppendLine("    ,(SELECT B.INP_NUMBER AS INP_NUMBER,B.ID, B.NODE1, B.NODE2, NVL(C.STATUS_SETTING, 'OPEN') STATUS               ");
            oStringBuilder.AppendLine("       FROM WH_PUMPS B, WH_STATUS C                                                                                ");
            oStringBuilder.AppendLine("      WHERE B.INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')                                ");
            oStringBuilder.AppendLine("        AND B.INP_NUMBER = C.INP_NUMBER(+)                                                                         ");
            oStringBuilder.AppendLine("        AND B.ID = C.ID(+)                                                                                         ");
            oStringBuilder.AppendLine("        ) B                                                                                                        ");
            oStringBuilder.AppendLine("WHERE A.ID  = B.ID                                                                                                 ");
            IDataParameter[] parameters =  {
                     new OracleParameter("PUMPSTATION_ID", OracleDbType.Varchar2)
                };
            parameters[0].Value = param["PUMPSTATION_ID"];

            m_pumptable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT A.ID                                                                                            ");
            oStringBuilder.AppendLine("       ,DECODE(B.VALUE,'1','000003','000004')        AS SIM_GBN                                        ");
            oStringBuilder.AppendLine("  FROM                                                                                                 ");
            oStringBuilder.AppendLine("        (SELECT A.PUMPSTATION_ID, A.PUMPSTATION_NAME, B.ID, B.REMARK, B.TAGNAME                        ");
            oStringBuilder.AppendLine("           FROM WE_PUMP_STATION A                                                                      ");
            oStringBuilder.AppendLine("               ,WE_PUMP B                                                                        ");
            oStringBuilder.AppendLine("          WHERE A.PUMPSTATION_ID = B.PUMPSTATION_ID                                                    ");
            oStringBuilder.AppendLine("        AND A.PUMPSTATION_ID = '" + param["PUMPSTATION_ID"] + "') A                                    ");
            oStringBuilder.AppendLine("      ,IF_GATHER_REALTIME B                                                                            ");
            oStringBuilder.AppendLine("  WHERE B.TIMESTAMP  = (SELECT MAX(TO_DATE(TARGET_DATE,'YYYYMMDDHH24MISS')) FROM WH_RPT_MASTER         ");
            oStringBuilder.AppendLine("                         WHERE AUTO_MANUAL = 'A'                                                       ");
            oStringBuilder.AppendLine("                           AND INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y'))  ");
            oStringBuilder.AppendLine("    AND A.TAGNAME = B.TAGNAME                                                                          ");

            //oStringBuilder.AppendLine("SELECT   A.ID                                                                                       ");
            //oStringBuilder.AppendLine("         ,DECODE(D.VALUE,'1','000003','000004')        AS SIM_GBN                                    ");
            //oStringBuilder.AppendLine("	FROM   WH_PUMPS                A                                                                   ");
            //oStringBuilder.AppendLine("	       ,WH_TAGS                B                                                                   ");
            //oStringBuilder.AppendLine("	       ,IF_TAG_MAPPING         C                                                                   ");
            //oStringBuilder.AppendLine("	       ,IF_GATHER_REALTIME     D                                                                   ");
            //oStringBuilder.AppendLine("	WHERE    A.INP_NUMBER            = (SELECT INP_NUMBER FROM WH_TITLE WHERE USE_GBN = 'WH')         ");
            //oStringBuilder.AppendLine("		AND      C.FTR_GBN               = '000004'                                                      ");
            //oStringBuilder.AppendLine("		AND      D.TIMESTAMP             = (SELECT MAX(TO_DATE(TARGET_DATE,'YYYYMMDDHH24MISS')) FROM WH_RPT_MASTER       ");
            //oStringBuilder.AppendLine("		                                     WHERE AUTO_MANUAL = 'A'                                                     ");
            //oStringBuilder.AppendLine("		                                       AND INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE USE_GBN = 'WH'))  ");
            //oStringBuilder.AppendLine("		AND      A.INP_NUMBER            = B.INP_NUMBER                                                                  ");
            //oStringBuilder.AppendLine("		AND      A.ID                    = B.ID                                                                          ");
            //oStringBuilder.AppendLine("		AND      B.VALUE1                = C.FTR_IDN                                                                     ");
            //oStringBuilder.AppendLine("		AND      C.TAGNAME               = D.TAGNAME                                                                     ");
            DataTable tempPump = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            foreach (DataRow row in m_pumptable.Rows)
            {
                foreach (DataRow temprow in tempPump.Rows)
                {
                    if (Convert.ToString(row["ID"]) == Convert.ToString(temprow["ID"]))
                    {
                        row["SIM_GBN"] = temprow["SIM_GBN"];
                        break;
                    }
                }
            }
            GridPumpCondition.DataSource = m_pumptable;

        }

        /// <summary>
        /// 밸브 그리드 데이터 설정하기
        /// </summary>
        private void GetValveConditionData(Hashtable param)
        {
            ///밸브 조건 설정
            StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.AppendLine("SELECT B.INP_NUMBER, B.CHPDVICD, B.ID, B.TITLE, B.NODE1, B.NODE2, 'VALVE' AS GBN                                   ");
            //oStringBuilder.AppendLine("            , decode(upper(b.status),'OPEN','000002','CLOSED','000005') AS SIM_GBN");
            //oStringBuilder.AppendLine("       ,A.VALUE1 FTR_IDN                                                                                           ");
            //oStringBuilder.AppendLine("  FROM  WH_TAGS A                                                                                                  ");
            //oStringBuilder.AppendLine("      ,(SELECT A.INP_NUMBER AS INP_NUMBER, A.CHPDVICD, A.ID, A.TITLE, B.NODE1, B.NODE2 ,decode(c.status_setting, null, 'OPEN', upper(c.status_setting)) status   ");
            //oStringBuilder.AppendLine("         FROM WH_CHECKPOINT A, WH_VALVES B , WH_STATUS C                                                           ");
            //oStringBuilder.AppendLine("        WHERE A.INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE USE_GBN = 'WH')                                ");
            //oStringBuilder.AppendLine("          AND A.INP_NUMBER = B.INP_NUMBER                                                                          ");
            //oStringBuilder.AppendLine("          AND B.INP_NUMBER = C.INP_NUMBER(+)                                                                         ");
            //oStringBuilder.AppendLine("          AND A.CHPDVICD(+) = '000003'                                                                                ");
            //oStringBuilder.AppendLine("          AND A.ID = B.ID and b.id = c.id(+) ) B                                                                       ");
            //oStringBuilder.AppendLine("  WHERE A.INP_NUMBER = B.INP_NUMBER                                                                                ");
            //oStringBuilder.AppendLine("    AND A.TYPE = 'LINK'                                                                                            ");
            //oStringBuilder.AppendLine("    AND A.ID = B.ID                                                                                                ");
            oStringBuilder.AppendLine("SELECT B.INP_NUMBER, A.ID, A.REMARK, B.NODE1, B.NODE2, 'VALVE' AS GBN , A.TAGNAME                      ");
            oStringBuilder.AppendLine("       , DECODE(UPPER(B.STATUS),'OPEN','000002','CLOSED','000005') AS SIM_GBN                          ");
            oStringBuilder.AppendLine("       , A.PUMPSTATION_ID, A.PUMPSTATION_NAME                                                          ");
            oStringBuilder.AppendLine("FROM                                                                                                   ");
            oStringBuilder.AppendLine("    (SELECT A.PUMPSTATION_ID, A.PUMPSTATION_NAME, B.ID, B.REMARK, B.TAGNAME                            ");
            oStringBuilder.AppendLine("       FROM WE_PUMP_STATION A                                                                          ");
            oStringBuilder.AppendLine("           ,WH_CHECKPOINT B                                                                            ");
            oStringBuilder.AppendLine("      WHERE A.PUMPSTATION_ID = B.PUMPSTATION_ID                                                        ");
            oStringBuilder.AppendLine("        AND A.PUMPSTATION_ID = '" + param["PUMPSTATION_ID"] + "'"                                       );
            oStringBuilder.AppendLine("        AND B.CHPDVICD(+) = '000003') A                                                                ");
            oStringBuilder.AppendLine("    ,(SELECT B.INP_NUMBER AS INP_NUMBER,B.ID, B.NODE1, B.NODE2, NVL(C.STATUS_SETTING, 'OPEN') STATUS   ");
            oStringBuilder.AppendLine("       FROM WH_VALVES B, WH_STATUS C                                                                   ");
            oStringBuilder.AppendLine("      WHERE B.INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')                    ");
            oStringBuilder.AppendLine("        AND B.INP_NUMBER = C.INP_NUMBER(+)                                                             ");
            oStringBuilder.AppendLine("        AND B.ID = C.ID(+)                                                                             ");
            oStringBuilder.AppendLine("        ) B                                                                                            ");
            oStringBuilder.AppendLine("WHERE A.ID  = B.ID                                                                                     ");

            IDataParameter[] parameters =  {
                     new OracleParameter("PUMPSTATION_ID", OracleDbType.Varchar2)
                };
            parameters[0].Value = param["PUMPSTATION_ID"];


            DataTable valvetable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT A.ID                                                                                            ");
            oStringBuilder.AppendLine("       ,DECODE(B.VALUE,'1','000002','000005')        AS SIM_GBN                                        ");
            oStringBuilder.AppendLine("  FROM                                                                                                 ");
            oStringBuilder.AppendLine("        (SELECT A.PUMPSTATION_ID, A.PUMPSTATION_NAME, B.ID, B.REMARK, B.TAGNAME                        ");
            oStringBuilder.AppendLine("           FROM WE_PUMP_STATION A                                                                      ");
            oStringBuilder.AppendLine("               ,WH_CHECKPOINT B                                                                        ");
            oStringBuilder.AppendLine("          WHERE A.PUMPSTATION_ID = B.PUMPSTATION_ID                                                    ");
            oStringBuilder.AppendLine("        AND A.PUMPSTATION_ID = '" + param["PUMPSTATION_ID"] + "'");
            oStringBuilder.AppendLine("            AND B.CHPDVICD(+) = '000003' ) A                                                           ");
            oStringBuilder.AppendLine("      ,IF_GATHER_REALTIME B                                                                            ");
            oStringBuilder.AppendLine("  WHERE B.TIMESTAMP  = (SELECT MAX(TO_DATE(TARGET_DATE,'YYYYMMDDHH24MISS')) FROM WH_RPT_MASTER         ");
            oStringBuilder.AppendLine("                         WHERE AUTO_MANUAL = 'A'                                                       ");
            oStringBuilder.AppendLine("                           AND INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y'))  ");
            oStringBuilder.AppendLine("    AND A.TAGNAME = B.TAGNAME                                                                          ");


            DataTable tempvalve = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            foreach (DataRow row in valvetable.Rows)
            {
                foreach (DataRow temprow in tempvalve.Rows)
                {
                    if (row["ID"] == temprow["ID"])
                    {
                        row["SIM_GBN"] = temprow["SIM_GBN"];
                        break;
                    }
                }
            }

            GridValveCondition.DataSource = valvetable.DefaultView;

        }

        /// <summary>
        /// 사용량 그리드 데이터 설정하기(실시간 운영조건)
        /// 기본적으로 모델의 Demand로 설정하고, 최종수리해석결과의 유량으로 적용한다.
        /// 최종수리해석결과에 값이 없는경우는 모델의 Demand를 적용한다.
        /// </summary>
        private void GetusedConditionData(Hashtable param)
        {
            ///사용량 조건 설정
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT B.INP_NUMBER, A.ID, A.REMARK, A.MIN_PRESS, A.TAGNAME                               ");
            oStringBuilder.AppendLine("       , B.DEMAND SIM_FLOW                                                                ");
            oStringBuilder.AppendLine("       , A.PUMPSTATION_ID, A.PUMPSTATION_NAME                                             ");
            oStringBuilder.AppendLine("FROM                                                                                      ");
            oStringBuilder.AppendLine("    (SELECT A.PUMPSTATION_ID, A.PUMPSTATION_NAME, B.ID, B.REMARK, B.TAGNAME ,B.MIN_PRESS  ");
            oStringBuilder.AppendLine("       FROM WE_PUMP_STATION A                                                             ");
            oStringBuilder.AppendLine("           ,WH_CHECKPOINT B                                                               ");
            oStringBuilder.AppendLine("      WHERE A.PUMPSTATION_ID = B.PUMPSTATION_ID                                           ");
            oStringBuilder.AppendLine("        AND A.PUMPSTATION_ID = '" + param["PUMPSTATION_ID"] + "'");
            //oStringBuilder.AppendLine("        AND B.CHPDVICD(+) = '000001') A                                                   ");
            oStringBuilder.AppendLine("     ) A                                                   ");
            oStringBuilder.AppendLine("    ,(SELECT B.INP_NUMBER AS INP_NUMBER, B.ID, B.ELEV, B.DEMAND                           ");
            oStringBuilder.AppendLine("       FROM WH_JUNCTIONS B                                                                ");
            oStringBuilder.AppendLine("      WHERE B.INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')       ");
            oStringBuilder.AppendLine("        ) B                                                                               ");
            oStringBuilder.AppendLine("WHERE A.ID  = B.ID                                                                        ");
            DataTable junctiontable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            //지방에서는 배분량을 사용할 수 없으므로, 최종해석결과의 Demand를 적용한다.
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT A.NODE_ID, A.DEMAND                                                                                            ");
            oStringBuilder.AppendLine("  FROM WH_RPT_NODES A                                                                                                 ");
            oStringBuilder.AppendLine(" WHERE A.RPT_NUMBER =                                                                                                 ");
            oStringBuilder.AppendLine("                    (SELECT RPT_NUMBER FROM WH_RPT_MASTER                                                             ");
            oStringBuilder.AppendLine("                      WHERE TO_DATE(TARGET_DATE,'YYYYMMDDHH24MISS') =                                                 ");
            oStringBuilder.AppendLine("                                        (SELECT MAX(TO_DATE(TARGET_DATE,'YYYYMMDDHH24MISS')) FROM WH_RPT_MASTER       ");
            oStringBuilder.AppendLine("                                         WHERE AUTO_MANUAL = 'A'                                                      ");
            oStringBuilder.AppendLine("                                           AND INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')) ");
            oStringBuilder.AppendLine("   AND INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')) ");

            DataTable temptable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            
            #region 광역-수리에서 배분량설정함수 사용가능할 경우
            //oStringBuilder.AppendLine("SELECT INP_NUMBER, TO_CHAR(MAX(TO_DATE(TARGET_DATE,'YYYYMMDDHH24MISS')),'YYYYMMDDHH24MI') DT FROM WH_RPT_MASTER       ");
            //oStringBuilder.AppendLine("	WHERE AUTO_MANUAL = 'A'                                                      ");
            //oStringBuilder.AppendLine("   AND INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')   ");
            //oStringBuilder.AppendLine(" GROUP BY INP_NUMBER                                                          ");
            //DataTable temptable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            //if (temptable.Rows.Count == 0) return;

            //string dt = Convert.ToString(temptable.Rows[0]["DT"]);
            //string inpnumber = Convert.ToString(temptable.Rows[0]["INP_NUMBER"]);
            //Hashtable junctionlist = new Hashtable();
            //WaterNet.WH_Common.work.CommonWork.GetInstance().DistributionDemand(m_oDBManager, junctionlist, inpnumber, dt);
            #endregion 광역-수리에서 배분량설정함수 사용가능할 경우

            foreach (DataRow row in junctiontable.Rows)
            {
                foreach (DataRow tmp in temptable.Rows)
                {
                    if (Convert.ToString(row["ID"]).Equals(Convert.ToString(tmp["NODE_ID"])))
                    {
                        row["SIM_FLOW"] = tmp["DEMAND"];
                        break;
                    }
                }
                //if (junctionlist.Contains(row["ID"]))
                //{
                //    object o = junctionlist[row["ID"]];
                //    if (o != null)
                //        row["SIM_FLOW"] = Convert.ToString(o);

                //}
            }

            GridUsedCondition.DataSource = junctiontable.DefaultView;
        }

        /// <summary>
        /// 배분량 : 모델의 Deman를 직접 사용한다.
        /// </summary>
        private void GetFixedusedConditionData(Hashtable param)
        {
            ///사용량 조건 설정
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT B.INP_NUMBER, A.ID, A.REMARK, A.MIN_PRESS, A.TAGNAME                               ");
            oStringBuilder.AppendLine("       , B.DEMAND SIM_FLOW                                                                ");
            oStringBuilder.AppendLine("       , A.PUMPSTATION_ID, A.PUMPSTATION_NAME                                             ");
            oStringBuilder.AppendLine("FROM                                                                                      ");
            oStringBuilder.AppendLine("    (SELECT A.PUMPSTATION_ID, A.PUMPSTATION_NAME, B.ID, B.REMARK, B.TAGNAME ,B.MIN_PRESS  ");
            oStringBuilder.AppendLine("       FROM WE_PUMP_STATION A                                                             ");
            oStringBuilder.AppendLine("           ,WH_CHECKPOINT B                                                               ");
            oStringBuilder.AppendLine("      WHERE A.PUMPSTATION_ID = B.PUMPSTATION_ID                                           ");
            oStringBuilder.AppendLine("        AND A.PUMPSTATION_ID = '" + param["PUMPSTATION_ID"] + "'");
            oStringBuilder.AppendLine("        AND B.CHPDVICD(+) = '000001') A                                                   ");
            oStringBuilder.AppendLine("    ,(SELECT B.INP_NUMBER AS INP_NUMBER, B.ID, B.ELEV, B.DEMAND                           ");
            oStringBuilder.AppendLine("       FROM WH_JUNCTIONS B                                                                ");
            oStringBuilder.AppendLine("      WHERE B.INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')       ");
            oStringBuilder.AppendLine("        ) B                                                                               ");
            oStringBuilder.AppendLine("WHERE A.ID  = B.ID                                                                        ");

            #region 광역
            //광역에서 wv_flowmeter에서는 배분량이 설정되어 있으나, 지방에서는 없으므로, 모델의 Demand로 적용함.
            //oStringBuilder.AppendLine("SELECT B.INP_NUMBER, B.CHPDVICD, B.ID, B.TITLE                                         ");
            //oStringBuilder.AppendLine("      ,B.FTR_IDN, NVL(ROUND((A.DIVIDE_VOLUMN /24),2),0) SIM_FLOW                       ");
            //oStringBuilder.AppendLine("      ,NVL(A.MIN_PRESS,0) MIN_PRESS                                                    ");
            //oStringBuilder.AppendLine("  FROM WV_FLOWMETER A                                                                  ");
            //oStringBuilder.AppendLine("      ,(                                                                               ");
            //oStringBuilder.AppendLine("        SELECT B.INP_NUMBER, B.CHPDVICD, B.ID, B.TITLE                                 ");
            //oStringBuilder.AppendLine("               ,A.VALUE1 FTR_IDN                                                       ");
            //oStringBuilder.AppendLine("          FROM WH_TAGS A                                                               ");
            //oStringBuilder.AppendLine("               ,WH_CHECKPOINT B                                                        ");
            //oStringBuilder.AppendLine("         WHERE B.INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE USE_GBN = 'WH')   ");
            //oStringBuilder.AppendLine("           AND A.INP_NUMBER = B.INP_NUMBER                                             ");
            //oStringBuilder.AppendLine("           AND B.CHPDVICD = '000001'                                                   ");
            //oStringBuilder.AppendLine("           AND A.TYPE = 'NODE'                                                         ");
            //oStringBuilder.AppendLine("           AND A.ID = B.ID                                                             ");
            //oStringBuilder.AppendLine("        ) B                                                                            ");
            //oStringBuilder.AppendLine(" WHERE A.FLOWMETER_FTR_IDN(+) = B.FTR_IDN                                              ");
            #endregion 광역
            DataTable junctiontable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            GridUsedCondition.DataSource = junctiontable.DefaultView;
        }

        /// <summary>
        /// 배분량 불러오기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSupplyflow_Click(object sender, EventArgs e)
        {
            Hashtable param = new Hashtable();
            param.Add("PUMPSTATION_ID", this.cboBzs.SelectedValue);
            GetFixedusedConditionData(param);
        }

        /// <summary>
        /// 사용량 그리드 데이터 설정하기(실시간 운영조건)
        /// 기본적으로 모델의 Demand로 설정하고, 최종수리해석결과의 유량으로 적용한다.
        /// 최종수리해석결과에 값이 없는경우는 모델의 Demand를 적용한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTMflow_Click(object sender, EventArgs e)
        {
            Hashtable param = new Hashtable();
            param.Add("PUMPSTATION_ID", this.cboBzs.SelectedValue);
            GetusedConditionData(param);

            GetPumpConditionData(param);
            GetValveConditionData(param);

            Bzs_SelectedValueChanged(this, new EventArgs());
        }

        //private double GetjunctionDemand(string id)
        //{
        //    StringBuilder oStringBuilder = new StringBuilder();
        //    oStringBuilder.AppendLine("select demand from wh_junctions ");
        //    oStringBuilder.AppendLine(" where INP_NUMBER = (SELECT INP_NUMBER FROM WH_TITLE WHERE USE_GBN = 'WH')   ");
        //    oStringBuilder.AppendLine("  and id = '" + id + "'");

        //    double demand = Convert.ToDouble(m_oDBManager.ExecuteScriptScalar(oStringBuilder.ToString(), null));

        //    return demand;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oDateTime"></param>
        /// <returns></returns>
        private string GetDateTimeToString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int year = oDateTime.Year;
            int month = oDateTime.Month;
            int day = oDateTime.Day;
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = year.ToString();
            nowtime += month.ToString().PadLeft(2, '0');
            nowtime += day.ToString().PadLeft(2, '0');
            nowtime += hour.ToString().PadLeft(2, '0');
            nowtime +=  (min / 10).ToString().PadRight(2, '0');
            //nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }


        /// <summary>
        /// 사용량 조건 설정그리드 HashTable 생성
        /// </summary>
        /// <param name="resetValue"></param>
        private void CreateJunctionDemandSection(ref Hashtable resetValue)
        {
            ///junction demand Section
            Hashtable JunctionData = new Hashtable();
            foreach (UltraGridRow row in GridUsedCondition.Rows)
            {
                JunctionData.Add(row.Cells["ID"].Value, Convert.ToString(Convert.IsDBNull(row.Cells["SIM_FLOW"].Value) ? 0 : Convert.ToDouble(row.Cells["SIM_FLOW"].Value)));
            }
            resetValue.Add("demand", JunctionData);
        }

        /// <summary>
        /// 펌프 조건설정 그리드에서 펌프 개도율 설정
        /// 모의설정 선택에서 고정된 항목만 추출하여 Resetvalue 생성
        /// open = 0, close = string.empty
        /// </summary>
        /// <returns></returns>
        private Hashtable CreateFixedValveSettingSection()
        {
            Hashtable hashtable = new Hashtable();

            foreach (UltraGridRow row in GridValveCondition.Rows)
            {
                if (!Convert.IsDBNull(row.Cells["SIM_GBN"].Value))
                {
                    switch (Convert.ToString(row.Cells["SIM_GBN"].Value))
                    {
                        case "000002":       //고정(OPEN)
                            hashtable.Add(row.Cells["ID"].Value, "0");
                            break;
                        case "000003":       //고정(가동)
                            hashtable.Add(row.Cells["ID"].Value, "0");
                            break;
                        case "000004":       //고정(정지)
                            hashtable.Add(row.Cells["ID"].Value, "0");
                            break;
                        case "000005":       //고정(CLOSE)
                            hashtable.Add(row.Cells["ID"].Value, "0");
                            break;
                    }
                }
            }

            return hashtable;
        }

        /// <summary>
        /// 펌프/밸브 조건설정 그리드에서
        /// 모의설정 선택에서 고정된 항목만 추출하여 Resetvalue 생성
        /// </summary>
        /// <param name="resetValue"></param>
        private Hashtable CreateFixedStatusSection()
        {
            Hashtable hashtable = new Hashtable();

            foreach (UltraGridRow row in GridPumpCondition.Rows)
            {
                if (!Convert.IsDBNull(row.Cells["SIM_GBN"].Value))
                {
                    switch (Convert.ToString(row.Cells["SIM_GBN"].Value))
                    {
                        case "000002":       //고정(OPEN)
                            hashtable.Add(row.Cells["ID"].Value, "OPEN");
                            break;
                        case "000003":       //고정(가동)
                            hashtable.Add(row.Cells["ID"].Value, "OPEN");
                            break;
                        case "000004":       //고정(정지)
                            hashtable.Add(row.Cells["ID"].Value, "CLOSED");
                            break;
                        case "000005":       //고정(CLOSE)
                            hashtable.Add(row.Cells["ID"].Value, "CLOSED");
                            break;
                    }
                }
            }

            foreach (UltraGridRow row in GridValveCondition.Rows)
            {
                if (!Convert.IsDBNull(row.Cells["SIM_GBN"].Value))
                {
                    switch (Convert.ToString(row.Cells["SIM_GBN"].Value))
                    {
                        case "000002":       //고정(OPEN)
                            hashtable.Add(row.Cells["ID"].Value, "OPEN");
                            break;
                        case "000003":       //고정(가동)
                            hashtable.Add(row.Cells["ID"].Value, "OPEN");
                            break;
                        case "000004":       //고정(정지)
                            hashtable.Add(row.Cells["ID"].Value, "CLOSED");
                            break;
                        case "000005":       //고정(CLOSE)
                            hashtable.Add(row.Cells["ID"].Value, "CLOSED");
                            break;
                    }
                }
            }

            return hashtable;
        }

        /// <summary>
        /// 제외된 해석결과를 표시한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnView_Click(object sender, EventArgs e)
        {
            if (m_AnalysisDeletedDataset.Count == 0)
            {
                MessageBox.Show("최저한계수압이하의 수리해석결과가 없습니다.");
                return;
            }

            frmCaseAnalysisData oform = new frmCaseAnalysisData(m_AnalysisDeletedDataset);
            oform.ShowDialog();
        }

        /// <summary>
        /// 최적 조합 분석 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAnalysis_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DataTable pumpdata = WE_SimCommon.WE_FunctionManager.GetDatasource2DataTable(this.GridPumpCondition);
                DataTable valvedata = WE_SimCommon.WE_FunctionManager.GetDatasource2DataTable(this.GridValveCondition);

                foreach (DataRow row in pumpdata.Rows)
                {
                    if (Convert.IsDBNull(row["SIM_GBN"]))
                    {
                        MessageBox.Show("펌프 가동정보는 Null일수 없습니다.");
                        return;
                    }
                }
                ///펌프 및 밸브가 변경으로 설정된 것만 추출
                DataRow[] pumprows1 = pumpdata.Select("SIM_GBN = '000001'");
                DataRow[] valverows1 = valvedata.Select("SIM_GBN = '000001'");

                ArrayList array = new ArrayList();
                foreach (DataRow row in pumprows1)
                {
                    array.Add(row["ID"]);
                }

                foreach (DataRow row in valverows1)
                {
                    array.Add(row["ID"]);
                }

                if (array.Count > 8)
                {
                    MessageBox.Show("변경항목이 너무 많습니다.");
                    return;
                }


                //해석결과 저장 DataSet 목록 초기화
                m_AnalysisDataset.Clear();
                m_AnalysisDeletedDataset.Clear();

                ///고정 조건 : Status HashTable
                Hashtable fixedstatus = CreateFixedStatusSection();
                ///밸브 고정 조건 개도율
                Hashtable fixedValveSet = CreateFixedValveSettingSection();

                array = GetListKarnaughMap(array);

                Hashtable resetValue = new Hashtable();  //최상위 hashtable

                try
                {
                    for (int i = 0; i < array.Count; i++)
                    {
                        resetValue.Clear();
                        ///변경 Status hashtable
                        Hashtable floatstatus = array[i] as Hashtable;

                        ///수용가 유량(사용량) resetValue에 설정
                        CreateJunctionDemandSection(ref resetValue);
                        ///Status 섹션 구성
                        string casekey = CreateStatusSection(ref resetValue, fixedstatus, floatstatus);
                        Console.WriteLine(casekey);
                        ///밸브 섹션내 Setting 설정
                        CreateValveSectionIsSetting(ref resetValue, fixedValveSet, floatstatus, ref valverows1);


                        ///해석결과 
                        Hashtable result = ExecuteAnalysis(resetValue);
                        if (result == null) continue;

                        AnalysisParse2DataSet(result, casekey);

                    }

                    DisplayRank();
                }
                catch (Exception)
                {
                    throw;
                }


                if (m_AnalysisDeletedDataset.Count > 0)
                {
                    int iCnt = 1;
                    StringBuilder description = new StringBuilder();
                    foreach (DataSet dataset in m_AnalysisDeletedDataset)
                    {
                        description.AppendLine(iCnt.ToString() + " : " + dataset.DataSetName);
                        iCnt++;
                    }
                    MessageBox.Show("수리해석을 완료하였으며, 아래의 케이스는 수압이 최저한계수압 이하인 결과가 있습니다.\n\r" + description.ToString());

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

           
        }

        /// <summary>
        /// 수리해석 결과를 Datatable로 변환하고, Case그리드에 추가한다.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="casekey"></param>
        private void AnalysisParse2DataSet(Hashtable result, string casekey)
        {
            //if (result == null)
            //{
            //    DataRow row = m_CaseTable.NewRow();
            //    //row["KEY"] = "";
            //    row["NAME"] = casekey;
            //    row["OBJECT"] = null;
            //    m_CaseTable.Rows.Add(row);
            //    GridCase.DataBind();
            //    return;
            //}

            DataTable nodeTable = new DataTable("NODE");
            DataTable linkTable = new DataTable("LINK");
            CreateAnalisysTable(ref nodeTable, ref linkTable);
            DataTable entirenodeTable = nodeTable.Clone(); //전체 해석 node 테이블
            entirenodeTable.TableName = "ENTIRENODE";
            DivideAnalysisData_INP(result, ref nodeTable, ref linkTable, ref entirenodeTable);

            DataSet dataset = new DataSet(casekey);
            dataset.Tables.Add(nodeTable);
            dataset.Tables.Add(linkTable);
            dataset.Tables.Add(entirenodeTable);

            //DataRow row2 = m_CaseTable.NewRow();
            //row2["NAME"] = casekey;
            //row2["OBJECT"] = dataset;
            //m_CaseTable.Rows.Add(row2);
            //GridCase.DataBind();

            m_AnalysisDataset.Add(dataset);
        }

        private DataSet GetDataSet(string key)
        {
            DataSet dataset = null;
            foreach (DataSet ds in m_AnalysisDataset)
            {
                if (ds.DataSetName.Equals(key))
                {
                    dataset = ds;
                    break;
                }
            }
            return dataset;
        }

        private void DisplayRank()
        {
            CalculateEnergy();

            Dictionary<string, double> sortedDic = CalculateRank() as Dictionary<string, double>;
            int iCnt = 1;
            foreach (string key in sortedDic.Keys)
            {
                if (iCnt == 4) break;
                string[] szPairs = key.Split(',');
                IEnumerator ez = szPairs.GetEnumerator();
                string[] szTemp;

                while (ez.MoveNext())
                {
                    szTemp = ez.Current.ToString().Split(':');
                    if (szTemp.Length == 1) continue;
                    string id = Convert.ToString(szTemp.GetValue(0));
                    string status = Convert.ToString(szTemp.GetValue(1));

                    //DataSet ds = GetDataSet(key);
                    //DataTable linkTable = ds.Tables["LINK"];

                    foreach (UltraGridRow row in GridOptimum.Rows)
                    {
                        if (id.Equals(row.Cells["ID"].Value))
                        {
                            if (Convert.ToString(row.Cells["GBN"].Value).Equals("PUMP"))
                            {
                                status = (status.Equals("OPEN") ? "가동" : "정지");
                            }
                            else if (Convert.ToString(row.Cells["GBN"].Value).Equals("VALVE"))
                            {
                                status = (status.Equals("OPEN") ? "OPEN" : "CLOSE");
                            }

                            //DataRow linkrow  = null;
                            switch (iCnt)
                            {
                                case 1:
                                    row.Cells["RANK1"].Value = status;

                                    rdRank1.Tag = key;
                                    //linkrow = GetMatchingData(Convert.ToString(row.Cells["ID"].Value), linkTable);
                                    //row.Cells["ENERGY1"].Value = (linkrow == null) ? 100 : linkrow["ENERGY"];
                                    //row.Cells["UNITCOST1"].Value = (linkrow == null) ? 100 : linkrow["PERUNIT"];
                                    break;
                                case 2:
                                    row.Cells["RANK2"].Value = status;
                                    rdRank2.Tag = key;
                                    //linkrow = GetMatchingData(Convert.ToString(row.Cells["ID"].Value), linkTable);
                                    //row.Cells["ENERGY2"].Value = (linkrow == null) ? 100 : linkrow["ENERGY"];
                                    //row.Cells["UNITCOST2"].Value = (linkrow == null) ? 100 : linkrow["PERUNIT"];
                                    break;
                                case 3:
                                    row.Cells["RANK3"].Value = status;
                                    rdRank3.Tag = key;
                                    //linkrow = GetMatchingData(Convert.ToString(row.Cells["ID"].Value), linkTable);
                                    //row.Cells["ENERGY3"].Value = (linkrow == null) ? 100 : linkrow["ENERGY"];
                                    //row.Cells["UNITCOST3"].Value = (linkrow == null) ? 100 : linkrow["PERUNIT"];
                                    break;
                            }
                            break;
                        }


                    }
                }

                //Console.WriteLine(key);
                DataSet ds = GetDataSet(key);
                DataTable linkTable = ds.Tables["LINK"];

                foreach (UltraGridRow row in GridOptimum.Rows)
                {
                    DataRow linkrow = null;
                    switch (iCnt)
                    {
                        case 1:
                            linkrow = WE_SimCommon.WE_FunctionManager.GetMatchingData(Convert.ToString(row.Cells["ID"].Value), linkTable);
                            row.Cells["ENERGY1"].Value = (linkrow == null) ? 0 : linkrow["ENERGY"];
                            row.Cells["UNITCOST1"].Value = (linkrow == null) ? 0 : linkrow["PERUNIT"];
                            break;
                        case 2:
                            linkrow = WE_SimCommon.WE_FunctionManager.GetMatchingData(Convert.ToString(row.Cells["ID"].Value), linkTable);
                            row.Cells["ENERGY2"].Value = (linkrow == null) ? 0 : linkrow["ENERGY"];
                            row.Cells["UNITCOST2"].Value = (linkrow == null) ? 0 : linkrow["PERUNIT"];
                            break;
                        case 3:
                            linkrow = WE_SimCommon.WE_FunctionManager.GetMatchingData(Convert.ToString(row.Cells["ID"].Value), linkTable);
                            row.Cells["ENERGY3"].Value = (linkrow == null) ? 0 : linkrow["ENERGY"];
                            row.Cells["UNITCOST3"].Value = (linkrow == null) ? 0 : linkrow["PERUNIT"];
                            break;
                    }
                }
                iCnt++;
            }


        }


        private DataTable CreateOptimunData()
        {
            DataTable Optimumtable = new DataTable();
            Optimumtable.Columns.Add("INP_NUMBER", typeof(System.String));
            Optimumtable.Columns.Add("ID", typeof(System.String));
            Optimumtable.Columns.Add("TAGNAME", typeof(System.String));
            Optimumtable.Columns.Add("REMARK", typeof(System.String));
            Optimumtable.Columns.Add("RANK1", typeof(System.String));
            Optimumtable.Columns.Add("ENERGY1", typeof(System.Double));
            Optimumtable.Columns.Add("UNITCOST1", typeof(System.Double));
            Optimumtable.Columns.Add("RANK2", typeof(System.String));
            Optimumtable.Columns.Add("ENERGY2", typeof(System.Double));
            Optimumtable.Columns.Add("UNITCOST2", typeof(System.Double));
            Optimumtable.Columns.Add("RANK3", typeof(System.String));
            Optimumtable.Columns.Add("ENERGY3", typeof(System.Double));
            Optimumtable.Columns.Add("UNITCOST3", typeof(System.Double));
            Optimumtable.Columns.Add("GBN", typeof(System.String));

            DataTable t1 = WE_SimCommon.WE_FunctionManager.GetDatasource2DataTable(this.GridPumpCondition).Copy();
            DataTable t2 = WE_SimCommon.WE_FunctionManager.GetDatasource2DataTable(this.GridValveCondition).Copy();
            foreach (DataRow row in t1.Rows)
            {
                DataRow _row = Optimumtable.NewRow();
                _row["INP_NUMBER"] = row["INP_NUMBER"];
                _row["ID"] = row["ID"];
                _row["REMARK"] = row["REMARK"];
                _row["TAGNAME"] = row["TAGNAME"];
                _row["GBN"] = row["GBN"];
                Optimumtable.Rows.Add(_row);
            }

            foreach (DataRow row in t2.Rows)
            {
                DataRow _row = Optimumtable.NewRow();
                _row["INP_NUMBER"] = row["INP_NUMBER"];
                _row["ID"] = row["ID"];
                _row["REMARK"] = row["REMARK"];
                _row["TAGNAME"] = row["TAGNAME"];
                _row["GBN"] = row["GBN"];
                Optimumtable.Rows.Add(_row);
            }

            return Optimumtable;
        }

        /// <summary>
        /// 펌프그리드와 밸브그리드에 있는 ID만 표시한다.
        /// </summary>
        private void DisplayOptimumGrid()
        {
            foreach (UltraGridRow row in GridOptimum.Rows)
            {
                row.Hidden = true;
            }
            foreach (UltraGridRow row in this.GridOptimum.Rows)
            {
                foreach (UltraGridRow row2 in GridPumpCondition.Rows)
                {
                    if (Convert.ToString(row.Cells["ID"].Value) == (Convert.ToString(row2.Cells["ID"].Value)))
                    {
                        row.Hidden = false;
                        continue;
                    }
                }
            }

            foreach (UltraGridRow row in this.GridOptimum.Rows)
            {
                foreach (UltraGridRow row2 in GridValveCondition.Rows)
                {
                    if (Convert.ToString(row.Cells["ID"].Value) == (Convert.ToString(row2.Cells["ID"].Value)))
                    {
                        row.Hidden = false;
                        continue;
                    }
                }
            }
        }

        /// <summary>
        /// 원단위를 기준으로 Sort된 Dictionary를 반환한다.
        /// </summary>
        /// <returns></returns>
        private object CalculateRank()
        {
            Dictionary<string, double> mm = new Dictionary<string, double>();
            
            foreach (DataSet dataset in m_AnalysisDataset)
            {
                DataTable linkTable = dataset.Tables["LINK"];
                //DataTable nodeTable = dataset.Tables["NODE"];
                DataRow[] rows = linkTable.Select("PERUNIT > 0");
                //if (rows.Length == 0) continue;
                double won = Convert.ToDouble(linkTable.Compute("Avg(PERUNIT)", "PERUNIT >= 0"));
                
                mm.Add(dataset.DataSetName, won);

                Console.WriteLine(dataset.DataSetName + " , " + won.ToString());
            }

            var sortedDict = (from entry in mm orderby entry.Value ascending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);

            return sortedDict;
        }
        /// <summary>
        /// 
        /// </summary>
        private void GetNODE_ID(DataRow row, ref string node1, ref string node2)
        {
            DataRow[] _rows = m_WH_PUMP.Select("ID = '" + row["ID"] + "'");
            if (_rows.Count() > 0)
            {
                node1 = Convert.ToString(_rows[0]["NODE1"]);
                node2 = Convert.ToString(_rows[0]["NODE2"]);

                return;
            }

            _rows = m_WH_VALVE.Select("ID = '" + row["ID"] + "'");
            if (_rows.Count() > 0)
            {
                node1 = Convert.ToString(_rows[0]["NODE1"]);
                node2 = Convert.ToString(_rows[0]["NODE2"]);

                return;
            }

            _rows = m_WH_PIPE.Select("ID = '" + row["ID"] + "'");
            if (_rows.Count() > 0)
            {
                node1 = Convert.ToString(_rows[0]["NODE1"]);
                node2 = Convert.ToString(_rows[0]["NODE2"]);

                return;
            }

            node1 = string.Empty; node2 = string.Empty;
            return;
        }

        /// <summary>
        /// 양정, 효율, 원단위를 계산하여 LINK 테이블에 Update한다.
        /// </summary>
        private void CalculateEnergy()
        {
            m_AnalysisDeletedDataset.Clear();

            for (int i = m_AnalysisDataset.Count -1; i >= 0; i--)
            {
                DataSet dataset = m_AnalysisDataset[i] as DataSet;

                DataTable nodeTable = dataset.Tables["NODE"];
                DataTable linkTable = dataset.Tables["LINK"];
                DataTable entirenodeTable = dataset.Tables["ENTIRENODE"];

                ///NODE table 과 사용량그리드를 비교하여
                ///사용량그리드의 ID의 수압이 0이하, 최소기준수압 이하를 체크하여, 해석결과 삭제한다.
                if (ValidatePressureData(nodeTable))
                {
                    Console.WriteLine(dataset.DataSetName + " : IsLower true : ");
                    m_AnalysisDeletedDataset.Add(dataset.Copy());
                    m_AnalysisDataset.Remove(dataset);
                    continue;
                }

                //DataRow[] linkrows = linkTable.Select("ENERGY > 0");
                foreach (DataRow row in linkTable.Rows)
                {
                    string node1 = string.Empty; string node2 = string.Empty;
                    double pressure1 = 0.0; double pressure2 = 0.0;

                    GetNODE_ID(row, ref node1, ref node2);

                    DataRow[] _rows1 = entirenodeTable.Select("ID = '" + node1 + "'");
                    if (_rows1.Count() > 0)
                        pressure1 = Convert.ToDouble(_rows1[0]["PRESSURE"]);
                    DataRow[] _rows2 = entirenodeTable.Select("ID = '" + node2 + "'");
                    if (_rows2.Count() > 0)
                        pressure2 = Convert.ToDouble(_rows2[0]["PRESSURE"]);

                    double h = Math.Abs(pressure2 - pressure1);
                    row["LIFT"] = h;
                    double perunit = Convert.IsDBNull(row["FLOW"]) ? 0 : Math.Round(Convert.ToDouble(row["ENERGY"]) / Convert.ToDouble(row["FLOW"]), 5);
                    //Console.WriteLine(Convert.ToString(row["ID"]) + " : " + Convert.ToString(Convert.ToDouble(row["ENERGY"])) + " : " + Convert.ToString(Convert.ToDouble(row["FLOW"])));

                    row["PERUNIT"] = (double.IsNaN(perunit) || double.IsInfinity(perunit)) ? 0 : perunit;
                    double effi = Convert.ToDouble(row["PERUNIT"]) == 0 ? 0 : Math.Round(0.002722 * (Convert.ToDouble(row["LIFT"]) / Convert.ToDouble(row["PERUNIT"])), 3);
                    row["EFFICIENCE"] = (double.IsNaN(effi) || double.IsInfinity(effi)) ? 0 : effi;
                }
                linkTable.AcceptChanges();
            }
        }

        /// <summary>
        /// 밸브의 개도율을 설정한다.
        /// resetValue
        /// </summary>
        /// <param name="resetValue"></param>
        /// <param name="fixedstatus"></param>
        /// <param name="floatstatus"></param>
        /// <returns></returns>
        private void CreateValveSectionIsSetting(ref Hashtable resetValue, Hashtable fixedValveSet, Hashtable floatstatus, ref DataRow[] floatValveData)
        {
            Hashtable valveValue = new Hashtable();    //Valve 섹션의 setting hashtable

            foreach (string ID in fixedValveSet.Keys)
            {
                valveValue.Add(ID, fixedValveSet[ID]);
            }

            foreach (string ID in floatstatus.Keys)
            {
                foreach (DataRow row in floatValveData)
                {
                    if (ID.Equals(row["ID"]))
                    {
                        string setting = (string)floatstatus[ID] == "OPEN" ? "0" : string.Empty;
                        valveValue.Add(ID, setting);
                        continue;
                    }
                }
            }

            resetValue.Add("valve", valveValue);

        }


        /// <summary>
        /// ResetValue에 고정 Status, 변경 Status의 Status 섹션을 채워넣는다.
        /// </summary>
        /// <param name="resetValue"></param>
        /// <param name="fixedstatus">고정 status</param>
        /// <param name="floatstatus">변경 status</param>
        private string CreateStatusSection(ref Hashtable resetValue, Hashtable fixedstatus, Hashtable floatstatus)
        {
            StringBuilder str = new StringBuilder();  //ID:Closed,.....
            Hashtable StatusValue = new Hashtable();  //Status 섹션 hashtable

            foreach (string ID in floatstatus.Keys)
            {
                StatusValue.Add(ID, floatstatus[ID]);
                str.Append(ID.ToString() + ":" + floatstatus[ID].ToString() + ",");
            }

            foreach (string ID in fixedstatus.Keys)
            {
                StatusValue.Add(ID, fixedstatus[ID]);
                str.Append(ID.ToString() + ":" + fixedstatus[ID].ToString() + ",");
            }

            resetValue.Add("status", StatusValue);
            str = str.Remove(str.Length - 1, 1);

            return str.ToString();
        }
        /// <summary>
        /// 수리해석 모델 실행 , 에너지 분석
        /// </summary>
        /// <param name="oList"></param>
        /// <returns></returns>
        private Hashtable ExecuteAnalysis(Hashtable resetValue)
        {
            /////수리 모델 해석실행
            WH_PipingNetworkAnalysis.epanet.AnalysisEngine engine = new WH_PipingNetworkAnalysis.epanet.AnalysisEngine();
            try
            {
                string inpnumber = Convert.ToString(FunctionManager.ExecuteScriptScalar("SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y'", null));
                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", inpnumber);
                conditions.Add("AUTO_MANUAL", "M");
                conditions.Add("analysisFlag", "H");
                conditions.Add("analysisType", 0);
                //conditions.Add("RPTTYP", "000001");
                conditions.Add("resetValues", resetValue);   //null : DB에 저장된 모델, HashTable
                conditions.Add("saveReport", false);
                conditions.Add("TARGET_DATE", GetNowDateTimeToString(DateTime.Now));

                Hashtable result = engine.Execute(conditions);
                if (result == null)
                {
                    Console.WriteLine("에너지해석 수행중 오류가 발생했습니다.");
                    return null;
                }

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("에너지해석 수행중 오류가 발생했습니다.");
                return null;
            }
        }

        /// <summary>
        /// 해석결과를 저장할 데이터테이블 및 필드생성
        /// nodeTable, linkTable
        /// </summary>
        private void CreateAnalisysTable(ref DataTable nodeTable, ref DataTable linkTable)
        {
            ////해석결과를 담는 객체 초기화
            nodeTable.Columns.Add("TIME", typeof(System.String));
            nodeTable.Columns.Add("ID", typeof(System.String));
            nodeTable.Columns.Add("ELEVATION", typeof(System.Double));
            nodeTable.Columns.Add("BASEDEMAND", typeof(System.Double));
            nodeTable.Columns.Add("PATTERN", typeof(System.Double));
            nodeTable.Columns.Add("EMITTER", typeof(System.Double));
            nodeTable.Columns.Add("INITQUAL", typeof(System.Double));
            nodeTable.Columns.Add("SOURCEQUAL", typeof(System.Double));
            nodeTable.Columns.Add("SOURCEPAT", typeof(System.Double));
            nodeTable.Columns.Add("SOURCETYPE", typeof(System.Double));
            nodeTable.Columns.Add("TANKLEVEL", typeof(System.Double));
            nodeTable.Columns.Add("DEMAND", typeof(System.Double));
            nodeTable.Columns.Add("HEAD", typeof(System.Double));
            nodeTable.Columns.Add("PRESSURE", typeof(System.Double));
            nodeTable.Columns.Add("QUALITY", typeof(System.Double));
            nodeTable.Columns.Add("SOURCEMASS", typeof(System.Double));
            nodeTable.Columns.Add("INITVOLUMN", typeof(System.Double));
            nodeTable.Columns.Add("MIXMODEL", typeof(System.Double));
            nodeTable.Columns.Add("MIXZONEVOL", typeof(System.Double));
            nodeTable.Columns.Add("TANKDIAM", typeof(System.Double));
            nodeTable.Columns.Add("MINVOLUMN", typeof(System.Double));
            nodeTable.Columns.Add("VOLCURVE", typeof(System.Double));
            nodeTable.Columns.Add("MINLEVEL", typeof(System.Double));
            nodeTable.Columns.Add("MAXLEVEL", typeof(System.Double));
            nodeTable.Columns.Add("MIXFRACTION", typeof(System.Double));
            nodeTable.Columns.Add("TANK_KBULK", typeof(System.Double));
            nodeTable.Columns.Add("ZERO", typeof(System.String));

            linkTable.Columns.Add("TIME", typeof(System.String));
            linkTable.Columns.Add("ID", typeof(System.String));
            linkTable.Columns.Add("DIAMETER", typeof(System.Double));
            linkTable.Columns.Add("LENGTH", typeof(System.Double));
            linkTable.Columns.Add("ROUGHNESS", typeof(System.Double));
            linkTable.Columns.Add("MINORLOSS", typeof(System.Double));
            linkTable.Columns.Add("INITSTATUS", typeof(System.Double));
            linkTable.Columns.Add("INITSETTING", typeof(System.Double));
            linkTable.Columns.Add("KBULK", typeof(System.Double));
            linkTable.Columns.Add("KWALL", typeof(System.Double));
            linkTable.Columns.Add("FLOW", typeof(System.Double));
            linkTable.Columns.Add("VELOCITY", typeof(System.Double));
            linkTable.Columns.Add("HEADLOSS", typeof(System.Double));
            linkTable.Columns.Add("STATUS", typeof(System.Double));
            linkTable.Columns.Add("SETTING", typeof(System.Double));
            linkTable.Columns.Add("ENERGY", typeof(System.Double));
            linkTable.Columns.Add("LIFT", typeof(System.Double));
            linkTable.Columns.Add("EFFICIENCE", typeof(System.Double));
            linkTable.Columns.Add("PERUNIT", typeof(System.Double));
            linkTable.Columns.Add("ZERO", typeof(System.String));

        }



        /// <summary>
        /// 수리모델 해석결과를 NODE, LINK 구분별로 분리하여
        /// DataTable에 저장한다.
        /// </summary>
        /// <param name="map"></param>
        /// <param name="analysisData"></param>
        private void DivideAnalysisData_INP(Hashtable result, ref DataTable nodeTable, ref DataTable linkTable, ref DataTable entirenodeTable)
        {
            DataRow row = null; DataRow entirenode = null;

            ////Node, Link별 해석결과 저장
            foreach (string time in result.Keys)
            {
                if (time.Equals("RPT_NUMBER")) continue;
                Hashtable resultByTime = (Hashtable)result[time];
                foreach (string type in resultByTime.Keys)
                {
                    //type은 node,link
                    Hashtable resultByType = (Hashtable)resultByTime[type];
                    foreach (string subtype in resultByType.Keys)
                    {
                        //subtype은 junction,reservior,tank,cvPipe,pipe,pump,prv,psv,pbv,fcv,tcv,gpv
                        ArrayList resultList = (ArrayList)resultByType[subtype];
                        for (int i = 0; i < resultList.Count; i++)
                        {
                            //ID별 해석결과 리스트
                            Hashtable resultData = (Hashtable)resultList[i];
                            switch (type.ToUpper())
                            {
                                case "NODE":
                                    //전체 해석결과를 저장한다.
                                    entirenode = entirenodeTable.NewRow();
                                    entirenode["TIME"] = time;
                                    entirenode["ID"] = Convert.ToString(resultData["NODE_ID"]);

                                    //entirenode["ELEVATION"] = Math.Round(Convert.ToDouble(resultData["EN_ELEVATION"]), 2);
                                    //entirenode["BASEDEMAND"] = Math.Round(Convert.ToDouble(resultData["EN_BASEDEMAND"]), 2);
                                    //entirenode["PATTERN"] = Math.Round(Convert.ToDouble(resultData["EN_PATTERN"]), 2);
                                    //entirenode["EMITTER"] = Math.Round(Convert.ToDouble(resultData["EN_EMITTER"]), 2);
                                    //entirenode["INITQUAL"] = Math.Round(Convert.ToDouble(resultData["EN_INITQUAL"]), 2);
                                    //entirenode["SOURCEQUAL"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCEQUAL"]), 2);
                                    //entirenode["SOURCEPAT"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCEPAT"]), 2);
                                    //entirenode["SOURCETYPE"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCETYPE"]), 2);
                                    //entirenode["TANKLEVEL"] = Math.Round(Convert.ToDouble(resultData["EN_TANKLEVEL"]), 2);
                                    //entirenode["DEMAND"] = Math.Round(Convert.ToDouble(resultData["EN_DEMAND"]), 2);
                                    //entirenode["HEAD"] = Math.Round(Convert.ToDouble(resultData["EN_HEAD"]), 2);
                                    entirenode["PRESSURE"] = Math.Round(Convert.ToDouble(resultData["EN_PRESSURE"]), 2);
                                    //entirenode["QUALITY"] = Math.Round(Convert.ToDouble(resultData["EN_QUALITY"]), 2);
                                    //entirenode["SOURCEMASS"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCEMASS"]), 2);
                                    //entirenode["INITVOLUMN"] = Math.Round(Convert.ToDouble(resultData["EN_INITVOLUMN"]), 2);
                                    //entirenode["MIXMODEL"] = Math.Round(Convert.ToDouble(resultData["EN_MIXMODEL"]), 2);
                                    //entirenode["MIXZONEVOL"] = Math.Round(Convert.ToDouble(resultData["EN_MIXZONEVOL"]), 2);
                                    //entirenode["TANKDIAM"] = Math.Round(Convert.ToDouble(resultData["EN_TANKDIAM"]), 2);
                                    //entirenode["MINVOLUMN"] = Math.Round(Convert.ToDouble(resultData["EN_MINVOLUMN"]), 2);
                                    //entirenode["VOLCURVE"] = Math.Round(Convert.ToDouble(resultData["EN_VOLCURVE"]), 2);
                                    //entirenode["MINLEVEL"] = Math.Round(Convert.ToDouble(resultData["EN_MINLEVEL"]), 2);
                                    //entirenode["MAXLEVEL"] = Math.Round(Convert.ToDouble(resultData["EN_MAXLEVEL"]), 2);
                                    //entirenode["MIXFRACTION"] = Math.Round(Convert.ToDouble(resultData["EN_MIXFRACTION"]), 2);
                                    //entirenode["TANK_KBULK"] = Math.Round(Convert.ToDouble(resultData["EN_TANK_KBULK"]), 2);

                                    entirenodeTable.Rows.Add(entirenode);

                                    if (IsContainJunction(Convert.ToString(resultData["NODE_ID"])))
                                    {
                                        row = nodeTable.NewRow();
                                        row["TIME"] = time;
                                        row["ID"] = Convert.ToString(resultData["NODE_ID"]);

                                        row["ELEVATION"] = Math.Round(Convert.ToDouble(resultData["EN_ELEVATION"]), 2);
                                        row["BASEDEMAND"] = Math.Round(Convert.ToDouble(resultData["EN_BASEDEMAND"]), 2);
                                        row["PATTERN"] = Math.Round(Convert.ToDouble(resultData["EN_PATTERN"]), 2);
                                        row["EMITTER"] = Math.Round(Convert.ToDouble(resultData["EN_EMITTER"]), 2);
                                        row["INITQUAL"] = Math.Round(Convert.ToDouble(resultData["EN_INITQUAL"]), 2);
                                        row["SOURCEQUAL"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCEQUAL"]), 2);
                                        row["SOURCEPAT"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCEPAT"]), 2);
                                        row["SOURCETYPE"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCETYPE"]), 2);
                                        row["TANKLEVEL"] = Math.Round(Convert.ToDouble(resultData["EN_TANKLEVEL"]), 2);
                                        row["DEMAND"] = Math.Round(Convert.ToDouble(resultData["EN_DEMAND"]), 2);
                                        row["HEAD"] = Math.Round(Convert.ToDouble(resultData["EN_HEAD"]), 2);
                                        row["PRESSURE"] = Math.Round(Convert.ToDouble(resultData["EN_PRESSURE"]), 2);
                                        row["QUALITY"] = Math.Round(Convert.ToDouble(resultData["EN_QUALITY"]), 2);
                                        row["SOURCEMASS"] = Math.Round(Convert.ToDouble(resultData["EN_SOURCEMASS"]), 2);
                                        row["INITVOLUMN"] = Math.Round(Convert.ToDouble(resultData["EN_INITVOLUMN"]), 2);
                                        row["MIXMODEL"] = Math.Round(Convert.ToDouble(resultData["EN_MIXMODEL"]), 2);
                                        row["MIXZONEVOL"] = Math.Round(Convert.ToDouble(resultData["EN_MIXZONEVOL"]), 2);
                                        row["TANKDIAM"] = Math.Round(Convert.ToDouble(resultData["EN_TANKDIAM"]), 2);
                                        row["MINVOLUMN"] = Math.Round(Convert.ToDouble(resultData["EN_MINVOLUMN"]), 2);
                                        row["VOLCURVE"] = Math.Round(Convert.ToDouble(resultData["EN_VOLCURVE"]), 2);
                                        row["MINLEVEL"] = Math.Round(Convert.ToDouble(resultData["EN_MINLEVEL"]), 2);
                                        row["MAXLEVEL"] = Math.Round(Convert.ToDouble(resultData["EN_MAXLEVEL"]), 2);
                                        row["MIXFRACTION"] = Math.Round(Convert.ToDouble(resultData["EN_MIXFRACTION"]), 2);
                                        row["TANK_KBULK"] = Math.Round(Convert.ToDouble(resultData["EN_TANK_KBULK"]), 2);

                                        nodeTable.Rows.Add(row);
                                    }
                                    break;
                                case "LINK":
                                    if (IsContainLinks(Convert.ToString(resultData["LINK_ID"])))
                                    {
                                        row = linkTable.NewRow();
                                        row["TIME"] = time;
                                        row["ID"] = Convert.ToString(resultData["LINK_ID"]);
                                        row["DIAMETER"] = Math.Round(Convert.ToDouble(resultData["EN_DIAMETER"]), 2);
                                        row["LENGTH"] = Math.Round(Convert.ToDouble(resultData["EN_LENGTH"]), 2);
                                        row["ROUGHNESS"] = Math.Round(Convert.ToDouble(resultData["EN_ROUGHNESS"]), 2);
                                        row["MINORLOSS"] = Math.Round(Convert.ToDouble(resultData["EN_MINORLOSS"]), 2);
                                        row["INITSTATUS"] = Math.Round(Convert.ToDouble(resultData["EN_INITSTATUS"]), 2);
                                        row["INITSETTING"] = Math.Round(Convert.ToDouble(resultData["EN_INITSETTING"]), 2);
                                        row["KBULK"] = Math.Round(Convert.ToDouble(resultData["EN_KBULK"]), 2);
                                        row["KWALL"] = Math.Round(Convert.ToDouble(resultData["EN_KWALL"]), 2);
                                        row["FLOW"] = Math.Round(Convert.ToDouble(resultData["EN_FLOW"]), 2);
                                        row["VELOCITY"] = Math.Round(Convert.ToDouble(resultData["EN_VELOCITY"]), 2);
                                        row["HEADLOSS"] = Math.Round(Convert.ToDouble(resultData["EN_HEADLOSS"]), 2);
                                        row["STATUS"] = Math.Round(Convert.ToDouble(resultData["EN_STATUS"]), 2);
                                        row["SETTING"] = Math.Round(Convert.ToDouble(resultData["EN_SETTING"]), 2);
                                        row["ENERGY"] = Math.Round(Convert.ToDouble(resultData["EN_ENERGY"]), 4);
                                        //if (Convert.ToDouble(row["FLOW"]) > 0)
                                        //{
                                        //    Console.WriteLine("FLOW : " + Convert.ToString(row["FLOW"]));
                                        //}
                                        //if (Convert.ToDouble(row["ENERGY"]) > 0)
                                        //{
                                        //    Console.WriteLine("ENERGY : " + Convert.ToString(row["ENERGY"]));
                                        //}
                                        row["LIFT"] = 0;
                                        row["EFFICIENCE"] = 0;
                                        row["PERUNIT"] = 0;
                                        linkTable.Rows.Add(row);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            linkTable.AcceptChanges();
            nodeTable.AcceptChanges();
            entirenodeTable.AcceptChanges();
        }

        private bool IsContainJunction(string ID)
        {
            bool b = false;
            foreach (UltraGridRow row in this.GridUsedCondition.Rows)
            {
                if (Convert.ToString(row.Cells["ID"].Value).Equals(ID))
                {
                    b = true;

                    break;
                }
            }

            return b;
        }

        private bool IsContainLinks(string ID)
        {
            bool b = false;
            foreach (UltraGridRow row in this.GridPumpCondition.Rows)
            {
                if (Convert.ToString(row.Cells["ID"].Value).Equals(ID))
                {
                    b = true;
                    break;
                }
            }

            if (!b)
            {
                foreach (UltraGridRow row in this.GridValveCondition.Rows)
                {
                    if (Convert.ToString(row.Cells["ID"].Value).Equals(ID))
                    {
                        b = true;
                        break;
                    }
                }                
            }
            return b;
        }
        /// <summary>
        /// 해석결과(NODE Table)에 수용가ID의 수압이 <= 0 경우의 NODE Row에 저장
        /// </summary>
        /// <param name="nodetable"></param>
        private bool ValidatePressureData(DataTable nodetable)
        {
            bool isLower = false;

            DataTable temptable = WE_SimCommon.WE_FunctionManager.GetDatasource2DataTable(this.GridUsedCondition);
            foreach (DataRow row in temptable.Rows)
            {
                double minpress = Convert.IsDBNull(row["MIN_PRESS"]) ? 0 : Convert.ToDouble(row["MIN_PRESS"]);
                DataRow noderow = WE_SimCommon.WE_FunctionManager.GetMatchingData(Convert.ToString(row["ID"]), nodetable);
                if (noderow == null) continue;

                double pressure = Convert.IsDBNull(noderow["PRESSURE"]) ? 0 : Convert.ToDouble(noderow["PRESSURE"]);
                if (pressure <= 0)
                {
                    isLower = true;
                    break;
                }

                if (pressure < minpress)
                {
                    isLower = true;
                    break;
                }
            }

            return isLower;
        }

        private string GetNowDateTimeToString(DateTime oDateTime)
        {
            string nowtime = string.Empty;
            // Get current time
            int year = oDateTime.Year;
            int month = oDateTime.Month;
            int day = oDateTime.Day;
            int hour = oDateTime.Hour;
            int min = oDateTime.Minute;
            int sec = oDateTime.Second;

            // Format current time into string
            nowtime = year.ToString();
            nowtime += month.ToString().PadLeft(2, '0');
            nowtime += day.ToString().PadLeft(2, '0');
            nowtime += hour.ToString().PadLeft(2, '0');
            nowtime += min.ToString().PadLeft(2, '0');
            nowtime += sec.ToString().PadLeft(2, '0');

            return nowtime;
        }

        /// <summary>
        /// 펌프와 밸브의 변경목록에 대하여 경우의 수를 모두 찾는다.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        private ArrayList GetListKarnaughMap(ArrayList array)
        {
            int cnts = (int)Math.Pow(2, array.Count);

            ArrayList lists = new ArrayList();
            for (int i = 0; i < cnts; i++)
            {
                Hashtable hashtable = new Hashtable();
                string str = Convert.ToString(i, 2).PadLeft(array.Count, '0');
                char[] split = str.ToCharArray();
                for (int j = 0; j < array.Count; j++)
                {
                    hashtable.Add(array[j], (split[j] == '0') ? "CLOSED" : "OPEN");
                }

                lists.Add(hashtable);
            }

            return lists;
        }

        /// <summary>
        /// 화면 Close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 모의분석 조건을 저장한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConditionSave_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 최적 조합 그리드 Row초기화
        /// 색변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridOptimum_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            foreach (UltraGridCell cell in e.Row.Cells)
            {
                switch (Convert.ToString(cell.Column.Key))
	            {
                    case "RANK1":
                    case "RANK2":
                    case "RANK3":
                        if (Convert.ToString(cell.Value).Equals("가동") || Convert.ToString(cell.Value).Equals("OPEN"))
                        {
                            cell.Appearance.BackColor = Color.Red;
                        }
                        else cell.Appearance.BackColor = Color.White;
                        break;

	            } 
            }

        }

        private void frmCaseSenario_Load(object sender, EventArgs e)
        {
            this.cboBzs.SelectedValueChanged += new EventHandler(Bzs_SelectedValueChanged);
            rdRank1.CheckedChanged += new EventHandler(Radiobutton_CheckedChanged);
            rdRank2.CheckedChanged += new EventHandler(Radiobutton_CheckedChanged);
            rdRank3.CheckedChanged += new EventHandler(Radiobutton_CheckedChanged);

            aa();
            //Bzs_SelectedValueChanged(this, new EventArgs());
        }

        private void Bzs_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cboBzs.DataSource == null) return;

            //aa();
            DisplaySetDatasourceGrid();
            GridOptimum.DataSource = CreateOptimunData().DefaultView;
            //DataRow[] rows = m_pumptable.Select("PUMPSTATION_ID = '" + cboBzs.SelectedValue + "'");

            //if (rows.Length == 0)
            //{
            //    DataTable clone = m_pumptable.Clone();
            //    this.GridPumpCondition.DataSource = clone.DefaultView;

            //    return;
            //}
            //this.GridPumpCondition.DataSource = rows.CopyToDataTable().DefaultView;

            //DisplayOptimumGrid();

            //DisplayRank();

            //Console.WriteLine(GridOptimum.Rows.Count.ToString());
        }

        /// <summary>
        /// 해석 케이스목록을 더블클릭하여, 해석의 해당NODE의 수압을 수용가그리드에 표시한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCase_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            DataSet DS = e.Row.Cells["OBJECT"].Value as DataSet;
            if (DS == null) return;

            DataTable nodetable = DS.Tables["NODE"];
            DataTable linktable = DS.Tables["LINK"];
            foreach (UltraGridRow row in this.GridUsedCondition.Rows)
            {
                DataRow[] rows = nodetable.Select("ID = '" + row.Cells["ID"].Value + "'");
                if (rows.Length > 0)
                {
                    row.Cells["PRESSURE"].Value = rows[0]["PRESSURE"];
                }
            }
        }

        private void GridPumpCondition_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (Convert.ToString(e.Row.Cells["SIM_GBN"].Value).Equals("000003"))
            {
                e.Row.Cells["SIM_GBN"].Appearance.ForeColor = Color.Red;
                //((UltraGrid)sender).DisplayLayout.Bands[0].Columns["SIM_GBN"].CellAppearance.ForeColor = Color.Red;
            }
            else if (Convert.ToString(e.Row.Cells["SIM_GBN"].Value).Equals("000001"))
            {
                e.Row.Cells["SIM_GBN"].Appearance.ForeColor = Color.Blue;
                //((UltraGrid)sender).DisplayLayout.Bands[0].Columns["SIM_GBN"].CellAppearance.ForeColor = Color.Red;
            }
        }

        private void GridValveCondition_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (Convert.ToString(e.Row.Cells["SIM_GBN"].Value).Equals("000002"))
            {
                e.Row.Cells["SIM_GBN"].Appearance.ForeColor = Color.Red;
            }
            else if (Convert.ToString(e.Row.Cells["SIM_GBN"].Value).Equals("000001"))
            {
                e.Row.Cells["SIM_GBN"].Appearance.ForeColor = Color.Blue;
                //((UltraGrid)sender).DisplayLayout.Bands[0].Columns["SIM_GBN"].CellAppearance.ForeColor = Color.Red;
            }

        }

        private void Radiobutton_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                if (((RadioButton)sender).Tag == null) return;

                DataSet DS = GetDataSet(Convert.ToString(((RadioButton)sender).Tag));
                if (DS == null) return;

                Console.WriteLine(Convert.ToString(((RadioButton)sender).Tag));
                DataTable nodetable = DS.Tables["NODE"];
                //DataTable linktable = DS.Tables["LINK"];
                foreach (UltraGridRow row in this.GridUsedCondition.Rows)
                {
                    DataRow[] rows = nodetable.Select("ID = '" + row.Cells["ID"].Value + "'");
                    if (rows.Length > 0)
                    {
                        row.Cells["PRESSURE"].Value = rows[0]["PRESSURE"];
                    }
                }
            }
        }



    }
}
