﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WE_CaseSenarioSimulate
{
    public partial class frmCaseAnalysisData : Form
    {
        private List<DataSet> m_AnalysisDeletedDataset = null;

        public frmCaseAnalysisData()
        {
            InitializeComponent();
        }

        public frmCaseAnalysisData(List<DataSet> DelCase)
        {
            InitializeComponent();

            m_AnalysisDeletedDataset = DelCase;
        }

        private void frmCaseAnalysisData_Load(object sender, EventArgs e)
        {
            for (int i = 1; i <= m_AnalysisDeletedDataset.Count; i++)
            {
                cboCASEID.Items.Add(i.ToString());

            }
        }

        private void cboCASEID_SelectedValueChanged(object sender, EventArgs e)
        {
            DataSet ds = m_AnalysisDeletedDataset[cboCASEID.SelectedIndex];
            txtCaseName.Text = ds.DataSetName;

            ultraGridNode.DataSource = ds.Tables["NODE"];
            ultraGridNode.DataBind();
            ultraGridLink.DataSource = ds.Tables["LINK"];
            ultraGridLink.DataBind();
        }
    }
}
