﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WE_SimCommon
{
    public class WE_FunctionManager
    {
        /// <summary>
        /// UltraGrid.Datasource를 DataTable로 반환한다.
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static DataTable GetDatasource2DataTable(UltraGrid grid)
        {
            DataTable pumpdata = null;
            if (grid.DataSource is DataTable)
            {
                pumpdata = grid.DataSource as DataTable;
            }
            else if (grid.DataSource is DataView)
            {
                pumpdata = ((DataView)grid.DataSource).Table;
            }

            return pumpdata;
        }

        public static DataRow GetMatchingData(string ID, DataTable datatable)
        {
            DataRow row = null;
            DataRow[] rows = datatable.Select("ID = '" + ID + "'");
            if (rows.Length >= 1)
            {
                row = rows[0];
            }
            return row;
        }

        public static DataRow GetMatchingData(string ID, DataRow[] datarows)
        {
            DataTable datatable = datarows.CopyToDataTable();
            
            return GetMatchingData(ID, datatable);
        }

        public static UltraGridGroup AddColumnGroup(UltraGrid Grid, string key, string caption, System.Collections.ArrayList oColumns)
        {
            UltraGridGroup gridGroup = Grid.DisplayLayout.Bands[0].Groups.Add(key);
            gridGroup.Header.Caption = caption;
            gridGroup.Header.Appearance.TextHAlign = HAlign.Center;
            gridGroup.Header.Appearance.TextVAlign = VAlign.Middle;
            gridGroup.Header.Appearance.BackGradientStyle = GradientStyle.None;

            ColumnsCollection ColumnCollection = Grid.DisplayLayout.Bands[0].Columns;
            foreach (UltraGridColumn Column in ColumnCollection)
            {
                foreach (string item in oColumns)
                {
                    if (Column.Key == item)
                    {
                        Column.Group = gridGroup;
                        continue;
                    }
                }
            }
            return gridGroup;
        }

        /// <summary>
        /// UltraGrid의 특정 Column을 AllowEdit 모드로 변경한다.
        /// </summary>
        /// <param name="ugList">UltraGrid</param>
        /// <param name="iCol">Column Index</param>
        public static void SetGridStyle_ColumeAllowEdit(UltraGrid ugList, int iCol)
        {
            ugList.DisplayLayout.Bands[0].Columns[iCol].CellActivation = Activation.AllowEdit;
            ugList.DisplayLayout.Bands[0].Columns[iCol].CellAppearance.BackColor = System.Drawing.Color.White;
            ugList.DisplayLayout.Bands[0].Columns[iCol].CellClickAction = CellClickAction.EditAndSelectText;
        }
    }
}
