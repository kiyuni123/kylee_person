﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WE_SimCommon
{
    public partial class frmDesignPumpEff : Form
    {
        WaterNetCore.OracleDBManager m_oDBManager = new OracleDBManager();
        public frmDesignPumpEff()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            m_oDBManager.Open();

            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            ///편방향 펌프 그리드
            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_GBN";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FLOW";
            oUltraGridColumn.Header.Caption = "유량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_100";
            oUltraGridColumn.Header.Caption = "100이하";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_150";
            oUltraGridColumn.Header.Caption = "100-150";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width =70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_200";
            oUltraGridColumn.Header.Caption = "150-200";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_250";
            oUltraGridColumn.Header.Caption = "200-250";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_300";
            oUltraGridColumn.Header.Caption = "250-300";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_350";
            oUltraGridColumn.Header.Caption = "300-350";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_400";
            oUltraGridColumn.Header.Caption = "350-400";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_450";
            oUltraGridColumn.Header.Caption = "400-450";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridoneway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_999";
            oUltraGridColumn.Header.Caption = "450이상";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            ///양방향 펌프 그리드
            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_GBN";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FLOW";
            oUltraGridColumn.Header.Caption = "유량";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_100";
            oUltraGridColumn.Header.Caption = "100이하";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_150";
            oUltraGridColumn.Header.Caption = "100-150";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_200";
            oUltraGridColumn.Header.Caption = "150-200";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_250";
            oUltraGridColumn.Header.Caption = "200-250";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_300";
            oUltraGridColumn.Header.Caption = "250-300";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_350";
            oUltraGridColumn.Header.Caption = "300-350";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_400";
            oUltraGridColumn.Header.Caption = "350-400";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_450";
            oUltraGridColumn.Header.Caption = "400-450";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = Gridtwoway.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED_999";
            oUltraGridColumn.Header.Caption = "450이상";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            WaterNetCore.FormManager.SetGridStyle(Gridoneway);
            WaterNetCore.FormManager.SetGridStyle(Gridtwoway);
            #endregion 그리드 설정

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT *                   ");
            oStringBuilder.AppendLine("  FROM WE_DESIGN_PUMP_EFF  ");
            oStringBuilder.AppendLine(" WHERE PUMP_GBN = '편흡입'  ");
            oStringBuilder.AppendLine(" ORDER BY TO_NUMBER(FLOW) ASC");
            Gridoneway.DataSource = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT *                   ");
            oStringBuilder.AppendLine("  FROM WE_DESIGN_PUMP_EFF  ");
            oStringBuilder.AppendLine(" WHERE PUMP_GBN = '양흡입'  ");
            oStringBuilder.AppendLine(" ORDER BY TO_NUMBER(FLOW) ASC");
            Gridtwoway.DataSource = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
        }
        #endregion 초기화설정
    }
}
