﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;
#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion

namespace WaterNet.WE_SimCommon
{
    public partial class frmElecUnitCost : Form
    {
        private WaterNetCore.OracleDBManager m_oDBManager = new OracleDBManager();
        private DataTable m_ElecUnitcost;

        public frmElecUnitCost()
        {
            InitializeComponent();

            InitializeSetting();
            Load += new EventHandler(frmElecUnitCost_Load);
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            m_oDBManager.Open();

            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            ///전력계약 종별 그리드
            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PROMISE_ID";
            oUltraGridColumn.Header.Caption = "계약종별";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.ValueList = WaterNetCore.FormManager.SetValueList_Code("6201");

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MON";
            oUltraGridColumn.Header.Caption = "월";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            object[,] aoSource = {
                {"1", "1"},
                {"2", "2"},
                {"3", "3"},
                {"4", "4"},
                {"5", "5"},
                {"6", "6"},
                {"7", "7"},
                {"8", "8"},
                {"9", "9"},
                {"10","10"},
                {"11","11"},
                {"12","12"}
            };
            oUltraGridColumn.ValueList = FormManager.SetValueList(aoSource);

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BASECOST";
            oUltraGridColumn.Header.Caption = "기본요금";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H1";
            oUltraGridColumn.Header.Caption = "1h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H2";
            oUltraGridColumn.Header.Caption = "2h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H3";
            oUltraGridColumn.Header.Caption = "3h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H4";
            oUltraGridColumn.Header.Caption = "4h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H5";
            oUltraGridColumn.Header.Caption = "5h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H6";
            oUltraGridColumn.Header.Caption = "6h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H7";
            oUltraGridColumn.Header.Caption = "7h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H8";
            oUltraGridColumn.Header.Caption = "8h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H9";
            oUltraGridColumn.Header.Caption = "9h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H10";
            oUltraGridColumn.Header.Caption = "10h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H11";
            oUltraGridColumn.Header.Caption = "11h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H12";
            oUltraGridColumn.Header.Caption = "12h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H13";
            oUltraGridColumn.Header.Caption = "13h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H14";
            oUltraGridColumn.Header.Caption = "14h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H15";
            oUltraGridColumn.Header.Caption = "15h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H16";
            oUltraGridColumn.Header.Caption = "16h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H17";
            oUltraGridColumn.Header.Caption = "17h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H18";
            oUltraGridColumn.Header.Caption = "18h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H19";
            oUltraGridColumn.Header.Caption = "19h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H20";
            oUltraGridColumn.Header.Caption = "20h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H21";
            oUltraGridColumn.Header.Caption = "21h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H22";
            oUltraGridColumn.Header.Caption = "22h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H23";
            oUltraGridColumn.Header.Caption = "23h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H24";
            oUltraGridColumn.Header.Caption = "24h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;
            WaterNetCore.FormManager.SetGridStyle(GridElectricUnit);
            WaterNetCore.FormManager.ColumeAllowEdit(GridElectricUnit, 0, 26);

            #endregion 그리드 설정

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT CODE AS CD, CODE_NAME AS CDNM FROM CM_CODE WHERE PCODE = '6201'");
            WaterNet.WaterNetCore.FormManager.SetComboBoxEX(cboPromiseID, oStringBuilder.ToString(), true);

            
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT * ");
            oStringBuilder.AppendLine("FROM we_electric_unitcost                                                  ");
            m_ElecUnitcost = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

        }
        #endregion 초기화설정

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string sfilter = string.IsNullOrEmpty((string)cboPromiseID.SelectedValue) ? string.Empty : "PROMISE_ID = '" + cboPromiseID.SelectedValue + "'";
                DataRow[] rows = m_ElecUnitcost.Select(sfilter);
                if (rows.Length == 0)
                {
                    DataTable clone = m_ElecUnitcost.Clone();
                    //clone.Rows.Clear();
                    GridElectricUnit.DataSource = clone.DefaultView;
                    return;
                }


                GridElectricUnit.DataSource = rows.CopyToDataTable().DefaultView;
                GridElectricUnit.DataBind();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
           
        }

        private void frmElecUnitCost_Load(object sender, EventArgs e)
        {
            //동진_2012.08.23
            object o = EMFrame.statics.AppStatic.USER_MENU["tsOptimunSpec"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.btnAppend.Enabled = false;
                this.btnSave.Enabled = false;
                this.btnDelete.Enabled = false;
            }

            GridElectricUnit.DataSource = m_ElecUnitcost;
            this.GridElectricUnit.AfterCellUpdate += new CellEventHandler(FormManager.AfterCellUpdate);
            this.GridElectricUnit.AfterRowInsert += new RowEventHandler(FormManager.AfterRowInsert);
        }

        private void btnAppend_Click(object sender, EventArgs e)
        {
            UltraGridRow NewRow = this.GridElectricUnit.DisplayLayout.Bands[0].AddNew();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.GridElectricUnit.ActiveRow == null) return;
                DialogResult qe = MessageBox.Show("선택항목을 삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (qe == DialogResult.Yes)
                {
                    StringBuilder oStringBuilder = new StringBuilder();
                    oStringBuilder.AppendLine("DELETE WE_ELECTRIC_UNITCOST  ");
                    oStringBuilder.AppendLine(" WHERE PROMISE_ID = :PROMISE_ID         ");
                    oStringBuilder.AppendLine("   AND MON = :MON                       ");

                    UltraGridRow row = this.GridElectricUnit.ActiveRow;
                    IDataParameter[] oParams = {
                        new OracleParameter(":PROMISE_ID", OracleDbType.Varchar2),
                        new OracleParameter(":MON", OracleDbType.Varchar2)
                    };

                    oParams[0].Value = row.Cells["PROMISE_ID"].Value;
                    oParams[1].Value = row.Cells["MON"].Value;

                    m_oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);

                    this.GridElectricUnit.ActiveRow.Delete(false);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult qe = MessageBox.Show("저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (qe != DialogResult.Yes) return;

                foreach (UltraGridRow row in this.GridElectricUnit.Rows)
                {
                    if (Convert.ToString(row.Tag) == "I")
                    {
                        InsertProcess(row);
                    }
                    else if (Convert.ToString(row.Tag) == "U")
                    {
                        UpdateProcess(row);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
           
        }

        private void InsertProcess(UltraGridRow row)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("INSERT INTO WE_ELECTRIC_UNITCOST (  ");
            oStringBuilder.AppendLine("		 PROMISE_ID                      ");
            oStringBuilder.AppendLine("		,MON                             ");
            oStringBuilder.AppendLine("		,BASECOST                        ");
            oStringBuilder.AppendLine("		,H1                              ");
            oStringBuilder.AppendLine("		,H2                              ");
            oStringBuilder.AppendLine("		,H3                              ");
            oStringBuilder.AppendLine("		,H4                              ");
            oStringBuilder.AppendLine("		,H5                              ");
            oStringBuilder.AppendLine("		,H6                              ");
            oStringBuilder.AppendLine("		,H7                              ");
            oStringBuilder.AppendLine("		,H8                              ");
            oStringBuilder.AppendLine("		,H9                              ");
            oStringBuilder.AppendLine("		,H10                             ");
            oStringBuilder.AppendLine("		,H11                             ");
            oStringBuilder.AppendLine("		,H12                             ");
            oStringBuilder.AppendLine("		,H13                             ");
            oStringBuilder.AppendLine("		,H14                             ");
            oStringBuilder.AppendLine("		,H15                             ");
            oStringBuilder.AppendLine("		,H16                             ");
            oStringBuilder.AppendLine("		,H17                             ");
            oStringBuilder.AppendLine("		,H18                             ");
            oStringBuilder.AppendLine("		,H19                             ");
            oStringBuilder.AppendLine("		,H20                             ");
            oStringBuilder.AppendLine("		,H21                             ");
            oStringBuilder.AppendLine("		,H22                             ");
            oStringBuilder.AppendLine("		,H23                             ");
            oStringBuilder.AppendLine("		,H24 )                           ");
            oStringBuilder.AppendLine("VALUES (	                           ");
            oStringBuilder.AppendLine(" 	 :PROMISE_ID                   ");
            oStringBuilder.AppendLine("		,:MON                            ");
            oStringBuilder.AppendLine("		,:BASECOST                       ");
            oStringBuilder.AppendLine("		,:H1                             ");
            oStringBuilder.AppendLine("		,:H2                             ");
            oStringBuilder.AppendLine("		,:H3                             ");
            oStringBuilder.AppendLine("		,:H4                             ");
            oStringBuilder.AppendLine("		,:H5                             ");
            oStringBuilder.AppendLine("		,:H6                             ");
            oStringBuilder.AppendLine("		,:H7                             ");
            oStringBuilder.AppendLine("		,:H8                             ");
            oStringBuilder.AppendLine("		,:H9                             ");
            oStringBuilder.AppendLine("		,:H10                            ");
            oStringBuilder.AppendLine("		,:H11                            ");
            oStringBuilder.AppendLine("		,:H12                            ");
            oStringBuilder.AppendLine("		,:H13                            ");
            oStringBuilder.AppendLine("		,:H14                            ");
            oStringBuilder.AppendLine("		,:H15                            ");
            oStringBuilder.AppendLine("		,:H16                            ");
            oStringBuilder.AppendLine("		,:H17                            ");
            oStringBuilder.AppendLine("		,:H18                            ");
            oStringBuilder.AppendLine("		,:H19                            ");
            oStringBuilder.AppendLine("		,:H20                            ");
            oStringBuilder.AppendLine("		,:H21                            ");
            oStringBuilder.AppendLine("		,:H22                            ");
            oStringBuilder.AppendLine("		,:H23                            ");
            oStringBuilder.AppendLine("		,:H24	)                        ");

            IDataParameter[] oParams = {
		            new OracleParameter(":PROMISE_ID",  OracleDbType.Varchar2),
		            new OracleParameter(":MON",         OracleDbType.Varchar2),
		            new OracleParameter(":BASECOST",    OracleDbType.Varchar2),
		            new OracleParameter(":H1",          OracleDbType.Double),
		            new OracleParameter(":H2",          OracleDbType.Double),
		            new OracleParameter(":H3",          OracleDbType.Double),
		            new OracleParameter(":H4",          OracleDbType.Double),
		            new OracleParameter(":H5",          OracleDbType.Double),
		            new OracleParameter(":H6",          OracleDbType.Double),
		            new OracleParameter(":H7",          OracleDbType.Double),
		            new OracleParameter(":H8",          OracleDbType.Double),
		            new OracleParameter(":H9",          OracleDbType.Double),
		            new OracleParameter(":H10",         OracleDbType.Double),
		            new OracleParameter(":H11",         OracleDbType.Double),
		            new OracleParameter(":H12",         OracleDbType.Double),
		            new OracleParameter(":H13",         OracleDbType.Double),
		            new OracleParameter(":H14",         OracleDbType.Double),
		            new OracleParameter(":H15",         OracleDbType.Double),
		            new OracleParameter(":H16",         OracleDbType.Double),
		            new OracleParameter(":H17",         OracleDbType.Double),
		            new OracleParameter(":H18",         OracleDbType.Double),
		            new OracleParameter(":H19",         OracleDbType.Double),
		            new OracleParameter(":H20",         OracleDbType.Double),
		            new OracleParameter(":H21",         OracleDbType.Double),
		            new OracleParameter(":H22",         OracleDbType.Double),
		            new OracleParameter(":H23",         OracleDbType.Double),
		            new OracleParameter(":H24",         OracleDbType.Double)
            };

            oParams[0].Value = row.Cells["PROMISE_ID"].Value;
            oParams[1].Value = row.Cells["MON"].Value;
            oParams[2].Value = row.Cells["BASECOST"].Value;
            oParams[3].Value = row.Cells["H1"].Value;
            oParams[4].Value = row.Cells["H2"].Value;
            oParams[5].Value = row.Cells["H3"].Value;
            oParams[6].Value = row.Cells["H4"].Value;
            oParams[7].Value = row.Cells["H5"].Value;
            oParams[8].Value = row.Cells["H6"].Value;
            oParams[9].Value = row.Cells["H7"].Value;
            oParams[10].Value = row.Cells["H8"].Value;
            oParams[11].Value = row.Cells["H9"].Value;
            oParams[12].Value = row.Cells["H10"].Value;
            oParams[13].Value = row.Cells["H11"].Value;
            oParams[14].Value = row.Cells["H12"].Value;
            oParams[15].Value = row.Cells["H13"].Value;
            oParams[16].Value = row.Cells["H14"].Value;
            oParams[17].Value = row.Cells["H15"].Value;
            oParams[18].Value = row.Cells["H16"].Value;
            oParams[19].Value = row.Cells["H17"].Value;
            oParams[20].Value = row.Cells["H18"].Value;
            oParams[21].Value = row.Cells["H19"].Value;
            oParams[22].Value = row.Cells["H20"].Value;
            oParams[23].Value = row.Cells["H21"].Value;
            oParams[24].Value = row.Cells["H22"].Value;
            oParams[25].Value = row.Cells["H23"].Value;
            oParams[26].Value = row.Cells["H24"].Value;

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);

        }

        private void UpdateProcess(UltraGridRow row)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("UPDATE WE_ELECTRIC_UNITCOST SET  ");
            oStringBuilder.AppendLine("		 BASECOST     = :BASECOST                      ");
            oStringBuilder.AppendLine("		,H1           = :H1                            ");
            oStringBuilder.AppendLine("		,H2           = :H2                            ");
            oStringBuilder.AppendLine("		,H3           = :H3                            ");
            oStringBuilder.AppendLine("		,H4           = :H4                            ");
            oStringBuilder.AppendLine("		,H5           = :H5                            ");
            oStringBuilder.AppendLine("		,H6           = :H6                            ");
            oStringBuilder.AppendLine("		,H7           = :H7                            ");
            oStringBuilder.AppendLine("		,H8           = :H8                            ");
            oStringBuilder.AppendLine("		,H9           = :H9                            ");
            oStringBuilder.AppendLine("		,H10          = :H10                           ");
            oStringBuilder.AppendLine("		,H11          = :H11                           ");
            oStringBuilder.AppendLine("		,H12          = :H12                           ");
            oStringBuilder.AppendLine("		,H13          = :H13                           ");
            oStringBuilder.AppendLine("		,H14          = :H14                           ");
            oStringBuilder.AppendLine("		,H15          = :H15                           ");
            oStringBuilder.AppendLine("		,H16          = :H16                           ");
            oStringBuilder.AppendLine("		,H17          = :H17                           ");
            oStringBuilder.AppendLine("		,H18          = :H18                           ");
            oStringBuilder.AppendLine("		,H19          = :H19                           ");
            oStringBuilder.AppendLine("		,H20          = :H20                           ");
            oStringBuilder.AppendLine("		,H21          = :H21                           ");
            oStringBuilder.AppendLine("		,H22          = :H22                           ");
            oStringBuilder.AppendLine("		,H23          = :H23                           ");
            oStringBuilder.AppendLine("		,H24          = :H24	                       ");
            oStringBuilder.AppendLine("WHERE  PROMISE_ID   = :PROMISE_ID                    ");
            oStringBuilder.AppendLine("  AND  MON          = :MON                           ");

            IDataParameter[] oParams = {
		            new OracleParameter(":BASECOST",    OracleDbType.Varchar2),
		            new OracleParameter(":H1",          OracleDbType.Double),
		            new OracleParameter(":H2",          OracleDbType.Double),
		            new OracleParameter(":H3",          OracleDbType.Double),
		            new OracleParameter(":H4",          OracleDbType.Double),
		            new OracleParameter(":H5",          OracleDbType.Double),
		            new OracleParameter(":H6",          OracleDbType.Double),
		            new OracleParameter(":H7",          OracleDbType.Double),
		            new OracleParameter(":H8",          OracleDbType.Double),
		            new OracleParameter(":H9",          OracleDbType.Double),
		            new OracleParameter(":H10",         OracleDbType.Double),
		            new OracleParameter(":H11",         OracleDbType.Double),
		            new OracleParameter(":H12",         OracleDbType.Double),
		            new OracleParameter(":H13",         OracleDbType.Double),
		            new OracleParameter(":H14",         OracleDbType.Double),
		            new OracleParameter(":H15",         OracleDbType.Double),
		            new OracleParameter(":H16",         OracleDbType.Double),
		            new OracleParameter(":H17",         OracleDbType.Double),
		            new OracleParameter(":H18",         OracleDbType.Double),
		            new OracleParameter(":H19",         OracleDbType.Double),
		            new OracleParameter(":H20",         OracleDbType.Double),
		            new OracleParameter(":H21",         OracleDbType.Double),
		            new OracleParameter(":H22",         OracleDbType.Double),
		            new OracleParameter(":H23",         OracleDbType.Double),
		            new OracleParameter(":H24",         OracleDbType.Double),
		            new OracleParameter(":PROMISE_ID",  OracleDbType.Varchar2),
		            new OracleParameter(":MON",         OracleDbType.Varchar2)
            };

            oParams[0].Value = row.Cells["BASECOST"].Value;
            oParams[1].Value = row.Cells["H1"].Value;
            oParams[2].Value = row.Cells["H2"].Value;
            oParams[3].Value = row.Cells["H3"].Value;
            oParams[4].Value = row.Cells["H4"].Value;
            oParams[5].Value = row.Cells["H5"].Value;
            oParams[6].Value = row.Cells["H6"].Value;
            oParams[7].Value = row.Cells["H7"].Value;
            oParams[8].Value = row.Cells["H8"].Value;
            oParams[9].Value = row.Cells["H9"].Value;
            oParams[10].Value = row.Cells["H10"].Value;
            oParams[11].Value = row.Cells["H11"].Value;
            oParams[12].Value = row.Cells["H12"].Value;
            oParams[13].Value = row.Cells["H13"].Value;
            oParams[14].Value = row.Cells["H14"].Value;
            oParams[15].Value = row.Cells["H15"].Value;
            oParams[16].Value = row.Cells["H16"].Value;
            oParams[17].Value = row.Cells["H17"].Value;
            oParams[18].Value = row.Cells["H18"].Value;
            oParams[19].Value = row.Cells["H19"].Value;
            oParams[20].Value = row.Cells["H20"].Value;
            oParams[21].Value = row.Cells["H21"].Value;
            oParams[22].Value = row.Cells["H22"].Value;
            oParams[23].Value = row.Cells["H23"].Value;
            oParams[24].Value = row.Cells["H24"].Value;
            oParams[25].Value = row.Cells["PROMISE_ID"].Value;
            oParams[26].Value = row.Cells["MON"].Value;

            m_oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
        }

        private void GridElectricUnit_AfterRowsDeleted(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// 선택 Row를 데이터베이스에서 삭제한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridElectricUnit_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {


        }


    }
}
