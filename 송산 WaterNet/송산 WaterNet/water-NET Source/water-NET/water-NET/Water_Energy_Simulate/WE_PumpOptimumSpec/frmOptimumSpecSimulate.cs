﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

using WaterNet.WaterNetCore;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;
#endregion

namespace WaterNet.WE_PumpOptimumSpec
{
    public partial class frmOptimumSpecSimulate : Form, WaterNet.WaterNetCore.IForminterface
    {
        private WaterNetCore.OracleDBManager m_oDBManager = new OracleDBManager();
        private DataTable m_DESIGN_PUMP = null;
        private DataTable m_ELECTRIC_UNITCOST = null;
        private Dictionary<string, DataTable> m_DetailData = new Dictionary<string, DataTable>();  //에너지 기초(10분) 데이터테이블

        public frmOptimumSpecSimulate()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            m_oDBManager.ConnectionString = WaterNet.WaterNetCore.FunctionManager.GetConnectionString();
            m_oDBManager.Open();

            StringBuilder oStringBuilder = new StringBuilder();
            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            ///펌프최적규격 설정 그리드
            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_NAME";
            oUltraGridColumn.Header.Caption = "사업장";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMPSTATION_ID";
            oUltraGridColumn.Header.Caption = "사업장ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "펌프";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TIMES";
            oUltraGridColumn.Header.Caption = "가동시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FLOW";
            oUltraGridColumn.Header.Caption = "유량(㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H";
            oUltraGridColumn.Header.Caption = "양정(m)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENERGY";
            oUltraGridColumn.Header.Caption = "전력량(kWh)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WON_UNIT";
            oUltraGridColumn.Header.Caption = "원단위(kWh/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "O_EFFICIENCY";
            oUltraGridColumn.Header.Caption = "운영효율(%)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENERGY_WON";
            oUltraGridColumn.Header.Caption = "전력비(천원)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_GBN";
            oUltraGridColumn.Header.Caption = "펌프구분";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.ValueList = WaterNet.WaterNetCore.FormManager.SetValueList_Code("PKP");

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SIM_GBN";
            oUltraGridColumn.Header.Caption = "계약전력종류";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.ValueList = WaterNet.WaterNetCore.FormManager.SetValueList_Code("6201");

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PUMP_RPM";
            oUltraGridColumn.Header.Caption = "회전수(rpm)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SPEED";
            oUltraGridColumn.Header.Caption = "비속도(Ns)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_FLOW";
            oUltraGridColumn.Header.Caption = "최적유량(㎥/h)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_H";
            oUltraGridColumn.Header.Caption = "최적양정(m)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "SEGMENT_H";
            oUltraGridColumn.Header.Caption = "체절양정(m)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_EFF";
            oUltraGridColumn.Header.Caption = "최적효율(%)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "a";
            oUltraGridColumn.Header.Caption = "a";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "b";
            oUltraGridColumn.Header.Caption = "b";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            //-----------------------------------------------------------

            FormManager.SetGridStyle(GridStandard);
            
            //GridStandard.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            //GridStandard.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.SeparateElement;
            //GridStandard.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.RowIndex;
            FormManager.SetGridStyle_ColumeAllowEdit(GridStandard, 15);
            FormManager.SetGridStyle_ColumeAllowEdit(GridStandard, 16);
            FormManager.SetGridStyle_ColumeAllowEdit(GridStandard, 17);
            GridStandard.DisplayLayout.Bands[0].Columns[15].CellAppearance.BackColor = Color.Yellow;
            GridStandard.DisplayLayout.Bands[0].Columns[16].CellAppearance.BackColor = Color.Yellow;
            GridStandard.DisplayLayout.Bands[0].Columns[17].CellAppearance.BackColor = Color.Yellow;
            GridStandard.DisplayLayout.Bands[0].Columns[18].CellAppearance.BackColor = Color.Lime;

            ///펌프최적규격 Detail 그리드
            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "펌프명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 120;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TIMESTAMP";
            oUltraGridColumn.Header.Caption = "시간";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "yyyy-MM-dd HH:mm";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 120;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FLOW";
            oUltraGridColumn.Header.Caption = "유량(㎥/h)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H";
            oUltraGridColumn.Header.Caption = "양정(m)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENERGY";
            oUltraGridColumn.Header.Caption = "전력량(kWh)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WON_UNIT";
            oUltraGridColumn.Header.Caption = "원단위(kWh/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "O_EFFICIENCY";
            oUltraGridColumn.Header.Caption = "운영효율(%)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENERGY_WON";
            oUltraGridColumn.Header.Caption = "전력비(원)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_FLOW";
            oUltraGridColumn.Header.Caption = "유량(㎥/h)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_H";
            oUltraGridColumn.Header.Caption = "양정(m)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_ENERGY";
            oUltraGridColumn.Header.Caption = "전력량(kWh)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_WON_UNIT";
            oUltraGridColumn.Header.Caption = "원단위(kWh/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_EFF";
            oUltraGridColumn.Header.Caption = "운영효율(%)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandardDetail.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_ENERGY_WON";
            oUltraGridColumn.Header.Caption = "전력비(원)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;

            FormManager.SetGridStyle(GridStandardDetail);
            SetGridGroup();
            //-----------------------------------------------------------
            #endregion 그리드 설정

            ///사업장 콤보박스 설정
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("select distinct a.pumpstation_id cd,  a.pumpstation_name cdnm ");
            oStringBuilder.AppendLine("  from we_pump_station a                                             ");
            oStringBuilder.AppendLine("      ,we_pump b                                                     ");
            oStringBuilder.AppendLine(" where a.pumpstation_id = b.pumpstation_id                           ");
            oStringBuilder.AppendLine(" order by pumpstation_name asc");
            FormManager.SetComboBoxEX(cboBzs, oStringBuilder.ToString(), false);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT * FROM WE_DESIGN_PUMP_EFF");
            m_DESIGN_PUMP =m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT * FROM WE_ELECTRIC_UNITCOST");
            m_ELECTRIC_UNITCOST = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
        }
        #endregion 초기화설정

        /// <summary>
        /// 그리드 설정 : 그룹
        /// </summary>
        private void SetGridGroup()
        {
            ArrayList columns = new ArrayList();
            columns.Add("ID"); columns.Add("REMARK"); columns.Add("TIMESTAMP");
            WE_SimCommon.WE_FunctionManager.AddColumnGroup(this.GridStandardDetail, "NO0", "기본정보", columns);

            columns.Clear();
            columns.Add("FLOW"); columns.Add("H"); columns.Add("ENERGY"); columns.Add("WON_UNIT"); columns.Add("O_EFFICIENCY"); columns.Add("ENERGY_WON");
            WE_SimCommon.WE_FunctionManager.AddColumnGroup(this.GridStandardDetail, "NO1", "기존규격", columns);

            columns.Clear();
            columns.Add("OPTM_FLOW"); columns.Add("OPTM_H"); columns.Add("OPTM_ENERGY");
            columns.Add("OPTM_WON_UNIT"); columns.Add("OPTM_EFF"); columns.Add("OPTM_ENERGY_WON");
            WE_SimCommon.WE_FunctionManager.AddColumnGroup(this.GridStandardDetail, "NO2", "최적규격", columns);

        }

        //------------------------------------------------------------------
        //메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.
        //FormID : 탭에 보여지는 이름
        //FormKey : 현재 프로젝트 이름
        //------------------------------------------------------------------
        #region IForminterface 멤버

        public string FormID
        {
            get { return "펌프 최적규격 분석"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion IForminterface 멤버

        public bool Open()
        {
            this.Show();

            return true;
        }

        ///// <summary>
        ///// 저장하기
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void btnSave_Click(object sender, EventArgs e)
        //{
        //    string startday = WaterNetCore.FunctionManager.DateTimeToString(dateTimePicker1.Value);
        //    string endday = WaterNetCore.FunctionManager.DateTimeToString(dateTimePicker2.Value);

        //    try
        //    {
        //        DeleteData(Convert.ToString(this.cboBzs.SelectedValue), startday, endday);

        //        StringBuilder oStringBuilder = new StringBuilder();
        //        oStringBuilder.Remove(0, oStringBuilder.Length);
        //        oStringBuilder.AppendLine("INSERT INTO WE_OPTIMUM_RPT (    ");
        //        oStringBuilder.AppendLine("             PUMP_FTR_IDN       ");
        //        oStringBuilder.AppendLine("            ,TIMESTAMP          ");
        //        oStringBuilder.AppendLine("            ,FLOW               ");
        //        oStringBuilder.AppendLine("            ,H                  ");
        //        oStringBuilder.AppendLine("            ,ENERGY             ");
        //        oStringBuilder.AppendLine("            ,WON_UNIT           ");
        //        oStringBuilder.AppendLine("            ,EFF                ");
        //        oStringBuilder.AppendLine("            ,ENERGY_WON         ");
        //        oStringBuilder.AppendLine("            ,OPTM_FLOW          ");
        //        oStringBuilder.AppendLine("            ,OPTM_H             ");
        //        oStringBuilder.AppendLine("            ,OPTM_ENERGY        ");
        //        oStringBuilder.AppendLine("            ,OPTM_WON_UNIT      ");
        //        oStringBuilder.AppendLine("            ,OPTM_EFF           ");
        //        oStringBuilder.AppendLine("            ,OPTM_ENERGY_WON    ");
        //        oStringBuilder.AppendLine("            ,PUMPSTATION_ID    ");
        //        oStringBuilder.AppendLine("            )                   ");
        //        oStringBuilder.AppendLine("            VALUES (            ");
        //        oStringBuilder.AppendLine("             :PUMP_FTR_IDN      ");
        //        oStringBuilder.AppendLine("            ,:TIMESTAMP         ");
        //        oStringBuilder.AppendLine("            ,:FLOW              ");
        //        oStringBuilder.AppendLine("            ,:H                 ");
        //        oStringBuilder.AppendLine("            ,:ENERGY            ");
        //        oStringBuilder.AppendLine("            ,:WON_UNIT          ");
        //        oStringBuilder.AppendLine("            ,:EFF               ");
        //        oStringBuilder.AppendLine("            ,:ENERGY_WON        ");
        //        oStringBuilder.AppendLine("            ,:OPTM_FLOW         ");
        //        oStringBuilder.AppendLine("            ,:OPTM_H            ");
        //        oStringBuilder.AppendLine("            ,:OPTM_ENERGY       ");
        //        oStringBuilder.AppendLine("            ,:OPTM_WON_UNIT     ");
        //        oStringBuilder.AppendLine("            ,:OPTM_EFF          ");
        //        oStringBuilder.AppendLine("            ,:OPTM_ENERGY_WON   ");
        //        oStringBuilder.AppendLine("            ,:PUMPSTATION_ID    ");
        //        oStringBuilder.AppendLine("            )                   ");

        //        foreach (UltraGridRow row in this.GridStandard.Rows)
        //        {
        //            string ftridn = Convert.ToString(row.Cells["FTR_IDN"].Value);
        //            if (!m_DetailData.ContainsKey(ftridn)) continue;

        //            DataTable datatable = m_DetailData[ftridn];

        //            foreach (DataRow trow in datatable.Rows)
        //            {
        //                IDataParameter[] oParams = {
        //                    new OracleParameter(":PUMP_FTR_IDN", OracleDbType.Varchar2, 50),
        //                    new OracleParameter(":TIMESTAMP", OracleDbType.Date),
        //                    new OracleParameter(":FLOW", OracleDbType.Double),
        //                    new OracleParameter(":H", OracleDbType.Double),
        //                    new OracleParameter(":ENERGY", OracleDbType.Double),
        //                    new OracleParameter(":WON_UNIT", OracleDbType.Double),
        //                    new OracleParameter(":EFF", OracleDbType.Double),
        //                    new OracleParameter(":ENERGY_WON", OracleDbType.Double),
        //                    new OracleParameter(":OPTM_FLOW", OracleDbType.Double),
        //                    new OracleParameter(":OPTM_H", OracleDbType.Double),
        //                    new OracleParameter(":OPTM_ENERGY", OracleDbType.Double),
        //                    new OracleParameter(":OPTM_WON_UNIT", OracleDbType.Double),
        //                    new OracleParameter(":OPTM_EFF", OracleDbType.Double),
        //                    new OracleParameter(":OPTM_ENERGY_WON", OracleDbType.Double),
        //                    new OracleParameter(":PUMPSTATION_ID", OracleDbType.Varchar2)
        //                };

        //                oParams[0].Value = row.Cells["FTR_IDN"].Value;
        //                oParams[1].Value = Convert.ToDateTime(trow["TIMESTAMP"]);
        //                //Console.WriteLine(row.Cells["FTR_IDN"].Value.ToString() + " : " + trow["TIMESTAMP"].ToString());

        //                oParams[2].Value = Convert.IsDBNull(trow["FLOW"]) ? 0 : trow["FLOW"];
        //                oParams[3].Value = Convert.IsDBNull(trow["H"]) ? 0 :trow["H"];
        //                oParams[4].Value = Convert.IsDBNull(trow["ENERGY"]) ? 0 : trow["ENERGY"];
        //                oParams[5].Value = Convert.IsDBNull(trow["WON_UNIT"]) ? 0 : trow["WON_UNIT"];
        //                oParams[6].Value = Convert.IsDBNull(trow["O_EFFICIENCY"]) ? 0 : trow["O_EFFICIENCY"];
        //                oParams[7].Value = Convert.IsDBNull(trow["ENERGY_WON"]) ? 0 : trow["ENERGY_WON"];
        //                oParams[8].Value = Convert.IsDBNull(trow["OPTM_FLOW"]) ? 0 : trow["OPTM_FLOW"];
        //                oParams[9].Value = Convert.IsDBNull(trow["OPTM_H"]) ? 0 : trow["OPTM_H"];
        //                oParams[10].Value = Convert.IsDBNull(trow["OPTM_ENERGY"]) ? 0 : trow["OPTM_ENERGY"];
        //                oParams[11].Value = Convert.IsDBNull(trow["OPTM_WON_UNIT"]) ? 0 : trow["OPTM_WON_UNIT"];
        //                oParams[12].Value = Convert.IsDBNull(trow["OPTM_EFF"]) ? 0 : trow["OPTM_EFF"];
        //                oParams[13].Value = Convert.IsDBNull(trow["OPTM_ENERGY_WON"]) ? 0 : trow["OPTM_ENERGY_WON"];
        //                oParams[14].Value = row.Cells["PUMPSTATION_ID"].Value;

        //                m_oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);
        //            }
        //        }
                
        //        //MessageBox.Show("정상적으로 저장되었습니다.");

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
           
        //}

        //private void DeleteData(string bzsId, string startday, string endday)
        //{
        //    StringBuilder oStringBuilder = new StringBuilder();
        //    oStringBuilder.AppendLine("truncate table we_optimum_rpt");

        //    //oStringBuilder.AppendLine("DELETE WE_OPTIMUM_RPT");
        //    //oStringBuilder.AppendLine("WHERE PUMPSTATION_ID = '" + this.cboBzs.SelectedValue + "'");
        //    //oStringBuilder.AppendLine("	 and timestamp between to_date('" + startday + "0000','yyyymmddhh24mi')         ");
        //    //oStringBuilder.AppendLine("	                   and to_date('" + endday + "2359','yyyymmddhh24mi')         ");
        //    int i = m_oDBManager.ExecuteScript(oStringBuilder.ToString(), null);
        //    Console.WriteLine(i.ToString());
        //}

        /// <summary>
        /// 계약종별 전력단가 정보 표시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnElecCost_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                WE_SimCommon.frmElecUnitCost oform = new WaterNet.WE_SimCommon.frmElecUnitCost();
                oform.Show();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// 펌프규격별 설계효율 정보 표시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDesignPump_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                WE_SimCommon.frmDesignPumpEff oform = new WaterNet.WE_SimCommon.frmDesignPumpEff();
                oform.Show();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// 실시간 관망모의 해석결과 질의
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelect_Click(object sender, EventArgs e)
        {
            string startday = dateTimePicker1.Value.Year.ToString();
            startday += dateTimePicker1.Value.Month.ToString().PadLeft(2, '0');
            startday += dateTimePicker1.Value.Day.ToString().PadLeft(2, '0');

            string endday = dateTimePicker2.Value.Year.ToString();
            endday += dateTimePicker2.Value.Month.ToString().PadLeft(2, '0');
            endday += dateTimePicker2.Value.Day.ToString().PadLeft(2, '0');

            StringBuilder oStringBuilder = new StringBuilder();
            #region 광역
            //oStringBuilder.AppendLine("    with pump as                                                                                                      ");
            //oStringBuilder.AppendLine("    (																			                                                                           ");
            //oStringBuilder.AppendLine("		select c.value1 pump_ftr_idn                                                                                       ");
            //oStringBuilder.AppendLine("		      ,b.remark remark                                                                                             ");
            //oStringBuilder.AppendLine("           ,d.id                                                                                                      ");
            //oStringBuilder.AppendLine("           ,d.title                                                                                                   ");
            //oStringBuilder.AppendLine("		  from wh_pumps b                                                                                                  ");
            //oStringBuilder.AppendLine("		      ,wh_tags c                                                                                                   ");
            //oStringBuilder.AppendLine("           ,wh_checkpoint d                                                                                           ");
            //oStringBuilder.AppendLine("		  where b.inp_number = (SELECT INP_NUMBER FROM WH_TITLE WHERE ENERGY_GBN = 'Y')                                    ");
            //oStringBuilder.AppendLine("		    and c.inp_number = b.inp_number                                                                                ");
            //oStringBuilder.AppendLine("		    and b.inp_number = d.inp_number                                                                                ");
            //oStringBuilder.AppendLine("		    and c.id = b.id  and c.type = 'LINK'                                                                           ");
            //oStringBuilder.AppendLine("         and b.id = d.id  and d.chpdvicd = '000004'                                                                     ");
            //oStringBuilder.AppendLine("         and d.useyn = 'Y'                                                                                              ");
            //oStringBuilder.AppendLine("    )																			                                                                           ");
            //oStringBuilder.AppendLine("		select a.id, a.title, b.pump_ftr_idn as ftr_idn                                                                                   ");
            //oStringBuilder.AppendLine("			  ,b.pump_gbn                                                                                                  ");
            //oStringBuilder.AppendLine("           ,b.pump_rpm                                                                                                 ");
            //oStringBuilder.AppendLine("           ,b.sim_gbn                                                                                                  ");
            //oStringBuilder.AppendLine("		      ,(select pumpstation_name from we_pump_station where pumpstation_id = b.pumpstation_id) as pumpstation_name  ");
            //oStringBuilder.AppendLine("		      ,b.pumpstation_id                                                                                        ");
            //oStringBuilder.AppendLine("		      ,c.time as times                                                                                        ");
            //oStringBuilder.AppendLine("		      ,c.flow                                                                                                      ");
            //oStringBuilder.AppendLine("		      ,c.h                                                                                                         ");
            ////oStringBuilder.AppendLine("		      ,c.flow OPTM_FLOW                                                                                            ");
            ////oStringBuilder.AppendLine("		      ,c.h optm_h                                                                                                  ");
            //oStringBuilder.AppendLine("		      ,c.energy                                                                                                    ");
            //oStringBuilder.AppendLine("           ,e.h as segment_h                                                                                           ");
            //oStringBuilder.AppendLine("           ,c.energy_won energy_won                                                                                    ");
            //oStringBuilder.AppendLine("      from pump a																                                                                     ");
            //oStringBuilder.AppendLine("          ,we_pump b															                                                                     ");
            //oStringBuilder.AppendLine("          ,(		                                                                                                       ");
            //oStringBuilder.AppendLine("		        select pump_ftr_idn                                                                                        ");
            ////oStringBuilder.AppendLine("		              ,to_date(to_char(timestamp, 'yyyymmdd'),'yyyymmdd') timestamp                                        ");
            //oStringBuilder.AppendLine("		              ,Round(sum(d_time),2) time                                                                                    ");
            //oStringBuilder.AppendLine("		              ,nvl(round(decode(avg(flow),0,null,avg(flow)),0),0) flow                                                    ");
            //oStringBuilder.AppendLine("		              ,nvl(round(decode(avg(h),0,null,avg(h)),0),0) h                                                      ");
            //oStringBuilder.AppendLine("		              ,nvl(round(decode(avg(energy),0,null,avg(energy)),0),0) energy                                                                           ");
            //oStringBuilder.AppendLine("		              ,nvl(round(decode(avg(energy_won),0,null,avg(energy_won)),0),0) energy_won                                                                   ");
            //oStringBuilder.AppendLine("		          from (                                                                                                   ");
            //oStringBuilder.AppendLine("		               select pump_ftr_idn                                                                                 ");
            //oStringBuilder.AppendLine("		                     ,timestamp                                                                                    ");
            //oStringBuilder.AppendLine("		                     ,decode(status, 0, 0,                                                                         ");
            //oStringBuilder.AppendLine("		                      round((lead(timestamp, 1)                                                                    ");
            //oStringBuilder.AppendLine("		                             over (partition by pump_ftr_idn                                                       ");
            //oStringBuilder.AppendLine("		                                       order by timestamp)-timestamp)*(24/1),10)) d_time                            ");
            //oStringBuilder.AppendLine("		                     ,decode(flow,0,null,flow) flow                                                                                    ");
            //oStringBuilder.AppendLine("		                     ,node2_press - node1_press h                                                                  ");
            //oStringBuilder.AppendLine("		                     ,decode(energy,0,null,energy) energy                                                                                ");
            //oStringBuilder.AppendLine("		                     ,decode(energy_won,0,null,energy_won) energy_won                                                                        ");
            //oStringBuilder.AppendLine("		                 from we_rpt                                                                                       ");
            //oStringBuilder.AppendLine("		                where timestamp between to_date('" + startday + "0000','yyyymmddhh24mi')                                   ");
            //oStringBuilder.AppendLine("		                                    and to_date('" + endday + "2359','yyyymmddhh24mi') + 1                               ");
            //oStringBuilder.AppendLine("		               )                                                                                                   ");
            //oStringBuilder.AppendLine("		          where timestamp < to_date('" + endday + "','yyyymmdd') + 1                                                                ");
            //oStringBuilder.AppendLine("		         group by pump_ftr_idn                                                                                  ");
            //oStringBuilder.AppendLine("           ) c																                                                                         ");
            //oStringBuilder.AppendLine("          ,(																	                                                                         ");
            //oStringBuilder.AppendLine("           select a.pump_ftr_idn												                                                               ");
            //oStringBuilder.AppendLine("                 ,b.flow									                                                                             ");
            //oStringBuilder.AppendLine("                 ,b.h										                                                                             ");
            //oStringBuilder.AppendLine("             from we_pump a, we_curve_data b            				                                                       ");
            //oStringBuilder.AppendLine("            where b.curve_id = a.curve_id                                                                             ");
            //oStringBuilder.AppendLine("              and to_number(b.flow) = 0                                                                               ");
            //oStringBuilder.AppendLine("           ) e																                                                                         ");
            //oStringBuilder.AppendLine("     where b.pump_ftr_idn = a.pump_ftr_idn									                                                       ");
            //oStringBuilder.AppendLine("       and c.pump_ftr_idn = b.pump_ftr_idn									                                                       ");
            //oStringBuilder.AppendLine("       and e.pump_ftr_idn = b.pump_ftr_idn					                                                       ");
            //oStringBuilder.AppendLine("       and b.pumpstation_id = '" + cboBzs.SelectedValue + "'"                                                        );
            //oStringBuilder.AppendLine("     order by ftr_idn		                                                               ");
            #endregion 광역
            oStringBuilder.AppendLine("WITH PUMP AS                                                                            ");
            oStringBuilder.AppendLine(" (SELECT A.PUMPSTATION_ID, A.PUMPSTATION_NAME, B.ID, B.REMARK, B.TAGNAME                ");
            oStringBuilder.AppendLine("        ,B.PUMP_GBN, B.PUMP_RPM, B.SIM_GBN, B.CURVE_ID                                  ");
            oStringBuilder.AppendLine("    FROM WE_PUMP_STATION A                                                              ");
            oStringBuilder.AppendLine("        ,WE_PUMP B                                                                      ");
            oStringBuilder.AppendLine("   WHERE A.PUMPSTATION_ID = B.PUMPSTATION_ID                                            ");
            oStringBuilder.AppendLine("     AND A.PUMPSTATION_ID = '" + cboBzs.SelectedValue + "'");
            oStringBuilder.AppendLine(" )                                                                                      ");
            oStringBuilder.AppendLine(" SELECT A.PUMPSTATION_ID, A.PUMPSTATION_NAME, A.ID, A.REMARK                            ");
            oStringBuilder.AppendLine("       ,A.PUMP_GBN, A.PUMP_RPM, A.SIM_GBN, NVL(C.H, 0) AS SEGMENT_H                     ");
            oStringBuilder.AppendLine("       ,B.TIME times, B.FLOW, B.H, B.ENERGY, B.ENERGY_WON                               ");
            oStringBuilder.AppendLine("   FROM PUMP A                                                                          ");
            oStringBuilder.AppendLine("     ,(SELECT ID                                                                        ");
            oStringBuilder.AppendLine("          ,ROUND(SUM(D_TIME),2) TIME                                                    ");
            //oStringBuilder.AppendLine("          ,NVL(ROUND(sum(FLOW) / 6,2),0) FLOW                      ");
            //oStringBuilder.AppendLine("          ,NVL(ROUND(DECODE(AVG(H),0,NULL,AVG(H)),2),0) H                               ");
            //oStringBuilder.AppendLine("          ,NVL(ROUND(sum(ENERGY) / 6,2),0) ENERGY                ");
            //oStringBuilder.AppendLine("          ,NVL(ROUND(sum(ENERGY_WON),2),0) ENERGY_WON    ");
            oStringBuilder.AppendLine("		              ,nvl(round(decode(avg(flow),0,null,avg(flow)),0),0) flow                                                    ");
            oStringBuilder.AppendLine("		              ,nvl(round(decode(avg(h),0,null,avg(h)),0),0) h                                                      ");
            oStringBuilder.AppendLine("		              ,nvl(round(decode(avg(energy),0,null,avg(energy)),0),0) energy                                                                           ");
            oStringBuilder.AppendLine("		              ,nvl(round(decode(avg(energy_won),0,null,avg(energy_won)),0),0) energy_won                                                                   ");

            oStringBuilder.AppendLine("      FROM (                                                                            ");
            oStringBuilder.AppendLine("           SELECT ID                                                                    ");
            oStringBuilder.AppendLine("                 ,TIMESTAMP                                                             ");
            oStringBuilder.AppendLine("                 ,DECODE(STATUS, 0, 0,                                                  ");
            oStringBuilder.AppendLine("                  ROUND((LEAD(TIMESTAMP, 1)                                             ");
            oStringBuilder.AppendLine("                         OVER (PARTITION BY ID                                          ");
            oStringBuilder.AppendLine("                                   ORDER BY TIMESTAMP)-TIMESTAMP)*(24/1),10)) D_TIME    ");
            oStringBuilder.AppendLine("                 ,nvl(DECODE(FLOW,0,NULL,FLOW),0) FLOW                                  ");

            //oStringBuilder.AppendLine("                 ,ABS(NODE2_PRESS - NODE1_PRESS) H                                           ");
            oStringBuilder.AppendLine("                 ,decode(sign((NODE2_PRESS - NODE1_PRESS)),1,(NODE2_PRESS - NODE1_PRESS), 0) H    ");

            oStringBuilder.AppendLine("                 ,nvl(DECODE(ENERGY,0,NULL,ENERGY),0) ENERGY                                   ");
            oStringBuilder.AppendLine("                 ,nvl(DECODE(ENERGY_WON,0,NULL,ENERGY_WON),0) ENERGY_WON                       ");
            oStringBuilder.AppendLine("             FROM WE_RPT                                                                ");
            oStringBuilder.AppendLine("		       where timestamp between to_date('" + startday + "0000','yyyymmddhh24mi')    ");
            oStringBuilder.AppendLine("		                           and to_date('" + endday + "2359','yyyymmddhh24mi')  ");
            oStringBuilder.AppendLine("		      )                                                                            ");
            oStringBuilder.AppendLine("		where timestamp < to_date('" + endday + "','yyyymmdd') + 1                         ");
            oStringBuilder.AppendLine("		  and flow >= 1                         ");
            oStringBuilder.AppendLine("     GROUP BY ID                                                                        ");
            oStringBuilder.AppendLine("     ) B                                                                                ");
            oStringBuilder.AppendLine("   , WE_CURVE_DATA C                                                                    ");
            oStringBuilder.AppendLine("WHERE A.ID = B.ID                                                                       ");
            oStringBuilder.AppendLine("  AND A.CURVE_ID = C.CURVE_ID(+) AND C.FLOW(+) = 0                                      ");
            DataTable datatable = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);
            GridStandard.DataSource = datatable.DefaultView;

            foreach (UltraGridRow row in GridStandard.Rows)
            {
                row.Cells["OPTM_FLOW"].Value = row.Cells["FLOW"].Value;
                row.Cells["OPTM_H"].Value = row.Cells["H"].Value;

                double result = 0;
                ///원단위 = 에너지/유량
                double flow = Convert.IsDBNull(row.Cells["FLOW"].Value) ? 0 : Convert.ToDouble(row.Cells["FLOW"].Value);
                double energy = Convert.IsDBNull(row.Cells["ENERGY"].Value) ? 0 : Convert.ToDouble(row.Cells["ENERGY"].Value);
                if (flow != 0)
                {
                    result = energy / flow;
                }
                row.Cells["WON_UNIT"].Value = (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : Math.Round(result, 6);

                ///효율 = ((양정 / 원단위) * 0.002722) * 100
                double h = Convert.IsDBNull(row.Cells["H"].Value) ? 0 : Convert.ToDouble(row.Cells["H"].Value);
                double wonunit = Convert.IsDBNull(row.Cells["WON_UNIT"].Value) ? 0 : Convert.ToDouble(row.Cells["WON_UNIT"].Value);
                if (wonunit != 0)
                {
                    result = (h / wonunit * 0.002722) * 100;
                    //Console.WriteLine(flow.ToString() + " , " + energy.ToString() + " , " + wonunit.ToString() + " , " + h.ToString() + " , " + result.ToString() + " , " + ((h / wonunit) * 100).ToString());
                }
                row.Cells["O_EFFICIENCY"].Value = (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : Math.Round(result, 3);
                

                ///SPEED
                double rpm = Convert.IsDBNull(row.Cells["PUMP_RPM"].Value) ? 0 : Convert.ToDouble(row.Cells["PUMP_RPM"].Value);
                if (h != 0)
                {
                    if (!Convert.IsDBNull(row.Cells["PUMP_GBN"]))
                    {
                        switch (Convert.ToString(row.Cells["PUMP_GBN"].Value))
                        {
                            case "1001":
                                //양흡입일경우 유량을 2로 나눈다
                                result = (rpm * Math.Pow((flow / 60 / 2), 0.5) / Math.Pow(h, 0.75));
                                break;
                            case "2001":
                                result = (rpm * Math.Pow((flow / 60), 0.5) / Math.Pow(h, 0.75));
                                break;
                        }
                    }
                }
                row.Cells["SPEED"].Value = (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : Math.Round(result, 2);

                ///최적효율 = speed
                row.Cells["OPTM_EFF"].Value = Math.Round(OPTM_EFF(row), 2);

                /// a = 최적양정 - 체절양정 / 최적유량^2
                double optmh = Convert.IsDBNull(row.Cells["OPTM_H"].Value) ? 0 : Convert.ToDouble(row.Cells["OPTM_H"].Value);
                double segmenth = Convert.IsDBNull(row.Cells["SEGMENT_H"].Value) ? 0 : Convert.ToDouble(row.Cells["SEGMENT_H"].Value);
                double optmflow = Convert.IsDBNull(row.Cells["OPTM_FLOW"].Value) ? 0 : Convert.ToDouble(row.Cells["OPTM_FLOW"].Value);

                double a = 0;
                if (optmflow != 0)
                {
                    a = (optmh - segmenth) / Math.Pow(optmflow, 2);
                }
                row.Cells["a"].Value = (double.IsInfinity(a) || double.IsNaN(a)) ? 0 : Math.Round(a, 10);
                row.Cells["b"].Value = segmenth;
            }

            this.GridStandard.ActiveRow = null;

            ///초기화
            m_DetailData.Clear();

        }



        /// <summary>
        /// 최적효율 : 펌프구분(양흡입,편흡입)에 따른 유량, SPeed에
        /// 대한 펌프설계효율로 변경되지 않음.
        /// </summary>
        private double OPTM_EFF(UltraGridRow row)
        {
            double result = 0;

            string pumpgbn = string.Empty;
            if (!Convert.IsDBNull(row.Cells["PUMP_GBN"]))
            {
                switch (Convert.ToString(row.Cells["PUMP_GBN"].Value))
                {
                    case "1001":
                        pumpgbn = "양흡입";
                        break;
                    case "2001":
                        pumpgbn = "편흡입";
                        break;
                }
            }
            //펌프설계효율 데이터가 유량/min으로 설정되어 잇으므로, 유량을 시간으로 나누어 분당유량을 구한다.
            double flow = Convert.IsDBNull(row.Cells["OPTM_FLOW"]) ? 0 : Convert.ToDouble(row.Cells["OPTM_FLOW"].Value);
            //flow = flow / Convert.ToDouble(row.Cells["TIMES"].Value);
            flow = flow / 10;
            double speed = Convert.IsDBNull(row.Cells["SPEED"]) ? 0 : Convert.ToDouble(row.Cells["SPEED"].Value);
            if (!string.IsNullOrEmpty(pumpgbn))
            {
                DataRow[] rows = m_DESIGN_PUMP.Select("PUMP_GBN = '" + pumpgbn + "' AND FLOW >= " + flow, "flow asc");
                if (rows.Length > 0)
                {
                    if (speed <= 100)
                    {
                        result = Convert.IsDBNull(rows[0]["SPEED_100"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_100"]);
                    }
                    else if (speed > 100 && speed <= 150)
                    {
                        result = Convert.IsDBNull(rows[0]["SPEED_150"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_150"]);
                    }
                    else if (speed > 150 && speed <= 200)
                    {
                        result = Convert.IsDBNull(rows[0]["SPEED_200"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_200"]);
                    }
                    else if (speed > 200 && speed <= 250)
                    {
                        result = Convert.IsDBNull(rows[0]["SPEED_250"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_250"]);
                    }
                    else if (speed > 250 && speed <= 300)
                    {
                        result = Convert.IsDBNull(rows[0]["SPEED_300"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_300"]);
                    }
                    else if (speed > 300 && speed <= 350)
                    {
                        result = Convert.IsDBNull(rows[0]["SPEED_350"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_350"]);
                    }
                    else if (speed > 350 && speed <= 400)
                    {
                        result = Convert.IsDBNull(rows[0]["SPEED_400"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_400"]);
                    }
                    else if (speed > 400 && speed <= 450)
                    {
                        result = Convert.IsDBNull(rows[0]["SPEED_450"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_450"]);
                    }
                    else if (speed > 450)
                    {
                        result = Convert.IsDBNull(rows[0]["SPEED_999"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_999"]);
                    }
                }
            }
           

            return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;

        }

        private void frmOptimumSpecSimulate_Load(object sender, EventArgs e)
        {
            this.GridStandard.InitializeRow += new InitializeRowEventHandler(GridInitializeRow);
            this.GridStandard.AfterRowUpdate += new RowEventHandler(GridStandard_AfterRowUpdate);
            this.GridStandard.DoubleClickRow +=new DoubleClickRowEventHandler(GridStandard_DoubleClickRow);
            this.GridStandard.AfterCellUpdate += new CellEventHandler(GridStandard_AfterCellUpdate);
            
        }

        void GridStandard_AfterCellUpdate(object sender, CellEventArgs e)
        {
            switch (e.Cell.Column.Key)
            {
                case "OPTM_FLOW":
                case "OPTM_H":
                case "SEGMENT_H":
                    break;
                default:
                    return;
            }

            UltraGridRow row = e.Cell.Row;

            double optmh = Convert.IsDBNull(row.Cells["OPTM_H"].Value) ? 0 : Convert.ToDouble(row.Cells["OPTM_H"].Value);
            double segmenth = Convert.IsDBNull(row.Cells["SEGMENT_H"].Value) ? 0 : Convert.ToDouble(row.Cells["SEGMENT_H"].Value);
            double optmflow = Convert.IsDBNull(row.Cells["OPTM_FLOW"].Value) ? 0 : Convert.ToDouble(row.Cells["OPTM_FLOW"].Value);

            double a = 0;
            if (optmflow != 0)
            {
                a = (optmh - segmenth) / Math.Pow(optmflow, 2);
            }
            row.Cells["a"].Value = (double.IsInfinity(a) || double.IsNaN(a)) ? 0 : Math.Round(a, 10);
            row.Cells["b"].Value = segmenth;

            row.Cells["OPTM_EFF"].Value = Math.Round(OPTM_EFF(row), 2);
        }


        private void GridInitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridRow row = e.Row;
            double optmflow = Convert.IsDBNull(row.Cells["OPTM_FLOW"].Value) ? 0 : Convert.ToDouble(row.Cells["OPTM_FLOW"].Value);
            if (optmflow == 0)
            {
                row.Cells["OPTM_EFF"].Value = "0";
                row.Cells["OPTM_H"].Value = "0";
            }
        }

        private void GridStandard_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGridRow row = e.Row;

            string id = Convert.ToString(row.Cells["ID"].Value);

            Hashtable parameter = new Hashtable();
            string startday = WaterNetCore.FunctionManager.DateTimeToString(dateTimePicker1.Value);
            string endday = WaterNetCore.FunctionManager.DateTimeToString(dateTimePicker2.Value);
            parameter.Add("STARTDAY", startday);
            parameter.Add("ENDDAY", endday);
            parameter.Add("ID", id);

            SimulationEnergyCalculate2 simCalculate = new SimulationEnergyCalculate2(m_oDBManager, parameter, e.Row);
            if (m_DetailData.ContainsKey(id))
            {
                m_DetailData.Remove(id);
            }
            m_DetailData.Add(id, simCalculate.PumpRptTable);
            this.GridStandardDetail.DataSource = simCalculate.PumpRptTable;
        }

        private void GridStandard_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //UltraGridRow row = e.Row;

            //double optmh = Convert.IsDBNull(row.Cells["OPTM_H"].Value) ? 0 : Convert.ToDouble(row.Cells["OPTM_H"].Value);
            //double segmenth = Convert.IsDBNull(row.Cells["SEGMENT_H"].Value) ? 0 : Convert.ToDouble(row.Cells["SEGMENT_H"].Value);
            //double optmflow = Convert.IsDBNull(row.Cells["OPTM_FLOW"].Value) ? 0 : Convert.ToDouble(row.Cells["OPTM_FLOW"].Value);

            //double a = 0;
            //if (optmflow != 0)
            //{
            //    a = (optmh - segmenth) / Math.Pow(optmflow, 2);
            //}
            //row.Cells["a"].Value = (double.IsInfinity(a) || double.IsNaN(a)) ? 0 : Math.Round(a, 10);
            //row.Cells["b"].Value = segmenth;

            //row.Cells["OPTM_EFF"].Value = Math.Round(OPTM_EFF(row), 2);
        }


        /// <summary>
        /// 최적규격분석 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExecute_Click(object sender, EventArgs e)
        {
            //if (this.GridStandard.ActiveRow == null) return;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                WaterNetCore.frmSplashMsg splash = new WaterNetCore.frmSplashMsg();
                splash.Message = "최적규격 분석중입니다.";
                splash.TopMost = true;
                Application.DoEvents();
                splash.Open();
                try
                {
                    foreach (UltraGridRow row in this.GridStandard.Rows)
                    {
                        string id = Convert.ToString(row.Cells["ID"].Value);

                        Hashtable parameter = new Hashtable();
                        string startday = WaterNetCore.FunctionManager.DateTimeToString(dateTimePicker1.Value);
                        string endday = WaterNetCore.FunctionManager.DateTimeToString(dateTimePicker2.Value);
                        parameter.Add("STARTDAY", startday);
                        parameter.Add("ENDDAY", endday);
                        parameter.Add("ID", id);

                        if (m_DetailData.ContainsKey(id))
                        {
                            m_DetailData.Remove(id);
                        }
                        SimulationEnergyCalculate2 simCalculate = new SimulationEnergyCalculate2(m_oDBManager, parameter, row);

                        m_DetailData.Add(id, simCalculate.PumpRptTable);
                    }

                    //btnSave_Click(this, new EventArgs());

                    //MessageBox.Show("펌프 최적규격 분석이 완료되었습니다. \n\r분석데이터를 저장하십시오.");
                }
                catch (Exception)
                {
                    MessageBox.Show("펌프 최적규격 분석중 오류가 발생되었습니다.");
                }
                finally
                {
                    splash.Close();
                }

                frmOptimumSpecview oform = new frmOptimumSpecview();
                oform.cboBzsData = cboBzs.SelectedValue;
                oform.starttime = dateTimePicker1.Value;
                oform.endtime = dateTimePicker2.Value;
                oform.DetailData = m_DetailData;
                oform.Open();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #region 내포클래스 : 에너지 계산식

        public class SimulationEnergyCalculate2
        {
            private DataTable m_datatable = null;
            private DataTable m_DESIGN_PUMP = null;
            private DataTable m_ELECTRIC_UNITCOST = null;
            private DataTable m_ELECTRIC_HOUSECOST = null;
            private UltraGridRow m_Row = null;

            public SimulationEnergyCalculate2(WaterNetCore.OracleDBManager oDBManager, Hashtable parameter)
            {
                string startday = Convert.ToString(parameter["STARTDAY"]);
                string endday = Convert.ToString(parameter["ENDDAY"]);
                string ftridn = Convert.ToString(parameter["ID"]);


                searchDetailData(oDBManager, startday, endday, ftridn);
            }

            public SimulationEnergyCalculate2(WaterNetCore.OracleDBManager oDBManager, Hashtable parameter, UltraGridRow row)
            {
                m_Row = row;

                string startday = Convert.ToString(parameter["STARTDAY"]);
                string endday = Convert.ToString(parameter["ENDDAY"]);
                string id = Convert.ToString(parameter["ID"]);
                
                searchDetailData(oDBManager, startday, endday, id);
                AddColumns(m_datatable);
                CalculateData(m_datatable);
            }

            /// <summary>
            /// 에너지 해석결과 정보를 질의한다.
            /// </summary>
            private void searchDetailData(WaterNetCore.OracleDBManager oDBManager, string startday,string endday, string id)
            {
                StringBuilder oStringBuilder = new StringBuilder();

                oStringBuilder.AppendLine("     select /*+ index_asc(WE_RPT WE_RPT_IDX) */ id id    ");
                oStringBuilder.AppendLine("           ,timestamp                                    ");
                oStringBuilder.AppendLine("           ,round(flow,2) flow                           ");
                //oStringBuilder.AppendLine("           ,round(node2_press - node1_press,2) h         ");
                oStringBuilder.AppendLine("           ,round(decode(sign((NODE2_PRESS - NODE1_PRESS)),1,(NODE2_PRESS - NODE1_PRESS), 0),2) H         ");
                oStringBuilder.AppendLine("           ,round(energy,2) energy                       ");
                oStringBuilder.AppendLine("           ,round(energy_won,2) energy_won               ");
                oStringBuilder.AppendLine("       from we_rpt                                                               ");
                oStringBuilder.AppendLine("      where timestamp between to_date('" + startday + "0000','yyyymmddhh24mi')   ");
                oStringBuilder.AppendLine("                          and to_date('" + endday + "2359','yyyymmddhh24mi') ");
                oStringBuilder.AppendLine("                          and id = '" + id + "'");
                oStringBuilder.AppendLine("      order by id, timestamp desc                         ");

                try
                {
                    m_datatable = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

                    oStringBuilder.Remove(0,oStringBuilder.Length);
                    oStringBuilder.AppendLine("SELECT * FROM WE_DESIGN_PUMP_EFF");
                    m_DESIGN_PUMP = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("SELECT * FROM WE_ELECTRIC_UNITCOST");
                    m_ELECTRIC_UNITCOST = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

                    oStringBuilder.Remove(0, oStringBuilder.Length);
                    oStringBuilder.AppendLine("SELECT * FROM WE_ELECTRIC_HOUSECOST");
                    m_ELECTRIC_HOUSECOST = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw ex;
                }
            }

            private void AddColumns(DataTable m_datatable)
            {
                //m_datatable.Columns.Add("ID", typeof(System.String));
                m_datatable.Columns.Add("REMARK", typeof(System.String));
                m_datatable.Columns.Add("WON_UNIT", typeof(System.Double));
                m_datatable.Columns.Add("O_EFFICIENCY", typeof(System.Double));

                m_datatable.Columns.Add("OPTM_FLOW", typeof(System.Double));
                m_datatable.Columns.Add("OPTM_H", typeof(System.Double));
                m_datatable.Columns.Add("OPTM_ENERGY", typeof(System.Double));
                m_datatable.Columns.Add("OPTM_WON_UNIT", typeof(System.Double));
                m_datatable.Columns.Add("OPTM_EFF", typeof(System.Double));
                m_datatable.Columns.Add("OPTM_ENERGY_WON", typeof(System.Double));
            }

            private void CalculateData(DataTable m_datatable)
            {
                double a = Convert.ToDouble(m_Row.Cells["a"].Value);
                double b = Convert.ToDouble(m_Row.Cells["b"].Value);

                double E = Convert.IsDBNull(m_Row.Cells["OPTM_EFF"]) ? 0 : Convert.ToDouble(m_Row.Cells["OPTM_EFF"].Value); //최적효율
                double Q = Convert.IsDBNull(m_Row.Cells["OPTM_FLOW"]) ? 0 : Convert.ToDouble(m_Row.Cells["OPTM_FLOW"].Value); //최적유량
                string pumpgbn = Convert.IsDBNull(m_Row.Cells["SIM_GBN"]) ? string.Empty : Convert.ToString(m_Row.Cells["SIM_GBN"].Value);

                Console.Write("E : " + E.ToString() + ",");
                Console.Write("Q : " + Q.ToString() + ",");
                Console.WriteLine();

                foreach (DataRow row in m_datatable.Rows)
                {
                    row["ID"] = m_Row.Cells["ID"].Value;
                    row["REMARK"] = m_Row.Cells["REMARK"].Value;

                    ///기존규격:원단위, 효율계산
                    row["WON_UNIT"] = Math.Round(this.UNIT_WON(row), 4);
                    row["O_EFFICIENCY"] = Math.Round(this.O_EFFICIENCY(row), 2);


                    ///최적규격:양정, 에너지, 전력요금, 원단위, 효율계산
                    row["OPTM_FLOW"] = row["FLOW"];  
                    double X = Convert.IsDBNull(row["OPTM_FLOW"]) ? 0 : Convert.ToDouble(row["OPTM_FLOW"]);
                    row["OPTM_H"] = Math.Round(this.OPTM_QH(X, a, b), 2);
                    row["OPTM_EFF"] = Math.Round(this.OPTM_QE(X, E, Q), 4);     //최적효율
                    row["OPTM_ENERGY"] = Math.Round(this.OPTM_ENERGY(row), 2);  //최적전력량 = (유량*양정*9800)/효율/1000

                    row["OPTM_WON_UNIT"] = Math.Round(this.OPTM_WON_UNIT(row), 6);
                    row["OPTM_ENERGY_WON"] = Math.Round(this.OPTM_ENERGY_WON(row, pumpgbn), 2);

                    //Console.WriteLine(row["OPTM_H"].ToString() + "," + row["OPTM_EFF"].ToString() + "," + row["OPTM_ENERGY"].ToString() + "," + row["OPTM_WON_UNIT"].ToString() + "," + row["OPTM_ENERGY_WON"].ToString());
                }
            }

            public DataTable PumpRptTable
            {
                get
                {
                    return m_datatable;
                }
            }


            /// <summary>
            /// 최적원단위
            /// </summary>
            private double OPTM_WON_UNIT(DataRow row)
            {
                double result = 0;
                double dh = Convert.IsDBNull(row["OPTM_H"]) ? 0 : Convert.ToDouble(row["OPTM_H"]);
                //double flow = Convert.IsDBNull(row["OPTM_FLOW"]) ? 0 : Convert.ToDouble(row["OPTM_FLOW"]);
                //double energy = Convert.IsDBNull(row["OPTM_ENERGY"]) ? 0 : Convert.ToDouble(row["OPTM_ENERGY"]);
                double deff = Convert.IsDBNull(row["OPTM_EFF"]) ? 0 : Convert.ToDouble(row["OPTM_EFF"]) / 100;
                //double deff = Convert.IsDBNull(m_Row.Cells["OPTM_EFF"]) ? 0 : Convert.ToDouble(m_Row.Cells["OPTM_EFF"].Value) / 100; //최적효율
                //result = (energy / flow);
                result = (0.002722 * dh / deff);

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;
                
            }

            ///// <summary>
            ///// 최적효율 : 계산식
            ///// </summary>
            //public double OPTM_EFF(UltraGridRow row)
            //{
            //    double result = 0;

            //    //string pumpgbn = string.Empty;
            //    //if (!Convert.IsDBNull(row.Cells["PUMP_GBN"]))
            //    //{
            //    //    switch (Convert.ToString(row.Cells["PUMP_GBN"].Value))
            //    //    {
            //    //        case "1":
            //    //        case "2":
            //    //            pumpgbn = "양흡입";
            //    //            break;
            //    //        case "3":
            //    //        case "4":
            //    //            pumpgbn = "편흡입";
            //    //            break;
            //    //        default:
            //    //            break;
            //    //    }
            //    //}
            //    //double flow = Convert.IsDBNull(row.Cells["OPTM_FLOW"]) ? 0 : Convert.ToDouble(row.Cells["OPTM_FLOW"].Value);
            //    //double speed = Convert.IsDBNull(row.Cells["SPEED"]) ? 0 : Convert.ToDouble(row.Cells["SPEED"].Value);

            //    //DataRow[] rows = m_DESIGN_PUMP.Select("PUMP_GBN = '" + pumpgbn + "' AND FLOW >= " + flow, "flow asc");
            //    //if (rows.Length > 0)
            //    //{
            //    //    if (speed <= 100)
            //    //    {
            //    //        result = Convert.IsDBNull(rows[0]["SPEED_100"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_100"]);
            //    //    }
            //    //    else if (speed > 100 && speed <= 150)
            //    //    {
            //    //        result = Convert.IsDBNull(rows[0]["SPEED_150"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_150"]);
            //    //    }
            //    //    else if (speed > 150 && speed <= 200)
            //    //    {
            //    //        result = Convert.IsDBNull(rows[0]["SPEED_200"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_200"]);
            //    //    }
            //    //    else if (speed > 200 && speed <= 250)
            //    //    {
            //    //        result = Convert.IsDBNull(rows[0]["SPEED_250"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_250"]);
            //    //    }
            //    //    else if (speed > 250 && speed <= 300)
            //    //    {
            //    //        result = Convert.IsDBNull(rows[0]["SPEED_300"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_300"]);
            //    //    }
            //    //    else if (speed > 300 && speed <= 350)
            //    //    {
            //    //        result = Convert.IsDBNull(rows[0]["SPEED_350"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_350"]);
            //    //    }
            //    //    else if (speed > 350 && speed <= 400)
            //    //    {
            //    //        result = Convert.IsDBNull(rows[0]["SPEED_400"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_400"]);
            //    //    }
            //    //    else if (speed > 400 && speed <= 450)
            //    //    {
            //    //        result = Convert.IsDBNull(rows[0]["SPEED_450"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_450"]);
            //    //    }
            //    //    else if (speed > 450)
            //    //    {
            //    //        result = Convert.IsDBNull(rows[0]["SPEED_999"]) ? 0 : Convert.ToDouble(rows[0]["SPEED_999"]);
            //    //    }
            //    //}

            //    ///효율 = ((양정 / 원단위) * 0.002722) * 100
            //    double h = Convert.IsDBNull(row.Cells["OPTM_H"].Value) ? 0 : Convert.ToDouble(row.Cells["OPTM_H"].Value);
            //    double wonunit = Convert.IsDBNull(row.Cells["OPTM_WON_UNIT"].Value) ? 0 : Convert.ToDouble(row.Cells["OPTM_WON_UNIT"].Value);
            //    if (wonunit != 0)
            //    {
            //        result = (h / wonunit * 0.002722) * 100;
            //    }
            //    //row.Cells["O_EFFICIENCY"].Value = (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : Math.Round(result, 3);

            //    return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;

            //}

            /// <summary>
            /// 최적 전력량
            /// </summary>
            private double OPTM_ENERGY(DataRow row)
            {
                double result = 0;
                //유량이 /hour이므로 3600(초)으로 나누어 유량/초으로 변경 : 초당 에너지
                double dflow = Convert.IsDBNull(row["OPTM_FLOW"]) ? 0 : Convert.ToDouble(row["OPTM_FLOW"]) / 3600;
                double dh = Convert.IsDBNull(row["OPTM_H"]) ? 0 : Convert.ToDouble(row["OPTM_H"]);
                //double deff = Convert.IsDBNull(row["OPTM_EFF"]) ? 0 : Convert.ToDouble(row["OPTM_EFF"]);// /100;  //효율

                double deff = Convert.IsDBNull(m_Row.Cells["OPTM_EFF"]) ? 0 : Convert.ToDouble(m_Row.Cells["OPTM_EFF"].Value)/100; //최적효율
                if (deff != 0)
                {
                    result = (9800 * dflow * dh) / deff / 1000;
                    //result = (9800 * dflow * dh) / deff;
                }

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;
            }

            //최적 전력비
            private double OPTM_ENERGY_WON(DataRow row, string pumpgbn)
            {
                double result = 0;

                double energy = Convert.IsDBNull(row["OPTM_ENERGY"]) ? 0 : Convert.ToDouble(row["OPTM_ENERGY"]);
                DateTime datetime = Convert.IsDBNull(row["TIMESTAMP"]) ? DateTime.Now : Convert.ToDateTime(row["TIMESTAMP"]);
                int year = datetime.Year; int month = datetime.Month; int hour = datetime.Hour;
                DataRow[] rows = null;
                //pumpgbn = "004";
                switch (pumpgbn)
                {
                    case "004": //주택용-저압
                    case "005": //주택용-고압
                        double tenergy = energy * DateTime.DaysInMonth(year, month);
                        rows = m_ELECTRIC_HOUSECOST.Select("PROMISE_ID = '" + pumpgbn + "' AND S_KW <= " + energy, "S_KW DESC");
                        if (rows.Length > 0)
                        {
                            foreach (DataRow tmp in rows)
                            {

                            }
                        }   
                            //result = Convert.ToDouble(rows[0]["H" + (hour + 1).ToString()]) * energy;
                        
                        break;
                    default:    //산업용
                        rows = m_ELECTRIC_UNITCOST.Select("PROMISE_ID = '" + pumpgbn + "' AND MON = '" + month + "'");
                        if (rows.Length > 0)
                        {
                            result = Convert.ToDouble(rows[0]["H" + (hour + 1).ToString()]) * energy;
                        }
                        break;
                }

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;

            }

            //운영효율
            private double O_EFFICIENCY(DataRow row)
            {
                double result = 0;
                double h = Convert.ToDouble(row["H"]);

                double unit_won = UNIT_WON(row);
                if (unit_won != 0)
                {
                    result = ((h / unit_won) * 0.002722) * 100;
                }

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;
            }

            private double O_EFFICIENCY(double h, double flow, double energy)
            {
                double result = 0;

                double unit_won = UNIT_WON(flow, energy);
                if (unit_won != 0)
                {
                    result = ((h / unit_won) * 0.002722) * 100;
                }

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;

            }


            //원단위
            private double UNIT_WON(DataRow row)
            {
                double result = 0;
                double flow = Convert.IsDBNull(row["FLOW"]) ? 0 : Convert.ToDouble(row["FLOW"]);
                double energy = Convert.IsDBNull(row["ENERGY"]) ? 0 : Convert.ToDouble(row["ENERGY"]);
                if (flow != 0)
                {
                    result = energy / flow;
                }
                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;
                
            }

            //원단위
            private double UNIT_WON(double flow, double energy)
            {
                double result = 0;
                if (flow != 0)
                {
                    result = energy / flow;
                }
                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;

            }

            //비속도
            public double SPEED(UltraGridRow row)
            {
                double result = 0;
                double h = Convert.IsDBNull(row.Cells["H"].Value) ? 0 : Convert.ToDouble(row.Cells["H"].Value);
                double rpm = Convert.IsDBNull(row.Cells["PUMP_RPM"].Value) ? 0 : Convert.ToDouble(row.Cells["PUMP_RPM"].Value);
                double flow = Convert.IsDBNull(row.Cells["FLOW"].Value) ? 0 : Convert.ToDouble(row.Cells["FLOW"].Value);

                if (h != 0)
                {
                    if (!Convert.IsDBNull(row.Cells["PUMP_GBN"]))
                    {
                        switch (Convert.ToString(row.Cells["PUMP_GBN"].Value))
                        {
                            case "1001":
                                //양흡입일경우 유량을 2로 나눈다
                                result = (rpm * Math.Pow((flow / 60  /2), 0.5) / Math.Pow(h, 0.75));
                                break;
                            case "2001":
                                result = (rpm * Math.Pow((flow / 60), 0.5) / Math.Pow(h, 0.75));
                                break;
                        }
                    }
                }

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;
                
            }

            //전력비
            public double ENERGY_WON(UltraGridRow row)
            {
                double result = 0;

                double energy = Convert.IsDBNull(row.Cells["ENERGY"].Value) ? 0 : Convert.ToDouble(row.Cells["ENERGY"].Value);

                string pumpgbn = Convert.IsDBNull(row.Cells["SIM_GBN"].Value) ? string.Empty : Convert.ToString(row.Cells["SIM_GBN"].Value);

                DateTime datetime = Convert.IsDBNull(row.Cells["TIMESTAMP"]) ? DateTime.Now : Convert.ToDateTime(row.Cells["TIMESTAMP"].Value);
                int month = datetime.Month;
                int hour = datetime.Hour;

                DataRow[] rows = m_ELECTRIC_UNITCOST.Select("PROMISE_ID = '" + pumpgbn + "' AND MON = '" + month + "'");
                if (rows.Length > 0)
                {
                    result = Convert.ToDouble(rows[0]["H" + (hour + 1).ToString()]) * energy;
                }

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;

            }

            /// <summary>
            /// 최적규격 효율 값 찾기
            /// </summary>
            public double OPTM_QE(UltraGridRow row)
            {
                double result = 0;
                double E = Convert.ToDouble(row.Cells["OPTM_EFF"].Value);
                double X = Convert.IsDBNull(row.Cells["X"].Value) ? 0 : Convert.ToDouble(row.Cells["X"].Value);
                double Q = Convert.ToDouble(row.Cells["OPTM_FLOW"].Value);

                result = -E * Math.Pow((X - Q), 2) / Math.Pow(Q, 2) + E;

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;

            }

            /// <summary>
            /// 최적규격 효율계산식 : 유량-효율곡선 Q-E곡선
            /// </summary>
            /// <param name="X">유량</param>
            /// <param name="E">최적 규격효율</param>
            /// <param name="Q">최적 규격유량</param>
            /// <returns></returns>
            public double OPTM_QE(double X, double E, double Q)
            {
                double result = 0;

                result = -E * Math.Pow((X - Q), 2) / Math.Pow(Q, 2) + E;

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;

            }

            /// <summary>
            /// 최적규격 양정 계산식 : 유량-양정 Q-H곡선
            /// </summary>
            /// <param name="X">유량</param>
            /// <param name="a">계수a</param>
            /// <param name="b">체절양정</param>
            /// <returns></returns>
            public double OPTM_QH(double X, double a, double b)
            {

                double result = 0;

                result = (a * Math.Pow(X, 2)) + b;

                return (double.IsInfinity(result) || double.IsNaN(result)) ? 0 : result;

            }

        }

        #endregion 내포클래스 : 에너지 계산식

        private void button1_Click(object sender, EventArgs e)
        {
            frmOptimumSpecview form = new frmOptimumSpecview();
            form.Show();
        }

        private void btnDataView_Click(object sender, EventArgs e)
        {
            DataTable datatable = WE_SimCommon.WE_FunctionManager.GetDatasource2DataTable(this.GridStandard);
            frmOptimumSpecview oform = new frmOptimumSpecview();
            oform.cboBzsData = cboBzs.SelectedValue;
            oform.OptimumSpectable = datatable;

            oform.Show();
        }

       












    }
}
