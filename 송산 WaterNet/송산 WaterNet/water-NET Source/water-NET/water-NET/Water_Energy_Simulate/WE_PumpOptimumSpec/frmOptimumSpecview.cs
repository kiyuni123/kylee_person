﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using ChartFX.WinForms;

using WaterNet.WaterNetCore;
using WaterNet.WE_SimCommon;

#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WE_PumpOptimumSpec
{
    public partial class frmOptimumSpecview : Form, WaterNet.WaterNetCore.IForminterface
    {
        private WaterNetCore.OracleDBManager m_oDBManager = new OracleDBManager();
        
        private ChartManager m_ChartManager = null;            //챠트매니저
        private DataTable m_pumptable = null;                  //WE_PUMP 테이블 

        private DataTable m_OptimumSpectable = null;           //최적규격 해석 결과 테이블

        private Dictionary<string, DataTable> m_DetailData = null;

        public frmOptimumSpecview()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            m_oDBManager.ConnectionString = WaterNet.WaterNetCore.FunctionManager.GetConnectionString();
            m_oDBManager.Open();

            StringBuilder oStringBuilder = new StringBuilder();
            #region 그리드 설정
            
            UltraGridColumn oUltraGridColumn;
            ///펌프최적규격 설정 그리드
            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ID";
            oUltraGridColumn.Header.Caption = "ID";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "REMARK";
            oUltraGridColumn.Header.Caption = "설명";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 120;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "TIMESTAMP";
            oUltraGridColumn.Header.Caption = "날짜";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Date;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Format = "yyyy-MM-dd";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "FLOW";
            oUltraGridColumn.Header.Caption = "유량(㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Format = "###,###,###.##";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Tag = "group2";
            //oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            //oUltraGridColumn.Key = "FLOW_M";
            //oUltraGridColumn.Header.Caption = "유량(㎥/min)";
            //oUltraGridColumn.CellActivation = Activation.NoEdit;
            //oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            //oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            //oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Hidden = false;
            //oUltraGridColumn.Width = 80;

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H";
            oUltraGridColumn.Header.Caption = "양정(m)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group2";

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENERGY_WON";
            oUltraGridColumn.Header.Caption = "전력비(원)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Format = "###,###,###.##";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group2";

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "ENERGY";
            oUltraGridColumn.Header.Caption = "전력량(kWh)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Format = "###,###,###.##";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group2";

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "WON_UNIT";
            oUltraGridColumn.Header.Caption = "원단위(kWh/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group2";

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "EFF";
            oUltraGridColumn.Header.Caption = "효율(%)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Format = "#########.##";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group2";

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_FLOW";
            oUltraGridColumn.Header.Caption = "최적유량(㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Format = "###,###,###.##";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 80;
            oUltraGridColumn.Tag = "group3";
    
            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_H";
            oUltraGridColumn.Header.Caption = "최적양정(m)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;
            oUltraGridColumn.Tag = "group3";

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_ENERGY_WON";
            oUltraGridColumn.Header.Caption = "전력비(원)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Format = "###,###,###";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.Tag = "group3";


            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_ENERGY";
            oUltraGridColumn.Header.Caption = "전력량(kWh)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Format = "###,###,###.##";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.Tag = "group3";


            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_WON_UNIT";
            oUltraGridColumn.Header.Caption = "원단위(kWh/㎥)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.Tag = "group3";

            oUltraGridColumn = GridStandard.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "OPTM_EFF";
            oUltraGridColumn.Header.Caption = "효율(%)";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Right;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //oUltraGridColumn.Format = "#########.##";
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 100;
            oUltraGridColumn.Tag = "group3";

            //-----------------------------------------------------------
            FormManager.SetGridStyle(GridStandard);
            this.SetGridGroup();

            #endregion 그리드 설정

            if (m_ChartManager == null) m_ChartManager = new ChartManager(chart1);
            m_ChartManager.ChartInit();

            ///펌프 목록
            m_pumptable = m_oDBManager.ExecuteScriptDataTable("SELECT * FROM WE_PUMP", null);

            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("select distinct a.pumpstation_id cd,  a.pumpstation_name cdnm ");
            oStringBuilder.AppendLine("  from we_pump_station a                                             ");
            oStringBuilder.AppendLine("      ,we_pump b                                                     ");
            oStringBuilder.AppendLine(" where a.pumpstation_id = b.pumpstation_id                           ");
            FormManager.SetComboBoxEX(cboBzs, oStringBuilder.ToString(), false);
            Bzs_SelectedValueChanged(cboBzs, new EventArgs());
        }
        #endregion 초기화설정


        //------------------------------------------------------------------
        //메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.
        //FormID : 탭에 보여지는 이름
        //FormKey : 현재 프로젝트 이름
        //------------------------------------------------------------------
        #region IForminterface 멤버

        public string FormID
        {
            get { return "펌프 최적규격 조회"; }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion IForminterface 멤버

        public Dictionary<string, DataTable> DetailData
        {
            set
            {
                m_DetailData = value;
            }
        }

        public DataTable OptimumSpectable
        {
            set
            {
                m_OptimumSpectable = value;
            }
        }

        public DateTime starttime
        {
            set
            {
                this.dateTimePicker1.Value = value;
            }
        }

        public DateTime endtime
        {
            set
            {
                this.dateTimePicker2.Value = value;
            }
        }

        public object cboBzsData
        {
            set
            {
                this.cboBzs.SelectedValue = value;
            }
        }
        /// <summary>
        /// 그리드 설정 : 그룹
        /// </summary>
        private void SetGridGroup()
        {
            UltraGridGroup Group1 = GridStandard.DisplayLayout.Bands[0].Groups.Add();
            Group1.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            Group1.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            Group1.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
            Group1.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
            Group1.RowLayoutGroupInfo.LabelSpan = 1;
            Group1.RowLayoutGroupInfo.SpanX = 1;
            Group1.RowLayoutGroupInfo.SpanY = 1;
            Group1.Header.VisiblePosition = 0;

            UltraGridGroup Group2 = GridStandard.DisplayLayout.Bands[0].Groups.Add("기존규격");
            Group2.Header.Caption = "기존규격";
            Group2.Key = "기존규격";
            Group2.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            Group2.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            Group2.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
            Group2.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
            Group2.RowLayoutGroupInfo.LabelSpan = 1;
            Group2.RowLayoutGroupInfo.SpanX = 1;
            Group2.RowLayoutGroupInfo.SpanY = 1;


            UltraGridGroup Group3 = GridStandard.DisplayLayout.Bands[0].Groups.Add("최적규격");
            Group3.Header.Caption = "최적규격";
            Group3.Key = "최적규격";
            Group3.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            Group3.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            Group3.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
            Group3.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
            Group3.RowLayoutGroupInfo.LabelSpan = 1;
            Group3.RowLayoutGroupInfo.SpanX = 1;
            Group3.RowLayoutGroupInfo.SpanY = 1;

            foreach (UltraGridColumn column in GridStandard.DisplayLayout.Bands[0].Columns)
            {
                if (column.Tag != null)
                {
                    if (column.Tag.ToString().Equals("group2"))
                    {
                        Group2.Columns.Add(column);
                    }
                    else if (column.Tag.ToString().Equals("group3"))
                    {
                        Group3.Columns.Add(column);
                    }
                }
                else Group1.Columns.Add(column);

            }
        }

        #region 생성자 및 환경설정 ----------------------------------------------------------------------
     
        #endregion 생성자 및 환경설정 ----------------------------------------------------------------------

        /// <summary>
        /// 높이 변환 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sizeChanged_EventHandler(object sender, EventArgs e)
        {
            int oHeight = this.flowLayoutPanel1.Height + this.groupBox2.Margin.Top + this.groupBox2.Margin.Bottom + 15;
            this.groupBox2.Height = oHeight;
            this.setPanel.Height = oHeight;
        }

        public void Open()
        {
            DataTable entire = m_DetailData.Values.ElementAt(0).Copy();
            for (int i = 1; i < m_DetailData.Values.Count; i++)
            {
                DataTable table = m_DetailData.Values.ElementAt(i);
                entire.Merge(table);
            }

            var query = from o in entire.AsEnumerable()
                         group o by new { ID = o.Field<string>("ID"),
                                          REMARK = o.Field<string>("REMARK"), 
                                          TIMESTAMP = o.Field<DateTime>("TIMESTAMP").ToShortDateString()} into grp
                         orderby grp.Key.ID, grp.Key.REMARK, grp.Key.TIMESTAMP
                        select new
                        {
                            ID = grp.Key.ID,
                            REMARK = grp.Key.REMARK,
                            TIMESTAMP = grp.Key.TIMESTAMP,
                            FLOW = grp.Sum(r=> Convert.ToDouble(r.Field<decimal>("FLOW"))),
                            H = grp.Average(r=> Convert.ToDouble( r.Field<decimal>("H"))),
                            ENERGY = grp.Sum(r => Convert.ToDouble(r.Field<decimal>("ENERGY"))),
                            EFF = grp.Average(r => Convert.ToDouble(r.Field<double>("O_EFFICIENCY"))),
                            WON_UNIT = grp.Average(r => Convert.ToDouble(r.Field<double>("WON_UNIT"))),
                            ENERGY_WON = grp.Sum(r => (Convert.IsDBNull(r.Field<decimal>("ENERGY_WON"))) ? 0 : Convert.ToDouble(r.Field<decimal>("ENERGY_WON"))),
                            OPTM_FLOW = grp.Sum(r => Convert.ToDouble(r.Field<double>("OPTM_FLOW"))),
                            OPTM_H = grp.Average(r => Convert.ToDouble(r.Field<double>("OPTM_H"))),
                            OPTM_ENERGY = grp.Sum(r => Convert.ToDouble(r.Field<double>("OPTM_ENERGY"))),
                            OPTM_EFF = grp.Average(r => Convert.ToDouble(r.Field<double>("OPTM_EFF"))),
                            OPTM_WON_UNIT = grp.Average(r => Convert.ToDouble(r.Field<double>("OPTM_WON_UNIT"))),
                            OPTM_ENERGY_WON = grp.Sum(r => Convert.ToDouble(r.Field<double>("OPTM_ENERGY_WON"))),
                        };

            DataTable datatable = new DataTable();
            datatable.Columns.Add("ID", typeof(System.String));
            datatable.Columns.Add("REMARK", typeof(System.String));
            datatable.Columns.Add("TIMESTAMP", typeof(System.String));
            datatable.Columns.Add("FLOW", typeof(System.Double));
            datatable.Columns.Add("H", typeof(System.Double));
            datatable.Columns.Add("ENERGY", typeof(System.Double));
            datatable.Columns.Add("EFF", typeof(System.Double));
            datatable.Columns.Add("WON_UNIT", typeof(System.Double));
            datatable.Columns.Add("ENERGY_WON", typeof(System.Double));
            datatable.Columns.Add("OPTM_FLOW", typeof(System.Double));
            datatable.Columns.Add("OPTM_H", typeof(System.Double));
            datatable.Columns.Add("OPTM_ENERGY", typeof(System.Double));
            datatable.Columns.Add("OPTM_EFF", typeof(System.Double));
            datatable.Columns.Add("OPTM_WON_UNIT", typeof(System.Double));
            datatable.Columns.Add("OPTM_ENERGY_WON", typeof(System.Double));

            foreach (var item in query.AsEnumerable())
            {
                DataRow row = datatable.NewRow();
                row["ID"] = item.ID;
                row["REMARK"] = item.REMARK;
                row["TIMESTAMP"] = item.TIMESTAMP;
                row["FLOW"] = string.IsNullOrEmpty(Convert.ToString(item.FLOW)) ? 0 : Math.Round(item.FLOW, 2);
                row["H"] = string.IsNullOrEmpty(Convert.ToString(item.H)) ? 0 : Math.Round(item.H, 2);
                row["ENERGY"] = string.IsNullOrEmpty(Convert.ToString(item.ENERGY)) ? 0 : Math.Round(item.ENERGY, 0);
                row["EFF"] = string.IsNullOrEmpty(Convert.ToString(item.EFF)) ? 0 : Math.Round(item.EFF, 2);
                row["WON_UNIT"] = string.IsNullOrEmpty(Convert.ToString(item.WON_UNIT)) ? 0 : Math.Round(item.WON_UNIT, 5);
                row["ENERGY_WON"] = string.IsNullOrEmpty(Convert.ToString(item.ENERGY_WON)) ? 0 : Math.Round(item.ENERGY_WON, 0);
                row["OPTM_FLOW"] = string.IsNullOrEmpty(Convert.ToString(item.OPTM_FLOW)) ? 0 : Math.Round(item.OPTM_FLOW, 2);
                row["OPTM_H"] = string.IsNullOrEmpty(Convert.ToString(item.OPTM_H)) ? 0 : Math.Round(item.OPTM_H, 2);
                row["OPTM_ENERGY"] = string.IsNullOrEmpty(Convert.ToString(item.OPTM_ENERGY)) ? 0 : Math.Round(item.OPTM_ENERGY, 0);
                row["OPTM_EFF"] = string.IsNullOrEmpty(Convert.ToString(item.OPTM_EFF)) ? 0 : Math.Round(item.OPTM_EFF, 2);
                row["OPTM_WON_UNIT"] = string.IsNullOrEmpty(Convert.ToString(item.OPTM_WON_UNIT)) ? 0 : Math.Round(item.OPTM_WON_UNIT, 5);
                row["OPTM_ENERGY_WON"] = string.IsNullOrEmpty(Convert.ToString(item.OPTM_ENERGY_WON)) ? 0 : Math.Round(item.OPTM_ENERGY_WON, 0);

                //Console.Write(item.ID.ToString() + " , ");
                //Console.Write(item.TIMESTAMP.ToString() + " , ");
                //Console.Write(item.FLOW.ToString() + " , ");
                //Console.Write(item.H.ToString() + " , ");
                //Console.Write(item.ENERGY.ToString() + " , ");
                //Console.Write(item.EFF.ToString() + " , ");
                //Console.Write(item.WON_UNIT.ToString() + " , ");
                //Console.Write(item.ENERGY_WON.ToString() + " , ");
                //Console.Write("==>");
                //Console.Write(item.OPTM_FLOW.ToString() + " , ");
                //Console.Write(item.OPTM_H.ToString() + " , ");
                //Console.Write(item.OPTM_ENERGY.ToString() + " , ");
                //Console.Write(item.OPTM_EFF.ToString() + " , ");
                //Console.Write(item.OPTM_WON_UNIT.ToString() + " , ");
                //Console.Write(item.OPTM_ENERGY_WON.ToString());
                //Console.WriteLine();
                datatable.Rows.Add(row);
            }

            datatable.AcceptChanges();
            GridStandard.DataSource = datatable; // m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);


            InitializeChartSetting();
            //btnSearch_Click(this, new EventArgs());
            this.ShowDialog();
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            //this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.flowLayoutPanel1.SizeChanged += new EventHandler(sizeChanged_EventHandler);
            this.cboBzs.SelectedValueChanged += new EventHandler(Bzs_SelectedValueChanged);
            //this.chkOrg.CheckedChanged += new EventHandler(chart_series_CheckedChanged);
            //this.chkAnalysis.CheckedChanged += new EventHandler(chart_series_CheckedChanged);
            this.chkEff.CheckedChanged += new EventHandler(chart_series_CheckedChanged);
            this.chkElec.CheckedChanged += new EventHandler(chart_series_CheckedChanged);
            this.chkUnit.CheckedChanged += new EventHandler(chart_series_CheckedChanged);

            this.chart1.MouseClick += new HitTestEventHandler(item_MouseClick);
            this.chart1.MouseDoubleClick += new HitTestEventHandler(item_MouseDoubleClick);
        }

        /// <summary>
        /// 사업장을 변경하였을때 펌프목록을 표시한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Bzs_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cboBzs.DataSource == null) return;
            this.flowLayoutPanel1.Controls.Clear();

            Console.WriteLine(cboBzs.SelectedValue.ToString());

            DataRow[] rows = m_pumptable.Select("PUMPSTATION_ID = '" + cboBzs.SelectedValue + "'", "ID ASC");
            
            foreach (DataRow row in rows)
            { 
                CheckBox chk = new CheckBox();
                chk.AutoSize = true;
                chk.Name = row["ID"].ToString();
                chk.Text = row["REMARK"].ToString();
                chk.Checked = false;

                chk.CheckedChanged += new EventHandler(chart_series_pumpCheckedChange);
                this.flowLayoutPanel1.Controls.Add(chk);
            }

            sizeChanged_EventHandler(this, new EventArgs());
        }

        /// <summary>
        /// 펌프 체크 체인지 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chart_series_pumpCheckedChange(object sender, EventArgs e)
        {
            bool isCheck = false;
            foreach (Control chk in this.flowLayoutPanel1.Controls)
            {
                if (chk is CheckBox)
                {
                    if (((CheckBox)chk).Checked)
                    {
                        if (chkEff.Checked)
                        {
                            chart_series_CheckedChanged(chkEff, new EventArgs());
                        }
                        if (chkElec.Checked)
                        {
                            chart_series_CheckedChanged(chkElec, new EventArgs());
                        }
                        if (chkUnit.Checked)
                        {
                            chart_series_CheckedChanged(chkUnit, new EventArgs());
                        }
                        //chkEff.Checked = true;
                        //chkElec.Checked = true;
                        //chkUnit.Checked = true;

                        isCheck = true;
                        break;
                    }
                }
            }


            //if (isCheck)
            //{
            //    chart_series_CheckedChanged(chkEff, new EventArgs());
            //    chart_series_CheckedChanged(chkElec, new EventArgs());
            //    chart_series_CheckedChanged(chkUnit, new EventArgs());
            //}
            //else
            //{
            //    chkEff.Checked = false;
            //    chkElec.Checked = false;
            //    chkUnit.Checked = false;
            //}
        }

        /// <summary>
        /// 전력량, 효율, 원단위 체크박스 체인지 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chart_series_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control chk in this.flowLayoutPanel1.Controls)
            {
                if (chk is CheckBox)
                {
                    if (((CheckBox)chk).Checked)
                    {
                        StringBuilder serisekey = new StringBuilder();
                        serisekey.Append(((CheckBox)sender).Text);
                        serisekey.Append("(" + chkOrg.Text + ")");
                        serisekey.Append("(" + ((CheckBox)chk).Text + ")");

                        SeriesAttributes series = m_ChartManager.GetSeries(serisekey.ToString());
                        if (series != null)
                        {
                            series.Visible = ((CheckBox)sender).Checked;
                        }

                        serisekey.Remove(0, serisekey.Length);
                        serisekey.Append(((CheckBox)sender).Text);
                        serisekey.Append("(" + chkAnalysis.Text + ")");
                        serisekey.Append("(" + ((CheckBox)chk).Text + ")");

                        SeriesAttributes series2 = m_ChartManager.GetSeries(serisekey.ToString());
                        if (series2 != null)
                        {
                            series2.Visible = ((CheckBox)sender).Checked;
                        }
                    }

                    else if (!((CheckBox)chk).Checked)
                    {
                        StringBuilder serisekey = new StringBuilder();
                        serisekey.Append(((CheckBox)sender).Text);
                        serisekey.Append("(" + chkOrg.Text + ")");
                        serisekey.Append("(" + ((CheckBox)chk).Text + ")");

                        SeriesAttributes series = m_ChartManager.GetSeries(serisekey.ToString());
                        if (series != null)
                        {
                            series.Visible = false;// ((CheckBox)sender).Checked;
                        }

                        serisekey.Remove(0, serisekey.Length);
                        serisekey.Append(((CheckBox)sender).Text);
                        serisekey.Append("(" + chkAnalysis.Text + ")");
                        serisekey.Append("(" + ((CheckBox)chk).Text + ")");

                        SeriesAttributes series2 = m_ChartManager.GetSeries(serisekey.ToString());
                        if (series2 != null)
                        {
                            series2.Visible = false; // ((CheckBox)sender).Checked;
                        }
                    }
                }
            }
        }
     
        private void InitializeChartSetting()
        {
            this.InitChart();
            m_ChartManager.chart.LegendBox.Visible = true;
            m_ChartManager.chart.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            m_ChartManager.chart.LegendBox.MarginY = 1;
            m_ChartManager.chart.AxisX.Staggered = false;

            if (this.GridStandard.DataSource == null) return;

            //X축 전체를 가져와야한다. : 날짜
            DataTable dt = this.GridStandard.DataSource as DataTable;
            try
            {
                DataTable distinctTable = dt.DefaultView.ToTable(/*distinct*/ true, "TIMESTAMP");
                DataRow[] rows = distinctTable.Select(string.Empty, "TIMESTAMP ASC");
                this.chart1.Data.Points = rows.Length;

                int i = 0;
                foreach (DataRow row in rows)
                {
                    this.chart1.AxisX.Labels[i] = WaterNetCore.FunctionManager.DateTimeToString(Convert.ToDateTime(row["TIMESTAMP"]));
                    i++;
                }
            }
            catch (Exception)
            {   
                return;
            }

            try
            {

                // i = 펌프Control Index, j : 0 =기존규격, 1 = 최적규격, k : 0 = 전력량, 1 = 효율, 2 = 원단위
                for (int k = 0; k < 3; k++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        for (int i = 0; i < this.flowLayoutPanel1.Controls.Count; i++)
                        {
                            int index = 0;
                            StringBuilder serisekey = new StringBuilder();
                            SeriesAttributes serise = m_ChartManager.AddSeries(ref index);
                            serise.Visible = false;

                            switch (k)
                            {
                                case 0:
                                    serisekey.Append(chkElec.Text);
                                    serise.AxisY.Visible = true;
                                    serise.AxisY.Position = AxisPosition.Near;
                                    serise.Gallery = ChartFX.WinForms.Gallery.Bar;
                                    serise.MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
                                    serise.Border.Visible = false;
                                    serise.AxisY.ForceZero = true;
                                    serise.MarkerSize = 1;
                                    serise.AxisY.Title.Text = chkElec.Text;
                                    break;
                                case 1:
                                    serisekey.Append(chkEff.Text);
                                    this.chart1.AxesY.Add(new AxisY());
                                    serise.AxisY = this.chart1.AxesY[1];
                                    serise.AxisY.Visible = true;
                                    serise.AxisY.Position = AxisPosition.Near;
                                    //serise.AxisY.DataFormat.Format = AxisFormat.Number;
                                    //serise.AxisY.DataFormat.CustomFormat = "n4";
                                    //serise.AxisY.LabelsFormat.Format = AxisFormat.Number;
                                    //serise.AxisY.LabelsFormat.CustomFormat = "n4";
                                    serise.Gallery = ChartFX.WinForms.Gallery.Bar;
                                    serise.MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
                                    serise.Border.Visible = false;
                                    serise.AxisY.ForceZero = true;
                                    serise.MarkerSize = 1;
                                    serise.AxisY.Title.Text = chkEff.Text;
                                    break;
                                case 2:
                                    serisekey.Append(chkUnit.Text);
                                    this.chart1.AxesY.Add(new AxisY());
                                    serise.AxisY = this.chart1.AxesY[2];
                                    serise.AxisY.Visible = true;
                                    serise.AxisY.Step = 0.1;
                                    serise.AxisY.Position = AxisPosition.Far;
                                    serise.Gallery = ChartFX.WinForms.Gallery.Bar;
                                    serise.MarkerShape = ChartFX.WinForms.MarkerShape.Circle;
                                    serise.Border.Visible = false;
                                    serise.AxisY.ForceZero = true;
                                    serise.MarkerSize = 1;
                                    serise.AxisY.DataFormat.Format = AxisFormat.Number;
                                    serise.AxisY.DataFormat.CustomFormat = "n4";
                                    serise.AxisY.LabelsFormat.Format = AxisFormat.Number;
                                    serise.AxisY.LabelsFormat.CustomFormat = "n4";
                                    serise.AxisY.Title.Text = chkUnit.Text;
                                    break;
                            }
                            switch (j)
                            {
                                case 0:
                                    serisekey.Append("(" + chkOrg.Text + ")");
                                    break;
                                case 1:
                                    serisekey.Append("(" + chkAnalysis.Text + ")");
                                    break;
                            }
                            serise.Tag = ((CheckBox)this.flowLayoutPanel1.Controls[i]).Name.ToString() + ":" + j.ToString() + ":" + k.ToString();
                            serise.Text = serisekey.ToString() + "(" + ((CheckBox)this.flowLayoutPanel1.Controls[i]).Text + ")";

                            InitializeChartData(serise, index);
                        }
                    }
                }
            }
            catch (Exception)
            {   
                return;
            }

            //this.chart1.AxisX.Title.Text = chkElec.Text;
            //this.chart1.AxisY.Title.Text = chkEff.Text;
            //this.chart1.AxisY2.Title.Text = chkUnit.Text;

            //m_ChartManager.chart.Data.Series = flowLayoutPanel1.Controls.Count * 3 * 2;

        }

        /// <summary>
        /// 챠트 시리즈에 데이터 생성하기
        /// </summary>
        /// <param name="serise"></param>
        private void InitializeChartData(SeriesAttributes serise, int seriseindex)
        {
            DataTable dt = this.GridStandard.DataSource as DataTable;

            string tag = Convert.ToString(serise.Tag);
            string[] split = tag.Split(':');
            //int i = Convert.ToInt16(split[0]);
            int j = Convert.ToInt16(split[1]);
            int k = Convert.ToInt16(split[2]);

            // i = 펌프Control Index, j : 0 =기존규격, 1 = 최적규격, k : 0 = 전력량, 1 = 효율, 2 = 원단위

            StringBuilder filter = new StringBuilder();
            filter.Append("ID = '" + split[0] + "'");
            DataRow[] rows = dt.Select(filter.ToString(), "TIMESTAMP ASC");

            List<double> step = new List<double>();

            foreach (DataRow row in rows)
            {
                if (j == 0)
                {
                    switch (k)
                    {
                        case 0:
                            step.Add(Convert.ToDouble(row["ENERGY"]));
                            break;
                        case 1:
                            step.Add(Convert.ToDouble(row["EFF"]));
                            break;
                        case 2:
                            step.Add(Convert.ToDouble(row["WON_UNIT"]));
                            break;
                    }
                }
                else if (j == 1)
                {
                    switch (k)
                    {
                        case 0:
                            step.Add(Convert.ToDouble(row["OPTM_ENERGY"]));
                            break;
                        case 1:
                            step.Add(Convert.ToDouble(row["OPTM_EFF"]));
                            break;
                        case 2:
                            step.Add(Convert.ToDouble(row["OPTM_WON_UNIT"]));
                            break;
                    }
                }

            }

            for (int n = 0; n < step.Count; n++)
            {
                this.chart1.Data.Y[seriseindex, n] = step[n];
            }
        }

        /// <summary>
        /// 챠트 초기화
        /// </summary>
        private void InitChart()
        {
            this.chart1.AxisY.Sections.Clear();
            this.chart1.ConditionalAttributes.Clear();
            this.chart1.Extensions.Clear();
            this.chart1.LegendBox.CustomItems.Clear();
            this.m_ChartManager.ChartInit();
        }

        /// <summary>
        /// 목록 보이기 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTable_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = false;
            splitContainer1.Panel1Collapsed = !splitContainer1.Panel1Collapsed;
        }

        /// <summary>
        /// 그래프 보이기 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChart_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1Collapsed = false;
            splitContainer1.Panel2Collapsed = !splitContainer1.Panel2Collapsed;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        ///// <summary>
        ///// 데이터 조회 버튼
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void btnSearch_Click(object sender, EventArgs e)
        //{
        //    string startday = WaterNetCore.FunctionManager.DateTimeToString(dateTimePicker1.Value);
        //    string endday = WaterNetCore.FunctionManager.DateTimeToString(dateTimePicker2.Value);

        //    StringBuilder oStringBuilder = new StringBuilder();
        //    //oStringBuilder.AppendLine("with pump as                                                                                                           ");
        //    //oStringBuilder.AppendLine("(																			                                                                                ");
        //    //oStringBuilder.AppendLine("select c.value1 pump_ftr_idn                                                                                           ");
        //    //oStringBuilder.AppendLine("      ,b.remark remark                                                                                                 ");
        //    //oStringBuilder.AppendLine("       ,d.id                                                                                                           ");
        //    //oStringBuilder.AppendLine("       ,d.title                                                                                                        ");
        //    //oStringBuilder.AppendLine("  from wh_title a                                                                                                      ");
        //    //oStringBuilder.AppendLine("      ,wh_pumps b                                                                                                      ");
        //    //oStringBuilder.AppendLine("      ,wh_tags c                                                                                                       ");
        //    //oStringBuilder.AppendLine("          ,wh_checkpoint d                                                                                             ");
        //    //oStringBuilder.AppendLine("  where a.USE_GBN = 'WH'                                                                                               ");
        //    //oStringBuilder.AppendLine("    and b.inp_number = a.inp_number                                                                                    ");
        //    //oStringBuilder.AppendLine("    and c.inp_number = b.inp_number                                                                                    ");
        //    //oStringBuilder.AppendLine("    and c.id = b.id                                                                                                    ");
        //    //oStringBuilder.AppendLine("     and b.id = d.id                                                                                                   ");
        //    //oStringBuilder.AppendLine("     and d.useyn = 'Y'                                                                                                 ");
        //    //oStringBuilder.AppendLine(" )	                                                                                                                  ");
        //    //oStringBuilder.AppendLine("    select a.id                                                                                                        ");
        //    //oStringBuilder.AppendLine("          ,b.remark as title                                                                                           ");
        //    //oStringBuilder.AppendLine("          ,b.pump_ftr_idn as ftr_idn                                                                                   ");
        //    //oStringBuilder.AppendLine("          ,to_char(c.timestamp, 'yyyy-mm-dd') timestamp                                                                ");
        //    //oStringBuilder.AppendLine("          ,(select pumpstation_name from we_pump_station where pumpstation_id = b.pumpstation_id) as pumpstation_name  ");
        //    //oStringBuilder.AppendLine("          ,c.times                                                                                                     ");
        //    //oStringBuilder.AppendLine("          ,c.flow                                                                                                      ");
        //    //oStringBuilder.AppendLine("          ,c.h                                                                                                         ");
        //    //oStringBuilder.AppendLine("          ,c.energy                                                                                                    ");
        //    //oStringBuilder.AppendLine("          ,c.energy_won                                                                                                ");
        //    //oStringBuilder.AppendLine("          ,c.won_unit                                                                                                  ");
        //    //oStringBuilder.AppendLine("          ,c.eff                                                                                                       ");
        //    //oStringBuilder.AppendLine("          ,c.optm_energy                                                                                               ");
        //    //oStringBuilder.AppendLine("          ,c.optm_won_unit                                                                                             ");
        //    //oStringBuilder.AppendLine("          ,c.optm_eff                                                                                                  ");
        //    //oStringBuilder.AppendLine("          ,c.optm_energy_won                                                                                           ");
        //    //oStringBuilder.AppendLine("	from pump a		                                                                                                        ");
        //    //oStringBuilder.AppendLine("	     ,we_pump b															                                                                          ");
        //    //oStringBuilder.AppendLine("	     ,(	                                                                                                              ");
        //    //oStringBuilder.AppendLine("	     select pump_ftr_idn                                                                                              ");
        //    //oStringBuilder.AppendLine("	           ,timestamp                                                                                                 ");
        //    //oStringBuilder.AppendLine("	           ,times                                                                                                     ");
        //    //oStringBuilder.AppendLine("	           ,flow                                                                                                      ");
        //    //oStringBuilder.AppendLine("	           ,h                                                                                                         ");
        //    //oStringBuilder.AppendLine("	           ,energy                                                                                                    ");
        //    //oStringBuilder.AppendLine("	           ,energy_won                                                                                                ");
        //    //oStringBuilder.AppendLine("	           ,won_unit                                                                                                  ");
        //    //oStringBuilder.AppendLine("	           ,eff                                                                                                       ");
        //    //oStringBuilder.AppendLine("	           ,optm_energy                                                                                               ");
        //    //oStringBuilder.AppendLine("	           ,optm_won_unit                                                                                             ");
        //    //oStringBuilder.AppendLine("	           ,optm_eff                                                                                                  ");
        //    //oStringBuilder.AppendLine("	           ,optm_energy_won                                                                                           ");
        //    //oStringBuilder.AppendLine("	       from we_optimum_rpt                                                                                            ");
        //    //oStringBuilder.AppendLine("	      where timestamp between to_date('" + startday +"','yyyymmdd')                                                          ");
        //    //oStringBuilder.AppendLine("	                      and to_date('" + endday + "','yyyymmdd')                                                              ");
        //    //oStringBuilder.AppendLine("	      ) c                                                                                                             ");
        //    //oStringBuilder.AppendLine(" where b.pump_ftr_idn = a.pump_ftr_idn									                                                            ");
        //    //oStringBuilder.AppendLine("   and c.pump_ftr_idn = b.pump_ftr_idn									                                                            ");
        //    //oStringBuilder.AppendLine(" order by pumpstation_id, ftr_idn	                                                                                    ");


        //    oStringBuilder.AppendLine("with pump as                                          ");
        //    oStringBuilder.AppendLine("(																			               ");
        //    oStringBuilder.AppendLine("select distinct c.value1 pump_ftr_idn                          ");
        //    oStringBuilder.AppendLine("      ,b.remark                                 ");
        //    oStringBuilder.AppendLine("       ,d.id                                          ");
        //    //oStringBuilder.AppendLine("       ,d.title                                       ");                                                            
        //    oStringBuilder.AppendLine("  from wh_title a                                     ");
        //    oStringBuilder.AppendLine("      ,wh_pumps b                                     ");
        //    oStringBuilder.AppendLine("      ,wh_tags c                                      ");
        //    oStringBuilder.AppendLine("       ,wh_checkpoint d                               ");
        //    oStringBuilder.AppendLine("  where a.USE_GBN = 'WH'                              ");
        //    oStringBuilder.AppendLine("    and b.inp_number = a.inp_number                   ");
        //    oStringBuilder.AppendLine("    and c.inp_number = b.inp_number                   ");
        //    oStringBuilder.AppendLine("    and c.id = b.id and c.type = 'LINK'               ");
        //    oStringBuilder.AppendLine("     and b.id = d.id  and d.chpdvicd = '000004'       ");
        //    oStringBuilder.AppendLine("     and d.useyn = 'Y'                                ");
        //    oStringBuilder.AppendLine(")																			               ");
        //    oStringBuilder.AppendLine("select a.id, a.remark title, b.pump_ftr_idn as ftr_idn       ");
        //    oStringBuilder.AppendLine("      ,c.timestamp                                    ");
        //    oStringBuilder.AppendLine("      ,(select pumpstation_name from we_pump_station where pumpstation_id = b.pumpstation_id) as pumpstation_name  ");
        //    oStringBuilder.AppendLine("      ,round(c.time,1) as times                                                         ");
        //    oStringBuilder.AppendLine("      ,c.flow flow                                                                      ");
        //    oStringBuilder.AppendLine("      ,c.h h                                                                            ");
        //    oStringBuilder.AppendLine("      ,c.energy energy                                                                  ");
        //    oStringBuilder.AppendLine("      ,c.eff eff                                                                        ");
        //    oStringBuilder.AppendLine("      ,c.won_unit won_unit                                                              ");
        //    oStringBuilder.AppendLine("      ,c.energy_won energy_won                                                          ");
        //    oStringBuilder.AppendLine("      ,c.optm_flow optm_flow                                                            ");
        //    oStringBuilder.AppendLine("      ,c.optm_h optm_h                                                                  ");
        //    oStringBuilder.AppendLine("      ,c.optm_energy optm_energy                                                        ");
        //    oStringBuilder.AppendLine("      ,c.optm_eff optm_eff                                                              ");
        //    oStringBuilder.AppendLine("      ,c.optm_won_unit optm_won_unit                                                    ");
        //    oStringBuilder.AppendLine("      ,c.optm_energy_won optm_energy_won                                                ");
        //    oStringBuilder.AppendLine("    from pump a																                                         ");
        //    oStringBuilder.AppendLine("      ,we_pump b															                                           ");
        //    oStringBuilder.AppendLine("      ,(		                                                                             ");
        //    oStringBuilder.AppendLine("        select pump_ftr_idn                                                             ");
        //    oStringBuilder.AppendLine("            ,to_date(to_char(timestamp, 'yyyymmdd'),'yyyymmdd') timestamp               ");
        //    oStringBuilder.AppendLine("            ,sum(d_time) time                                                           ");
        //    oStringBuilder.AppendLine("            ,round(sum(flow) / 6,2) flow                                                    ");
        //    oStringBuilder.AppendLine("            ,nvl(round(decode(avg(h),0,null,avg(h)),2),0) h                                                          ");
        //    oStringBuilder.AppendLine("            ,round(sum(energy) / 6,2) energy                                                ");
        //    oStringBuilder.AppendLine("            ,nvl(round(decode(avg(eff),0,null,avg(eff)),2),0) eff                                                      ");
        //    oStringBuilder.AppendLine("            ,nvl(round(decode(avg(won_unit),0,null,avg(won_unit)),3),0) won_unit                                            ");
        //    oStringBuilder.AppendLine("            ,round(sum(energy_won)  / 6,2) energy_won                                        ");
        //    oStringBuilder.AppendLine("            ,round(sum(optm_flow) / 6,2) optm_flow                                          ");
        //    oStringBuilder.AppendLine("            ,nvl(round(decode(avg(optm_h),0,null,avg(optm_h)),2),0) optm_h                                                ");
        //    oStringBuilder.AppendLine("            ,round(sum(optm_energy) / 6,2) optm_energy                                      ");
        //    oStringBuilder.AppendLine("            ,nvl(round(decode(avg(optm_eff),0,null,avg(optm_eff)),2),0) optm_eff                                            ");
        //    oStringBuilder.AppendLine("            ,nvl(round(decode(avg(optm_won_unit),0,null,avg(optm_won_unit)),3),0) optm_won_unit                                  ");
        //    oStringBuilder.AppendLine("            ,round(sum(optm_energy_won) / 6,2) optm_energy_won                              ");
        //    oStringBuilder.AppendLine("            from (                                                                      ");
        //    oStringBuilder.AppendLine("            select pump_ftr_idn                                                         ");
        //    oStringBuilder.AppendLine("                 ,timestamp                                                             ");
        //    oStringBuilder.AppendLine("                 ,decode(flow, 0, 0,                                                    ");
        //    oStringBuilder.AppendLine("                  round((lead(timestamp, 1)                                             ");
        //    oStringBuilder.AppendLine("                         over (partition by pump_ftr_idn                                ");
        //    oStringBuilder.AppendLine("                                   order by timestamp)-timestamp)*(24/1),10)) d_time    ");
        //    oStringBuilder.AppendLine("                 ,flow flow                                                             ");
        //    oStringBuilder.AppendLine("                 ,h h                                                                   ");
        //    oStringBuilder.AppendLine("                 ,energy energy                                                         ");
        //    oStringBuilder.AppendLine("                 ,won_unit                                                              ");
        //    oStringBuilder.AppendLine("                 ,eff                                                                   ");
        //    oStringBuilder.AppendLine("                 ,energy_won                                                            ");
        //    oStringBuilder.AppendLine("                 ,optm_flow                                                             ");
        //    oStringBuilder.AppendLine("                 ,optm_h                                                                ");
        //    oStringBuilder.AppendLine("                 ,optm_energy                                                           ");
        //    oStringBuilder.AppendLine("                 ,optm_won_unit                                                         ");
        //    oStringBuilder.AppendLine("                 ,optm_eff                                                              ");
        //    oStringBuilder.AppendLine("                 ,optm_energy_won                                                       ");
        //    oStringBuilder.AppendLine("             from we_optimum_rpt                                                        ");

        //    oStringBuilder.AppendLine("            where timestamp between to_date('" + startday + "0000','yyyymmddhh24mi')    ");
        //    oStringBuilder.AppendLine("                                and to_date('" + endday + "2359','yyyymmddhh24mi')      ");
        //    oStringBuilder.AppendLine("              and pumpstation_id = '" + cboBzs.SelectedValue + "'");
        //    oStringBuilder.AppendLine("            )                                                                           ");
        //    oStringBuilder.AppendLine("            where timestamp < to_date('" + endday + "','yyyymmdd') + 1                        ");
        //    oStringBuilder.AppendLine("            group by pump_ftr_idn, to_date(to_char(timestamp, 'yyyymmdd'),'yyyymmdd')   ");
        //    oStringBuilder.AppendLine("       ) c																                                               ");
        //    oStringBuilder.AppendLine(" where b.pump_ftr_idn = a.pump_ftr_idn									                                 ");
        //    oStringBuilder.AppendLine("   and c.pump_ftr_idn = b.pump_ftr_idn									                                 ");
        //    oStringBuilder.AppendLine(" order by ftr_idn, timestamp		                                         ");

        //    DataTable table = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

        //    foreach (DataRow row in table.Rows)
        //    {
        //        if (Convert.IsDBNull(row["FLOW"]) || Convert.IsDBNull(row["ENERGY"])) row["WON_UNIT"] = 0;
        //        else if (Convert.ToDouble(row["FLOW"]) == 0) row["WON_UNIT"] = 0;
        //        else row["WON_UNIT"] = Math.Round(Convert.ToDouble(row["ENERGY"]) / Convert.ToDouble(row["FLOW"]), 5);

        //        if (Convert.IsDBNull(row["H"]) || Convert.IsDBNull(row["WON_UNIT"])) row["EFF"] = 0;
        //        else if (Convert.ToDouble(row["WON_UNIT"]) == 0) row["EFF"] = 0;
        //        else row["EFF"] = Math.Round((0.002722 * Convert.ToDouble(row["H"]) / Convert.ToDouble(row["WON_UNIT"]) * 100), 2);

        //        if (Convert.IsDBNull(row["OPTM_FLOW"]) || Convert.IsDBNull(row["OPTM_ENERGY"])) row["OPTM_WON_UNIT"] = 0;
        //        else if (Convert.ToDouble(row["OPTM_FLOW"]) == 0) row["OPTM_WON_UNIT"] = 0;
        //        else row["OPTM_WON_UNIT"] = Math.Round(Convert.ToDouble(row["OPTM_ENERGY"]) / Convert.ToDouble(row["OPTM_FLOW"]), 5);

        //        if (Convert.IsDBNull(row["OPTM_H"]) || Convert.IsDBNull(row["OPTM_WON_UNIT"])) row["OPTM_EFF"] = 0;
        //        else if (Convert.ToDouble(row["OPTM_WON_UNIT"]) == 0) row["OPTM_EFF"] = 0;
        //        else row["OPTM_EFF"] = Math.Round((0.002722 * Convert.ToDouble(row["OPTM_H"]) / Convert.ToDouble(row["OPTM_WON_UNIT"]) * 100), 2);
        //    }
        //    table.AcceptChanges();
        //    GridStandard.DataSource = table; // m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);


        //    InitializeChartSetting();
        //}

        private void frmOptimumSpecview_Load(object sender, EventArgs e)
        {
            this.InitializeEvent();
        }

        //private void InitializeEvent(Chart item)
        //{
        //    //item.MouseDoubleClick += new ChartFX.WinForms.HitTestEventHandler(item_MouseDoubleClick);
        //    item.MouseClick += new HitTestEventHandler(item_MouseClick);
        //}

        private void item_MouseClick(object sender, HitTestEventArgs e)
        {
            Chart chart = (Chart)sender;

            e = chart.HitTest(e.AbsoluteLocation.X, e.AbsoluteLocation.Y, true);

            if (e.HitType.ToString() == "LegendBox")
            {
                if (chart.Series.Count == 1)
                {
                    return;
                }

                if (e.Series != -1)
                {
                    SeriesAttributes series = chart.Series[e.Series];

                    //숨겨진 목록에 있음 -> 목록에 제거하고 보임
                    if (this.hiddenSeries.ContainsKey(series))
                    {
                        List<double> data = hiddenSeries[series];

                        for (int i = 0; i < chart.Data.Points; i++)
                        {
                            chart.Data[e.Series, i] = data[i];
                        }

                        this.hiddenSeries.Remove(series);

                        chart.LegendBox.ItemAttributes[chart.Series, e.Series].TextColor = System.Drawing.Color.Black;

                    }
                    //숨겨진 목록에 없음 -> 목록에 추가하고 숨김
                    else if (!this.hiddenSeries.ContainsKey(series))
                    {
                        List<double> data = new List<double>();
                        for (int i = 0; i < chart.Data.Points; i++)
                        {
                            data.Add(chart.Data[e.Series, i]);
                            chart.Data[e.Series, i] = Chart.Hidden;
                        }

                        this.hiddenSeries.Add(series, data);

                        chart.LegendBox.ItemAttributes[chart.Series, e.Series].TextColor = System.Drawing.Color.DarkGray;
                    }
                }
            }
        }

        private Dictionary<SeriesAttributes, List<Double>> hiddenSeries = new Dictionary<SeriesAttributes, List<double>>();

        private void item_MouseDoubleClick(object sender, ChartFX.WinForms.HitTestEventArgs e)
        {
            Chart chart = (Chart)sender;

            e = chart.HitTest(e.AbsoluteLocation.X, e.AbsoluteLocation.Y, true);

            if (e.HitType.ToString() == "LegendBox")
            {
                if (chart.Series.Count == 1)
                {
                    return;
                }

                if (e.Series != -1)
                {
                    SeriesAttributes series = chart.Series[e.Series];

                    //숨겨진 목록에 있음
                    if (this.hiddenSeries.ContainsKey(series))
                    {
                        List<double> data = hiddenSeries[series];

                        for (int i = 0; i < chart.Data.Points; i++)
                        {
                            chart.Data[e.Series, i] = data[i];
                        }

                        this.hiddenSeries.Remove(series);

                        //차트 시리즈 제목 색 전환
                    }
                    //숨겨진 목록에 없음
                    else if (!this.hiddenSeries.ContainsKey(series))
                    {
                        List<double> data = new List<double>();
                        for (int i = 0; i < chart.Data.Points; i++)
                        {
                            data.Add(chart.Data[e.Series, i]);
                            chart.Data[e.Series, i] = Chart.Hidden;
                        }
                        this.hiddenSeries.Add(series, data);

                        //차트 시리즈 제목 색 전환
                    }
                }
            }
        }
    }
}
