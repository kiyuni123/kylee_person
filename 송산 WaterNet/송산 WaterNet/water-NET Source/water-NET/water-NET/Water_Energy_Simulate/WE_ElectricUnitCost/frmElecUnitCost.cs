﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
#region UltraGrid를 사용=>namespace선언
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace WaterNet.WE_ElectricUnitCost
{
    public partial class frmElecUnitCost : Form
    {
        WaterNetCore.OracleDBManager m_oDBManager = new OracleDBManager();

        public frmElecUnitCost()
        {
            InitializeComponent();

            InitializeSetting();
        }

        #region 초기화설정
        private void InitializeSetting()
        {
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            m_oDBManager.Open();

            #region 그리드 설정
            UltraGridColumn oUltraGridColumn;

            ///관로사고 이력 그리드
            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "PROMISE_ID";
            oUltraGridColumn.Header.Caption = "관리번호";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "MON";
            oUltraGridColumn.Header.Caption = "월";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "BASECOST";
            oUltraGridColumn.Header.Caption = "기본요금";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H1";
            oUltraGridColumn.Header.Caption = "1h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H2";
            oUltraGridColumn.Header.Caption = "2h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H3";
            oUltraGridColumn.Header.Caption = "3h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;
            oUltraGridColumn.ValueList = WaterNetCore.FormManager.SetValueList_Code("STA");

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H4";
            oUltraGridColumn.Header.Caption = "4h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H5";
            oUltraGridColumn.Header.Caption = "5h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H6";
            oUltraGridColumn.Header.Caption = "6h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H7";
            oUltraGridColumn.Header.Caption = "7h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = true;
            oUltraGridColumn.Width = 50;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H8";
            oUltraGridColumn.Header.Caption = "8h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 70;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H9";
            oUltraGridColumn.Header.Caption = "9h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H10";
            oUltraGridColumn.Header.Caption = "10h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H11";
            oUltraGridColumn.Header.Caption = "11h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H12";
            oUltraGridColumn.Header.Caption = "12h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H13";
            oUltraGridColumn.Header.Caption = "13h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H14";
            oUltraGridColumn.Header.Caption = "14h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H15";
            oUltraGridColumn.Header.Caption = "15h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H16";
            oUltraGridColumn.Header.Caption = "16h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H17";
            oUltraGridColumn.Header.Caption = "17h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H18";
            oUltraGridColumn.Header.Caption = "18h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H19";
            oUltraGridColumn.Header.Caption = "19h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H20";
            oUltraGridColumn.Header.Caption = "20h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H21";
            oUltraGridColumn.Header.Caption = "21h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H22";
            oUltraGridColumn.Header.Caption = "22h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H23";
            oUltraGridColumn.Header.Caption = "23h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;

            oUltraGridColumn = GridElectricUnit.DisplayLayout.Bands[0].Columns.Add();
            oUltraGridColumn.Key = "H24";
            oUltraGridColumn.Header.Caption = "24h";
            oUltraGridColumn.CellActivation = Activation.NoEdit;
            oUltraGridColumn.CellClickAction = CellClickAction.RowSelect;
            oUltraGridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            oUltraGridColumn.CellAppearance.TextHAlign = HAlign.Left;
            oUltraGridColumn.CellAppearance.TextVAlign = VAlign.Middle;
            oUltraGridColumn.Hidden = false;
            oUltraGridColumn.Width = 60;
            WaterNetCore.FormManager.SetGridStyle(GridElectricUnit);

            #endregion 그리드 설정

            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT * ");
            oStringBuilder.AppendLine("FROM we_electric_unitcost                                                  ");
            GridElectricUnit.DataSource = m_oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null);

        }
        #endregion 초기화설정
    }
}
