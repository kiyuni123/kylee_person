﻿namespace WaterNet.WaterAOCore
{
    partial class frmSetText
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnConform = new System.Windows.Forms.Button();
            this.checkBoxShow = new System.Windows.Forms.CheckBox();
            this.txtminScale = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtmaxScale = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboFields = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.picPreView = new System.Windows.Forms.PictureBox();
            this.btnConfig = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.picPreView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(208, 263);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(94, 35);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "취소";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnConform
            // 
            this.btnConform.Location = new System.Drawing.Point(108, 263);
            this.btnConform.Name = "btnConform";
            this.btnConform.Size = new System.Drawing.Size(94, 35);
            this.btnConform.TabIndex = 9;
            this.btnConform.Text = "확인";
            this.btnConform.UseVisualStyleBackColor = true;
            this.btnConform.Click += new System.EventHandler(this.btnConform_Click);
            // 
            // checkBoxShow
            // 
            this.checkBoxShow.AutoSize = true;
            this.checkBoxShow.Location = new System.Drawing.Point(30, 20);
            this.checkBoxShow.Name = "checkBoxShow";
            this.checkBoxShow.Size = new System.Drawing.Size(128, 16);
            this.checkBoxShow.TabIndex = 11;
            this.checkBoxShow.Text = "주석을 표시합니다.";
            this.checkBoxShow.UseVisualStyleBackColor = true;
            this.checkBoxShow.CheckedChanged += new System.EventHandler(this.checkBoxShow_CheckedChanged);
            // 
            // txtminScale
            // 
            this.txtminScale.Location = new System.Drawing.Point(218, 79);
            this.txtminScale.Name = "txtminScale";
            this.txtminScale.Size = new System.Drawing.Size(65, 21);
            this.txtminScale.TabIndex = 16;
            this.txtminScale.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmaxScale_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(134, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 15;
            this.label2.Text = "≤ 축척 ≤ 1 /";
            // 
            // txtmaxScale
            // 
            this.txtmaxScale.Location = new System.Drawing.Point(69, 79);
            this.txtmaxScale.Name = "txtmaxScale";
            this.txtmaxScale.Size = new System.Drawing.Size(59, 21);
            this.txtmaxScale.TabIndex = 14;
            this.txtmaxScale.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtminScale_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "1 /";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(201, 12);
            this.label3.TabIndex = 17;
            this.label3.Text = "다음 축척내에서만 주석이 보입니다.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 18;
            this.label4.Text = "주석내용";
            // 
            // cboFields
            // 
            this.cboFields.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFields.FormattingEnabled = true;
            this.cboFields.Location = new System.Drawing.Point(113, 143);
            this.cboFields.Name = "cboFields";
            this.cboFields.Size = new System.Drawing.Size(185, 20);
            this.cboFields.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 20;
            this.label5.Text = "심볼";
            // 
            // picPreView
            // 
            this.picPreView.Location = new System.Drawing.Point(37, 194);
            this.picPreView.Name = "picPreView";
            this.picPreView.Size = new System.Drawing.Size(244, 51);
            this.picPreView.TabIndex = 21;
            this.picPreView.TabStop = false;
            // 
            // btnConfig
            // 
            this.btnConfig.Location = new System.Drawing.Point(227, 164);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(65, 28);
            this.btnConfig.TabIndex = 22;
            this.btnConfig.Text = "설정";
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(25, 51);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(272, 71);
            this.panel1.TabIndex = 23;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(23, 173);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(272, 82);
            this.panel2.TabIndex = 24;
            // 
            // frmSetText
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(318, 306);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.picPreView);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cboFields);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtminScale);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtmaxScale);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxShow);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConform);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSetText";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "라벨설정";
            ((System.ComponentModel.ISupportInitialize)(this.picPreView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnConform;
        private System.Windows.Forms.CheckBox checkBoxShow;
        private System.Windows.Forms.TextBox txtminScale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtmaxScale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboFields;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picPreView;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}