﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;

namespace WaterNet.WaterAOCore
{
    public class SymbolAccess
    {
        private ISimpleRenderer m_SimpleRenderer = null;
        private IUniqueValueRenderer m_UniqueRenderer = null;
        private IClassBreaksRenderer m_ClassRenderer = null;
        private IDataExclusion m_DataExclusion = null;
        private ISymbol m_Symbol = null;

        private string m_UniqueValue = string.Empty;
        private int m_ClassIndex = -1;
        private int m_Image;

        public void SimpleRenderer(ISimpleRenderer Renderer)
        {
            m_SimpleRenderer = Renderer;
            m_Symbol = Renderer.Symbol;
        }

        public void UniqueRenderer(IUniqueValueRenderer Renderer, string Value)
        {
            m_UniqueRenderer = Renderer;
//            Console.WriteLine(m_UniqueRenderer.get_Value(2));
            m_UniqueValue = Value;
            m_Symbol = Renderer.get_Symbol(Value);
        }

        public void ClassRenderer(IClassBreaksRenderer Renderer, int Index)
        {
            m_ClassRenderer = Renderer;
            m_ClassIndex = Index;

            IClone pClone = EsriClone((IClone)Renderer.get_Symbol(Index));
            m_Symbol = pClone as ISymbol;
        }

        private IClone EsriClone(IClone Clone)
        {
            IClone pClone = Clone.Clone();
            return pClone;
        }
        public void DataExclusion(IDataExclusion Renderer)
        {
          m_DataExclusion = Renderer;
          m_Symbol = Renderer.ExclusionSymbol;
        }

        public ISymbol Selector()
        {
            ISymbol pSymbol = SymbolSelect(m_Symbol);

#if DEBUG
            //Console.WriteLine("Selector => " + m_Symbol.GetType().ToString());
#endif
            if (pSymbol == null) return null;

            if (m_SimpleRenderer != null)
            {
                m_SimpleRenderer.Symbol = pSymbol;
            }
            else if (m_UniqueRenderer != null)
            {
                m_UniqueRenderer.set_Symbol(m_UniqueValue, pSymbol);
            }
            else if (m_ClassRenderer != null)
            {
                m_ClassRenderer.set_Symbol(m_ClassIndex, pSymbol);
            }
            return pSymbol;
        }

        public ISymbol SymbolSelect(ISymbol Symbol)
        {
            if (Symbol == null) return null;

            frmSymbolSelect SymSelector = new frmSymbolSelect();
            ISymbol pNewSym = SymSelector.SelectSymbol(Symbol);

            return pNewSym;
        }

        public ISymbol Symbol
        {
            get {
                return m_Symbol;
            }
        }

        public int Image{
            get  {return m_Image; }
            set  { m_Image = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------

    }
}
