﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.ADF;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;

namespace WaterNet.WaterAOCore
{
    public partial class frmLayerDataInfo : Form
    {
        private IMapControl3 m_map = null;
        private IMapControlEvents2_Event MapEvents;
        private EAGL.Data.Wrapper.ITableWrapper m_tr = null;
        private ISymbol SelectSymbol = null;

        public frmLayerDataInfo(EAGL.Data.Wrapper.ITableWrapper tr)
        {
            InitializeComponent();
            this.m_tr = tr;

        }

        public IMapControl3 MAP
        {
            set
            {
                this.m_map = value;
                MapEvents = (IMapControlEvents2_Event)this.m_map;
            }
        }

        private void InitializeEventSetting()
        {
            this.ugData.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(ugData_DoubleClickRow);

            this.MapEvents.OnSelectionChanged += new IMapControlEvents2_OnSelectionChangedEventHandler(MapEvents_OnSelectionChanged);            
        }

        void MapEvents_OnSelectionChanged()
        {
            
        }

        private void ugData_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            IGeometry geometry = (IGeometry)e.Row.Cells["Shape"].Value;
            if (SelectSymbol == null)
                SetSymbol(geometry.GeometryType);

            if (!geometry.IsEmpty)
            {
                IArea area = geometry.Envelope as IArea;
                m_map.CenterAt(area.Centroid);
                Application.DoEvents();
                m_map.FlashShape(geometry, 5, 200, SelectSymbol);
                
            }
        }

        private void SetSymbol(esriGeometryType type)
        {
            //EAGL.Display.SimpleLineSymbol psym = new EAGL.Display.SimpleLineSymbol();

            IRgbColor pColor = new RgbColorClass();
            ILineSymbol pLineSymbol = new SimpleLineSymbolClass();


            switch (type)
            {
                case esriGeometryType.esriGeometryAny:
                    break;
                case esriGeometryType.esriGeometryBag:
                    break;
                case esriGeometryType.esriGeometryBezier3Curve:
                    break;
                case esriGeometryType.esriGeometryCircularArc:
                    break;
                case esriGeometryType.esriGeometryEllipticArc:
                    break;
                case esriGeometryType.esriGeometryEnvelope:
                    break;
                case esriGeometryType.esriGeometryLine:
                    //psym.Color = Color.Red;
                    //psym.Style = esriSimpleLineStyle.esriSLSSolid;
                    //psym.Width = 5;
                    //SelectSymbol = psym.EsriSymbol;
                    pColor = (IRgbColor)ArcManager.GetRandomColor;
                    break;
                case esriGeometryType.esriGeometryMultiPatch:
                    break;
                case esriGeometryType.esriGeometryMultipoint:
                    break;
                case esriGeometryType.esriGeometryNull:
                    break;
                case esriGeometryType.esriGeometryPath:
                    break;
                case esriGeometryType.esriGeometryPoint:
                    //EAGL.Display.SimpleFillSymbol lsym = new EAGL.Display.SimpleFillSymbol();
                    //lsym.Color = Color.Red;
                    //lsym.Style = esriSimpleFillStyle.esriSFSCross;
                    //SelectSymbol = lsym.EsriSymbol; 
                    break;
                case esriGeometryType.esriGeometryPolygon:
                    break;
                case esriGeometryType.esriGeometryPolyline:
                    //psym.Color = Color.Red;
                    //psym.Style = esriSimpleLineStyle.esriSLSSolid;
                    //psym.Width = 5;
                    //SelectSymbol = psym.EsriSymbol;
                    break;
                case esriGeometryType.esriGeometryRay:
                    break;
                case esriGeometryType.esriGeometryRing:
                    break;
                case esriGeometryType.esriGeometrySphere:
                    break;
                case esriGeometryType.esriGeometryTriangleFan:
                    break;
                case esriGeometryType.esriGeometryTriangleStrip:
                    break;
                case esriGeometryType.esriGeometryTriangles:
                    break;
                default:
                    break;
            }
        }

        private void frmLayerDataInfo_Load(object sender, EventArgs e)
        {
            this.InitializeEventSetting();

           
        }

        private void frmLayerDataInfo_Shown(object sender, EventArgs e)
        {
            this.ugData.DataSource = this.m_tr.DATATABLE;
            this.ugData.DisplayLayout.Bands[0].Columns["FID"].Hidden = true;
            this.ugData.DisplayLayout.Bands[0].Columns["Shape"].Hidden = true;
            WaterNetCore.FormManager.SetGridStyle(this.ugData);
            WaterNetCore.FormManager.SetGridStyle_ColumeAllowEdit(this.ugData, 0, this.ugData.DisplayLayout.Bands[0].Columns.Count - 1);
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }
    }
}
