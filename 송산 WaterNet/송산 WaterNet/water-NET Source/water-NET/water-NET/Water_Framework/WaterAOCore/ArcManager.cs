﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.DataSourcesGDB;
using ESRI.ArcGIS.DataSourcesFile;
using ESRI.ArcGIS.DataSourcesRaster;

namespace WaterNet.WaterAOCore
{
    public class ArcManager
    {
        #region Workspace
        ///<summary>
        /// SDE 워크스페이스 인터페이스 구하기
        /// </summary>
        /// <param name="strServer">서버</param>
        /// <param name="strInstance">인스턴스</param>
        /// <param name="strDatabase">데이터베이스</param>
        /// <param name="strUser">사용자</param>
        /// <param name="strPassword">패스워드</param>
        /// <param name="strVersion">버전</param>
        /// <returns>워크스페이스 인터페이스</returns>
        /// <remarks></remarks>
        public static IWorkspace getSDEWorkspace(string strServer, string strInstance, string strDatabase, string strUser, string strPassword, string strVersion)
        {

            IPropertySet pPropertySet = new PropertySetClass();

            pPropertySet.SetProperty("SERVER", strServer);
            pPropertySet.SetProperty("INSTANCE", strInstance);
            pPropertySet.SetProperty("DATABASE", strDatabase);
            pPropertySet.SetProperty("USER", strUser);
            pPropertySet.SetProperty("PASSWORD", strPassword);
            pPropertySet.SetProperty("VERSION", strVersion);

            IWorkspaceFactory2 pWorkspaceFactory = (IWorkspaceFactory2)new SdeWorkspaceFactoryClass();

            return pWorkspaceFactory.Open(pPropertySet, 0);
        }

        /// <summary>
        /// ShapeWorkspace 구하기
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        public static IWorkspace getShapeWorkspace(string sPath)
        {
            System.Object obj = Activator.CreateInstance(Type.GetTypeFromProgID("esriDataSourcesFile.ShapefileWorkspaceFactory"));
            IWorkspaceFactory2 pWSFact = obj as IWorkspaceFactory2;
            IWorkspace pWS = null;
            IPropertySet pProp = new PropertySetClass();

            try
            {
                pProp.SetProperty("DATABASE", sPath);
                
                if (pWSFact.IsWorkspace(sPath))
                {
                    pWS = pWSFact.Open(pProp, 0);
                }
            }
            catch (COMException oComEx)
            {
                Console.WriteLine("getShapeWorkspace : " + oComEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("getShapeWorkspace : " + ex.Message);
            }
            finally
            {
                //Console.WriteLine("getShapeWorkspace" + " : " + System.Runtime.InteropServices.Marshal.FinalReleaseComObject(pWSFact).ToString());
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(pWSFact);
            }

            return pWS;

        }

        /// <summary>
        /// RasterWorkspace 구하기
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        public static IWorkspace getRasterWorkspace(string sPath)
        {
            IWorkspaceFactory2 pWSFact = (IWorkspaceFactory2)new RasterWorkspaceFactoryClass();

            IWorkspace pWS = null;
            try
            {
                if (pWSFact.IsWorkspace(sPath))
                {
                    pWS = pWSFact.OpenFromFile(sPath, 0);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Failed In opening RasterWorkspace");
            }
            finally
            {
                Console.WriteLine("getShapeWorkspace" + " : " + System.Runtime.InteropServices.Marshal.FinalReleaseComObject(pWSFact).ToString());
                //ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pWSFact);
            }
            return pWS;
        }

        /// <summary>
        /// CoverageWorkspace 구하기
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        public static IWorkspace getCoverageWorkspace(string sPath)
        {
            IWorkspaceFactory2 pWSFact = (IWorkspaceFactory2)new ArcInfoWorkspaceFactoryClass();

            IWorkspace pWS = null;
            try
            {
                if (pWSFact.IsWorkspace(sPath))
                {
                    pWS = pWSFact.OpenFromFile(sPath, 0);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Failed In opening CoverageWorkspace");
                throw;
            }
            return pWS;
        }
        
        /// <summary>
        /// PersnalGeo Workspace 구하기
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        public static IWorkspace getAccessWorkspace(string sPath)
        {
            IWorkspaceFactory2 pWSFact = (IWorkspaceFactory2)new AccessWorkspaceFactoryClass();

            IWorkspace pWS = null;
            try
            {
                IPropertySet pPropSet = new PropertySetClass();
                pPropSet.SetProperties("DATABASE", sPath);

                if (pWSFact.IsWorkspace(sPath))
                {
                    pWS = pWSFact.Open(pPropSet, 0);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Failed In opening AccessWorkspace");
            
            }
            finally
            {
                Console.WriteLine("getShapeWorkspace" + " : " + System.Runtime.InteropServices.Marshal.FinalReleaseComObject(pWSFact).ToString());
                //ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pWSFact);
            }
            return pWS;
        }

        public static IWorkspace getWorkspace(ILayer pLayer)
        {
            if (!(pLayer is IFeatureLayer)) return null;

            return getWorkspace(pLayer as IFeatureLayer);

        }

        public static IWorkspace getWorkspace(IFeatureLayer pLayer)
        {
            IDataset pDataset = pLayer.FeatureClass as IDataset;
            if (pDataset == null) return null;
            IWorkspace pWorkspace = pDataset.Workspace;

            return pWorkspace;
        }

        public static IWorkspaceEdit getWorkspaceEdit(IFeatureLayer pLayer)
        {
            return getWorkspace(pLayer) as IWorkspaceEdit;

        }

        public static IWorkspaceEdit getWorkspaceEdit(ILayer pLayer)
        {
            if (!(pLayer is IFeatureLayer)) return null;
            return getWorkspaceEdit(pLayer as IFeatureLayer) as IWorkspaceEdit;

        }

        /// <summary>
        /// ShapeWorkspace 생성하기
        /// </summary>
        /// <param name="parentDir"></param>
        /// <param name="sName"></param>
        /// <returns></returns>
        public static IWorkspace CreateShapeWorkspace(string parentDir, string sName)
        {
            if (!System.IO.Directory.Exists(parentDir)) return null;

            IWorkspace pWS = getShapeWorkspace(parentDir + "\\" + sName);
            if (pWS != null) return pWS;

            IWorkspaceFactory2 pWSFact = null;
            try
            {
                pWSFact = (IWorkspaceFactory2)new ShapefileWorkspaceFactoryClass();

                IWorkspaceName pWSName = pWSFact.Create(parentDir, sName, null, 0);

                IName Name = (IName)pWSName;
                pWS = (IWorkspace)(Name.Open());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(pWSFact);
            }

            return pWS;
        }

        public static IWorkspace CreateInMemoryWorkspace(string sName)
        {
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                ///Create an InMemory workspace factory.
                IWorkspaceFactory workspaceFactory = new InMemoryWorkspaceFactoryClass();
                comReleaser.ManageLifetime(workspaceFactory);

                ESRI.ArcGIS.esriSystem.IPropertySet propertySet = new ESRI.ArcGIS.esriSystem.PropertySetClass();
                propertySet.SetProperty("Name", sName);
                /// Create an InMemory geodatabase.
                IWorkspaceName workspaceName = workspaceFactory.Create("", sName, propertySet, 0);

                /// Cast for IName.
                IName Name = (IName)workspaceName;

                ///Open a reference to the InMemory workspace through the name object.
                IWorkspace workspace = (IWorkspace)Name.Open();

                return workspace;

            }
        }


        #endregion
                
        #region DataSet, FeatureClass
        public static IRasterDataset getRasterDataset(string sPath, string sName)
        {
            IRasterWorkspace pWS = getRasterWorkspace(sPath) as IRasterWorkspace;
            IRasterDataset pRasterDS = null;
            try
            {
                if (pWS != null)
                {
                    pRasterDS = pWS.OpenRasterDataset(sName);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Failed In opening RasterDataset");   
                throw;
            }

            return pRasterDS;
        }

        public static IFeatureClass getShapeFeatureClass(string sPath, string sName)
        {
            IWorkspace pWS = getShapeWorkspace(sPath);

            return getShapeFeatureClass(pWS, sName);
        }

        public static IFeatureClass getShapeFeatureClass(IWorkspace pWS, string sName)
        {
            if (pWS == null) return null;
            if (!(pWS is IFeatureWorkspace)) return null;

            return getShapeFeatureClass((IFeatureWorkspace)pWS, sName);
        }

        public static IFeatureClass getShapeFeatureClass(IFeatureWorkspace pWS, string sName)
        {
            if (pWS == null) return null;
            if (!(pWS is IFeatureWorkspace)) return null;

            IFeatureClass pFC = null;
            try
            {
                pFC = pWS.OpenFeatureClass(sName);
            }
            catch (Exception)
            {
                Console.WriteLine("Failed In opening ShapeFeatureClass");
            }

            return pFC;
        }

        public static IFeatureClass getCoverageFeatureClass(string sPath, string sName, int ClassID)
        {
            IFeatureWorkspace pWS = getCoverageWorkspace(sPath) as IFeatureWorkspace;
            IFeatureClass pFC = null;
            try
            {
                if (pWS != null)
                {
                    IFeatureDataset pFDS = pWS.OpenFeatureDataset(sName);
                    
                    pFDS = pWS.OpenFeatureDataset(sName);

                    IFeatureClassContainer pFCContainer = pFDS as IFeatureClassContainer;
                    pFC = pFCContainer.get_Class(ClassID);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Failed In opening CoverageFeatureClass");
                throw;
            }

            return pFC;
        }

        /// <summary>
        /// 레이어의 모든 객체를 삭제한다.
        /// </summary>
        public static void DeleteAllFeatures(ILayer pLayer)
        {
            if (!(pLayer is IFeatureLayer)) return;
            IFeatureLayer pFeatureLayer = pLayer as IFeatureLayer;

            IWorkspaceEdit pWorkspaceEdit = getWorkspaceEdit(pFeatureLayer);
            if (pWorkspaceEdit == null) return;

            try
            {
                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureCursor pFeatureCursor = pFeatureLayer.FeatureClass.Search(null, false);
                    comReleaser.ManageLifetime(pFeatureCursor);
                    IFeature pFeature = null;

                    while ((pFeature = pFeatureCursor.NextFeature()) != null)      pFeature.Delete();
                }
                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
            }
            catch (Exception ex)
            {
                pWorkspaceEdit.AbortEditOperation();
                pWorkspaceEdit.StopEditing(false);
                throw ex;
            }
        }
        #endregion

        #region Layer
        public static ILayer GetShapeLayer(IWorkspace pWS, string sName, string sAliasName)
        {
            if (pWS == null) return null;
            if (!(pWS is IFeatureWorkspace)) return null;
            if (pWS.Type != esriWorkspaceType.esriFileSystemWorkspace) return null;
            if (!(pWS.WorkspaceFactory is ShapefileWorkspaceFactory)) return null;

            ESRI.ArcGIS.Carto.IFeatureLayer pFL = null;
            try
            {
                IEnumDataset pEnumDS = pWS.get_Datasets(esriDatasetType.esriDTFeatureClass);
                pEnumDS.Reset();
                IDataset pDS = pEnumDS.Next();
                while (pDS != null)
                {
                    if (pDS.Name.ToUpper() == sName.ToUpper())
                    {
                        pFL = new ESRI.ArcGIS.Carto.FeatureLayer();
                        pFL.FeatureClass = pDS as IFeatureClass;
                        pFL.Name = sAliasName;
                        return pFL;
                    }

                    pDS = pEnumDS.Next();
                }
            }
            catch (System.Runtime.InteropServices.COMException eComExc)
            {
                MessageBox.Show(eComExc.ErrorCode.ToString() + " : " + eComExc.Message + " : " + eComExc.Source + " : " + eComExc.HelpLink + " : " + eComExc.StackTrace);
            }

            return pFL;
        }

        public static ILayer GetShapeLayer(IWorkspace pWS, string sName)
        {
            return GetShapeLayer(pWS, sName, sName);
        }

        public static ILayer GetShapeLayer(string sDir, string sName)
        {
            IWorkspace pWS = getShapeWorkspace(sDir);
            if (pWS == null) return null;

            return GetShapeLayer(pWS, sName);
        }

        public static ILayer CreateShapeLayer(IWorkspace pWS, string sName, IFields pFields)
        {
            if (pWS == null) return null;
            if (!(pWS is IFeatureWorkspace)) return null;
            if (pWS.Type != esriWorkspaceType.esriFileSystemWorkspace) return null;
            if (!(pWS.WorkspaceFactory is ShapefileWorkspaceFactory)) return null;

            ILayer pLayer = null;
            try
            {
                IFeatureWorkspace pFWS = pWS as IFeatureWorkspace;
                if (pFWS == null) return null;

                IFeatureClass pFeatureClass = pFWS.CreateFeatureClass(sName, pFields, null, null, esriFeatureType.esriFTSimple, "SHAPE", "");

                ((IFeatureLayer)pLayer).FeatureClass = pFeatureClass;
            }
            catch (System.Runtime.InteropServices.COMException eComExc)
            {
                MessageBox.Show(eComExc.ErrorCode.ToString() + " : " + eComExc.Message + " : " + eComExc.Source + " : " + eComExc.HelpLink + " : " + eComExc.StackTrace);
                
            }


            return pLayer;

        }

        /// <summary>
        /// FeatureClass 생성
        /// </summary>
        /// <param name="objectWorkspace"></param>
        /// <param name="name"></param>
        /// <param name="spatialReference"></param>
        /// <param name="featureType"></param>
        /// <param name="geometryType"></param>
        /// <param name="fields"></param>
        /// <param name="uidCLSID"></param>
        /// <param name="uidCLSEXT"></param>
        /// <param name="configWord"></param>
        /// <returns></returns>
        public static IFeatureClass CreateFeatureClass(object objectWorkspace,
                                                       string name,
                                                       ISpatialReference spatialReference,
                                                       esriFeatureType featureType,
                                                       esriGeometryType geometryType,
                                                       IFields fields,
                                                       UID uidCLSID,
                                                       UID uidCLSEXT,
                                                       string configWord)
        {
            // Check for invalid parameters.
            if (objectWorkspace == null)
            {
                throw (new Exception("[objectWorkspace] cannot be null"));
            }
            if (!((objectWorkspace is IWorkspace) || (objectWorkspace is IFeatureDataset)))
            {
                throw (new Exception("[objectWorkspace] must be IWorkspace or IFeatureDataset"));
            }
            if (name == "")
            {
                throw (new Exception("[name] cannot be empty"));
            }
            if ((objectWorkspace is IWorkspace) && (spatialReference == null))
            {
                throw (new Exception("[spatialReference] cannot be null for StandAlong FeatureClasses"));
            }

            // Set ClassID (if Null)
            if (uidCLSID == null)
            {
                uidCLSID = new UIDClass();
                switch (featureType)
                {
                    case (esriFeatureType.esriFTSimple):
                        uidCLSID.Value = "{52353152-891A-11D0-BEC6-00805F7C4268}";
                        break;
                    case (esriFeatureType.esriFTSimpleJunction):
                        geometryType = esriGeometryType.esriGeometryPoint;
                        uidCLSID.Value = "{CEE8D6B8-55FE-11D1-AE55-0000F80372B4}";
                        break;
                    case (esriFeatureType.esriFTComplexJunction):
                        uidCLSID.Value = "{DF9D71F4-DA32-11D1-AEBA-0000F80372B4}";
                        break;
                    case (esriFeatureType.esriFTSimpleEdge):
                        geometryType = esriGeometryType.esriGeometryPolyline;
                        uidCLSID.Value = "{E7031C90-55FE-11D1-AE55-0000F80372B4}";
                        break;
                    case (esriFeatureType.esriFTComplexEdge):
                        geometryType = esriGeometryType.esriGeometryPolyline;
                        uidCLSID.Value = "{A30E8A2A-C50B-11D1-AEA9-0000F80372B4}";
                        break;
                    case (esriFeatureType.esriFTAnnotation):
                        geometryType = esriGeometryType.esriGeometryPolygon;
                        uidCLSID.Value = "{E3676993-C682-11D2-8A2A-006097AFF44E}";
                        break;
                    case (esriFeatureType.esriFTDimension):
                        geometryType = esriGeometryType.esriGeometryPolygon;
                        uidCLSID.Value = "{496764FC-E0C9-11D3-80CE-00C04F601565}";
                        break;                        
                }
            }

            // Set uidCLSEXT (if Null)
            if (uidCLSEXT == null)
            {
                switch (featureType)
                {
                    case (esriFeatureType.esriFTAnnotation):
                        uidCLSEXT = new UIDClass();
                        uidCLSEXT.Value = "{24429589-D711-11D2-9F41-00C04F6BC6A5}";
                        break;
                    case (esriFeatureType.esriFTDimension):
                        uidCLSEXT = new UIDClass();
                        uidCLSEXT.Value = "{48F935E2-DA66-11D3-80CE-00C04F601565}";
                        break;
                }
            }

            // Add Fields
            if (fields == null)
            {
                // Create fields collection
                fields = new FieldsClass();
                IFieldsEdit fieldsEdit = (IFieldsEdit)fields;

                // Create the geometry field
                IGeometryDef geometryDef = new GeometryDefClass();
                IGeometryDefEdit geometryDefEdit = (IGeometryDefEdit)geometryDef;

                // Assign Geometry Definition
                geometryDefEdit.GeometryType_2 = geometryType;
                geometryDefEdit.GridCount_2 = 1;
                geometryDefEdit.set_GridSize(0, 0.5);
                geometryDefEdit.AvgNumPoints_2 = 2;
                geometryDefEdit.HasM_2 = false;
                geometryDefEdit.HasZ_2 = true;
                if (objectWorkspace is IWorkspace)
                {
                    geometryDefEdit.SpatialReference_2 = spatialReference;
                }

                // Create OID Field
                IField fieldOID = new FieldClass();
                IFieldEdit fieldEditOID = (IFieldEdit)fieldOID;
                fieldEditOID.Name_2 = "ObjectId";
                fieldEditOID.AliasName_2 = "FID";
                fieldEditOID.Type_2 = esriFieldType.esriFieldTypeOID;
                fieldsEdit.AddField(fieldOID);

                // Create Geometry Field
                IField fieldShape = new FieldClass();
                IFieldEdit fieldEditShape = (IFieldEdit)fieldShape;
                fieldEditShape.Name_2 = "Shape";
                fieldEditShape.AliasName_2 = "SHAPE";
                fieldEditShape.Type_2 = esriFieldType.esriFieldTypeGeometry;
                fieldEditShape.GeometryDef_2 = geometryDef;
                fieldsEdit.AddField(fieldShape);
            }

            // Locate Shape Field
            string stringShapeFieldName = string.Empty;
            for (int i = 0; i <= fields.FieldCount - 1; i++)
            {
                if (fields.get_Field(i).Type == esriFieldType.esriFieldTypeGeometry)
                {
                    stringShapeFieldName = fields.get_Field(i).Name;
                    break;
                }
            }
            if (stringShapeFieldName.Length == 0)
            {
                throw (new Exception("Cannot locate geometry field in FIELDS"));
            }

            IFeatureClass featureClass = null;
            try
            {
                if (objectWorkspace is IWorkspace)
                {
                    // Create a STANDALONE FeatureClass
                    IWorkspace workspace = (IWorkspace)objectWorkspace;
                    IFeatureWorkspace featureWorkspace = (IFeatureWorkspace)workspace;

                    featureClass = featureWorkspace.CreateFeatureClass(name, fields, uidCLSID, uidCLSEXT,
                                                                       featureType, stringShapeFieldName,
                                                                       configWord);
                }
                else if (objectWorkspace is IFeatureDataset)
                {
                    IFeatureDataset featureDataset = (IFeatureDataset)objectWorkspace;
                    featureClass = featureDataset.CreateFeatureClass(name, fields, uidCLSID, uidCLSEXT,
                                                                     featureType, stringShapeFieldName,
                                                                     configWord);
                }
            }
            catch (System.Runtime.InteropServices.COMException eComExc)
            {
                MessageBox.Show(eComExc.ErrorCode.ToString() + " : " + eComExc.Message + " : " + eComExc.Source + " : " + eComExc.HelpLink + " : " + eComExc.StackTrace);
            }


            // Return FeatureClass
            return featureClass;
        }

        #endregion

        #region ITable
        public static ITable GetTable(IWorkspace pWorkspace, string tName)
        {
            ITable pTable = null;
            if (!(pWorkspace is IFeatureWorkspace)) return pTable;
            IFeatureWorkspace pFeatureWorkspace = pWorkspace as IFeatureWorkspace;

            pTable = pFeatureWorkspace.OpenTable(tName);

            return pTable;
        }

        public static ITable GetTable(ILayer pLayer)
        {
            ITable pTable = null;
            if (!(pLayer is IFeatureLayer)) return pTable;

            IFeatureLayer pFeatureLayer = pLayer as IFeatureLayer;

            pTable = (ITable)pFeatureLayer.FeatureClass;

            return pTable;
        }

        #endregion

        #region Point
        public static IPointCollection getPointCollection(string strData)
        {
            return getPointCollection(strData, '|');
        }

        public static IPointCollection getPointCollection(string strData, char chDelimeter)
        {
            IPointCollection pPointColletion = new PolylineClass();

            string[] szPairs = strData.Split(chDelimeter);
            IEnumerator ez = szPairs.GetEnumerator();
            string[] szTemp;

            while (ez.MoveNext())
            {
                szTemp = ez.Current.ToString().Split(',');
                double x = Convert.ToDouble(szTemp.GetValue(0));
                double y = Convert.ToDouble(szTemp.GetValue(1));

                IPoint pPoint = CreatePoint4XY(x, y);
                if (!pPoint.IsEmpty)
                {
                    object missing = Type.Missing;
                    pPointColletion.AddPoint(pPoint, ref missing, ref missing);
                }
            }

            return pPointColletion;
        }    

        public static IPoint CreatePoint4XY(double x, double y)
        {
            IPoint pPoint = new PointClass();
            pPoint.PutCoords(x, y);

            return pPoint;
        }

        #endregion
        
        #region Featurelayer 의 라벨(Annotation)

        public static string getLabelField(ILayer pLayer)
        {
            if (!(pLayer is IFeatureLayer)) return string.Empty;

            return getLabelField((IFeatureLayer)pLayer);
        }

        public static string getLabelField(IFeatureLayer pFeatureLayer)
        {
            IAnnotateLayerProperties pAnnoLayerProps = getAnnotateProperty(pFeatureLayer);
            if (pAnnoLayerProps == null) return string.Empty;

            ILabelEngineLayerProperties pLabelEng = pAnnoLayerProps as ILabelEngineLayerProperties;
            string s = pLabelEng.Expression;

            return s.Substring(2, s.Length - 2);
        }

        public static IAnnotateLayerProperties getAnnotateProperty(IFeatureLayer pFeatureLayer)
        {
            if (!(pFeatureLayer is IGeoFeatureLayer)) return null;

            IGeoFeatureLayer pGeoFeatureLayer = pFeatureLayer as IGeoFeatureLayer;

            IAnnotateLayerPropertiesCollection pAnnoLayerPropsColl = pGeoFeatureLayer.AnnotationProperties;

            IAnnotateLayerProperties pAnnoLayerProps;
            IElementCollection iec;
            pAnnoLayerPropsColl.QueryItem(0, out pAnnoLayerProps, out iec, out iec);

            return pAnnoLayerProps;
        }

        public static IAnnotateLayerProperties getAnnotateProperty(ILayer pLayer)
        {
            if (!(pLayer is IFeatureLayer)) return null;

            return getAnnotateProperty((IFeatureLayer)pLayer);
        }

        public static void SetLabelProperty(IFeatureLayer pFeatureLayer, string sField, string sFont, int sFontSize, bool bFontBold, IColor pColor, 
                                                   double dMinScale, double dMaxScale, string sWhereClause)
        {
            if (!(pFeatureLayer is IGeoFeatureLayer)) return;
            IGeoFeatureLayer pGeoFeatureLayer = pFeatureLayer as IGeoFeatureLayer;

            IAnnotateLayerPropertiesCollection pAnnoLayerPropsColl = pGeoFeatureLayer.AnnotationProperties;
            pAnnoLayerPropsColl.Clear();

            //라벨의 특성(위치, Conflict 등등)
            IBasicOverposterLayerProperties4 pBasicOverposterLayerProps = new BasicOverposterLayerPropertiesClass();
            switch (pFeatureLayer.FeatureClass.ShapeType)//判断图层类型
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    pBasicOverposterLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPolygon;
                    pBasicOverposterLayerProps.GenerateUnplacedLabels = true;
                    pBasicOverposterLayerProps.LabelWeight = esriBasicOverposterWeight.esriHighWeight;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    pBasicOverposterLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPoint;
                    pBasicOverposterLayerProps.PointPlacementOnTop = false;
                    pBasicOverposterLayerProps.GenerateUnplacedLabels = true;
                    pBasicOverposterLayerProps.LabelWeight = esriBasicOverposterWeight.esriHighWeight;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                    pBasicOverposterLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPolyline;
                    pBasicOverposterLayerProps.GenerateUnplacedLabels = true;
                    pBasicOverposterLayerProps.LabelWeight = esriBasicOverposterWeight.esriHighWeight;
                    break;
            }
            pBasicOverposterLayerProps.NumLabelsOption = esriBasicNumLabelsOption.esriOneLabelPerShape;

            //스케일별 라벨 보이기/감추기
            IAnnotateLayerProperties pAnnoLayerProps = new LabelEngineLayerPropertiesClass();
            pAnnoLayerProps.AnnotationMinimumScale = dMinScale;
            pAnnoLayerProps.AnnotationMaximumScale = dMaxScale;
            pAnnoLayerProps.CreateUnplacedElements = true;
            pAnnoLayerProps.DisplayAnnotation = true;
            //어느 Feature를 Labeling할 것인가?
            pAnnoLayerProps.WhereClause = sWhereClause;
            
            IFormattedTextSymbol pText = new TextSymbolClass();
            pText.Font.Bold = bFontBold;
            pText.Font.Name = sFont;
            pText.Color = (IColor)pColor;
            pText.Size = sFontSize;

            //라벨 폰트 & 칼라 정의
            ILabelEngineLayerProperties pLabelEng = pAnnoLayerProps as ILabelEngineLayerProperties;
            pLabelEng.Expression = "[" + sField + "]";
            pLabelEng.Symbol = pText;
            pLabelEng.BasicOverposterLayerProperties = pBasicOverposterLayerProps as IBasicOverposterLayerProperties;

            pAnnoLayerPropsColl.Add((IAnnotateLayerProperties)pLabelEng);
            pGeoFeatureLayer.DisplayField = sField;
            pGeoFeatureLayer.DisplayAnnotation = true;

        }


        //public static void SetLabelProperty(IFeatureLayer pFeatureLayer, string sField, stdole.StdFont pFont, IColor pColor, double dMinScale, double dMaxScale, string sWhereClause)
        //{
        //    if (!(pFeatureLayer is IGeoFeatureLayer)) return;

        //    IGeoFeatureLayer pGeoFeatureLayer = pFeatureLayer as IGeoFeatureLayer;

        //    //Console.WriteLine(pGeoFeatureLayer.FeatureClass.Fields.FieldCount.ToString() + " : " + ((IDisplayTable)pGeoFeatureLayer).DisplayTable.Fields.FieldCount.ToString() + " : " + sField);
        //    //for (int i = 0; i < ((IDisplayTable)pGeoFeatureLayer).DisplayTable.Fields.FieldCount; i++)
        //    //{
        //    //    Console.WriteLine(((IDisplayTable)pGeoFeatureLayer).DisplayTable.Fields.get_Field(i).Name);
        //    //}

        //    IAnnotateLayerPropertiesCollection pAnnoLayerPropsColl = pGeoFeatureLayer.AnnotationProperties;
        //    pAnnoLayerPropsColl.Clear();

        //    //라벨의 특성(위치, Conflict 등등)
        //    IBasicOverposterLayerProperties pBasicOverposterLayerProps = new BasicOverposterLayerPropertiesClass();
        //    switch (pFeatureLayer.FeatureClass.ShapeType)//判断图层类型
        //    {
        //        case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
        //            pBasicOverposterLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPolygon;
        //            break;
        //        case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
        //            pBasicOverposterLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPoint;
        //            break;
        //        case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
        //            pBasicOverposterLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPolyline;
        //            break;
        //    }
        //    pBasicOverposterLayerProps.NumLabelsOption = esriBasicNumLabelsOption.esriOneLabelPerShape;
        //    pBasicOverposterLayerProps.GenerateUnplacedLabels = true;
        //    pBasicOverposterLayerProps.PointPlacementMethod = esriOverposterPointPlacementMethod.esriAroundPoint;
        //    pBasicOverposterLayerProps.PointPlacementOnTop = true;

        //    //스케일별 라벨 보이기/감추기
        //    IAnnotateLayerProperties pAnnoLayerProps = new LabelEngineLayerPropertiesClass();
        //    pAnnoLayerProps.AnnotationMinimumScale = dMinScale;
        //    pAnnoLayerProps.AnnotationMaximumScale = dMaxScale;
        //    pAnnoLayerProps.DisplayAnnotation = true;
        //    //어느 Feature를 Labeling할 것인가?
        //    pAnnoLayerProps.WhereClause = sWhereClause;

        //    //stdole.StdFont pFont = new stdole.StdFontClass();
        //    //pFont.Bold = bFontBold;
        //    //pFont.Name = sFont;    //굴림
        //    //pFont.Size = sFontSize;  //10

        //    IFormattedTextSymbol pText = new TextSymbolClass();
        //    //pText.Font = (stdole.IFontDisp)pFont;
        //    pText.Font.Bold = false;
        //    pText.Font.Name = "굴림";
        //    pText.Color = (IColor)pColor;
        //    pText.Size = 9;

        //    //라벨 폰트 & 칼라 정의
        //    ILabelEngineLayerProperties pLabelEng = pAnnoLayerProps as ILabelEngineLayerProperties;
        //    //ILabelEngineLayerProperties pLabelEng = new LabelEngineLayerPropertiesClass();
        //    pLabelEng.Expression = "[" + sField + "]";
        //    pLabelEng.Symbol = pText;
        //    pLabelEng.BasicOverposterLayerProperties = pBasicOverposterLayerProps;

        //    pAnnoLayerPropsColl.Add((IAnnotateLayerProperties)pLabelEng);
        //    pGeoFeatureLayer.DisplayField = sField;
        //    pGeoFeatureLayer.DisplayAnnotation = true;

        //    //pGeoFeatureLayer.DisplayField = sField;
        //    //pGeoFeatureLayer.DisplayAnnotation = true;

        //    //IAnnotateLayerProperties pAnnoLayerProps;
        //    //IAnnotateLayerPropertiesCollection pAnnoLayerPropsColl = pGeoFeatureLayer.AnnotationProperties;

        //    //IElementCollection iec;
        //    //pAnnoLayerPropsColl.QueryItem(0, out pAnnoLayerProps, out iec, out iec);

        //    ////스케일별 라벨 보이기/감추기
        //    //pAnnoLayerProps.AnnotationMinimumScale = dMinScale;
        //    //pAnnoLayerProps.AnnotationMaximumScale = dMaxScale;
        //    //pAnnoLayerProps.DisplayAnnotation = true;
        //    ////어느 Feature를 Labeling할 것인가?
        //    //pAnnoLayerProps.WhereClause = sWhereClause;

        //    ////라벨 폰트 & 칼라 정의
        //    //ILabelEngineLayerProperties pLabelEng = pAnnoLayerProps as ILabelEngineLayerProperties;
        //    //IFormattedTextSymbol pText = new TextSymbolClass();
        //    ////pTextfont.Font = (stdole.IFontDisp)pFont;
        //    //pText.Color = (IColor)pColor;
                
        //    //pLabelEng.Symbol = pText;
        //    //pLabelEng.Expression = "[" + sField + "]";
        //    ////라벨의 특성(위치, Conflict 등등)
        //    //IBasicOverposterLayerProperties pBasicOverposterLayerProps = pLabelEng.BasicOverposterLayerProperties;
        //    //pBasicOverposterLayerProps.NumLabelsOption = esriBasicNumLabelsOption.esriOneLabelPerShape;
        //    //pBasicOverposterLayerProps.GenerateUnplacedLabels = true;

        //}


        #endregion

        #region Definition 설정
        public static void SetFeaturelayerDefinition(IFeatureLayer pFeatureLayer, string sWhereClause)
        {
            if (pFeatureLayer == null) return;
            
            IFeatureLayerDefinition pFLDef;
            IQueryFilter pQF = new QueryFilterClass();
            pQF.WhereClause = sWhereClause;

            if (pFeatureLayer.FeatureClass.FeatureCount(pQF) > 0)
            {
                pFLDef = pFeatureLayer as IFeatureLayerDefinition;
                pFLDef.DefinitionExpression = sWhereClause;
            }
            else
            {
                MessageBox.Show(pFeatureLayer.Name + " 레이어에서 [" + sWhereClause + "] 조건에 맞는 데이터가 없습니다", "레이어필터설정", MessageBoxButtons.OK);
            }
        }

        public static void SetFeaturelayerDefinition2(IFeatureLayer pFeatureLayer, string sWhereClause)
        {
            if (pFeatureLayer == null) return;

            IFeatureLayerDefinition pFLDef;
            IQueryFilter pQF = new QueryFilterClass();
            pQF.WhereClause = sWhereClause;

            pFLDef = pFeatureLayer as IFeatureLayerDefinition;
            pFLDef.DefinitionExpression = sWhereClause;
        }
        #endregion
        
        #region GetLayerRenderer - FeatureLayer이 Renderer를 반환한다.
        public static IFeatureRenderer GetLayerRenderer(ILayer pLayer)
        {
            IFeatureRenderer pRenderer = null;
            
            if (pLayer is IGeoFeatureLayer)
            {
                IGeoFeatureLayer pGeoLayer = (IGeoFeatureLayer)pLayer;
                pRenderer = pGeoLayer.Renderer;
            }
            return pRenderer;
        }

        public static IFeatureRenderer GetLayerRenderer(IFeatureLayer pLayer)
        {
            IFeatureRenderer pRenderer = null;

            if (pLayer is IGeoFeatureLayer)
            {
                IGeoFeatureLayer pGeoLayer = (IGeoFeatureLayer)pLayer;
                pRenderer = pGeoLayer.Renderer;
            }
            return pRenderer;
        }

        #endregion

        #region SetLayerRenderer - FeatureLayer의 Renderer를 설정한다.
        /// <summary>
        /// 레이어의 색상을 설정한다.
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pRend"></param>
        public static void SetLayerRenderer(ILayer pLayer, IFeatureRenderer pRend)
        {
            if (pLayer is IGeoFeatureLayer)
            {
                IGeoFeatureLayer pGeoLayer = (IGeoFeatureLayer)pLayer;
                pGeoLayer.Renderer = pRend;
            }
        }

        /// <summary>
        /// 레이어의 색상을 설정한다.
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pRend"></param>
        public static void SetLayerRenderer(IFeatureLayer pLayer, IFeatureRenderer pRend)
        {
            if (pLayer is IGeoFeatureLayer)
            {
                IGeoFeatureLayer pGeoLayer = (IGeoFeatureLayer)pLayer;
                pGeoLayer.Renderer = pRend;
            }
        }

        #region Default Renderer

        public static void LoadRendererStream(IFeatureLayer pLayer, System.Data.DataRow row)
        {
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            try
            {
                if (!Convert.IsDBNull(row["F_RENDERE"]))
                {
                    byte[] pData = row["F_RENDERE"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;

                    object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IFeatureRenderer"));
                    if (o != null)
                        geoFeatureLayer.Renderer = o as IFeatureRenderer;
                }
                if (!Convert.IsDBNull(row["F_ANNOPROP"]))
                {
                    byte[] pData = row["F_ANNOPROP"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;
                    object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IAnnotateLayerPropertiesCollection"));
                    if (o != null)
                        geoFeatureLayer.AnnotationProperties = o as IAnnotateLayerPropertiesCollection;

                }
                if (!Convert.IsDBNull(row["F_MINSCALE"]))
                {
                    geoFeatureLayer.MinimumScale = Convert.ToDouble(row["F_MINSCALE"]);
                }
                if (!Convert.IsDBNull(row["F_MAXSCALE"]))
                {
                    geoFeatureLayer.MaximumScale = Convert.ToDouble(row["F_MAXSCALE"]);
                }
                if (!Convert.IsDBNull(row["F_FIELDDISP"]))
                {
                    geoFeatureLayer.DisplayField = Convert.ToString(row["F_FIELDDISP"]);
                }
                if (!Convert.IsDBNull(row["F_ANNODISP"]))
                {
                    geoFeatureLayer.DisplayAnnotation = Convert.ToBoolean(row["F_ANNODISP"]);
                }
                if (!Convert.IsDBNull(row["F_TIPDISP"]))
                {
                    geoFeatureLayer.ShowTips = Convert.ToBoolean(row["F_TIPDISP"]);
                }
            }
            catch { }
        }

        /// <summary>
        /// 레이어의 색상, 라벨링 기 설정된 환경으로 초기화한다.
        /// 지도를 ReDrawing한다.
        /// </summary>
        /// <param name="map"></param>
        /// <param name="pLayer"></param>
        public static void SetDefaultRenderer(IMap map, IFeatureLayer pLayer)
        {
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            WaterNetCore.OracleDBManager oDBManager = new WaterNetCore.OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();
                string oQuerystring = "SELECT * FROM SI_LAYER WHERE F_ALIAS = '" + pLayer.Name + "'";
                System.Data.DataTable table = oDBManager.ExecuteScriptDataTable(oQuerystring, null);
                if (table.Rows.Count < 1) return;

                LoadRendererStream(pLayer, table.Rows[0]);
            
                IActiveView pActiveView = map as IActiveView;
                pActiveView.ContentsChanged();
                pActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, pActiveView.Extent);
                
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }

        /// <summary>
        /// 레이어의 색상, 라벨링 기 설정된 환경으로 초기화한다.
        /// </summary>
        /// <param name="map"></param>
        /// <param name="pLayer"></param>
        public static void SetDefaultRenderer2(IMap map, IFeatureLayer pLayer)
        {
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            WaterNetCore.OracleDBManager oDBManager = new WaterNetCore.OracleDBManager();
            oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                oDBManager.Open();
                string oQuerystring = "SELECT * FROM SI_LAYER WHERE F_ALIAS = '" + pLayer.Name + "'";
                System.Data.DataTable table = oDBManager.ExecuteScriptDataTable(oQuerystring, null);
                if (table.Rows.Count < 1) return;

                LoadRendererStream(pLayer, table.Rows[0]);

            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
            }
            finally
            {
                if (oDBManager != null) oDBManager.Close();
            }
        }
        #endregion Default Renderer

        #endregion

        #region ICursor

        public static ICursor GetSortedCursor(ITable ipTable, string sWhereClause, string[] fieldList, bool ascending)
        {
            IQueryFilter pQueryFilter = new QueryFilterClass();
            if (string.IsNullOrEmpty(sWhereClause))
            {
                pQueryFilter.WhereClause = "";
            }
            else
            {
                pQueryFilter.WhereClause = sWhereClause;
            }
            return GetSortedCursor(ipTable, pQueryFilter, fieldList, ascending);

        }

        public static ICursor GetSortedCursor(ITable ipTable, IQueryFilter ipFilter, string[] fieldList, bool ascending)
        {
            ICursor ipCursor = null;
            ITableSort ipTableSort = new TableSortClass();

            ipTableSort.Table = ipTable;
            ipTableSort.QueryFilter = ipFilter;
            ipTableSort.Fields = string.Join(", ", fieldList);

            foreach (string fieldName in fieldList)
            {
                ipTableSort.set_Ascending(fieldName, ascending);

                int idxField = ipTable.FindField(fieldName);
                IField ipField = ipTable.Fields.get_Field(idxField);

                if (ipField.Type == esriFieldType.esriFieldTypeString)
                {
                    ipTableSort.set_CaseSensitive(fieldName, true);
                }
            }

            try
            {
                ipTableSort.Sort(null);

                ipCursor = ipTableSort.Rows;
            }
            catch (Exception Ex)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("GetSortedCursor Failed : {0}", Ex.Message));
            }

            return ipCursor;
        }

        public static ICursor GetSortedCursorGDB(ITable ipTable, IQueryFilter ipFilter, string[] fieldList, bool ascending)
        {
            ICursor ipCursor = null;

            IWorkspace ipWs = ((IDataset)ipTable).Workspace;

            if (ipWs.Type == esriWorkspaceType.esriRemoteDatabaseWorkspace)
            {
                string order = ascending ? " ASC" : " DESC";

                if (ipFilter == null) ipFilter = new QueryFilterClass();

                IQueryFilterDefinition ipFDef = (IQueryFilterDefinition)ipFilter;
                ipFDef.PostfixClause = "ORDER BY " + string.Join(", ", fieldList) + order;

                ipCursor = ipTable.Search(ipFilter, false);
            }
            else
            {
                ipCursor = GetSortedCursor(ipTable, ipFilter, fieldList, ascending);
            }

            return ipCursor;
        }

        public static IFeatureCursor GetSpatialCursor(ILayer pLayer, IGeometry pGeometry, string sWhereClause)
        {
            if (!(pLayer is IFeatureLayer)) return null;
            IFeatureLayer pFeatureLayer = pLayer as IFeatureLayer;
            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.SubFields = "*";
            pSpatialFilter.Geometry = pGeometry;
            pSpatialFilter.GeometryField = pFeatureClass.ShapeFieldName;
            pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelContains;
            pSpatialFilter.WhereClause = sWhereClause;

            IFeatureCursor pFeatureCursor = pFeatureClass.Search(pSpatialFilter, true);

            return pFeatureCursor;
        }

        public static IFeatureCursor GetSpatialCursor(ILayer pLayer, IGeometry pGeometry, esriSpatialRelEnum eSpatialRel, string sWhereClause)
        {
            if (!(pLayer is IFeatureLayer)) return null;
            IFeatureLayer pFeatureLayer = pLayer as IFeatureLayer;
            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.SubFields = "*";
            pSpatialFilter.Geometry = pGeometry;
            pSpatialFilter.GeometryField = pFeatureClass.ShapeFieldName;
            pSpatialFilter.SpatialRel = eSpatialRel;
            pSpatialFilter.WhereClause = sWhereClause;

            IFeatureCursor pFeatureCursor = pFeatureClass.Search(pSpatialFilter, true);

            return pFeatureCursor;
        }

        public static IFeatureCursor GetSpatialCursor(ILayer pLayer, IGeometry pGeometry, double buffer, esriSpatialRelEnum eSpatialRel, string sWhereClause)
        {
            if (!(pLayer is IFeatureLayer)) return null;
            IFeatureLayer pFeatureLayer = pLayer as IFeatureLayer;
            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            IGeometry Geombuffer = GetBuffer(pGeometry, buffer);
            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.SubFields = "*";
            pSpatialFilter.Geometry = Geombuffer;
            pSpatialFilter.GeometryField = pFeatureClass.ShapeFieldName;
            pSpatialFilter.SpatialRel = eSpatialRel;
            pSpatialFilter.WhereClause = sWhereClause;

            IFeatureCursor pFeatureCursor = pFeatureClass.Search(pSpatialFilter, true);

            return pFeatureCursor;
        }

        public static ICursor GetCursor(IFeatureClass pFeatureClass, string sWhereClause)
        {
            IQueryFilter pQueryFilter = new QueryFilterClass();
            pQueryFilter.WhereClause = sWhereClause;

            IFeatureCursor pFeatureCursor = null;
            if (sWhereClause == string.Empty)
            {
                pFeatureCursor = pFeatureClass.Search(null, true);
            }
            else
	        {
                pFeatureCursor = pFeatureClass.Search(pQueryFilter, true);
	        }

            return (ICursor)pFeatureCursor;
        }

        public static ICursor GetCursor(ILayer pLayer, string sWhereClause)
        {
            if (!(pLayer is IFeatureLayer)) return null;
            IFeatureLayer pFeatureLayer = pLayer as IFeatureLayer;
            IFeatureClass pFeatureClass = ((IGeoFeatureLayer)pFeatureLayer).DisplayFeatureClass;
            
            return GetCursor(pFeatureClass, sWhereClause);
        }

        public static ICursor GetCursor(ITable pTable, string sWhereClause)
        {
            IQueryFilter pQueryFilter = new QueryFilterClass();
            pQueryFilter.WhereClause = sWhereClause;

            ICursor pCursor = default(ICursor);
            try
            {
                if (sWhereClause == string.Empty)
                {
                    pCursor = pTable.Search(null, true);
                }
                else
                {
                    pCursor = pTable.Search(pQueryFilter, true);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return pCursor;
        }

        public static IRow GetRow(ITable pTable, int nOID)
        {
            return pTable.GetRow(nOID);
        }

        public static IRow GetRow(ITable pTable, string sWhereClause)
        {
            ICursor pCursor = GetCursor(pTable, sWhereClause);
            if (pCursor == null) return null;

            return pCursor.NextRow();
        }

        public static IFeature GetFeature(ILayer pLayer,int nOID)
        {    
            if (!(pLayer is IFeatureLayer)) return null;
            IFeatureLayer pFeatureLayer = pLayer as IFeatureLayer;
            IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;

            IFeature pFeature = pFeatureClass.GetFeature(nOID);

            return pFeature;
        }

        public static IFeature GetFeature(ILayer pLayer, string sWhereClause)
        {
            if (!(pLayer is IFeatureLayer)) return null;

            IFeature pFeature = null;

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureCursor pFeatureCursor = (IFeatureCursor)GetCursor(pLayer, sWhereClause);
                comReleaser.ManageLifetime(pFeatureCursor);

                if (pFeatureCursor == null) return null;

                pFeature = pFeatureCursor.NextFeature();
            }


            return pFeature;
        }

        public static IGeometry GetBuffer(IGeometry pGeometry, double distance)
        {
            ITopologicalOperator pTopoOp = pGeometry as ITopologicalOperator;
            pTopoOp.Simplify();

            return pTopoOp.Buffer(distance);
        }

        #endregion

        #region iFields
        public static IFields CreateFields(VariableManager.ShapeType shapeType, VariableManager.FieldStruct[] structFields)
        {            
            // Create a new fields collection.
            IFields fields = new FieldsClass();
            IFieldsEdit fieldsEdit = (IFieldsEdit)fields;
            
            // Set the number of fields the collection will contain.
            //fieldsEdit.FieldCount_2 = structFields.Length + 2; //2 : FID, Shape 추가
            
            // Create the ObjectID field.
            IField Field = new FieldClass();            
            IFieldEdit FieldEdit = (IFieldEdit)Field;

            FieldEdit.Name_2 = "ObjectId";
            FieldEdit.AliasName_2 = "FID";
            FieldEdit.Type_2 = esriFieldType.esriFieldTypeOID;
            fieldsEdit.AddField(Field);

            try
            {
                if (structFields != null)
                {
                    // Create the Custom field.
                    foreach (var item in structFields)
                    {
                        Field = new FieldClass();
                        FieldEdit = (IFieldEdit)Field;
                        FieldEdit.Name_2 = item.sFieldName;
                        FieldEdit.AliasName_2 = item.sAliasName;
                        switch (item.sFieldType)
                        {
                            case VariableManager.FieldType.IntegerField:
                                FieldEdit.Type_2 = esriFieldType.esriFieldTypeInteger;
                                break;
                            case VariableManager.FieldType.DoubleField:
                                FieldEdit.Type_2 = esriFieldType.esriFieldTypeDouble;
                                FieldEdit.Precision_2 = item.sFieldLength;
                                FieldEdit.Scale_2 = item.sScale;
                                break;
                            case VariableManager.FieldType.StringField:
                                FieldEdit.Type_2 = esriFieldType.esriFieldTypeString;
                                FieldEdit.Length_2 = item.sFieldLength;
                                break;
                            case VariableManager.FieldType.DateField:
                                FieldEdit.Type_2 = esriFieldType.esriFieldTypeDate;
                                break;
                            default:
                                break;
                        }
                        fieldsEdit.AddField(Field);
                    }                    
                }


                // Create the geometry field
                IGeometryDef geometryDef = new GeometryDefClass();
                IGeometryDefEdit geometryDefEdit = (IGeometryDefEdit)geometryDef;

                //Depending on the feature type (ie. Point, Polyline, or Polygon)
                //set the appropriate .Type_2 and .GeometryType_2 Properties.
                if (shapeType == VariableManager.ShapeType.ShapePoint)
                {
                    geometryDefEdit.GeometryType_2 = esriGeometryType.esriGeometryPoint;
                }
                else if (shapeType == VariableManager.ShapeType.ShapePolyline)
                {
                    geometryDefEdit.GeometryType_2 = esriGeometryType.esriGeometryPolyline;
                }
                else if (shapeType == VariableManager.ShapeType.ShapePolygon)
                {
                    geometryDefEdit.GeometryType_2 = esriGeometryType.esriGeometryPolygon;
                }

                // Assign Geometry Definition
                geometryDefEdit.GridCount_2 = 1;
                geometryDefEdit.set_GridSize(0, 0.5);
                geometryDefEdit.AvgNumPoints_2 = 2;
                geometryDefEdit.HasM_2 = false;
                geometryDefEdit.HasZ_2 = false;

                //Generate a default Spatial Reference
                ISpatialReference spatialReference = MakeSpatialReference("PCS_ITRF2000_TM.prj");
                geometryDefEdit.SpatialReference_2 = spatialReference;

                //Create the SHAPE field.
                IField shapeField = new FieldClass();
                IFieldEdit shapeFieldEdit = (IFieldEdit)shapeField;
                shapeFieldEdit.Name_2 = "Shape";
                shapeFieldEdit.AliasName_2 = "SHAPE";
                shapeFieldEdit.IsNullable_2 = true;
                shapeFieldEdit.Required_2 = true;
                shapeFieldEdit.Type_2 = esriFieldType.esriFieldTypeGeometry;
                shapeFieldEdit.GeometryDef_2 = geometryDef;

                fieldsEdit.AddField(shapeFieldEdit);
            }
            catch (Exception Exec)
            {
                MessageBox.Show(Exec.Message + " : " + Exec.InnerException + " : " + Exec.Source + " : " + Exec.StackTrace);
            }

            return fields;

        }
        #endregion

        #region GetValue, SetValue

        public static object GetValue(IRow pRow, string pFieldName)
        {
            int fieldIndex = pRow.Fields.FindField(pFieldName);
            if (fieldIndex == -1) return null;

            return GetValue(pRow, fieldIndex);

        }

        public static object GetValue(IRow pRow, int pIndex)
        {
            if (pIndex == -1) return null;

            object o = new object();
            if (Convert.IsDBNull(pRow.get_Value(pIndex)))
            {
                return null;
            }
            else  o = pRow.get_Value(pIndex);
            return o;
        }

        public static object GetValue(IFeature pFeature, string pFieldName)
        {
            int fieldIndex = pFeature.Fields.FindField(pFieldName);
            if (fieldIndex == -1) return null;

            return GetValue(pFeature, fieldIndex);

        }

        public static object GetValue(IFeature pFeature, int pIndex)
        {
            if (pIndex == -1) return null;

            object o = new object();
            if (Convert.IsDBNull(pFeature.get_Value(pIndex)))
            {
                return null;
            }
            else o = pFeature.get_Value(pIndex);
            return o;
        }

        public static object GetValue(IFeature pFeature)
        {
            if (!pFeature.HasOID) return null;

            object o = new object();
            if (Convert.IsDBNull(pFeature.OID))
            {
                return null;
            }
            else o = pFeature.OID;
            return o;
        }

        public static void SetValue(IRow pRow, string pFieldName, object pValue)
        {
            int fieldIndex = pRow.Fields.FindField(pFieldName);
            if (fieldIndex == -1) return;

            SetValue(pRow, fieldIndex, pValue);
        }

        public static void SetValue(IRow pRow, int pIndex, object pValue)
        {
            if (pIndex == -1) return;

            pRow.set_Value(pIndex, pValue);
        }

        public static void SetValue(IFeature pFeature, string pFieldName, object pValue)
        {
            int fieldIndex = pFeature.Fields.FindField(pFieldName);
            if (fieldIndex == -1) return;

            SetValue(pFeature, fieldIndex, pValue);
        }

        public static void SetValue(IFeature pFeature, int pIndex, object pValue)
        {
            if (pIndex == -1) return;

            pFeature.set_Value(pIndex, pValue);
        }
        #endregion

        #region SpatialReference
        public static ISpatialReference MakeSpatialReference(esriSRProjCSType coordinateSystem)
        {
            ISpatialReferenceFactory spatialReferenceFactory = new SpatialReferenceEnvironmentClass();

            //Create a projected coordinate system and define its domain, resolution, and x,y tolerance.
            ISpatialReferenceResolution spatialReferenceResolution = spatialReferenceFactory.CreateProjectedCoordinateSystem(System.Convert.ToInt32(coordinateSystem)) as ISpatialReferenceResolution;

            spatialReferenceResolution.ConstructFromHorizon();
            spatialReferenceResolution.SetDefaultXYResolution();
            spatialReferenceResolution.SetDefaultZResolution();

            ISpatialReferenceTolerance spatialReferenceTolerance = spatialReferenceResolution as ISpatialReferenceTolerance;
            spatialReferenceTolerance.SetDefaultXYTolerance();
            spatialReferenceTolerance.SetDefaultZTolerance();

            ISpatialReference spatialReference = spatialReferenceResolution as ISpatialReference;

            return spatialReference;

        }

        public static ISpatialReference MakeSpatialReference(string sfilename)
        {
            string sfile = Application.StartupPath + "\\" + sfilename;

            ISpatialReferenceFactory pSpatialReferenceFactory =new SpatialReferenceEnvironmentClass();
            ISpatialReference pSpatialReference = pSpatialReferenceFactory.CreateESRISpatialReferenceFromPRJFile(sfile);
        
            return pSpatialReference;
        }

        public static ESRI.ArcGIS.Geometry.ISpatialReference GetWGS84SpatialRef()
        {
            // Define the spatial reference to be used for geopolylines,
            ESRI.ArcGIS.Geometry.ISpatialReference spatialReference;
            ESRI.ArcGIS.Geometry.ISpatialReferenceFactory2 spatialReferenceFactory = (ISpatialReferenceFactory2)new ESRI.ArcGIS.Geometry.SpatialReferenceEnvironment();
            spatialReference = (ISpatialReference)spatialReferenceFactory.CreateSpatialReference((int)ESRI.ArcGIS.Geometry.esriSRGeoCSType.esriSRGeoCS_WGS1984);
            return spatialReference;
        }

        public static ESRI.ArcGIS.Geometry.ISpatialReference GetUnknownSpatialReference()
        {
            return new UnknownCoordinateSystemClass();
        }
        #endregion

        #region SetScale
        public static void SetLayer2Scale(ILayer pLayer, int min, int max)
        {
            if (pLayer == null) return;
            pLayer.MinimumScale = (double)min;
            pLayer.MaximumScale = (double)max;
        }
        #endregion

        #region ISelectionSet
        public static ISelectionSet getSelectedFeatures(ILayer pLayer)
        {
            if (!(pLayer is IFeatureLayer)) return null;

            return getSelectedFeatures((IFeatureLayer)pLayer);
        }

        public static ISelectionSet getSelectedFeatures(IFeatureLayer pFeatureLayer)
        {
            IFeatureSelection pFeatureSelection = pFeatureLayer as IFeatureSelection;
            if (pFeatureSelection == null) return null;

            ISelectionSet pSelectionSet = pFeatureSelection.SelectionSet;
            return pSelectionSet;
            
        }
        #endregion

        #region Composite Renderer
        public static void SetClassBreakRenderer(IFeatureLayer pFL, string sField, VariableManager.ClassBreakRender[] pClassRend, string sLegendLabel, double minValue)
        {
            if (!(pFL is IGeoFeatureLayer)) return;

            //sRendererField필드 체크
            if (((IDisplayTable)pFL).DisplayTable.FindField(sField) == -1)
            {
                MessageBox.Show(pFL.Name.ToString() + " 레이어의 " + sField + " 필드가 없어 더이상 진행할 수 없습니다", "레이어설정", MessageBoxButtons.OK);
                return;
            }

            //IGeoFeatureLayer geoFeatureLayer = pFL as IGeoFeatureLayer;
            //IDisplayTable displaytable = geoFeatureLayer as IDisplayTable;
            //ITable table = displaytable.DisplayTable;

            //using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            //{
            //    ICursor pCursor = table.Search(null, true);
            //    comReleaser.ManageLifetime(pCursor);

            //    IDataStatistics dataStatistics = new DataStatisticsClass();
            //    dataStatistics.Field = sField;
            //    dataStatistics.Cursor = pCursor as ICursor;

            //}

            IClassBreaksRenderer pRender = null;
            switch (pFL.FeatureClass.ShapeType)
            {
                case esriGeometryType.esriGeometryPoint:
                    pRender = MakeClassBreakRender_Point(sField, pClassRend, minValue);
                    break;
                case esriGeometryType.esriGeometryPolyline:
                case esriGeometryType.esriGeometryLine:
                    pRender = MakeClassBreakRender_Line(sField, pClassRend, minValue);
                    break;
                case esriGeometryType.esriGeometryPolygon:
                    pRender = MakeClassBreakRender_Polygon(sField, pClassRend, minValue);
                    break;
            }

            if (pRender == null) return;

            ILegendInfo pLegendInfo = (ILegendInfo)pRender;
            if (pLegendInfo.LegendGroupCount > 0)
            {
                ILegendGroup pLegendGroup = pLegendInfo.get_LegendGroup(0);
                pLegendGroup.Heading = sLegendLabel;
            }
            pRender.SortClassesAscending = true;
            ((IGeoFeatureLayer)pFL).Renderer = pRender as IFeatureRenderer;

        }

        public static IClassBreaksRenderer MakeClassBreakRender_Point(string sField, VariableManager.ClassBreakRender[] pClassRend, double minValue)
        {
            IClassBreaksRenderer pRender = new ClassBreaksRendererClass();
            IClassBreaksUIProperties pRenderUI = pRender as IClassBreaksUIProperties;

            INumericFormat pNumericFormat = new NumericFormatClass();
            pNumericFormat.RoundingOption = esriRoundingOptionEnum.esriRoundNumberOfDecimals;
            pNumericFormat.AlignmentWidth = 10;
            pNumericFormat.RoundingValue = 2;
            pNumericFormat.ZeroPad = false;

            #region Renderer Set
            pRender.Field = sField;
            pRender.BreakCount = pClassRend.Length;
            pRender.MinimumBreak = minValue;      //min value in dataset

            for (int i = 0; i < pClassRend.GetLength(0); i++)
            {
                pRender.set_Break(i, pClassRend[i].dNumber);
                if (i == 0) pRenderUI.set_LowBreak(i, (double)pRender.MinimumBreak);
                else pRenderUI.set_LowBreak(i, (double)pRender.get_Break(i - 1));

                pRender.set_Label(i, pClassRend[i].sLabel);


                //라인 색깔 세팅하기
                IRgbColor pColor = new RgbColorClass();
                ILineSymbol pLineSymbol = new SimpleLineSymbolClass();
                if (pClassRend[i].pLineColor == null)
                {
                    pColor = (IRgbColor)ArcManager.GetRandomColor;
                }
                else pColor = pClassRend[i].pLineColor;     //outline 색깔 지정
                pLineSymbol.Width = pClassRend[i].dLineWidth;
                pLineSymbol.Color = pColor;
                //pRender.set_Symbol(i, (ISymbol)pLineSymbol);

                //setup an algorithmic color ramp   
                ISimpleMarkerSymbol pSimpleSymbol = new SimpleMarkerSymbolClass();
                if (pClassRend[i].pFillColor == null)
                {
                    pColor = (IRgbColor)ArcManager.GetRandomColor;
                }
                else pColor = pClassRend[i].pFillColor;     //색깔 지정
                pSimpleSymbol.Color = pColor;
                pSimpleSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
                pSimpleSymbol.OutlineColor = pLineSymbol.Color;
                pSimpleSymbol.Outline = true;
                pSimpleSymbol.OutlineSize = 0.4;
                pSimpleSymbol.Size = pClassRend[i].dLineWidth;

                pRender.set_Symbol(i, (ISymbol)pSimpleSymbol);

            }
            #endregion
            return pRender;
        }

        public static IClassBreaksRenderer MakeClassBreakRender_Line(string sField, VariableManager.ClassBreakRender[] pClassRend, double minValue)
        {
            IClassBreaksRenderer pRender = new ClassBreaksRendererClass();
            IClassBreaksUIProperties pRenderUI = pRender as IClassBreaksUIProperties;

            INumericFormat pNumericFormat = new NumericFormatClass();
            pNumericFormat.RoundingOption = esriRoundingOptionEnum.esriRoundNumberOfDecimals;
            pNumericFormat.AlignmentWidth = 10;
            pNumericFormat.RoundingValue = 2;
            pNumericFormat.ZeroPad = false;

            #region Renderer Set
            pRender.Field = sField;
            pRender.BreakCount = pClassRend.Length;
            pRender.MinimumBreak = minValue;      //min value in dataset

            for (int i = 0; i < pClassRend.GetLength(0); i++)
            {
                pRender.set_Break(i, pClassRend[i].dNumber);
                if (i == 0) pRenderUI.set_LowBreak(i, (double)pRender.MinimumBreak);
                else pRenderUI.set_LowBreak(i, (double)pRender.get_Break(i - 1));

                pRender.set_Label(i, pClassRend[i].sLabel);

                //라인 색깔 세팅하기
                IRgbColor pColor = new RgbColorClass();
                ILineSymbol pLineSymbol = new SimpleLineSymbolClass();
                if (pClassRend[i].pLineColor == null)
                {
                    pColor = (IRgbColor)ArcManager.GetRandomColor;
                }
                else pColor = pClassRend[i].pLineColor;     //outline 색깔 지정
                pColor.Transparency = 100;
                pColor.UseWindowsDithering = true;
                pLineSymbol.Color = pColor;
                pLineSymbol.Width = pClassRend[i].dLineWidth;

                pRender.set_Symbol(i, (ISymbol)pLineSymbol);
            }
            #endregion
            return pRender;
        }

        /// <summary>
        /// ClassBreak Renderer
        /// </summary>
        /// <param name="sField"></param>
        /// <param name="pClassRend"></param>
        /// <returns></returns>
        public static IClassBreaksRenderer MakeClassBreakRender_Polygon(string sField, VariableManager.ClassBreakRender[] pClassRend, double minValue)
        {
            IClassBreaksRenderer pRender = new ClassBreaksRendererClass();
            IClassBreaksUIProperties pRenderUI = pRender as IClassBreaksUIProperties;

            INumericFormat pNumericFormat = new NumericFormatClass();
            pNumericFormat.RoundingOption = esriRoundingOptionEnum.esriRoundNumberOfDecimals;
            pNumericFormat.AlignmentWidth = 10;
            pNumericFormat.RoundingValue = 2;
            pNumericFormat.ZeroPad = false;

           #region Renderer Set
            pRender.Field = sField;
            pRender.BreakCount = pClassRend.Length;
            pRender.MinimumBreak = minValue;      //min value in dataset

            for (int i = 0; i < pClassRend.GetLength(0); i++)
            {
                pRender.set_Break(i, pClassRend[i].dNumber);
                if (i == 0) pRenderUI.set_LowBreak(i, (double)pRender.MinimumBreak);
                else pRenderUI.set_LowBreak(i, (double)pRender.get_Break(i - 1));

                pRender.set_Label(i, pClassRend[i].sLabel);

                //라인 색깔 세팅하기
                IRgbColor pColor = new RgbColorClass();
                ILineSymbol pLineSymbol = new SimpleLineSymbolClass();
                if (pClassRend[i].pLineColor == null)
                {
                    pColor = (IRgbColor)ArcManager.GetRandomColor; 
                }
                else pColor = pClassRend[i].pLineColor;     //outline 색깔 지정
                pLineSymbol.Width = pClassRend[i].dLineWidth;
                pLineSymbol.Color = pColor;
                pRender.set_Symbol(i, (ISymbol)pLineSymbol);

                //setup an algorithmic color ramp            
                ISimpleFillSymbol pSimpleFillSymbol = new SimpleFillSymbolClass();
                pSimpleFillSymbol.Outline = pLineSymbol;

                if (pClassRend[i].pFillColor == null)
                {
                    pColor = (IRgbColor)ArcManager.GetRandomColor;
                }
                else pColor = pClassRend[i].pFillColor;     //색깔 지정

                pSimpleFillSymbol.Color = (IColor)pColor;
                pRender.set_Symbol(i, (ISymbol)pSimpleFillSymbol);
            }
           #endregion

            return pRender;
        }

        /// <summary>
        /// void - CreateUniqueValueRenderer
        /// </summary>
        /// <param name="pAV"></param>
        /// <param name="featureClass"></param>
        /// <param name="fieldName"></param>
        public static void SetUniqueValueRenderer(IFeatureLayer pFL, string sField)
        {
            if (!(pFL is IGeoFeatureLayer)) return;

            //sRendererField필드 체크
            if (pFL.FeatureClass.FindField(sField) == -1)
            {
                MessageBox.Show(pFL.Name.ToString() + " 레이어의 " + sField + " 필드가 없어 더이상 진행할 수 없습니다", "레이어설정", MessageBoxButtons.OK);
                return;
            }

            esriGeometryType pGeomType = ((IGeoFeatureLayer)pFL).FeatureClass.ShapeType;

            //** Markersymbol Settings
            ISimpleMarkerSymbol pPointSymbol = (ISimpleMarkerSymbol)MakeSimpleMarkerSymbol(10);
            //** Polyline Symbol Settings
            ISimpleLineSymbol pLineSymbol = (ISimpleLineSymbol)MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 0.01);
            //** Polygon Symbol Settings
            ISimpleFillSymbol pPolySymbol = (ISimpleFillSymbol)MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, pLineSymbol);
     
     
            //** Make the renderer and the classify objects
            IUniqueValueRenderer pRender = new UniqueValueRendererClass();
            pRender.FieldCount = 1;
            pRender.set_Field(0, sField);

            switch (pGeomType)
            {
                case esriGeometryType.esriGeometryPoint:
                    pRender.DefaultSymbol = pPointSymbol as ISymbol;
                    break;
                case esriGeometryType.esriGeometryPolyline:
                    pRender.DefaultSymbol = pLineSymbol as ISymbol;
                    break;
                case esriGeometryType.esriGeometryPolygon:
                    pRender.DefaultSymbol = pPolySymbol as ISymbol;
                    break;
                default:
                    break;
            }
            pRender.UseDefaultSymbol = true;

            IFeatureClass pFeatureClass = pFL.FeatureClass;
            //IEnumVariantSimple는 c#에서는 에러 => System.Collectins.IEnumerator로 변경
            //IEnumVariantSimple pEnumVar = pDataStat.UniqueValues as IEnumVariantSimple;

            System.Collections.IEnumerator pEnumVar = GetSortedUniqueValues((ITable)pFeatureClass, sField, null);
            pEnumVar.Reset();
           
            string sValue;
            while (pEnumVar.MoveNext())
            {
                object sCodeValue = pEnumVar.Current;
                sValue = Convert.ToString(sCodeValue);
                switch (pGeomType)
                {
                    case esriGeometryType.esriGeometryPoint:
                        pRender.AddValue(sValue, sField, pPointSymbol as ISymbol);
                        pRender.set_Symbol(sValue, pPointSymbol as ISymbol);
                        break;
                    case esriGeometryType.esriGeometryPolyline:
                        pRender.AddValue(sValue, sField, pLineSymbol as ISymbol);
                        pRender.set_Symbol(sValue, pLineSymbol as ISymbol);
                        break;
                    case esriGeometryType.esriGeometryPolygon:
                        pRender.AddValue(sValue, sField, pPolySymbol as ISymbol);
                        pRender.set_Symbol(sValue, pPolySymbol as ISymbol);
                        break;
                    default:
                        break;
                }
                pRender.set_Label(sValue, sValue);
            }
            if (pRender.ValueCount == 0 ) 
            {
                ((IGeoFeatureLayer)pFL).Renderer = pRender as IFeatureRenderer;
                return;
            }
            //** Markersymbol Settings
            ISimpleMarkerSymbol pMarkerSym = (ISimpleMarkerSymbol)MakeSimpleMarkerSymbol(10);
            //** Polyline Symbol Settings
            ISimpleLineSymbol pLineSym = (ISimpleLineSymbol)MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 0.01);
            //** Polygon Symbol Settings
            ISimpleFillSymbol pFillSym =(ISimpleFillSymbol)MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, pLineSymbol);

            string xv = string.Empty;

            IRandomColorRamp rx = MakeRandomColorRamp(pRender.ValueCount, pRender.ValueCount * 10);
            IEnumColors enumColors = rx.Colors;
            for (int ny = 0; ny < pRender.ValueCount; ny++)
            {
                xv = pRender.get_Value(ny);
                switch (pGeomType)
                {
                    case esriGeometryType.esriGeometryPoint:
                        pMarkerSym = new SimpleMarkerSymbolClass();

                        pMarkerSym.Color = enumColors.Next();
                        pMarkerSym.Size = 10;
                        pMarkerSym.Style = esriSimpleMarkerStyle.esriSMSDiamond;
                        pRender.set_Symbol(xv, (ISymbol)pMarkerSym);
                        break;
                    case esriGeometryType.esriGeometryPolyline:
                        pLineSym = new SimpleLineSymbolClass();
                        pLineSym.Color = enumColors.Next();
                        pRender.set_Symbol(xv, (ISymbol)pLineSym);
                        break;
                    case esriGeometryType.esriGeometryPolygon:
                        pFillSym = new SimpleFillSymbolClass();
                        pFillSym.Color = enumColors.Next();
                        pFillSym.Outline = pLineSym as ILineSymbol;
                        pRender.set_Symbol(xv, (ISymbol)pFillSym);
                        break;
                    default:
                        break;
                }
            }

            ((IGeoFeatureLayer)pFL).Renderer = pRender as IFeatureRenderer;

        }

        /// <summary>
        /// Marker을 원하는 형태로 Fix해서 Set (작성 : 오두석)
        /// </summary>
        /// <param name="pAV"></param>
        /// <param name="featureClass"></param>
        /// <param name="fieldName"></param>
        public static void SetSimpleMarkerValueRenderer(IFeatureLayer pFL, string sField, double dblSize, esriSimpleMarkerStyle eStyle, IColor pColor)
        {
            if (!(pFL is IGeoFeatureLayer)) return;

            //sRendererField필드 체크
            if (pFL.FeatureClass.FindField(sField) == -1)
            {
                MessageBox.Show(pFL.Name.ToString() + " 레이어의 " + sField + " 필드가 없어 더이상 진행할 수 없습니다", "레이어설정", MessageBoxButtons.OK);
                return;
            }

            //** Markersymbol Settings
            ISimpleMarkerSymbol pPointSymbol = (ISimpleMarkerSymbol)MakeSimpleMarkerSymbol((int)dblSize, eStyle, pColor);

            //** Make the renderer and the classify objects
            IUniqueValueRenderer pRender = new UniqueValueRendererClass();
            pRender.FieldCount = 1;
            pRender.set_Field(0, sField);
            pRender.DefaultSymbol = pPointSymbol as ISymbol;

            pRender.UseDefaultSymbol = true;

            ((IGeoFeatureLayer)pFL).Renderer = pRender as IFeatureRenderer;

        }

        /// <summary>
        /// Line을 원하는 형태로 Fix해서 Set (작성 : 오두석)
        /// </summary>
        /// <param name="pAV"></param>
        /// <param name="featureClass"></param>
        /// <param name="fieldName"></param>
        public static void SetSimpleLineValueRenderer(IFeatureLayer pFL, string sField, double dblSize, esriSimpleLineStyle eStyle, IColor pColor)
        {
            if (!(pFL is IGeoFeatureLayer)) return;

            //sRendererField필드 체크
            if (pFL.FeatureClass.FindField(sField) == -1)
            {
                MessageBox.Show(pFL.Name.ToString() + " 레이어의 " + sField + " 필드가 없어 더이상 진행할 수 없습니다", "레이어설정", MessageBoxButtons.OK);
                return;
            }

            //** Polyline Symbol Settings
            ISimpleLineSymbol pLineSymbol = (ISimpleLineSymbol)MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, dblSize, pColor);

            //** Make the renderer and the classify objects
            IUniqueValueRenderer pRender = new UniqueValueRendererClass();
            pRender.FieldCount = 1;
            pRender.set_Field(0, sField);
            pRender.DefaultSymbol = pLineSymbol as ISymbol;

            pRender.UseDefaultSymbol = true;

            ((IGeoFeatureLayer)pFL).Renderer = pRender as IFeatureRenderer;
        }

        public static void SetSimpleRenderer(IFeatureLayer pFL)
        {
            if (!(pFL is IGeoFeatureLayer)) return;

            esriGeometryType pGeomType = ((IGeoFeatureLayer)pFL).FeatureClass.ShapeType;

            //** Markersymbol Settings
            ISimpleMarkerSymbol pPointSymbol = (ISimpleMarkerSymbol)MakeSimpleMarkerSymbol(12);
            //** Polyline Symbol Settings
            ISimpleLineSymbol pLineSymbol = (ISimpleLineSymbol)MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 0.01);
            //** Polygon Symbol Settings
            ISimpleFillSymbol pPolySymbol = (ISimpleFillSymbol)MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, pLineSymbol);

            ISimpleRenderer pRender = new SimpleRendererClass();

            switch (pGeomType)
            {
                case esriGeometryType.esriGeometryPoint:
                    pRender.Symbol = pPointSymbol as ISymbol;
                    break;
                case esriGeometryType.esriGeometryPolyline:
                    pRender.Symbol = pLineSymbol as ISymbol;
                    break;
                case esriGeometryType.esriGeometryPolygon:
                    pRender.Symbol = pPolySymbol as ISymbol;
                    break;
                default:
                    break;
            }

            ((IGeoFeatureLayer)pFL).Renderer = pRender as IFeatureRenderer;
        }

        public static void SetSimpleRenderer(IFeatureLayer pFL, ISymbol pSymbol)
        {
            if (!(pFL is IGeoFeatureLayer)) return;

            esriGeometryType pGeomType = ((IGeoFeatureLayer)pFL).FeatureClass.ShapeType;

            if (pSymbol == null)
            {
                SetSimpleRenderer(pFL);
                return;
            }
            
            ISimpleRenderer pRender = new SimpleRendererClass();

            pRender.Symbol = pSymbol;

            ((IGeoFeatureLayer)pFL).Renderer = pRender as IFeatureRenderer;

        }

        //public static IDataStatistics GetSortedValues2(ITable pTable, string sFieldName, IQueryFilter pQueryFilter)
        //{
        //    int Fieldindex = pTable.FindField(sFieldName);
        //    if (Fieldindex == -1) return null;

        //    ITableSort pTableSort = new TableSortClass();
        //    pTableSort.Fields = sFieldName;
        //    pTableSort.set_Ascending(sFieldName, true);
        //    pTableSort.QueryFilter = pQueryFilter;
        //    pTableSort.Table = pTable;
        //    pTableSort.Sort(null);

        //    //ICursor pCursor = pTableSort.Rows;

        //    IDataStatistics pDataStatistics = new DataStatisticsClass();
        //    pDataStatistics.Cursor = pTableSort.Rows as ICursor;
        //    pDataStatistics.Field = sFieldName;
        //    pDataStatistics.SampleRate = -1;
        //    pDataStatistics.SimpleStats = true;

        //    //ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);

        //    return pDataStatistics;
        //}

        public static System.Collections.IEnumerator GetSortedUniqueValues(ITable pTable, string sFieldName, IQueryFilter pQueryFilter)
        {
            int Fieldindex = pTable.FindField(sFieldName);
            if (Fieldindex == -1) return null;

            ITableSort pTableSort = new TableSortClass();
            pTableSort.Fields = sFieldName;
            pTableSort.set_Ascending(sFieldName, true);
            pTableSort.QueryFilter = pQueryFilter;
            pTableSort.Table = pTable;
            pTableSort.Sort(null);

            //ICursor pCursor = pTableSort.Rows;

            IDataStatistics pDataStatistics = new DataStatisticsClass();
            pDataStatistics.Cursor = pTableSort.Rows as ICursor;
            pDataStatistics.Field = sFieldName;
            pDataStatistics.SampleRate = -1;
            pDataStatistics.SimpleStats = true;

            return pDataStatistics.UniqueValues;

        }

        #endregion

        #region MakeCharacterSymbol
        public static ISymbol MakeCharacterMarkerSymbol(int dSize, int dIndex)
        {
            return MakeCharacterMarkerSymbol(dSize, dIndex, 0.0);
        }

        public static ISymbol MakeCharacterMarkerSymbol(int dSize, int dIndex, double dAngle)
        {
            IColor pColor = GetRandomColor;
            return MakeCharacterMarkerSymbol(dSize, dIndex, dAngle, pColor);
        }

        public static ISymbol MakeCharacterMarkerSymbol(int dSize, int dIndex, double dAngle, IColor pColor)
        {
            stdole.StdFont pfont = (stdole.StdFont)MakeStdFont();
            return MakeCharacterMarkerSymbol(dSize, dIndex, dAngle, pColor, pfont);
        }

        public static ISymbol MakeCharacterMarkerSymbol(int dSize, int dIndex, double dAngle, IColor pColor, stdole.StdFont pFont)
        {
            ICharacterMarkerSymbol pCharSym = new CharacterMarkerSymbolClass();
            if (pColor == null) pCharSym.Color = GetRandomColor;
            else pCharSym.Color = pColor;


            if (pFont == null) pCharSym.Font = (stdole.IFontDisp)MakeStdFont();
            else pCharSym.Font = (stdole.IFontDisp)pFont;

            pCharSym.Size = dSize;
            pCharSym.CharacterIndex = dIndex;
            pCharSym.Angle = dAngle;


            return (ISymbol)pCharSym;
        }
        #endregion

        #region makeSimpleMarker(Point)
        public static ISymbol MakeSimpleMarkerSymbol(int dSize)
        {
            ISimpleMarkerSymbol pSimpSym = new SimpleMarkerSymbolClass();
            pSimpSym.Size = dSize;
            pSimpSym.Style = esriSimpleMarkerStyle.esriSMSCircle;
            pSimpSym.Color = GetRandomColor;
            pSimpSym.Outline = true;
            pSimpSym.OutlineSize = 0.4;
            pSimpSym.OutlineColor = GetRandomColor;

            return (ISymbol)pSimpSym;
        }

        public static ISymbol MakeSimpleMarkerSymbol(int dSize, esriSimpleMarkerStyle eStyle, IColor pColor)
        {
            ISimpleMarkerSymbol pSimpSym = new SimpleMarkerSymbolClass();
            pSimpSym.Size = dSize;
            pSimpSym.Style = eStyle;

            if (pColor == null) pSimpSym.Color = GetRandomColor;
            else pSimpSym.Color = pColor;     
       
            pSimpSym.Outline = true;
            pSimpSym.OutlineSize = 0.4;
            pSimpSym.OutlineColor = GetRandomColor;

            return (ISymbol)pSimpSym;
        }

        public static ISymbol MakeSimpleMarkerSymbol(int dSize, esriSimpleMarkerStyle eStyle, IColor pColor, bool bOutline, int dOutSize)
        {
            ISimpleMarkerSymbol pSimpSym = new SimpleMarkerSymbolClass();
            pSimpSym.Size = dSize;
            pSimpSym.Style = eStyle;

            if (pColor == null) pSimpSym.Color = GetRandomColor;
            else pSimpSym.Color = pColor;

            pSimpSym.Outline = bOutline;
            pSimpSym.OutlineSize = dOutSize;
            pSimpSym.OutlineColor = GetRandomColor;

            return (ISymbol)pSimpSym;
        }

        public static ISymbol MakeSimpleMarkerSymbol(int dSize, esriSimpleMarkerStyle eStyle, IColor pColor, bool bOutline, int dOutSize, IColor pOutColor)
        {
            ISimpleMarkerSymbol pSimpSym = new SimpleMarkerSymbolClass();
            pSimpSym.Size = dSize;
            pSimpSym.Style = eStyle;

            if (pColor == null) pSimpSym.Color = GetRandomColor;
            else pSimpSym.Color = pColor;

            pSimpSym.Outline = bOutline;
            pSimpSym.OutlineSize = dOutSize;

            if (pOutColor == null) pSimpSym.Color = GetRandomColor;
            else pSimpSym.OutlineColor = pOutColor;

            return (ISymbol)pSimpSym;
        }

        public static IMarkerSymbol MakeArrowSymbol(Color color, double length, double width, double angle)
        {
            IArrowMarkerSymbol sym = new ArrowMarkerSymbolClass();
            sym.Color = EAGL.Display.ColorManager.GetESRIColor(color);
            sym.Length = length;
            sym.Style = esriArrowMarkerStyle.esriAMSPlain;
            sym.Width = width;
            sym.Angle = angle;
            sym.XOffset = 0;
            sym.YOffset = 0;
            return sym;
        }

        #endregion

        #region MakeSimpleFill(Polygon)
        public static ISymbol MakeSimpleFillSymbol(ESRI.ArcGIS.Display.esriSimpleFillStyle eStyle, ILineSymbol pOutLineSym, int dColor )
        {   
            IColor pColor = new RgbColorClass();
            ISimpleFillSymbol pSimpSym = new SimpleFillSymbolClass();

            if (eStyle == esriSimpleFillStyle.esriSFSHollow) pSimpSym.Color.NullColor = true;
            else           pColor.RGB = dColor;

            pSimpSym.Color = pColor;
            pSimpSym.Style = eStyle;
            pSimpSym.Outline = pOutLineSym;

            return pSimpSym as ISymbol;
        }


        public static ISymbol MakeSimpleFillSymbol(ESRI.ArcGIS.Display.esriSimpleFillStyle eStyle, ILineSymbol pOutLineSym, IColor pColor)
        {
            ISimpleFillSymbol pSimpSym = new SimpleFillSymbolClass();

            if (eStyle == esriSimpleFillStyle.esriSFSHollow) pSimpSym.Color.NullColor = true;

            if (pColor == null) pSimpSym.Color = GetRandomColor;
            else pSimpSym.Color = pColor;

            pSimpSym.Style = eStyle;
            pSimpSym.Outline = pOutLineSym;

            return pSimpSym as ISymbol;
        }

        public static ISymbol MakeSimpleFillSymbol(ESRI.ArcGIS.Display.esriSimpleFillStyle eStyle, ILineSymbol pOutLineSym)
        {
            ISimpleFillSymbol pSimpSym = new SimpleFillSymbolClass();

            if (eStyle == esriSimpleFillStyle.esriSFSHollow) pSimpSym.Color.NullColor = true;

            pSimpSym.Color = GetRandomColor;
            pSimpSym.Style = eStyle;
            pSimpSym.Outline = pOutLineSym;

            return pSimpSym as ISymbol;
        }

        public static ISymbol MakeSimpleLineSymbol(ESRI.ArcGIS.Display.esriSimpleLineStyle eStyle, double dWidth, IColor pColor)
        {

            ISimpleLineSymbol pSimpSym = new SimpleLineSymbolClass();

            if (pColor == null) pSimpSym.Color = GetRandomColor;
            else pSimpSym.Color = pColor;

            pSimpSym.Color = pColor;
            pSimpSym.Style = eStyle;
            pSimpSym.Width = dWidth;

            return pSimpSym as ISymbol;
        }

        public static ISymbol MakeSimpleLineSymbol(ESRI.ArcGIS.Display.esriSimpleLineStyle eStyle, double dWidth, int dColor)
        {
            IColor pColor = new RgbColorClass();
            pColor.RGB = dColor;

            ISimpleLineSymbol pSimpSym = new SimpleLineSymbolClass();

            pSimpSym.Color = pColor;
            pSimpSym.Style = eStyle;
            pSimpSym.Width = dWidth;

            return pSimpSym as ISymbol;
        }

        public static ISymbol MakeSimpleLineSymbol(ESRI.ArcGIS.Display.esriSimpleLineStyle eStyle, double dWidth)
        {   
            IColor pColor = new RgbColorClass();
            pColor = GetRandomColor;

            ISimpleLineSymbol pSimpSym = new SimpleLineSymbolClass();

            pSimpSym.Color = pColor;
            pSimpSym.Style = eStyle;
            pSimpSym.Width = dWidth;

            return pSimpSym as ISymbol;
        }
        #endregion

        #region ISymbol - 레이어 심볼 정보 가져오기
        public static ISymbol getSymbol4Layer(ILayer pLayer)
        {
            if (pLayer == null) return null;
            if (!(pLayer is IFeatureLayer)) return null;
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return null;

            IFeatureRenderer pFeatureRenderer = geoFeatureLayer.Renderer;
            IFeatureClass pFeatureClass = geoFeatureLayer.FeatureClass;
            IQueryFilter pQueryFilter = new QueryFilterClass();
            
            
            pFeatureRenderer.PrepareFilter(pFeatureClass, pQueryFilter);
            IFeatureCursor pFeatureCursor = pFeatureClass.Search(pQueryFilter, true);
            IFeature pFeature = pFeatureCursor.NextFeature();
            //pFeatureRenderer.CanRender(pFeatureClass,)
            //pFeatureRenderer.CanRender();
            //return pFeatureRenderer.SymbolByFeature(pFeature);

            return null;
        }
   
        #endregion

        #region StdFont
        public static stdole.IFontDisp MakeStdFont(string sFontName)
        {
            stdole.StdFont stdFont = new stdole.StdFontClass();
            try
            {
                stdFont.Name = sFontName;
            }
            catch (Exception Ex)
            {
                stdFont.Name = System.Drawing.SystemFonts.DefaultFont.Name;
                System.Diagnostics.Debug.WriteLine(Ex.Message);
            }

            return (stdole.IFontDisp)stdFont;
        }

        public static stdole.IFontDisp MakeStdFont()
        {
            return MakeStdFont("ESRI Default Marker");    
        }


        public static stdole.IFontDisp MakeStdFont(string Name, decimal Size, bool Bold, bool Italic, bool Strikethrough, bool Underline)
        {
            stdole.StdFont stdFont = new stdole.StdFontClass();

            stdFont.Size = Size;
            stdFont.Bold = Bold;
            stdFont.Italic = Italic;
            stdFont.Strikethrough = Strikethrough;
            stdFont.Underline = Underline;

            try
            {
                stdFont.Name = Name;
            }
            catch (Exception Ex)
            {
                stdFont.Name = System.Drawing.SystemFonts.DefaultFont.Name;
                System.Diagnostics.Debug.WriteLine(Ex.Message);
            }

            return (stdole.IFontDisp)stdFont;
        }

        #endregion

        #region ColorRamp
        public static IRandomColorRamp MakeRandomColorRamp()
        {
            return MakeRandomColorRamp(20, 43, 20, 40, 85, 100, 76, 188);
        }

        public static IRandomColorRamp MakeRandomColorRamp(int dSize, int dSeed)
        {
            return MakeRandomColorRamp(dSize, dSeed, 20, 40, 85, 100, 76, 188);
        }

        public static IRandomColorRamp MakeRandomColorRamp(int dSize, int dSeed, int dMinSaturation, int dMaxSaturation, int dMinVal, int dMaxVal, int dStartHue, int dEndHue)
        {
            IRandomColorRamp randomColorRamp = new RandomColorRampClass();
            randomColorRamp.MinSaturation = dMinSaturation;
            randomColorRamp.MaxSaturation = dMaxSaturation;
            randomColorRamp.MaxValue = dMinVal;
            randomColorRamp.MaxValue = dMaxVal;
            randomColorRamp.StartHue = dStartHue;
            randomColorRamp.EndHue = dEndHue;
            randomColorRamp.UseSeed = true;
            randomColorRamp.Seed = dSeed;
            randomColorRamp.Size = dSize;

            bool bOk = true;
            randomColorRamp.CreateRamp(out bOk);

            return randomColorRamp;
        }

        public static IAlgorithmicColorRamp MakeAlgorithmicColorRamp(int dSize, esriColorRampAlgorithm eAlgorithm, int fromRgb, int toRgb)
        {
            IAlgorithmicColorRamp nx = new AlgorithmicColorRampClass();
            IColor pfromColor = new RgbColorClass();
            IColor ptoColor = new RgbColorClass();

            pfromColor.RGB = fromRgb;
            ptoColor.RGB = toRgb;

            nx.ToColor = ptoColor;
            nx.FromColor = pfromColor;
            nx.Algorithm = eAlgorithm;
            nx.Size = dSize;
            bool bOk = true;
            nx.CreateRamp(out bOk);

            return nx;
        }

        public static IAlgorithmicColorRamp MakeAlgorithmicColorRamp(int dSize, esriColorRampAlgorithm eAlgorithm)
        {
            IAlgorithmicColorRamp nx = new AlgorithmicColorRampClass();
            IColor pfromColor = new RgbColorClass();
            IColor ptoColor = new RgbColorClass();

            pfromColor = GetRandomColor;
            ptoColor = GetRandomColor;

            nx.ToColor = ptoColor;
            nx.FromColor = pfromColor;
            nx.Algorithm = eAlgorithm;
            nx.Size = dSize;
            bool bOk = true;
            nx.CreateRamp(out bOk);

            return nx;
        }

        #endregion

        #region Map
        public static IDisplay GetMapDisplay(IMapControl3 map)
        {
            IDisplay pDisp = null;
            pDisp = (IDisplay)map.ActiveView.ScreenDisplay;

            return pDisp;
        }

        public static IActiveView GetMapActiveView(IMapControl3 map)
        {
            IActiveView pAV = null;
            pAV = (IActiveView)map.ActiveView;

            return pAV;
        }

        public static ILayer GetMapLayer(IMap map, string sName)
        {
            IEnumLayer pEnumLayer;
            pEnumLayer = map.get_Layers(null, true);
            pEnumLayer.Reset();

            ILayer pLayer = (ILayer)pEnumLayer.Next();

            do
            {
                if (pLayer is IFeatureLayer)
                {
                    IFeatureLayer pFeatureLayer = (IFeatureLayer)pLayer;
                    if (pFeatureLayer.Name == sName)
                    {
                        return pLayer;
                    }
                }
                pLayer = pEnumLayer.Next();
            }
            while (pLayer != null);

            return null;            
        }

        public static ILayer GetMapLayer(IMapControl3 map,string sName)
        {
            return GetMapLayer(map.Map, sName);
        }

        /// <summary>
        /// 지도화면에서 해당레이어를 삭제한다.
        /// </summary>
        /// <param name="map"></param>
        /// <param name="sName"></param>
        public static void RemoveMapLayer(IMapControl3 map, string sName)
        {
            IEnumLayer pEnumLayer;
            pEnumLayer = map.Map.get_Layers(null, true);
            pEnumLayer.Reset();

            ILayer pLayer = (ILayer)pEnumLayer.Next();

            do
            {
                if (pLayer is IFeatureLayer)
                {
                    IFeatureLayer pFeatureLayer = (IFeatureLayer)pLayer;
                    if (pFeatureLayer.Name == sName)
                    {
                        map.Map.DeleteLayer(pLayer);
                        return;
                    }
                }
                pLayer = pEnumLayer.Next();
            }
            while (pLayer != null);
        }

        /// <summary>
        /// MapScale 변경
        /// </summary>
        /// <param name="map"></param>
        /// <param name="scale"></param>
        public static void SetMapScale(IMapControl3 map, double scale)
        {
            if (scale < (double)1.0) return;

            map.ActiveView.FocusMap.MapScale = scale;

            //PartialRefresh(map.ActiveView.FocusMap, esriViewDrawPhase.esriViewNone);
        }

        public static double GetMapScale(IMapControl3 map)
        {
            return map.ActiveView.FocusMap.MapScale;
        }

        /// <summary>
        /// 중심점 이동
        /// </summary>
        /// <param name="map"></param>
        /// <param name="ipTargetGeom"></param>
        public static void MoveCenterAt(IMapControl3 map, IGeometry ipTargetGeom)
        {
            if (ipTargetGeom == null || ipTargetGeom.IsEmpty) return;

            IPoint ipCenPoint = new PointClass();
            ipCenPoint.X = (ipTargetGeom.Envelope.XMin + ipTargetGeom.Envelope.XMax) / 2.0;
            ipCenPoint.Y = (ipTargetGeom.Envelope.YMin + ipTargetGeom.Envelope.YMax) / 2.0;

            map.CenterAt(ipCenPoint);
            map.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground | esriViewDrawPhase.esriViewBackground, null, ((IActiveView)map.ActiveView).Extent.Envelope);
            Application.DoEvents();
        }

        /// <summary>
        /// 중심점 이동
        /// </summary>
        /// <param name="map"></param>
        /// <param name="ipTargetGeom"></param>
        public static void MoveCenterAt(IMapControl3 map, IFeature ipTargetGeom)
        {
            if (ipTargetGeom == null) return;

            IGeometry pGeomery = (IGeometry)ipTargetGeom.Shape;


            MoveCenterAt(map, pGeomery);

        }

        public static void MoveCenterAt(IActiveView activeview, IGeometry ipTargetGeom)
        {
            if (ipTargetGeom == null || ipTargetGeom.IsEmpty) return;

            IPoint ipCenPoint = new PointClass();
            switch (ipTargetGeom.GeometryType)
            {
                case esriGeometryType.esriGeometryAny:
                    break;
                case esriGeometryType.esriGeometryBag:
                    break;
                case esriGeometryType.esriGeometryBezier3Curve:
                    break;
                case esriGeometryType.esriGeometryCircularArc:
                    break;
                case esriGeometryType.esriGeometryEllipticArc:
                    break;
                case esriGeometryType.esriGeometryEnvelope:
                    break;
                case esriGeometryType.esriGeometryMultiPatch:
                    break;
                case esriGeometryType.esriGeometryMultipoint:
                    break;
                case esriGeometryType.esriGeometryNull:
                    break;
                case esriGeometryType.esriGeometryPath:
                    break;
                case esriGeometryType.esriGeometryPoint:
                    ipCenPoint = (IPoint)ipTargetGeom;
                    break;
                case esriGeometryType.esriGeometryPolygon:
                case esriGeometryType.esriGeometryPolyline:
                case esriGeometryType.esriGeometryLine:
                    ipCenPoint.X = (ipTargetGeom.Envelope.XMin + ipTargetGeom.Envelope.XMax) / 2.0;
                    ipCenPoint.Y = (ipTargetGeom.Envelope.YMin + ipTargetGeom.Envelope.YMax) / 2.0;
                    break;
                case esriGeometryType.esriGeometryRay:
                    break;
                case esriGeometryType.esriGeometryRing:
                    break;
                case esriGeometryType.esriGeometrySphere:
                    break;
                case esriGeometryType.esriGeometryTriangleFan:
                    break;
                case esriGeometryType.esriGeometryTriangleStrip:
                    break;
                case esriGeometryType.esriGeometryTriangles:
                    break;
                default:
                    break;
            }

            IEnvelope extentcopy = activeview.Extent.Envelope;
            extentcopy.CenterAt(ipCenPoint);

            activeview.Extent = extentcopy;
            activeview.PartialRefresh(esriViewDrawPhase.esriViewGraphics | esriViewDrawPhase.esriViewBackground, null, activeview.Extent);

            Application.DoEvents();
        }

        public static void ClearSelection(IMap pMap)
        {
            pMap.ClearSelection();
            PartialRefresh(pMap, esriViewDrawPhase.esriViewGeography | esriViewDrawPhase.esriViewGeoSelection, null, null);
        }

        public static void ClearSelection(IMapControl3 map)
        {
            IMap pMap = map.ActiveView.FocusMap;

            ClearSelection(pMap);
        }


        #endregion

        #region GeoSpatial HashTable

        /// <summary>
        /// 공간 Query한 결과값을 Hashtable으로 반환한다. (KeyField 이용해서 Hashtable에 Key값으로 이용.)
        /// </summary>
        /// <param name="pFeatureClass"></param>
        /// <param name="KeyField">관리번호</param>
        /// <param name="pGeo">Geometry</param>
        /// <param name="eSpatialRel"></param>
        /// <returns></returns>
        public static ArrayList GetSpatialFilterResultToFeatureCollection(IFeatureClass pFeatureClass, IGeometry pGeo, esriSpatialRelEnum eSpatialRel)
        {
            string strWhere = "";
            //밸브중에서 제수밸브(SA200)만 해당
            if (pFeatureClass.AliasName == "밸브") strWhere = "FTR_CDE = 'SA200'";
            
            return GetSpatialFilterResultToFeatureCollection(pFeatureClass, pGeo, eSpatialRel, strWhere);
        }

        public static ArrayList GetSpatialFilterResultToFeatureCollection(IFeatureClass pFeatureClass, IGeometry pGeo, esriSpatialRelEnum eSpatialRel, string strWhere)
        {
            ArrayList pResults = new ArrayList();
            if (pFeatureClass == null) return pResults;
            string strKeyValue = string.Empty;

            ISpatialFilter pSpatialFilter = new SpatialFilter();
            {
                pSpatialFilter.Geometry = pGeo;
                pSpatialFilter.GeometryField = pFeatureClass.ShapeFieldName;
                pSpatialFilter.SpatialRel = eSpatialRel;
                pSpatialFilter.WhereClause = strWhere;
            }

            IFeatureCursor pFCursor = pFeatureClass.Search(pSpatialFilter, false);
            if (pFCursor == null) return pResults;

            try
            {
                IFeature pFeature = pFCursor.NextFeature();
                while (pFeature != null)
                {
                    pResults.Add(pFeature);
                    pFeature = pFCursor.NextFeature();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("GetSpatialFilterResultToFeatureCollection : " + ex.Message);
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pFCursor);
            }
            return pResults;
        }

        public static Hashtable GetSpatialFilterResultToFeatureHashtable(IFeatureClass pFeatureClass, IGeometry pGeo, esriSpatialRelEnum eSpatialRel)
        {
            return GetSpatialFilterResultToFeatureHashtable(pFeatureClass, string.Empty, pGeo, eSpatialRel);
        }

        /// <summary>
        /// 공간 Query한 결과값을 Hashtable으로 반환한다. (KeyField 이용해서 Hashtable에 Key값으로 이용.)
        /// </summary>
        /// <param name="pFeatureClass"></param>
        /// <param name="KeyField">관리번호</param>
        /// <param name="pGeo">Geometry</param>
        /// <param name="eSpatialRel"></param>
        /// <returns></returns>
        public static Hashtable GetSpatialFilterResultToFeatureHashtable(IFeatureClass pFeatureClass, string KeyField, IGeometry pGeo, esriSpatialRelEnum eSpatialRel)
        {
            string strWhere = "";
            return GetSpatialFilterResultToFeatureHashtable(pFeatureClass, KeyField, pGeo, eSpatialRel, strWhere);
        }

        public static Hashtable GetSpatialFilterResultToFeatureHashtable(IFeatureClass pFeatureClass, string KeyField, IGeometry pGeo, esriSpatialRelEnum eSpatialRel, string strWhere)
        {
            Hashtable pResults = new Hashtable();
            if (pFeatureClass == null) return pResults;
            string strKeyValue = string.Empty;

            ISpatialFilter pSpatialFilter = new SpatialFilter();
            {
                pSpatialFilter.Geometry = pGeo;
                pSpatialFilter.GeometryField = pFeatureClass.ShapeFieldName;
                pSpatialFilter.SpatialRel = eSpatialRel;
                pSpatialFilter.WhereClause = strWhere;
            }

            IFeatureCursor pFCursor = pFeatureClass.Search(pSpatialFilter, false);
            if (pFCursor == null) return pResults;

            try
            {
                IFeature pFeature = pFCursor.NextFeature();
                while (pFeature != null)
                {
                    if (string.IsNullOrEmpty(KeyField))
                    {
                        pResults.Add(pResults.Count, pFeature);
                    }
                    else
                    {
                        strKeyValue = Convert.ToString(GetValue(pFeature, KeyField));
                        if (string.IsNullOrEmpty(strKeyValue)) continue;

                        pResults.Add(strKeyValue, pFeature);
                    }

                    pFeature = pFCursor.NextFeature();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pFCursor);
            }
            return pResults;
        }

    
       

        #endregion

        #region GetRGBColor - 색상(ESRI)

        public static IColor GetColor(System.Drawing.Color color)
        {
            IRgbColor ipRgbColor = new RgbColorClass();

            ipRgbColor.RGB = System.Drawing.ColorTranslator.ToWin32(color);

            if (color == System.Drawing.Color.Transparent) ipRgbColor.NullColor = true;

            ipRgbColor.UseWindowsDithering = true;

            return ipRgbColor;
        }

        public static IColor GetColor(int red, int green, int blue)
        {
            IRgbColor ipRgbColor = new RgbColorClass();

            ipRgbColor.Red = red;
            ipRgbColor.Green = green;
            ipRgbColor.Blue = blue;

            ipRgbColor.UseWindowsDithering = true;

            return ipRgbColor;
        }

        public static IColor GetColor(int red, int green, int blue, byte pTransparency)
        {
            IRgbColor ipRgbColor = new RgbColorClass();

            ipRgbColor.Red = red;
            ipRgbColor.Green = green;
            ipRgbColor.Blue = blue;
            ipRgbColor.Transparency = pTransparency;

            ipRgbColor.UseWindowsDithering = true;

            return ipRgbColor;
        }

        public static IColor GetColor(int RGB)
        {
            IRgbColor ipRgbColor = new RgbColorClass();

            //RGB = Long (ASCII) number calculated from the Red, Green and Blue color attributes. 
            ipRgbColor.RGB = RGB;
            ipRgbColor.UseWindowsDithering = true;

            return ipRgbColor;
        }

        public static IColor GetRandomColor
        {
            get
            {
                System.Random rnd = new System.Random();
                return GetColor(rnd.Next(255), rnd.Next(255), rnd.Next(255));
            }
        }

        public static IColor GetNullColor()
        {
            IRgbColor ipRgbColor = new RgbColorClass();
            ipRgbColor.NullColor = true;
            ipRgbColor.UseWindowsDithering = true;

            return ipRgbColor;
        }

        #endregion

        #region Serializer XML
        public static string Propset2XML(IPropertySet propset)
        {
            IXMLSerializer xmlserializer = new XMLSerializerClass();
            return xmlserializer.SaveToString(propset, null, null);
        }

        public static IPropertySet XML2Propset(string xml)
        {
            IXMLSerializer xmlserializer = new XMLSerializerClass();
            return xmlserializer.LoadFromString(xml, null, null)  as IPropertySet;
        }
        #endregion

        #region IPersistStream

        public static byte[] SerializeFeatureRenderer(IFeatureRenderer renderer)
        {
            IObjectStream ostrm = new ObjectStreamClass();
            IXMLStream stream = new XMLStreamClass();
            ostrm.Stream = stream as IStream;
            ostrm.SaveObject(renderer);
            byte[] b = stream.SaveToBytes();

            return b;
        }

        public static IFeatureRenderer DeSerializeFeatureRenderer(byte[] renderer)
        {
            IObjectStream ostrm = new ObjectStreamClass();
            IXMLStream stream = new XMLStreamClass();
            ostrm.Stream = stream as IStream;
            stream.LoadFromBytes(ref renderer);
            object o = new object();
            Guid guid = new Guid("{40A9E884-5533-11D0-98BE-00805F7CED21}");
            o = ostrm.LoadObject(ref guid, null);
            return o as IFeatureRenderer;
        }


        public static void SaveRenderer2XML(ILayer pLayer, string xmlPathFile)
        {
            if (pLayer == null) return;
            if (!(pLayer is IFeatureLayer)) return;
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            try
            {
                IXMLStream xmlStream = new XMLStreamClass();
                IXMLWriter xmlWriter = new XMLWriterClass();
                xmlWriter.WriteTo(xmlStream as IStream);
                IXMLSerializer xmlSerializer = new XMLSerializerClass();

                ESRI.ArcGIS.esriSystem.IPropertySet ipPropertySet = new ESRI.ArcGIS.esriSystem.PropertySetClass();
                ipPropertySet.SetProperty("Layer", pLayer);
                ipPropertySet.SetProperty("Render", geoFeatureLayer.Renderer);

                xmlSerializer.WriteObject(xmlWriter, ipPropertySet, null, "", "", geoFeatureLayer.Renderer);
                xmlStream.SaveToFile(xmlPathFile);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void SaveMapLayerToBlob(IMap ipMap, ITable ipTable)
        {
            if (ipMap.LayerCount == 0) return;

            IWorkspace ipWs = ((IDataset)ipTable).Workspace;
            IWorkspaceEdit ipWsEdit = (IWorkspaceEdit)ipWs;
            ipWsEdit.StartEditing(false);

            for (int k = 0; k < ipMap.LayerCount; k++)
            {
                ILayer ipLayer = ipMap.get_Layer(0);

                IRow ipRow = ipTable.CreateRow();
                uint streamSize = SaveStream(ipRow, "LYR_BLOB", ipLayer);

                ipRow.Store();
            }

            ipWsEdit.StopEditing(true);
        }


        public static uint SaveStream(ESRI.ArcGIS.Geodatabase.IRow pRow, string fieldName, ILayer streamValue)
        {
            int idxField = pRow.Fields.FindField(fieldName);
            if (idxField == -1) return 0;
            
            ESRI.ArcGIS.esriSystem.IMemoryBlobStream pMemBlobStream = new ESRI.ArcGIS.esriSystem.MemoryBlobStreamClass();
            ESRI.ArcGIS.esriSystem.IObjectStream ipObjectStream = new ESRI.ArcGIS.esriSystem.ObjectStreamClass();

            ipObjectStream.Stream = pMemBlobStream;

            ESRI.ArcGIS.esriSystem.IPropertySet ipPropertySet = new ESRI.ArcGIS.esriSystem.PropertySetClass();
            ipPropertySet.SetProperty("Layer", streamValue);

            ESRI.ArcGIS.esriSystem.IPersistStream ipPersistStream = (ESRI.ArcGIS.esriSystem.IPersistStream)ipPropertySet;
            ipPersistStream.Save(ipObjectStream, 0);

            pRow.set_Value(idxField, pMemBlobStream);

            return pMemBlobStream.Size;
        }




        /// <summary>
        /// 렌더러 XML -> ILayer 렌더러 적용
        /// </summary>
        /// <param name="pMap"></param>
        /// <param name="pLayer"></param>
        /// <param name="xmlPathFile"></param>
        public static void LoadRenderer2XML(IMapControl3 pMap, ILayer pLayer, string xmlPathFile)
        {
            if (pLayer == null) return;
            if (!(pLayer is IFeatureLayer)) return;
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            try
            {
                IXMLStream xmlStream = new XMLStreamClass();
                xmlStream.LoadFromFile(xmlPathFile);

                // Create xmlReader and read the XML stream
                ESRI.ArcGIS.esriSystem.IXMLReader xmlReader = new ESRI.ArcGIS.esriSystem.XMLReaderClass();
                xmlReader.ReadFrom((ESRI.ArcGIS.esriSystem.IStream)xmlStream); // Explicit Cast

                IXMLSerializer xmlSerializer = new XMLSerializerClass();
                object o = xmlSerializer.ReadObject(xmlReader, null, null);

                geoFeatureLayer.Renderer = (IFeatureRenderer)o;

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //Fire contents changed event that the TOCControl listens to
            pMap.ActiveView.ContentsChanged();
            //Refresh the display
            pMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, pMap.ActiveView.Extent);
        }

        public static ILayer LoadStream(ESRI.ArcGIS.Geodatabase.IRow ipRow, string fieldName)
        {
            int idxField = ipRow.Fields.FindField(fieldName);
            if (idxField == -1) return null;

            ESRI.ArcGIS.esriSystem.IMemoryBlobStream ipMemBlobStream = null;
            ipMemBlobStream = (ESRI.ArcGIS.esriSystem.IMemoryBlobStream)ipRow.get_Value(idxField);

            ESRI.ArcGIS.esriSystem.IObjectStream ipObjectStream = null;
            ipObjectStream = new ESRI.ArcGIS.esriSystem.ObjectStreamClass();

            ipObjectStream.Stream = ipMemBlobStream;

            ESRI.ArcGIS.esriSystem.IPropertySet ipPropertySet = null;
            ipPropertySet = new ESRI.ArcGIS.esriSystem.PropertySetClass();

            ESRI.ArcGIS.esriSystem.IPersistStream ipPersistStream = null;
            ipPersistStream = (ESRI.ArcGIS.esriSystem.IPersistStream)ipPropertySet;

            ILayer ipLayer = null;
            try
            {
                ipPersistStream.Load(ipObjectStream);

                ipLayer = (ILayer)ipPropertySet.GetProperty("Layer");
            }
            catch (Exception Ex)
            {
                System.Diagnostics.Debug.WriteLine(Ex.Message);
            }

            return ipLayer;
        }

        public static void LoadLayers(IMap ipMap, ITable ipTable, IQueryFilter ipQueryFilter)
        {
            ipMap.ClearLayers();

            ICursor ipCursor = ipTable.Search(ipQueryFilter, false);
            IRow ipRow = ipCursor.NextRow();

            while (ipRow != null)
            {
                ILayer ipLayer = LoadStream(ipRow, "LYR_BLOB");

                if (ipLayer != null)
                {
                    ipMap.AddLayer(ipLayer);
                }

                ipRow = ipCursor.NextRow();
            }
            ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(ipCursor);
        }
        #endregion

        #region 영역찾기
        public static void FlashExtent(ILayer pLayer, string OIDList)
        {
            if (pLayer == null) return;
            if (!(pLayer is IFeatureLayer)) return;

            IGeometry pSearchExtent = default(IGeometry);
            IFeatureLayer pFeatureLayer = pLayer as IFeatureLayer;
            pSearchExtent = GetSearchExtent(pFeatureLayer.FeatureClass, OIDList);
            if (pSearchExtent == null) return;
        }
        #endregion

        #region ESRICarto

        public static void PartialRefresh(IMap pMap, esriViewDrawPhase pDrawPhase, object pData, IEnvelope pEnvelope)
        {
            IActiveView pActiveView = pMap as IActiveView;
            if (pEnvelope != null)
            {
                pActiveView.PartialRefresh(pDrawPhase, pData, pEnvelope);
            }
            else
            {
                pActiveView.PartialRefresh(pDrawPhase, pData, pActiveView.Extent);
            }
        }

        public static void PartialRefresh(IMap pMap, esriViewDrawPhase pDrawPhase, object pData)
        {
            PartialRefresh(pMap, pDrawPhase, pData, null);
        }

        public static void PartialRefresh(IMap pMap, esriViewDrawPhase pDrawPhase)
        {
            PartialRefresh(pMap, pDrawPhase, null);
        }

        #region Flash Geometry
        ///<summary>Flash geometry on the display.</summary>
        ///<param name="geometry"> The input IGeometry to flash.  Supported geometry types are GeometryBag, Polygon, Polyline, Point and Multipoint.</param>
        ///<param name="screenDisplay">An IScreenDisplay reference</param>
        ///<param name="delay">An integer that is the time in milliseconds to wait.</param>
        public static void FlashGeometry(IActiveView activeView, IGeometry geometry, int delay)
        {
            if (geometry == null || geometry.IsEmpty)
            {
                return;
            }
            IScreenDisplay screenDisplay = activeView.ScreenDisplay;
            bool continueFlashing = true;

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                ITrackCancel cancelTracker = new CancelTrackerClass();
                comReleaser.ManageLifetime(cancelTracker);
                screenDisplay.CancelTracker = cancelTracker;
                short cacheID = screenDisplay.AddCache();
                int cacheMemDC = screenDisplay.get_CacheMemDC(cacheID);
                IRgbColor fillColor = new RgbColorClass();
                comReleaser.ManageLifetime(fillColor);
                fillColor.Green = 128;
                IRgbColor lineColor = new RgbColorClass();
                comReleaser.ManageLifetime(lineColor);
                screenDisplay.StartDrawing(cacheMemDC, cacheID);
                DrawGeometry(geometry, fillColor, lineColor, (IDisplay)screenDisplay, cancelTracker);
                ESRI.ArcGIS.esriSystem.tagRECT RECT = new tagRECT();
                screenDisplay.FinishDrawing();
                if (continueFlashing == true)
                {
                    screenDisplay.DrawCache(screenDisplay.hDC, cacheID, ref RECT, ref RECT);
                    if (delay > 0)
                    {
                        for (int i = 0; i < delay; i += 200)
                        {
                            if (continueFlashing = cancelTracker.Continue())
                            {
                                System.Threading.Thread.Sleep(200);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    screenDisplay.Invalidate(null, true, cacheID);
                    screenDisplay.UpdateWindow();
                }
                screenDisplay.RemoveCache(cacheID);
                cancelTracker.Reset();
            }
        }

        /// <summary>
        /// Draws the input geometry using the specified colors.
        /// </summary>
        /// <param name="geometry">The input IGeometry to draw. Supported geometry types are GeometryBag, Polygon, Polyline, Point and Multipoint.</param>
        /// <param name="fillColor">An IRgbColor reference for the fill color</param>
        /// <param name="lineColor">An IRgbColor reference for the line or outline color</param>
        /// <param name="display">An IDisplay reference</param>
        /// <param name="cancelTracker">An ITrackCancel reference</param>
        private static void DrawGeometry(IGeometry geometry, IRgbColor fillColor, IRgbColor lineColor, IDisplay display, ITrackCancel cancelTracker)
        {
            bool continueDrawing = true;
            switch (geometry.GeometryType)
            {
                case esriGeometryType.esriGeometryBag:
                    {
                        IEnumGeometry enumGeometry = (IEnumGeometry)geometry;
                        IGeometry innerGeometry = enumGeometry.Next();
                        while (innerGeometry != null && continueDrawing == true)
                        {
                            DrawGeometry(innerGeometry, fillColor, lineColor, display, cancelTracker); // Recursive method call
                            innerGeometry = enumGeometry.Next();
                            if (cancelTracker != null)
                            {
                                continueDrawing = cancelTracker.Continue();
                            }
                        }
                        break;
                    }
                case esriGeometryType.esriGeometryPolygon:
                    {
                        // Set the input polygon geometry's symbol.
                        ISimpleFillSymbol fillSymbol = new SimpleFillSymbolClass();
                        fillSymbol.Color = (IColor)fillColor;
                        fillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
                        ILineSymbol lineSymbol = new SimpleLineSymbolClass();
                        lineSymbol.Color = lineColor;
                        lineSymbol.Width = 10;
                        fillSymbol.Outline = lineSymbol;

                        // Draw the input polygon geometry.
                        display.SetSymbol((ISymbol)fillSymbol);
                        display.DrawPolygon(geometry);
                        break;
                    }
                case esriGeometryType.esriGeometryPolyline:
                    {
                        // Set the input polyline geometry's symbol.
                        IMultiLayerLineSymbol multiLineSymbol = new MultiLayerLineSymbolClass();
                        ISimpleLineSymbol simpleLineSymbol1 = new SimpleLineSymbolClass();
                        ISimpleLineSymbol simpleLineSymbol2 = new SimpleLineSymbolClass();
                        simpleLineSymbol1.Width = 5;
                        simpleLineSymbol1.Color = fillColor;
                        simpleLineSymbol2.Width = 8;
                        simpleLineSymbol2.Color = lineColor;
                        multiLineSymbol.AddLayer((ILineSymbol)simpleLineSymbol2);
                        multiLineSymbol.AddLayer((ILineSymbol)simpleLineSymbol1);

                        // Draw the input polyline geometry.
                        display.SetSymbol((ISymbol)multiLineSymbol);
                        display.DrawPolyline(geometry);
                        break;
                    }
                case esriGeometryType.esriGeometryPoint:
                    {
                        // Set the input point geometry's symbol.
                        ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
                        simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
                        simpleMarkerSymbol.Size = 20;
                        simpleMarkerSymbol.Color = fillColor;
                        simpleMarkerSymbol.Outline = true;
                        simpleMarkerSymbol.OutlineColor = lineColor;

                        // Draw the input point geometry.
                        display.SetSymbol((ISymbol)simpleMarkerSymbol);
                        display.DrawPoint(geometry);
                        break;
                    }
                case esriGeometryType.esriGeometryMultipoint:
                    {
                        // Set the input multipoint geometry's symbol.
                        ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
                        simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
                        simpleMarkerSymbol.Size = 20;
                        simpleMarkerSymbol.Color = fillColor;
                        simpleMarkerSymbol.Outline = true;
                        simpleMarkerSymbol.OutlineColor = lineColor;

                        // Draw the input multipoint geometry.
                        display.SetSymbol((ISymbol)simpleMarkerSymbol);
                        display.DrawMultipoint(geometry);
                        break;
                    }
            }
        }    

        public static void FlashShape(IActiveView pActiveView, IGeometry pGeometry, short Count)
        {
            FlashShape(pActiveView, pGeometry, Count, 100);
        }

        public static void FlashShape(IActiveView pActiveView, IGeometry pGeometry, short Count, int timegap)
        {
            pActiveView.ScreenDisplay.StartDrawing(0, (short)esriScreenCache.esriNoScreenCache);

            if (Count <= 1) Count = 1;

            for (short i = 1; i <= Count; i++)
            {
                switch (pGeometry.GeometryType)
                {
                    case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                        FlashPoint((pActiveView.ScreenDisplay), pGeometry, timegap);
                        break;
                    case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                        FlashLine((pActiveView.ScreenDisplay), pGeometry, timegap);
                        break;
                    case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                        FlashPolygon((pActiveView.ScreenDisplay), pGeometry, timegap);
                        break;
                }
            }
            pActiveView.ScreenDisplay.FinishDrawing();
        }

        private static void FlashPoint(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry)
        {
            FlashPoint(pDisplay, pGeometry, 100);
        }

        private static void FlashPoint(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry, int timegap)
        {
            FlashPointA(pDisplay, pGeometry, timegap);
            FlashPointA(pDisplay, pGeometry);
        }

        private static void FlashPointA(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry)
        {
            FlashPointA(pDisplay, pGeometry, 100);
        }

        private static void FlashPointA(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry, int timegap)
        {
            ESRI.ArcGIS.Display.ISimpleMarkerSymbol pMarkerSymbol = new ESRI.ArcGIS.Display.SimpleMarkerSymbol();
            pMarkerSymbol.Style = ESRI.ArcGIS.Display.esriSimpleMarkerStyle.esriSMSCircle;

            ESRI.ArcGIS.Display.ISymbol pSymbol = (ISymbol)pMarkerSymbol;
            pSymbol.ROP2 = ESRI.ArcGIS.Display.esriRasterOpCode.esriROPNotXOrPen;

            pDisplay.SetSymbol(pSymbol);
            System.Threading.Thread.Sleep(timegap);
            pDisplay.DrawPoint(pGeometry);
        }

        private static void FlashLine(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry)
        {
            FlashLine(pDisplay, pGeometry, 100);
        }

        private static void FlashLine(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry, int timegap)
        {
            FlashLineA(pDisplay, pGeometry, timegap);

            FlashLineA(pDisplay, pGeometry);
        }

        private static void FlashLineA(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry)
        {
            FlashLineA(pDisplay, pGeometry, 100);
        }

        private static void FlashLineA(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry, int timegap)
        {
            ESRI.ArcGIS.Display.ISimpleLineSymbol pLineSymbol = new ESRI.ArcGIS.Display.SimpleLineSymbol();
            pLineSymbol.Width = 10;

            ESRI.ArcGIS.Display.ISymbol pSymbol = pLineSymbol as ISymbol;
            pSymbol.ROP2 = ESRI.ArcGIS.Display.esriRasterOpCode.esriROPNotXOrPen;

            pDisplay.SetSymbol(pSymbol);
            System.Threading.Thread.Sleep(timegap);
            pDisplay.DrawPolyline(pGeometry);
        }

        private static void FlashPolygon(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry)
        {
            FlashPolygon(pDisplay, pGeometry, 100);
        }

        private static void FlashPolygon(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry, int timegap)
        {
            FlashPolygonA(pDisplay, pGeometry, timegap);
            FlashPolygonA(pDisplay, pGeometry);

        }

        private static void FlashPolygonA(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry)
        {
            FlashPolygonA(pDisplay, pGeometry, 100);
        }

        private static void FlashPolygonA(ESRI.ArcGIS.Display.IScreenDisplay pDisplay, ESRI.ArcGIS.Geometry.IGeometry pGeometry, int timegap)
        {
            ESRI.ArcGIS.Display.ISimpleFillSymbol pFillSymbol = new ESRI.ArcGIS.Display.SimpleFillSymbol();
            pFillSymbol.Outline = null;
            ESRI.ArcGIS.Display.ISymbol pSymbol = pFillSymbol as ISymbol;
            pSymbol.ROP2 = ESRI.ArcGIS.Display.esriRasterOpCode.esriROPNotXOrPen;

            pDisplay.SetSymbol(pSymbol);
            System.Threading.Thread.Sleep(timegap);
            pDisplay.DrawPolygon(pGeometry);
        }

        #endregion Flash Geometry

        public static IGeometry GetSearchExtent(IFeatureClass pFeatureClass, string OIDList)
        {
            IQueryFilter pQueryFilter = new QueryFilter();
            pQueryFilter.WhereClause = "OBJECTID in (" + OIDList + ")";

            IFeatureCursor pFeatureCursor = pFeatureClass.Search(pQueryFilter, false);
            IFeature pFeature = pFeatureCursor.NextFeature();

            IGeometry pSearchUnion = default(IGeometry);
            pSearchUnion = pFeature.Shape;

            ITopologicalOperator pTopoOp = (ITopologicalOperator)pSearchUnion;

            pFeature = pFeatureCursor.NextFeature();
            while (pFeature != null)
            {
                pSearchUnion = pTopoOp.Union(pFeature.Shape);
                pTopoOp = (ITopologicalOperator)pSearchUnion;

                pFeature = pFeatureCursor.NextFeature();
            }
            return pSearchUnion;
        }
        #endregion

        #region AOLicense
        /// <summary>
        /// ArcGis License 체크
        /// </summary>
        /// <param name="oCode"></param>
        /// <returns></returns>
        public static esriLicenseStatus CheckOutLicenses(esriLicenseProductCode oCode)
        {
            esriLicenseStatus licenseStatus = esriLicenseStatus.esriLicenseUnavailable;
            if (VariableManager.m_AoInitialize == null)
            {
                VariableManager.m_AoInitialize = new AoInitializeClass();
            }

            licenseStatus = VariableManager.m_AoInitialize.IsProductCodeAvailable(oCode);
            return licenseStatus;
        }
        #endregion

        //#End Region
        //--------------------------------------------------------------------------------------------------------------------------------------------
        //사용가능 - 세모
        //--------------------------------------------------------------------------------------------------------------------------------------------

        public static System.Collections.IEnumerator GetUniqueValues(ITable ipTable, IQueryFilter ipFilter, string fieldName, bool useSorting)
        {
            ICursor ipCursor = null;

            if (useSorting)
            {
                ipCursor = GetSortedCursor(ipTable, ipFilter, new string[] { fieldName }, true);
            }
            else
            {
                ipCursor = ipTable.Search(ipFilter, true);
            }

            IDataStatistics ipDataStat = new DataStatisticsClass();
            ipDataStat.Field = fieldName;
            ipDataStat.Cursor = ipCursor;

            System.Collections.IEnumerator ipEnum = ipDataStat.UniqueValues;

            System.Runtime.InteropServices.Marshal.ReleaseComObject(ipCursor);

            return ipEnum;
        }

        public static System.Collections.Generic.List<object> GetUniqueValuesList(ITable ipTable, IQueryFilter ipFilter, string fieldName, bool useSorting)
        {
            System.Collections.Generic.List<object> uvList = new System.Collections.Generic.List<object>();

            int idxField = ipTable.FindField(fieldName);
            if (idxField == -1) return uvList;

            ICursor ipCursor = ipTable.Search(ipFilter, true);
            IRow ipRow = ipCursor.NextRow();
            while (ipRow != null)
            {
                object curValue = ipRow.get_Value(idxField);

                if (!uvList.Contains(curValue))
                {
                    uvList.Add(curValue);
                }

                ipRow = ipCursor.NextRow();
            }

            if (useSorting)
            {
                uvList.Sort();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(ipCursor);

            return uvList;
        }


        //※ GUIDs

        //{6CA416B1-E160-11D2-9F4E-00C04F6BC78E} IDataLayer

        //{E156D7E5-22AF-11D3-9F99-00C04F6BC78E} IGeoFeatureLayer

        //{34B2EF81-F4AC-11D1-A245-080009B6F22B} IGraphicsLayer

        //{5CEAE408-4C0A-437F-9DB3-054D83919850} IFDOGraphicsLayer

        //{0C22A4C7-DAFD-11D2-9F46-00C04F6BC78E} ICoverageAnnotationLayer

        //{EDAD6644-1810-11D1-86AE-0000F8751720} IGroupLayer
        public ILayer GetLayerByName(IMap ipMap, string layerName)
        {
            if (ipMap.LayerCount == 0) return null;

            ESRI.ArcGIS.esriSystem.UID uid = new UIDClass();
            uid.Value = "{6CA416B1-E160-11D2-9F4E-00C04F6BC78E}";

            try
            {
                IEnumLayer ipEnumLayer = ipMap.get_Layers(uid, true);
                ipEnumLayer.Reset();

                ILayer ipCurLayer = ipEnumLayer.Next();
                while (ipCurLayer != null)
                {
                    if ((string.Compare(ipCurLayer.Name, layerName, true) == 0))
                    {
                        return ipCurLayer;
                    }
                    ipCurLayer = ipEnumLayer.Next();
                }
            }
            catch (Exception Ex)
            {
                System.Diagnostics.Debug.WriteLine(Ex.Message);
            }

            return null;
        }




        //--------------------------------------------------------------------------------------------------------------------------------------------
        //사용여부 - 검토필요
        //--------------------------------------------------------------------------------------------------------------------------------------------


        #region FeatureLayer
        public static IFeatureLayer SetFeatureLayer(IFeatureClass pFeatureClass, string sName)
        {
            if (pFeatureClass == null) return null;

            IFeatureLayer pFeatureLayer = new FeatureLayerClass();
            pFeatureLayer.FeatureClass = pFeatureClass;

            pFeatureLayer.Name = pFeatureClass.AliasName;

            return pFeatureLayer;
        }

        #endregion

        /// <summary>
        /// create the renderer used to draw the tracks
        /// </summary>
        /// <param name="featureClass"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static IUniqueValueRenderer CreateUniqueValueRenderer_LineSymbol(IFeatureClass featureClass, string fieldName)
        {
            IRgbColor color = new RgbColorClass();
            color.Red = 0;
            color.Blue = 0;
            color.Green = 255;


            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Color = (IColor)color;
            simpleLineSymbol.Style = esriSimpleLineStyle.esriSLSSolid;
            simpleLineSymbol.Width = 1.0;

            IUniqueValueRenderer uniqueRenderer = new UniqueValueRendererClass();
            uniqueRenderer.FieldCount = 1;
            uniqueRenderer.set_Field(0, fieldName);
            uniqueRenderer.DefaultSymbol = (ISymbol)simpleLineSymbol;
            uniqueRenderer.UseDefaultSymbol = true;

            Random rand = new Random();
            bool bValFound = false;
            IFeatureCursor featureCursor = featureClass.Search(null, true);
            IFeature feature = null;
            string val = string.Empty;
            int fieldID = featureClass.FindField(fieldName);
            if (-1 == fieldID)
                return uniqueRenderer;

            while ((feature = featureCursor.NextFeature()) != null)
            {
                bValFound = false;
                val = Convert.ToString(feature.get_Value(fieldID));
                for (int i = 0; i < uniqueRenderer.ValueCount - 1; i++)
                {
                    if (uniqueRenderer.get_Value(i) == val)
                        bValFound = true;
                }

                if (!bValFound)//need to add the value to the renderer
                {
                    color.Red = rand.Next(255);
                    color.Blue = rand.Next(255);
                    color.Green = rand.Next(255);

                    simpleLineSymbol = new SimpleLineSymbolClass();
                    simpleLineSymbol.Color = (IColor)color;
                    simpleLineSymbol.Style = esriSimpleLineStyle.esriSLSSolid;
                    simpleLineSymbol.Width = 1.0;

                    //add the value to the renderer
                    uniqueRenderer.AddValue(val, "name", (ISymbol)simpleLineSymbol);
                }
            }

            //release the featurecursor
            ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(featureCursor);

            return uniqueRenderer;

        }

        public static IUniqueValueRenderer CreateUniqueValueRenderer_FillSymbol(IFeatureClass featureClass, string fieldName)
        {

            ESRI.ArcGIS.Geodatabase.IQueryFilter pQF = new ESRI.ArcGIS.Geodatabase.QueryFilter();

            ESRI.ArcGIS.Geodatabase.IFeatureCursor pFCur;
            pFCur = featureClass.Search(pQF, false);

            ESRI.ArcGIS.Display.IRandomColorRamp pRCR = null;
            pRCR = new ESRI.ArcGIS.Display.RandomColorRampClass();
            pRCR.MinSaturation = 0;
            pRCR.MaxSaturation = 100;
            pRCR.MinValue = 0;
            pRCR.MaxValue = 100;
            pRCR.StartHue = 0;
            pRCR.EndHue = 360;
            pRCR.UseSeed = true;
            pRCR.Seed = 87;

            ESRI.ArcGIS.Carto.IUniqueValueRenderer pRender = new ESRI.ArcGIS.Carto.UniqueValueRenderer();

            pRender.FieldCount = 1;
            pRender.set_Field(0, fieldName);

            ESRI.ArcGIS.Display.ISimpleFillSymbol pSFS = new ESRI.ArcGIS.Display.SimpleFillSymbolClass();
            pSFS.Style = ESRI.ArcGIS.Display.esriSimpleFillStyle.esriSFSSolid;
            pSFS.Outline.Width = 0.4;
            pRender.DefaultSymbol = pSFS as ESRI.ArcGIS.Display.ISymbol;
            pRender.UseDefaultSymbol = true;

            ESRI.ArcGIS.Geodatabase.IFeature pFeat;
            long n = featureClass.FeatureCount(pQF);
            ESRI.ArcGIS.Geodatabase.IFields pFields;
            pFields = featureClass.Fields;
            int iField = pFields.FindField(fieldName);
            for (long i = 0; i < n; i++)
            {
                ESRI.ArcGIS.Display.ISimpleFillSymbol pUniqueSFS = null;
                pUniqueSFS = new ESRI.ArcGIS.Display.SimpleFillSymbolClass();
                pUniqueSFS.Style = ESRI.ArcGIS.Display.esriSimpleFillStyle.esriSFSSolid;
                pUniqueSFS.Outline.Width = 0.4;


                pFeat = pFCur.NextFeature();
                string value = pFeat.get_Value(iField) as String;
                bool valFound = false;
                for (int uh = 0; uh < pRender.ValueCount; uh++)
                {
                    if (pRender.get_Value(uh) == value)
                    {
                        valFound = true;
                        break;
                    }
                }

                if (!valFound)
                {
                    pRender.AddValue(value, "Name",
                        pUniqueSFS as ESRI.ArcGIS.Display.ISymbol);
                    pRender.set_Label(value, value);
                    pRender.set_Symbol(value, pUniqueSFS as ESRI.ArcGIS.Display.ISymbol);
                }
            }

            pRCR.Size = pRender.ValueCount;
            bool bOK = true;
            pRCR.CreateRamp(out bOK);
            ESRI.ArcGIS.Display.IEnumColors pEnumClrs;
            pEnumClrs = pRCR.Colors;
            pEnumClrs.Reset();
            for (int ny = 0; ny < pRender.ValueCount; ++ny)
            {
                string xv;
                xv = pRender.get_Value(ny);
                ESRI.ArcGIS.Display.ISimpleFillSymbol pMySFS;
                pMySFS = pRender.get_Symbol(xv) as ESRI.ArcGIS.Display.ISimpleFillSymbol;
                pMySFS.Color = pEnumClrs.Next();
            }

            pRender.ColorScheme = "Custom";
            pRender.set_FieldType(0, true);

            return pRender;


        }

        public static IUniqueValueRenderer CreateUniqueValueRenderer_MarkerSymbol(IFeatureClass featureClass, string fieldName)
        {
            IRgbColor color = new RgbColorClass();
            color.Red = 255;
            color.Blue = 0;
            color.Green = 0;

            ICharacterMarkerSymbol charMarkersymbol = new CharacterMarkerSymbolClass();
            charMarkersymbol.Font = ESRI.ArcGIS.ADF.Converter.ToStdFont(new Font(new FontFamily("ESRI Default Marker"), 12.0f, FontStyle.Regular));
            charMarkersymbol.CharacterIndex = 96;
            charMarkersymbol.Size = 12.0;
            charMarkersymbol.Color = (IColor)color;


            IRandomColorRamp randomColorRamp = new RandomColorRampClass();
            randomColorRamp.MinSaturation = 20;
            randomColorRamp.MaxSaturation = 40;
            randomColorRamp.MaxValue = 85;
            randomColorRamp.MaxValue = 100;
            randomColorRamp.StartHue = 75;
            randomColorRamp.EndHue = 190;
            randomColorRamp.UseSeed = true;
            randomColorRamp.Seed = 45;

            IUniqueValueRenderer uniqueRenderer = new UniqueValueRendererClass();
            uniqueRenderer.FieldCount = 1;
            uniqueRenderer.set_Field(0, fieldName);
            uniqueRenderer.DefaultSymbol = (ISymbol)charMarkersymbol;
            uniqueRenderer.UseDefaultSymbol = true;



            Random rand = new Random();
            bool bValFound = false;
            IFeatureCursor featureCursor = featureClass.Search(null, true);
            IFeature feature = null;
            string val = string.Empty;
            int fieldID = featureClass.FindField(fieldName);
            if (-1 == fieldID)
                return uniqueRenderer;

            while ((feature = featureCursor.NextFeature()) != null)
            {
                bValFound = false;
                val = Convert.ToString(feature.get_Value(fieldID));
                for (int i = 0; i < uniqueRenderer.ValueCount - 1; i++)
                {
                    if (uniqueRenderer.get_Value(i) == val)
                        bValFound = true;
                }

                if (!bValFound)//need to add the value to the renderer
                {
                    color.Red = rand.Next(255);
                    color.Blue = rand.Next(255);
                    color.Green = rand.Next(255);

                    charMarkersymbol = new CharacterMarkerSymbolClass();
                    charMarkersymbol.Font = ESRI.ArcGIS.ADF.Converter.ToStdFont(new Font(new FontFamily("ESRI Default Marker"), 10.0f, FontStyle.Regular));
                    charMarkersymbol.CharacterIndex = rand.Next(40, 118);
                    charMarkersymbol.Size = 20.0;
                    charMarkersymbol.Color = (IColor)color;

                    //add the value to the renderer
                    uniqueRenderer.AddValue(val, "name", (ISymbol)charMarkersymbol);
                }
            }

            //release the featurecursor
            ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(featureCursor);

            return uniqueRenderer;
        }

        private bool IsHighPrecisionWorkspace(IWorkspace Workspace)
        {
            bool isHighPrecision = false;
            if (Workspace is IWorkspaceProperties)
            {
                IWorkspaceProperties ipWsProps = (IWorkspaceProperties)Workspace;
                IWorkspaceProperty ipProp = ipWsProps.get_Property(
                esriWorkspacePropertyGroupType.esriWorkspacePropertyGroup,
                (int)esriWorkspacePropertyType.esriWorkspacePropSupportsHighPrecisionStorage);
                if (ipProp.IsSupported)
                {
                    isHighPrecision = Convert.ToBoolean(ipProp.PropertyValue);
                }
            }
            return isHighPrecision;
        }

        private static QuickGraph.Geometries.Rectangle get_Rectangle(IGeometry geometry)
        {
            IEnvelope envelope = geometry.Envelope;
            QuickGraph.Geometries.Rectangle rect = new QuickGraph.Geometries.Rectangle((float)envelope.XMin, (float)envelope.YMin, (float)envelope.XMax, (float)envelope.YMax, (float)0.0, (float)0.0);
            return rect;
        }

        public static void GetMapFullExtent(out double xmin, out double xmax, out double ymin, out double ymax)
        {
            xmin = 0.0; xmax = 0.0; ymin = 0.0; ymax = 0.0;

            ILayer layer = GetShapeLayer(VariableManager.m_Pipegraphic, "WTL_PIPE_LS");
            if (layer == null) return;

            QuickGraph.Geometries.Index.RTree<ESRI.ArcGIS.Geometry.IGeometry> rtree = new QuickGraph.Geometries.Index.RTree<ESRI.ArcGIS.Geometry.IGeometry>();

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureCursor FCursor = ((IFeatureLayer)layer).Search(null, true);
                comReleaser.ManageLifetime(FCursor);

                IFeature feature = null;
                while ((feature = FCursor.NextFeature()) != null)
                {
                    if (feature.Shape.IsEmpty) continue;
                    rtree.Add(get_Rectangle(feature.ShapeCopy), feature.ShapeCopy);
                }
            }

            QuickGraph.Geometries.Rectangle rect = rtree.getBounds();
            IEnvelope env = new EnvelopeClass();
            env.XMin = rect.XMin; env.XMax = rect.XMax; env.YMax = rect.YMax; env.YMin = rect.YMin;
            env.Expand(1.2, 1.2, true);

            xmin = env.XMin; xmax = env.XMax; ymin = env.YMin; ymax = env.YMax;
        }

        public static IEnvelope SetFullExtent()
        {
            IEnvelope env = new EnvelopeClass();
            env.XMin = EMFrame.statics.AppStatic.MIN_X;
            env.XMax = EMFrame.statics.AppStatic.MAX_X;
            env.YMin = EMFrame.statics.AppStatic.MIN_Y;
            env.YMax = EMFrame.statics.AppStatic.MAX_Y;

            return env;
        }

        public static void SetFullExtent(IMap map)
        {
            IEnvelope env = new EnvelopeClass();
            env.XMin = EMFrame.statics.AppStatic.MIN_X;
            env.XMax = EMFrame.statics.AppStatic.MAX_X;
            env.YMin = EMFrame.statics.AppStatic.MIN_Y;
            env.YMax = EMFrame.statics.AppStatic.MAX_Y;

            map.AreaOfInterest = env;
        }
    }

}
