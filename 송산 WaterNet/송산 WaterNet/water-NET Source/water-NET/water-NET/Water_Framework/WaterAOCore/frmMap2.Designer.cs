﻿namespace WaterNet.WaterAOCore
{
    partial class frmMap2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMap2));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnBlock = new System.Windows.Forms.Button();
            this.btnIndexMap = new System.Windows.Forms.Button();
            this.spcContents = new System.Windows.Forms.SplitContainer();
            this.spcIndexMap = new System.Windows.Forms.SplitContainer();
            this.axIndexMap = new ESRI.ArcGIS.Controls.AxMapControl();
            this.spcTOC = new System.Windows.Forms.SplitContainer();
            this.tvBlock = new System.Windows.Forms.TreeView();
            this.axTOC = new ESRI.ArcGIS.Controls.AxTOCControl();
            this.contextMenuStripTOC = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemScale = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSymbol = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLabel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemTransfy = new System.Windows.Forms.ToolStripMenuItem();
            this.axMap = new ESRI.ArcGIS.Controls.AxMapControl();
            this.axToolbar = new ESRI.ArcGIS.Controls.AxToolbarControl();
            this.serialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deSerialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            this.spcTOC.Panel1.SuspendLayout();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            this.contextMenuStripTOC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.btnBlock);
            this.panel1.Controls.Add(this.btnIndexMap);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(67, 548);
            this.panel1.TabIndex = 4;
            // 
            // btnBlock
            // 
            this.btnBlock.Location = new System.Drawing.Point(3, 62);
            this.btnBlock.Name = "btnBlock";
            this.btnBlock.Size = new System.Drawing.Size(62, 54);
            this.btnBlock.TabIndex = 1;
            this.btnBlock.Text = "블록";
            this.btnBlock.UseVisualStyleBackColor = true;
            this.btnBlock.Click += new System.EventHandler(this.btnBlock_Click);
            // 
            // btnIndexMap
            // 
            this.btnIndexMap.Location = new System.Drawing.Point(3, 4);
            this.btnIndexMap.Name = "btnIndexMap";
            this.btnIndexMap.Size = new System.Drawing.Size(62, 54);
            this.btnIndexMap.TabIndex = 0;
            this.btnIndexMap.Text = "인덱스맵";
            this.btnIndexMap.UseVisualStyleBackColor = true;
            this.btnIndexMap.Click += new System.EventHandler(this.btnIndexMap_Click);
            // 
            // spcContents
            // 
            this.spcContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcContents.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcContents.Location = new System.Drawing.Point(67, 0);
            this.spcContents.Name = "spcContents";
            // 
            // spcContents.Panel1
            // 
            this.spcContents.Panel1.Controls.Add(this.spcIndexMap);
            // 
            // spcContents.Panel2
            // 
            this.spcContents.Panel2.Controls.Add(this.axMap);
            this.spcContents.Panel2.Controls.Add(this.axToolbar);
            this.spcContents.Panel2.Padding = new System.Windows.Forms.Padding(1, 3, 3, 3);
            this.spcContents.Size = new System.Drawing.Size(842, 548);
            this.spcContents.SplitterDistance = 257;
            this.spcContents.SplitterWidth = 3;
            this.spcContents.TabIndex = 8;
            // 
            // spcIndexMap
            // 
            this.spcIndexMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcIndexMap.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcIndexMap.Location = new System.Drawing.Point(0, 0);
            this.spcIndexMap.Name = "spcIndexMap";
            this.spcIndexMap.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcIndexMap.Panel1
            // 
            this.spcIndexMap.Panel1.Controls.Add(this.axIndexMap);
            this.spcIndexMap.Panel1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 1);
            // 
            // spcIndexMap.Panel2
            // 
            this.spcIndexMap.Panel2.Controls.Add(this.spcTOC);
            this.spcIndexMap.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.spcIndexMap.Size = new System.Drawing.Size(257, 548);
            this.spcIndexMap.SplitterDistance = 173;
            this.spcIndexMap.SplitterWidth = 3;
            this.spcIndexMap.TabIndex = 0;
            // 
            // axIndexMap
            // 
            this.axIndexMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axIndexMap.Location = new System.Drawing.Point(4, 4);
            this.axIndexMap.Name = "axIndexMap";
            this.axIndexMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axIndexMap.OcxState")));
            this.axIndexMap.Size = new System.Drawing.Size(249, 168);
            this.axIndexMap.TabIndex = 6;
            this.axIndexMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(this.axIndexMap_OnMouseDown);
            this.axIndexMap.OnAfterDraw += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnAfterDrawEventHandler(this.axIndexMap_OnAfterDraw);
            // 
            // spcTOC
            // 
            this.spcTOC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcTOC.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcTOC.Location = new System.Drawing.Point(3, 3);
            this.spcTOC.Name = "spcTOC";
            this.spcTOC.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcTOC.Panel1
            // 
            this.spcTOC.Panel1.Controls.Add(this.tvBlock);
            // 
            // spcTOC.Panel2
            // 
            this.spcTOC.Panel2.Controls.Add(this.axTOC);
            this.spcTOC.Size = new System.Drawing.Size(251, 366);
            this.spcTOC.SplitterDistance = 114;
            this.spcTOC.TabIndex = 9;
            // 
            // tvBlock
            // 
            this.tvBlock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvBlock.Location = new System.Drawing.Point(0, 0);
            this.tvBlock.Name = "tvBlock";
            this.tvBlock.Size = new System.Drawing.Size(251, 114);
            this.tvBlock.TabIndex = 0;
            // 
            // axTOC
            // 
            this.axTOC.ContextMenuStrip = this.contextMenuStripTOC;
            this.axTOC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axTOC.Location = new System.Drawing.Point(0, 0);
            this.axTOC.Name = "axTOC";
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            this.axTOC.Padding = new System.Windows.Forms.Padding(4);
            this.axTOC.Size = new System.Drawing.Size(251, 248);
            this.axTOC.TabIndex = 9;
            this.axTOC.OnMouseDown += new ESRI.ArcGIS.Controls.ITOCControlEvents_Ax_OnMouseDownEventHandler(this.axTOC_OnMouseDown);
            // 
            // contextMenuStripTOC
            // 
            this.contextMenuStripTOC.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemScale,
            this.toolStripMenuItemSymbol,
            this.toolStripMenuItemLabel,
            this.toolStripMenuItemTransfy,
            this.serialToolStripMenuItem,
            this.deSerialToolStripMenuItem});
            this.contextMenuStripTOC.Name = "contextMenuStripTOC";
            this.contextMenuStripTOC.Size = new System.Drawing.Size(153, 158);
            // 
            // toolStripMenuItemScale
            // 
            this.toolStripMenuItemScale.Name = "toolStripMenuItemScale";
            this.toolStripMenuItemScale.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemScale.Text = "유효축척 설정";
            this.toolStripMenuItemScale.Click += new System.EventHandler(this.toolStripMenuItemScale_Click);
            // 
            // toolStripMenuItemSymbol
            // 
            this.toolStripMenuItemSymbol.Name = "toolStripMenuItemSymbol";
            this.toolStripMenuItemSymbol.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemSymbol.Text = "심볼 설정";
            this.toolStripMenuItemSymbol.Click += new System.EventHandler(this.toolStripMenuItemSymbol_Click);
            // 
            // toolStripMenuItemLabel
            // 
            this.toolStripMenuItemLabel.Name = "toolStripMenuItemLabel";
            this.toolStripMenuItemLabel.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemLabel.Text = "주석 설정";
            this.toolStripMenuItemLabel.Click += new System.EventHandler(this.toolStripMenuItemLabel_Click);
            // 
            // toolStripMenuItemTransfy
            // 
            this.toolStripMenuItemTransfy.Name = "toolStripMenuItemTransfy";
            this.toolStripMenuItemTransfy.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemTransfy.Text = "투명도 설정";
            // 
            // axMap
            // 
            this.axMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axMap.Location = new System.Drawing.Point(1, 31);
            this.axMap.Name = "axMap";
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Padding = new System.Windows.Forms.Padding(4);
            this.axMap.Size = new System.Drawing.Size(578, 514);
            this.axMap.TabIndex = 11;
            this.axMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(this.axMap_OnMouseDown);
            this.axMap.OnAfterDraw += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnAfterDrawEventHandler(this.axMap_OnAfterDraw);
            // 
            // axToolbar
            // 
            this.axToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.axToolbar.Location = new System.Drawing.Point(1, 3);
            this.axToolbar.Name = "axToolbar";
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            this.axToolbar.Size = new System.Drawing.Size(578, 28);
            this.axToolbar.TabIndex = 10;
            // 
            // serialToolStripMenuItem
            // 
            this.serialToolStripMenuItem.Name = "serialToolStripMenuItem";
            this.serialToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.serialToolStripMenuItem.Text = "Serial";
            // 
            // deSerialToolStripMenuItem
            // 
            this.deSerialToolStripMenuItem.Name = "deSerialToolStripMenuItem";
            this.deSerialToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deSerialToolStripMenuItem.Text = "DeSerial";
            // 
            // frmMap2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(909, 548);
            this.Controls.Add(this.spcContents);
            this.Controls.Add(this.panel1);
            this.Name = "frmMap2";
            this.Text = "frmMap";
            this.Load += new System.EventHandler(this.frmMap_Load);
            this.ResizeBegin += new System.EventHandler(this.frmMap_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.frmMap_ResizeEnd);
            this.panel1.ResumeLayout(false);
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.ResumeLayout(false);
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            this.spcTOC.Panel1.ResumeLayout(false);
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            this.contextMenuStripTOC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnIndexMap;
        private System.Windows.Forms.SplitContainer spcIndexMap;
        protected ESRI.ArcGIS.Controls.AxMapControl axMap;
        protected ESRI.ArcGIS.Controls.AxMapControl axIndexMap;
        protected ESRI.ArcGIS.Controls.AxToolbarControl axToolbar;
        protected System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer spcTOC;
        private System.Windows.Forms.TreeView tvBlock;
        protected ESRI.ArcGIS.Controls.AxTOCControl axTOC;
        private System.Windows.Forms.Button btnBlock;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemScale;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSymbol;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLabel;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemTransfy;
        protected System.Windows.Forms.SplitContainer spcContents;
        public System.Windows.Forms.ContextMenuStrip contextMenuStripTOC;
        protected System.Windows.Forms.ToolStripMenuItem serialToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem deSerialToolStripMenuItem;
    }
}