﻿namespace WaterNet.WaterAOCore
{
    partial class frmSetTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTrans = new System.Windows.Forms.Label();
            this.nudTrans = new System.Windows.Forms.NumericUpDown();
            this.lblPersent = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrans)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblPersent);
            this.panel1.Controls.Add(this.nudTrans);
            this.panel1.Controls.Add(this.lblTrans);
            this.panel1.Location = new System.Drawing.Point(18, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 66);
            this.panel1.TabIndex = 0;
            // 
            // lblTrans
            // 
            this.lblTrans.AutoSize = true;
            this.lblTrans.Location = new System.Drawing.Point(21, 26);
            this.lblTrans.Name = "lblTrans";
            this.lblTrans.Size = new System.Drawing.Size(41, 12);
            this.lblTrans.TabIndex = 19;
            this.lblTrans.Text = "투명도";
            // 
            // nudTrans
            // 
            this.nudTrans.Location = new System.Drawing.Point(82, 22);
            this.nudTrans.Name = "nudTrans";
            this.nudTrans.Size = new System.Drawing.Size(89, 21);
            this.nudTrans.TabIndex = 20;
            this.nudTrans.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPersent
            // 
            this.lblPersent.AutoSize = true;
            this.lblPersent.Location = new System.Drawing.Point(175, 27);
            this.lblPersent.Name = "lblPersent";
            this.lblPersent.Size = new System.Drawing.Size(15, 12);
            this.lblPersent.TabIndex = 21;
            this.lblPersent.Text = "%";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdCancel.Location = new System.Drawing.Point(147, 96);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(86, 30);
            this.cmdCancel.TabIndex = 7;
            this.cmdCancel.Text = "닫기";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdOK.Location = new System.Drawing.Point(60, 96);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(86, 30);
            this.cmdOK.TabIndex = 6;
            this.cmdOK.Text = "확인";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // frmSetTrans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(249, 137);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSetTrans";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "투명도 설정";
            this.Load += new System.EventHandler(this.frmSetTrans_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrans)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTrans;
        private System.Windows.Forms.Label lblPersent;
        private System.Windows.Forms.NumericUpDown nudTrans;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdOK;
    }
}