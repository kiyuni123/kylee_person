﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.ADF;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

using WaterNet.WaterNetCore;

namespace WaterNet.WaterAOCore
{
    public partial class frmMap : Form
    {
        //--------------------protected------------------------
        protected DataSet m_dsLayer = new DataSet();
        #region members
        private ESRI.ArcGIS.Carto.IActiveViewEvents_Event m_activeViewEvents;
        private readonly string[] DICLAYERNAME = {"밸브","유량계","수압계","소방시설","상수관로","급수관로","수원지","밸브실","소블록",
                                                  "중블록","대블록","수도계량기","취수장","가압장","정수장","배수지"};
        #endregion members
        //--------------------private------------------------
        private ILayer m_workLayer = null;
        #region axTOC의 Item을 선택했을때 Private 멤버
        private esriTOCControlItem item = new esriTOCControlItem();
        private ILayer layer = null;
        private IBasicMap map = new MapClass(); 
        private object legendGroup = null; private object index = null;
        #endregion

        public frmMap()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 동적 DLL로드
        /// </summary>
        /// <param name="dllName"></param>
        /// <param name="instanceName"></param>
        /// <returns></returns>
        private object LoadDll(string dllName, string instanceName)
        {
            object obj = null;
            if (System.IO.File.Exists(dllName))
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(dllName);
                if (assembly == null) return null;
                Type[] types = assembly.GetExportedTypes();
                foreach (var type in types)
                {
                    if (type.Name.Equals(instanceName))
                    {
                        obj = Activator.CreateInstance(type);
                        break;
                    }
                }
            }
            return obj;
        }

        /// <summary>
        /// 지도 및 툴바 초기설정
        /// </summary>
        protected virtual void InitializeSetting()
        {
            m_activeViewEvents = (IActiveViewEvents_Event)this.axMap.Map;
            m_activeViewEvents.ItemAdded += new IActiveViewEvents_ItemAddedEventHandler(OnActiveViewEventsItemAdded);
            m_activeViewEvents.ItemDeleted += new IActiveViewEvents_ItemDeletedEventHandler(OnActiveViewEventsItemDeleted);
            
            if (ArcManager.CheckOutLicenses(ESRI.ArcGIS.esriSystem.esriLicenseProductCode.esriLicenseProductCodeEngine) != ESRI.ArcGIS.esriSystem.esriLicenseStatus.esriLicenseAvailable)
            {
                WaterNetCore.MessageManager.ShowExclamationMessage("ArcEngine Runtime License가 없습니다.");
                return;
            }

            #region axToolbar 설정
            axToolbar.CustomProperty = this;

            //Add map navigation commands
            axToolbar.AddItem("esriControls.ControlsMapZoomInTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomOutTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapPanTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            object instance = this.LoadDll(@Application.StartupPath + "\\WaterNet.WaterFullExtent.dll", "CmdFullExtent");
            if (instance != null)
                axToolbar.AddItem(instance, -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            //axToolbar.AddItem("esriControls.ControlsMapFullExtentCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomToLastExtentBackCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomToLastExtentForwardCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomToolControl", 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            axToolbar.AddItem("esriControls.ControlsMapIdentifyTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            //axToolbar.AddItem("esriControls.ControlsMapFindCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapMeasureTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapRefreshViewCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            //axToolbar.AddItem("esriControls.ControlsLayerListToolControl", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            //axToolbar.AddItem("esriControls.ControlsSelectTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsSelectFeaturesTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);             
            //axToolbar.AddItem("esriControls.ControlsSelectAllCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsClearSelectionCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            this.axToolbar.SetBuddyControl(axMap);
            this.axTOC.SetBuddyControl(axMap);
            this.axMap.SpatialReference = ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");
            this.axMap.ActiveView.FocusMap.Name = EMFrame.statics.AppStatic.USER_SGCNM;
            this.axMap.ActiveView.FocusMap.UseSymbolLevels = false;
            this.axMap.ActiveView.FocusMap.DelayDrawing(false);
            this.axMap.ActiveView.FocusMap.ReferenceScale = 0;
            this.axMap.MapUnits = esriUnits.esriMeters;
            this.axMap.AutoMouseWheel = true;
            this.axMap.AutoKeyboardScrolling = true;
            ESRI.ArcGIS.Carto.IAnnotateMap annoMap = new AnnotateMapClass();
            this.axMap.ActiveView.FocusMap.AnnotationEngine = annoMap;

            toolStrip1.Items.Add(new ToolStripControlHost(axToolbar));
            #endregion axToolbar 설정

            pnlLayerFunc.Visible = false;  //수압값 보기 패널
            //this.tsSatelImage.Enabled = false; this.tsBasemap.Enabled = false;

            #region 건물찾기
            foreach (TabPage tab in this.tabTOC.TabPages)
            {
                if (tab.Name.Equals("tabPageBldg"))
                {
                    this.tabTOC.TabPages.Remove(tab);
                    continue;
                }
            }
            #endregion 건물찾기
            #region 주소찾기 - 읍면동 콤보박스 설정
            //StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.AppendLine("SELECT DISTINCT DONG_CODE CD, DONG_NAME CDNM");
            //oStringBuilder.AppendLine("  FROM CM_DONGCODE");
            //oStringBuilder.AppendLine(" WHERE GUBUN = '1' AND SI_CODE = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            //oStringBuilder.AppendLine(" ORDER BY DONG_CODE ASC                 ");

            //FormManager.SetComboBoxEX(cboDong, oStringBuilder.ToString(), false);
            foreach (TabPage tab in this.tabTOC.TabPages)
            {
                if (tab.Name.Equals("tabPageAddr"))
                {
                    this.tabTOC.TabPages.Remove(tab);
                    continue;
                }
            }            
            #endregion 주소찾기

            #region 수용가찾기
            txtNo1.Text = EMFrame.statics.AppStatic.USER_SGCCD;
            #endregion 수용가찾기
        }

        private void OnActiveViewEventsItemAdded(object Item)
        {
            // Add your code here
            if (Item is ESRI.ArcGIS.Carto.IFeatureLayer)
            {
                string lname = ((IFeatureLayer)Item).Name;
                if (this.DICLAYERNAME.Contains(lname))
                {
                    if (!VariableManager.LAYER_DIC.ContainsKey(lname))
                    {
                        EAGL.Data.Wrapper.ITableWrapper tablewrapper = new EAGL.Data.Wrapper.ITableWrapper(((IFeatureLayer)Item) as ITable);
                        VariableManager.LAYER_DIC.Add(lname, tablewrapper);
                    }
                }
            }
        }

        // Event handler
        private void OnActiveViewEventsItemDeleted(object Item)
        {
            if (Item is ESRI.ArcGIS.Carto.IFeatureLayer)
            {
                string lname = ((IFeatureLayer)Item).Name;

                if (VariableManager.LAYER_DIC.ContainsKey(lname))
                {
                    VariableManager.LAYER_DIC.Remove(lname);
                }
            }
        }

        public virtual void Open()
        {
            Initialize_LayerInfo();
            if (m_dsLayer.Tables["SI_LAYER"] == null)
            {
                MessageBox.Show("레이어 환경 정보가 생성되지 않았습니다. \n\r관리자에게 문의하십시오", "경고", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Indexmap_Load();
            Viewmap_Load();
            InitializeBlock();
        }

        #region FeatureLayer PropertySet

        private object setSimpleValueRenderer(ILayer layer, string fieldname)
        {
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return null;

            EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
            vrm.Fields = new string[] { fieldname };

            switch (geoFeatureLayer.FeatureClass.ShapeType)
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    EAGL.Display.Rendering.SimpleAreaRenderer sar = new EAGL.Display.Rendering.SimpleAreaRenderer();
                    sar.FillColor = EAGL.Display.ColorManager.GetRandomColor();
                    sar.FillStyle = esriSimpleFillStyle.esriSFSSolid;
                    sar.OutlineColor = EAGL.Display.ColorManager.GetRandomColor();
                    sar.OutlineWidth = 0.4;

                    geoFeatureLayer.Renderer = sar.EsriRenderer;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    EAGL.Display.Rendering.SimplePointRenderer spr = new EAGL.Display.Rendering.SimplePointRenderer();
                    spr.Color = EAGL.Display.ColorManager.GetRandomColor();
                    spr.Style = esriSimpleMarkerStyle.esriSMSCircle;
                    spr.Size = 10;
                    geoFeatureLayer.Renderer = spr.EsriRenderer;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                    EAGL.Display.Rendering.SimpleLineRenderer slr = new EAGL.Display.Rendering.SimpleLineRenderer();
                    slr.Color = EAGL.Display.ColorManager.GetRandomColor();
                    slr.Style = esriSimpleLineStyle.esriSLSSolid;
                    slr.Width = 1;
                    geoFeatureLayer.Renderer = slr.EsriRenderer;
                    break;
            }

            return geoFeatureLayer.Renderer;
        }

        private object setUniqueValueRenderer(ILayer layer, string fieldname)
        {
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return null;

            EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
            vrm.Fields = new string[] { fieldname };

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IDisplayTable displaytable = geoFeatureLayer as IDisplayTable;
                ITable table = displaytable.DisplayTable;
                ICursor pCursor = table.Search(null, false);
                comReleaser.ManageLifetime(pCursor);

                IDataStatistics dataStatistics = new DataStatisticsClass();
                dataStatistics.Field = fieldname;
                dataStatistics.Cursor = pCursor as ICursor;
                System.Collections.IEnumerator enumerator = dataStatistics.UniqueValues;

                IColorRamp colorRamp = null;
                if ((new string[] {"대블록","중블록","소블록" }).Contains(layer.Name))
	            {
                    IColor fromcolor =  EAGL.Display.ColorManager.GetESRIColorFromRGB(216,225,215);
                    IColor tocolor = EAGL.Display.ColorManager.GetESRIColorFromRGB(80,109,78);
            	    colorRamp = EAGL.Display.ColorManager.CreateAlgorithmicColorRamp(fromcolor, tocolor);
                    colorRamp.Size = dataStatistics.UniqueValueCount * 2;
                    bool bOK = false;
                    colorRamp.CreateRamp(out bOK);
	            }

                List<ISymbol> Symbols = new List<ISymbol>();
                for (int i = 0; i < dataStatistics.UniqueValueCount; i++)
                {
                    switch (geoFeatureLayer.FeatureClass.ShapeType)
                    {
                        case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                            EAGL.Display.Rendering.SimpleAreaRenderer sar = new EAGL.Display.Rendering.SimpleAreaRenderer();

                            sar.FillColor = colorRamp != null ? System.Drawing.ColorTranslator.FromWin32(colorRamp.get_Color(i).RGB) : EAGL.Display.ColorManager.GetRandomColor();
                            sar.FillStyle = esriSimpleFillStyle.esriSFSSolid;
                            sar.OutlineColor = colorRamp != null ? System.Drawing.ColorTranslator.FromWin32(colorRamp.get_Color(colorRamp.Size - 1).RGB) : EAGL.Display.ColorManager.GetRandomColor();
                            sar.OutlineWidth = 0.4;
                            Symbols.Add(sar.Symbol);
                            break;
                        case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                            EAGL.Display.Rendering.SimplePointRenderer spr = new EAGL.Display.Rendering.SimplePointRenderer();
                            spr.Color = EAGL.Display.ColorManager.GetRandomColor();
                            spr.Style = esriSimpleMarkerStyle.esriSMSCircle;
                            spr.Size = 10;
                            Symbols.Add(spr.Symbol);
                            break;
                        case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                            EAGL.Display.Rendering.SimpleLineRenderer slr = new EAGL.Display.Rendering.SimpleLineRenderer();
                            slr.Color = EAGL.Display.ColorManager.GetRandomColor();
                            slr.Style = esriSimpleLineStyle.esriSLSSolid;
                            slr.Width = 1;
                            Symbols.Add(slr.Symbol);
                            break;
                    }
                }

                enumerator.Reset();
                int j = 0;
                while (enumerator.MoveNext())
                {
                    object value = enumerator.Current;
                    vrm.Add(value, Symbols[j] as ISymbol, value.ToString());
                    j++;
                }
            }
            ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;

            return geoFeatureLayer.Renderer;
        }

        private object setAnnoProperties(ILayer layer, string fieldname)
        {
            return setAnnoProperties(layer, fieldname, 0, 0);
        }

        private object setAnnoProperties(ILayer layer, string fieldname, double minScale, double maxScale)
        {
            return setAnnoProperties(layer, fieldname, minScale, maxScale, Color.Black);
        }

        private object setAnnoProperties(ILayer layer, string fieldname, double minScale, double maxScale, Color textcolor)
        {
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return null;

            IAnnotateLayerPropertiesCollection pAnnoLayerPropsColl = geoFeatureLayer.AnnotationProperties;
            pAnnoLayerPropsColl.Clear();

            //라벨의 특성(위치, Conflict 등등)
            IBasicOverposterLayerProperties4 pBasicOverposterLayerProps = new BasicOverposterLayerPropertiesClass();
            switch (geoFeatureLayer.FeatureClass.ShapeType)
            {
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon:
                    pBasicOverposterLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPolygon;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint:
                    pBasicOverposterLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPoint;
                    break;
                case ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline:
                    pBasicOverposterLayerProps.FeatureType = esriBasicOverposterFeatureType.esriOverposterPolyline;
                    break;
            }
            pBasicOverposterLayerProps.PointPlacementOnTop = false;
            pBasicOverposterLayerProps.LabelWeight = esriBasicOverposterWeight.esriHighWeight;
            pBasicOverposterLayerProps.GenerateUnplacedLabels = true;
            pBasicOverposterLayerProps.NumLabelsOption = esriBasicNumLabelsOption.esriOneLabelPerShape;

            //스케일별 라벨 보이기/감추기
            IAnnotateLayerProperties pAnnoLayerProps = new LabelEngineLayerPropertiesClass();
            pAnnoLayerProps.AnnotationMinimumScale = minScale;
            pAnnoLayerProps.AnnotationMaximumScale = maxScale;
            pAnnoLayerProps.CreateUnplacedElements = true;
            pAnnoLayerProps.DisplayAnnotation = true;
            //어느 Feature를 Labeling할 것인가?
            pAnnoLayerProps.WhereClause = string.Empty;

            IFormattedTextSymbol pText = new TextSymbolClass();
            pText.Font.Bold = false;
            pText.Font.Name = "굴림";
            pText.Color = EAGL.Display.ColorManager.GetESRIColor(textcolor);
            pText.Size = 9;

            //라벨 폰트 & 칼라 정의
            ILabelEngineLayerProperties pLabelEng = pAnnoLayerProps as ILabelEngineLayerProperties;
            pLabelEng.Expression = "[" + fieldname + "]";
            pLabelEng.Symbol = pText;
            pLabelEng.BasicOverposterLayerProperties = pBasicOverposterLayerProps as IBasicOverposterLayerProperties;

            pAnnoLayerPropsColl.Add((IAnnotateLayerProperties)pLabelEng);
            geoFeatureLayer.AnnotationProperties = pAnnoLayerPropsColl;
            return geoFeatureLayer.AnnotationProperties;
        }

        private object setDisplayAnno(ILayer layer, bool display)
        {
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return null;

            geoFeatureLayer.DisplayAnnotation = display;
            return geoFeatureLayer.DisplayAnnotation;
        }

        private object setMinimumScale(ILayer layer, double minScale)
        {
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return null;

            geoFeatureLayer.MinimumScale = minScale;
            return geoFeatureLayer.MinimumScale;
        }

        private object setMaximumScale(ILayer layer, double maxScale)
        {
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return null;
            geoFeatureLayer.MaximumScale = maxScale;
            return geoFeatureLayer.MaximumScale;
        }

        private object setDisplayField(ILayer layer, string fieldname)
        {
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return null;
            geoFeatureLayer.DisplayField = fieldname;
            return geoFeatureLayer.DisplayField;
        }

        private object setTooltip(ILayer layer, bool tooltip)
        {
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return null;
            geoFeatureLayer.ShowTips = tooltip;
            return geoFeatureLayer.ShowTips;
        }

        private void SaveLayerRendererData(ref WaterNetCore.OracleDBManager oDBManager, string lname, Hashtable param)
        {   
            System.Text.StringBuilder oStringBuilder = new System.Text.StringBuilder();
            oStringBuilder.AppendLine("UPDATE SI_LAYER SET ");
            oStringBuilder.AppendLine("F_RENDERE = :F_RENDERE");
            //oStringBuilder.AppendLine(",F_ANNOPROP = :F_ANNOPROP");
            //oStringBuilder.AppendLine(",F_MINSCALE = :F_MINSCALE");
            //oStringBuilder.AppendLine(",F_MAXSCALE = :F_MAXSCALE");
            //oStringBuilder.AppendLine(",F_FIELDDISP = :F_FIELDDISP");
            //oStringBuilder.AppendLine(",F_ANNODISP = :F_ANNODISP");
            //oStringBuilder.AppendLine(",F_TIPDISP = :F_TIPDISP");
            oStringBuilder.AppendLine("WHERE F_ALIAS = :F_ALIAS");

            IDataParameter[] oParams = {
                new Oracle.DataAccess.Client.OracleParameter("F_RENDERE",Oracle.DataAccess.Client.OracleDbType.Blob),
                //new Oracle.DataAccess.Client.OracleParameter("F_ANNOPROP",Oracle.DataAccess.Client.OracleDbType.Blob),
                //new Oracle.DataAccess.Client.OracleParameter("F_MINSCALE",Oracle.DataAccess.Client.OracleDbType.Double),
                //new Oracle.DataAccess.Client.OracleParameter("F_MAXSCALE",Oracle.DataAccess.Client.OracleDbType.Double),
                //new Oracle.DataAccess.Client.OracleParameter("F_FIELDDISP",Oracle.DataAccess.Client.OracleDbType.Varchar2, 30),
                //new Oracle.DataAccess.Client.OracleParameter("F_ANNODISP",Oracle.DataAccess.Client.OracleDbType.Varchar2, 10),
                //new Oracle.DataAccess.Client.OracleParameter("F_TIPDISP",Oracle.DataAccess.Client.OracleDbType.Varchar2, 10),
                new Oracle.DataAccess.Client.OracleParameter("F_ALIAS",Oracle.DataAccess.Client.OracleDbType.Varchar2,30)
            };

            oParams[0].Value = param.ContainsKey("F_RENDERE") ? param["F_RENDERE"] : DBNull.Value;
            //oParams[1].Value = GetStream(ipPropertySet.GetProperty("Annotation"));
            //oParams[2].Value = ipPropertySet.GetProperty("MinScale") == null ? DBNull.Value : ipPropertySet.GetProperty("MinScale");
            //oParams[3].Value = ipPropertySet.GetProperty("MaxScale") == null ? DBNull.Value : ipPropertySet.GetProperty("MaxScale");
            //oParams[4].Value = ipPropertySet.GetProperty("DisplayField") == null ? DBNull.Value : ipPropertySet.GetProperty("DisplayField");
            //oParams[5].Value = ipPropertySet.GetProperty("DisplayAnno") == null ? DBNull.Value : ipPropertySet.GetProperty("DisplayAnno");
            //oParams[6].Value = ipPropertySet.GetProperty("Tooltip") == null ? DBNull.Value : ipPropertySet.GetProperty("Tooltip");
            oParams[1].Value = lname;

            oDBManager.ExecuteScript(oStringBuilder.ToString(), oParams);

        }

        /// <summary>
        /// 등록된 렌더링 객체가 없을경우, 렌더링생성하여, DB에 저장
        /// </summary>
        /// <param name="oDBManager"></param>
        private void Initialize_LayerRenderer(ref WaterNetCore.OracleDBManager oDBManager)
        {
            try
            {
                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT * ");
                oStringBuilder.AppendLine(" FROM SI_LAYER A ");
                oStringBuilder.AppendLine("WHERE A.F_ORDER IS NOT NULL AND A.F_CATEGORY = '상수'");
                oStringBuilder.AppendLine(" ORDER BY A.F_ORDER DESC");

                DataTable table = oDBManager.ExecuteScriptDataTable(oStringBuilder.ToString(), null, "SI_LAYER");

                ///등록된 렌더링 객체가 없을경우, 렌더링생성하여, DB에 저장
                ILayer pLayer = null;
                IWorkspace pWS = null;
                foreach (System.Data.DataRow row in table.Rows)
                {
                    string lname = Convert.ToString(row["F_ALIAS"]);
                    if (!this.DICLAYERNAME.Contains(lname)) continue;
                    if (!Convert.IsDBNull(row["F_RENDERE"])) continue;

                    #region Workspace
                    if (pWS == null)
                        if (row["F_CATEGORY"].ToString() == "상수") pWS = VariableManager.m_Pipegraphic;
                    if (pWS == null) continue;
                    #endregion
                    pLayer = ArcManager.GetShapeLayer(pWS, row["F_NAME"].ToString(), row["F_ALIAS"].ToString());
                    if (pLayer == null) continue;

                    object o = null; Hashtable param = new Hashtable();
                    switch (pLayer.Name)
                    {
                        case "밸브":
                            o = this.setUniqueValueRenderer(pLayer, "FTR_CDE");
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o)); param.Add("RENDERE", o);
                            break;
                        case "유량계":
                            o = this.setSimpleValueRenderer(pLayer, string.Empty);
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o));
                            break;
                        case "수압계":
                            o = this.setSimpleValueRenderer(pLayer, string.Empty);
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o));
                            break;
                        case "소방시설":
                            o = this.setSimpleValueRenderer(pLayer, string.Empty);
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o));
                            break;
                        case "상수관로":
                            o = this.setUniqueValueRenderer(pLayer, "SAA_CDE");
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o));
                            break;
                        case "급수관로":
                            o = this.setUniqueValueRenderer(pLayer, "MOP_CDE");
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o));
                            break;
                        case "밸브실":
                            o = this.setSimpleValueRenderer(pLayer, string.Empty);
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o));
                            break;
                        case "소블록":
                        case "중블록":
                        case "대블록":
                            o = this.setUniqueValueRenderer(pLayer, "BLK_NAM");
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o));
                            break;
                        case "수도계량기":
                            o = this.setUniqueValueRenderer(pLayer, "MET_DIP");
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o));
                            break;
                        case "취수장":
                        case "가압장":
                        case "정수장":
                        case "배수지":
                        case "수원지":
                            o = this.setSimpleValueRenderer(pLayer, string.Empty);
                            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(o));
                            break;
                    }

                    //byte[] data = EAGL.Core.SerializationManager.Serialize(o);
                    if (param.Count == 0) continue;
                    try
                    {
                        SaveLayerRendererData(ref oDBManager, pLayer.Name, param);
                    }
                    catch { }
                }
            }
            catch { }
        }

        #endregion FeatureLayer PropertySet

        /// <summary>
        /// 레이어 환경
        /// </summary>
        private void Initialize_LayerInfo()
        {
            WaterNetCore.OracleDBManager m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            try
            {
                m_oDBManager.Open();
                ///SI_LAYER 테이블에 상수관망레이어의 렌더링객체가 없을경우 생성한다.
                Initialize_LayerRenderer(ref m_oDBManager);

                StringBuilder oStringBuilder = new StringBuilder();
                oStringBuilder.AppendLine("SELECT * ");
                //oStringBuilder.AppendLine("      A.F_NAME AS F_NAME, A.F_SOURCE AS F_SOURCE, A.F_TYPE AS F_TYPE, ");
                //oStringBuilder.AppendLine("      A.F_ORDER F_ORDER, A.F_ALIAS AS F_ALIAS, ");
                //oStringBuilder.AppendLine("      A.F_VIEW AS F_VIEW, A.F_OBJECT AS F_OBJECT, ");
                //oStringBuilder.AppendLine("      A.F_CATEGORY AS F_CATEGORY ");
                oStringBuilder.AppendLine(" FROM SI_LAYER A ");
                oStringBuilder.AppendLine("WHERE A.F_ORDER IS NOT NULL and a.f_category in ('상수')");
                oStringBuilder.AppendLine(" ORDER BY A.F_ORDER DESC");

                m_dsLayer = m_oDBManager.ExecuteScriptDataSet(oStringBuilder.ToString(), null, "SI_LAYER");

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.GetType().Name);
            }
            finally
            {
                m_oDBManager.Close();
            }
        }

        /// <summary>
        /// 인덱스 맵 로드
        /// </summary>
        private void Indexmap_Load()
        {
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            IWorkspace pWS = null;
            ILayer pLayer = null;

            frmSplashMsg oSplash = new frmSplashMsg();
            oSplash.Open();
            try
            {
                foreach (DataRow item in pDataTable.Rows)
                {
                    if ((new string[] {"중블록", "소블록" }).Contains(Convert.ToString(item["F_ALIAS"])))
                    {
                        if (item["F_CATEGORY"].ToString() == "지형") pWS = VariableManager.m_Topographic;
                        else if (item["F_CATEGORY"].ToString() == "상수") pWS = VariableManager.m_Pipegraphic;
                        else if (item["F_CATEGORY"].ToString() == "INP") pWS = VariableManager.m_INPgraphic;
                        else continue;
                        if (pWS == null) continue;

                        pLayer = ArcManager.GetShapeLayer(pWS, item["F_NAME"].ToString(), item["F_ALIAS"].ToString());
                        if (pLayer == null) continue;

                        oSplash.Message = "인덱스 맵을 설정중입니다.\n\r" + "[" + pLayer.Name + "]" + " 레이어를 불러오는 중입니다.";
                        Application.DoEvents();

                        LoadRendererStream2IndexMap(pLayer, item);
                        axIndexMap.AddLayer(pLayer);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                oSplash.Close();
            }

            //if (System.IO.File.Exists(Application.StartupPath + @"\" + "basemap.lyr"))
            //{
            //    this.axIndexMap.AddLayerFromFile(Application.StartupPath + @"\" + "basemap.lyr", this.axIndexMap.Map.LayerCount);
            //}
            this.axIndexMap.Extent = ArcManager.SetFullExtent();

        }

        private ESRI.ArcGIS.Carto.ILayerEvents_Event m_SatelImageLayerEvents;
        private ESRI.ArcGIS.Carto.ILayerEvents_Event m_BasemapLayerEvents;
                        
        /// <summary>
        /// Viewmap_Load - 지도화면에 레이어를 표시한다.
        /// </summary>
        protected virtual void Viewmap_Load()
        {
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            ILayer pLayer = null; 
            IWorkspace pWS = null;

            frmSplashMsg oSplash = new frmSplashMsg();
            oSplash.Open();
            try
            {
                foreach (DataRow item in pDataTable.Rows)
                {
                    #region Workspace
                    if (item["F_CATEGORY"].ToString() == "지형") pWS = VariableManager.m_Topographic;
                    else if (item["F_CATEGORY"].ToString() == "상수") pWS = VariableManager.m_Pipegraphic;
                    else if (item["F_CATEGORY"].ToString() == "INP") pWS = VariableManager.m_INPgraphic;
                    else continue;

                    if (pWS == null) continue;
                    #endregion

                    pLayer = ArcManager.GetShapeLayer(pWS, item["F_NAME"].ToString(), item["F_ALIAS"].ToString());
                    if (pLayer == null) continue;
                    oSplash.Message = "맵을 설정중입니다.\n\r" + "[" + pLayer.Name + "]" + " 레이어를 불러오는 중입니다.";
                    Application.DoEvents();

                    switch (pLayer.Name)
                    {
                        case "재염소처리지점":
                        case "중요시설":
                        case "감시지점":
                        case "누수지점":
                        case "민원지점":
                        case "관세척구간":
                            continue;
                    }
                    LoadRendererStream(pLayer, item);
                    if ((new string[] { "대블록", "중블록"}).Contains(pLayer.Name))
                    {
                        ILayerEffects pLayerEffect = pLayer as ILayerEffects;
                        pLayerEffect.Transparency = (short)80;
                    }
                    else if ((new string[] { "소블록"}).Contains(pLayer.Name))
                    {
                        ILayerEffects pLayerEffect = pLayer as ILayerEffects;
                        pLayerEffect.Transparency = (short)90;
                    }
                    pLayer.Visible = item["F_VIEW"].ToString() == "1" ? true : false;
                    axMap.AddLayer(pLayer);
                }

                if (EMFrame.statics.AppStatic.MIN_X + EMFrame.statics.AppStatic.MAX_Y == 0)
                {
                    double xmin = 0; double xmax = 0; double ymin = 0; double ymax = 0;
                    ArcManager.GetMapFullExtent(out xmin, out xmax, out ymin, out ymax);
                    EMFrame.statics.AppStatic.MIN_X = xmin; EMFrame.statics.AppStatic.MAX_X = xmax;
                    EMFrame.statics.AppStatic.MIN_Y = ymin; EMFrame.statics.AppStatic.MAX_Y = ymax;
                }

                //string ApiKey = "bd93484f66bde1eb7390de3cf9db4473afd7e617";
                //string cachedir = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Daum");
                //string cachepath = System.IO.Path.Combine(cachedir, "Map");
                //BruTile.Cache.FileCache fileCache = new BruTile.Cache.FileCache(System.IO.Path.Combine(cachepath, "tiles"), "png");
                //BruTile.ITileSource tilesource = BruTile.TileSource.Create(BruTile.Web.KnownTileServers.DaumMap, ApiKey, fileCache);
                //var cLayer = new WaterAOCore.tileCustomLayer(this.axMap.Map, tilesource, fileCache)
                //{
                //    Name = "생활지도(다음)",
                //    Visible = true
                //};
                //this.axMap.AddLayer(cLayer, this.axMap.LayerCount);

                //cachepath = System.IO.Path.Combine(cachedir, "Satellite");
                //BruTile.Cache.FileCache fileCache2 = new BruTile.Cache.FileCache(System.IO.Path.Combine(cachepath, "tiles"), "jpg");
                //BruTile.ITileSource tilesource2 = BruTile.TileSource.Create(BruTile.Web.KnownTileServers.DaumSatellite, ApiKey, fileCache2);
                //var cLayer2 = new WaterAOCore.tileCustomLayer(this.axMap.Map, tilesource2, fileCache2)
                //{
                //    Name = "위성영상(다음)",
                //    Visible = false
                //};
                //this.axMap.AddLayer(cLayer2, this.axMap.LayerCount);
            }
            catch { }
            finally
            {
                oSplash.Close();
            }
            this.axMap.Extent = ArcManager.SetFullExtent();
            this.axMap.ActiveView.ContentsChanged();
        }

        void m_BasemapLayerEvents_VisibilityChanged(bool currentState)
        {
            this.tsBasemap.Checked = currentState;
        }

        void m_SatelImageLayerEvents_VisibilityChanged(bool currentState)
        {
            this.tsSatelImage.Checked = currentState;
        }

        #region 모델레이어 지도표시여부
        /// <summary>
        /// 기존의 INP 레이어를 지도화면에서 삭제한다.
        /// </summary>
        protected void Remove_INP_Layer(string[] strLayers)
        {
            EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
            List<IFeatureLayer> layers = mapExplorer.FeatureLayers;
            for (int i = layers.Count - 1; i >= 0; i--)
            {
                if (strLayers.Contains(layers[i].Name))
                {
                    this.axMap.Map.DeleteLayer(layers[i]);
                }
            }
        }

        /// <summary>
        /// INP Shape을 지도화면에 로드한다.
        /// </summary>
        protected void Load_INP_Layer(Dictionary<string, IFeatureLayer> modelLayers, string[] strLayers)
        {
            Dictionary<string, IFeatureLayer> polygon = new Dictionary<string, IFeatureLayer>();
            Dictionary<string, IFeatureLayer> polyline = new Dictionary<string, IFeatureLayer>();
            Dictionary<string, IFeatureLayer> point = new Dictionary<string, IFeatureLayer>();
            foreach (KeyValuePair<string, IFeatureLayer> layer in modelLayers)
            {
                if (layer.Value.FeatureClass.ShapeType == esriGeometryType.esriGeometryPolygon)
                    polygon.Add(layer.Key, layer.Value);
                else if (layer.Value.FeatureClass.ShapeType == esriGeometryType.esriGeometryPolyline)
                    polyline.Add(layer.Key, layer.Value);
                else if (layer.Value.FeatureClass.ShapeType == esriGeometryType.esriGeometryPoint)
                    point.Add(layer.Key, layer.Value);
            }

            Dictionary<string, IFeatureLayer> layers = polygon.Union(polyline).Union(point).ToDictionary(item => item.Key, item => item.Value);
            foreach (KeyValuePair<string, IFeatureLayer> layer in layers)
            {
                if (strLayers.Contains(layer.Key))
                {
                    ((IFeatureLayer)layer.Value).Name = layer.Key;
                    ArcManager.SetDefaultRenderer2(axMap.ActiveView.FocusMap, (IFeatureLayer)layer.Value);
                    axMap.AddLayer(layer.Value);
                }
            }
        }
        #endregion 모델레이어 지도표시여부
        /// <summary>
        /// frmMap화면 로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMap_Load(object sender, EventArgs e)
        {
            axMap.MapUnits = esriUnits.esriMeters;

            this.tsBasemap.CheckedChanged += new EventHandler(tsBasemap_CheckedChanged);
            this.tsSatelImage.CheckedChanged += new EventHandler(tsSatelImage_CheckedChanged);
            this.tsBasemap.Click += new EventHandler(tsBasemap_Click);            
            this.tsSatelImage.Click += new EventHandler(tsSatelImage_Click);
            this.tsBasemap.PerformClick();
        }

        //void tsSatelImage_Click(object sender, EventArgs e)
        void tsSatelImage_Click(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            try
            {
                EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
                if (mapExplorer.GetLayer("항공영상") != null) return;
                if (VariableManager.global_extLayer.ContainsKey("항공영상"))
                {
                    if (!VariableManager.global_extLayer["항공영상"].Valid) return;

                    ILayer L = VariableManager.global_extLayer["항공영상"];
                    axMap.AddLayer(L, axMap.Map.LayerCount);
                    this.axTOC.Update();
                    this.m_SatelImageLayerEvents = L as ILayerEvents_Event;
                    this.m_SatelImageLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_SatelImageLayerEvents_VisibilityChanged);
                    //this.tsSatelImage.Enabled = true; this.tsSatelImage.Checked = true;
                }
                else
                {
                    string file = @Application.StartupPath + "\\" + "Airphoto_Airphoto2013.lyr";
                    ILayerFile layerfile = new LayerFileClass();

                    try
                    {
                        layerfile.Open(file);
                        if (!layerfile.get_IsPresent(file)) return;
                        if (!layerfile.get_IsLayerFile(file)) return;
                        if (layerfile.Layer != null)
                        {
                            if (!layerfile.Layer.Valid) return;
                            //axMap.AddLayerFromFile(Application.StartupPath + @"\" + "Aerial_grs80.lyr", axMap.Map.LayerCount);
                            ILayer L = layerfile.Layer; // axMap.Map.get_Layer(axMap.Map.LayerCount - 1);
                            L.Visible = true;
                            L.Name = "항공영상";
                            axMap.AddLayer(L, axMap.Map.LayerCount);
                            this.axTOC.Update();
                            this.m_SatelImageLayerEvents = L as ILayerEvents_Event;
                            this.m_SatelImageLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_SatelImageLayerEvents_VisibilityChanged);
                            this.tsSatelImage.Enabled = true; this.tsSatelImage.Checked = true;

                            VariableManager.global_extLayer.Add("항공영상", L);
                        }
                    }
                    catch { }
                }

                
            }
            catch {}
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
           
        }

        void tsBasemap_Click(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            try
            {
                EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
                if (mapExplorer.GetLayer("지형도") != null) return;
                if (VariableManager.global_extLayer.ContainsKey("지형도"))
                {
                    if (!VariableManager.global_extLayer["지형도"].Valid) return;

                    ILayer L = VariableManager.global_extLayer["지형도"];
                    L.Name = "지형도";
                    axMap.AddLayer(L, axMap.Map.LayerCount);
                    this.axTOC.Update();
                    this.m_BasemapLayerEvents = L as ILayerEvents_Event;
                    this.m_BasemapLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_BasemapLayerEvents_VisibilityChanged);
                    this.tsBasemap.Enabled = true; this.tsBasemap.Checked = true;
                }
                else
                {
                    string file = @Application.StartupPath + "\\" + "basemap_base_map.lyr";
                    ILayerFile layerfile = new LayerFileClass();
                    try
                    {
                        layerfile.Open(file);
                        if (!layerfile.get_IsPresent(file)) return;
                        if (!layerfile.get_IsLayerFile(file)) return;
                        if (layerfile.Layer != null)
                        {
                            if (!layerfile.Layer.Valid) return;
                            ILayer L = layerfile.Layer; // axMap.Map.get_Layer(axMap.Map.LayerCount - 1);
                            L.Visible = true;
                            L.Name = "지형도";
                            axMap.AddLayer(L, axMap.Map.LayerCount);
                            this.axTOC.Update();
                            this.m_BasemapLayerEvents = L as ILayerEvents_Event;
                            this.m_BasemapLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_BasemapLayerEvents_VisibilityChanged);
                            this.tsBasemap.Enabled = true; this.tsBasemap.Checked = true;
                            VariableManager.global_extLayer.Add("지형도", L);

                        }
                    }
                    catch { }
                }
                
            }
            catch { }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
            
        }

        void tsSatelImage_CheckedChanged(object sender, EventArgs e)
        {
            if (!((ILayer)this.m_SatelImageLayerEvents).Valid) return;

            ((ILayer)this.m_SatelImageLayerEvents).Visible = tsSatelImage.Checked;

            this.axMap.ActiveView.Refresh();
        }

        void tsBasemap_CheckedChanged(object sender, EventArgs e)
        {
            if (!((ILayer)this.m_BasemapLayerEvents).Valid) return;

            ((ILayer)this.m_BasemapLayerEvents).Visible = tsBasemap.Checked;

            this.axMap.ActiveView.Refresh();
        }

        /// <summary>
        /// axMap 그리기 메소드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void axMap_OnAfterDraw(object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            esriViewDrawPhase viewDrawPhase = (esriViewDrawPhase)e.viewDrawPhase;
            if (viewDrawPhase == esriViewDrawPhase.esriViewForeground)
            {
                axIndexMap.Refresh(esriViewDrawPhase.esriViewForeground, null, null);
            }
        }

        /// <summary>
        /// 지도에서 마우스이동 관련 메소드
        /// 현재 지도제어 명령에 따라 마우스포인터 변경
        /// 마우스의 위치를 StatusBar에 표시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void axMap_OnMouseMove(object sender, IMapControlEvents2_OnMouseMoveEvent e)
        {
            FunctionManager.SetStatusMessage(string.Format("{0}, {1}  {2}", e.mapX.ToString("#######.##"), e.mapY.ToString("#######.##"), axMap.MapUnits.ToString().Substring(4)));
            //사용자정의 툴버튼에 따라 마우스포인트 변경
            if ((axMap.CurrentTool == null) & (toolActionCommand.Checked))
            {
                axMap.MousePointer = esriControlsMousePointer.esriPointerCrosshair;
            }
            else if (axMap.CurrentTool == null)
            {
                axMap.MousePointer = esriControlsMousePointer.esriPointerDefault;
            }
        }

        protected virtual void axIndexMap_OnAfterDraw(object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            esriViewDrawPhase viewDrawPhase = (esriViewDrawPhase)e.viewDrawPhase;
            if (viewDrawPhase == esriViewDrawPhase.esriViewForeground)
            {
                //Get the IRGBColor interface
                IRgbColor color = new RgbColorClass();
                //Set the color properties
                color.RGB = 255;
                //Get the ILine symbol interface
                ILineSymbol outline = new SimpleLineSymbolClass();
                //Set the line symbol properties
                outline.Width = 1.5;
                outline.Color = color;
                //Get the IFillSymbol interface
                ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
                //Set the fill symbol properties
                simpleFillSymbol.Outline = outline;
                simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSHollow;

                object oLineSymbol = simpleFillSymbol;

                IGeometry geometry = axMap.Extent;

                axIndexMap.DrawShape(geometry, ref oLineSymbol);
            }
        }

        protected virtual void axIndexMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            if (e.button != 1) return;

            IEnvelope pEnvelope = null;
            IPoint pPoint = axIndexMap.ToMapPoint(e.x, e.y);
            pEnvelope = axIndexMap.TrackRectangle();
            if (!pEnvelope.IsEmpty)
            {
                axMap.Extent = pEnvelope;
            }
        }

        /// <summary>
        /// 내부 점인지 알아오기
        /// </summary>
        /// <param name="pEnvelpoe"></param>
        /// <param name="pPoint"></param>
        /// <returns></returns>
        private Boolean IsPointIn(IEnvelope pEnvelpoe, IPoint pPoint)
        {
            return (pEnvelpoe.XMin <= pPoint.X && pEnvelpoe.XMax >= pPoint.X) && (pEnvelpoe.YMin <= pPoint.Y && pEnvelpoe.YMax >= pPoint.Y);
        }

        protected virtual void frmMap_ResizeBegin(object sender, EventArgs e)
        {
            axMap.SuppressResizeDrawing(true);
        }

        protected virtual void frmMap_ResizeEnd(object sender, EventArgs e)
        {
            axMap.SuppressResizeDrawing(false);
        }

        public void axTOC_OnMouseDown(object sender, ITOCControlEvents_OnMouseDownEvent e)
        {
            if (2 != e.button) return;
            //do the HitTest
            axTOC.HitTest(e.x, e.y, ref item, ref map, ref layer, ref legendGroup, ref index);

            
            //axMap.CustomProperty = layer;

            switch (item)
            {
                case esriTOCControlItem.esriTOCControlItemHeading:        //UniqueValue : 글자선택
                    break;
                case esriTOCControlItem.esriTOCControlItemLayer:            //레이어선택
                    if (null == layer || !(layer is IFeatureLayer)) return;
                    toolStripMenuItemLabel.Enabled = true;
                    toolStripMenuItemScale.Enabled = true;
                    toolStripMenuItemSymbol.Enabled = false;
                    toolStripMenuItemTransfy.Enabled = true;
                    ToolStripMenuItemSaveSymbol.Enabled = true;

                    if (VariableManager.LAYER_DIC.ContainsKey(layer.Name))
                        ToolStripMenuItemInfo.Enabled = true;
                    else
                        ToolStripMenuItemInfo.Enabled = false;
                    //popup a context menu with a 'Properties' command
                    SatelImageMenuItem.Enabled = false;
                    BasemapMenuItem.Enabled = false;
                    contextMenuStripTOC.Show(axTOC, e.x, e.y);
                    break;
                case esriTOCControlItem.esriTOCControlItemLegendClass:   //심볼선택
                    if (null == layer || !(layer is IFeatureLayer)) return;
                    toolStripMenuItemLabel.Enabled = false;
                    toolStripMenuItemScale.Enabled = false;
                    toolStripMenuItemSymbol.Enabled = true;
                    toolStripMenuItemTransfy.Enabled = false;

                    ToolStripMenuItemSaveSymbol.Enabled = false;
                    ToolStripMenuItemInfo.Enabled = false;
                    SatelImageMenuItem.Enabled = false;
                    BasemapMenuItem.Enabled = false;
                    //popup a context menu with a 'Properties' command
                    contextMenuStripTOC.Show(axTOC, e.x, e.y);
                    break;
                case esriTOCControlItem.esriTOCControlItemMap:             //TOC 맵 선택
                    toolStripMenuItemLabel.Enabled = false;
                    toolStripMenuItemScale.Enabled = false;
                    toolStripMenuItemSymbol.Enabled = false;
                    toolStripMenuItemTransfy.Enabled = false;
                    ToolStripMenuItemInfo.Enabled = false;
                    ToolStripMenuItemSaveSymbol.Enabled = false;

                    SatelImageMenuItem.Enabled = true;// SatelImageMenuItem.Tag != null ? false : true;
                    BasemapMenuItem.Enabled = true;// BasemapMenuItem.Tag != null ? false : true;
                    contextMenuStripTOC.Show(axTOC, e.x, e.y);
                    break;
                case esriTOCControlItem.esriTOCControlItemNone:
                    break;
                default:
                    break;
            }

            ToolStripMenuItemInfo.Visible = false;

            m_workLayer = layer;
        }

        #region TOC 팝업메뉴
        /// <summary>
        /// 지형도 불러오기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BasemapMenuItem_Click(object sender, EventArgs e)
        {
            EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
            if (mapExplorer.GetLayer("지형도") != null) return;
            if (VariableManager.global_extLayer.ContainsKey("지형도"))
            {
                if (!VariableManager.global_extLayer["지형도"].Valid) return;

                ILayer L = VariableManager.global_extLayer["지형도"];
                L.Name = "지형도";
                axMap.AddLayer(L, axMap.Map.LayerCount);
                
                this.m_BasemapLayerEvents = L as ILayerEvents_Event;
                this.m_BasemapLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_BasemapLayerEvents_VisibilityChanged);
                this.tsBasemap.Enabled = true; this.tsBasemap.Checked = true;
            }
            else
            {
                string file = @Application.StartupPath + "\\" + "basemap_base_map.lyr";
                ILayerFile layerfile = new LayerFileClass();
                try
                {
                    layerfile.Open(file);
                    if (layerfile.Layer != null)
                    {
                        if (!layerfile.Layer.Valid) return;
                        ILayer L = layerfile.Layer; // axMap.Map.get_Layer(axMap.Map.LayerCount - 1);
                        L.Visible = true;
                        L.Name = "지형도";
                        axMap.AddLayer(L, axMap.Map.LayerCount);

                        this.m_BasemapLayerEvents = L as ILayerEvents_Event;
                        this.m_BasemapLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_BasemapLayerEvents_VisibilityChanged);
                        this.tsBasemap.Enabled = true; this.tsBasemap.Checked = true;
                        VariableManager.global_extLayer.Add("지형도", L);

                    }
                }
                catch { }
            }
            this.axTOC.Update();
        }

        /// <summary>
        /// 항공사진 불러오기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SatelImageMenuItem_Click(object sender, EventArgs e)
        {
            EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
            if (mapExplorer.GetLayer("항공영상") != null) return;
            if (VariableManager.global_extLayer.ContainsKey("항공영상"))
            {
                if (!VariableManager.global_extLayer["항공영상"].Valid) return;

                ILayer L = VariableManager.global_extLayer["항공영상"];
                axMap.AddLayer(L, axMap.Map.LayerCount);
                
                this.m_SatelImageLayerEvents = L as ILayerEvents_Event;
                this.m_SatelImageLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_SatelImageLayerEvents_VisibilityChanged);
                this.tsSatelImage.Enabled = true; this.tsSatelImage.Checked = true;
            }
            else
            {
                string file = @Application.StartupPath + "\\" + "Airphoto_Airphoto2013.lyr";
                ILayerFile layerfile = new LayerFileClass();

                try
                {
                    layerfile.Open(file);
                    if (layerfile.Layer != null)
                    {
                        if (!layerfile.Layer.Valid) return;
                        //axMap.AddLayerFromFile(Application.StartupPath + @"\" + "Aerial_grs80.lyr", axMap.Map.LayerCount);
                        ILayer L = layerfile.Layer; // axMap.Map.get_Layer(axMap.Map.LayerCount - 1);
                        L.Visible = true;
                        L.Name = "항공영상";
                        axMap.AddLayer(L, axMap.Map.LayerCount);
                        this.m_SatelImageLayerEvents = L as ILayerEvents_Event;
                        this.m_SatelImageLayerEvents.VisibilityChanged += new ILayerEvents_VisibilityChangedEventHandler(m_SatelImageLayerEvents_VisibilityChanged);
                        this.tsSatelImage.Enabled = true; this.tsSatelImage.Checked = true;

                        VariableManager.global_extLayer.Add("항공영상", L);                        
                    }
                }
                catch {}
            }

            this.axTOC.Update();
        }

        /// <summary>
        /// 축척설정 화면
        /// TOC에서 어떤것이 선택되었는지 상관없음.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemScale_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;

            Form frmScale = new frmSetScale(m_workLayer);
            if (frmScale.ShowDialog() != DialogResult.OK) return;

            //Fire contents changed event that the TOCControl listens to
            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        /// <summary>
        /// 주석설정
        /// TOC에서 어떤것이 선택되었는지 상관없음.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemLabel_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;
            if (item != esriTOCControlItem.esriTOCControlItemLayer) return;

            frmSetText frmText = new frmSetText();
            frmText.SelectSymbol(m_workLayer);

            if (frmText.ShowDialog() != DialogResult.OK) return;
            //Fire contents changed event that the TOCControl listens to
            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        /// <summary>
        /// 심볼설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemSymbol_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;

            IFeatureLayer featureLayer = m_workLayer as IFeatureLayer;
            if (featureLayer == null) return;

            IGeoFeatureLayer geoFeatureLayer = (IGeoFeatureLayer)featureLayer;
            IFeatureRenderer Renderer = geoFeatureLayer.Renderer;

            ISimpleRenderer pSimpleRenderer = null;
            IUniqueValueRenderer pUniqueRenderer = null;
            IClassBreaksRenderer pClassRenderer = null;

            SymbolAccess scAccess = new SymbolAccess();
            if (Renderer is ISimpleRenderer)
            {
                pSimpleRenderer = Renderer as ISimpleRenderer;
                scAccess.SimpleRenderer(pSimpleRenderer);
            }
            else if (Renderer is IClassBreaksRenderer)
            {
                pClassRenderer = Renderer as IClassBreaksRenderer;
                scAccess.ClassRenderer(pClassRenderer, Convert.ToInt32(index));
            }
            else if (Renderer is IUniqueValueRenderer)
            {
                pUniqueRenderer = Renderer as IUniqueValueRenderer;
                scAccess.UniqueRenderer(pUniqueRenderer, pUniqueRenderer.get_Value((int)index));
            }

            ISymbol pSymbol = scAccess.Selector();
            if (pSymbol != null)
            {
                if (Renderer is ISimpleRenderer)
                {
                    pSimpleRenderer = Renderer as ISimpleRenderer;
                    pSimpleRenderer.Symbol = pSymbol;
                }
                else if (Renderer is IClassBreaksRenderer)
                {
                    pClassRenderer = Renderer as IClassBreaksRenderer;
                    pClassRenderer.set_Symbol(Convert.ToInt32(index), pSymbol);
                }
                else if (Renderer is IUniqueValueRenderer)
                {
                    pUniqueRenderer = Renderer as IUniqueValueRenderer;
                    pUniqueRenderer.set_Symbol(pUniqueRenderer.get_Value((int)index), pSymbol);
                }
                
                //Fire contents changed event that the TOCControl listens to
                axMap.ActiveView.ContentsChanged();
                //Refresh the display
                axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
            }
        }

        /// <summary>
        /// 투명도 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemTransfy_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;
            if (item != esriTOCControlItem.esriTOCControlItemLayer) return;

            frmSetTrans frmTrans = new frmSetTrans(m_workLayer);
            if (frmTrans.ShowDialog() != DialogResult.OK) return;

            //Fire contents changed event that the TOCControl listens to
            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        /// <summary>
        ///선택레이어 렌더링 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItemSelInit_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            foreach (DataRow item in pDataTable.Rows)
            {
                if (Convert.ToString(item["F_ALIAS"]) == m_workLayer.Name)
                {
                    LoadRendererStream(m_workLayer, item);

                    axMap.ActiveView.ContentsChanged();
                    axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);

                    break;
                }
            }
        }

        /// <summary>
        /// 전체레이어 렌더링 초기화 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItemAllInit_Click(object sender, EventArgs e)
        {
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            ILayer pLayer = null;
            for (int i = 0; i < axMap.LayerCount; i++)
            {
                pLayer = axMap.get_Layer(i);
                if (pLayer != null)
                {
                    foreach (DataRow item in pDataTable.Rows)
                    {
                        if (Convert.ToString(item["F_ALIAS"]) == pLayer.Name)
                        {
                            LoadRendererStream(pLayer, item);

                            break;
                        }
                    }
                }
            }

            axMap.ActiveView.ContentsChanged();
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        /// <summary>
        /// 레이어 속성정보
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItemInfo_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;

            string lname = ((IFeatureLayer)m_workLayer).Name;
            if (VariableManager.LAYER_DIC.ContainsKey(lname))
            {
                EAGL.Data.Wrapper.ITableWrapper tr = VariableManager.LAYER_DIC[lname];
                if (tr != null)
                {
                    Form oform = new frmLayerDataInfo(VariableManager.LAYER_DIC[lname]);
                    oform.Text = lname + " 속성정보";
                    ((frmLayerDataInfo)oform).MAP = (IMapControl3)this.axMap.Object;
                    oform.Show();
                }
            }
        }



        #endregion TOC 팝업메뉴

        #region 블록정보 트리뷰에 적용
        private void InitializeBlock()
        {
            ITable b_Table = ArcManager.GetTable(VariableManager.m_Pipegraphic, "WTL_BLBG_AS");
            ITable m_Table = ArcManager.GetTable(VariableManager.m_Pipegraphic, "WTL_BLBM_AS");
            ITable s_Table = ArcManager.GetTable(VariableManager.m_Pipegraphic, "WTL_BLSM_AS");
            if (b_Table == null) return;
            if (m_Table == null) return;
            if (s_Table == null) return;
            string[] fieldList = { "BLK_NAM" };
            ICursor pCursor = ArcManager.GetSortedCursor(b_Table, string.Empty, fieldList, true);
            if (pCursor == null) return;

            IRow pRow = pCursor.NextRow();
            while (pRow != null)
            {
                //대블록 IRow
                TreeNode tnode = new TreeNode();
                string strNam = Convert.ToString(ArcManager.GetValue(pRow, "BLK_NAM"));
                string strCDE = Convert.ToString(ArcManager.GetValue(pRow, "FTR_CDE"));
                string strIDN = Convert.ToString(ArcManager.GetValue(pRow, "FTR_IDN"));

                tnode.Text = strNam;
                tnode.Tag = strIDN;
                tnode.Name = strCDE;
                tvBlock.Nodes.Add(tnode);
                
                SetTreeNode(tnode, m_Table, "UBL_IDN = '" + strIDN + "'");

                pRow = pCursor.NextRow();
            }

            tvBlock.ExpandAll();

            ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
        }

        private void SetTreeNode(TreeNode pNode, ITable pTable, string sWhereClause)
        {
            if (pTable == null) return;
            string[] fieldList = {"BLK_NAM"};
            ICursor pCursor = ArcManager.GetSortedCursor(pTable, sWhereClause,fieldList,true);
            if (pCursor == null) return;

            IRow pRow = pCursor.NextRow();
            while (pRow != null)
            {
                TreeNode tnode = new TreeNode();
                string strNam = Convert.ToString(ArcManager.GetValue(pRow, "BLK_NAM"));
                string strCDE = Convert.ToString(ArcManager.GetValue(pRow, "FTR_CDE"));
                string strIDN = Convert.ToString(ArcManager.GetValue(pRow, "FTR_IDN"));

                tnode.Text = strNam;
                tnode.Tag = strIDN;
                tnode.Name = strCDE;

                pNode.Nodes.Add(tnode);

                //소블록 
                ITable s_Table = ArcManager.GetTable(VariableManager.m_Pipegraphic, "WTL_BLSM_AS");
                if (s_Table == null) continue;

                SetTreeNode(tnode, s_Table, "UBL_IDN = '" + strIDN + "'");

                pRow = pCursor.NextRow();
            }

            ESRI.ArcGIS.ADF.ComReleaser.ReleaseCOMObject(pCursor);
        }


        protected virtual void tvBlock_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode oNode = e.Node; // tvBlock.SelectedNode;
            if (oNode == null) return;

            IFeatureLayer pFeatureLayer = default(IFeatureLayer);
            switch (oNode.Name)
            {
                case "BZ001":    //대블록
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "대블록");
                    break;
                case "BZ002":    //중블록
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "중블록");
                    break;
                case "BZ003":    //소블록
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "소블록");
                    break;
            }
            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '" + oNode.Tag.ToString() + "'");
            if (pFeature == null) return;

            axMap.Extent = pFeature.Shape.Envelope;
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);

        }    
        #endregion

        #region TabControl - ActiveTab
        public int ActiveTabIndex
        {
            set {tabTOC.SelectedIndex = value;  }
        }

        public string ActiveTabName
        {
            set 
            {
                foreach (TabPage item in tabTOC.TabPages)
                {
                    if (item.Text == value)
                    {
                        tabTOC.SelectedTab = item;
                        return;
                    }
                }
            }
        }
        #endregion

        private void tabTOC_Selected(object sender, TabControlEventArgs e)
        {
            //switch (e.TabPage.Name)
            //{ 
            //    case 0:    //TOC
            //        break;
            //    case 1:    //블록정보
            //        break;
            //    case 2:
            //        if (ArcManager.GetMapLayer((IMapControl3)axMap.Object, "건물") == null)
            //        {
            //            btnSearchBldg.Enabled = false;
            //            MessageBox.Show("건물레이어가 로드되지 않았습니다.");
            //        }
            //        else
            //        {
            //            txtBldg.Focus();
            //            btnSearchBldg.Enabled = true;
            //        }
            //        break;
            //    case 3:
            //        if (ArcManager.GetMapLayer((IMapControl3)axMap.Object, "지형지번") == null)
            //        {
            //            MessageBox.Show("지번레이어가 로드되지 않았습니다.");
            //        }
            //        break;
            //    default:
            //        break;
            //}
        }

        #region 건물명 찾기
        private void btnSearchBldg_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtBldg.Text))
	        {
                MessageBox.Show("건물명을 입력하세요.", "안내", MessageBoxButtons.OK);
                return;
	        }

            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "건물");
            if (pLayer == null)
            {
                MessageBox.Show("건물레이어가 로드되지 않았습니다.");
                return;
            }

            string sWhereClause = "BLD_NAM like '%" + txtBldg.Text + "%'";
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                string[] fieldList = { "BLD_NAM" };
                ICursor pCursor = ArcManager.GetSortedCursor((ITable)pLayer, sWhereClause, fieldList, true);
                comReleaser.ManageLifetime(pCursor);

                IRow pRow = pCursor.NextRow();
                if (pRow == null)
                {
                    MessageBox.Show("검색된 건물이 없습니다.");
                    return;
                }

                listBldg.Items.Clear();
                listBldg.BeginUpdate();
                while (pRow != null)
                {
                    ListViewItem pitem = new ListViewItem();
                    pitem.Text = Convert.ToString(pRow.get_Value(pRow.Fields.FindField("BLD_NAM")));

                    pitem.Tag = pRow.OID;
                    listBldg.Items.Add(pitem);
                    pRow = pCursor.NextRow();
                }
                listBldg.EndUpdate();
            }
           
        }

        /// <summary>
        /// 건물찾기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBldg_DoubleClick(object sender, EventArgs e)
        {
            if (listBldg.SelectedItems.Count != 1) return;

            string sOID = Convert.ToString(listBldg.SelectedItems[0].Tag);
            if (string.IsNullOrEmpty(sOID)) return;

            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "건물");
            IFeature pFeature = ArcManager.GetFeature(pLayer, Convert.ToInt32(sOID));
            if (pFeature == null) return;

            ArcManager.MoveCenterAt((IMapControl3)axMap.Object, pFeature.Shape);
            Application.DoEvents();
            ArcManager.FlashShape(axMap.ActiveView, pFeature.Shape, 6, 100);
        }

        private void frmMap_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (tabTOC.SelectedIndex < 2) return;
            
            if ((tabTOC.SelectedIndex == 2) && (e.KeyChar == (char)Keys.Return))
            {
                btnSearchBldg_Click(sender, new EventArgs());
            }
            else if ((tabTOC.SelectedIndex == 3) && (e.KeyChar == (char)Keys.Return))
            {
                btnSearchAddr_Click(sender, new EventArgs());
            }
        }
        #endregion

        /// <summary>
        /// 해당 문자열이 숫자인지를 확인하는 정규식
        /// Char.IsNumber 메서드 (Char)로 변환 해주세요
        /// </summary>
        /// <param name="strData">문자열</param>
        /// <returns></returns>
        private bool IsNumeric(string strData)
        {
            System.Text.RegularExpressions.Regex isNumber = new System.Text.RegularExpressions.Regex(@"^\d+$");
            System.Text.RegularExpressions.Match m = isNumber.Match(strData);

            return m.Success;
        }

        #region 주소찾기
        private void btnSearchAddr_Click(object sender, EventArgs e)
        {
            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "지형지번");
            if (pLayer == null)
            {
                MessageBox.Show("지형지번 레이어가 로드되지 않았습니다.");
                return;
            }

            if (String.IsNullOrEmpty(txtBonbun.Text))
            {
                MessageBox.Show("본번 지번을 입력하세요.", "안내", MessageBoxButtons.OK);
                txtBonbun.Focus();
                return;
            }

            if (!IsNumeric(txtBonbun.Text))
            {
                MessageBox.Show("본번 지번에 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                txtBonbun.Focus();
                return;                
            }

            if (!String.IsNullOrEmpty(txtBubun.Text))
            {
                if (!IsNumeric(txtBubun.Text))
                {
                    MessageBox.Show("부번 지번에 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtBubun.Focus();
                    return;
                }
            }

            string sWhereClause = sWhereClause = "BJD_CDE = '" + cboDong.SelectedValue + "'";

            //if (cboRi.SelectedIndex == -1) sWhereClause = "BJD_CDE = '" + cboDong.SelectedValue + "'";
            //else sWhereClause = "BJD_CDE = '" + cboRi.SelectedValue + "'";

            if (checkSan.Checked) sWhereClause += " AND SAN_CDE = '1'";
            else                  sWhereClause += " AND SAN_CDE <> '1'";
            sWhereClause += " AND FAC_NUM = '" + txtBonbun.Text + "'";

            if (txtBubun.Text.Length != 0) sWhereClause += " AND FAD_NUM = '" + txtBubun.Text + "'";

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                string[] fieldList = { "SAN_CDE", "FAC_NUM", "FAD_NUM" };
                ICursor pCursor = ArcManager.GetSortedCursor((ITable)pLayer, sWhereClause, fieldList, true);
                comReleaser.ManageLifetime(pCursor);

                IRow pRow = pCursor.NextRow();
                if (pRow == null)
                {
                    MessageBox.Show("검색된 주소가 없습니다.");
                    return;
                }

                listAddr.Items.Clear();
                listAddr.BeginUpdate();
                while (pRow != null)
                {
                    string pAddress = cboDong.Text;
                    if (Convert.ToString(pRow.get_Value(pRow.Fields.FindField("SAN_CDE"))).Equals("1")) pAddress = "산 ";
                    pAddress += Convert.ToString(pRow.get_Value(pRow.Fields.FindField("FAC_NUM")));
                    if (Convert.ToString(pRow.get_Value(pRow.Fields.FindField("FAD_NUM"))).Trim().Length != 0)
                        pAddress += "-" + Convert.ToString(pRow.get_Value(pRow.Fields.FindField("FAD_NUM")));

                    pAddress += "번지";

                    ListViewItem pitem = new ListViewItem();
                    pitem.SubItems[0].Text = pAddress;
                    pitem.Tag = pRow.OID;
                    listAddr.Items.Add(pitem);
                    pRow = pCursor.NextRow();
                }
                listAddr.EndUpdate();
            }
        }

        /// <summary>
        /// 읍/면/동을 선택하여 해당 리를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboDong_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT DISTINCT RI_CODE CD, RI_NAME CDNM");
            oStringBuilder.AppendLine("  FROM CM_DONGCODE");
            oStringBuilder.AppendLine(" WHERE GUBUN = '1' AND SI_CODE = '" + EMFrame.statics.AppStatic.USER_SGCCD + "'");
            oStringBuilder.AppendLine("   AND DONG_CODE = '" + cboDong.SelectedValue + "'");
            oStringBuilder.AppendLine(" ORDER BY RI_CODE ASC");

            FormManager.SetComboBoxEX(cboRi, oStringBuilder.ToString(), false);
        }

        /// <summary>
        /// 주소찾기 목록을 선택하여, 지번위치로 지도이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listAddr_DoubleClick(object sender, EventArgs e)
        {
            if (listAddr.SelectedItems.Count != 1) return;

            string sOID = Convert.ToString(listAddr.SelectedItems[0].Tag);
            if (string.IsNullOrEmpty(sOID)) return;

            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "지형지번");
            IFeature pFeature = ArcManager.GetFeature(pLayer, Convert.ToInt32(sOID));
            if (pFeature == null) return;

            ArcManager.MoveCenterAt((IMapControl3)axMap.Object, pFeature.Shape);
            Application.DoEvents();
            ArcManager.FlashShape(axMap.ActiveView, pFeature.Shape, 6, 100);
        }

        #endregion 주소찾기

        #region 수용가찾기
        /// <summary>
        /// 수용가 명
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdCustName_CheckedChanged(object sender, EventArgs e)
        {
            txtNo1.Visible = false;
            txtNo2.Visible = false;
            txtNo3.Visible = false;
            lblNo1.Visible = false;
            lblNo2.Visible = false;

            txtCustName.Visible = true;
        }

        /// <summary>
        /// 수용가번호
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdCustNo_CheckedChanged(object sender, EventArgs e)
        {
            txtNo1.Visible = true;
            txtNo2.Visible = true;
            txtNo3.Visible = true;
            lblNo1.Visible = true;
            lblNo2.Visible = true;

            txtCustName.Visible = false;
        }

        /// <summary>
        /// 수용가 검색을 수행한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearchCust_Click(object sender, EventArgs e)
        {
            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "수도계량기");
            if (pLayer == null)
            {
                MessageBox.Show("수도계량기 레이어가 로드되지 않았습니다.");
                return;
            }

            #region 검색조건 체크
            if (rdCustName.Checked)
            {
                if (String.IsNullOrEmpty(txtCustName.Text))
                {
                    MessageBox.Show("수용가명 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtCustName.Focus();
                    return;
                }
            }
            else if (rdCustNo.Checked)
            {
                if (String.IsNullOrEmpty(txtNo1.Text))
                {
                    MessageBox.Show("수용가번호를 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtNo1.Focus();
                    return;
                }

                if (!String.IsNullOrEmpty(txtNo1.Text))
                {
                    if (!IsNumeric(txtNo1.Text))
                    {
                        MessageBox.Show("수용가번호에는 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                        txtNo1.Focus();
                        return;
                    }
                }

                if (String.IsNullOrEmpty(txtNo2.Text))
                {
                    MessageBox.Show("수용가번호를 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtNo2.Focus();
                    return;
                }

                if (!String.IsNullOrEmpty(txtNo2.Text))
                {
                    if (!IsNumeric(txtNo2.Text))
                    {
                        MessageBox.Show("수용가번호에는 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                        txtNo2.Focus();
                        return;
                    }
                }

                if (String.IsNullOrEmpty(txtNo3.Text))
                {
                    MessageBox.Show("수용가번호를 입력하세요.", "안내", MessageBoxButtons.OK);
                    txtNo3.Focus();
                    return;
                }

                if (!String.IsNullOrEmpty(txtNo3.Text))
                {
                    if (!IsNumeric(txtNo3.Text))
                    {
                        MessageBox.Show("수용가번호에는 숫자만 입력하세요.", "안내", MessageBoxButtons.OK);
                        txtNo3.Focus();
                        return;
                    }
                }
            }
            #endregion 검색조건 체크

            string sWhereClause = string.Empty;
            if (rdCustName.Checked)
            {
                sWhereClause = "DMNM LIKE '" + txtCustName.Text + "%'";
            }
            else if (rdCustNo.Checked)
            {
                sWhereClause = "DMNO LIKE '" + txtNo1.Text + txtNo2.Text + txtNo3.Text + "%'";
            }

            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                string[] fieldList = { "DMNM", "DMNO" };
                ICursor pCursor = ArcManager.GetSortedCursor((ITable)pLayer, sWhereClause, fieldList, true);
                comReleaser.ManageLifetime(pCursor);

                IRow pRow = pCursor.NextRow();
                if (pRow == null)
                {
                    MessageBox.Show("검색된 수용가정보가 없습니다.");
                    return;
                }

                listCust.Items.Clear();
                listCust.BeginUpdate();
                while (pRow != null)
                {
                    ListViewItem pitem = new ListViewItem();
                    pitem.SubItems.Add("DMNO");

                    pitem.Text = Convert.ToString(pRow.get_Value(pRow.Fields.FindField("DMNM")));
                    pitem.SubItems[1].Text = Convert.ToString(pRow.get_Value(pRow.Fields.FindField("DMNO"))).Substring(0,5)
                                           + "-" + Convert.ToString(pRow.get_Value(pRow.Fields.FindField("DMNO"))).Substring(5, 2)
                                           + "-" + Convert.ToString(pRow.get_Value(pRow.Fields.FindField("DMNO"))).Substring(7,5);
                    pitem.Tag = pRow.OID;
                    listCust.Items.Add(pitem);
                    pRow = pCursor.NextRow();
                }
                listCust.EndUpdate();
            }
             
        }

        /// <summary>
        /// 수용가목록 클릭하여 위치찾기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listCust_DoubleClick(object sender, EventArgs e)
        {
            if (listCust.SelectedItems.Count != 1) return;

            string sOID = Convert.ToString(listCust.SelectedItems[0].Tag);
            if (string.IsNullOrEmpty(sOID)) return;

            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "수도계량기");
            IFeature pFeature = ArcManager.GetFeature(pLayer, Convert.ToInt32(sOID));
            if (pFeature == null) return;

            ArcManager.MoveCenterAt((IMapControl3)axMap.Object, pFeature.Shape);
            Application.DoEvents();
            ArcManager.FlashShape(axMap.ActiveView, pFeature.Shape, 6, 100);
        }

        #endregion 수용가찾기

        #region 지도제어 툴바
        protected virtual void toolActionCommand_Click(object sender, EventArgs e)
        {
            if (toolActionCommand.Checked) axMap.CurrentTool = null;
        }

        private void axToolbar_OnItemClick(object sender, IToolbarControlEvents_OnItemClickEvent e)
        {
            toolActionCommand.Checked = false;
        }

        #endregion 지도제어 툴바

        /// <summary>
        /// SI_LAYER에 저장된 PropertySet을 레이어에 적용한다.
        /// 렌더러, 라벨, activeScale 설정값 => IndexMap에 적용
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pData"></param>
        private void LoadRendererStream2IndexMap(ILayer pLayer, DataRow row)
        {
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            try
            {
                if (!Convert.IsDBNull(row["F_RENDERE"]))
                {
                    byte[] pData = row["F_RENDERE"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;

                    object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IFeatureRenderer"));
                    if (o != null)
                        geoFeatureLayer.Renderer = o as IFeatureRenderer;
                }

                if (pLayer.Name.Equals("대블록") || pLayer.Name.Equals("중블록") || pLayer.Name.Equals("소블록"))
                {
                    geoFeatureLayer.AnnotationProperties = this.setAnnoProperties(pLayer, "BLK_NAM",0,0,Color.Blue) as IAnnotateLayerPropertiesCollection;
                    geoFeatureLayer.DisplayAnnotation = (bool)(new string[] {"중블록","소블록"}).Contains(pLayer.Name);
                    //if (!Convert.IsDBNull(row["F_ANNOPROP"]))
                    //{
                    //    byte[] pData = row["F_ANNOPROP"] as byte[];
                    //    if (pData == null) return;
                    //    if (pData.Length == 0) return;
                    //    object o = EAGL.Core.SerializationManager.Deserialize(pData, EAGL.Core.GUIDManager.GetGUIDForInterface("IAnnotateLayerPropertiesCollection"));
                    //    if (o != null)
                    //        geoFeatureLayer.AnnotationProperties = o as IAnnotateLayerPropertiesCollection;
                    //}
                }
            }
            catch { }

        }

        private void LoadRendererStream(ILayer pLayer, DataRow row)
        {
            IGeoFeatureLayer geoFeatureLayer = pLayer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            try
            {
                if (!Convert.IsDBNull(row["F_RENDERE"]))
                {
                    byte[] pData = row["F_RENDERE"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;

                    object o = EAGL.Core.SerializationManager.Deserialize(pData, new Guid("40A9E884-5533-11D0-98BE-00805F7CED21"));// EAGL.Core.GUIDManager.GetGUIDForInterface("IFeatureRenderer"));
                    if (o != null)
                        geoFeatureLayer.Renderer = o as IFeatureRenderer;
                }
                if (!Convert.IsDBNull(row["F_ANNOPROP"]))
                {
                    byte[] pData = row["F_ANNOPROP"] as byte[];
                    if (pData == null) return;
                    if (pData.Length == 0) return;
                    object o = EAGL.Core.SerializationManager.Deserialize(pData, new Guid("1D5849F2-0D33-11D2-A26F-080009B6F22B"));// EAGL.Core.GUIDManager.GetGUIDForInterface("IAnnotateLayerPropertiesCollection"));
                    if (o != null)
                        geoFeatureLayer.AnnotationProperties = o as IAnnotateLayerPropertiesCollection;

                }
                if (!Convert.IsDBNull(row["F_MINSCALE"]))
                {
                    geoFeatureLayer.MinimumScale = Convert.ToDouble(row["F_MINSCALE"]);
                }
                if (!Convert.IsDBNull(row["F_MAXSCALE"]))
                {
                    geoFeatureLayer.MaximumScale = Convert.ToDouble(row["F_MAXSCALE"]);
                }
                if (!Convert.IsDBNull(row["F_FIELDDISP"]))
                {
                    geoFeatureLayer.DisplayField = Convert.ToString(row["F_FIELDDISP"]);
                }
                if (!Convert.IsDBNull(row["F_ANNODISP"]))
                {
                    geoFeatureLayer.DisplayAnnotation = Convert.ToBoolean(row["F_ANNODISP"]);
                }
                if (!Convert.IsDBNull(row["F_TIPDISP"]))
                {
                    geoFeatureLayer.ShowTips = Convert.ToBoolean(row["F_TIPDISP"]);
                }
            }
            catch { }
        }

        /// <summary>
        /// 심볼저장 - SI_LAYER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItemSaveSymbol_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;

            IFeatureLayer featureLayer = m_workLayer as IFeatureLayer;
            if (featureLayer == null) return;

            IGeoFeatureLayer geoFeatureLayer = (IGeoFeatureLayer)featureLayer;
            IFeatureRenderer Renderer = geoFeatureLayer.Renderer;

            Hashtable param = new Hashtable();
            param.Add("F_RENDERE", EAGL.Core.SerializationManager.Serialize(Renderer));
            using (EMFrame.dm.EMapper mapper = new EMFrame.dm.EMapper(EMFrame.statics.AppStatic.USER_SGCCD))
            {
                SaveLayerRendererData(mapper, featureLayer.Name, param);
            }
        }

        private void SaveLayerRendererData(EMFrame.dm.EMapper mapper, string lname, Hashtable param)
        {
            System.Text.StringBuilder oStringBuilder = new System.Text.StringBuilder();
            oStringBuilder.AppendLine("UPDATE SI_LAYER SET ");
            oStringBuilder.AppendLine("F_RENDERE = :F_RENDERE");
            //oStringBuilder.AppendLine(",F_ANNOPROP = :F_ANNOPROP");
            //oStringBuilder.AppendLine(",F_MINSCALE = :F_MINSCALE");
            //oStringBuilder.AppendLine(",F_MAXSCALE = :F_MAXSCALE");
            //oStringBuilder.AppendLine(",F_FIELDDISP = :F_FIELDDISP");
            //oStringBuilder.AppendLine(",F_ANNODISP = :F_ANNODISP");
            //oStringBuilder.AppendLine(",F_TIPDISP = :F_TIPDISP");
            oStringBuilder.AppendLine("WHERE F_ALIAS = :F_ALIAS");

            IDataParameter[] oParams = {
                new Oracle.DataAccess.Client.OracleParameter("F_RENDERE",Oracle.DataAccess.Client.OracleDbType.Blob),
                //new Oracle.DataAccess.Client.OracleParameter("F_ANNOPROP",Oracle.DataAccess.Client.OracleDbType.Blob),
                //new Oracle.DataAccess.Client.OracleParameter("F_MINSCALE",Oracle.DataAccess.Client.OracleDbType.Double),
                //new Oracle.DataAccess.Client.OracleParameter("F_MAXSCALE",Oracle.DataAccess.Client.OracleDbType.Double),
                //new Oracle.DataAccess.Client.OracleParameter("F_FIELDDISP",Oracle.DataAccess.Client.OracleDbType.Varchar2, 30),
                //new Oracle.DataAccess.Client.OracleParameter("F_ANNODISP",Oracle.DataAccess.Client.OracleDbType.Varchar2, 10),
                //new Oracle.DataAccess.Client.OracleParameter("F_TIPDISP",Oracle.DataAccess.Client.OracleDbType.Varchar2, 10),
                new Oracle.DataAccess.Client.OracleParameter("F_ALIAS",Oracle.DataAccess.Client.OracleDbType.Varchar2,30)
            };

            oParams[0].Value = param.ContainsKey("F_RENDERE") ? param["F_RENDERE"] : DBNull.Value;
            //oParams[1].Value = GetStream(ipPropertySet.GetProperty("Annotation"));
            //oParams[2].Value = ipPropertySet.GetProperty("MinScale") == null ? DBNull.Value : ipPropertySet.GetProperty("MinScale");
            //oParams[3].Value = ipPropertySet.GetProperty("MaxScale") == null ? DBNull.Value : ipPropertySet.GetProperty("MaxScale");
            //oParams[4].Value = ipPropertySet.GetProperty("DisplayField") == null ? DBNull.Value : ipPropertySet.GetProperty("DisplayField");
            //oParams[5].Value = ipPropertySet.GetProperty("DisplayAnno") == null ? DBNull.Value : ipPropertySet.GetProperty("DisplayAnno");
            //oParams[6].Value = ipPropertySet.GetProperty("Tooltip") == null ? DBNull.Value : ipPropertySet.GetProperty("Tooltip");
            oParams[1].Value = lname;

            mapper.ExecuteScript(oStringBuilder.ToString(), oParams);

        }
        
        /// <summary>
        /// 선택레이어 렌더링 초기화(외부 메소드)
        /// </summary>
        /// <param name="pLayer"></param>
        protected void SetDefaultRenderer(ILayer pLayer)
        {
            if (pLayer == null) return;
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            foreach (DataRow item in pDataTable.Rows)
            {
                if (Convert.ToString(item["F_ALIAS"]) == pLayer.Name)
                {
                    LoadRendererStream(pLayer, item);

                    this.axMap.ActiveView.ContentsChanged();
                    this.axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);

                    break;
                }
            }
        }

        #region 수리해석결과 유량 방향성 렌더링
        /// <summary>
        /// 해석결과(유량) 방향성 설정
        /// </summary>
        /// <param name="values"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        private List<string> getValueMapRow(ref Dictionary<string, string> values, double min, double max, out int direction)
        {
            List<string> ids = null;
            if (min == max) //0
            {
                ids = values.Where(entry => (double.Parse(entry.Value) == min && double.Parse(entry.Value) == max))
                                                   .Select(v => string.Format("{0}", v.Key)).ToList();
                direction = 0;
            }
            else if (min < 0 && max < 0) //-
            {
                ids = values.Where(entry => (double.Parse(entry.Value) >= min && double.Parse(entry.Value) < max))
                                                   .Select(v => string.Format("{0}", v.Key)).ToList();
                direction = -1;
            }
            else if (min >= 0 && max > 0)
            {
                ids = values.Where(entry => (double.Parse(entry.Value) > min && double.Parse(entry.Value) <= max))
                                                   .Select(v => string.Format("{0}", v.Key)).ToList();
                direction = 1;
            }
            else
            {
                ids = values.Select(v => string.Format("{0}", v.Key)).ToList();
                direction = 2;
            }
            return ids;
        }

        public void DrawArrowRenderer(EpaSimulator.ISimulator model)
        {
            DrawArrowRenderer(model, 0);
        }
        public void DrawArrowRenderer(EpaSimulator.ISimulator model, int timeperiod)
        {
            EAGL.Explore.MapExplorer mapExplorer = new EAGL.Explore.MapExplorer(this.axMap.Map);
            ILayer layer = mapExplorer.GetLayer("PIPE");
            IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
            if (geoFeatureLayer == null) return;

            Dictionary<string, float> values = model.Output.GetFlowDir(timeperiod);
            if (values.Count == 0) return;

            EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
            vrm.Fields = new string[] { "ID" };
            Dictionary<int, List<string>> idsList = new Dictionary<int, List<string>>(3);
            idsList.Add(-1, values.Where(entry => entry.Value < 0).Select(entry => entry.Key).ToList());
            idsList.Add(0, values.Where(entry => entry.Value == 0).Select(entry => entry.Key).ToList());
            idsList.Add(1, values.Where(entry => entry.Value > 0).Select(entry => entry.Key).ToList());

            foreach (var ids in idsList)
            {
                if (ids.Value.Count == 0) continue;

                ISimpleLineDecorationElement simpleLineDecorationElement = new SimpleLineDecorationElementClass();
                simpleLineDecorationElement.AddPosition(0.5);
                simpleLineDecorationElement.PositionAsRatio = true;

                ICartographicLineSymbol cartoGraphicLineSymbol = new CartographicLineSymbolClass();
                cartoGraphicLineSymbol.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Gray);
                cartoGraphicLineSymbol.Width = 1;
                ILineProperties lineProperties = cartoGraphicLineSymbol as ILineProperties;

                ILineDecoration lineDecoration = new LineDecorationClass();
                lineDecoration.AddElement(simpleLineDecorationElement);
                lineProperties.LineDecoration = lineDecoration;
                ISymbol sym = cartoGraphicLineSymbol as ISymbol;


                if (ids.Key == -1)
                    simpleLineDecorationElement.MarkerSymbol = ArcManager.MakeArrowSymbol(Color.Black, 8, 6, 180);
                else if (ids.Key == 1)
                    simpleLineDecorationElement.MarkerSymbol = ArcManager.MakeArrowSymbol(Color.Black, 8, 6, 0);
                else
                {
                    sym = new EAGL.Display.SimpleLineSymbol() { Color = Color.Gray }.EsriSymbol;
                }
                object key = ids.Value[0];
                List<object> refValues = new List<object>();
                refValues.AddRange(ids.Value.GetRange(1, ids.Value.Count - 1).ToArray());

                vrm.Add(key, sym, ids.Key == -1 ? "minus" : ids.Key == 0 ? string.Empty : "plus");
                vrm.AddReferanceValue2(key, refValues);
            }

            ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
            this.axMap.ActiveView.ContentsChanged();
        }
        #endregion 수리해석결과 유량 방향성 렌더링
    }
}
