// Copyright 2008 ESRI
// 
// All rights reserved under the copyright laws of the United States
// and applicable international laws, treaties, and conventions.
// 
// You may freely redistribute and use this sample code, with or
// without modification, provided you include the original copyright
// notice and use restrictions.
// 
// See use restrictions at <your ArcGIS install location>/developerkit/userestrictions.txt.
// 

using System;
using System.Windows.Forms;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;

namespace WaterNet.WaterAOCore
{

	public class frmSymbolSelector : System.Windows.Forms.Form
    {
		private System.ComponentModel.Container components = null;
        private SplitContainer splitContainer1;
        private AxSymbologyControl axSymbologyControl1;
        private Button cmdCancel;
        private Button cmdOK;
        private PictureBox pictureBox1;
        private GroupBox gbPoint;
        private Label label1;
        private Label label3;
        private Label label2;
        private Label label4;
        private NumericUpDown nudPointSize;
        private NumericUpDown nudPointAngle;
        private ColorDialog colorDialog1;
        private Panel pnPointColor;
        private GroupBox gbLine;
        private Panel pnLineColor;
        private NumericUpDown nudLineWidth;
        private Label label6;
        private Label label7;
        private GroupBox gbPolygon;
        private Panel pnPolygonColor2;
        private Label label9;
        private Panel pnPolygonColor;
        private NumericUpDown nudPolygonWidth;
        private Label label5;
        private Label label8;
		public IStyleGalleryItem m_styleGalleryItem;

        public frmSymbolSelector()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSymbolSelector));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.axSymbologyControl1 = new ESRI.ArcGIS.Controls.AxSymbologyControl();
            this.gbPolygon = new System.Windows.Forms.GroupBox();
            this.pnPolygonColor2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.pnPolygonColor = new System.Windows.Forms.Panel();
            this.nudPolygonWidth = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gbLine = new System.Windows.Forms.GroupBox();
            this.pnLineColor = new System.Windows.Forms.Panel();
            this.nudLineWidth = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gbPoint = new System.Windows.Forms.GroupBox();
            this.pnPointColor = new System.Windows.Forms.Panel();
            this.nudPointSize = new System.Windows.Forms.NumericUpDown();
            this.nudPointAngle = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axSymbologyControl1)).BeginInit();
            this.gbPolygon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPolygonWidth)).BeginInit();
            this.gbLine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLineWidth)).BeginInit();
            this.gbPoint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPointSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPointAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.axSymbologyControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gbPolygon);
            this.splitContainer1.Panel2.Controls.Add(this.gbLine);
            this.splitContainer1.Panel2.Controls.Add(this.gbPoint);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.cmdCancel);
            this.splitContainer1.Panel2.Controls.Add(this.cmdOK);
            this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer1.Size = new System.Drawing.Size(508, 486);
            this.splitContainer1.SplitterDistance = 319;
            this.splitContainer1.TabIndex = 4;
            // 
            // axSymbologyControl1
            // 
            this.axSymbologyControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axSymbologyControl1.Location = new System.Drawing.Point(0, 0);
            this.axSymbologyControl1.Name = "axSymbologyControl1";
            this.axSymbologyControl1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axSymbologyControl1.OcxState")));
            this.axSymbologyControl1.Size = new System.Drawing.Size(319, 486);
            this.axSymbologyControl1.TabIndex = 1;
            this.axSymbologyControl1.OnItemSelected += new ESRI.ArcGIS.Controls.ISymbologyControlEvents_Ax_OnItemSelectedEventHandler(this.axSymbologyControl1_OnItemSelected);
            // 
            // gbPolygon
            // 
            this.gbPolygon.Controls.Add(this.pnPolygonColor2);
            this.gbPolygon.Controls.Add(this.label9);
            this.gbPolygon.Controls.Add(this.pnPolygonColor);
            this.gbPolygon.Controls.Add(this.nudPolygonWidth);
            this.gbPolygon.Controls.Add(this.label5);
            this.gbPolygon.Controls.Add(this.label8);
            this.gbPolygon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbPolygon.Location = new System.Drawing.Point(11, 162);
            this.gbPolygon.Name = "gbPolygon";
            this.gbPolygon.Size = new System.Drawing.Size(166, 141);
            this.gbPolygon.TabIndex = 10;
            this.gbPolygon.TabStop = false;
            this.gbPolygon.Text = "옵션";
            // 
            // pnPolygonColor2
            // 
            this.pnPolygonColor2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnPolygonColor2.Location = new System.Drawing.Point(86, 110);
            this.pnPolygonColor2.Name = "pnPolygonColor2";
            this.pnPolygonColor2.Size = new System.Drawing.Size(61, 15);
            this.pnPolygonColor2.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 12);
            this.label9.TabIndex = 13;
            this.label9.Text = "외곽선 색상";
            // 
            // pnPolygonColor
            // 
            this.pnPolygonColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnPolygonColor.Location = new System.Drawing.Point(86, 33);
            this.pnPolygonColor.Name = "pnPolygonColor";
            this.pnPolygonColor.Size = new System.Drawing.Size(61, 15);
            this.pnPolygonColor.TabIndex = 12;
            this.pnPolygonColor.Paint += new System.Windows.Forms.PaintEventHandler(this.pnPolygonColor_Paint);
            this.pnPolygonColor.Click += new System.EventHandler(this.pnPolygonColor_Click);
            // 
            // nudPolygonWidth
            // 
            this.nudPolygonWidth.DecimalPlaces = 1;
            this.nudPolygonWidth.Location = new System.Drawing.Point(85, 69);
            this.nudPolygonWidth.Name = "nudPolygonWidth";
            this.nudPolygonWidth.Size = new System.Drawing.Size(64, 21);
            this.nudPolygonWidth.TabIndex = 11;
            this.nudPolygonWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "외곽선 넓이";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "색상";
            // 
            // gbLine
            // 
            this.gbLine.Controls.Add(this.pnLineColor);
            this.gbLine.Controls.Add(this.nudLineWidth);
            this.gbLine.Controls.Add(this.label6);
            this.gbLine.Controls.Add(this.label7);
            this.gbLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbLine.Location = new System.Drawing.Point(11, 162);
            this.gbLine.Name = "gbLine";
            this.gbLine.Size = new System.Drawing.Size(166, 104);
            this.gbLine.TabIndex = 8;
            this.gbLine.TabStop = false;
            this.gbLine.Text = "옵션";
            // 
            // pnLineColor
            // 
            this.pnLineColor.Location = new System.Drawing.Point(74, 33);
            this.pnLineColor.Name = "pnLineColor";
            this.pnLineColor.Size = new System.Drawing.Size(61, 15);
            this.pnLineColor.TabIndex = 12;
            // 
            // nudLineWidth
            // 
            this.nudLineWidth.Location = new System.Drawing.Point(73, 63);
            this.nudLineWidth.Name = "nudLineWidth";
            this.nudLineWidth.Size = new System.Drawing.Size(64, 21);
            this.nudLineWidth.TabIndex = 11;
            this.nudLineWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "넓이";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 7;
            this.label7.Text = "색상";
            // 
            // gbPoint
            // 
            this.gbPoint.Controls.Add(this.pnPointColor);
            this.gbPoint.Controls.Add(this.nudPointSize);
            this.gbPoint.Controls.Add(this.nudPointAngle);
            this.gbPoint.Controls.Add(this.label4);
            this.gbPoint.Controls.Add(this.label3);
            this.gbPoint.Controls.Add(this.label2);
            this.gbPoint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbPoint.Location = new System.Drawing.Point(11, 162);
            this.gbPoint.Name = "gbPoint";
            this.gbPoint.Size = new System.Drawing.Size(166, 134);
            this.gbPoint.TabIndex = 7;
            this.gbPoint.TabStop = false;
            this.gbPoint.Text = "옵션";
            // 
            // pnPointColor
            // 
            this.pnPointColor.Location = new System.Drawing.Point(74, 33);
            this.pnPointColor.Name = "pnPointColor";
            this.pnPointColor.Size = new System.Drawing.Size(61, 15);
            this.pnPointColor.TabIndex = 12;
            // 
            // nudPointSize
            // 
            this.nudPointSize.Location = new System.Drawing.Point(73, 63);
            this.nudPointSize.Name = "nudPointSize";
            this.nudPointSize.Size = new System.Drawing.Size(64, 21);
            this.nudPointSize.TabIndex = 11;
            this.nudPointSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudPointAngle
            // 
            this.nudPointAngle.Location = new System.Drawing.Point(73, 96);
            this.nudPointAngle.Name = "nudPointAngle";
            this.nudPointAngle.Size = new System.Drawing.Size(64, 21);
            this.nudPointAngle.TabIndex = 10;
            this.nudPointAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "각도";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "크기";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "색상";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "미리보기";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdCancel.Location = new System.Drawing.Point(98, 441);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(86, 40);
            this.cmdCancel.TabIndex = 5;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdOK.Location = new System.Drawing.Point(8, 440);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(86, 41);
            this.cmdOK.TabIndex = 4;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(11, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(167, 103);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // frmSymbolSelector
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
            this.ClientSize = new System.Drawing.Size(508, 486);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmSymbolSelector";
            this.Text = "SymbolForm";
            this.Load += new System.EventHandler(this.frmSymbol_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axSymbologyControl1)).EndInit();
            this.gbPolygon.ResumeLayout(false);
            this.gbPolygon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPolygonWidth)).EndInit();
            this.gbLine.ResumeLayout(false);
            this.gbLine.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLineWidth)).EndInit();
            this.gbPoint.ResumeLayout(false);
            this.gbPoint.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPointSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPointAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void frmSymbol_Load(object sender, System.EventArgs e)
		{
            pnLineColor.BorderStyle = BorderStyle.FixedSingle;
            pnPointColor.BorderStyle = BorderStyle.FixedSingle;
            pnPolygonColor.BorderStyle = BorderStyle.FixedSingle;
            pnPolygonColor.BorderStyle = BorderStyle.FixedSingle;

			//Get the ArcGIS install location
			string sInstall = ReadRegistry("SOFTWARE\\ESRI\\CoreRuntime");

			//Load the ESRI.ServerStyle file into the SymbologyControl
			axSymbologyControl1.LoadStyleFile(sInstall + "\\Styles\\ESRI.ServerStyle");


		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			m_styleGalleryItem = null;
			this.Hide();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			this.Hide();
		}

		private void axSymbologyControl1_OnItemSelected(object sender, ESRI.ArcGIS.Controls.ISymbologyControlEvents_OnItemSelectedEvent e)
		{
			//Preview the selected item
			m_styleGalleryItem = (IStyleGalleryItem) e.styleGalleryItem;
			PreviewImage();
		}

		private void PreviewImage()
		{
			//Get and set the style class 
			ISymbologyStyleClass symbologyStyleClass = axSymbologyControl1.GetStyleClass(axSymbologyControl1.StyleClass);

			//Preview an image of the symbol
			stdole.IPictureDisp picture = symbologyStyleClass.PreviewItem(m_styleGalleryItem, pictureBox1.Width, pictureBox1.Height);
			System.Drawing.Image image = System.Drawing.Image.FromHbitmap(new System.IntPtr(picture.Handle));
			pictureBox1.Image = image;
		}

		public IStyleGalleryItem GetItem(esriSymbologyStyleClass styleClass, ISymbol symbol)
		{
			m_styleGalleryItem = null;

			//Get and set the style class
			axSymbologyControl1.StyleClass = styleClass;
			ISymbologyStyleClass symbologyStyleClass = axSymbologyControl1.GetStyleClass(styleClass);


            switch (symbologyStyleClass.StyleClass)
            {
                case esriSymbologyStyleClass.esriStyleClassMarkerSymbols:   //Point
                    gbPoint.Visible = true; gbLine.Visible = false; gbPolygon.Visible = false;
                    break;
                case esriSymbologyStyleClass.esriStyleClassLineSymbols:     //Line
                    gbPoint.Visible = false; gbLine.Visible = true; gbPolygon.Visible = false;
                    break;
                case esriSymbologyStyleClass.esriStyleClassFillSymbols:     //Polygon
                    gbPoint.Visible = false; gbLine.Visible = false; gbPolygon.Visible = true;
                    break;
                default:
                    break;
            }
            
			//Create a new server style gallery item with its style set
			IStyleGalleryItem styleGalleryItem = new ServerStyleGalleryItem();
			styleGalleryItem.Item = symbol;
			styleGalleryItem.Name = "mySymbol";

			//Add the item to the style class and select it
			symbologyStyleClass.AddItem(styleGalleryItem, 0);

			symbologyStyleClass.SelectItem(0);

			//Show the modal form
			this.ShowDialog();



			return m_styleGalleryItem;
		}

		private string ReadRegistry(string sKey) 
		{
			//Open the subkey for reading
			Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sKey, true);
			if (rk == null) return ""; 
			// Get the data from a specified item in the key.
			return (string) rk.GetValue("InstallDir");
		}

        private void pnPolygonColor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnPolygonColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                pnPolygonColor.BackColor = colorDialog1.Color;            
            }
        }

        private void ShowColorDialog(Panel pPanel)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                pPanel.BackColor = colorDialog1.Color;
            }
        }
	}
}
