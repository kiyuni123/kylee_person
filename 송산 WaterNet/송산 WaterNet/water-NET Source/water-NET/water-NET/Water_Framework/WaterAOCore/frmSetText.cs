﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geometry;

namespace WaterNet.WaterAOCore
{
    public partial class frmSetText : Form
    {
        [DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

        private IGeoFeatureLayer m_pWorkLayer = null;
        private IAnnotateLayerProperties m_pALP = null;
        private ILabelEngineLayerProperties m_pLELP = null;
        private System.Collections.ArrayList m_pFields = new ArrayList();
        private ITextSymbol m_Symbol = null;

        public frmSetText()
        {
            InitializeComponent();
        }

        public void SelectSymbol(ILayer pLayer)
        {
            if (pLayer == null) return;
            IFeatureLayer featureLayer = pLayer as IFeatureLayer;
            if (featureLayer == null) return;
            IGeoFeatureLayer geoFeatureLayer = (IGeoFeatureLayer)featureLayer;
            
            m_pWorkLayer = (IGeoFeatureLayer)pLayer;
            IAnnotateLayerPropertiesCollection pALPC = m_pWorkLayer.AnnotationProperties;

            IElementCollection place = null;
            IElementCollection unplace = null;
            pALPC.QueryItem(0, out m_pALP, out place, out unplace);

            m_pLELP = m_pALP as ILabelEngineLayerProperties;

            SetScale();
            SetFields();
            SetSymbol();

            this.BringToFront();
            //this.ShowDialog();
            
        }

        private void SetScale()
        {
            checkBoxShow.Checked = m_pWorkLayer.DisplayAnnotation;
            txtmaxScale.Text = m_pALP.AnnotationMinimumScale.ToString();
            txtminScale.Text = m_pALP.AnnotationMaximumScale.ToString();

            checkBoxShow_CheckedChanged(checkBoxShow, new EventArgs());
        }

        private void SetFields()
        {
            cboFields.Items.Clear();

            ILayerFields pFields = m_pWorkLayer as ILayerFields;
            IField pField = null;
            for (int i = 0; i < pFields.FieldCount - 1; i++)
            {
                pField = pFields.get_Field(i);
                switch (pField.Type)
                {
                    case esriFieldType.esriFieldTypeBlob:
                    case esriFieldType.esriFieldTypeGUID:
                    case esriFieldType.esriFieldTypeGeometry:
                    case esriFieldType.esriFieldTypeGlobalID:
                    case esriFieldType.esriFieldTypeOID:
                    case esriFieldType.esriFieldTypeRaster:
                    case esriFieldType.esriFieldTypeXML:
                        break;
                    default:
                        cboFields.Items.Add(pField.AliasName);
                        m_pFields.Add(pField);
                        break;
                }
            }
            cboFields.Text = m_pWorkLayer.DisplayField;
        }

        private void SetSymbol()
        {
            m_Symbol = m_pLELP.Symbol;
            PreviewImage();
        }
  
        private void checkBoxShow_CheckedChanged(object sender, EventArgs e)
        {
            txtmaxScale.Enabled = ((CheckBox)sender).Checked;
            txtminScale.Enabled = ((CheckBox)sender).Checked;
            cboFields.Enabled = ((CheckBox)sender).Checked;
            btnConfig.Enabled = ((CheckBox)sender).Checked;
        }

        private void txtminScale_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void txtmaxScale_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            if (m_pWorkLayer == null) return;
            IFeatureLayer featureLayer = m_pWorkLayer as IFeatureLayer;
            if (featureLayer == null) return;
            IGeoFeatureLayer geoFeatureLayer = (IGeoFeatureLayer)featureLayer;
            IFeatureRenderer Renderer = geoFeatureLayer.Renderer;
            
            ISimpleRenderer pSimpleRenderer = Renderer as ISimpleRenderer;

            frmSymbolSelect SymSelector = new frmSymbolSelect();
            ISymbol pNewSym = SymSelector.SelectSymbol((ISymbol)m_Symbol);

            m_Symbol = pNewSym as ITextSymbol;

            PreviewImage();            
        }

        private void btnConform_Click(object sender, EventArgs e)
        {
            m_pWorkLayer.DisplayAnnotation = checkBoxShow.Checked;
            if (string.IsNullOrEmpty(txtmaxScale.Text)) txtmaxScale.Text = "0";
            if (string.IsNullOrEmpty(txtminScale.Text)) txtminScale.Text = "0";
            m_pALP.AnnotationMinimumScale = Convert.ToDouble(txtmaxScale.Text);
            m_pALP.AnnotationMaximumScale = Convert.ToDouble(txtminScale.Text);

            if (cboFields.SelectedIndex > -1)
            {
                IField pField = m_pFields[cboFields.SelectedIndex] as IField;

                if (pField == null)
                {
                    pField = m_pFields[0] as IField;
                }
                m_pLELP.Expression = "[" + pField.Name + "]";

                m_pLELP.Symbol = m_Symbol;
            }           


            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void PreviewImage()
        {
            picPreView.Image = ImageFromSymbol((ISymbol)m_Symbol, picPreView.Width, picPreView.Height);
        }

        private ITransformation CreateTransFromDC(IntPtr hDC, int Width, int Height)
        {
            IEnvelope pBounds = new EnvelopeClass();
            pBounds.PutCoords(0, 0, Width - 1, Height - 1);

            ESRI.ArcGIS.esriSystem.tagRECT deviceRect = new ESRI.ArcGIS.esriSystem.tagRECT();
            deviceRect.left = 0;
            deviceRect.top = 0;
            deviceRect.right = Width - 1;
            deviceRect.bottom = Height - 1;

            int dpi = GetDeviceCaps(hDC, 90);
            if (dpi == 0) return null;

            IDisplayTransformation pDT = new DisplayTransformationClass();
            pDT.VisibleBounds = pBounds;
            pDT.Bounds = pBounds;
            pDT.set_DeviceFrame(ref deviceRect);
            pDT.Resolution = dpi;

            return pDT;
        }

        private IGeometry CreateSymShape(ISymbol pSymbol, IEnvelope pEnvelope)
        {
            IGeometry pGeom = null;
            IArea pArea = pEnvelope as IArea;

            if (pSymbol is IMarkerSymbol)
            {
                pGeom = (IGeometry)pArea.Centroid;
            }
            else if (pSymbol is ILineSymbol || pSymbol is ITextSymbol)
            {
                IPolyline pPolyline = new PolylineClass();
                IPoint pPoint = new PointClass();
                ITransform2D pT2D;

                pPoint = pArea.Centroid;
                pT2D = pPoint as ITransform2D;
                pT2D.Move(-50, 0);
                pPolyline.FromPoint = pPoint;
                pT2D.Move(50, 0);
                pPolyline.ToPoint = pPoint;

                pGeom = (IGeometry)pPolyline;

            }
            else if (pSymbol is IFillSymbol)
            {
                IEnvelope pNewEnv = new EnvelopeClass();
                pNewEnv.PutCoords(pEnvelope.XMin + 20, pEnvelope.YMin + 20, pEnvelope.XMax - 20, pEnvelope.YMax - 20);

                pGeom = (IGeometry)pNewEnv;
            }

            return pGeom;

        }

        //심볼 미리보기 
        private Image ImageFromSymbol(ISymbol symbol, Int32 width, Int32 height)
        {
            Bitmap bitmap = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap);
            IntPtr hdc = g.GetHdc();
            try
            {
                IEnvelope pEnvelope = new EnvelopeClass();
                pEnvelope.PutCoords(0, 0, width - 1, height - 1);

                ITransformation pTransformation = CreateTransFromDC(hdc, width, height);
                if (pTransformation == null) return null;

                IGeometry pGeom = CreateSymShape(symbol, pEnvelope);

                if (pTransformation == null || pGeom == null) return null;

                symbol.SetupDC(hdc.ToInt32(), pTransformation);
                symbol.Draw(pGeom);
                symbol.ResetDC();

                return bitmap as Image;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                g.ReleaseHdc(hdc);
                g.Dispose();
            }
        }
    }
}
