﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;

namespace WaterNet.WaterAOCore
{
    public partial class frmSymbolSelect : Form
    {
        [DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

        private IStyleGalleryItem m_styleGalleryItem = null;
        private esriSymbologyStyleClass m_SymbolType;     //0:IMarkerSymbol, 1:ILineSymbol, 2:IFillSymbol , 3:TextSymbol
        private ISymbol m_Default = default(ISymbol);       //원본 심볼
        private ISymbol m_Symbol = default(ISymbol);        //작업 심볼
        private ISymbol m_Result = default(ISymbol);         //결과 심볼

        private IStyleGallery m_SG = null;
        private IStyleGalleryStorage m_SGS = null;
        private string m_StyleSet = "ESRI";
        private const string STYLE_EXT = ".ServerStyle";  //확장자


        public frmSymbolSelect()
        {
            InitializeComponent();

            InitializeSetting();
        }

        private void InitializeSetting()
        {
            gbText.Location = new System.Drawing.Point(11, 188); gbText.Visible = false;
            gbLine.Location = new System.Drawing.Point(11, 188); gbLine.Visible = false;
            gbPolygon.Location = new System.Drawing.Point(11, 188); gbPolygon.Visible = false;
            gbPoint.Location = new System.Drawing.Point(11, 188); gbPoint.Visible = false;

            pnLineColor.BorderStyle = BorderStyle.FixedSingle;
            pnPointColor.BorderStyle = BorderStyle.FixedSingle;
            pnPolygonColor.BorderStyle = BorderStyle.FixedSingle;
            pnPolygonColor2.BorderStyle = BorderStyle.FixedSingle;
            pnTextColor.BorderStyle = BorderStyle.FixedSingle;
        }

        private void frmSymbolSelect_Load(object sender, EventArgs e)
        {
            m_SG = new ServerStyleGalleryClass();
            m_SGS = (IStyleGalleryStorage)m_SG;

            LoadStyles();
            cboStyleSet.Text = m_StyleSet;

            this.BringToFront();
        }

        /// <summary>
        /// 콤보박스에 리스트
        /// </summary>
        private void LoadStyles()
        {
            string[] stylefile = System.IO.Directory.GetFiles(m_SGS.DefaultStylePath, "*" + STYLE_EXT);
            foreach (var item in stylefile)
            {
                cboStyleSet.Items.Add(System.IO.Path.GetFileNameWithoutExtension(item));
            }
        }

        private void frmSymbolSelect_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    break;
                case Keys.Escape:
                    break;
                default:
                    break;
            }
        }

        private void cboStyleSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_StyleSet = cboStyleSet.Text;

            string StyleSet = m_SGS.DefaultStylePath + m_StyleSet + STYLE_EXT;

            ShowStyleSet(StyleSet);
        }

        private int ClassIndex(string ClassName)
        {
            for (int i = 0; i < m_SG.ClassCount - 1; i++)
            {
                if (string.Compare(m_SG.get_Class(i).Name, ClassName) == 0)
                {
                    return i;
                }
            }
            return -1;
        }

        private void SetSymbolType()
        {
            if (m_Default is IMarkerSymbol)
            {
                m_SymbolType = esriSymbologyStyleClass.esriStyleClassMarkerSymbols;
                gbPoint.Visible = true; gbPoint.BringToFront();
            }
            else if (m_Default is ILineSymbol)
            {
                m_SymbolType = esriSymbologyStyleClass.esriStyleClassLineSymbols;
                gbLine.Visible = true; gbLine.BringToFront();
            }
            else if (m_Default is IFillSymbol)
            {
                m_SymbolType = esriSymbologyStyleClass.esriStyleClassFillSymbols;
                gbPolygon.Visible = true; gbPolygon.BringToFront();
            }
            else if (m_Default is ITextSymbol)
            {
                m_SymbolType = esriSymbologyStyleClass.esriStyleClassTextSymbols;
                gbText.Visible = true; gbText.BringToFront();
            }
        }

        private void ShowSymbol(ISymbol Symbol)
        {
            m_Symbol = Symbol;
            switch (m_SymbolType)
            {
                case esriSymbologyStyleClass.esriStyleClassFillSymbols:
                    IFillSymbol pFillSymbol = m_Symbol as IFillSymbol;
                    if (pFillSymbol.Color == null)
                    {
                        pnPolygonColor.Visible = false;
                    }
                    else
                    {
                        pnPolygonColor.BackColor = ESRI.ArcGIS.ADF.Converter.FromRGBColor((IRgbColor)ArcManager.GetColor(pFillSymbol.Color.RGB));
                        pnPolygonColor.Visible = true;
                    }
                    nudPolygonWidth.Value = (decimal)pFillSymbol.Outline.Width;
                    pnPolygonColor2.BackColor = ESRI.ArcGIS.ADF.Converter.FromRGBColor((IRgbColor)ArcManager.GetColor(pFillSymbol.Outline.Color.RGB));
                    break;
                case esriSymbologyStyleClass.esriStyleClassLineSymbols:
                    ILineSymbol pLineSymbol = m_Symbol as ILineSymbol;
                    pnLineColor.BackColor = ESRI.ArcGIS.ADF.Converter.FromRGBColor((IRgbColor)ArcManager.GetColor(pLineSymbol.Color.RGB));
                    nudLineWidth.Value = (decimal)pLineSymbol.Width;
                    break;
                case esriSymbologyStyleClass.esriStyleClassMarkerSymbols:
                    IMarkerSymbol pMarkerSymbol = m_Symbol as IMarkerSymbol;
                    if (pMarkerSymbol.Color == null)
                    {
                        pnPointColor.Visible = false;    
                    }
                    else
                    {
                        pnPointColor.BackColor = ESRI.ArcGIS.ADF.Converter.FromRGBColor((IRgbColor)ArcManager.GetColor(pMarkerSymbol.Color.RGB)); 
                        pnPointColor.Visible = true;
                    }
                    nudPointSize.Value = (decimal)pMarkerSymbol.Size;
                    nudPointAngle.Value = (decimal)pMarkerSymbol.Angle;
                    break;
                case esriSymbologyStyleClass.esriStyleClassTextSymbols:
                    ITextSymbol pTextSymbol = m_Symbol as ITextSymbol;
                    if (pTextSymbol.Color == null)
                    {
                        pnTextColor.Visible = false;
                    }
                    else
                    {
                        pnTextColor.BackColor = ESRI.ArcGIS.ADF.Converter.FromRGBColor((IRgbColor)ArcManager.GetColor(pTextSymbol.Color.RGB)); 
                        pnTextColor.Visible = true;
                    }
                    labelFontName.Text = pTextSymbol.Font.Name;
                    labelFontSize.Text = Convert.ToString(pTextSymbol.Font.Size);
                    break;
                default:
                    break;
            }

            PreviewImage();
        }

        #region Public
        public ISymbol SelectSymbol(ISymbol DefaultSymbol)
        {
            m_Default = DefaultSymbol;
            SetSymbolType();
            ShowSymbol(m_Default);

            m_Result = null;
            this.ShowDialog();

            return m_Result;
        }
        #endregion


        private void ShowStyleSet(string StyleSet)
        {
            m_styleGalleryItem = null;

            //Set the style class of SymbologyControl1
            axSymbologyControl1.LoadStyleFile(StyleSet);
            axSymbologyControl1.StyleClass = m_SymbolType;

            //Change cursor
            this.Cursor = Cursors.Default;

        }

        private void axSymbologyControl1_OnItemSelected(object sender, ISymbologyControlEvents_OnItemSelectedEvent e)
        {
            //Preview the selected item
			m_styleGalleryItem = (IStyleGalleryItem) e.styleGalleryItem;
            try
            {
                ISymbol pSymbol = (ISymbol)m_styleGalleryItem.Item;

                ShowSymbol(pSymbol);

                PreviewImage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

		}

		private void PreviewImage()
		{
            pictureBox1.Image = ImageFromSymbol(m_Symbol, pictureBox1.Width, pictureBox1.Height);
		}

        private ITransformation CreateTransFromDC(IntPtr hDC, int Width, int Height)
        {
            IEnvelope pBounds = new EnvelopeClass();
            pBounds.PutCoords(0, 0, Width - 1, Height - 1);

            ESRI.ArcGIS.esriSystem.tagRECT deviceRect = new ESRI.ArcGIS.esriSystem.tagRECT();
            deviceRect.left = 0;
            deviceRect.top = 0;
            deviceRect.right = Width - 1;
            deviceRect.bottom = Height - 1;

            int dpi = GetDeviceCaps(hDC, 90);
            if (dpi == 0) return null;

            IDisplayTransformation pDT = new DisplayTransformationClass();
            pDT.VisibleBounds = pBounds;
            pDT.Bounds = pBounds;
            pDT.set_DeviceFrame(ref deviceRect);
            pDT.Resolution = dpi;

            return pDT;
        }

        private IGeometry CreateSymShape(ISymbol pSymbol, IEnvelope pEnvelope)
        {
            IGeometry pGeom = null;
            IArea pArea = pEnvelope as IArea;

            if (pSymbol is IMarkerSymbol)
            {
                pGeom = (IGeometry)pArea.Centroid;
            }
            else if (pSymbol is ILineSymbol || pSymbol is ITextSymbol)
            {
                IPolyline pPolyline = new PolylineClass();
                IPoint pPoint = new PointClass();
                ITransform2D pT2D;

                pPoint = pArea.Centroid;
                pT2D = pPoint as ITransform2D;
                pT2D.Move(-50, 0);
                pPolyline.FromPoint = pPoint;
                pT2D.Move(100, 0);
                pPolyline.ToPoint = pPoint;

                pGeom = (IGeometry)pPolyline;

            }
            else if (pSymbol is IFillSymbol)
            {
                IEnvelope pNewEnv = new EnvelopeClass();
                pNewEnv.PutCoords(pEnvelope.XMin + 20, pEnvelope.YMin + 20, pEnvelope.XMax - 20, pEnvelope.YMax - 20);

                pGeom = (IGeometry)pNewEnv;
            }

            return pGeom;

        }



        //심볼 미리보기 
        private Image ImageFromSymbol(ISymbol symbol, Int32 width, Int32 height)
        {   
            Bitmap bitmap = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap);
            IntPtr hdc = g.GetHdc();
            try
            {
                IEnvelope pEnvelope = new EnvelopeClass();
                pEnvelope.PutCoords(0, 0, width - 1, height - 1);

                ITransformation pTransformation = CreateTransFromDC(hdc, width, height);
                if (pTransformation == null) return null;

                IGeometry pGeom = CreateSymShape(symbol, pEnvelope);

                if (pTransformation == null || pGeom == null) return null;

                symbol.SetupDC(hdc.ToInt32(), pTransformation);
                symbol.Draw(pGeom);
                symbol.ResetDC();

                return bitmap as Image;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            finally
            {   
                g.ReleaseHdc(hdc);
                g.Dispose();
            }
        }

        private void btnTextSymbol_Click(object sender, EventArgs e)
        {
            ITextSymbol pTextSymbol = (ITextSymbol)m_Symbol;
            
            FontDialog fontdialog = new FontDialog();
            if (fontdialog.ShowDialog() == DialogResult.OK)
            {
                stdole.StdFont pFont = new stdole.StdFontClass();

                pFont.Name = fontdialog.Font.Name;
                pFont.Size = (decimal)fontdialog.Font.Size;
                pTextSymbol.Font = (stdole.IFontDisp)pFont;

                labelFontName.Text = pTextSymbol.Font.Name;
                labelFontSize.Text = Convert.ToString(pTextSymbol.Font.Size);
            }

        }

        #region Button Click
        private void btnDefault_Click(object sender, EventArgs e)
        {
            ShowSymbol(m_Default);
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            m_Result = m_Symbol;
            this.Close();
        }
        #endregion

        #region ValueChanged
        private void nudPolygonWidth_ValueChanged(object sender, EventArgs e)
        {
            if (!(m_Symbol is IFillSymbol)) return;
            IFillSymbol pFillSymbol = m_Symbol as IFillSymbol;
            ILineSymbol pLineSymbol = pFillSymbol.Outline;
            pLineSymbol.Width = (double)nudPolygonWidth.Value;
            pFillSymbol.Outline = pLineSymbol;

            PreviewImage();
        }

        private void nudPointAngle_ValueChanged(object sender, EventArgs e)
        {
            if (!(m_Symbol is IMarkerSymbol)) return;
            IMarkerSymbol pMarkerSymbol = m_Symbol as IMarkerSymbol;
            pMarkerSymbol.Angle = (double)nudPointAngle.Value;

            PreviewImage();
        }

        private void nudPointSize_ValueChanged(object sender, EventArgs e)
        {
            if (!(m_Symbol is IMarkerSymbol)) return;
            IMarkerSymbol pMarkerSymbol = m_Symbol as IMarkerSymbol;
            pMarkerSymbol.Size = (double)nudPointSize.Value;

            PreviewImage();
        }

        private void nudLineWidth_ValueChanged(object sender, EventArgs e)
        {
            if (!(m_Symbol is ILineSymbol)) return;
            ILineSymbol pLineSymbol = m_Symbol as ILineSymbol;
            pLineSymbol.Width = (double)nudLineWidth.Value;

            PreviewImage();
        }
        #endregion


        #region 색상변경 - Click
        private void pnPolygonColor_Click(object sender, EventArgs e)
        {
            if (!(m_Symbol is IFillSymbol)) return;
            IColor pColor = GetiColor2Dialog(pnPolygonColor);
            if (pColor == null) return;

            IFillSymbol pFillSymbol = m_Symbol as IFillSymbol;
            pFillSymbol.Color = pColor;

            PreviewImage();
        }

        private void pnPolygonColor2_Click(object sender, EventArgs e)
        {
            if (!(m_Symbol is IFillSymbol)) return;
            
            IColor pColor = GetiColor2Dialog(pnPolygonColor2);
            if (pColor == null) return;

            IFillSymbol pFillSymbol = m_Symbol as IFillSymbol;
            ILineSymbol pLineSymbol = pFillSymbol.Outline;
            pLineSymbol.Color = pColor;
            pFillSymbol.Outline = pLineSymbol;

            PreviewImage();
        }

        private void pnLineColor_Click(object sender, EventArgs e)
        {
            IColor pColor = GetiColor2Dialog(pnLineColor);
            if (pColor == null) return;

            if (!(m_Symbol is ILineSymbol)) return;
            ILineSymbol pLineSymbol = m_Symbol as ILineSymbol;
            pLineSymbol.Color = pColor;

            PreviewImage();
        }

        private void pnPointColor_Click(object sender, EventArgs e)
        {
            IColor pColor = GetiColor2Dialog(pnPointColor);
            if (pColor == null) return;

            if (!(m_Symbol is IMarkerSymbol)) return;
            IMarkerSymbol pMarkerSymbol = m_Symbol as IMarkerSymbol;
            pMarkerSymbol.Color = pColor;

            PreviewImage();
        }

        private void pnTextColor_Click(object sender, EventArgs e)
        {
            IColor pColor = GetiColor2Dialog(pnTextColor);
            if (pColor == null) return;

            if (!(m_Symbol is ITextSymbol)) return;
            ITextSymbol pTextSymbol = m_Symbol as ITextSymbol;
            pTextSymbol.Color = pColor;

            PreviewImage();
        }
        #endregion

        /// <summary>
        /// ColorDialog -> IColor
        /// </summary>
        /// <param name="pControl">Panel</param>
        /// <returns></returns>
        private IRgbColor GetiColor2Dialog(Control pControl)
        {
            IRgbColor pColor = null;
            if (pControl is Panel)
            {
                ColorDialog pColorDialog = new ColorDialog();
                pColorDialog.Color = pControl.BackColor;
                if (pColorDialog.ShowDialog(this) == DialogResult.OK)
                {    
                    pControl.BackColor = pColorDialog.Color;
                    pColor = new RgbColorClass();

                    pColor = ESRI.ArcGIS.ADF.Converter.ToRGBColor(pColorDialog.Color);
                }
            }
            return pColor;
        }


    }
}
