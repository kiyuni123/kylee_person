﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Display;

namespace WaterNet.WaterAOCore
{
    public class CreateINPLayerManager
    {
        #region 멤버 변수 및 상수
        private string m_QuerySelectJUNCTION = string.Empty;
        private string m_QuerySelectRESERVOIR = string.Empty;
        private string m_QuerySelectTANK = string.Empty;
        private string m_QuerySelectPIPE = string.Empty;
        private string m_QuerySelectPUMP = string.Empty;
        private string m_QuerySelectVALVE = string.Empty;
        private string m_QuerySelectVertics = string.Empty;

        private IFeatureLayer m_JUNCTION = new FeatureLayerClass();
        private IFeatureLayer m_RESERVOIR = new FeatureLayerClass();
        private IFeatureLayer m_TANK = new FeatureLayerClass();
        private IFeatureLayer m_PIPE = new FeatureLayerClass();
        private IFeatureLayer m_PUMP = new FeatureLayerClass();
        private IFeatureLayer m_VALVE = new FeatureLayerClass();

        private IWorkspace m_Workspace = default(IWorkspace);
        private OracleDBManager m_oDBManager = null;
        private string m_INP_No = string.Empty;
        #endregion

        /// <summary>
        /// 기본생성자
        /// </summary>
        /// <param name="strInpNo"></param>
        public CreateINPLayerManager(string strInpNo)
        {
            m_oDBManager = new OracleDBManager();
            m_oDBManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
            m_oDBManager.Open();

            m_INP_No = strInpNo;
            InitializeSetting();

            string strWorkspace = VariableManager.m_INPgraphicRootDirectory + "\\" + strInpNo;
            m_Workspace = ArcManager.getShapeWorkspace(strWorkspace);
            if (m_Workspace != null) return;

            m_Workspace = ArcManager.CreateShapeWorkspace(VariableManager.m_INPgraphicRootDirectory, strInpNo);
        }
        /// <summary>
        /// 기본생성자
        /// </summary>
        public CreateINPLayerManager(OracleDBManager oDBManager, string strInpNo)
        {
            m_oDBManager = oDBManager;
            m_INP_No = strInpNo;
            InitializeSetting();

            string strWorkspace = VariableManager.m_INPgraphicRootDirectory + "\\" + strInpNo;
            m_Workspace = ArcManager.getShapeWorkspace(strWorkspace);
            if (m_Workspace != null)  return;

            m_Workspace = ArcManager.CreateShapeWorkspace(VariableManager.m_INPgraphicRootDirectory, strInpNo);
        }

        private void InitializeSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            #region SelectQuery String
            ///--------Junction
            oStringBuilder.Remove(0,oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    A.ID                 AS NODE_ID ");
            oStringBuilder.AppendLine("      ,   C.X                  AS X");
            oStringBuilder.AppendLine("      ,   C.Y                  AS Y");
            oStringBuilder.AppendLine("      ,   D.POSITION_INFO      AS POSITION_INFO");
            oStringBuilder.AppendLine("FROM    WH_JUNCTIONS           A");
            oStringBuilder.AppendLine("       ,WH_COORDINATES         C");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'NODE'                    ");
            oStringBuilder.AppendLine("         ) D                                       ");
            oStringBuilder.AppendLine("WHERE   A.ID    = C.ID ");
            oStringBuilder.AppendLine("  AND   A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   C.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   A.ID = D.ID(+) ");
            m_QuerySelectJUNCTION = oStringBuilder.ToString();

            ///--------RESERVOIR
            oStringBuilder.Remove(0,oStringBuilder.Length);
            
            oStringBuilder.AppendLine("SELECT    A.ID                 AS NODE_ID ");
            oStringBuilder.AppendLine("      ,   C.X                  AS X");
            oStringBuilder.AppendLine("      ,   C.Y                  AS Y");
            oStringBuilder.AppendLine("      ,   D.POSITION_INFO      AS POSITION_INFO");
            oStringBuilder.AppendLine("FROM    WH_RESERVOIRS           A");
            oStringBuilder.AppendLine("       ,WH_COORDINATES         C");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'NODE'                    ");
            oStringBuilder.AppendLine("         ) D                                       ");
            oStringBuilder.AppendLine("WHERE   A.ID    = C.ID ");
            oStringBuilder.AppendLine("  AND   A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   C.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   A.ID = D.ID(+) ");
            m_QuerySelectRESERVOIR = oStringBuilder.ToString();

            ///-------TANK
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    A.ID                 AS NODE_ID ");
            oStringBuilder.AppendLine("      ,   C.X                  AS X");
            oStringBuilder.AppendLine("      ,   C.Y                  AS Y");
            oStringBuilder.AppendLine("      ,   D.POSITION_INFO      AS POSITION_INFO");
            oStringBuilder.AppendLine("FROM    WH_TANK           A");
            oStringBuilder.AppendLine("       ,WH_COORDINATES         C");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'NODE'                    ");
            oStringBuilder.AppendLine("         ) D                                       ");
            oStringBuilder.AppendLine("WHERE   A.ID    = C.ID ");
            oStringBuilder.AppendLine("  AND   A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   C.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   A.ID = D.ID(+) ");
            m_QuerySelectTANK = oStringBuilder.ToString();
        
            ///-------PIPE
            oStringBuilder.Remove(0,oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   LINK_ID,");
            oStringBuilder.AppendLine("         ST_NODE,");
            oStringBuilder.AppendLine("         ED_NODE,");
            oStringBuilder.AppendLine("         POSITION_INFO");
            oStringBuilder.AppendLine("FROM");
            oStringBuilder.AppendLine("   (SELECT  A.ID          AS LINK_ID");
            oStringBuilder.AppendLine("          , (select x || ',' || y from WH_COORDINATES where INP_NUMBER = '" + m_INP_No + "' and id = a.node1)    AS ST_NODE");
            oStringBuilder.AppendLine("          , (select x || ',' || y from WH_COORDINATES where INP_NUMBER = '" + m_INP_No + "' and id = a.node2)    AS ED_NODE");
            oStringBuilder.AppendLine("          ,E.POSITION_INFO           AS POSITION_INFO");
            oStringBuilder.AppendLine("   FROM    WH_PIPES                A");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'LINK'                    ");
            oStringBuilder.AppendLine("         ) E                                       ");
            oStringBuilder.AppendLine("   WHERE  A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.ID = E.ID(+) ");
            oStringBuilder.AppendLine("   order by to_number(a.idx))"); 
            //oStringBuilder.AppendLine("SELECT A.ID AS LINK_ID                                                                                                ");
            //oStringBuilder.AppendLine("      ,C.X||','||C.Y ||'|'|| A.XY || D.X||','||D.Y VERTICS                                                            ");
            //oStringBuilder.AppendLine("      ,E.POSITION_INFO AS POSITION_INFO                                                                               ");
            //oStringBuilder.AppendLine("  FROM                                                                                                                ");
            //oStringBuilder.AppendLine("      (                                                                                                               ");
            //oStringBuilder.AppendLine("       SELECT A.INP_NUMBER                                                                                            ");
            //oStringBuilder.AppendLine("             ,A.ID                                                                                                    ");
            //oStringBuilder.AppendLine("             ,A.NODE1                                                                                                 ");
            //oStringBuilder.AppendLine("             ,A.NODE2                                                                                                 ");
            //oStringBuilder.AppendLine("             ,DECODE(XMLAGG(XMLELEMENT(\"XY\", B.X||','||B.Y, '|') ORDER BY TO_NUMBER(B.IDX) ASC).EXTRACT('//text()').GETSTRINGVAL(),  ");
            //oStringBuilder.AppendLine("                     ',|',                                                                                            ");
            //oStringBuilder.AppendLine("                     null,                                                                                            ");
            //oStringBuilder.AppendLine("                     XMLAGG(XMLELEMENT(\"XY\", B.X||','||B.Y, '|') ORDER BY TO_NUMBER(B.IDX) ASC).EXTRACT('//text()').GETSTRINGVAL()   ");
            //oStringBuilder.AppendLine("                    ) XY                                                                                              ");
            //oStringBuilder.AppendLine("         FROM (                                                                                                       ");
            //oStringBuilder.AppendLine("               SELECT INP_NUMBER                                                                                      ");
            //oStringBuilder.AppendLine("                     ,ID                                                                                              ");
            //oStringBuilder.AppendLine("                     ,NODE1                                                                                           ");
            //oStringBuilder.AppendLine("                     ,NODE2                                                                                           ");
            //oStringBuilder.AppendLine("                 FROM WH_PIPES                                                                                        ");
            //oStringBuilder.AppendLine("                WHERE INP_NUMBER = '" + m_INP_No + "'"                                                                 );
            //oStringBuilder.AppendLine("              ) A                                                                                                     ");
            //oStringBuilder.AppendLine("              ,WH_VERTICES B                                                                                          ");
            //oStringBuilder.AppendLine("         WHERE B.INP_NUMBER(+) = A.INP_NUMBER                                                                         ");
            //oStringBuilder.AppendLine("           AND B.ID(+) = A.ID                                                                                         ");
            //oStringBuilder.AppendLine("         GROUP BY A.INP_NUMBER, A.ID, A.NODE1, A.NODE2                                                                ");
            //oStringBuilder.AppendLine("       ) A                                                                                                            ");
            //oStringBuilder.AppendLine("       ,WH_COORDINATES C                                                                                              ");
            //oStringBuilder.AppendLine("       ,WH_COORDINATES D                                                                                              ");
            //oStringBuilder.AppendLine("       ,(                                                                                                             ");
            //oStringBuilder.AppendLine("        SELECT INP_NUMBER                                                                                             ");
            //oStringBuilder.AppendLine("              ,ID                                                                                                     ");
            //oStringBuilder.AppendLine("              ,POSITION_INFO                                                                                          ");
            //oStringBuilder.AppendLine("          FROM WH_TAGS                                                                                                ");
            //oStringBuilder.AppendLine("         WHERE TYPE = 'LINK'                                                                                          ");
            //oStringBuilder.AppendLine("        ) E                                                                                                           ");
            //oStringBuilder.AppendLine("  WHERE C.INP_NUMBER = A.INP_NUMBER                                                                                   ");
            //oStringBuilder.AppendLine("    AND C.ID = A.NODE1                                                                                                ");
            //oStringBuilder.AppendLine("    AND D.INP_NUMBER = A.INP_NUMBER                                                                                   ");
            //oStringBuilder.AppendLine("    AND D.ID = A.NODE2                                                                                                ");
            //oStringBuilder.AppendLine("    AND E.INP_NUMBER(+) = A.INP_NUMBER                                                                                ");
            //oStringBuilder.AppendLine("    AND E.ID(+) = A.ID                                                                                                ");
            m_QuerySelectPIPE = oStringBuilder.ToString();

            ///-------PUMP
            oStringBuilder.Remove(0,oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   LINK_ID,");
            oStringBuilder.AppendLine("         ST_NODE,");
            oStringBuilder.AppendLine("         ED_NODE,");
            oStringBuilder.AppendLine("         POSITION_INFO");
            oStringBuilder.AppendLine("FROM");
            oStringBuilder.AppendLine("   (SELECT  A.ID          AS LINK_ID");
            oStringBuilder.AppendLine("          , (select x || ',' || y from WH_COORDINATES where INP_NUMBER = '" + m_INP_No + "' and id = a.node1)    AS ST_NODE");
            oStringBuilder.AppendLine("          , (select x || ',' || y from WH_COORDINATES where INP_NUMBER = '" + m_INP_No + "' and id = a.node2)    AS ED_NODE");
            oStringBuilder.AppendLine("          ,E.POSITION_INFO           AS POSITION_INFO");
            oStringBuilder.AppendLine("   FROM    WH_PUMPS                A");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'LINK'                    ");
            oStringBuilder.AppendLine("         ) E                                       ");
            oStringBuilder.AppendLine("   WHERE  A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.ID = E.ID(+)) "); 

            //oStringBuilder.AppendLine("SELECT A.ID AS LINK_ID                                                                                                ");
            //oStringBuilder.AppendLine("      ,C.X||','||C.Y ||'|'|| A.XY || D.X||','||D.Y VERTICS                                                            ");
            //oStringBuilder.AppendLine("      ,E.POSITION_INFO AS POSITION_INFO                                                                               ");
            //oStringBuilder.AppendLine("  FROM                                                                                                                ");
            //oStringBuilder.AppendLine("      (                                                                                                               ");
            //oStringBuilder.AppendLine("       SELECT A.INP_NUMBER                                                                                            ");
            //oStringBuilder.AppendLine("             ,A.ID                                                                                                    ");
            //oStringBuilder.AppendLine("             ,A.NODE1                                                                                                 ");
            //oStringBuilder.AppendLine("             ,A.NODE2                                                                                                 ");
            //oStringBuilder.AppendLine("             ,DECODE(XMLAGG(XMLELEMENT(\"XY\", B.X||','||B.Y, '|') ORDER BY TO_NUMBER(B.IDX) ASC).EXTRACT('//text()').GETSTRINGVAL(),  ");
            //oStringBuilder.AppendLine("                     ',|',                                                                                            ");
            //oStringBuilder.AppendLine("                     null,                                                                                            ");
            //oStringBuilder.AppendLine("                     XMLAGG(XMLELEMENT(\"XY\", B.X||','||B.Y, '|') ORDER BY TO_NUMBER(B.IDX) ASC).EXTRACT('//text()').GETSTRINGVAL()   ");
            //oStringBuilder.AppendLine("                    ) XY                                                                                              ");
            //oStringBuilder.AppendLine("         FROM (                                                                                                       ");
            //oStringBuilder.AppendLine("               SELECT INP_NUMBER                                                                                      ");
            //oStringBuilder.AppendLine("                     ,ID                                                                                              ");
            //oStringBuilder.AppendLine("                     ,NODE1                                                                                           ");
            //oStringBuilder.AppendLine("                     ,NODE2                                                                                           ");
            //oStringBuilder.AppendLine("                 FROM WH_PUMPS                                                                                        ");
            //oStringBuilder.AppendLine("                WHERE INP_NUMBER = '" + m_INP_No + "'");
            //oStringBuilder.AppendLine("              ) A                                                                                                     ");
            //oStringBuilder.AppendLine("              ,WH_VERTICES B                                                                                          ");
            //oStringBuilder.AppendLine("         WHERE B.INP_NUMBER(+) = A.INP_NUMBER                                                                         ");
            //oStringBuilder.AppendLine("           AND B.ID(+) = A.ID                                                                                         ");
            //oStringBuilder.AppendLine("         GROUP BY A.INP_NUMBER, A.ID, A.NODE1, A.NODE2                                                                ");
            //oStringBuilder.AppendLine("       ) A                                                                                                            ");
            //oStringBuilder.AppendLine("       ,WH_COORDINATES C                                                                                              ");
            //oStringBuilder.AppendLine("       ,WH_COORDINATES D                                                                                              ");
            //oStringBuilder.AppendLine("       ,(                                                                                                             ");
            //oStringBuilder.AppendLine("        SELECT INP_NUMBER                                                                                             ");
            //oStringBuilder.AppendLine("              ,ID                                                                                                     ");
            //oStringBuilder.AppendLine("              ,POSITION_INFO                                                                                          ");
            //oStringBuilder.AppendLine("          FROM WH_TAGS                                                                                                ");
            //oStringBuilder.AppendLine("         WHERE TYPE = 'LINK'                                                                                          ");
            //oStringBuilder.AppendLine("        ) E                                                                                                           ");
            //oStringBuilder.AppendLine("  WHERE C.INP_NUMBER = A.INP_NUMBER                                                                                   ");
            //oStringBuilder.AppendLine("    AND C.ID = A.NODE1                                                                                                ");
            //oStringBuilder.AppendLine("    AND D.INP_NUMBER = A.INP_NUMBER                                                                                   ");
            //oStringBuilder.AppendLine("    AND D.ID = A.NODE2                                                                                                ");
            //oStringBuilder.AppendLine("    AND E.INP_NUMBER(+) = A.INP_NUMBER                                                                                ");
            //oStringBuilder.AppendLine("    AND E.ID(+) = A.ID                                                                                                ");           
            m_QuerySelectPUMP = oStringBuilder.ToString();

            ///-------VALVE
            oStringBuilder.Remove(0,oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   LINK_ID,");
            oStringBuilder.AppendLine("         ST_NODE,");
            oStringBuilder.AppendLine("         ED_NODE,");
            oStringBuilder.AppendLine("         POSITION_INFO");
            oStringBuilder.AppendLine("FROM");
            oStringBuilder.AppendLine("   (SELECT  A.ID          AS LINK_ID");
            oStringBuilder.AppendLine("          , (select x || ',' || y from WH_COORDINATES where INP_NUMBER = '" + m_INP_No + "' and id = a.node1)    AS ST_NODE");
            oStringBuilder.AppendLine("          , (select x || ',' || y from WH_COORDINATES where INP_NUMBER = '" + m_INP_No + "' and id = a.node2)    AS ED_NODE");
            oStringBuilder.AppendLine("          ,E.POSITION_INFO           AS POSITION_INFO");
            oStringBuilder.AppendLine("   FROM    WH_VALVES                A");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'LINK'                    ");
            oStringBuilder.AppendLine("         ) E                                       ");
            oStringBuilder.AppendLine("   WHERE  A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.ID = E.ID(+)) "); 


            //oStringBuilder.AppendLine("SELECT A.ID AS LINK_ID                                                                                                ");
            //oStringBuilder.AppendLine("      ,C.X||','||C.Y ||'|'|| A.XY || D.X||','||D.Y VERTICS                                                            ");
            //oStringBuilder.AppendLine("      ,E.POSITION_INFO AS POSITION_INFO                                                                               ");
            //oStringBuilder.AppendLine("  FROM                                                                                                                ");
            //oStringBuilder.AppendLine("      (                                                                                                               ");
            //oStringBuilder.AppendLine("       SELECT A.INP_NUMBER                                                                                            ");
            //oStringBuilder.AppendLine("             ,A.ID                                                                                                    ");
            //oStringBuilder.AppendLine("             ,A.NODE1                                                                                                 ");
            //oStringBuilder.AppendLine("             ,A.NODE2                                                                                                 ");
            //oStringBuilder.AppendLine("             ,DECODE(XMLAGG(XMLELEMENT(\"XY\", B.X||','||B.Y, '|') ORDER BY TO_NUMBER(B.IDX) ASC).EXTRACT('//text()').GETSTRINGVAL(),  ");
            //oStringBuilder.AppendLine("                     ',|',                                                                                            ");
            //oStringBuilder.AppendLine("                     null,                                                                                            ");
            //oStringBuilder.AppendLine("                     XMLAGG(XMLELEMENT(\"XY\", B.X||','||B.Y, '|') ORDER BY TO_NUMBER(B.IDX) ASC).EXTRACT('//text()').GETSTRINGVAL()   ");
            //oStringBuilder.AppendLine("                    ) XY                                                                                              ");
            //oStringBuilder.AppendLine("         FROM (                                                                                                       ");
            //oStringBuilder.AppendLine("               SELECT INP_NUMBER                                                                                      ");
            //oStringBuilder.AppendLine("                     ,ID                                                                                              ");
            //oStringBuilder.AppendLine("                     ,NODE1                                                                                           ");
            //oStringBuilder.AppendLine("                     ,NODE2                                                                                           ");
            //oStringBuilder.AppendLine("                 FROM WH_VALVES                                                                                        ");
            //oStringBuilder.AppendLine("                WHERE INP_NUMBER = '" + m_INP_No + "'");
            //oStringBuilder.AppendLine("              ) A                                                                                                     ");
            //oStringBuilder.AppendLine("              ,WH_VERTICES B                                                                                          ");
            //oStringBuilder.AppendLine("         WHERE B.INP_NUMBER(+) = A.INP_NUMBER                                                                         ");
            //oStringBuilder.AppendLine("           AND B.ID(+) = A.ID                                                                                         ");
            //oStringBuilder.AppendLine("         GROUP BY A.INP_NUMBER, A.ID, A.NODE1, A.NODE2                                                                ");
            //oStringBuilder.AppendLine("       ) A                                                                                                            ");
            //oStringBuilder.AppendLine("       ,WH_COORDINATES C                                                                                              ");
            //oStringBuilder.AppendLine("       ,WH_COORDINATES D                                                                                              ");
            //oStringBuilder.AppendLine("       ,(                                                                                                             ");
            //oStringBuilder.AppendLine("        SELECT INP_NUMBER                                                                                             ");
            //oStringBuilder.AppendLine("              ,ID                                                                                                     ");
            //oStringBuilder.AppendLine("              ,POSITION_INFO                                                                                          ");
            //oStringBuilder.AppendLine("          FROM WH_TAGS                                                                                                ");
            //oStringBuilder.AppendLine("         WHERE TYPE = 'LINK'                                                                                          ");
            //oStringBuilder.AppendLine("        ) E                                                                                                           ");
            //oStringBuilder.AppendLine("  WHERE C.INP_NUMBER = A.INP_NUMBER                                                                                   ");
            //oStringBuilder.AppendLine("    AND C.ID = A.NODE1                                                                                                ");
            //oStringBuilder.AppendLine("    AND D.INP_NUMBER = A.INP_NUMBER                                                                                   ");
            //oStringBuilder.AppendLine("    AND D.ID = A.NODE2                                                                                                ");
            //oStringBuilder.AppendLine("    AND E.INP_NUMBER(+) = A.INP_NUMBER                                                                                ");
            //oStringBuilder.AppendLine("    AND E.ID(+) = A.ID                                                                                                ");       

            m_QuerySelectVALVE = oStringBuilder.ToString();

            ///-------Vertics
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT  ID, IDX, X, Y");
            oStringBuilder.AppendLine("FROM    WH_VERTICES");
            oStringBuilder.AppendLine("WHERE INP_NUMBER = '" + m_INP_No + "'");
            m_QuerySelectVertics = oStringBuilder.ToString();
            #endregion

        }

        /// <summary>
        /// Workspace
        /// </summary>
        public IWorkspace Worksapce
        {
            get
            {
                return this.m_Workspace;
            }
        }

        /// <summary>
        /// Shape 생성
        /// </summary>
        public void CreateINP_Shape()
        {
            CreateINP_Shape(FunctionManager.INPShapeType.JUNCTIONS);
            CreateINP_Shape(FunctionManager.INPShapeType.PIPES);
            CreateINP_Shape(FunctionManager.INPShapeType.PUMPS);
            CreateINP_Shape(FunctionManager.INPShapeType.RESERVOIRS);
            CreateINP_Shape(FunctionManager.INPShapeType.TANKS);
            CreateINP_Shape(FunctionManager.INPShapeType.VALVES);

            CreateINP_Point_Feature(m_JUNCTION);
            CreateINP_Point_Feature(m_RESERVOIR);
            CreateINP_Point_Feature(m_TANK);
            CreateINP_Line_Feature(m_PIPE);
            CreateINP_Line_Feature(m_PUMP);
            CreateINP_Line_Feature(m_VALVE);

        }

        private void CreateINP_Line_Feature(IFeatureLayer pFeatureLayer)
        {

            System.Data.DataTable table = null;
            try
            {
                IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;
                if (pFeatureClass.AliasName.ToUpper().Equals("PIPE"))
                {
                    if (string.IsNullOrEmpty(m_QuerySelectPIPE)) return;
                    table = m_oDBManager.ExecuteScriptDataTable(m_QuerySelectPIPE, null);
                }
                else if (pFeatureClass.AliasName.ToUpper().Equals("PUMP"))
                {
                    if (string.IsNullOrEmpty(m_QuerySelectPUMP)) return;
                    table = m_oDBManager.ExecuteScriptDataTable(m_QuerySelectPUMP, null);
                }
                else if (pFeatureClass.AliasName.ToUpper().Equals("VALVE"))
                {
                    if (string.IsNullOrEmpty(m_QuerySelectVALVE)) return;
                    table = m_oDBManager.ExecuteScriptDataTable(m_QuerySelectVALVE, null);
                }
                if (table == null) return;
                if (table.Rows.Count == 0) return;

                //Buffer 생성
                IFeatureBuffer pFeatureBuffer = pFeatureClass.CreateFeatureBuffer();
                IFeatureCursor pFeatureCursor = pFeatureClass.Insert(true);
                IFeature pFeature = pFeatureBuffer as IFeature;

                try
                {
                    foreach (System.Data.DataRow row in table.Rows)
                    {
                        string strvertics = getVerticsPositions(row["LINK_ID"].ToString());
                        string strStNode = row["ST_NODE"].ToString();
                        string strEdNode = row["ED_NODE"].ToString();
                        string strPositions = string.Empty;

                        if (strvertics.Length == 0) strPositions = strStNode + "|" + strEdNode;
                        else strPositions = strStNode + "|" + strvertics + "|" + strEdNode;

                        IPointCollection pPointCollection = ArcManager.getPointCollection(strPositions);

                        IGeometry pGeom = pPointCollection as IGeometry;
                        if (pGeom.IsEmpty) continue;

                        ArcManager.SetValue(pFeature, "ID", row["LINK_ID"].ToString());
                        ArcManager.SetValue(pFeature, "POSITION", row["POSITION_INFO"].ToString());

                        pFeature.Shape = pGeom;

                        pFeatureCursor.InsertFeature(pFeatureBuffer);                        
                    }

                    pFeatureCursor.Flush();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            catch (Exception oException)
            {
                Console.WriteLine(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.StackTrace); //oException.InnerException.Message);
            }
        }

        private string getVerticsPositions(string sID)
        {
            System.Data.DataTable table = null;
            string sWhere = m_QuerySelectVertics + " AND ID = '" + sID + "'" + " ORDER BY TO_NUMBER(IDX)";
            StringBuilder oStringBuilder = new StringBuilder();
            try
            {
                table = m_oDBManager.ExecuteScriptDataTable(sWhere, null);

                if (table == null) return string.Empty;
                if (table.Rows.Count == 0) return string.Empty;

                foreach (System.Data.DataRow row in table.Rows)
                {
                    oStringBuilder.Append(row["X"].ToString() + "," + row["Y"].ToString() + "|");
                }

                oStringBuilder.Remove(oStringBuilder.Length - 1, 1);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return oStringBuilder.ToString();
        }

        private void CreateINP_Point_Feature(IFeatureLayer pFeatureLayer)
        {
            System.Data.DataTable table = null;
            try
            {
                IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;
                if (pFeatureClass.AliasName.ToUpper().Equals("JUNCTION"))
                {

                    if (string.IsNullOrEmpty(m_QuerySelectJUNCTION)) return;
                    table = m_oDBManager.ExecuteScriptDataTable(m_QuerySelectJUNCTION, null);
                }
                else if (pFeatureClass.AliasName.ToUpper().Equals("RESERVOIR"))
                {
                    if (string.IsNullOrEmpty(m_QuerySelectRESERVOIR)) return;
                    table = m_oDBManager.ExecuteScriptDataTable(m_QuerySelectRESERVOIR, null);
                }
                else if (pFeatureClass.AliasName.ToUpper().Equals("TANK"))
                {
                    if (string.IsNullOrEmpty(m_QuerySelectTANK)) return;
                    table = m_oDBManager.ExecuteScriptDataTable(m_QuerySelectTANK, null);
                }

                if (table == null) return;
                if (table.Rows.Count == 0) return;

                IFeatureBuffer pFeatureBuffer = pFeatureClass.CreateFeatureBuffer();
                IFeatureCursor pFeatureCursor = pFeatureClass.Insert(true);
                IFeature pFeature = pFeatureBuffer as IFeature;

                try
                {
                    foreach (System.Data.DataRow row in table.Rows)
                    {
                        IPoint point = new PointClass();
                        point.PutCoords(Convert.ToDouble(row["X"]), Convert.ToDouble(row["Y"]));

                        IGeometry pGeom = point as IGeometry;
                        if (pGeom.IsEmpty) continue;

                        ArcManager.SetValue(pFeature, "ID", row["NODE_ID"].ToString());
                        ArcManager.SetValue(pFeature, "POSITION", row["POSITION_INFO"].ToString());
                        pFeature.Shape = pGeom;

                        pFeatureCursor.InsertFeature(pFeatureBuffer);                        
                    }
                    pFeatureCursor.Flush();
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                    Console.WriteLine(ex.Message);
                }

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.StackTrace); //oException.InnerException.Message);
            }
        }

        private void CreateINP_Shape(FunctionManager.INPShapeType ShapeType)
        {
            IFeatureClass pFeatureClass = null;
            switch (ShapeType)
            {
                case FunctionManager.INPShapeType.JUNCTIONS:
                    pFeatureClass = CreateShape_Point("JUNCTION");
                    if (pFeatureClass != null)  m_JUNCTION.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.RESERVOIRS:
                    pFeatureClass = CreateShape_Point("RESERVOIR");
                    if (pFeatureClass != null) m_RESERVOIR.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.TANKS:
                    pFeatureClass = CreateShape_Point("TANK");
                    if (pFeatureClass != null) m_TANK.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.PIPES:
                    pFeatureClass = CreateShape_Line("PIPE");
                    if (pFeatureClass != null) m_PIPE.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.PUMPS:
                    pFeatureClass = CreateShape_Line("PUMP");
                    if (pFeatureClass != null) m_PUMP.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.VALVES:
                    pFeatureClass = CreateShape_Line("VALVE");
                    if (pFeatureClass != null) m_VALVE.FeatureClass = pFeatureClass;
                    break;
            }
        }

        private IFeatureClass CreateShape_Point(string sName)
        {
            //Shape Field 구성
            WaterNet.WaterAOCore.VariableManager.FieldStruct[] structFields = new WaterNet.WaterAOCore.VariableManager.FieldStruct[2];
            structFields[0].sFieldName = "ID";
            structFields[1].sFieldName = "POSITION";
            //------------------------------------------------
            structFields[0].sAliasName = "ID";
            structFields[1].sAliasName = "POSITION";
            //-------------------------------------------------------
            structFields[0].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;
            structFields[1].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;
            //---------------------------------------------
            structFields[0].sFieldLength = 50;
            structFields[1].sFieldLength = 500;
            //--------------------------------------------------

            IFields pFields = WaterNet.WaterAOCore.ArcManager.CreateFields(WaterNet.WaterAOCore.VariableManager.ShapeType.ShapePoint, structFields);

            //좌표계 구성 --------axMap.spatialReference
            ISpatialReference spatialReference = WaterNet.WaterAOCore.ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");

            IFeatureClass pFeatureClass = WaterNet.WaterAOCore.ArcManager.CreateFeatureClass(m_Workspace, sName, spatialReference, esriFeatureType.esriFTSimple,
                                                                                                     esriGeometryType.esriGeometryPoint, pFields, null, null, "");

            return pFeatureClass;
        }

        private IFeatureClass CreateShape_Line(string sName)
        {
            //Shape Field 구성
            WaterNet.WaterAOCore.VariableManager.FieldStruct[] structFields = new WaterNet.WaterAOCore.VariableManager.FieldStruct[2];
            structFields[0].sFieldName = "ID";
            structFields[1].sFieldName = "POSITION";
            //------------------------------------------------
            structFields[0].sAliasName = "ID";
            structFields[1].sAliasName = "POSITION";
            //-------------------------------------------------------
            structFields[0].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;
            structFields[1].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;
            //---------------------------------------------
            structFields[0].sFieldLength = 50;
            structFields[1].sFieldLength = 500;
            //--------------------------------------------------

            IFields pFields = WaterNet.WaterAOCore.ArcManager.CreateFields(WaterNet.WaterAOCore.VariableManager.ShapeType.ShapePolyline, structFields);

            //좌표계 구성 --------axMap.spatialReference
            ISpatialReference spatialReference = WaterNet.WaterAOCore.ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");

            IFeatureClass pFeatureClass = WaterNet.WaterAOCore.ArcManager.CreateFeatureClass(m_Workspace, sName, spatialReference, esriFeatureType.esriFTSimple, esriGeometryType.esriGeometryPolyline, pFields, null, null, "");

            return pFeatureClass;
        }

        //private IFeatureClass CreateShape_Point(string sName)
        //{
        //    IFeatureClass pFeatureClass = null;

        //    //Shape Field 구성
        //    WaterNet.WaterAOCore.VariableManager.FieldStruct[] structFields = new WaterNet.WaterAOCore.VariableManager.FieldStruct[26];
        //    structFields[0].sFieldName = "ID";
        //    structFields[1].sFieldName = "EN_ELEVATION";
        //    structFields[2].sFieldName = "EN_BASEDEMAND";
        //    structFields[3].sFieldName = "EN_PATTERN";
        //    structFields[4].sFieldName = "EN_EMITTER";
        //    structFields[5].sFieldName = "EN_INITQUAL";
        //    structFields[6].sFieldName = "EN_SOURCEQUAL";
        //    structFields[7].sFieldName = "EN_SOURCEPAT";
        //    structFields[8].sFieldName = "EN_SOURCETYPE";
        //    structFields[9].sFieldName = "EN_TANKLEVEL";
        //    structFields[10].sFieldName = "EN_DEMAND";
        //    structFields[11].sFieldName = "EN_HEAD";
        //    structFields[12].sFieldName = "EN_PRESSURE";
        //    structFields[13].sFieldName = "EN_QUALITY";
        //    structFields[14].sFieldName = "EN_SOURCEMASS";
        //    structFields[15].sFieldName = "EN_INITVOLUME";
        //    structFields[16].sFieldName = "EN_MIXMODEL";
        //    structFields[17].sFieldName = "EN_MIXZONEVOL";
        //    structFields[18].sFieldName = "EN_TANKDIAM";
        //    structFields[19].sFieldName = "EN_MINVOLUME";
        //    structFields[20].sFieldName = "EN_VOLCURV";
        //    structFields[21].sFieldName = "EN_MINLEVEL";
        //    structFields[22].sFieldName = "EN_MAXLEVEL";
        //    structFields[23].sFieldName = "EN_MIXFRACTION";
        //    structFields[24].sFieldName = "EN_TANK_KBULK";
        //    structFields[25].sFieldName = "POSITION";
        //    //------------------------------------------------
        //    structFields[0].sAliasName = "ID";
        //    structFields[1].sAliasName = "EN_ELEVATION";
        //    structFields[2].sAliasName = "EN_BASEDEMAND";
        //    structFields[3].sAliasName = "EN_PATTERN";
        //    structFields[4].sAliasName = "EN_EMITTER";
        //    structFields[5].sAliasName = "EN_INITQUAL";
        //    structFields[6].sAliasName = "EN_SOURCEQUAL";
        //    structFields[7].sAliasName = "EN_SOURCEPAT";
        //    structFields[8].sAliasName = "EN_SOURCETYPE";
        //    structFields[9].sAliasName = "EN_TANKLEVEL";
        //    structFields[10].sAliasName = "EN_DEMAND";
        //    structFields[11].sAliasName = "EN_HEAD";
        //    structFields[12].sAliasName = "EN_PRESSURE";
        //    structFields[13].sAliasName = "EN_QUALITY";
        //    structFields[14].sAliasName = "EN_SOURCEMASS";
        //    structFields[15].sAliasName = "EN_INITVOLUME";
        //    structFields[16].sAliasName = "EN_MIXMODEL";
        //    structFields[17].sAliasName = "EN_MIXZONEVOL";
        //    structFields[18].sAliasName = "EN_TANKDIAM";
        //    structFields[19].sAliasName = "EN_MINVOLUME";
        //    structFields[20].sAliasName = "EN_VOLCURV";
        //    structFields[21].sAliasName = "EN_MINLEVEL";
        //    structFields[22].sAliasName = "EN_MAXLEVEL";
        //    structFields[23].sAliasName = "EN_MIXFRACTION";
        //    structFields[24].sAliasName = "EN_TANK_KBULK";
        //    structFields[25].sAliasName = "POSITION";
        //    //-------------------------------------------------------
        //    structFields[0].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;
        //    structFields[1].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[2].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[3].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[4].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[5].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[6].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[7].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[8].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[9].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[10].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[11].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[12].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[13].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[14].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[15].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[16].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[17].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[18].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[19].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[20].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[21].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[22].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[23].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[24].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[25].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;

        //    //---------------------------------------------
        //    structFields[0].sFieldLength = 100;
        //    structFields[1].sFieldLength = 32;
        //    structFields[1].sScale = 2;
        //    structFields[2].sFieldLength = 32;
        //    structFields[2].sScale = 2;
        //    structFields[3].sFieldLength = 32;
        //    structFields[3].sScale = 2;
        //    structFields[4].sFieldLength = 32;
        //    structFields[4].sScale = 2;
        //    structFields[5].sFieldLength = 32;
        //    structFields[5].sScale = 2;
        //    structFields[6].sFieldLength = 32;
        //    structFields[6].sScale = 2;
        //    structFields[7].sFieldLength = 32;
        //    structFields[7].sScale = 2;
        //    structFields[8].sFieldLength = 32;
        //    structFields[8].sScale = 2;
        //    structFields[9].sFieldLength = 32;
        //    structFields[9].sScale = 2;
        //    structFields[10].sFieldLength = 32;
        //    structFields[10].sScale = 2;
        //    structFields[11].sFieldLength = 32;
        //    structFields[11].sScale = 2;
        //    structFields[12].sFieldLength = 32;
        //    structFields[12].sScale = 2;
        //    structFields[13].sFieldLength = 32;
        //    structFields[13].sScale = 2;
        //    structFields[14].sFieldLength = 32;
        //    structFields[14].sScale = 2;
        //    structFields[15].sFieldLength = 32;
        //    structFields[15].sScale = 2;
        //    structFields[16].sFieldLength = 32;
        //    structFields[16].sScale = 2;
        //    structFields[17].sFieldLength = 32;
        //    structFields[17].sScale = 2;
        //    structFields[18].sFieldLength = 32;
        //    structFields[18].sScale = 2;
        //    structFields[19].sFieldLength = 32;
        //    structFields[19].sScale = 2;
        //    structFields[20].sFieldLength = 32;
        //    structFields[20].sScale = 2;
        //    structFields[21].sFieldLength = 32;
        //    structFields[21].sScale = 2;
        //    structFields[22].sFieldLength = 32;
        //    structFields[22].sScale = 2;
        //    structFields[23].sFieldLength = 32;
        //    structFields[23].sScale = 2;
        //    structFields[24].sFieldLength = 32;
        //    structFields[24].sScale = 2;
        //    structFields[25].sFieldLength = 500;
        //    //--------------------------------------------------


        //    IFields pFields = WaterNet.WaterAOCore.ArcManager.CreateFields(WaterNet.WaterAOCore.VariableManager.ShapeType.ShapePoint, structFields);

        //    //좌표계 구성 --------axMap.spatialReference
        //    ISpatialReference spatialReference = WaterNet.WaterAOCore.ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");

        //    //Shape FeatreClass생성
        //    //if (WaterNet.WaterAOCore.ArcManager.getShapeFeatureClass(pWorkspace, sName) != null)
        //    //{
        //    //    MessageManager.ShowInformationMessage(pWorkspace.PathName + " 경로에 이미 [" + sName + "] 레이어가 있습니다.");
        //    //    return null;
        //    //}

        //    pFeatureClass = WaterNet.WaterAOCore.ArcManager.CreateFeatureClass(m_Workspace, sName, spatialReference, esriFeatureType.esriFTSimple,
        //                                                                                             esriGeometryType.esriGeometryPoint, pFields, null, null, "");

        //    return pFeatureClass;
        //}

        //private IFeatureClass CreateShape_Line(string sName)
        //{
        //    IFeatureClass pFeatureClass = null;

        //    //Shape Field 구성
        //    WaterNet.WaterAOCore.VariableManager.FieldStruct[] structFields = new WaterNet.WaterAOCore.VariableManager.FieldStruct[16];
        //    structFields[0].sFieldName = "ID";
        //    structFields[1].sFieldName =  "EN_DIAMETER";
        //    structFields[2].sFieldName =  "EN_LENGTH";
        //    structFields[3].sFieldName =  "EN_ROUGHNESS";
        //    structFields[4].sFieldName =  "EN_MINORLOSS";
        //    structFields[5].sFieldName =  "EN_INITSTATUS";
        //    structFields[6].sFieldName =  "EN_INITSETTING";
        //    structFields[7].sFieldName =  "EN_KBULK";
        //    structFields[8].sFieldName =  "EN_KWALL";
        //    structFields[9].sFieldName =  "EN_FLOW";
        //    structFields[10].sFieldName = "EN_VELOCITY";
        //    structFields[11].sFieldName = "EN_HEADLOSS";
        //    structFields[12].sFieldName = "EN_STATUS";
        //    structFields[13].sFieldName = "EN_SETTING";
        //    structFields[14].sFieldName = "EN_ENERGY";
        //    structFields[15].sFieldName = "POSITION";
        //    //------------------------------------------------
        //    structFields[0].sAliasName = "ID";
        //    structFields[1].sAliasName =  "EN_DIAMETER";
        //    structFields[2].sAliasName =  "EN_LENGTH";
        //    structFields[3].sAliasName =  "EN_ROUGHNESS";
        //    structFields[4].sAliasName =  "EN_MINORLOSS";
        //    structFields[5].sAliasName =  "EN_INITSTATUS";
        //    structFields[6].sAliasName =  "EN_INITSETTING";
        //    structFields[7].sAliasName =  "EN_KBULK";
        //    structFields[8].sAliasName =  "EN_KWALL";
        //    structFields[9].sAliasName =  "EN_FLOW";
        //    structFields[10].sAliasName = "EN_VELOCITY";
        //    structFields[11].sAliasName = "EN_HEADLOSS";
        //    structFields[12].sAliasName = "EN_STATUS";
        //    structFields[13].sAliasName = "EN_SETTING";
        //    structFields[14].sAliasName = "EN_ENERGY";
        //    structFields[15].sAliasName = "POSITION";
        //    //-------------------------------------------------------
        //    structFields[0].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;
        //    structFields[1].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[2].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[3].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[4].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[5].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[6].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[7].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[8].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[9].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[10].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[11].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[12].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[13].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[14].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
        //    structFields[15].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;

        //    //---------------------------------------------
        //    structFields[0].sFieldLength = 100;
        //    structFields[1].sFieldLength = 32;
        //    structFields[1].sScale = 2;
        //    structFields[2].sFieldLength = 32;
        //    structFields[2].sScale = 2;
        //    structFields[3].sFieldLength = 32;
        //    structFields[3].sScale = 2;
        //    structFields[4].sFieldLength = 32;
        //    structFields[4].sScale = 2;
        //    structFields[5].sFieldLength = 32;
        //    structFields[5].sScale = 2;
        //    structFields[6].sFieldLength = 32;
        //    structFields[6].sScale = 2;
        //    structFields[7].sFieldLength = 32;
        //    structFields[7].sScale = 2;
        //    structFields[8].sFieldLength = 32;
        //    structFields[8].sScale = 2;
        //    structFields[9].sFieldLength = 32;
        //    structFields[9].sScale = 2;
        //    structFields[10].sFieldLength = 32;
        //    structFields[10].sScale = 2;
        //    structFields[11].sFieldLength = 32;
        //    structFields[11].sScale = 2;
        //    structFields[12].sFieldLength = 32;
        //    structFields[12].sScale = 2;
        //    structFields[13].sFieldLength = 32;
        //    structFields[13].sScale = 2;
        //    structFields[14].sFieldLength = 32;
        //    structFields[14].sScale = 2;
        //    structFields[15].sFieldLength = 500;
        //    //--------------------------------------------------

        //    IFields pFields = WaterNet.WaterAOCore.ArcManager.CreateFields(WaterNet.WaterAOCore.VariableManager.ShapeType.ShapePolyline, structFields);

        //    //좌표계 구성 --------axMap.spatialReference
        //    ISpatialReference spatialReference = WaterNet.WaterAOCore.ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");

        //    //Shape FeatreClass생성
        //    //if (WaterNet.WaterAOCore.ArcManager.getShapeFeatureClass(pWorkspace, sName) != null)
        //    //{
        //    //    MessageManager.ShowInformationMessage(pWorkspace.PathName + " 경로에 이미 [" + sName + "] 레이어가 있습니다.");
        //    //    return null;
        //    //}

        //    pFeatureClass = WaterNet.WaterAOCore.ArcManager.CreateFeatureClass(m_Workspace, sName, spatialReference, esriFeatureType.esriFTSimple, esriGeometryType.esriGeometryPolyline, pFields, null, null, "");

        //    return pFeatureClass;
        //}

        /// <summary>
        /// ArcEditor StartEdit
        /// </summary>
        /// <param name="pWorkSpace"></param>
        /// <remarks></remarks>
        public bool StartEditor()
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)m_Workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.StartEditOperation();
                return true;
            }
            else
            {
                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();
                return false;
            }

        }

        /// <summary>
        /// ArcEditor StopEdit
        /// </summary>
        /// <param name="pWorkSpace"></param>
        /// <param name="pSave"></param>
        /// <remarks></remarks>
        public void StopEditor(bool pSave)
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)m_Workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(pSave);
            }

        }

        /// <summary>
        /// ArcEditor AbortEditor
        /// </summary>
        public void AbortEditor()
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)m_Workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.AbortEditOperation();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CreateINPLayerManager2
    {
        private EpaSimulator.ISimulator network;
        private ISpatialReference spatialReference;
        private Dictionary<string, IFeatureLayer> modelLayers = new Dictionary<string, IFeatureLayer>();
        private IWorkspace workspace = default(IWorkspace);
        private readonly string[] strLayerList = { "TANK", "RESERVOIR", "JUNCTION", "PIPE", "PUMP", "VALVE", "NODE", "LINK" };

        public CreateINPLayerManager2(EpaSimulator.ISimulator _network)
        {
            this.network = _network;
            //좌표계 구성 --------axMap.spatialReference
            this.spatialReference = WaterNet.WaterAOCore.ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");
        }

        /// <summary>
        /// workspace != null : 기존의 모델레이어를 반환가능.
        /// </summary>
        /// <returns></returns>
        private bool ExistsWorkspace()
        {
            bool result = false;
            string strWorkspace = System.IO.Path.Combine(VariableManager.m_INPgraphicRootDirectory, ((EpaSimulator.DBNetwork)this.network).inpNUMBER);
            this.workspace = ArcManager.getShapeWorkspace(strWorkspace);
            if (this.workspace != null)
            {
                this.modelLayers.Clear();
                EAGL.Explore.WorkspaceExplorer wsExplorer = new EAGL.Explore.WorkspaceExplorer(this.workspace);
                foreach (var layer in wsExplorer.FeatureLayers)
                {
                    if (this.strLayerList.Contains(layer.Name))
                    {
                        this.modelLayers.Add(layer.Name, (IFeatureLayer)layer);
                    }
                }
                result = true;
            }
            return result;
        }

        /// <summary>
        /// 1. 기존 workspace 존재 여부체크
        /// 2. workspace에 FeatureClass 존재 여부 체크
        /// 3. return : -1 = workspace있음, 0 = 실패, 1 = 정상
        /// </summary>
        /// <returns></returns>
        public int Execute()
        {
            int result = 0;
            if (ExistsWorkspace()) //workspace가 있는경우
            {
                return -1;
            }

            WaterNetCore.frmSplashMsg splash = new WaterNetCore.frmSplashMsg();
            splash.Open();
            splash.Message = "수리해석모델을 Shape으로 변환중입니다.\n\r잠시만 기다리십시오.";
            try
            {
                this.workspace = ArcManager.CreateShapeWorkspace(VariableManager.m_INPgraphicRootDirectory, ((EpaSimulator.DBNetwork)this.network).inpNUMBER);
                this.CreateShapeFeature();
                result = 1;
            }
            catch (System.Exception ex)
            {
                System.IO.Directory.Delete(VariableManager.m_INPgraphicRootDirectory + "\\" + ((EpaSimulator.DBNetwork)this.network).inpNUMBER, true);
                System.Windows.Forms.MessageBox.Show(ex.ToString());
            }
            finally
            {
                splash.Close();
                splash.Dispose();
            }
            return result;
        }

        /// <summary>
        /// 모델(INP)레이어 생성
        /// </summary>
        /// <param name="worksapce"></param>
        private void CreateShapeFeature()
        {
            try
            {
                this.modelLayers.Clear();
                this.CreateNodeShape();
                this.CreateLinkShape();

                this.StartEditor();

                this.CreateJunctionFeature();
                this.CreateReservoirFeature();
                this.CreateTankFeature();
                this.CreateNodeFeature();
                this.CreatePipeFeature();
                this.CreatePumpFeature();
                this.CreateValveFeature();
                this.CreateLinkFeature();

                this.StopEditor(true);
            }
            catch (Exception)
            {
                this.AbortEditor();
                throw;
            }
        }

        public Dictionary<string, IFeatureLayer> ModelLayers
        {
            get { return this.modelLayers; }
        }

        private void CreateJunctionFeature()
        {
            IFeatureClass fClass = this.modelLayers["JUNCTION"].FeatureClass;
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureBuffer pBuffer = fClass.CreateFeatureBuffer();
                IFeatureCursor pCursor = fClass.Insert(true);
                IFeature feature = pBuffer as IFeature;
                int id = pCursor.FindField("ID"); int desc = pCursor.FindField("DESCRIPT"); int tag = pCursor.FindField("TAG");
                comReleaser.ManageLifetime(pCursor);
                foreach (var v in this.network.NETWORK.Graph.Vertices)
                {
                    if (!(v is EpaNetwork.xJunc)) continue;
                    feature.set_Value(id, ((EpaNetwork.xNode)v).Id);
                    feature.set_Value(desc, ((EpaNetwork.xNode)v).Description);
                    feature.set_Value(tag, ((EpaNetwork.xNode)v).Tag);
                    feature.Shape = EpaNetwork.xUtils.makePoint2D(v.X, v.Y);
                    pCursor.InsertFeature(pBuffer);
                }
                pCursor.Flush();
            }
        }

        private void CreateReservoirFeature()
        {
            IFeatureClass fClass = this.modelLayers["RESERVOIR"].FeatureClass;
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureBuffer pBuffer = fClass.CreateFeatureBuffer();
                IFeatureCursor pCursor = fClass.Insert(false);
                IFeature feature = pBuffer as IFeature;
                int id = pCursor.FindField("ID"); int desc = pCursor.FindField("DESCRIPT"); int tag = pCursor.FindField("TAG");
                comReleaser.ManageLifetime(pCursor);
                foreach (var v in this.network.NETWORK.Graph.Vertices)
                {
                    if (!(v is EpaNetwork.xReservoir)) continue;
                    feature.set_Value(id, ((EpaNetwork.xNode)v).Id);
                    feature.set_Value(desc, ((EpaNetwork.xNode)v).Description);
                    feature.set_Value(tag, ((EpaNetwork.xNode)v).Tag);
                    feature.Shape = EpaNetwork.xUtils.makePoint2D(v.X, v.Y);
                    pCursor.InsertFeature(pBuffer);
                }
                pCursor.Flush();
            }
        }

        private void CreateTankFeature()
        {
            IFeatureClass fClass = this.modelLayers["TANK"].FeatureClass;
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureBuffer pBuffer = fClass.CreateFeatureBuffer();
                IFeatureCursor pCursor = fClass.Insert(false);
                IFeature feature = pBuffer as IFeature;
                int id = pCursor.FindField("ID"); int desc = pCursor.FindField("DESCRIPT"); int tag = pCursor.FindField("TAG");
                comReleaser.ManageLifetime(pCursor);
                foreach (var v in this.network.NETWORK.Graph.Vertices)
                {
                    if (!(v is EpaNetwork.xTank)) continue;
                    feature.set_Value(id, ((EpaNetwork.xNode)v).Id);
                    feature.set_Value(desc, ((EpaNetwork.xNode)v).Description);
                    feature.set_Value(tag, ((EpaNetwork.xNode)v).Tag);
                    feature.Shape = EpaNetwork.xUtils.makePoint2D(v.X, v.Y);
                    pCursor.InsertFeature(pBuffer);
                }
                pCursor.Flush();
            }
        }

        private void CreateNodeFeature()
        {
            IFeatureClass fClass = this.modelLayers["NODE"].FeatureClass;
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureBuffer pBuffer = fClass.CreateFeatureBuffer();
                IFeatureCursor pCursor = fClass.Insert(false);
                IFeature feature = pBuffer as IFeature;
                int id = pCursor.FindField("ID"); int desc = pCursor.FindField("DESCRIPT");
                int tag = pCursor.FindField("TAG"); int type = pCursor.FindField("TYPE");
                comReleaser.ManageLifetime(pCursor);
                foreach (var v in this.network.NETWORK.Graph.Vertices)
                {
                    feature.set_Value(id, ((EpaNetwork.xNode)v).Id);
                    feature.set_Value(desc, ((EpaNetwork.xNode)v).Description);
                    feature.set_Value(tag, ((EpaNetwork.xNode)v).Tag);
                    if (v is EpaNetwork.xJunc)
                    {
                        feature.set_Value(type, "Junction");
                    }
                    else if (v is EpaNetwork.xReservoir)
                    {
                        feature.set_Value(type, "Reservoir");
                    }
                    else if (v is EpaNetwork.xTank)
                    {
                        feature.set_Value(type, "Tank");
                    }
                    feature.Shape = EpaNetwork.xUtils.makePoint2D(v.X, v.Y);
                    pCursor.InsertFeature(pBuffer);
                }
                pCursor.Flush();
            }
        }

        private void CreatePipeFeature()
        {
            IFeatureClass fClass = this.modelLayers["PIPE"].FeatureClass;
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureBuffer pBuffer = fClass.CreateFeatureBuffer();
                IFeatureCursor pCursor = fClass.Insert(false);
                IFeature feature = pBuffer as IFeature;
                int id = pCursor.FindField("ID"); int desc = pCursor.FindField("DESCRIPT");
                int tag = pCursor.FindField("TAG"); int node1 = pCursor.FindField("NODE1"); int node2 = pCursor.FindField("NODE2");
                comReleaser.ManageLifetime(pCursor);
                foreach (var v in this.network.NETWORK.Graph.Edges)
                {
                    if (!(v is EpaNetwork.xPipe)) continue;
                    feature.set_Value(id, ((EpaNetwork.xLink)v).Id);
                    feature.set_Value(desc, ((EpaNetwork.xLink)v).Description);
                    feature.set_Value(tag, ((EpaNetwork.xLink)v).Tag);
                    feature.set_Value(node1, ((EpaNetwork.xLink)v).Source.Id);
                    feature.set_Value(node2, ((EpaNetwork.xLink)v).Target.Id);
                    feature.Shape = EpaNetwork.xUtils.makeGeometry((EpaNetwork.xLink)v);
                    pCursor.InsertFeature(pBuffer);
                }
                pCursor.Flush();
            }
        }

        private void CreateValveFeature()
        {
            IFeatureClass fClass = this.modelLayers["VALVE"].FeatureClass;
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureBuffer pBuffer = fClass.CreateFeatureBuffer();
                IFeatureCursor pCursor = fClass.Insert(false);
                IFeature feature = pBuffer as IFeature;
                int id = pCursor.FindField("ID"); int desc = pCursor.FindField("DESCRIPT");
                int tag = pCursor.FindField("TAG"); int node1 = pCursor.FindField("NODE1"); int node2 = pCursor.FindField("NODE2");
                comReleaser.ManageLifetime(pCursor);
                foreach (var v in this.network.NETWORK.Graph.Edges)
                {
                    if (!(v is EpaNetwork.xValve)) continue;

                    feature.set_Value(id, ((EpaNetwork.xLink)v).Id);
                    feature.set_Value(desc, ((EpaNetwork.xLink)v).Description);
                    feature.set_Value(tag, ((EpaNetwork.xLink)v).Tag);
                    feature.set_Value(node1, ((EpaNetwork.xLink)v).Source.Id);
                    feature.set_Value(node2, ((EpaNetwork.xLink)v).Target.Id);
                    feature.Shape = EpaNetwork.xUtils.makeGeometry((EpaNetwork.xLink)v);
                    pCursor.InsertFeature(pBuffer);
                }
                pCursor.Flush();
            }
        }

        private void CreatePumpFeature()
        {
            IFeatureClass fClass = this.modelLayers["PUMP"].FeatureClass;
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureBuffer pBuffer = fClass.CreateFeatureBuffer();
                IFeatureCursor pCursor = fClass.Insert(false);
                IFeature feature = pBuffer as IFeature;
                int id = pCursor.FindField("ID"); int desc = pCursor.FindField("DESCRIPT");
                int tag = pCursor.FindField("TAG"); int node1 = pCursor.FindField("NODE1"); int node2 = pCursor.FindField("NODE2");
                comReleaser.ManageLifetime(pCursor);
                foreach (var v in this.network.NETWORK.Graph.Edges)
                {
                    if (!(v is EpaNetwork.xPump)) continue;
                    feature.set_Value(id, ((EpaNetwork.xLink)v).Id);
                    feature.set_Value(desc, ((EpaNetwork.xLink)v).Description);
                    feature.set_Value(tag, ((EpaNetwork.xLink)v).Tag);
                    feature.set_Value(node1, ((EpaNetwork.xLink)v).Source.Id);
                    feature.set_Value(node2, ((EpaNetwork.xLink)v).Target.Id);
                    feature.Shape = EpaNetwork.xUtils.makeGeometry((EpaNetwork.xLink)v);
                    pCursor.InsertFeature(pBuffer);
                }
                pCursor.Flush();
            }
        }

        private void CreateLinkFeature()
        {
            IFeatureClass fClass = this.modelLayers["LINK"].FeatureClass;
            using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            {
                IFeatureBuffer pBuffer = fClass.CreateFeatureBuffer();
                IFeatureCursor pCursor = fClass.Insert(false);
                IFeature feature = pBuffer as IFeature;
                int id = pCursor.FindField("ID"); int desc = pCursor.FindField("DESCRIPT"); int tag = pCursor.FindField("TAG");
                int node1 = pCursor.FindField("NODE1"); int node2 = pCursor.FindField("NODE2"); int type = pCursor.FindField("TYPE");
                comReleaser.ManageLifetime(pCursor);
                foreach (var v in this.network.NETWORK.Graph.Edges)
                {
                    feature.set_Value(id, ((EpaNetwork.xLink)v).Id);
                    feature.set_Value(desc, ((EpaNetwork.xLink)v).Description);
                    feature.set_Value(tag, ((EpaNetwork.xLink)v).Tag);
                    feature.set_Value(node1, ((EpaNetwork.xLink)v).Source.Id);
                    feature.set_Value(node2, ((EpaNetwork.xLink)v).Target.Id);
                    if (v is EpaNetwork.xPipe)
                    {
                        feature.set_Value(type, "Pipe");
                    }
                    else if (v is EpaNetwork.xValve)
                    {
                        feature.set_Value(type, "Valve");
                    }
                    else if (v is EpaNetwork.xPump)
                    {
                        feature.set_Value(type, "Pump");
                    }
                    feature.Shape = EpaNetwork.xUtils.makeGeometry((EpaNetwork.xLink)v);
                    pCursor.InsertFeature(pBuffer);
                }
                pCursor.Flush();
            }
        }

        /// <summary>
        /// 노드(junction, reservoir, tank)레이어 생성
        /// </summary>
        /// <param name="workspace"></param>
        /// <param name="fields"></param>
        private void CreateNodeShape()
        {
            List<ESRI.ArcGIS.Geodatabase.IField> fields = new List<ESRI.ArcGIS.Geodatabase.IField>();
            ESRI.ArcGIS.Geodatabase.IField field;
            ESRI.ArcGIS.Geodatabase.IFieldEdit fieldedit;

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "ID";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 100;
            fields.Add(field);

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "DESCRIPT";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 200;
            fields.Add(field);

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "TAG";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 200;
            fields.Add(field);

            EAGL.Data.FeatureClassBuilder fcb = new EAGL.Data.FeatureClassBuilder(fields);
            fcb.Workspace = this.workspace;
            fcb.SpatialReference = this.spatialReference;
            fcb.GeometryType = ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint;
            fcb.NAME = "JUNCTION";
            ESRI.ArcGIS.Geodatabase.IFeatureClass junction = fcb.FeatureClass;
            fcb.NAME = "RESERVOIR";
            ESRI.ArcGIS.Geodatabase.IFeatureClass reservoir = fcb.FeatureClass;
            fcb.NAME = "TANK";
            ESRI.ArcGIS.Geodatabase.IFeatureClass tank = fcb.FeatureClass;

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "TYPE";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 10;
            fields.Add(field);
            fcb = new EAGL.Data.FeatureClassBuilder(fields);
            fcb.Workspace = this.workspace;
            fcb.SpatialReference = this.spatialReference;
            fcb.NAME = "NODE";
            ESRI.ArcGIS.Geodatabase.IFeatureClass node = fcb.FeatureClass;

            IFeatureClass[] l = { junction, reservoir, tank, node };
            for (int i = 0; i < l.Length; i++)
            {
                IFeatureLayer fLayer = new FeatureLayerClass();
                fLayer.FeatureClass = l[i];
                this.modelLayers.Add(l[i].AliasName, fLayer);
            }
        }

        /// <summary>
        /// 링크(pipe, valve, pump)레이어 생성
        /// </summary>
        /// <param name="workspace"></param>
        /// <param name="fields"></param>
        private void CreateLinkShape()
        {
            List<ESRI.ArcGIS.Geodatabase.IField> fields = new List<ESRI.ArcGIS.Geodatabase.IField>();
            ESRI.ArcGIS.Geodatabase.IField field;
            ESRI.ArcGIS.Geodatabase.IFieldEdit fieldedit;

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "ID";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 100;
            fields.Add(field);

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "NODE1";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 10;
            fields.Add(field);

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "NODE2";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 10;
            fields.Add(field);

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "DESCRIPT";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 200;
            fields.Add(field);

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "TAG";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 200;
            fields.Add(field);

            EAGL.Data.FeatureClassBuilder fcb = new EAGL.Data.FeatureClassBuilder(fields);
            fcb.Workspace = this.workspace;
            fcb.SpatialReference = this.spatialReference;
            fcb.GeometryType = ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline;
            fcb.NAME = "PIPE";
            ESRI.ArcGIS.Geodatabase.IFeatureClass pipe = fcb.FeatureClass;
            fcb.NAME = "VALVE";
            ESRI.ArcGIS.Geodatabase.IFeatureClass valve = fcb.FeatureClass;
            fcb.NAME = "PUMP";
            ESRI.ArcGIS.Geodatabase.IFeatureClass pump = fcb.FeatureClass;

            field = new ESRI.ArcGIS.Geodatabase.FieldClass();
            fieldedit = field as ESRI.ArcGIS.Geodatabase.IFieldEdit;
            fieldedit.Name_2 = "TYPE";
            fieldedit.Type_2 = ESRI.ArcGIS.Geodatabase.esriFieldType.esriFieldTypeString;
            fieldedit.Scale_2 = 10;
            fields.Add(field);

            fcb = new EAGL.Data.FeatureClassBuilder(fields);
            fcb.Workspace = this.workspace;
            fcb.SpatialReference = this.spatialReference;
            fcb.GeometryType = ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline;
            fcb.NAME = "LINK";
            ESRI.ArcGIS.Geodatabase.IFeatureClass link = fcb.FeatureClass;
            IFeatureClass[] l = { pipe, valve, pump, link };
            for (int i = 0; i < l.Length; i++)
            {
                IFeatureLayer fLayer = new FeatureLayerClass();
                fLayer.FeatureClass = l[i];
                this.modelLayers.Add(l[i].AliasName, fLayer);
            }
        }

        /// <summary>
        /// ArcEditor StartEdit
        /// </summary>
        /// <param name="pWorkSpace"></param>
        /// <remarks></remarks>
        public bool StartEditor()
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)this.workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.StartEditOperation();
                return true;
            }
            else
            {
                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();
                return false;
            }
        }

        /// <summary>
        /// ArcEditor StopEdit
        /// </summary>
        /// <param name="pWorkSpace"></param>
        /// <param name="pSave"></param>
        /// <remarks></remarks>
        public void StopEditor(bool pSave)
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)this.workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(pSave);
            }

        }

        /// <summary>
        /// ArcEditor AbortEditor
        /// </summary>
        public void AbortEditor()
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)this.workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.AbortEditOperation();
            }
        }
    }
}
