﻿namespace WaterNet.WaterAOCore
{
    partial class frmSymbolSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSymbolSelect));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.axSymbologyControl1 = new ESRI.ArcGIS.Controls.AxSymbologyControl();
            this.gbPolygon = new System.Windows.Forms.GroupBox();
            this.pnPolygonColor = new System.Windows.Forms.Panel();
            this.pnPolygonColor2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.nudPolygonWidth = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gbText = new System.Windows.Forms.GroupBox();
            this.labelFontSize = new System.Windows.Forms.Label();
            this.labelFontName = new System.Windows.Forms.Label();
            this.btnTextSymbol = new System.Windows.Forms.Button();
            this.pnTextColor = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDefault = new System.Windows.Forms.Button();
            this.cboStyleSet = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gbLine = new System.Windows.Forms.GroupBox();
            this.pnLineColor = new System.Windows.Forms.Panel();
            this.nudLineWidth = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gbPoint = new System.Windows.Forms.GroupBox();
            this.pnPointColor = new System.Windows.Forms.Panel();
            this.nudPointSize = new System.Windows.Forms.NumericUpDown();
            this.nudPointAngle = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axSymbologyControl1)).BeginInit();
            this.gbPolygon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPolygonWidth)).BeginInit();
            this.gbText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbLine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLineWidth)).BeginInit();
            this.gbPoint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPointSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPointAngle)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.axSymbologyControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gbPolygon);
            this.splitContainer1.Panel2.Controls.Add(this.gbText);
            this.splitContainer1.Panel2.Controls.Add(this.btnDefault);
            this.splitContainer1.Panel2.Controls.Add(this.cboStyleSet);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.cmdCancel);
            this.splitContainer1.Panel2.Controls.Add(this.cmdOK);
            this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel2.Controls.Add(this.gbLine);
            this.splitContainer1.Panel2.Controls.Add(this.gbPoint);
            this.splitContainer1.Size = new System.Drawing.Size(494, 558);
            this.splitContainer1.SplitterDistance = 305;
            this.splitContainer1.TabIndex = 5;
            // 
            // axSymbologyControl1
            // 
            this.axSymbologyControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axSymbologyControl1.Location = new System.Drawing.Point(0, 0);
            this.axSymbologyControl1.Name = "axSymbologyControl1";
            this.axSymbologyControl1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axSymbologyControl1.OcxState")));
            this.axSymbologyControl1.Size = new System.Drawing.Size(305, 558);
            this.axSymbologyControl1.TabIndex = 0;
            this.axSymbologyControl1.OnItemSelected += new ESRI.ArcGIS.Controls.ISymbologyControlEvents_Ax_OnItemSelectedEventHandler(this.axSymbologyControl1_OnItemSelected);
            // 
            // gbPolygon
            // 
            this.gbPolygon.Controls.Add(this.pnPolygonColor);
            this.gbPolygon.Controls.Add(this.pnPolygonColor2);
            this.gbPolygon.Controls.Add(this.label9);
            this.gbPolygon.Controls.Add(this.nudPolygonWidth);
            this.gbPolygon.Controls.Add(this.label5);
            this.gbPolygon.Controls.Add(this.label8);
            this.gbPolygon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbPolygon.Location = new System.Drawing.Point(12, 49);
            this.gbPolygon.Name = "gbPolygon";
            this.gbPolygon.Size = new System.Drawing.Size(166, 141);
            this.gbPolygon.TabIndex = 10;
            this.gbPolygon.TabStop = false;
            this.gbPolygon.Text = "옵션";
            // 
            // pnPolygonColor
            // 
            this.pnPolygonColor.BackColor = System.Drawing.Color.Red;
            this.pnPolygonColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnPolygonColor.Location = new System.Drawing.Point(86, 33);
            this.pnPolygonColor.Name = "pnPolygonColor";
            this.pnPolygonColor.Size = new System.Drawing.Size(61, 15);
            this.pnPolygonColor.TabIndex = 15;
            this.pnPolygonColor.Click += new System.EventHandler(this.pnPolygonColor_Click);
            // 
            // pnPolygonColor2
            // 
            this.pnPolygonColor2.BackColor = System.Drawing.Color.Red;
            this.pnPolygonColor2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnPolygonColor2.Location = new System.Drawing.Point(86, 110);
            this.pnPolygonColor2.Name = "pnPolygonColor2";
            this.pnPolygonColor2.Size = new System.Drawing.Size(61, 15);
            this.pnPolygonColor2.TabIndex = 14;
            this.pnPolygonColor2.Click += new System.EventHandler(this.pnPolygonColor2_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 12);
            this.label9.TabIndex = 13;
            this.label9.Text = "외곽선 색상";
            // 
            // nudPolygonWidth
            // 
            this.nudPolygonWidth.Location = new System.Drawing.Point(85, 69);
            this.nudPolygonWidth.Name = "nudPolygonWidth";
            this.nudPolygonWidth.Size = new System.Drawing.Size(64, 21);
            this.nudPolygonWidth.TabIndex = 11;
            this.nudPolygonWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPolygonWidth.ValueChanged += new System.EventHandler(this.nudPolygonWidth_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "외곽선 넓이";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "색상";
            // 
            // gbText
            // 
            this.gbText.Controls.Add(this.labelFontSize);
            this.gbText.Controls.Add(this.labelFontName);
            this.gbText.Controls.Add(this.btnTextSymbol);
            this.gbText.Controls.Add(this.pnTextColor);
            this.gbText.Controls.Add(this.label10);
            this.gbText.Controls.Add(this.label11);
            this.gbText.Controls.Add(this.label12);
            this.gbText.Controls.Add(this.panel1);
            this.gbText.Location = new System.Drawing.Point(11, 188);
            this.gbText.Name = "gbText";
            this.gbText.Size = new System.Drawing.Size(166, 134);
            this.gbText.TabIndex = 11;
            this.gbText.TabStop = false;
            this.gbText.Text = "옵션";
            // 
            // labelFontSize
            // 
            this.labelFontSize.AutoSize = true;
            this.labelFontSize.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelFontSize.Location = new System.Drawing.Point(68, 102);
            this.labelFontSize.Name = "labelFontSize";
            this.labelFontSize.Size = new System.Drawing.Size(31, 12);
            this.labelFontSize.TabIndex = 15;
            this.labelFontSize.Text = "없음";
            // 
            // labelFontName
            // 
            this.labelFontName.AutoSize = true;
            this.labelFontName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelFontName.Location = new System.Drawing.Point(68, 82);
            this.labelFontName.Name = "labelFontName";
            this.labelFontName.Size = new System.Drawing.Size(31, 12);
            this.labelFontName.TabIndex = 14;
            this.labelFontName.Text = "없음";
            // 
            // btnTextSymbol
            // 
            this.btnTextSymbol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTextSymbol.Location = new System.Drawing.Point(79, 52);
            this.btnTextSymbol.Name = "btnTextSymbol";
            this.btnTextSymbol.Size = new System.Drawing.Size(67, 23);
            this.btnTextSymbol.TabIndex = 6;
            this.btnTextSymbol.Text = "Font";
            this.btnTextSymbol.Click += new System.EventHandler(this.btnTextSymbol_Click);
            // 
            // pnTextColor
            // 
            this.pnTextColor.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnTextColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnTextColor.Location = new System.Drawing.Point(80, 28);
            this.pnTextColor.Name = "pnTextColor";
            this.pnTextColor.Size = new System.Drawing.Size(64, 15);
            this.pnTextColor.TabIndex = 12;
            this.pnTextColor.Click += new System.EventHandler(this.pnTextColor_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 9;
            this.label10.Text = "크기";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 8;
            this.label11.Text = "이름";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 7;
            this.label12.Text = "색상";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(17, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(137, 65);
            this.panel1.TabIndex = 24;
            // 
            // btnDefault
            // 
            this.btnDefault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDefault.Location = new System.Drawing.Point(12, 470);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(172, 40);
            this.btnDefault.TabIndex = 13;
            this.btnDefault.Text = "원래대로";
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // cboStyleSet
            // 
            this.cboStyleSet.FormattingEnabled = true;
            this.cboStyleSet.Location = new System.Drawing.Point(12, 10);
            this.cboStyleSet.Name = "cboStyleSet";
            this.cboStyleSet.Size = new System.Drawing.Size(167, 20);
            this.cboStyleSet.TabIndex = 12;
            this.cboStyleSet.SelectedIndexChanged += new System.EventHandler(this.cboStyleSet_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "미리보기";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdCancel.Location = new System.Drawing.Point(98, 513);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(86, 40);
            this.cmdCancel.TabIndex = 5;
            this.cmdCancel.Text = "닫기";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdOK.Location = new System.Drawing.Point(11, 513);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(86, 40);
            this.cmdOK.TabIndex = 4;
            this.cmdOK.Text = "확인";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(11, 63);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(167, 103);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // gbLine
            // 
            this.gbLine.Controls.Add(this.pnLineColor);
            this.gbLine.Controls.Add(this.nudLineWidth);
            this.gbLine.Controls.Add(this.label6);
            this.gbLine.Controls.Add(this.label7);
            this.gbLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbLine.Location = new System.Drawing.Point(12, 206);
            this.gbLine.Name = "gbLine";
            this.gbLine.Size = new System.Drawing.Size(166, 104);
            this.gbLine.TabIndex = 8;
            this.gbLine.TabStop = false;
            this.gbLine.Text = "옵션";
            // 
            // pnLineColor
            // 
            this.pnLineColor.Location = new System.Drawing.Point(74, 33);
            this.pnLineColor.Name = "pnLineColor";
            this.pnLineColor.Size = new System.Drawing.Size(61, 15);
            this.pnLineColor.TabIndex = 12;
            this.pnLineColor.Click += new System.EventHandler(this.pnLineColor_Click);
            // 
            // nudLineWidth
            // 
            this.nudLineWidth.Location = new System.Drawing.Point(73, 63);
            this.nudLineWidth.Name = "nudLineWidth";
            this.nudLineWidth.Size = new System.Drawing.Size(64, 21);
            this.nudLineWidth.TabIndex = 11;
            this.nudLineWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudLineWidth.ValueChanged += new System.EventHandler(this.nudLineWidth_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "넓이";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 7;
            this.label7.Text = "색상";
            // 
            // gbPoint
            // 
            this.gbPoint.Controls.Add(this.pnPointColor);
            this.gbPoint.Controls.Add(this.nudPointSize);
            this.gbPoint.Controls.Add(this.nudPointAngle);
            this.gbPoint.Controls.Add(this.label4);
            this.gbPoint.Controls.Add(this.label3);
            this.gbPoint.Controls.Add(this.label2);
            this.gbPoint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbPoint.Location = new System.Drawing.Point(12, 328);
            this.gbPoint.Name = "gbPoint";
            this.gbPoint.Size = new System.Drawing.Size(166, 134);
            this.gbPoint.TabIndex = 7;
            this.gbPoint.TabStop = false;
            this.gbPoint.Text = "옵션";
            // 
            // pnPointColor
            // 
            this.pnPointColor.Location = new System.Drawing.Point(74, 33);
            this.pnPointColor.Name = "pnPointColor";
            this.pnPointColor.Size = new System.Drawing.Size(61, 15);
            this.pnPointColor.TabIndex = 12;
            this.pnPointColor.Click += new System.EventHandler(this.pnPointColor_Click);
            // 
            // nudPointSize
            // 
            this.nudPointSize.DecimalPlaces = 1;
            this.nudPointSize.Location = new System.Drawing.Point(73, 63);
            this.nudPointSize.Name = "nudPointSize";
            this.nudPointSize.Size = new System.Drawing.Size(64, 21);
            this.nudPointSize.TabIndex = 11;
            this.nudPointSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPointSize.ValueChanged += new System.EventHandler(this.nudPointSize_ValueChanged);
            // 
            // nudPointAngle
            // 
            this.nudPointAngle.Location = new System.Drawing.Point(73, 96);
            this.nudPointAngle.Name = "nudPointAngle";
            this.nudPointAngle.Size = new System.Drawing.Size(64, 21);
            this.nudPointAngle.TabIndex = 10;
            this.nudPointAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPointAngle.ValueChanged += new System.EventHandler(this.nudPointAngle_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "각도";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "크기";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "색상";
            // 
            // frmSymbolSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 558);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSymbolSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "심볼선택";
            this.Load += new System.EventHandler(this.frmSymbolSelect_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmSymbolSelect_KeyDown);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axSymbologyControl1)).EndInit();
            this.gbPolygon.ResumeLayout(false);
            this.gbPolygon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPolygonWidth)).EndInit();
            this.gbText.ResumeLayout(false);
            this.gbText.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbLine.ResumeLayout(false);
            this.gbLine.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLineWidth)).EndInit();
            this.gbPoint.ResumeLayout(false);
            this.gbPoint.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPointSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPointAngle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox gbText;
        private System.Windows.Forms.Button btnTextSymbol;
        private System.Windows.Forms.Panel pnTextColor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox gbPolygon;
        private System.Windows.Forms.Panel pnPolygonColor2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudPolygonWidth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox gbLine;
        private System.Windows.Forms.Panel pnLineColor;
        private System.Windows.Forms.NumericUpDown nudLineWidth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbPoint;
        private System.Windows.Forms.Panel pnPointColor;
        private System.Windows.Forms.NumericUpDown nudPointSize;
        private System.Windows.Forms.NumericUpDown nudPointAngle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelFontSize;
        private System.Windows.Forms.Label labelFontName;
        private System.Windows.Forms.ComboBox cboStyleSet;
        private System.Windows.Forms.Panel pnPolygonColor;
        private System.Windows.Forms.Button btnDefault;
        private System.Windows.Forms.Panel panel1;
        private ESRI.ArcGIS.Controls.AxSymbologyControl axSymbologyControl1;
    }
}