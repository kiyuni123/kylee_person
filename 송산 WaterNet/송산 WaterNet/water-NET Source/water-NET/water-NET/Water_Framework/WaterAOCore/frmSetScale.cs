﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ESRI.ArcGIS.Carto;

namespace WaterNet.WaterAOCore
{
    public partial class frmSetScale : Form
    {
        private ILayer m_Layer = null;
        
        public frmSetScale()
        {
            InitializeComponent();
        }

        public frmSetScale(ILayer pLayer)
        {
            InitializeComponent();

            m_Layer = pLayer;
            if (pLayer.MinimumScale == 0 && pLayer.MaximumScale == 0) rdAll.Checked = true;
            else rdPart.Checked = true;

            txtminScale.Text = pLayer.MinimumScale.ToString();
            txtmaxScale.Text = pLayer.MaximumScale.ToString();
        }

        private void btnConform_Click(object sender, EventArgs e)
        {
            if (rdAll.Checked)
            {
                m_Layer.MinimumScale = 0; m_Layer.MaximumScale = 0;
            }
            else
            {
                if (string.IsNullOrEmpty(txtminScale.Text)) txtminScale.Text = "0";
                if (string.IsNullOrEmpty(txtmaxScale.Text)) txtmaxScale.Text = "0";

                m_Layer.MinimumScale = Convert.ToDouble(txtminScale.Text);
                m_Layer.MaximumScale = Convert.ToDouble(txtmaxScale.Text);
            }
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void rdAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rdAll.Checked)
            {
                txtminScale.Enabled = false; txtmaxScale.Enabled = false;
            }
        }

        private void rdPart_CheckedChanged(object sender, EventArgs e)
        {
            if (rdPart.Checked)
            {
                txtminScale.Enabled = true; txtmaxScale.Enabled = true;
            }
        }

        private void txtminScale_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void txtmaxScale_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void frmSetScale_Load(object sender, EventArgs e)
        {
            this.BringToFront();
        }
    }
}
