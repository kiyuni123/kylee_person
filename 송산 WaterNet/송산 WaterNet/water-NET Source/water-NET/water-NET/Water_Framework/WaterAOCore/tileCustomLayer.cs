﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

using BruTile;
using BruTile.Cache;

using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.DataSourcesRaster;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;

namespace WaterNet.WaterAOCore
{
    public sealed class tileCustomLayer : ESRI.ArcGIS.ADF.BaseClasses.BaseCustomLayer
    {
        private readonly ITileSource _tileSource;
        private readonly FileCache _fileCache;
        private readonly SimpleFileFetcher _simplefilefetcher;
        private readonly ISpatialReference _dataSpatialReference;
        private readonly IMap _map;

        public tileCustomLayer(IMap map, ITileSource tileSource, FileCache fileCache)
        {
            _map = map;
            _tileSource = tileSource;
            _fileCache = fileCache;
            _simplefilefetcher = new SimpleFileFetcher(tileSource, fileCache);

            
            _dataSpatialReference = map.SpatialReference;
            //var spatialReferences = new SpatialReferences();
            //_dataSpatialReference = spatialReferences.GetSpatialReference(_tileSource.Schema.Srs);
            //_dataSpatialReference = spatialReferences.GetSpatialReference2(_tileSource.Schema.SrsWkt);
            //_dataSpatialReference = spatialReferences.GetSpatialReference2(SpatialReferences.GetWGS84());
            //Console.WriteLine(_tileSource.Schema.SrsWkt);

            var envelope = GetDefaultEnvelope();

            if (_map.SpatialReference == null)
                _map.SpatialReference = _dataSpatialReference;

            if (SpatialReference.FactoryCode == 0)
                m_spatialRef = _dataSpatialReference;

            if (_map.LayerCount == 0)
                ((IActiveView)_map).Extent = envelope;
        }

        public override void Draw(esriDrawPhase drawPhase, IDisplay display, ITrackCancel trackCancel)
        {
            switch (drawPhase)
            {
                case esriDrawPhase.esriDPGeography:
                    if (!Valid) return;
                    if (!Visible) return;
                    if (((IActiveView)_map).Extent.IsEmpty) return;

                    var activeView = (IActiveView)_map;
                    //var env = ProjectEnvelope(activeView.Extent, _tileSource.Schema.Srs);

                    var clipEnvelope = activeView.Extent; // display.ClipEnvelope;
                    var mapWidth = activeView.ExportFrame.right;
                    var mapHeight = activeView.ExportFrame.bottom;
                    var resolution = clipEnvelope.GetMapResolution(mapWidth);
                    var centerPoint = clipEnvelope.GetCenterPoint();
                    
                    //Console.WriteLine(string.Format("Draw extent:{0},{1},{2},{3} : {4},{5},{6},{7} : {8},{9},{10},{11}", 
                    //    clipEnvelope.XMin, clipEnvelope.YMin, clipEnvelope.XMax, clipEnvelope.YMax,
                    //    env.XMin, env.YMin, env.XMax, env.YMax,
                    //    activeView.Extent.XMin, activeView.Extent.YMin, activeView.Extent.XMax, activeView.Extent.YMax));

                    var extent = new Extent(clipEnvelope.XMin, clipEnvelope.YMin, clipEnvelope.XMax, clipEnvelope.YMax);

                    

                    //var transform = new Transform(centerPoint, resolution, mapWidth, mapHeight);

                    _simplefilefetcher.Fetch(extent, resolution);

                    var level = Utilities.GetNearestLevel(_tileSource.Schema.Resolutions,resolution);
                    var tileInfos = _tileSource.Schema.GetTilesInView(extent, level);
                    tileInfos = SortByPriority(tileInfos, extent.CenterX, extent.CenterY);

                    foreach (var tileInfo in tileInfos)
                    {
                        var tile = _fileCache.Find(tileInfo.Index);
                        if (tile != null)
                        {
                            var filename = _fileCache.GetFileName(tileInfo.Index);
                            DrawRaster(filename, display, tileInfo);
                        }
                    }
                    break;
                case esriDrawPhase.esriDPAnnotation:
                    break;
            }
        }

        public override bool Visible
        {
            get { return m_visible; }
            set
            {
                m_visible = value;
                ((IActiveView)_map).Refresh();
            }
        }

        private static IEnvelope ProjectEnvelope(IEnvelope envelope, string srs)
        {
            var spatialReferences = new SpatialReferences();
            var dataSpatialReference = spatialReferences.GetSpatialReference(srs);
            envelope.Project(dataSpatialReference);
            return envelope;
        }

        private static IEnumerable<TileInfo> SortByPriority(IEnumerable<TileInfo> tiles, double centerX, double centerY)
        {
            return tiles.OrderBy(t => Distance(centerX, centerY, t.Extent.CenterX, t.Extent.CenterY));
        }

        public static double Distance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow(x1 - x2, 2.0) + Math.Pow(y1 - y2, 2.0));
        }

        private IEnvelope GetDefaultEnvelope()
        {
            var ext = _tileSource.Schema.Extent;
            var envelope = new EnvelopeClass
            {
                XMin = ext.MinX,
                XMax = ext.MaxX,
                YMin = ext.MinY,
                YMax = ext.MaxY,
                SpatialReference = _dataSpatialReference
            };
            return envelope;
        }

        private void DrawRaster(string filename, IDisplay display, TileInfo tileinfo)
        {
            try
            {  
                var rl = new RasterLayerClass();
                rl.CreateFromFilePath(filename);

                var props = (IRasterProps)rl.Raster;
                props.SpatialReference = _dataSpatialReference;
                //IEnvelope newenv = new EnvelopeClass();
                //newenv.PutCoords(tileinfo.Extent.MinX-0.5, tileinfo.Extent.MinY-0.5, tileinfo.Extent.MaxX+0.5, tileinfo.Extent.MaxY+0.5);
                //props.Extent = newenv;

                var image = new Bitmap(filename, true);
                var format = image.PixelFormat;
                if (format == PixelFormat.Format24bppRgb || format == PixelFormat.Format32bppArgb || format == PixelFormat.Format32bppRgb)
                {
                    var rasterRGBRenderer = new RasterRGBRendererClass();
                    ((IRasterStretch2)rasterRGBRenderer).StretchType = esriRasterStretchTypesEnum.esriRasterStretch_NONE;
                    rasterRGBRenderer.Raster = rl.Raster;
                    rl.Renderer = rasterRGBRenderer;
                }
                rl.Renderer.ResamplingType = rstResamplingTypes.RSP_BilinearInterpolation;
                rl.Renderer.Update();

                // Now set the spatial reference to the dataframe spatial reference! 
                // Do not remove this line...
                rl.SpatialReference = SpatialReference;
                rl.Draw(ESRI.ArcGIS.esriSystem.esriDrawPhase.esriDPGeography, display, null);
            }
            catch { }
        }
    }

    public static class EnvelopeExtensionMethods
    {
        public static float GetMapResolution(this IEnvelope env, int mapWidth)
        {
            var dx = env.XMax - env.XMin;
            var res = Convert.ToSingle(dx / mapWidth);
            return res;
        }


        public static PointF GetCenterPoint(this IEnvelope env)
        {
            var p = new PointF
            {
                X = Convert.ToSingle(env.XMin + (env.XMax - env.XMin) / 2),
                Y = Convert.ToSingle(env.YMin + (env.YMax - env.YMin) / 2)
            };
            return p;
        }
    }

    public static class ExtensionMethods
    {
        public static void AddWorldFile(this FileCache fileCache, TileInfo tileInfo, int width, int height, string format)
        {
            var fileName = fileCache.GetFileName(tileInfo.Index);
            var fi = new System.IO.FileInfo(fileName);
            var tfwFile = fileName.Replace(fi.Extension, "." + GetWorldFile(format));
            var extent = tileInfo.Extent;

            using (var sw = new System.IO.StreamWriter(tfwFile))
            {
                var resX = (extent.MaxX - extent.MinX) / width;
                var resY = (extent.MaxY - extent.MinY) / height;
                sw.WriteLine(resX.ToString(System.Globalization.CultureInfo.InvariantCulture));
                sw.WriteLine("0");
                sw.WriteLine("0");
                sw.WriteLine((resY * -1).ToString(System.Globalization.CultureInfo.InvariantCulture));
                sw.WriteLine(extent.MinX.ToString(System.Globalization.CultureInfo.InvariantCulture));
                sw.WriteLine(extent.MaxY.ToString(System.Globalization.CultureInfo.InvariantCulture));
                sw.Close();
            }

        }

        private static string GetWorldFile(string format)
        {
            var res = String.Empty;

            format = (format.Contains(@"image/") ? format.Substring(6, format.Length - 6) : format);

            if (format == "jpg")
            {
                res = "jgw";
            }
            if (format == "jpeg")
            {
                res = "jgw";
            }
            else if (format == "png")
            {
                res = "pgw";
            }
            else if (format == "png8")
            {
                res = "pgw";
            }

            else if (format == "tif")
            {
                res = "tfw";
            }

            return res;

        }

    }

    public class SimpleFileFetcher
    {
        private readonly ITileSource _tileSource;
        private readonly FileCache _fileCache;
        private const bool async = false;

        public SimpleFileFetcher(ITileSource tileSource, FileCache filecache)
        {
            if (tileSource == null) throw new ArgumentNullException("tileSource");
            if (filecache == null) throw new ArgumentNullException("filecache");
            _tileSource = tileSource;
            _fileCache = filecache;
        }

        public void Fetch(Extent newExtent, double newResolution)
        {
            var levelId = Utilities.GetNearestLevel(_tileSource.Schema.Resolutions, newResolution);
            var tilesWanted = GetTilesWanted(_tileSource.Schema, newExtent, levelId);
            var tilesMissing = GetTilesMissing(tilesWanted, _fileCache);
            //var stp = new Amib.Threading.SmartThreadPool(1000, 5);

            foreach (var tileInfo in tilesMissing)
            {
                Fetch(tileInfo);

                //// for debugging
                //if (!async)
                //    Fetch(tileInfo);
                //else
                //{
                //    stp.QueueWorkItem(GetTileOnThread, new object[] { tileInfo });
                //}
            }
        }

        //private void GetTileOnThread(object parameter)
        //{

        //    var @params = (object[])parameter;
        //    var tileInfo = (TileInfo)@params[0];

        //    Fetch(tileInfo);
        //}


        private void Fetch(TileInfo tileInfo)
        {
            try
            {
                var data = _tileSource.Provider.GetTile(tileInfo);
                _fileCache.Add(tileInfo.Index, data);
                _fileCache.AddWorldFile(tileInfo, _tileSource.Schema.Width, _tileSource.Schema.Height, _tileSource.Schema.Format);
            }
            catch { }
        }


        public IList<TileInfo> GetTilesWanted(ITileSchema schema, Extent extent, string levelId)
        {
            return schema.GetTilesInView(extent, (levelId)).ToList();
        }

        public IList<TileInfo> GetTilesMissing(IEnumerable<TileInfo> tilesWanted, FileCache fileCache)
        {
            return tilesWanted.Where(info => fileCache.Find(info.Index) == null).ToList();
        }
    }

    public class SpatialReferences
    {
        public ISpatialReference GetSpatialReference2(string prj)
        {
            ISpatialReference res = null;
            try
            {
                var pSrf = new SpatialReferenceEnvironmentClass();
                var geographicCoordinateSystem = pSrf.CreateESRISpatialReferenceFromPRJ(prj);
                // ReSharper disable once UnusedVariable
                var spatialReference = (ISpatialReference)geographicCoordinateSystem;
            }
            catch (Exception ex)
            {   
                Console.WriteLine(ex.ToString());
            }
         

            return res;
        }
        public ISpatialReference GetSpatialReference(string epsgCode)
        {
            ISpatialReference res = null;

            // first get the code
            var start = epsgCode.IndexOf(":", System.StringComparison.Ordinal) + 1;
            var end = epsgCode.Length;

            int code = int.Parse(epsgCode.Substring(start, end - start));

            // Handle non official EPSG codes...
            if (code == 900913 | code == 41001) code = 102113;
            if (code == 5179 | code == 5181) code = 102113;

            if (IsProjectedSpatialReference(code))
            {
                res = GetProjectedSpatialReference(code);
            }
            else if (IsGeographicSpatialReference(code))
            {
                res = GetGeographicSpatialReference(code);
            }

            return res;
        }


        private static bool IsGeographicSpatialReference(int gcsType)
        {
            try
            {
                var pSrf = new SpatialReferenceEnvironmentClass();
                var geographicCoordinateSystem = pSrf.CreateGeographicCoordinateSystem(gcsType);
                // ReSharper disable once UnusedVariable
                var spatialReference = (ISpatialReference)geographicCoordinateSystem;
                return true;
            }
            catch
            {
                return false;
            }
        }


        private static bool IsProjectedSpatialReference(int pcsType)
        {
            try
            {
                var pSrf = new SpatialReferenceEnvironmentClass();
                var mProjectedCoordinateSystem = pSrf.CreateProjectedCoordinateSystem(pcsType);
                // ReSharper disable once UnusedVariable
                var spatialReference = (ISpatialReference)mProjectedCoordinateSystem;
                return true;
            }
            catch
            {
                return false;
            }
        }


        protected ISpatialReference GetGeographicSpatialReference(int gcsType)
        {
            var pSrf = new SpatialReferenceEnvironmentClass();
            var geographicCoordinateSystem = pSrf.CreateGeographicCoordinateSystem(gcsType);
            var spatialReference = (ISpatialReference)geographicCoordinateSystem;
            return spatialReference;
        }


        protected ISpatialReference GetProjectedSpatialReference(int pcsType)
        {
            var pSrf = new SpatialReferenceEnvironmentClass();
            var projectedCoordinateSystem = pSrf.CreateProjectedCoordinateSystem(pcsType);
            var spatialReference = (ISpatialReference)projectedCoordinateSystem;
            return spatialReference;
        }

        public static string GetWebMercator()
        {
            return "PROJCS[&quot;WGS_1984_Web_Mercator&quot;,GEOGCS[&quot;GCS_WGS_1984_Major_Auxiliary_Sphere&quot;,DATUM[&quot;WGS_1984_Major_Auxiliary_Sphere&quot;,SPHEROID[&quot;WGS_1984_Major_Auxiliary_Sphere&quot;,6378137.0,0.0]],PRIMEM[&quot;Greenwich&quot;,0.0],UNIT[&quot;Degree&quot;,0.0174532925199433]],PROJECTION[&quot;Mercator_1SP&quot;],PARAMETER[&quot;False_Easting&quot;,0.0],PARAMETER[&quot;False_Northing&quot;,0.0],PARAMETER[&quot;Central_Meridian&quot;,0.0],PARAMETER[&quot;latitude_of_origin&quot;,0.0],UNIT[&quot;Meter&quot;,1.0]]";
        }

        public static string GetWGS84()
        {
            return "GEOGCS[&quot;GCS_WGS_1984&quot;,DATUM[&quot;WGS_1984&quot;,SPHEROID[&quot;WGS_1984&quot;,6378137.0,298.257223563]],PRIMEM[&quot;Greenwich&quot;,0.0],UNIT[&quot;Degree&quot;,0.0174532925199433]]";
        }

        public static string GetRDNew()
        {
            return "PROJCS[&quot;RD_New&quot;,GEOGCS[&quot;GCS_Amersfoort&quot;,DATUM[&quot;D_Amersfoort&quot;,SPHEROID[&quot;Bessel_1841&quot;,6377397.155,299.1528128]],PRIMEM[&quot;Greenwich&quot;,0.0],UNIT[&quot;Degree&quot;,0.0174532925199433]],PROJECTION[&quot;Double_Stereographic&quot;],PARAMETER[&quot;False_Easting&quot;,155000.0],PARAMETER[&quot;False_Northing&quot;,463000.0],PARAMETER[&quot;Central_Meridian&quot;,5.38763888888889],PARAMETER[&quot;Scale_Factor&quot;,0.9999079],PARAMETER[&quot;Latitude_Of_Origin&quot;,52.15616055555555],UNIT[&quot;Meter&quot;,1.0]]";
        }

    }

    public class Transform
    {
        float _resolution;
        PointF _center;
        float _width;
        float _height;
        BruTile.Extent _extent;

        public Transform(PointF center, float resolution, float width, float height)
        {
            _center = center;
            _resolution = resolution;
            _width = width;
            _height = height;
            UpdateExtent();
        }

        public float Resolution
        {
            set
            {
                _resolution = value;
                UpdateExtent();
            }
            get
            {
                return _resolution;
            }
        }

        public PointF Center
        {
            set
            {
                _center = value;
                UpdateExtent();
            }
        }

        public float Width
        {
            set
            {
                _width = value;
                UpdateExtent();
            }
        }

        public float Height
        {
            set
            {
                _height = value;
                UpdateExtent();
            }
        }

        public BruTile.Extent Extent
        {
            get { return _extent; }
        }

        public PointF WorldToMap(double x, double y)
        {
            return new PointF((float)(x - _extent.MinX) / _resolution, (float)(_extent.MaxY - y) / _resolution);
        }

        public PointF MapToWorld(double x, double y)
        {
            return new PointF((float)(_extent.MinX + x) * _resolution, (float)(_extent.MaxY - y) * _resolution);
        }

        public RectangleF WorldToMap(double x1, double y1, double x2, double y2)
        {
            var point1 = WorldToMap(x1, y1);
            var point2 = WorldToMap(x2, y2);
            return new RectangleF(point1.X, point2.Y, point2.X - point1.X, point1.Y - point2.Y);
        }

        private void UpdateExtent()
        {
            var spanX = _width * _resolution;
            var spanY = _height * _resolution;
            _extent = new BruTile.Extent(_center.X - spanX * 0.5f, _center.Y - spanY * 0.5f,
              _center.X + spanX * 0.5f, _center.Y + spanY * 0.5f);
        }
    }
}
