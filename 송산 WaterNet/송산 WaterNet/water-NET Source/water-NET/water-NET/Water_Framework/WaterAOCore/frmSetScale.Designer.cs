﻿namespace WaterNet.WaterAOCore
{
    partial class frmSetScale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdAll = new System.Windows.Forms.RadioButton();
            this.rdPart = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtminScale = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtmaxScale = new System.Windows.Forms.TextBox();
            this.btnConform = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // rdAll
            // 
            this.rdAll.AutoSize = true;
            this.rdAll.Location = new System.Drawing.Point(36, 24);
            this.rdAll.Name = "rdAll";
            this.rdAll.Size = new System.Drawing.Size(219, 16);
            this.rdAll.TabIndex = 0;
            this.rdAll.TabStop = true;
            this.rdAll.Text = "축척과 관계없이 레이어가 보입니다.";
            this.rdAll.UseVisualStyleBackColor = true;
            this.rdAll.CheckedChanged += new System.EventHandler(this.rdAll_CheckedChanged);
            // 
            // rdPart
            // 
            this.rdPart.AutoSize = true;
            this.rdPart.Location = new System.Drawing.Point(36, 56);
            this.rdPart.Name = "rdPart";
            this.rdPart.Size = new System.Drawing.Size(231, 16);
            this.rdPart.TabIndex = 2;
            this.rdPart.TabStop = true;
            this.rdPart.Text = "다음 축척내에서만 레이어가 보입니다.";
            this.rdPart.UseVisualStyleBackColor = true;
            this.rdPart.CheckedChanged += new System.EventHandler(this.rdPart_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "1 /";
            // 
            // txtminScale
            // 
            this.txtminScale.Location = new System.Drawing.Point(221, 99);
            this.txtminScale.Name = "txtminScale";
            this.txtminScale.Size = new System.Drawing.Size(61, 21);
            this.txtminScale.TabIndex = 4;
            this.txtminScale.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtminScale_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(139, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "≤ 축척 ≤ 1 /";
            // 
            // txtmaxScale
            // 
            this.txtmaxScale.Location = new System.Drawing.Point(73, 99);
            this.txtmaxScale.Name = "txtmaxScale";
            this.txtmaxScale.Size = new System.Drawing.Size(61, 21);
            this.txtmaxScale.TabIndex = 6;
            this.txtmaxScale.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmaxScale_KeyPress);
            // 
            // btnConform
            // 
            this.btnConform.Location = new System.Drawing.Point(100, 159);
            this.btnConform.Name = "btnConform";
            this.btnConform.Size = new System.Drawing.Size(94, 35);
            this.btnConform.TabIndex = 7;
            this.btnConform.Text = "확인";
            this.btnConform.UseVisualStyleBackColor = true;
            this.btnConform.Click += new System.EventHandler(this.btnConform_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(200, 159);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(94, 35);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "취소";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(25, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(269, 82);
            this.panel1.TabIndex = 9;
            // 
            // frmSetScale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(329, 212);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConform);
            this.Controls.Add(this.txtmaxScale);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtminScale);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rdPart);
            this.Controls.Add(this.rdAll);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSetScale";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "유효축척 설정";
            this.Load += new System.EventHandler(this.frmSetScale_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rdAll;
        private System.Windows.Forms.RadioButton rdPart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtminScale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtmaxScale;
        private System.Windows.Forms.Button btnConform;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel1;
    }
}