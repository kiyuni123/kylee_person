﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.ADF;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

using WaterNet.WaterNetCore;

namespace WaterNet.WaterAOCore
{
    public partial class frmMap2 : Form
    {
        protected ILayer m_workLayer = null;
        public frmMap2()
        {
            InitializeComponent();
        }

        protected virtual void InitializeSetting()
        {
            axToolbar.CustomProperty = this;

            //m_tocContextMenu = new TocContextMenuClass(axMap.Object);

            //Add map navigation commands
            axToolbar.AddItem("esriControls.ControlsMapZoomInTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomOutTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapPanTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapFullExtentCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomToLastExtentBackCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomToLastExtentForwardCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapZoomToolControl", 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            //Add map inquiry commands
            axToolbar.AddItem("esriControls.ControlsMapIdentifyTool", -1, -1, true, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapFindCommand", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            axToolbar.AddItem("esriControls.ControlsMapMeasureTool", -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            //Set buddy control
            axToolbar.SetBuddyControl(axMap);
            axTOC.SetBuddyControl(axMap);

        }

        public virtual void Open()
        {
            Indexmap_Load();
        }

        private void Indexmap_Load()
        {
            IniManager.Win32Reg Config = new IniManager.Win32Reg(Application.StartupPath + "\\Configuration.ini");
            string sPath = Config.GetPairValueInSection("Topographic Dir", "WorkSpace");

            IWorkspace pWS = VariableManager.m_Topographic;
            if (pWS == null) return;

            string[] szPairs = Config.GetPairsBySection("IndexMap Layer");
            IEnumerator ez = szPairs.GetEnumerator();
            string[] szTemp;

            ILayer pLayer = null; IColor pColor = null;
            try
            {
                while (ez.MoveNext())
                {
                    szTemp = IniManager.Win32Reg.SplitPair(ez.Current.ToString());
                    if (szTemp.GetValue(0).ToString().Length != 0)
                    {
                        pLayer = ArcManager.GetShapeLayer(pWS, szTemp.GetValue(0).ToString(), szTemp.GetValue(1).ToString());
                        if (pLayer != null)
                        {
                            if (szTemp.GetValue(0).ToString() == "BZ003")
                            {
                                pColor = ArcManager.GetRandomColor;
                                ArcManager.SetLabelProperty((IFeatureLayer)pLayer,"블록명","굴림",10,false,pColor,0,0,string.Empty);
                                ArcManager.SetUniqueValueRenderer((IFeatureLayer)pLayer, "블록명");
                            }

                            axIndexMap.AddLayer(pLayer);
                        }
#if DEBUG
                        Console.WriteLine("IndexMap = " + szTemp.GetValue(0).ToString());
#endif

                    }
                }
            }
            catch (Exception e)
            {
#if DEBUG
                MessageBox.Show(e.Message + " : " + e.InnerException + " : " + e.Source + " : " + e.StackTrace);       
#endif
                Console.WriteLine(e.Message + " : " + e.InnerException + " : " + e.Source + " : " + e.StackTrace);
            }

        }

        protected virtual void btnIndexMap_Click(object sender, EventArgs e)
        {
            spcIndexMap.Panel1Collapsed = !(spcIndexMap.Panel1Collapsed);
        }

        protected virtual void frmMap_Load(object sender, EventArgs e)
        {
            axMap.MapUnits = esriUnits.esriMeters;
        }

        protected virtual void axMap_OnAfterDraw(object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            esriViewDrawPhase viewDrawPhase = (esriViewDrawPhase)e.viewDrawPhase;
            if (viewDrawPhase == esriViewDrawPhase.esriViewForeground)
            {
                axIndexMap.Refresh(esriViewDrawPhase.esriViewForeground, null, null);
            }
        }

        protected virtual void axIndexMap_OnAfterDraw(object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            esriViewDrawPhase viewDrawPhase = (esriViewDrawPhase)e.viewDrawPhase;
            if (viewDrawPhase == esriViewDrawPhase.esriViewForeground)
            {
                //Get the IRGBColor interface
                IRgbColor color = new RgbColorClass();
                //Set the color properties
                color.RGB = 255;
                //Get the ILine symbol interface
                ILineSymbol outline = new SimpleLineSymbolClass();
                //Set the line symbol properties
                outline.Width = 1.5;
                outline.Color = color;
                //Get the IFillSymbol interface
                ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
                //Set the fill symbol properties
                simpleFillSymbol.Outline = outline;
                simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSHollow;

                object oLineSymbol = simpleFillSymbol;

                IGeometry geometry = axMap.Extent;

                axIndexMap.DrawShape(geometry, ref oLineSymbol);
            }
        }

        protected virtual void axIndexMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            if (e.button != 1) return;

            IEnvelope pEnvelope = null;
            IPoint pPoint = axIndexMap.ToMapPoint(e.x, e.y);
            pEnvelope = axIndexMap.TrackRectangle();
            if (!pEnvelope.IsEmpty)
            {
                axMap.Extent = pEnvelope;
            }
        }

        //내부 점인지 알아오기
        private Boolean IsPointIn(IEnvelope pEnvelpoe, IPoint pPoint)
        {
            return (pEnvelpoe.XMin <= pPoint.X && pEnvelpoe.XMax >= pPoint.X) && (pEnvelpoe.YMin <= pPoint.Y && pEnvelpoe.YMax >= pPoint.Y);
        }

        private void frmMap_ResizeBegin(object sender, EventArgs e)
        {
            axMap.SuppressResizeDrawing(true, 0);
        }

        private void frmMap_ResizeEnd(object sender, EventArgs e)
        {
            axMap.SuppressResizeDrawing(false, 0);
        }

        private void btnBlock_Click(object sender, EventArgs e)
        {
            spcTOC.Panel1Collapsed = !(spcTOC.Panel1Collapsed);
           
        }

        protected virtual void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {

        }

        protected virtual void axTOC_OnMouseDown(object sender, ITOCControlEvents_OnMouseDownEvent e)
        {
            //use HitTest in order to test whether the user has selected a featureLayer
            esriTOCControlItem item = esriTOCControlItem.esriTOCControlItemNone;
            IBasicMap map = null; ILayer layer = null;
            object other = null; object index = null;

            //do the HitTest
            axTOC.HitTest(e.x, e.y, ref item, ref map, ref layer, ref other, ref index);

            //Determine what kind of item has been clicked on
            if (null == layer || !(layer is IFeatureLayer))      return;

            //set the featurelayer as the custom property of the MapControl
            axMap.CustomProperty = layer;

            //멤버변수 : 작업레이어에 저장
            m_workLayer = layer;

            if (2 != e.button) return;
            //popup a context menu with a 'Properties' command
            contextMenuStripTOC.Show(axTOC, e.x, e.y);

        }

        #region TOC 팝업메뉴
        /// <summary>
        /// 축척설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemScale_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;

            Form frmScale = new frmSetScale(m_workLayer);
            frmScale.ShowDialog();

            //Fire contents changed event that the TOCControl listens to
            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }
        
        /// <summary>
        /// 주석설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemLabel_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;

            Form frmText = new frmSetText();
            frmText.ShowDialog();

            //Fire contents changed event that the TOCControl listens to
            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        /// <summary>
        /// 심볼설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemSymbol_Click(object sender, EventArgs e)
        {
            if (m_workLayer == null) return;

            IFeatureLayer featureLayer = m_workLayer as IFeatureLayer;
            if (featureLayer == null) return;

            IGeoFeatureLayer geoFeatureLayer = (IGeoFeatureLayer)featureLayer;
            ISimpleRenderer simpleRenderer = (ISimpleRenderer)geoFeatureLayer.Renderer;

            //Form frmSymbol = null;
            //Select SymbologyStyleClass based upon feature type
            //switch (featureLayer.FeatureClass.ShapeType)
            //{
            //    case esriGeometryType.esriGeometryPoint:
            //        frmSymbol = new frmSymbolSelect(simpleRenderer.Symbol, esriSymbologyStyleClass.esriStyleClassMarkerSymbols);
            //        //((frmSymbolSelect)frmSymbol).SelectSymbol(simpleRenderer.Symbol, esriSymbologyStyleClass.esriStyleClassMarkerSymbols);
            //        break;
            //    case esriGeometryType.esriGeometryPolyline:
            //        frmSymbol = new frmSymbolSelect(simpleRenderer.Symbol, esriSymbologyStyleClass.esriStyleClassLineSymbols);
            //        //((frmSymbolSelect)frmSymbol).SelectSymbol(simpleRenderer.Symbol, esriSymbologyStyleClass.esriStyleClassLineSymbols);
            //        break;
            //    case esriGeometryType.esriGeometryPolygon:
            //        frmSymbol = new frmSymbolSelect(simpleRenderer.Symbol, esriSymbologyStyleClass.esriStyleClassFillSymbols);
            //        //((frmSymbolSelect)frmSymbol).SelectSymbol(simpleRenderer.Symbol, esriSymbologyStyleClass.esriStyleClassFillSymbols);
            //        break;
            //}
            //ISymbol pSymbol = ((frmSymbolSelect)frmSymbol).Open();
            //if (pSymbol != null)
            //{
            //    //Create a new renderer
            //    simpleRenderer = new SimpleRendererClass();
            //    //Set its symbol from the styleGalleryItem
            //    simpleRenderer.Symbol = pSymbol;
            //    //Set the renderer into the geoFeatureLayer
            //    geoFeatureLayer.Renderer = (IFeatureRenderer)simpleRenderer;
            //}
 

            //Fire contents changed event that the TOCControl listens to
            axMap.ActiveView.ContentsChanged();
            //Refresh the display
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);


            //IFeatureLayer featureLayer = m_workLayer as IFeatureLayer;
            //if (featureLayer == null) return;

            //axMap.CustomProperty = m_workLayer;

            //IGeoFeatureLayer geoFeatureLayer = (IGeoFeatureLayer)featureLayer;
            //ISimpleRenderer simpleRenderer = (ISimpleRenderer)geoFeatureLayer.Renderer;

            ////Create the form with the SymbologyControl
            //frmSymbolSelector symbolForm = new frmSymbolSelector();

            ////Get the IStyleGalleryItem
            //IStyleGalleryItem styleGalleryItem = null;
            ////Select SymbologyStyleClass based upon feature type
            //switch (featureLayer.FeatureClass.ShapeType)
            //{
            //    case esriGeometryType.esriGeometryPoint:
            //        styleGalleryItem = symbolForm.GetItem(esriSymbologyStyleClass.esriStyleClassMarkerSymbols, simpleRenderer.Symbol);
            //        break;
            //    case esriGeometryType.esriGeometryPolyline:
            //        styleGalleryItem = symbolForm.GetItem(esriSymbologyStyleClass.esriStyleClassLineSymbols, simpleRenderer.Symbol);
            //        break;
            //    case esriGeometryType.esriGeometryPolygon:
            //        styleGalleryItem = symbolForm.GetItem(esriSymbologyStyleClass.esriStyleClassFillSymbols, simpleRenderer.Symbol);
            //        break;
            //}

            ////Release the form
            //symbolForm.Dispose();
            //this.Activate();

            //if (styleGalleryItem == null) return;

            ////Create a new renderer
            //simpleRenderer = new SimpleRendererClass();
            ////Set its symbol from the styleGalleryItem
            //simpleRenderer.Symbol = (ISymbol)styleGalleryItem.Item;
            ////Set the renderer into the geoFeatureLayer
            //geoFeatureLayer.Renderer = (IFeatureRenderer)simpleRenderer;

            ////Fire contents changed event that the TOCControl listens to
            //axMap.ActiveView.ContentsChanged();
            ////Refresh the display
            //axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, null, axMap.ActiveView.Extent);
        }

        #endregion

    }
}
