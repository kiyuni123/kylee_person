﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.DataSourcesFile;

namespace WaterNet.WaterAOCore
{
    public static class VariableManager
    {
        public static IAoInitialize m_AoInitialize; //ArcGIS Engine Licenses

        public static Dictionary<string, EAGL.Data.Wrapper.ITableWrapper> LAYER_DIC = new Dictionary<string, EAGL.Data.Wrapper.ITableWrapper>();

        public static IWorkspace m_Topographic = null;
        public static IWorkspace m_Pipegraphic = null;
        public static IWorkspace m_INPgraphic = null;
        public static string m_Netgraphic = string.Empty;
        public static string m_INPgraphicRootDirectory = string.Empty;

        public static IHookHelper m_HookHelper = new HookHelperClass();

        public static IMapControl3 m_Map = null;    //IMapControl3 = axMap.Object

        public static ISpatialReference m_spatialReference = null;

        public enum ShapeType
        {
            ShapePoint = 1,
            ShapePolyline = 2,
            ShapePolygon = 3
        }

        public enum FieldType
        {
            IntegerField = 1,
            DoubleField = 2,
            StringField = 3,
            DateField = 4
        }

        public struct FieldStruct
        {
            public string sFieldName;
            public string sAliasName;
            public FieldType sFieldType;
            public int sFieldLength;
            public int sScale;
        }

        /// <summary>
        /// ClassBreakRender
        /// </summary>
        public struct ClassBreakRender
        {
            public double dNumber;
            public string sLabel;
            public IRgbColor pLineColor;
            public IRgbColor pFillColor;
            public double dLineWidth;
        }

        /// <summary>
        /// 지형도, 항공영상 레이어
        /// </summary>
        public static Dictionary<string, ESRI.ArcGIS.Carto.ILayer> global_extLayer = new Dictionary<string, ESRI.ArcGIS.Carto.ILayer>();
    }
}
