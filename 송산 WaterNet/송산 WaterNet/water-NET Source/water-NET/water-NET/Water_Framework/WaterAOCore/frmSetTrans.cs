﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ESRI.ArcGIS.Carto;

namespace WaterNet.WaterAOCore
{
    public partial class frmSetTrans : Form
    {
        private ILayer m_Layer = null;
        public frmSetTrans()
        {
            InitializeComponent();
        }

        public frmSetTrans(ILayer pLayer)
        {
            InitializeComponent();

            m_Layer = pLayer;
        }

        private void frmSetTrans_Load(object sender, EventArgs e)
        {
            ILayerEffects pLayerEffect = m_Layer as ILayerEffects;
            if (pLayerEffect == null) this.Close();

            nudTrans.Value = (decimal)pLayerEffect.Transparency;

            this.BringToFront();

        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            ILayerEffects pLayerEffect = m_Layer as ILayerEffects;
            pLayerEffect.Transparency = (short)nudTrans.Value;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
