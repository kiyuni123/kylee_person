﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WaterAOCore
{
    public class CustomSimpleRenderer : IFeatureRenderer, ISimpleRenderer
    {
        private ILegendGroup m_LegendGroup = new LegendGroupClass();

        private ISymbol m_pSym = null;
        private ISimpleMarkerSymbol m_pMSym = new SimpleMarkerSymbolClass();
        private ISimpleLineSymbol m_pLSym = new SimpleLineSymbolClass();
        private ISimpleFillSymbol m_pFSym = new SimpleFillSymbolClass();
        private IRgbColor m_pColor = new RgbColorClass();
        private esriGeometryType m_geomType;

        public CustomSimpleRenderer()
        {
            ILegendClass pLegendClass = new LegendClassClass();
            m_LegendGroup.AddClass(pLegendClass);
            m_LegendGroup.Visible = true;
            m_LegendGroup.Editable = true;


            m_pColor.Red = 255;

            m_pMSym.Size = 12;
            m_pMSym.Color = m_pColor;
            m_pMSym.Style = esriSimpleMarkerStyle.esriSMSX;

            m_pLSym.Width = 1;
            m_pLSym.Color = m_pColor;
            m_pLSym.Style = esriSimpleLineStyle.esriSLSDashDotDot;

            m_pFSym.Color = m_pColor;
            m_pFSym.Style = esriSimpleFillStyle.esriSFSBackwardDiagonal;
        }

        public ISymbol symbol
        {
            get
            {
                return m_pSym;
            }
            set
            {
                m_pSym = value;
            }
        }

        #region IFeatureRenderer 멤버

        public bool CanRender(ESRI.ArcGIS.Geodatabase.IFeatureClass featClass, IDisplay Display)
        {
            return !(featClass.ShapeType == esriGeometryType.esriGeometryNull);
        }

        public void Draw(ESRI.ArcGIS.Geodatabase.IFeatureCursor cursor, esriDrawPhase DrawPhase, IDisplay Display, ITrackCancel TrackCancel)
        {
            if (Display != null) Display.SetSymbol(m_pSym);

            //ISymbol pSym = m_LegendGroup.get_Class(0).Symbol;
            //if (pSym == null) return;

            IFeature pFeature = cursor.NextFeature();
            while (pFeature != null)
            {
                IFeatureDraw pFD = pFeature as IFeatureDraw;
                pFD.Draw(DrawPhase, Display, m_pSym, false, null, esriDrawStyle.esriDSNormal);

                pFeature = cursor.NextFeature();
            }

        }

        public IFeatureIDSet ExclusionSet
        {
            set { throw new NotImplementedException(); }
        }

        public void PrepareFilter(ESRI.ArcGIS.Geodatabase.IFeatureClass fc, ESRI.ArcGIS.Geodatabase.IQueryFilter queryFilter)
        {
            m_geomType = fc.ShapeType;
            switch (m_geomType)
	        {
                case esriGeometryType.esriGeometryPoint:
                    m_pSym = (ISymbol)m_pMSym;
                    break;
                case esriGeometryType.esriGeometryPolygon:
                    m_pSym = (ISymbol)m_pFSym;
                    break;
                case esriGeometryType.esriGeometryPolyline:
                    m_pSym = (ISymbol)m_pLSym;
                    break;
                default:
                    break;
        	}

        }

        public bool get_RenderPhase(esriDrawPhase DrawPhase)
        {
            return (DrawPhase == esriDrawPhase.esriDPGeography);
        }

        public ISymbol get_SymbolByFeature(ESRI.ArcGIS.Geodatabase.IFeature Feature)
        {
            ;
            //ISymbol pSym = m_LegendGroup.get_Class(0).Symbol;
            //return pSym;
        }

        #endregion


        #region ISimpleRenderer 멤버

        public string Description
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Label
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public ISymbol Symbol
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
