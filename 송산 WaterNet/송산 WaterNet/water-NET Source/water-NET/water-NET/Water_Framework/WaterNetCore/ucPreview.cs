﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.Printing;
using Infragistics.Win.UltraWinToolbars;

namespace WaterNet.WaterNetCore
{
    public partial class ucPreview : UserControl
    {

        UltraGridPrintDocument m_ugPrintDocument = new UltraGridPrintDocument();
        UltraPrintPreviewDialog m_uPrintPreviewDialog = new UltraPrintPreviewDialog();

        UltraGrid m_uGrid = new UltraGrid();

        /// <summary>
        /// 수용가명
        /// </summary>
        public UltraGrid ucUltraGrid
        {
            set
            {
                m_uGrid = value;
            }
        }

        public ucPreview()
        {
            InitializeComponent();
        }

        #region Preview & Excel Function

        private void btnPreview_Click(object sender, EventArgs e)
        {
            this.Preview_Document();
        }

        private void Preview_Document()
        {
            this.Load_PrintDocument();

            this.m_uPrintPreviewDialog.ShowDialog(this);
        }

        private void Load_PrintDocument()
        {
            this.m_ugPrintDocument.Grid = this.m_uGrid;

            this.m_uPrintPreviewDialog.Document = this.m_ugPrintDocument;
            this.m_uPrintPreviewDialog.Name = "uPrintPreviewDialog";
            this.m_uPrintPreviewDialog.Load += new System.EventHandler(this.uPrintPreviewDialog_Load);
        }

        private void uPrintPreviewDialog_Load(object sender, System.EventArgs e)
        {
            this.m_uPrintPreviewDialog.Left = 0;
            this.m_uPrintPreviewDialog.Top = 0;
            this.m_uPrintPreviewDialog.Width = 1024;
            this.m_uPrintPreviewDialog.Height = 768;
            this.m_uPrintPreviewDialog.Style = ToolbarStyle.Office2010;
            this.m_uPrintPreviewDialog.PreviewSettings.Zoom = 100 * .01;
            this.m_uPrintPreviewDialog.Document.DocumentName = "프리뷰 셈플";
            this.m_uPrintPreviewDialog.Text = "프리뷰";
        }

        #endregion

    }
}
