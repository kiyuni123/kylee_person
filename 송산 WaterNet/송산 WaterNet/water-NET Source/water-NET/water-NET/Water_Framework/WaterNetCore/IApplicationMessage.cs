﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WaterNetCore
{
    public interface IApplicationMessage
    {
        void SetStatusMessage(string message);
    }
}
