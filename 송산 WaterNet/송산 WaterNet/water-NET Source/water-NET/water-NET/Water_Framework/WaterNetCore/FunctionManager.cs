﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using System.Security;
using System.Security.Cryptography;

namespace WaterNet.WaterNetCore
{
    public class FunctionManager
    {
        //public static ReadXml m_doc = null;

        /// <summary>
        /// INP Shape
        /// </summary>
        public enum INPShapeType
        {
            JUNCTIONS = 1,
            RESERVOIRS = 2,
            TANKS = 3,
            PIPES = 4,
            PUMPS = 5,
            VALVES = 6
        }

        #region SetStatusMessage - 메인화면의 상태바에 메세지 표시
        /// <summary>
        /// 메인화면의 상태바에 메세지 표시
        /// </summary>
        /// <param name="message">표시 메세지</param>
        public static void SetStatusMessage(string message)
        {
            Form oform = Application.OpenForms["frmLMain"];
            if (oform == null) return;

            if (oform is IApplicationMessage)
            {
                (oform as IApplicationMessage).SetStatusMessage(message);
            }
        }
        #endregion

        public static bool GetAvailableDirName(string Directory)
        {
            return (System.IO.Directory.Exists(Directory));
        }

        public static string GetFileExtension(string sFileName)
        {
            return (System.IO.Path.GetExtension(sFileName));
        }

        public static string GetWithoutExtension(string sFileName)
        {
            return (System.IO.Path.GetFileNameWithoutExtension(sFileName));
        }

        #region " String Max Value 찾기 - GetStringMaxValue()"
        public static string GetStringMaxValue(string strSQLScript)
        {
            OracleDBManager oDBManager = new OracleDBManager();
            string strResult = "";

            try
            {
                oDBManager.ConnectionString = GetConnectionString();
                oDBManager.Open();

                object o = oDBManager.ExecuteScriptScalar(strSQLScript, null);

                if (o != System.DBNull.Value)
                    strResult = (Convert.ToInt32(o) + 1).ToString();
                else
                    strResult = (Convert.ToInt32(1)).ToString();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
            finally
            {
                oDBManager.Close();
            }
            return strResult;
        }
        #endregion

        #region "연결 문자열 구하기 - GetConnectionString()"
        ///// <summary>
        ///// 연결 문자열 구하기
        ///// 연결환경을 XML file로 변경함
        ///// </summary>
        ///// <returns>연결 문자열</returns>
        ///// <remarks></remarks>
        //public static string GetConnectionString()
        //{
        //    if (m_doc == null)
        //    {
        //        m_doc = new ReadXml(AppStatic.DB_CONFIG_FILE_PATH);
        //    }
 
        //    string svcName = DecryptKey(m_doc.getProp("/Root/waternet/servicename"));
        //    string ip = DecryptKey(m_doc.getProp("/Root/waternet/ip"));
        //    string id = DecryptKey(m_doc.getProp("/Root/waternet/id"));
        //    string passwd = DecryptKey(m_doc.getProp("/Root/waternet/password"));
        //    string port = DecryptKey(m_doc.getProp("/Root/waternet/port"));

        //    //string conStr = "Data Source=(DESCRIPTION="
        //    //                + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=" + port + ")))"
        //    //                + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
        //    //                + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";

        //    string conStr = "Data Source=(DESCRIPTION="
        //        + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + ip + ")(PORT=" + port + ")))"
        //        + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + svcName + ")));"
        //        + "User Id=" + id + ";Password=" + passwd + ";Enlist=false";
        //    return conStr;
        //}

        public static string GetConnectionString()
        {
            return EMFrame.dm.EMapper.ConnectionString[EMFrame.statics.AppStatic.USER_SGCCD];

        }
        #endregion


        #region "데이터셋 가져오기 - GetDataSet(strSQLScript)"
        /// <summary>
        /// 데이터셋 가져오기
        /// </summary>
        /// <param name="strSQLScript">SQL 스크립트</param>
        /// <returns>데이터셋</returns>
        /// <remarks></remarks>
        public static DataSet GetDataSet(string strSQLScript)
        {
            OracleDBManager oDBManager = null;
            DataSet oDataSet = null;

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = GetConnectionString();
                oDBManager.Open();

                oDataSet = oDBManager.ExecuteScriptDataSet(strSQLScript, null, "Table");
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(" WaterNet.WaterNetCore", "FunctionManager", "GetData(strSQLScript)", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }

            return oDataSet;
        }

        public static DataSet GetDataSet(string strSQLScript, string tableName)
        {
            OracleDBManager oDBManager = null;
            DataSet oDataSet = null;

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = GetConnectionString();
                oDBManager.Open();

                oDataSet = oDBManager.ExecuteScriptDataSet(strSQLScript, null, tableName);
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(" WaterNet.WaterNetCore", "FunctionManager", "GetData(strSQLScript)", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }

            return oDataSet;
        }
        #endregion

        #region "스크립트 실행하기 - ExecuteScript(strSQLScript)"

        /// <summary>
        /// 스크립트 실행하기
        /// </summary>
        /// <param name="strSQLScript">SQL 스크립트</param>
        /// <remarks></remarks>
        public static int ExecuteScript(string strSQLScript, IDataParameter[] aoDataParameter)
        {
            OracleDBManager oDBManager = null;
            int nRowCount = 0;

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = GetConnectionString();
                oDBManager.Open();

                oDBManager.BeginTransaction();
                nRowCount = oDBManager.ExecuteScript(strSQLScript, aoDataParameter);
                oDBManager.CommitTransaction();
            }
            catch (ExceptionManager oExceptionManager)
            {
                oDBManager.RollbackTransaction();
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                oDBManager.RollbackTransaction();
                throw new ExceptionManager("WaterNet.WaterNetCore", "FunctionManager", "ExecuteScript()", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
            return nRowCount;
        }

        #endregion

        #region "스크립트 실행하기 - ExecuteScript(strSQLScript)"

        /// <summary>
        /// 스크립트 실행하기
        /// </summary>
        /// <param name="strSQLScript">SQL 스크립트</param>
        /// <remarks></remarks>
        public static object ExecuteScriptScalar(string strSQLScript, IDataParameter[] aoDataParameter)
        {
            OracleDBManager oDBManager = null;

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = GetConnectionString();
                oDBManager.Open();

                return oDBManager.ExecuteScriptScalar(strSQLScript, aoDataParameter);
            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.WaterNetCore", "FunctionManager", "ExecuteScript()", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
        }

        public static int ExecuteScriptCount(string strSQLScript, IDataParameter[] aoDataParameter)
        {
            OracleDBManager oDBManager = null;
            int nRowCount = 0;

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = GetConnectionString();
                oDBManager.Open();

                nRowCount = Convert.ToInt32(oDBManager.ExecuteScriptScalar(strSQLScript, aoDataParameter));

            }
            catch (ExceptionManager oExceptionManager)
            {
                throw oExceptionManager;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterNet.WaterNetCore", "FunctionManager", "ExecuteScript()", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
            return nRowCount;
        }
        #endregion

        #region DataTable에서 해당 컬럼가져오기
        /// <summary>
        /// DataTable에서 해당 컬럼가져오기
        /// </summary>
        /// <param name="table"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static DataColumn getDataColumn(DataTable table, string key)
        {
            DataColumn oDataColumn = null;

            //DataTable oDataTable = GetDataSource();
            if (table != null)
            {
                foreach (DataColumn item in table.Columns)
                {
                    if (item.ColumnName == key)
                    {
                        return item;
                    }
                }
            }

            return oDataColumn;
        }
        #endregion

        #region "관리번호(Max+1) 생성기 - GetMaxID(pTableName, pFieldName, nStart, nCount)"
        public static int GetMaxID(string pTableName, string pFieldName)
        {
            OracleDBManager oDBManager = null;

            StringBuilder sbSQLScript = new StringBuilder();
            sbSQLScript.AppendLine(" SELECT  MAX(" + pFieldName + ") FROM ");
            sbSQLScript.AppendLine(pTableName);

            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = GetConnectionString();
                oDBManager.Open();

                object o = oDBManager.ExecuteScriptScalar(sbSQLScript.ToString(), null);

                if (Convert.IsDBNull(o)) return 1;

                return Convert.ToInt32(o) + 1 ;

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(" WaterNet.WaterNetCore", "FunctionManager", "GetMaxID()", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }

        }

        //public static string GetMaxID(string pTableName, string pFieldName, int nStart, int nCount)
        //{
        //    OracleDBManager oDBManager = null;
        //    object pResult = null;

        //    StringBuilder sbSQLScript = new StringBuilder();
        //    sbSQLScript.AppendLine(" SELECT");
        //    sbSQLScript.AppendLine("    MAX(SUBSTR(" + pFieldName + "," + nStart + "," + nCount + ")) ");
        //    sbSQLScript.AppendLine("    FROM ");
        //    sbSQLScript.AppendLine(pTableName);
        //    sbSQLScript.AppendLine(" WHERE ");
        //    sbSQLScript.AppendLine(" SUBSTR(" + pFieldName + ",1,4) = TO_CHAR(SYSDATE, 'YYYY') ");
        //    sbSQLScript.AppendLine(" AND SGCCD = '" + VarManager.g_SiHookHelper.UserSGCCD + "'  ");

        //    try
        //    {
        //        oDBManager = new OracleDBManager();
        //        oDBManager.ConnectionString = FormManager.GetConnectionString();
        //        oDBManager.Open();

        //        pResult = oDBManager.ExecuteScriptScalar(sbSQLScript.ToString(), null);
        //        if (Information.IsDBNull(pResult))
        //        {
        //            return "";
        //        }

        //        return pResult;

        //    }
        //    catch (Exception oException)
        //    {
        //        throw new ExceptionManager("WaterWay", "frmCowkConsBook", "SetList", "에러", "에러");
        //    }
        //    finally
        //    {
        //        oDBManager.Close();
        //    }
        //}

        #endregion


        #region "데이터베이스 연도 구하기 - GetDBYear()"
        /// <summary>
        /// 데이터베이스 연도 구하기
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string GetDBYear()
        {
            OracleDBManager oDBManager = new OracleDBManager();

            try
            {
                oDBManager.ConnectionString = GetConnectionString();
                oDBManager.Open();

                object o = oDBManager.ExecuteScriptScalar("SELECT TO_CHAR(SYSDATE, 'YYYY') FROM DUAL ", null);

                if (Convert.IsDBNull(o)) return string.Empty;

                return Convert.ToString(o);
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterWay.TEwwwbCore", "FormManager", "GetDBYear()", oException, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
        }
        #endregion

        #region "데이터베이스 날짜 구하기 - GetDBDate()"

        /// <summary>
        /// 데이터베이스 날짜 구하기
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string GetDBDate()
        {
            OracleDBManager oDBManager = null;
            try
            {
                oDBManager = new OracleDBManager();
                oDBManager.ConnectionString = GetConnectionString();
                oDBManager.Open();

                object o = oDBManager.ExecuteScriptScalar("SELECT TO_CHAR(SYSDATE, 'yyyymmddhh24miss') FROM DUAL ", null);

                if (Convert.IsDBNull(o)) return string.Empty;

                return Convert.ToString(o);
            }
            catch (Exception oException)
            {
                throw new ExceptionManager("WaterWay.TEwwwbCore", "FormManager", "GetDBDate()", oException.Message, oException.GetType().Name);
            }
            finally
            {
                oDBManager.Close();
            }
        }

        #endregion

        #region "Directory 존재 여부 및 생성 및 Remove"

        /// <summary>
        /// Directory의 존재 여부를 반환한다.
        /// </summary>
        /// <param name="strDirPath">존재 여부를 알아 볼 Directory의 Full Path</param>
        public static bool IsDirectoryExists(string strDirPath)
        {
            bool blRtn;

            blRtn = Directory.Exists(strDirPath);

            return blRtn;
        }

        /// <summary>
        /// Directory를 생성한다.
        /// </summary>
        /// <param name="strDirPath">생성할 Directory의 Full Path</param>
        public static void CreateDirectory(string strDirPath)
        {
            Directory.CreateDirectory(strDirPath);
        }

        /// <summary>
        /// Directory를 Remove한다.(무조건 Remove)
        /// </summary>
        /// <param name="strDirPath">Remove할 Directory의 Full Path</param>
        /// <param name="SubDirDelete">하위 디렉토리 까지 삭제할지 여부</param>
        public static void RemoveDirectory(string strDirPath, bool SubDirDelete)
        {
            if (IsDirectoryExists(strDirPath))
            {
                Directory.Delete(strDirPath, SubDirDelete);    
            }
        }

        #endregion

        /// <summary>
        /// DateTime을 받아 YYYYMMDD Format의 String로 변환해 반환한다.
        /// </summary>
        /// <param name="oDateTime">DateTime Object</param>
        /// <returns>string YYYYMMDD</returns>
        public static string StringToDateTime(DateTime oDateTime)
        {
            string strRtn = string.Empty;

            strRtn = Convert.ToString(oDateTime.Year) + Convert.ToString(oDateTime.Month).PadLeft(2, '0') + Convert.ToString(oDateTime.Day).PadLeft(2, '0');

            return strRtn;

        }

        /// <summary>
        /// DateTime을 받아 YYYYMMDD Format의 String로 변환해 반환한다.
        /// </summary>
        /// <param name="oDateTime">DateTime Object</param>
        /// <returns>string YYYYMMDD</returns>
        public static string DateTimeToString(DateTime oDateTime)
        {
            string strRtn = string.Empty;

            strRtn = Convert.ToString(oDateTime.Year) + Convert.ToString(oDateTime.Month).PadLeft(2, '0') + Convert.ToString(oDateTime.Day).PadLeft(2, '0');

            return strRtn;

        }

        /// <summary>
        /// DateTime을 받아 YYYY-MM-DD Format의 String로 변환해 반환한다.
        /// </summary>
        /// <param name="oDateTime"></param>
        /// <returns></returns>
        public static string DateTimeToDashString(DateTime oDateTime)
        {
            string strRtn = string.Empty;

            strRtn = Convert.ToString(oDateTime.Year) + "-" + Convert.ToString(oDateTime.Month).PadLeft(2, '0') + "-" + Convert.ToString(oDateTime.Day).PadLeft(2, '0');

            return strRtn;
        }

        /// <summary>
        /// 문자열에서 해당 Char를 삭제한다.
        /// </summary>
        /// <param name="ostring"></param>
        /// <param name="cDelemeter"></param>
        /// <returns></returns>
        public static string getRemovedChar(string ostring, char cDelemeter)
        {
            string o = string.Empty;
            for (int i = 0; i < ostring.Length; i++)
            {
                if (ostring[i] != cDelemeter)
                {
                    o += ostring[i];
                }
            }
            return o;
        }

        public static double getRound(object value, int digit)
        {
            double dvalue = 0.0;
            dvalue = Convert.ToDouble(value);

            return getRound(dvalue, digit);
        }

        public static double getRound(string value, int digit)
        {
            double dvalue = 0.0;
            dvalue = Convert.ToDouble(value);

            return getRound(dvalue, digit);
        }

        public static double getRound(double value, int digit)
        {
            return Math.Round(value, digit);
        }

        //#region C# 암호화관련 Function

        ///// <summary>
        ///// 현재는 8자리의 암호키를 설정해야함.
        ///// 추후 기능개선 필요
        ///// </summary>
        ///// <param name="inputString"></param>
        ///// <returns></returns>
        //public static string EncryptKey(string strToEncrypt)
        //{
        //    try
        //    {
        //        byte[] key = { };
        //        byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
        //        byte[] inputByteArray; //Convert.ToByte(stringToEncrypt.Length) 

        //        key = Encoding.UTF8.GetBytes("WATERNET");
        //        DESCryptoServiceProvider des = new DESCryptoServiceProvider();
        //        inputByteArray = Encoding.UTF8.GetBytes(strToEncrypt);
        //        MemoryStream ms = new MemoryStream();
        //        CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
        //        cs.Write(inputByteArray, 0, inputByteArray.Length);
        //        cs.FlushFinalBlock();
        //        return Convert.ToBase64String(ms.ToArray());
        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw new Exception(ex.Message.ToString());
        //    }
        //}

        ///// <summary>
        ///// 현재는 8자리의 암호키를 설정해야함.[추후 기능개선 필요]
        ///// </summary>
        ///// <param name="inputString"></param>
        ///// <returns></returns>
        //public static string DecryptKey(string strToDecrypt)
        //{
        //    try
        //    {
        //        byte[] key = { };
        //        byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
        //        byte[] inputByteArray = new byte[strToDecrypt.Length];
        //        key = Encoding.UTF8.GetBytes("WATERNET");
        //        DESCryptoServiceProvider des = new DESCryptoServiceProvider();

        //        inputByteArray = Convert.FromBase64String(strToDecrypt);
        //        MemoryStream ms = new MemoryStream();
        //        CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
        //        cs.Write(inputByteArray, 0, inputByteArray.Length);
        //        cs.FlushFinalBlock();
        //        Encoding encoding = Encoding.UTF8;
        //        return encoding.GetString(ms.ToArray());
        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw new Exception(ex.Message.ToString());
        //    }
        //}

        //#endregion

        //---------------------------------------------------------------------------

        public class ReadXml
        {
            private XmlDocument m_doc = null;

            public ReadXml(string strFilePath)
            {
                m_doc = new XmlDocument();
                m_doc.Load(strFilePath);
            }

            public XmlDocument doc
            {
                get
                {
                    return m_doc;
                }
            }

            /// <summary>
            /// XML 파일에서 xpath에 의한 값을 리턴한다.
            /// </summary>
            /// <param name="key">xpath</param>
            /// <returns>노드 값(여기서는 설정 값)</returns>
            public string getProp(string key)
            {
                string returnValue = "";

                XmlNodeList nodes = m_doc.SelectNodes(key);

                foreach (XmlNode node in nodes)
                {
                    returnValue = node.InnerText;
                }

                return returnValue;
            }

        }
    }

 

 
}
