﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WaterNetCore
{
    /// <summary>
    /// Water-NET 프로그램을 사용하는데 있어 공통 정보를 갖는 Class
    /// </summary>
    public class AppStatic
    {
        #region 로그온한 사용자 정보

        public static string USER_ID = string.Empty;        //로그온한 사용자 ID
        public static string USER_NAME = string.Empty;      //로그온한 사용자 성명
        public static string USER_DIVISION = string.Empty;  //로그온한 사용자 부서
        public static string USER_RIGHT = string.Empty;     //로그온한 사용자 권한
        public static string USER_IP = string.Empty;        //로그온한 사용자의 IP
        public static string USER_SGCCD = string.Empty;     //로그온한 사용자의 지자체 코드
        public static string USER_SGCNM = string.Empty;     //로그온한 사용자의 지자체 명

        #endregion

        #region Config File 정보

        public static string DB_CONFIG_FILE_PATH = Application.StartupPath + @"\Config\DB_Config.xml";        //Database Config File 경로 및 파일 명

        #endregion

    }
}
