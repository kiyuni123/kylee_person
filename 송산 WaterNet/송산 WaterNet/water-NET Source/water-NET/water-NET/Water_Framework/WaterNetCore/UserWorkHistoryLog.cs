﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMFrame.statics;

namespace WaterNet.WaterNetCore
{
    public class UserWorkHistoryLog
    {
        public static void INSERT(string MENU_ID, string WORK_DESCRIPTION)
        {
            InsertUserWorkHistory(GetMenuFullName(MENU_ID), "입력", WORK_DESCRIPTION);
        }

        public static void UPDATE(string MENU_ID, string WORK_DESCRIPTION)
        {
            InsertUserWorkHistory(GetMenuFullName(MENU_ID), "수정", WORK_DESCRIPTION);
        }

        public static void DELETE(string MENU_ID, string WORK_DESCRIPTION)
        {
            InsertUserWorkHistory(GetMenuFullName(MENU_ID), "삭제", WORK_DESCRIPTION);
        }

        private static void InsertUserWorkHistory(string MENU_NAME, string WORK_KIND, string WORK_DESCRIPTION)
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("INSERT INTO CM_WORK_HISTORY");
            sb.AppendLine("SELECT '" + AppStatic.USER_ID + "'");
            sb.AppendLine("      ,TO_DATE('" + DateTime.Now.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')");
            sb.AppendLine("      ,'" + MENU_NAME + "'");
            sb.AppendLine("      ,'" + WORK_KIND + "'");
            sb.AppendLine("      ,'" + WORK_DESCRIPTION + "'");
            sb.AppendLine("  FROM DUAL");

            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper(AppStatic.USER_SGCCD);
                mapper.ExecuteScript(sb.ToString(), null);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }
        }

        private static string GetMenuFullName(string MENU_ID)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("SELECT MU_NM");
            sb.AppendLine("  FROM MENU_MASTER");
            sb.AppendLine(" START WITH MU_ID = '" + MENU_ID + "'");
            sb.AppendLine("CONNECT BY PRIOR P_MU_ID = MU_ID");
            sb.AppendLine(" ORDER BY ROWNUM DESC");

            string result = string.Empty;
            System.Data.DataTable data = null;
            EMFrame.dm.EMapper mapper = null;

            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");
                data = mapper.ExecuteScriptDataTable(sb.ToString(), null);

                foreach (System.Data.DataRow row in data.Rows)
                {
                    result += row["MU_NM"].ToString();

                    if (data.Rows.Count - 1 != data.Rows.IndexOf(row))
                    {
                        result += " => ";
                    }
                }
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }
    }
}
