﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace WaterNet.WaterNetCore
{
    #region Log Media 종류 - LogMediaType

    /// <summary>
    /// Log Media 종류
    /// </summary>
    public enum LogMediaType
    {
        System,
        Database,
        File
    }

    #endregion

    /// <summary>
    /// Log 관리자
    /// </summary>
    public class LogManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////// Method

        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region 작성하기 - Write(oExceptionManager, eLogMediaType)

        /// <summary>
        /// 작성하기
        /// </summary>
        /// <param name="oExceptionManager">예외 관리자</param>
        /// <param name="eLogMediaType">Log Media 종류</param>
        public static void Write(ExceptionManager oExceptionManager, LogMediaType eLogMediaType)
        {
            switch(eLogMediaType)
            {
                case LogMediaType.System :
                    WriteToSystem(oExceptionManager);                
                    break;
                case LogMediaType.Database :
                    WriteToDatabase(oExceptionManager);
                    break;
                case LogMediaType.File :
                    WriteToFile(oExceptionManager);
                    break;
            }
        }

        #endregion

        //#region 작성하기 - Write(oPerformanceManager, eLogMediaType)

        ///// <summary>
        ///// 작성하기
        ///// </summary>
        ///// <param name="oPerformanceManager">성능 관리자</param>
        ///// <param name="eLogMediaType">Log Media 종류</param>
        //public static void Write(PerformanceManager oPerformanceManager, LogMediaType eLogMediaType)
        //{
        //    switch(eLogMediaType)
        //    {
        //        case LogMediaType.System :
        //            WriteToSystem(oPerformanceManager);                
        //            break;
        //        case LogMediaType.Database :
        //            WriteToDatabase(oPerformanceManager);
        //            break;
        //        case LogMediaType.File :
        //            WriteToFile(oPerformanceManager);
        //            break;
        //    }
        //}

        //#endregion

        ////////////////////////////////////////////////////////////////////////////////////////// Protected

        #region System에 작성하기 - WriteToSystem(oExceptionManager)

        /// <summary>
        /// System에 작성하기
        /// </summary>
        /// <param name="oExceptionManager">예외 관리자</param>
        protected static void WriteToSystem(ExceptionManager oExceptionManager)
        {
            string strLogName = oExceptionManager.ProgramName;
            string strSource  = oExceptionManager.NamespaceName + "." + oExceptionManager.ClassName;
            string strMessage = oExceptionManager.InnerException.Message;

            try
            {
                if(!EventLog.SourceExists(strSource))
                {
                    EventLog.CreateEventSource(strSource, strLogName);
                }

                EventLog oEventLog = new EventLog();
                
                oEventLog.Log = strLogName;
                oEventLog.Log = strSource;
                
                oEventLog.WriteEntry(strMessage, EventLogEntryType.Error);
            }
            catch(Exception oException)
            {
                throw oException;
            }
        }

        #endregion

        //#region System에 작성하기 - WriteToSystem(oPerformanceManager)

        ///// <summary>
        ///// System에 작성하기
        ///// </summary>
        ///// <param name="oPerformanceManager">성능 관리자</param>
        //protected static void WriteToSystem(PerformanceManager oPerformanceManager)
        //{
        //    string strLogName = oPerformanceManager.ProgramName;
        //    string strSource  = oPerformanceManager.NamespaceName + "." + oPerformanceManager.ClassName;
        //    string strMessage = string.Format
        //    (
        //        "{0}.{1}.{2}\r\n시작 시각:{3}\r\n종료 시각:{4}\r\n경과 시간:{5}\r\n성공 여부:{6}",
        //        oPerformanceManager.NamespaceName,
        //        oPerformanceManager.ClassName,
        //        oPerformanceManager.MethodName,
        //        oPerformanceManager.StartTime.ToLongTimeString(),
        //        oPerformanceManager.EndTime.ToLongTimeString(),
        //        ((TimeSpan)(oPerformanceManager.StartTime - oPerformanceManager.EndTime)).ToString(),
        //        oPerformanceManager.IsSuccessful.ToString()
        //    );

        //    try
        //    {
        //        if(!EventLog.SourceExists(strSource))
        //        {
        //            EventLog.CreateEventSource(strSource, strLogName);
        //        }

        //        EventLog oEventLog = new EventLog();
                
        //        oEventLog.Log = strLogName;
        //        oEventLog.Log = strSource;
                
        //        oEventLog.WriteEntry(strMessage, EventLogEntryType.Error);
        //    }
        //    catch(Exception oException)
        //    {
        //        throw oException;
        //    }
        //}

        //#endregion

        #region Database에 작성하기 - WriteToDatabase(oExceptionManager)

        /// <summary>
        /// Database에 작성하기
        /// </summary>
        /// <param name="oExceptionManager">예외 관리자</param>
        protected static void WriteToDatabase(ExceptionManager oExceptionManager)
        {
            // TODO : 추후 작성 예정
        }

        #endregion

        //#region Database에 작성하기 - WriteToDatabase(oPerformanceManager)

        ///// <summary>
        ///// Database에 작성하기
        ///// </summary>
        ///// <param name="oPerformanceManager">성능 관리자</param>
        //protected static void WriteToDatabase(PerformanceManager oPerformanceManager)
        //{
        //    // TODO : 추후 작성 예정
        //}

        //#endregion

        #region File에 작성하기 - WriteToFile(oExceptionManager)

        /// <summary>
        /// File에 Log 작성하기
        /// </summary>
        /// <param name="oExceptionManager">예외 관리자</param>
        protected static void WriteToFile(ExceptionManager oExceptionManager)
        {
            try
            {
                string strLogPath = string.Empty;
                string strLogFileName = string.Empty;
                string strLogData = string.Empty;
                string strLogDate = string.Empty;
                string strLogDateTime = string.Empty;

                string strLogName = oExceptionManager.ProgramName;
                string strSource  = oExceptionManager.NamespaceName + "." + oExceptionManager.ClassName;
                string strMessage = oExceptionManager.InnerException.Message;

                strLogDate = Convert.ToString(DateTime.Now.Year) + Convert.ToString(DateTime.Now.Month).PadLeft(2, '0') + Convert.ToString(DateTime.Now.Day).PadLeft(2, '0');
                strLogDateTime = strLogDate + " " + Convert.ToString(DateTime.Now.Hour).PadLeft(2, '0') + ":" + Convert.ToString(DateTime.Now.Minute).PadLeft(2, '0') + ":" + Convert.ToString(DateTime.Now.Second).PadLeft(2, '0');
                strLogPath = Application.StartupPath.ToString() + @"\Log\";
                strLogFileName = "SYS" + strLogDate + ".LOG";

                //Download할 Directory가 없다면 생성한다.
                if (!(FunctionManager.IsDirectoryExists(strLogPath)))
                {
                    FunctionManager.CreateDirectory(strLogPath);
                }

                strLogData = "[ " + strLogDateTime + " ] < " + strLogName + " > < " + strSource + " > " + strMessage + "\r\n";

                //FileStream 객체를 생성한다. 화일이 존재하지 않으면 새로 만들고 읽기쓰기 모드로 연다.
                FileStream oFileStream = new FileStream(strLogPath + strLogFileName, FileMode.Append, FileAccess.Write, FileShare.Write);
                StreamWriter oStreamWriter = new StreamWriter(oFileStream);

                //Log 내용을 쓴다.
                oStreamWriter.Write(strLogData);

                //버퍼를 비운다.
                //화일을 닫는다.
                oStreamWriter.Flush();
                oStreamWriter.Close();
            }
            catch
            {
                return;
            }
        }

        #endregion

        //#region File에 작성하기 - WriteToFile(oPerformanceManager)

        ///// <summary>
        ///// File에 작성하기
        ///// </summary>
        ///// <param name="oPerformanceManager">성능 관리자</param>
        //protected static void WriteToFile(PerformanceManager oPerformanceManager)
        //{
        //    // TODO : 추후 작성 예정
        //}

        //#endregion
    }
}

