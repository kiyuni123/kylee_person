﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WaterNetCore
{
    public partial class frmSplashMsg : Form
    {
        private string MSG = "작업중입니다......";

        public frmSplashMsg()
        {
            InitializeComponent();

            m_lblMsg.Parent = picIntro;
            m_lblMsg.BackColor = Color.Transparent;
            m_lblMsg.BringToFront();
        }

        public string Message
        {
            set { 
                MSG = value;
                m_lblMsg.Text = MSG;
                System.Windows.Forms.Application.DoEvents();
                //this.Refresh();
            }
        }

        public void Open()
        {
            this.Opacity = 1.0;
            m_lblMsg.Text = MSG;
            this.BringToFront();
            //System.Windows.Forms.Application.DoEvents();
            this.StartPosition = FormStartPosition.CenterScreen;
            System.Windows.Forms.Application.DoEvents();
            this.Show();
            //System.Windows.Forms.Application.DoEvents();
        }
    }
}
