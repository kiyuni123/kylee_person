﻿namespace WaterNet.WaterNetCore
{
    partial class frmSplashMsg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSplashMsg));
            this.picIntro = new System.Windows.Forms.PictureBox();
            this.m_lblMsg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picIntro)).BeginInit();
            this.SuspendLayout();
            // 
            // picIntro
            // 
            this.picIntro.BackColor = System.Drawing.Color.White;
            this.picIntro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picIntro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picIntro.Image = ((System.Drawing.Image)(resources.GetObject("picIntro.Image")));
            this.picIntro.InitialImage = ((System.Drawing.Image)(resources.GetObject("picIntro.InitialImage")));
            this.picIntro.Location = new System.Drawing.Point(0, 0);
            this.picIntro.Name = "picIntro";
            this.picIntro.Size = new System.Drawing.Size(636, 194);
            this.picIntro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picIntro.TabIndex = 2;
            this.picIntro.TabStop = false;
            this.picIntro.UseWaitCursor = true;
            this.picIntro.WaitOnLoad = true;
            // 
            // m_lblMsg
            // 
            this.m_lblMsg.AutoSize = true;
            this.m_lblMsg.BackColor = System.Drawing.Color.Transparent;
            this.m_lblMsg.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.m_lblMsg.ForeColor = System.Drawing.Color.Red;
            this.m_lblMsg.Location = new System.Drawing.Point(196, 50);
            this.m_lblMsg.Name = "m_lblMsg";
            this.m_lblMsg.Size = new System.Drawing.Size(50, 19);
            this.m_lblMsg.TabIndex = 3;
            this.m_lblMsg.Text = "label1";
            this.m_lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.m_lblMsg.UseWaitCursor = true;
            // 
            // frmSplashMsg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(636, 194);
            this.Controls.Add(this.m_lblMsg);
            this.Controls.Add(this.picIntro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSplashMsg";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmSplashMsg";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.UseWaitCursor = true;
            ((System.ComponentModel.ISupportInitialize)(this.picIntro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picIntro;
        private System.Windows.Forms.Label m_lblMsg;
    }
}