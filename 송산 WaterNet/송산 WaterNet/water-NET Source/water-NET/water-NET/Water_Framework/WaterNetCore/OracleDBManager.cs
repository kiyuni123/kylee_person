﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace WaterNet.WaterNetCore
{
    /// <summary>
    /// 오라클 데이터베이스 관리자
    /// </summary>
    /// <remarks></remarks>
    public class OracleDBManager
    {

        ////////////////////////////////////////////////////////////////////////////////////////////////////// Field

        //////////////////////////////////////////////////////////////////////////////////////////// Private

        #region "필드 선언"

        // 연결
        private OracleConnection m_oConnection = null;
        // 연결 문자열
        private string m_strConnectionString = null;
        // 트랜잭션
        private OracleTransaction m_oTransaction = null;

        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////////////////// Property

        #region "연결 - Connection"

        /// <summary>
        /// 연결
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public OracleConnection Connection
        {
            get { return m_oConnection; }

            set { m_oConnection = value; }
        }

        #endregion

        #region "연결 문자열 - ConnectionString"

        /// <summary>
        /// 연결 문자열
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string ConnectionString
        {
            get { return m_strConnectionString; }

            set { m_strConnectionString = value; }
        }

        #endregion

        #region "트랜잭션 - Transaction"

        /// <summary>
        /// 트랜잭션
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public OracleTransaction Transaction
        {
            get { return m_oTransaction; }

            set { m_oTransaction = value; }
        }


        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////////////////// Method

        //////////////////////////////////////////////////////////////////////////////////////////// Public

        ////////////////////////////////////////////////////////////////////////////////// Common

        #region "열기 - Open()"

        /// <summary>
        /// 열기
        /// </summary>
        /// <remarks></remarks>

        public void Open()
        {
            if (string.IsNullOrEmpty(m_strConnectionString))
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "Open()", "설정된 연결 문자열이 없습니다.", "");
            }

            if (m_oConnection == null)
            {
                try
                {
                    m_oConnection = new OracleConnection();
                }
                catch (Exception oException)
                {
                    throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "Open()", oException.Message, oException.GetType().Name);
                }
            }

            if (m_oConnection.State == ConnectionState.Closed)
            {
                try
                {
                    m_oConnection.ConnectionString = m_strConnectionString;
                    m_oConnection.Open();
                }
                catch (Exception oException)
                {
                    throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "Open()", oException.Message, oException.GetType().Name);
                }
            }
            else
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "Open()", "이미 연결되어 있습니다.", "");
            }

        }

        #endregion

        #region "트랜잭션 시작하기 - BeginTransaction()"

        /// <summary>
        /// 트랜잭션 시작하기
        /// </summary>
        /// <remarks></remarks>
        public void BeginTransaction()
        {
            if (m_oTransaction == null)
            {
                if (m_oConnection == null)
                {
                    throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "BeginTransaction()", "설정된 연결이 없습니다.", "");
                }
                else
                {
                    if (m_oConnection.State == ConnectionState.Open)
                    {
                        try
                        {
                            m_oTransaction = m_oConnection.BeginTransaction();
                        }
                        catch (Exception oException)
                        {
                            throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "BeginTransaction()", oException.Message, oException.GetType().Name);
                        }
                    }
                    else
                    {
                        throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "BeginTransaction()", "연결이 트랜잭션을 시작할 수 없습니다.", "");
                    }
                }
            }
            else
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "BeginTransaction()", "트랜잭션이 이미 설정되어 있습니다.", "");
            }
        }

        #endregion

        #region "트랜잭션 완료하기 - CommitTransaction()"

        /// <summary>
        /// 트랜잭션 완료하기
        /// </summary>
        /// <remarks></remarks>
        public void CommitTransaction()
        {
            if (m_oTransaction == null)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "CommitTransaction()", "설정된 트랜잭션이 없습니다.", "");
            }
            else
            {
                try
                {
                    m_oTransaction.Commit();
                }
                catch (Exception oException)
                {
                    throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "CommitTransaction()", oException.Message, oException.GetType().Name);
                }
            }
        }

        #endregion

        #region "트랜잭션 취소하기 - RollbackTransaction()"

        /// <summary>
        /// 트랜잭션 취소하기
        /// </summary>
        /// <remarks></remarks>
        public void RollbackTransaction()
        {
            if (m_oTransaction == null)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "RollbackTransaction()", "설정된 트랜잭션이 없습니다.", "");
            }
            else
            {
                try
                {
                    m_oTransaction.Rollback();
                }
                catch (Exception oException)
                {
                    throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "RollbackTransaction()", oException.Message, oException.GetType().Name);
                }
            }
        }

        #endregion

        #region "닫기 - Close()"

        /// <summary>
        /// 닫기
        /// </summary>
        /// <remarks></remarks>
        public void Close()
        {
            if (m_oConnection != null)
            {
                if (m_oConnection.State != ConnectionState.Closed)
                {
                    try
                    {
                        m_oConnection.Close();
                        m_oConnection.Dispose();
                        m_oConnection = null;
                    }
                    catch (Exception oException)
                    {
                        throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "Close()", oException.Message, oException.GetType().Name);
                    }
                }
            }
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////// Script

        //////////////////////////////////////////////////////////////////////// Insert, Update, Delete

        #region "스크립트 실행하기 - ExecuteScript(strScript, aoDataParameter)"

        /// <summary>
        /// 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public int ExecuteScript(string strScript, IDataParameter[] aoDataParameter)
        {
            int nRowCount = 0;

            ExecuteScript(strScript, aoDataParameter, ref nRowCount);

            return nRowCount;
        }
        #endregion

        #region "스크립트 실행하기 - ExecuteScript(strScript, aoDataParameter, nRowCount)"

        /// <summary>
        /// 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="nRowCount">행 수</param>
        /// <remarks></remarks>
        public void ExecuteScript(string strScript, IDataParameter[] aoDataParameter, ref int nRowCount)
        {
            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

                nRowCount = oCommand.ExecuteNonQuery();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteScript(strScript, aoDataParameter, nRowCount)", oException.Message, oException.GetType().Name);
            }
        }

        #endregion

        #region "결과값을 돌려주는 스크립트 실행하기 - ExecuteScriptReturnValue(strScript, aoDataParameter, eSqlDbTyoe, nSize)"

        /// <summary>
        /// 결과값을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteScriptReturnValue(string strScript, IDataParameter[] aoDataParameter, DbType eDbType, int nSize)
        {
            int nRowCount = 0;

            return ExecuteScriptReturnValue(strScript, aoDataParameter, eDbType, nSize, ref nRowCount);
        }

        #endregion

        #region "결과값을 돌려주는 스크립트 실행하기 - ExecuteScriptReturnValue(strScript, aoDataParameter, eSqlDbTyoe, nSize, nRowCount)"

        /// <summary>
        /// 결과값을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">SQL 데이터 종류</param>
        /// <param name="nSize">크기</param>
        /// <param name="nRowCount">행 수</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteScriptReturnValue(string strScript, IDataParameter[] aoDataParameter, DbType eDbType, int nSize, ref int nRowCount)
        {
            object oReturnValue = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommandReturnValue(strScript, aoDataParameter, eDbType, nSize);

                nRowCount = oCommand.ExecuteNonQuery();

                oReturnValue = oCommand.Parameters["ReturnValue"].Value;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteScriptReturnValue(strScript, aoDataParameter, eSqlDbTyoe, nSize, byPrecision, byScale)", oException.Message, oException.GetType().Name);
            }

            return oReturnValue;
        }

        #endregion

        #region "해시 테이블을 돌려주는 스크립트 실행하기 - ExecuteScriptHashtable(strScript, aoDataParameter)"

        /// <summary>
        /// 해시 테이블을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable ExecuteScriptHashtable(string strScript, IDataParameter[] aoDataParameter)
        {
            Hashtable oHashtable = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

                oCommand.ExecuteNonQuery();

                oHashtable = new Hashtable();

                foreach (OracleParameter oParameter in oCommand.Parameters)
                {
                    if (oParameter.Direction == ParameterDirection.InputOutput | oParameter.Direction == ParameterDirection.Output)
                    {
                        oHashtable.Add(oParameter.ParameterName, oParameter.Value);
                    }
                }
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteScriptHashtable(strScript, aoDataParameter)", oException.Message, oException.GetType().Name);
            }

            return oHashtable;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////// Select

        #region "스칼라 값을 돌려주는 스크립트 실행하기 - ExecuteScriptScalar(strScript, aoDataParameter)"

        /// <summary>
        /// 스칼라 값을 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>스칼라 값</returns>
        /// <remarks></remarks>
        public object ExecuteScriptScalar(string strScript, IDataParameter[] aoDataParameter)
        {
            object oScalar = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

                oScalar = oCommand.ExecuteScalar();

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteScriptScalar(strScript, aoDataParameter)", oException.Message, oException.GetType().Name);
            }

            return oScalar;
        }

        #endregion

        #region "데이터 리더를 돌려주는 스크립트 실행하기 - ExecuteScriptDataReader(strScript, aoDataParameter)"

        /// <summary>
        /// 데이터 리더를 돌려주는 스크립트 실행하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>데이터 리더</returns>
        /// <remarks></remarks>
        public OracleDataReader ExecuteScriptDataReader(string strScript, IDataParameter[] aoDataParameter)
        {
            OracleDataReader oDataReader = null;

            try
            {
                OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

                oDataReader = oCommand.ExecuteReader();
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteScriptOracleDataReader(strScript, aoDataParameter)", oException.Message, oException.GetType().Name);
            }

            return oDataReader;
        }

        #endregion

        #region "데이터Table을 돌려주는 스크립트 실행하기 - ExecuteScriptDataTable(strScript, aoDataParameter, strTableName)"
        public DataTable ExecuteScriptDataTable(string strScript, IDataParameter[] aoDataParameter)
        {
            return ExecuteScriptDataTable(strScript, aoDataParameter, "Table0");
        }

        public DataTable ExecuteScriptDataTable(string strScript, IDataParameter[] aoDataParameter, string strTableName)
        {
            if (string.IsNullOrEmpty(strScript)) return null;

            DataTable oDataTable = new DataTable();
            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataTable.TableName = strTableName;
                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.Fill(oDataTable);
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.ToString());
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteScriptDataSet(strScript, aoDataParameter, strTableName)", oException.Message, oException.GetType().Name);
            }

            return oDataTable;
        }
        #endregion

        #region "데이터셋을 돌려주는 스크립트 실행하기 - ExecuteScriptDataSet(strScript, aoDataParameter, strTableName)"
        public DataSet ExecuteScriptDataSet(string strScript, IDataParameter[] aoDataParameter)
        {
            DataSet oDataSet = null;

            if (string.IsNullOrEmpty(strScript)) return null;

            try
            {
                oDataSet = new DataSet();
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();

                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                //oDataAdapter.ReturnProviderSpecificTypes = true;
                //oDataAdapter.FillLoadOption = LoadOption.OverwriteChanges;
                oDataAdapter.Fill(oDataSet, "Table0");
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteScriptDataSet(strScript, aoDataParameter, strTableName)", oException.Message, oException.GetType().Name);
            }

            return oDataSet;

        }

        public DataSet ExecuteScriptDataSet(string strScript, IDataParameter[] aoDataParameter, string strTableName)
        {
            DataSet oDataSet = null;

            if (string.IsNullOrEmpty(strScript)) return null;
            
            try
            {
                oDataSet = new DataSet();
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();
                oDataAdapter.Requery = false;
                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                oDataAdapter.FillLoadOption = LoadOption.OverwriteChanges;
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteScriptDataSet(strScript, aoDataParameter, strTableName)", oException.Message, oException.GetType().Name);
            }

            return oDataSet;

        }

        #endregion

        #region "스크립트를 실행해 데이터셋 채우기 - FillDataSetScript(oDataSet, strTableName, strScript, aoDataParameter)"

        /// <summary>
        /// 스크립트를 실행해 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public void FillDataSetScript(DataSet oDataSet, string strTableName, string strScript, IDataParameter[] aoDataParameter)
        {
            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();

                oDataAdapter.SelectCommand = CreateScriptCommand(strScript, aoDataParameter);
                //oDataAdapter.ReturnProviderSpecificTypes = true;
                //oDataAdapter.FillLoadOption = LoadOption.OverwriteChanges;
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
                //throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "FillDataSetScript(strScript, aoDataParameter, strTableName)", oException.Message, oException.GetType().Name);
            }
        }

        #endregion

        #region "스크립트를 실행해 해시 테이블을 돌려주고 데이터셋 채우기 - FillDataSetScriptHashtable(oDataSet, strTableName, strScript, aoDataParameter)"

        /// <summary>
        /// 스크립트를 실행해 해시 테이블을 돌려주고 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable FillDataSetScriptHashtable(DataSet oDataSet, string strTableName, string strScript, IDataParameter[] aoDataParameter)
        {
            Hashtable oHashtable = null;

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();

                oDataAdapter.SelectCommand = CreateStoredProcedureCommand(strScript, aoDataParameter);
                //oDataAdapter.ReturnProviderSpecificTypes = true;
                //oDataAdapter.FillLoadOption = LoadOption.OverwriteChanges;
                oDataAdapter.Fill(oDataSet, strTableName);

                foreach (OracleParameter oParameter in oDataAdapter.SelectCommand.Parameters)
                {
                    if (oParameter.Direction == ParameterDirection.InputOutput | oParameter.Direction == ParameterDirection.Output)
                    {
                        oHashtable.Add(oParameter.ParameterName, oParameter.Value);
                    }
                }
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "FillDataSetScriptHashtable(oDataSet, strTableName, strScript, aoDataParameter)", oException.Message, oException.GetType().Name);
            }

            return oHashtable;
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////// Stored Procedure

        //////////////////////////////////////////////////////////////////////// Insert, Update, Delete

        #region "저장 프로시저 실행하기 - ExecuteStoredProcedure(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public void ExecuteStoredProcedure(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            int nRowCount = 0;

            ExecuteStoredProcedure(strStoredProcedureName, aoDataParameter, ref nRowCount);
        }

        #endregion

        #region "저장 프로시저 실행하기 - ExecuteStoredProcedure(strStoredProcedureName, aoDataParameter, nRowCount)"

        /// <summary>
        /// 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="nRowCount">행 수</param>
        /// <remarks></remarks>
        public void ExecuteStoredProcedure(string strStoredProcedureName, IDataParameter[] aoDataParameter, ref int nRowCount)
        {
            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);

                nRowCount = oCommand.ExecuteNonQuery();
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteStoredProcedure(strStoredProcedureName, aoDataParameter, nRowCount)", oException.Message, oException.GetType().Name);
            }
        }

        #endregion

        #region "결과값을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureReturnValue(strStoredProcedureName, aoDataParameter, eSqlDbTyoe, nSize)"

        /// <summary>
        /// 결과값을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteStoredProcedureReturnValue(string strStoredProcedureName, IDataParameter[] aoDataParameter, DbType eDbType, int nSize)
        {
            int nRowCount = 0;

            return ExecuteStoredProcedureReturnValue(strStoredProcedureName, aoDataParameter, eDbType, nSize, ref nRowCount);
        }

        #endregion

        #region "결과값을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureReturnValue(strStoredProcedureName, aoDataParameter, eSqlDbTyoe, nSize, nRowCount)"

        /// <summary>
        /// 결과값을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <param name="nRowCount">행 수</param>
        /// <returns>결과값</returns>
        /// <remarks></remarks>
        public object ExecuteStoredProcedureReturnValue(string strStoredProcedureName, IDataParameter[] aoDataParameter, DbType eDbType, int nSize, ref int nRowCount)
        {
            object oReturnValue = null;

            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommandReturnValue(strStoredProcedureName, aoDataParameter, eDbType, nSize);

                nRowCount = oCommand.ExecuteNonQuery();

                oReturnValue = oCommand.Parameters["ReturnValue"].Value;
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteStoredProcedureReturnValue(strStoredProcedureName, aoDataParameter, eSqlDbTyoe, nSize, byPrecision, byScale)", oException.Message, oException.GetType().Name);
            }

            return oReturnValue;
        }

        #endregion

        #region "해시 테이블을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureHashtable(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 해시 테이블을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable ExecuteStoredProcedureHashtable(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            Hashtable oHashtable = null;

            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);

                oCommand.ExecuteNonQuery();

                oHashtable = new Hashtable();

                foreach (OracleParameter oParameter in oCommand.Parameters)
                {
                    if (oParameter.Direction == ParameterDirection.InputOutput | oParameter.Direction == ParameterDirection.Output)
                    {
                        oHashtable.Add(oParameter.ParameterName, oParameter.Value);
                    }
                }
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteStoredProcedureHashtable(strStoredProcedureName, aoDataParameter)", oException.Message, oException.GetType().Name);
            }

            return oHashtable;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////// Select

        #region "스칼라 값을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureScalar(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 스칼라 값을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>스칼라 값</returns>
        /// <remarks></remarks>
        public object ExecuteStoredProcedureScalar(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            object oScalar = null;

            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);

                oScalar = oCommand.ExecuteScalar();
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteStoredProcedureScalar(strScript, aoDataParameter)", oException.Message, oException.GetType().Name);
            }

            return oScalar;
        }

        #endregion

        #region "데이터 리더를 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureOracleDataReader(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 데이터 리더를 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>데이터 리더</returns>
        /// <remarks></remarks>
        public OracleDataReader ExecuteStoredProcedureOracleDataReader(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            OracleDataReader oDataReader = null;

            try
            {
                OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);

                oDataReader = oCommand.ExecuteReader();
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteStoredProcedureOracleDataReader(strStoredProcedureName, aoDataParameter)", oException.Message, oException.GetType().Name);
            }

            return oDataReader;
        }

        #endregion

        #region "데이터셋을 돌려주는 저장 프로시저 실행하기 - ExecuteStoredProcedureDataSet(strStoredProcedureName, aoDataParameter, strTableName)"

        /// <summary>
        /// 데이터셋을 돌려주는 저장 프로시저 실행하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="strTableName">테이블명</param>
        /// <returns>데이터셋</returns>
        /// <remarks></remarks>
        public DataSet ExecuteStoredProcedureDataSet(string strStoredProcedureName, IDataParameter[] aoDataParameter, string strTableName)
        {
            DataSet oDataSet = null;

            try
            {
                oDataSet = new DataSet();

                OracleDataAdapter oDataAdapter = new OracleDataAdapter();

                oDataAdapter.SelectCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                //oDataAdapter.ReturnProviderSpecificTypes = true;
                //oDataAdapter.FillLoadOption = LoadOption.OverwriteChanges;
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "ExecuteStoredProcedureDataSet(strStoredProcedureName, aoDataParameter, strTableName)", oException.Message, oException.GetType().Name);
            }

            return oDataSet;
        }

        #endregion

        #region "저장 프로시저를 실행해 데이터셋 채우기 - FillDataSetStoredProcedure(oDataSet, strTableName, strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 저장 프로시저를 실행해 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <remarks></remarks>
        public void FillDataSetStoredProcedure(DataSet oDataSet, string strTableName, string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();

                oDataAdapter.SelectCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                //oDataAdapter.ReturnProviderSpecificTypes = true;
                //oDataAdapter.FillLoadOption = LoadOption.OverwriteChanges;
                oDataAdapter.Fill(oDataSet, strTableName);
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "FillDataSetStoredProcedure(strStoredProcedureName, aoDataParameter, strTableName)", oException.Message, oException.GetType().Name);
            }
        }

        #endregion

        #region "저장 프로시저를 실행해 해시 테이블을 돌려주고 데이터셋 채우기 - FillDataSetStoredProcedureHashtable(oDataSet, strTableName, strStoredProcedure, aoDataParameter)"

        /// <summary>
        /// 저장 프로시저를 실행해 해시 테이블을 돌려주고 데이터셋 채우기
        /// </summary>
        /// <param name="oDataSet">데이터셋</param>
        /// <param name="strTableName">테이블명</param>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>해시 테이블</returns>
        /// <remarks></remarks>
        public Hashtable FillDataSetStoredProcedureHashtable(DataSet oDataSet, string strTableName, string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            Hashtable oHashtable = null;

            try
            {
                OracleDataAdapter oDataAdapter = new OracleDataAdapter();

                oDataAdapter.SelectCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);
                //oDataAdapter.ReturnProviderSpecificTypes = true;
                //oDataAdapter.FillLoadOption = LoadOption.OverwriteChanges;
                oDataAdapter.Fill(oDataSet, strTableName);

                foreach (OracleParameter oParameter in oDataAdapter.SelectCommand.Parameters)
                {
                    if (oParameter.Direction == ParameterDirection.InputOutput | oParameter.Direction == ParameterDirection.Output)
                    {
                        oHashtable.Add(oParameter.ParameterName, oParameter.Value);
                    }
                }
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, "FillDataSetStoredProcedureHashtable(oDataSet, strTableName, strStoredProcedure, aoDataParameter)", oException.Message, oException.GetType().Name);
            }

            return oHashtable;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////// Private

        ////////////////////////////////////////////////////////////////////////////////// Script

        #region "스크립트 명령 생성하기 - CreateScriptCommand(strScript, aoDataParameter)"

        /// <summary>
        /// 스크립트 명령 생성하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>생성 명령</returns>
        /// <remarks></remarks>
        private OracleCommand CreateScriptCommand(string strScript, IDataParameter[] aoDataParameter)
        {
            OracleCommand oCommand = new OracleCommand(strScript, m_oConnection);
            oCommand.CommandType = CommandType.Text;

            if (aoDataParameter != null)
            {
                foreach (OracleParameter oParameter in aoDataParameter)
                {
                    oCommand.Parameters.Add(oParameter);
                }
            }

            if (m_oTransaction != null)
            {
                oCommand.Transaction = m_oTransaction;
            }

            return oCommand;
        }


        #endregion

        #region "결과값을 돌려주는 스크립트 명령 생성하기 - CreateScriptCommandReturnValue(strScript, adoDataParameter, eDBType, nSize)"

        /// <summary>
        /// 결과값을 돌려주는 스크립트 명령 생성하기
        /// </summary>
        /// <param name="strScript">스크립트</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <returns>생성 명령</returns>
        /// <remarks></remarks>
        private OracleCommand CreateScriptCommandReturnValue(string strScript, IDataParameter[] aoDataParameter, DbType eDbType, int nSize)
        {
            OracleCommand oCommand = CreateScriptCommand(strScript, aoDataParameter);

            OracleParameter oParameter = new OracleParameter();

            oParameter.ParameterName = "ReturnValue";
            oParameter.DbType = eDbType;
            oParameter.Direction = ParameterDirection.ReturnValue;
            oParameter.Size = nSize;

            oCommand.Parameters.Add(oParameter);

            return oCommand;
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////// Stored Procedure

        #region "저장 프로시저 명령 생성하기 - CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter)"

        /// <summary>
        /// 저장 프로시저 명령 생성하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <returns>생성 명령</returns>
        /// <remarks></remarks>
        private OracleCommand CreateStoredProcedureCommand(string strStoredProcedureName, IDataParameter[] aoDataParameter)
        {
            OracleCommand oCommand = new OracleCommand(strStoredProcedureName, m_oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;

            if (aoDataParameter != null)
            {
                foreach (OracleParameter oParameter in aoDataParameter)
                {
                    oCommand.Parameters.Add(oParameter);
                }
            }

            if (m_oTransaction != null)
            {
                oCommand.Transaction = m_oTransaction;
            }

            return oCommand;
        }

        #endregion

        #region "결과값을 돌려주는 저장 프로시저 명령 생성하기 - CreateStoredProcedureCommandReturnValue(strStoredProcedureName, aoDataParameter, eDbType, nSize)"

        /// <summary>
        /// 결과값을 돌려주는 저장 프로시저 명령 생성하기
        /// </summary>
        /// <param name="strStoredProcedureName">저장 프로시저명</param>
        /// <param name="aoDataParameter">데이터 매개 변수</param>
        /// <param name="eDbType">데이터베이스 데이터 타입</param>
        /// <param name="nSize">데이터 타입 크기</param>
        /// <returns>생성 명령</returns>
        /// <remarks></remarks>
        private OracleCommand CreateStoredProcedureCommandReturnValue(string strStoredProcedureName, IDataParameter[] aoDataParameter, DbType eDbType, int nSize)
        {
            OracleCommand oCommand = CreateStoredProcedureCommand(strStoredProcedureName, aoDataParameter);

            OracleParameter oParameter = new OracleParameter();

            oParameter.ParameterName = "ReturnValue";
            oParameter.DbType = eDbType;
            oParameter.Direction = ParameterDirection.ReturnValue;
            oParameter.Size = nSize;

            oCommand.Parameters.Add(oParameter);

            return oCommand;
        }

        #endregion

        //pFieldName => Varchar일경우
        public int GetSequenceID(string pTableName, string pFieldName)
        {
            StringBuilder sbSQLScript = new StringBuilder();
            sbSQLScript.AppendLine(" SELECT  MAX(TO_NUMBER(" + pFieldName + ")) FROM ");
            sbSQLScript.AppendLine(pTableName);

            object o = ExecuteScriptScalar(sbSQLScript.ToString(), null);

            if (Convert.IsDBNull(o)) return 1;

            return Convert.ToInt32(o) + 1;
        }

        public int GetSequenceID(string pTableName, string pFieldName, Type pType)
        {
            
            StringBuilder sbSQLScript = new StringBuilder();
            sbSQLScript.AppendLine(" SELECT  MAX(TO_NUMBER(" + pFieldName + ")) FROM ");
            sbSQLScript.AppendLine(pTableName);

            object o = ExecuteScriptScalar(sbSQLScript.ToString(), null);

            if (Convert.IsDBNull(o)) return 1;

            return Convert.ToInt32(o) + 1;
        }
    }
}
