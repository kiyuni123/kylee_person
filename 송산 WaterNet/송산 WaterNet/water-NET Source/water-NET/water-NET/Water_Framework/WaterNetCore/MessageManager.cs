﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaterNet.WaterNetCore
{
    public class MessageManager
    {
        public const string MSG_QUERY_SAVE         = "저장하시겠습니까?";
        public const string MSG_SAVE_COMPLETE     = "저장이 완료되었습니다!";
        public const string MSG_SAVE_CANCEL        = "저장이 취소되었습니다!";
                                                
        public const string MSG_QUERY_ADD          = "추가하시겠습니까?";
        public const string MSG_ADD_COMPLETE      = "추가가 완료되었습니다!";
        public const string MSG_ADD_CANCEL         = "추가가 취소되었습니다!";
                                                
        public const string MSG_QUERY_DELETE       = "삭제하시겠습니까?";
        public const string MSG_DELETE_COMPLETE   = "삭제가 완료되었습니다!";
        public const string MSG_DELETE_CANCEL      = "삭제가 취소되었습니다!";

        #region "정보 메시지 보여주기 - ShowInformationMessage(strMessage, strCaption)"
        /// <summary>
        /// 정보 메시지 보여주기
        /// </summary>
        /// <param name="strMessage">메시지</param>
        /// <param name="strCaption">제목</param>
        /// <remarks></remarks>
        public static void ShowInformationMessage(string strMessage)
        {
            ShowInformationMessage(strMessage, "정보");
        }

        public static void ShowInformationMessage(string strMessage, string strCaption)
        {
	        MessageBox.Show(strMessage, strCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        #endregion

        #region "에러 메시지 보여주기 - ShowErrorMessage(strMessage, strCaption)"
        /// <summary>
        /// 에러 메시지 보여주기
        /// </summary>
        /// <param name="strMessage">메시지</param>
        /// <param name="strCaption">제목</param>
        /// <remarks></remarks>
        public static void ShowErrorMessage(string strMessage)
        {
            ShowErrorMessage(strMessage, "에러");
        }
        
        public static void ShowErrorMessage(string strMessage, string strCaption)
        {
	        MessageBox.Show(strMessage, strCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion

        #region "주의 메시지 보여주기 - ShowExclamationMessage(strMessage, strCaption)"

        /// <summary>
        /// 주의 메시지 보여주기
        /// </summary>
        /// <param name="strMessage">메시지</param>
        /// <param name="strCaption">제목</param>
        /// <remarks></remarks>
        public static void ShowExclamationMessage(string strMessage)
        {
            ShowExclamationMessage(strMessage, "주의");
        }

        public static void ShowExclamationMessage(string strMessage, string strCaption)
        {
	        MessageBox.Show(strMessage, strCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        #endregion

        #region "예/아니오 메시지 보여주기 - ShowYesNoMessage(strMessage, strCaption)"

        /// <summary>
        /// 예/아니오 메시지 보여주기
        /// </summary>
        /// <param name="strMessage">메시지</param>
        /// <param name="strCaption">제목</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static DialogResult ShowYesNoMessage(string strMessage)
        {
            return ShowYesNoMessage(strMessage, "확인");
        }

        public static DialogResult ShowYesNoMessage(string strMessage, string strCaption)
        {
	        return MessageBox.Show(strMessage, strCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        #endregion
    }
}
