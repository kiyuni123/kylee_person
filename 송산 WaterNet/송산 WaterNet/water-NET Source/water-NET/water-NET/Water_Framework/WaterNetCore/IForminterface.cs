﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WaterNetCore
{
    public interface IForminterface
    {
        string FormID  { get; }
        string FormKey { get; }
    }
}
