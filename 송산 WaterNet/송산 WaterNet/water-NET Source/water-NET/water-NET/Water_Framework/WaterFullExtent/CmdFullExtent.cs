﻿using System;
using System.Drawing;
using System.Resources;
using System.Reflection;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.ADF.BaseClasses;
using ESRI.ArcGIS.ADF.CATIDs;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WaterFullExtent
{
    /// <summary>
    /// Summary description for CmdFullExtent.
    /// </summary>
    [Guid("4ccbaff3-d53b-4a2f-ab7c-b68b84bd7dca")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("WaterFullExtent.CmdFullExtent")]
    public sealed class CmdFullExtent : BaseCommand
    {
        #region COM Registration Function(s)
        [ComRegisterFunction()]
        [ComVisible(false)]
        static void RegisterFunction(Type registerType)
        {
            // Required for ArcGIS Component Category Registrar support
            ArcGISCategoryRegistration(registerType);

            //
            // TODO: Add any COM registration code here
            //
        }

        [ComUnregisterFunction()]
        [ComVisible(false)]
        static void UnregisterFunction(Type registerType)
        {
            // Required for ArcGIS Component Category Registrar support
            ArcGISCategoryUnregistration(registerType);

            //
            // TODO: Add any COM unregistration code here
            //
        }

        #region ArcGIS Component Category Registrar generated code
        /// <summary>
        /// Required method for ArcGIS Component Category registration -
        /// Do not modify the contents of this method with the code editor.
        /// </summary>
        private static void ArcGISCategoryRegistration(Type registerType)
        {
            string regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            ControlsCommands.Register(regKey);

        }
        /// <summary>
        /// Required method for ArcGIS Component Category unregistration -
        /// Do not modify the contents of this method with the code editor.
        /// </summary>
        private static void ArcGISCategoryUnregistration(Type registerType)
        {
            string regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            ControlsCommands.Unregister(regKey);

        }

        #endregion
        #endregion

        private IHookHelper m_hookHelper;

        public CmdFullExtent()
        {
            //
            // TODO: Define values for the public properties
            //
            base.m_category = "영역관리"; //localizable text
            base.m_caption = "전체영역";  //localizable text
            base.m_message = "지도 영역을 전체 영역으로 전환합니다.";  //localizable text 
            base.m_toolTip = "";  //localizable text 
            base.m_name = base.Category + "_" + base.m_caption;   //unique id, non-localizable (e.g. "MyCategory_MyCommand")

            try
            {
                //
                // TODO: change bitmap name if necessary
                //
                base.m_bitmap = Properties.Resources.CmdFullExtent;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message, "Invalid Bitmap");
            }
        }

        #region Overriden Class Methods

        /// <summary>
        /// Occurs when this command is created
        /// </summary>
        /// <param name="hook">Instance of the application</param>
        public override void OnCreate(object hook)
        {
            if (hook == null)
                return;

            if (m_hookHelper == null)
                m_hookHelper = new HookHelperClass();

            m_hookHelper.Hook = hook;

            // TODO:  Add other initialization code
            //m_ActiveView = m_hookHelper.ActiveView;
        }

        /// <summary>
        /// Occurs when this command is clicked
        /// </summary>
        public override void OnClick()
        {
            IEnvelope env = new EnvelopeClass();
            env.XMin = EMFrame.statics.AppStatic.MIN_X;
            env.XMax = EMFrame.statics.AppStatic.MAX_X;
            env.YMin = EMFrame.statics.AppStatic.MIN_Y;
            env.YMax = EMFrame.statics.AppStatic.MAX_Y;

            this.m_hookHelper.ActiveView.Extent = env;
            this.m_hookHelper.ActiveView.Refresh();

            //// TODO: Add CmdFullExtent.OnClick implementation
            //IEnvelope pFullEnv = null;
            //ILayer pLayer = GetLayer("행정읍면동");
            //if (pLayer == null)
            //{
            //    System.Windows.Forms.MessageBox.Show("행정읍면동 레이어가 로드되지 않았습니다.", "안내");
            //    return;
            //}
            //using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
            //{
            //    IFeatureCursor featureCursor = ((IFeatureLayer)pLayer).Search(null, false);
            //    comReleaser.ManageLifetime(featureCursor);

            //    IFeature pFeature = null;
            //    while ((pFeature = featureCursor.NextFeature()) != null)
            //    {
            //        if (pFullEnv == null)
            //            pFullEnv = pFeature.Shape.Envelope;
            //        else
            //            pFullEnv.Union(pFeature.Shape.Envelope);
            //    }
            //}

            //pFullEnv.Expand(1.1, 1.1, true);

            //m_hookHelper.ActiveView.Extent = pFullEnv;
            //m_hookHelper.ActiveView.Refresh();
        }

        public override bool Checked
        {
            get
            {
                return base.Checked;
            }
        }

        public override bool Enabled
        {
            get
            {
                return true;
            }
        }

        #endregion
    }
}
