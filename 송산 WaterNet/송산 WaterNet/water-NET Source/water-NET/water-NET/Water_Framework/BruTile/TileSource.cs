﻿﻿// Copyright (c) BruTile developers team. All rights reserved. See License.txt in the project root for license information.

using System;
using System.Linq;
using BruTile.Predefined;
using BruTile.Web;

namespace BruTile
{
    public class TileSource : ITileSource
    {
        public ITileProvider Provider { get; private set; }
        public ITileSchema Schema { get; private set; }
        public string Title { get; set; }

        public TileSource(ITileProvider tileProvider, ITileSchema tileSchema)
        {
            Provider = tileProvider;
            Schema = tileSchema;
        }

        public static ITileSource Create(KnownTileServers source, string apiKey, BruTile.Cache.IPersistentCache<byte[]> persistentCache)
        {
            switch (source)
            {
                case KnownTileServers.DaumMap:
                    return new TileSource(
                        //new WebTileProvider(new DaumRequest("http://i{s}.maps.daum-img.net/map/image/G03/i/2.30/L{z}/{x}/{y}.png", new[] { "0", "1", "2", "3" }, apiKey), persistentCache, null),
                        new WebTileProvider(new DaumRequest("http://i{s}.maps.daum-img.net/map/image/G03/i/201407rain/L{z}/{x}/{y}.png", new[] { "0", "1", "2", "3" }, apiKey), persistentCache, null),
                        new DaumSchema("png"));
                case KnownTileServers.DaumSatellite:
                    return new TileSource(
                        new WebTileProvider(new DaumRequest("http://s{s}.maps.daum-img.net/L{z}/{x}/{y}.jpg", new[] { "0", "1", "2", "3" }, apiKey), persistentCache, null),
                        new DaumSchema("jpg"));
                //case KnownTileServers.NaverMap:
                //    return new TileSource(
                //        new WebTileProvider(new NaverRequest("http://onetile{s}.map.naver.net/get/35/0/0/{z}/{x}/{y}/bl_vc_bg/ol_vc_an", new[] { "1", "2", "3", "4" }, apiKey), persistentCache, null),
                //        new NaverSchema());
                //case KnownTileServers.NaverSatellite:
                //    return new TileSource(
                //        new WebTileProvider(new NaverRequest("http://onetile{s}.map.naver.net/get/35/0/0/{z}/{x}/{y}/bl_st_bg/ol_st_an", new[] { "1", "2", "3", "4" }, apiKey), persistentCache, null),
                //        new NaverSchema("tif"));
                default:
                    throw new NotSupportedException("KnownTileServer not known");
            }
        }
    }
}