﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using BruTile.Cache;
using BruTile.Predefined;

namespace BruTile.Web
{
    [Serializable]
    public class DaumTileSource : ITileSource
    {
        private readonly DaumSchema _tileSchema;
        private readonly WebTileProvider _tileProvider;
        public const string UserAgent = @"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7";
        public const string Referer = "http://map.daum.net/";
        private string _title = string.Empty;

        public DaumTileSource(string baseUrl)
            : this(new DaumRequest(baseUrl))
        {
        }

        public DaumTileSource(DaumRequest request)
            : this(request, null) {}

        public DaumTileSource(DaumRequest request, IPersistentCache<byte[]> persistentCache)
        {
            _tileSchema = new DaumSchema();
            _tileProvider = new WebTileProvider(request, persistentCache, 
                // The Google requests needs to fake the UserAgent en Referer.
                uri =>
                    {
                        var httpWebRequest = (HttpWebRequest) WebRequest.Create(uri);
                        httpWebRequest.UserAgent = UserAgent;
                        httpWebRequest.Referer = Referer;
                        return RequestHelper.FetchImage(httpWebRequest);
                    });
        }

        public ITileProvider Provider
        {
            get { return _tileProvider; }
        }

        public ITileSchema Schema
        {
            get { return _tileSchema; }
        }

        public string Title
        {
            get { return _title; }
            private set { _title = "DaumMap"; }
        }
    }
}
