﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace BruTile.Web
{
    public class DaumRequest : IRequest
    {
        private readonly string _urlFormatter;
        private const string ServerNodeTag = "{s}";
        private const string XTag = "{x}";
        private const string YTag = "{y}";
        private const string ZTag = "{z}";
        private const string ApiKeyTag = "{k}";
        private int _nodeCounter;
        private readonly IList<string> _serverNodes;
        private readonly string _apiKey;

        public DaumRequest(string urlFormatter)
            : this(urlFormatter, null, null) {}

        public DaumRequest(string urlFormatter, IEnumerable<string> serverNodes)
            : this(urlFormatter, serverNodes, null) {}

        public DaumRequest(string urlFormatter, IEnumerable<string> serverNodes, string apiKey)
        {
            _urlFormatter = urlFormatter;
            _serverNodes = serverNodes != null ? serverNodes.ToList() : null;

            // for backward compatibility
            _urlFormatter = _urlFormatter.Replace("{0}", ZTag);
            _urlFormatter = _urlFormatter.Replace("{1}", XTag);
            _urlFormatter = _urlFormatter.Replace("{2}", YTag);
            _apiKey = apiKey;
        }

        /// <summary>
        /// Generates a URI at which to get the data for a tile.
        /// </summary>
        /// <param name="info">Information about a tile.</param>
        /// <returns>The URI at which to get the data for the specified tile.</returns>
        public Uri GetUri(TileInfo info)
        {
            var stringBuilder = new StringBuilder(_urlFormatter);
            stringBuilder.Replace(XTag, info.Index.Row.ToString(CultureInfo.InvariantCulture));
            stringBuilder.Replace(YTag, info.Index.Col.ToString(CultureInfo.InvariantCulture));
            stringBuilder.Replace(ZTag, info.Index.Level);
            stringBuilder.Replace(ApiKeyTag, _apiKey);
            InsertServerNode(stringBuilder, _serverNodes, ref _nodeCounter);
            //Console.WriteLine(stringBuilder.ToString());
            return new Uri(stringBuilder.ToString());
        }

        private void InsertServerNode(StringBuilder baseUrl, IList<string> serverNodes, ref int nodeCounter)
        {
            if (serverNodes != null && serverNodes.Count > 0)
            {
                baseUrl.Replace(ServerNodeTag, serverNodes[nodeCounter]);
                nodeCounter++;
                if (nodeCounter >= serverNodes.Count) nodeCounter = 0;
            }
        }

        public static string UrlDaum
        {
            get
            {
                return "http://i0.maps.daum-img.net/map/image/G03/i/2.30";
            }
        }
    }
}
