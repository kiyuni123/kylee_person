﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BruTile.Predefined
{
    public class DaumSchema : TileSchema
    {
        public DaumSchema()
            : this("png")
        { }

        public DaumSchema(string format)
        {
            var resolutions = new[] { 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5, 0.25 };
            
            var count = 14;
            foreach (var resolution in resolutions)
            {
                var levelId = count.ToString(CultureInfo.InvariantCulture);
                Resolutions[levelId] = new Resolution { Id = levelId, UnitsPerPixel = resolution };
                count--;
            }
            Height = 256;
            Width = 256;
            Extent = new Extent(-30000, -60000, 494288, 988576);
            OriginX = -30000;
            OriginY = -60000;
            Name = "DaumMap";
            Format = format;
            Axis = AxisDirection.Normal;
            Srs = "EPSG:5181";
            SrsWkt = srsWkt;
        }

        /// <summary>
        /// Gets the Well-Known-Text representation of the spatial reference system
        /// </summary>
        private string srsWkt
        {
            get
            {
                StringBuilder wkt = new StringBuilder();
                //wkt.AppendFormat("PROJCS[&quot;{0}&quot;,", "Korea_2000_Korea_Central_Belt_2010");
                //wkt.AppendFormat("GEOGCS[&quot;{0}&quot;,", "GCS_Korea_2000");
                //wkt.AppendFormat("DATUM[&quot;{0}&quot;,", "D_Korea_2000");
                //wkt.AppendFormat("SPHEROID[&quot;{0}&quot;,{1},{2}]],", "GRS_1980", 6378137.0, 298.257222101);
                //wkt.AppendFormat("PRIMEM[&quot;{0}&quot;,{1}],", "Greenwich", 0.0);
                //wkt.AppendFormat("UNIT[&quot;{0}&quot;,{1}]],", "Degree", 0.0174532925199433);
                //wkt.AppendFormat("PROJECTION[&quot;{0}&quot;],", "Transverse_Mercator");
                //wkt.AppendFormat("PARAMETER[&quot;False_Easting&quot;,{0}],", 200000.0);
                //wkt.AppendFormat("PARAMETER[&quot;False_Northing&quot;,{0}],", 500000.0);
                //wkt.AppendFormat("PARAMETER[&quot;Central_Meridian&quot;,{0}],", 127.0);
                //wkt.AppendFormat("PARAMETER[&quot;Scale_Factor&quot;,{0}],", 1.0);
                //wkt.AppendFormat("PARAMETER[&quot;Latitude_Of_Origin&quot;,{0}],", 38.0);
                //wkt.AppendFormat("UNIT[&quot;Meter&quot;,{0}]]", 1.0);

                wkt.AppendFormat("PROJCS[\"{0}\",", "Korea_2000_Central_Belt");
                wkt.AppendFormat("GEOGCS[\"{0}\",", "GCS_Korea 2000");
                wkt.AppendFormat("DATUM[\"{0}\",", "D_Korea_2000");
                wkt.AppendFormat("SPHEROID[\"{0}\",{1},{2}]],", "GRS_1980", "6378137.0", "298.257222101");
                wkt.AppendFormat("PRIMEM[\"{0}\",{1}],", "Greenwich", "0.0");
                wkt.AppendFormat("UNIT[\"{0}\",{1}]],", "Degree", "0.017453292519943295");
                wkt.AppendFormat("PROJECTION[\"{0}\"],", "Transverse_Mercator");
                wkt.AppendFormat("PARAMETER[\"Latitude_Of_Origin\",{0}],", "38.0");
                wkt.AppendFormat("PARAMETER[\"Central_Meridian\",{0}],", "127.0");
                wkt.AppendFormat("PARAMETER[\"Scale_Factor\",{0}],", "1.0");
                wkt.AppendFormat("PARAMETER[\"False_Easting\",{0}],", "200000.0");
                wkt.AppendFormat("PARAMETER[\"False_Northing\",{0}],", "600000.0");
                wkt.AppendFormat("UNIT[\"Meter\",{0}]]", "1.0");

                return wkt.ToString();
                //return
                //    "PROJCS[\"Korea_2000_Korea_Central_Belt_2010\", " +
                //    "GEOGCS[\"GCS_Korea_2000\", " +
                //        "DATUM[\"D_Korea_2000\", " +
                //        "SPHEROID[\"GRS_1980\",6378137.0,298.257222101]], " +
                //        "PRIMEM[\"Greenwich\",0.0], " +
                //        "UNIT[\"Degree\",0.0174532925199433]], " +
                //    "PROJECTION[\"Transverse_Mercator\"], " +
                //    "PARAMETER[\"False_Easting\",200000.0], " +
                //    "PARAMETER[\"False_Northing\",500000.0], " +
                //    "PARAMETER[\"Central_Meridian\",127.0028902777778], " +
                //    "PARAMETER[\"Scale_Factor\",1.0], " +
                //    "PARAMETER[\"Latitude_Of_Origin\",38.0], " +
                //    "UNIT[\"Meter\",1.0]]";
            }
        }
    }
}
