﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace WS_Common.work
{
    public class Commonwork:EMFrame.work.BaseWork
    {
        private static Commonwork work = null;
        public static Commonwork GetInstance()
        {
            if (work == null)
            {
                work = new Commonwork();
            }

            return work;
        }

        /// <summary>
        /// 지역본부, 관리부서(센터) 코드를 인자로 사용자를 쿼리한다.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public DataTable GetUserInfoData(Hashtable param)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.AppendLine("SELECT A.ROHQCD                     ");
            oStringBuilder.AppendLine("      ,A.ROHQNM                     ");
            oStringBuilder.AppendLine("      ,A.MGDPCD                     ");
            oStringBuilder.AppendLine("      ,A.MGDPNM                     ");
            oStringBuilder.AppendLine("      ,A.USER_ID                    ");
            oStringBuilder.AppendLine("      ,A.USER_NAME                  ");
            oStringBuilder.AppendLine("      ,A.USER_PWD                   ");
            oStringBuilder.AppendLine("      ,A.USER_RIGHT                 ");
            oStringBuilder.AppendLine("      ,A.EXT_YN                     ");
            oStringBuilder.AppendLine("      ,A.L_USER_GROUP_ID            ");
            oStringBuilder.AppendLine("      ,A.G_USER_GROUP_ID            ");
            oStringBuilder.AppendLine("      ,A.USER_DIVISION              ");
            oStringBuilder.AppendLine("      ,A.USE_YN                     ");
            oStringBuilder.AppendLine("      ,A.SABUN                      ");
            oStringBuilder.AppendLine("  FROM V_USER A                     ");
            oStringBuilder.AppendLine("   WHERE ROHQCD = DECODE(:ROHQCD,'', ROHQCD, :ROHQCD          ) ");
            oStringBuilder.AppendLine("   AND MGDPCD = DECODE(:MGDPCD, '', MGDPCD, :MGDPCD           ) ");

            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(oStringBuilder.ToString(), param);
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }

        public DataTable GetScriptData(string strScript)
        {
            System.Data.DataTable result = null;
            EMFrame.dm.EMapper mapper = null;
            try
            {
                mapper = new EMFrame.dm.EMapper("SVR");

                result = mapper.ExecuteScriptDataTable(strScript, new IDataParameter[] { });
            }
            catch (Exception oException)
            {
                EMFrame.utils.LoggerUtils.logger.Info("에러", oException);
            }
            finally
            {
                mapper.Close();
            }

            return result;
        }
        
    }
}
