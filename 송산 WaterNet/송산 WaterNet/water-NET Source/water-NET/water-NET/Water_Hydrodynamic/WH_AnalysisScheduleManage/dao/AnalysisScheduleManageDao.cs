﻿/**************************************************************************
 * 파일명   : AnalysisScheduleManageDao.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.19
 * 설명     : INP파일 내용을 핸들링하는 Data Access Object
 * 변경이력 : 2010.10.19 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WaterNetCore;
using System.Collections;
using System.Data;
using Oracle.DataAccess.Client;

namespace WaterNet.WH_AnalysisScheduleManage.dao
{
    public class AnalysisScheduleManageDao
    {
        private static AnalysisScheduleManageDao dao = null;

        private AnalysisScheduleManageDao()
        {
        }

        public static AnalysisScheduleManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new AnalysisScheduleManageDao();
            }

            return dao;
        }

        //INP Master리스트 조회
        public DataSet SelectINPMasterList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.INP_NUMBER                                                                                        ");
            queryString.AppendLine("            ,a.USE_GBN                                                                                          ");
            queryString.AppendLine("            ,e.CODE_NAME       as USE_GBN_NAME                                                                  ");
            queryString.AppendLine("            ,a.LFTRIDN                                                                                          ");
            queryString.AppendLine("            ,b.LOC_NAME        as LFTR_NAME                                                                     ");
            queryString.AppendLine("            ,a.MFTRIDN                                                                                          ");
            queryString.AppendLine("            ,c.LOC_NAME        as MFTR_NAME                                                                     ");
            queryString.AppendLine("            ,a.SFTRIDN                                                                                          ");
            queryString.AppendLine("            ,d.LOC_NAME        as SFTR_NAME                                                                     ");
            queryString.AppendLine("            ,to_char(to_date(a.INS_DATE,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')      as INS_DATE      ");
            queryString.AppendLine("            ,a.TITLE                                                                                            ");
            queryString.AppendLine("            ,a.REMARK                                                                                           ");
            queryString.AppendLine("            ,a.FILE_NAME                                                                                        ");
            queryString.AppendLine("from        WH_TITLE                 a                                                                          ");
            queryString.AppendLine("            ,CM_LOCATION             b                                                                          ");
            queryString.AppendLine("            ,CM_LOCATION             c                                                                          ");
            queryString.AppendLine("            ,CM_LOCATION             d                                                                          ");
            queryString.AppendLine("            ,CM_CODE                 e                                                                          ");
            queryString.AppendLine("where       1 = 1                                                                                               ");

            if (conditions["USE_GBN"] != null)
            {
                queryString.AppendLine("and     a.USE_GBN     = '" + conditions["USE_GBN"] + "'                                                     ");
            }

            if (conditions["LFTRIDN"] != null)
            {
                queryString.AppendLine("and     a.LFTRIDN     = '" + conditions["LFTRIDN"] + "'                                                     ");
            }

            if (conditions["MFTRIDN"] != null)
            {
                queryString.AppendLine("and     a.MFTRIDN     = '" + conditions["MFTRIDN"] + "'                                                     ");
            }

            if (conditions["SFTRIDN"] != null)
            {
                queryString.AppendLine("and     a.SFTRIDN     = '" + conditions["SFTRIDN"] + "'                                                     ");
            }

            if (conditions["startDate"] != null && conditions["endDate"] != null)
            {
                queryString.AppendLine("and         a.INS_DATE                                                                                      ");
                queryString.AppendLine("between     '" + conditions["startDate"] + "'                                                               ");
                queryString.AppendLine("and         '" + conditions["endDate"] + "235959'                                                           ");
            }

            if (conditions["TITLE"] != null)
            {
                queryString.AppendLine("and     a.TITLE       like '%" + conditions["TITLE"] + "%'                                                  ");
            }

            queryString.AppendLine("and         a.LFTRIDN   = b.LOC_CODE(+)                                                                         ");
            queryString.AppendLine("and         a.MFTRIDN   = c.LOC_CODE(+)                                                                         ");
            queryString.AppendLine("and         a.SFTRIDN   = d.LOC_CODE(+)                                                                         ");
            queryString.AppendLine("and         a.USE_GBN   = e.CODE(+)                                                                             ");
            queryString.AppendLine("and         e.PCODE(+)     = '1001'                                                                             ");
            queryString.AppendLine("order by    to_date(a.INS_DATE,'yyyy-mm-dd hh24:mi:ss')                                                         ");
            queryString.AppendLine("desc                                                                                                            ");
            queryString.AppendLine("            ,a.LFTRIDN                                                                                          ");
            queryString.AppendLine("            ,a.MFTRIDN                                                                                          ");
            queryString.AppendLine("            ,a.SFTRIDN                                                                                          ");
            queryString.AppendLine("asc                                                                                                             ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //해당모델이 실시간 작업에 등록되어있는지 확인
        public DataSet IsExistModelInJobList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          IDX                                                 ");
            queryString.AppendLine("from            WH_HYDRA_SCHEDULE                                   ");
            queryString.AppendLine("where           INP_NUMBER = '" + conditions["INP_NUMBER"] + "'     ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_HYDRA_SCHEDULE");
        }

        //실시간 관망해석 정보 등록
        public void InsertRealtimeAnalysisData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_HYDRA_SCHEDULE   (                                               ");
            queryString.AppendLine("                                    IDX                                         ");
            queryString.AppendLine("                                    ,WSP_NAM                                    ");
            queryString.AppendLine("                                    ,LFTRIDN                                    ");
            queryString.AppendLine("                                    ,MFTRIDN                                    ");
            queryString.AppendLine("                                    ,SFTRIDN                                    ");
            queryString.AppendLine("                                    ,TITLE                                      ");
            queryString.AppendLine("                                    ,INP_NUMBER                                 ");
            queryString.AppendLine("                                    ,HYDRA_CYCLE                                ");
            queryString.AppendLine("                                    ,LAST_EXE_TIME                              ");
            queryString.AppendLine("                                    ,STATUS_CODE                                ");
            queryString.AppendLine("                                    ,INS_DATE                                   ");
            queryString.AppendLine("                                    ,SAVE_RESULT                                ");
            queryString.AppendLine("                                ) values(                                       ");
            queryString.AppendLine("                                    :1                                          ");
            queryString.AppendLine("                                    ,:2                                         ");
            queryString.AppendLine("                                    ,:3                                         ");
            queryString.AppendLine("                                    ,:4                                         ");
            queryString.AppendLine("                                    ,:5                                         ");
            queryString.AppendLine("                                    ,:6                                         ");
            queryString.AppendLine("                                    ,:7                                         ");
            queryString.AppendLine("                                    ,:8                                         ");
            queryString.AppendLine("                                    ,:9                                         ");
            queryString.AppendLine("                                    ,:10                                        ");
            queryString.AppendLine("                                    ,to_char(sysdate,'yyyymmddhh24miss')        ");
            queryString.AppendLine("                                    ,:11                                        ");
            queryString.AppendLine("                                )                                               ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
                ,new OracleParameter("10", OracleDbType.Varchar2)
                ,new OracleParameter("11", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["IDX"];
            parameters[1].Value = (string)conditions["WSP_NAM"];
            parameters[2].Value = (string)conditions["LFTRIDN"];
            parameters[3].Value = (string)conditions["MFTRIDN"];
            parameters[4].Value = (string)conditions["SFTRIDN"];
            parameters[5].Value = (string)conditions["TITLE"];
            parameters[6].Value = (string)conditions["INP_NUMBER"];
            parameters[7].Value = (string)conditions["HYDRA_CYCLE"];
            parameters[8].Value = (string)conditions["LAST_EXE_TIME"];
            parameters[9].Value = (string)conditions["STATUS_CODE"];
            parameters[10].Value = (string)conditions["SAVE_RESULT"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //등록된 실시간 관망해석 작업 리스트
        public DataSet SelectRegistedJobList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.IDX                                                                                                       ");
            queryString.AppendLine("            ,a.WSP_NAM                                                                                                  ");
            queryString.AppendLine("            ,a.LFTRIDN                                                                                                  ");
            queryString.AppendLine("            ,a.MFTRIDN                                                                                                  ");
            queryString.AppendLine("            ,a.SFTRIDN                                                                                                  ");
            queryString.AppendLine("            ,a.TITLE                                                                                                    ");
            queryString.AppendLine("            ,a.INP_NUMBER                                                                                               ");
            queryString.AppendLine("            ,a.HYDRA_CYCLE                                                                                              ");
            queryString.AppendLine("            ,to_char(to_date(a.LAST_EXE_TIME,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')       as LAST_EXE_TIME   ");
            queryString.AppendLine("            ,a.STATUS_CODE                                                                                              ");
            queryString.AppendLine("            ,b.CODE_NAME        as STATUS_NAME                                                                          ");
            queryString.AppendLine("            ,to_char(to_date(a.INS_DATE,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')            as INS_DATE        ");
            queryString.AppendLine("            ,a.SAVE_RESULT                                                                                              ");
            queryString.AppendLine("from        WH_HYDRA_SCHEDULE   a                                                                                       ");
            queryString.AppendLine("            ,CM_CODE            b                                                                                       ");
            queryString.AppendLine("where       1 = 1                                                                                                       ");

            if (conditions["INP_NUMBER"] != null)
            {
                queryString.AppendLine("and     INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'                                                        ");
            }

            queryString.AppendLine("and         b.PCODE         = '1002'                                                                                    ");
            queryString.AppendLine("and         a.STATUS_CODE   = b.CODE                                                                                    ");
            queryString.AppendLine("order by    INS_DATE            desc                                                                                    ");
            queryString.AppendLine("            ,IDX                asc                                                                                     ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_HYDRA_SCHEDULE");
        }

        //실행상태 update
        public void UpdateRunningStatus(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update WH_HYDRA_SCHEDULE set                        ");
            queryString.AppendLine("                                STATUS_CODE = :1    ");
            queryString.AppendLine("where                           IDX         = :2    ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["STATUS_CODE"];
            parameters[1].Value = (string)conditions["IDX"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //최종실행시간 update
        public void UpdateLastRunTime(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_HYDRA_SCHEDULE set                                   ");
            queryString.AppendLine("        LAST_EXE_TIME = to_char(sysdate,'yyyymmddhh24miss')     ");
            queryString.AppendLine("where   IDX = '" + conditions["IDX"] + "'                       ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //작업정보 update
        public void UpdateJobData(OracleDBManager dbManager, Hashtable conditons)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_HYDRA_SCHEDULE set           ");
            queryString.AppendLine("        HYDRA_CYCLE     = '" + conditons["HYDRA_CYCLE"] + "'    ");
            queryString.AppendLine("        ,SAVE_RESULT    = '" + conditons["SAVE_RESULT"] + "'    ");
            queryString.AppendLine("where   IDX             = '" + conditons["IDX"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //작업정보 delete
        public void DeleteJobData(OracleDBManager dbManager, Hashtable conditons)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_HYDRA_SCHEDULE                   ");
            queryString.AppendLine("where       IDX = '" + conditons["IDX"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //INP에 등록된 절점을 포함한 블록의 해당시간 순시유량을 조회한다.
        public DataSet SelectRealtimeFlow(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select          a.BSM_CDE			                                                                                                        ");
            //queryString.AppendLine("                ,a.BSM_IDN			                                                                                                        ");
            //queryString.AppendLine("                ,b.LOC_CODE			                                                                                                        ");
            //queryString.AppendLine("                ,a.SUM_BASEDEMAND			                                                                                                ");
            //queryString.AppendLine("                ,d.TIMESTAMP			                                                                                                    ");
            //queryString.AppendLine("                ,d.VALUE                                                                                         			                ");
            //queryString.AppendLine("from            WH_BLOCK_SUM_USEAGE     a                                                                                         			");
            //queryString.AppendLine("                ,CM_LOCATION            b                                                                                         			");
            //queryString.AppendLine("                ,IF_IHTAGS              c                                                                                         			");
            //queryString.AppendLine("                ,IF_GATHER_REALTIME     d                                                                                         			");
            //queryString.AppendLine("where           1 = 1                                                                                                             			");
            //queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                              			");
            ////queryString.AppendLine("and             to_char(to_date(d.TIMESTAMP, 'yyyy-mm-dd am hh:mi:ss'), 'yyyy-mm-dd hh24:mi') = '" + conditions["TIMESTAMP"] + "'		        ");
            //queryString.AppendLine("and             to_char(d.TIMESTAMP, 'yyyy-mm-dd hh24:mi') = '" + conditions["TIMESTAMP"] + "'		                                        ");
            //queryString.AppendLine("and             a.BSM_CDE       = b.FTR_CODE                                                                                      			");
            //queryString.AppendLine("and             a.BSM_IDN       = b.FTR_IDN                                                                                       			");
            //queryString.AppendLine("and             c.BR_CODE       = 'FR'                                                                                                      ");
            //queryString.AppendLine("and             c.FN_CODE       = 'I'                                                                                                       ");
            //queryString.AppendLine("and             b.LOC_CODE      = c.LOC_CODE                                                                                      			");
            //queryString.AppendLine("and             c.TAGNAME       = d.TAGNAME                                                                                                 ");

            queryString.AppendLine("select          a.BSM_CDE			                                                                ");
            queryString.AppendLine("                ,a.BSM_IDN			                                                                ");
            queryString.AppendLine("                ,b.LOC_CODE			                                                                ");
            queryString.AppendLine("                ,a.SUM_BASEDEMAND			                                                        ");
            queryString.AppendLine("                ,e.TIMESTAMP			                                                            ");
            queryString.AppendLine("                ,e.VALUE         			                                                        ");
            queryString.AppendLine("from            WH_BLOCK_SUM_USEAGE     a			                                                ");
            queryString.AppendLine("                ,CM_LOCATION            b			                                                ");
            queryString.AppendLine("                ,IF_IHTAGS              c   			                                            ");
            queryString.AppendLine("                ,IF_TAG_GBN             d			                                                ");
            queryString.AppendLine("                ,IF_GATHER_REALTIME     e			                                                ");
            queryString.AppendLine("where           a.inp_number    = '" + conditions["INP_NUMBER"] + "'			                    ");
            queryString.AppendLine("and             c.USE_YN        = 'Y'                                                               ");
            queryString.AppendLine("and             a.BSM_CDE       = b.FTR_CODE                                                        ");
            queryString.AppendLine("and             a.BSM_IDN       = b.FTR_IDN 			                                            ");
            queryString.AppendLine("and             b.LOC_CODE      = c.LOC_CODE			                                            ");
            queryString.AppendLine("and             c.TAGNAME       = d.TAGNAME			                                                ");
            queryString.AppendLine("and             d.TAG_GBN       = 'FRI'			                                                    ");
            queryString.AppendLine("and             d.TAGNAME       = e.TAGNAME			                                                ");
            queryString.AppendLine("and             e.TIMESTAMP     = to_date('" + conditions["TIMESTAMP"] + "', 'yyyy-mm-dd hh24:mi')	");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "realtimeFlow");
        }

        //Block정보와 연계된 Junction List와 BaseDemand를 조회 (수질 등 대수용가랑 상관없는동네에서 사용하는 메소드)
        public DataSet SelectJunctionListInBlock(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select  a.ID			                                                                                                                                                    ");
            queryString.AppendLine("        ,a.DEMAND			                                                                                                                                                ");
            queryString.AppendLine("from    WH_JUNCTIONS       a			                                                                                                                                    ");
            queryString.AppendLine("        ,WH_TAGS           b			                                                                                                                                    ");
            queryString.AppendLine("where   a.ID = b.ID			                                                                                                                                                ");
            queryString.AppendLine("and     a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'	                                                                                                            ");
            queryString.AppendLine("and     b.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'	                                                                                                            ");
            queryString.AppendLine("and     b.TYPE          = 'NODE'			                                                                                                                                ");
            queryString.AppendLine("and     substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 1)+1,lengthb(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 1)) = '" + conditions["POSITION_INFO"] + "'    ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_JUNCTIONS");
        }

        //Block정보와 연계된 Junction List와 BaseDemand를 조회 (대수용가정보 제외한 Junction List)
        public DataSet SelectJunctionListInBlockExceptLargeConsumer(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select  a.ID			                                                                                                            			                            ");
            queryString.AppendLine("        ,a.DEMAND                                                                                                                           			                    ");
            queryString.AppendLine("from    WH_JUNCTIONS        a			                                                                                            			                            ");
            queryString.AppendLine("        ,WH_TAGS            b                                                                                                               			                    ");
            queryString.AppendLine("        ,WH_LCONSUMER_INFO  c                                                                                                               			                    ");
            queryString.AppendLine("where   a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'	                                                                                            			    ");
            queryString.AppendLine("and     b.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                                                             			        ");
            queryString.AppendLine("and     c.INP_NUMBER(+) = '" + conditions["INP_NUMBER"] + "'                                                                                             			        ");
            queryString.AppendLine("and     b.TYPE          = 'NODE'			                                                                                    			                                ");
            queryString.AppendLine("and     substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 1)+1,lengthb(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 1)) = '" + conditions["POSITION_INFO"] + "'	");
            queryString.AppendLine("and     a.ID = b.ID                                                                                                                         			                    ");
            queryString.AppendLine("and     a.ID = c.ID(+)                                                                                                                      			                    ");
            queryString.AppendLine("and     c.ID is null                                                                                                                        			                    ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_JUNCTIONS");
        }

        //해당 INP의 배수지별 수위값을 조회 (작은 수위값을 가지고 한다)
        public DataSet SelectReservoirLevel(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select      a.ID			                                                                                                ");
            //queryString.AppendLine("            ,min(c.VALUE)       as VALUE			                                                                        ");
            //queryString.AppendLine("from        WH_RESERVOIRS       a		    	                                                                            ");
            //queryString.AppendLine("            ,IF_IHTAGS          b			                                                                                ");
            //queryString.AppendLine("            ,IF_GATHER_REALTIME c			                                                                                ");
            //queryString.AppendLine("where       a.INP_NUMBER                    = '"+conditions["INP_NUMBER"]+"'			                                    ");
            //queryString.AppendLine("and         b.BR_CODE                       = 'LE'			                                                                ");
            //queryString.AppendLine("and         b.FN_CODE                       = 'I'			                                                                ");
            //queryString.AppendLine("and         b.BESUJI                        = 'LEVEL'		    	                                                        ");
            //queryString.AppendLine("and         substr(a.ID,2,length(a.ID)-1)   = b.FTR_IDN			                                                            ");
            //queryString.AppendLine("and         b.TAGNAME       = c.TAGNAME			                                                                            ");
            //queryString.AppendLine("and         to_char(c.TIMESTAMP,'yyyy-mm-dd hh24:mi') = '" + conditions["TIMESTAMP"] + "'		");
            //queryString.AppendLine("group by    a.ID                                                                                                            ");

            queryString.AppendLine("select      a.ID			                                                                                    ");
            queryString.AppendLine("            ,min(d.VALUE)       as VALUE			                                                            ");
            queryString.AppendLine("from        WH_RESERVOIRS       a		    	                                                                ");
            queryString.AppendLine("            ,IF_IHTAGS          b				                                                                ");
            queryString.AppendLine("            ,IF_TAG_GBN         c			                                                                    ");
            queryString.AppendLine("            ,IF_GATHER_REALTIME d			                                                                    ");
            queryString.AppendLine("where       a.INP_NUMBER                    = '" + conditions["INP_NUMBER"] + "'			                    ");
            queryString.AppendLine("and         substr(a.ID,2,length(a.ID)-1)   = b.FTR_IDN			                                                ");
            queryString.AppendLine("and         b.TAGNAME                       = c.TAGNAME			                                                ");
            queryString.AppendLine("and         c.TAG_GBN                       = 'LEI'			                                                    ");
            queryString.AppendLine("and         c.TAGNAME                       = d.TAGNAME			                        				        ");
            queryString.AppendLine("and         d.TIMESTAMP                     = to_date('" + conditions["TIMESTAMP"] + "', 'yyyy-mm-dd hh24:mi')	");
            queryString.AppendLine("group by    a.ID                                                                                                ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "reservoir_level");
        }

        //해당 INP의 밸브(PRV)별 압력값을 조회
        public DataSet SelectValvePressure(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select          a.ID			                                                                                                ");
            //queryString.AppendLine("                ,c.VALUE			                                                                                            ");
            //queryString.AppendLine("from            WH_VALVES           a			                                                                                ");
            //queryString.AppendLine("                ,IF_IHTAGS          b			                                                                                ");
            //queryString.AppendLine("                ,IF_GATHER_REALTIME c    			                                                                            ");
            //queryString.AppendLine("where           a.INP_NUMBER                    = '" + conditions["INP_NUMBER"] + "'    			                            ");
            //queryString.AppendLine("and             a.TYPE                          = 'PRV'                                                                         ");
            //queryString.AppendLine("and             b.BR_CODE                       = 'PR'			                                                                ");
            //queryString.AppendLine("and             b.FN_CODE                       = 'I'			                                                                ");
            //queryString.AppendLine("and             substr(a.ID,2,length(a.ID)-1)   = b.FTR_IDN			                                                            ");
            //queryString.AppendLine("and             b.TAGNAME                       = c.TAGNAME			                                                            ");
            ////queryString.AppendLine("and             to_char(to_date(c.TIMESTAMP,'yyyy-mm-dd am hh:mi:ss'),'yyyy-mm-dd hh24:mi') = '"+conditions["TIMESTAMP"]+"'     ");
            //queryString.AppendLine("and             to_char(c.TIMESTAMP,'yyyy-mm-dd hh24:mi') = '" + conditions["TIMESTAMP"] + "'                                   ");

            queryString.AppendLine("select          a.ID			                                                                			        ");
            queryString.AppendLine("                ,d.VALUE			                                                        			            ");
            queryString.AppendLine("from            WH_VALVES           a			                                                			        ");
            queryString.AppendLine("                ,IF_IHTAGS          b                                                                   			");
            queryString.AppendLine("                ,IF_TAG_GBN         c                                                                   			");
            queryString.AppendLine("                ,IF_GATHER_REALTIME d    			                                        			            ");
            queryString.AppendLine("where           a.INP_NUMBER                    = '" + conditions["INP_NUMBER"] + "'    			        		");
            queryString.AppendLine("and             a.TYPE                          = 'PRV'                                                 			");
            queryString.AppendLine("and             substr(a.ID,2,length(a.ID)-1)   = b.FTR_IDN                                             			");
            queryString.AppendLine("and             b.TAGNAME                       = c.TAGNAME                                             			");
            queryString.AppendLine("and             c.TAG_GBN                       = 'PRI'                                                 			");
            queryString.AppendLine("and             c.TAGNAME                       = d.TAGNAME	                                        			    ");
            queryString.AppendLine("and             d.TIMESTAMP                     = to_date('" + conditions["TIMESTAMP"] + "', 'yyyy-mm-dd hh24:mi')	");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "valve_pressure");
        }

        //해당 블록의 대수용가 중 TAG정보가 있고 유량이 측정된 항목 조회
        public DataSet SelectObservableLargeConsumer(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select          a.ID			                                                                                                                                                                ");
            //queryString.AppendLine("                ,a.TAGNAME			                                                                                                                                                            ");
            //queryString.AppendLine("                ,b.VALUE			                                                                                                                                                            ");
            //queryString.AppendLine("                ,c.POSITION_INFO			                                                                                                                                                    ");
            //queryString.AppendLine("from            WH_LCONSUMER_INFO       a			                                                                                                                                            ");
            //queryString.AppendLine("                ,IF_GATHER_REALTIME     b   			                                                                                                                                        ");
            //queryString.AppendLine("                ,WH_TAGS                c			                                                                                                                                            ");
            //queryString.AppendLine("where           a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                ");
            //queryString.AppendLine("and             a.APPLY_YN      = 'Y'			                                                                                                                                                ");
            //queryString.AppendLine("and             b.TIMESTAMP     = to_date('" + conditions["TIMESTAMP"] + "','yyyy-mm-dd hh24:mi')			                                                                                    ");
            //queryString.AppendLine("and             c.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                ");
            //queryString.AppendLine("and             c.TYPE          = 'NODE'			                                                                                                                                            ");
            //queryString.AppendLine("and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 1)+1,instr(c.POSITION_INFO, '|', 1, 2)-1 - instr(c.POSITION_INFO, '|', 1, 1)) =  '" + conditions["FTR_CODE"] + "'			");
            //queryString.AppendLine("and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 2)+1,length(c.POSITION_INFO) - instr(c.POSITION_INFO, '|', 1, 2)) = '" + conditions["FTR_IDN"] + "'			            ");
            //queryString.AppendLine("and             a.TAGNAME       = b.TAGNAME			                                                                                                                                            ");
            //queryString.AppendLine("and             a.ID            = c.ID	                                                                                                                                                        ");

            queryString.AppendLine("select          bb.ID			                                                                                                                                                                                ");
            queryString.AppendLine("                ,nvl(aa.VALUE,'0')  as VALUE      			                                                                                                                                                    ");
            queryString.AppendLine("from			                                                                                                                                                                                                ");
            queryString.AppendLine("                (			                                                                                                                                                                                    ");
            queryString.AppendLine("                    select          a.INP_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("                                    ,a.ID			                                                                                                                                                            ");
            queryString.AppendLine("                                    ,a.TAGNAME			                                                                                                                              			                ");
            queryString.AppendLine("                                    ,b.VALUE			                                                                                                                             			                ");
            queryString.AppendLine("                    from            WH_LCONSUMER_INFO       a			                                                                                                               			                ");
            queryString.AppendLine("                                    ,IF_GATHER_REALTIME     b   			                                                                                                          			                ");
            queryString.AppendLine("                                    ,WH_TAGS                c			                                                                                                            			                ");
            queryString.AppendLine("                    where           a.INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'			                                                                                                        ");
            queryString.AppendLine("                    and             a.APPLY_YN          = 'Y'			                                                                                                            			                ");
            queryString.AppendLine("                    and             b.TIMESTAMP         = to_date('" + conditions["TARGETDATE"] + "','yyyy-mm-dd hh24:mi')			                                                                            ");
            queryString.AppendLine("                    and             c.INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'			                                                                                                        ");
            queryString.AppendLine("                    and             c.TYPE              = 'NODE'			                                                                                                                                    ");
            queryString.AppendLine("                    and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 1)+1,instr(c.POSITION_INFO, '|', 1, 2)-1 - instr(c.POSITION_INFO, '|', 1, 1)) =  '" + conditions["FTR_CODE"] + "'		");
            queryString.AppendLine("                    and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 2)+1,length(c.POSITION_INFO) - instr(c.POSITION_INFO, '|', 1, 2)) = '" + conditions["FTR_IDN"] + "'			        ");
            queryString.AppendLine("                    and             a.TAGNAME           = b.TAGNAME			                                                                                                                                    ");
            queryString.AppendLine("                    and             a.ID                = c.ID				                                                                                                                                    ");
            queryString.AppendLine("                )               aa			                                                                                                                                                                    ");
            queryString.AppendLine("                ,(			                                                                                                                                                                                    ");
            queryString.AppendLine("                    select          a.INP_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("                                    ,a.ID			                                                                                                                                                            ");
            queryString.AppendLine("                    from            WH_LCONSUMER_INFO       a			                                                                                                                                        ");
            queryString.AppendLine("                                    ,WH_TAGS                b			                                                                                                                                        ");
            queryString.AppendLine("                    where           a.INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'			                                                                                                        ");
            queryString.AppendLine("                    and             a.APPLY_YN          = 'Y'			                                                                                                                                        ");
            queryString.AppendLine("                    and             a.TAGNAME           is not null			                                                                                                                                    ");
            queryString.AppendLine("                    and             b.TYPE              = 'NODE'			                                                                                                                                    ");
            queryString.AppendLine("                    and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 1)+1,instr(b.POSITION_INFO, '|', 1, 2)-1 - instr(b.POSITION_INFO, '|', 1, 1)) =  '" + conditions["FTR_CODE"] + "'		");
            queryString.AppendLine("                    and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 2)+1,length(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 2)) = '" + conditions["FTR_IDN"] + "'			        ");
            queryString.AppendLine("                    and             a.INP_NUMBER        = b.INP_NUMBER			                                                                                                                                ");
            queryString.AppendLine("                    and             a.ID                = b.ID			                                                                                                                                        ");
            queryString.AppendLine("                )               bb			                                                                                                                                                                    ");
            queryString.AppendLine("where           aa.INP_NUMBER(+)    = bb.INP_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("and             aa.ID(+)            = bb.ID			                                                                                                                                                            ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_INFO");
        }

        //해당 기간의 유량리스트를 조회
        public DataSet SelectFlowList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select		d.VALUE                                                                                                                            			                                                            ");
            queryString.AppendLine("		    ,d.TIMESTAMP                                                                                              			   			                                                                        ");
            queryString.AppendLine("from	    CM_LOCATION         a                                                                                                              			                                                            ");
            queryString.AppendLine("		    ,IF_IHTAGS          b                                                                                                              			                                                            ");
            queryString.AppendLine("		    ,IF_TAG_GBN         c                                                                                                              			                                                            ");
            queryString.AppendLine("		    ,IF_GATHER_REALTIME d                                                                                                              			                                                            ");
            queryString.AppendLine("where		a.LOC_CODE                      = '" + conditions["LOC_CODE"] + "'                                                                                                 			                            ");
            queryString.AppendLine("and		    a.LOC_CODE                      = b.LOC_CODE                                                                                               			                                                    ");
            queryString.AppendLine("and		    c.TAG_GBN                       = 'FRI'                                                                                                    			                                                    ");
            queryString.AppendLine("and		    b.TAGNAME                       = c.TAGNAME                                                                                                			                                                    ");
            queryString.AppendLine("and         d.TIMESTAMP     between         add_months(to_date('" + conditions["analysisDate"] + "', 'yyyy-mm-dd'),-1) and to_date('" + conditions["analysisDate"] + "'||' 23:59:00', 'yyyy-mm-dd hh24:mi:ss')      ");
            queryString.AppendLine("and         to_char(to_date('" + conditions["analysisDate"] + "','yyyy-mm-dd'),'D') = to_char(d.timestamp,'D')                                                                 		                                ");
            queryString.AppendLine("and		    c.TAGNAME                       = d.TAGNAME                                                                                                			                                                    ");
            queryString.AppendLine("order by	to_number(d.VALUE)  desc																                                                                                                                ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "IF_GATHER_REALTIME");
        }

        //특정 대수용가의 해당요일/시간대 동작설정 조회
        public DataSet SelectLargeConsumerSetting(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      H" + conditions["time"] + "			                                                            ");
            queryString.AppendLine("from        WH_LCONSUMER_TIME			                                                                    ");
            queryString.AppendLine("where       INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'			                                ");
            queryString.AppendLine("and         ID              = '" + conditions["ID"] + "'			                                        ");
            queryString.AppendLine("and         WEEKDAY         = to_char(to_date('" + conditions["analysisTime"] + "','yyyy-mm-dd'),'D')		");

            //Console.WriteLine("=============SelectLargeConsumerSetting=============");
            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_TIME");
        }

        //해당 해석 모델의 관망해석 주기 조회
        public DataSet SelectHydraCycle(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          HYDRA_CYCLE                                         ");
            queryString.AppendLine("from            WH_HYDRA_SCHEDULE                                   ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_HYDRA_SCHEDULE");
        }

        //2차...

        //해당 블록의 해석시작시간부터 이전 n분 전의 유량 데이터를 조회
        public DataSet SelectFlowHistoryByAnalysis(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      c.VALUE			                                                                                                                                                                                        ");
            queryString.AppendLine("            ,to_char(c.TIMESTAMP,'yyyy-mm-dd hh24:mi')   as TIMESTAMP                                                                                                                                               ");
            queryString.AppendLine("from        IF_IHTAGS               a			                                                                                                                                                                    ");
            queryString.AppendLine("            ,IF_TAG_GBN             b			                                                                                                                                                                    ");
            queryString.AppendLine("            ,IF_GATHER_REALTIME     c			                                                                                                                                                                    ");
            queryString.AppendLine("where       a.LOC_CODE      = '" + conditions["LOC_CODE"] + "'			                                                                                                                                            ");
            queryString.AppendLine("and         b.TAG_GBN       = 'FRI'			                                                                                                                                                                        ");
            queryString.AppendLine("and         a.TAGNAME       = b.TAGNAME			                                                                                                                                                                    ");
            queryString.AppendLine("and         b.TAGNAME       = c.TAGNAME			                                                                                                                                                                    ");
            queryString.AppendLine("and         c.TIMESTAMP between to_date('" + conditions["TARGETDATE"] + "','yyyy-mm-dd hh24:mi') - " + conditions["HYDRA_CYCLE"] + "/24/60 and to_date('" + conditions["TARGETDATE"] + "','yyyy-mm-dd hh24:mi') 	");
            queryString.AppendLine("order by    c.TIMESTAMP                                                                                                                                                                                             ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "IF_IHTAGS");
        }

        //해당 블록별 적용대상 대수용가 리스트 조회
        public DataSet SelectAppliableLargeConsumerList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          bb.INP_NUMBER			                                                                                                                                                                        ");
            queryString.AppendLine("                ,bb.ID			                                                                                                                                                                                ");
            queryString.AppendLine("                ,bb.LCONSUMER_NM			                                                                                                                                                                    ");
            queryString.AppendLine("                ,bb.APPLY_INCREASE			                                                                                                                                                                    ");
            queryString.AppendLine("                ,bb.INCREASE_ERROR			                                                                                                                                                                    ");
            queryString.AppendLine("                ,bb.APPLY_CONTINUE_TIME			                                                                                                                                                                ");
            queryString.AppendLine("                ,rownum                     as IDX                                                                                                                                                              ");
            queryString.AppendLine("from            (			                                                                                                                                                                                    ");
            queryString.AppendLine("                    select          a.INP_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("                                    ,a.ID			                                                                                                                                                            ");
            queryString.AppendLine("                                    ,a.ACTIVATE_TIME			                                                                                                                                                ");
            queryString.AppendLine("                                    ,a.STATUS			                                                                                                                                                        ");
            queryString.AppendLine("                                    ,b.LCONSUMER_NM			                                                                                                                                                    ");
            queryString.AppendLine("                                    ,b.APPLY_INCREASE			                                                                                                                                                ");
            queryString.AppendLine("                                    ,b.INCREASE_ERROR			                                                                                                                                                ");
            queryString.AppendLine("                                    ,b.APPLY_CONTINUE_TIME			                                                                                                                                            ");
            queryString.AppendLine("                    from            WH_LCONSUMER_ACTIVATE       a			                                                                                                                                    ");
            queryString.AppendLine("                                    ,WH_LCONSUMER_INFO          b 			                                                                                                                                    ");
            queryString.AppendLine("                                    ,WH_TAGS                    c			                                                                                                                                    ");
            queryString.AppendLine("                    where           a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                            ");
            queryString.AppendLine("                    and             b.APPLY_YN      = 'Y'			                                                                                                                                            ");
            queryString.AppendLine("                    and             b.TAGNAME       is null			                                                                                                                                            ");
            queryString.AppendLine("                    and             c.TYPE          = 'NODE'			                                                                                                                                        ");
            queryString.AppendLine("                    and             a.INP_NUMBER    = b.INP_NUMBER			                                                                                                                                    ");
            queryString.AppendLine("                    and             a.ID            = b.ID			                                                                                                                                            ");
            queryString.AppendLine("                    and             a.INP_NUMBER    = c.INP_NUMBER			                                                                                                                                    ");
            queryString.AppendLine("                    and             a.ID            = c.ID			                                                                                                                                            ");
            queryString.AppendLine("                    and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 1)+1,instr(c.POSITION_INFO, '|', 1, 2)-1 - instr(c.POSITION_INFO, '|', 1, 1)) = '" + conditions["FTR_CODE"] + "'		");
            queryString.AppendLine("                    and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 2)+1,length(c.POSITION_INFO) - instr(c.POSITION_INFO, '|', 1, 2)) = '" + conditions["FTR_IDN"] + "'		      		");
            queryString.AppendLine("                 )          aa			                                                                                                                                                                        ");
            queryString.AppendLine("                 ,(			                                                                                                                                                                                    ");
            queryString.AppendLine("                    select          a.INP_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("                                    ,a.ID			                                                                                                                                                            ");
            queryString.AppendLine("                                    ,a.LCONSUMER_NM			                                                                                                                                                    ");
            queryString.AppendLine("                                    ,a.APPLY_INCREASE			                                                                                                                                                ");
            queryString.AppendLine("                                    ,a.INCREASE_ERROR			                                                                                                                                                ");
            queryString.AppendLine("                                    ,a.APPLY_CONTINUE_TIME			                                                                                                                                            ");
            queryString.AppendLine("                                    ,a.TAGNAME			                                                                                                                                                        ");
            queryString.AppendLine("                    from            WH_LCONSUMER_INFO   a     			                                                                                                                                        ");
            queryString.AppendLine("                                    ,WH_TAGS            b			                                                                                                                                            ");
            queryString.AppendLine("                    where           a.INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'			                                                                                                        ");
            queryString.AppendLine("                    and             a.APPLY_YN          = 'Y'			                                                                                                                                        ");
            queryString.AppendLine("                    and             a.TAGNAME           is null			                                                                                                                                        ");
            queryString.AppendLine("                    and             b.TYPE              = 'NODE'			                                                                                                                                    ");
            queryString.AppendLine("                    and             a.INP_NUMBER        = b.INP_NUMBER			                                                                                                                                ");
            queryString.AppendLine("                    and             a.ID                = b.ID			                                                                                                                                        ");
            queryString.AppendLine("                    and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 1)+1,instr(b.POSITION_INFO, '|', 1, 2)-1 - instr(b.POSITION_INFO, '|', 1, 1)) = '" + conditions["FTR_CODE"] + "'		");
            queryString.AppendLine("                    and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 2)+1,length(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 2)) = '" + conditions["FTR_IDN"] + "'					");
            queryString.AppendLine("                 )          bb			                                                                                                                                                                        ");
            queryString.AppendLine("where           aa.INP_NUMBER(+)    = bb.INP_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("and             aa.ID(+)            = bb.ID			                                                                                                                                                            ");
            queryString.AppendLine("and             aa.INP_NUMBER       is null				                                                                                                                                                        ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_INFO");
        }

        //해당블록 대수용가 리스트 조회
        public DataSet SelectLargeConsumerList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          a.INP_NUMBER                                                                                                                                  			                ");
            queryString.AppendLine("                ,a.ID                                                                                                                                         			                ");
            queryString.AppendLine("                ,a.LCONSUMER_NM                                                                                                                               			                ");
            queryString.AppendLine("                ,a.APPLY_INCREASE                                                                                                                             			                ");
            queryString.AppendLine("                ,a.APPLY_CONTINUE_TIME                                                                                                                             			            ");
            queryString.AppendLine("                ,a.INCREASE_ERROR                                                                                                                             			                ");
            queryString.AppendLine("                ,a.PRIORITY                                                                                                                                   			                ");
            queryString.AppendLine("                ,rownum                 as IDX                                                                                                                                          ");
            queryString.AppendLine("from            WH_LCONSUMER_INFO       a                                                                                                                     			                ");
            queryString.AppendLine("                ,WH_TAGS                b                                                                                                                     			                ");
            queryString.AppendLine("where           a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                                                                                    ");
            queryString.AppendLine("and             a.APPLY_YN      = '" + conditions["APPLY_YN"] + "'                                                                                                                      ");
            queryString.AppendLine("and             b.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                                                                       			    ");
            queryString.AppendLine("and             b.TYPE          = 'NODE'                                                                                                                      			                ");
            queryString.AppendLine("and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 1)+1,instr(b.POSITION_INFO, '|', 1, 2)-1 - instr(b.POSITION_INFO, '|', 1, 1)) = '" + conditions["FTR_CODE"] + "' 	");
            queryString.AppendLine("and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 2)+1,length(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 2)) = '" + conditions["FTR_IDN"] + "'		      	");
            queryString.AppendLine("and             a.ID            = b.ID                                                                                                                                        			");
            //queryString.AppendLine("order by        to_number(a.APPLY_INCREASE) desc                                                                                                                                    	");
            //queryString.AppendLine("                ,a.PRIORITY                                                                                                                               			      			    ");
            queryString.AppendLine("order by        a.LCONSUMER_NM                                                                                                                               			      			");

            //Console.WriteLine("=============SelectLargeConsumerList=============");
            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_INFO");
        }

        //해당블록의 최소증가량을 가진 대수용가를 조회한다.
        public DataSet SelectMinimumConsumer(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          min(a.APPLY_INCREASE)   as APPLY_INCREASE                                                                                                                        		");
            queryString.AppendLine("from            WH_LCONSUMER_INFO       a                                                                                                                    			                ");
            queryString.AppendLine("                ,WH_TAGS                b                                                                                                                    			                ");
            queryString.AppendLine("where           a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                                                                      			    ");
            queryString.AppendLine("and             a.APPLY_YN      = '" + conditions["APPLY_YN"] + "'                                                                                                                      ");
            queryString.AppendLine("and             b.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                                                                      			    ");
            queryString.AppendLine("and             b.TYPE          = 'NODE'                                                                                                                     			                ");
            queryString.AppendLine("and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 1)+1,instr(b.POSITION_INFO, '|', 1, 2)-1 - instr(b.POSITION_INFO, '|', 1, 1)) = '" + conditions["FTR_CODE"] + "'	");
            queryString.AppendLine("and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 2)+1,length(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 2)) = '" + conditions["FTR_IDN"] + "'		     	");
            queryString.AppendLine("and             a.ID            = b.ID                                                                                                                       			                ");

            //Console.WriteLine("=============SelectMinimumConsumer=============");
            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_INFO");
        }

        //대수용가 동작정보를 조회한다.
        public DataSet SelectActivateLargeConsumerList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          a.INP_NUMBER			                                                                                                                                                        ");
            queryString.AppendLine("                ,a.ID			                                                                                                                                                                ");
            queryString.AppendLine("                ,a.ACTIVATE_TIME			                                                                                                                                                    ");
            queryString.AppendLine("                ,a.STATUS                                                                                                                                                                       ");
            queryString.AppendLine("                ,b.LCONSUMER_NM			                                                                                                                                                        ");
            queryString.AppendLine("                ,b.APPLY_INCREASE			                                                                                                                                                    ");
            queryString.AppendLine("                ,b.INCREASE_ERROR			                                                                                                                                                    ");
            queryString.AppendLine("                ,b.APPLY_CONTINUE_TIME			                                                                                                                                                ");
            queryString.AppendLine("from            WH_LCONSUMER_ACTIVATE       a			                                                                                                                                        ");
            queryString.AppendLine("                ,WH_LCONSUMER_INFO          b 			                                                                                                                                        ");
            queryString.AppendLine("                ,WH_TAGS                    c			                                                                                                                                        ");
            queryString.AppendLine("where           a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                ");
            queryString.AppendLine("and             b.APPLY_YN      = 'Y'			                                                                                                                                                ");
            queryString.AppendLine("and             c.TYPE          = 'NODE'			                                                                                                                                            ");
            queryString.AppendLine("and             a.INP_NUMBER    = b.INP_NUMBER			                                                                                                                                        ");
            queryString.AppendLine("and             a.ID            = b.ID			                                                                                                                                                ");
            queryString.AppendLine("and             a.INP_NUMBER    = c.INP_NUMBER			                                                                                                                                        ");
            queryString.AppendLine("and             a.ID            = c.ID			                                                                                                                                                ");
            queryString.AppendLine("and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 1)+1,instr(c.POSITION_INFO, '|', 1, 2)-1 - instr(c.POSITION_INFO, '|', 1, 1)) = '" + conditions["FTR_CODE"] + "'			");
            queryString.AppendLine("and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 2)+1,length(c.POSITION_INFO) - instr(c.POSITION_INFO, '|', 1, 2)) = '" + conditions["FTR_IDN"] + "'		      	        ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_ACTIVATE");
        }

        //대수용가 동작정보를 삭제한다.
        public void DeleteLargeConsumerActivationData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_LCONSUMER_ACTIVATE                               ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //대수용가 동작정보를 저장한다.
        public void InsertLargeConsumerActivationData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_LCONSUMER_ACTIVATE (                     ");
            queryString.AppendLine("                                    INP_NUMBER          ");
            queryString.AppendLine("                                    ,ID                 ");
            queryString.AppendLine("                                    ,STATUS             ");
            queryString.AppendLine("                                    ,INCREASE_AMOUNT    ");
            queryString.AppendLine("                                    ,ACTIVATE_TIME      ");
            queryString.AppendLine("                                    ) values (          ");
            queryString.AppendLine("                                    :1                  ");
            queryString.AppendLine("                                    ,:2                 ");
            queryString.AppendLine("                                    ,:3                 ");
            queryString.AppendLine("                                    ,:4                 ");
            queryString.AppendLine("                                    ,:5                 ");
            queryString.AppendLine("                                    )                   ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["ID"];
            parameters[2].Value = (string)conditions["STATUS"];
            parameters[3].Value = (string)conditions["INCREASE_AMOUNT"];
            parameters[4].Value = (string)conditions["ACTIVATE_TIME"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //특정기간 이전의 해석결과 마스터 조회 (실시간 결과만 조회)
        public DataSet SelectOldAnalysisResultList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      RPT_NUMBER                                                                                          ");
            queryString.AppendLine("from        WH_RPT_MASTER                                                                                       ");
            queryString.AppendLine("where       AUTO_MANUAL     = 'A'                                                                               ");
            queryString.AppendLine("and         to_date(TARGET_DATE,'yyyymmddhh24miss') < to_date('" + conditions["checkDate"] + "','yyyymmdd')     ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_MASTER");
        }

        //해석결과 마스터 삭제
        public void DeleteAnalysisResultMaster(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WH_RPT_MASTER                                   ");
            queryString.AppendLine("where           RPT_NUMBER = '" + conditions["RPT_NUMBER"] + "' ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Node 해석결과 삭제
        public void DeleteNodeAnalysisResult(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WH_RPT_NODES                                    ");
            queryString.AppendLine("where           RPT_NUMBER = '" + conditions["RPT_NUMBER"] + "' ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Link 해석결과 삭제
        public void DeleteLinkAnalysisResult(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WH_RPT_LINKS                                    ");
            queryString.AppendLine("where           RPT_NUMBER = '" + conditions["RPT_NUMBER"] + "' ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //인덱스 리빌드
        public void RebuildIndex(OracleDBManager dbManager, string indexTable)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("alter index " + indexTable + " rebuild");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }
    }
}
