﻿/**************************************************************************
 * 파일명   : RealtimeValueSetterByJob.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.20
 * 설명     : Thread Pool 상태를 감시하는 Thread
 * 변경이력 : 2010.10.20 - 최초생성
 **************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using WaterNet.WH_Common.utils;
using WaterNet.WH_AnalysisScheduleManage.work;

namespace WaterNet.WH_AnalysisScheduleManage.etc
{
    class WatcherThread
    {
        private static StringBuilder logString = new StringBuilder();
        private static WatcherThread watcher = null;

        private AnalysisScheduleManageWork work = null;

        private string executeDate = "";

        private WatcherThread()
        {
            work = AnalysisScheduleManageWork.GetInstance();
        }

        public static WatcherThread GetInstance()
        {
            if(watcher == null)
            {
                watcher = new WatcherThread();
            }

            return watcher;
        }

        public void RunThread(Object obj)
        {
            while(true)
            {
                try
                {
                    Hashtable timeData = Utils.GetTime();

                    if ("01:27".Equals(timeData["hhmm"].ToString()) || "12:32".Equals(timeData["hhmm"].ToString()))
                    {
                        //매일 12시와 01시에 삭제작업 수행
                        if ("".Equals(executeDate) || !executeDate.Equals(timeData["deleteCheck"].ToString()))
                        {
                            work.DeleteOldAnalysisResult();
                            executeDate = timeData["deleteCheck"].ToString();

                            work.AnalysisResultIndexTableRebuild();
                        }
                    }

                    //10초에 한번씩 메니져로 부터 Thread의 상태를 받아온다.
                    AnalysisScheduleManager.SetThreadpoolStatus();
                    Thread.Sleep(10 * 1000);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
        }

        //누적된 메세지를 반환
        public static string GetThreadStatus()
        {
            return logString.ToString();
        }

        //외부에서 로그스트링에 메세지를 추가
        public static void SetStatus(string message)
        {
            logString.Remove(0, logString.Length);
            logString.AppendLine(message);
        }
    }
}
