﻿/**************************************************************************
 * 파일명   : AnalysisThread.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.20
 * 설명     : 관망해석을 수행하는 Thread
 * 변경이력 : 2010.10.20 - 최초생성
 **************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Data;
using System.Windows.Forms;

using WaterNet.WH_PipingNetworkAnalysis.epanet;
using WaterNet.WH_AnalysisScheduleManage.form;
using WaterNet.WH_AnalysisScheduleManage.work;
using WaterNet.WH_Common.utils;

namespace WaterNet.WH_AnalysisScheduleManage.etc
{
    class AnalysisThread
    {
        //스레드간 상태값을 check하기 위한 공통변수 (동기화를 위해 필요)
        private static Object thislock = new Object();

        public AnalysisThread()
        {
        }

        //최초 Thread 가동 시 호출
        public void RunThread(Object obj)
        {
            AnalysisEngine engine = new AnalysisEngine();
            AnalysisScheduleManageWork work = AnalysisScheduleManageWork.GetInstance();
            RealtimeValueSetter setter = RealtimeValueSetter.GetInstance();

            Hashtable analysisConditions = null;
            Hashtable updateTimeConditions = null;
            Hashtable realtimeValueConditions = null;

            try
            {
                while (true)
                {
                    //매 00초때 작업수행여부를 판단한다.
                    if ("00".Equals(Utils.GetTime()["ss"].ToString()))
                    {
                        //Time Interval과 나누어 떨어지는 경우에만 가동
                        if (int.Parse(Utils.GetTime()["mm"].ToString()) % int.Parse((string)((Hashtable)obj)["HYDRA_CYCLE"]) == 0)
                        {
                            lock (thislock)
                            {
                                //epanet dll이 static으로 사용됨으로 스레드간 동시접근은 막아야 한다. (메모리 접근 익셉션 발생)
                                
                                Console.WriteLine("락킹 기동!!! : " + (string)((Hashtable)obj)["INP_NUMBER"]);

                                #region 동기화대상
                                //실시간 계측정보 조회 (INP번호 앞의 접두어를 이용할까?...)
                                realtimeValueConditions = new Hashtable();
                                realtimeValueConditions.Add("INP_NUMBER", (string)((Hashtable)obj)["INP_NUMBER"]);

                                Hashtable timeData = Utils.GetCalcTime("mi", -3);

                                realtimeValueConditions.Add("TIMESTAMP", timeData["yyyymmddhhmm"]);                 //3분전으로 세팅 (해석기준일시)
                                realtimeValueConditions.Add("timeData", timeData);                                  //해석실행시간데이터 (필요한대로 빼쓰면 됨)

                                Hashtable realtimeValues = setter.GetRealtimeValue(realtimeValueConditions);

                                if ("WH".Equals(((string)((Hashtable)obj)["INP_NUMBER"]).Substring(0, 2)) && realtimeValues == null)
                                {
                                    Console.WriteLine("실시간 유량 데이터가 없어서 해석하지 않음");
                                    engine.ExternalAnalysisErrorHandling(((Hashtable)obj)["INP_NUMBER"].ToString(), timeData["TARGETDATE"].ToString(), 1001);
                                }
                                else
                                {
                                    if ("WH".Equals(((string)((Hashtable)obj)["INP_NUMBER"]).Substring(0, 2)) && realtimeValues["demand"] == null)
                                    {
                                        //수리해석
                                        if (realtimeValues["demand"] == null)
                                        {
                                            //사용량정보가 없으면 해석을 실행하지 않는다.
                                            Console.WriteLine("사용량정보가 없어서 해석하지 않음...");
                                            engine.ExternalAnalysisErrorHandling(((Hashtable)obj)["INP_NUMBER"].ToString(), timeData["TARGETDATE"].ToString(), 1002);
                                        }
                                    }
                                    else if ("EN".Equals(((string)((Hashtable)obj)["INP_NUMBER"]).Substring(0, 2)) && realtimeValues["status"] == null)
                                    {
                                        //에너지해석
                                        Console.WriteLine("상태정보가 없어서 해석하지 않음...");
                                        engine.ExternalAnalysisErrorHandling(((Hashtable)obj)["INP_NUMBER"].ToString(), timeData["TARGETDATE"].ToString(), 1005);
                                    }
                                    else
                                    {
                                        //최종 해석실행시간을 update
                                        updateTimeConditions = new Hashtable();
                                        updateTimeConditions.Add("IDX", (string)((Hashtable)obj)["IDX"]);
                                        work.UpdateLastRunTime(updateTimeConditions);

                                        //실시간 관망해석에 필요한 정보
                                        analysisConditions = new Hashtable();
                                        analysisConditions.Add("INP_NUMBER", (string)((Hashtable)obj)["INP_NUMBER"]);
                                        analysisConditions.Add("analysisType", 1);
                                        analysisConditions.Add("resetValues", realtimeValues);
                                        analysisConditions.Add("TARGET_DATE", timeData["resultTimeStamp"]);

                                        if ("Y".Equals(Utils.nts((string)((Hashtable)obj)["SAVE_RESULT"])))
                                        {
                                            analysisConditions.Add("saveReport", true);
                                        }
                                        else
                                        {
                                            analysisConditions.Add("saveReport", false);
                                        }

                                        analysisConditions.Add("AUTO_MANUAL", "A");

                                        //관망해석 실행
                                        try
                                        {
                                            Console.WriteLine("관망해석 실행...");
                                            engine.Execute(analysisConditions);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine("관망해석 Thread 오류 catch...");
                                            MessageBox.Show(e.ToString());
                                        }
                                    }
                                }
                                #endregion 동기화대상종료

                                Console.WriteLine("락킹 종료!!! : " + (string)((Hashtable)obj)["INP_NUMBER"]);
                            }
                        }
                    }

                    //1초단위로 시간을 check하여 매 00초일때 작업을 수행할지 여부를 판단
                    Thread.Sleep(1000);
                }
            }
            catch (ThreadAbortException te)
            {
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
                MessageBox.Show(e1.ToString());
            }
        }
    }
}
