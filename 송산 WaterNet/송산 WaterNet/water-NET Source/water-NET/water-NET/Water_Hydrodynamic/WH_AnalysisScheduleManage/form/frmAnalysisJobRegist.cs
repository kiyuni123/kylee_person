﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WH_Common.utils;
using WaterNet.WH_Common.work;
using WaterNet.WH_AnalysisScheduleManage.work;
using EMFrame.log;

namespace WaterNet.WH_AnalysisScheduleManage.form
{
    public partial class frmAnalysisJobRegist : Form
    {
        private AnalysisScheduleManageWork work = null;
        private CommonWork cWork = null;
        public frmAnalysisScheduleManage parentForm = null;

        public frmAnalysisJobRegist()
        {
            InitializeComponent();
            InitializeSetting();
        }

        //화면초기화
        private void InitializeSetting()
        {
            work = AnalysisScheduleManageWork.GetInstance();
            cWork = CommonWork.GetInstance();

            //메인 그리드 초기화
            UltraGridColumn masterColumn;

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "INP_NUMBER";
            masterColumn.Header.Caption = "모델일련번호";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Center;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 180;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "TITLE";
            masterColumn.Header.Caption = "제목";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 250;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "USE_GBN";
            masterColumn.Header.Caption = "사용구분";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "USE_GBN_NAME";
            masterColumn.Header.Caption = "사용구분";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "LFTRIDN";
            masterColumn.Header.Caption = "대블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "LFTR_NAME";
            masterColumn.Header.Caption = "대블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "MFTRIDN";
            masterColumn.Header.Caption = "중블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "MFTR_NAME";
            masterColumn.Header.Caption = "중블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "SFTRIDN";
            masterColumn.Header.Caption = "소블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "SFTR_NAME";
            masterColumn.Header.Caption = "소블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "INS_DATE";
            masterColumn.Header.Caption = "등록일자";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Center;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 180;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "FILE_NAME";
            masterColumn.Header.Caption = "파일명";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 200;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "REMARK";
            masterColumn.Header.Caption = "비고";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 300;
            masterColumn.Hidden = true;   //필드 보이기

            //INP리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griINPList);
        }

        //Form이 최초 로드될때 실행
        private void frmAnalysisJobRegist_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================에너지관리 

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간작업관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.butAddJob.Enabled = false;
            }

            //===========================================================================

            //검색일자 설정
            datStartDate.Value = Utils.GetCalcTime("mm", -1)["yyyymmdd"];
            datEndDate.Value = Utils.GetTime()["yyyymmdd"];

            //사용구분정보 할당(검색조건)
            comUseGbn.ValueMember = "CODE";
            comUseGbn.DisplayMember = "CODE_NAME";

            Hashtable conditions = new Hashtable();
            conditions.Add("PCODE", "1001");

            DataSet useGbnDataSet = cWork.GetCodeList(conditions);

            if (useGbnDataSet != null)
            {
                comUseGbn.DataSource = useGbnDataSet.Tables["CM_CODE"];
            }

            //블록정보 할당(검색조건)
            comLftridn.ValueMember = "LOC_CODE";
            comLftridn.DisplayMember = "LOC_NAME";
            comMftridn.ValueMember = "LOC_CODE";
            comMftridn.DisplayMember = "LOC_NAME";
            comSftridn.ValueMember = "LOC_CODE";
            comSftridn.DisplayMember = "LOC_NAME";

            DataSet blockDataSet = cWork.GetLblockDataList(new Hashtable());

            if (blockDataSet != null)
            {
                comLftridn.DataSource = blockDataSet.Tables["CM_LOCATION"];
            }

            //조회버튼에 최초 포커스
            butSelectINPList.Focus();
        }

        //------------------------------------- 이벤트 정의 시작 -------------------------------------

        //대블록 콤보박스가 변경된 경우
        private void comLftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comLftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comMftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }

        //중블록 콤보박스가 변경된 경우
        private void comMftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comMftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comSftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }

        //조회버튼 클릭 시
        private void butSelectINPList_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //검색조건 할당
                Hashtable conditions = new Hashtable();

                if (!"".Equals(comUseGbn.SelectedValue))
                {
                    conditions.Add("USE_GBN", comUseGbn.SelectedValue);
                }

                if (!"".Equals(comLftridn.SelectedValue))
                {
                    conditions.Add("LFTRIDN", comLftridn.SelectedValue);
                }

                if (!"".Equals(comMftridn.SelectedValue))
                {
                    conditions.Add("MFTRIDN", comMftridn.SelectedValue);
                }

                if (!"".Equals(comSftridn.SelectedValue))
                {
                    conditions.Add("SFTRIDN", comSftridn.SelectedValue);
                }

                if (!"".Equals(texTitle.Text))
                {
                    conditions.Add("TITLE", texTitle.Text);
                }

                conditions.Add("startDate", datStartDate.DateTime.Year + Convert.ToString(datStartDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datStartDate.DateTime.Day).PadLeft(2, '0'));
                conditions.Add("endDate", datEndDate.DateTime.Year + Convert.ToString(datEndDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datEndDate.DateTime.Day).PadLeft(2, '0'));

                try
                {
                    DataTable dTable = work.SelectINPList(conditions).Tables["WH_TITLE"];

                    griINPList.DataSource = dTable;

                    //조회 종료 시 첫행에 focus를 주고 해당 내용을 채움
                    if (griINPList.Rows.Count > 0)
                    {
                        griINPList.Rows[0].Selected = true;
                    }
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.ToString());
                    Console.WriteLine(e1.StackTrace);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //등록버튼 클릭 시
        private void butAddJob_Click(object sender, EventArgs e)
        {
            if (griINPList.Selected.Rows.Count != 0)
            {
                DialogResult result = MessageBox.Show(Utils.nts(griINPList.Selected.Rows[0].GetCellValue("INP_NUMBER")) + " 모델을 실시간 관망해석작업에 추가하시겠습니까?", "작업추가", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    try
                    {
                        Hashtable conditions = new Hashtable();

                        conditions.Add("IDX", Utils.GetSerialNumber("SC"));
                        conditions.Add("WSP_NAM", "");
                        conditions.Add("LFTRIDN", "");
                        conditions.Add("MFTRIDN", "");
                        conditions.Add("SFTRIDN", "");
                        conditions.Add("TITLE", Utils.nts(griINPList.Selected.Rows[0].GetCellValue("TITLE")));
                        conditions.Add("INP_NUMBER", Utils.nts(griINPList.Selected.Rows[0].GetCellValue("INP_NUMBER")));
                        
                        conditions.Add("LAST_EXE_TIME", "");
                        conditions.Add("STATUS_CODE", "000001");
                        conditions.Add("HYDRA_CYCLE", "15");        //초기값은 15분
                        conditions.Add("SAVE_RESULT", "N");         //초기값은 DB저장하지 않음
                        
                        //작업추가요청
                        work.InsertRealtimeAnalysisJobData(conditions);
                        //MessageBox.Show("정상적으로 처리되었습니다.");
                        parentForm.GetScheduledJobList();
                        this.Close();
                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show(e1.Message);
                        Console.WriteLine(e1.ToString());
                    }
                }
            }
        }

        //등록버튼 클릭

        //------------------------------------- 이벤트 정의 종료 -------------------------------------
    }
}
