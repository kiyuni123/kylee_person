﻿/**************************************************************************
 * 파일명   : AnalysisScheduleManageWork.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.19
 * 설명     : 실시간 관망해석 관리용 Business Logic
 * 변경이력 : 2010.10.19 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

using WaterNet.WaterNetCore;
using WaterNet.WH_AnalysisScheduleManage.dao;
using WaterNet.WH_AnalysisScheduleManage.etc;
using WaterNet.WH_Common.utils;

namespace WaterNet.WH_AnalysisScheduleManage.work
{
    public class AnalysisScheduleManageWork
    {
        private static AnalysisScheduleManageWork work = null;
        private AnalysisScheduleManageDao dao = null;

        private AnalysisScheduleManageWork()
        {
            dao = AnalysisScheduleManageDao.GetInstance();
        }

        public static AnalysisScheduleManageWork GetInstance()
        {
            if(work == null)
            {
                work = new AnalysisScheduleManageWork();
            }

            return work;  
        }

        //INP 파일 리스트 조회
        public DataSet SelectINPList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                result = dao.SelectINPMasterList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.StackTrace);
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실시간 관망해석 작업 추가
        public void InsertRealtimeAnalysisJobData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet dSet = dao.IsExistModelInJobList(dbManager, conditions);

                if (dSet.Tables["WH_HYDRA_SCHEDULE"].Rows.Count > 0)
                {
                    throw new Exception("이미 등록된 모델입니다.");
                }

                dao.InsertRealtimeAnalysisData(dbManager, conditions);
                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //실시간 관망해석 작업리스트 조회
        public DataSet SelectRealtimeAnalysisJobList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                return dao.SelectRegistedJobList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //실시간 등록 Job 상태변경
        public void UpdateRunningStatus(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.UpdateRunningStatus(dbManager, conditions);

                AnalysisScheduleManager manager = AnalysisScheduleManager.GetInstance();

                if ("000002".Equals((string)conditions["STATUS_CODE"]))
                {
                    //작업시작 및 재시작요청
                    manager.RunSchedulingJob(conditions);
                    Console.WriteLine("재시작요청..." + conditions["STATUS_CODE"]);
                }
                else
                {
                    //작업중지요청
                    manager.StopSchedulingJob(conditions);
                    Console.WriteLine("중지요청..." + conditions["STATUS_CODE"]);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //최종작업시간 update
        public void UpdateLastRunTime(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.UpdateLastRunTime(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //실시간작업정보 변경
        public void UpdateJobData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.UpdateJobData(dbManager, conditions);
                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //실시간작업정보 삭제
        public void DeleteJobData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                dbManager.BeginTransaction();

                dao.DeleteJobData(dbManager, conditions);
                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //대수용가 사용량 처리
        public Hashtable ApplyLageConsumer(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            Hashtable result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                Hashtable timeData = (Hashtable)conditions["timeData"];

                string analysisDate = timeData["yyyymmdd"].ToString();

                //오늘날짜
                Hashtable flowConditions = new Hashtable();

                flowConditions.Add("analysisDate",analysisDate);
                flowConditions.Add("LOC_CODE", conditions["LOC_CODE"]);

                double standardFlow = 0;

                if (conditions["STANDARD_FLOW"] == null)
                {
                    //한달치 같은요일의 평균유량을 조회
                    DataSet flowDataSet = dao.SelectFlowList(dbManager, flowConditions);

                    if (flowDataSet.Tables["IF_GATHER_REALTIME"].Rows.Count != 0)
                    {
                        //주어진 제거비율만큼 상위유량을 제거하고 평균을 산출
                        
                        //상위 유량의 일정량을 제거하고 평균유량을 산출한다. (기본적으로 10%)
                        double errorRatio = 10;

                        if(conditions["ERROR_RATIO"] != null)
                        {
                            errorRatio = double.Parse(conditions["ERROR_RATIO"].ToString());
                        }

                        double count = flowDataSet.Tables["IF_GATHER_REALTIME"].Rows.Count;
                        int startPoint = int.Parse((count * (errorRatio / 100)).ToString("N0"));
                        double sumValue = 0;

                        for (int i = startPoint + 1; i < count; i++)
                        {
                            sumValue = sumValue + double.Parse(flowDataSet.Tables["IF_GATHER_REALTIME"].Rows[i]["VALUE"].ToString());
                        }

                        standardFlow = sumValue / startPoint;
                    }
                }
                else
                {
                    standardFlow = double.Parse(conditions["STANDARD_FLOW"].ToString());
                }

                if (standardFlow != 0)
                {
                    Console.WriteLine("평균유량 산정완료 : " + standardFlow);

                    //평균유량 산정 후 진행
                    double nowFlow = double.Parse(conditions["nowFlow"].ToString());
                    double flowGap = nowFlow - standardFlow;

                    //등록된 대수용가중 최소증가량 조회
                    Hashtable minCounsumerConditions = new Hashtable();

                    minCounsumerConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                    minCounsumerConditions.Add("APPLY_YN", "Y");
                    minCounsumerConditions.Add("FTR_CODE", conditions["FTR_CODE"]);
                    minCounsumerConditions.Add("FTR_IDN", conditions["FTR_IDN"]);

                    DataSet minConsumerDataSet = dao.SelectMinimumConsumer(dbManager, minCounsumerConditions);

                    if (minConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows.Count != 0)
                    {
                        Console.WriteLine("등록된 대수용가 있음...");
                        //등록된 대수용가가 있을 경우만 진행

                        //유량차가 0이상이고 등록된 대수용가중 최소량보다 유량차가 큰 경우만 프로세스 진행
                        Console.WriteLine("flowGap : " + flowGap + ", APPLY_INCREASE" + minConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[0]["APPLY_INCREASE"].ToString());

                        if (flowGap > double.Parse(minConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[0]["APPLY_INCREASE"].ToString()) && flowGap > 0)
                        {
                            //적용된 대수용가 리스트
                            Hashtable applyedLargeConsumerList = new Hashtable();

                            //해당블록의 대수용가 정보 조회
                            Hashtable consumerListConditions = new Hashtable();

                            consumerListConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                            consumerListConditions.Add("APPLY_YN", "Y");
                            consumerListConditions.Add("FTR_CODE", conditions["FTR_CODE"]);
                            consumerListConditions.Add("FTR_IDN", conditions["FTR_IDN"]);

                            DataSet largeConsumerDataSet = dao.SelectLargeConsumerList(dbManager, consumerListConditions);

                            if (largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows.Count != 0)
                            {
                                Console.WriteLine("대수용가 리스트 조회 완료");

                                //요일/시간조건에 부합하는 대수용가 리스트
                                ArrayList fitSettingLargeConsumerList = new ArrayList();

                                Console.WriteLine("=====================조건에 부합하는 대수용가 조회=====================");
                                Console.WriteLine("해석시간 : " + timeData["yyyymmddhhmmss"]);
                                for(int i = 0; i < largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows.Count; i ++)
                                {
                                    //대수용가 별로 해당 요일/시간에 맞는 항목이 있는지 조회
                                    Hashtable settingConditions = new Hashtable();
                                    
                                    settingConditions.Add("time",timeData["hh"]);
                                    settingConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                                    settingConditions.Add("ID", largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]["ID"]);
                                    settingConditions.Add("analysisTime",timeData["yyyymmdd"]);

                                    DataSet settingDataSet = dao.SelectLargeConsumerSetting(dbManager, settingConditions);

                                    if(settingDataSet.Tables["WH_LCONSUMER_TIME"].Rows.Count != 0)
                                    {
                                        if ("Y".Equals(settingDataSet.Tables["WH_LCONSUMER_TIME"].Rows[0][0].ToString()))
                                        {
                                            Console.WriteLine("조건에 부합... ID : " + largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]["ID"]);
                                            fitSettingLargeConsumerList.Add(largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]);
                                        }
                                    }

                                    //반환할 결과에 대수용가정보 저장 (수수량은 0으로 초기화)
                                    if (result == null)
                                    {
                                        result = new Hashtable();
                                    }

                                    result.Add(largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]["ID"].ToString(), "0");
                                }

                                //조건에 부합하는 대수용가 적용
                                Console.WriteLine("===================조건에 부합하는 대수용가 적용===================");
                                for (int i = 0; i < fitSettingLargeConsumerList.Count; i++)
                                {
                                    double applyIncrease = double.Parse(((DataRow)fitSettingLargeConsumerList[i])["APPLY_INCREASE"].ToString());

                                    if (flowGap - applyIncrease > 0)
                                    {
                                        flowGap = flowGap - applyIncrease;
                                        Console.WriteLine("대수용가 적용 : " + ((DataRow)fitSettingLargeConsumerList[i])["ID"] + " applyIncrease : " + applyIncrease + " 남은 flowGap : " + flowGap);
                                        applyedLargeConsumerList.Add(((DataRow)fitSettingLargeConsumerList[i])["ID"].ToString(), fitSettingLargeConsumerList[i]);
                                        result[((DataRow)fitSettingLargeConsumerList[i])["ID"].ToString()] = applyIncrease.ToString();
                                    }
                                    else
                                    {
                                        Console.WriteLine(((DataRow)fitSettingLargeConsumerList[i])["ID"] + "수요량 차감이 음수라 반영안됨 (요일/시간) : " + (flowGap - applyIncrease) + ", flowGap : " + flowGap + ", applyIncrease : " + applyIncrease);
                                    }
                                }

                                //나머지 잔여유량에 대해 적용할 수 있는 대수용가 조회
                                Console.WriteLine("===================나머지 대수용가 적용===================");
                                for (int i = 0; i < largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows.Count; i++)
                                {
                                    //기존에 대입한 대수용가인지 검색
                                    if (applyedLargeConsumerList[largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]["ID"].ToString()] == null)
                                    {
                                        double applyIncrease = double.Parse(largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]["APPLY_INCREASE"].ToString());

                                        if (flowGap - applyIncrease > 0)
                                        {
                                            flowGap = flowGap - applyIncrease;
                                            Console.WriteLine("대수용가 적용 : " + largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]["ID"] + " 적용량 : " + applyIncrease + " 남은 flowGap : " + flowGap);
                                            applyedLargeConsumerList.Add(largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]["ID"].ToString(), largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]);
                                            result[largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]["ID"].ToString()] = applyIncrease.ToString();
                                        }
                                        else
                                        {
                                            Console.WriteLine(largeConsumerDataSet.Tables["WH_LCONSUMER_INFO"].Rows[i]["ID"] + "수요량 차감이 음수라 반영안됨 (나머지) : " + (flowGap - applyIncrease) + ", flowGap : " + flowGap + ", applyIncrease : " + applyIncrease);
                                        }
                                    }
                                }

                                Console.WriteLine("===================결과표출===================");
                                foreach (string key in result.Keys)
                                {
                                    Console.WriteLine("ID : " + key + " 수수량 : " + result[key]);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //3일 이전 해석결과를 삭제한다. (실시간 관망해석)
        public void DeleteOldAnalysisResult()
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();
                dbManager.BeginTransaction();

                Hashtable analysisConditions = new Hashtable();

                analysisConditions.Add("checkDate", Utils.GetCalcTime("dd", -3)["checkDate"]);

                result = dao.SelectOldAnalysisResultList(dbManager, analysisConditions);

                foreach (DataRow row in result.Tables["WH_RPT_MASTER"].Rows)
                {
                    Hashtable deleteConditions = new Hashtable();

                    deleteConditions.Add("RPT_NUMBER", row["RPT_NUMBER"]);

                    dao.DeleteAnalysisResultMaster(dbManager, deleteConditions);
                    dao.DeleteNodeAnalysisResult(dbManager, deleteConditions);
                    dao.DeleteLinkAnalysisResult(dbManager, deleteConditions);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void AnalysisResultIndexTableRebuild()
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                dao.RebuildIndex(dbManager, "PK_WH_RPT_MASTER");
                dao.RebuildIndex(dbManager, "WH_RPT_JUNCTIONS");
                dao.RebuildIndex(dbManager, "WH_RPT_LINKS");
            }
            catch (Exception e1)
            {
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }
        }
    }
}
