﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.Collections;
using System.Timers;

using WaterNet.WH_AnalysisScheduleManage.work;
using WaterNet.WH_AnalysisScheduleManage.etc;
using WaterNet.WH_PipingNetworkAnalysis.epanet;
using WaterNet.WH_Common.utils;

namespace WaterNet.WH_AnalysisScheduleManage.form
{
    public partial class frmAnalysisScheduleManage : Form, WaterNet.WaterNetCore.IForminterface
    {
        private AnalysisScheduleManageWork work = null;

        //Watcher로 부터 Push가 불가능하기 때문에 App에서 일정시간 간격으로 Watcher의 Log를 조회하기 위한 타이머
        //(Push가 불가능한 이유는 System의 재기동 시 Scheduling job table을 읽어 상태가 실행중인 Job에 대해 화면이 없는 채로 일괄로 실행시키기 때문이며
        //소켓등을 사용하기 전에는 해당 textArea 객체를 넘겨야 해서 참조 할 방법이 없음)
        private System.Timers.Timer timer = new System.Timers.Timer();

        //Status창에 Watcher의 상태를 쓰기위한 Delegate 정의
        private delegate void SetStatusTextCallback(string text);

        public frmAnalysisScheduleManage()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //메인폼에서 호출
        public void Open()
        {
        }

        //메인폼에서 호출
        private void frmWQMain_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        //화면초기화
        private void InitializeSetting()
        {
            work = AnalysisScheduleManageWork.GetInstance();

            //실시간 관망해석 작업 리스트
            UltraGridColumn jobColumn;

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "IDX";
            jobColumn.Header.Caption = "작업일련번호";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 180;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "WSP_NAM";
            jobColumn.Header.Caption = "계통명";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "LFTRIDN";
            jobColumn.Header.Caption = "대블록";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "MFTRIDN";
            jobColumn.Header.Caption = "중블록";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "SFTRIDN";
            jobColumn.Header.Caption = "소블록";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "INP_NUMBER";
            jobColumn.Header.Caption = "모델 일련번호";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 180;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "TITLE";
            jobColumn.Header.Caption = "모델제목";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 250;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "HYDRA_CYCLE";
            jobColumn.Header.Caption = "실행주기(분)";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Left;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 100;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "LAST_EXE_TIME";
            jobColumn.Header.Caption = "최종실행시간";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 180;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "STATUS_CODE";
            jobColumn.Header.Caption = "현재상태";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = true;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "STATUS_NAME";
            jobColumn.Header.Caption = "현재상태";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 120;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "INS_DATE";
            jobColumn.Header.Caption = "등록일자";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 180;
            jobColumn.Hidden = false;   //필드 보이기

            jobColumn = griJobList.DisplayLayout.Bands[0].Columns.Add();
            jobColumn.Key = "SAVE_RESULT";
            jobColumn.Header.Caption = "결과저장여부";
            jobColumn.CellActivation = Activation.NoEdit;
            jobColumn.CellClickAction = CellClickAction.RowSelect;
            jobColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            jobColumn.CellAppearance.TextHAlign = HAlign.Center;
            jobColumn.CellAppearance.TextVAlign = VAlign.Middle;
            jobColumn.Width = 100;
            jobColumn.Hidden = false;   //필드 보이기

            //실시간 관망해석 작업 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griJobList);
        }

        //form 최초로딩 시 실행
        private void frmAnalysisScheduleManage_Load(object sender, EventArgs e)
        {
            try
            {
                //Watcher log 조회용 타이머 활성화
                timer.Interval = 10000;
                timer.Start();
                timer.Elapsed += new ElapsedEventHandler(StartSearch);

                GetScheduledJobList();
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        //------------------------------------- 기타 이벤트 시작 -------------------------------------

        //타이머의 interval마다 실행되는 메소드로 Watcher의 Log 내용을 richText에 뿌려주는 역할과 진행중인 작업 리스트를 갱신하는 역할을 수행
        public void StartSearch(Object sender, EventArgs eArgs)
        {
            SetStatusText(WatcherThread.GetThreadStatus());
        }

        //------------------------------------- 기타 이벤트 종료 -------------------------------------

        //------------------------------------- Model 선택 및 작업추가 관련 이벤트 시작 -------------------------------------

        //신규추가버튼 클릭 시
        private void butAddSchedule_Click(object sender, EventArgs e)
        {
            frmAnalysisJobRegist form = new frmAnalysisJobRegist();
            form.parentForm = this;
            form.ShowDialog();
        }

        //저장버튼 클릭 시 $$$$$$$$$$$$$$$
        private void butSaveSchedule_Click(object sender, EventArgs e)
        {
            if (griJobList.Selected.Rows.Count != 0)
            {
                //수정이 가능한 상태인 경우만 동작
                if (!texHydraCycle.ReadOnly)
                {
                    if ("".Equals(texHydraCycle.Text))
                    {
                        MessageBox.Show("실행주기에 값을 입력하십시오.");
                        texHydraCycle.Focus();
                    }

                    //숫자값만 오게하는 로직 필요...

                    Hashtable conditions = new Hashtable();

                    conditions.Add("IDX", griJobList.Selected.Rows[0].GetCellValue("IDX"));
                    conditions.Add("HYDRA_CYCLE", texHydraCycle.Text);

                    if (cheIsRemainResult.Checked)
                    {
                        conditions.Add("SAVE_RESULT", "Y");
                    }
                    else
                    {
                        conditions.Add("SAVE_RESULT", "N");
                    }

                    work.UpdateJobData(conditions);
                    GetScheduledJobList();
                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                else
                {
                    MessageBox.Show("작업이 진행중일때는 수정/삭제가 불가능합니다.");
                }
            }
        }

        //삭제버튼 클릭 시 
        private void butDeleteSchedule_Click(object sender, EventArgs e)
        {
            if (griJobList.Selected.Rows.Count != 0)
            {
                //삭제가 가능한 상태인 경우만 동작
                if (!texHydraCycle.ReadOnly)
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();

                        conditions.Add("IDX", griJobList.Selected.Rows[0].GetCellValue("IDX"));

                        work.DeleteJobData(conditions);
                        GetScheduledJobList();
                        MessageBox.Show("정상적으로 처리되었습니다.");
                    }
                }
                else
                {
                    MessageBox.Show("작업이 진행중일때는 수정/삭제가 불가능합니다.");
                }
            }
        }

        //------------------------------------- 모델선택 및 작업추가 관련 이벤트 종료 -------------------------------------

        //------------------------------------- 실시간 작업 관련 이벤트 시작 -------------------------------------

        //현재 진행중인 작업 List의 상태 컬럼 버튼 클릭 시
        private void griJobList_ClickCellButton(object sender, CellEventArgs e)
        {
            Hashtable conditions = new Hashtable();

            conditions.Add("IDX", Utils.nts(e.Cell.Row.GetCellValue("IDX")));
            conditions.Add("INP_NUMBER", Utils.nts(e.Cell.Row.GetCellValue("INP_NUMBER")));
            conditions.Add("HYDRA_CYCLE", Utils.nts(e.Cell.Row.GetCellValue("HYDRA_CYCLE")));
            conditions.Add("SAVE_RESULT", Utils.nts(e.Cell.Row.GetCellValue("SAVE_RESULT")));

            if ("000001".Equals(Utils.nts(e.Cell.Row.GetCellValue("STATUS_CODE"))) || "000003".Equals(Utils.nts(e.Cell.Row.GetCellValue("STATUS_CODE"))))
            {
                //등록, 정지상태 -> 실행
                conditions.Add("STATUS_CODE", "000002");
            }
            else
            {
                //실행중 상태 -> 정지
                conditions.Add("STATUS_CODE", "000003");
            }

            try
            {
                work.UpdateRunningStatus(conditions);
                GetScheduledJobList();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        //------------------------------------- 실시간 작업 관련 이벤트 종료 -------------------------------------

        //등록된 작업리스트 조회
        public void GetScheduledJobList()
        {
            try
            {
                griJobList.DataSource = (work.SelectRealtimeAnalysisJobList(new Hashtable())).Tables["WH_HYDRA_SCHEDULE"];

                //조회 종료 시 첫행에 focus를 주고 해당 내용을 채움
                if (griJobList.Rows.Count > 0)
                {
                    griJobList.Rows[0].Selected = true;
                }
            }
            catch(Exception e1)
            {
                MessageBox.Show(e1.Message);
                Console.WriteLine(e1.ToString());
            }
        }

        //Status창에 Watcher의 상태를 쓰기위한 Delegate (Cross Thread 오류때문에 사용해야함)
        private void SetStatusText(string text)
        {
            if (ricJobStatus.InvokeRequired)
            {
                SetStatusTextCallback setStatusTextCallback = new SetStatusTextCallback(SetStatusText);
                this.Invoke(setStatusTextCallback, new object[] { text });
            }
            else
            {
                ricJobStatus.AppendText(text);
                ricJobStatus.ScrollToCaret();
                GetScheduledJobList();
            }
        }

        //joblist의 그리드 행 선택이 바뀌었을 경우
        private void griJobList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griJobList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griJobList.DataSource).Rows[griJobList.Selected.Rows[0].Index]);
                if ("000002".Equals((string)row["STATUS_CODE"]))
                {
                    //현재상태가 실행중인경우는 읽기전용 처리
                    texHydraCycle.ReadOnly = true;
                    cheIsRemainResult.Enabled = false;
                }
                else
                {
                    texHydraCycle.ReadOnly = false;
                    cheIsRemainResult.Enabled = true;
                }

                texHydraCycle.Text = Utils.nts(griJobList.Selected.Rows[0].GetCellValue("HYDRA_CYCLE"));

                if("Y".Equals((string)griJobList.Selected.Rows[0].GetCellValue("SAVE_RESULT")))
                {
                    cheIsRemainResult.Checked = true;
                }
                else
                {
                    cheIsRemainResult.Checked = false;
                }
            }
        }
    }
}
