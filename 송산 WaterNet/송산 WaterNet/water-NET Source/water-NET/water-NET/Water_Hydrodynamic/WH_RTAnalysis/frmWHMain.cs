﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WH_RTAnalysis
{
    public partial class frmWHMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        double m_MapX; //GIS의 Map X 좌표 값
        double m_MapY; //GIS의 Map Y 좌표 값
        int m_X; //GIS의 윈도우 X 좌표
        int m_Y; //GIS의 윈도우 Y 좌표

        ILayer m_Layer = null; //민원지점 레이어
 
        public frmWHMain()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버 (메인화면에 AddIn하는 화면은 IForminterface를 상속하여 정의해야 함.)

        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            get { return this.Text.ToString(); }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        //------------------------------------------------------------------
        //기본적으로 맵제어 기능은 이미 적용되어 있음.
        //추가로 초기 설정이 필요하면, 다음을 적용해야 함.
        //------------------------------------------------------------------
        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            //추가적인 구현은 여기에....
        }

        //------------------------------------------------------------------
        //기본적으로 IndexMap 기능은 이미 적용되어 있음.
        //지도맵 관련 레이어 로드기능은 추가로 구현해야함.
        //------------------------------------------------------------------
        public override void Open()
        {
            base.Open();
            //추가적인 구현은 여기에....
            Viewmap_Load();

            Viewmap_DefaultBlock("BZ002");

            m_Layer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "상수관로");

            IMapCache pMapCache = axMap.ActiveView.FocusMap as IMapCache;
            pMapCache.AutoCacheActive = true;
            pMapCache.MaxScale = 3000;
            pMapCache.ScaleLimit = true;
            pMapCache.RefreshAutoCache();
        }

        private void frmWHMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            //m_ArrayContents.Clear();
        }

        #region Button Events
        
        #endregion


        #region Control Events

        /// <summary>
        /// Map에서 오른쪽 마우스 버튼 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            ///*****************************************************************************
            ///GIS MAP에서 Mouse 다운이 일어난 위치
            ///----------------------------------------------------------------------------S
            ///GIS 상 좌표
            m_MapX = e.mapX;    
            m_MapY = e.mapY;
            ///Windows 상 좌표
            m_X = e.x;          
            m_Y = e.y;
            ///----------------------------------------------------------------------------E

            ///*****************************************************************************
            ///GIS 상 좌표로 포인트를 만들고 포인트로 지오메트리를 생성
            ///----------------------------------------------------------------------------S
            ILayer pLayer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "민원지점");

            IPoint pPoint = new PointClass();
            pPoint.X = m_MapX;
            pPoint.Y = m_MapY;
            pPoint.Z = 0;

            IGeometry pGeom = (IGeometry)pPoint;
            ///----------------------------------------------------------------------------E
            
            ///*****************************************************************************
            ///ArcManager.GetSpatialCursor()로 Map에 마우스 클릭한 Map 상 좌표에서 반경 10m 
            ///지점의 특정 값을 가져와 FeatureCursor로 만들고 FeatureCursor로 Feature를
            ///만든 다음, ArcManager.GetValue()로 Layer 상의 특정 Field값을 Object로 받는다.
            ///----------------------------------------------------------------------------S
            IFeatureCursor pIFCursor = ArcManager.GetSpatialCursor(pLayer, pGeom, (double)10, esriSpatialRelEnum.esriSpatialRelContains, string.Empty);
            IFeature pFeature = pIFCursor.NextFeature();

            while (pFeature != null)
            {
                object oCANO = WaterAOCore.ArcManager.GetValue(pFeature, "CANO");
                
                if (oCANO != null)
                {
                    //m_CANO = oCANO.ToString();

                }
                pFeature = pIFCursor.NextFeature();
            }
            ///----------------------------------------------------------------------------E
            switch (e.button)
            {
                case 1:    //왼쪽버튼
                    break;

                case 2:    //오른쪽버튼
                    //ContextMenuStripPopup.Show(axMap, m_X, m_Y);
                    break;
            }
        }
        
        #endregion


        #region User Function

        /// <summary>
        /// Viewmap_Load - 지도화면에 레이어를 표시한다.
        /// </summary>
        private void Viewmap_Load()
        {
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            ILayer pLayer = null; IColor pLabelColor = null; IColor pOutColor = null; IColor pColor = null;
            ISymbol pLineSymbol = null; ISymbol pFillSymbol = null; ISymbol pMarkSymbol = null;
            IWorkspace pWS = null;

            WaterNet.WaterNetCore.frmSplashMsg oSplash = new WaterNet.WaterNetCore.frmSplashMsg();
            oSplash.Open();
            try
            {
                foreach (DataRow item in pDataTable.Rows)
                {
                    #region Workspace
                    if (item["F_WORKSPACE_NAME"].ToString() == "지형도")
                    {
                        pWS = VariableManager.m_Topographic;
                    }
                    else if (item["F_WORKSPACE_NAME"].ToString() == "관망도")
                    {
                        pWS = VariableManager.m_Pipegraphic;
                    }
                    else if (item["F_WORKSPACE_NAME"].ToString() == "INP")
                    {
                        pWS = VariableManager.m_INPgraphic;
                    }
                    else continue;

                    if (pWS == null) continue;
                    #endregion

                    pLayer = ArcManager.GetShapeLayer(pWS, item["F_NAME"].ToString(), item["F_ALIAS"].ToString());
                    if (pLayer == null) continue;

                    oSplash.Message = "[" + pLayer.Name + "]" + " 레이어를 불러오는 중입니다.";

                    switch (pLayer.Name)
                    {
                        case "건물":
                            //레이어 라벨
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(204, 217, 222);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "BLD_NAM", "굴림", 10, false, pLabelColor, 0, 0, string.Empty);
                            //외곽선 심볼
                            pOutColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1, pOutColor);
                            //내부 심볼
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pFillSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, (ILineSymbol)pLineSymbol, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pFillSymbol);
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 2000, 1);
                            break;
                        case "가압장":
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(204, 217, 222);
                            pMarkSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleMarkerSymbol(10, esriSimpleMarkerStyle.esriSMSCircle, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pMarkSymbol);
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 0);
                            break;
                        case "밸브":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 200, 0);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "FTR_CDE", "굴림", 10, false, pLabelColor, 0, 0, string.Empty);
                            WaterNet.WaterAOCore.ArcManager.SetUniqueValueRenderer((IFeatureLayer)pLayer, "FTR_CDE");
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 0);
                            pLayer.Visible = false;
                            break;
                        case "소블록":                            
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 0);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "BLK_NAM", "굴림", 10, false, pLabelColor, 0, 0, string.Empty);
                            WaterNet.WaterAOCore.ArcManager.SetUniqueValueRenderer((IFeatureLayer)pLayer, "BLK_NAM");
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 10000, 1);
                            break;
                        case "중블록":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 0);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "BLK_NAM", "굴림", 10, false, pLabelColor, 0, 0, string.Empty);
                            WaterNet.WaterAOCore.ArcManager.SetUniqueValueRenderer((IFeatureLayer)pLayer, "BLK_NAM");
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 10000);
                            break;
                        case "지형지번":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 0);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "PAR_LBL", "굴림", 9, false, pLabelColor, 0, 0, string.Empty);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer);
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 1000, 0);
                            pLayer.Visible = false;
                            break;
                        case "행정읍면동":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 200);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "HJD_NAM", "굴림", 9, true, pLabelColor, 0, 0, string.Empty);
                            //외곽선 심볼
                            pOutColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSDashDotDot, 3, pOutColor);
                            //내부 심볼
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pFillSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSHollow, (ILineSymbol)pLineSymbol, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pFillSymbol);
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 0);
                            pLayer.Visible = true;
                            break;
                        case "법정읍면동":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 200);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "BJD_NAM", "굴림", 9, true, pLabelColor, 0, 0, string.Empty);
                            //외곽선 심볼
                            pOutColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSDashDotDot, 3, pOutColor);
                            //내부 심볼
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pFillSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSHollow, (ILineSymbol)pLineSymbol, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pFillSymbol);
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 0);
                            pLayer.Visible = false;
                            break;
                        case "등고선":
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSDashDotDot, 1, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pLineSymbol);
                            pLayer.Visible = false;
                            break;
                        case "상수관로":
                            //레이어 라벨
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 255, 110);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "FTR_IDN", "굴림", 9, true, pLabelColor, 2000, 0, string.Empty);
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(0, 255, 0);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 2, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pLineSymbol);
                            pLayer.Visible = true;
                            break;
                        case "급수관로":
                            //레이어 라벨
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 255, 222);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "FTR_IDN", "굴림", 9, true, pLabelColor, 2000, 0, string.Empty);
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(0, 0, 100);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1.5, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pLineSymbol);
                            pLayer.Visible = true;
                            break;

                        case "민원지점":
                            pColor = WaterAOCore.ArcManager.GetColor(248, 24, 24);
                            //sField는 Map에 Lable로 표시하고 싶은 필드명
                            WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "CANO", "굴림", 12, true, pColor, 0, 0, string.Empty);
                            //sField는 구분 필드명
                            WaterAOCore.ArcManager.SetSimpleMarkerValueRenderer((IFeatureLayer)pLayer, "MW_GBN", (double)15, esriSimpleMarkerStyle.esriSMSDiamond, pColor);
                            pLayer.Visible = false;
                            break;

                        case "감시지점":
                            pColor = WaterAOCore.ArcManager.GetColor(0, 192, 0);
                            //sField는 Map에 Lable로 표시하고 싶은 필드명
                            WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "NAMES", "굴림", 12, false, pColor, 0, 0, string.Empty);
                            //sField는 구분 필드명
                            WaterAOCore.ArcManager.SetSimpleMarkerValueRenderer((IFeatureLayer)pLayer, "NAMES", (double)15, esriSimpleMarkerStyle.esriSMSCircle, pColor);
                            pLayer.Visible = false;
                            break;

                        case "중요시설":
                            pColor = WaterAOCore.ArcManager.GetColor(0, 0, 192);
                            //sField는 Map에 Lable로 표시하고 싶은 필드명
                            WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "NAMES", "굴림", 12, false, pColor, 0, 0, string.Empty);
                            //sField는 구분 필드명
                            WaterAOCore.ArcManager.SetSimpleMarkerValueRenderer((IFeatureLayer)pLayer, "NAMES", (double)15, esriSimpleMarkerStyle.esriSMSSquare, pColor);
                            pLayer.Visible = false;
                            break;

                        case "재염소처리지점":
                            pColor = WaterAOCore.ArcManager.GetColor(230, 155, 120);
                            //sField는 Map에 Lable로 표시하고 싶은 필드명
                            WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "NAMES", "굴림", 9, false, pColor, 0, 0, string.Empty);
                            //sField는 구분 필드명
                            WaterAOCore.ArcManager.SetUniqueValueRenderer((IFeatureLayer)pLayer, "NAMES");
                            pLayer.Visible = false;
                            break;
                    }
                    axMap.AddLayer(pLayer);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                oSplash.Close();
            }
        }

        /// <summary>
        /// Map을 Load한 뒤 대,중,소 블록 중 인자로 받아 해당 블록을 보이게 함.
        /// </summary>
        /// <param name="strBlockType">BZ001 : 대블록, BZ002 : 중블록, BZ003 : 소블록</param>
        private void Viewmap_DefaultBlock(string strBlockType)
        {
            IFeatureLayer pFeatureLayer = default(IFeatureLayer);
            switch (strBlockType)
            {
                case "BZ001":    //대블록
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "대블록");
                    break;
                case "BZ002":    //중블록
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "중블록");
                    break;
                case "BZ003":    //소블록
                    pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "소블록");
                    break;
            }
            if (pFeatureLayer == null) return;

            IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "");
            if (pFeature == null) return;

            axMap.Extent = pFeature.Shape.Envelope;
            axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);
        }

        #endregion

    }
}
