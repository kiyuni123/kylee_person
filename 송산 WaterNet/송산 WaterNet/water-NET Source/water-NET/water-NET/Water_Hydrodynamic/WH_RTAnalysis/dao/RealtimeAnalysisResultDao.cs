﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WaterNet.WaterNetCore;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace WaterNet.WH_RTAnalysis.dao
{
    public class RealtimeAnalysisResultDao
    {
        private static RealtimeAnalysisResultDao dao = null;

        private RealtimeAnalysisResultDao()
        {
        }

        public static RealtimeAnalysisResultDao GetInstance()
        {
            if(dao == null)
            {
                dao = new RealtimeAnalysisResultDao();
            }

            return dao;
        }

        //해당 INP번호의 모델정보를 조회
        public DataSet SelectModelData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          a.INP_NUMBER			                                ");
            queryString.AppendLine("                ,B.FTR_IDN            			                        ");
            queryString.AppendLine("from            WH_TITLE            a			                        ");
            queryString.AppendLine("                ,CM_LOCATION        b			                        ");
            queryString.AppendLine("where           a.INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'  ");
            queryString.AppendLine("and             A.MFTRIDN         = b.LOC_CODE 			                ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //등록된 모델 중 최초 1개 조회(실시간 관망해석 화면 로드 시 최초로 띄움 - 소블록 코드도 맨위 한개 같이 조회 [화면 포커스용]-)
        public DataSet SelectFirstModel(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select          a.INP_NUMBER                        ");
            //queryString.AppendLine("                ,a.LFTRIDN                          ");
            //queryString.AppendLine("                ,a.MFTRIDN                          ");
            //queryString.AppendLine("                ,b.FTR_IDN                          ");
            //queryString.AppendLine("from            WH_TITLE        a                   ");
            //queryString.AppendLine("                ,CM_LOCATION    b                   ");
            //queryString.AppendLine("where           a.USE_GBN       = 'WH'              ");
            //queryString.AppendLine("and             a.MFTRIDN       is not null         ");
            //queryString.AppendLine("and             b.FTR_CODE      = 'BZ003'           ");
            //queryString.AppendLine("and             a.MFTRIDN       = b.PLOC_CODE       ");
            //queryString.AppendLine("and             rownum      = 1                     ");

            //wjs 수정(2012-06-28)
            queryString.AppendLine("select          a.INP_NUMBER                        ");
            queryString.AppendLine("                ,a.LFTRIDN                          ");
            queryString.AppendLine("                ,a.MFTRIDN                          ");
            //queryString.AppendLine("                ,b.FTR_IDN                          ");
            queryString.AppendLine("from            WH_TITLE        a                   ");
            //queryString.AppendLine("                ,CM_LOCATION    b                   ");
            queryString.AppendLine("where           a.USE_GBN       = 'WH'              ");
            //queryString.AppendLine("and             a.MFTRIDN       is not null         ");
            //queryString.AppendLine("and             b.FTR_CODE      = 'BZ003'           ");
            //queryString.AppendLine("and             a.MFTRIDN       = b.PLOC_CODE       ");
            queryString.AppendLine("and             rownum      = 1                     ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //해당 중블록의 INP번호와 최초 소블록코드를 조회
        public DataSet SelectModelByMiddleBlock(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.INP_NUMBER			                                ");
            queryString.AppendLine("            ,b.FTR_IDN			                                    ");
            queryString.AppendLine("from        WH_TITLE        a			                            ");
            queryString.AppendLine("            ,CM_LOCATION    b			                            ");
            queryString.AppendLine("where       a.USE_GBN        = 'WH'			                        ");
            queryString.AppendLine("and         b.PFTR_CODE      = 'BZ002'			                    ");
            queryString.AppendLine("and         b.PFTR_IDN       = '" + conditions["FTR_IDN"] + "'		");
            queryString.AppendLine("and         b.PLOC_CODE      = a.MFTRIDN                            ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //해당 소블록의 INP번호를 조회
        public DataSet SelectModelBySmallBlock(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      b.INP_NUMBER			                                ");
            queryString.AppendLine("from        CM_LOCATION     a			                            ");
            queryString.AppendLine("            ,WH_TITLE       b			                            ");
            queryString.AppendLine("where       a.FTR_CODE    = 'BZ003'			                        ");
            queryString.AppendLine("and         a.FTR_IDN     = '" + conditions["FTR_IDN"] + "'			");
            queryString.AppendLine("and         b.USE_GBN     = 'WH'                                    ");
            queryString.AppendLine("and         b.MFTRIDN     = a.PLOC_CODE                             ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //특정 시간(분까지)의 절점 해석결과를 조회
        public DataSet GetAnalysisNodeResultData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select          a.RPT_NUMBER			                                                                                        ");
            //queryString.AppendLine("                ,b.NODE_ID			                                                                                            ");
            //queryString.AppendLine("                ,b.RPT_NUMBER				                                                                                    ");
            //queryString.AppendLine("                ,b.NODE_ID				                                                                                        ");
            //queryString.AppendLine("                ,b.ANALYSIS_TIME			                                                                                    ");
            //queryString.AppendLine("                ,b.ELEVATION				                                                                                    ");
            //queryString.AppendLine("                ,b.BASEDEMAND				                                                                                    ");
            //queryString.AppendLine("                ,b.PATTERN				                                                                                        ");
            //queryString.AppendLine("                ,b.EMITTER				                                                                                        ");
            //queryString.AppendLine("                ,b.INITQUAL				                                                                                        ");
            //queryString.AppendLine("                ,b.SOURCEQUAL				                                                                                    ");
            //queryString.AppendLine("                ,b.SOURCEPAT				                                                                                    ");
            //queryString.AppendLine("                ,b.SOURCETYPE				                                                                                    ");
            //queryString.AppendLine("                ,b.TANKLEVEL				                                                                                    ");
            //queryString.AppendLine("                ,b.DEMAND					                                                                                    ");
            //queryString.AppendLine("                ,b.HEAD					                                                                                        ");
            //queryString.AppendLine("                ,b.PRESSURE				                                                                                        ");
            //queryString.AppendLine("                ,b.QUALITY				                                                                                        ");
            //queryString.AppendLine("                ,b.SOURCEMASS				                                                                                    ");
            //queryString.AppendLine("                ,b.INITVOLUME				                                                                                    ");
            //queryString.AppendLine("                ,b.MIXMODEL				                                                                                        ");
            //queryString.AppendLine("                ,b.MIXZONEVOL				                                                                                    ");
            //queryString.AppendLine("                ,b.TANKDIAM				                                                                                        ");
            //queryString.AppendLine("                ,b.MINVOLUME				                                                                                    ");
            //queryString.AppendLine("                ,b.VOLCURVE				                                                                                        ");
            //queryString.AppendLine("                ,b.MINLEVEL				                                                                                        ");
            //queryString.AppendLine("                ,b.MAXLEVEL				                                                                                        ");
            //queryString.AppendLine("                ,b.MIXFRACTION				                                                                                    ");
            //queryString.AppendLine("                ,b.TANK_KBULK				                                                                                    ");
            //queryString.AppendLine("                ,b.ANALYSIS_TYPE			                                                                                    ");
            //queryString.AppendLine("from            WH_RPT_MASTER           a			                                                                            ");
            //queryString.AppendLine("                ,WH_RPT_NODES           b			                                                                            ");
            //queryString.AppendLine("where           1 = 1			                                                                                                ");
            //queryString.AppendLine("and             a.AUTO_MANUAL   = 'A'                                                                                           ");
            ////queryString.AppendLine("and             to_char(to_date(a.TARGET_DATE,'yyyymmddhh24mi'),'yyyy-mm-dd hh24:mi') = '" + conditions["TARGET_DATE"] + "'		");
            //queryString.AppendLine("and             a.TARGET_DATE   = '" + conditions["TARGET_DATE"] + "'		                                                    ");
            //queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                            ");
            //queryString.AppendLine("and             a.RPT_NUMBER    = b.RPT_NUMBER			                                                                        ");

            queryString.AppendLine("select          a.RPT_NUMBER			                                                                                        ");
            //queryString.AppendLine("                ,b.NODE_ID			                                                                                            ");
            //queryString.AppendLine("                ,b.RPT_NUMBER				                                                                                    ");
            queryString.AppendLine("                ,b.NODE_ID				                                                                                        ");
            queryString.AppendLine("                ,b.ANALYSIS_TIME			                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.ELEVATION,2)) as ELEVATION		                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.BASEDEMAND,2)) as BASEDEMAND				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.PATTERN,2)) as PATTERN				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.EMITTER,2)) as EMITTER				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.INITQUAL,2)) as INITQUAL				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.SOURCEQUAL,2)) as SOURCEQUAL				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.SOURCEPAT,2)) as SOURCEPAT				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.SOURCETYPE,2)) as SOURCETYPE				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.TANKLEVEL,2)) as TANKLEVEL				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.DEMAND,2)) as DEMAND					                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.HEAD,2)) as HEAD					                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.PRESSURE,2)) as PRESSURE				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.QUALITY,2)) as QUALITY				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.SOURCEMASS,2)) as SOURCEMASS				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.INITVOLUME,2)) as INITVOLUME				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.MIXMODEL,2)) as MIXMODEL				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.MIXZONEVOL,2)) as MIXZONEVOL				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.TANKDIAM,2)) as TANKDIAM				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.MINVOLUME,2)) as MINVOLUME				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.VOLCURVE,2)) as VOLCURVE				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.MINLEVEL,2)) as MINLEVEL				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.MAXLEVEL,2)) as MAXLEVEL				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.MIXFRACTION,2)) as MIXFRACTION				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.TANK_KBULK,2)) as TANK_KBULK				                                                                                    ");
            queryString.AppendLine("                ,b.ANALYSIS_TYPE			                                                                                    ");
            queryString.AppendLine("from            WH_RPT_MASTER           a			                                                                            ");
            queryString.AppendLine("                ,WH_RPT_NODES           b			                                                                            ");
            queryString.AppendLine("where           1 = 1			                                                                                                ");
            queryString.AppendLine("and             a.TARGET_DATE   = '" + conditions["TARGET_DATE"] + "'		                                                    ");
            queryString.AppendLine("and             b.RPT_NUMBER    = a.RPT_NUMBER			                                                                        ");
            queryString.AppendLine("and             b.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                            ");
            queryString.AppendLine("and             b.NODE_ID    in (select id from wh_tags where inp_number = '" + conditions["INP_NUMBER"] + "' and type = 'NODE') ");
            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_NODES");
        }

        //특정 시간(분까지)의 링크 해석결과를 조회
        public DataSet GetAnalysisLinkResultData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select          a.RPT_NUMBER			                                                                                        ");
            //queryString.AppendLine("                ,b.LINK_ID					                                                                                    ");
            //queryString.AppendLine("                ,b.ANALYSIS_TIME				                                                                                ");
            //queryString.AppendLine("                ,b.ANALYSIS_TYPE				                                                                                ");
            //queryString.AppendLine("                ,b.DIAMETER				                                                                                        ");
            //queryString.AppendLine("                ,b.LENGTH					                                                                                    ");
            //queryString.AppendLine("                ,b.ROUGHNESS				                                                                                    ");
            //queryString.AppendLine("                ,b.MINORLOSS				                                                                                    ");
            //queryString.AppendLine("                ,b.INITSTATUS				                                                                                    ");
            //queryString.AppendLine("                ,b.INITSETTING				                                                                                    ");
            //queryString.AppendLine("                ,b.KBULK					                                                                                    ");
            //queryString.AppendLine("                ,b.KWALL					                                                                                    ");
            //queryString.AppendLine("                ,b.FLOW					                                                                                        ");
            //queryString.AppendLine("                ,b.VELOCITY				                                                                                        ");
            //queryString.AppendLine("                ,b.HEADLOSS				                                                                                        ");
            //queryString.AppendLine("                ,b.STATUS					                                                                                    ");
            //queryString.AppendLine("                ,b.SETTING					                                                                                    ");
            //queryString.AppendLine("                ,b.ENERGY					                                                                                    ");
            //queryString.AppendLine("from            WH_RPT_MASTER           a			                                                                            ");
            //queryString.AppendLine("                ,WH_RPT_LINKS           b			                                                                            ");
            //queryString.AppendLine("where           1 = 1			                                                                                                ");
            //queryString.AppendLine("and             a.AUTO_MANUAL   = 'A'                                                                                           ");
            ////queryString.AppendLine("and             to_char(to_date(a.TARGET_DATE,'yyyymmddhh24mi'),'yyyy-mm-dd hh24:mi') = '" + conditions["TARGET_DATE"] + "'		");
            //queryString.AppendLine("and             a.TARGET_DATE   = '" + conditions["TARGET_DATE"] + "'		                                                    ");
            //queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                            ");
            //queryString.AppendLine("and             a.RPT_NUMBER    = b.RPT_NUMBER                                                                                  ");

            //마곡 1블럭만 출력
            queryString.AppendLine("select          a.RPT_NUMBER			                                                                                        ");
            queryString.AppendLine("                ,b.LINK_ID					                                                                                    ");
            queryString.AppendLine("                ,b.ANALYSIS_TIME				                                                                                ");
            queryString.AppendLine("                ,b.ANALYSIS_TYPE				                                                                                ");
            queryString.AppendLine("                ,to_char(round(b.DIAMETER,2)) as DIAMETER				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.LENGTH,2)) as LENGTH					                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.ROUGHNESS,2)) as ROUGHNESS				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.MINORLOSS,2)) as MINORLOSS				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.INITSTATUS,2)) as INITSTATUS				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.INITSETTING,2)) as INITSETTING				                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.KBULK,2)) as KBULK					                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.KWALL,2)) as KWALL					                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.FLOW,2)) as FLOW					                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.VELOCITY,2)) as VELOCITY				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.HEADLOSS,2)) as HEADLOSS				                                                                                        ");
            queryString.AppendLine("                ,to_char(round(b.STATUS,2)) as STATUS					                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.SETTING,2)) as SETTING					                                                                                    ");
            queryString.AppendLine("                ,to_char(round(b.ENERGY,2)) as ENERGY					                                                                                    ");
            queryString.AppendLine("from            WH_RPT_MASTER           a			                                                                            ");
            queryString.AppendLine("                ,WH_RPT_LINKS           b			                                                                            ");
            queryString.AppendLine("where           1 = 1			                                                                                                ");
            queryString.AppendLine("and             a.TARGET_DATE   = '" + conditions["TARGET_DATE"] + "'		                                                    ");
            queryString.AppendLine("and             b.RPT_NUMBER    = a.RPT_NUMBER			                                                                        ");
            queryString.AppendLine("and             b.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                            ");
            queryString.AppendLine("and             b.LINK_ID    in (select id from wh_tags where inp_number = '" + conditions["INP_NUMBER"] + "' and type = 'LINK') ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_LINKS");
        }

        //최종해석시간 조회
        public DataSet SelectLastAnalysisTime(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select          to_char(max(to_date(TARGET_DATE,'yyyymmddhh24mi')), 'yyyymmddhh24mi')       as TARGET_DATE  ");
            //queryString.AppendLine("                ,to_char(max(to_date(TARGET_DATE,'yyyymmddhh24mi')),'yyyy-mm-dd hh24:mi')   as FORMED_DATE  ");
            //queryString.AppendLine("from            WH_RPT_MASTER                                                                               ");
            //queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'                                            ");
            //queryString.AppendLine("and             AUTO_MANUAL = 'A'                                                                           ");

            queryString.AppendLine("select          to_char(max(to_date(a.TARGET_DATE,'yyyymmddhh24mi')), 'yyyymmddhh24mi')       as TARGET_DATE  			");
            queryString.AppendLine("                ,to_char(max(to_date(a.TARGET_DATE,'yyyymmddhh24mi')),'yyyy-mm-dd hh24:mi')   as FORMED_DATE  			");
            queryString.AppendLine("                ,max(c.LOC_NAME)                                                              as LOC_NAME  			    ");
            queryString.AppendLine("from            WH_RPT_MASTER       a 			                                                                        ");
            queryString.AppendLine("                ,WH_TITLE           b			                                                                        ");
            queryString.AppendLine("                ,CM_LOCATION        c			                                                                        ");
            queryString.AppendLine("where           a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                    ");
            queryString.AppendLine("and             a.AUTO_MANUAL   = 'A'                                                                           		");
            queryString.AppendLine("and             a.INP_NUMBER    = b.INP_NUMBER			                                                                ");
            queryString.AppendLine("and             a.MFTRIDN       = c.LOC_CODE                                                                            ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_MASTER");
        }

        //해석결과 항목별 지역코드 (NODE)
        public DataSet GetNodeLocationDataList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          e.LOC_NAME 			                                                                                                                                                            ");
            queryString.AppendLine("                ,e.LOC_CODE                                                                                                                                                                     ");
            queryString.AppendLine("from            WH_TITLE            a                                                                     			                                                                            ");
            queryString.AppendLine("                ,WH_RPT_MASTER      b                                                                     			                                                                            ");
            queryString.AppendLine("                ,WH_RPT_NODES       c			                                                                                                                                                ");
            queryString.AppendLine("                ,WH_TAGS            d			                                                                                                                                                ");
            queryString.AppendLine("                ,CM_LOCATION        e			                                                                                                                                                ");
            queryString.AppendLine("where           a.INP_NUMBER    = b.INP_NUMBER                                                            			                                                                            ");
            queryString.AppendLine("and             b.INP_NUMBER    = c.INP_NUMBER                                                            			                                                                            ");
            queryString.AppendLine("and             b.RPT_NUMBER    = c.RPT_NUMBER			                                                                                                                                        ");
            queryString.AppendLine("and             c.NODE_ID       = d.ID			                                                                                                                                                ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 1)+1,instr(d.POSITION_INFO, '|', 1, 2)-1 - instr(d.POSITION_INFO, '|', 1, 1)) = e.FTR_CODE		                        ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 2)+1,length(d.POSITION_INFO) - instr(d.POSITION_INFO, '|', 1, 2)) = e.FTR_IDN			                                    ");
            queryString.AppendLine("and             b.AUTO_MANUAL   = 'A'                                                                     			                                                                            ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                ");
            queryString.AppendLine("and             d.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                ");
            queryString.AppendLine("and             d.TYPE          = 'NODE'			                                                                                                                                            ");
            queryString.AppendLine("group by        e.LOC_CODE			                                                                                                                                                            ");
            queryString.AppendLine("                ,e.LOC_NAME			                                                                                                                                                            ");
            queryString.AppendLine("order by        e.LOC_NAME                                                                                                                                                                      ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "nodeLocation");
        }
        
        //해석결과 항목별 지역코드 (LINK)
        public DataSet GetLinkLocationDataList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          e.LOC_NAME 			                                                                                                                                    ");
            queryString.AppendLine("                ,e.LOC_CODE                                                                                                                                             ");
            queryString.AppendLine("from            WH_TITLE            a                                                                     			                                                    ");
            queryString.AppendLine("                ,WH_RPT_MASTER      b                                                                     			                                                    ");
            queryString.AppendLine("                ,WH_RPT_LINKS       c			                                                                                                                        ");
            queryString.AppendLine("                ,WH_TAGS            d			                                                                                                                        ");
            queryString.AppendLine("                ,CM_LOCATION        e			                                                                                                                        ");
            queryString.AppendLine("where           a.INP_NUMBER    = b.INP_NUMBER                                                            			                                                    ");
            queryString.AppendLine("and             b.INP_NUMBER    = c.INP_NUMBER                                                            			                                                    ");
            queryString.AppendLine("and             b.RPT_NUMBER    = c.RPT_NUMBER			                                                                                                                ");
            queryString.AppendLine("and             c.LINK_ID       = d.ID			                                                                                                                        ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(POSITION_INFO, '|', 1, 5)+1,instr(d.POSITION_INFO, '|', 1, 6)-1 - instr(d.POSITION_INFO, '|', 1, 5)) = e.FTR_CODE		    ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 6)+1,length(d.POSITION_INFO) - instr(d.POSITION_INFO, '|', 1, 6)) = e.FTR_IDN			            ");
            queryString.AppendLine("and             b.AUTO_MANUAL   = 'A'                                                                     			                                                    ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                        ");
            queryString.AppendLine("and             d.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                        ");
            queryString.AppendLine("and             d.TYPE          = 'LINK'			                                                                                                                    ");

            //if (conditions["period"] != null)
            //{
                //queryString.Append("and         to_date(b.RPT_DATE,'yyyymmdd') between to_date('" + conditions["startDate"] + "','yyyy-mm-dd') and to_date('" + conditions["endDate"] + "'||' 23:59:59','yyyy-mm-dd hh24:mi:ss')    ");
                //queryString.AppendLine("and         to_date(b.RPT_DATE,'yyyymmddhh24miss') > sysdate - " + conditions["period"] + " / 24                            ");
            //}

            queryString.AppendLine("group by        e.LOC_CODE			                                                                                                                                    ");
            queryString.AppendLine("                ,e.LOC_NAME			                                                                                                                                    ");
            queryString.AppendLine("order by        e.LOC_NAME                                                                                                                                              ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "linkLocation");
        }

        //해석결과 항목별 지역코드(통합)
        public DataSet SelectLocationData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          LOC_NAME			                                                                                                                                        ");
            queryString.AppendLine("                ,LOC_CODE			                                                                                                                                        ");
            queryString.AppendLine("from (               			                                                                                                                                            ");
            queryString.AppendLine("    select          c.LOC_NAME 			                                                                                                                     			    ");
            queryString.AppendLine("                    ,c.LOC_CODE                                                                                                                                          	");
            queryString.AppendLine("    from            WH_TITLE            a                                                                                                                          			");
            queryString.AppendLine("                    ,WH_TAGS            b			                                                                                                             			");
            queryString.AppendLine("                    ,CM_LOCATION        c			                                                                                                             			");
            queryString.AppendLine("    where           substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 1)+1,instr(b.POSITION_INFO, '|', 1, 2)-1 - instr(b.POSITION_INFO, '|', 1, 1)) = c.FTR_CODE		");
            queryString.AppendLine("    and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 2)+1,length(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 2)) = c.FTR_IDN						");
            queryString.AppendLine("    and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                             			");
            queryString.AppendLine("    and             a.INP_NUMBER    = b.INP_NUMBER			                                                                                                                ");
            queryString.AppendLine("    and             b.TYPE          = 'NODE'			                                                                                                                    ");
            queryString.AppendLine("    group by        c.LOC_CODE			                                                                                                                			        ");
            queryString.AppendLine("                    ,c.LOC_NAME			                                                                                                                			        ");
            queryString.AppendLine("    union			                                                                                                                                                        ");
            queryString.AppendLine("    select          c.LOC_NAME 			                                                                                                                     			    ");
            queryString.AppendLine("                    ,c.LOC_CODE                                                                                                                                          	");
            queryString.AppendLine("    from            WH_TITLE            a                                                                                                                        			");
            queryString.AppendLine("                    ,WH_TAGS            b			                                                                                                             			");
            queryString.AppendLine("                    ,CM_LOCATION        c			                                                                                                             			");
            queryString.AppendLine("    where           substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 5)+1,instr(b.POSITION_INFO, '|', 1, 6)-1 - instr(b.POSITION_INFO, '|', 1, 5)) = c.FTR_CODE		");
            queryString.AppendLine("    and             substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 6)+1,length(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 6)) = c.FTR_IDN						");
            queryString.AppendLine("    and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                             			");
            queryString.AppendLine("    and             a.INP_NUMBER    = b.INP_NUMBER			                                                                                                                ");
            queryString.AppendLine("    and             b.TYPE          = 'LINK'			                                                                                                                    ");
            queryString.AppendLine("    group by        c.LOC_CODE			                                                                                                                			        ");
            queryString.AppendLine("                    ,c.LOC_NAME			  			                                                                                                                        ");
            queryString.AppendLine("    )                    			                                                                                                                                        ");
            queryString.AppendLine("order by LOC_NAME			                                                                                                                                                ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_LOCATION");
        }

        //결과 팝업화면의 Node해석 결과 조회
        public DataSet GetAnalysisNodeResultList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          to_char(to_date(b.TARGET_DATE,'yyyymmddhh24miss'),'yyyy-mm-dd hh24:mi:ss')    as TARGET_DATE    			                                                                                    ");
            queryString.AppendLine("                ,c.NODE_ID			                                                          			                                                                                                        ");
            queryString.AppendLine("                ,round(c.ELEVATION,2) ELEVATION				                                                  			                                                                                                        ");
            queryString.AppendLine("                ,round(c.DEMAND,2) DEMAND					                                          			                                                                                                                ");
            queryString.AppendLine("                ,round(c.HEAD,2) HEAD					                                                  			                                                                                                            ");
            queryString.AppendLine("                ,round(c.PRESSURE,2) PRESSURE				                                                  			                                                                                                            ");
            queryString.AppendLine("                ,round(c.QUALITY,2)	QUALITY			                                                  			                                                                                                            ");
            queryString.AppendLine("                ,c.ANALYSIS_TYPE			                                                                                                                                                                    ");
            queryString.AppendLine("                ,e.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("from            WH_TITLE            a                                                                     			                                                                                            ");
            queryString.AppendLine("                ,WH_RPT_MASTER      b                                                                     			                                                                                            ");
            queryString.AppendLine("                ,WH_RPT_NODES       c			                                                                                                                                                                ");
            queryString.AppendLine("                ,WH_TAGS            d			                                                                                                                                                                ");
            queryString.AppendLine("                ,CM_LOCATION        e			                                                                                                                                                                ");
            queryString.AppendLine("where           a.INP_NUMBER    = b.INP_NUMBER                                                            			                                                                                            ");
            queryString.AppendLine("and             b.INP_NUMBER    = c.INP_NUMBER                                                            			                                                                                            ");
            queryString.AppendLine("and             b.RPT_NUMBER    = c.RPT_NUMBER			                                                                                                                                                        ");
            queryString.AppendLine("and             c.NODE_ID       = d.ID			                                                                                                                                                                ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 1)+1,instr(d.POSITION_INFO, '|', 1, 2)-1 - instr(d.POSITION_INFO, '|', 1, 1)) = e.FTR_CODE		                                        ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 2)+1,length(d.POSITION_INFO) - instr(d.POSITION_INFO, '|', 1, 2)) = e.FTR_IDN			                                                    ");
            queryString.AppendLine("and             b.AUTO_MANUAL   = 'A'                                                                     			                                                                                            ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                                ");
            queryString.AppendLine("and             d.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                                ");
            queryString.AppendLine("and             d.TYPE          = 'NODE'			                                                                                                                                                            ");
            //queryString.AppendLine("and             to_date(b.TARGET_DATE,'yyyymmddhh24mi') between to_date('" + conditions["startDate"] + "','yyyy-mm-dd') and to_date('" + conditions["endDate"] + "'||' 23:59:59','yyyy-mm-dd hh24:mi:ss')       ");
            queryString.AppendLine("and             substr(b.TARGET_DATE,0,8) = '" + conditions["startDate"] + "'                                                                                                                                   ");

            if (conditions["nodeItem"] != null && conditions["nodeFormular"] != null && conditions["nodeValue"] != null)
            {
                queryString.AppendLine("and         c." + conditions["nodeItem"] + " " + conditions["nodeFormular"] + " " + conditions["nodeValue"] + "                                                                                             ");
            }

            if(conditions["nodeLocation"] != null)
            {
                queryString.AppendLine("and         e.LOC_CODE      = '" + conditions["nodeLocation"] + "'                                                                                                                                          ");
            }

            queryString.AppendLine("order by        e.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("                ,to_date(b.TARGET_DATE,'yyyymmddhh24miss') desc                                                                                                                                                 ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_NODES");
        }

        //결과 팝업화면의 Node해석 결과 조회
        public DataSet GetAnalysisNodeResultList(OracleDBManager dbManager, Hashtable conditions, string nodelist)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          to_char(to_date(b.TARGET_DATE,'yyyymmddhh24miss'),'yyyy-mm-dd hh24:mi:ss')    as TARGET_DATE    			                                                                                    ");
            queryString.AppendLine("                ,c.NODE_ID			                                                          			                                                                                                        ");
            queryString.AppendLine("                ,round(c.ELEVATION,2) ELEVATION				                                                  			                                                                                                        ");
            queryString.AppendLine("                ,round(c.DEMAND,2) DEMAND					                                          			                                                                                                                ");
            queryString.AppendLine("                ,round(c.HEAD,2) HEAD					                                                  			                                                                                                            ");
            queryString.AppendLine("                ,round(c.PRESSURE,2) PRESSURE				                                                  			                                                                                                            ");
            queryString.AppendLine("                ,round(c.QUALITY,2)	QUALITY			                                                  			                                                                                                            ");
            queryString.AppendLine("                ,c.ANALYSIS_TYPE			                                                                                                                                                                    ");
            queryString.AppendLine("                ,e.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("from            WH_TITLE            a                                                                     			                                                                                            ");
            queryString.AppendLine("                ,WH_RPT_MASTER      b                                                                     			                                                                                            ");
            queryString.AppendLine("                ,WH_RPT_NODES       c			                                                                                                                                                                ");
            queryString.AppendLine("                ,WH_TAGS            d			                                                                                                                                                                ");
            queryString.AppendLine("                ,CM_LOCATION        e			                                                                                                                                                                ");
            queryString.AppendLine("where           a.INP_NUMBER    = b.INP_NUMBER                                                            			                                                                                            ");
            queryString.AppendLine("and             b.INP_NUMBER    = c.INP_NUMBER                                                            			                                                                                            ");
            queryString.AppendLine("and             b.RPT_NUMBER    = c.RPT_NUMBER			                                                                                                                                                        ");
            queryString.AppendLine("and             c.NODE_ID       = d.ID			                                                                                                                                                                ");
            queryString.AppendLine("and             c.NODE_ID in (" + nodelist + ")");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 1)+1,instr(d.POSITION_INFO, '|', 1, 2)-1 - instr(d.POSITION_INFO, '|', 1, 1)) = e.FTR_CODE		                                        ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 2)+1,length(d.POSITION_INFO) - instr(d.POSITION_INFO, '|', 1, 2)) = e.FTR_IDN			                                                    ");
            queryString.AppendLine("and             b.AUTO_MANUAL   = 'A'                                                                     			                                                                                            ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                                ");
            queryString.AppendLine("and             d.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                                ");
            queryString.AppendLine("and             d.TYPE          = 'NODE'			                                                                                                                                                            ");
            //queryString.AppendLine("and             to_date(b.TARGET_DATE,'yyyymmddhh24mi') between to_date('" + conditions["startDate"] + "','yyyy-mm-dd') and to_date('" + conditions["endDate"] + "'||' 23:59:59','yyyy-mm-dd hh24:mi:ss')       ");
            queryString.AppendLine("and             substr(b.TARGET_DATE,0,8) = '" + conditions["startDate"] + "'                                                                                                                                   ");

            if (conditions["nodeItem"] != null && conditions["nodeFormular"] != null && conditions["nodeValue"] != null)
            {
                queryString.AppendLine("and         c." + conditions["nodeItem"] + " " + conditions["nodeFormular"] + " " + conditions["nodeValue"] + "                                                                                             ");
            }

            if (conditions["nodeLocation"] != null)
            {
                queryString.AppendLine("and         e.LOC_CODE      = '" + conditions["nodeLocation"] + "'                                                                                                                                          ");
            }

            queryString.AppendLine("order by        e.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("                ,to_date(b.TARGET_DATE,'yyyymmddhh24miss') desc                                                                                                                                                 ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_NODES");
        }

        //결과 팝업화면의 Link해석 결과 조회
        public DataSet GetAnalysisLinkResultList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          to_char(to_date(b.TARGET_DATE,'yyyymmddhh24miss'),'yyyy-mm-dd hh24:mi:ss')    as TARGET_DATE    			                                                                                    ");
            queryString.AppendLine("                ,c.LINK_ID					                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.DIAMETER,2) DIAMETER				                                                                                                                                                                        ");
            queryString.AppendLine("                ,round(c.LENGTH,2) LENGTH					                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.FLOW,2) FLOW					                                                                                                                                                                        ");
            queryString.AppendLine("                ,round(c.VELOCITY,2) VELOCITY				                                                                                                                                                                        ");
            queryString.AppendLine("                ,round(c.HEADLOSS,2) HEADLOSS				                                                                                                                                                                        ");
            queryString.AppendLine("                ,round(c.ROUGHNESS,2) ROUGHNESS				                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.MINORLOSS,2) MINORLOSS				                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.KBULK,2) KBULK					                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.KWALL,2) KWALL					                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.STATUS,2) STATUS					                                                                                                                                                                    ");
            queryString.AppendLine("                ,c.ANALYSIS_TYPE                                                                                                                                                                                ");
            queryString.AppendLine("                ,e.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("from            WH_TITLE            a                                                                     			                                                                                            ");
            queryString.AppendLine("                ,WH_RPT_MASTER      b                                                                     			                                                                                            ");
            queryString.AppendLine("                ,WH_RPT_LINKS       c			                                                                                                                                                                ");
            queryString.AppendLine("                ,WH_TAGS            d			                                                                                                                                                                ");
            queryString.AppendLine("                ,CM_LOCATION        e			                                                                                                                                                                ");
            queryString.AppendLine("where           a.INP_NUMBER    = b.INP_NUMBER                                                            			                                                                                            ");
            queryString.AppendLine("and             b.INP_NUMBER    = c.INP_NUMBER                                                            			                                                                                            ");
            queryString.AppendLine("and             b.RPT_NUMBER    = c.RPT_NUMBER			                                                                                                                                                        ");
            queryString.AppendLine("and             c.LINK_ID       = d.ID			                                                                                                                                                                ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 5)+1,instr(d.POSITION_INFO, '|', 1, 6)-1 - instr(d.POSITION_INFO, '|', 1, 5)) = e.FTR_CODE		                                        ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 6)+1,length(d.POSITION_INFO) - instr(d.POSITION_INFO, '|', 1, 6)) = e.FTR_IDN			                                                    ");
            queryString.AppendLine("and             b.AUTO_MANUAL   = 'A'                                                                     			                                                                                            ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                                ");
            queryString.AppendLine("and             d.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                                ");
            queryString.AppendLine("and             d.TYPE          = 'LINK'			                                                                                                                                                            ");
            //queryString.AppendLine("and             to_date(b.TARGET_DATE,'yyyymmddhh24mi') between to_date('" + conditions["startDate"] + "','yyyy-mm-dd') and to_date('" + conditions["endDate"] + "'||' 23:59:59','yyyy-mm-dd hh24:mi:ss')       ");
            queryString.AppendLine("and             substr(b.TARGET_DATE,0,8) = '" + conditions["startDate"] + "'                                                                                                                                   ");

            if (conditions["linkItem"] != null && conditions["linkFormular"] != null && conditions["linkValue"] != null)
            {
                queryString.AppendLine("and         c." + conditions["linkItem"] + " " + conditions["linkFormular"] + " " + conditions["linkValue"] + "                                                                                             ");
            }

            if (conditions["linkLocation"] != null)
            {
                queryString.AppendLine("and         e.LOC_CODE      = '" + conditions["linkLocation"] + "'                                                                                                                                          ");
            }

            queryString.AppendLine("order by        e.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("                ,to_date(b.TARGET_DATE,'yyyymmddhh24miss') desc                                                                                                                                                 ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_LINKS");
        }

        //결과 팝업화면의 Link해석 결과 조회
        public DataSet GetAnalysisLinkResultList(OracleDBManager dbManager, Hashtable conditions, string linklist)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          to_char(to_date(b.TARGET_DATE,'yyyymmddhh24miss'),'yyyy-mm-dd hh24:mi:ss')    as TARGET_DATE    			                                                                                    ");
            queryString.AppendLine("                ,c.LINK_ID					                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.DIAMETER,2) DIAMETER				                                                                                                                                                                        ");
            queryString.AppendLine("                ,round(c.LENGTH,2) LENGTH					                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.FLOW,2) FLOW					                                                                                                                                                                        ");
            queryString.AppendLine("                ,round(c.VELOCITY,2) VELOCITY				                                                                                                                                                                        ");
            queryString.AppendLine("                ,round(c.HEADLOSS,2) HEADLOSS				                                                                                                                                                                        ");
            queryString.AppendLine("                ,round(c.ROUGHNESS,2) ROUGHNESS				                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.MINORLOSS,2) MINORLOSS				                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.KBULK,2) KBULK					                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.KWALL,2) KWALL					                                                                                                                                                                    ");
            queryString.AppendLine("                ,round(c.STATUS,2) STATUS					                                                                                                                                                                    ");
            queryString.AppendLine("                ,c.ANALYSIS_TYPE                                                                                                                                                                                ");
            queryString.AppendLine("                ,e.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("from            WH_TITLE            a                                                                     			                                                                                            ");
            queryString.AppendLine("                ,WH_RPT_MASTER      b                                                                     			                                                                                            ");
            queryString.AppendLine("                ,WH_RPT_LINKS       c			                                                                                                                                                                ");
            queryString.AppendLine("                ,WH_TAGS            d			                                                                                                                                                                ");
            queryString.AppendLine("                ,CM_LOCATION        e			                                                                                                                                                                ");
            queryString.AppendLine("where           a.INP_NUMBER    = b.INP_NUMBER                                                            			                                                                                            ");
            queryString.AppendLine("and             b.INP_NUMBER    = c.INP_NUMBER                                                            			                                                                                            ");
            queryString.AppendLine("and             b.RPT_NUMBER    = c.RPT_NUMBER			                                                                                                                                                        ");
            queryString.AppendLine("and             c.LINK_ID       = d.ID			                                                                                                                                                                ");
            queryString.AppendLine("and             c.LINK_ID in (" + linklist + ")");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 5)+1,instr(d.POSITION_INFO, '|', 1, 6)-1 - instr(d.POSITION_INFO, '|', 1, 5)) = e.FTR_CODE		                                        ");
            queryString.AppendLine("and             substr(d.POSITION_INFO,instr(d.POSITION_INFO, '|', 1, 6)+1,length(d.POSITION_INFO) - instr(d.POSITION_INFO, '|', 1, 6)) = e.FTR_IDN			                                                    ");
            queryString.AppendLine("and             b.AUTO_MANUAL   = 'A'                                                                     			                                                                                            ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                                ");
            queryString.AppendLine("and             d.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                                ");
            queryString.AppendLine("and             d.TYPE          = 'LINK'			                                                                                                                                                            ");
            //queryString.AppendLine("and             to_date(b.TARGET_DATE,'yyyymmddhh24mi') between to_date('" + conditions["startDate"] + "','yyyy-mm-dd') and to_date('" + conditions["endDate"] + "'||' 23:59:59','yyyy-mm-dd hh24:mi:ss')       ");
            queryString.AppendLine("and             substr(b.TARGET_DATE,0,8) = '" + conditions["startDate"] + "'                                                                                                                                   ");

            if (conditions["linkItem"] != null && conditions["linkFormular"] != null && conditions["linkValue"] != null)
            {
                queryString.AppendLine("and         c." + conditions["linkItem"] + " " + conditions["linkFormular"] + " " + conditions["linkValue"] + "                                                                                             ");
            }

            if (conditions["linkLocation"] != null)
            {
                queryString.AppendLine("and         e.LOC_CODE      = '" + conditions["linkLocation"] + "'                                                                                                                                          ");
            }

            queryString.AppendLine("order by        e.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("                ,to_date(b.TARGET_DATE,'yyyymmddhh24miss') desc                                                                                                                                                 ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_LINKS");
        }

        //결과 팝업화면의 Node별 해석결과 조회(그래프)
        public DataSet SelectNodeAnalysisDataForGraph(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      to_char(to_date(a.TARGET_DATE,'yyyymmddhh24mi'),'yyyy-mm-dd hh24:mi')   as  TARGET_DATE                                                                                                     ");
            queryString.AppendLine("            ,b.NODE_ID                                                                                                                                                                                  ");
            queryString.AppendLine("            ,round(b.DEMAND,2) DEMAND                                                                                                                                                                                   ");
            queryString.AppendLine("            ,round(b.HEAD,2) HEAD                                                                                                                                                                                    ");
            queryString.AppendLine("            ,round(b.PRESSURE,2) PRESSURE                                                                                                        ");
            queryString.AppendLine("from        WH_RPT_MASTER       a                                                                                                                                                                       ");
            queryString.AppendLine("            ,WH_RPT_NODES       b                                                                                                                                                                       ");
            queryString.AppendLine("where       a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                                                                                                        ");
            queryString.AppendLine("and         b.NODE_ID       = '" + conditions["NODE_ID"] + "'                                                                                                                                           ");
            //queryString.AppendLine("and         to_date(a.TARGET_DATE,'yyyymmddhh24mi') between to_date('" + conditions["startDate"] + "','yyyy-mm-dd') and to_date('" + conditions["endDate"] + "'||' 23:59:59','yyyy-mm-dd hh24:mi:ss')   ");
            queryString.AppendLine("and         substr(a.TARGET_DATE,0,8) = '" + conditions["startDate"] + "'                                                                                                                               ");
            queryString.AppendLine("and         a.AUTO_MANUAL   = 'A'                                                                                                                                                                       ");
            queryString.AppendLine("and         a.RPT_NUMBER    = b.RPT_NUMBER                                                                                                                                                              ");
            queryString.AppendLine("order by    to_date(a.TARGET_DATE,'yyyymmddhh24mi')                                                                                                                                                     ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_MASTER");
        }

        //결과 팝업화면의 Link별 해석결과 조회(그래프)
        public DataSet SelectLinkAnalysisDataForGraph(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      to_char(to_date(a.TARGET_DATE,'yyyymmddhh24mi'),'yyyy-mm-dd hh24:mi') TARGET_DATE			                                                                                                ");
            queryString.AppendLine("            ,LINK_ID			                                                                                                                                                                        ");
            queryString.AppendLine("            ,round(FLOW,2) FLOW			                                                                                                                                                                            ");
            queryString.AppendLine("            ,round(VELOCITY,2) VELOCITY		                                                                                                                                                                        ");
            queryString.AppendLine("from        WH_RPT_MASTER       a			                                                                                                                                                            ");
            queryString.AppendLine("            ,WH_RPT_LINKS       b			                                                                                                                                                            ");
            queryString.AppendLine("where       a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                            ");
            queryString.AppendLine("and         b.LINK_ID       = '" + conditions["NODE_ID"] + "'			                                                                                                                                ");
            //queryString.AppendLine("and         to_date(a.TARGET_DATE,'yyyymmddhh24mi') between to_date('" + conditions["startDate"] + "','yyyy-mm-dd') and to_date('" + conditions["endDate"] + "'||' 23:59:59','yyyy-mm-dd hh24:mi:ss')   ");
            queryString.AppendLine("and         substr(a.TARGET_DATE,0,8) = '" + conditions["startDate"] + "'                                                                                                                               ");
            queryString.AppendLine("and         a.AUTO_MANUAL   = 'A'			                                                                                                                                                            ");
            queryString.AppendLine("and         a.RPT_NUMBER    = b.RPT_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("order by    to_date(a.TARGET_DATE,'yyyymmddhh24mi')	                                                                                                                                                    ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_MASTER");
        }

        //해당 모델의 실측지점 조회
        public DataSet GetRealtimeCheckpointList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          b.DESCRIPTION			                                ");
            queryString.AppendLine("                ,a.TAGNAME			                                    ");
            queryString.AppendLine("from            WH_MEASURE_CHECKPOINT           a			            ");
            queryString.AppendLine("                ,IF_IHTAGS                      b			            ");
            queryString.AppendLine("where           a.TAGNAME       = b.TAGNAME         			        ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             a.METER_TYPE    = '" + conditions["METER_TYPE"] + "'    ");
            queryString.AppendLine("order by        b.DESCRIPTION				                            ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MEASURE_CHECKPOINT");
        }

        //실측값 비교 List 조회
        public DataSet GetAnalysisResultVsRealtimeCheck(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          to_char(to_date(a.TARGET_DATE,'yyyymmddhh24miss'),'yyyy-mm-dd hh24:mi:ss')      as TARGET_DATE			                                                                                        ");
            queryString.AppendLine("                ,b." + conditions["layerIDField"] + "			                                as ID                                                                                                           ");
            queryString.AppendLine("                ,b.ANALYSIS_TYPE			                                                                                                                                                                    ");
            queryString.AppendLine("                ,c.FTR_IDN			                                                                                                                                                                            ");
            queryString.AppendLine("                ,round(b." + conditions["analysisItem"] + ",2)                                  as ANALYSIS_RESULT 			                                                                                    ");
            queryString.AppendLine("                ,round(d.VALUE,2)                                                               as VALUE			                                                                                            ");
            queryString.AppendLine("from            WH_RPT_MASTER                               a			                                                                                                                                        ");
            queryString.AppendLine("                ," + conditions["layerTable"] + "           b			                                                                                                                                        ");
            queryString.AppendLine("                ,WH_MEASURE_CHECKPOINT                      c			                                                                                                                                        ");
            queryString.AppendLine("                ,IF_GATHER_REALTIME                         d			                                                                                                                                        ");
            queryString.AppendLine("where           a.RPT_NUMBER                            = b.RPT_NUMBER			                                                                                                                                ");
            queryString.AppendLine("and             a.INP_NUMBER                            = b.INP_NUMBER			                                                                                                                                ");
            queryString.AppendLine("and             b." + conditions["layerIDField"] + "    = c.ID			                                                                                                                                        ");
            queryString.AppendLine("and             c.INP_NUMBER                            = a.INP_NUMBER			                                                                                                                                ");
            queryString.AppendLine("and             c.TAGNAME                               = d.TAGNAME			                                                                                                                                    ");
            queryString.AppendLine("and             c.METER_TYPE                            = '" + conditions["METER_TYPE"] + "'                                                                                                                    ");
            queryString.AppendLine("and             c.TAGNAME                               = '" + conditions["TAGNAME"] + "'                                                                                                                       ");
            //queryString.AppendLine("and             substr(a.RPT_DATE,0,12)                 = to_char(to_date(d.TIMESTAMP,'yyyy-mm-dd am hh:mi:ss'),'yyyymmddhh24mi') 			");
            queryString.AppendLine("and             substr(a.TARGET_DATE,0,12)              = to_char(d.TIMESTAMP,'yyyymmddhh24mi') 			                                                                                                    ");
            queryString.AppendLine("and             a.INP_NUMBER                            = '" + conditions["INP_NUMBER"] + "'			                                                                                                        ");
            queryString.AppendLine("and             to_date(a.TARGET_DATE,'yyyymmddhh24mi') between to_date('" + conditions["startDate"] + "','yyyy-mm-dd') and to_date('" + conditions["endDate"] + "'||' 23:59:59','yyyy-mm-dd hh24:mi:ss')       ");

            //if (conditions["period"] != null)
            //{
                //queryString.Append("and         to_date(a.TARGET_DATE,'yyyymmdd') between to_date('" + conditions["startDate"] + "','yyyy-mm-dd') and to_date('" + conditions["endDate"] + "'||' 23:59:59','yyyy-mm-dd hh24:mi:ss')    ");
                //queryString.AppendLine("and         to_date(a.RPT_DATE,'yyyymmddhh24miss') > sysdate - " + conditions["period"] + " / 24                            ");
            //}

            queryString.AppendLine("order by        to_date(a.TARGET_DATE,'yyyymmddhh24miss')                                                                                                                                                       ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MEASURE_CHECKPOINT");
        }

        //해석항목 Rage 조회
        public DataSet SelectAnalysisItemRangeData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          RESULT_TYPE                                             ");
            queryString.AppendLine("                ,RESULT_CODE                                            ");
            queryString.AppendLine("                ,RANGE1                                                 ");
            queryString.AppendLine("                ,RANGE2                                                 ");
            queryString.AppendLine("                ,RANGE3                                                 ");
            queryString.AppendLine("                ,RANGE4                                                 ");
            queryString.AppendLine("                ,INP_NUMBER                                             ");
            queryString.AppendLine("from            WH_ANALYSIS_RESULT_RANGE                                ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'        ");
            queryString.AppendLine("and             RESULT_TYPE = '" + conditions["RESULT_TYPE"] + "'       ");
            queryString.AppendLine("and             RESULT_CODE = '" + conditions["RESULT_CODE"] + "'       ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_ANALYSIS_RESULT_RANGE");
        }

        //실측지점 리스트 조회
        public DataSet SelectRealtimeCheckpointList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select          a.LAYER_TYPE			                                ");
            queryString.AppendLine("select          a.ID				                                    ");
            queryString.AppendLine("                ,a.METER_TYPE			                                ");
            queryString.AppendLine("                ,d.CODE_NAME            as METER_TYPE_NAME			    ");
            queryString.AppendLine("                ,a.TAGNAME			                                    ");
            queryString.AppendLine("                ,a.FTR_IDN			                                    ");
            queryString.AppendLine("                ,b.LOC_CODE			                                    ");
            queryString.AppendLine("                ,c.LOC_NAME			                                    ");
            queryString.AppendLine("from            WH_MEASURE_CHECKPOINT   a			                    ");
            queryString.AppendLine("                ,IF_IHTAGS              b			                    ");
            queryString.AppendLine("                ,CM_LOCATION            c			                    ");
            queryString.AppendLine("                ,CM_CODE                d			                    ");
            queryString.AppendLine("where           a.INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'      ");
            queryString.AppendLine("and             a.TAGNAME       = b.TAGNAME(+)			                ");
            queryString.AppendLine("and             b.LOC_CODE      = c.LOC_CODE(+)			                ");
            queryString.AppendLine("and             d.PCODE         = '1003'			                    ");
            queryString.AppendLine("and             a.METER_TYPE    = d.CODE  			                    ");
            queryString.AppendLine("order by        LOC_NAME				                                ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MEASURE_CHECKPOINT");
        }

        //실측지점 입력
        public void InsertRealtimeCheckpoint(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_MEASURE_CHECKPOINT   (                           ");
            queryString.AppendLine("                                        INP_NUMBER              ");
            queryString.AppendLine("                                        ,ID                     ");
            queryString.AppendLine("                                        ,METER_TYPE             ");
            queryString.AppendLine("                                        ,FTR_IDN                ");
            queryString.AppendLine("                                        ,TAGNAME                ");
            queryString.AppendLine("                                    ) values (                  ");
            queryString.AppendLine("                                        :1                      ");
            queryString.AppendLine("                                        ,:2                     ");
            queryString.AppendLine("                                        ,:3                     ");
            queryString.AppendLine("                                        ,:4                     ");
            queryString.AppendLine("                                        ,:5                     ");
            queryString.AppendLine("                                    )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["ID"];
            parameters[2].Value = (string)conditions["METER_TYPE"];
            parameters[3].Value = (string)conditions["FTR_IDN"];
            parameters[4].Value = (string)conditions["TAGNAME"];

            //Console.WriteLine(queryString);

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //실측지점 수정
        public void UpdateRealtimeCheckpoint(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_MEASURE_CHECKPOINT set                                                       ");
            queryString.AppendLine("                                    FTR_IDN    = '" + conditions["FTR_IDN"] + "'        ");
            queryString.AppendLine("                                    ,TAGNAME    = '" + conditions["TAGNAME"] + "'       ");
            queryString.AppendLine("where   INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'                                ");
            queryString.AppendLine("and     METER_TYPE  = '" + conditions["METER_TYPE"] + "'                                ");
            queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'                                        ");

            //Console.WriteLine(queryString);

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //실측지점 삭제
        public void DeleteRealtimeCheckpoint(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from     WH_MEASURE_CHECKPOINT                               ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             METER_TYPE  = '" + conditions["METER_TYPE"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //실측지점 존재여부 확인
        public DataSet IsExistRealtimeCheckpoint(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          INP_NUMBER                                          ");
            queryString.AppendLine("                ,ID                                                 ");
            queryString.AppendLine("from            WH_MEASURE_CHECKPOINT                               ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             METER_TYPE  = '" + conditions["METER_TYPE"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MEASURE_CHECKPOINT");
        }

        //해석결과표출범위 설정 리스트 조회
        public DataSet SelectAnalysisResultRangeList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.INP_NUMBER			                                                ");
            queryString.AppendLine("            ,a.RESULT_TYPE			                                                ");
            queryString.AppendLine("            ,a.RESULT_CODE			                                                ");
            queryString.AppendLine("            ,decode(a.RESULT_TYPE,'1004','NODE','LINK')     as LAYER_TYPE			");
            queryString.AppendLine("            ,b.CODE_NAME			                        as RESULT_CODE_NAME     ");
            queryString.AppendLine("            ,a.RANGE1			                                                    ");
            queryString.AppendLine("            ,a.RANGE2			                                                    ");
            queryString.AppendLine("            ,a.RANGE3			                                                    ");
            queryString.AppendLine("            ,a.RANGE4			                                                    ");
            queryString.AppendLine("from        WH_ANALYSIS_RESULT_RANGE    a			                                ");
            queryString.AppendLine("            ,CM_CODE                    b			                                ");
            queryString.AppendLine("where       a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			        ");
            queryString.AppendLine("and         a.RESULT_TYPE   = b.PCODE			                                    ");
            queryString.AppendLine("and         a.RESULT_CODE   = b.CODE                                                ");
            queryString.AppendLine("order by    decode(a.RESULT_TYPE,'1004','NODE','LINK')                              ");
            queryString.AppendLine("            ,b.CODE_NAME                                                            ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_ANALYSIS_RESULT_RANGE");
        }

        //해석결과표출범위 설정 수정
        public void UpdateAnalysisResultRange(OracleDBManager dbManager,Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_ANALYSIS_RESULT_RANGE set                                ");
            queryString.AppendLine("        RANGE1          = '" + conditions["RANGE1"] + "'            ");
            queryString.AppendLine("        ,RANGE2         = '" + conditions["RANGE2"] + "'            ");
            queryString.AppendLine("        ,RANGE3         = '" + conditions["RANGE3"] + "'            ");
            queryString.AppendLine("        ,RANGE4         = '" + conditions["RANGE4"] + "'            ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'        ");
            queryString.AppendLine("and     RESULT_TYPE     = '" + conditions["RESULT_TYPE"] + "'       ");
            queryString.AppendLine("and     RESULT_CODE     = '" + conditions["RESULT_CODE"] + "'       ");

            //Console.WriteLine(queryString);

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //대수용가 리스트 조회
        public DataSet SelectLargeConsumerList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.ID			                                                                                                                                            ");
            queryString.AppendLine("            ,a.LCONSUMER_NM			                                                                                                                                    ");
            queryString.AppendLine("            ,a.APPLY_INCREASE			                                                                                                                                ");
            queryString.AppendLine("            ,a.APPLY_CONTINUE_TIME			                                                                                                                            ");
            queryString.AppendLine("            ,c.LOC_NAME			                                                                                                                                        ");
            queryString.AppendLine("            ,TAGNAME                                                                                                                                                    ");
            queryString.AppendLine("            ,a.APPLY_YN			                                                                                                                                        ");
            queryString.AppendLine("from        WH_LCONSUMER_INFO           a			                                                                                                                    ");
            queryString.AppendLine("            ,WH_TAGS                    b			                                                                                                                    ");
            queryString.AppendLine("            ,CM_LOCATION                c			                                                                                                                    ");
            queryString.AppendLine("where       a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                            ");
            queryString.AppendLine("and         b.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                            ");
            queryString.AppendLine("and         b.TYPE          = 'NODE'			                                                                                                                        ");
            queryString.AppendLine("and         a.INP_NUMBER    = b.INP_NUMBER			                                                                                                                    ");
            queryString.AppendLine("and         a.ID            = b.ID			                                                                                                                            ");
            queryString.AppendLine("and         substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 1)+1,instr(b.POSITION_INFO, '|', 1, 2)-1 - instr(b.POSITION_INFO, '|', 1, 1)) = c.FTR_CODE			");
            queryString.AppendLine("and         substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 2)+1,length(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 2))             = c.FTR_IDN			    ");
            queryString.AppendLine("order by    c.LOC_NAME			                                                                                                                                        ");
            queryString.AppendLine("            ,a.LCONSUMER_NM	                                                                                                                                            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_INFO");
        }

        //대수용가 입력
        public void InsertLargeConsumer(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_LCONSUMER_INFO (                                 ");
            queryString.AppendLine("                                INP_NUMBER                      ");
            queryString.AppendLine("                                ,ID                             ");
            queryString.AppendLine("                                ,LCONSUMER_NM                   ");
            queryString.AppendLine("                                ,APPLY_INCREASE                 ");
            queryString.AppendLine("                                ,APPLY_CONTINUE_TIME            ");
            queryString.AppendLine("                                ,APPLY_YN                       ");
            queryString.AppendLine("                                ,TAGNAME                        ");
            queryString.AppendLine("                            ) values (                          ");
            queryString.AppendLine("                                :1                              ");
            queryString.AppendLine("                                ,:2                             ");
            queryString.AppendLine("                                ,:3                             ");
            queryString.AppendLine("                                ,:4                             ");
            queryString.AppendLine("                                ,:5                             ");
            queryString.AppendLine("                                ,:6                             ");
            queryString.AppendLine("                                ,:7                             ");
            queryString.AppendLine("                            )                                   ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["ID"];
            parameters[2].Value = (string)conditions["LCONSUMER_NM"];
            parameters[3].Value = (string)conditions["APPLY_INCREASE"];
            parameters[4].Value = (string)conditions["APPLY_CONTINUE_TIME"];
            parameters[5].Value = (string)conditions["APPLY_YN"];
            parameters[6].Value = (string)conditions["TAGNAME"];

            //Console.WriteLine(queryString);

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //대수용가 수정
        public void UpdateLargeConsumer(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_LCONSUMER_INFO set                                                   ");
            queryString.AppendLine("        LCONSUMER_NM            = '" + conditions["LCONSUMER_NM"] + "'          ");
            queryString.AppendLine("        ,APPLY_INCREASE         = '" + conditions["APPLY_INCREASE"] + "'        ");
            queryString.AppendLine("        ,APPLY_CONTINUE_TIME    = '" + conditions["APPLY_CONTINUE_TIME"] + "'   ");
            queryString.AppendLine("        ,APPLY_YN               = '" + conditions["APPLY_YN"] + "'              ");
            queryString.AppendLine("        ,TAGNAME                = '" + conditions["TAGNAME"] + "'               ");
            queryString.AppendLine("where   INP_NUMBER              = '" + conditions["INP_NUMBER"] + "'            ");
            queryString.AppendLine("and     ID                      = '" + conditions["ID"] + "'                    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //대수용가 삭제
        public void DeleteLargeConsumer(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_LCONSUMER_INFO                                   ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and         ID          = '" + conditions["ID"] + "'            ");

            //Console.WriteLine(queryString);

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //대수용가 존재여부 확인
        public DataSet IsExistLargeConsumer(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          INP_NUMBER                                          ");
            queryString.AppendLine("                ,ID                                                 ");
            queryString.AppendLine("from            WH_LCONSUMER_INFO                                   ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_INFO");
        }

        //대수용가 동작 타임테이블 조회
        public DataSet SelectLargeConsumerTimeTable(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          case WEEKDAY			                                ");
            queryString.AppendLine("                when '1'			                                    ");
            queryString.AppendLine("                    then '일'			                                ");
            queryString.AppendLine("                when '2'			                                    ");
            queryString.AppendLine("                    then '월'			                                ");
            queryString.AppendLine("                when '3'			                                    ");
            queryString.AppendLine("                    then '화'			                                ");
            queryString.AppendLine("                when '4'			                                    ");
            queryString.AppendLine("                    then '수'			                                ");
            queryString.AppendLine("                when '5'			                                    ");
            queryString.AppendLine("                    then '목'			                                ");
            queryString.AppendLine("                when '6'			                                    ");
            queryString.AppendLine("                    then '금'			                                ");
            queryString.AppendLine("                when '7'			                                    ");
            queryString.AppendLine("                    then '토'			                                ");
            queryString.AppendLine("                end             as WEEKDAY_TEXT			                ");
            queryString.AppendLine("                ,WEEKDAY                                                ");
            queryString.AppendLine("                ,H01			                                        ");
            queryString.AppendLine("                ,H02			                                        ");
            queryString.AppendLine("                ,H03			                                        ");
            queryString.AppendLine("                ,H04			                                        ");
            queryString.AppendLine("                ,H05			                                        ");
            queryString.AppendLine("                ,H06			                                        ");
            queryString.AppendLine("                ,H07			                                        ");
            queryString.AppendLine("                ,H08			                                        ");
            queryString.AppendLine("                ,H09			                                        ");
            queryString.AppendLine("                ,H10			                                        ");
            queryString.AppendLine("                ,H11			                                        ");
            queryString.AppendLine("                ,H12			                                        ");
            queryString.AppendLine("                ,H13			                                        ");
            queryString.AppendLine("                ,H14			                                        ");
            queryString.AppendLine("                ,H15			                                        ");
            queryString.AppendLine("                ,H16			                                        ");
            queryString.AppendLine("                ,H17			                                        ");
            queryString.AppendLine("                ,H18			                                        ");
            queryString.AppendLine("                ,H19			                                        ");
            queryString.AppendLine("                ,H20			                                        ");
            queryString.AppendLine("                ,H21			                                        ");
            queryString.AppendLine("                ,H22			                                        ");
            queryString.AppendLine("                ,H23			                                        ");
            queryString.AppendLine("                ,H24			                                        ");
            queryString.AppendLine("from            WH_LCONSUMER_TIME			                            ");
            queryString.AppendLine("where           INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID              = '" + conditions["ID"] + "'			");
            queryString.AppendLine("order by        WEEKDAY                                                 ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_TIME");
        }

        //대수용가 동작 타임테이블 입력
        public void InsertLargeConsumerTimeTable(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_LCONSUMER_TIME   (                       ");
            queryString.AppendLine("                                    INP_NUMBER          ");
            queryString.AppendLine("                                    ,ID                 ");
            queryString.AppendLine("                                    ,WEEKDAY            ");
            queryString.AppendLine("									,H01			    ");
            queryString.AppendLine("									,H02			    ");
            queryString.AppendLine("									,H03			    ");
            queryString.AppendLine("									,H04			    ");
            queryString.AppendLine("									,H05			    ");
            queryString.AppendLine("									,H06			    ");
            queryString.AppendLine("									,H07			    ");
            queryString.AppendLine("									,H08			    ");
            queryString.AppendLine("									,H09			    ");
            queryString.AppendLine("									,H10			    ");
            queryString.AppendLine("									,H11			    ");
            queryString.AppendLine("									,H12			    ");
            queryString.AppendLine("									,H13			    ");
            queryString.AppendLine("									,H14			    ");
            queryString.AppendLine("									,H15			    ");
            queryString.AppendLine("									,H16			    ");
            queryString.AppendLine("									,H17			    ");
            queryString.AppendLine("									,H18			    ");
            queryString.AppendLine("									,H19			    ");
            queryString.AppendLine("									,H20			    ");
            queryString.AppendLine("									,H21			    ");
            queryString.AppendLine("									,H22			    ");
            queryString.AppendLine("									,H23			    ");
            queryString.AppendLine("									,H24			    ");
            queryString.AppendLine("                                ) values (              ");
            queryString.AppendLine("                                     :1                 ");
            queryString.AppendLine("                                     ,:2                ");
            queryString.AppendLine("                                     ,:3                ");
            queryString.AppendLine("                                     ,:4                ");
            queryString.AppendLine("                                     ,:5                ");
            queryString.AppendLine("                                     ,:6                ");
            queryString.AppendLine("                                     ,:7                ");
            queryString.AppendLine("                                     ,:8                ");
            queryString.AppendLine("                                     ,:9                ");
            queryString.AppendLine("                                     ,:10               ");
            queryString.AppendLine("                                     ,:11               ");
            queryString.AppendLine("                                     ,:12               ");
            queryString.AppendLine("                                     ,:13               ");
            queryString.AppendLine("                                     ,:14               ");
            queryString.AppendLine("                                     ,:15               ");
            queryString.AppendLine("                                     ,:16               ");
            queryString.AppendLine("                                     ,:17               ");
            queryString.AppendLine("                                     ,:18               ");
            queryString.AppendLine("                                     ,:19               ");
            queryString.AppendLine("                                     ,:20               ");
            queryString.AppendLine("                                     ,:21               ");
            queryString.AppendLine("                                     ,:22               ");
            queryString.AppendLine("                                     ,:23               ");
            queryString.AppendLine("                                     ,:24               ");
            queryString.AppendLine("                                     ,:25               ");
            queryString.AppendLine("                                     ,:26               ");
            queryString.AppendLine("                                     ,:27               ");
            queryString.AppendLine("                                 )                      ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
                ,new OracleParameter("10", OracleDbType.Varchar2)
                ,new OracleParameter("11", OracleDbType.Varchar2)
                ,new OracleParameter("12", OracleDbType.Varchar2)
                ,new OracleParameter("13", OracleDbType.Varchar2)
                ,new OracleParameter("14", OracleDbType.Varchar2)
                ,new OracleParameter("15", OracleDbType.Varchar2)
                ,new OracleParameter("16", OracleDbType.Varchar2)
                ,new OracleParameter("17", OracleDbType.Varchar2)
                ,new OracleParameter("18", OracleDbType.Varchar2)
                ,new OracleParameter("19", OracleDbType.Varchar2)
                ,new OracleParameter("20", OracleDbType.Varchar2)
                ,new OracleParameter("21", OracleDbType.Varchar2)
                ,new OracleParameter("22", OracleDbType.Varchar2)
                ,new OracleParameter("23", OracleDbType.Varchar2)
                ,new OracleParameter("24", OracleDbType.Varchar2)
                ,new OracleParameter("25", OracleDbType.Varchar2)
                ,new OracleParameter("26", OracleDbType.Varchar2)
                ,new OracleParameter("27", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["ID"];
            parameters[2].Value = (string)conditions["WEEKDAY"];
            parameters[3].Value = (string)conditions["H01"];
            parameters[4].Value = (string)conditions["H02"];
            parameters[5].Value = (string)conditions["H03"];
            parameters[6].Value = (string)conditions["H04"];
            parameters[7].Value = (string)conditions["H05"];
            parameters[8].Value = (string)conditions["H06"];
            parameters[9].Value = (string)conditions["H07"];
            parameters[10].Value = (string)conditions["H08"];
            parameters[11].Value = (string)conditions["H09"];
            parameters[12].Value = (string)conditions["H10"];
            parameters[13].Value = (string)conditions["H11"];
            parameters[14].Value = (string)conditions["H12"];
            parameters[15].Value = (string)conditions["H13"];
            parameters[16].Value = (string)conditions["H14"];
            parameters[17].Value = (string)conditions["H15"];
            parameters[18].Value = (string)conditions["H16"];
            parameters[19].Value = (string)conditions["H17"];
            parameters[20].Value = (string)conditions["H18"];
            parameters[21].Value = (string)conditions["H19"];
            parameters[22].Value = (string)conditions["H20"];
            parameters[23].Value = (string)conditions["H21"];
            parameters[24].Value = (string)conditions["H22"];
            parameters[25].Value = (string)conditions["H23"];
            parameters[26].Value = (string)conditions["H24"];

            //Console.WriteLine(queryString);

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //대수용가 동작 타임테이블 수정
        public void UpdateLargeConsumerTimeTable(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_LCONSUMER_TIME set                               ");
            queryString.AppendLine("        H01         = '" + conditions["H01"] + "'           ");
            queryString.AppendLine("        ,H02         = '" + conditions["H02"] + "'          ");
            queryString.AppendLine("        ,H03         = '" + conditions["H03"] + "'          ");
            queryString.AppendLine("        ,H04         = '" + conditions["H04"] + "'          ");
            queryString.AppendLine("        ,H05         = '" + conditions["H05"] + "'          ");
            queryString.AppendLine("        ,H06         = '" + conditions["H06"] + "'          ");
            queryString.AppendLine("        ,H07         = '" + conditions["H07"] + "'          ");
            queryString.AppendLine("        ,H08         = '" + conditions["H08"] + "'          ");
            queryString.AppendLine("        ,H09         = '" + conditions["H09"] + "'          ");
            queryString.AppendLine("        ,H10         = '" + conditions["H10"] + "'          ");
            queryString.AppendLine("        ,H11         = '" + conditions["H11"] + "'          ");
            queryString.AppendLine("        ,H12         = '" + conditions["H12"] + "'          ");
            queryString.AppendLine("        ,H13         = '" + conditions["H13"] + "'          ");
            queryString.AppendLine("        ,H14         = '" + conditions["H14"] + "'          ");
            queryString.AppendLine("        ,H15         = '" + conditions["H15"] + "'          ");
            queryString.AppendLine("        ,H16         = '" + conditions["H16"] + "'          ");
            queryString.AppendLine("        ,H17         = '" + conditions["H17"] + "'          ");
            queryString.AppendLine("        ,H18         = '" + conditions["H18"] + "'          ");
            queryString.AppendLine("        ,H19         = '" + conditions["H19"] + "'          ");
            queryString.AppendLine("        ,H20         = '" + conditions["H20"] + "'          ");
            queryString.AppendLine("        ,H21         = '" + conditions["H21"] + "'          ");
            queryString.AppendLine("        ,H22         = '" + conditions["H22"] + "'          ");
            queryString.AppendLine("        ,H23         = '" + conditions["H23"] + "'          ");
            queryString.AppendLine("        ,H24         = '" + conditions["H24"] + "'          ");
            queryString.AppendLine("where   INP_NUMBER   = '" + conditions["INP_NUMBER"] + "'   ");
            queryString.AppendLine("and     ID           = '" + conditions["ID"] + "'           ");
            queryString.AppendLine("and     WEEKDAY      = '" + conditions["WEEKDAY"] + "'      ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //대수용가 동작정보 삭제
        public void DeleteActivateLargeConsumer(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_LCONSUMER_ACTIVATE                               ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and         ID          = '" + conditions["ID"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //대수용가 동작 타임테이블 삭제
        public void DeleteLargeConsumerTimeTable(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_LCONSUMER_TIME                                   ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and         ID          = '" + conditions["ID"] + "'            ");
            //queryString.AppendLine("and         WEEKDAY     = '" + conditions["WEEKDAY"] + "'       ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //실측데이터(이동식) Dump Master를 반환한다.
        public DataSet SelectMeasuerDumpMaster(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          a.DUMP_NUMBER			                                                                                                                                                        ");
            queryString.AppendLine("                ,a.DUMP_TYPE			                                                                                                                                                        ");
            queryString.AppendLine("                ,a.LOC_CODE			                                                                                                                                                            ");
            queryString.AppendLine("                ,b.LOC_NAME			                                                                                                                                                            ");
            queryString.AppendLine("                ,a.ID			                                                                                                                                                                ");
            queryString.AppendLine("                ,to_char(to_date(a.DUMP_DATE,'yyyymmddhh24miss'),'yyyy-mm-dd hh24:mi:ss') as DUMP_DATE			                                                                                ");
            queryString.AppendLine("                ,DUMP_COMMENT			                                                                                                                                                        ");
            queryString.AppendLine("                ,DUMP_FILE_PATH			                                                                                                                                                        ");
            queryString.AppendLine("from            WH_MEASURE_DUMP_MASTER          a			                                                                                                                                    ");
            queryString.AppendLine("                ,CM_LOCATION                    b			                                                                                                                                    ");
            queryString.AppendLine("where           a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                ");
            
            if(conditions["LOC_CODE"] != null)
            {
                queryString.AppendLine("and         a.LOC_CODE      = " + conditions["LOC_CODE"] + "                                                                                                                                ");
            }

            if (conditions["DUMP_TYPE"] != null)
            {
                queryString.AppendLine("and         a.DUMP_TYPE     = '" + conditions["DUMP_TYPE"] + "'                                                                                                                             ");
            }

            queryString.AppendLine("and             to_date(a.DUMP_DATE,'yyyymmddhh24miss') between to_date('" + conditions["startDate"] + "','yyyymmdd') and to_date('" + conditions["endDate"] + "235959','yyyymmddhh24miss')     ");
            queryString.AppendLine("and             a.LOC_CODE      = b.LOC_CODE			                                                                                                                                        ");
            queryString.AppendLine("order by        to_char(to_date(a.DUMP_DATE,'yyyymmddhh24miss'),'yyyy-mm-dd hh24:mi:ss') desc			                                                                                        ");
            queryString.AppendLine("                ,b.LOC_NAME	                                                                                                                                                                    ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MEASURE_DUMP_MASTER");
        }

        //실즉데이터(이동식) Dump Master를 입력한다.
        public void InsertMeasureDumpMaster(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_MEASURE_DUMP_MASTER (                                            ");
            queryString.AppendLine("                                        DUMP_NUMBER                             ");
            queryString.AppendLine("                                        ,INP_NUMBER                             ");
            queryString.AppendLine("                                        ,DUMP_TYPE                              ");
            queryString.AppendLine("                                        ,ID                                     ");
            queryString.AppendLine("                                        ,DUMP_DATE                              ");
            queryString.AppendLine("                                        ,DUMP_COMMENT                           ");
            queryString.AppendLine("                                        ,LOC_CODE                               ");
            queryString.AppendLine("                                        ,DUMP_FILE_PATH                         ");
            queryString.AppendLine("                                    ) values (                                  ");
            queryString.AppendLine("                                        :1                                      ");
            queryString.AppendLine("                                        ,:2                                     ");
            queryString.AppendLine("                                        ,:3                                     ");
            queryString.AppendLine("                                        ,:4                                     ");
            queryString.AppendLine("                                        ,to_char(sysdate,'yyyymmddhh24miss')    ");
            queryString.AppendLine("                                        ,:5                                     ");
            queryString.AppendLine("                                        ,:6                                     ");
            queryString.AppendLine("                                        ,:7                                     ");
            queryString.AppendLine("                                    )                                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["DUMP_NUMBER"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];
            parameters[2].Value = (string)conditions["DUMP_TYPE"];
            parameters[3].Value = (string)conditions["ID"];
            parameters[4].Value = (string)conditions["DUMP_COMMENT"];
            parameters[5].Value = (string)conditions["LOC_CODE"];
            parameters[6].Value = (string)conditions["DUMP_FILE_PATH"];

            //Console.WriteLine(queryString);

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //실즉데이터(이동식) Dump Master를 수정한다 (지점 ID에 한해서만 수정가능).
        public void UpdateMeasureDumpMaster(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_MEASURE_DUMP_MASTER  set                                 ");
            queryString.AppendLine("        ID                  = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("        ,DUMP_COMMENT       = '" + conditions["DUMP_COMMENT"] + "'  ");
            queryString.AppendLine("        ,LOC_CODE           = '" + conditions["LOC_CODE"] + "'      ");
            queryString.AppendLine("where   DUMP_NUMBER         = '" + conditions["DUMP_NUMBER"] + "'   ");
            queryString.AppendLine("and     INP_NUMBER          = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //실측데이터(이동식) Dump Master를 삭제한다.
        public void DeleteMeasureDumpMaster(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_MEASURE_DUMP_MASTER                              ");
            queryString.AppendLine("where       DUMP_NUMBER = '" + conditions["DUMP_NUMBER"] + "'   ");
            queryString.AppendLine("and         INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //실측데이터(이동식)를 입력한다.
        public void InsertMeasureDumpData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_MEASURE_DUMP_DATA    (                   ");
            queryString.AppendLine("                                        DUMP_NUMBER     ");
            queryString.AppendLine("                                        ,INP_NUMBER     ");
            queryString.AppendLine("                                        ,DUMP_DATE      ");
            queryString.AppendLine("                                        ,VALUE          ");
            queryString.AppendLine("                                    ) values (          ");
            queryString.AppendLine("                                        :1              ");
            queryString.AppendLine("                                        ,:2             ");
            queryString.AppendLine("                                        ,:3             ");
            queryString.AppendLine("                                        ,:4             ");
            queryString.AppendLine("                                    )                   ");

            OracleCommand commandOracle = new OracleCommand();
            commandOracle.CommandText = queryString.ToString();
            commandOracle.Connection = dbManager.Connection;
            commandOracle.ArrayBindCount = ((List<string>)conditions["dumpNumberList"]).Count;

            OracleParameter prmDumpNumberList = new OracleParameter("1", OracleDbType.Varchar2);
            prmDumpNumberList.Direction = ParameterDirection.Input;
            prmDumpNumberList.Value = ((List<string>)conditions["dumpNumberList"]).ToArray();
            commandOracle.Parameters.Add(prmDumpNumberList);

            OracleParameter prmInpNumberList = new OracleParameter("2", OracleDbType.Varchar2);
            prmInpNumberList.Direction = ParameterDirection.Input;
            prmInpNumberList.Value = ((List<string>)conditions["inpNumberList"]).ToArray();
            commandOracle.Parameters.Add(prmInpNumberList);

            OracleParameter prmDumpDateList = new OracleParameter("3", OracleDbType.Varchar2);
            prmDumpDateList.Direction = ParameterDirection.Input;
            prmDumpDateList.Value = ((List<string>)conditions["dumpDateList"]).ToArray();
            commandOracle.Parameters.Add(prmDumpDateList);

            OracleParameter prmValueList = new OracleParameter("4", OracleDbType.Varchar2);
            prmValueList.Direction = ParameterDirection.Input;
            prmValueList.Value = ((List<string>)conditions["valueList"]).ToArray();
            commandOracle.Parameters.Add(prmValueList);

            commandOracle.ExecuteNonQuery();
        }

        //실측데이터(이동식)와 유량해석결과를 비교한다.
        public DataSet SelectFlowCompareData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      to_char(to_date(d.DUMP_DATE,'yyyymmddhh24mi'),'yyyy-mm-dd hh24:mi')    as DUMP_DATE				");
            queryString.AppendLine("            ,round(b.FLOW,2) 								                			       as ANALYSIS_DATA         ");
            queryString.AppendLine("            ,round(d.VALUE,2) as VALUE								                			                            ");
            queryString.AppendLine("from        WH_RPT_MASTER               a			                                			            ");
            queryString.AppendLine("            ,WH_RPT_LINKS               b			                                			            ");
            queryString.AppendLine("            ,WH_MEASURE_DUMP_MASTER     c			                                			            ");
            queryString.AppendLine("            ,WH_MEASURE_DUMP_DATA       d			                                			            ");
            queryString.AppendLine("where       a.INP_NUMBER                            = c.INP_NUMBER				                            ");
            queryString.AppendLine("and         a.AUTO_MANUAL                           = 'A'			                					    ");
            queryString.AppendLine("and         c.DUMP_NUMBER                           = '" + conditions["DUMP_NUMBER"] + "'       	       	");
            queryString.AppendLine("and         a.RPT_NUMBER                            = b.RPT_NUMBER			        			            ");
            queryString.AppendLine("and         b.LINK_ID                               = c.ID			                			            ");
            queryString.AppendLine("and         c.DUMP_NUMBER                           = d.DUMP_NUMBER			        			            ");
            queryString.AppendLine("and         to_date(d.DUMP_DATE,'yyyymmddhh24mi')   = to_date(a.TARGET_DATE,'yyyymmddhh24mi')				");
            queryString.AppendLine("order by    to_date(d.DUMP_DATE,'yyyymmddhh24miss')     								                    ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MEASURE_DUMP_DATA");
        }

        //실측데이터(이동식)와 압력해석결과를 비교한다
        public DataSet SelectPressureCompareData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      to_char(to_date(d.DUMP_DATE,'yyyymmddhh24miss'),'yyyy-mm-dd hh24:mi')	as DUMP_DATE				");
            queryString.AppendLine("            ,round(b.PRESSURE,2)									                            as ANALYSIS_DATA			");
            queryString.AppendLine("            ,round(d.VALUE,2) as VALUE								                				                            ");
            queryString.AppendLine("from        WH_RPT_MASTER               a			                                				            ");
            queryString.AppendLine("            ,WH_RPT_NODES               b			                                				            ");
            queryString.AppendLine("            ,WH_MEASURE_DUMP_MASTER     c			                                				            ");
            queryString.AppendLine("            ,WH_MEASURE_DUMP_DATA       d			                                				            ");
            queryString.AppendLine("where       a.INP_NUMBER        = c.INP_NUMBER				        			                                ");
            queryString.AppendLine("and         a.AUTO_MANUAL       = 'A'			                				                                ");
            queryString.AppendLine("and         c.DUMP_NUMBER       = '" + conditions["DUMP_NUMBER"] + "'       				                	");
            queryString.AppendLine("and         a.RPT_NUMBER        = b.RPT_NUMBER			        				                                ");
            queryString.AppendLine("and         b.NODE_ID           = c.ID			                				                                ");
            queryString.AppendLine("and         c.DUMP_NUMBER       = d.DUMP_NUMBER			        				                                ");
            queryString.AppendLine("and         d.DUMP_DATE         = a.TARGET_DATE||'00'				                                            ");
            queryString.AppendLine("order by    to_date(d.DUMP_DATE,'yyyymmddhh24miss')     					                                    ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MEASURE_DUMP_DATA");
        }

        //동작중인 대수용가 리스트 조회
        public DataSet SelectActivateLargeConsumerList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          d.LOC_NAME			                                                                                                                                            ");
            queryString.AppendLine("                ,b.LCONSUMER_NM		    	                                                                                                                                    ");
            queryString.AppendLine("                ,a.ID			                                                                                                                                                ");
            queryString.AppendLine("                ,a.INCREASE_AMOUNT			                                                                                                                                    ");
            queryString.AppendLine("                ,a.ACTIVATE_TIME			                                                                                                                                    ");
            queryString.AppendLine("from            WH_LCONSUMER_ACTIVATE   a			                                                                                                                            ");
            queryString.AppendLine("                ,WH_LCONSUMER_INFO      b			                                                                                                                            ");
            queryString.AppendLine("                ,WH_TAGS                c			                                                                                                                            ");
            queryString.AppendLine("                ,CM_LOCATION            d			                                                                                                                            ");
            queryString.AppendLine("where           a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                ");
            queryString.AppendLine("and             a.STATUS        = 'A'			                                                                                                                                ");
            queryString.AppendLine("and             c.TYPE          = 'NODE'			                                                                                                                            ");
            queryString.AppendLine("and             a.INP_NUMBER    = b.INP_NUMBER			                                                                                                                        ");
            queryString.AppendLine("and             a.INP_NUMBER    = c.INP_NUMBER			                                                                                                                        ");
            queryString.AppendLine("and             a.ID            = b.ID			                                                                                                                                ");
            queryString.AppendLine("and             a.ID            = c.ID 			                                                                                                                                ");
            queryString.AppendLine("and             d.FTR_CODE      = substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 1)+1,instr(c.POSITION_INFO, '|', 1, 2)-1 - instr(c.POSITION_INFO, '|', 1, 1))			");
            queryString.AppendLine("and             d.FTR_IDN       = substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 2)+1,length(c.POSITION_INFO) - instr(c.POSITION_INFO, '|', 1, 2))			            ");
            queryString.AppendLine("order by        d.LOC_NAME                                                                                                                                                      ");
            queryString.AppendLine("                ,b.LCONSUMER_NM                                                                                                                                                 ");

            Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LCONSUMER_ACTIVATE");
        }

        //태그리스트 조회
        public DataSet SelectTagList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          a.TAGNAME			                                    ");
            queryString.AppendLine("                ,a.DESCRIPTION			                                ");
            queryString.AppendLine("from            IF_IHTAGS       a			                            ");
            queryString.AppendLine("                ,CM_LOCATION    b			                            ");
            queryString.AppendLine("                ,IF_TAG_GBN     c			                            ");
            queryString.AppendLine("                ,WH_TITLE       d			                            ");
            queryString.AppendLine("where           d.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'    ");
            //queryString.AppendLine("and             c.TAG_GBN       = '" + conditions["TAG_GBN"] + "'	    ");
            queryString.AppendLine("and             c.TAG_GBN       = 'FRI'	                                ");
            queryString.AppendLine("and             d.MFTRIDN      = b.PLOC_CODE			                ");
            queryString.AppendLine("and             a.LOC_CODE      = b.LOC_CODE			                ");
            queryString.AppendLine("and             a.TAGNAME       = c.TAGNAME			                    ");
            queryString.AppendLine("order by        a.DESCRIPTION				                            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "IF_IHTAGS");
        }

        //실시간 관망해석 설정 조회
        public DataSet SelectAnalysisSettingData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          INP_NUMBER                                          ");
            queryString.AppendLine("                ,ERROR_SAVE_YN                                      ");
            queryString.AppendLine("                ,LOW_PRESSURE                                       ");
            queryString.AppendLine("from            WH_ANALYSIS_SETTING                                 ");
            queryString.AppendLine("where           INP_NUMBER = '" + conditions["INP_NUMBER"] + "'     ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_ANALYSIS_SETTING");
        }

        //실시간 관망해석 설정 저장
        public void UpdateAnalysisSettingData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_ANALYSIS_SETTING     set                                                             ");
            queryString.AppendLine("                                ERROR_SAVE_YN       = '" + conditions["ERROR_SAVE_YN"] + "'     ");
            queryString.AppendLine("                                ,LOW_PRESSURE       = '" + conditions["LOW_PRESSURE"] + "'      ");
            queryString.AppendLine("where   INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'                                        ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //해석오류리스트 조회
        public DataSet SelectAnalysisErrorList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          a.INP_NUMBER			                                                                                                                                                                        ");
            queryString.AppendLine("                ,b.TITLE			                                                                                                                                                                            ");
            queryString.AppendLine("                ,to_char(to_date(a.INCREASE_DATE, 'yyyymmddhh24mi'),'yyyy-mm-dd hh24:mi')   as INCREASE_DATE			                                                                                        ");
            queryString.AppendLine("                ,INCREASE_DATE                                                              as INCREASE_DATE_ORI                                                                                                ");
            queryString.AppendLine("from            WH_REALTIME_ANALYSIS_ERROR      a			                                                                                                                                                    ");
            queryString.AppendLine("                ,WH_TITLE                       b			                                                                                                                                                    ");
            queryString.AppendLine("where           a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                                                                ");
            queryString.AppendLine("and             to_date(a.INCREASE_DATE, 'yyyymmddhh24mi') between to_date('" + conditions["startErrDate"] + "0000', 'yyyymmddhh24mi') and to_date('" + conditions["endErrDate"] + "2359', 'yyyymmddhh24mi')  	");
            queryString.AppendLine("and             a.INP_NUMBER    = b.INP_NUMBER			                                                                                                                                                        ");
            queryString.AppendLine("group by        a.INCREASE_DATE			                                                                                                                                                                        ");
            queryString.AppendLine("                ,a.INP_NUMBER			                                                                                                                                                                        ");
            queryString.AppendLine("                ,b.TITLE			                                                                                                                                                                            ");
            queryString.AppendLine("order by        to_date(a.INCREASE_DATE, 'yyyymmddhh24mi') desc			                                                                                                                                        ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_REALTIME_ANALYSIS_ERROR");
        }

        //해석오류 상세내역
        public DataSet SelectAnalysisErrorDetailList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          a.ERROR_CODE			                                                                        ");
            queryString.AppendLine("                ,a.IDX			                                                                                ");
            queryString.AppendLine("                ,b.CODE_NAME			                                                                        ");
            queryString.AppendLine("                ,to_char(to_date(a.INCREASE_DATE, 'yyyymmddhh24mi'), 'yyyy-mm-dd hh24:mi')  as INCREASE_DATE	");
            queryString.AppendLine("from            WH_REALTIME_ANALYSIS_ERROR      a			                                                    ");
            queryString.AppendLine("                ,CM_CODE                        b			                                                    ");
            queryString.AppendLine("where           a.INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'			                            ");
            queryString.AppendLine("and             a.INCREASE_DATE     = '" + conditions["INCREASE_DATE"] + "'			                            ");
            queryString.AppendLine("and             b.PCODE             = '1006'			                                                        ");
            queryString.AppendLine("and             a.ERROR_CODE        = trim(b.CODE)			                                                    ");
            queryString.AppendLine("order by        to_number(a.IDX)		                                                                        ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_REALTIME_ANALYSIS_ERROR");
        }

        //경고리스트 조회
        public DataSet SelectAnalysisWarningList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          a.INP_NUMBER			                                                                                                                                                                        ");
            queryString.AppendLine("                ,a.TITLE			                                                                                                                                                                            ");
            queryString.AppendLine("                ,to_char(to_date(a.TARGET_DATE, 'yyyymmddhh24mi'), 'yyyy-mm-dd hh24:mi')    as TARGET_DATE			                                                                                            ");
            queryString.AppendLine("                ,d.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("                ,a.TARGET_DATE                                                              as TARGET_DATE_ORI                                                                                                  ");
            queryString.AppendLine("                ,d.LOC_CODE                                                                                                                                                                                     ");
            queryString.AppendLine("from            WH_RPT_MASTER       a			                                                                                                                                                                ");
            queryString.AppendLine("                ,WH_RPT_NODES       b			                                                                                                                                                                ");
            queryString.AppendLine("                ,WH_TAGS            c			                                                                                                                                                                ");
            queryString.AppendLine("                ,CM_LOCATION        d			                                                                                                                                                                ");
            queryString.AppendLine("where           a.INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'			                                                                                                                            ");
            queryString.AppendLine("and             a.AUTO_MANUAL       = 'A'			                                                                                                                                                            ");
            queryString.AppendLine("and             to_date(a.TARGET_DATE, 'yyyymmddhh24mi') between to_date('" + conditions["startWarDate"] + "0000', 'yyyymmddhh24mi') and to_date('" + conditions["endWarDate"] + "2359', 'yyyymmddhh24mi')		");
            queryString.AppendLine("and             b.PRESSURE          <= " + conditions["PRESSURE"] + "			                                                                                                                                ");
            queryString.AppendLine("and             c.TYPE              = 'NODE'			                                                                                                                                                        ");

            if (conditions["LOC_CODE"] != null)
            {
                queryString.AppendLine("and         d.LOC_CODE          = '" + conditions["LOC_CODE"] + "'                                                                                                                                          ");
            }
            
            queryString.AppendLine("and             a.INP_NUMBER        = b.INP_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("and             a.RPT_NUMBER        = b.RPT_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("and             a.INP_NUMBER        = c.INP_NUMBER			                                                                                                                                                    ");
            queryString.AppendLine("and             b.NODE_ID           = c.ID			                                                                                                                                                            ");
            queryString.AppendLine("and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 1)+1,instr(c.POSITION_INFO, '|', 1, 2)-1 - instr(c.POSITION_INFO, '|', 1, 1))     = d.FTR_CODE			                                ");
            queryString.AppendLine("and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 2)+1,length(c.POSITION_INFO) - instr(c.POSITION_INFO, '|', 1, 2))                 = d.FTR_IDN    			                                ");
            queryString.AppendLine("group by        a.INP_NUMBER			                                                                                                                                                                        ");
            queryString.AppendLine("                ,a.TITLE			                                                                                                                                                                            ");
            queryString.AppendLine("                ,a.TARGET_DATE			                                                                                                                                                                        ");
            queryString.AppendLine("                ,d.LOC_NAME			                                                                                                                                                                            ");
            queryString.AppendLine("                ,d.LOC_CODE			                                                                                                                                                                            ");
            queryString.AppendLine("order by        to_date(a.TARGET_DATE, 'yyyymmddhh24mi') desc                			                                                                                                                        ");
            queryString.AppendLine("                ,d.LOC_NAME               			                                                                                                                                                            ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_MASTER");
        }

        //경고리스트 상세 조회
        public DataSet SelectAnalysisWarningDetailList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          b.NODE_ID			                                                                                                                                        ");
            queryString.AppendLine("                ,b.PRESSURE			                                                                                                                                        ");
            queryString.AppendLine("                ,b.ELEVATION			                                                                                                                                    ");
            queryString.AppendLine("                ,b.DEMAND			                                                                                                                                        ");
            queryString.AppendLine("                ,b.HEAD			                                                                                                                                            ");
            queryString.AppendLine("from            WH_RPT_MASTER       a			                                                                                                                            ");
            queryString.AppendLine("                ,WH_RPT_NODES       b			                                                                                                                            ");
            queryString.AppendLine("                ,WH_TAGS            c			                                                                                                                            ");
            queryString.AppendLine("                ,CM_LOCATION        d			                                                                                                                            ");
            queryString.AppendLine("where           a.INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'			                                                                                        ");
            queryString.AppendLine("and             a.AUTO_MANUAL       = 'A'			                                                                                                                        ");
            queryString.AppendLine("and             a.TARGET_DATE       = '" + conditions["TARGET_DATE"] + "'			                                                                                        ");
            queryString.AppendLine("and             c.TYPE              = 'NODE'			                                                                                                                    ");
            queryString.AppendLine("and             b.PRESSURE          <= " + conditions["PRESSURE"] + "			                                                                                            ");

            if (conditions["LOC_CODE"] != null)
            {
                queryString.AppendLine("and         d.LOC_CODE          = '" + conditions["LOC_CODE"] + "'			                                                                                            ");
            }

            queryString.AppendLine("and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 1)+1,instr(c.POSITION_INFO, '|', 1, 2)-1 - instr(c.POSITION_INFO, '|', 1, 1))     = d.FTR_CODE		");
            queryString.AppendLine("and             substr(c.POSITION_INFO,instr(c.POSITION_INFO, '|', 1, 2)+1,length(c.POSITION_INFO) - instr(c.POSITION_INFO, '|', 1, 2))                 = d.FTR_IDN         ");
            queryString.AppendLine("and             a.INP_NUMBER        = b.INP_NUMBER			                                                                                                                ");
            queryString.AppendLine("and             a.RPT_NUMBER        = b.RPT_NUMBER			                                                                                                                ");
            queryString.AppendLine("and             a.INP_NUMBER        = c.INP_NUMBER			                                                                                                                ");
            queryString.AppendLine("and             b.NODE_ID           = c.ID			                                                                                                                        ");
            queryString.AppendLine("order by        to_number(b.PRESSURE)                                                                                                                                       ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_MASTER");
        }

        //평균수압지점 리스트 조회
        public DataSet SelectAveragePressurePointList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      c.LOC_NAME			                                                                                                                                        ");
            queryString.AppendLine("            ,a.ID			                                                                                                                                            ");
            queryString.AppendLine("from        WH_AVG_PRES_NODE        a			                                                                                                                        ");
            queryString.AppendLine("            ,WH_TAGS                b			                                                                                                                        ");
            queryString.AppendLine("            ,CM_LOCATION            c			                                                                                                                        ");
            queryString.AppendLine("where       a.INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'			                                                                                        ");
            queryString.AppendLine("and         b.TYPE              = 'NODE'			                                                                                                                    ");
            queryString.AppendLine("and         a.INP_NUMBER        = b.INP_NUMBER			                                                                                                                ");
            queryString.AppendLine("and         a.ID                = b.ID			                                                                                                                        ");
            queryString.AppendLine("and         substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 1)+1,instr(b.POSITION_INFO, '|', 1, 2)-1 - instr(b.POSITION_INFO, '|', 1, 1))     = c.FTR_CODE		");
            queryString.AppendLine("and         substr(b.POSITION_INFO,instr(b.POSITION_INFO, '|', 1, 2)+1,length(b.POSITION_INFO) - instr(b.POSITION_INFO, '|', 1, 2))                 = c.FTR_IDN			");
            queryString.AppendLine("order by    LOC_NAME                                                                                                                                                    ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_AVG_PRES_NODE");
        }

        //평균수압지점 입력
        public void InsertAveragePressurePointList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_AVG_PRES_NODE    (                       ");
            queryString.AppendLine("                                    LOC_CODE            ");
            queryString.AppendLine("                                    ,INP_NUMBER         ");
            queryString.AppendLine("                                    ,ID                 ");
            queryString.AppendLine("                                ) values (              ");
            queryString.AppendLine("                                    :1                  ");
            queryString.AppendLine("                                    ,:2                 ");
            queryString.AppendLine("                                    ,:3                 ");
            queryString.AppendLine("                                )                       ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["LOC_CODE"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];
            parameters[2].Value = (string)conditions["ID"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //평균수압지점 삭제
        public void DeleteAveragePressurePointData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_AVG_PRES_NODE        ");
            queryString.AppendLine("where       INP_NUMBER          = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and         ID                  = '" + conditions["ID"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }
    }
}
