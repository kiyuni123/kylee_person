﻿/**************************************************************************
 * 파일명   : WHINPManageDao.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.09.29
 * 설명     : INP파일 내용을 DB에 저장하기 위한 Data Access Object
 * 변경이력 : 2010.09.29 - 최초생성
 **************************************************************************/
using System;
using System.Data;
using System.Data.OleDb;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using System.Runtime.InteropServices;

using WaterNet.WH_INPManage.dao;
using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;
using WaterNet.WH_RTAnalysis.dao;
using WaterNet.WH_RTAnalysis.form;
using WaterNet.WH_Common.utils;
using WaterNet.WH_Common.dao;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

namespace WaterNet.WH_RTAnalysis.work
{
    public class RealtimeAnalysisResultWork
    {
        private static RealtimeAnalysisResultWork work = null;
        private INPManageDao dao = null;
        private RealtimeAnalysisResultDao rDao = null;
        private CommonDao cDao = null;

        private RealtimeAnalysisResultWork()
        {
            dao = INPManageDao.GetInstance();
            rDao = RealtimeAnalysisResultDao.GetInstance();
            cDao = CommonDao.GetInstance();
        }

        public static RealtimeAnalysisResultWork GetInstance()
        {
            if (work == null)
            {
                work = new RealtimeAnalysisResultWork();
            }

            return work;
        }

        //해당 INP번호의 모델정보를 조회
        public DataSet SelectModelData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.SelectModelData(dbManager, conditions);

                //Shape파일 존재여부 확인 후 없으면 생성시킨다.
                if (result.Tables["WH_TITLE"].Rows.Count != 0)
                {
                    string inpNumber = result.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString();

                    IWorkspace pWorkspace = ArcManager.getShapeWorkspace(VariableManager.m_INPgraphicRootDirectory + "\\" + inpNumber);

                    if (pWorkspace == null)
                    {
                        CreateINPLayerManager manager = new CreateINPLayerManager(dbManager, conditions["INP_NUMBER"].ToString());
                        manager.CreateINP_Shape();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //등록된 모델 중 최초 1개 조회(실시간 관망해석 화면 로드 시 최초로 띄움)
        public DataSet SelectFirstModel(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.SelectFirstModel(dbManager, conditions);

                //Shape파일 존재여부 확인 후 없으면 생성시킨다.
                if (result.Tables["WH_TITLE"].Rows.Count != 0)
                {
                    string inpNumber = result.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString();

                    IWorkspace pWorkspace = ArcManager.getShapeWorkspace(VariableManager.m_INPgraphicRootDirectory + "\\" + inpNumber);

                    //if (pWorkspace != null)
                    //{
                    //    FunctionManager.RemoveDirectory(VariableManager.m_INPgraphicRootDirectory + "\\" + result.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"], true);
                    //}

                    if (pWorkspace == null)
                    {
                        CreateINPLayerManager manager = new CreateINPLayerManager(dbManager, result.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString());
                        manager.CreateINP_Shape();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //블록트리를 선택했을 경우 해당 모델정보를 조회
        public DataSet SelectModelByBlock(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                if ("BZ002".Equals(conditions["FTR_CODE"].ToString()))
                {
                    //중블록코드로 접근 시
                    result = rDao.SelectModelByMiddleBlock(dbManager, conditions);
                }
                else if ("BZ003".Equals(conditions["FTR_CODE"].ToString()))
                {
                    //소블록코드로 접근 시
                    result = rDao.SelectModelBySmallBlock(dbManager, conditions);
                }

                //Shape파일 존재여부 확인 후 없으면 생성시킨다.
                if (result.Tables["WH_TITLE"].Rows.Count != 0)
                {
                    string inpNumber = result.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString();

                    IWorkspace pWorkspace = ArcManager.getShapeWorkspace(VariableManager.m_INPgraphicRootDirectory + "\\" + inpNumber);

                    //if (pWorkspace != null)
                    //{
                    //    FunctionManager.RemoveDirectory(VariableManager.m_INPgraphicRootDirectory + "\\" + result.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"], true);
                    //}

                    if (pWorkspace == null)
                    {
                        CreateINPLayerManager manager = new CreateINPLayerManager(dbManager, result.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString());
                        manager.CreateINP_Shape();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        ////지도상의 INP Layer의 실시간 감시항목 데이터를 전부 초기화한다.
        //public void ClearLayerDatas(string inpNumber, Hashtable layerData)
        //{
        //    OracleDBManager dbManager = null;

        //    try
        //    {
        //        dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
        //        dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

        //        //DB작업
        //        dbManager.Open();

        //        Hashtable conditions = new Hashtable();
        //        conditions.Add("INP_NUMBER", inpNumber);

        //        DataSet junctionList = dao.SelectJunctionList(dbManager, conditions);
        //        DataSet reservoirList = dao.SelectReservoirList(dbManager, conditions);
        //        DataSet tankList = dao.SelectTankList(dbManager, conditions);
        //        DataSet pipeList = dao.SelectPipeList(dbManager, conditions);
        //        DataSet pumpList = dao.SelectPumpList(dbManager, conditions);
        //        DataSet valveList = dao.SelectValveList(dbManager, conditions);

        //        ITable junctionTable = (ITable)layerData["junction"];
        //        ITable tankTable = (ITable)layerData["tank"];
        //        ITable reservoirTable = (ITable)layerData["reservoir"];
        //        ITable pipeTable = (ITable)layerData["pipe"];
        //        ITable pumpTable = (ITable)layerData["pump"];
        //        ITable valveTable = (ITable)layerData["valve"];

        //        UpdateNodeData2(junctionTable, null, junctionList.Tables["WH_JUNCTIONS"], true);
        //        UpdateNodeData2(tankTable, null, tankList.Tables["WH_TANKS"], true);
        //        UpdateNodeData2(reservoirTable, null, reservoirList.Tables["WH_RESERVOIRS"], true);

        //        UpdateLinkData2(pipeTable, null, pipeList.Tables["WH_PIPES"], true);
        //        UpdateLinkData2(pumpTable, null, pumpList.Tables["WH_PUMPS"], true);
        //        UpdateLinkData2(valveTable, null, valveList.Tables["WH_VALVES"], true);
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //        throw e;
        //    }
        //    finally
        //    {
        //        dbManager.Close();
        //    }
        //}

        public string SetLayerDatas(string inpNumber)
        {
            Console.WriteLine("해석결과 적용 시작 " + DateTime.Now);
            OracleDBManager dbManager = null;

            string result = "";

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                Hashtable lastAnalysisTimeConditions = new Hashtable();
                lastAnalysisTimeConditions.Add("INP_NUMBER", inpNumber);

                //최종 해석시간 조회
                DataSet lastAnalysisTimeDataSet = rDao.SelectLastAnalysisTime(dbManager, lastAnalysisTimeConditions);

                //lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["TARGET_DATE"] = "201204110420";
                //lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["FORMED_DATE"] = Convert.ToDateTime("2012-04-11 04:30:01");

                if (lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows.Count != 0)
                {
                    Hashtable conditions = new Hashtable();
                    conditions.Add("INP_NUMBER", inpNumber);
                    conditions.Add("TARGET_DATE", lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["TARGET_DATE"].ToString());
                    result = lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["LOC_NAME"].ToString() + " 최종해석시간 : " + lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["FORMED_DATE"].ToString();

                    DataSet nodeDataSet = rDao.GetAnalysisNodeResultData(dbManager, conditions);
                    DataSet linkDataSet = rDao.GetAnalysisLinkResultData(dbManager, conditions);

                    if (nodeDataSet.Tables["WH_RPT_NODES"].Rows.Count == 0 || linkDataSet.Tables["WH_RPT_LINKS"].Rows.Count == 0) return string.Empty;

                    WH_Common.WH_VariableManager.analysis_Data.Tables.Clear();
                    WH_Common.WH_VariableManager.analysis_Data.Tables.Add(nodeDataSet.Tables["WH_RPT_NODES"].Copy());
                    WH_Common.WH_VariableManager.analysis_Data.Tables.Add(linkDataSet.Tables["WH_RPT_LINKS"].Copy());
                    
                    Console.WriteLine("해석결과 적용 종료... " + DateTime.Now);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        ////해당시간의 관망해석 결과를 지도상에 표출한다.
        //public string SetLayerDatas(string inpNumber, Hashtable layerData)
        //{
        //    Console.WriteLine("해석결과 적용 시작 2 : " + DateTime.Now);
        //    OracleDBManager dbManager = null;

        //    string result = "";

        //    try
        //    {
        //        dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
        //        dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

        //        //DB작업
        //        dbManager.Open();

        //        Hashtable lastAnalysisTimeConditions = new Hashtable();
        //        lastAnalysisTimeConditions.Add("INP_NUMBER", inpNumber);

        //        //최종 해석시간 조회
        //        DataSet lastAnalysisTimeDataSet = rDao.SelectLastAnalysisTime(dbManager, lastAnalysisTimeConditions);

        //        //lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["TARGET_DATE"] = "201204110420";
        //        //lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["FORMED_DATE"] = Convert.ToDateTime("2012-04-11 04:30:01");

        //        if (lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows.Count != 0)
        //        {
        //            Hashtable conditions = new Hashtable();
        //            conditions.Add("INP_NUMBER", inpNumber);
        //            conditions.Add("TARGET_DATE", lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["TARGET_DATE"].ToString());
        //            result = lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["LOC_NAME"].ToString() + " 최종해석시간 : " + lastAnalysisTimeDataSet.Tables["WH_RPT_MASTER"].Rows[0]["FORMED_DATE"].ToString();
        //            //Console.WriteLine("DB ..." + DateTime.Now);
        //            DataSet nodeDataSet = rDao.GetAnalysisNodeResultData(dbManager, conditions);
        //            DataSet linkDataSet = rDao.GetAnalysisLinkResultData(dbManager, conditions);
        //            //Console.WriteLine("DB ..." + DateTime.Now);
        //            if (nodeDataSet.Tables["WH_RPT_NODES"].Rows.Count == 0 || linkDataSet.Tables["WH_RPT_LINKS"].Rows.Count == 0) return string.Empty;

        //            ITable junctionTable = (ITable)layerData["junction"];
        //            ITable tankTable = (ITable)layerData["tank"];
        //            ITable reservoirTable = (ITable)layerData["reservoir"];
        //            ITable pipeTable = (ITable)layerData["pipe"];
        //            ITable pumpTable = (ITable)layerData["pump"];
        //            ITable valveTable = (ITable)layerData["valve"];

        //            //Console.WriteLine("node ..." + DateTime.Now);
        //            EAGL.Data.Wrapper.DataTableWrapper tW1 = new EAGL.Data.Wrapper.DataTableWrapper();
        //            ITable nodetable = tW1.CreateITable(nodeDataSet.Tables["WH_RPT_NODES"]);
        //            UpdateNodeData(junctionTable, null, nodetable, false);
        //            UpdateNodeData(tankTable, null, nodetable, false);
        //            UpdateNodeData(reservoirTable, null, nodetable, false);

        //            //Console.WriteLine("link ..." + DateTime.Now);
        //            EAGL.Data.Wrapper.DataTableWrapper tW2 = new EAGL.Data.Wrapper.DataTableWrapper();
        //            ITable linktable = tW2.CreateITable(linkDataSet.Tables["WH_RPT_LINKS"]);
        //            UpdateLinkData(pipeTable, null, linktable, false);
        //            UpdateLinkData(pumpTable, null, linktable, false);
        //            UpdateLinkData(valveTable, null, linktable, false);

        //            Console.WriteLine("해석결과 적용 종료... 2 : " + DateTime.Now);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //        throw e;
        //    }
        //    finally
        //    {
        //        dbManager.Close();
        //    }

        //    return result;
        //}

        //INP 파일 리스트 조회
        public DataSet SelectINPList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                result = dao.SelectINPMasterList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.StackTrace);
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Node 지역코드 리스트 조회
        public DataSet GetNodeLocationDataList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.GetNodeLocationDataList(dbManager, conditions);

                //최상위에 Blank 추가
                if (result != null)
                {
                    DataRow row = result.Tables["nodeLocation"].NewRow();
                    row["LOC_CODE"] = "";
                    row["LOC_NAME"] = "전체";

                    result.Tables["nodeLocation"].Rows.InsertAt(row, 0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Link 지역코드 리스트 조회
        public DataSet GetLinkLocationDataList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.GetLinkLocationDataList(dbManager, conditions);

                //최상위에 Blank 추가
                if (result != null)
                {
                    DataRow row = result.Tables["linkLocation"].NewRow();
                    row["LOC_CODE"] = "";
                    row["LOC_NAME"] = "전체";

                    result.Tables["linkLocation"].Rows.InsertAt(row, 0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //해석결과 항목별 지역코드(통합)
        public DataSet SelectLocationData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.SelectLocationData(dbManager, conditions);

                //최상위에 Blank 추가
                if (result != null)
                {
                    DataRow row = result.Tables["CM_LOCATION"].NewRow();
                    row["LOC_CODE"] = "";
                    row["LOC_NAME"] = "전체";

                    result.Tables["CM_LOCATION"].Rows.InsertAt(row, 0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Node 해석결과 리스트 조회
        public DataSet GetAnalysisNodeResultList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.GetAnalysisNodeResultList(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Node 해석결과 리스트 조회
        public DataSet GetAnalysisNodeResultList(Hashtable conditions, List<string> nodelist)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                string lists = string.Empty;
                foreach (string s in nodelist)
                    lists += "'" + s + "',";
                lists = lists.Remove(lists.Length - 1, 1);

                result = rDao.GetAnalysisNodeResultList(dbManager, conditions, lists);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Link 해석결과 리스트 조회
        public DataSet GetAnalysisLinkResultList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.GetAnalysisLinkResultList(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Link 해석결과 리스트 조회
        public DataSet GetAnalysisLinkResultList(Hashtable conditions, List<string> linklist)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                string lists = string.Empty;
                foreach (string s in linklist)
                    lists += "'" + s + "',";
                lists = lists.Remove(lists.Length - 1, 1);

                result = rDao.GetAnalysisLinkResultList(dbManager, conditions, lists);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //결과 팝업화면의 Node별 해석결과 조회(그래프)
        public DataSet SelectNodeAnalysisDataForGraph(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.SelectNodeAnalysisDataForGraph(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;

        }

        //결과 팝업화면의 Link별 해석결과 조회(그래프)
        public DataSet SelectLinkAnalysisDataForGraph(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.SelectLinkAnalysisDataForGraph(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //해당 모델의 실측지점 조회
        public DataSet GetRealtimeCheckpointList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.GetRealtimeCheckpointList(dbManager, conditions);

                //최상위에 Blank 추가
                if (result != null)
                {
                    DataRow row = result.Tables["WH_MEASURE_CHECKPOINT"].NewRow();
                    row["TAGNAME"] = "";
                    row["DESCRIPTION"] = "선택";

                    result.Tables["WH_MEASURE_CHECKPOINT"].Rows.InsertAt(row, 0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실측지점 비교 리스트 조회
        public DataSet GetAnalysisResultVsRealtimeCheck(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                if ("000001".Equals((string)conditions["METER_TYPE"]))
                {
                    //유량계 (Link)
                    conditions["layerIDField"] = "LINK_ID";
                    conditions["analysisItem"] = "FLOW";
                    conditions["layerTable"] = "WH_RPT_LINKS";
                }
                else if ("000002".Equals((string)conditions["METER_TYPE"]))
                {
                    //압력계 (Node)
                    conditions["layerIDField"] = "NODE_ID";
                    conditions["analysisItem"] = "PRESSURE";
                    conditions["layerTable"] = "WH_RPT_NODES";
                }
                else if ("000003".Equals((string)conditions["METER_TYPE"]))
                {
                    //수질계 (Quality)
                    conditions["layerIDField"] = "NODE_ID";
                    conditions["analysisItem"] = "QUALITY";
                    conditions["layerTable"] = "WH_RPT_NODES";
                }

                result = rDao.GetAnalysisResultVsRealtimeCheck(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
            return result;
        }

        //해석항목 Rage 조회
        public DataSet SelectAnalysisItemRangeData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.SelectAnalysisItemRangeData(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //수리모델 상 절점에 해석결과를 표출
        private void UpdateNodeData(ITable oTable, IQueryFilter oQueryFilter, ITable nodetable, bool Initialize)
        {

            //EAGL.Data.Wrapper.DataTableWrapper tW = new EAGL.Data.Wrapper.DataTableWrapper();
            //ITable nodetable = tW.CreateITable(oDataTable);

            EAGL.Join.AttributeJoin.GetInstance().RemoveAllJoins(oTable);
            EAGL.Join.AttributeJoin.GetInstance().getJoinedLayer(oTable, nodetable, "NODE_ID", "ID", esriRelCardinality.esriRelCardinalityOneToOne);
            //Console.WriteLine(((IDisplayTable)oTable).DisplayTable.Fields.FieldCount.ToString());
        }

        private void UpdateLinkData(ITable oTable, IQueryFilter oQueryFilter, ITable linktable, bool Initialize)
        {
            //EAGL.Data.Wrapper.DataTableWrapper tW = new EAGL.Data.Wrapper.DataTableWrapper();
            //ITable linktable = tW.CreateITable(oDataTable);

            EAGL.Join.AttributeJoin.GetInstance().RemoveAllJoins(oTable);
            EAGL.Join.AttributeJoin.GetInstance().getJoinedLayer(oTable, linktable, "LINK_ID", "ID", esriRelCardinality.esriRelCardinalityOneToOne);
            //Console.WriteLine(((IDisplayTable)oTable).DisplayTable.Fields.FieldCount.ToString());
        }

        ////수리모델 상 절점에 해석결과를 표출
        //private void UpdateNodeData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        //{
        //    ICursor pCusror = oTable.Update(oQueryFilter, true);
        //    IRow oRow = null;
        //    string idField = "";

        //    if (Initialize)
        //    {
        //        idField = "ID";
        //    }
        //    else
        //    {
        //        idField = "NODE_ID";
        //    }

        //    try
        //    {
        //        while ((oRow = pCusror.NextRow()) != null)
        //        {
        //            DataRow nRow = getDataRow(Convert.ToString(oRow.get_Value(oRow.Fields.FindField("ID"))), oDataTable, idField);
        //            if (nRow != null)
        //            {
        //                if (Initialize)
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ELEVATI"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_BASEDEM"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PATTERN"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_EMITTER"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITQUA"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEQ"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEP"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCET"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKLEV"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DEMAND"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEAD"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PRESSUR"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_QUALITY"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEM"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITVOL"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXMODE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXZONE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKDIA"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINVOLU"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VOLCURV"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINLEVE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MAXLEVE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXFRAC"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANK_KB"), "0");
        //                }
        //                else
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ELEVATI"), nRow["ELEVATION"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_BASEDEM"), nRow["BASEDEMAND"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PATTERN"), nRow["PATTERN"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_EMITTER"), nRow["EMITTER"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITQUA"), nRow["INITQUAL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEQ"), nRow["SOURCEQUAL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEP"), nRow["SOURCEPAT"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCET"), nRow["SOURCETYPE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKLEV"), nRow["TANKLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DEMAND"), nRow["DEMAND"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEAD"), nRow["HEAD"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_PRESSUR"), nRow["PRESSURE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_QUALITY"), nRow["QUALITY"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SOURCEM"), nRow["SOURCEMASS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITVOL"), nRow["INITVOLUME"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXMODE"), nRow["MIXMODEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXZONE"), nRow["MIXZONEVOL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANKDIA"), nRow["TANKDIAM"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINVOLU"), nRow["MINVOLUME"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VOLCURV"), nRow["VOLCURVE"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINLEVE"), nRow["MINLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MAXLEVE"), nRow["MAXLEVEL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MIXFRAC"), nRow["MIXFRACTION"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_TANK_KB"), nRow["TANK_KBULK"]);
        //                }

        //                pCusror.UpdateRow(oRow);
        //            }
        //        }
        //        pCusror.Flush();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //        throw e;
        //    }
        //    finally
        //    {
        //        Marshal.ReleaseComObject(pCusror);
        //    }
        //}

        //수리모델 상 링크에 해석결과를 표출
        //private void UpdateLinkData(ITable oTable, IQueryFilter oQueryFilter, DataTable oDataTable, bool Initialize)
        //{
        //    ICursor pCusror = oTable.Update(oQueryFilter, true);
        //    IRow oRow = null;
        //    string idField = "";

        //    if (Initialize)
        //    {
        //        idField = "ID";
        //    }
        //    else
        //    {
        //        idField = "LINK_ID";
        //    }

        //    try
        //    {
        //        while ((oRow = pCusror.NextRow()) != null)
        //        {
        //            DataRow nRow = getDataRow(Convert.ToString(oRow.get_Value(oRow.Fields.FindField("ID"))), oDataTable, idField);
        //            if (nRow != null)
        //            {
        //                if (Initialize)
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DIAMETE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_LENGTH"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ROUGHNE"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINORLO"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSTA"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSET"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KBULK"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KWALL"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_FLOW"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VELOCIT"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEADLOS"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_STATUS"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SETTING"), "0");
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ENERGY"), "0");
        //                }
        //                else
        //                {
        //                    oRow.set_Value(oRow.Fields.FindField("EN_DIAMETE"), nRow["DIAMETER"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_LENGTH"), nRow["LENGTH"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ROUGHNE"), nRow["ROUGHNESS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_MINORLO"), nRow["MINORLOSS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSTA"), nRow["INITSTATUS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_INITSET"), nRow["INITSETTING"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KBULK"), nRow["KBULK"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_KWALL"), nRow["KWALL"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_FLOW"), nRow["FLOW"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_VELOCIT"), nRow["VELOCITY"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_HEADLOS"), nRow["HEADLOSS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_STATUS"), nRow["STATUS"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_SETTING"), nRow["SETTING"]);
        //                    oRow.set_Value(oRow.Fields.FindField("EN_ENERGY"), nRow["ENERGY"]);
        //                }

        //                pCusror.UpdateRow(oRow);
        //            }
        //        }
        //        pCusror.Flush();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //        throw e;
        //    }
        //    finally
        //    {
        //        Marshal.ReleaseComObject(pCusror);
        //    }
        //}

        private DataRow getDataRow(string ID, DataTable oDataTable, string dataID)
        {
            DataRow oRow = null;
            DataRow[] oRows = oDataTable.Select(dataID + " = '" + ID + "'");
            if (oRows.Length == 1)
            {
                oRow = oRows[0];
            }
            return oRow;
        }

        //실측지점 리스트 조회
        public DataSet SelectRealtimeCheckpointList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = rDao.SelectRealtimeCheckpointList(dbManager, conditions);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실측지점 저장
        public DataSet SaveRealtimeCheckpoint(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //실시간 계측지점 증록여부 확인
                        Hashtable existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("METER_TYPE", row["METER_TYPE"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = rDao.IsExistRealtimeCheckpoint(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_MEASURE_CHECKPOINT"]).Rows.Count != 0)
                        {
                            throw new Exception("같은지점에 등록된 계측기가 있습니다. (ID : " + row["ID"] + ")");
                        }

                        //추가인경우
                        Hashtable insertConditions = new Hashtable();

                        insertConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        insertConditions.Add("ID", row["ID"].ToString());
                        insertConditions.Add("METER_TYPE", row["METER_TYPE"].ToString());
                        insertConditions.Add("FTR_IDN", row["FTR_IDN"].ToString());
                        insertConditions.Add("TAGNAME", row["TAGNAME"].ToString());

                        rDao.InsertRealtimeCheckpoint(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        Hashtable updateConditions = new Hashtable();

                        updateConditions.Add("FTR_IDN", row["FTR_IDN"].ToString());
                        updateConditions.Add("TAGNAME", row["TAGNAME"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("METER_TYPE", row["METER_TYPE"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        rDao.UpdateRealtimeCheckpoint(dbManager, updateConditions);
                    }
                }

                result = rDao.SelectRealtimeCheckpointList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실측지점 삭제
        public DataSet DeleteRealtimeCheckpoint(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                rDao.DeleteRealtimeCheckpoint(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = rDao.SelectRealtimeCheckpointList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //해석결과표출 범위 설정 리스트 조회
        public DataSet SelectAnalysisResultRangeList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectAnalysisResultRangeList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //해석결과표출 범위 저장
        public DataSet SaveAnalysisResultRange(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in changedTable.Rows)
                {
                    //수정인경우
                    Hashtable updateConditions = new Hashtable();

                    updateConditions.Add("RANGE1", row["RANGE1"].ToString());
                    updateConditions.Add("RANGE2", row["RANGE2"].ToString());
                    updateConditions.Add("RANGE3", row["RANGE3"].ToString());
                    updateConditions.Add("RANGE4", row["RANGE4"].ToString());
                    updateConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                    updateConditions.Add("RESULT_TYPE", row["RESULT_TYPE"].ToString());
                    updateConditions.Add("RESULT_CODE", row["RESULT_CODE"].ToString());

                    rDao.UpdateAnalysisResultRange(dbManager, updateConditions);
                }

                //Update 후 리스트 재조회
                result = rDao.SelectAnalysisResultRangeList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //대수용가 리스트 조회
        public DataSet SelectLargeConsumerList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectLargeConsumerList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //대수용가 동작정보 삭제
        public DataSet DeleteActivateLargeConsumer(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                rDao.DeleteLargeConsumer(dbManager, conditions);

                //대수용가 타임테이블 삭제
                rDao.DeleteActivateLargeConsumer(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = rDao.SelectActivateLargeConsumerList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //대수용가 저장
        public DataSet SaveLargeConsumerList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //대수용가 중복여부 확인
                        Hashtable existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = rDao.IsExistLargeConsumer(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_LCONSUMER_INFO"]).Rows.Count != 0)
                        {
                            throw new Exception("같은지점에 등록된 대수용가가 있습니다. (ID : " + row["ID"] + ")");
                        }

                        //추가인경우
                        Hashtable insertConditions = new Hashtable();

                        insertConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        insertConditions.Add("ID", row["ID"].ToString());
                        insertConditions.Add("LCONSUMER_NM", row["LCONSUMER_NM"].ToString());
                        insertConditions.Add("APPLY_CONTINUE_TIME", row["APPLY_CONTINUE_TIME"].ToString());
                        insertConditions.Add("APPLY_INCREASE", row["APPLY_INCREASE"].ToString());
                        insertConditions.Add("APPLY_YN", row["APPLY_YN"].ToString());
                        insertConditions.Add("TAGNAME", row["TAGNAME"].ToString());

                        rDao.InsertLargeConsumer(dbManager, insertConditions);

                        //대수용가 타임테이블 입력
                        for (int i = 1; i < 8; i++)
                        {
                            Hashtable timeTableConditions = new Hashtable();

                            timeTableConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                            timeTableConditions.Add("ID", row["ID"].ToString());
                            timeTableConditions.Add("WEEKDAY", i.ToString());
                            timeTableConditions.Add("H01", "N");
                            timeTableConditions.Add("H02", "N");
                            timeTableConditions.Add("H03", "N");
                            timeTableConditions.Add("H04", "N");
                            timeTableConditions.Add("H05", "N");
                            timeTableConditions.Add("H06", "N");
                            timeTableConditions.Add("H07", "N");
                            timeTableConditions.Add("H08", "N");
                            timeTableConditions.Add("H09", "N");
                            timeTableConditions.Add("H10", "N");
                            timeTableConditions.Add("H11", "N");
                            timeTableConditions.Add("H12", "N");
                            timeTableConditions.Add("H13", "N");
                            timeTableConditions.Add("H14", "N");
                            timeTableConditions.Add("H15", "N");
                            timeTableConditions.Add("H16", "N");
                            timeTableConditions.Add("H17", "N");
                            timeTableConditions.Add("H18", "N");
                            timeTableConditions.Add("H19", "N");
                            timeTableConditions.Add("H20", "N");
                            timeTableConditions.Add("H21", "N");
                            timeTableConditions.Add("H22", "N");
                            timeTableConditions.Add("H23", "N");
                            timeTableConditions.Add("H24", "N");

                            rDao.InsertLargeConsumerTimeTable(dbManager, timeTableConditions);
                        }
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        Hashtable updateConditions = new Hashtable();

                        updateConditions.Add("LCONSUMER_NM", row["LCONSUMER_NM"].ToString());
                        updateConditions.Add("APPLY_INCREASE", row["APPLY_INCREASE"].ToString());
                        updateConditions.Add("APPLY_CONTINUE_TIME", row["APPLY_CONTINUE_TIME"].ToString());
                        updateConditions.Add("APPLY_YN", row["APPLY_YN"].ToString());
                        updateConditions.Add("TAGNAME", row["TAGNAME"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        rDao.UpdateLargeConsumer(dbManager, updateConditions);
                    }
                }

                result = rDao.SelectLargeConsumerList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //대수용가 삭제
        public DataSet DeleteLargeConsumer(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                rDao.DeleteLargeConsumer(dbManager, conditions);

                //대수용가 타임테이블 삭제
                rDao.DeleteLargeConsumerTimeTable(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = rDao.SelectLargeConsumerList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //대수용가 타임테이블 조회
        public DataSet SelectLargeConsumerTimeTable(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectLargeConsumerTimeTable(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //대수용가 타임테이블 저장
        public DataSet SaveLargeConsumerTimeTable(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in changedTable.Rows)
                {
                    //수정인경우
                    Hashtable updateConditions = new Hashtable();

                    updateConditions.Add("H01", row["H01"].ToString().ToUpper());
                    updateConditions.Add("H02", row["H02"].ToString().ToUpper());
                    updateConditions.Add("H03", row["H03"].ToString().ToUpper());
                    updateConditions.Add("H04", row["H04"].ToString().ToUpper());
                    updateConditions.Add("H05", row["H05"].ToString().ToUpper());
                    updateConditions.Add("H06", row["H06"].ToString().ToUpper());
                    updateConditions.Add("H07", row["H07"].ToString().ToUpper());
                    updateConditions.Add("H08", row["H08"].ToString().ToUpper());
                    updateConditions.Add("H09", row["H09"].ToString().ToUpper());
                    updateConditions.Add("H10", row["H10"].ToString().ToUpper());
                    updateConditions.Add("H11", row["H11"].ToString().ToUpper());
                    updateConditions.Add("H12", row["H12"].ToString().ToUpper());
                    updateConditions.Add("H13", row["H13"].ToString().ToUpper());
                    updateConditions.Add("H14", row["H14"].ToString().ToUpper());
                    updateConditions.Add("H15", row["H15"].ToString().ToUpper());
                    updateConditions.Add("H16", row["H16"].ToString().ToUpper());
                    updateConditions.Add("H17", row["H17"].ToString().ToUpper());
                    updateConditions.Add("H18", row["H18"].ToString().ToUpper());
                    updateConditions.Add("H19", row["H19"].ToString().ToUpper());
                    updateConditions.Add("H20", row["H20"].ToString().ToUpper());
                    updateConditions.Add("H21", row["H21"].ToString().ToUpper());
                    updateConditions.Add("H22", row["H22"].ToString().ToUpper());
                    updateConditions.Add("H23", row["H23"].ToString().ToUpper());
                    updateConditions.Add("H24", row["H24"].ToString().ToUpper());
                    updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                    updateConditions.Add("ID", conditions["ID"].ToString());
                    updateConditions.Add("WEEKDAY", row["WEEKDAY"].ToString());

                    rDao.UpdateLargeConsumerTimeTable(dbManager, updateConditions);
                }

                //Update 후 리스트 재조회
                result = rDao.SelectLargeConsumerTimeTable(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실측데이터 Master Dump List를 반환한다.
        public DataSet SelectMeasuerDumpMaster(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectMeasuerDumpMaster(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실측데이터 Master Dump List를 저장한다.
        public DataSet SaveMeasureDumpMaster(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in changedTable.Rows)
                {
                    //지역정보 조회
                    Hashtable locationConditions = new Hashtable();

                    locationConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                    locationConditions.Add("ID", row["ID"].ToString());

                    if ("Flow".Equals(row["DUMP_TYPE"].ToString()))
                    {
                        //유량인 경우는 LINK
                        locationConditions.Add("TYPE", "LINK");
                    }
                    else
                    {
                        //압력인 경우는 NODE
                        locationConditions.Add("TYPE", "NODE");
                    }

                    DataSet locationDataSet = cDao.GetLocationInfo(dbManager, locationConditions);

                    if (row.RowState == DataRowState.Added)
                    {
                        //추가인경우
                        Hashtable insertConditions = new Hashtable();

                        string dumpNumber = Utils.GetSerialNumber("DP");

                        insertConditions.Add("DUMP_NUMBER", dumpNumber);
                        insertConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        insertConditions.Add("DUMP_TYPE", row["DUMP_TYPE"].ToString());
                        insertConditions.Add("ID", row["ID"].ToString());
                        insertConditions.Add("DUMP_COMMENT", row["DUMP_COMMENT"].ToString());
                        insertConditions.Add("DUMP_FILE_PATH", @row["DUMP_FILE_PATH"].ToString());

                        if (locationDataSet.Tables["WH_TAGS"].Rows.Count != 0)
                        {
                            insertConditions.Add("LOC_CODE", locationDataSet.Tables["WH_TAGS"].Rows[0]["LOC_CODE"]);
                        }
                        else
                        {
                            insertConditions.Add("LOC_CODE", "");
                        }

                        rDao.InsertMeasureDumpMaster(dbManager, insertConditions);

                        //엑셀덤프 업로드
                        Hashtable measureData = ReadMeasureData(dumpNumber, conditions["INP_NUMBER"].ToString(), row["DUMP_FILE_PATH"].ToString(), row["DUMP_TYPE"].ToString());

                        rDao.InsertMeasureDumpData(dbManager, measureData);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        Hashtable updateConditions = new Hashtable();

                        updateConditions.Add("ID", row["ID"].ToString());
                        updateConditions.Add("DUMP_COMMENT", row["DUMP_COMMENT"].ToString());
                        updateConditions.Add("DUMP_NUMBER", row["DUMP_NUMBER"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());

                        if (locationDataSet.Tables["WH_TAGS"].Rows.Count != 0)
                        {
                            updateConditions.Add("LOC_CODE", locationDataSet.Tables["WH_TAGS"].Rows[0]["LOC_CODE"]);
                        }
                        else
                        {
                            updateConditions.Add("LOC_CODE", "");
                        }

                        rDao.UpdateMeasureDumpMaster(dbManager, updateConditions);
                    }
                }

                result = rDao.SelectMeasuerDumpMaster(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실측데이터 Master Dump List를 삭제한다.
        public DataSet DeleteMeasureDumpMaster(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                rDao.DeleteMeasureDumpMaster(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = rDao.SelectMeasuerDumpMaster(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실측데이터(엑셀)파일을 읽어들인다
        private Hashtable ReadMeasureData(string dumpNumber, string inpNumber, string filePath, string type)
        {
            DataSet dSet = LiAsExcelDB.OpenExcelDB(filePath);
            DataTable dTable = dSet.Tables[0];

            List<string> dumpDateList = new List<string>();
            List<string> valueList = new List<string>();
            List<string> dumpNumberList = new List<string>();
            List<string> inpNumberList = new List<string>();

            Hashtable result = new Hashtable();

            if ("Flow".Equals(type))
            {
                //유량데이터인 경우
                for (int i = 0; i < dTable.Rows.Count; i++)
                {
                    DataRow row = dTable.Rows[i];

                    if (!"".Equals(row[0].ToString()) && !"".Equals(row[1].ToString()) && !"".Equals(row[4].ToString()))
                    {
                        string[] date = row[0].ToString().Split('.');
                        string[] time = row[1].ToString().Split('.');

                        string timeString = "";

                        if (time.Length == 2)
                        {
                            timeString = time[0].PadLeft(2, '0') + time[1].PadRight(2, '0');
                        }
                        else
                        {
                            timeString = time[0].PadLeft(2, '0');
                        }

                        dumpNumberList.Add(dumpNumber);
                        inpNumberList.Add(inpNumber);
                        dumpDateList.Add(date[2] + date[0] + date[1] + timeString);
                        valueList.Add(row[4].ToString());

                        //Console.WriteLine(dumpNumber + "," + inpNumber + "," + date[2] + date[0] + date[1] + timeString + "," + row[4]);
                    }
                }
            }
            else
            {
                //압력데이터인 경우
                for (int i = 0; i < dTable.Rows.Count; i++)
                {
                    DataRow row = dTable.Rows[i];

                    if (!"".Equals(row[0].ToString()) && !"".Equals(row[1].ToString()) && !"".Equals(row[2].ToString()))
                    {
                        string[] date = row[0].ToString().Split('-');
                        string[] time = row[1].ToString().Split(':');

                        string dateString = "";
                        string timeString = "";

                        if (date[2].Length > 2)
                        {
                            dateString = date[2].Substring(0, 2);
                        }
                        else
                        {
                            dateString = date[2];
                        }

                        if ("0".Equals(time[2].Substring(3, 1)))
                        {
                            timeString = time[0] + time[1] + time[2].Substring(0, 2);

                            dumpNumberList.Add(dumpNumber);
                            inpNumberList.Add(inpNumber);
                            dumpDateList.Add(date[0] + date[1] + dateString.Trim() + timeString.Trim());
                            valueList.Add((double.Parse(row[2].ToString()) * 10).ToString());               //기본단위 kgf/㎠이기 때문에 10을 곱해준다. (해석값은 m)

                            //Console.WriteLine(dumpNumber + "," + inpNumber + "," + date[0] + date[1] + dateString.Trim() + timeString.Trim() + "," + row[2]);
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }

            result.Add("dumpNumberList", dumpNumberList);
            result.Add("inpNumberList", inpNumberList);
            result.Add("dumpDateList", dumpDateList);
            result.Add("valueList", valueList);

            return result;
        }

        //실측데이터(이동식)와 유량해석결과를 비교한다.
        public DataSet SelectCompareData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                if ("Flow".Equals(conditions["DUMP_TYPE"].ToString()))
                {
                    result = rDao.SelectFlowCompareData(dbManager, conditions);
                }
                else if ("Pressure".Equals(conditions["DUMP_TYPE"].ToString()))
                {
                    result = rDao.SelectPressureCompareData(dbManager, conditions);
                }
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //대수용가 동작정보를 조회
        public DataSet SelectActivateLargeConsumerList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectActivateLargeConsumerList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //등록 태그리스트 조회
        public DataSet SelectTagList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectTagList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실시간 관망해석 설정 조회
        public DataSet SelectAnalysisSettingData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectAnalysisSettingData(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //실시간 관망해석 설정 저장
        public void UpdateAnalysisSettingData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();
                dbManager.BeginTransaction();

                rDao.UpdateAnalysisSettingData(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //해석오류리스트 조회
        public DataSet SelectAnalysisErrorList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectAnalysisErrorList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //해석오류 상세내역
        public DataSet SelectAnalysisErrorDetailList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectAnalysisErrorDetailList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //경고리스트 조회
        public DataSet SelectAnalysisWarningList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectAnalysisWarningList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //경고리스트 상세 조회
        public DataSet SelectAnalysisWarningDetailList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectAnalysisWarningDetailList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //평균수압지점 리스트 조회
        public DataSet SelectAveragePressurePointList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당
                dbManager.Open();

                result = rDao.SelectAveragePressurePointList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //평균수압지점 저장
        public DataSet SaveAveragePressurePointData(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                foreach (DataRow row in changedTable.Rows)
                {
                    //지역정보 조회
                    Hashtable locationConditions = new Hashtable();

                    locationConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                    locationConditions.Add("ID", row["ID"].ToString());
                    locationConditions.Add("TYPE", "NODE");

                    DataSet locationDataSet = cDao.GetLocationInfo(dbManager, locationConditions);

                    if (row.RowState == DataRowState.Added)
                    {
                        //추가인경우
                        Hashtable insertConditions = new Hashtable();

                        insertConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        insertConditions.Add("ID", row["ID"].ToString());

                        if (locationDataSet.Tables["WH_TAGS"].Rows.Count != 0)
                        {
                            insertConditions.Add("LOC_CODE", locationDataSet.Tables["WH_TAGS"].Rows[0]["LOC_CODE"]);
                        }
                        else
                        {
                            insertConditions.Add("LOC_CODE", "");
                        }

                        rDao.InsertAveragePressurePointList(dbManager, insertConditions);
                    }
                }

                result = rDao.SelectAveragePressurePointList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //평균수압지점 삭제
        public DataSet DeleteAveragePressurePointData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                rDao.DeleteAveragePressurePointData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = rDao.SelectAveragePressurePointList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }
    }
}
