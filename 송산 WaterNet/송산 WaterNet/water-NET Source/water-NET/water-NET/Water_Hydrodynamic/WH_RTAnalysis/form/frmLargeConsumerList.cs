﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WH_RTAnalysis.work;
using WaterNet.WH_Common.utils;
using WaterNet.WaterNetCore;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmLargeConsumerList : Form
    {
        public string dummyInpNumber = "";
        private RealtimeAnalysisResultWork work = null;
        public frmWHMain parentForm = null;

        public frmLargeConsumerList()
        {
            work = RealtimeAnalysisResultWork.GetInstance();

            InitializeComponent();
            InitializeSetting();
        }

        private void InitializeSetting()
        {
            UltraGridColumn largeConsumerColumn;

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "LOC_NAME";
            largeConsumerColumn.Header.Caption = "소블록명";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "LCONSUMER_NM";
            largeConsumerColumn.Header.Caption = "대수용가명";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "ID";
            largeConsumerColumn.Header.Caption = "ID";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "INCREASE_AMOUNT";
            largeConsumerColumn.Header.Caption = "수수량";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Right;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "ACTIVATE_TIME";
            largeConsumerColumn.Header.Caption = "적용시작시간";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griLargeConsumerList);
        }

        //폼 최초실행 시
        private void frmLargeConsumerList_Load(object sender, EventArgs e)
        {
            SelectActivateLargeConsumer();

            //=========================================================샘플
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================대수용가 동작정보 

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간관망해석ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.butDeleteLargeConsumer.Enabled = false;
            }

            //===========================================================================
        }

        //대수용가 동작정보 리스트 조회
        public void SelectActivateLargeConsumer()
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("INP_NUMBER", dummyInpNumber);

            DataSet dSet = work.SelectActivateLargeConsumerList(conditions);

            griLargeConsumerList.DataSource = dSet.Tables["WH_LCONSUMER_ACTIVATE"];
            FormManager.SetGridStyle_PerformAutoResize(griLargeConsumerList);
        }

        //대수용가 동작정보 Row를 더블클릭 시
        private void griLargeConsumerList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            parentForm.MoveFocusAt("JUNCTION", "ID = '" + Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("ID")) + "'");
        }

        //대수용가 삭제
        private void butDeleteLargeConsumer_Click(object sender, EventArgs e)
        {
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                int idx = griLargeConsumerList.Selected.Rows[0].Index;

                DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    Hashtable conditions = new Hashtable();

                    conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
                    conditions.Add("ID", griLargeConsumerList.Selected.Rows[0].GetCellValue("ID").ToString());

                    try
                    {
                        DataSet dSet = work.DeleteActivateLargeConsumer(conditions);
                        griLargeConsumerList.DataSource = dSet.Tables["WH_LCONSUMER_ACTIVATE"];

                        MessageBox.Show("정상적으로 처리되었습니다.");
                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show(e1.Message);
                    }
                }
            }
        }
    }
}
