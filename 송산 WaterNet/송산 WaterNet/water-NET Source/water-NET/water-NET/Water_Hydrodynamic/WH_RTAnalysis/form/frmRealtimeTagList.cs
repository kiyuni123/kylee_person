﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WH_RTAnalysis.form;
using WaterNet.WH_RTAnalysis.work;
using WaterNet.WaterNetCore;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmRealtimeTagList : Form
    {
        public frmCheckpointManage parentForm = null;
        public string dummyInpNumber = "";
        public string type = "";
        private RealtimeAnalysisResultWork work = null;

        public frmRealtimeTagList()
        {
            work = RealtimeAnalysisResultWork.GetInstance();

            InitializeComponent();
            InitializeSetting();
        }

        private void InitializeSetting()
        {
            UltraGridColumn largeConsumerTagColumn;

            largeConsumerTagColumn = griLargeConsumerTagList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTagColumn.Key = "TAGNAME";
            largeConsumerTagColumn.Header.Caption = "태그명";
            largeConsumerTagColumn.CellActivation = Activation.NoEdit;
            largeConsumerTagColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTagColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTagColumn.Hidden = false;   //필드 보이기

            largeConsumerTagColumn = griLargeConsumerTagList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTagColumn.Key = "DESCRIPTION";
            largeConsumerTagColumn.Header.Caption = "설명";
            largeConsumerTagColumn.CellActivation = Activation.NoEdit;
            largeConsumerTagColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerTagColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTagColumn.CellAppearance.TextHAlign = HAlign.Left;
            largeConsumerTagColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTagColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griLargeConsumerTagList);
        }

        //초기 로딩시 실행
        private void frmRealtimeTagList_Load(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();

            conditions.Add("INP_NUMBER", dummyInpNumber);

            if ("000001".Equals(type))
            {
                //유량 실측값
                conditions.Add("TAG_GBN","MC");
            }
            else if("000002".Equals(type))
            {
                //압력 실측값
                conditions.Add("TAG_GBN","PC");
            }
            else if("000003".Equals(type))
            {
                //대수용가 실측값
                conditions.Add("TAG_GBN","LC");
            }

            DataSet dSet = work.SelectTagList(conditions);

            griLargeConsumerTagList.DataSource = dSet.Tables["IF_IHTAGS"];
            FormManager.SetGridStyle_PerformAutoResize(griLargeConsumerTagList);

            if (griLargeConsumerTagList.Rows.Count!=0)
            {
                griLargeConsumerTagList.Rows[0].Selected = true;
            }
        }

        //태그선택
        private void btnSelectTag_Click(object sender, EventArgs e)
        {
            if (griLargeConsumerTagList.Rows.Count != 0)
            {
                string tagName = griLargeConsumerTagList.Selected.Rows[0].GetCellValue("TAGNAME").ToString();

                if("000001".Equals(type) || "000002".Equals(type))
                {
                    parentForm.texTagname.Text = tagName;
                }
                else if("000003".Equals(type))
                {
                    parentForm.texLCTagname.Text = tagName;
                }

                this.Close();
            }
        }
    }
}
