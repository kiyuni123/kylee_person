﻿using System;
using stdole;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using WaterNet.WH_RTAnalysis.work;
using WaterNet.WH_Common.utils;
using WaterNet.WaterNetCore;
using WaterNet.WaterAOCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SystemUI;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmWHMain : WaterNet.WaterAOCore.frmMap, WaterNet.WaterNetCore.IForminterface
    {
        public string dummyInpNumber = "";

        private RealtimeAnalysisResultWork work = null;
        public frmCheckpointManage checkpointForm = null;
        public frmAnalysisResultRangeManage rangeManageForm = null;
        public frmAnalysisResultControl resultControlForm = null;
        public frmAnalysisResult resultForm = null;
        public frmLargeConsumerList largeConsumerForm = null;
        public frmRealtimeAnalysisAlarm alarmForm = null;
        
        private Hashtable modelLayers = new Hashtable();
        private string loadedInpNumber = "";
        private int toolbarIdx = 0;
        private System.Timers.Timer timer = new System.Timers.Timer();

        private delegate void SetLastAnalysisTimeCallback(string text);

        //경고창으로부터 호출에서 사용할 변수
        public bool isWarning = false;
        public string increaseDate = "";
        public int tabIdx = 0;

        //public IMultiLayerLineSymbol negativeSymbol = null;
        //public IMultiLayerLineSymbol positiveSymbol = null;

        public frmWHMain()
        {

            //관망해석 결과표출에 사용할 타이머 설정
            timer.Interval = 1000 * 60 * 10;
            timer.Elapsed += new ElapsedEventHandler(DrawAnalysisResult);

            this.InitializeComponent();
            this.InitializeSetting();
            //this.getArrorLineSymbol();
            //경고창으로부터 호출일 경우 처리
            if(isWarning)
            {
                //경고 팝업 호출
                butAnalysisAlarm_Click(null,null);
            }
        }

        ///// <summary>
        ///// 물흐름방향을 정의하기 위한 심볼
        ///// Desktop : ESRI.Style을 사용하지만, Engine에서는 ESRI.ServerStyle을 사용한다.
        ///// </summary>
        //private void getArrorLineSymbol()
        //{
        //    IStyleGallery pStyleGlry = new ServerStyleGalleryClass();
        //    IStyleGalleryStorage pStyleStor = pStyleGlry as IStyleGalleryStorage;
        //    string pStylePath = pStyleStor.DefaultStylePath + "ESRI.ServerStyle";
        //    try
        //    {
        //        using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
        //        {
        //            pStyleStor.AddFile(pStylePath);
        //            //comReleaser.ManageLifetime(pStyleGlry);
        //            IEnumStyleGalleryItem pItems = pStyleGlry.get_Items("Line Symbols", pStylePath, "ArrowMid");
        //            pItems.Reset();
        //            IStyleGalleryItem pItem = pItems.Next();
        //            while (pItem != null)
        //            {
        //                if (pItem.Name.Equals("Arrow Left Middle"))
        //                {
        //                    this.negativeSymbol = pItem.Item as IMultiLayerLineSymbol;
        //                }
        //                else if (pItem.Name.Equals("Arrow Right Middle"))
        //                {
        //                    this.positiveSymbol = pItem.Item as IMultiLayerLineSymbol;
        //                }
        //                pItem = pItems.Next();
        //            }
        //        }
        //    }
        //    catch { }
        //    finally
        //    {
        //        pStyleStor.RemoveFile(pStylePath);
        //    }
        //}

        public void CallWarning()
        {
            Console.WriteLine("경보설정 진입...");


            //경보화면이 떠 있다면 일단 내린다.
            if (alarmForm != null)
            {
                alarmForm.Close();
                alarmForm = null;
            }

            Hashtable modelConditions = new Hashtable();

            modelConditions.Add("INP_NUMBER", dummyInpNumber);

            DataSet dSet = work.SelectModelData(modelConditions);

            UnloadModelLayer();

            if (dSet.Tables["WH_TITLE"] != null)
            {
                loadModelLayer(dSet.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString(), dSet.Tables["WH_TITLE"].Rows[0]["FTR_IDN"].ToString());

                //경고 팝업 호출
                alarmForm = new frmRealtimeAnalysisAlarm();

                alarmForm.StartPosition = FormStartPosition.Manual;
                alarmForm.parentForm = this;
                alarmForm.isWarning = true;
                alarmForm.TopLevel = false;
                alarmForm.Parent = this;
                alarmForm.Tag = "WH";
                alarmForm.SetBounds(this.axMap.Left, this.axMap.Top, alarmForm.Width, alarmForm.Height);
                alarmForm.BringToFront();
                alarmForm.Visible = true;

                DockManagerCommand.FlyIn();

                MoveFocusAt("중블록", "FTR_IDN = '" + dSet.Tables["WH_TITLE"].Rows[0]["FTR_IDN"] + "'");
                axMap.MapScale = 25000;
            }
            else
            {
                dummyInpNumber = "";
                MessageBox.Show("등록된 모델이 없습니다.");
            }
        }

        #region 외부상속구현

        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            get { return this.Text.ToString(); }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            work = RealtimeAnalysisResultWork.GetInstance();
        }

        public override void Open()
        {
            base.Open();
            //Viewmap_Load();

            //IMapCache pMapCache = axMap.ActiveView.FocusMap as IMapCache;
            //pMapCache.AutoCacheActive = true;
            //pMapCache.MaxScale = 3000;
            //pMapCache.ScaleLimit = true;
            //pMapCache.RefreshAutoCache();

            base.ActiveTabIndex = 1;
        }

        #endregion

        #region User Function

        /// <summary>
        /// Viewmap_Load - 지도화면에 레이어를 표시한다.
        /// </summary>
        private void Viewmap_Load()
        {
            DataTable pDataTable = m_dsLayer.Tables["SI_LAYER"];
            if (pDataTable == null) return;

            ILayer pLayer = null; IColor pLabelColor = null; IColor pOutColor = null; IColor pColor = null;
            ISymbol pLineSymbol = null; ISymbol pFillSymbol = null; ISymbol pMarkSymbol = null;
            IWorkspace pWS = null;

            WaterNet.WaterNetCore.frmSplashMsg oSplash = new WaterNet.WaterNetCore.frmSplashMsg();
            oSplash.Open();
            try
            {
                foreach (DataRow item in pDataTable.Rows)
                {
                    #region Workspace
                    if (item["F_WORKSPACE_NAME"].ToString() == "지형도")
                    {
                        pWS = VariableManager.m_Topographic;
                    }
                    else if (item["F_WORKSPACE_NAME"].ToString() == "관망도")
                    {
                        pWS = VariableManager.m_Pipegraphic;
                    }
                    else if (item["F_WORKSPACE_NAME"].ToString() == "INP")
                    {
                        pWS = VariableManager.m_INPgraphic;
                    }
                    else continue;

                    if (pWS == null) continue;
                    #endregion

                    pLayer = ArcManager.GetShapeLayer(pWS, item["F_NAME"].ToString(), item["F_ALIAS"].ToString());
                    if (pLayer == null) continue;

                    oSplash.Message = "[" + pLayer.Name + "]" + " 레이어를 불러오는 중입니다.";

                    switch (pLayer.Name)
                    {
                        case "건물":
                            //레이어 라벨
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(204, 217, 222);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "BLD_NAM", "굴림", 10, false, pLabelColor, 0, 0, string.Empty);
                            //외곽선 심볼
                            pOutColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1, pOutColor);
                            //내부 심볼
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pFillSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, (ILineSymbol)pLineSymbol, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pFillSymbol);
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 2000, 1);
                            break;
                        case "가압장":
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(204, 217, 222);
                            pMarkSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleMarkerSymbol(10, esriSimpleMarkerStyle.esriSMSCircle, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pMarkSymbol);
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 0);
                            break;
                        case "밸브":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 200, 0);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "FTR_CDE", "굴림", 10, false, pLabelColor, 0, 0, string.Empty);
                            WaterNet.WaterAOCore.ArcManager.SetUniqueValueRenderer((IFeatureLayer)pLayer, "FTR_CDE");
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 0);
                            //pLayer.Visible = false;
                            break;
                        case "소블록":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 0);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "BLK_NAM", "굴림", 10, false, pLabelColor, 0, 0, string.Empty);
                            WaterNet.WaterAOCore.ArcManager.SetUniqueValueRenderer((IFeatureLayer)pLayer, "BLK_NAM");
                            //레이어 뷰 스케일
                            //WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 10000, 1);
                            break;
                        case "중블록":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 0);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "BLK_NAM", "굴림", 10, false, pLabelColor, 0, 0, string.Empty);
                            WaterNet.WaterAOCore.ArcManager.SetUniqueValueRenderer((IFeatureLayer)pLayer, "BLK_NAM");
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 10000);
                            break;
                        case "지형지번":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 0);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "PAR_LBL", "굴림", 9, false, pLabelColor, 0, 0, string.Empty);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer);
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 1000, 0);
                            //pLayer.Visible = false;
                            break;
                        case "행정읍면동":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 200);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "HJD_NAM", "굴림", 9, true, pLabelColor, 0, 0, string.Empty);
                            //외곽선 심볼
                            pOutColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSDashDotDot, 3, pOutColor);
                            //내부 심볼
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pFillSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSHollow, (ILineSymbol)pLineSymbol, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pFillSymbol);
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 0);
                            //pLayer.Visible = true;
                            break;
                        case "법정읍면동":
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 122, 200);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "BJD_NAM", "굴림", 9, true, pLabelColor, 0, 0, string.Empty);
                            //외곽선 심볼
                            pOutColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSDashDotDot, 3, pOutColor);
                            //내부 심볼
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pFillSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSHollow, (ILineSymbol)pLineSymbol, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pFillSymbol);
                            //레이어 뷰 스케일
                            WaterNet.WaterAOCore.ArcManager.SetLayer2Scale(pLayer, 0, 0);
                            //pLayer.Visible = false;
                            break;
                        case "등고선":
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(110, 110, 110);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSDashDotDot, 1, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pLineSymbol);
                            //pLayer.Visible = false;
                            break;
                        case "상수관로":
                            //레이어 라벨
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 255, 110);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "FTR_IDN", "굴림", 9, true, pLabelColor, 2000, 0, string.Empty);
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(0, 255, 0);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 2, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pLineSymbol);
                            //pLayer.Visible = true;
                            break;
                        case "급수관로":
                            //레이어 라벨
                            pLabelColor = WaterNet.WaterAOCore.ArcManager.GetColor(255, 255, 222);
                            WaterNet.WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "FTR_IDN", "굴림", 9, true, pLabelColor, 2000, 0, string.Empty);
                            pColor = WaterNet.WaterAOCore.ArcManager.GetColor(0, 0, 100);
                            pLineSymbol = WaterNet.WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 1.5, pColor);
                            WaterNet.WaterAOCore.ArcManager.SetSimpleRenderer((IFeatureLayer)pLayer, pLineSymbol);
                            //pLayer.Visible = true;
                            break;

                        //case "민원지점":
                        //    pColor = WaterAOCore.ArcManager.GetColor(248, 24, 24);
                        //    //sField는 Map에 Lable로 표시하고 싶은 필드명
                        //    WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "CANO", "굴림", 12, true, pColor, 0, 0, string.Empty);
                        //    //sField는 구분 필드명
                        //    WaterAOCore.ArcManager.SetSimpleMarkerValueRenderer((IFeatureLayer)pLayer, "MW_GBN", (double)15, esriSimpleMarkerStyle.esriSMSDiamond, pColor);
                        //    //pLayer.Visible = false;
                        //    break;

                        //case "감시지점":
                        //    pColor = WaterAOCore.ArcManager.GetColor(0, 192, 0);
                        //    //sField는 Map에 Lable로 표시하고 싶은 필드명
                        //    WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "NAMES", "굴림", 12, false, pColor, 0, 0, string.Empty);
                        //    //sField는 구분 필드명
                        //    WaterAOCore.ArcManager.SetSimpleMarkerValueRenderer((IFeatureLayer)pLayer, "NAMES", (double)15, esriSimpleMarkerStyle.esriSMSCircle, pColor);
                        //    //pLayer.Visible = false;
                        //    break;

                        //case "중요시설":
                        //    pColor = WaterAOCore.ArcManager.GetColor(0, 0, 192);
                        //    //sField는 Map에 Lable로 표시하고 싶은 필드명
                        //    WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "NAMES", "굴림", 12, false, pColor, 0, 0, string.Empty);
                        //    //sField는 구분 필드명
                        //    WaterAOCore.ArcManager.SetSimpleMarkerValueRenderer((IFeatureLayer)pLayer, "NAMES", (double)15, esriSimpleMarkerStyle.esriSMSSquare, pColor);
                        //    //pLayer.Visible = false;
                        //    break;

                        //case "재염소처리지점":
                        //    pColor = WaterAOCore.ArcManager.GetColor(230, 155, 120);
                        //    //sField는 Map에 Lable로 표시하고 싶은 필드명
                        //    WaterAOCore.ArcManager.SetLabelProperty((IFeatureLayer)pLayer, "NAMES", "굴림", 9, false, pColor, 0, 0, string.Empty);
                        //    //sField는 구분 필드명
                        //    WaterAOCore.ArcManager.SetUniqueValueRenderer((IFeatureLayer)pLayer, "NAMES");
                        //    //pLayer.Visible = false;
                        //    break;
                    }

                    //관망해석 화면은 기본도는 비활성으로 시작한다. (블록만 활성)
                    if ("소블록".Equals(pLayer.Name))
                    {
                        pLayer.Visible = true;
                    }
                    else
                    {
                        pLayer.Visible = false;
                    }
                    
                    axMap.AddLayer(pLayer);
                }

                //최초로딩 시 등록된 모델중 최초1개를 조회
                DataSet dSet = work.SelectFirstModel(new Hashtable());

                if (dSet.Tables["WH_TITLE"] != null)
                {
                    loadModelLayer(dSet.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString(), dSet.Tables["WH_TITLE"].Rows[0]["FTR_IDN"].ToString());
                    //Shape 파일 확인 후 없으면 생성시킨다.
                }
                else
                {
                    dummyInpNumber = "";
                    MessageBox.Show("해당지역에 등록된 모델이 없습니다.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                oSplash.Close();
            }
        }

        //INP layer 로딩
        private void loadModelLayer(string inpNumber, string ftrIdn)
        {
            try
            {
                dummyInpNumber = inpNumber;

                WaterAOCore.VariableManager.m_INPgraphic = WaterAOCore.ArcManager.getShapeWorkspace(WaterAOCore.VariableManager.m_INPgraphicRootDirectory + "\\" + inpNumber);

                if (WaterAOCore.VariableManager.m_INPgraphic != null)
                {
                    ILayer junctionLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "JUNCTION", "JUNCTION");
                    ILayer pipeLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "PIPE", "PIPE");
                    ILayer pumpLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "PUMP", "PUMP");
                    ILayer reservoirLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "RESERVOIR", "RESERVOIR");
                    ILayer tankLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "TANK", "TANK");
                    ILayer valveLayer = ArcManager.GetShapeLayer(WaterAOCore.VariableManager.m_INPgraphic, "VALVE", "VALVE");

                    if (pipeLayer != null)
                    {
                        axMap.AddLayer(pipeLayer);
                    }
                    if (pumpLayer != null)
                    {
                        axMap.AddLayer(pumpLayer);
                    }
                    if (valveLayer != null)
                    {
                        axMap.AddLayer(valveLayer);
                    }
                    if (junctionLayer != null)
                    {
                        axMap.AddLayer(junctionLayer);
                    }
                    if (reservoirLayer != null)
                    {
                        axMap.AddLayer(reservoirLayer);
                    }
                    if (tankLayer != null)
                    {
                        axMap.AddLayer(tankLayer);
                    }

                    modelLayers.Add("pipe", (ITable)pipeLayer);
                    modelLayers.Add("pump", (ITable)pumpLayer);
                    modelLayers.Add("valve", (ITable)valveLayer);
                    modelLayers.Add("junction", (ITable)junctionLayer);
                    modelLayers.Add("reservoir", (ITable)reservoirLayer);
                    modelLayers.Add("tank", (ITable)tankLayer);

                    loadedInpNumber = dummyInpNumber;              //로컬 적용

                    ////레이어 초기화
                    //string lastAnalysisTime = work.SetLayerDatas(loadedInpNumber, modelLayers);
                    string lastAnalysisTime = work.SetLayerDatas(loadedInpNumber);
                    if (string.IsNullOrEmpty(lastAnalysisTime)) return;
                    SetLastAnalysisTime(lastAnalysisTime);

                    //관망도에 해석 결과를 표출하기 위한 타이머
                    Viewmap_MonitorBlock(ftrIdn);

                    //timer.Start();

                    if (resultControlForm == null) resultControlForm = new frmAnalysisResultControl();
                    resultControlForm.parentForm = this;

                    resultControlForm.StartPosition = FormStartPosition.Manual;
                    resultControlForm.TopLevel = false;
                    resultControlForm.Parent = this;
                    resultControlForm.Tag = "WH";
                    resultControlForm.SetBounds(this.axMap.Left, this.axMap.Top, resultControlForm.Width, resultControlForm.Height);
                    resultControlForm.BringToFront();
                    resultControlForm.Visible = true;
                }
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
                //MessageBox.Show(e1.ToString());
            }
        }

        //INP layer 언로딩
        private void UnloadModelLayer()
        {
            try
            {
                //현재 모델 inp number 초기화
                dummyInpNumber = "";

                //타이머 정지
                timer.Stop();

                //활성화된 팝업 종료
                if (resultForm != null)
                {
                    resultForm.Close();
                    resultForm = null;
                }

                if (checkpointForm != null)
                {
                    checkpointForm.Close();
                    checkpointForm = null;
                }

                if (rangeManageForm != null)
                {
                    rangeManageForm.Close();
                    rangeManageForm = null;
                }

                if (resultControlForm != null)
                {
                    resultControlForm.Close();
                    resultControlForm = null;
                }

                if (largeConsumerForm != null)
                {
                    largeConsumerForm.Close();
                    largeConsumerForm = null;
                }

                if (alarmForm != null)
                {
                    alarmForm.Close();
                    alarmForm = null;
                }

                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "JUNCTION");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "PIPE");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "PUMP");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "RESERVOIR");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "TANK");
                ArcManager.RemoveMapLayer((IMapControl3)axMap.Object, "VALVE");

                modelLayers.Clear();
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
            }
        }

        /// <summary>
        /// Map을 Load한 뒤 대,중,소 블록 중 인자로 받아 해당 블록을 보이게 함.
        /// </summary>
        /// <param name="strBlockType">BZ001 : 대블록, BZ002 : 중블록, BZ003 : 소블록</param>
        private void Viewmap_DefaultBlock(string strBlockType)
        {
            try
            {
                IFeatureLayer pFeatureLayer = default(IFeatureLayer);
                switch (strBlockType)
                {
                    case "BZ001":    //대블록
                        pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "대블록");
                        break;
                    case "BZ002":    //중블록
                        pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "중블록");
                        break;
                    case "BZ003":    //소블록
                        pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "소블록");
                        break;
                }
                if (pFeatureLayer == null) return;

                IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "");
                if (pFeature == null) return;

                axMap.Extent = pFeature.Shape.Envelope;
                //axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewForeground, null, pFeature.Shape.Envelope);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
            }
        }

        //해석모델을 로딩했을때 특정 소블록을 포커싱
        private void Viewmap_MonitorBlock(string ftrIdn)
        {
            try
            {
                string strFTR_IDN = string.Empty;

                IFeatureLayer pFeatureLayer = default(IFeatureLayer);

                pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "소블록");

                if (pFeatureLayer == null) return;

                IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '" + ftrIdn + "'");
                if (pFeature == null) return;

                axMap.Extent = pFeature.Shape.Envelope;
                axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGraphics, null, pFeature.Shape.Envelope);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
            }
        }

        //Dictionary<string, IElementCollection> graphicDic = new Dictionary<string, IElementCollection>();
        //public void SettingInpLayer2(Hashtable settingData)
        //{
        //    #region graphElement Delete

        //    IGraphicsContainer pGraphicsContainer = this.axMap.ActiveView as IGraphicsContainer;
        //    if (settingData["NODE_RESULT_TYPE"] != null)
        //    {
        //        string[] l = {"JUNCTION", "RESERVOIR", "TANK" };
        //        foreach (string item in l)
        //        {
        //            if (graphicDic.ContainsKey(item))
        //            {
        //                IElementCollection elems = graphicDic[item];
        //                IElement elem = null; int id = -1;
        //                for (int i = 0; i < elems.Count; i++)
        //                {
        //                    elems.QueryItem(i, out elem, out id);
        //                    pGraphicsContainer.DeleteElement(elem);
        //                }
        //                graphicDic.Remove(item);
        //            }
        //            if (graphicDic.ContainsKey(item + "_Label"))
        //            {
        //                IElementCollection elems = graphicDic[item + "_Label"];
        //                IElement elem = null; int id = -1;
        //                for (int i = 0; i < elems.Count; i++)
        //                {
        //                    elems.QueryItem(i, out elem, out id);
        //                    pGraphicsContainer.DeleteElement(elem);
        //                }
        //                graphicDic.Remove(item + "_Label");
        //            }                  
        //        }

        //    }
        //    else if (settingData["LINK_RESULT_TYPE"] != null)
        //    {
        //        string[] l = { "PIPE", "PUMP", "VALVE" };
        //        foreach (string item in l)
        //        {
        //            if (graphicDic.ContainsKey(item))
        //            {
        //                IElementCollection elems = graphicDic[item];
        //                IElement elem = null; int id = -1;
        //                for (int i = 0; i < elems.Count; i++)
        //                {
        //                    elems.QueryItem(i, out elem, out id);
        //                    pGraphicsContainer.DeleteElement(elem);
        //                }

        //                graphicDic.Remove(item);
        //            }
        //            if (graphicDic.ContainsKey(item + "_Label"))
        //            {
        //                IElementCollection elems = graphicDic[item + "_Label"];
        //                IElement elem = null; int id = -1;
        //                for (int i = 0; i < elems.Count; i++)
        //                {
        //                    elems.QueryItem(i, out elem, out id);
        //                    pGraphicsContainer.DeleteElement(elem);
        //                }
        //                graphicDic.Remove(item + "_Label");
        //            }  
        //        }
        //    }

        //    #endregion graphElement Delete

        //    #region graphElement Node ADD
        //    if (settingData["NODE_RESULT_TYPE"] != null)
        //    {
        //        ////layer가 node인 경우
        //        double range1 = double.Parse((string)settingData["nodeRange1"]);
        //        double range2 = double.Parse((string)settingData["nodeRange2"]);
        //        double range3 = double.Parse((string)settingData["nodeRange3"]);
        //        double range4 = double.Parse((string)settingData["nodeRange4"]);

        //        string fieldName = "";
        //        string title = "";

        //        if ("000001".Equals((string)settingData["NODE_RESULT_CODE"]))
        //        {
        //            fieldName = "WH_RPT_NODES.ELEVATION";
        //            title = "Elevation";
        //        }
        //        else if ("000002".Equals((string)settingData["NODE_RESULT_CODE"]))
        //        {
        //            fieldName = "WH_RPT_NODES.BASEDEMAND";
        //            title = "BaseDemand";
        //        }
        //        else if ("000003".Equals((string)settingData["NODE_RESULT_CODE"]))
        //        {
        //            fieldName = "WH_RPT_NODES.INITQUAL";
        //            title = "Initial Quality";
        //        }
        //        else if ("000004".Equals((string)settingData["NODE_RESULT_CODE"]))
        //        {
        //            fieldName = "WH_RPT_NODES.DEMAND";
        //            title = "Demand";
        //        }
        //        else if ("000005".Equals((string)settingData["NODE_RESULT_CODE"]))
        //        {
        //            fieldName = "WH_RPT_NODES.HEAD";
        //            title = "Head";
        //        }
        //        else if ("000006".Equals((string)settingData["NODE_RESULT_CODE"]))
        //        {
        //            fieldName = "WH_RPT_NODES.PRESSURE";
        //            title = "Pressure";
        //        }
        //        else if ("000007".Equals((string)settingData["NODE_RESULT_CODE"]))
        //        {
        //            fieldName = "WH_RPT_NODES.QUALITY";
        //            title = "Quality Item";
        //        }

        //        Console.WriteLine("Add Node Start: " + DateTime.Now);
        //        ISymbol TextSymbol = GetTextSymbol(ArcManager.GetColor(Color.Black), "굴림", 8);
        //        string[] l = {"JUNCTION", "RESERVOIR", "TANK" };
        //        foreach (string item in l)
        //        {
        //            IFeatureLayer pLayer = ArcManager.GetMapLayer(this.axMap.Map, item) as IFeatureLayer;
        //            if (pLayer != null)
        //            {
        //                //ArcManager.SetLabelProperty(pLayer, fieldName, "굴림", 8, false, ArcManager.GetColor(Color.Black), 0, 0, string.Empty);

        //                int index = ((IDisplayTable)pLayer).DisplayTable.FindField(fieldName);
        //                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
        //                {   
        //                    if (index >= 0)
        //                    {
        //                        IFeatureCursor pFeatureCursor = ((IDisplayTable)pLayer).DisplayTable.Search(null, true) as IFeatureCursor;
        //                        comReleaser.ManageLifetime(pFeatureCursor);
        //                        IFeature pFeature = null;

        //                        IColor color = null; int size = 1;
        //                        IElementCollection elemCol = new ElementCollectionClass();
        //                        IElementCollection elemColText = new ElementCollectionClass();

        //                        IGraphicElements elemCol2 = new GraphicElementsClass();
        //                        while ((pFeature = pFeatureCursor.NextFeature()) != null)
        //                        {
        //                            //  Point elements
        //                            double value = Convert.ToDouble(pFeature.get_Value(index));
        //                            if (value < range1)
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Blue);
        //                                size = 2;
        //                            }
        //                            else if (value < range2)
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Cyan);
        //                                size = 4;
        //                            }
        //                            else if (value < range3)
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Green);
        //                                size = 6;
        //                            }
        //                            else if (value < range4)
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Yellow);
        //                                size = 8;
        //                            }
        //                            else
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Red);
        //                                size = 10;
        //                            }
        //                            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
        //                            simpleMarkerSymbol.Outline = true;
        //                            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
        //                            simpleMarkerSymbol.Color = color;
        //                            simpleMarkerSymbol.OutlineColor = color;
        //                            simpleMarkerSymbol.Size = size;
                                    

        //                            IMarkerElement markerElement = new MarkerElementClass();
        //                            markerElement.Symbol = simpleMarkerSymbol;
        //                            IElement element = (IElement)markerElement; // Explicit Cast
        //                            element.Geometry = pFeature.ShapeCopy as ESRI.ArcGIS.Geometry.IGeometry;
        //                            elemCol.Add(element, pFeature.OID);

        //                            ITextElement textElement = new TextElementClass();
        //                            textElement.Symbol = TextSymbol as ITextSymbol;
        //                            textElement.Text = Convert.ToString(value);
        //                            IElement element2 = textElement as IElement;
        //                            element2.Geometry = pFeature.Shape as ESRI.ArcGIS.Geometry.IGeometry;
        //                            elemColText.Add(element2, pFeature.OID);
                                    
        //                        }

        //                        pGraphicsContainer.AddElements(elemCol, -1);
        //                        pGraphicsContainer.AddElements(elemColText, -1);
        //                        graphicDic.Add(item, elemCol);
        //                        graphicDic.Add(item + "_Label", elemColText);
        //                    }
        //                }
        //            }                    
        //        }
        //        Console.WriteLine("Add Node End: " + DateTime.Now);
        //    }
        //    #endregion graphElement Node ADD

        //    #region  graphElement Link ADD
        //    else if (settingData["LINK_RESULT_TYPE"] != null)
        //    {
        //        double range1 = double.Parse((string)settingData["linkRange1"]);
        //        double range2 = double.Parse((string)settingData["linkRange2"]);
        //        double range3 = double.Parse((string)settingData["linkRange3"]);
        //        double range4 = double.Parse((string)settingData["linkRange4"]);

        //        string fieldName = "";
        //        string title = "";

        //        if ("000001".Equals((string)settingData["LINK_RESULT_CODE"]))
        //        {
        //            //fieldName = "EN_LENGTH";
        //            fieldName = "WH_RPT_LINKS.LENGTH";
        //            title = "Length";
        //        }
        //        else if ("000002".Equals((string)settingData["LINK_RESULT_CODE"]))
        //        {
        //            //fieldName = "EN_DIAMETE";
        //            fieldName = "WH_RPT_LINKS.DIAMETER";
        //            title = "Diameter";
        //        }
        //        else if ("000003".Equals((string)settingData["LINK_RESULT_CODE"]))
        //        {
        //            //fieldName = "EN_ROUGHNE";
        //            fieldName = "WH_RPT_LINKS.ROUGHNESS";
        //            title = "Roughness";
        //        }
        //        else if ("000004".Equals((string)settingData["LINK_RESULT_CODE"]))
        //        {
        //            //fieldName = "EN_KBULK";
        //            fieldName = "WH_RPT_LINKS.KBULK";
        //            title = "Bulk Coeff.";
        //        }
        //        else if ("000005".Equals((string)settingData["LINK_RESULT_CODE"]))
        //        {
        //            //fieldName = "EN_KWALL";
        //            fieldName = "WH_RPT_LINKS.KWALL";
        //            title = "Wall Coeff.";
        //        }
        //        else if ("000006".Equals((string)settingData["LINK_RESULT_CODE"]))
        //        {
        //            //fieldName = "EN_FLOW";
        //            fieldName = "WH_RPT_LINKS.FLOW";
        //            title = "Flow";
        //        }
        //        else if ("000007".Equals((string)settingData["LINK_RESULT_CODE"]))
        //        {
        //            //fieldName = "EN_VELOCIT";
        //            fieldName = "WH_RPT_LINKS.VELOCITY";
        //            title = "Velocity";
        //        }
        //        else if ("000008".Equals((string)settingData["LINK_RESULT_CODE"]))
        //        {
        //            //fieldName = "EN_HEADLOS";
        //            fieldName = "WH_RPT_LINKS.HEADLOSS";
        //            title = "Unit Headloss";
        //        }

        //        string[] l = { "PIPE", "PUMP", "VALVE" };
        //        Console.WriteLine("Add Link Start: " + DateTime.Now);
        //        ISymbol TextSymbol = GetTextSymbol(ArcManager.GetColor(Color.Black), "굴림", 8);

        //        foreach (string item in l)
        //        {
        //            IFeatureLayer pLayer = ArcManager.GetMapLayer(this.axMap.Map, item) as IFeatureLayer;
        //            if (pLayer != null)
        //            {
        //                //ArcManager.SetLabelProperty(pLayer, fieldName, "굴림", 8, false, ArcManager.GetColor(Color.Blue), 0, 0, string.Empty);
        //                int index = ((IDisplayTable)pLayer).DisplayTable.FindField(fieldName);
        //                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
        //                {
        //                    if (index >= 0)
        //                    {
        //                        IFeatureCursor pFeatureCursor = ((IDisplayTable)pLayer).DisplayTable.Search(null, true) as IFeatureCursor;
        //                        comReleaser.ManageLifetime(pFeatureCursor);
        //                        IFeature pFeature = null;

        //                        IColor color = null; int size = 1;
        //                        IElementCollection elemCol = new ElementCollectionClass();
        //                        IElementCollection elemColText = new ElementCollectionClass();
        //                        while ((pFeature = pFeatureCursor.NextFeature()) != null)
        //                        {
        //                            //  Point elements
        //                            double value = Convert.ToDouble(pFeature.get_Value(index));
                                    
        //                            if (value < (-(range4 + 0.001)))
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Red);
        //                                size = 5;
        //                            }
        //                            else if (value < (-range4))
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Yellow);
        //                                size = 4;
        //                            }
        //                            else if (value < (-range3))
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Green);
        //                                size = 3;
        //                            }
        //                            else if (value < (-range2))
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Cyan);
        //                                size = 2;
        //                            }
        //                            else if (value < (range1))
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Blue);
        //                                size = 1;
        //                            }
        //                            else if (value < (range2))
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Cyan);
        //                                size = 2;
        //                            }
        //                            else if (value < (range3))
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Green);
        //                                size = 3;
        //                            }
        //                            else if (value < (range4))
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Yellow);
        //                                size = 4;
        //                            }
        //                            else
        //                            {
        //                                color = (IRgbColor)ArcManager.GetColor(Color.Red);
        //                                size = 5;
        //                            }
        //                            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
        //                            simpleLineSymbol.Color = color;
        //                            simpleLineSymbol.Style = esriSimpleLineStyle.esriSLSSolid;
        //                            simpleLineSymbol.Width = size;

        //                            ILineElement lineElement = new LineElementClass();
        //                            lineElement.Symbol = simpleLineSymbol;
        //                            IElement element = (IElement)lineElement; // Explicit Cast
        //                            element.Geometry = pFeature.ShapeCopy as ESRI.ArcGIS.Geometry.IGeometry;
        //                            elemCol.Add(element, pFeature.OID);

        //                            ITextElement textElement = new TextElementClass();
        //                            textElement.Symbol = TextSymbol as ITextSymbol;
        //                            textElement.Text = Convert.ToString(value);
        //                            IElement element2 = textElement as IElement;
        //                            IPoint p = new PointClass();
        //                            IPolyline line = pFeature.Shape as IPolyline;
        //                            ((IPolycurve)pFeature.Shape).QueryPoint(esriSegmentExtension.esriNoExtension, 0.5, true, p);// line.Length / 2, false, p);
        //                            element2.Geometry = p as ESRI.ArcGIS.Geometry.IGeometry;
        //                            elemColText.Add(element2, pFeature.OID);
        //                        }

        //                        pGraphicsContainer.AddElements(elemCol, -1);
        //                        pGraphicsContainer.AddElements(elemColText, -1);
        //                        graphicDic.Add(item, elemCol);
        //                        graphicDic.Add(item + "_Label", elemColText);
        //                    }
        //                }
        //            }
        //        }
        //        Console.WriteLine("Add Link End: " + DateTime.Now);
        //    }
        //    #endregion  graphElement Link ADD
        //    IEnvelope extent = axMap.ActiveView.Extent;
        //    ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics, null, axMap.ActiveView.Extent);
        //}

        //private ISymbol GetTextSymbol(IColor color, string fontname, int fontsize)
        //{
        //    ITextSymbol symbol = new TextSymbolClass();
            

        //    IFontDisp fontDisp = (IFontDisp)(new StdFont());
        //    fontDisp.Name = fontname == "" ? "굴림" : fontname;
        //    fontDisp.Size = fontsize;
        //    symbol.Color = color;
        //    symbol.Size = fontsize;

        //    return symbol as ISymbol;
        //}

        #region 해석결과 ID, Value Mapping 렌더링
        public void SettingInpLayer3(Hashtable settingData)
        {
            this.DrawRenderer(settingData);
            this.DrawAnnotation(settingData);
            
            ((IActiveView)axMap.Map).ContentsChanged();
            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics | esriViewDrawPhase.esriViewGeography);
        }

       
        private void DrawRenderer(Hashtable settingData)
        {
            //링크, 노드 렌더링
            try
            {
                #region 노드
                if (settingData["NODE_RESULT_TYPE"] != null)  //node
                {
                    if (!WH_Common.WH_VariableManager.analysis_Data.Tables.Contains("WH_RPT_NODES")) return;
                    #region 노드-해석결과데이터
                    string fieldName = string.Empty;
                    switch ((string)settingData["NODE_RESULT_CODE"])
                    {
                        case "000001":
                            fieldName = "ELEVATION";
                            break;
                        case "000002":
                            fieldName = "BASEDEMAND";
                            break;
                        case "000003":
                            fieldName = "INITQUAL";
                            break;
                        case "000004":
                            fieldName = "DEMAND";
                            break;
                        case "000005":
                            fieldName = "HEAD";
                            break;
                        case "000006":
                            fieldName = "PRESSURE";
                            break;
                        case "000007":
                            fieldName = "QUALITY";
                            break;
                        case "000008":
                            fieldName = "HEADLOSS";
                            break;
                    }

                    var values = (from table in WH_Common.WH_VariableManager.analysis_Data.Tables["WH_RPT_NODES"].AsEnumerable()
                                  select new
                                  {
                                      ID = table.Field<string>("NODE_ID"),
                                      VALUE = table.Field<string>(fieldName)
                                  });
                    #endregion 노드-해석결과데이터

                    EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
                    vrm.Fields = new string[] { "ID" };

                    double[] scope = { double.Parse((string)settingData["nodeRange1"]), double.Parse((string)settingData["nodeRange2"]),
                                       double.Parse((string)settingData["nodeRange3"]), double.Parse((string)settingData["nodeRange4"])};
                    Color[] colors = { Color.Blue, Color.Cyan, Color.Green, Color.Yellow, Color.Red };
                    int[] size = { 2, 2, 2, 2, 2 };

                    for (int i = 0; i <= scope.Length; i++)
                    {
                        double min = double.MinValue; double max = double.MaxValue;
                        if (i == 0)
                            max = scope[i];
                        else if (i == scope.Length)
                            min = scope[i - 1];
                        else
                        {
                            min = scope[i - 1];
                            max = scope[i];
                        }

                        //렌더링
                        var ids = values.Where(entry => (double.Parse(entry.VALUE) > min && double.Parse(entry.VALUE) <= max))
                                        .Select(v => string.Format("{0}", v.ID)).ToList();
                        if (ids.Count == 0) continue;

                        EAGL.Display.Rendering.SimplePointRenderer spr = new EAGL.Display.Rendering.SimplePointRenderer();
                        spr.Color = colors[i];
                        spr.Style = esriSimpleMarkerStyle.esriSMSCircle;
                        spr.Size = size[i];
                        ISymbol sym = spr.Symbol;

                        object key = ids[0];
                        List<object> refValues = new List<object>();
                        for (int j = 1; j < ids.Count(); j++)
                        {
                            refValues.Add(ids[j]);
                        }

                        if (min == double.MinValue || max == double.MaxValue)
                            vrm.Add(key, sym, string.Format("{0}{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));
                        else
                            vrm.Add(key, sym, string.Format("{0}-{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));

                        vrm.AddReferanceValue2(key, refValues);
                    }

                    ILayer layer = null;
                    layer = ArcManager.GetMapLayer(axMap.Map, "JUNCTION");
                    if (layer != null)
                    {
                        ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                    }
                    layer = ArcManager.GetMapLayer(axMap.Map, "RESERVOIR");
                    if (layer != null)
                    {
                        ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                    }
                    layer = ArcManager.GetMapLayer(axMap.Map, "TANK");
                    if (layer != null)
                    {
                        ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                    }

                }
                #endregion 노드

                #region 링크
                if (settingData["LINK_RESULT_TYPE"] != null)  //link
                {
                    if (!WH_Common.WH_VariableManager.analysis_Data.Tables.Contains("WH_RPT_LINKS")) return;

                    Dictionary<string, string> flows = (from table in WH_Common.WH_VariableManager.analysis_Data.Tables["WH_RPT_LINKS"].AsEnumerable()
                                                        select new
                                                        {
                                                            ID = table.Field<string>("LINK_ID"),
                                                            VALUE = table.Field<string>("FLOW")
                                                        }).ToDictionary(k => k.ID, k => k.VALUE);

                    #region 링크-해석결과데이터
                    string fieldName = string.Empty;
                    switch ((string)settingData["LINK_RESULT_CODE"])
                    {
                        case "000001":
                            fieldName = "LENGTH";
                            break;
                        case "000002":
                            fieldName = "DIAMETER";
                            break;
                        case "000003":
                            fieldName = "ROUGHNESS";
                            break;
                        case "000004":
                            fieldName = "KBULK";
                            break;
                        case "000005":
                            fieldName = "KWALL";
                            break;
                        case "000006":
                            fieldName = "FLOW";
                            break;
                        case "000007":
                            fieldName = "VELOCITY";
                            break;
                        case "000008":
                            fieldName = "HEADLOSS";
                            break;
                    }

                    Dictionary<string,string> values = (from table in WH_Common.WH_VariableManager.analysis_Data.Tables["WH_RPT_LINKS"].AsEnumerable()
                                  select new
                                  {
                                      ID = table.Field<string>("LINK_ID"),
                                      VALUE = table.Field<string>(fieldName)
                                  }).ToDictionary(k => k.ID, k => k.VALUE);
                    #endregion 링크-해석결과데이터

                    EAGL.Display.Rendering.ValueRenderingMap vrm = new EAGL.Display.Rendering.ValueRenderingMap();
                    vrm.Fields = new string[] { "ID" };

                    double[] scope = { double.Parse((string)settingData["linkRange4"]) * -1, double.Parse((string)settingData["linkRange3"]) * -1,
                                       double.Parse((string)settingData["linkRange2"]) * -1, double.Parse((string)settingData["linkRange1"]) * -1,
                                       0, 0,
                                       double.Parse((string)settingData["linkRange1"]), double.Parse((string)settingData["linkRange2"]),
                                       double.Parse((string)settingData["linkRange3"]), double.Parse((string)settingData["linkRange4"])};
                    Color[] colors = { Color.Red, Color.Yellow, Color.Green, Color.Cyan, Color.Blue, Color.Gray, Color.Blue, Color.Cyan, Color.Green, Color.Yellow, Color.Red };
                    int[] width = { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };

                    for (int i = 0; i <= scope.Length; i++)
                    {
                        double min = double.MinValue; double max = double.MaxValue;
                        if (i == 0)
                            max = scope[i];
                        else if (i == scope.Length)
                            min = scope[i - 1];
                        else
                        {
                            min = scope[i - 1];
                            max = scope[i];
                        }

                        int direction;
                        List<string> ids = getValueMapRow(ref values, min, max, out direction);
                        if (ids == null || ids.Count == 0) continue;

                        ISimpleLineDecorationElement simpleLineDecorationElement = new SimpleLineDecorationElementClass();
                        simpleLineDecorationElement.AddPosition(0.5);
                        simpleLineDecorationElement.PositionAsRatio = true;

                        ICartographicLineSymbol cartoGraphicLineSymbol = new CartographicLineSymbolClass();
                        cartoGraphicLineSymbol.Color = EAGL.Display.ColorManager.GetESRIColor(colors[i]);
                        cartoGraphicLineSymbol.Width = width[i];

                        ILineProperties lineProperties = cartoGraphicLineSymbol as ILineProperties;
                        ILineDecoration lineDecoration = new LineDecorationClass();
                        lineDecoration.AddElement(simpleLineDecorationElement);
                        lineProperties.LineDecoration = lineDecoration;
                        ISymbol sym = cartoGraphicLineSymbol as ISymbol;

                        switch (direction)
                        {
                            case -1:
                                simpleLineDecorationElement.MarkerSymbol = ArcManager.MakeArrowSymbol(Color.Black, 6, 4, 180);
                                break;
                            case 2:
                            case 0:
                                EAGL.Display.Rendering.SimpleLineRenderer slr = new EAGL.Display.Rendering.SimpleLineRenderer();
                                slr.Color = colors[i];
                                slr.Style = esriSimpleLineStyle.esriSLSSolid;
                                slr.Width = width[i];
                                sym = slr.Symbol;
                                break;
                            case 1:
                                simpleLineDecorationElement.MarkerSymbol = ArcManager.MakeArrowSymbol(Color.Black, 6, 4, 0);
                                break;
                        }

                        object key = ids[0];
                        List<object> refValues = new List<object>();
                        for (int j = 1; j < ids.Count(); j++)
                        {
                            refValues.Add(ids[j]);
                        }

                        if (min == max)
                            vrm.Add(key, sym, string.Format("{0}", "0"));
                        else if (min == double.MinValue || max == double.MaxValue)
                            vrm.Add(key, sym, string.Format("{0}{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));
                        else
                            vrm.Add(key, sym, string.Format("{0}-{1}", (min == double.MinValue ? " < " : min.ToString()), (max == double.MaxValue ? " < " : max.ToString())));

                        vrm.AddReferanceValue2(key, refValues);
                    }

                    ILayer layer = null;
                    layer = ArcManager.GetMapLayer(axMap.Map, "PIPE");
                    if (layer != null)
                    {
                        ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                    }
                    layer = ArcManager.GetMapLayer(axMap.Map, "PUMP");
                    if (layer != null)
                    {
                        ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                    }
                    layer = ArcManager.GetMapLayer(axMap.Map, "VALVE");
                    if (layer != null)
                    {
                        ((IGeoFeatureLayer)layer).Renderer = vrm.EsriRenderer;
                    }
                }
                #endregion 링크
            }
            catch { }
        }

        private List<string> getValueMapRow(ref Dictionary<string, string> values, double min, double max, out int direction)
        {
            List<string> ids = null;
            if (min == max) //0
            {
                ids = values.Where(entry => (double.Parse(entry.Value) == min && double.Parse(entry.Value) == max))
                                                   .Select(v => string.Format("{0}", v.Key)).ToList();
                direction = 0;
            }
            else if (min < 0 && max <= 0) //-
            {
                ids = values.Where(entry => (double.Parse(entry.Value) >= min && double.Parse(entry.Value) < max))
                                                   .Select(v => string.Format("{0}", v.Key)).ToList();
                direction = -1;
            }
            else if (min >= 0 && max > 0)
            {
                ids = values.Where(entry => (double.Parse(entry.Value) > min && double.Parse(entry.Value) <= max))
                                                   .Select(v => string.Format("{0}", v.Key)).ToList();
                direction = 1;
            }
            else
            {
                ids = values.Select(v => string.Format("{0}", v.Key)).ToList();
                direction = 2;
            }
            return ids;
        }

        private void DrawAnnotation(Hashtable settingData)
        {
            //링크, 노드 라벨 렌더링
            try
            {
                #region 노드
                if (settingData["NODE_RESULT_TYPE"] != null)  //node
                {
                    if (!WH_Common.WH_VariableManager.analysis_Data.Tables.Contains("WH_RPT_NODES")) return;
                    #region 노드-해석결과데이터
                    string fieldName = string.Empty;
                    switch ((string)settingData["NODE_RESULT_CODE"])
                    {
                        case "000001":
                            fieldName = "ELEVATION";
                            break;
                        case "000002":
                            fieldName = "BASEDEMAND";
                            break;
                        case "000003":
                            fieldName = "INITQUAL";
                            break;
                        case "000004":
                            fieldName = "DEMAND";
                            break;
                        case "000005":
                            fieldName = "HEAD";
                            break;
                        case "000006":
                            fieldName = "PRESSURE";
                            break;
                        case "000007":
                            fieldName = "QUALITY";
                            break;
                        case "000008":
                            fieldName = "HEADLOSS";
                            break;
                    }

                    var q = (from table in WH_Common.WH_VariableManager.analysis_Data.Tables["WH_RPT_NODES"].AsEnumerable()
                             select new
                             {
                                ID = table.Field<string>("NODE_ID"),
                                VALUE = table.Field<string>(fieldName)
                             });
                    Dictionary<string, string> values = q.ToDictionary(k => k.ID, k => k.VALUE);

                    #endregion 노드-해석결과데이터

                    EAGL.Display.LabelRenderingMap LRM = new EAGL.Display.LabelRenderingMap();
                    //LRM.FeatureLayer = (IFeatureLayer)layer;
                    LRM.Extent = this.axMap.Extent;
                    LRM.valueMap = values;

                    IFormattedTextSymbol pText = new TextSymbolClass();
                    pText.Font.Bold = false;
                    pText.Font.Name = "굴림";
                    pText.Size = 8;
                    pText.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Black);

                    LRM.Add("0", string.Empty, "ID", (ITextSymbol)pText);

                    ILayer layer = null;
                    layer = ArcManager.GetMapLayer(axMap.Map, "JUNCTION");
                    if (layer != null)
                    {
                        LRM.FeatureLayer = (IFeatureLayer)layer;
                        IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                        for (int i = 0; i < ac.Count; i++)
                        {
                            IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                            ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                            ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                        }
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
                    }
                    layer = ArcManager.GetMapLayer(axMap.Map, "RESERVOIR");
                    if (layer != null)
                    {
                        LRM.FeatureLayer = (IFeatureLayer)layer;
                        IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                        for (int i = 0; i < ac.Count; i++)
                        {
                            IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                            ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                            ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                        }
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
                    }
                    layer = ArcManager.GetMapLayer(axMap.Map, "TANK");
                    if (layer != null)
                    {
                        LRM.FeatureLayer = (IFeatureLayer)layer;
                        IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                        for (int i = 0; i < ac.Count; i++)
                        {
                            IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                            ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                            ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                        }
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
                    }

                }
                #endregion 노드

                #region 링크
                if (settingData["LINK_RESULT_TYPE"] != null)  //link
                {
                    if (!WH_Common.WH_VariableManager.analysis_Data.Tables.Contains("WH_RPT_LINKS")) return;

                    #region 링크-해석결과데이터
                    string fieldName = string.Empty;
                    switch ((string)settingData["LINK_RESULT_CODE"])
                    {
                        case "000001":
                            fieldName = "LENGTH";
                            break;
                        case "000002":
                            fieldName = "DIAMETER";
                            break;
                        case "000003":
                            fieldName = "ROUGHNESS";
                            break;
                        case "000004":
                            fieldName = "KBULK";
                            break;
                        case "000005":
                            fieldName = "KWALL";
                            break;
                        case "000006":
                            fieldName = "FLOW";
                            break;
                        case "000007":
                            fieldName = "VELOCITY";
                            break;
                        case "000008":
                            fieldName = "HEADLOSS";
                            break;
                    }

                    var q = (from table in WH_Common.WH_VariableManager.analysis_Data.Tables["WH_RPT_LINKS"].AsEnumerable()
                             select new
                             {
                                ID = table.Field<string>("LINK_ID"),
                                VALUE = table.Field<string>(fieldName)
                             });
                    Dictionary<string, string> values = q.ToDictionary(k => k.ID, k => k.VALUE);
                    #endregion 링크-해석결과데이터

                    EAGL.Display.LabelRenderingMap LRM = new EAGL.Display.LabelRenderingMap();
                    //LRM.FeatureLayer = (IFeatureLayer)layer;
                    LRM.Extent = this.axMap.Extent;
                    LRM.valueMap = values;

                    IFormattedTextSymbol pText = new TextSymbolClass();
                    pText.Font.Bold = false;
                    pText.Font.Name = "굴림";
                    pText.Size = 9;
                    pText.Color = EAGL.Display.ColorManager.GetESRIColor(Color.Blue);

                    LRM.Add("0", string.Empty, "ID", (ITextSymbol)pText);

                    ILayer layer = null;
                    layer = ArcManager.GetMapLayer(axMap.Map, "PIPE");
                    if (layer != null)
                    {
                        LRM.FeatureLayer = (IFeatureLayer)layer;
                        IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                        for (int i = 0; i < ac.Count; i++)
                        {
                            IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                            ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                            ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                        }
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
                    }
                    layer = ArcManager.GetMapLayer(axMap.Map, "PUMP");
                    if (layer != null)
                    {
                        LRM.FeatureLayer = (IFeatureLayer)layer;
                        IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                        for (int i = 0; i < ac.Count; i++)
                        {
                            IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                            ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                            ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                        }
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
                    }
                    layer = ArcManager.GetMapLayer(axMap.Map, "VALVE");
                    if (layer != null)
                    {
                        LRM.FeatureLayer = (IFeatureLayer)layer;
                        IAnnotateLayerPropertiesCollection ac = LRM.createAnnotate();
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Clear();
                        for (int i = 0; i < ac.Count; i++)
                        {
                            IElementCollection placedElements; IElementCollection unplacedElements; IAnnotateLayerProperties alProps;
                            ac.QueryItem(i, out alProps, out placedElements, out unplacedElements);
                            ((IGeoFeatureLayer)((IFeatureLayer)layer)).AnnotationProperties.Add(alProps);
                        }
                        ((IGeoFeatureLayer)((IFeatureLayer)layer)).DisplayAnnotation = true;
                    }
                }
                #endregion 링크
            }
            catch { }
        }

        #endregion 해석결과 ID, Value Mapping 렌더링

        ////Layer별 출력데이터와 색상구성을 설정
        //public void SettingInpLayer(Hashtable settingData)
        //{   
        //    try
        //    {
        //        if(settingData["NODE_RESULT_TYPE"]!=null)
        //        {
        //            //layer가 node인 경우
        //            ILayer junctionLayer = null;
        //            ILayer reservoirLayer = null;
        //            ILayer tankLayer = null;

        //            //layer 호출
        //            for (int i = 0; i < axMap.LayerCount; i++)
        //            {
        //                if ("JUNCTION".Equals(axMap.get_Layer(i).Name))
        //                {
        //                    //Junction layer
        //                    junctionLayer = axMap.get_Layer(i);
        //                }
        //                else if ("RESERVOIR".Equals(axMap.get_Layer(i).Name))
        //                {
        //                    //Reservoir layer
        //                    reservoirLayer = axMap.get_Layer(i);
        //                }
        //                else if ("TANK".Equals(axMap.get_Layer(i).Name))
        //                {
        //                    //Tank layer
        //                    tankLayer = axMap.get_Layer(i);
        //                }
        //            }
                    

        //            //if ("none".Equals(settingData["NODE_RESULT_TYPE"].ToString()))
        //            //{
        //            //    //"선택"인 경우 node의 출력을 비활성화한다.
        //            //    ((IGeoFeatureLayer)junctionLayer).DisplayAnnotation = false;
        //            //    ((IGeoFeatureLayer)reservoirLayer).DisplayAnnotation = false;
        //            //    ((IGeoFeatureLayer)tankLayer).DisplayAnnotation = false;
        //            //}
        //            //else
        //            //{
        //                VariableManager.ClassBreakRender[] tmp = new VariableManager.ClassBreakRender[5];

        //                tmp[0].dNumber = double.Parse((string)settingData["nodeRange1"]);
        //                tmp[0].dLineWidth = 2;
        //                tmp[0].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Blue);
        //                tmp[0].pFillColor = (IRgbColor)ArcManager.GetColor(Color.Blue);
        //                tmp[0].sLabel = "";

        //                tmp[1].dNumber = double.Parse((string)settingData["nodeRange2"]);
        //                tmp[1].dLineWidth = 4;
        //                tmp[1].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Cyan);
        //                tmp[1].pFillColor = (IRgbColor)ArcManager.GetColor(Color.Cyan);
        //                tmp[1].sLabel = "";

        //                tmp[2].dNumber = double.Parse((string)settingData["nodeRange3"]);
        //                tmp[2].dLineWidth = 6;
        //                tmp[2].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Green);
        //                tmp[2].pFillColor = (IRgbColor)ArcManager.GetColor(Color.Green);
        //                tmp[2].sLabel = "";

        //                tmp[3].dNumber = double.Parse((string)settingData["nodeRange4"]);
        //                tmp[3].dLineWidth = 8;
        //                tmp[3].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Yellow);
        //                tmp[3].pFillColor = (IRgbColor)ArcManager.GetColor(Color.Yellow);
        //                tmp[3].sLabel = "";

        //                tmp[4].dNumber = 100000;
        //                tmp[4].dLineWidth = 10;
        //                tmp[4].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Red);
        //                tmp[4].pFillColor = (IRgbColor)ArcManager.GetColor(Color.Red);
        //                tmp[4].sLabel = "";

        //                string fieldName = "";
        //                string title = "";

        //                if ("000001".Equals((string)settingData["NODE_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_ELEVATI";
        //                    fieldName = "WH_RPT_NODES.ELEVATION";
        //                    title = "Elevation";
        //                }
        //                else if ("000002".Equals((string)settingData["NODE_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_BASEDEM";
        //                    fieldName = "WH_RPT_NODES.BASEDEMAND";
        //                    title = "BaseDemand";
        //                }
        //                else if ("000003".Equals((string)settingData["NODE_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_INITQUA";
        //                    fieldName = "WH_RPT_NODES.INITQUAL";
        //                    title = "Initial Quality";
        //                }
        //                else if ("000004".Equals((string)settingData["NODE_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_DEMAND";
        //                    fieldName = "WH_RPT_NODES.DEMAND";
        //                    title = "Demand";
        //                }
        //                else if ("000005".Equals((string)settingData["NODE_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_HEAD";
        //                    fieldName = "WH_RPT_NODES.HEAD";
        //                    title = "Head";
        //                }
        //                else if ("000006".Equals((string)settingData["NODE_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_PRESSUR";
        //                    fieldName = "WH_RPT_NODES.PRESSURE";
        //                    title = "Pressure";
        //                }
        //                else if ("000007".Equals((string)settingData["NODE_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_QUALITY";
        //                    fieldName = "WH_RPT_NODES.QUALITY";
        //                    title = "Quality Item";
        //                }

        //                //Junction Layer 설정
        //                ArcManager.SetLabelProperty((IFeatureLayer)junctionLayer, fieldName, "굴림", 8, false, ArcManager.GetColor(Color.Black), 0, 0, string.Empty);
        //                ArcManager.SetClassBreakRenderer((IFeatureLayer)junctionLayer, fieldName, tmp, title, (double)-100000);

        //                //Reservoir Layer 설정
        //                ArcManager.SetLabelProperty((IFeatureLayer)reservoirLayer, fieldName, "굴림", 8, false, ArcManager.GetColor(Color.Black), 0, 0, string.Empty);
        //                ArcManager.SetClassBreakRenderer((IFeatureLayer)reservoirLayer, fieldName, tmp, title, (double)-100000);

        //                //Tank Layer 설정
        //                ArcManager.SetLabelProperty((IFeatureLayer)tankLayer, fieldName, "굴림", 8, false, ArcManager.GetColor(Color.Black), 0, 0, string.Empty);
        //                ArcManager.SetClassBreakRenderer((IFeatureLayer)tankLayer, fieldName, tmp, title, (double)-100000);

        //            //}
        //        }
                
        //        if (settingData["LINK_RESULT_TYPE"] != null)
        //        {
        //            //layer가 link인 경우
        //            ILayer pipeLayer = null;
        //            ILayer pumpLayer = null;
        //            ILayer valveLayer = null;

        //            //layer 호출
        //            for (int i = 0; i < axMap.LayerCount; i++)
        //            {
        //                if ("PIPE".Equals(axMap.get_Layer(i).Name))
        //                {
        //                    //Junction layer
        //                    pipeLayer = axMap.get_Layer(i);
        //                }
        //                else if ("PUMP".Equals(axMap.get_Layer(i).Name))
        //                {
        //                    //Reservoir layer
        //                    pumpLayer = axMap.get_Layer(i);
        //                }
        //                else if ("VALVE".Equals(axMap.get_Layer(i).Name))
        //                {
        //                    //Tank layer
        //                    valveLayer = axMap.get_Layer(i);
        //                }
        //            }

        //            //if ("none".Equals(settingData["LINK_RESULT_TYPE"].ToString()))
        //            //{
        //            //    //"선택"인 경우 node의 출력을 비활성화한다.
        //            //    ((IGeoFeatureLayer)pipeLayer).DisplayAnnotation = false;
        //            //    ((IGeoFeatureLayer)pumpLayer).DisplayAnnotation = false;
        //            //    ((IGeoFeatureLayer)valveLayer).DisplayAnnotation = false;
        //            //}
        //            //else
        //            //{
        //                VariableManager.ClassBreakRender[] tmp = new VariableManager.ClassBreakRender[10];

        //                tmp[0].dNumber = 0 - double.Parse((string)settingData["linkRange4"]);  //-100
        //                tmp[0].dLineWidth = 5;
        //                tmp[0].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Red);
        //                tmp[0].pFillColor = null;
        //                tmp[0].sLabel = " < " + tmp[0].dNumber.ToString();

        //                tmp[1].dNumber = 0 - double.Parse((string)settingData["linkRange3"]);  //-100
        //                tmp[1].dLineWidth = 4;
        //                tmp[1].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Yellow);
        //                tmp[1].pFillColor = null;
        //                tmp[1].sLabel = tmp[0].dNumber.ToString() + " < " + tmp[1].dNumber.ToString();

        //                tmp[2].dNumber = 0 - double.Parse((string)settingData["linkRange2"]);  //-75
        //                tmp[2].dLineWidth = 3;
        //                tmp[2].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Green);
        //                tmp[2].pFillColor = null;
        //                tmp[2].sLabel = tmp[1].dNumber.ToString() + " < " + tmp[2].dNumber.ToString();

        //                tmp[3].dNumber = 0 - double.Parse((string)settingData["linkRange1"]);   //-50
        //                tmp[3].dLineWidth = 2;
        //                tmp[3].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Cyan);
        //                tmp[3].pFillColor = null;
        //                tmp[3].sLabel = tmp[2].dNumber.ToString() + " < " + tmp[3].dNumber.ToString();

        //                tmp[4].dNumber = 0; // 0 - double.Parse((string)settingData["linkRange1"]);  //-25
        //                tmp[4].dLineWidth = 1;
        //                tmp[4].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Blue);
        //                tmp[4].pFillColor = null;
        //                tmp[4].sLabel = tmp[3].dNumber.ToString() + " < " + tmp[4].dNumber.ToString();

        //                tmp[5].dNumber = double.Parse((string)settingData["linkRange1"]);   //25
        //                tmp[5].dLineWidth = 1;
        //                tmp[5].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Blue);
        //                tmp[5].pFillColor = null;
        //                tmp[5].sLabel = tmp[4].dNumber.ToString() + " < " + tmp[5].dNumber.ToString();

        //                tmp[6].dNumber = double.Parse((string)settingData["linkRange2"]);   //50
        //                tmp[6].dLineWidth = 2;
        //                tmp[6].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Cyan);
        //                tmp[6].pFillColor = null;
        //                tmp[6].sLabel = tmp[5].dNumber.ToString() + " < " + tmp[6].dNumber.ToString();

        //                tmp[7].dNumber = double.Parse((string)settingData["linkRange3"]);  //75
        //                tmp[7].dLineWidth = 3;
        //                tmp[7].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Green);
        //                tmp[7].pFillColor = null;
        //                tmp[7].sLabel = tmp[6].dNumber.ToString() + " < " + tmp[7].dNumber.ToString();

        //                tmp[8].dNumber = double.Parse((string)settingData["linkRange4"]);  //100
        //                tmp[8].dLineWidth = 4;
        //                tmp[8].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Yellow);
        //                tmp[8].pFillColor = null;
        //                tmp[8].sLabel = tmp[7].dNumber.ToString() + " < " + tmp[8].dNumber.ToString();

        //                tmp[9].dNumber = double.Parse((string)settingData["linkRange4"]);  //100
        //                tmp[9].dLineWidth = 5;
        //                tmp[9].pLineColor = (IRgbColor)ArcManager.GetColor(Color.Red);
        //                tmp[9].pFillColor = null;
        //                tmp[9].sLabel = tmp[8].dNumber.ToString() + " < ";

        //                string fieldName = "";
        //                string title = "";

        //                if ("000001".Equals((string)settingData["LINK_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_LENGTH";
        //                    fieldName = "WH_RPT_LINKS.LENGTH";
        //                    title = "Length";
        //                }
        //                else if ("000002".Equals((string)settingData["LINK_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_DIAMETE";
        //                    fieldName = "WH_RPT_LINKS.DIAMETER";
        //                    title = "Diameter";
        //                }
        //                else if ("000003".Equals((string)settingData["LINK_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_ROUGHNE";
        //                    fieldName = "WH_RPT_LINKS.ROUGHNESS";
        //                    title = "Roughness";
        //                }
        //                else if ("000004".Equals((string)settingData["LINK_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_KBULK";
        //                    fieldName = "WH_RPT_LINKS.KBULK";
        //                    title = "Bulk Coeff.";
        //                }
        //                else if ("000005".Equals((string)settingData["LINK_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_KWALL";
        //                    fieldName = "WH_RPT_LINKS.KWALL";
        //                    title = "Wall Coeff.";
        //                }
        //                else if ("000006".Equals((string)settingData["LINK_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_FLOW";
        //                    fieldName = "WH_RPT_LINKS.FLOW";
        //                    title = "Flow";
        //                }
        //                else if ("000007".Equals((string)settingData["LINK_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_VELOCIT";
        //                    fieldName = "WH_RPT_LINKS.VELOCITY";
        //                    title = "Velocity";
        //                }
        //                else if ("000008".Equals((string)settingData["LINK_RESULT_CODE"]))
        //                {
        //                    //fieldName = "EN_HEADLOS";
        //                    fieldName = "WH_RPT_LINKS.HEADLOSS";
        //                    title = "Unit Headloss";
        //                }

        //                //Pipe Layer 설정
        //                ArcManager.SetLabelProperty((IFeatureLayer)pipeLayer, fieldName, "굴림", 8, false, ArcManager.GetColor(Color.Blue), 0, 0, string.Empty);
        //                //Pump Layer 설정
        //                ArcManager.SetLabelProperty((IFeatureLayer)pumpLayer, fieldName, "굴림", 8, false, ArcManager.GetColor(Color.Blue), 0, 0, string.Empty);
        //                //Valve Layer 설정
        //                ArcManager.SetLabelProperty((IFeatureLayer)valveLayer, fieldName, "굴림", 8, false, ArcManager.GetColor(Color.Blue), 0, 0, string.Empty);

        //                if (fieldName.Equals("WH_RPT_LINKS.FLOW"))
        //                {
        //                    this.SetArrowRenderering((IFeatureLayer)pipeLayer, fieldName, tmp);
        //                    this.SetArrowRenderering((IFeatureLayer)pumpLayer, fieldName, tmp);
        //                    this.SetArrowRenderering((IFeatureLayer)valveLayer, fieldName, tmp);
        //                }
        //                else
        //                {
        //                    ArcManager.SetClassBreakRenderer((IFeatureLayer)pipeLayer, fieldName, tmp, title, (double)-100000);
        //                    ArcManager.SetClassBreakRenderer((IFeatureLayer)pumpLayer, fieldName, tmp, title, (double)-100000);
        //                    ArcManager.SetClassBreakRenderer((IFeatureLayer)valveLayer, fieldName, tmp, title, (double)-100000);
        //                }
        //            //}
        //        }

        //        ((IActiveView)axMap.Map).ContentsChanged();
        //        ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGeography);
        //    }
        //    catch (Exception e)
        //    {
        //        //MessageBox.Show(e.ToString());
        //    }
        //}

        //private void SetArrowRenderering(IFeatureLayer layer, string fieldName, VariableManager.ClassBreakRender[] tmp)
        //{
        //    IGeoFeatureLayer geoFeatureLayer = layer as IGeoFeatureLayer;
        //    if (geoFeatureLayer == null) return;
        //    IDisplayTable displaytable = geoFeatureLayer as IDisplayTable;
        //    ITable table = displaytable.DisplayTable;

        //    using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
        //    {
        //        ICursor pCursor = table.Search(null, true);
        //        comReleaser.ManageLifetime(pCursor);

        //        IDataStatistics dataStatistics = new DataStatisticsClass();
        //        dataStatistics.Field = fieldName;
        //        dataStatistics.Cursor = pCursor as ICursor;

        //        ESRI.ArcGIS.Carto.IClassBreaksRenderer r = new ESRI.ArcGIS.Carto.ClassBreaksRendererClass();
        //        r.Field = fieldName;
        //        r.BreakCount = tmp.Length;
        //        r.MinimumBreak = tmp[0].dNumber;// dataStatistics.Statistics.Minimum;

        //        for (int i = 0; i < tmp.Length; i++)
        //        {
        //            ILineSymbol linesymbol = null;
        //            if (tmp[i].dNumber <= 0)
        //                linesymbol = (ILineSymbol)this.negativeSymbol;
        //            else
        //                linesymbol = (ILineSymbol)this.positiveSymbol;
        //            linesymbol.Color = tmp[i].pLineColor;
        //            linesymbol.Width = tmp[i].dLineWidth;

        //            if (i == 0)
        //            {
        //                r.set_Break(i, tmp[i].dNumber);
        //            }
        //            else if (i == tmp.Length - 1)
        //            {
        //                r.set_Break(i, dataStatistics.Statistics.Maximum);
        //            }
        //            else
        //            {
        //                r.set_Break(i, tmp[i].dNumber);
        //            }
        //            r.set_Label(i, tmp[i].sLabel);
        //            r.set_Symbol(i, (ISymbol)linesymbol);
        //        }

        //        ILegendInfo pLegendInfo = (ILegendInfo)r;
        //        if (pLegendInfo.LegendGroupCount > 0)
        //        {
        //            ILegendGroup pLegendGroup = pLegendInfo.get_LegendGroup(0);
        //            pLegendGroup.Heading = "Flow";
        //        }

        //        r.SortClassesAscending = true;
        //        geoFeatureLayer.Renderer = r as IFeatureRenderer;
        //    }

        //}

        //Layer에 Node/Link 해석결과 표출 설정
        public void SettingLayerLabel(string gbn, bool isShow)
        {
            if ("node".Equals(gbn))
            {
                //layer가 node인 경우
                ILayer junctionLayer = null;
                ILayer reservoirLayer = null;
                ILayer tankLayer = null;

                //layer 호출
                for (int i = 0; i < axMap.LayerCount; i++)
                {
                    if ("JUNCTION".Equals(axMap.get_Layer(i).Name))
                    {
                        //Junction layer
                        junctionLayer = axMap.get_Layer(i);
                    }
                    else if ("RESERVOIR".Equals(axMap.get_Layer(i).Name))
                    {
                        //Reservoir layer
                        reservoirLayer = axMap.get_Layer(i);
                    }
                    else if ("TANK".Equals(axMap.get_Layer(i).Name))
                    {
                        //Tank layer
                        tankLayer = axMap.get_Layer(i);
                    }
                }

                //Node 해석결과 출력여부 설정
                if (isShow)
                {
                    ((IGeoFeatureLayer)junctionLayer).DisplayAnnotation = true;
                    ((IGeoFeatureLayer)reservoirLayer).DisplayAnnotation = true;
                    ((IGeoFeatureLayer)tankLayer).DisplayAnnotation = true;
                }
                else
                {
                    ((IGeoFeatureLayer)junctionLayer).DisplayAnnotation = false;
                    ((IGeoFeatureLayer)reservoirLayer).DisplayAnnotation = false;
                    ((IGeoFeatureLayer)tankLayer).DisplayAnnotation = false;
                }
            }
            else if ("link".Equals(gbn))
            {
                //layer가 link인 경우
                ILayer pipeLayer = null;
                ILayer pumpLayer = null;
                ILayer valveLayer = null;

                //layer 호출
                for (int i = 0; i < axMap.LayerCount; i++)
                {
                    if ("PIPE".Equals(axMap.get_Layer(i).Name))
                    {
                        //Junction layer
                        pipeLayer = axMap.get_Layer(i);
                    }
                    else if ("PUMP".Equals(axMap.get_Layer(i).Name))
                    {
                        //Reservoir layer
                        pumpLayer = axMap.get_Layer(i);
                    }
                    else if ("VALVE".Equals(axMap.get_Layer(i).Name))
                    {
                        //Tank layer
                        valveLayer = axMap.get_Layer(i);
                    }
                }

                if (isShow)
                {
                    ((IGeoFeatureLayer)pipeLayer).DisplayAnnotation = true;
                    ((IGeoFeatureLayer)pumpLayer).DisplayAnnotation = true;
                    ((IGeoFeatureLayer)valveLayer).DisplayAnnotation = true;
                }

                else
                {
                    ((IGeoFeatureLayer)pipeLayer).DisplayAnnotation = false;
                    ((IGeoFeatureLayer)pumpLayer).DisplayAnnotation = false;
                    ((IGeoFeatureLayer)valveLayer).DisplayAnnotation = false;
                }
            }

            ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics);
        }

        //해당하는 feature를 찾아서 중앙에 위치시킨다.
        public void MoveFocusAt(string layerName, string whereCase)
        {
            try
            {
                ILayer layer = ArcManager.GetMapLayer((IMapControl3)axMap.Object, layerName);

                if (layer != null)
                {
                    ESRI.ArcGIS.Geodatabase.IFeature feature = ArcManager.GetFeature(layer, whereCase);

                    if (feature != null)
                    {
                        ArcManager.FlashShape(axMap.ActiveView, (IGeometry)feature.Shape, 10);
                        ArcManager.MoveCenterAt((IMapControl3)axMap.Object, feature);
                    }
                }
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
            }
        }

        //좌측 블록 컨트롤을 click했을 경우
        protected override void tvBlock_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                TreeNode oNode = e.Node; // tvBlock.SelectedNode;
                if (oNode == null) return;

                IFeatureLayer pFeatureLayer = default(IFeatureLayer);
                switch (oNode.Name)
                {
                    case "BZ001":    //대블록
                        pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "대블록");
                        break;
                    case "BZ002":    //중블록
                        pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "중블록");
                        break;
                    case "BZ003":    //소블록
                        pFeatureLayer = (IFeatureLayer)ArcManager.GetMapLayer((IMapControl3)axMap.Object, "소블록");
                        break;
                }
                if (pFeatureLayer == null) return;

                IFeature pFeature = ArcManager.GetFeature((ILayer)pFeatureLayer, "FTR_IDN = '" + oNode.Tag.ToString() + "'");
                
                if (pFeature == null) return;

                if (!"BZ001".Equals(oNode.Name))
                {
                    texLastAnalysisTime.Text = "";

                    //대블록이 아닌경우에만 동작

                    Hashtable conditions = new Hashtable();

                    conditions.Add("FTR_CODE", oNode.Name);
                    conditions.Add("FTR_IDN", oNode.Tag);

                    DataSet dset = work.SelectModelByBlock(conditions);

                    if (dset.Tables["WH_TITLE"].Rows.Count != 0)
                    {
                        if (!dummyInpNumber.Equals(dset.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString()))
                        {
                            UnloadModelLayer();
                            //로드된 inp와 다른경우
                            string frtIdn = oNode.Tag.ToString();

                            if ("BZ002".Equals(oNode.Name))
                            {
                                //중블록 선택 시
                                frtIdn = dset.Tables["WH_TITLE"].Rows[0]["FTR_IDN"].ToString();
                            }

                            loadModelLayer(dset.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString(), frtIdn);
                        }
                        else
                        {
                            //string lastAnalysisTime = work.SetLayerDatas(loadedInpNumber, modelLayers);
                            string lastAnalysisTime = work.SetLayerDatas(loadedInpNumber);
                            SetLastAnalysisTime(lastAnalysisTime);
                        }
                    }
                    else
                    {
                        UnloadModelLayer();
                        MessageBox.Show("등록된 모델이 없습니다.");
                    }

                    timer.Start();
                }

                axMap.Extent = pFeature.Shape.Envelope;
                axMap.ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGraphics, pFeature, axMap.Extent);
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }
        #endregion

        #region Control Function

        //해석결과 출력
        private void DrawAnalysisResult(System.Object sender, EventArgs eArgs)
        {
            try
            {
                //string lastAnalysisTime = work.SetLayerDatas(loadedInpNumber, modelLayers);
                string lastAnalysisTime = work.SetLayerDatas(loadedInpNumber);
                SetLastAnalysisTime(lastAnalysisTime);
                ArcManager.PartialRefresh(axMap.ActiveView.FocusMap, esriViewDrawPhase.esriViewGraphics);
                
                //대수용가 동작정보 갱신
                if (largeConsumerForm != null)
                {
                    largeConsumerForm.SelectActivateLargeConsumer();
                }
            }
            catch (Exception e)
            {
                timer.Stop();
                timer.Dispose();
                Console.WriteLine(e.ToString());
                //MessageBox.Show(e.ToString());
            }
        }

        #endregion

        //폼 최초실행
        private void frmWHMain_Load(object sender, EventArgs e)
        {
            //============================================================================
            this.panLastAnalysisTime.BringToFront();

            try
            {
                if (isWarning)
                {
                    Hashtable modelConditions = new Hashtable();

                    modelConditions.Add("INP_NUMBER", dummyInpNumber);

                    DataSet dSet = work.SelectModelData(modelConditions);

                    if (dSet.Tables["WH_TITLE"] != null)
                    {
                        loadModelLayer(dSet.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString(), dSet.Tables["WH_TITLE"].Rows[0]["FTR_IDN"].ToString());

                        //경고 팝업 호출
                        alarmForm = new frmRealtimeAnalysisAlarm();

                        alarmForm.StartPosition = FormStartPosition.Manual;
                        alarmForm.parentForm = this;
                        alarmForm.isWarning = true;
                        alarmForm.TopLevel = false;
                        alarmForm.Parent = this;
                        alarmForm.Tag = "WH";
                        alarmForm.SetBounds(this.axMap.Left, this.axMap.Top, alarmForm.Width, alarmForm.Height);
                        alarmForm.BringToFront();
                        alarmForm.Visible = true;

                        DockManagerCommand.FlyIn();

                        MoveFocusAt("중블록", "FTR_IDN = '" + dSet.Tables["WH_TITLE"].Rows[0]["FTR_IDN"] + "'");
                        axMap.MapScale = 25000;
                    }
                    else
                    {
                        dummyInpNumber = "";
                        MessageBox.Show("등록된 모델이 없습니다.");
                    }

                    isWarning = false;
                }
                else
                {
                    //최초로딩 시 등록된 모델중 최초1개를 조회
                    DataSet dSet = work.SelectFirstModel(new Hashtable());

                    if (dSet.Tables["WH_TITLE"] != null)
                    {
                        loadModelLayer(dSet.Tables["WH_TITLE"].Rows[0]["INP_NUMBER"].ToString(), dSet.Tables["WH_TITLE"].Rows[0]["MFTRIDN"].ToString());
                    }
                    else
                    {
                        dummyInpNumber = "";
                        MessageBox.Show("등록된 모델이 없습니다.");
                    }
                }
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }

        //해석결과 팝업 호출
        private void butAnalysisResult_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkpointForm != null)
                {
                    MessageBox.Show("지점관리화면이 열려있습니다. 지점관리를 닫고 실행해주십시오.");
                    return;
                }

                if("".Equals(dummyInpNumber))
                {
                    MessageBox.Show("해당지역의 모델이 없습니다.");
                }
                else
                {
                    //frmAnalysisResult oForm = new frmAnalysisResult();
                    resultForm = new frmAnalysisResult();
                    resultForm.parentForm = this;
                    //oForm.dummyInpNumber = dummyInpNumber;

                    resultForm.StartPosition = FormStartPosition.Manual;
                    resultForm.TopLevel = false;
                    resultForm.Parent = this;
                    resultForm.Tag = "WH";
                    resultForm.SetBounds(this.axMap.Left, this.axMap.Top, resultForm.Width, resultForm.Height);
                    resultForm.BringToFront();
                    resultForm.Visible = true;

                    DockManagerCommand.FlyIn();
                }
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }

        //지점관리 팝업 호출
        private void butCheckpointManage_Click(object sender, EventArgs e)
        {
            try
            {
                if (resultForm != null)
                {
                    MessageBox.Show("해석결과화면이 열려있습니다. 해석결과를 닫고 실행해주십시오.");
                    return;
                }

                if ("".Equals(dummyInpNumber))
                {
                    MessageBox.Show("해당지역의 모델이 없습니다.");
                }
                else
                {

                    checkpointForm = new frmCheckpointManage();
                    checkpointForm.parentForm = this;
                    checkpointForm.StartPosition = FormStartPosition.Manual;
                    checkpointForm.TopLevel = false;
                    checkpointForm.Parent = this;
                    checkpointForm.Tag = "WH";
                    checkpointForm.SetBounds(this.axMap.Left, this.axMap.Top, checkpointForm.Width, checkpointForm.Height);
                    checkpointForm.BringToFront();
                    checkpointForm.Visible = true;

                    DockManagerCommand.FlyIn();
                }
            }
            catch (Exception e1)
            {
               //MessageBox.Show(e1.ToString());
            }

        }

        //해석결과 범위 팝업 호출
        private void butAnalysisResultRangeManage_Click(object sender, EventArgs e)
        {
            try
            {
                if ("".Equals(dummyInpNumber))
                {
                    MessageBox.Show("해당지역의 모델이 없습니다.");
                }
                else
                {
                    rangeManageForm = new frmAnalysisResultRangeManage();
                    //rangeManageForm.parent = this;

                    rangeManageForm.StartPosition = FormStartPosition.Manual;
                    rangeManageForm.TopLevel = false;
                    rangeManageForm.Parent = this;
                    rangeManageForm.Tag = "WH";
                    rangeManageForm.SetBounds(this.axMap.Left, this.axMap.Top, rangeManageForm.Width, rangeManageForm.Height);
                    rangeManageForm.BringToFront();
                    rangeManageForm.Visible = true;

                    DockManagerCommand.FlyIn();
                }
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }

        //대수용가 동작정보 팝업 호출
        private void butAnalysisResultLargeConsumer_Click(object sender, EventArgs e)
        {
            try
            {
                if ("".Equals(dummyInpNumber))
                {
                    MessageBox.Show("해당지역의 모델이 없습니다.");
                }
                else
                {
                    largeConsumerForm = new frmLargeConsumerList();

                    largeConsumerForm.StartPosition = FormStartPosition.Manual;
                    largeConsumerForm.dummyInpNumber = dummyInpNumber;
                    largeConsumerForm.TopLevel = false;
                    largeConsumerForm.parentForm = this;
                    largeConsumerForm.Parent = this;
                    largeConsumerForm.Tag = "WH";
                    largeConsumerForm.SetBounds(this.axMap.Left, this.axMap.Top, largeConsumerForm.Width, largeConsumerForm.Height);
                    largeConsumerForm.BringToFront();
                    largeConsumerForm.Visible = true;

                    DockManagerCommand.FlyIn();
                }
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }

        //경고 팝업 호출
        private void butAnalysisAlarm_Click(object sender, EventArgs e)
        {
            try
            {
                alarmForm = new frmRealtimeAnalysisAlarm();

                alarmForm.StartPosition = FormStartPosition.Manual;
                alarmForm.parentForm = this;
                alarmForm.TopLevel = false;
                alarmForm.Parent = this;
                alarmForm.Tag = "WH";
                alarmForm.SetBounds(this.axMap.Left, this.axMap.Top, alarmForm.Width, alarmForm.Height);
                alarmForm.BringToFront();
                alarmForm.Visible = true;

                DockManagerCommand.FlyIn();
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }

        //지도를 click 했을 경우 이벤트
        private void axMap_OnMouseDown(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            try
            {
                if (e.button == 2) //오른쪽버튼
                {
                    List<string> nodes = new List<string>();
                    nodes.Add("JUNCTION"); nodes.Add("TANK"); nodes.Add("RESERVOIR");
                    nodes.Add("PIPE"); nodes.Add("VALVE"); nodes.Add("PUMP");

                    ILayer layer = null;
                    foreach (string node in nodes)
                    {
                        layer = ArcManager.GetMapLayer(this.axMap.Map, node);
                        if (layer != null)
                        {
                            EAGL.Search.AttributeSelector abs = new EAGL.Search.AttributeSelector((IFeatureSelection)layer);
                            if (abs.FeatureSelection.SelectionSet.Count > 0)
                            {
                                this.contextMenuStrip1.Show(this.axMap, e.x, e.y);
                                return;
                            }
                        }
                    }
                }
                else if (toolActionCommand.Checked)
                {
                    //checkpoint form이 열려있고 Toolbar가 Select Features일때만 구동

                    //GIS 상 좌표
                    IPoint pPoint = new PointClass();
                    pPoint.X = e.mapX;
                    pPoint.Y = e.mapY;
                    pPoint.Z = 0;

                    IGeometry pGeom = (IGeometry)pPoint;

                    double dblContains = 0;

                    if (ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)100000)
                    {
                        dblContains = 300;
                    }
                    else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)100000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)50000)
                    {
                        dblContains = 200;
                    }
                    else if (ArcManager.GetMapScale((IMapControl3)axMap.Object) <= (double)50000 && ArcManager.GetMapScale((IMapControl3)axMap.Object) > (double)15000)
                    {
                        dblContains = 100;
                    }
                    else
                    {
                        dblContains = 10;
                    }

                    ILayer layer = null;

                    if (resultForm != null)
                    {
                        //해석결과가 열린 경우
                        if ("실측값 비교(이동식)".Equals(resultForm.tabAnalysisResult.SelectedTab.Text))
                        {
                            //실측지점의 row상태가 추가가 아니라면 동작하지 않는다.
                            if (((DataTable)resultForm.griMeasureDump.DataSource).Rows[resultForm.griMeasureDump.Selected.Rows[0].Index].RowState == DataRowState.Added)
                            {
                                if ("Flow".Equals(resultForm.comDetailDumpType.SelectedValue))
                                {
                                    //유량일 경우 PIPE layer
                                    layer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "PIPE");
                                }
                                else
                                {
                                    //압력일때는 JUNCTION layer
                                    layer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "JUNCTION");
                                }

                                IFeatureCursor pIFCursorMP = ArcManager.GetSpatialCursor(layer, pGeom, dblContains, esriSpatialRelEnum.esriSpatialRelIntersects, string.Empty);

                                IFeature pFeatureMP = pIFCursorMP.NextFeature();

                                if (pFeatureMP != null)
                                {
                                    //axMap.Map.SelectFeature(layer, pFeatureMP);
                                    ArcManager.FlashShape(axMap.ActiveView, (IGeometry)pFeatureMP.Shape, 10);

                                    object oNO = WaterAOCore.ArcManager.GetValue(pFeatureMP, "ID");

                                    resultForm.texDetailId.Text = oNO.ToString();
                                }
                            }
                        }
                    }
                    else if (checkpointForm != null)
                    {
                        //지점관리가 열린 경우
                        if ("실측지점".Equals(checkpointForm.tabCheckpoint.SelectedTab.Text))
                        {
                            if (checkpointForm.griRealtimeCheckpointList.Rows.Count != 0)
                            {
                                //실측지점의 row상태가 추가가 아니라면 동작하지 않는다.
                                if (((DataTable)checkpointForm.griRealtimeCheckpointList.DataSource).Rows[checkpointForm.griRealtimeCheckpointList.Selected.Rows[0].Index].RowState == DataRowState.Added)
                                {
                                    if ("000001".Equals(checkpointForm.comMeterType.SelectedValue))
                                    {
                                        //유량계일때는 PIPE layer
                                        layer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "PIPE");
                                    }
                                    else
                                    {
                                        //압력계일때는 JUNCTION layer
                                        layer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "JUNCTION");
                                    }

                                    IFeatureCursor pIFCursorMP = ArcManager.GetSpatialCursor(layer, pGeom, dblContains, esriSpatialRelEnum.esriSpatialRelIntersects, string.Empty);

                                    IFeature pFeatureMP = pIFCursorMP.NextFeature();

                                    if (pFeatureMP != null)
                                    {
                                        //axMap.Map.SelectFeature(layer, pFeatureMP);
                                        ArcManager.FlashShape(axMap.ActiveView, (IGeometry)pFeatureMP.Shape, 10);

                                        object oNO = WaterAOCore.ArcManager.GetValue(pFeatureMP, "ID");

                                        checkpointForm.texRealtimeId.Text = oNO.ToString();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("추가 시 외에는 지점 변경을 할 수 없습니다. 삭제 후 다시 등록하십시오.");
                                }
                            }
                        }
                        else if ("대수용가".Equals(checkpointForm.tabCheckpoint.SelectedTab.Text))
                        {
                            if (checkpointForm.griLargeConsumerList.Rows.Count != 0)
                            {
                                //대수용가의 row상태가 추가가 아니라면 동작하지 않는다.
                                if (((DataTable)checkpointForm.griLargeConsumerList.DataSource).Rows[checkpointForm.griLargeConsumerList.Selected.Rows[0].Index].RowState == DataRowState.Added)
                                {
                                    layer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "JUNCTION");

                                    IFeatureCursor pIFCursorMP = ArcManager.GetSpatialCursor(layer, pGeom, dblContains, esriSpatialRelEnum.esriSpatialRelIntersects, string.Empty);

                                    IFeature pFeatureMP = pIFCursorMP.NextFeature();

                                    if (pFeatureMP != null)
                                    {
                                        //axMap.Map.SelectFeature(layer, pFeatureMP);
                                        ArcManager.FlashShape(axMap.ActiveView, (IGeometry)pFeatureMP.Shape, 10);

                                        object oNO = WaterAOCore.ArcManager.GetValue(pFeatureMP, "ID");

                                        checkpointForm.texLCId.Text = oNO.ToString();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("추가 시 외에는 지점 변경을 할 수 없습니다. 삭제 후 다시 등록하십시오.");
                                }
                            }
                        }

                        else if ("평균수압지점".Equals(checkpointForm.tabCheckpoint.SelectedTab.Text))
                        {
                            if (checkpointForm.griAveragePressureList.Rows.Count != 0)
                            {
                                //평균수압지점 row상태가 추가가 아니라면 동작하지 않는다.
                                if (((DataTable)checkpointForm.griAveragePressureList.DataSource).Rows[checkpointForm.griAveragePressureList.Selected.Rows[0].Index].RowState == DataRowState.Added)
                                {
                                    layer = WaterAOCore.ArcManager.GetMapLayer((IMapControl3)axMap.Object, "JUNCTION");

                                    IFeatureCursor pIFCursorMP = ArcManager.GetSpatialCursor(layer, pGeom, dblContains, esriSpatialRelEnum.esriSpatialRelIntersects, string.Empty);

                                    IFeature pFeatureMP = pIFCursorMP.NextFeature();

                                    if (pFeatureMP != null)
                                    {
                                        //axMap.Map.SelectFeature(layer, pFeatureMP);
                                        ArcManager.FlashShape(axMap.ActiveView, (IGeometry)pFeatureMP.Shape, 10);

                                        object oNO = WaterAOCore.ArcManager.GetValue(pFeatureMP, "ID");

                                        checkpointForm.texPreId.Text = oNO.ToString();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("추가 시 외에는 지점 변경을 할 수 없습니다. 삭제 후 다시 등록하십시오.");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }

        //Toolbar의 버튼을 클릭할때 이벤트
        private void axToolbar_OnItemClick(object sender, IToolbarControlEvents_OnItemClickEvent e)
        {
            try
            {
                toolbarIdx = e.index;
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }

        //하단 해석시간 표출
        private void SetLastAnalysisTime(string text)
        {
            try
            {
                if (texLastAnalysisTime.InvokeRequired)
                {
                    SetLastAnalysisTimeCallback setLastAnalysisTimeCallBack = new SetLastAnalysisTimeCallback(SetLastAnalysisTime);
                    this.Invoke(setLastAnalysisTimeCallBack, new object[] { text });
                }
                else
                {
                    texLastAnalysisTime.Text = text;
                }
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }
 
        //폼 종료시 호출
        private void frmWHMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                timer.Stop();
                timer.Dispose();
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
            }
        }

        /// <summary>
        /// 맵에서 Junction, Pipe 객체를 선택하여 해석결과화면 표시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsMenuAnalysisResult_Click(object sender, EventArgs e)
        {
            List<string> nodelist = new List<string>();
            List<string> linklist = new List<string>();

            List<string> nodes = new List<string>();
            nodes.Add("JUNCTION"); nodes.Add("TANK"); nodes.Add("RESERVOIR");
            ILayer layer = null;
            foreach (string node in nodes)
            {
                layer = ArcManager.GetMapLayer(this.axMap.Map, node);
                if (layer != null)
                {
                    EAGL.Search.AttributeSelector abs = new EAGL.Search.AttributeSelector((IFeatureSelection)layer);
                    using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                    {
                        ICursor cursor = abs.GetSelectionAsCusror(string.Empty);
                        comReleaser.ManageLifetime(cursor);
                        IRow ipRow = cursor.NextRow();
                        while (ipRow != null)
                        {
                            nodelist.Add(Convert.ToString(ipRow.get_Value(ipRow.Fields.FindField("ID"))));
                            ipRow = cursor.NextRow();
                        }
                    }
                }
            }
            
            
            List<string> links = new List<string>();
            links.Add("PIPE"); links.Add("VALVE"); links.Add("PUMP");
            foreach (string link in links)
            {
                layer = ArcManager.GetMapLayer(this.axMap.Map, link);
                if (layer != null)
                {
                    EAGL.Search.AttributeSelector abs = new EAGL.Search.AttributeSelector((IFeatureSelection)layer);
                    using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                    {
                        ICursor cursor = abs.GetSelectionAsCusror(string.Empty);
                        comReleaser.ManageLifetime(cursor);
                        IRow ipRow = cursor.NextRow();
                        while (ipRow != null)
                        {
                            linklist.Add(Convert.ToString(ipRow.get_Value(ipRow.Fields.FindField("ID"))));
                            ipRow = cursor.NextRow();
                        }
                    }
                }
            }

            string[] split = texLastAnalysisTime.Text.Split(new string[] {" : "},StringSplitOptions.RemoveEmptyEntries);
            if (split.Length != 2) return;
            split = split[1].Split(' ');
            if (split.Length != 2) return;

            if (resultForm == null)
                resultForm = new frmAnalysisResult(nodelist, linklist);    
            
            resultForm.parentForm = this;
            resultForm.StartPosition = FormStartPosition.Manual;
            resultForm.TopLevel = false;
            resultForm.Parent = this;
            resultForm.Tag = "WH";
            resultForm.SetBounds(this.axMap.Left, this.axMap.Top, resultForm.Width, resultForm.Height);
            resultForm.BringToFront();
            resultForm.Visible = true;
            DockManagerCommand.FlyIn();

            string lastAnalysisTime = split[0].Trim();
            resultForm.SetLockControls(lastAnalysisTime);
        }

    }
}
