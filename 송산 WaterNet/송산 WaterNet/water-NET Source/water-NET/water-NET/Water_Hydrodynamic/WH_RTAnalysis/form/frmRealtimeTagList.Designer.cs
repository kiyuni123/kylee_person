﻿namespace WaterNet.WH_RTAnalysis.form
{
    partial class frmRealtimeTagList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.btnSelectTag = new System.Windows.Forms.Button();
            this.griLargeConsumerTagList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griLargeConsumerTagList)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox3.Location = new System.Drawing.Point(10, 257);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(472, 9);
            this.pictureBox3.TabIndex = 117;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(10, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(472, 9);
            this.pictureBox2.TabIndex = 116;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(482, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(10, 266);
            this.pictureBox1.TabIndex = 115;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 0);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 266);
            this.picFrLeftM1.TabIndex = 114;
            this.picFrLeftM1.TabStop = false;
            // 
            // btnSelectTag
            // 
            this.btnSelectTag.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSelectTag.Font = new System.Drawing.Font("굴림", 9F);
            this.btnSelectTag.Location = new System.Drawing.Point(432, 229);
            this.btnSelectTag.Name = "btnSelectTag";
            this.btnSelectTag.Size = new System.Drawing.Size(44, 22);
            this.btnSelectTag.TabIndex = 120;
            this.btnSelectTag.Text = "선택";
            this.btnSelectTag.UseVisualStyleBackColor = true;
            this.btnSelectTag.Click += new System.EventHandler(this.btnSelectTag_Click);
            // 
            // griLargeConsumerTagList
            // 
            this.griLargeConsumerTagList.Dock = System.Windows.Forms.DockStyle.Top;
            this.griLargeConsumerTagList.Location = new System.Drawing.Point(10, 9);
            this.griLargeConsumerTagList.Name = "griLargeConsumerTagList";
            this.griLargeConsumerTagList.Size = new System.Drawing.Size(472, 214);
            this.griLargeConsumerTagList.TabIndex = 123;
            this.griLargeConsumerTagList.Text = "ultraGrid1";
            // 
            // frmRealtimeTagList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 266);
            this.Controls.Add(this.griLargeConsumerTagList);
            this.Controls.Add(this.btnSelectTag);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.picFrLeftM1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRealtimeTagList";
            this.Text = "실시간 태그 선택";
            this.Load += new System.EventHandler(this.frmRealtimeTagList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griLargeConsumerTagList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnSelectTag;
        public Infragistics.Win.UltraWinGrid.UltraGrid griLargeConsumerTagList;
    }
}