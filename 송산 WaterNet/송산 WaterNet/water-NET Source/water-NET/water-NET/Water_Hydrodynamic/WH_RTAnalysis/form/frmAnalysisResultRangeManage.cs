﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;

using WaterNet.WH_RTAnalysis.work;
using WaterNet.WH_Common.utils;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using EMFrame.log;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmAnalysisResultRangeManage : Form
    {
        private RealtimeAnalysisResultWork work = null;
        //public frmWHMain parent = null;
        public string dummyInpNumber = "";
        public frmAnalysisResultControl parentForm;

        public frmAnalysisResultRangeManage()
        {
            InitializeComponent();
            InitializeSetting();

            work = RealtimeAnalysisResultWork.GetInstance();
        }

        private void InitializeSetting()
        {
            //Node 해석결과 그리드 초기화
            UltraGridColumn analysisResultRangeColumn;

            analysisResultRangeColumn = griAnalysisResultRange.DisplayLayout.Bands[0].Columns.Add();
            analysisResultRangeColumn.Key = "INP_NUMBER";
            analysisResultRangeColumn.Header.Caption = "INP 번호";
            analysisResultRangeColumn.CellActivation = Activation.NoEdit;
            analysisResultRangeColumn.CellClickAction = CellClickAction.RowSelect;
            analysisResultRangeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisResultRangeColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisResultRangeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisResultRangeColumn.Hidden = false;   //필드 보이기

            analysisResultRangeColumn = griAnalysisResultRange.DisplayLayout.Bands[0].Columns.Add();
            analysisResultRangeColumn.Key = "RESULT_TYPE";
            analysisResultRangeColumn.Header.Caption = "RESULT_TYPE";
            analysisResultRangeColumn.CellActivation = Activation.NoEdit;
            analysisResultRangeColumn.CellClickAction = CellClickAction.RowSelect;
            analysisResultRangeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisResultRangeColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisResultRangeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisResultRangeColumn.Hidden = true;   //필드 보이기

            analysisResultRangeColumn = griAnalysisResultRange.DisplayLayout.Bands[0].Columns.Add();
            analysisResultRangeColumn.Key = "LAYER_TYPE";
            analysisResultRangeColumn.Header.Caption = "Layer 형식";
            analysisResultRangeColumn.CellActivation = Activation.NoEdit;
            analysisResultRangeColumn.CellClickAction = CellClickAction.RowSelect;
            analysisResultRangeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisResultRangeColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisResultRangeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisResultRangeColumn.Hidden = false;   //필드 보이기

            analysisResultRangeColumn = griAnalysisResultRange.DisplayLayout.Bands[0].Columns.Add();
            analysisResultRangeColumn.Key = "RESULT_CODE";
            analysisResultRangeColumn.Header.Caption = "RESULT_CODE";
            analysisResultRangeColumn.CellActivation = Activation.NoEdit;
            analysisResultRangeColumn.CellClickAction = CellClickAction.RowSelect;
            analysisResultRangeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisResultRangeColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisResultRangeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisResultRangeColumn.Hidden = true;   //필드 보이기

            analysisResultRangeColumn = griAnalysisResultRange.DisplayLayout.Bands[0].Columns.Add();
            analysisResultRangeColumn.Key = "RESULT_CODE_NAME";
            analysisResultRangeColumn.Header.Caption = "출력항목";
            analysisResultRangeColumn.CellActivation = Activation.NoEdit;
            analysisResultRangeColumn.CellClickAction = CellClickAction.RowSelect;
            analysisResultRangeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisResultRangeColumn.CellAppearance.TextHAlign = HAlign.Left;
            analysisResultRangeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisResultRangeColumn.Hidden = false;   //필드 보이기

            analysisResultRangeColumn = griAnalysisResultRange.DisplayLayout.Bands[0].Columns.Add();
            analysisResultRangeColumn.Key = "RANGE1";
            analysisResultRangeColumn.Header.Caption = "1st Value";
            analysisResultRangeColumn.CellActivation = Activation.NoEdit;
            analysisResultRangeColumn.CellClickAction = CellClickAction.RowSelect;
            analysisResultRangeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisResultRangeColumn.CellAppearance.TextHAlign = HAlign.Right;
            analysisResultRangeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisResultRangeColumn.Hidden = false;   //필드 보이기

            analysisResultRangeColumn = griAnalysisResultRange.DisplayLayout.Bands[0].Columns.Add();
            analysisResultRangeColumn.Key = "RANGE2";
            analysisResultRangeColumn.Header.Caption = "2nd Value";
            analysisResultRangeColumn.CellActivation = Activation.NoEdit;
            analysisResultRangeColumn.CellClickAction = CellClickAction.RowSelect;
            analysisResultRangeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisResultRangeColumn.CellAppearance.TextHAlign = HAlign.Right;
            analysisResultRangeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisResultRangeColumn.Hidden = false;   //필드 보이기


            analysisResultRangeColumn = griAnalysisResultRange.DisplayLayout.Bands[0].Columns.Add();
            analysisResultRangeColumn.Key = "RANGE3";
            analysisResultRangeColumn.Header.Caption = "3rd Value";
            analysisResultRangeColumn.CellActivation = Activation.NoEdit;
            analysisResultRangeColumn.CellClickAction = CellClickAction.RowSelect;
            analysisResultRangeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisResultRangeColumn.CellAppearance.TextHAlign = HAlign.Right;
            analysisResultRangeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisResultRangeColumn.Hidden = false;   //필드 보이기


            analysisResultRangeColumn = griAnalysisResultRange.DisplayLayout.Bands[0].Columns.Add();
            analysisResultRangeColumn.Key = "RANGE4";
            analysisResultRangeColumn.Header.Caption = "4th Value";
            analysisResultRangeColumn.CellActivation = Activation.NoEdit;
            analysisResultRangeColumn.CellClickAction = CellClickAction.RowSelect;
            analysisResultRangeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisResultRangeColumn.CellAppearance.TextHAlign = HAlign.Right;
            analysisResultRangeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisResultRangeColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griAnalysisResultRange);
        }

        //폼 최초 로딩 시 실행
        private void frmAnalysisResultRangeManage_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간관망해석ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.butSaveAnalysisResultRange.Enabled = false;
            }

            //============================================================================

            Hashtable conditions = new Hashtable();
            //conditions.Add("INP_NUMBER", parent.dummyInpNumber);
            conditions.Add("INP_NUMBER", dummyInpNumber);

            griAnalysisResultRange.DataSource = (work.SelectAnalysisResultRangeList(conditions)).Tables["WH_ANALYSIS_RESULT_RANGE"];
            FormManager.SetGridStyle_PerformAutoResize(griAnalysisResultRange);

            if (griAnalysisResultRange.Rows.Count > 0)
            {
                griAnalysisResultRange.Rows[0].Selected = true;
                griAnalysisResultRange.ActiveRowScrollRegion.ScrollPosition = 0;
            }
        }

        //Grid 최초행이 선택된 경우
        private void griAnalysisResultRange_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griAnalysisResultRange.Selected.Rows.Count != 0)
            {
                //DataRow row = ((DataRow)((DataTable)griAnalysisResultRange.DataSource).Rows[griAnalysisResultRange.Selected.Rows[0].Index]);

                texInpNumber.Text = Utils.nts(griAnalysisResultRange.Selected.Rows[0].GetCellValue("INP_NUMBER"));
                texLayerType.Text = Utils.nts(griAnalysisResultRange.Selected.Rows[0].GetCellValue("LAYER_TYPE"));
                texItem.Text = Utils.nts(griAnalysisResultRange.Selected.Rows[0].GetCellValue("RESULT_CODE_NAME"));
                texRange1.Text = Utils.nts(griAnalysisResultRange.Selected.Rows[0].GetCellValue("RANGE1"));
                texRange2.Text = Utils.nts(griAnalysisResultRange.Selected.Rows[0].GetCellValue("RANGE2"));
                texRange3.Text = Utils.nts(griAnalysisResultRange.Selected.Rows[0].GetCellValue("RANGE3"));
                texRange4.Text = Utils.nts(griAnalysisResultRange.Selected.Rows[0].GetCellValue("RANGE4"));
            }
        }

        //1st Value 변경
        private void texRange1_TextChanged(object sender, EventArgs e)
        {
            if (griAnalysisResultRange.Selected.Rows.Count != 0)
            {
                ((DataTable)griAnalysisResultRange.DataSource).Rows[griAnalysisResultRange.Selected.Rows[0].Index].SetField("RANGE1", texRange1.Text);
                griAnalysisResultRange.UpdateData();
            }
        }

        //2nd Value 변경
        private void texRange2_TextChanged(object sender, EventArgs e)
        {
            if (griAnalysisResultRange.Selected.Rows.Count != 0)
            {
                ((DataTable)griAnalysisResultRange.DataSource).Rows[griAnalysisResultRange.Selected.Rows[0].Index].SetField("RANGE2", texRange2.Text);
                griAnalysisResultRange.UpdateData();
            }
        }

        //3rd Value 변경
        private void texRange3_TextChanged(object sender, EventArgs e)
        {
            if (griAnalysisResultRange.Selected.Rows.Count != 0)
            {
                ((DataTable)griAnalysisResultRange.DataSource).Rows[griAnalysisResultRange.Selected.Rows[0].Index].SetField("RANGE3", texRange3.Text);
                griAnalysisResultRange.UpdateData();
            }
        }

        //4th Value 변경
        private void texRange4_TextChanged(object sender, EventArgs e)
        {
            if (griAnalysisResultRange.Selected.Rows.Count != 0)
            {
                ((DataTable)griAnalysisResultRange.DataSource).Rows[griAnalysisResultRange.Selected.Rows[0].Index].SetField("RANGE4", texRange4.Text);
                griAnalysisResultRange.UpdateData();
            }
        }

        //저장버튼 click 시
        private void butSaveAnalysisResultRange_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (griAnalysisResultRange.Selected.Rows.Count != 0)
                {
                    DataTable changedTable = ((DataTable)griAnalysisResultRange.DataSource).GetChanges();
                    Hashtable conditions = new Hashtable();

                    if (changedTable == null)
                    {
                        return;
                    }

                    //입력값 validation
                    foreach (DataRow row in changedTable.Rows)
                    {
                        if ("".Equals(row["RANGE1"]))
                        {
                            MessageBox.Show("1st Value가 입력되지 않은 행이 있습니다.");
                            return;
                        }

                        if ("".Equals(row["RANGE2"]))
                        {
                            MessageBox.Show("2nd Value가 입력되지 않은 행이 있습니다.");
                            return;
                        }

                        if ("".Equals(row["RANGE3"]))
                        {
                            MessageBox.Show("3rd Value가 입력되지 않은 행이 있습니다.");
                            return;
                        }

                        if ("".Equals(row["RANGE4"]))
                        {
                            MessageBox.Show("4th Value가 입력되지 않은 행이 있습니다.");
                            return;
                        }
                    }

                    try
                    {
                        conditions.Add("changedTable", changedTable);
                        //conditions.Add("INP_NUMBER",parent.dummyInpNumber);
                        conditions.Add("INP_NUMBER", dummyInpNumber);

                        DataSet result = work.SaveAnalysisResultRange(conditions);
                        griAnalysisResultRange.DataSource = result.Tables["WH_ANALYSIS_RESULT_RANGE"];

                        if (griAnalysisResultRange.Rows.Count > 0)
                        {
                            griAnalysisResultRange.Rows[0].Selected = true;
                            griAnalysisResultRange.ActiveRowScrollRegion.ScrollPosition = 0;
                        }

                        MessageBox.Show("정상적으로 처리되었습니다.");
                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show(e1.Message);
                        Console.WriteLine(e1.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //닫길때는 부모창의 초기화 함수 호출
        private void frmAnalysisResultRangeManage_FormClosed(object sender, FormClosedEventArgs e)
        {
            parentForm.InitializingData();
        }

    }
}
