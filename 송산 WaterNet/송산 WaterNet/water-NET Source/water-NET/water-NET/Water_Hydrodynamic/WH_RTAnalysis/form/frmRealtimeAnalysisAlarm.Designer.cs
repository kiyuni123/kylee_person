﻿namespace WaterNet.WH_RTAnalysis.form
{
    partial class frmRealtimeAnalysisAlarm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.griAnalysisWarningDetailList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.griAnalysisWarningList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.datStartWarDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.datEndWarDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.butSelectWarList = new System.Windows.Forms.Button();
            this.comSblock = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.griAnalysisErrorDetailList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.griAnalysisErrorList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.datStartErrDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.datEndErrDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.butSelectErrList = new System.Windows.Forms.Button();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.butApplyNodeFilter = new System.Windows.Forms.Button();
            this.cheErrorSaveYn = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.texLowPressure = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.tabWarningManage = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraTabPageControl1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisWarningDetailList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisWarningList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartWarDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndWarDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisErrorDetailList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisErrorList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartErrDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndErrDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabWarningManage)).BeginInit();
            this.tabWarningManage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.groupBox7);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox37);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox32);
            this.ultraTabPageControl1.Controls.Add(this.groupBox6);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox31);
            this.ultraTabPageControl1.Controls.Add(this.groupBox4);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox30);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox25);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox20);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(768, 504);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.griAnalysisWarningDetailList);
            this.groupBox7.Controls.Add(this.pictureBox26);
            this.groupBox7.Controls.Add(this.pictureBox27);
            this.groupBox7.Controls.Add(this.pictureBox28);
            this.groupBox7.Controls.Add(this.pictureBox29);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(10, 288);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(748, 207);
            this.groupBox7.TabIndex = 136;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "상세정보";
            // 
            // griAnalysisWarningDetailList
            // 
            this.griAnalysisWarningDetailList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griAnalysisWarningDetailList.Location = new System.Drawing.Point(13, 26);
            this.griAnalysisWarningDetailList.Name = "griAnalysisWarningDetailList";
            this.griAnalysisWarningDetailList.Size = new System.Drawing.Size(722, 169);
            this.griAnalysisWarningDetailList.TabIndex = 134;
            this.griAnalysisWarningDetailList.Text = "ultraGrid1";
            this.griAnalysisWarningDetailList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griAnalysisWarningDetailList_DoubleClickRow);
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox26.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox26.Location = new System.Drawing.Point(735, 26);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(10, 169);
            this.pictureBox26.TabIndex = 133;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox27.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox27.Location = new System.Drawing.Point(3, 26);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(10, 169);
            this.pictureBox27.TabIndex = 132;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox28.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox28.Location = new System.Drawing.Point(3, 195);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(742, 9);
            this.pictureBox28.TabIndex = 131;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox29.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox29.Location = new System.Drawing.Point(3, 17);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(742, 9);
            this.pictureBox29.TabIndex = 130;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox37.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox37.Location = new System.Drawing.Point(10, 495);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(748, 9);
            this.pictureBox37.TabIndex = 141;
            this.pictureBox37.TabStop = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox32.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox32.Location = new System.Drawing.Point(10, 279);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(748, 9);
            this.pictureBox32.TabIndex = 140;
            this.pictureBox32.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.griAnalysisWarningList);
            this.groupBox6.Controls.Add(this.pictureBox21);
            this.groupBox6.Controls.Add(this.pictureBox22);
            this.groupBox6.Controls.Add(this.pictureBox23);
            this.groupBox6.Controls.Add(this.pictureBox24);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(10, 74);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(748, 205);
            this.groupBox6.TabIndex = 134;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "경보발생내역";
            // 
            // griAnalysisWarningList
            // 
            this.griAnalysisWarningList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griAnalysisWarningList.Location = new System.Drawing.Point(13, 26);
            this.griAnalysisWarningList.Name = "griAnalysisWarningList";
            this.griAnalysisWarningList.Size = new System.Drawing.Size(722, 167);
            this.griAnalysisWarningList.TabIndex = 133;
            this.griAnalysisWarningList.Text = "ultraGrid1";
            this.griAnalysisWarningList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griAnalysisWarningList_AfterSelectChange);
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox21.Location = new System.Drawing.Point(735, 26);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(10, 167);
            this.pictureBox21.TabIndex = 132;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox22.Location = new System.Drawing.Point(3, 26);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(10, 167);
            this.pictureBox22.TabIndex = 131;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox23.Location = new System.Drawing.Point(3, 193);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(742, 9);
            this.pictureBox23.TabIndex = 130;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox24.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox24.Location = new System.Drawing.Point(3, 17);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(742, 9);
            this.pictureBox24.TabIndex = 129;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox31.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox31.Location = new System.Drawing.Point(10, 65);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(748, 9);
            this.pictureBox31.TabIndex = 139;
            this.pictureBox31.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel3);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(10, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(748, 56);
            this.groupBox4.TabIndex = 121;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "검색기간";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 8;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.datStartWarDate, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.datEndWarDate, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.butSelectWarList, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.comSblock, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label9, 3, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 17);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(742, 30);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F);
            this.label5.Location = new System.Drawing.Point(38, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 26;
            this.label5.Text = "조회기간";
            // 
            // datStartWarDate
            // 
            this.datStartWarDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datStartWarDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datStartWarDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datStartWarDate.Location = new System.Drawing.Point(97, 5);
            this.datStartWarDate.Name = "datStartWarDate";
            this.datStartWarDate.Size = new System.Drawing.Size(94, 19);
            this.datStartWarDate.TabIndex = 29;
            this.datStartWarDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // datEndWarDate
            // 
            this.datEndWarDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datEndWarDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datEndWarDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datEndWarDate.Location = new System.Drawing.Point(203, 5);
            this.datEndWarDate.Name = "datEndWarDate";
            this.datEndWarDate.Size = new System.Drawing.Size(94, 19);
            this.datEndWarDate.TabIndex = 30;
            this.datEndWarDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // butSelectWarList
            // 
            this.butSelectWarList.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butSelectWarList.Font = new System.Drawing.Font("굴림", 9F);
            this.butSelectWarList.Location = new System.Drawing.Point(694, 4);
            this.butSelectWarList.Name = "butSelectWarList";
            this.butSelectWarList.Size = new System.Drawing.Size(43, 22);
            this.butSelectWarList.TabIndex = 41;
            this.butSelectWarList.Text = "조회";
            this.butSelectWarList.UseVisualStyleBackColor = true;
            this.butSelectWarList.Click += new System.EventHandler(this.butSelectWarList_Click);
            // 
            // comSblock
            // 
            this.comSblock.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comSblock.Font = new System.Drawing.Font("굴림", 9F);
            this.comSblock.FormattingEnabled = true;
            this.comSblock.Location = new System.Drawing.Point(421, 5);
            this.comSblock.Name = "comSblock";
            this.comSblock.Size = new System.Drawing.Size(100, 20);
            this.comSblock.TabIndex = 42;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F);
            this.label9.Location = new System.Drawing.Point(386, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 43;
            this.label9.Text = "지역";
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox30.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox30.Location = new System.Drawing.Point(758, 9);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(10, 495);
            this.pictureBox30.TabIndex = 138;
            this.pictureBox30.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox25.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox25.Location = new System.Drawing.Point(0, 9);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(10, 495);
            this.pictureBox25.TabIndex = 137;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox20.Location = new System.Drawing.Point(0, 0);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(768, 9);
            this.pictureBox20.TabIndex = 133;
            this.pictureBox20.TabStop = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.groupBox5);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox36);
            this.ultraTabPageControl2.Controls.Add(this.groupBox2);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox35);
            this.ultraTabPageControl2.Controls.Add(this.groupBox1);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox34);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox33);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox11);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox2);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(768, 504);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.griAnalysisErrorDetailList);
            this.groupBox5.Controls.Add(this.pictureBox19);
            this.groupBox5.Controls.Add(this.pictureBox18);
            this.groupBox5.Controls.Add(this.pictureBox17);
            this.groupBox5.Controls.Add(this.pictureBox16);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(10, 288);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(748, 207);
            this.groupBox5.TabIndex = 132;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "상세정보";
            // 
            // griAnalysisErrorDetailList
            // 
            this.griAnalysisErrorDetailList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griAnalysisErrorDetailList.Location = new System.Drawing.Point(13, 26);
            this.griAnalysisErrorDetailList.Name = "griAnalysisErrorDetailList";
            this.griAnalysisErrorDetailList.Size = new System.Drawing.Size(722, 169);
            this.griAnalysisErrorDetailList.TabIndex = 134;
            this.griAnalysisErrorDetailList.Text = "ultraGrid1";
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox19.Location = new System.Drawing.Point(735, 26);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(10, 169);
            this.pictureBox19.TabIndex = 133;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox18.Location = new System.Drawing.Point(3, 26);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(10, 169);
            this.pictureBox18.TabIndex = 132;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox17.Location = new System.Drawing.Point(3, 195);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(742, 9);
            this.pictureBox17.TabIndex = 131;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox16.Location = new System.Drawing.Point(3, 17);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(742, 9);
            this.pictureBox16.TabIndex = 130;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox36.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox36.Location = new System.Drawing.Point(10, 279);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(748, 9);
            this.pictureBox36.TabIndex = 138;
            this.pictureBox36.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.griAnalysisErrorList);
            this.groupBox2.Controls.Add(this.pictureBox15);
            this.groupBox2.Controls.Add(this.pictureBox14);
            this.groupBox2.Controls.Add(this.pictureBox13);
            this.groupBox2.Controls.Add(this.pictureBox12);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(10, 74);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(748, 205);
            this.groupBox2.TabIndex = 130;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "해석오류발생내역";
            // 
            // griAnalysisErrorList
            // 
            this.griAnalysisErrorList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griAnalysisErrorList.Location = new System.Drawing.Point(13, 26);
            this.griAnalysisErrorList.Name = "griAnalysisErrorList";
            this.griAnalysisErrorList.Size = new System.Drawing.Size(722, 167);
            this.griAnalysisErrorList.TabIndex = 133;
            this.griAnalysisErrorList.Text = "ultraGrid1";
            this.griAnalysisErrorList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griAnalysisErrorList_AfterSelectChange);
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox15.Location = new System.Drawing.Point(735, 26);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(10, 167);
            this.pictureBox15.TabIndex = 132;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox14.Location = new System.Drawing.Point(3, 26);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(10, 167);
            this.pictureBox14.TabIndex = 131;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox13.Location = new System.Drawing.Point(3, 193);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(742, 9);
            this.pictureBox13.TabIndex = 130;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox12.Location = new System.Drawing.Point(3, 17);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(742, 9);
            this.pictureBox12.TabIndex = 129;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox35.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox35.Location = new System.Drawing.Point(10, 65);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(748, 9);
            this.pictureBox35.TabIndex = 137;
            this.pictureBox35.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(748, 56);
            this.groupBox1.TabIndex = 121;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색기간";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.datStartErrDate, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.datEndErrDate, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.butSelectErrList, 6, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 17);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(742, 30);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F);
            this.label3.Location = new System.Drawing.Point(38, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 26;
            this.label3.Text = "조회기간";
            // 
            // datStartErrDate
            // 
            this.datStartErrDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datStartErrDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datStartErrDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datStartErrDate.Location = new System.Drawing.Point(97, 5);
            this.datStartErrDate.Name = "datStartErrDate";
            this.datStartErrDate.Size = new System.Drawing.Size(94, 19);
            this.datStartErrDate.TabIndex = 29;
            this.datStartErrDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // datEndErrDate
            // 
            this.datEndErrDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datEndErrDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datEndErrDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datEndErrDate.Location = new System.Drawing.Point(203, 5);
            this.datEndErrDate.Name = "datEndErrDate";
            this.datEndErrDate.Size = new System.Drawing.Size(94, 19);
            this.datEndErrDate.TabIndex = 30;
            this.datEndErrDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // butSelectErrList
            // 
            this.butSelectErrList.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butSelectErrList.Font = new System.Drawing.Font("굴림", 9F);
            this.butSelectErrList.Location = new System.Drawing.Point(694, 4);
            this.butSelectErrList.Name = "butSelectErrList";
            this.butSelectErrList.Size = new System.Drawing.Size(43, 22);
            this.butSelectErrList.TabIndex = 41;
            this.butSelectErrList.Text = "조회";
            this.butSelectErrList.UseVisualStyleBackColor = true;
            this.butSelectErrList.Click += new System.EventHandler(this.butSelectErrList_Click);
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox34.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox34.Location = new System.Drawing.Point(758, 9);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(10, 486);
            this.pictureBox34.TabIndex = 136;
            this.pictureBox34.TabStop = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox33.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox33.Location = new System.Drawing.Point(0, 9);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(10, 486);
            this.pictureBox33.TabIndex = 135;
            this.pictureBox33.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox11.Location = new System.Drawing.Point(0, 495);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(768, 9);
            this.pictureBox11.TabIndex = 134;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(768, 9);
            this.pictureBox2.TabIndex = 133;
            this.pictureBox2.TabStop = false;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.groupBox3);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox10);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox9);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox6);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox5);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(768, 504);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.butApplyNodeFilter);
            this.groupBox3.Controls.Add(this.cheErrorSaveYn);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.texLowPressure);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(10, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(748, 486);
            this.groupBox3.TabIndex = 121;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "경고설정";
            // 
            // butApplyNodeFilter
            // 
            this.butApplyNodeFilter.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butApplyNodeFilter.Font = new System.Drawing.Font("굴림", 9F);
            this.butApplyNodeFilter.Location = new System.Drawing.Point(42, 324);
            this.butApplyNodeFilter.Name = "butApplyNodeFilter";
            this.butApplyNodeFilter.Size = new System.Drawing.Size(43, 22);
            this.butApplyNodeFilter.TabIndex = 40;
            this.butApplyNodeFilter.Text = "저장";
            this.butApplyNodeFilter.UseVisualStyleBackColor = true;
            this.butApplyNodeFilter.Click += new System.EventHandler(this.butApplyNodeFilter_Click);
            // 
            // cheErrorSaveYn
            // 
            this.cheErrorSaveYn.AutoSize = true;
            this.cheErrorSaveYn.Location = new System.Drawing.Point(181, 65);
            this.cheErrorSaveYn.Name = "cheErrorSaveYn";
            this.cheErrorSaveYn.Size = new System.Drawing.Size(15, 14);
            this.cheErrorSaveYn.TabIndex = 37;
            this.cheErrorSaveYn.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(40, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 12);
            this.label8.TabIndex = 36;
            this.label8.Text = "1. 경보발생 시 경보저장";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 12);
            this.label7.TabIndex = 35;
            this.label7.Text = "관망해석 설정";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 243);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 12);
            this.label6.TabIndex = 34;
            this.label6.Text = "관망해석결과 경보설정";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(217, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 12);
            this.label4.TabIndex = 31;
            this.label4.Text = "이하일 경우";
            // 
            // texLowPressure
            // 
            this.texLowPressure.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texLowPressure.Font = new System.Drawing.Font("굴림", 9F);
            this.texLowPressure.Location = new System.Drawing.Point(161, 266);
            this.texLowPressure.Name = "texLowPressure";
            this.texLowPressure.Size = new System.Drawing.Size(50, 21);
            this.texLowPressure.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 271);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "1. 출수불량 : 압력이";
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox10.Location = new System.Drawing.Point(758, 9);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(10, 486);
            this.pictureBox10.TabIndex = 120;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox9.Location = new System.Drawing.Point(0, 9);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(10, 486);
            this.pictureBox9.TabIndex = 119;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox6.Location = new System.Drawing.Point(0, 495);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(768, 9);
            this.pictureBox6.TabIndex = 118;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(768, 9);
            this.pictureBox5.TabIndex = 117;
            this.pictureBox5.TabStop = false;
            // 
            // tabWarningManage
            // 
            this.tabWarningManage.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabWarningManage.Controls.Add(this.ultraTabPageControl1);
            this.tabWarningManage.Controls.Add(this.ultraTabPageControl2);
            this.tabWarningManage.Controls.Add(this.ultraTabPageControl3);
            this.tabWarningManage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabWarningManage.Location = new System.Drawing.Point(10, 18);
            this.tabWarningManage.Name = "tabWarningManage";
            this.tabWarningManage.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabWarningManage.Size = new System.Drawing.Size(772, 530);
            this.tabWarningManage.TabIndex = 133;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "경보확인";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "관망해석실행오류";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "경보설정";
            this.tabWarningManage.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(768, 504);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 9);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(772, 9);
            this.pictureBox4.TabIndex = 132;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(782, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 539);
            this.pictureBox3.TabIndex = 131;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox7.Location = new System.Drawing.Point(10, 548);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(782, 9);
            this.pictureBox7.TabIndex = 130;
            this.pictureBox7.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 548);
            this.picFrLeftM1.TabIndex = 129;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 557);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(792, 9);
            this.pictureBox1.TabIndex = 127;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox8.Location = new System.Drawing.Point(0, 0);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(792, 9);
            this.pictureBox8.TabIndex = 128;
            this.pictureBox8.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 8;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 184F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.Location = new System.Drawing.Point(38, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 26;
            this.label1.Text = "조회기간";
            // 
            // ultraDateTimeEditor1
            // 
            this.ultraDateTimeEditor1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ultraDateTimeEditor1.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.ultraDateTimeEditor1.Font = new System.Drawing.Font("굴림", 8F);
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(97, 5);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(94, 19);
            this.ultraDateTimeEditor1.TabIndex = 29;
            this.ultraDateTimeEditor1.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // frmRealtimeAnalysisAlarm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 566);
            this.Controls.Add(this.tabWarningManage);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox8);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRealtimeAnalysisAlarm";
            this.Text = "관망해석 경보관리";
            this.Load += new System.EventHandler(this.frmRealtimeAnalysisAlarm_Load);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisWarningDetailList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisWarningList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartWarDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndWarDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisErrorDetailList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisErrorList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datStartErrDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndErrDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabWarningManage)).EndInit();
            this.tabWarningManage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button butApplyNodeFilter;
        private System.Windows.Forms.CheckBox cheErrorSaveYn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox texLowPressure;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabWarningManage;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.GroupBox groupBox4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datStartErrDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datEndErrDate;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        public Infragistics.Win.UltraWinGrid.UltraGrid griAnalysisErrorList;
        public Infragistics.Win.UltraWinGrid.UltraGrid griAnalysisErrorDetailList;
        private System.Windows.Forms.Button butSelectErrList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label5;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datStartWarDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datEndWarDate;
        private System.Windows.Forms.Button butSelectWarList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.GroupBox groupBox7;
        public Infragistics.Win.UltraWinGrid.UltraGrid griAnalysisWarningDetailList;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.GroupBox groupBox6;
        public Infragistics.Win.UltraWinGrid.UltraGrid griAnalysisWarningList;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.ComboBox comSblock;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox37;
    }
}