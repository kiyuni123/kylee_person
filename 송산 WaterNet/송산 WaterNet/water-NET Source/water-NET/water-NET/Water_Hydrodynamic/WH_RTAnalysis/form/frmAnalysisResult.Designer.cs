﻿namespace WaterNet.WH_RTAnalysis.form
{
    partial class frmAnalysisResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ChartFX.WinForms.SeriesAttributes seriesAttributes1 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes2 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes3 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes4 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes5 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes6 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes7 = new ChartFX.WinForms.SeriesAttributes();
            ChartFX.WinForms.SeriesAttributes seriesAttributes8 = new ChartFX.WinForms.SeriesAttributes();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.chaNodeResult = new ChartFX.WinForms.Chart();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.griNodeResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.datNodeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label15 = new System.Windows.Forms.Label();
            this.comNodeSBlock = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comNodeItem = new System.Windows.Forms.ComboBox();
            this.texNodeValue = new System.Windows.Forms.TextBox();
            this.comNodeFormular = new System.Windows.Forms.ComboBox();
            this.butApplyNodeFilter = new System.Windows.Forms.Button();
            this.butApplyNodeExcel = new System.Windows.Forms.Button();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.chaLinkResult = new ChartFX.WinForms.Chart();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.griLinkResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.texLinkValue = new System.Windows.Forms.TextBox();
            this.butApplyLinkFilter = new System.Windows.Forms.Button();
            this.comLinkFormular = new System.Windows.Forms.ComboBox();
            this.comLinkItem = new System.Windows.Forms.ComboBox();
            this.comLinkSBlock = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.datLinkDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.butApplyLinkExcel = new System.Windows.Forms.Button();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.chaRealtimeCheck = new ChartFX.WinForms.Chart();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.griRealtimeCheck = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.datMeterDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label4 = new System.Windows.Forms.Label();
            this.butGetCheckpointData = new System.Windows.Forms.Button();
            this.comTagname = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comMeterType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.butGetCheckpointExcel = new System.Windows.Forms.Button();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butCompareResult = new System.Windows.Forms.Button();
            this.butAddMeasureBulk = new System.Windows.Forms.Button();
            this.butSaveMeasureBulk = new System.Windows.Forms.Button();
            this.butDeleteMeasureBulk = new System.Windows.Forms.Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.butFindFile = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.texDetailDumpNumber = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.texDetailLocName = new System.Windows.Forms.TextBox();
            this.texDetailDumpDate = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comDetailDumpType = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.texDetailId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.texDetailComment = new System.Windows.Forms.TextBox();
            this.texDumpFilePath = new System.Windows.Forms.TextBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.griMeasureDump = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.datDumpEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.datDumpStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.comDumpSBlock = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comDumpType = new System.Windows.Forms.ComboBox();
            this.butSelectDumpData = new System.Windows.Forms.Button();
            this.butSelectDumpExcel = new System.Windows.Forms.Button();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ultraTabControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.tabAnalysisResult = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaNodeResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griNodeResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datNodeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaLinkResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griLinkResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datLinkDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaRealtimeCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datMeterDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griMeasureDump)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datDumpEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDumpStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabAnalysisResult)).BeginInit();
            this.tabAnalysisResult.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.chaNodeResult);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox25);
            this.ultraTabPageControl1.Controls.Add(this.griNodeResult);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox9);
            this.ultraTabPageControl1.Controls.Add(this.tableLayoutPanel2);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox8);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox7);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox6);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox5);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(2, 24);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(821, 513);
            // 
            // chaNodeResult
            // 
            this.chaNodeResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chaNodeResult.Location = new System.Drawing.Point(10, 257);
            this.chaNodeResult.Name = "chaNodeResult";
            seriesAttributes1.Visible = false;
            seriesAttributes2.Visible = false;
            seriesAttributes3.Visible = false;
            this.chaNodeResult.Series.AddRange(new ChartFX.WinForms.SeriesAttributes[] {
            seriesAttributes1,
            seriesAttributes2,
            seriesAttributes3});
            this.chaNodeResult.Size = new System.Drawing.Size(801, 247);
            this.chaNodeResult.TabIndex = 126;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox25.Location = new System.Drawing.Point(10, 248);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(801, 9);
            this.pictureBox25.TabIndex = 127;
            this.pictureBox25.TabStop = false;
            // 
            // griNodeResult
            // 
            this.griNodeResult.Dock = System.Windows.Forms.DockStyle.Top;
            this.griNodeResult.Location = new System.Drawing.Point(10, 48);
            this.griNodeResult.Name = "griNodeResult";
            this.griNodeResult.Size = new System.Drawing.Size(801, 200);
            this.griNodeResult.TabIndex = 121;
            this.griNodeResult.Text = "ultraGrid1";
            this.griNodeResult.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griNodeResult_DoubleClickRow);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox9.Location = new System.Drawing.Point(10, 39);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(801, 9);
            this.pictureBox9.TabIndex = 120;
            this.pictureBox9.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 10;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.label16, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.datNodeDate, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.comNodeSBlock, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.comNodeItem, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.texNodeValue, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.comNodeFormular, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.butApplyNodeFilter, 8, 0);
            this.tableLayoutPanel2.Controls.Add(this.butApplyNodeExcel, 9, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(10, 9);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(801, 30);
            this.tableLayoutPanel2.TabIndex = 119;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F);
            this.label16.Location = new System.Drawing.Point(162, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 33;
            this.label16.Text = "지역";
            // 
            // datNodeDate
            // 
            this.datNodeDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datNodeDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datNodeDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datNodeDate.Location = new System.Drawing.Point(44, 5);
            this.datNodeDate.Name = "datNodeDate";
            this.datNodeDate.Size = new System.Drawing.Size(94, 19);
            this.datNodeDate.TabIndex = 31;
            this.datNodeDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F);
            this.label15.Location = new System.Drawing.Point(9, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 30;
            this.label15.Text = "일자";
            // 
            // comNodeSBlock
            // 
            this.comNodeSBlock.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comNodeSBlock.Font = new System.Drawing.Font("굴림", 9F);
            this.comNodeSBlock.FormattingEnabled = true;
            this.comNodeSBlock.Location = new System.Drawing.Point(197, 5);
            this.comNodeSBlock.Name = "comNodeSBlock";
            this.comNodeSBlock.Size = new System.Drawing.Size(100, 20);
            this.comNodeSBlock.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F);
            this.label5.Location = new System.Drawing.Point(307, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "결과 필터링";
            // 
            // comNodeItem
            // 
            this.comNodeItem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comNodeItem.Font = new System.Drawing.Font("굴림", 9F);
            this.comNodeItem.FormattingEnabled = true;
            this.comNodeItem.Location = new System.Drawing.Point(382, 5);
            this.comNodeItem.Name = "comNodeItem";
            this.comNodeItem.Size = new System.Drawing.Size(100, 20);
            this.comNodeItem.TabIndex = 23;
            // 
            // texNodeValue
            // 
            this.texNodeValue.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texNodeValue.Font = new System.Drawing.Font("굴림", 9F);
            this.texNodeValue.Location = new System.Drawing.Point(491, 4);
            this.texNodeValue.Name = "texNodeValue";
            this.texNodeValue.Size = new System.Drawing.Size(100, 21);
            this.texNodeValue.TabIndex = 29;
            // 
            // comNodeFormular
            // 
            this.comNodeFormular.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comNodeFormular.Font = new System.Drawing.Font("굴림", 9F);
            this.comNodeFormular.FormattingEnabled = true;
            this.comNodeFormular.Location = new System.Drawing.Point(599, 5);
            this.comNodeFormular.Name = "comNodeFormular";
            this.comNodeFormular.Size = new System.Drawing.Size(73, 20);
            this.comNodeFormular.TabIndex = 25;
            // 
            // butApplyNodeFilter
            // 
            this.butApplyNodeFilter.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butApplyNodeFilter.Font = new System.Drawing.Font("굴림", 9F);
            this.butApplyNodeFilter.Location = new System.Drawing.Point(702, 4);
            this.butApplyNodeFilter.Name = "butApplyNodeFilter";
            this.butApplyNodeFilter.Size = new System.Drawing.Size(43, 22);
            this.butApplyNodeFilter.TabIndex = 28;
            this.butApplyNodeFilter.Text = "조회";
            this.butApplyNodeFilter.UseVisualStyleBackColor = true;
            this.butApplyNodeFilter.Click += new System.EventHandler(this.butApplyNodeFilter_Click);
            // 
            // butApplyNodeExcel
            // 
            this.butApplyNodeExcel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butApplyNodeExcel.Font = new System.Drawing.Font("굴림", 9F);
            this.butApplyNodeExcel.Location = new System.Drawing.Point(755, 4);
            this.butApplyNodeExcel.Name = "butApplyNodeExcel";
            this.butApplyNodeExcel.Size = new System.Drawing.Size(43, 22);
            this.butApplyNodeExcel.TabIndex = 35;
            this.butApplyNodeExcel.Text = "엑셀";
            this.butApplyNodeExcel.UseVisualStyleBackColor = true;
            this.butApplyNodeExcel.Click += new System.EventHandler(this.butApplyNodeExcel_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox8.Location = new System.Drawing.Point(811, 9);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(10, 495);
            this.pictureBox8.TabIndex = 118;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox7.Location = new System.Drawing.Point(0, 9);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 495);
            this.pictureBox7.TabIndex = 117;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox6.Location = new System.Drawing.Point(0, 504);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(821, 9);
            this.pictureBox6.TabIndex = 116;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(821, 9);
            this.pictureBox5.TabIndex = 115;
            this.pictureBox5.TabStop = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.chaLinkResult);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox26);
            this.ultraTabPageControl2.Controls.Add(this.griLinkResult);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox14);
            this.ultraTabPageControl2.Controls.Add(this.tableLayoutPanel8);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox13);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox12);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox11);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox10);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(821, 513);
            // 
            // chaLinkResult
            // 
            this.chaLinkResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chaLinkResult.Location = new System.Drawing.Point(10, 257);
            this.chaLinkResult.Name = "chaLinkResult";
            this.chaLinkResult.RandomData.Series = 2;
            seriesAttributes4.Visible = false;
            seriesAttributes5.Visible = false;
            this.chaLinkResult.Series.AddRange(new ChartFX.WinForms.SeriesAttributes[] {
            seriesAttributes4,
            seriesAttributes5});
            this.chaLinkResult.Size = new System.Drawing.Size(801, 247);
            this.chaLinkResult.TabIndex = 127;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox26.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox26.Location = new System.Drawing.Point(10, 248);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(801, 9);
            this.pictureBox26.TabIndex = 128;
            this.pictureBox26.TabStop = false;
            // 
            // griLinkResult
            // 
            this.griLinkResult.Dock = System.Windows.Forms.DockStyle.Top;
            this.griLinkResult.Location = new System.Drawing.Point(10, 48);
            this.griLinkResult.Name = "griLinkResult";
            this.griLinkResult.Size = new System.Drawing.Size(801, 200);
            this.griLinkResult.TabIndex = 122;
            this.griLinkResult.Text = "ultraGrid1";
            this.griLinkResult.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griLinkResult_DoubleClickRow);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox14.Location = new System.Drawing.Point(10, 39);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(801, 9);
            this.pictureBox14.TabIndex = 121;
            this.pictureBox14.TabStop = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 10;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Controls.Add(this.texLinkValue, 6, 0);
            this.tableLayoutPanel8.Controls.Add(this.butApplyLinkFilter, 8, 0);
            this.tableLayoutPanel8.Controls.Add(this.comLinkFormular, 7, 0);
            this.tableLayoutPanel8.Controls.Add(this.comLinkItem, 5, 0);
            this.tableLayoutPanel8.Controls.Add(this.comLinkSBlock, 3, 0);
            this.tableLayoutPanel8.Controls.Add(this.label19, 4, 0);
            this.tableLayoutPanel8.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label17, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.datLinkDate, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.butApplyLinkExcel, 9, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(10, 9);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(801, 30);
            this.tableLayoutPanel8.TabIndex = 129;
            // 
            // texLinkValue
            // 
            this.texLinkValue.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texLinkValue.Font = new System.Drawing.Font("굴림", 9F);
            this.texLinkValue.Location = new System.Drawing.Point(491, 4);
            this.texLinkValue.Name = "texLinkValue";
            this.texLinkValue.Size = new System.Drawing.Size(100, 21);
            this.texLinkValue.TabIndex = 29;
            // 
            // butApplyLinkFilter
            // 
            this.butApplyLinkFilter.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butApplyLinkFilter.Font = new System.Drawing.Font("굴림", 9F);
            this.butApplyLinkFilter.Location = new System.Drawing.Point(702, 4);
            this.butApplyLinkFilter.Name = "butApplyLinkFilter";
            this.butApplyLinkFilter.Size = new System.Drawing.Size(43, 22);
            this.butApplyLinkFilter.TabIndex = 28;
            this.butApplyLinkFilter.Text = "조회";
            this.butApplyLinkFilter.UseVisualStyleBackColor = true;
            this.butApplyLinkFilter.Click += new System.EventHandler(this.butApplyLinkFilter_Click);
            // 
            // comLinkFormular
            // 
            this.comLinkFormular.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comLinkFormular.Font = new System.Drawing.Font("굴림", 9F);
            this.comLinkFormular.FormattingEnabled = true;
            this.comLinkFormular.Location = new System.Drawing.Point(599, 5);
            this.comLinkFormular.Name = "comLinkFormular";
            this.comLinkFormular.Size = new System.Drawing.Size(73, 20);
            this.comLinkFormular.TabIndex = 25;
            // 
            // comLinkItem
            // 
            this.comLinkItem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comLinkItem.Font = new System.Drawing.Font("굴림", 9F);
            this.comLinkItem.FormattingEnabled = true;
            this.comLinkItem.Location = new System.Drawing.Point(382, 5);
            this.comLinkItem.Name = "comLinkItem";
            this.comLinkItem.Size = new System.Drawing.Size(100, 20);
            this.comLinkItem.TabIndex = 23;
            // 
            // comLinkSBlock
            // 
            this.comLinkSBlock.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comLinkSBlock.Font = new System.Drawing.Font("굴림", 9F);
            this.comLinkSBlock.FormattingEnabled = true;
            this.comLinkSBlock.Location = new System.Drawing.Point(197, 5);
            this.comLinkSBlock.Name = "comLinkSBlock";
            this.comLinkSBlock.Size = new System.Drawing.Size(100, 20);
            this.comLinkSBlock.TabIndex = 34;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 9F);
            this.label19.Location = new System.Drawing.Point(307, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 12);
            this.label19.TabIndex = 3;
            this.label19.Text = "결과 필터링";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 9F);
            this.label18.Location = new System.Drawing.Point(9, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 12);
            this.label18.TabIndex = 30;
            this.label18.Text = "일자";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9F);
            this.label17.Location = new System.Drawing.Point(162, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 33;
            this.label17.Text = "지역";
            // 
            // datLinkDate
            // 
            this.datLinkDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datLinkDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datLinkDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datLinkDate.Location = new System.Drawing.Point(44, 5);
            this.datLinkDate.Name = "datLinkDate";
            this.datLinkDate.Size = new System.Drawing.Size(94, 19);
            this.datLinkDate.TabIndex = 31;
            this.datLinkDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // butApplyLinkExcel
            // 
            this.butApplyLinkExcel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butApplyLinkExcel.Font = new System.Drawing.Font("굴림", 9F);
            this.butApplyLinkExcel.Location = new System.Drawing.Point(755, 4);
            this.butApplyLinkExcel.Name = "butApplyLinkExcel";
            this.butApplyLinkExcel.Size = new System.Drawing.Size(43, 22);
            this.butApplyLinkExcel.TabIndex = 35;
            this.butApplyLinkExcel.Text = "엑셀";
            this.butApplyLinkExcel.UseVisualStyleBackColor = true;
            this.butApplyLinkExcel.Click += new System.EventHandler(this.butApplyLinkExcel_Click);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox13.Location = new System.Drawing.Point(811, 9);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(10, 495);
            this.pictureBox13.TabIndex = 119;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox12.Location = new System.Drawing.Point(0, 9);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(10, 495);
            this.pictureBox12.TabIndex = 118;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox11.Location = new System.Drawing.Point(0, 504);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(821, 9);
            this.pictureBox11.TabIndex = 117;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox10.Location = new System.Drawing.Point(0, 0);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(821, 9);
            this.pictureBox10.TabIndex = 116;
            this.pictureBox10.TabStop = false;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.chaRealtimeCheck);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox27);
            this.ultraTabPageControl3.Controls.Add(this.griRealtimeCheck);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox19);
            this.ultraTabPageControl3.Controls.Add(this.tableLayoutPanel5);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox18);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox17);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox16);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox15);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(821, 513);
            // 
            // chaRealtimeCheck
            // 
            this.chaRealtimeCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chaRealtimeCheck.Location = new System.Drawing.Point(10, 257);
            this.chaRealtimeCheck.Name = "chaRealtimeCheck";
            seriesAttributes6.Visible = false;
            seriesAttributes7.Visible = false;
            seriesAttributes8.Visible = false;
            this.chaRealtimeCheck.Series.AddRange(new ChartFX.WinForms.SeriesAttributes[] {
            seriesAttributes6,
            seriesAttributes7,
            seriesAttributes8});
            this.chaRealtimeCheck.Size = new System.Drawing.Size(801, 247);
            this.chaRealtimeCheck.TabIndex = 125;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox27.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox27.Location = new System.Drawing.Point(10, 248);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(801, 9);
            this.pictureBox27.TabIndex = 126;
            this.pictureBox27.TabStop = false;
            // 
            // griRealtimeCheck
            // 
            this.griRealtimeCheck.Dock = System.Windows.Forms.DockStyle.Top;
            this.griRealtimeCheck.Location = new System.Drawing.Point(10, 48);
            this.griRealtimeCheck.Name = "griRealtimeCheck";
            this.griRealtimeCheck.Size = new System.Drawing.Size(801, 200);
            this.griRealtimeCheck.TabIndex = 123;
            this.griRealtimeCheck.Text = "ultraGrid1";
            this.griRealtimeCheck.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griRealtimeCheck_DoubleClickRow);
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox19.Location = new System.Drawing.Point(10, 39);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(801, 9);
            this.pictureBox19.TabIndex = 122;
            this.pictureBox19.TabStop = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 8;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 299F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel5.Controls.Add(this.datMeterDate, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.butGetCheckpointData, 6, 0);
            this.tableLayoutPanel5.Controls.Add(this.comTagname, 5, 0);
            this.tableLayoutPanel5.Controls.Add(this.label7, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.comMeterType, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.label6, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.butGetCheckpointExcel, 7, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(10, 9);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(801, 30);
            this.tableLayoutPanel5.TabIndex = 121;
            // 
            // datMeterDate
            // 
            this.datMeterDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datMeterDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datMeterDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datMeterDate.Location = new System.Drawing.Point(44, 5);
            this.datMeterDate.Name = "datMeterDate";
            this.datMeterDate.Size = new System.Drawing.Size(94, 19);
            this.datMeterDate.TabIndex = 32;
            this.datMeterDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F);
            this.label4.Location = new System.Drawing.Point(9, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 31;
            this.label4.Text = "일자";
            // 
            // butGetCheckpointData
            // 
            this.butGetCheckpointData.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butGetCheckpointData.Font = new System.Drawing.Font("굴림", 9F);
            this.butGetCheckpointData.Location = new System.Drawing.Point(702, 4);
            this.butGetCheckpointData.Name = "butGetCheckpointData";
            this.butGetCheckpointData.Size = new System.Drawing.Size(43, 22);
            this.butGetCheckpointData.TabIndex = 28;
            this.butGetCheckpointData.Text = "조회";
            this.butGetCheckpointData.UseVisualStyleBackColor = true;
            this.butGetCheckpointData.Click += new System.EventHandler(this.butGetCheckpointData_Click);
            // 
            // comTagname
            // 
            this.comTagname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comTagname.Font = new System.Drawing.Font("굴림", 9F);
            this.comTagname.FormattingEnabled = true;
            this.comTagname.Location = new System.Drawing.Point(390, 5);
            this.comTagname.Name = "comTagname";
            this.comTagname.Size = new System.Drawing.Size(250, 20);
            this.comTagname.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F);
            this.label7.Location = new System.Drawing.Point(331, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "실측지점";
            // 
            // comMeterType
            // 
            this.comMeterType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comMeterType.Font = new System.Drawing.Font("굴림", 9F);
            this.comMeterType.FormattingEnabled = true;
            this.comMeterType.Location = new System.Drawing.Point(215, 5);
            this.comMeterType.Name = "comMeterType";
            this.comMeterType.Size = new System.Drawing.Size(100, 20);
            this.comMeterType.TabIndex = 23;
            this.comMeterType.SelectedIndexChanged += new System.EventHandler(this.comMeterType_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F);
            this.label6.Location = new System.Drawing.Point(156, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "측정항목";
            // 
            // butGetCheckpointExcel
            // 
            this.butGetCheckpointExcel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butGetCheckpointExcel.Font = new System.Drawing.Font("굴림", 9F);
            this.butGetCheckpointExcel.Location = new System.Drawing.Point(755, 4);
            this.butGetCheckpointExcel.Name = "butGetCheckpointExcel";
            this.butGetCheckpointExcel.Size = new System.Drawing.Size(43, 22);
            this.butGetCheckpointExcel.TabIndex = 33;
            this.butGetCheckpointExcel.Text = "엑셀";
            this.butGetCheckpointExcel.UseVisualStyleBackColor = true;
            this.butGetCheckpointExcel.Click += new System.EventHandler(this.butGetCheckpointExcel_Click);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox18.Location = new System.Drawing.Point(811, 9);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(10, 495);
            this.pictureBox18.TabIndex = 120;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox17.Location = new System.Drawing.Point(0, 9);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(10, 495);
            this.pictureBox17.TabIndex = 119;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox16.Location = new System.Drawing.Point(0, 504);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(821, 9);
            this.pictureBox16.TabIndex = 118;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox15.Location = new System.Drawing.Point(0, 0);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(821, 9);
            this.pictureBox15.TabIndex = 117;
            this.pictureBox15.TabStop = false;
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.panel1);
            this.ultraTabPageControl4.Controls.Add(this.tableLayoutPanel7);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox28);
            this.ultraTabPageControl4.Controls.Add(this.griMeasureDump);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox24);
            this.ultraTabPageControl4.Controls.Add(this.tableLayoutPanel6);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox23);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox22);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox21);
            this.ultraTabPageControl4.Controls.Add(this.pictureBox20);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(821, 513);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butCompareResult);
            this.panel1.Controls.Add(this.butAddMeasureBulk);
            this.panel1.Controls.Add(this.butSaveMeasureBulk);
            this.panel1.Controls.Add(this.butDeleteMeasureBulk);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(10, 373);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(801, 131);
            this.panel1.TabIndex = 127;
            // 
            // butCompareResult
            // 
            this.butCompareResult.Location = new System.Drawing.Point(506, 30);
            this.butCompareResult.Name = "butCompareResult";
            this.butCompareResult.Size = new System.Drawing.Size(89, 23);
            this.butCompareResult.TabIndex = 32;
            this.butCompareResult.Text = "측정값 비교";
            this.butCompareResult.UseVisualStyleBackColor = true;
            this.butCompareResult.Click += new System.EventHandler(this.butCompareResult_Click);
            // 
            // butAddMeasureBulk
            // 
            this.butAddMeasureBulk.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butAddMeasureBulk.Font = new System.Drawing.Font("굴림", 9F);
            this.butAddMeasureBulk.Location = new System.Drawing.Point(654, 31);
            this.butAddMeasureBulk.Name = "butAddMeasureBulk";
            this.butAddMeasureBulk.Size = new System.Drawing.Size(43, 22);
            this.butAddMeasureBulk.TabIndex = 31;
            this.butAddMeasureBulk.Text = "추가";
            this.butAddMeasureBulk.UseVisualStyleBackColor = true;
            this.butAddMeasureBulk.Click += new System.EventHandler(this.butAddMeasureBulk_Click);
            // 
            // butSaveMeasureBulk
            // 
            this.butSaveMeasureBulk.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSaveMeasureBulk.Font = new System.Drawing.Font("굴림", 9F);
            this.butSaveMeasureBulk.Location = new System.Drawing.Point(703, 31);
            this.butSaveMeasureBulk.Name = "butSaveMeasureBulk";
            this.butSaveMeasureBulk.Size = new System.Drawing.Size(43, 22);
            this.butSaveMeasureBulk.TabIndex = 30;
            this.butSaveMeasureBulk.Text = "저장";
            this.butSaveMeasureBulk.UseVisualStyleBackColor = true;
            this.butSaveMeasureBulk.Click += new System.EventHandler(this.butSaveMeasureBulk_Click);
            // 
            // butDeleteMeasureBulk
            // 
            this.butDeleteMeasureBulk.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butDeleteMeasureBulk.Font = new System.Drawing.Font("굴림", 9F);
            this.butDeleteMeasureBulk.Location = new System.Drawing.Point(752, 31);
            this.butDeleteMeasureBulk.Name = "butDeleteMeasureBulk";
            this.butDeleteMeasureBulk.Size = new System.Drawing.Size(43, 22);
            this.butDeleteMeasureBulk.TabIndex = 29;
            this.butDeleteMeasureBulk.Text = "삭제";
            this.butDeleteMeasureBulk.UseVisualStyleBackColor = true;
            this.butDeleteMeasureBulk.Click += new System.EventHandler(this.butDeleteMeasureBulk_Click);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 266F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 118F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 330F));
            this.tableLayoutPanel7.Controls.Add(this.butFindFile, 3, 3);
            this.tableLayoutPanel7.Controls.Add(this.label13, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.texDetailDumpNumber, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.texDetailLocName, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.texDetailDumpDate, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.label8, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.comDetailDumpType, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.label12, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.texDetailId, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.label11, 2, 2);
            this.tableLayoutPanel7.Controls.Add(this.texDetailComment, 3, 2);
            this.tableLayoutPanel7.Controls.Add(this.texDumpFilePath, 1, 3);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(10, 257);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 4;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(801, 116);
            this.tableLayoutPanel7.TabIndex = 126;
            // 
            // butFindFile
            // 
            this.butFindFile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butFindFile.Font = new System.Drawing.Font("굴림", 9F);
            this.butFindFile.Location = new System.Drawing.Point(474, 90);
            this.butFindFile.Name = "butFindFile";
            this.butFindFile.Size = new System.Drawing.Size(78, 22);
            this.butFindFile.TabIndex = 32;
            this.butFindFile.Text = "파일찾기";
            this.butFindFile.UseVisualStyleBackColor = true;
            this.butFindFile.Click += new System.EventHandler(this.butFindFile_Click);
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F);
            this.label13.Location = new System.Drawing.Point(55, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 12);
            this.label13.TabIndex = 46;
            this.label13.Text = "파일";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.Location = new System.Drawing.Point(31, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "일련번호";
            // 
            // texDetailDumpNumber
            // 
            this.texDetailDumpNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texDetailDumpNumber.Font = new System.Drawing.Font("굴림", 9F);
            this.texDetailDumpNumber.Location = new System.Drawing.Point(90, 3);
            this.texDetailDumpNumber.Name = "texDetailDumpNumber";
            this.texDetailDumpNumber.ReadOnly = true;
            this.texDetailDumpNumber.Size = new System.Drawing.Size(150, 21);
            this.texDetailDumpNumber.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F);
            this.label9.Location = new System.Drawing.Point(415, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 33;
            this.label9.Text = "입력일자";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F);
            this.label10.Location = new System.Drawing.Point(31, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 35;
            this.label10.Text = "소블록명";
            // 
            // texDetailLocName
            // 
            this.texDetailLocName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texDetailLocName.Font = new System.Drawing.Font("굴림", 9F);
            this.texDetailLocName.Location = new System.Drawing.Point(90, 31);
            this.texDetailLocName.Name = "texDetailLocName";
            this.texDetailLocName.ReadOnly = true;
            this.texDetailLocName.Size = new System.Drawing.Size(100, 21);
            this.texDetailLocName.TabIndex = 39;
            // 
            // texDetailDumpDate
            // 
            this.texDetailDumpDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texDetailDumpDate.Font = new System.Drawing.Font("굴림", 9F);
            this.texDetailDumpDate.Location = new System.Drawing.Point(474, 3);
            this.texDetailDumpDate.Name = "texDetailDumpDate";
            this.texDetailDumpDate.ReadOnly = true;
            this.texDetailDumpDate.Size = new System.Drawing.Size(150, 21);
            this.texDetailDumpDate.TabIndex = 40;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F);
            this.label8.Location = new System.Drawing.Point(415, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 37;
            this.label8.Text = "측정항목";
            // 
            // comDetailDumpType
            // 
            this.comDetailDumpType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comDetailDumpType.Font = new System.Drawing.Font("굴림", 9F);
            this.comDetailDumpType.FormattingEnabled = true;
            this.comDetailDumpType.Location = new System.Drawing.Point(474, 32);
            this.comDetailDumpType.Name = "comDetailDumpType";
            this.comDetailDumpType.Size = new System.Drawing.Size(100, 20);
            this.comDetailDumpType.TabIndex = 42;
            this.comDetailDumpType.SelectedIndexChanged += new System.EventHandler(this.comDetailDumpType_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F);
            this.label12.Location = new System.Drawing.Point(44, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 12);
            this.label12.TabIndex = 43;
            this.label12.Text = "지점ID";
            // 
            // texDetailId
            // 
            this.texDetailId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texDetailId.Font = new System.Drawing.Font("굴림", 9F);
            this.texDetailId.Location = new System.Drawing.Point(90, 60);
            this.texDetailId.Name = "texDetailId";
            this.texDetailId.ReadOnly = true;
            this.texDetailId.Size = new System.Drawing.Size(100, 21);
            this.texDetailId.TabIndex = 44;
            this.texDetailId.TextChanged += new System.EventHandler(this.texDetailId_TextChanged);
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F);
            this.label11.Location = new System.Drawing.Point(439, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 41;
            this.label11.Text = "비고";
            // 
            // texDetailComment
            // 
            this.texDetailComment.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texDetailComment.Font = new System.Drawing.Font("굴림", 9F);
            this.texDetailComment.Location = new System.Drawing.Point(474, 60);
            this.texDetailComment.Name = "texDetailComment";
            this.texDetailComment.Size = new System.Drawing.Size(200, 21);
            this.texDetailComment.TabIndex = 45;
            this.texDetailComment.TextChanged += new System.EventHandler(this.texDetailComment_TextChanged);
            // 
            // texDumpFilePath
            // 
            this.texDumpFilePath.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel7.SetColumnSpan(this.texDumpFilePath, 2);
            this.texDumpFilePath.Font = new System.Drawing.Font("굴림", 9F);
            this.texDumpFilePath.Location = new System.Drawing.Point(90, 90);
            this.texDumpFilePath.Name = "texDumpFilePath";
            this.texDumpFilePath.ReadOnly = true;
            this.texDumpFilePath.Size = new System.Drawing.Size(378, 21);
            this.texDumpFilePath.TabIndex = 47;
            this.texDumpFilePath.TextChanged += new System.EventHandler(this.texDumpFilePath_TextChanged);
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox28.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox28.Location = new System.Drawing.Point(10, 248);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(801, 9);
            this.pictureBox28.TabIndex = 125;
            this.pictureBox28.TabStop = false;
            // 
            // griMeasureDump
            // 
            this.griMeasureDump.Dock = System.Windows.Forms.DockStyle.Top;
            this.griMeasureDump.Location = new System.Drawing.Point(10, 48);
            this.griMeasureDump.Name = "griMeasureDump";
            this.griMeasureDump.Size = new System.Drawing.Size(801, 200);
            this.griMeasureDump.TabIndex = 124;
            this.griMeasureDump.Text = "ultraGrid1";
            this.griMeasureDump.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griMeasureDump_AfterSelectChange);
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox24.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox24.Location = new System.Drawing.Point(10, 39);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(801, 9);
            this.pictureBox24.TabIndex = 123;
            this.pictureBox24.TabStop = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 9;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 107F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel6.Controls.Add(this.datDumpEndDate, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.datDumpStartDate, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label21, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.comDumpSBlock, 4, 0);
            this.tableLayoutPanel6.Controls.Add(this.label2, 5, 0);
            this.tableLayoutPanel6.Controls.Add(this.comDumpType, 6, 0);
            this.tableLayoutPanel6.Controls.Add(this.butSelectDumpData, 7, 0);
            this.tableLayoutPanel6.Controls.Add(this.butSelectDumpExcel, 8, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(10, 9);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(801, 30);
            this.tableLayoutPanel6.TabIndex = 122;
            // 
            // datDumpEndDate
            // 
            this.datDumpEndDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datDumpEndDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datDumpEndDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datDumpEndDate.Location = new System.Drawing.Point(151, 5);
            this.datDumpEndDate.Name = "datDumpEndDate";
            this.datDumpEndDate.Size = new System.Drawing.Size(94, 19);
            this.datDumpEndDate.TabIndex = 31;
            this.datDumpEndDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // datDumpStartDate
            // 
            this.datDumpStartDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.datDumpStartDate.DateTime = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            this.datDumpStartDate.Font = new System.Drawing.Font("굴림", 8F);
            this.datDumpStartDate.Location = new System.Drawing.Point(44, 5);
            this.datDumpStartDate.Name = "datDumpStartDate";
            this.datDumpStartDate.Size = new System.Drawing.Size(94, 19);
            this.datDumpStartDate.TabIndex = 30;
            this.datDumpStartDate.Value = new System.DateTime(2010, 9, 16, 0, 0, 0, 0);
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 9F);
            this.label20.Location = new System.Drawing.Point(9, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 12);
            this.label20.TabIndex = 29;
            this.label20.Text = "일자";
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("굴림", 9F);
            this.label21.Location = new System.Drawing.Point(288, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 12);
            this.label21.TabIndex = 33;
            this.label21.Text = "지역";
            // 
            // comDumpSBlock
            // 
            this.comDumpSBlock.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comDumpSBlock.Font = new System.Drawing.Font("굴림", 9F);
            this.comDumpSBlock.FormattingEnabled = true;
            this.comDumpSBlock.Location = new System.Drawing.Point(323, 5);
            this.comDumpSBlock.Name = "comDumpSBlock";
            this.comDumpSBlock.Size = new System.Drawing.Size(100, 20);
            this.comDumpSBlock.TabIndex = 34;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F);
            this.label2.Location = new System.Drawing.Point(464, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "측정항목";
            // 
            // comDumpType
            // 
            this.comDumpType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comDumpType.Font = new System.Drawing.Font("굴림", 9F);
            this.comDumpType.FormattingEnabled = true;
            this.comDumpType.Location = new System.Drawing.Point(523, 5);
            this.comDumpType.Name = "comDumpType";
            this.comDumpType.Size = new System.Drawing.Size(100, 20);
            this.comDumpType.TabIndex = 23;
            // 
            // butSelectDumpData
            // 
            this.butSelectDumpData.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSelectDumpData.Font = new System.Drawing.Font("굴림", 9F);
            this.butSelectDumpData.Location = new System.Drawing.Point(700, 4);
            this.butSelectDumpData.Name = "butSelectDumpData";
            this.butSelectDumpData.Size = new System.Drawing.Size(43, 22);
            this.butSelectDumpData.TabIndex = 28;
            this.butSelectDumpData.Text = "조회";
            this.butSelectDumpData.UseVisualStyleBackColor = true;
            this.butSelectDumpData.Click += new System.EventHandler(this.button1_Click);
            // 
            // butSelectDumpExcel
            // 
            this.butSelectDumpExcel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSelectDumpExcel.Font = new System.Drawing.Font("굴림", 9F);
            this.butSelectDumpExcel.Location = new System.Drawing.Point(755, 4);
            this.butSelectDumpExcel.Name = "butSelectDumpExcel";
            this.butSelectDumpExcel.Size = new System.Drawing.Size(43, 22);
            this.butSelectDumpExcel.TabIndex = 35;
            this.butSelectDumpExcel.Text = "엑셀";
            this.butSelectDumpExcel.UseVisualStyleBackColor = true;
            this.butSelectDumpExcel.Click += new System.EventHandler(this.butSelectDumpExcel_Click);
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox23.Location = new System.Drawing.Point(811, 9);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(10, 495);
            this.pictureBox23.TabIndex = 121;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox22.Location = new System.Drawing.Point(0, 9);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(10, 495);
            this.pictureBox22.TabIndex = 120;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox21.Location = new System.Drawing.Point(0, 504);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(821, 9);
            this.pictureBox21.TabIndex = 119;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox20.Location = new System.Drawing.Point(0, 0);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(821, 9);
            this.pictureBox20.TabIndex = 118;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(835, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 548);
            this.pictureBox3.TabIndex = 117;
            this.pictureBox3.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 548);
            this.picFrLeftM1.TabIndex = 116;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 557);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(845, 9);
            this.pictureBox1.TabIndex = 115;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(845, 9);
            this.pictureBox2.TabIndex = 114;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(8, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 100);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // ultraTabControl2
            // 
            this.ultraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl2.Name = "ultraTabControl2";
            this.ultraTabControl2.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.ultraTabControl2.Size = new System.Drawing.Size(200, 100);
            this.ultraTabControl2.TabIndex = 0;
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(196, 77);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 9);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(825, 9);
            this.pictureBox4.TabIndex = 119;
            this.pictureBox4.TabStop = false;
            // 
            // tabAnalysisResult
            // 
            this.tabAnalysisResult.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabAnalysisResult.Controls.Add(this.ultraTabPageControl1);
            this.tabAnalysisResult.Controls.Add(this.ultraTabPageControl2);
            this.tabAnalysisResult.Controls.Add(this.ultraTabPageControl3);
            this.tabAnalysisResult.Controls.Add(this.ultraTabPageControl4);
            this.tabAnalysisResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabAnalysisResult.Location = new System.Drawing.Point(10, 18);
            this.tabAnalysisResult.Name = "tabAnalysisResult";
            this.tabAnalysisResult.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabAnalysisResult.Size = new System.Drawing.Size(825, 539);
            this.tabAnalysisResult.TabIndex = 120;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Node";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Link";
            ultraTab7.TabPage = this.ultraTabPageControl3;
            ultraTab7.Text = "실측값 비교(고정식)";
            ultraTab3.TabPage = this.ultraTabPageControl4;
            ultraTab3.Text = "실측값 비교(이동식)";
            this.tabAnalysisResult.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab7,
            ultraTab3});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(821, 513);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 114F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 194F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 192F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.textBox1, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox1.Font = new System.Drawing.Font("굴림", 9F);
            this.textBox1.Location = new System.Drawing.Point(3, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(71, 21);
            this.textBox1.TabIndex = 29;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBox1.Font = new System.Drawing.Font("굴림", 9F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(80, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 20);
            this.comboBox1.TabIndex = 23;
            // 
            // frmAnalysisResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 566);
            this.Controls.Add(this.tabAnalysisResult);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAnalysisResult";
            this.Text = "해석결과 조회";
            this.Load += new System.EventHandler(this.frmAnalysisResult_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAnalysisResult_FormClosing);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaNodeResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griNodeResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datNodeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraTabPageControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaLinkResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griLinkResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datLinkDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.ultraTabPageControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaRealtimeCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datMeterDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griMeasureDump)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datDumpEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDumpStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabAnalysisResult)).EndInit();
            this.tabAnalysisResult.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox3;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ComboBox comNodeItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comNodeFormular;
        private System.Windows.Forms.Button butApplyNodeFilter;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid griNodeResult;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.TextBox texNodeValue;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid griLinkResult;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.TextBox texLinkValue;
        private System.Windows.Forms.ComboBox comLinkItem;
        private System.Windows.Forms.ComboBox comLinkFormular;
        private System.Windows.Forms.Button butApplyLinkFilter;
        private Infragistics.Win.UltraWinGrid.UltraGrid griRealtimeCheck;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comMeterType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button butGetCheckpointData;
        private System.Windows.Forms.ComboBox comTagname;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private ChartFX.WinForms.Chart chaRealtimeCheck;
        private ChartFX.WinForms.Chart chaNodeResult;
        private ChartFX.WinForms.Chart chaLinkResult;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.ComboBox comDumpType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button butSelectDumpData;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox texDetailDumpNumber;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox texDetailLocName;
        private System.Windows.Forms.TextBox texDetailDumpDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox texDetailComment;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butAddMeasureBulk;
        private System.Windows.Forms.Button butSaveMeasureBulk;
        private System.Windows.Forms.Button butDeleteMeasureBulk;
        private System.Windows.Forms.Button butFindFile;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox texDumpFilePath;
        public Infragistics.Win.UltraWinTabControl.UltraTabControl tabAnalysisResult;
        public System.Windows.Forms.ComboBox comDetailDumpType;
        public System.Windows.Forms.TextBox texDetailId;
        private System.Windows.Forms.Button butCompareResult;
        public Infragistics.Win.UltraWinGrid.UltraGrid griMeasureDump;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datNodeDate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comNodeSBlock;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.ComboBox comLinkSBlock;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datLinkDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datMeterDate;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datDumpEndDate;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comDumpSBlock;
        private System.Windows.Forms.Label label20;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datDumpStartDate;
        private System.Windows.Forms.Button butSelectDumpExcel;
        private System.Windows.Forms.Button butApplyNodeExcel;
        private System.Windows.Forms.Button butApplyLinkExcel;
        private System.Windows.Forms.Button butGetCheckpointExcel;
    }
}