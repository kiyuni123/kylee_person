﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WH_Common.work;
using WaterNet.WH_RTAnalysis.work;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmAnalysisResultControl : Form
    {
        private System.Drawing.Size m_formsize = new System.Drawing.Size(); //폼사이즈
        private System.Drawing.Point m_formLocation = new System.Drawing.Point(); //폼위치
        private IntPtr m_formParam = new IntPtr();                                //최종 윈도우 파라메터
        
        public frmWHMain parentForm = null;

        private CommonWork cWork = null;
        private RealtimeAnalysisResultWork rWork = null;

        public frmAnalysisResultControl()
        {
            cWork = CommonWork.GetInstance();
            rWork = RealtimeAnalysisResultWork.GetInstance();

            InitializeComponent();
        }

        /// <summary>
        /// 윈도우 메세지 후킹 : 화면 최소화, 복구 이벤트 기능
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                if (m.WParam == new IntPtr(0xF020)) // SC_MINIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    m_formsize = this.Size;
                    m_formLocation = this.Location;
                    m_formParam = m.WParam;

                    this.Size = new System.Drawing.Size(150, SystemInformation.CaptionHeight);
                    if (this.ParentForm != null)
                        this.Location = new System.Drawing.Point(this.ParentForm.Width - 170, this.ParentForm.Height - 40);
                    else
                        this.Location = new System.Drawing.Point(0, 0);

                    this.BringToFront();
                    m.Result = new IntPtr(0);
                    return;
                }
                else if (m.WParam == new IntPtr(0xF030))  // SC_MAXIMIZE
                {
                    if (m_formParam.Equals(m.WParam)) return;
                    if (m_formsize.Height == 0 | m_formsize.Width == 0) return;

                    this.Size = m_formsize;
                    this.Location = m_formLocation;
                    this.BringToFront();
                    m_formParam = m.WParam;

                    m.Result = new IntPtr(0);
                    return;
                }
                else if ((m.WParam.ToInt32() >= 61441) && (m.WParam.ToInt32() <= 61448))  // SC_SIZE변경
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF032))  //최대화(타이틀바 더블클릭)시.
                {
                    if (m_formParam.ToInt32().Equals(61472)) return;  //현재 최소화일때 리턴
                }
                else if (m.WParam == new IntPtr(0xF060))  //SC_CLOSE버튼 실행안함.
                {
                    return;
                }
                m.Result = new IntPtr(0);
            }

            base.WndProc(ref m);
        }

        //폼 최초로딩 시 실행
        private void frmAnalysisResultControl_Load(object sender, EventArgs e)
        {
            InitializingData();

            comNodeItem.SelectedIndexChanged += new EventHandler(comNodeItem_SelectedIndexChanged);
            comLinkItem.SelectedIndexChanged +=new EventHandler(comLinkItem_SelectedIndexChanged);

            comNodeItem_SelectedIndexChanged(this.comNodeItem, new EventArgs());
            comLinkItem_SelectedIndexChanged(this.comLinkItem, new EventArgs());
        }

        //데이터 초기화
        public void InitializingData()
        {
            //Node표출항목을 조회한다.
            comNodeItem.ValueMember = "CODE";
            comNodeItem.DisplayMember = "CODE_NAME";

            Hashtable nodeItemConditions = new Hashtable();

            nodeItemConditions.Add("PCODE", "1004");
            nodeItemConditions.Add("removeAllSelect", "Y");

            DataSet nodeItemData = cWork.GetCodeList(nodeItemConditions);

            comNodeItem.DataSource = nodeItemData.Tables["CM_CODE"];

            //Link표출항목을 조회한다.
            comLinkItem.ValueMember = "CODE";
            comLinkItem.DisplayMember = "CODE_NAME";

            Hashtable linkItemConditions = new Hashtable();

            linkItemConditions.Add("PCODE", "1005");
            linkItemConditions.Add("removeAllSelect", "Y");

            DataSet linkItemData = cWork.GetCodeList(linkItemConditions);

            comLinkItem.DataSource = linkItemData.Tables["CM_CODE"];

            //초기화로 노드는 압력 링크는 유량으로 맞춘다.
            comNodeItem.SelectedValue = "000006";
            comLinkItem.SelectedValue = "000006";

            //초기실행 시 모델에 표출세팅
            Hashtable settingData = new Hashtable();

            settingData.Add("NODE_RESULT_TYPE", "1004");
            settingData.Add("LINK_RESULT_TYPE", "1005");
            settingData.Add("NODE_RESULT_CODE", comNodeItem.SelectedValue);
            settingData.Add("LINK_RESULT_CODE", comLinkItem.SelectedValue);

            Hashtable nodeItemCondition = new Hashtable();

            nodeItemCondition.Add("INP_NUMBER", parentForm);
            nodeItemCondition.Add("RESULT_TYPE", "1004");
            nodeItemCondition.Add("RESULT_CODE", comNodeItem.SelectedValue);

            DataSet nodeRangeDataSet = rWork.SelectAnalysisItemRangeData(nodeItemCondition);

            if (nodeRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows.Count != 0)
            {
                settingData.Add("nodeRange1", nodeRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE1"].ToString());
                settingData.Add("nodeRange2", nodeRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE2"].ToString());
                settingData.Add("nodeRange3", nodeRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE3"].ToString());
                settingData.Add("nodeRange4", nodeRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE4"].ToString());
            }

            Hashtable linkItemCondition = new Hashtable();

            linkItemCondition.Add("INP_NUMBER", parentForm.dummyInpNumber);
            linkItemCondition.Add("RESULT_TYPE", "1004");
            linkItemCondition.Add("RESULT_CODE", comNodeItem.SelectedValue);

            DataSet linkRangeDataSet = rWork.SelectAnalysisItemRangeData(linkItemCondition);

            if (linkRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows.Count != 0)
            {
                settingData.Add("linkRange1", linkRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE1"].ToString());
                settingData.Add("linkRange2", linkRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE2"].ToString());
                settingData.Add("linkRange3", linkRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE3"].ToString());
                settingData.Add("linkRange4", linkRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE4"].ToString());
            }
        }


        //Node Item 변경 시
        private void comNodeItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("node Start: " + DateTime.Now);
            labNodeItem.Text = comNodeItem.Text;

            //항목별 Rage를 조회한다.
            Hashtable nodeItemCondition = new Hashtable();

            nodeItemCondition.Add("INP_NUMBER", parentForm.dummyInpNumber);
            nodeItemCondition.Add("RESULT_TYPE", "1004");
            nodeItemCondition.Add("RESULT_CODE", comNodeItem.SelectedValue);

            DataSet itemRangeDataSet = rWork.SelectAnalysisItemRangeData(nodeItemCondition);

            if (itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows.Count != 0)
            {
                labNodeRange1.Text = itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE1"].ToString();
                labNodeRange2.Text = itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE2"].ToString();
                labNodeRange3.Text = itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE3"].ToString();
                labNodeRange4.Text = itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE4"].ToString();

                //단위설정
                if ("000001".Equals(comNodeItem.SelectedValue.ToString()))
                {
                    //Elevation
                    labNodeUnit.Text = "m";
                }
                else if ("000002".Equals(comNodeItem.SelectedValue.ToString()))
                {
                    //Base Demand
                    labNodeUnit.Text = "CMH";
                }
                else if ("000003".Equals(comNodeItem.SelectedValue.ToString()))
                {
                    //Initial Quality
                    labNodeUnit.Text = "";
                }
                else if ("000004".Equals(comNodeItem.SelectedValue.ToString()))
                {
                    //Demand
                    labNodeUnit.Text = "CMH";
                }
                else if ("000005".Equals(comNodeItem.SelectedValue.ToString()))
                {
                    //HEAD
                    labNodeUnit.Text = "m";
                }
                else if ("000006".Equals(comNodeItem.SelectedValue.ToString()))
                {
                    //Pressure
                    labNodeUnit.Text = "m";
                }
                else if ("000007".Equals(comNodeItem.SelectedValue.ToString()))
                {
                    //Quality
                    labNodeUnit.Text = "";
                }

                Hashtable settingData = new Hashtable();

                settingData.Add("NODE_RESULT_TYPE", "1004");
                settingData.Add("NODE_RESULT_CODE", comNodeItem.SelectedValue);
                settingData.Add("nodeRange1", itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE1"].ToString());
                settingData.Add("nodeRange2", itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE2"].ToString());
                settingData.Add("nodeRange3", itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE3"].ToString());
                settingData.Add("nodeRange4", itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE4"].ToString());

                //parentForm.SettingInpLayer(settingData);
                parentForm.SettingInpLayer3(settingData);

                Console.WriteLine("node End: " + DateTime.Now);
            }
        }

        //Link Item 변경 시
        private void comLinkItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("link Start: " + DateTime.Now);
            labLinkItem.Text = comLinkItem.Text;

            //항목별 Rage를 조회한다.
            Hashtable linkItemCondition = new Hashtable();

            linkItemCondition.Add("INP_NUMBER", parentForm.dummyInpNumber);
            linkItemCondition.Add("RESULT_TYPE", "1005");
            linkItemCondition.Add("RESULT_CODE", comLinkItem.SelectedValue);

            DataSet itemRangeDataSet = rWork.SelectAnalysisItemRangeData(linkItemCondition);

            if (itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows.Count != 0)
            {
                labLinkRange1.Text = itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE1"].ToString();
                labLinkRange2.Text = itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE2"].ToString();
                labLinkRange3.Text = itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE3"].ToString();
                labLinkRange4.Text = itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE4"].ToString();

                //단위설정
                if ("000001".Equals(comLinkItem.SelectedValue.ToString()))
                {
                    //Length
                    labLinkUnit.Text = "m";
                }
                else if ("000002".Equals(comLinkItem.SelectedValue.ToString()))
                {
                    //Diameter
                    labLinkUnit.Text = "mm";
                }
                else if ("000003".Equals(comLinkItem.SelectedValue.ToString()))
                {
                    //Roughness
                    labLinkUnit.Text = "";
                }
                else if ("000004".Equals(comLinkItem.SelectedValue.ToString()))
                {
                    //Bulk Coeff.
                    labLinkUnit.Text = "";
                }
                else if ("000005".Equals(comLinkItem.SelectedValue.ToString()))
                {
                    //Wall Coeff.
                    labLinkUnit.Text = "";
                }
                else if ("000006".Equals(comLinkItem.SelectedValue.ToString()))
                {
                    //Flow
                    labLinkUnit.Text = "CMH";
                }
                else if ("000007".Equals(comLinkItem.SelectedValue.ToString()))
                {
                    //Velocity
                    labLinkUnit.Text = "m/s";
                }
                else if ("000008".Equals(comLinkItem.SelectedValue.ToString()))
                {
                    //Unit Headloss
                    labLinkUnit.Text = "m/km";
                }

                Hashtable settingData = new Hashtable();

                settingData.Add("LINK_RESULT_TYPE", "1005");
                settingData.Add("LINK_RESULT_CODE", comLinkItem.SelectedValue);
                settingData.Add("linkRange1", itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE1"].ToString());
                settingData.Add("linkRange2", itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE2"].ToString());
                settingData.Add("linkRange3", itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE3"].ToString());
                settingData.Add("linkRange4", itemRangeDataSet.Tables["WH_ANALYSIS_RESULT_RANGE"].Rows[0]["RANGE4"].ToString());

                //parentForm.SettingInpLayer(settingData);
                parentForm.SettingInpLayer3(settingData);
                Console.WriteLine("link End: " + DateTime.Now);
            }
        }

        //표시범위 설정 팝업 호출
        private void butTimeTable_Click(object sender, EventArgs e)
        {
            frmAnalysisResultRangeManage rangeManageForm = new frmAnalysisResultRangeManage();
            rangeManageForm.dummyInpNumber = parentForm.dummyInpNumber;
            rangeManageForm.parentForm = this;
            rangeManageForm.ShowDialog();
        }

        //Node 출력 표시 check 시
        private void cheNodeResultView_CheckedChanged(object sender, EventArgs e)
        {
            if (cheNodeResultView.Checked)
            {
                parentForm.SettingLayerLabel("node", true);
            }
            else
            {
                parentForm.SettingLayerLabel("node", false);
            }
        }

        //Link 출력 표시 check 시
        private void checLinkResultView_CheckedChanged(object sender, EventArgs e)
        {
            if (checLinkResultView.Checked)
            {
                parentForm.SettingLayerLabel("link", true);
            }
            else
            {
                parentForm.SettingLayerLabel("link", false);
            }
        }
    }
}
