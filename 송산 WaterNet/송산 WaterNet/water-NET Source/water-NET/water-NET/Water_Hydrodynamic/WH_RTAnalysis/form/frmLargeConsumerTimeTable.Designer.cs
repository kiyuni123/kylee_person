﻿namespace WaterNet.WH_RTAnalysis.form
{
    partial class frmLargeConsumerTimeTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.griLargeConsumerTimeTable = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.butSaveCheckpoint = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.griLargeConsumerTimeTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.SuspendLayout();
            // 
            // griLargeConsumerTimeTable
            // 
            this.griLargeConsumerTimeTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.griLargeConsumerTimeTable.Location = new System.Drawing.Point(10, 9);
            this.griLargeConsumerTimeTable.Name = "griLargeConsumerTimeTable";
            this.griLargeConsumerTimeTable.Size = new System.Drawing.Size(792, 190);
            this.griLargeConsumerTimeTable.TabIndex = 123;
            this.griLargeConsumerTimeTable.Text = "대수용가 시간별 동작여부";
            this.griLargeConsumerTimeTable.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.griLargeConsumerTimeTable_DoubleClickCell);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Location = new System.Drawing.Point(802, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 221);
            this.pictureBox2.TabIndex = 121;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox16.Location = new System.Drawing.Point(0, 9);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(10, 221);
            this.pictureBox16.TabIndex = 120;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 230);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(812, 9);
            this.pictureBox1.TabIndex = 118;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox14.Location = new System.Drawing.Point(0, 0);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(812, 9);
            this.pictureBox14.TabIndex = 117;
            this.pictureBox14.TabStop = false;
            // 
            // butSaveCheckpoint
            // 
            this.butSaveCheckpoint.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSaveCheckpoint.Font = new System.Drawing.Font("굴림", 9F);
            this.butSaveCheckpoint.Location = new System.Drawing.Point(755, 203);
            this.butSaveCheckpoint.Name = "butSaveCheckpoint";
            this.butSaveCheckpoint.Size = new System.Drawing.Size(43, 22);
            this.butSaveCheckpoint.TabIndex = 124;
            this.butSaveCheckpoint.Text = "저장";
            this.butSaveCheckpoint.UseVisualStyleBackColor = true;
            this.butSaveCheckpoint.Click += new System.EventHandler(this.butSaveCheckpoint_Click);
            // 
            // frmLargeConsumerTimeTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 239);
            this.Controls.Add(this.butSaveCheckpoint);
            this.Controls.Add(this.griLargeConsumerTimeTable);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox14);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLargeConsumerTimeTable";
            this.Text = "대수용가 동작설정";
            this.Load += new System.EventHandler(this.frmLargeConsumerTimeTable_Load);
            ((System.ComponentModel.ISupportInitialize)(this.griLargeConsumerTimeTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox2;
        public Infragistics.Win.UltraWinGrid.UltraGrid griLargeConsumerTimeTable;
        private System.Windows.Forms.Button butSaveCheckpoint;
    }
}