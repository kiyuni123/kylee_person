﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using ChartFX.WinForms;

using WaterNet.WH_RTAnalysis.work;
using WaterNet.WaterNetCore;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmResultCompare : Form
    {
        private RealtimeAnalysisResultWork rWork = null;
        public string dumpNumber = "";
        public string dumpType = "";

        public frmResultCompare()
        {
            rWork = RealtimeAnalysisResultWork.GetInstance();

            InitializeComponent();
            InitializeSetting();
        }

        //화면초기화
        private void InitializeSetting()
        {
            //Node 해석결과 그리드 초기화
            UltraGridColumn compareResultColumn;

            compareResultColumn = griCompareResult.DisplayLayout.Bands[0].Columns.Add();
            compareResultColumn.Key = "DUMP_DATE";
            compareResultColumn.Header.Caption = "일자";
            compareResultColumn.CellActivation = Activation.NoEdit;
            compareResultColumn.CellClickAction = CellClickAction.RowSelect;
            compareResultColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            compareResultColumn.CellAppearance.TextHAlign = HAlign.Center;
            compareResultColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            compareResultColumn.Hidden = false;   //필드 보이기

            compareResultColumn = griCompareResult.DisplayLayout.Bands[0].Columns.Add();
            compareResultColumn.Key = "ANALYSIS_DATA";
            compareResultColumn.Header.Caption = "해석결과";
            compareResultColumn.CellActivation = Activation.NoEdit;
            compareResultColumn.CellClickAction = CellClickAction.RowSelect;
            compareResultColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            compareResultColumn.CellAppearance.TextHAlign = HAlign.Right;
            compareResultColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            compareResultColumn.Hidden = false;   //필드 보이기

            compareResultColumn = griCompareResult.DisplayLayout.Bands[0].Columns.Add();
            compareResultColumn.Key = "VALUE";
            compareResultColumn.Header.Caption = "실측값";
            compareResultColumn.CellActivation = Activation.NoEdit;
            compareResultColumn.CellClickAction = CellClickAction.RowSelect;
            compareResultColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            compareResultColumn.CellAppearance.TextHAlign = HAlign.Right;
            compareResultColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            compareResultColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griCompareResult);

            //차트 초기화
            this.chaCompareResult.LegendBox.MarginY = 1;
            this.chaCompareResult.AxisX.DataFormat.Format = AxisFormat.DateTime;
            this.chaCompareResult.AxisX.DataFormat.CustomFormat = "hh:mm";
            this.chaCompareResult.AxisX.Staggered = true;
        }

        //화면 최초 로딩 시 실행
        private void frmResultCompare_Load(object sender, EventArgs e)
        {
            //비교값 조회
            Hashtable conditions = new Hashtable();
            conditions.Add("DUMP_NUMBER",dumpNumber);
            conditions.Add("DUMP_TYPE",dumpType);

            DataSet dSet = rWork.SelectCompareData(conditions);

            griCompareResult.DataSource = dSet.Tables["WH_MEASURE_DUMP_DATA"];
            FormManager.SetGridStyle_PerformAutoResize(griCompareResult);

            this.chaCompareResult.Data.Series = 2;
            this.chaCompareResult.Data.Points = dSet.Tables["WH_MEASURE_DUMP_DATA"].Rows.Count;

            foreach (DataRow row in dSet.Tables["WH_MEASURE_DUMP_DATA"].Rows)
            {
                int pointIndex = dSet.Tables["WH_MEASURE_DUMP_DATA"].Rows.IndexOf(row);
                this.chaCompareResult.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["DUMP_DATE"]).ToString("hh:mm");
                this.chaCompareResult.Data[0, pointIndex] = Convert.ToDouble(row["ANALYSIS_DATA"]);
                this.chaCompareResult.Data[1, pointIndex] = Convert.ToDouble(row["VALUE"]);
            }

            this.chaCompareResult.Series[0].Text = "해석결과";
            this.chaCompareResult.Series[1].Text = "실측값";

            this.chaCompareResult.Series[0].Gallery = Gallery.Lines;
            this.chaCompareResult.Series[0].MarkerSize = 1;

            this.chaCompareResult.Series[1].Gallery = Gallery.Lines;
            this.chaCompareResult.Series[1].MarkerSize = 1;

            if (dSet.Tables["WH_MEASURE_DUMP_DATA"].Rows.Count != 0)
            {
                chaCompareResult.Series[0].Visible = true;
                chaCompareResult.Series[1].Visible = true;
            }

            if ("Flow".Equals(dumpType))
            {
                //유량인 경우
                chaCompareResult.Series[0].AxisY.Title.Text = "cmh";
            }
            else
            {
                //압력인 경우
                chaCompareResult.Series[0].AxisY.Title.Text = "m";
            }
        }
    }
}
