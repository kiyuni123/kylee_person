﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WaterNetCore;

using WaterNet.WH_RTAnalysis.work;
using WaterNet.WH_Common.utils;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmRealtimeAnalysisAlarm : Form
    {
        public frmWHMain parentForm = null;
        private RealtimeAnalysisResultWork work = null;

        public bool isWarning = false;

        public frmRealtimeAnalysisAlarm()
        {
            work = RealtimeAnalysisResultWork.GetInstance();
            InitializeComponent();
            InitializeSetting();
        }

        //화면초기화
        private void InitializeSetting()
        {
            //해석오류 마스터 그리드 초기화
            UltraGridColumn analysisErrorColumn;

            analysisErrorColumn = griAnalysisErrorList.DisplayLayout.Bands[0].Columns.Add();
            analysisErrorColumn.Key = "INP_NUMBER";
            analysisErrorColumn.Header.Caption = "모델번호";
            analysisErrorColumn.CellActivation = Activation.NoEdit;
            analysisErrorColumn.CellClickAction = CellClickAction.RowSelect;
            analysisErrorColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisErrorColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisErrorColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisErrorColumn.Hidden = false;   //필드 보이기
            
            analysisErrorColumn = griAnalysisErrorList.DisplayLayout.Bands[0].Columns.Add();
            analysisErrorColumn.Key = "TITLE";
            analysisErrorColumn.Header.Caption = "제목";
            analysisErrorColumn.CellActivation = Activation.NoEdit;
            analysisErrorColumn.CellClickAction = CellClickAction.RowSelect;
            analysisErrorColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisErrorColumn.CellAppearance.TextHAlign = HAlign.Left;
            analysisErrorColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisErrorColumn.Hidden = false;   //필드 보이기

            analysisErrorColumn = griAnalysisErrorList.DisplayLayout.Bands[0].Columns.Add();
            analysisErrorColumn.Key = "INCREASE_DATE";
            analysisErrorColumn.Header.Caption = "발생시간";
            analysisErrorColumn.CellActivation = Activation.NoEdit;
            analysisErrorColumn.CellClickAction = CellClickAction.RowSelect;
            analysisErrorColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisErrorColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisErrorColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisErrorColumn.Hidden = false;   //필드 보이기

            analysisErrorColumn = griAnalysisErrorList.DisplayLayout.Bands[0].Columns.Add();
            analysisErrorColumn.Key = "INCREASE_DATE_ORI";
            analysisErrorColumn.Header.Caption = "원래 발생시간";
            analysisErrorColumn.CellActivation = Activation.NoEdit;
            analysisErrorColumn.CellClickAction = CellClickAction.RowSelect;
            analysisErrorColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisErrorColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisErrorColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisErrorColumn.Hidden = true;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griAnalysisErrorList);

            //해석오류 디테일 그리드 초기화
            UltraGridColumn analysisErrorDetailColumn;

            analysisErrorDetailColumn = griAnalysisErrorDetailList.DisplayLayout.Bands[0].Columns.Add();
            analysisErrorDetailColumn.Key = "ERROR_CODE";
            analysisErrorDetailColumn.Header.Caption = "오류코드";
            analysisErrorDetailColumn.CellActivation = Activation.NoEdit;
            analysisErrorDetailColumn.CellClickAction = CellClickAction.RowSelect;
            analysisErrorDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisErrorDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisErrorDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisErrorDetailColumn.Hidden = false;   //필드 보이기

            analysisErrorDetailColumn = griAnalysisErrorDetailList.DisplayLayout.Bands[0].Columns.Add();
            analysisErrorDetailColumn.Key = "IDX";
            analysisErrorDetailColumn.Header.Caption = "일련번호";
            analysisErrorDetailColumn.CellActivation = Activation.NoEdit;
            analysisErrorDetailColumn.CellClickAction = CellClickAction.RowSelect;
            analysisErrorDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisErrorDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisErrorDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisErrorDetailColumn.Hidden = false;   //필드 보이기

            analysisErrorDetailColumn = griAnalysisErrorDetailList.DisplayLayout.Bands[0].Columns.Add();
            analysisErrorDetailColumn.Key = "CODE_NAME";
            analysisErrorDetailColumn.Header.Caption = "설명";
            analysisErrorDetailColumn.CellActivation = Activation.NoEdit;
            analysisErrorDetailColumn.CellClickAction = CellClickAction.RowSelect;
            analysisErrorDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisErrorDetailColumn.CellAppearance.TextHAlign = HAlign.Left;
            analysisErrorDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisErrorDetailColumn.Hidden = false;   //필드 보이기

            analysisErrorDetailColumn = griAnalysisErrorDetailList.DisplayLayout.Bands[0].Columns.Add();
            analysisErrorDetailColumn.Key = "INCREASE_DATE";
            analysisErrorDetailColumn.Header.Caption = "발생일자";
            analysisErrorDetailColumn.CellActivation = Activation.NoEdit;
            analysisErrorDetailColumn.CellClickAction = CellClickAction.RowSelect;
            analysisErrorDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisErrorDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisErrorDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisErrorDetailColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griAnalysisErrorDetailList);

            //경고 마스터 그리드 초기화
            UltraGridColumn analysisWarningColumn;

            analysisWarningColumn = griAnalysisWarningList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningColumn.Key = "INP_NUMBER";
            analysisWarningColumn.Header.Caption = "모델번호";
            analysisWarningColumn.CellActivation = Activation.NoEdit;
            analysisWarningColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisWarningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningColumn.Hidden = false;   //필드 보이기

            analysisWarningColumn = griAnalysisWarningList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningColumn.Key = "TITLE";
            analysisWarningColumn.Header.Caption = "제목";
            analysisWarningColumn.CellActivation = Activation.NoEdit;
            analysisWarningColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningColumn.CellAppearance.TextHAlign = HAlign.Left;
            analysisWarningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningColumn.Hidden = false;   //필드 보이기

            analysisWarningColumn = griAnalysisWarningList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningColumn.Key = "TARGET_DATE";
            analysisWarningColumn.Header.Caption = "해석일자";
            analysisWarningColumn.CellActivation = Activation.NoEdit;
            analysisWarningColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisWarningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningColumn.Hidden = false;   //필드 보이기

            analysisWarningColumn = griAnalysisWarningList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningColumn.Key = "LOC_NAME";
            analysisWarningColumn.Header.Caption = "지역명";
            analysisWarningColumn.CellActivation = Activation.NoEdit;
            analysisWarningColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningColumn.CellAppearance.TextHAlign = HAlign.Left;
            analysisWarningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningColumn.Hidden = false;   //필드 보이기

            analysisWarningColumn = griAnalysisWarningList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningColumn.Key = "TARGET_DATE_ORI";
            analysisWarningColumn.Header.Caption = "원래해석일자";
            analysisWarningColumn.CellActivation = Activation.NoEdit;
            analysisWarningColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisWarningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningColumn.Hidden = true;   //필드 보이기

            analysisWarningColumn = griAnalysisWarningList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningColumn.Key = "LOC_CODE";
            analysisWarningColumn.Header.Caption = "지역코드";
            analysisWarningColumn.CellActivation = Activation.NoEdit;
            analysisWarningColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisWarningColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningColumn.Hidden = true;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griAnalysisWarningList);

            //경고 디테일 그리드 초기화
            UltraGridColumn analysisWarningDetailColumn;

            analysisWarningDetailColumn = griAnalysisWarningDetailList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningDetailColumn.Key = "NODE_ID";
            analysisWarningDetailColumn.Header.Caption = "ID";
            analysisWarningDetailColumn.CellActivation = Activation.NoEdit;
            analysisWarningDetailColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisWarningDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningDetailColumn.Hidden = false;   //필드 보이기

            analysisWarningDetailColumn = griAnalysisWarningDetailList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningDetailColumn.Key = "PRESSURE";
            analysisWarningDetailColumn.Header.Caption = "압력";
            analysisWarningDetailColumn.CellActivation = Activation.NoEdit;
            analysisWarningDetailColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisWarningDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningDetailColumn.Hidden = false;   //필드 보이기

            analysisWarningDetailColumn = griAnalysisWarningDetailList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningDetailColumn.Key = "ELEVATION";
            analysisWarningDetailColumn.Header.Caption = "매설심도";
            analysisWarningDetailColumn.CellActivation = Activation.NoEdit;
            analysisWarningDetailColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisWarningDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningDetailColumn.Hidden = false;   //필드 보이기

            analysisWarningDetailColumn = griAnalysisWarningDetailList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningDetailColumn.Key = "DEMAND";
            analysisWarningDetailColumn.Header.Caption = "수요량";
            analysisWarningDetailColumn.CellActivation = Activation.NoEdit;
            analysisWarningDetailColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisWarningDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningDetailColumn.Hidden = false;   //필드 보이기

            analysisWarningDetailColumn = griAnalysisWarningDetailList.DisplayLayout.Bands[0].Columns.Add();
            analysisWarningDetailColumn.Key = "HEAD";
            analysisWarningDetailColumn.Header.Caption = "수두";
            analysisWarningDetailColumn.CellActivation = Activation.NoEdit;
            analysisWarningDetailColumn.CellClickAction = CellClickAction.RowSelect;
            analysisWarningDetailColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            analysisWarningDetailColumn.CellAppearance.TextHAlign = HAlign.Center;
            analysisWarningDetailColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            analysisWarningDetailColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griAnalysisWarningDetailList);
        }

        //폼 최초로딩 시 실행
        private void frmRealtimeAnalysisAlarm_Load(object sender, EventArgs e)
        {
            //=========================================================샘플
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================관망해석 정보관리 

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간관망해석ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.butApplyNodeFilter.Enabled = false;
            }

            //===========================================================================

            //모델내 소블록리스트 할당
            Hashtable sblockConditions = new Hashtable();
            sblockConditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

            comSblock.ValueMember = "LOC_CODE";
            comSblock.DisplayMember = "LOC_NAME";

            comSblock.DataSource = (work.SelectLocationData(sblockConditions)).Tables["CM_LOCATION"];

            if (isWarning)
            {
                datStartWarDate.Value = parentForm.increaseDate.Substring(0,10);
                datEndWarDate.Value = parentForm.increaseDate.Substring(0, 10);

                datStartErrDate.Value = parentForm.increaseDate.Substring(0, 10);
                datEndErrDate.Value = parentForm.increaseDate.Substring(0, 10);
            }
            else
            {
                //날짜 세팅 (3일전까지만)
                datStartWarDate.Value = Utils.GetCalcTime("dd", -3)["yyyymmdd"];
                datEndWarDate.Value = Utils.GetTime()["yyyymmdd"];

                datStartErrDate.Value = Utils.GetCalcTime("dd", -3)["yyyymmdd"];
                datEndErrDate.Value = Utils.GetTime()["yyyymmdd"];
            }

            Hashtable analysisSettingConditions = new Hashtable();

            analysisSettingConditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

            DataSet analysisSettingDataSet = work.SelectAnalysisSettingData(analysisSettingConditions);
            DataTable analysisSettingDataTable = analysisSettingDataSet.Tables["WH_ANALYSIS_SETTING"];

            if ("Y".Equals(analysisSettingDataTable.Rows[0]["ERROR_SAVE_YN"].ToString()))
            {
                cheErrorSaveYn.Checked = true;
            }
            else
            {
                cheErrorSaveYn.Checked = false;
            }

            texLowPressure.Text = analysisSettingDataTable.Rows[0]["LOW_PRESSURE"].ToString();

            //경고호출인 경우
            if(isWarning)
            {
                Console.WriteLine(parentForm.tabIdx);

                tabWarningManage.SelectedTab = tabWarningManage.Tabs[parentForm.tabIdx];

                if(parentForm.tabIdx == 0)
                {
                    butSelectWarList_Click(null, null);
                }
                else if (parentForm.tabIdx == 1)
                {
                    butSelectErrList_Click(null, null);
                }

                isWarning = false;
            }
        }

        //관망해석 오류내역 조회
        private void butSelectErrList_Click(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();

            conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
            conditions.Add("startErrDate", ((DateTime)datStartErrDate.Value).ToString("yyyyMMdd"));
            conditions.Add("endErrDate", ((DateTime)datEndErrDate.Value).ToString("yyyyMMdd"));

            DataSet analysisErrorDataSet = work.SelectAnalysisErrorList(conditions);
            griAnalysisErrorList.DataSource = analysisErrorDataSet.Tables["WH_REALTIME_ANALYSIS_ERROR"];
            FormManager.SetGridStyle_PerformAutoResize(griAnalysisErrorList);

            if (griAnalysisErrorList.Rows.Count != 0)
            {
                griAnalysisErrorList.Rows[0].Selected = true;
            }
            else
            {
                if (griAnalysisErrorDetailList.DataSource != null)
                {
                    ((DataTable)griAnalysisErrorDetailList.DataSource).Clear();
                }
            }
        }

        //관망해석 오류내역 master row가 변경되었을 경우
        private void griAnalysisErrorList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if(griAnalysisErrorList.Selected.Rows.Count != 0)
            {
                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", griAnalysisErrorList.Selected.Rows[0].GetCellValue("INP_NUMBER").ToString());
                conditions.Add("INCREASE_DATE", griAnalysisErrorList.Selected.Rows[0].GetCellValue("INCREASE_DATE_ORI").ToString());

                DataSet analysisErrorDetailDataSet = work.SelectAnalysisErrorDetailList(conditions);
                griAnalysisErrorDetailList.DataSource = analysisErrorDetailDataSet.Tables["WH_REALTIME_ANALYSIS_ERROR"];
                FormManager.SetGridStyle_PerformAutoResize(griAnalysisErrorDetailList);

                if (griAnalysisErrorDetailList.Rows.Count > 0)
                {
                    griAnalysisErrorDetailList.Rows[0].Selected = true;
                }
            }
        }

        //경고내역 조회
        private void butSelectWarList_Click(object sender, EventArgs e)
        {
            //경고 설정치(최저압력) 조회
            Hashtable settingConditions = new Hashtable();
            settingConditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

            DataSet settingDataSet = work.SelectAnalysisSettingData(settingConditions);
            DataTable settingDataTable = settingDataSet.Tables["WH_ANALYSIS_SETTING"];

            string pressure = "";

            if (settingDataTable.Rows.Count == 0)
            {
                MessageBox.Show("최저압력값이 설정되지 않았습니다.");
                tabWarningManage.TabIndex = 2;
                texLowPressure.Focus();
                return;
            }
            else
            {
                pressure = settingDataTable.Rows[0]["LOW_PRESSURE"].ToString();
            }

            Hashtable warningConditions = new Hashtable();
            warningConditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
            warningConditions.Add("startWarDate", ((DateTime)datStartWarDate.Value).ToString("yyyyMMdd"));
            warningConditions.Add("endWarDate", ((DateTime)datEndWarDate.Value).ToString("yyyyMMdd"));
            warningConditions.Add("PRESSURE", pressure);

            if(!"".Equals(comSblock.SelectedValue.ToString()))
            {
                warningConditions.Add("LOC_CODE", comSblock.SelectedValue.ToString());
            }

            DataSet analysisWarningDataSet = work.SelectAnalysisWarningList(warningConditions);

            griAnalysisWarningList.DataSource = analysisWarningDataSet.Tables["WH_RPT_MASTER"];
            FormManager.SetGridStyle_PerformAutoResize(griAnalysisWarningList);

            if (griAnalysisWarningList.Rows.Count != 0)
            {
                griAnalysisWarningList.Rows[0].Selected = true;
            }
            else
            {
                if (griAnalysisWarningDetailList.DataSource != null)
                {
                    ((DataTable)griAnalysisWarningDetailList.DataSource).Clear();
                }
            }
        }

        //경고내역 master row가 변경되었을 경우
        private void griAnalysisWarningList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (griAnalysisWarningList.Selected.Rows.Count != 0)
            {
                //경고 설정치(최저압력) 조회
                Hashtable settingConditions = new Hashtable();
                settingConditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

                DataSet settingDataSet = work.SelectAnalysisSettingData(settingConditions);
                DataTable settingDataTable = settingDataSet.Tables["WH_ANALYSIS_SETTING"];

                string pressure = "";

                if (settingDataTable.Rows.Count == 0)
                {
                    MessageBox.Show("최저압력값이 설정되지 않았습니다.");
                    tabWarningManage.TabIndex = 2;
                    texLowPressure.Focus();
                    return;
                }
                else
                {
                    pressure = settingDataTable.Rows[0]["LOW_PRESSURE"].ToString();
                }

                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", griAnalysisWarningList.Selected.Rows[0].GetCellValue("INP_NUMBER").ToString());
                conditions.Add("TARGET_DATE", griAnalysisWarningList.Selected.Rows[0].GetCellValue("TARGET_DATE_ORI").ToString());
                conditions.Add("PRESSURE", pressure);
                conditions.Add("LOC_CODE", griAnalysisWarningList.Selected.Rows[0].GetCellValue("LOC_CODE").ToString());

                DataSet analysisWarningDetailDataSet = work.SelectAnalysisWarningDetailList(conditions);
                griAnalysisWarningDetailList.DataSource = analysisWarningDetailDataSet.Tables["WH_RPT_MASTER"];
                FormManager.SetGridStyle_PerformAutoResize(griAnalysisWarningDetailList);

                if (griAnalysisWarningDetailList.Rows.Count > 0)
                {
                    griAnalysisWarningDetailList.Rows[0].Selected = true;
                }
            }
        }

        //경고설정 정보 저장
        private void butApplyNodeFilter_Click(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();

            if (cheErrorSaveYn.Checked)
            {
                conditions.Add("ERROR_SAVE_YN", "Y");
            }
            else
            {
                conditions.Add("ERROR_SAVE_YN", "N");
            }

            conditions.Add("LOW_PRESSURE", texLowPressure.Text);
            conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

            work.UpdateAnalysisSettingData(conditions);

            MessageBox.Show("정상적으로 처리되었습니다.");
        }

        private void griAnalysisWarningDetailList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            parentForm.MoveFocusAt("JUNCTION", "ID = '" + Utils.nts(griAnalysisWarningDetailList.Selected.Rows[0].GetCellValue("NODE_ID")) + "'");
        }

        //private void griAnalysisWarningDetailList_Click(object sender, EventArgs e)
        //{
        //    parentForm.MoveFocusAt("JUNCTION", "ID = '" + Utils.nts(griAnalysisWarningDetailList.Selected.Rows[0].GetCellValue("NODE_ID")) + "'");
        //}
    }
}
