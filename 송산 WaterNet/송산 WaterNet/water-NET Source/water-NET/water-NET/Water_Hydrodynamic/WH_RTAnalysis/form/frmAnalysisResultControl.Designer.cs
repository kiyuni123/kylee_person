﻿namespace WaterNet.WH_RTAnalysis.form
{
    partial class frmAnalysisResultControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labNodeItem = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.labNodeRange1 = new System.Windows.Forms.Label();
            this.labNodeRange2 = new System.Windows.Forms.Label();
            this.labNodeRange3 = new System.Windows.Forms.Label();
            this.labNodeRange4 = new System.Windows.Forms.Label();
            this.labNodeUnit = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labLinkUnit = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.labLinkRange1 = new System.Windows.Forms.Label();
            this.labLinkRange2 = new System.Windows.Forms.Label();
            this.labLinkRange3 = new System.Windows.Forms.Label();
            this.labLinkRange4 = new System.Windows.Forms.Label();
            this.labLinkItem = new System.Windows.Forms.Label();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.comNodeItem = new System.Windows.Forms.ComboBox();
            this.comLinkItem = new System.Windows.Forms.ComboBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.butTimeTable = new System.Windows.Forms.Button();
            this.cheNodeResultView = new System.Windows.Forms.CheckBox();
            this.checLinkResultView = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.pictureBox9);
            this.panel1.Controls.Add(this.pictureBox8);
            this.panel1.Controls.Add(this.pictureBox7);
            this.panel1.Controls.Add(this.pictureBox6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(110, 3);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel3.SetRowSpan(this.panel1, 4);
            this.panel1.Size = new System.Drawing.Size(110, 161);
            this.panel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.163265F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.83673F));
            this.tableLayoutPanel2.Controls.Add(this.labNodeItem, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox10, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox11, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox12, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox13, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox14, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.labNodeRange1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.labNodeRange2, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.labNodeRange3, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.labNodeRange4, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.labNodeUnit, 1, 6);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(10, 9);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(88, 141);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // labNodeItem
            // 
            this.labNodeItem.AutoSize = true;
            this.labNodeItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labNodeItem.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labNodeItem.Location = new System.Drawing.Point(10, 0);
            this.labNodeItem.Name = "labNodeItem";
            this.labNodeItem.Size = new System.Drawing.Size(75, 20);
            this.labNodeItem.TabIndex = 11;
            this.labNodeItem.Text = "88.888";
            this.labNodeItem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox10.BackColor = System.Drawing.Color.Blue;
            this.pictureBox10.Location = new System.Drawing.Point(0, 20);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(7, 20);
            this.pictureBox10.TabIndex = 1;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox11.BackColor = System.Drawing.Color.Cyan;
            this.pictureBox11.Location = new System.Drawing.Point(0, 40);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(7, 20);
            this.pictureBox11.TabIndex = 2;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox12.BackColor = System.Drawing.Color.Lime;
            this.pictureBox12.Location = new System.Drawing.Point(0, 60);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(7, 20);
            this.pictureBox12.TabIndex = 3;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox13.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox13.Location = new System.Drawing.Point(0, 80);
            this.pictureBox13.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(7, 20);
            this.pictureBox13.TabIndex = 4;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox14.BackColor = System.Drawing.Color.Red;
            this.pictureBox14.Location = new System.Drawing.Point(0, 100);
            this.pictureBox14.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(7, 20);
            this.pictureBox14.TabIndex = 5;
            this.pictureBox14.TabStop = false;
            // 
            // labNodeRange1
            // 
            this.labNodeRange1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labNodeRange1.AutoSize = true;
            this.labNodeRange1.Font = new System.Drawing.Font("굴림", 8F);
            this.labNodeRange1.Location = new System.Drawing.Point(10, 29);
            this.labNodeRange1.Name = "labNodeRange1";
            this.labNodeRange1.Size = new System.Drawing.Size(0, 11);
            this.labNodeRange1.TabIndex = 6;
            this.labNodeRange1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labNodeRange2
            // 
            this.labNodeRange2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labNodeRange2.AutoSize = true;
            this.labNodeRange2.Font = new System.Drawing.Font("굴림", 8F);
            this.labNodeRange2.Location = new System.Drawing.Point(10, 49);
            this.labNodeRange2.Name = "labNodeRange2";
            this.labNodeRange2.Size = new System.Drawing.Size(0, 11);
            this.labNodeRange2.TabIndex = 7;
            this.labNodeRange2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labNodeRange3
            // 
            this.labNodeRange3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labNodeRange3.AutoSize = true;
            this.labNodeRange3.Font = new System.Drawing.Font("굴림", 8F);
            this.labNodeRange3.Location = new System.Drawing.Point(10, 69);
            this.labNodeRange3.Name = "labNodeRange3";
            this.labNodeRange3.Size = new System.Drawing.Size(0, 11);
            this.labNodeRange3.TabIndex = 8;
            this.labNodeRange3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labNodeRange4
            // 
            this.labNodeRange4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labNodeRange4.AutoSize = true;
            this.labNodeRange4.Font = new System.Drawing.Font("굴림", 8F);
            this.labNodeRange4.Location = new System.Drawing.Point(10, 89);
            this.labNodeRange4.Name = "labNodeRange4";
            this.labNodeRange4.Size = new System.Drawing.Size(0, 11);
            this.labNodeRange4.TabIndex = 9;
            this.labNodeRange4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labNodeUnit
            // 
            this.labNodeUnit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labNodeUnit.AutoSize = true;
            this.labNodeUnit.Location = new System.Drawing.Point(28, 129);
            this.labNodeUnit.Name = "labNodeUnit";
            this.labNodeUnit.Size = new System.Drawing.Size(38, 12);
            this.labNodeUnit.TabIndex = 12;
            this.labNodeUnit.Text = "label1";
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox9.Location = new System.Drawing.Point(98, 9);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(10, 141);
            this.pictureBox9.TabIndex = 119;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox8.Location = new System.Drawing.Point(0, 9);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(10, 141);
            this.pictureBox8.TabIndex = 118;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox7.Location = new System.Drawing.Point(0, 150);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(108, 9);
            this.pictureBox7.TabIndex = 117;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox6.Location = new System.Drawing.Point(0, 0);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(108, 9);
            this.pictureBox6.TabIndex = 116;
            this.pictureBox6.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Controls.Add(this.pictureBox15);
            this.panel2.Controls.Add(this.pictureBox16);
            this.panel2.Controls.Add(this.pictureBox17);
            this.panel2.Controls.Add(this.pictureBox18);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(226, 3);
            this.panel2.Name = "panel2";
            this.tableLayoutPanel3.SetRowSpan(this.panel2, 4);
            this.panel2.Size = new System.Drawing.Size(119, 161);
            this.panel2.TabIndex = 120;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.163265F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.83673F));
            this.tableLayoutPanel1.Controls.Add(this.labLinkUnit, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labLinkRange1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labLinkRange2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labLinkRange3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.labLinkRange4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.labLinkItem, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 9);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(97, 141);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // labLinkUnit
            // 
            this.labLinkUnit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labLinkUnit.AutoSize = true;
            this.labLinkUnit.Location = new System.Drawing.Point(33, 129);
            this.labLinkUnit.Name = "labLinkUnit";
            this.labLinkUnit.Size = new System.Drawing.Size(38, 12);
            this.labLinkUnit.TabIndex = 13;
            this.labLinkUnit.Text = "label1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Blue;
            this.pictureBox1.Location = new System.Drawing.Point(0, 20);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(7, 20);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Cyan;
            this.pictureBox2.Location = new System.Drawing.Point(0, 40);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(7, 20);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackColor = System.Drawing.Color.Lime;
            this.pictureBox3.Location = new System.Drawing.Point(0, 60);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(7, 20);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox4.Location = new System.Drawing.Point(0, 80);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(7, 20);
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.BackColor = System.Drawing.Color.Red;
            this.pictureBox5.Location = new System.Drawing.Point(0, 100);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(7, 20);
            this.pictureBox5.TabIndex = 5;
            this.pictureBox5.TabStop = false;
            // 
            // labLinkRange1
            // 
            this.labLinkRange1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labLinkRange1.AutoSize = true;
            this.labLinkRange1.Font = new System.Drawing.Font("굴림", 8F);
            this.labLinkRange1.Location = new System.Drawing.Point(10, 29);
            this.labLinkRange1.Name = "labLinkRange1";
            this.labLinkRange1.Size = new System.Drawing.Size(0, 11);
            this.labLinkRange1.TabIndex = 6;
            this.labLinkRange1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labLinkRange2
            // 
            this.labLinkRange2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labLinkRange2.AutoSize = true;
            this.labLinkRange2.Font = new System.Drawing.Font("굴림", 8F);
            this.labLinkRange2.Location = new System.Drawing.Point(10, 49);
            this.labLinkRange2.Name = "labLinkRange2";
            this.labLinkRange2.Size = new System.Drawing.Size(0, 11);
            this.labLinkRange2.TabIndex = 7;
            this.labLinkRange2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labLinkRange3
            // 
            this.labLinkRange3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labLinkRange3.AutoSize = true;
            this.labLinkRange3.Font = new System.Drawing.Font("굴림", 8F);
            this.labLinkRange3.Location = new System.Drawing.Point(10, 69);
            this.labLinkRange3.Name = "labLinkRange3";
            this.labLinkRange3.Size = new System.Drawing.Size(0, 11);
            this.labLinkRange3.TabIndex = 8;
            this.labLinkRange3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labLinkRange4
            // 
            this.labLinkRange4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labLinkRange4.AutoSize = true;
            this.labLinkRange4.Font = new System.Drawing.Font("굴림", 8F);
            this.labLinkRange4.Location = new System.Drawing.Point(10, 89);
            this.labLinkRange4.Name = "labLinkRange4";
            this.labLinkRange4.Size = new System.Drawing.Size(0, 11);
            this.labLinkRange4.TabIndex = 9;
            this.labLinkRange4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labLinkItem
            // 
            this.labLinkItem.AutoSize = true;
            this.labLinkItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labLinkItem.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labLinkItem.Location = new System.Drawing.Point(10, 0);
            this.labLinkItem.Name = "labLinkItem";
            this.labLinkItem.Size = new System.Drawing.Size(84, 20);
            this.labLinkItem.TabIndex = 11;
            this.labLinkItem.Text = "88.888";
            this.labLinkItem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox15.Location = new System.Drawing.Point(107, 9);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(10, 141);
            this.pictureBox15.TabIndex = 119;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox16.Location = new System.Drawing.Point(0, 9);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(10, 141);
            this.pictureBox16.TabIndex = 118;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox17.Location = new System.Drawing.Point(0, 150);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(117, 9);
            this.pictureBox17.TabIndex = 117;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox18.Location = new System.Drawing.Point(0, 0);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(117, 9);
            this.pictureBox18.TabIndex = 116;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox19.Location = new System.Drawing.Point(0, 0);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(395, 9);
            this.pictureBox19.TabIndex = 121;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox20.Location = new System.Drawing.Point(0, 250);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(395, 9);
            this.pictureBox20.TabIndex = 122;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox21.Location = new System.Drawing.Point(0, 9);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(10, 241);
            this.pictureBox21.TabIndex = 123;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox22.Location = new System.Drawing.Point(385, 9);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(10, 241);
            this.pictureBox22.TabIndex = 124;
            this.pictureBox22.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.Controls.Add(this.pictureBox26);
            this.groupBox1.Controls.Add(this.pictureBox25);
            this.groupBox1.Controls.Add(this.pictureBox24);
            this.groupBox1.Controls.Add(this.pictureBox23);
            this.groupBox1.Location = new System.Drawing.Point(12, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(371, 205);
            this.groupBox1.TabIndex = 125;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "출력항목 설정";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 107F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel3.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.comNodeItem, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.comLinkItem, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel2, 2, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(13, 26);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.83562F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.16438F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(345, 167);
            this.tableLayoutPanel3.TabIndex = 126;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F);
            this.label13.Location = new System.Drawing.Point(3, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 12);
            this.label13.TabIndex = 28;
            this.label13.Text = "Node 출력";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F);
            this.label14.Location = new System.Drawing.Point(3, 91);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 12);
            this.label14.TabIndex = 29;
            this.label14.Text = "Link 출력";
            // 
            // comNodeItem
            // 
            this.comNodeItem.Font = new System.Drawing.Font("굴림", 9F);
            this.comNodeItem.FormattingEnabled = true;
            this.comNodeItem.Location = new System.Drawing.Point(3, 38);
            this.comNodeItem.Name = "comNodeItem";
            this.comNodeItem.Size = new System.Drawing.Size(100, 20);
            this.comNodeItem.TabIndex = 26;
            // 
            // comLinkItem
            // 
            this.comLinkItem.Font = new System.Drawing.Font("굴림", 9F);
            this.comLinkItem.FormattingEnabled = true;
            this.comLinkItem.Location = new System.Drawing.Point(3, 118);
            this.comLinkItem.Name = "comLinkItem";
            this.comLinkItem.Size = new System.Drawing.Size(100, 20);
            this.comLinkItem.TabIndex = 27;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox26.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox26.Location = new System.Drawing.Point(358, 26);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(10, 167);
            this.pictureBox26.TabIndex = 125;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox25.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox25.Location = new System.Drawing.Point(3, 26);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(10, 167);
            this.pictureBox25.TabIndex = 124;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox24.Location = new System.Drawing.Point(3, 193);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(365, 9);
            this.pictureBox24.TabIndex = 123;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox23.Location = new System.Drawing.Point(3, 17);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(365, 9);
            this.pictureBox23.TabIndex = 122;
            this.pictureBox23.TabStop = false;
            // 
            // butTimeTable
            // 
            this.butTimeTable.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butTimeTable.Font = new System.Drawing.Font("굴림", 9F);
            this.butTimeTable.Location = new System.Drawing.Point(294, 225);
            this.butTimeTable.Name = "butTimeTable";
            this.butTimeTable.Size = new System.Drawing.Size(89, 22);
            this.butTimeTable.TabIndex = 126;
            this.butTimeTable.Text = "표출범위 설정";
            this.butTimeTable.UseVisualStyleBackColor = true;
            this.butTimeTable.Click += new System.EventHandler(this.butTimeTable_Click);
            // 
            // cheNodeResultView
            // 
            this.cheNodeResultView.AutoSize = true;
            this.cheNodeResultView.Checked = true;
            this.cheNodeResultView.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cheNodeResultView.Location = new System.Drawing.Point(15, 231);
            this.cheNodeResultView.Name = "cheNodeResultView";
            this.cheNodeResultView.Size = new System.Drawing.Size(110, 16);
            this.cheNodeResultView.TabIndex = 127;
            this.cheNodeResultView.Text = "Node 출력 표시";
            this.cheNodeResultView.UseVisualStyleBackColor = true;
            this.cheNodeResultView.CheckedChanged += new System.EventHandler(this.cheNodeResultView_CheckedChanged);
            // 
            // checLinkResultView
            // 
            this.checLinkResultView.AutoSize = true;
            this.checLinkResultView.Checked = true;
            this.checLinkResultView.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checLinkResultView.Location = new System.Drawing.Point(135, 231);
            this.checLinkResultView.Name = "checLinkResultView";
            this.checLinkResultView.Size = new System.Drawing.Size(103, 16);
            this.checLinkResultView.TabIndex = 128;
            this.checLinkResultView.Text = "Link 출력 표시";
            this.checLinkResultView.UseVisualStyleBackColor = true;
            this.checLinkResultView.CheckedChanged += new System.EventHandler(this.checLinkResultView_CheckedChanged);
            // 
            // frmAnalysisResultControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 259);
            this.Controls.Add(this.checLinkResultView);
            this.Controls.Add(this.cheNodeResultView);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.butTimeTable);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox19);
            this.Name = "frmAnalysisResultControl";
            this.Text = "해석결과 출력항목 설정";
            this.Load += new System.EventHandler(this.frmAnalysisResultControl_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label labNodeRange1;
        private System.Windows.Forms.Label labNodeRange2;
        private System.Windows.Forms.Label labNodeRange3;
        private System.Windows.Forms.Label labNodeRange4;
        private System.Windows.Forms.Label labNodeItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label labLinkRange1;
        private System.Windows.Forms.Label labLinkRange2;
        private System.Windows.Forms.Label labLinkRange3;
        private System.Windows.Forms.Label labLinkRange4;
        private System.Windows.Forms.Label labLinkItem;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ComboBox comNodeItem;
        private System.Windows.Forms.ComboBox comLinkItem;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button butTimeTable;
        private System.Windows.Forms.Label labNodeUnit;
        private System.Windows.Forms.Label labLinkUnit;
        private System.Windows.Forms.CheckBox cheNodeResultView;
        private System.Windows.Forms.CheckBox checLinkResultView;

    }
}