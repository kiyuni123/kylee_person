﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WaterNetCore;
using WaterNet.WH_RTAnalysis.work;
using WaterNet.WH_Common.utils;
using WaterNet.WH_Common.work;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmCheckpointManage : Form
    {
        private RealtimeAnalysisResultWork work = null;
        private CommonWork cWork = null;
        public frmWHMain parentForm = null;

        public frmCheckpointManage()
        {
            work = RealtimeAnalysisResultWork.GetInstance();
            cWork = CommonWork.GetInstance();
            InitializeComponent();
            InitializeSetting();
        }

        private void InitializeSetting()
        {
            //실측지점 그리드 초기화
            UltraGridColumn realtimeCheckpointColumn;

            realtimeCheckpointColumn = griRealtimeCheckpointList.DisplayLayout.Bands[0].Columns.Add();
            realtimeCheckpointColumn.Key = "ID";
            realtimeCheckpointColumn.Header.Caption = "ID";
            realtimeCheckpointColumn.CellActivation = Activation.NoEdit;
            realtimeCheckpointColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeCheckpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeCheckpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeCheckpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            realtimeCheckpointColumn.Hidden = false;   //필드 보이기

            realtimeCheckpointColumn = griRealtimeCheckpointList.DisplayLayout.Bands[0].Columns.Add();
            realtimeCheckpointColumn.Key = "METER_TYPE";
            realtimeCheckpointColumn.Header.Caption = "측정기구분";
            realtimeCheckpointColumn.CellActivation = Activation.NoEdit;
            realtimeCheckpointColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeCheckpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeCheckpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeCheckpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            realtimeCheckpointColumn.Hidden = true;   //필드 보이기

            realtimeCheckpointColumn = griRealtimeCheckpointList.DisplayLayout.Bands[0].Columns.Add();
            realtimeCheckpointColumn.Key = "METER_TYPE_NAME";
            realtimeCheckpointColumn.Header.Caption = "측정기구분";
            realtimeCheckpointColumn.CellActivation = Activation.NoEdit;
            realtimeCheckpointColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeCheckpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeCheckpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeCheckpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            realtimeCheckpointColumn.Hidden = false;   //필드 보이기

            realtimeCheckpointColumn = griRealtimeCheckpointList.DisplayLayout.Bands[0].Columns.Add();
            realtimeCheckpointColumn.Key = "FTR_IDN";
            realtimeCheckpointColumn.Header.Caption = "측정기일련번호";
            realtimeCheckpointColumn.CellActivation = Activation.NoEdit;
            realtimeCheckpointColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeCheckpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeCheckpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeCheckpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            realtimeCheckpointColumn.Hidden = false;   //필드 보이기

            realtimeCheckpointColumn = griRealtimeCheckpointList.DisplayLayout.Bands[0].Columns.Add();
            realtimeCheckpointColumn.Key = "TAGNAME";
            realtimeCheckpointColumn.Header.Caption = "태그명";
            realtimeCheckpointColumn.CellActivation = Activation.NoEdit;
            realtimeCheckpointColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeCheckpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeCheckpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeCheckpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            realtimeCheckpointColumn.Hidden = false;   //필드 보이기

            realtimeCheckpointColumn = griRealtimeCheckpointList.DisplayLayout.Bands[0].Columns.Add();
            realtimeCheckpointColumn.Key = "LOC_CODE";
            realtimeCheckpointColumn.Header.Caption = "블록코드";
            realtimeCheckpointColumn.CellActivation = Activation.NoEdit;
            realtimeCheckpointColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeCheckpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeCheckpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeCheckpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            realtimeCheckpointColumn.Hidden = true;   //필드 보이기

            realtimeCheckpointColumn = griRealtimeCheckpointList.DisplayLayout.Bands[0].Columns.Add();
            realtimeCheckpointColumn.Key = "LOC_NAME";
            realtimeCheckpointColumn.Header.Caption = "소블록명";
            realtimeCheckpointColumn.CellActivation = Activation.NoEdit;
            realtimeCheckpointColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeCheckpointColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeCheckpointColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeCheckpointColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            realtimeCheckpointColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griRealtimeCheckpointList);

            //대수용가 초기화
            UltraGridColumn largeConsumerColumn;

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "LOC_NAME";
            largeConsumerColumn.Header.Caption = "소블록명";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Left;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "ID";
            largeConsumerColumn.Header.Caption = "대수용가 ID";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "LCONSUMER_NM";
            largeConsumerColumn.Header.Caption = "대수용가명";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Left;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "APPLY_CONTINUE_TIME";
            largeConsumerColumn.Header.Caption = "적용증가시간";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Right;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "APPLY_INCREASE";
            largeConsumerColumn.Header.Caption = "수수량";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Right;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "APPLY_YN";
            largeConsumerColumn.Header.Caption = "적용여부";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            largeConsumerColumn = griLargeConsumerList.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerColumn.Key = "TAGNAME";
            largeConsumerColumn.Header.Caption = "태그명";
            largeConsumerColumn.CellActivation = Activation.NoEdit;
            largeConsumerColumn.CellClickAction = CellClickAction.RowSelect;
            largeConsumerColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griLargeConsumerList);

            //평균수압지점 초기화
            UltraGridColumn griAveragePressureColumn;

            griAveragePressureColumn = griAveragePressureList.DisplayLayout.Bands[0].Columns.Add();
            griAveragePressureColumn.Key = "LOC_NAME";
            griAveragePressureColumn.Header.Caption = "지역명";
            griAveragePressureColumn.CellActivation = Activation.NoEdit;
            griAveragePressureColumn.CellClickAction = CellClickAction.RowSelect;
            griAveragePressureColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            griAveragePressureColumn.CellAppearance.TextHAlign = HAlign.Left;
            griAveragePressureColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            griAveragePressureColumn.Hidden = false;   //필드 보이기

            griAveragePressureColumn = griAveragePressureList.DisplayLayout.Bands[0].Columns.Add();
            griAveragePressureColumn.Key = "ID";
            griAveragePressureColumn.Header.Caption = "지점번호";
            griAveragePressureColumn.CellActivation = Activation.NoEdit;
            griAveragePressureColumn.CellClickAction = CellClickAction.RowSelect;
            griAveragePressureColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            griAveragePressureColumn.CellAppearance.TextHAlign = HAlign.Left;
            griAveragePressureColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            griAveragePressureColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griAveragePressureList);
        }

        //폼 최초로딩 시 실행
        private void frmCheckpointManage_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)
            //=========================================================지점관리 폼

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간관망해석ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                //실측지점 탭
                this.butAddRealtimePoint.Enabled = false;
                this.butDeleteCheckpoint.Enabled = false;
                this.butSaveCheckpoint.Enabled = false;

                //대수용가 탭
                this.butAddLargeConsumer.Enabled = false;
                this.butDeleteLargeConsumer.Enabled = false;
                this.butSaveLargeConsumer.Enabled = false;

                //평균수압지점 탭
                this.butAddPrePosition.Enabled = false;
                this.butDeletePrePosition.Enabled = false;
                this.butSavePrePosition.Enabled = false;
            }

            //============================================================================

            texInpNumber.Text = parentForm.dummyInpNumber;
            texLCInpNmber.Text = parentForm.dummyInpNumber;

            //측정기 종류코드 조회
            Hashtable codeConditions = new Hashtable();
            codeConditions.Add("PCODE","1003");
            codeConditions.Add("removeAllSelect","Y");

            comMeterType.ValueMember = "CODE";
            comMeterType.DisplayMember = "CODE_NAME";

            comMeterType.DataSource = (cWork.GetCodeList(codeConditions)).Tables["CM_CODE"];

            //실측지점 리스트 조회
            Hashtable conditions = new Hashtable();

            conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

            griRealtimeCheckpointList.DataSource = work.SelectRealtimeCheckpointList(conditions).Tables["WH_MEASURE_CHECKPOINT"];
            FormManager.SetGridStyle_PerformAutoResize(griRealtimeCheckpointList);

            if (griRealtimeCheckpointList.Rows.Count > 0)
            {
                griRealtimeCheckpointList.Rows[0].Selected = true;
                //griRealtimeCheckpointList.ActiveRowScrollRegion.ScrollPosition = 0;

                DataRow row = ((DataRow)((DataTable)griRealtimeCheckpointList.DataSource).Rows[griRealtimeCheckpointList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    comMeterType.Enabled = false;
                }
                else
                {
                    comMeterType.Enabled = true;
                }

                comMeterType.SelectedValue = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("METER_TYPE"));
                texFtrIdn.Text = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("FTR_IDN"));
                texRealtimeId.Text = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("ID"));
                texTagname.Text = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("TAGNAME"));
                textLocName.Text = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("LOC_NAME"));

                comMeterType.Enabled = false;
                //texRealtimeId.ReadOnly = false;
                //texFtrIdn.ReadOnly = false;
                //texTagname.ReadOnly = false;
            }
            else
            {
                //comMeterType.Enabled = true;
                //texRealtimeId.ReadOnly = true;
                //texFtrIdn.ReadOnly = true;
                //texTagname.ReadOnly = true;
            }

            //대수용가 적용여부 Combobox 설정
            object[,] comApplyYnData = new object[3, 2];

            comApplyYnData[0, 0] = "";
            comApplyYnData[0, 1] = "선택";

            comApplyYnData[1, 0] = "Y";
            comApplyYnData[1, 1] = "적용";

            comApplyYnData[2, 0] = "N";
            comApplyYnData[2, 1] = "미적용";

            FormManager.SetComboBoxEX(comApplyYn, comApplyYnData, false);

            //대수용가 리스트 조회
            griLargeConsumerList.DataSource = work.SelectLargeConsumerList(conditions).Tables["WH_LCONSUMER_INFO"];
            FormManager.SetGridStyle_PerformAutoResize(griLargeConsumerList);

            if (griLargeConsumerList.Rows.Count > 0)
            {
                griLargeConsumerList.Rows[0].Selected = true;
                griLargeConsumerList.ActiveRowScrollRegion.ScrollPosition = 0;

                DataRow row = ((DataRow)((DataTable)griLargeConsumerList.DataSource).Rows[griLargeConsumerList.Selected.Rows[0].Index]);

                texLCLocName.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("LOC_NAME"));
                texLCId.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("ID"));
                texLCLconsumerNm.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("LCONSUMER_NM"));
                texLCApplyIncrease.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("APPLY_INCREASE"));
                comApplyYn.SelectedValue = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("APPLY_YN"));
                texLCTagname.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("TAGNAME"));

                if ("".Equals(texLCApplyIncrease.Text))
                {
                    //TAG 정보가 입력된 경우
                    cheUseRealtimeData.Checked = false;
                    butLCTagSelect.Enabled = true;
                }
                else
                {
                    //수수량이 입력된 경우
                    cheUseRealtimeData.Checked = true;
                    butLCTagSelect.Enabled = false;
                }
            }

            //평균수압지점 리스트 조회
            texPreInpNumber.Text = parentForm.dummyInpNumber;

            griAveragePressureList.DataSource = work.SelectAveragePressurePointList(conditions).Tables["WH_AVG_PRES_NODE"];
            FormManager.SetGridStyle_PerformAutoResize(griAveragePressureList);

            if (griAveragePressureList.Rows.Count > 0)
            {
                griAveragePressureList.Rows[0].Selected = true;
                griAveragePressureList.ActiveRowScrollRegion.ScrollPosition = 0;

                DataRow row = ((DataRow)((DataTable)griAveragePressureList.DataSource).Rows[griAveragePressureList.Selected.Rows[0].Index]);

                texPreLocName.Text = Utils.nts(row["LOC_NAME"]);
                texPreId.Text = Utils.nts(row["ID"]);
            }
        }

        //실측지점 추가버튼 클릭 시
        private void butAddRealtimePoint_Click(object sender, EventArgs e)
        {
            int count = ((DataTable)griRealtimeCheckpointList.DataSource).Rows.Count;
            griRealtimeCheckpointList.ActiveRow = null;

            DataRow row = ((DataTable)griRealtimeCheckpointList.DataSource).NewRow();
            ((DataTable)griRealtimeCheckpointList.DataSource).Rows.Add(row);

            griRealtimeCheckpointList.Rows[count].Selected = true;
            griRealtimeCheckpointList.ActiveRowScrollRegion.ScrollRowIntoView(griRealtimeCheckpointList.Rows[count]);

            ((DataTable)griRealtimeCheckpointList.DataSource).Rows[griRealtimeCheckpointList.Selected.Rows[0].Index].SetField("METER_TYPE", "000001");
            ((DataTable)griRealtimeCheckpointList.DataSource).Rows[griRealtimeCheckpointList.Selected.Rows[0].Index].SetField("METER_TYPE_NAME", "유량계"); 
            comMeterType.SelectedValue = "000001";

            comMeterType.Enabled = true;

            griRealtimeCheckpointList.UpdateData();
        }

        //실측지점 저장버튼 클릭 시
        private void butSaveCheckpoint_Click(object sender, EventArgs e)
        {
            if (griRealtimeCheckpointList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griRealtimeCheckpointList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"].ToString()))
                    {
                        MessageBox.Show("ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["FTR_IDN"].ToString()))
                    {
                        MessageBox.Show("측정기 일련번호가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["TAGNAME"].ToString()))
                    {
                        MessageBox.Show("태그명이 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

                    DataSet result = work.SaveRealtimeCheckpoint(conditions);
                    griRealtimeCheckpointList.DataSource = result.Tables["WH_MEASURE_CHECKPOINT"];

                    if (griRealtimeCheckpointList.Rows.Count > 0)
                    {
                        griRealtimeCheckpointList.Rows[0].Selected = true;
                        griRealtimeCheckpointList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //실측지점 삭제버튼 클릭 시
        private void butDeleteCheckpoint_Click(object sender, EventArgs e)
        {
            if (griRealtimeCheckpointList.Selected.Rows.Count != 0)
            {
                int idx = griRealtimeCheckpointList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griRealtimeCheckpointList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griRealtimeCheckpointList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griRealtimeCheckpointList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griRealtimeCheckpointList.DataSource).Rows.Count;

                        griRealtimeCheckpointList.Rows[count - 1].Selected = true;
                        griRealtimeCheckpointList.ActiveRowScrollRegion.ScrollRowIntoView(griRealtimeCheckpointList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();

                        conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
                        conditions.Add("ID", texRealtimeId.Text);
                        conditions.Add("METER_TYPE", comMeterType.SelectedValue);

                        try
                        {
                            DataSet dSet = work.DeleteRealtimeCheckpoint(conditions);
                            griRealtimeCheckpointList.DataSource = dSet.Tables["WH_MEASURE_CHECKPOINT"];

                            if (griRealtimeCheckpointList.Rows.Count > 0)
                            {
                                griRealtimeCheckpointList.Rows[0].Selected = true;
                                griRealtimeCheckpointList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                comMeterType.Enabled = true;
                                //texRealtimeId.ReadOnly = false;

                                comMeterType.SelectedValue = "000001";
                                texRealtimeId.Text = "";
                                texFtrIdn.Text = "";
                                texTagname.Text = "";
                                textLocName.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }

            if (griRealtimeCheckpointList.Rows.Count == 0)
            {
                comMeterType.Enabled = false;
            }
        }

        //계측기 종류 선택 시
        private void comMeterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (griRealtimeCheckpointList.Rows.Count != 0)
            {
                ((DataTable)griRealtimeCheckpointList.DataSource).Rows[griRealtimeCheckpointList.Selected.Rows[0].Index].SetField("METER_TYPE", comMeterType.SelectedValue);
                ((DataTable)griRealtimeCheckpointList.DataSource).Rows[griRealtimeCheckpointList.Selected.Rows[0].Index].SetField("METER_TYPE_NAME", comMeterType.Text);
                griRealtimeCheckpointList.UpdateData();               
            }
        }

        //계측기 일련번호 입력 시
        private void texFtrIdn_TextChanged(object sender, EventArgs e)
        {
            if (griRealtimeCheckpointList.Selected.Rows.Count != 0)
            {
                ((DataTable)griRealtimeCheckpointList.DataSource).Rows[griRealtimeCheckpointList.Selected.Rows[0].Index].SetField("FTR_IDN", texFtrIdn.Text);
                griRealtimeCheckpointList.UpdateData();
            }
        }

        //지점 ID 입력 시
        private void texId_TextChanged(object sender, EventArgs e)
        {
            if (griRealtimeCheckpointList.Selected.Rows.Count != 0)
            {
                ((DataTable)griRealtimeCheckpointList.DataSource).Rows[griRealtimeCheckpointList.Selected.Rows[0].Index].SetField("ID", texRealtimeId.Text);
                griRealtimeCheckpointList.UpdateData();
            }
        }

        //TAG명 입력 시
        private void texTagname_TextChanged(object sender, EventArgs e)
        {
            if (griRealtimeCheckpointList.Selected.Rows.Count != 0)
            {
                ((DataTable)griRealtimeCheckpointList.DataSource).Rows[griRealtimeCheckpointList.Selected.Rows[0].Index].SetField("TAGNAME", texTagname.Text);
                griRealtimeCheckpointList.UpdateData();
            }
        }

        //Form이 닫길때 이벤트
        private void frmCheckpointManage_FormClosed(object sender, FormClosedEventArgs e)
        {
            parentForm.checkpointForm = null;
        }

        //대수용가 추가버튼 클릭 
        private void butAddLargeConsumer_Click(object sender, EventArgs e)
        {
            int count = ((DataTable)griLargeConsumerList.DataSource).Rows.Count;
            griLargeConsumerList.ActiveRow = null;

            DataRow row = ((DataTable)griLargeConsumerList.DataSource).NewRow();
            ((DataTable)griLargeConsumerList.DataSource).Rows.Add(row);

            griLargeConsumerList.Rows[count].Selected = true;
            griLargeConsumerList.ActiveRowScrollRegion.ScrollRowIntoView(griLargeConsumerList.Rows[count]);

            griLargeConsumerList.UpdateData();

            cheUseRealtimeData.Checked = true;
            butTimeTable.Enabled = false;
        }

        //대수용가 삭제버튼 클릭 
        private void butDeleteLargeConsumer_Click(object sender, EventArgs e)
        {
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                int idx = griLargeConsumerList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griLargeConsumerList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griLargeConsumerList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griLargeConsumerList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griLargeConsumerList.DataSource).Rows.Count;

                        griLargeConsumerList.Rows[count - 1].Selected = true;
                        griLargeConsumerList.ActiveRowScrollRegion.ScrollRowIntoView(griLargeConsumerList.Rows[count - 1]);
                    }
                    else
                    {
                        texLCLocName.Text = "";
                        texLCId.Text = "";
                        texLCLconsumerNm.Text = "";
                        texApplyContinueTime.Text = "";
                        texLCApplyIncrease.Text = "";
                        comApplyYn.SelectedValue = "";
                        texLCTagname.Text = "";
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();

                        conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
                        conditions.Add("ID", texLCId.Text);

                        try
                        {
                            DataSet dSet = work.DeleteLargeConsumer(conditions);
                            griLargeConsumerList.DataSource = dSet.Tables["WH_LCONSUMER_INFO"];

                            if (griLargeConsumerList.Rows.Count > 0)
                            {
                                griLargeConsumerList.Rows[0].Selected = true;
                                griLargeConsumerList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texLCLocName.Text = "";
                                texLCId.Text = "";
                                texLCLconsumerNm.Text = "";
                                texApplyContinueTime.Text = "";
                                texLCApplyIncrease.Text = "";
                                comApplyYn.SelectedValue = "";
                                texLCTagname.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //대수용가 저장버튼 클릭
        private void butSaveLargeConsumer_Click(object sender, EventArgs e)
        {
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griLargeConsumerList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"].ToString()))
                    {
                        MessageBox.Show("ID가 선택되지 않은 행이 있습니다. 관망모델에서 절점을 선택하십시오.");
                        return;
                    }

                    if ("".Equals(row["LCONSUMER_NM"].ToString()))
                    {
                        MessageBox.Show("대수용가명이 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["APPLY_CONTINUE_TIME"].ToString()))
                    {
                        MessageBox.Show("적용 증가시간이 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["APPLY_INCREASE"].ToString()))
                    {
                        MessageBox.Show("수수량이 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["APPLY_YN"].ToString()))
                    {
                        MessageBox.Show("적용여부가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

                    DataSet result = work.SaveLargeConsumerList(conditions);
                    griLargeConsumerList.DataSource = result.Tables["WH_LCONSUMER_INFO"];

                    if (griLargeConsumerList.Rows.Count > 0)
                    {
                        griLargeConsumerList.Rows[0].Selected = true;
                        griLargeConsumerList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //대수용가 ID 입력 시
        private void texLCId_TextChanged(object sender, EventArgs e)
        {
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLargeConsumerList.DataSource).Rows[griLargeConsumerList.Selected.Rows[0].Index].SetField("ID", texLCId.Text);
                griLargeConsumerList.UpdateData();
            }
        }

        //대수용가명 입력 시
        private void texLCLconsumerNm_TextChanged(object sender, EventArgs e)
        {
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLargeConsumerList.DataSource).Rows[griLargeConsumerList.Selected.Rows[0].Index].SetField("LCONSUMER_NM", texLCLconsumerNm.Text);
                griLargeConsumerList.UpdateData();
            }
        }

        //적용 증가시간 입력 시
        private void texApplyContinueTime_TextChanged(object sender, EventArgs e)
        {
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLargeConsumerList.DataSource).Rows[griLargeConsumerList.Selected.Rows[0].Index].SetField("APPLY_CONTINUE_TIME", texApplyContinueTime.Text);
                griLargeConsumerList.UpdateData();
            }
        }

        //수수량 입력 시
        private void texLCApplyIncrease_TextChanged(object sender, EventArgs e)
        {
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLargeConsumerList.DataSource).Rows[griLargeConsumerList.Selected.Rows[0].Index].SetField("APPLY_INCREASE", texLCApplyIncrease.Text);
                griLargeConsumerList.UpdateData();
            }
        }

        //적용여부 변경 시
        private void comApplyYn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLargeConsumerList.DataSource).Rows[griLargeConsumerList.Selected.Rows[0].Index].SetField("APPLY_YN", comApplyYn.SelectedValue);
                griLargeConsumerList.UpdateData();
            }
        }

        //TAGNAME 변경 시
        private void texLCTagname_TextChanged(object sender, EventArgs e)
        {
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLargeConsumerList.DataSource).Rows[griLargeConsumerList.Selected.Rows[0].Index].SetField("TAGNAME", texLCTagname.Text);
                griLargeConsumerList.UpdateData();
            }
        }

        //대수용가 타임테이블 화면 호출
        private void butTimeTable_Click(object sender, EventArgs e)
        {
            frmLargeConsumerTimeTable timeTableForm = new frmLargeConsumerTimeTable();
            timeTableForm.parentForm = this;
            timeTableForm.ShowDialog();
        }

        //TAG 선택화면 호출 (대수용가)
        private void butLCTagSelect_Click(object sender, EventArgs e)
        {
            if (!"".Equals(texLCInpNmber.Text) && cheUseRealtimeData.Checked == false)
            {
                frmRealtimeTagList tagList = new frmRealtimeTagList();
                tagList.dummyInpNumber = texLCInpNmber.Text;
                tagList.type = "000003";
                tagList.parentForm = this;
                tagList.ShowDialog();
            }
        }

        //TAG 선택화면 호출 (지점관리)
        private void butTagSelect_Click(object sender, EventArgs e)
        {
            frmRealtimeTagList tagList = new frmRealtimeTagList();
            tagList.dummyInpNumber = texLCInpNmber.Text;
            tagList.parentForm = this;

            if ("000001".Equals(comMeterType.SelectedValue.ToString()))
            {
                //유량계
                tagList.type = "000001";
            }
            else if ("000002".Equals(comMeterType.SelectedValue.ToString()))
            {
                //압력계
                tagList.type = "000002";
            }
            
            tagList.ShowDialog();
        }

        //수수량 수동입력을 check한 경우
        private void cheUseRealtimeData_CheckedChanged(object sender, EventArgs e)
        {
            if (cheUseRealtimeData.Checked)
            {
                texLCApplyIncrease.ReadOnly = false;
                texLCTagname.Text = "";
                butLCTagSelect.Enabled = false;
            }
            else
            {
                texLCApplyIncrease.ReadOnly = true;
                texLCApplyIncrease.Text = "";
                butLCTagSelect.Enabled = true;
            }
        }

        private void griRealtimeCheckpointList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griRealtimeCheckpointList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griRealtimeCheckpointList.DataSource).Rows[griRealtimeCheckpointList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    comMeterType.Enabled = false;
                }
                else
                {
                    comMeterType.Enabled = true;
                }

                comMeterType.SelectedValue = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("METER_TYPE"));
                texFtrIdn.Text = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("FTR_IDN"));
                texRealtimeId.Text = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("ID"));
                texTagname.Text = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("TAGNAME"));
                textLocName.Text = Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("LOC_NAME"));
            }
            else
            {
                comMeterType.Enabled = false;
            }
        }

        //대수용가 그리드 선택변경 시
        private void griLargeConsumerList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griLargeConsumerList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griLargeConsumerList.DataSource).Rows[griLargeConsumerList.Selected.Rows[0].Index]);

                if (row.RowState == DataRowState.Added)
                {
                    butTimeTable.Enabled = false;
                }
                else
                {
                    butTimeTable.Enabled = true;
                }

                texLCLocName.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("LOC_NAME"));
                texLCId.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("ID"));
                texLCLconsumerNm.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("LCONSUMER_NM"));
                texApplyContinueTime.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("APPLY_CONTINUE_TIME"));
                texLCApplyIncrease.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("APPLY_INCREASE"));
                comApplyYn.SelectedValue = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("APPLY_YN"));
                texLCTagname.Text = Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("TAGNAME"));

                if ("".Equals(texLCApplyIncrease.Text))
                {
                    //TAG 정보가 입력된 경우
                    cheUseRealtimeData.Checked = false;
                }
                else
                {
                    //수수량이 입력된 경우
                    cheUseRealtimeData.Checked = true;
                }
            }
        }

        //실측지점 그리드 더블클릭 시
        private void griRealtimeCheckpointList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            //지도 위치이동
            if ("000001".Equals(Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("METER_TYPE"))))
            {
                //유량계인 경우
                parentForm.MoveFocusAt("PIPE", "ID = '" + Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("ID")) + "'");
            }
            else if ("000002".Equals(Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("METER_TYPE"))))
            {
                //압력계인 경우
                parentForm.MoveFocusAt("JUNCTION", "ID = '" + Utils.nts(griRealtimeCheckpointList.Selected.Rows[0].GetCellValue("ID")) + "'");
            }
        }

        //대수용가 그리드 더블클릭 시
        private void griLargeConsumerList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            parentForm.MoveFocusAt("JUNCTION", "ID = '" + Utils.nts(griLargeConsumerList.Selected.Rows[0].GetCellValue("ID")) + "'");
        }

        //평균수압지점 추가
        private void butAddPrePosition_Click(object sender, EventArgs e)
        {
            int count = ((DataTable)griAveragePressureList.DataSource).Rows.Count;
            griAveragePressureList.ActiveRow = null;

            DataRow row = ((DataTable)griAveragePressureList.DataSource).NewRow();
            ((DataTable)griAveragePressureList.DataSource).Rows.Add(row);

            griAveragePressureList.Rows[count].Selected = true;
            griAveragePressureList.ActiveRowScrollRegion.ScrollRowIntoView(griAveragePressureList.Rows[count]);

            griAveragePressureList.UpdateData();
        }

        //평균수압지점 입력 시
        private void texPreId_TextChanged(object sender, EventArgs e)
        {
            if (griAveragePressureList.Selected.Rows.Count != 0)
            {
                ((DataTable)griAveragePressureList.DataSource).Rows[griAveragePressureList.Selected.Rows[0].Index].SetField("ID", texPreId.Text);
                griAveragePressureList.UpdateData();
            }
        }

        //평균수압지점 삭제 시
        private void butDeletePrePosition_Click(object sender, EventArgs e)
        {
            if (griAveragePressureList.Selected.Rows.Count != 0)
            {
                int idx = griAveragePressureList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griAveragePressureList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griAveragePressureList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griAveragePressureList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griAveragePressureList.DataSource).Rows.Count;

                        griAveragePressureList.Rows[count - 1].Selected = true;
                        griAveragePressureList.ActiveRowScrollRegion.ScrollRowIntoView(griAveragePressureList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();

                        conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
                        conditions.Add("ID", texPreId.Text);

                        try
                        {
                            DataSet dSet = work.DeleteAveragePressurePointData(conditions);
                            griAveragePressureList.DataSource = dSet.Tables["WH_AVG_PRES_NODE"];

                            if (griAveragePressureList.Rows.Count > 0)
                            {
                                griAveragePressureList.Rows[0].Selected = true;
                                griAveragePressureList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texPreLocName.Text = "";
                                texPreId.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        private void butSavePrePosition_Click(object sender, EventArgs e)
        {
            if (griAveragePressureList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griAveragePressureList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"].ToString()))
                    {
                        MessageBox.Show("ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

                    DataSet result = work.SaveAveragePressurePointData(conditions);
                    griAveragePressureList.DataSource = result.Tables["WH_AVG_PRES_NODE"];

                    if (griAveragePressureList.Rows.Count > 0)
                    {
                        griAveragePressureList.Rows[0].Selected = true;
                        griAveragePressureList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //평균수압지점 row 변경 시
        private void griAveragePressureList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griAveragePressureList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griAveragePressureList.DataSource).Rows[griAveragePressureList.Selected.Rows[0].Index]);

                texPreLocName.Text = Utils.nts(griAveragePressureList.Selected.Rows[0].GetCellValue("LOC_NAME"));
                texPreId.Text = Utils.nts(griAveragePressureList.Selected.Rows[0].GetCellValue("ID"));
            }
        }

        //평균수압지점 row 더블클릭 시
        private void griAveragePressureList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            parentForm.MoveFocusAt("JUNCTION", "ID = '" + Utils.nts(griAveragePressureList.Selected.Rows[0].GetCellValue("ID")) + "'");
        }
    }
}
