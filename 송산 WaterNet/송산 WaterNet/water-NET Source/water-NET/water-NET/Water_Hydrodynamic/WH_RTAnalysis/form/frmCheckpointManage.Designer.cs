﻿namespace WaterNet.WH_RTAnalysis.form
{
    partial class frmCheckpointManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.butSaveCheckpoint = new System.Windows.Forms.Button();
            this.butDeleteCheckpoint = new System.Windows.Forms.Button();
            this.butAddRealtimePoint = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.texInpNumber = new System.Windows.Forms.TextBox();
            this.texRealtimeId = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.texTagname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comMeterType = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textLocName = new System.Windows.Forms.TextBox();
            this.butTagSelect = new System.Windows.Forms.Button();
            this.texFtrIdn = new System.Windows.Forms.TextBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.griRealtimeCheckpointList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.butTimeTable = new System.Windows.Forms.Button();
            this.butSaveLargeConsumer = new System.Windows.Forms.Button();
            this.butDeleteLargeConsumer = new System.Windows.Forms.Button();
            this.butAddLargeConsumer = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.texLCInpNmber = new System.Windows.Forms.TextBox();
            this.texLCId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.texLCLconsumerNm = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.texLCLocName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.texLCTagname = new System.Windows.Forms.TextBox();
            this.butLCTagSelect = new System.Windows.Forms.Button();
            this.texLCApplyIncrease = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cheUseRealtimeData = new System.Windows.Forms.CheckBox();
            this.comApplyYn = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.texApplyContinueTime = new System.Windows.Forms.TextBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.griLargeConsumerList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.butSavePrePosition = new System.Windows.Forms.Button();
            this.butDeletePrePosition = new System.Windows.Forms.Button();
            this.butAddPrePosition = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.texPreInpNumber = new System.Windows.Forms.TextBox();
            this.texPreId = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.texPreLocName = new System.Windows.Forms.TextBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.griAveragePressureList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.tabCheckpoint = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ultraTabPageControl1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeCheckpointList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griLargeConsumerList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griAveragePressureList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabCheckpoint)).BeginInit();
            this.tabCheckpoint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.groupBox1);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox7);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox6);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox5);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox4);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(768, 522);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Controls.Add(this.pictureBox13);
            this.groupBox1.Controls.Add(this.griRealtimeCheckpointList);
            this.groupBox1.Controls.Add(this.pictureBox12);
            this.groupBox1.Controls.Add(this.pictureBox11);
            this.groupBox1.Controls.Add(this.pictureBox10);
            this.groupBox1.Controls.Add(this.pictureBox9);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(748, 504);
            this.groupBox1.TabIndex = 122;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "실측지점";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel2.Controls.Add(this.butSaveCheckpoint, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.butDeleteCheckpoint, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.butAddRealtimePoint, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(13, 407);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(722, 30);
            this.tableLayoutPanel2.TabIndex = 125;
            // 
            // butSaveCheckpoint
            // 
            this.butSaveCheckpoint.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSaveCheckpoint.Font = new System.Drawing.Font("굴림", 9F);
            this.butSaveCheckpoint.Location = new System.Drawing.Point(676, 4);
            this.butSaveCheckpoint.Name = "butSaveCheckpoint";
            this.butSaveCheckpoint.Size = new System.Drawing.Size(43, 22);
            this.butSaveCheckpoint.TabIndex = 29;
            this.butSaveCheckpoint.Text = "저장";
            this.butSaveCheckpoint.UseVisualStyleBackColor = true;
            this.butSaveCheckpoint.Click += new System.EventHandler(this.butSaveCheckpoint_Click);
            // 
            // butDeleteCheckpoint
            // 
            this.butDeleteCheckpoint.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butDeleteCheckpoint.Font = new System.Drawing.Font("굴림", 9F);
            this.butDeleteCheckpoint.Location = new System.Drawing.Point(625, 4);
            this.butDeleteCheckpoint.Name = "butDeleteCheckpoint";
            this.butDeleteCheckpoint.Size = new System.Drawing.Size(43, 22);
            this.butDeleteCheckpoint.TabIndex = 30;
            this.butDeleteCheckpoint.Text = "삭제";
            this.butDeleteCheckpoint.UseVisualStyleBackColor = true;
            this.butDeleteCheckpoint.Click += new System.EventHandler(this.butDeleteCheckpoint_Click);
            // 
            // butAddRealtimePoint
            // 
            this.butAddRealtimePoint.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butAddRealtimePoint.Font = new System.Drawing.Font("굴림", 9F);
            this.butAddRealtimePoint.Location = new System.Drawing.Point(574, 4);
            this.butAddRealtimePoint.Name = "butAddRealtimePoint";
            this.butAddRealtimePoint.Size = new System.Drawing.Size(43, 22);
            this.butAddRealtimePoint.TabIndex = 31;
            this.butAddRealtimePoint.Text = "추가";
            this.butAddRealtimePoint.UseVisualStyleBackColor = true;
            this.butAddRealtimePoint.Click += new System.EventHandler(this.butAddRealtimePoint_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.48199F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.45707F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.40443F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.3795F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.texInpNumber, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.texRealtimeId, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.texTagname, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.comMeterType, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.textLocName, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.butTagSelect, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.texFtrIdn, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 285);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(722, 122);
            this.tableLayoutPanel1.TabIndex = 124;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F);
            this.label3.Location = new System.Drawing.Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 27;
            this.label3.Text = "INP 번호";
            // 
            // texInpNumber
            // 
            this.texInpNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texInpNumber.Font = new System.Drawing.Font("굴림", 9F);
            this.texInpNumber.Location = new System.Drawing.Point(122, 4);
            this.texInpNumber.Name = "texInpNumber";
            this.texInpNumber.ReadOnly = true;
            this.texInpNumber.Size = new System.Drawing.Size(150, 21);
            this.texInpNumber.TabIndex = 34;
            // 
            // texRealtimeId
            // 
            this.texRealtimeId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texRealtimeId.Font = new System.Drawing.Font("굴림", 9F);
            this.texRealtimeId.Location = new System.Drawing.Point(122, 34);
            this.texRealtimeId.Name = "texRealtimeId";
            this.texRealtimeId.ReadOnly = true;
            this.texRealtimeId.Size = new System.Drawing.Size(100, 21);
            this.texRealtimeId.TabIndex = 36;
            this.texRealtimeId.TextChanged += new System.EventHandler(this.texId_TextChanged);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F);
            this.label5.Location = new System.Drawing.Point(3, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 12);
            this.label5.TabIndex = 32;
            this.label5.Text = "지점 ID";
            // 
            // texTagname
            // 
            this.texTagname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.texTagname, 2);
            this.texTagname.Font = new System.Drawing.Font("굴림", 9F);
            this.texTagname.Location = new System.Drawing.Point(122, 95);
            this.texTagname.Name = "texTagname";
            this.texTagname.ReadOnly = true;
            this.texTagname.Size = new System.Drawing.Size(354, 21);
            this.texTagname.TabIndex = 37;
            this.texTagname.TextChanged += new System.EventHandler(this.texTagname_TextChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F);
            this.label4.Location = new System.Drawing.Point(3, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 12);
            this.label4.TabIndex = 31;
            this.label4.Text = "TAG명";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.Location = new System.Drawing.Point(3, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 12);
            this.label1.TabIndex = 30;
            this.label1.Text = "측정기 일련번호";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F);
            this.label2.Location = new System.Drawing.Point(378, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 12);
            this.label2.TabIndex = 29;
            this.label2.Text = "측정기 구분";
            // 
            // comMeterType
            // 
            this.comMeterType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comMeterType.Font = new System.Drawing.Font("굴림", 9F);
            this.comMeterType.FormattingEnabled = true;
            this.comMeterType.Location = new System.Drawing.Point(482, 35);
            this.comMeterType.Name = "comMeterType";
            this.comMeterType.Size = new System.Drawing.Size(100, 20);
            this.comMeterType.TabIndex = 33;
            this.comMeterType.SelectedIndexChanged += new System.EventHandler(this.comMeterType_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 9F);
            this.label13.Location = new System.Drawing.Point(378, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 43;
            this.label13.Text = "소블록명";
            // 
            // textLocName
            // 
            this.textLocName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textLocName.Font = new System.Drawing.Font("굴림", 9F);
            this.textLocName.Location = new System.Drawing.Point(482, 4);
            this.textLocName.Name = "textLocName";
            this.textLocName.ReadOnly = true;
            this.textLocName.Size = new System.Drawing.Size(100, 21);
            this.textLocName.TabIndex = 44;
            // 
            // butTagSelect
            // 
            this.butTagSelect.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butTagSelect.Font = new System.Drawing.Font("굴림", 9F);
            this.butTagSelect.Location = new System.Drawing.Point(482, 95);
            this.butTagSelect.Name = "butTagSelect";
            this.butTagSelect.Size = new System.Drawing.Size(82, 22);
            this.butTagSelect.TabIndex = 45;
            this.butTagSelect.Text = "TAG 선택";
            this.butTagSelect.UseVisualStyleBackColor = true;
            this.butTagSelect.Click += new System.EventHandler(this.butTagSelect_Click);
            // 
            // texFtrIdn
            // 
            this.texFtrIdn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texFtrIdn.Font = new System.Drawing.Font("굴림", 9F);
            this.texFtrIdn.Location = new System.Drawing.Point(122, 64);
            this.texFtrIdn.Name = "texFtrIdn";
            this.texFtrIdn.Size = new System.Drawing.Size(100, 21);
            this.texFtrIdn.TabIndex = 35;
            this.texFtrIdn.TextChanged += new System.EventHandler(this.texFtrIdn_TextChanged);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox13.Location = new System.Drawing.Point(13, 276);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(722, 9);
            this.pictureBox13.TabIndex = 123;
            this.pictureBox13.TabStop = false;
            // 
            // griRealtimeCheckpointList
            // 
            this.griRealtimeCheckpointList.Dock = System.Windows.Forms.DockStyle.Top;
            this.griRealtimeCheckpointList.Location = new System.Drawing.Point(13, 26);
            this.griRealtimeCheckpointList.Name = "griRealtimeCheckpointList";
            this.griRealtimeCheckpointList.Size = new System.Drawing.Size(722, 250);
            this.griRealtimeCheckpointList.TabIndex = 122;
            this.griRealtimeCheckpointList.Text = "ultraGrid1";
            this.griRealtimeCheckpointList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griRealtimeCheckpointList_DoubleClickRow);
            this.griRealtimeCheckpointList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griRealtimeCheckpointList_AfterSelectChange);
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox12.Location = new System.Drawing.Point(735, 26);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(10, 466);
            this.pictureBox12.TabIndex = 120;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox11.Location = new System.Drawing.Point(3, 26);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(10, 466);
            this.pictureBox11.TabIndex = 119;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox10.Location = new System.Drawing.Point(3, 492);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(742, 9);
            this.pictureBox10.TabIndex = 117;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox9.Location = new System.Drawing.Point(3, 17);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(742, 9);
            this.pictureBox9.TabIndex = 116;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox7.Location = new System.Drawing.Point(758, 9);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 504);
            this.pictureBox7.TabIndex = 119;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox6.Location = new System.Drawing.Point(0, 9);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(10, 504);
            this.pictureBox6.TabIndex = 118;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(0, 513);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(768, 9);
            this.pictureBox5.TabIndex = 116;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(768, 9);
            this.pictureBox4.TabIndex = 115;
            this.pictureBox4.TabStop = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.groupBox2);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox17);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox16);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox15);
            this.ultraTabPageControl2.Controls.Add(this.pictureBox14);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(768, 522);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel3);
            this.groupBox2.Controls.Add(this.tableLayoutPanel4);
            this.groupBox2.Controls.Add(this.pictureBox8);
            this.groupBox2.Controls.Add(this.griLargeConsumerList);
            this.groupBox2.Controls.Add(this.pictureBox18);
            this.groupBox2.Controls.Add(this.pictureBox19);
            this.groupBox2.Controls.Add(this.pictureBox20);
            this.groupBox2.Controls.Add(this.pictureBox21);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(10, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(748, 504);
            this.groupBox2.TabIndex = 123;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "대수용가";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel3.Controls.Add(this.butTimeTable, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.butSaveLargeConsumer, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.butDeleteLargeConsumer, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.butAddLargeConsumer, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(13, 434);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(722, 30);
            this.tableLayoutPanel3.TabIndex = 125;
            // 
            // butTimeTable
            // 
            this.butTimeTable.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butTimeTable.Font = new System.Drawing.Font("굴림", 9F);
            this.butTimeTable.Location = new System.Drawing.Point(484, 4);
            this.butTimeTable.Name = "butTimeTable";
            this.butTimeTable.Size = new System.Drawing.Size(82, 22);
            this.butTimeTable.TabIndex = 32;
            this.butTimeTable.Text = "동작설정";
            this.butTimeTable.UseVisualStyleBackColor = true;
            this.butTimeTable.Click += new System.EventHandler(this.butTimeTable_Click);
            // 
            // butSaveLargeConsumer
            // 
            this.butSaveLargeConsumer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSaveLargeConsumer.Font = new System.Drawing.Font("굴림", 9F);
            this.butSaveLargeConsumer.Location = new System.Drawing.Point(676, 4);
            this.butSaveLargeConsumer.Name = "butSaveLargeConsumer";
            this.butSaveLargeConsumer.Size = new System.Drawing.Size(43, 22);
            this.butSaveLargeConsumer.TabIndex = 29;
            this.butSaveLargeConsumer.Text = "저장";
            this.butSaveLargeConsumer.UseVisualStyleBackColor = true;
            this.butSaveLargeConsumer.Click += new System.EventHandler(this.butSaveLargeConsumer_Click);
            // 
            // butDeleteLargeConsumer
            // 
            this.butDeleteLargeConsumer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butDeleteLargeConsumer.Font = new System.Drawing.Font("굴림", 9F);
            this.butDeleteLargeConsumer.Location = new System.Drawing.Point(625, 4);
            this.butDeleteLargeConsumer.Name = "butDeleteLargeConsumer";
            this.butDeleteLargeConsumer.Size = new System.Drawing.Size(43, 22);
            this.butDeleteLargeConsumer.TabIndex = 30;
            this.butDeleteLargeConsumer.Text = "삭제";
            this.butDeleteLargeConsumer.UseVisualStyleBackColor = true;
            this.butDeleteLargeConsumer.Click += new System.EventHandler(this.butDeleteLargeConsumer_Click);
            // 
            // butAddLargeConsumer
            // 
            this.butAddLargeConsumer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butAddLargeConsumer.Font = new System.Drawing.Font("굴림", 9F);
            this.butAddLargeConsumer.Location = new System.Drawing.Point(574, 4);
            this.butAddLargeConsumer.Name = "butAddLargeConsumer";
            this.butAddLargeConsumer.Size = new System.Drawing.Size(43, 22);
            this.butAddLargeConsumer.TabIndex = 31;
            this.butAddLargeConsumer.Text = "추가";
            this.butAddLargeConsumer.UseVisualStyleBackColor = true;
            this.butAddLargeConsumer.Click += new System.EventHandler(this.butAddLargeConsumer_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.48199F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.45707F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.40443F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.3795F));
            this.tableLayoutPanel4.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.texLCInpNmber, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.texLCId, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label10, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.texLCLconsumerNm, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.label12, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.texLCLocName, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.label11, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.texLCTagname, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.butLCTagSelect, 3, 4);
            this.tableLayoutPanel4.Controls.Add(this.texLCApplyIncrease, 3, 3);
            this.tableLayoutPanel4.Controls.Add(this.label9, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.label14, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.cheUseRealtimeData, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.comApplyYn, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.label8, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.label17, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.texApplyContinueTime, 1, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(13, 285);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(722, 149);
            this.tableLayoutPanel4.TabIndex = 124;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F);
            this.label6.Location = new System.Drawing.Point(3, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 27;
            this.label6.Text = "INP 번호";
            // 
            // texLCInpNmber
            // 
            this.texLCInpNmber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texLCInpNmber.Font = new System.Drawing.Font("굴림", 9F);
            this.texLCInpNmber.Location = new System.Drawing.Point(122, 4);
            this.texLCInpNmber.Name = "texLCInpNmber";
            this.texLCInpNmber.ReadOnly = true;
            this.texLCInpNmber.Size = new System.Drawing.Size(150, 21);
            this.texLCInpNmber.TabIndex = 34;
            // 
            // texLCId
            // 
            this.texLCId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texLCId.Font = new System.Drawing.Font("굴림", 9F);
            this.texLCId.Location = new System.Drawing.Point(122, 34);
            this.texLCId.Name = "texLCId";
            this.texLCId.ReadOnly = true;
            this.texLCId.Size = new System.Drawing.Size(100, 21);
            this.texLCId.TabIndex = 36;
            this.texLCId.TextChanged += new System.EventHandler(this.texLCId_TextChanged);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F);
            this.label7.Location = new System.Drawing.Point(3, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "대수용가 ID";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F);
            this.label10.Location = new System.Drawing.Point(378, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 32;
            this.label10.Text = "대수용가명";
            // 
            // texLCLconsumerNm
            // 
            this.texLCLconsumerNm.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texLCLconsumerNm.Font = new System.Drawing.Font("굴림", 9F);
            this.texLCLconsumerNm.Location = new System.Drawing.Point(482, 34);
            this.texLCLconsumerNm.Name = "texLCLconsumerNm";
            this.texLCLconsumerNm.Size = new System.Drawing.Size(100, 21);
            this.texLCLconsumerNm.TabIndex = 38;
            this.texLCLconsumerNm.TextChanged += new System.EventHandler(this.texLCLconsumerNm_TextChanged);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 9F);
            this.label12.Location = new System.Drawing.Point(378, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 42;
            this.label12.Text = "소블록명";
            // 
            // texLCLocName
            // 
            this.texLCLocName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texLCLocName.Font = new System.Drawing.Font("굴림", 9F);
            this.texLCLocName.Location = new System.Drawing.Point(482, 4);
            this.texLCLocName.Name = "texLCLocName";
            this.texLCLocName.ReadOnly = true;
            this.texLCLocName.Size = new System.Drawing.Size(100, 21);
            this.texLCLocName.TabIndex = 43;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F);
            this.label11.Location = new System.Drawing.Point(3, 127);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 12);
            this.label11.TabIndex = 39;
            this.label11.Text = "TAG명";
            // 
            // texLCTagname
            // 
            this.texLCTagname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel4.SetColumnSpan(this.texLCTagname, 2);
            this.texLCTagname.Font = new System.Drawing.Font("굴림", 9F);
            this.texLCTagname.Location = new System.Drawing.Point(122, 123);
            this.texLCTagname.Name = "texLCTagname";
            this.texLCTagname.ReadOnly = true;
            this.texLCTagname.Size = new System.Drawing.Size(354, 21);
            this.texLCTagname.TabIndex = 40;
            this.texLCTagname.TextChanged += new System.EventHandler(this.texLCTagname_TextChanged);
            // 
            // butLCTagSelect
            // 
            this.butLCTagSelect.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.butLCTagSelect.Font = new System.Drawing.Font("굴림", 9F);
            this.butLCTagSelect.Location = new System.Drawing.Point(482, 122);
            this.butLCTagSelect.Name = "butLCTagSelect";
            this.butLCTagSelect.Size = new System.Drawing.Size(82, 22);
            this.butLCTagSelect.TabIndex = 44;
            this.butLCTagSelect.Text = "TAG 선택";
            this.butLCTagSelect.UseVisualStyleBackColor = true;
            this.butLCTagSelect.Click += new System.EventHandler(this.butLCTagSelect_Click);
            // 
            // texLCApplyIncrease
            // 
            this.texLCApplyIncrease.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texLCApplyIncrease.Font = new System.Drawing.Font("굴림", 9F);
            this.texLCApplyIncrease.Location = new System.Drawing.Point(482, 93);
            this.texLCApplyIncrease.Name = "texLCApplyIncrease";
            this.texLCApplyIncrease.Size = new System.Drawing.Size(100, 21);
            this.texLCApplyIncrease.TabIndex = 35;
            this.texLCApplyIncrease.TextChanged += new System.EventHandler(this.texLCApplyIncrease_TextChanged);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 9F);
            this.label9.Location = new System.Drawing.Point(378, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 12);
            this.label9.TabIndex = 30;
            this.label9.Text = "수수량 (cmh)";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 9F);
            this.label14.Location = new System.Drawing.Point(3, 98);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(93, 12);
            this.label14.TabIndex = 127;
            this.label14.Text = "수수량 수동입력";
            // 
            // cheUseRealtimeData
            // 
            this.cheUseRealtimeData.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cheUseRealtimeData.AutoSize = true;
            this.cheUseRealtimeData.Location = new System.Drawing.Point(122, 97);
            this.cheUseRealtimeData.Name = "cheUseRealtimeData";
            this.cheUseRealtimeData.Size = new System.Drawing.Size(15, 14);
            this.cheUseRealtimeData.TabIndex = 126;
            this.cheUseRealtimeData.UseVisualStyleBackColor = true;
            this.cheUseRealtimeData.CheckedChanged += new System.EventHandler(this.cheUseRealtimeData_CheckedChanged);
            // 
            // comApplyYn
            // 
            this.comApplyYn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comApplyYn.Font = new System.Drawing.Font("굴림", 9F);
            this.comApplyYn.FormattingEnabled = true;
            this.comApplyYn.Location = new System.Drawing.Point(482, 65);
            this.comApplyYn.Name = "comApplyYn";
            this.comApplyYn.Size = new System.Drawing.Size(100, 20);
            this.comApplyYn.TabIndex = 41;
            this.comApplyYn.SelectedIndexChanged += new System.EventHandler(this.comApplyYn_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F);
            this.label8.Location = new System.Drawing.Point(378, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 31;
            this.label8.Text = "적용여부";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 69);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(103, 12);
            this.label17.TabIndex = 128;
            this.label17.Text = "적용 증가시간(분)";
            // 
            // texApplyContinueTime
            // 
            this.texApplyContinueTime.AcceptsReturn = true;
            this.texApplyContinueTime.Location = new System.Drawing.Point(122, 63);
            this.texApplyContinueTime.Name = "texApplyContinueTime";
            this.texApplyContinueTime.Size = new System.Drawing.Size(100, 21);
            this.texApplyContinueTime.TabIndex = 129;
            this.texApplyContinueTime.TextChanged += new System.EventHandler(this.texApplyContinueTime_TextChanged);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox8.Location = new System.Drawing.Point(13, 276);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(722, 9);
            this.pictureBox8.TabIndex = 123;
            this.pictureBox8.TabStop = false;
            // 
            // griLargeConsumerList
            // 
            this.griLargeConsumerList.Dock = System.Windows.Forms.DockStyle.Top;
            this.griLargeConsumerList.Location = new System.Drawing.Point(13, 26);
            this.griLargeConsumerList.Name = "griLargeConsumerList";
            this.griLargeConsumerList.Size = new System.Drawing.Size(722, 250);
            this.griLargeConsumerList.TabIndex = 122;
            this.griLargeConsumerList.Text = "ultraGrid1";
            this.griLargeConsumerList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griLargeConsumerList_DoubleClickRow);
            this.griLargeConsumerList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griLargeConsumerList_AfterSelectChange);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox18.Location = new System.Drawing.Point(735, 26);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(10, 466);
            this.pictureBox18.TabIndex = 120;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox19.Location = new System.Drawing.Point(3, 26);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(10, 466);
            this.pictureBox19.TabIndex = 119;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox20.Location = new System.Drawing.Point(3, 492);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(742, 9);
            this.pictureBox20.TabIndex = 117;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox21.Location = new System.Drawing.Point(3, 17);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(742, 9);
            this.pictureBox21.TabIndex = 116;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox17.Location = new System.Drawing.Point(758, 9);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(10, 504);
            this.pictureBox17.TabIndex = 120;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox16.Location = new System.Drawing.Point(0, 9);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(10, 504);
            this.pictureBox16.TabIndex = 119;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox15.Location = new System.Drawing.Point(0, 513);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(768, 9);
            this.pictureBox15.TabIndex = 117;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox14.Location = new System.Drawing.Point(0, 0);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(768, 9);
            this.pictureBox14.TabIndex = 116;
            this.pictureBox14.TabStop = false;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.groupBox3);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox25);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox24);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox23);
            this.ultraTabPageControl3.Controls.Add(this.pictureBox22);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(768, 522);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel5);
            this.groupBox3.Controls.Add(this.tableLayoutPanel6);
            this.groupBox3.Controls.Add(this.pictureBox26);
            this.groupBox3.Controls.Add(this.griAveragePressureList);
            this.groupBox3.Controls.Add(this.pictureBox27);
            this.groupBox3.Controls.Add(this.pictureBox28);
            this.groupBox3.Controls.Add(this.pictureBox29);
            this.groupBox3.Controls.Add(this.pictureBox30);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(10, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(748, 504);
            this.groupBox3.TabIndex = 124;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "평균수압지점";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel5.Controls.Add(this.butSavePrePosition, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.butDeletePrePosition, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.butAddPrePosition, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(13, 434);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(722, 30);
            this.tableLayoutPanel5.TabIndex = 125;
            // 
            // butSavePrePosition
            // 
            this.butSavePrePosition.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSavePrePosition.Font = new System.Drawing.Font("굴림", 9F);
            this.butSavePrePosition.Location = new System.Drawing.Point(676, 4);
            this.butSavePrePosition.Name = "butSavePrePosition";
            this.butSavePrePosition.Size = new System.Drawing.Size(43, 22);
            this.butSavePrePosition.TabIndex = 29;
            this.butSavePrePosition.Text = "저장";
            this.butSavePrePosition.UseVisualStyleBackColor = true;
            this.butSavePrePosition.Click += new System.EventHandler(this.butSavePrePosition_Click);
            // 
            // butDeletePrePosition
            // 
            this.butDeletePrePosition.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butDeletePrePosition.Font = new System.Drawing.Font("굴림", 9F);
            this.butDeletePrePosition.Location = new System.Drawing.Point(625, 4);
            this.butDeletePrePosition.Name = "butDeletePrePosition";
            this.butDeletePrePosition.Size = new System.Drawing.Size(43, 22);
            this.butDeletePrePosition.TabIndex = 30;
            this.butDeletePrePosition.Text = "삭제";
            this.butDeletePrePosition.UseVisualStyleBackColor = true;
            this.butDeletePrePosition.Click += new System.EventHandler(this.butDeletePrePosition_Click);
            // 
            // butAddPrePosition
            // 
            this.butAddPrePosition.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butAddPrePosition.Font = new System.Drawing.Font("굴림", 9F);
            this.butAddPrePosition.Location = new System.Drawing.Point(574, 4);
            this.butAddPrePosition.Name = "butAddPrePosition";
            this.butAddPrePosition.Size = new System.Drawing.Size(43, 22);
            this.butAddPrePosition.TabIndex = 31;
            this.butAddPrePosition.Text = "추가";
            this.butAddPrePosition.UseVisualStyleBackColor = true;
            this.butAddPrePosition.Click += new System.EventHandler(this.butAddPrePosition_Click);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.48199F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.45707F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.40443F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.3795F));
            this.tableLayoutPanel6.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.texPreInpNumber, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.texPreId, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.label16, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label18, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.texPreLocName, 3, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(13, 285);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 5;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(722, 149);
            this.tableLayoutPanel6.TabIndex = 124;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 9F);
            this.label15.Location = new System.Drawing.Point(3, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 27;
            this.label15.Text = "INP 번호";
            // 
            // texPreInpNumber
            // 
            this.texPreInpNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texPreInpNumber.Font = new System.Drawing.Font("굴림", 9F);
            this.texPreInpNumber.Location = new System.Drawing.Point(122, 4);
            this.texPreInpNumber.Name = "texPreInpNumber";
            this.texPreInpNumber.ReadOnly = true;
            this.texPreInpNumber.Size = new System.Drawing.Size(150, 21);
            this.texPreInpNumber.TabIndex = 34;
            // 
            // texPreId
            // 
            this.texPreId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texPreId.Font = new System.Drawing.Font("굴림", 9F);
            this.texPreId.Location = new System.Drawing.Point(122, 34);
            this.texPreId.Name = "texPreId";
            this.texPreId.ReadOnly = true;
            this.texPreId.Size = new System.Drawing.Size(100, 21);
            this.texPreId.TabIndex = 36;
            this.texPreId.TextChanged += new System.EventHandler(this.texPreId_TextChanged);
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F);
            this.label16.Location = new System.Drawing.Point(3, 39);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 12);
            this.label16.TabIndex = 29;
            this.label16.Text = "평균수압지점 ID";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 9F);
            this.label18.Location = new System.Drawing.Point(378, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 42;
            this.label18.Text = "지역명";
            // 
            // texPreLocName
            // 
            this.texPreLocName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texPreLocName.Font = new System.Drawing.Font("굴림", 9F);
            this.texPreLocName.Location = new System.Drawing.Point(482, 4);
            this.texPreLocName.Name = "texPreLocName";
            this.texPreLocName.ReadOnly = true;
            this.texPreLocName.Size = new System.Drawing.Size(100, 21);
            this.texPreLocName.TabIndex = 43;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox26.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox26.Location = new System.Drawing.Point(13, 276);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(722, 9);
            this.pictureBox26.TabIndex = 123;
            this.pictureBox26.TabStop = false;
            // 
            // griAveragePressureList
            // 
            this.griAveragePressureList.Dock = System.Windows.Forms.DockStyle.Top;
            this.griAveragePressureList.Location = new System.Drawing.Point(13, 26);
            this.griAveragePressureList.Name = "griAveragePressureList";
            this.griAveragePressureList.Size = new System.Drawing.Size(722, 250);
            this.griAveragePressureList.TabIndex = 122;
            this.griAveragePressureList.Text = "ultraGrid1";
            this.griAveragePressureList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griAveragePressureList_DoubleClickRow);
            this.griAveragePressureList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griAveragePressureList_AfterSelectChange);
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox27.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox27.Location = new System.Drawing.Point(735, 26);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(10, 466);
            this.pictureBox27.TabIndex = 120;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox28.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox28.Location = new System.Drawing.Point(3, 26);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(10, 466);
            this.pictureBox28.TabIndex = 119;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox29.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox29.Location = new System.Drawing.Point(3, 492);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(742, 9);
            this.pictureBox29.TabIndex = 117;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox30.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox30.Location = new System.Drawing.Point(3, 17);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(742, 9);
            this.pictureBox30.TabIndex = 116;
            this.pictureBox30.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox25.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox25.Location = new System.Drawing.Point(758, 9);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(10, 504);
            this.pictureBox25.TabIndex = 121;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox24.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox24.Location = new System.Drawing.Point(0, 9);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(10, 504);
            this.pictureBox24.TabIndex = 120;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox23.Location = new System.Drawing.Point(0, 513);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(768, 9);
            this.pictureBox23.TabIndex = 118;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox22.Location = new System.Drawing.Point(0, 0);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(768, 9);
            this.pictureBox22.TabIndex = 117;
            this.pictureBox22.TabStop = false;
            // 
            // tabCheckpoint
            // 
            this.tabCheckpoint.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabCheckpoint.Controls.Add(this.ultraTabPageControl1);
            this.tabCheckpoint.Controls.Add(this.ultraTabPageControl2);
            this.tabCheckpoint.Controls.Add(this.ultraTabPageControl3);
            this.tabCheckpoint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCheckpoint.Location = new System.Drawing.Point(10, 9);
            this.tabCheckpoint.Name = "tabCheckpoint";
            this.tabCheckpoint.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabCheckpoint.Size = new System.Drawing.Size(772, 548);
            this.tabCheckpoint.TabIndex = 119;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "실측지점";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "대수용가";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "평균수압지점";
            this.tabCheckpoint.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(768, 522);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(782, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 548);
            this.pictureBox3.TabIndex = 118;
            this.pictureBox3.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 548);
            this.picFrLeftM1.TabIndex = 117;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 557);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(792, 9);
            this.pictureBox1.TabIndex = 116;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(792, 9);
            this.pictureBox2.TabIndex = 115;
            this.pictureBox2.TabStop = false;
            // 
            // frmCheckpointManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 566);
            this.Controls.Add(this.tabCheckpoint);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCheckpointManage";
            this.Text = "지점관리";
            this.Load += new System.EventHandler(this.frmCheckpointManage_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmCheckpointManage_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griRealtimeCheckpointList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griLargeConsumerList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griAveragePressureList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabCheckpoint)).EndInit();
            this.tabCheckpoint.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox texFtrIdn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button butSaveCheckpoint;
        private System.Windows.Forms.Button butDeleteCheckpoint;
        private System.Windows.Forms.Button butAddRealtimePoint;
        public System.Windows.Forms.ComboBox comMeterType;
        public Infragistics.Win.UltraWinTabControl.UltraTabControl tabCheckpoint;
        public System.Windows.Forms.TextBox texRealtimeId;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button butSaveLargeConsumer;
        private System.Windows.Forms.Button butDeleteLargeConsumer;
        private System.Windows.Forms.Button butAddLargeConsumer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox texLCInpNmber;
        private System.Windows.Forms.TextBox texLCApplyIncrease;
        public System.Windows.Forms.TextBox texLCId;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        public System.Windows.Forms.TextBox texLCLconsumerNm;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.ComboBox comApplyYn;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox texLCLocName;
        private System.Windows.Forms.Button butTimeTable;
        public Infragistics.Win.UltraWinGrid.UltraGrid griRealtimeCheckpointList;
        public Infragistics.Win.UltraWinGrid.UltraGrid griLargeConsumerList;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox textLocName;
        public System.Windows.Forms.TextBox texInpNumber;
        private System.Windows.Forms.Button butLCTagSelect;
        public System.Windows.Forms.TextBox texLCTagname;
        private System.Windows.Forms.Button butTagSelect;
        public System.Windows.Forms.TextBox texTagname;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox cheUseRealtimeData;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button butSavePrePosition;
        private System.Windows.Forms.Button butDeletePrePosition;
        private System.Windows.Forms.Button butAddPrePosition;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox texPreInpNumber;
        public System.Windows.Forms.TextBox texPreId;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox texPreLocName;
        private System.Windows.Forms.PictureBox pictureBox26;
        public Infragistics.Win.UltraWinGrid.UltraGrid griAveragePressureList;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox texApplyContinueTime;
    }
}