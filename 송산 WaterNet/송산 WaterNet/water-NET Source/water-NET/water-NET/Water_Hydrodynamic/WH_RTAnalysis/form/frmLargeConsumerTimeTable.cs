﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WH_RTAnalysis.form;
using WaterNet.WH_RTAnalysis.work;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmLargeConsumerTimeTable : Form
    {
        public frmCheckpointManage parentForm = null;
        private RealtimeAnalysisResultWork work = null;

        public frmLargeConsumerTimeTable()
        {
            work = RealtimeAnalysisResultWork.GetInstance();
            InitializeComponent();
            InitializeSetting();
        }

        private void InitializeSetting()
        {
            //실측지점 그리드 초기화
            UltraGridColumn largeConsumerTimeTableColumn;

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "WEEKDAY_TEXT";
            largeConsumerTimeTableColumn.Header.Caption = "요일";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "WEEKDAY";
            largeConsumerTimeTableColumn.Header.Caption = "WEEKDAY";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = true;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H01";
            largeConsumerTimeTableColumn.Header.Caption = "1시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H02";
            largeConsumerTimeTableColumn.Header.Caption = "2시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H03";
            largeConsumerTimeTableColumn.Header.Caption = "3시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H04";
            largeConsumerTimeTableColumn.Header.Caption = "4시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H05";
            largeConsumerTimeTableColumn.Header.Caption = "5시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H06";
            largeConsumerTimeTableColumn.Header.Caption = "6시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H07";
            largeConsumerTimeTableColumn.Header.Caption = "7시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H08";
            largeConsumerTimeTableColumn.Header.Caption = "8시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H09";
            largeConsumerTimeTableColumn.Header.Caption = "9시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H10";
            largeConsumerTimeTableColumn.Header.Caption = "10시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H11";
            largeConsumerTimeTableColumn.Header.Caption = "11시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H12";
            largeConsumerTimeTableColumn.Header.Caption = "12시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H13";
            largeConsumerTimeTableColumn.Header.Caption = "13시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H14";
            largeConsumerTimeTableColumn.Header.Caption = "14시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H15";
            largeConsumerTimeTableColumn.Header.Caption = "15시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H16";
            largeConsumerTimeTableColumn.Header.Caption = "16시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H17";
            largeConsumerTimeTableColumn.Header.Caption = "17시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H18";
            largeConsumerTimeTableColumn.Header.Caption = "18시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H19";
            largeConsumerTimeTableColumn.Header.Caption = "19시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H20";
            largeConsumerTimeTableColumn.Header.Caption = "20시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H21";
            largeConsumerTimeTableColumn.Header.Caption = "21시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H22";
            largeConsumerTimeTableColumn.Header.Caption = "22시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H23";
            largeConsumerTimeTableColumn.Header.Caption = "23시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            largeConsumerTimeTableColumn = griLargeConsumerTimeTable.DisplayLayout.Bands[0].Columns.Add();
            largeConsumerTimeTableColumn.Key = "H24";
            largeConsumerTimeTableColumn.Header.Caption = "24시";
            largeConsumerTimeTableColumn.CellActivation = Activation.NoEdit;
            largeConsumerTimeTableColumn.CellClickAction = CellClickAction.CellSelect;
            largeConsumerTimeTableColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            largeConsumerTimeTableColumn.CellAppearance.TextHAlign = HAlign.Center;
            largeConsumerTimeTableColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            largeConsumerTimeTableColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle_PerformAutoResize(griLargeConsumerTimeTable);
            //WaterNetCore.FormManager.SetGridStyle_ColumeAllowEdit(griLargeConsumerTimeTable,1,24);
        }

        //폼 최초로딩시 실행
        private void frmLargeConsumerTimeTable_Load(object sender, EventArgs e)
        {
            //=========================================================샘플
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================대수용가 동작정보 

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간관망해석ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.butSaveCheckpoint.Enabled = false;
            }

            //===========================================================================

            Hashtable conditions = new Hashtable();
            conditions.Add("INP_NUMBER", parentForm.texInpNumber.Text);
            conditions.Add("ID", parentForm.texLCId.Text);
            
            griLargeConsumerTimeTable.DataSource = work.SelectLargeConsumerTimeTable(conditions).Tables["WH_LCONSUMER_TIME"];
        }

        //저장버튼 클릭 시
        private void butSaveCheckpoint_Click(object sender, EventArgs e)
        {
            DataTable changedTable = ((DataTable)griLargeConsumerTimeTable.DataSource).GetChanges();
            Hashtable conditions = new Hashtable();

            if (changedTable == null)
            {
                return;
            }

            //입력값 validation
            foreach (DataRow row in changedTable.Rows)
            {
                

                for (int i = 1; i < 25; i++)
                {
                    string time = "H";

                    if (i < 10)
                    {
                        time = time + "0" + i;
                    }
                    else
                    {
                        time = time + i;
                    }

                    if ("".Equals(row[time]) || (!"N".Equals(row[time].ToString().ToUpper()) && !"Y".Equals(row[time].ToString().ToUpper())))
                    {
                        MessageBox.Show("가동여부는 Y나 N만 입력 가능하며 비울 수 없습니다.");
                        return;
                    }
                }
            }

            try
            {
                conditions.Add("changedTable", changedTable);
                conditions.Add("INP_NUMBER", parentForm.texInpNumber.Text);
                conditions.Add("ID", parentForm.texLCId.Text);

                DataSet result = work.SaveLargeConsumerTimeTable(conditions);
                griLargeConsumerTimeTable.DataSource = result.Tables["WH_LCONSUMER_TIME"];

                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
                Console.WriteLine(e1.ToString());
            }
        }

        //그리드의 cell을 더블클릭 한 경우
        private void griLargeConsumerTimeTable_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            if ("Y".Equals(e.Cell.Value.ToString()))
            {
                e.Cell.Value = "N";
            }
            else
            {
                e.Cell.Value = "Y";
            }
        }
    }
}
