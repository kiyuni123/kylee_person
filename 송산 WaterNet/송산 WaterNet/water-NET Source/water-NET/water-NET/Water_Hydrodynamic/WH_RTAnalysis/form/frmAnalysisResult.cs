﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using ChartFX.WinForms;

using WaterNet.WH_RTAnalysis.work;
using WaterNet.WH_Common.work;
using WaterNet.WaterNetCore;
using WaterNet.WH_Common.utils;
using EMFrame.log;

namespace WaterNet.WH_RTAnalysis.form
{
    public partial class frmAnalysisResult : Form
    {
        private RealtimeAnalysisResultWork rWork = null;
        private CommonWork cWork = null;
        public frmWHMain parentForm = null;

        private List<string> nodelist = null;
        private List<string> linklist = null;
        public frmAnalysisResult()
        {
            InitializeComponent();
            InitializeSetting();
        }

        public frmAnalysisResult(List<string> _nodelist, List<string> _linklist)
        {
            InitializeComponent();
            InitializeSetting();
            this.nodelist = _nodelist;
            this.linklist = _linklist;
            this.Text = "해석결과 조회 - 선택객체";
        }

        //화면초기화
        private void InitializeSetting()
        {
            rWork = RealtimeAnalysisResultWork.GetInstance();
            cWork = CommonWork.GetInstance();

            //Node 해석결과 그리드 초기화
            UltraGridColumn nodeColumn;

            nodeColumn = griNodeResult.DisplayLayout.Bands[0].Columns.Add();
            nodeColumn.Key = "TARGET_DATE";
            nodeColumn.Header.Caption = "해석일자";
            nodeColumn.CellActivation = Activation.NoEdit;
            nodeColumn.CellClickAction = CellClickAction.RowSelect;
            nodeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            nodeColumn.CellAppearance.TextHAlign = HAlign.Center;
            nodeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 150;
            nodeColumn.Hidden = false;   //필드 보이기

            nodeColumn = griNodeResult.DisplayLayout.Bands[0].Columns.Add();
            nodeColumn.Key = "NODE_ID";
            nodeColumn.Header.Caption = "NODE ID";
            nodeColumn.CellActivation = Activation.NoEdit;
            nodeColumn.CellClickAction = CellClickAction.RowSelect;
            nodeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            nodeColumn.CellAppearance.TextHAlign = HAlign.Left;
            nodeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 80;
            nodeColumn.Hidden = false;   //필드 보이기

            nodeColumn = griNodeResult.DisplayLayout.Bands[0].Columns.Add();
            nodeColumn.Key = "DEMAND";
            nodeColumn.Header.Caption = "DEMAND (cmh)";
            nodeColumn.CellActivation = Activation.NoEdit;
            nodeColumn.CellClickAction = CellClickAction.RowSelect;
            nodeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            nodeColumn.CellAppearance.TextHAlign = HAlign.Right;
            nodeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 80;
            nodeColumn.Hidden = false;   //필드 보이기

            nodeColumn = griNodeResult.DisplayLayout.Bands[0].Columns.Add();
            nodeColumn.Key = "HEAD";
            nodeColumn.Header.Caption = "HEAD (m)";
            nodeColumn.CellActivation = Activation.NoEdit;
            nodeColumn.CellClickAction = CellClickAction.RowSelect;
            nodeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            nodeColumn.CellAppearance.TextHAlign = HAlign.Right;
            nodeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 80;
            nodeColumn.Hidden = false;   //필드 보이기

            nodeColumn = griNodeResult.DisplayLayout.Bands[0].Columns.Add();
            nodeColumn.Key = "PRESSURE";
            nodeColumn.Header.Caption = "PRESSURE (m)";
            nodeColumn.CellActivation = Activation.NoEdit;
            nodeColumn.CellClickAction = CellClickAction.RowSelect;
            nodeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            nodeColumn.CellAppearance.TextHAlign = HAlign.Right;
            nodeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 80;
            nodeColumn.Hidden = false;   //필드 보이기

            nodeColumn = griNodeResult.DisplayLayout.Bands[0].Columns.Add();
            nodeColumn.Key = "QUALITY";
            nodeColumn.Header.Caption = "QUALITY";
            nodeColumn.CellActivation = Activation.NoEdit;
            nodeColumn.CellClickAction = CellClickAction.RowSelect;
            nodeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            nodeColumn.CellAppearance.TextHAlign = HAlign.Right;
            nodeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 80;
            nodeColumn.Hidden = false;   //필드 보이기

            nodeColumn = griNodeResult.DisplayLayout.Bands[0].Columns.Add();
            nodeColumn.Key = "ELEVATION";
            nodeColumn.Header.Caption = "ELEVATION (m)";
            nodeColumn.CellActivation = Activation.NoEdit;
            nodeColumn.CellClickAction = CellClickAction.RowSelect;
            nodeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            nodeColumn.CellAppearance.TextHAlign = HAlign.Right;
            nodeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 90;
            nodeColumn.Hidden = false;   //필드 보이기

            nodeColumn = griNodeResult.DisplayLayout.Bands[0].Columns.Add();
            nodeColumn.Key = "ANALYSIS_TYPE";
            nodeColumn.Header.Caption = "구분";
            nodeColumn.CellActivation = Activation.NoEdit;
            nodeColumn.CellClickAction = CellClickAction.RowSelect;
            nodeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            nodeColumn.CellAppearance.TextHAlign = HAlign.Left;
            nodeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 90;
            nodeColumn.Hidden = false;   //필드 보이기

            nodeColumn = griNodeResult.DisplayLayout.Bands[0].Columns.Add();
            nodeColumn.Key = "LOC_NAME";
            nodeColumn.Header.Caption = "지역정보";
            nodeColumn.CellActivation = Activation.NoEdit;
            nodeColumn.CellClickAction = CellClickAction.RowSelect;
            nodeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            nodeColumn.CellAppearance.TextHAlign = HAlign.Left;
            nodeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //nodeColumn.Width = 90;
            nodeColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griNodeResult);

            //Link 해석결과 그리드 초기화
            UltraGridColumn linkColumn;

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "TARGET_DATE";
            linkColumn.Header.Caption = "해석일자";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Center;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "LINK_ID";
            linkColumn.Header.Caption = "LINK ID";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Left;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "DIAMETER";
            linkColumn.Header.Caption = "DIAMETER (mm)";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Right;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "LENGTH";
            linkColumn.Header.Caption = "LENGTH (m)";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Right;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "ROUGHNESS";
            linkColumn.Header.Caption = "ROUGHNESS";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Right;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "MINORLOSS";
            linkColumn.Header.Caption = "MINORLOSS";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Right;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "KBULK";
            linkColumn.Header.Caption = "KBULK";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Right;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "KWALL";
            linkColumn.Header.Caption = "KWALL";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Right;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "FLOW";
            linkColumn.Header.Caption = "FLOW (cmh)";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Right;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "VELOCITY";
            linkColumn.Header.Caption = "VELOCITY (m/s)";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Right;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "HEADLOSS";
            linkColumn.Header.Caption = "HEADLOSS (m/km)";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Right;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "STATUS";
            linkColumn.Header.Caption = "STATUS";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Center;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 150;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "ANALYSIS_TYPE";
            linkColumn.Header.Caption = "구분";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Left;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 90;
            linkColumn.Hidden = false;   //필드 보이기

            linkColumn = griLinkResult.DisplayLayout.Bands[0].Columns.Add();
            linkColumn.Key = "LOC_NAME";
            linkColumn.Header.Caption = "지역정보";
            linkColumn.CellActivation = Activation.NoEdit;
            linkColumn.CellClickAction = CellClickAction.RowSelect;
            linkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            linkColumn.CellAppearance.TextHAlign = HAlign.Left;
            linkColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //linkColumn.Width = 90;
            linkColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griLinkResult);

            //실시간 데이터 비교 그리드 초기화
            UltraGridColumn realtimeColumn;

            realtimeColumn = griRealtimeCheck.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "TARGET_DATE";
            realtimeColumn.Header.Caption = "해석일자";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            realtimeColumn.Hidden = false;   //필드 보이기

            realtimeColumn = griRealtimeCheck.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "ID";
            realtimeColumn.Header.Caption = "지점 ID";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            realtimeColumn.Hidden = false;   //필드 보이기

            realtimeColumn = griRealtimeCheck.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "FTR_IDN";
            realtimeColumn.Header.Caption = "계측기번호";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Center;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            realtimeColumn.Hidden = false;   //필드 보이기

            realtimeColumn = griRealtimeCheck.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "ANALYSIS_RESULT";
            realtimeColumn.Header.Caption = "해석결과";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Right;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            realtimeColumn.Hidden = false;   //필드 보이기

            realtimeColumn = griRealtimeCheck.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "VALUE";
            realtimeColumn.Header.Caption = "측정결과";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Right;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            realtimeColumn.Hidden = false;   //필드 보이기

            realtimeColumn = griRealtimeCheck.DisplayLayout.Bands[0].Columns.Add();
            realtimeColumn.Key = "ANALYSIS_TYPE";
            realtimeColumn.Header.Caption = "구분";
            realtimeColumn.CellActivation = Activation.NoEdit;
            realtimeColumn.CellClickAction = CellClickAction.RowSelect;
            realtimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            realtimeColumn.CellAppearance.TextHAlign = HAlign.Left;
            realtimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 90;
            linkColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griRealtimeCheck);

            //실측값(이동식) 그리드 초기화
            UltraGridColumn measureDumpColumn;

            measureDumpColumn = griMeasureDump.DisplayLayout.Bands[0].Columns.Add();
            measureDumpColumn.Key = "DUMP_NUMBER";
            measureDumpColumn.Header.Caption = "측정일련번호";
            measureDumpColumn.CellActivation = Activation.NoEdit;
            measureDumpColumn.CellClickAction = CellClickAction.RowSelect;
            measureDumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            measureDumpColumn.CellAppearance.TextHAlign = HAlign.Center;
            measureDumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            measureDumpColumn.Hidden = false;   //필드 보이기

            measureDumpColumn = griMeasureDump.DisplayLayout.Bands[0].Columns.Add();
            measureDumpColumn.Key = "DUMP_TYPE";
            measureDumpColumn.Header.Caption = "측정항목";
            measureDumpColumn.CellActivation = Activation.NoEdit;
            measureDumpColumn.CellClickAction = CellClickAction.RowSelect;
            measureDumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            measureDumpColumn.CellAppearance.TextHAlign = HAlign.Center;
            measureDumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            measureDumpColumn.Hidden = false;   //필드 보이기

            measureDumpColumn = griMeasureDump.DisplayLayout.Bands[0].Columns.Add();
            measureDumpColumn.Key = "LOC_CODE";
            measureDumpColumn.Header.Caption = "LOC_CODE";
            measureDumpColumn.CellActivation = Activation.NoEdit;
            measureDumpColumn.CellClickAction = CellClickAction.RowSelect;
            measureDumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            measureDumpColumn.CellAppearance.TextHAlign = HAlign.Center;
            measureDumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            measureDumpColumn.Hidden = true;   //필드 보이기

            measureDumpColumn = griMeasureDump.DisplayLayout.Bands[0].Columns.Add();
            measureDumpColumn.Key = "LOC_NAME";
            measureDumpColumn.Header.Caption = "소블록명";
            measureDumpColumn.CellActivation = Activation.NoEdit;
            measureDumpColumn.CellClickAction = CellClickAction.RowSelect;
            measureDumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            measureDumpColumn.CellAppearance.TextHAlign = HAlign.Left;
            measureDumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            measureDumpColumn.Hidden = false;   //필드 보이기

            measureDumpColumn = griMeasureDump.DisplayLayout.Bands[0].Columns.Add();
            measureDumpColumn.Key = "ID";
            measureDumpColumn.Header.Caption = "지역ID";
            measureDumpColumn.CellActivation = Activation.NoEdit;
            measureDumpColumn.CellClickAction = CellClickAction.RowSelect;
            measureDumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            measureDumpColumn.CellAppearance.TextHAlign = HAlign.Left;
            measureDumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            measureDumpColumn.Hidden = false;   //필드 보이기

            measureDumpColumn = griMeasureDump.DisplayLayout.Bands[0].Columns.Add();
            measureDumpColumn.Key = "DUMP_DATE";
            measureDumpColumn.Header.Caption = "입력일자";
            measureDumpColumn.CellActivation = Activation.NoEdit;
            measureDumpColumn.CellClickAction = CellClickAction.RowSelect;
            measureDumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            measureDumpColumn.CellAppearance.TextHAlign = HAlign.Center;
            measureDumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            measureDumpColumn.Hidden = false;   //필드 보이기

            measureDumpColumn = griMeasureDump.DisplayLayout.Bands[0].Columns.Add();
            measureDumpColumn.Key = "DUMP_COMMENT";
            measureDumpColumn.Header.Caption = "비고";
            measureDumpColumn.CellActivation = Activation.NoEdit;
            measureDumpColumn.CellClickAction = CellClickAction.RowSelect;
            measureDumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            measureDumpColumn.CellAppearance.TextHAlign = HAlign.Left;
            measureDumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            measureDumpColumn.Hidden = false;   //필드 보이기

            measureDumpColumn = griMeasureDump.DisplayLayout.Bands[0].Columns.Add();
            measureDumpColumn.Key = "DUMP_FILE_PATH";
            measureDumpColumn.Header.Caption = "실측자료파일";
            measureDumpColumn.CellActivation = Activation.NoEdit;
            measureDumpColumn.CellClickAction = CellClickAction.RowSelect;
            measureDumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            measureDumpColumn.CellAppearance.TextHAlign = HAlign.Left;
            measureDumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //realtimeColumn.Width = 150;
            measureDumpColumn.Hidden = false;   //필드 보이기

            WaterNetCore.FormManager.SetGridStyle(griMeasureDump);

            //차트 초기화
            this.chaRealtimeCheck.LegendBox.MarginY = 1;
            this.chaRealtimeCheck.AxisX.DataFormat.Format = AxisFormat.DateTime;
            this.chaRealtimeCheck.AxisX.DataFormat.CustomFormat = "hh:mm";
            this.chaRealtimeCheck.AxisX.Staggered = true;

            this.chaNodeResult.LegendBox.MarginY = 1;
            this.chaNodeResult.AxisX.DataFormat.Format = AxisFormat.DateTime;
            this.chaNodeResult.AxisX.DataFormat.CustomFormat = "hh:mm";
            this.chaNodeResult.AxisX.Staggered = true;

            this.chaLinkResult.LegendBox.MarginY = 1;
            this.chaLinkResult.AxisX.DataFormat.Format = AxisFormat.DateTime;
            this.chaLinkResult.AxisX.DataFormat.CustomFormat = "hh:mm";
            this.chaLinkResult.AxisX.Staggered = true;

            WaterNetCore.FormManager.SetGridStyle(griMeasureDump);
        }

        //폼 최초로딩 시 실행
        private void frmAnalysisResult_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["실시간관망해석ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.butAddMeasureBulk.Enabled = false;
                this.butSaveMeasureBulk.Enabled = false;
                this.butDeleteMeasureBulk.Enabled = false;
            }

            //============================================================================

            //공용으로 사용되는 소블록 리스트
            Hashtable sblockConditions = new Hashtable();
            sblockConditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

            DataTable nodeSBlockDataTable = rWork.SelectLocationData(sblockConditions).Tables["CM_LOCATION"];
            DataTable linkSBlockDataTable = rWork.SelectLocationData(sblockConditions).Tables["CM_LOCATION"];
            DataTable dumpSBlockDataTable = rWork.SelectLocationData(sblockConditions).Tables["CM_LOCATION"];

            //Node 설정


            //Node 검색일자 세팅
            datNodeDate.Value = Utils.GetTime()["yyyymmdd"];

            //Node 모델 내 소블록 리스트 할당
            comNodeSBlock.ValueMember = "LOC_CODE";
            comNodeSBlock.DisplayMember = "LOC_NAME";

            comNodeSBlock.DataSource = nodeSBlockDataTable;

            //Node 해석항목 세팅
            object[,] comNodeItemData = new object[4, 2];

            comNodeItemData[0, 0] = "";
            comNodeItemData[0, 1] = "해석항목";

            comNodeItemData[1, 0] = "DEMAND";
            comNodeItemData[1, 1] = "Demand";

            comNodeItemData[2, 0] = "HEAD";
            comNodeItemData[2, 1] = "Head";

            comNodeItemData[3, 0] = "PRESSURE";
            comNodeItemData[3, 1] = "Pressure";

            FormManager.SetComboBoxEX(comNodeItem, comNodeItemData, false);

            //Node 연산자 세팅
            object[,] comNodeFormularData = new object[6, 2];

            comNodeFormularData[0, 0] = "";
            comNodeFormularData[0, 1] = "연산자";

            comNodeFormularData[1, 0] = "=";
            comNodeFormularData[1, 1] = "=";

            comNodeFormularData[2, 0] = ">=";
            comNodeFormularData[2, 1] = "이상";

            comNodeFormularData[3, 0] = "<=";
            comNodeFormularData[3, 1] = "이하";

            comNodeFormularData[4, 0] = ">";
            comNodeFormularData[4, 1] = "초과";

            comNodeFormularData[5, 0] = "<";
            comNodeFormularData[5, 1] = "미만";

            FormManager.SetComboBoxEX(comNodeFormular, comNodeFormularData, false);


            //Link 설정


            //Link 검색일자 세팅
            datLinkDate.Value = Utils.GetTime()["yyyymmdd"];

            //Link 모델 내 소블록 리스트 할당
            comLinkSBlock.ValueMember = "LOC_CODE";
            comLinkSBlock.DisplayMember = "LOC_NAME";

            comLinkSBlock.DataSource = linkSBlockDataTable;

            //Link 해석항목 세팅
            object[,] comLinkItemData = new object[10, 2];

            comLinkItemData[0, 0] = "";
            comLinkItemData[0, 1] = "해석항목";

            comLinkItemData[1, 0] = "DIAMETER";
            comLinkItemData[1, 1] = "Diameter";

            comLinkItemData[2, 0] = "LENGTH";
            comLinkItemData[2, 1] = "Length";

            comLinkItemData[3, 0] = "FLOW";
            comLinkItemData[3, 1] = "Flow";

            comLinkItemData[4, 0] = "VELOCITY";
            comLinkItemData[4, 1] = "Velocity";

            comLinkItemData[5, 0] = "HEADLOSS";
            comLinkItemData[5, 1] = "Headloss";

            comLinkItemData[6, 0] = "ROUGHNESS";
            comLinkItemData[6, 1] = "Roughness";

            comLinkItemData[7, 0] = "MINORLOSS";
            comLinkItemData[7, 1] = "Minorloss";

            comLinkItemData[8, 0] = "KBULK";
            comLinkItemData[8, 1] = "Kbulk";

            comLinkItemData[9, 0] = "KWALL";
            comLinkItemData[9, 1] = "Kwall";

            FormManager.SetComboBoxEX(comLinkItem, comLinkItemData, false);

            //Link 연산자 세팅
            object[,] comLinkFormularData = new object[6, 2];

            comLinkFormularData[0, 0] = "";
            comLinkFormularData[0, 1] = "연산자";

            comLinkFormularData[1, 0] = "=";
            comLinkFormularData[1, 1] = "=";

            comLinkFormularData[2, 0] = ">=";
            comLinkFormularData[2, 1] = "이상";

            comLinkFormularData[3, 0] = "<=";
            comLinkFormularData[3, 1] = "이하";

            comLinkFormularData[4, 0] = ">";
            comLinkFormularData[4, 1] = "초과";

            comLinkFormularData[5, 0] = "<";
            comLinkFormularData[5, 1] = "미만";

            FormManager.SetComboBoxEX(comLinkFormular, comLinkFormularData, false);


            //실측지점(고정식) 설정


            //실측지점(고정식) 검색일자 세팅
            datMeterDate.Value = Utils.GetTime()["yyyymmdd"];

            //실측지점(고정식) 측정항목 세팅
            comMeterType.ValueMember = "CODE";
            comMeterType.DisplayMember = "CODE_NAME";

            Hashtable meterConditions = new Hashtable();
            meterConditions.Add("PCODE", "1003");
            meterConditions.Add("removeAllSelect", "Y");

            DataSet meterTypeDataSet = cWork.GetCodeList(meterConditions);

            if (meterTypeDataSet != null)
            {
                comMeterType.DataSource = meterTypeDataSet.Tables["CM_CODE"];
            }

            //실측지점(고정식) 지점 세팅
            comTagname.ValueMember = "TAGNAME";
            comTagname.DisplayMember = "DESCRIPTION";


            //실측지점(이동식) 설정


            //실측지점(이동식) 검색일자 세팅
            datDumpStartDate.Value = Utils.GetCalcTime("mm", -3)["yyyymmdd"];
            datDumpEndDate.Value = Utils.GetTime()["yyyymmdd"];

            //실측지점(이동식) 지역 세팅
            comDumpSBlock.ValueMember = "LOC_CODE";
            comDumpSBlock.DisplayMember = "LOC_NAME";

            comDumpSBlock.DataSource = dumpSBlockDataTable;

            //실측지점(이동식) 측정항목 세팅
            object[,] comDumpTypeData = new object[3, 2];

            comDumpTypeData[0, 0] = "";
            comDumpTypeData[0, 1] = "전체";

            comDumpTypeData[1, 0] = "Flow";
            comDumpTypeData[1, 1] = "Flow";

            comDumpTypeData[2, 0] = "Pressure";
            comDumpTypeData[2, 1] = "Pressure";

            FormManager.SetComboBoxEX(comDumpType, comDumpTypeData, false);

            //실측지점(이동식) 상세의 측정항목 세팅
            object[,] comDetailDumpTypeData = new object[2, 2];

            comDetailDumpTypeData[0, 0] = "Flow";
            comDetailDumpTypeData[0, 1] = "Flow";

            comDetailDumpTypeData[1, 0] = "Pressure";
            comDetailDumpTypeData[1, 1] = "Pressure";

            FormManager.SetComboBoxEX(comDetailDumpType, comDetailDumpTypeData, false);
        }

        //측정항목 Combobox 변경 시 실측지점 Combobox 변경
        private void comMeterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();

            conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
            conditions.Add("METER_TYPE", comMeterType.SelectedValue);

            DataSet checkpointDataSet = rWork.GetRealtimeCheckpointList(conditions);

            comTagname.DataSource = checkpointDataSet.Tables["WH_MEASURE_CHECKPOINT"];
        }

        //조회버튼 클릭
        private void butSelectAnalysisResult_Click(object sender, EventArgs e)
        {
            //필터링 초기화
            comNodeItem.SelectedValue = "";
            texNodeValue.Text = "";
            comNodeFormular.SelectedValue = "";

            comLinkItem.SelectedValue = "";
            texLinkValue.Text = "";
            comLinkFormular.SelectedValue = "";

            SelectAnalysisResult("A");

            //실측지점 조회결과 초기화
            comMeterType.SelectedIndex = 0;
            comTagname.SelectedIndex = 0;

            if (griRealtimeCheck.DataSource != null)
            {
                ((DataTable)griRealtimeCheck.DataSource).Clear();
            }

            chaRealtimeCheck.Data.Clear();
            chaNodeResult.Data.Clear();
            chaLinkResult.Data.Clear();
        }

        //Node 필터적용 버튼 클릭
        private void butApplyNodeFilter_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                chaNodeResult.Data.Clear();

                //필터링 옵션
                if (!"".Equals(comNodeItem.SelectedValue))
                {
                    if ("".Equals(texNodeValue.Text))
                    {
                        MessageBox.Show(comNodeItem.Text + "의 수치를 입력하십시오.");
                        texNodeValue.Focus();
                        this.Cursor = System.Windows.Forms.Cursors.Default;
                        return;
                    }

                    if ("".Equals(comNodeFormular.SelectedValue))
                    {
                        MessageBox.Show("비교연산자를 선택하십시오.");
                        comNodeFormular.Focus();
                        this.Cursor = System.Windows.Forms.Cursors.Default;
                        return;
                    }
                }

                SelectAnalysisResult("N");
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //Link 필터적용 버튼 클릭
        private void butApplyLinkFilter_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                chaLinkResult.Data.Clear();

                if (!"".Equals(comLinkItem.SelectedValue))
                {
                    if ("".Equals(texLinkValue.Text))
                    {
                        MessageBox.Show(comLinkItem.Text + "의 수치를 입력하십시오.");
                        texLinkValue.Focus();
                        this.Cursor = System.Windows.Forms.Cursors.Default;
                        return;
                    }

                    if ("".Equals(comLinkFormular.SelectedValue))
                    {
                        MessageBox.Show("비교연산자를 선택하십시오.");
                        comLinkFormular.Focus();
                        this.Cursor = System.Windows.Forms.Cursors.Default;
                        return;
                    }
                }

                SelectAnalysisResult("L");
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
           
        }

        //실측지점 비교버튼 클릭
        private void butGetCheckpointData_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                if ("".Equals(comTagname.SelectedValue))
                {
                    this.Cursor = System.Windows.Forms.Cursors.Default;
                    MessageBox.Show("실측지점을 선택하십시오.");
                    comTagname.Focus();
                    return;
                }

                //차트 초기화
                chaRealtimeCheck.Data.Clear();

                Hashtable conditions = new Hashtable();

                conditions.Add("startDate", ((DateTime)datMeterDate.Value).ToString("yyyyMMdd"));
                conditions.Add("endDate", ((DateTime)datMeterDate.Value).ToString("yyyyMMdd"));
                conditions.Add("METER_TYPE", comMeterType.SelectedValue);
                conditions.Add("TAGNAME", comTagname.SelectedValue);
                conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

                DataSet realtimeDataSet = rWork.GetAnalysisResultVsRealtimeCheck(conditions);

                griRealtimeCheck.DataSource = realtimeDataSet.Tables["WH_MEASURE_CHECKPOINT"];
                FormManager.SetGridStyle_PerformAutoResize(griRealtimeCheck);

                this.chaRealtimeCheck.Data.Series = 2;
                this.chaRealtimeCheck.Data.Points = realtimeDataSet.Tables["WH_MEASURE_CHECKPOINT"].Rows.Count;

                foreach (DataRow row in realtimeDataSet.Tables["WH_MEASURE_CHECKPOINT"].Rows)
                {
                    int pointIndex = realtimeDataSet.Tables["WH_MEASURE_CHECKPOINT"].Rows.IndexOf(row);
                    this.chaRealtimeCheck.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["TARGET_DATE"]).ToString("hh:mm");
                    this.chaRealtimeCheck.Data[0, pointIndex] = Convert.ToDouble(row["ANALYSIS_RESULT"]);
                    this.chaRealtimeCheck.Data[1, pointIndex] = Convert.ToDouble(row["VALUE"]);
                }

                this.chaRealtimeCheck.Series[0].Text = "해석값";
                this.chaRealtimeCheck.Series[1].Text = "실측값";

                this.chaRealtimeCheck.Series[0].Gallery = Gallery.Lines;
                this.chaRealtimeCheck.Series[0].MarkerSize = 1;

                this.chaRealtimeCheck.Series[1].Gallery = Gallery.Lines;
                this.chaRealtimeCheck.Series[1].MarkerSize = 1;

                chaRealtimeCheck.Series[0].Visible = true;
                chaRealtimeCheck.Series[1].Visible = true;

                if ("000001".Equals(comMeterType.SelectedValue.ToString()))
                {
                    //유량인 경우
                    chaRealtimeCheck.Series[0].AxisY.Title.Text = "cmh";
                }
                else
                {
                    //압력인 경우
                    chaRealtimeCheck.Series[0].AxisY.Title.Text = "m";
                }

                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
           
        }

        //해석결과 조회
        private void SelectAnalysisResult(string flag)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            Hashtable conditions = new Hashtable();

            conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

            if("A".Equals(flag) || "N".Equals(flag))
            {
                conditions.Add("startDate", ((DateTime)datNodeDate.Value).ToString("yyyyMMdd"));
                conditions.Add("endDate", ((DateTime)datNodeDate.Value).ToString("yyyyMMdd"));

                if (!"".Equals(comNodeItem.SelectedValue))
                {
                    conditions.Add("nodeItem", comNodeItem.SelectedValue);
                }
                if (!"".Equals(texNodeValue.Text))
                {
                    conditions.Add("nodeValue", texNodeValue.Text);
                }
                if (!"".Equals(comNodeFormular.SelectedValue))
                {
                    conditions.Add("nodeFormular", comNodeFormular.SelectedValue);
                }
                if (!"".Equals(comNodeSBlock.SelectedValue))
                {
                    conditions.Add("nodeLocation", comNodeSBlock.SelectedValue);
                }

                DataSet nodeData = null;
                if (this.nodelist == null)
                    nodeData = rWork.GetAnalysisNodeResultList(conditions);    
                else
                    nodeData = rWork.GetAnalysisNodeResultList(conditions, this.nodelist);    
                
                griNodeResult.DataSource = nodeData.Tables["WH_RPT_NODES"];
                FormManager.SetGridStyle_PerformAutoResize(griNodeResult);
            }

            if ("A".Equals(flag) || "L".Equals(flag))
            {
                conditions.Add("startDate", ((DateTime)datLinkDate.Value).ToString("yyyyMMdd"));
                conditions.Add("endDate", ((DateTime)datLinkDate.Value).ToString("yyyyMMdd"));

                if (!"".Equals(comLinkItem.SelectedValue))
                {
                    conditions.Add("linkItem", comLinkItem.SelectedValue);
                }
                if (!"".Equals(texLinkValue.Text))
                {
                    conditions.Add("linkValue", texLinkValue.Text);
                }
                if (!"".Equals(comLinkFormular.SelectedValue))
                {
                    conditions.Add("linkFormular", comLinkFormular.SelectedValue);
                }
                if (!"".Equals(comLinkSBlock.SelectedValue))
                {
                    conditions.Add("linkLocation", comLinkSBlock.SelectedValue);
                }

                DataSet linkData = null;
                if (this.linklist == null)
                    linkData = rWork.GetAnalysisLinkResultList(conditions);
                else
                    linkData = rWork.GetAnalysisLinkResultList(conditions, this.linklist);

                griLinkResult.DataSource = linkData.Tables["WH_RPT_LINKS"];
                FormManager.SetGridStyle_PerformAutoResize(griLinkResult);
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        //실측데이터(이동식) 조회
        private void SelectMeasuerDump()
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            Hashtable conditions = new Hashtable();

            conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
            conditions.Add("startDate", ((DateTime)datDumpStartDate.Value).ToString("yyyyMMdd"));
            conditions.Add("endDate", ((DateTime)datDumpEndDate.Value).ToString("yyyyMMdd"));

            if (!"".Equals(comDumpSBlock.SelectedValue))
            {
                conditions.Add("LOC_CODE", comDumpSBlock.SelectedValue);
            }

            if(!"".Equals(comDumpType.SelectedValue))
            {
                conditions.Add("DUMP_TYPE", comDumpType.SelectedValue);
            }

            DataSet measuerDumpDataSet = rWork.SelectMeasuerDumpMaster(conditions);

            griMeasureDump.DataSource = measuerDumpDataSet.Tables["WH_MEASURE_DUMP_MASTER"];
            FormManager.SetGridStyle_PerformAutoResize(griMeasureDump);

            if (griMeasureDump.Rows.Count > 0)
            {
                griMeasureDump.Rows[0].Selected = true;
                griMeasureDump.ActiveRowScrollRegion.ScrollPosition = 0;
            }
            else
            {
                comDetailDumpType.Enabled = false;
                butFindFile.Enabled = false;

                texDetailDumpNumber.Text = "";
                texDetailDumpDate.Text = "";
                texDetailLocName.Text = "";
                comDetailDumpType.SelectedValue = "Flow";
                texDetailId.Text = "";
                texDetailComment.Text = "";
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        //Form이 닫길때
        private void frmAnalysisResult_FormClosing(object sender, FormClosingEventArgs e)
        {
            parentForm.resultForm = null;
        }

        //파일찾기 버튼 클릭
        private void butFindFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog oOFD = new OpenFileDialog();

            oOFD.Filter = "Excel files (*.xls,*.xlsx)|*.xls;*.xlsx";
            oOFD.FilterIndex = 1;
            oOFD.RestoreDirectory = true;
            //oOFD.InitialDirectory = @"D:\00.Work\01.Water-Net\09.etc\FTPUploadSample\bin\Debug";
            oOFD.Multiselect = false;
            oOFD.ShowDialog();

            texDumpFilePath.Text = oOFD.FileName;
        }

        //측정값 비교 버튼 클릭 시
        private void butCompareResult_Click(object sender, EventArgs e)
        {
            if (griMeasureDump.Selected.Rows.Count != 0)
            {
                frmResultCompare resultCompare = new frmResultCompare();
                resultCompare.dumpNumber = griMeasureDump.Selected.Rows[0].GetCellValue("DUMP_NUMBER").ToString();
                resultCompare.dumpType = griMeasureDump.Selected.Rows[0].GetCellValue("DUMP_TYPE").ToString();
                resultCompare.ShowDialog();
            }
        }

        //실측데이터(이동식) 추가 버튼 클릭 시
        private void butAddMeasureBulk_Click(object sender, EventArgs e)
        {
            if (griMeasureDump.DataSource == null)
            {
                DataTable tmpDataTable = new DataTable();
                tmpDataTable.Columns.Add("DUMP_NUMBER", typeof(string));
                tmpDataTable.Columns.Add("DUMP_TYPE", typeof(string));
                tmpDataTable.Columns.Add("LOC_CODE", typeof(string));
                tmpDataTable.Columns.Add("LOC_NAME", typeof(string));
                tmpDataTable.Columns.Add("ID", typeof(string));
                tmpDataTable.Columns.Add("DUMP_DATE", typeof(string));
                tmpDataTable.Columns.Add("DUMP_COMMENT", typeof(string));
                tmpDataTable.Columns.Add("DUMP_FILE_PATH", typeof(string));
                
                griMeasureDump.DataSource = tmpDataTable;
            }

            int count = ((DataTable)griMeasureDump.DataSource).Rows.Count;
            griMeasureDump.ActiveRow = null;

            DataRow row = ((DataTable)griMeasureDump.DataSource).NewRow();
            ((DataTable)griMeasureDump.DataSource).Rows.Add(row);

            griMeasureDump.Rows[count].Selected = true;
            griMeasureDump.ActiveRowScrollRegion.ScrollRowIntoView(griMeasureDump.Rows[count]);

            ((DataTable)griMeasureDump.DataSource).Rows[griMeasureDump.Selected.Rows[0].Index].SetField("DUMP_TYPE", "Flow");
            comDetailDumpType.SelectedValue = "Flow";

            griMeasureDump.UpdateData();
        }

        //실측데이터(이동식) 저장 버튼 클릭 시
        private void butSaveMeasureBulk_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (griMeasureDump.Selected.Rows.Count != 0)
                {
                    DataTable changedTable = ((DataTable)griMeasureDump.DataSource).GetChanges();
                    Hashtable conditions = new Hashtable();

                    if (changedTable == null)
                    {
                        return;
                    }

                    //입력값 validation
                    foreach (DataRow row in changedTable.Rows)
                    {
                        if ("".Equals(row["ID"]))
                        {
                            MessageBox.Show("ID가 선택되지 않은 행이 있습니다.");
                            return;
                        }
                    }

                    try
                    {
                        conditions.Add("changedTable", changedTable);
                        conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);

                        DataSet result = rWork.SaveMeasureDumpMaster(conditions);
                        griMeasureDump.DataSource = result.Tables["WH_MEASURE_DUMP_MASTER"];

                        if (griMeasureDump.Rows.Count > 0)
                        {
                            griMeasureDump.Rows[0].Selected = true;
                            griMeasureDump.ActiveRowScrollRegion.ScrollPosition = 0;
                        }

                        MessageBox.Show("정상적으로 처리되었습니다.");
                        SelectMeasuerDump();
                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show(e1.Message);
                        Console.WriteLine(e1.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //실측데이터(이동식) 삭제 버튼 클릭 시
        private void butDeleteMeasureBulk_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (griMeasureDump.Selected.Rows.Count != 0)
                {
                    int idx = griMeasureDump.Selected.Rows[0].Index;

                    //DataRow의 상태확인
                    if (((DataTable)griMeasureDump.DataSource).Rows[idx].RowState == DataRowState.Added)
                    {
                        //추가인 경우 그냥 삭제만 한다.
                        ((DataTable)griMeasureDump.DataSource).Rows.RemoveAt(idx);

                        //삭제하고 행이 남았을 경우만 포커싱
                        if (griMeasureDump.Rows.Count > 0)
                        {
                            int count = ((DataTable)griMeasureDump.DataSource).Rows.Count;

                            griMeasureDump.Rows[count - 1].Selected = true;
                            griMeasureDump.ActiveRowScrollRegion.ScrollRowIntoView(griMeasureDump.Rows[count - 1]);
                        }
                    }
                    else
                    {
                        DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (result == DialogResult.Yes)
                        {
                            Hashtable conditions = new Hashtable();

                            conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
                            conditions.Add("DUMP_NUMBER", texDetailDumpNumber.Text);

                            try
                            {
                                DataSet dSet = rWork.DeleteMeasureDumpMaster(conditions);
                                griMeasureDump.DataSource = dSet.Tables["WH_MEASURE_DUMP_MASTER"];

                                if (griMeasureDump.Rows.Count > 0)
                                {
                                    griMeasureDump.Rows[0].Selected = true;
                                    griMeasureDump.ActiveRowScrollRegion.ScrollPosition = 0;
                                }
                                else
                                {
                                    comDetailDumpType.Enabled = false;
                                    butFindFile.Enabled = false;

                                    texDetailDumpNumber.Text = "";
                                    texDetailDumpDate.Text = "";
                                    texDetailLocName.Text = "";
                                    comDetailDumpType.SelectedValue = "Flow";
                                    texDetailId.Text = "";
                                    texDetailComment.Text = "";
                                    texDumpFilePath.Text = "";
                                }

                                MessageBox.Show("정상적으로 처리되었습니다.");
                                SelectMeasuerDump();
                            }
                            catch (Exception e1)
                            {
                                MessageBox.Show(e1.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //실측데이터(이동식) 지점ID 변경 시
        private void texDetailId_TextChanged(object sender, EventArgs e)
        {
            if (griMeasureDump.Selected.Rows.Count != 0)
            {
                ((DataTable)griMeasureDump.DataSource).Rows[griMeasureDump.Selected.Rows[0].Index].SetField("ID", texDetailId.Text);
                griMeasureDump.UpdateData();
            }
        }

        //실측데이터(이동식) 측정항목 변경 시
        private void comDetailDumpType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (griMeasureDump.Selected.Rows.Count != 0)
            {
                ((DataTable)griMeasureDump.DataSource).Rows[griMeasureDump.Selected.Rows[0].Index].SetField("DUMP_TYPE", comDetailDumpType.SelectedValue);
                griMeasureDump.UpdateData();
            }
        }

        //실측데이터(이동식) 비고 변경 시
        private void texDetailComment_TextChanged(object sender, EventArgs e)
        {
            if (griMeasureDump.Selected.Rows.Count != 0)
            {
                ((DataTable)griMeasureDump.DataSource).Rows[griMeasureDump.Selected.Rows[0].Index].SetField("DUMP_COMMENT", texDetailComment.Text);
                griMeasureDump.UpdateData();
            }
        }

        //실측데이터(이동식) 파일 변경 시
        private void texDumpFilePath_TextChanged(object sender, EventArgs e)
        {
            if (griMeasureDump.Selected.Rows.Count != 0)
            {
                ((DataTable)griMeasureDump.DataSource).Rows[griMeasureDump.Selected.Rows[0].Index].SetField("DUMP_FILE_PATH", texDumpFilePath.Text.ToString());
                griMeasureDump.UpdateData();
            }
        }

        //실측데이터(이동식) Grid의 행이 선택된 경우
        private void griMeasureDump_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griMeasureDump.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griMeasureDump.DataSource).Rows[griMeasureDump.Selected.Rows[0].Index]);

                texDetailDumpNumber.Text = Utils.nts(griMeasureDump.Selected.Rows[0].GetCellValue("DUMP_NUMBER"));
                texDetailDumpDate.Text = Utils.nts(griMeasureDump.Selected.Rows[0].GetCellValue("DUMP_DATE"));
                texDetailLocName.Text = Utils.nts(griMeasureDump.Selected.Rows[0].GetCellValue("LOC_NAME"));
                comDetailDumpType.SelectedValue = Utils.nts(griMeasureDump.Selected.Rows[0].GetCellValue("DUMP_TYPE"));
                texDetailId.Text = Utils.nts(griMeasureDump.Selected.Rows[0].GetCellValue("ID"));
                texDetailComment.Text = Utils.nts(griMeasureDump.Selected.Rows[0].GetCellValue("DUMP_COMMENT"));
                texDumpFilePath.Text = Utils.nts(griMeasureDump.Selected.Rows[0].GetCellValue("DUMP_FILE_PATH"));

                if (row.RowState == DataRowState.Added)
                {
                    comDetailDumpType.Enabled = true;
                    butFindFile.Enabled = true;
                }
                else
                {
                    comDetailDumpType.Enabled = false;
                    butFindFile.Enabled = false;
                }
            }
            else
            {
                comDetailDumpType.Enabled = false;
                butFindFile.Enabled = false;

                texDetailDumpNumber.Text = "";
                texDetailDumpDate.Text = "";
                texDetailLocName.Text = "";
                comDetailDumpType.SelectedValue = "Flow";
                texDetailId.Text = "";
                texDetailComment.Text = "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SelectMeasuerDump();
        }

        private void griNodeResult_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (griNodeResult.Selected.Rows.Count != 0)
            {
                string layerName = "";

                if ("junction".Equals((string)griNodeResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "JUNCTION";
                }
                else if ("reservoir".Equals((string)griNodeResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "RESERVOIR";
                }
                else if ("tank".Equals((string)griNodeResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "TANK";
                }

                //그래프 데이터 표출
                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
                conditions.Add("NODE_ID", griNodeResult.Selected.Rows[0].GetCellValue("NODE_ID"));
                conditions.Add("startDate", ((DateTime)datNodeDate.Value).ToString("yyyyMMdd"));
                conditions.Add("endDate", ((DateTime)datNodeDate.Value).ToString("yyyyMMdd"));

                DataSet dset = rWork.SelectNodeAnalysisDataForGraph(conditions);

                this.chaNodeResult.Data.Series = 3;
                this.chaNodeResult.Data.Points = dset.Tables["WH_RPT_MASTER"].Rows.Count;

                foreach (DataRow row in dset.Tables["WH_RPT_MASTER"].Rows)
                {
                    int pointIndex = dset.Tables["WH_RPT_MASTER"].Rows.IndexOf(row);
                    this.chaNodeResult.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["TARGET_DATE"]).ToString("hh:mm");
                    this.chaNodeResult.Data[0, pointIndex] = Convert.ToDouble(row["DEMAND"]);
                    this.chaNodeResult.Data[1, pointIndex] = Convert.ToDouble(row["HEAD"]);
                    this.chaNodeResult.Data[2, pointIndex] = Convert.ToDouble(row["PRESSURE"]);
                }

                this.chaNodeResult.AxesY.Clear();

                this.chaNodeResult.Series[0].AxisY = this.chaNodeResult.AxesY[0];

                this.chaNodeResult.AxesY.Add(new AxisY());
                this.chaNodeResult.AxesY.Add(new AxisY());

                this.chaNodeResult.Series[1].AxisY = this.chaNodeResult.AxesY[1];
                this.chaNodeResult.Series[2].AxisY = this.chaNodeResult.AxesY[2];

                this.chaNodeResult.Series[0].Text = "Demand";
                this.chaNodeResult.Series[1].Text = "Head";
                this.chaNodeResult.Series[2].Text = "Pressure";

                this.chaNodeResult.AxesY[0].Visible = true;
                this.chaNodeResult.LegendBox.Dock = DockArea.Bottom;

                this.chaNodeResult.Series[0].Gallery = Gallery.Lines;
                this.chaNodeResult.Series[0].MarkerSize = 1;
                this.chaNodeResult.Series[1].Gallery = Gallery.Lines;
                this.chaNodeResult.Series[1].MarkerSize = 1;
                this.chaNodeResult.Series[2].Gallery = Gallery.Lines;
                this.chaNodeResult.Series[2].MarkerSize = 1;

                chaNodeResult.Series[0].Visible = true;
                chaNodeResult.Series[1].Visible = true;
                chaNodeResult.Series[2].Visible = true;

                chaNodeResult.AxesY[0].Title.Text = "cmh";
                chaNodeResult.AxesY[1].Title.Text = "m";
                chaNodeResult.AxesY[2].Title.Text = "m";

                parentForm.MoveFocusAt(layerName, "ID = '" + griNodeResult.Selected.Rows[0].GetCellValue("NODE_ID") + "'");
            }
        }

        private void griLinkResult_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (griLinkResult.Selected.Rows.Count != 0)
            {
                string layerName = "";

                if ("pipe".Equals((string)griLinkResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "PIPE";
                }
                else if ("pump".Equals((string)griLinkResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "PUMP";
                }
                else if ("prv".Equals((string)griLinkResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "psv".Equals((string)griLinkResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "pbv".Equals((string)griLinkResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "fcv".Equals((string)griLinkResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "tcv".Equals((string)griLinkResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "gpv".Equals((string)griLinkResult.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE"))
                    )
                {
                    //Junction Row를 click한 경우
                    layerName = "VALVE";
                }

                //그래프 데이터 표출
                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", parentForm.dummyInpNumber);
                conditions.Add("NODE_ID", griLinkResult.Selected.Rows[0].GetCellValue("LINK_ID"));
                conditions.Add("startDate", ((DateTime)datLinkDate.Value).ToString("yyyyMMdd"));
                conditions.Add("endDate", ((DateTime)datLinkDate.Value).ToString("yyyyMMdd"));

                DataSet dset = rWork.SelectLinkAnalysisDataForGraph(conditions);

                this.chaLinkResult.Data.Series = 2;
                this.chaLinkResult.Data.Points = dset.Tables["WH_RPT_MASTER"].Rows.Count;

                foreach (DataRow row in dset.Tables["WH_RPT_MASTER"].Rows)
                {
                    int pointIndex = dset.Tables["WH_RPT_MASTER"].Rows.IndexOf(row);
                    this.chaLinkResult.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["TARGET_DATE"]).ToString("hh:mm");
                    this.chaLinkResult.Data[0, pointIndex] = Convert.ToDouble(row["FLOW"]);
                    this.chaLinkResult.Data[1, pointIndex] = Convert.ToDouble(row["VELOCITY"]);
                }

                this.chaLinkResult.AxesY.Clear();

                this.chaLinkResult.Series[0].AxisY = this.chaLinkResult.AxesY[0];

                this.chaLinkResult.AxesY.Add(new AxisY());

                this.chaLinkResult.Series[1].AxisY = this.chaLinkResult.AxesY[1];

                this.chaLinkResult.Series[0].Text = "Flow";
                this.chaLinkResult.Series[1].Text = "Velocity";

                this.chaLinkResult.AxesY[0].Visible = true;
                this.chaLinkResult.LegendBox.Dock = DockArea.Bottom;

                this.chaLinkResult.Series[0].Gallery = Gallery.Lines;
                this.chaLinkResult.Series[0].MarkerSize = 1;
                this.chaLinkResult.Series[1].Gallery = Gallery.Lines;
                this.chaLinkResult.Series[1].MarkerSize = 1;

                chaLinkResult.Series[0].Visible = true;
                chaLinkResult.Series[1].Visible = true;

                chaLinkResult.AxesY[0].Title.Text = "cmh";
                chaLinkResult.AxesY[1].Title.Text = "m/s";

                parentForm.MoveFocusAt(layerName, "ID = '" + griLinkResult.Selected.Rows[0].GetCellValue("LINK_ID") + "'");
            }
        }

        private void griRealtimeCheck_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (griRealtimeCheck.Selected.Rows.Count != 0)
            {
                string layerName = "";

                if ("junction".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "JUNCTION";
                    parentForm.MoveFocusAt(layerName, "ID = '" + griRealtimeCheck.Selected.Rows[0].GetCellValue("ID") + "'");
                }
                else if ("reservoir".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "RESERVOIR";
                    parentForm.MoveFocusAt(layerName, "ID = '" + griRealtimeCheck.Selected.Rows[0].GetCellValue("ID") + "'");
                }
                else if ("tank".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "TANK";
                    parentForm.MoveFocusAt(layerName, "ID = '" + griRealtimeCheck.Selected.Rows[0].GetCellValue("ID") + "'");
                }
                else if ("pipe".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "PIPE";
                    parentForm.MoveFocusAt(layerName, "ID = '" + griRealtimeCheck.Selected.Rows[0].GetCellValue("ID") + "'");
                }
                else if ("pump".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")))
                {
                    //Junction Row를 click한 경우
                    layerName = "PUMP";
                    parentForm.MoveFocusAt(layerName, "ID = '" + griRealtimeCheck.Selected.Rows[0].GetCellValue("ID") + "'");
                }
                else if ("prv".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "psv".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "pbv".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "fcv".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "tcv".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE")) ||
                    "gpv".Equals((string)griRealtimeCheck.Selected.Rows[0].GetCellValue("ANALYSIS_TYPE"))
                    )
                {
                    //Junction Row를 click한 경우
                    layerName = "VALVE";
                    parentForm.MoveFocusAt(layerName, "ID = '" + griRealtimeCheck.Selected.Rows[0].GetCellValue("ID") + "'");
                }
            }
        }

        #region 엑셀내보내기
        private void butApplyNodeExcel_Click(object sender, EventArgs e)
        {
            WaterNetCore.FormManager.ExportToExcelFromUltraGrid(this.griNodeResult, "NODE");
        }

        private void butApplyLinkExcel_Click(object sender, EventArgs e)
        {
            WaterNetCore.FormManager.ExportToExcelFromUltraGrid(this.griLinkResult, "LINK");
        }

        private void butGetCheckpointExcel_Click(object sender, EventArgs e)
        {
            WaterNetCore.FormManager.ExportToExcelFromUltraGrid(this.griRealtimeCheck, "실측값(고정식)");
        }

        private void butSelectDumpExcel_Click(object sender, EventArgs e)
        {
            WaterNetCore.FormManager.ExportToExcelFromUltraGrid(this.griMeasureDump, "실측값(이동식)");
        }
        #endregion 엑셀내보내기

        public void SetLockControls(string date)
        {
            if (this.tabAnalysisResult.Tabs.GetItem(3) != null) this.tabAnalysisResult.Tabs.RemoveAt(3);
            if (this.tabAnalysisResult.Tabs.GetItem(2) != null) this.tabAnalysisResult.Tabs.RemoveAt(2);

            this.tableLayoutPanel2.Enabled = false;
            this.tableLayoutPanel8.Enabled = false;
            this.datNodeDate.Value = date;
            this.datLinkDate.Value = date;
            this.butApplyNodeFilter_Click(this, new EventArgs());
            this.butApplyLinkFilter_Click(this, new EventArgs());
        }
    }
}
