﻿namespace WaterNet.WH_RTAnalysis.form
{
    partial class frmAnalysisResultRangeManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.griAnalysisResultRange = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.texInpNumber = new System.Windows.Forms.TextBox();
            this.texLayerType = new System.Windows.Forms.TextBox();
            this.texItem = new System.Windows.Forms.TextBox();
            this.texRange1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.texRange2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.texRange3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.texRange4 = new System.Windows.Forms.TextBox();
            this.butSaveAnalysisResultRange = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisResultRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // griAnalysisResultRange
            // 
            this.griAnalysisResultRange.Dock = System.Windows.Forms.DockStyle.Top;
            this.griAnalysisResultRange.Location = new System.Drawing.Point(10, 9);
            this.griAnalysisResultRange.Name = "griAnalysisResultRange";
            this.griAnalysisResultRange.Size = new System.Drawing.Size(772, 300);
            this.griAnalysisResultRange.TabIndex = 123;
            this.griAnalysisResultRange.Text = "ultraGrid1";
            this.griAnalysisResultRange.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griAnalysisResultRange_AfterSelectChange);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(782, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 462);
            this.pictureBox3.TabIndex = 120;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox6.Location = new System.Drawing.Point(0, 9);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(10, 462);
            this.pictureBox6.TabIndex = 119;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 471);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(792, 9);
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(792, 9);
            this.pictureBox2.TabIndex = 116;
            this.pictureBox2.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.texInpNumber, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.texLayerType, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.texItem, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.texRange1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.texRange2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.texRange3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.texRange4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.butSaveAnalysisResultRange, 7, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 318);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(772, 152);
            this.tableLayoutPanel1.TabIndex = 124;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F);
            this.label3.Location = new System.Drawing.Point(40, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 28;
            this.label3.Text = "INP 번호";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F);
            this.label2.Location = new System.Drawing.Point(312, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 12);
            this.label2.TabIndex = 30;
            this.label2.Text = "레이어 구분";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F);
            this.label1.Location = new System.Drawing.Point(544, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 31;
            this.label1.Text = "항목";
            // 
            // texInpNumber
            // 
            this.texInpNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.texInpNumber, 2);
            this.texInpNumber.Font = new System.Drawing.Font("굴림", 9F);
            this.texInpNumber.Location = new System.Drawing.Point(99, 4);
            this.texInpNumber.Name = "texInpNumber";
            this.texInpNumber.ReadOnly = true;
            this.texInpNumber.Size = new System.Drawing.Size(150, 21);
            this.texInpNumber.TabIndex = 36;
            // 
            // texLayerType
            // 
            this.texLayerType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texLayerType.Font = new System.Drawing.Font("굴림", 9F);
            this.texLayerType.Location = new System.Drawing.Point(387, 4);
            this.texLayerType.Name = "texLayerType";
            this.texLayerType.ReadOnly = true;
            this.texLayerType.Size = new System.Drawing.Size(90, 21);
            this.texLayerType.TabIndex = 37;
            // 
            // texItem
            // 
            this.texItem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texItem.Font = new System.Drawing.Font("굴림", 9F);
            this.texItem.Location = new System.Drawing.Point(579, 4);
            this.texItem.Name = "texItem";
            this.texItem.ReadOnly = true;
            this.texItem.Size = new System.Drawing.Size(90, 21);
            this.texItem.TabIndex = 38;
            // 
            // texRange1
            // 
            this.texRange1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texRange1.Font = new System.Drawing.Font("굴림", 9F);
            this.texRange1.Location = new System.Drawing.Point(99, 34);
            this.texRange1.Name = "texRange1";
            this.texRange1.Size = new System.Drawing.Size(90, 21);
            this.texRange1.TabIndex = 39;
            this.texRange1.TextChanged += new System.EventHandler(this.texRange1_TextChanged);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 9F);
            this.label7.Location = new System.Drawing.Point(36, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 12);
            this.label7.TabIndex = 35;
            this.label7.Text = "1st Value";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F);
            this.label4.Location = new System.Drawing.Point(32, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 12);
            this.label4.TabIndex = 32;
            this.label4.Text = "2nd Value";
            // 
            // texRange2
            // 
            this.texRange2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texRange2.Font = new System.Drawing.Font("굴림", 9F);
            this.texRange2.Location = new System.Drawing.Point(99, 64);
            this.texRange2.Name = "texRange2";
            this.texRange2.Size = new System.Drawing.Size(90, 21);
            this.texRange2.TabIndex = 40;
            this.texRange2.TextChanged += new System.EventHandler(this.texRange2_TextChanged);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F);
            this.label6.Location = new System.Drawing.Point(35, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 12);
            this.label6.TabIndex = 34;
            this.label6.Text = "3rd Value";
            // 
            // texRange3
            // 
            this.texRange3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texRange3.Font = new System.Drawing.Font("굴림", 9F);
            this.texRange3.Location = new System.Drawing.Point(99, 94);
            this.texRange3.Name = "texRange3";
            this.texRange3.Size = new System.Drawing.Size(90, 21);
            this.texRange3.TabIndex = 41;
            this.texRange3.TextChanged += new System.EventHandler(this.texRange3_TextChanged);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F);
            this.label5.Location = new System.Drawing.Point(36, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 12);
            this.label5.TabIndex = 33;
            this.label5.Text = "4th Value";
            // 
            // texRange4
            // 
            this.texRange4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.texRange4.Font = new System.Drawing.Font("굴림", 9F);
            this.texRange4.Location = new System.Drawing.Point(99, 125);
            this.texRange4.Name = "texRange4";
            this.texRange4.Size = new System.Drawing.Size(90, 21);
            this.texRange4.TabIndex = 42;
            this.texRange4.TextChanged += new System.EventHandler(this.texRange4_TextChanged);
            // 
            // butSaveAnalysisResultRange
            // 
            this.butSaveAnalysisResultRange.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butSaveAnalysisResultRange.Font = new System.Drawing.Font("굴림", 9F);
            this.butSaveAnalysisResultRange.Location = new System.Drawing.Point(726, 125);
            this.butSaveAnalysisResultRange.Name = "butSaveAnalysisResultRange";
            this.butSaveAnalysisResultRange.Size = new System.Drawing.Size(43, 22);
            this.butSaveAnalysisResultRange.TabIndex = 29;
            this.butSaveAnalysisResultRange.Text = "저장";
            this.butSaveAnalysisResultRange.UseVisualStyleBackColor = true;
            this.butSaveAnalysisResultRange.Click += new System.EventHandler(this.butSaveAnalysisResultRange_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 309);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(772, 9);
            this.pictureBox4.TabIndex = 125;
            this.pictureBox4.TabStop = false;
            // 
            // frmAnalysisResultRangeManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 480);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.griAnalysisResultRange);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAnalysisResultRangeManage";
            this.Text = "해석결과출력설정";
            this.Load += new System.EventHandler(this.frmAnalysisResultRangeManage_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmAnalysisResultRangeManage_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.griAnalysisResultRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid griAnalysisResultRange;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button butSaveAnalysisResultRange;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox texInpNumber;
        public System.Windows.Forms.TextBox texLayerType;
        public System.Windows.Forms.TextBox texItem;
        public System.Windows.Forms.TextBox texRange2;
        public System.Windows.Forms.TextBox texRange1;
        public System.Windows.Forms.TextBox texRange3;
        public System.Windows.Forms.TextBox texRange4;
    }
}