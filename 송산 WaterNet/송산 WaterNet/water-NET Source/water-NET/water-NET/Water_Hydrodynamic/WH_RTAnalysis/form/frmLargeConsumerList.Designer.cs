﻿namespace WaterNet.WH_RTAnalysis.form
{
    partial class frmLargeConsumerList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.griLargeConsumerList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.butDeleteLargeConsumer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.griLargeConsumerList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // griLargeConsumerList
            // 
            this.griLargeConsumerList.Dock = System.Windows.Forms.DockStyle.Top;
            this.griLargeConsumerList.Location = new System.Drawing.Point(10, 9);
            this.griLargeConsumerList.Name = "griLargeConsumerList";
            this.griLargeConsumerList.Size = new System.Drawing.Size(422, 148);
            this.griLargeConsumerList.TabIndex = 123;
            this.griLargeConsumerList.Text = "ultraGrid1";
            this.griLargeConsumerList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.griLargeConsumerList_DoubleClickRow);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox3.Location = new System.Drawing.Point(10, 190);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(422, 9);
            this.pictureBox3.TabIndex = 122;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(10, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(422, 9);
            this.pictureBox2.TabIndex = 121;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(432, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(10, 199);
            this.pictureBox1.TabIndex = 120;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox7.Location = new System.Drawing.Point(0, 0);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 199);
            this.pictureBox7.TabIndex = 119;
            this.pictureBox7.TabStop = false;
            // 
            // butDeleteLargeConsumer
            // 
            this.butDeleteLargeConsumer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.butDeleteLargeConsumer.Font = new System.Drawing.Font("굴림", 9F);
            this.butDeleteLargeConsumer.Location = new System.Drawing.Point(387, 165);
            this.butDeleteLargeConsumer.Name = "butDeleteLargeConsumer";
            this.butDeleteLargeConsumer.Size = new System.Drawing.Size(43, 22);
            this.butDeleteLargeConsumer.TabIndex = 125;
            this.butDeleteLargeConsumer.Text = "삭제";
            this.butDeleteLargeConsumer.UseVisualStyleBackColor = true;
            this.butDeleteLargeConsumer.Click += new System.EventHandler(this.butDeleteLargeConsumer_Click);
            // 
            // frmLargeConsumerList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 199);
            this.Controls.Add(this.butDeleteLargeConsumer);
            this.Controls.Add(this.griLargeConsumerList);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLargeConsumerList";
            this.Text = "동작중 대수용가";
            this.Load += new System.EventHandler(this.frmLargeConsumerList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.griLargeConsumerList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid griLargeConsumerList;
        private System.Windows.Forms.Button butDeleteLargeConsumer;
    }
}