﻿/**************************************************************************
 * 파일명   : FTPManager.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.09.29
 * 설명     : INP파일을 FTP로 업로드하기 위한 모듈
 * 변경이력 : 2010.09.29 - 최초생성
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;

namespace WaterNet.WH_INPManage.etc
{
    public class FTPManager
    {
        //서버설정
        private string ftpIp    = "192.168.40.28";                              //ftp서버 IP
        private string ftpPort  = "21";                                         //ftp서버 Port
        private string ftpUid   = "administrator";                              //ftp서버 사용자 ID
        private string ftpPwd   = "dserver";                                    //ftp서버 사용자 password

        //기본설정
        const int       TRANSFER_BUFFER_SIZE    = 1024;
        const int       READ_BUFFER_SIZE        = 512;
        const int       TRANSFER_ASCII          = 0;
        const int       TRANSFER_BINARY         = 1;
        const string    FTP_UP_DIR              = "";

        //소켓설정
        private Socket  mFTPClient              = null;

        //기타설정
        private bool    mIsConnected            = false;
        private bool    mIsFirstConn            = true;
/***********************************************************************
    외부노출 메소드 선언 시작
 ***********************************************************************/
        //생성자
        public FTPManager()
        {
        }

        //FTP파일전송 요청
        public string TransferFile(Hashtable serverInfos, Hashtable fileInfos)
        {
            string convertedFileName = "";

            if(serverInfos != null)
            {
                //외부에서 넘겨받은 ftp 서버정보가 있다면 할당한다.
                ftpIp = (String)serverInfos["ftpIp"];
                ftpPort = (String)serverInfos["ftpPort"];
                ftpUid = (String)serverInfos["ftpUid"];
                ftpPwd = (String)serverInfos["ftpPwd"];
            }

            //ftp서버와 연결
            ConnectClient();
            convertedFileName = UploadFile1(fileInfos);
            DisconnectClient();

            return convertedFileName;
        }

        //파일 찾아보기
        public Hashtable SelectLocalFile()
        {
            OpenFileDialog oOFD = new OpenFileDialog();

            oOFD.Filter = "INP Files files (*.inp)|*.inp";
            oOFD.FilterIndex = 1;
            oOFD.RestoreDirectory = true;
            oOFD.InitialDirectory = @"D:\00.Work\01.Water-Net\09.etc\FTPUploadSample\bin\Debug";
            oOFD.Multiselect = false;
            oOFD.ShowDialog();

            //파일 물리경로 분해
            string fileName = oOFD.FileName;
            char[] delemeter = { '\\' };
            string[] tmpArray = fileName.Split(delemeter);
            StringBuilder filePath = new StringBuilder();

            for (int i = 0; i < tmpArray.Length - 1; i++)
            {
                filePath.Append(tmpArray[i]);

                if (i != tmpArray.Length - 2)
                {
                    filePath.Append(@"\");
                }
            }

            //파일정보 할당
            Hashtable fileInfos = new Hashtable();

            fileInfos.Add("filePath", filePath.ToString());
            fileInfos.Add("fileName", oOFD.SafeFileName);
            fileInfos.Add("fileSize", GetFileSize(filePath.ToString(), oOFD.SafeFileName));

            return fileInfos;
        }

        //테스트용 메소드
        public void StartTest()
        {
            //ConnectClient();
            //UploadFile(ExtractFileInfo());

            try
            {
                //TransferFile(null);
            }
            catch (Exception e2)
            {
                Console.WriteLine(e2.StackTrace);
            }
        }
/***********************************************************************
    외부노출 메소드 선언 종료
 ***********************************************************************/
/***********************************************************************
    내부 메소드 선언 시작
 ***********************************************************************/
        //ftp서버에 연결
        private void ConnectClient()
        {
            string strConnResult = string.Empty;

            int intPort         = Int32.Parse(this.ftpPort);
            IPHostEntry IPHost  = Dns.GetHostByName(this.ftpIp);
            IPAddress   IPAddr  = IPHost.AddressList[0];               //ip of the host name
            IPEndPoint IPEPoint = new IPEndPoint(IPAddr, intPort);
            mFTPClient          = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            
            mFTPClient.Connect(IPEPoint);

            if (mFTPClient.Connected == true)
            {
                string strCommand = string.Empty;

                //FTP User ID Send
                strCommand = "USER " + this.ftpUid + "\r\n";
                SendFTPCommand(strCommand);
                strConnResult = GetFTPResponseCommand();

                //FTP User Password Send
                strCommand = "PASS " + this.ftpPwd + "\r\n";
                SendFTPCommand(strCommand);

                do
                {
                    strConnResult = GetFTPResponseCommand();
                    if (strConnResult == "")
                    {
                        strConnResult = "530 로그인 실패";
                        break;
                    }
                } while (strConnResult.Substring(0, 3) != "230");

                //230 : Login successful.
                //530 : Login Incorrect.
                if (strConnResult.Substring(0, 3) != "230")
                {
                    DisconnectClient();
                    mIsConnected = false;
                }
                else
                {
                    mIsConnected = true;
                }

                mIsFirstConn = true;
            }
            else
            {
                mIsConnected = false;
            }
        }

        //ftp서버로 command 전송
        private void SendFTPCommand(string strCommand)
        {
            mFTPClient.Send(Encoding.ASCII.GetBytes(strCommand), strCommand.Length, 0);
        }

        //ftp서버의 응답을 전송받음
        private string GetFTPResponseCommand()
        {
            string strRcvMessage = string.Empty;

            Byte[] btReadBytes;
            int intRcvSize = 0;
            //***string strRcvMessage = string.Empty;
            bool blLastLine;

            mFTPClient.Poll(500, SelectMode.SelectRead);

            // Go back for more if necessary
            do
            {
                btReadBytes = new Byte[512];
                intRcvSize = mFTPClient.Receive(btReadBytes);
                strRcvMessage += Encoding.ASCII.GetString(btReadBytes, 0, intRcvSize);
            }
            while (intRcvSize == btReadBytes.Length && !strRcvMessage.EndsWith("\r\n"));

            if (strRcvMessage == "")
            {
                return string.Empty;
            }

            int intCode = Convert.ToInt32(strRcvMessage.Substring(0, 3));
            if (strRcvMessage[3] == '-' & strRcvMessage.IndexOf("\r\n" + intCode + ' ') == -1)
            {
                // Go back for more if necessary
                do
                {
                    btReadBytes = new Byte[512];
                    intRcvSize = mFTPClient.Receive(btReadBytes);
                    strRcvMessage += Encoding.ASCII.GetString(btReadBytes, 0, intRcvSize);

                    blLastLine = strRcvMessage.IndexOf("\r\n" + intCode + ' ') > 0;
                }
                while (!blLastLine | !strRcvMessage.EndsWith("\r\n"));
            }

            return strRcvMessage;
        }

        //ftp서버와 연결을 종료
        private void DisconnectClient()
        {
            string strCommand = string.Empty;

            strCommand = "quit \r\n";
            SendFTPCommand(strCommand);
            GetFTPResponseCommand();
            mFTPClient.Close();

            mIsConnected = false;
        }

        //접속한 ftp서버 내부의 파일리스트를 반환
        private string GetFileListFromFTPServer(string fileName)
        {
            string strCommand = string.Empty;
            string strRes = string.Empty;
            Byte[] btReadBytes;
            int intRcvSize;
            Socket oPasvSoc = null;

            try
            {
                oPasvSoc = OepnAndGetPASVSocket();

                if (fileName == "")
                {
                    strCommand = "LIST *.*\r\n";
                }
                else
                {
                    strCommand = "LIST " + fileName + "\r\n";
                }
                SendFTPCommand(strCommand);
                GetFTPResponseCommand();

                do
                {
                    btReadBytes = new Byte[READ_BUFFER_SIZE];
                    intRcvSize = oPasvSoc.Receive(btReadBytes, btReadBytes.Length, SocketFlags.None);
                    strRes += Encoding.ASCII.GetString(btReadBytes, 0, intRcvSize);
                    if (intRcvSize == 0) break;
                }
                while (true);  //sizeReceived == readBytes.Length & !serverMessage.EndsWith("\r\n")); // Go back for more if necessary

            }
            catch (Exception ex)
            {
                //발생한 Exception을 그대로 돌려준다. (finally절을 사용하기 위해 try절을 사용)
                throw ex;
            }
            finally
            {
                oPasvSoc.Close(); //위에서 패시브 모드용으로 열었기 때문에 반드시 닫아야 한다.
            }

            return strRes;
        }
        
        //소켓할당 및 Open
        private Socket OepnAndGetPASVSocket()
        {
            Socket oPasvSock = null;

            string strCommand = string.Empty;
            string strResCommand = string.Empty;
            string strFullAddress = string.Empty;

            //********************************************************************
            //필독 : PASV 모드로 전환시.. 그 응답 Command가 늦게 오는 경우가 있음.
            //       따라서 아래와 같이 227을 꼭 받고 Pasv모드가 되야만 넘어가야 한다.
            //********************************************************************

            //PASV:Specifies that the server data transfer process is to listen for a connection request from the client data transfer process. 
            strCommand = "PASV" + "\r\n";

            SendFTPCommand(strCommand);

            if (mIsFirstConn == true)
            {
                strResCommand = GetFTPResponseCommand();
                mIsFirstConn = false;
            }

            // Get server IP and port address
            bool IsPasvOK = false;


            if (strResCommand.Length > 0 && strResCommand.Substring(0, 3) == "227")
            {
                IsPasvOK = true;
            }
            else
            {
                do
                {
                    strResCommand = GetFTPResponseCommand();
                    if (strResCommand.Length > 0 && strResCommand.Substring(0, 3) == "227")
                    {
                        IsPasvOK = true;
                        break;
                    }
                } while (IsPasvOK == false);
            }
            strFullAddress = strResCommand;

            strFullAddress = strFullAddress.Remove(0, strFullAddress.IndexOf('(') + 1);//takes only what is 
            strFullAddress = strFullAddress.Substring(0, strFullAddress.IndexOf(')'));//between paranthesis
            string[] arrAddrParts = strFullAddress.Split(',');
            string strPasvAddress = arrAddrParts[0] + "." + arrAddrParts[1] + "." + arrAddrParts[2] + "." + arrAddrParts[3];
            int intPasvPort = Convert.ToInt32(arrAddrParts[4]) * 256 + Convert.ToInt32(arrAddrParts[5]);

            // Open the Data socket
            oPasvSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            oPasvSock.Connect(new IPEndPoint(IPAddress.Parse(strPasvAddress), intPasvPort));

            return oPasvSock;
        }

        //File Size 리턴
        private int GetFileSize(string strSrcFilePath, string strSrcFileName)
        {
            try
            {
                int iSize = 0;

                DirectoryInfo   diRootDir       = new DirectoryInfo(strSrcFilePath);
                FileInfo[]      arrFileInfor    = diRootDir.GetFiles(strSrcFileName);

                foreach (FileInfo arrFileInfo in arrFileInfor)
                {
                    iSize = Convert.ToInt32(arrFileInfo.Length.ToString());
                }

                return iSize;
            }
            catch
            {
                return 0;
            }
        }

        //ftp로 파일을 업로드
        private string UploadFile1(Hashtable fileInfos)
        {
            //같은 파일이 존재하는 경우 파일명을 변환한다.
            string splitedFileName = ((String)fileInfos["fileName"]).Substring(0, ((String)fileInfos["fileName"]).Length - 4);
            string tmpFileName = splitedFileName;
            int fileIndex = 1;

            while (!GetFileListFromFTPServer(tmpFileName + ".inp").Equals(""))
            {
                tmpFileName = splitedFileName + "_" + fileIndex;
                fileIndex++;
            }

            Byte[] btReadBytes;
            int intRcvSize = 0;
            int intReadBytes = 0;
            int intTotalByte = 0;

            string strCommand = string.Empty;

            //Upload할 FTP Type을 Binary로 설정한다.
            SetFTPTransferType();

            ChangeFTPDirectory(FTP_UP_DIR + "/");

            FileStream oFileStream = null;
            Socket oPasvSoc = null;

            try
            {
                oFileStream = new FileStream((string)fileInfos["filePath"] + @"\" + (string)fileInfos["fileName"], FileMode.Open);

                //Upload용 PASV Socket Object를 생성한다.
                oPasvSoc = OepnAndGetPASVSocket();

                //Store file on remote system over-writing the file if it already exists. 
                strCommand = "stor " + tmpFileName + ".inp";
                strCommand += "\r\n";
                SendFTPCommand(strCommand);

                string strRcvCommand = GetFTPResponseCommand();

                FileInfo oFi = new FileInfo((string)fileInfos["fileName"]);

                do
                {
                    if (intTotalByte == (int)fileInfos["fileSize"])
                    {
                        break;
                    }
                    if ((intTotalByte < (int)fileInfos["fileSize"]) && ((int)fileInfos["fileSize"] < TRANSFER_BUFFER_SIZE))
                    {
                        btReadBytes = new Byte[(int)fileInfos["fileSize"]];
                    }
                    else
                    {
                        if ((int)fileInfos["fileSize"] < (intTotalByte + TRANSFER_BUFFER_SIZE))
                        {
                            btReadBytes = new Byte[(((int)fileInfos["fileSize"]) - intTotalByte)];
                        }
                        else
                        {
                            btReadBytes = new Byte[TRANSFER_BUFFER_SIZE];
                        }
                    }
                    intReadBytes = oFileStream.Read(btReadBytes, 0, btReadBytes.Length);
                    if (intReadBytes == 0)
                    {
                        break;
                    }
                    intRcvSize = oPasvSoc.Send(btReadBytes);
                    strRcvCommand = Encoding.ASCII.GetString(btReadBytes, 0, intRcvSize);
                    intTotalByte += intReadBytes;
                }
                while (true);
            }
            catch (Exception e1)
            {
                throw e1;
            }
            finally
            {
                oFileStream.Close();//close file
                oPasvSoc.Close();//close socket
            }

            return tmpFileName + ".inp";
        }

        //ftp Server의 Transfer Type을 변경
        private void SetFTPTransferType()
        {
            string strCommand = string.Empty;

            //set type as BINARY
            strCommand = "TYPE I\r\n";

            SendFTPCommand(strCommand);
            GetFTPResponseCommand();
        }

        //ftp Server에서 특정 Directory로 이동
        private void ChangeFTPDirectory(string strPath)
        {
            string strCommand = string.Empty;

            strCommand = "CWD " + strPath + "\r\n";
            SendFTPCommand(strCommand);
            GetFTPResponseCommand();
        }
/***********************************************************************
    내부 메소드 선언 종료
 ***********************************************************************/
    }
}
