﻿/**************************************************************************
 * 파일명   : frmWHINPRegist.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.25
 * 설명     : INP파일 등록용 화면
 * 변경이력 : 2010.10.25 - 최초생성
 **************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WH_INPManage.work;
using WaterNet.WH_INPManage.etc;
using WaterNet.WH_PipingNetworkAnalysis.epanet;
using WaterNet.WH_Common.utils;
using WaterNet.WH_Common.work;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using EMFrame.log;

namespace WaterNet.WH_INPManage.form
{
    public partial class frmWHINPRegist : Form
    {
        public string INP_NUMBER = string.Empty;  //새로이 생성된 INP번호
        private INPManagetWork work = null;
        private CommonWork cWork = null;          
        private Hashtable fileInfos = null;
        public frmWHINPManage manager = null;

        FTPManager fManager = new FTPManager();

        public frmWHINPRegist()
        {
            work = INPManagetWork.GetInstance();                                                     //Business layer 생성
            cWork = CommonWork.GetInstance();
            InitializeComponent();
        }

        //INP파일 등록 버튼
        private void butRegistINPFile_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                //입력값 validation
                if (!RegistValidation())
                {
                    return;
                }

                try
                {
                    //FTP로 파일전송
                    //string convertedFileName = fManager.TransferFile(null, fileInfos);

                    Hashtable conditions = new Hashtable();

                    conditions.Add("INP_NUMBER", Utils.GetSerialNumber((string)comUseGbn.SelectedValue));
                    conditions.Add("USE_GBN", comUseGbn.SelectedValue);
                    conditions.Add("WSP_NAM", "");
                    conditions.Add("LFTRIDN", comLftridn.SelectedValue);
                    conditions.Add("MFTRIDN", comMftridn.SelectedValue);
                    conditions.Add("SFTRIDN", comSftridn.SelectedValue);
                    conditions.Add("TITLE", texTitle.Text);
                    conditions.Add("REMARK", texRemark.Text);
                    conditions.Add("ENERGY_GBN", chkEnergyGbn.Checked ? "Y" : string.Empty);
                    conditions.Add("FILE_NAME", "1111");
                    conditions.Add("sectionData", ParsingInpData(fileInfos["filePath"] + @"\" + fileInfos["fileName"]));                  //Parsing한 INP의 Section 정보

                    work.RegistINPData(conditions);
                    MessageBox.Show("정상적으로 처리되었습니다.");

                    this.INP_NUMBER = Convert.ToString(conditions["INP_NUMBER"]);
                    manager.SelectInpList();
                    this.Close();
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }

                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //찾아보기 버튼
        private void butSelectFile_Click(object sender, EventArgs e)
        {
            fileInfos = fManager.SelectLocalFile();

            if (fileInfos["filePath"] == null || fileInfos["fileName"] == null || fileInfos["fileSize"] == null)
            {
                MessageBox.Show("파일정보 오류");
            }
            else
            {
                texFilePath.Text = (string)fileInfos["filePath"] + "\\" + (string)fileInfos["fileName"];
            }
        }

        //INP파일을 섹션별로 분해
        private Hashtable ParsingInpData(string fileFullPath)
        {
            EPANETMethodDefinition.LoadNet();
            
            StringBuilder ffp = new StringBuilder(fileFullPath);

            EPANETMethodDefinition.ReadInpFile(ffp, false);

            Hashtable result = new Hashtable();                                 //전체 섹션 parsing 결과를 저장할 ArraList            

            try
            {
                result.Add("junction", GetSectionalData("[JUNCTION"));          //Junction 정보 발췌
                result.Add("reservoir", GetSectionalData("[RESERVOIR"));        //Reservior 정보 발췌
                result.Add("tank", GetSectionalData("[TANK"));                  //Tank 정보 발췌
                result.Add("pipe", GetSectionalData("[PIPE"));                  //Pipe 정보 발췌
                result.Add("pump", GetSectionalData("[PUMP"));                  //Pump 정보 발췌
                result.Add("valve", GetSectionalData("[VALVE"));                //Valve 정보 발췌
                result.Add("demand", GetSectionalData("[DEMAND"));              //Demand 정보 발췌
                result.Add("status", GetSectionalData("[STATUS"));              //Status 정보 발췌
                result.Add("curve", GetSectionalData("[CURVE"));                //Curve 정보 발췌
                result.Add("pattern", GetSectionalData("pattern"));             //Pattern 정보 발췌 - Pattern은 parsing하는 양식이 다른관계로 key의 형태가 다름
                result.Add("control", GetSectionalData("[CONTROL"));            //Control 정보 발췌
                result.Add("rule", GetSectionalData("[RULE"));                  //Rule 정보 발췌
                result.Add("energy", GetSectionalData("[ENERGY"));              //Energy 정보 발췌
                result.Add("emitter", GetSectionalData("[EMITTER"));            //Emitter 정보 발췌
                result.Add("quality", GetSectionalData("[QUALITY"));            //Quality 정보 발췌
                result.Add("source", GetSectionalData("[SOURCE"));              //Source 정보 발췌
                result.Add("reaction", GetSectionalData("[REACTION"));          //Reaction 정보 발췌
                result.Add("mixing", GetSectionalData("[MIXING"));              //Mixing 정보 발췌
                result.Add("timeOption", GetSectionalData("[TIME"));            //Time Option 정보 발췌
                result.Add("reportOption", GetSectionalData("[REPORT"));        //Report Option 정보 발췌
                result.Add("normalOption", GetSectionalData("[OPTION"));        //Normal Option 정보 발췌
                result.Add("coordinate", GetSectionalData("[COORDINATE"));      //Coordinate 정보 발췌                
                result.Add("vertices", GetSectionalData("[VERTICE"));           //Vertices 정보 발췌
                result.Add("label", GetSectionalData("[LABEL"));                //Label 정보 발췌
                result.Add("tag", GetSectionalData("[TAG"));                    //Tag 정보 발췌
            }
            catch (Exception e1)
            {
                throw e1;
            }
            finally
            {
                EPANETMethodDefinition.UnloadNet();
            }

            return result;
        }

        //섹션별 데이터를 반환
        private ArrayList GetSectionalData(string sectionKey)
        {
            //ArrayList  result = new ArrayList();
            ArrayList result = null;

            if(sectionKey.Equals("pattern"))
            {
                //패턴 섹션의 정보를 parsing
                int cnt = EPANETMethodDefinition.CountAllPattern();

                for (int i = 0; i < cnt; i++ )
                {
                    if (result == null) result = new ArrayList();

                    //패턴의 종류 수 만큼 순환

                    Hashtable patternData   = new Hashtable();                              //패턴 ID를 key로 패턴 데이터들을 저장
                    ArrayList patternValues = new ArrayList();                              //패턴의 순차값

                    //StringBuilder patternId = EPANETMethodDefinition.GetPatternId(i);
                    StringBuilder patternId = new StringBuilder();
                    EPANETMethodDefinition.GetPatternId(i, ref patternId);
                    int valueCount = EPANETMethodDefinition.CountPattern(i);

                    for (int j = 0; j < valueCount; j++ )
                    {
                        //해당 Pattern ID에 대한 Value를 저장
                        patternValues.Add(EPANETMethodDefinition.GetPatternValue(i,j));
                    }

                    patternData.Add("id",patternId.ToString());
                    patternData.Add("values", patternValues);
                    
                    result.Add(patternData);
                }
            }
            else
            {
                //기타 순환정보에 대한 parsing
                int sIdx = 0;
                int eIdx = 0;

                EPANETMethodDefinition.SectionBetween(new StringBuilder(sectionKey), ref sIdx, ref eIdx);

                for (int i = sIdx; i <= eIdx; i++)
                {
                    if (result == null) result = new ArrayList();

                    StringBuilder v1 = new StringBuilder(255);
                    StringBuilder v2 = new StringBuilder(255);
                    StringBuilder v3 = new StringBuilder(255);
                    StringBuilder v4 = new StringBuilder(255);
                    StringBuilder v5 = new StringBuilder(255);
                    StringBuilder v6 = new StringBuilder(255);
                    StringBuilder v7 = new StringBuilder(255);
                    StringBuilder v8 = new StringBuilder(255);
                    StringBuilder v9 = new StringBuilder(255);
                    StringBuilder v10 = new StringBuilder(255);

                    if (!EPANETMethodDefinition.GetSectionValue1(i, ref v1, ref  v2, ref  v3, ref  v4, ref  v5, ref  v6, ref  v7, ref  v8, ref  v9, ref  v10))
                    {
                        continue;
                    }

                    ArrayList tmpList = new ArrayList();
                    //StringBuilder comment = EPANETMethodDefinition.GetSectionComment(i);

                    tmpList.Add(v1.ToString());
                    tmpList.Add(v2.ToString());
                    tmpList.Add(v3.ToString());
                    tmpList.Add(v4.ToString());
                    tmpList.Add(v5.ToString());
                    tmpList.Add(v6.ToString());
                    tmpList.Add(v7.ToString());
                    tmpList.Add(v8.ToString());
                    tmpList.Add(v9.ToString());
                    tmpList.Add(v10.ToString());
                    //tmpList.Add(comment.ToString());
                    tmpList.Add("");

                    result.Add(tmpList);
                }
            }

            return result;
        }

        //폼 최초로딩때 실행
        private void frmWHINPRegist_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================에너지관리 

            object o = EMFrame.statics.AppStatic.USER_MENU["iNP파일관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.butRegistINPFile.Enabled = false;
            }

            //===========================================================================

            //사용구분정보 할당(검색조건)
            comUseGbn.ValueMember = "CODE";
            comUseGbn.DisplayMember = "CODE_NAME";

            Hashtable conditions = new Hashtable();
            conditions.Add("PCODE", "1001");

            DataSet useGbnDataSet = cWork.GetCodeList(conditions);

            if (useGbnDataSet != null)
            {
                comUseGbn.DataSource = useGbnDataSet.Tables["CM_CODE"];
            }

            //블록정보 할당(검색조건)
            comLftridn.ValueMember = "LOC_CODE";
            comLftridn.DisplayMember = "LOC_NAME";
            comMftridn.ValueMember = "LOC_CODE";
            comMftridn.DisplayMember = "LOC_NAME";
            comSftridn.ValueMember = "LOC_CODE";
            comSftridn.DisplayMember = "LOC_NAME";

            DataSet blockDataSet = cWork.GetLblockDataList(new Hashtable());

            if (blockDataSet != null)
            {
                comLftridn.DataSource = blockDataSet.Tables["CM_LOCATION"];
            }
        }

        //입력값 validation
        private bool RegistValidation()
        {
            //제목 미입력
            if (texTitle.Text == "")
            {
                MessageBox.Show("제목을 입력하십시오.");
                texTitle.Focus();

                return false;
            }

            //수리/수질인 경우
            //if ("WH".Equals(comUseGbn.SelectedValue.ToString()) || "WQ".Equals(comUseGbn.SelectedValue.ToString()))
            if ("WH".Equals(comUseGbn.SelectedValue.ToString()))
            {
                if ("".Equals(comLftridn.SelectedValue))
                {
                    MessageBox.Show("대블록을 선택하십시오.");
                    comLftridn.Focus();

                    return false;
                }

                //if ("".Equals(comMftridn.SelectedValue))
                //{
                //    MessageBox.Show("중블록을 선택하십시오.");
                //    comMftridn.Focus();

                //    return false;
                //}
            }

            //inp파일 미선택
            if (texFilePath.Text == "")
            {
                MessageBox.Show("업로드 하실 파일을 선택하십시오.");
                butSelectFile.Focus();

                return false;
            }

            return true;
        }

        //닫기버튼 클릭
        private void butFrmClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //사용구분 콤보박스가 변경된 경우
        private void comUseGbn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ("WH".Equals(comUseGbn.SelectedValue.ToString()) || "WQ".Equals(comUseGbn.SelectedValue.ToString()))
            {
                comSftridn.SelectedValue = "";
                comSftridn.Enabled = false;
            }
            else
            {
                comSftridn.Enabled = true;
            }
        }

        //대블록 콤보박스가 변경된 경우
        private void comLftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comLftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comMftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }

        //중블록 콤보박스가 변경된 경우
        private void comMftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comMftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comSftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }
    }
}
