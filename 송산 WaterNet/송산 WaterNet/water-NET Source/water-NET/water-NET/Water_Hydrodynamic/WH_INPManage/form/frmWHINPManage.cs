﻿/**************************************************************************
 * 파일명   : frmWHINPManage.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.09.29
 * 설명     : INP파일 관리용 메인화면
 * 변경이력 : 2010.09.29 - 최초생성
 **************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WaterNet.WaterNetCore;
using WaterNet.WH_INPManage.work;
using WaterNet.WH_Common.utils;
using WaterNet.WH_Common.work;
using WaterNet.WH_PipingNetworkAnalysis.epanet;

using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using EMFrame.log;

namespace WaterNet.WH_INPManage.form
{
    public partial class frmWHINPManage : Form, WaterNet.WaterNetCore.IForminterface
    {
        private INPManagetWork work = null;
        private CommonWork cWork = null;

        public frmWHINPManage()
        {
            InitializeComponent();
            InitializeSetting();
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        #region 화면 초기화
        //메인폼에서 호출
        public void Open()
        {
        }

        //메인폼에서 호출
        private void frmWQMain_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        //화면초기화
        private void InitializeSetting()
        {
            work = INPManagetWork.GetInstance();
            cWork = CommonWork.GetInstance();

            //메인 그리드 초기화
            UltraGridColumn masterColumn;

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "INP_NUMBER";
            masterColumn.Header.Caption = "일련번호";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Center;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 180;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "TITLE";
            masterColumn.Header.Caption = "제목";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 250;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "USE_GBN";
            masterColumn.Header.Caption = "사용구분";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "USE_GBN_NAME";
            masterColumn.Header.Caption = "사용구분";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "LFTRIDN";
            masterColumn.Header.Caption = "대블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "LFTR_NAME";
            masterColumn.Header.Caption = "대블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "MFTRIDN";
            masterColumn.Header.Caption = "중블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "MFTR_NAME";
            masterColumn.Header.Caption = "중블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "SFTRIDN";
            masterColumn.Header.Caption = "소블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "SFTR_NAME";
            masterColumn.Header.Caption = "소블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "INS_DATE";
            masterColumn.Header.Caption = "등록일자";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Center;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 180;
            masterColumn.Hidden = false;   //필드 보이기

            //masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            //masterColumn.Key = "FILE_NAME";
            //masterColumn.Header.Caption = "파일명";
            //masterColumn.CellActivation = Activation.NoEdit;
            //masterColumn.CellClickAction = CellClickAction.RowSelect;
            //masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            //masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            //masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            ////masterColumn.Width = 200;
            //masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "REMARK";
            masterColumn.Header.Caption = "비고";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //masterColumn.Width = 300;
            masterColumn.Hidden = false;   //필드 보이기

            //INP리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griINPList);

            //Junction tab의 Grid
            UltraGridColumn junctionColumn;

            junctionColumn = griJunctionList.DisplayLayout.Bands[0].Columns.Add();
            junctionColumn.Key = "ACTION";
            junctionColumn.Header.Caption = "ACTION";
            junctionColumn.CellActivation = Activation.NoEdit;
            junctionColumn.CellClickAction = CellClickAction.RowSelect;
            junctionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            junctionColumn.CellAppearance.TextHAlign = HAlign.Left;
            junctionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //junctionColumn.Width = 10;
            junctionColumn.Hidden = true;   //필드 보이기

            junctionColumn = griJunctionList.DisplayLayout.Bands[0].Columns.Add();
            junctionColumn.Key = "ID";
            junctionColumn.Header.Caption = "ID";
            junctionColumn.CellActivation = Activation.NoEdit;
            junctionColumn.CellClickAction = CellClickAction.RowSelect;
            junctionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            junctionColumn.CellAppearance.TextHAlign = HAlign.Left;
            junctionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //junctionColumn.Width = 180;
            junctionColumn.Hidden = false;   //필드 보이기

            junctionColumn = griJunctionList.DisplayLayout.Bands[0].Columns.Add();
            junctionColumn.Key = "ELEV";
            junctionColumn.Header.Caption = "Elev";
            junctionColumn.CellActivation = Activation.NoEdit;
            junctionColumn.CellClickAction = CellClickAction.RowSelect;
            junctionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            junctionColumn.CellAppearance.TextHAlign = HAlign.Right;
            junctionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //junctionColumn.Width = 100;
            junctionColumn.Hidden = false;   //필드 보이기

            junctionColumn = griJunctionList.DisplayLayout.Bands[0].Columns.Add();
            junctionColumn.Key = "DEMAND";
            junctionColumn.Header.Caption = "Demand";
            junctionColumn.CellActivation = Activation.NoEdit;
            junctionColumn.CellClickAction = CellClickAction.RowSelect;
            junctionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            junctionColumn.CellAppearance.TextHAlign = HAlign.Right;
            junctionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //junctionColumn.Width = 100;
            junctionColumn.Hidden = false;   //필드 보이기

            junctionColumn = griJunctionList.DisplayLayout.Bands[0].Columns.Add();
            junctionColumn.Key = "PATTERN_ID";
            junctionColumn.Header.Caption = "Pattern";
            junctionColumn.CellActivation = Activation.NoEdit;
            junctionColumn.CellClickAction = CellClickAction.RowSelect;
            junctionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            junctionColumn.CellAppearance.TextHAlign = HAlign.Right;
            junctionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //junctionColumn.Width = 120;
            junctionColumn.Hidden = false;   //필드 보이기

            junctionColumn = griJunctionList.DisplayLayout.Bands[0].Columns.Add();
            junctionColumn.Key = "SFTRIDN";
            junctionColumn.Header.Caption = "소블록";
            junctionColumn.CellActivation = Activation.NoEdit;
            junctionColumn.CellClickAction = CellClickAction.RowSelect;
            junctionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            junctionColumn.CellAppearance.TextHAlign = HAlign.Left;
            junctionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //junctionColumn.Width = 120;
            junctionColumn.Hidden = true;   //필드 보이기

            junctionColumn = griJunctionList.DisplayLayout.Bands[0].Columns.Add();
            junctionColumn.Key = "SFTR_NAME";
            junctionColumn.Header.Caption = "소블록";
            junctionColumn.CellActivation = Activation.NoEdit;
            junctionColumn.CellClickAction = CellClickAction.RowSelect;
            junctionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            junctionColumn.CellAppearance.TextHAlign = HAlign.Left;
            junctionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //junctionColumn.Width = 120;
            junctionColumn.Hidden = false;   //필드 보이기

            junctionColumn = griJunctionList.DisplayLayout.Bands[0].Columns.Add();
            junctionColumn.Key = "REMARK";
            junctionColumn.Header.Caption = "비고";
            junctionColumn.CellActivation = Activation.NoEdit;
            junctionColumn.CellClickAction = CellClickAction.RowSelect;
            junctionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            junctionColumn.CellAppearance.TextHAlign = HAlign.Left;
            junctionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //junctionColumn.Width = 300;
            junctionColumn.Hidden = false;   //필드 보이기

            //Junction 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griJunctionList);

            //reservoir tab의 Grid
            UltraGridColumn reservoirColumn;

            reservoirColumn = griReservoirList.DisplayLayout.Bands[0].Columns.Add();
            reservoirColumn.Key = "ID";
            reservoirColumn.Header.Caption = "ID";
            reservoirColumn.CellActivation = Activation.NoEdit;
            reservoirColumn.CellClickAction = CellClickAction.RowSelect;
            reservoirColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            reservoirColumn.CellAppearance.TextHAlign = HAlign.Left;
            reservoirColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //reservoirColumn.Width = 180;
            reservoirColumn.Hidden = false;   //필드 보이기

            reservoirColumn = griReservoirList.DisplayLayout.Bands[0].Columns.Add();
            reservoirColumn.Key = "HEAD";
            reservoirColumn.Header.Caption = "Head";
            reservoirColumn.CellActivation = Activation.NoEdit;
            reservoirColumn.CellClickAction = CellClickAction.RowSelect;
            reservoirColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            reservoirColumn.CellAppearance.TextHAlign = HAlign.Right;
            reservoirColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //reservoirColumn.Width = 100;
            reservoirColumn.Hidden = false;   //필드 보이기

            reservoirColumn = griReservoirList.DisplayLayout.Bands[0].Columns.Add();
            reservoirColumn.Key = "PATTERN_ID";
            reservoirColumn.Header.Caption = "Pattern";
            reservoirColumn.CellActivation = Activation.NoEdit;
            reservoirColumn.CellClickAction = CellClickAction.RowSelect;
            reservoirColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            reservoirColumn.CellAppearance.TextHAlign = HAlign.Left;
            reservoirColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //reservoirColumn.Width = 120;
            reservoirColumn.Hidden = false;   //필드 보이기
            
            //Reservoir 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griReservoirList);

            //Tank tab의 Grid
            UltraGridColumn tankColumn;

            tankColumn = griTankList.DisplayLayout.Bands[0].Columns.Add();
            tankColumn.Key = "ID";
            tankColumn.Header.Caption = "ID";
            tankColumn.CellActivation = Activation.NoEdit;
            tankColumn.CellClickAction = CellClickAction.RowSelect;
            tankColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tankColumn.CellAppearance.TextHAlign = HAlign.Left;
            tankColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tankColumn.Width = 180;
            tankColumn.Hidden = false;   //필드 보이기

            tankColumn = griTankList.DisplayLayout.Bands[0].Columns.Add();
            tankColumn.Key = "ELEV";
            tankColumn.Header.Caption = "Elevation";
            tankColumn.CellActivation = Activation.NoEdit;
            tankColumn.CellClickAction = CellClickAction.RowSelect;
            tankColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tankColumn.CellAppearance.TextHAlign = HAlign.Left;
            tankColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tankColumn.Width = 100;
            tankColumn.Hidden = false;   //필드 보이기

            tankColumn = griTankList.DisplayLayout.Bands[0].Columns.Add();
            tankColumn.Key = "INITLVL";
            tankColumn.Header.Caption = "InitLevel";
            tankColumn.CellActivation = Activation.NoEdit;
            tankColumn.CellClickAction = CellClickAction.RowSelect;
            tankColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tankColumn.CellAppearance.TextHAlign = HAlign.Right;
            tankColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tankColumn.Width = 100;
            tankColumn.Hidden = false;   //필드 보이기

            tankColumn = griTankList.DisplayLayout.Bands[0].Columns.Add();
            tankColumn.Key = "MINLVL";
            tankColumn.Header.Caption = "MinLevel";
            tankColumn.CellActivation = Activation.NoEdit;
            tankColumn.CellClickAction = CellClickAction.RowSelect;
            tankColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tankColumn.CellAppearance.TextHAlign = HAlign.Right;
            tankColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tankColumn.Width = 100;
            tankColumn.Hidden = false;   //필드 보이기

            tankColumn = griTankList.DisplayLayout.Bands[0].Columns.Add();
            tankColumn.Key = "MAXLVL";
            tankColumn.Header.Caption = "MaxLevel";
            tankColumn.CellActivation = Activation.NoEdit;
            tankColumn.CellClickAction = CellClickAction.RowSelect;
            tankColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tankColumn.CellAppearance.TextHAlign = HAlign.Right;
            tankColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tankColumn.Width = 100;
            tankColumn.Hidden = false;   //필드 보이기

            tankColumn = griTankList.DisplayLayout.Bands[0].Columns.Add();
            tankColumn.Key = "DIAM";
            tankColumn.Header.Caption = "Diameter";
            tankColumn.CellActivation = Activation.NoEdit;
            tankColumn.CellClickAction = CellClickAction.RowSelect;
            tankColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tankColumn.CellAppearance.TextHAlign = HAlign.Right;
            tankColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tankColumn.Width = 100;
            tankColumn.Hidden = false;   //필드 보이기

            tankColumn = griTankList.DisplayLayout.Bands[0].Columns.Add();
            tankColumn.Key = "MINVOL";
            tankColumn.Header.Caption = "MinVol";
            tankColumn.CellActivation = Activation.NoEdit;
            tankColumn.CellClickAction = CellClickAction.RowSelect;
            tankColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tankColumn.CellAppearance.TextHAlign = HAlign.Right;
            tankColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tankColumn.Width = 100;
            tankColumn.Hidden = false;   //필드 보이기

            tankColumn = griTankList.DisplayLayout.Bands[0].Columns.Add();
            tankColumn.Key = "VOLCURVE_ID";
            tankColumn.Header.Caption = "VolCurve";
            tankColumn.CellActivation = Activation.NoEdit;
            tankColumn.CellClickAction = CellClickAction.RowSelect;
            tankColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tankColumn.CellAppearance.TextHAlign = HAlign.Left;
            tankColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //tankColumn.Width = 120;
            tankColumn.Hidden = false;   //필드 보이기

            //Tank 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griTankList);

            //Quality tab의 Grid
            UltraGridColumn qualityColumn;

            qualityColumn = griQualityList.DisplayLayout.Bands[0].Columns.Add();
            qualityColumn.Key = "ID";
            qualityColumn.Header.Caption = "Node";
            qualityColumn.CellActivation = Activation.NoEdit;
            qualityColumn.CellClickAction = CellClickAction.RowSelect;
            qualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            qualityColumn.CellAppearance.TextHAlign = HAlign.Left;
            qualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //qualityColumn.Width = 180;
            qualityColumn.Hidden = false;   //필드 보이기

            qualityColumn = griQualityList.DisplayLayout.Bands[0].Columns.Add();
            qualityColumn.Key = "INITQUAL";
            qualityColumn.Header.Caption = "InitQual";
            qualityColumn.CellActivation = Activation.NoEdit;
            qualityColumn.CellClickAction = CellClickAction.RowSelect;
            qualityColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            qualityColumn.CellAppearance.TextHAlign = HAlign.Right;
            qualityColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //qualityColumn.Width = 100;
            qualityColumn.Hidden = false;   //필드 보이기

            //Quality 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griQualityList);

            //Source tab의 Grid
            UltraGridColumn sourceColumn;

            sourceColumn = griSourceList.DisplayLayout.Bands[0].Columns.Add();
            sourceColumn.Key = "ID";
            sourceColumn.Header.Caption = "Node";
            sourceColumn.CellActivation = Activation.NoEdit;
            sourceColumn.CellClickAction = CellClickAction.RowSelect;
            sourceColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            sourceColumn.CellAppearance.TextHAlign = HAlign.Left;
            sourceColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //sourceColumn.Width = 180;
            sourceColumn.Hidden = false;   //필드 보이기

            sourceColumn = griSourceList.DisplayLayout.Bands[0].Columns.Add();
            sourceColumn.Key = "TYPE";
            sourceColumn.Header.Caption = "Type";
            sourceColumn.CellActivation = Activation.NoEdit;
            sourceColumn.CellClickAction = CellClickAction.RowSelect;
            sourceColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            sourceColumn.CellAppearance.TextHAlign = HAlign.Left;
            sourceColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //sourceColumn.Width = 120;
            sourceColumn.Hidden = false;   //필드 보이기

            sourceColumn = griSourceList.DisplayLayout.Bands[0].Columns.Add();
            sourceColumn.Key = "STRENGTH";
            sourceColumn.Header.Caption = "Strength";
            sourceColumn.CellActivation = Activation.NoEdit;
            sourceColumn.CellClickAction = CellClickAction.RowSelect;
            sourceColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            sourceColumn.CellAppearance.TextHAlign = HAlign.Right;
            sourceColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //sourceColumn.Width = 100;
            sourceColumn.Hidden = false;   //필드 보이기

            sourceColumn = griSourceList.DisplayLayout.Bands[0].Columns.Add();
            sourceColumn.Key = "PATTERN_ID";
            sourceColumn.Header.Caption = "Pattern";
            sourceColumn.CellActivation = Activation.NoEdit;
            sourceColumn.CellClickAction = CellClickAction.RowSelect;
            sourceColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            sourceColumn.CellAppearance.TextHAlign = HAlign.Left;
            sourceColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //sourceColumn.Width = 120;
            sourceColumn.Hidden = false;   //필드 보이기


            //Quality 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griSourceList);

            //Emitter tab의 Grid
            UltraGridColumn emitterColumn;

            emitterColumn = griEmitterList.DisplayLayout.Bands[0].Columns.Add();
            emitterColumn.Key = "ID";
            emitterColumn.Header.Caption = "Junction";
            emitterColumn.CellActivation = Activation.NoEdit;
            emitterColumn.CellClickAction = CellClickAction.RowSelect;
            emitterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            emitterColumn.CellAppearance.TextHAlign = HAlign.Left;
            emitterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //emitterColumn.Width = 180;
            emitterColumn.Hidden = false;   //필드 보이기

            emitterColumn = griEmitterList.DisplayLayout.Bands[0].Columns.Add();
            emitterColumn.Key = "FLOW_COFFICIENT";
            emitterColumn.Header.Caption = "Coefficient";
            emitterColumn.CellActivation = Activation.NoEdit;
            emitterColumn.CellClickAction = CellClickAction.RowSelect;
            emitterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            emitterColumn.CellAppearance.TextHAlign = HAlign.Right;
            emitterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //emitterColumn.Width = 100;
            emitterColumn.Hidden = false;   //필드 보이기

            //Emitter 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griEmitterList);

            //Label tab의 Grid
            UltraGridColumn labelColumn;

            labelColumn = griLabelList.DisplayLayout.Bands[0].Columns.Add();
            labelColumn.Key = "IDX";
            labelColumn.Header.Caption = "IDX";
            labelColumn.CellActivation = Activation.NoEdit;
            labelColumn.CellClickAction = CellClickAction.RowSelect;
            labelColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            labelColumn.CellAppearance.TextHAlign = HAlign.Left;
            labelColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //labelColumn.Width = 100;
            labelColumn.Hidden = false;   //필드 보이기

            labelColumn = griLabelList.DisplayLayout.Bands[0].Columns.Add();
            labelColumn.Key = "X";
            labelColumn.Header.Caption = "X-Coord";
            labelColumn.CellActivation = Activation.NoEdit;
            labelColumn.CellClickAction = CellClickAction.RowSelect;
            labelColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            labelColumn.CellAppearance.TextHAlign = HAlign.Left;
            labelColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //labelColumn.Width = 100;
            labelColumn.Hidden = false;   //필드 보이기

            labelColumn = griLabelList.DisplayLayout.Bands[0].Columns.Add();
            labelColumn.Key = "Y";
            labelColumn.Header.Caption = "Y-Coord";
            labelColumn.CellActivation = Activation.NoEdit;
            labelColumn.CellClickAction = CellClickAction.RowSelect;
            labelColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            labelColumn.CellAppearance.TextHAlign = HAlign.Left;
            labelColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //labelColumn.Width = 100;
            labelColumn.Hidden = false;   //필드 보이기

            labelColumn = griLabelList.DisplayLayout.Bands[0].Columns.Add();
            labelColumn.Key = "REMARK";
            labelColumn.Header.Caption = "Label Text";
            labelColumn.CellActivation = Activation.NoEdit;
            labelColumn.CellClickAction = CellClickAction.RowSelect;
            labelColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            labelColumn.CellAppearance.TextHAlign = HAlign.Left;
            labelColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //labelColumn.Width = 120;
            labelColumn.Hidden = false;   //필드 보이기

            //Label 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griLabelList);

            //Demand tab의 Grid
            UltraGridColumn demandColumn;

            demandColumn = griDemandList.DisplayLayout.Bands[0].Columns.Add();
            demandColumn.Key = "ID";
            demandColumn.Header.Caption = "Junction";
            demandColumn.CellActivation = Activation.NoEdit;
            demandColumn.CellClickAction = CellClickAction.RowSelect;
            demandColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            demandColumn.CellAppearance.TextHAlign = HAlign.Left;
            demandColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //demandColumn.Width = 180;
            demandColumn.Hidden = false;   //필드 보이기

            demandColumn = griDemandList.DisplayLayout.Bands[0].Columns.Add();
            demandColumn.Key = "DEMAND";
            demandColumn.Header.Caption = "Demand";
            demandColumn.CellActivation = Activation.NoEdit;
            demandColumn.CellClickAction = CellClickAction.RowSelect;
            demandColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            demandColumn.CellAppearance.TextHAlign = HAlign.Right;
            demandColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //demandColumn.Width = 100;
            demandColumn.Hidden = false;   //필드 보이기

            demandColumn = griDemandList.DisplayLayout.Bands[0].Columns.Add();
            demandColumn.Key = "PATTERN_ID";
            demandColumn.Header.Caption = "Pattern";
            demandColumn.CellActivation = Activation.NoEdit;
            demandColumn.CellClickAction = CellClickAction.RowSelect;
            demandColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            demandColumn.CellAppearance.TextHAlign = HAlign.Left;
            demandColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //demandColumn.Width = 120;
            demandColumn.Hidden = false;   //필드 보이기

            demandColumn = griDemandList.DisplayLayout.Bands[0].Columns.Add();
            demandColumn.Key = "CATEGORY";
            demandColumn.Header.Caption = "Category";
            demandColumn.CellActivation = Activation.NoEdit;
            demandColumn.CellClickAction = CellClickAction.RowSelect;
            demandColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            demandColumn.CellAppearance.TextHAlign = HAlign.Left;
            demandColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //demandColumn.Width = 120;
            demandColumn.Hidden = false;   //필드 보이기


            //Demand 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griDemandList);

            //Mixing tab의 Grid
            UltraGridColumn mixingColumn;

            mixingColumn = griMixingList.DisplayLayout.Bands[0].Columns.Add();
            mixingColumn.Key = "ID";
            mixingColumn.Header.Caption = "Tank";
            mixingColumn.CellActivation = Activation.NoEdit;
            mixingColumn.CellClickAction = CellClickAction.RowSelect;
            mixingColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            mixingColumn.CellAppearance.TextHAlign = HAlign.Left;
            mixingColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //mixingColumn.Width = 180;
            mixingColumn.Hidden = false;   //필드 보이기

            mixingColumn = griMixingList.DisplayLayout.Bands[0].Columns.Add();
            mixingColumn.Key = "MODEL";
            mixingColumn.Header.Caption = "Model";
            mixingColumn.CellActivation = Activation.NoEdit;
            mixingColumn.CellClickAction = CellClickAction.RowSelect;
            mixingColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            mixingColumn.CellAppearance.TextHAlign = HAlign.Left;
            mixingColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //mixingColumn.Width = 120;
            mixingColumn.Hidden = false;   //필드 보이기

            mixingColumn = griMixingList.DisplayLayout.Bands[0].Columns.Add();
            mixingColumn.Key = "FLACTION";
            mixingColumn.Header.Caption = "Flaction";
            mixingColumn.CellActivation = Activation.NoEdit;
            mixingColumn.CellClickAction = CellClickAction.RowSelect;
            mixingColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            mixingColumn.CellAppearance.TextHAlign = HAlign.Right;
            mixingColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //mixingColumn.Width = 100;
            mixingColumn.Hidden = false;   //필드 보이기

            //Mixing 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griMixingList);

            //Pipe tab의 Grid
            UltraGridColumn pipeColumn;

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "ID";
            pipeColumn.Header.Caption = "ID";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 180;
            pipeColumn.Hidden = false;   //필드 보이기

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "NODE1";
            pipeColumn.Header.Caption = "Node1";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 180;
            pipeColumn.Hidden = false;   //필드 보이기

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "NODE2";
            pipeColumn.Header.Caption = "Node2";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 180;
            pipeColumn.Hidden = false;   //필드 보이기

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "LENGTH";
            pipeColumn.Header.Caption = "Length";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Right;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 100;
            pipeColumn.Hidden = false;   //필드 보이기

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "DIAM";
            pipeColumn.Header.Caption = "Diameter";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Right;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 100;
            pipeColumn.Hidden = false;   //필드 보이기

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "LEAKAGE_COEFFICIENT";
            pipeColumn.Header.Caption = "Leakage";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Right;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 100;
            pipeColumn.Hidden = false;   //필드 보이기

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "ROUGHNESS";
            pipeColumn.Header.Caption = "Roughness";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Right;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 100;
            pipeColumn.Hidden = false;   //필드 보이기

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "MLOSS";
            pipeColumn.Header.Caption = "MinorLoss";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Right;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 100;
            pipeColumn.Hidden = false;   //필드 보이기

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "STATUS";
            pipeColumn.Header.Caption = "Status";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 120;
            pipeColumn.Hidden = false;   //필드 보이기

            pipeColumn = griPipeList.DisplayLayout.Bands[0].Columns.Add();
            pipeColumn.Key = "REMARK";
            pipeColumn.Header.Caption = "비고";
            pipeColumn.CellActivation = Activation.NoEdit;
            pipeColumn.CellClickAction = CellClickAction.RowSelect;
            pipeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pipeColumn.CellAppearance.TextHAlign = HAlign.Left;
            pipeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pipeColumn.Width = 300;
            pipeColumn.Hidden = false;   //필드 보이기

            //Pipe 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griPipeList);

            //Coordinate tab의 Grid
            UltraGridColumn coordinateColumn;

            coordinateColumn = griCoordinateList.DisplayLayout.Bands[0].Columns.Add();
            coordinateColumn.Key = "ID";
            coordinateColumn.Header.Caption = "Node";
            coordinateColumn.CellActivation = Activation.NoEdit;
            coordinateColumn.CellClickAction = CellClickAction.RowSelect;
            coordinateColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            coordinateColumn.CellAppearance.TextHAlign = HAlign.Left;
            coordinateColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //coordinateColumn.Width = 180;
            coordinateColumn.Hidden = false;   //필드 보이기

            coordinateColumn = griCoordinateList.DisplayLayout.Bands[0].Columns.Add();
            coordinateColumn.Key = "X";
            coordinateColumn.Header.Caption = "X-Coord";
            coordinateColumn.CellActivation = Activation.NoEdit;
            coordinateColumn.CellClickAction = CellClickAction.RowSelect;
            coordinateColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            coordinateColumn.CellAppearance.TextHAlign = HAlign.Left;
            coordinateColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //coordinateColumn.Width = 100;
            coordinateColumn.Hidden = false;   //필드 보이기

            coordinateColumn = griCoordinateList.DisplayLayout.Bands[0].Columns.Add();
            coordinateColumn.Key = "Y";
            coordinateColumn.Header.Caption = "Y-Coord";
            coordinateColumn.CellActivation = Activation.NoEdit;
            coordinateColumn.CellClickAction = CellClickAction.RowSelect;
            coordinateColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            coordinateColumn.CellAppearance.TextHAlign = HAlign.Left;
            coordinateColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //coordinateColumn.Width = 100;
            coordinateColumn.Hidden = false;   //필드 보이기

            //Coordinate 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griCoordinateList);

            //Vertices tab의 Grid
            UltraGridColumn verticesColumn;

            verticesColumn = griVerticesList.DisplayLayout.Bands[0].Columns.Add();
            verticesColumn.Key = "ID";
            verticesColumn.Header.Caption = "Link";
            verticesColumn.CellActivation = Activation.NoEdit;
            verticesColumn.CellClickAction = CellClickAction.RowSelect;
            verticesColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            verticesColumn.CellAppearance.TextHAlign = HAlign.Left;
            verticesColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //verticesColumn.Width = 180;
            verticesColumn.Hidden = false;   //필드 보이기

            verticesColumn = griVerticesList.DisplayLayout.Bands[0].Columns.Add();
            verticesColumn.Key = "IDX";
            verticesColumn.Header.Caption = "Index";
            verticesColumn.CellActivation = Activation.NoEdit;
            verticesColumn.CellClickAction = CellClickAction.RowSelect;
            verticesColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            verticesColumn.CellAppearance.TextHAlign = HAlign.Left;
            verticesColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //verticesColumn.Width = 100;
            verticesColumn.Hidden = false;   //필드 보이기

            verticesColumn = griVerticesList.DisplayLayout.Bands[0].Columns.Add();
            verticesColumn.Key = "X";
            verticesColumn.Header.Caption = "X-Coord";
            verticesColumn.CellActivation = Activation.NoEdit;
            verticesColumn.CellClickAction = CellClickAction.RowSelect;
            verticesColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            verticesColumn.CellAppearance.TextHAlign = HAlign.Left;
            verticesColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //verticesColumn.Width = 100;
            verticesColumn.Hidden = false;   //필드 보이기

            verticesColumn = griVerticesList.DisplayLayout.Bands[0].Columns.Add();
            verticesColumn.Key = "Y";
            verticesColumn.Header.Caption = "Y-Coord";
            verticesColumn.CellActivation = Activation.NoEdit;
            verticesColumn.CellClickAction = CellClickAction.RowSelect;
            verticesColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            verticesColumn.CellAppearance.TextHAlign = HAlign.Left;
            verticesColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //verticesColumn.Width = 100;
            verticesColumn.Hidden = false;   //필드 보이기

            //Vertices 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griVerticesList);

            //Pump tab의 Grid
            UltraGridColumn pumpColumn;

            pumpColumn = griPumpList.DisplayLayout.Bands[0].Columns.Add();
            pumpColumn.Key = "ID";
            pumpColumn.Header.Caption = "ID";
            pumpColumn.CellActivation = Activation.NoEdit;
            pumpColumn.CellClickAction = CellClickAction.RowSelect;
            pumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pumpColumn.CellAppearance.TextHAlign = HAlign.Left;
            pumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pumpColumn.Width = 180;
            pumpColumn.Hidden = false;   //필드 보이기

            pumpColumn = griPumpList.DisplayLayout.Bands[0].Columns.Add();
            pumpColumn.Key = "NODE1";
            pumpColumn.Header.Caption = "Node1";
            pumpColumn.CellActivation = Activation.NoEdit;
            pumpColumn.CellClickAction = CellClickAction.RowSelect;
            pumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pumpColumn.CellAppearance.TextHAlign = HAlign.Left;
            pumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pumpColumn.Width = 180;
            pumpColumn.Hidden = false;   //필드 보이기

            pumpColumn = griPumpList.DisplayLayout.Bands[0].Columns.Add();
            pumpColumn.Key = "NODE2";
            pumpColumn.Header.Caption = "Node2";
            pumpColumn.CellActivation = Activation.NoEdit;
            pumpColumn.CellClickAction = CellClickAction.RowSelect;
            pumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pumpColumn.CellAppearance.TextHAlign = HAlign.Left;
            pumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pumpColumn.Width = 180;
            pumpColumn.Hidden = false;   //필드 보이기

            pumpColumn = griPumpList.DisplayLayout.Bands[0].Columns.Add();
            pumpColumn.Key = "PROPERTIES";
            pumpColumn.Header.Caption = "Parameters";
            pumpColumn.CellActivation = Activation.NoEdit;
            pumpColumn.CellClickAction = CellClickAction.RowSelect;
            pumpColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            pumpColumn.CellAppearance.TextHAlign = HAlign.Left;
            pumpColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //pumpColumn.Width = 300;
            pumpColumn.Hidden = false;   //필드 보이기

            //Pump 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griPumpList);

            //Valve tab의 Grid
            UltraGridColumn valveColumn;

            valveColumn = griValveList.DisplayLayout.Bands[0].Columns.Add();
            valveColumn.Key = "ID";
            valveColumn.Header.Caption = "ID";
            valveColumn.CellActivation = Activation.NoEdit;
            valveColumn.CellClickAction = CellClickAction.RowSelect;
            valveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            valveColumn.CellAppearance.TextHAlign = HAlign.Left;
            valveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //valveColumn.Width = 180;
            valveColumn.Hidden = false;   //필드 보이기

            valveColumn = griValveList.DisplayLayout.Bands[0].Columns.Add();
            valveColumn.Key = "NODE1";
            valveColumn.Header.Caption = "Node1";
            valveColumn.CellActivation = Activation.NoEdit;
            valveColumn.CellClickAction = CellClickAction.RowSelect;
            valveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            valveColumn.CellAppearance.TextHAlign = HAlign.Left;
            valveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //valveColumn.Width = 180;
            valveColumn.Hidden = false;   //필드 보이기

            valveColumn = griValveList.DisplayLayout.Bands[0].Columns.Add();
            valveColumn.Key = "NODE2";
            valveColumn.Header.Caption = "Node2";
            valveColumn.CellActivation = Activation.NoEdit;
            valveColumn.CellClickAction = CellClickAction.RowSelect;
            valveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            valveColumn.CellAppearance.TextHAlign = HAlign.Left;
            valveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //valveColumn.Width = 180;
            valveColumn.Hidden = false;   //필드 보이기

            valveColumn = griValveList.DisplayLayout.Bands[0].Columns.Add();
            valveColumn.Key = "DIAMETER";
            valveColumn.Header.Caption = "Diameter";
            valveColumn.CellActivation = Activation.NoEdit;
            valveColumn.CellClickAction = CellClickAction.RowSelect;
            valveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            valveColumn.CellAppearance.TextHAlign = HAlign.Right;
            valveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //valveColumn.Width = 100;
            valveColumn.Hidden = false;   //필드 보이기

            valveColumn = griValveList.DisplayLayout.Bands[0].Columns.Add();
            valveColumn.Key = "TYPE";
            valveColumn.Header.Caption = "Type";
            valveColumn.CellActivation = Activation.NoEdit;
            valveColumn.CellClickAction = CellClickAction.RowSelect;
            valveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            valveColumn.CellAppearance.TextHAlign = HAlign.Left;
            valveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //valveColumn.Width = 120;
            valveColumn.Hidden = false;   //필드 보이기

            valveColumn = griValveList.DisplayLayout.Bands[0].Columns.Add();
            valveColumn.Key = "SETTING";
            valveColumn.Header.Caption = "Setting";
            valveColumn.CellActivation = Activation.NoEdit;
            valveColumn.CellClickAction = CellClickAction.RowSelect;
            valveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            valveColumn.CellAppearance.TextHAlign = HAlign.Right;
            valveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //valveColumn.Width = 100;
            valveColumn.Hidden = false;   //필드 보이기

            valveColumn = griValveList.DisplayLayout.Bands[0].Columns.Add();
            valveColumn.Key = "MINORLOSS";
            valveColumn.Header.Caption = "MinorLoss";
            valveColumn.CellActivation = Activation.NoEdit;
            valveColumn.CellClickAction = CellClickAction.RowSelect;
            valveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            valveColumn.CellAppearance.TextHAlign = HAlign.Right;
            valveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //valveColumn.Width = 100;
            valveColumn.Hidden = false;   //필드 보이기

            //Valve 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griValveList);

            //Enerty tab의 Grid
            UltraGridColumn energyColumn;

            energyColumn = griEnergyList.DisplayLayout.Bands[0].Columns.Add();
            energyColumn.Key = "IDX";
            energyColumn.Header.Caption = "index";
            energyColumn.CellActivation = Activation.NoEdit;
            energyColumn.CellClickAction = CellClickAction.RowSelect;
            energyColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            energyColumn.CellAppearance.TextHAlign = HAlign.Left;
            energyColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //energyColumn.Width = 100;
            energyColumn.Hidden = false;   //필드 보이기

            energyColumn = griEnergyList.DisplayLayout.Bands[0].Columns.Add();
            energyColumn.Key = "ENERGY_STATEMENT";
            energyColumn.Header.Caption = "Statement";
            energyColumn.CellActivation = Activation.NoEdit;
            energyColumn.CellClickAction = CellClickAction.RowSelect;
            energyColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            energyColumn.CellAppearance.TextHAlign = HAlign.Left;
            energyColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //energyColumn.Width = 300;
            energyColumn.Hidden = false;   //필드 보이기

            //Energy 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griEnergyList);

            //Curve tab의 Grid
            UltraGridColumn curveColumn;

            curveColumn = griCurveList.DisplayLayout.Bands[0].Columns.Add();
            curveColumn.Key = "ID";
            curveColumn.Header.Caption = "ID";
            curveColumn.CellActivation = Activation.NoEdit;
            curveColumn.CellClickAction = CellClickAction.RowSelect;
            curveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            curveColumn.CellAppearance.TextHAlign = HAlign.Left;
            curveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //curveColumn.Width = 180;
            curveColumn.Hidden = false;   //필드 보이기

            curveColumn = griCurveList.DisplayLayout.Bands[0].Columns.Add();
            curveColumn.Key = "IDX";
            curveColumn.Header.Caption = "Index";
            curveColumn.CellActivation = Activation.NoEdit;
            curveColumn.CellClickAction = CellClickAction.RowSelect;
            curveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            curveColumn.CellAppearance.TextHAlign = HAlign.Left;
            curveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //curveColumn.Width = 100;
            curveColumn.Hidden = false;   //필드 보이기

            curveColumn = griCurveList.DisplayLayout.Bands[0].Columns.Add();
            curveColumn.Key = "X";
            curveColumn.Header.Caption = "X-Value";
            curveColumn.CellActivation = Activation.NoEdit;
            curveColumn.CellClickAction = CellClickAction.RowSelect;
            curveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            curveColumn.CellAppearance.TextHAlign = HAlign.Left;
            curveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //curveColumn.Width = 100;
            curveColumn.Hidden = false;   //필드 보이기

            curveColumn = griCurveList.DisplayLayout.Bands[0].Columns.Add();
            curveColumn.Key = "Y";
            curveColumn.Header.Caption = "Y-Value";
            curveColumn.CellActivation = Activation.NoEdit;
            curveColumn.CellClickAction = CellClickAction.RowSelect;
            curveColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            curveColumn.CellAppearance.TextHAlign = HAlign.Left;
            curveColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //curveColumn.Width = 100;
            curveColumn.Hidden = false;   //필드 보이기

            //Curve 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griCurveList);

            //Pattern tab의 Grid
            UltraGridColumn patternColumn;

            patternColumn = griPatternList.DisplayLayout.Bands[0].Columns.Add();
            patternColumn.Key = "PATTERN_ID";
            patternColumn.Header.Caption = "ID";
            patternColumn.CellActivation = Activation.NoEdit;
            patternColumn.CellClickAction = CellClickAction.RowSelect;
            patternColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            patternColumn.CellAppearance.TextHAlign = HAlign.Left;
            patternColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //patternColumn.Width = 180;
            patternColumn.Hidden = false;   //필드 보이기

            patternColumn = griPatternList.DisplayLayout.Bands[0].Columns.Add();
            patternColumn.Key = "IDX";
            patternColumn.Header.Caption = "Index";
            patternColumn.CellActivation = Activation.NoEdit;
            patternColumn.CellClickAction = CellClickAction.RowSelect;
            patternColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            patternColumn.CellAppearance.TextHAlign = HAlign.Left;
            patternColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //patternColumn.Width = 100;
            patternColumn.Hidden = false;   //필드 보이기

            patternColumn = griPatternList.DisplayLayout.Bands[0].Columns.Add();
            patternColumn.Key = "MULTIPLIER";
            patternColumn.Header.Caption = "Multipliers";
            patternColumn.CellActivation = Activation.NoEdit;
            patternColumn.CellClickAction = CellClickAction.RowSelect;
            patternColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            patternColumn.CellAppearance.TextHAlign = HAlign.Right;
            patternColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //patternColumn.Width = 100;
            patternColumn.Hidden = false;   //필드 보이기

            //Pattern 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griPatternList);

            //Status tab의 Grid
            UltraGridColumn statusColumn;

            statusColumn = griStatusList.DisplayLayout.Bands[0].Columns.Add();
            statusColumn.Key = "ID";
            statusColumn.Header.Caption = "ID";
            statusColumn.CellActivation = Activation.NoEdit;
            statusColumn.CellClickAction = CellClickAction.RowSelect;
            statusColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            statusColumn.CellAppearance.TextHAlign = HAlign.Left;
            statusColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //statusColumn.Width = 180;
            statusColumn.Hidden = false;   //필드 보이기

            statusColumn = griStatusList.DisplayLayout.Bands[0].Columns.Add();
            statusColumn.Key = "IDX";
            statusColumn.Header.Caption = "Index";
            statusColumn.CellActivation = Activation.NoEdit;
            statusColumn.CellClickAction = CellClickAction.RowSelect;
            statusColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            statusColumn.CellAppearance.TextHAlign = HAlign.Left;
            statusColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //statusColumn.Width = 100;
            statusColumn.Hidden = false;   //필드 보이기

            statusColumn = griStatusList.DisplayLayout.Bands[0].Columns.Add();
            statusColumn.Key = "STATUS_SETTING";
            statusColumn.Header.Caption = "Status/Setting";
            statusColumn.CellActivation = Activation.NoEdit;
            statusColumn.CellClickAction = CellClickAction.RowSelect;
            statusColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            statusColumn.CellAppearance.TextHAlign = HAlign.Left;
            statusColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //statusColumn.Width = 120;
            statusColumn.Hidden = false;   //필드 보이기

            //Status 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griStatusList);

            //Control tab의 Grid
            UltraGridColumn controlColumn;

            controlColumn = griControlList.DisplayLayout.Bands[0].Columns.Add();
            controlColumn.Key = "IDX";
            controlColumn.Header.Caption = "Index";
            controlColumn.CellActivation = Activation.NoEdit;
            controlColumn.CellClickAction = CellClickAction.RowSelect;
            controlColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            controlColumn.CellAppearance.TextHAlign = HAlign.Left;
            controlColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //controlColumn.Width = 100;
            controlColumn.Hidden = false;   //필드 보이기

            controlColumn = griControlList.DisplayLayout.Bands[0].Columns.Add();
            controlColumn.Key = "CONTROLS_STATEMENT";
            controlColumn.Header.Caption = "Statement";
            controlColumn.CellActivation = Activation.NoEdit;
            controlColumn.CellClickAction = CellClickAction.RowSelect;
            controlColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            controlColumn.CellAppearance.TextHAlign = HAlign.Left;
            controlColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //controlColumn.Width = 300;
            controlColumn.Hidden = false;   //필드 보이기

            //Control 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griControlList);

            //Rule tab의 Grid
            UltraGridColumn ruleColumn;

            ruleColumn = griRuleList.DisplayLayout.Bands[0].Columns.Add();
            ruleColumn.Key = "IDX";
            ruleColumn.Header.Caption = "Index";
            ruleColumn.CellActivation = Activation.NoEdit;
            ruleColumn.CellClickAction = CellClickAction.RowSelect;
            ruleColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            ruleColumn.CellAppearance.TextHAlign = HAlign.Left;
            ruleColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //ruleColumn.Width = 100;
            ruleColumn.Hidden = false;   //필드 보이기

            ruleColumn = griRuleList.DisplayLayout.Bands[0].Columns.Add();
            ruleColumn.Key = "RULES_STATEMENT";
            ruleColumn.Header.Caption = "Statement";
            ruleColumn.CellActivation = Activation.NoEdit;
            ruleColumn.CellClickAction = CellClickAction.RowSelect;
            ruleColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            ruleColumn.CellAppearance.TextHAlign = HAlign.Left;
            ruleColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //ruleColumn.Width = 300;
            ruleColumn.Hidden = false;   //필드 보이기

            //Rule 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griRuleList);

            //Reaction tab의 Grid
            UltraGridColumn reactionColumn;

            reactionColumn = griReactionList.DisplayLayout.Bands[0].Columns.Add();
            reactionColumn.Key = "IDX";
            reactionColumn.Header.Caption = "Index";
            reactionColumn.CellActivation = Activation.NoEdit;
            reactionColumn.CellClickAction = CellClickAction.RowSelect;
            reactionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            reactionColumn.CellAppearance.TextHAlign = HAlign.Left;
            reactionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //reactionColumn.Width = 100;
            reactionColumn.Hidden = false;   //필드 보이기

            reactionColumn = griReactionList.DisplayLayout.Bands[0].Columns.Add();
            reactionColumn.Key = "REACTION_STATEMENT";
            reactionColumn.Header.Caption = "Statement";
            reactionColumn.CellActivation = Activation.NoEdit;
            reactionColumn.CellClickAction = CellClickAction.RowSelect;
            reactionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            reactionColumn.CellAppearance.TextHAlign = HAlign.Left;
            reactionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //reactionColumn.Width = 300;
            reactionColumn.Hidden = false;   //필드 보이기

            //Reaction 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griReactionList);

            //Option tab의 Grid
            UltraGridColumn optionColumn;

            optionColumn = griOptionList.DisplayLayout.Bands[0].Columns.Add();
            optionColumn.Key = "IDX";
            optionColumn.Header.Caption = "Index";
            optionColumn.CellActivation = Activation.NoEdit;
            optionColumn.CellClickAction = CellClickAction.RowSelect;
            optionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            optionColumn.CellAppearance.TextHAlign = HAlign.Left;
            optionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //optionColumn.Width = 100;
            optionColumn.Hidden = false;   //필드 보이기

            reactionColumn = griOptionList.DisplayLayout.Bands[0].Columns.Add();
            reactionColumn.Key = "OPTIONS_STATEMENT";
            reactionColumn.Header.Caption = "Statement";
            reactionColumn.CellActivation = Activation.NoEdit;
            reactionColumn.CellClickAction = CellClickAction.RowSelect;
            reactionColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            reactionColumn.CellAppearance.TextHAlign = HAlign.Left;
            reactionColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //reactionColumn.Width = 300;
            reactionColumn.Hidden = false;   //필드 보이기

            //Option 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griOptionList);

            //Time tab의 Grid
            UltraGridColumn timeColumn;

            timeColumn = griTimeList.DisplayLayout.Bands[0].Columns.Add();
            timeColumn.Key = "IDX";
            timeColumn.Header.Caption = "Index";
            timeColumn.CellActivation = Activation.NoEdit;
            timeColumn.CellClickAction = CellClickAction.RowSelect;
            timeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            timeColumn.CellAppearance.TextHAlign = HAlign.Left;
            timeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //timeColumn.Width = 100;
            timeColumn.Hidden = false;   //필드 보이기

            timeColumn = griTimeList.DisplayLayout.Bands[0].Columns.Add();
            timeColumn.Key = "TIMES_STATEMENT";
            timeColumn.Header.Caption = "Statement";
            timeColumn.CellActivation = Activation.NoEdit;
            timeColumn.CellClickAction = CellClickAction.RowSelect;
            timeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            timeColumn.CellAppearance.TextHAlign = HAlign.Left;
            timeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //timeColumn.Width = 300;
            timeColumn.Hidden = false;   //필드 보이기

            //Time 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griTimeList);

            //Report tab의 Grid
            UltraGridColumn reportColumn;

            reportColumn = griRptOptionList.DisplayLayout.Bands[0].Columns.Add();
            reportColumn.Key = "IDX";
            reportColumn.Header.Caption = "Index";
            reportColumn.CellActivation = Activation.NoEdit;
            reportColumn.CellClickAction = CellClickAction.RowSelect;
            reportColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            reportColumn.CellAppearance.TextHAlign = HAlign.Left;
            reportColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //reportColumn.Width = 100;
            reportColumn.Hidden = false;   //필드 보이기

            reportColumn = griRptOptionList.DisplayLayout.Bands[0].Columns.Add();
            reportColumn.Key = "REPORT_STATEMENT";
            reportColumn.Header.Caption = "Statement";
            reportColumn.CellActivation = Activation.NoEdit;
            reportColumn.CellClickAction = CellClickAction.RowSelect;
            reportColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            reportColumn.CellAppearance.TextHAlign = HAlign.Left;
            reportColumn.CellAppearance.TextVAlign = VAlign.Middle;
            //reportColumn.Width = 300;
            reportColumn.Hidden = false;   //필드 보이기

            //Report 리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griRptOptionList);
        }

        //폼이 최초 로드될때 실행
        private void frmWHINPManage_Load(object sender, EventArgs e)
        {
            //=========================================================샘플
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       '0'으로 바꿔줘야함.
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["iNP파일관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))  
            {
                this.btnOpenWHINPRegist.Enabled = false;
                this.butSaveINPData.Enabled = false;
                this.butDeleteINPData.Enabled = false;

                //Junctions탭
                this.butSaveJunction.Enabled = false;

                //Reservoirs탭
                this.butSaveReservoir.Enabled = false;

                //Tanks탭
                this.butSaveTank.Enabled = false;

                //Quality탭
                this.butAddQuality.Enabled = false;
                this.butSaveQuality.Enabled = false;
                this.butDeleteQuality.Enabled = false;

                //Source탭
                this.butAddSource.Enabled = false;
                this.butSaveSource.Enabled = false;
                this.butDeleteSource.Enabled = false;

                //Emitters탭
                this.butAddEmitter.Enabled = false;
                this.butSaveEmitters.Enabled = false;
                this.butDeleteEmitter.Enabled = false;

                //Labels
                this.butAddLabel.Enabled = false;
                this.butSaveLabel.Enabled = false;
                this.butDeleteLabel.Enabled = false;

                //Demands
                this.butAddDemand.Enabled = false;
                this.butSaveDemand.Enabled = false;
                this.butDeleteDemand.Enabled = false;

                //Mixing
                this.butAddMixing.Enabled = false;
                this.butSaveMixing.Enabled = false;
                this.butDeleteMixing.Enabled = false;

                //Pipes
                this.butSavePipe.Enabled = false;

                //Coordinates
                this.butSaveCoordinate.Enabled = false;

                //Vertices
                this.butSaveVertices.Enabled = false;

                //Pumps
                this.butSavePumps.Enabled = false;

                //Valves
                this.butSaveValve.Enabled = false;

                //Energy 추가,저장,삭제
                this.butAddEnergy.Enabled = false;
                this.butSaveEnergy.Enabled = false;
                this.butDeleteEnergy.Enabled = false;

                //Curves
                this.butAddCurve.Enabled = false;
                this.butSaveCurve.Enabled = false;
                this.butDeleteCurve.Enabled = false;

                //Patterns
                this.butAddPattern.Enabled = false;
                this.buSavePattern.Enabled = false;
                this.butDeletePattern.Enabled = false;

                //Status
                this.butAddStatus.Enabled = false;
                this.butSaveStatus.Enabled = false;
                this.butDeleteStatus.Enabled = false;

                //Controls
                this.butAddControl.Enabled = false;
                this.butSaveControl.Enabled = false;
                this.butDeleteControl.Enabled = false;

                //Rules
                this.butAddRule.Enabled = false;
                this.butSaveRule.Enabled = false;
                this.butDeleteRule.Enabled = false;

                //Reactions
                this.butAddReaction.Enabled = false;
                this.butSaveReaction.Enabled = false;
                this.butDeleteReaction.Enabled = false;

                //Options
                this.butAddOption.Enabled = false;
                this.butSaveOption.Enabled = false;
                this.butDeleteOption.Enabled = false;

                //Times
                this.butAddTime.Enabled = false;
                this.butSaveTime.Enabled = false;
                this.butDeleteTime.Enabled = false;

                //Reports
                this.butAddReport.Enabled = false;
                this.butSaveReport.Enabled = false;
                this.butDeleteReport.Enabled = false;
            }

            //============================================================================

            //검색조건에 날짜 세팅
            //datStartDate.Value = Utils.GetCalcTime("mm", -3)["yyyymmdd"].ToString();
            //datEndDate.Value = Utils.GetTime()["yyyymmdd"].ToString();

            //조회버튼에 최초 포커스
            butSelectINPList.Focus();

            //사용구분정보 할당(검색조건)
            comUseGbn.ValueMember = "CODE";
            comUseGbn.DisplayMember = "CODE_NAME";

            //사용구분정보 할당(상세)
            comDetailUseGbn.ValueMember = "CODE";
            comDetailUseGbn.DisplayMember = "CODE_NAME";

            Hashtable conditions = new Hashtable();
            conditions.Add("PCODE", "1001");

            DataSet useGbnDataSet = cWork.GetCodeList(conditions);
            DataSet detailUseGbnDataSet = cWork.GetCodeList(conditions);

            if (useGbnDataSet != null)
            {
                comUseGbn.DataSource = useGbnDataSet.Tables["CM_CODE"];
            }

            if (detailUseGbnDataSet != null)
            {
                comDetailUseGbn.DataSource = detailUseGbnDataSet.Tables["CM_CODE"];
            }

            //블록정보 할당(검색조건)
            comLftridn.ValueMember = "LOC_CODE";
            comLftridn.DisplayMember = "LOC_NAME";
            comMftridn.ValueMember = "LOC_CODE";
            comMftridn.DisplayMember = "LOC_NAME";
            comSftridn.ValueMember = "LOC_CODE";
            comSftridn.DisplayMember = "LOC_NAME";

            //블록정보 할당(상세)
            comDetailLftridn.ValueMember = "LOC_CODE";
            comDetailLftridn.DisplayMember = "LOC_NAME";
            comDetailMftridn.ValueMember = "LOC_CODE";
            comDetailMftridn.DisplayMember = "LOC_NAME";
            comDetailSftridn.ValueMember = "LOC_CODE";
            comDetailSftridn.DisplayMember = "LOC_NAME";

            DataSet blockDataSet = cWork.GetLblockDataList(new Hashtable());
            DataSet detailBlockDataSet = cWork.GetLblockDataList(new Hashtable());

            if (blockDataSet != null)
            {
                comLftridn.DataSource = blockDataSet.Tables["CM_LOCATION"];
            }

            if(detailBlockDataSet != null)
            {
                comDetailLftridn.DataSource = detailBlockDataSet.Tables["CM_LOCATION"];
            }

            //Shape코드를 사용하는 블록정보 할당 (junction,link tab)
            comJunctionSftridn.ValueMember = "FTR_IDN";
            comJunctionSftridn.DisplayMember = "LOC_NAME";

            DataSet junctionBlockDataSet = cWork.GetShapeSblockDataList(new Hashtable());

            comJunctionSftridn.DataSource = junctionBlockDataSet.Tables["CM_LOCATION"];
        }

        #endregion

        #region 사용자 이벤트

        #region INP 마스터 이벤트

        //INP파일 등록화면 호출
        private void btnOpenWHINPRegist_Click(object sender, EventArgs e)
        {
            //if (EMFrame.statics.AppStatic.USER_RIGHT == "1")
            //{
                frmWHINPRegist inpRegistForm = new frmWHINPRegist();
                inpRegistForm.manager = this;
                inpRegistForm.ShowDialog();

                if (!string.IsNullOrEmpty(inpRegistForm.INP_NUMBER))
                {
                    string work_desc = inpRegistForm.INP_NUMBER + " " + "신규등록";
                    WaterNetCore.UserWorkHistoryLog.INSERT("실시간관망해석ToolStripMenuItem", work_desc);
                }
            //}
            //else
            //{
            //    MessageBox.Show("INP파일 등록은 관리자만 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        //조회버튼 클릭 이벤트
        private void butSelectINPList_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                SelectInpList();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        //저장버튼 클릭 이벤트
        private void butSaveINPData_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //if (EMFrame.statics.AppStatic.USER_RIGHT == "1")
                //{
                if (!"".Equals(texDetailINPNumber.Text))
                {
                    //제목 입력여부 check
                    if ("".Equals(texDetailTitle.Text))
                    {
                        MessageBox.Show("제목을 입력하십시오.");
                        texDetailTitle.Focus();

                        return;
                    }

                    if ("".Equals(comDetailUseGbn.SelectedValue))
                    {
                        MessageBox.Show("사용구분을 입력하십시오.");
                        comDetailUseGbn.Focus();

                        return;
                    }

                    DataTable changedTable = ((DataTable)griINPList.DataSource).GetChanges();
                    Hashtable conditions = new Hashtable();

                    try
                    {
                        conditions.Add("inpMasterData", changedTable);
                        work.UpdateInpMasterData(conditions);
                        MessageBox.Show("정상적으로 처리되었습니다.");

                        string work_desc = texDetailINPNumber.Text + " " + "수정";
                        WaterNetCore.UserWorkHistoryLog.UPDATE("실시간관망해석ToolStripMenuItem", work_desc);

                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show(e1.Message);
                    }
                    finally
                    {
                        SelectInpList();
                    }
                }
                //}
                //else
                //{
                //    MessageBox.Show("INP파일 저장은 관리자만 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteINPData_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //if (EMFrame.statics.AppStatic.USER_RIGHT == "1")
                //{
                try
                {
                    if (griINPList.Selected.Rows.Count != 0)
                    {
                        DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (result == DialogResult.Yes)
                        {
                            Hashtable conditions = new Hashtable();

                            conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                            work.DeleteInpMasterData(conditions);

                            MessageBox.Show("정상적으로 처리되었습니다.");

                            string work_desc = texDetailINPNumber.Text + " " + "삭제";
                            WaterNetCore.UserWorkHistoryLog.DELETE("실시간관망해석ToolStripMenuItem", work_desc);

                            SelectInpList();
                        }
                    }
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                }
                //}
                //else
                //{
                //    MessageBox.Show("INP파일 삭제는 관리자만 가능합니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //Title필드 입력 이벤트
        private void texDetailTitle_TextChanged(object sender, EventArgs e)
        {
            if (griINPList.Selected.Rows.Count != 0)
            {
                ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("TITLE", texDetailTitle.Text);
                griINPList.UpdateData();
            }
        }

        //Remark필드 입력 이벤트
        private void texDetailRemark_TextChanged(object sender, EventArgs e)
        {
            if (griINPList.Selected.Rows.Count != 0)
            {
                ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("REMARK", texDetailRemark.Text);
                griINPList.UpdateData();
            }
        }

        //사용구분 변경 이벤트
        private void comDetailUseGbn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ("WH".Equals(comDetailUseGbn.SelectedValue.ToString()) || "WH".Equals(comDetailUseGbn.SelectedValue.ToString()))
            {
                comDetailSftridn.SelectedValue = "";
                comDetailSftridn.Enabled = false;
            }
            else
            {
                comDetailSftridn.Enabled = true;
            }

            if (griINPList.Selected.Rows.Count != 0)
            {
                ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("USE_GBN", comDetailUseGbn.SelectedValue);
                ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("USE_GBN_NAME", comDetailUseGbn.Text);
                griINPList.UpdateData();
            }
        }

        //블록정보 combo event에 비동기 작업이 두개 걸려있어 폼이 로딩될때와 클릭해서 블록정보를 변경할때를
        //구분하기 위해 구분자를 둠 (이벤트 꼬임)
        bool isLClick = false;
        bool isMClick = false;

        //대블록 클릭 이벤트
        private void comDetailLftridn_Click(object sender, EventArgs e)
        {
            isLClick = true;
        }

        //중블록 클릭 이벤트
        private void comDetailMftridn_Click(object sender, EventArgs e)
        {
            isMClick = true;
        }

        //대블록 변경 이벤트
        private void comDetailLftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comDetailLftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comDetailMftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }

            if(isLClick)
            {
                if (griINPList.Selected.Rows.Count != 0)
                {
                    string selectedText = "";

                    if (!"".Equals(comDetailLftridn.SelectedValue))
                    {
                        selectedText = comDetailLftridn.Text;
                    }

                    ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("LFTRIDN", comDetailLftridn.SelectedValue);
                    ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("LFTR_NAME", selectedText);
                    griINPList.UpdateData();
                }

                isLClick = false;
            }
        }

        //중블록 변경 이벤트
        private void comDetailMftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comDetailMftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comDetailSftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }

            if (isMClick || isLClick)
            {
                if (griINPList.Selected.Rows.Count != 0)
                {
                    string selectedText = "";

                    if (!"".Equals(comDetailMftridn.SelectedValue))
                    {
                        selectedText = comDetailMftridn.Text;
                    }

                    ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("MFTRIDN", comDetailMftridn.SelectedValue);
                    ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("MFTR_NAME", selectedText);
                    griINPList.UpdateData();
                }

                isMClick = false;
            }
        }

        //소블록 변경 이벤트
        private void comDetailSftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (griINPList.Selected.Rows.Count != 0)
            {
                string selectedText = "";

                if (!"".Equals(comDetailSftridn.SelectedValue))
                {
                    selectedText = comDetailSftridn.Text;
                }

                ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("SFTRIDN", comDetailSftridn.SelectedValue);
                ((DataTable)griINPList.DataSource).Rows[griINPList.Selected.Rows[0].Index].SetField("SFTR_NAME", selectedText);
                griINPList.UpdateData();
            }
        }

        #endregion

        #region junction 이벤트

        //추가버튼 클릭 이벤트
        private void butAddJunction_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griJunctionList.DataSource).Rows.Count;
                griJunctionList.ActiveRow = null;

                DataRow row = ((DataTable)griJunctionList.DataSource).NewRow();
                ((DataTable)griJunctionList.DataSource).Rows.Add(row);

                griJunctionList.Rows[count].Selected = true;
                griJunctionList.ActiveRowScrollRegion.ScrollRowIntoView(griJunctionList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveJunction_Click(object sender, EventArgs e)
        {
            if (griJunctionList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griJunctionList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach(DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable",changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveJunctionDataList(conditions);
                    griJunctionList.DataSource = result.Tables["WH_JUNCTIONS"];

                    if (griJunctionList.Rows.Count > 0)
                    {
                        griJunctionList.Rows[0].Selected = true;
                        griJunctionList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteJunction_Click(object sender, EventArgs e)
        {
            if (griJunctionList.Selected.Rows.Count != 0)
            {
                int idx = griJunctionList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griJunctionList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griJunctionList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griJunctionList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griJunctionList.DataSource).Rows.Count;

                        griJunctionList.Rows[count - 1].Selected = true;
                        griJunctionList.ActiveRowScrollRegion.ScrollRowIntoView(griJunctionList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texJunctionId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteJunctionData(conditions);
                            griJunctionList.DataSource = dSet.Tables["WH_JUNCTIONS"];

                            if (griJunctionList.Rows.Count > 0)
                            {
                                griJunctionList.Rows[0].Selected = true;
                                griJunctionList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texReservoirId.ReadOnly = false;

                                texJunctionId.Text = "";
                                texJunctionElev.Text = "";
                                texJunctionDemand.Text = "";
                                texJunctionPatternID.Text = "";
                                comJunctionSftridn.SelectedValue = "";
                                texJunctionRemark.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }   
        }

        //ID필드 입력 이벤트
        private void texJunctionId_TextChanged(object sender, EventArgs e)
        {
            if(griJunctionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griJunctionList.DataSource).Rows[griJunctionList.Selected.Rows[0].Index].SetField("ID", texJunctionId.Text);
                griJunctionList.UpdateData();
            }
        }

        //Elv필드 입력 이벤트
        private void texJunctionElev_TextChanged(object sender, EventArgs e)
        {
            if (griJunctionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griJunctionList.DataSource).Rows[griJunctionList.Selected.Rows[0].Index].SetField("ELEV", texJunctionElev.Text);
                griJunctionList.UpdateData();
            }
        }

        //Demand필드 입력 이벤트
        private void texJunctionDemand_TextChanged(object sender, EventArgs e)
        {
            if (griJunctionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griJunctionList.DataSource).Rows[griJunctionList.Selected.Rows[0].Index].SetField("DEMAND", texJunctionDemand.Text);
                griJunctionList.UpdateData();
            }
        }

        //Pattern필드 입력 이벤트
        private void texJunctionPatternID_TextChanged(object sender, EventArgs e)
        {
            if (griJunctionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griJunctionList.DataSource).Rows[griJunctionList.Selected.Rows[0].Index].SetField("PATTERN_ID", texJunctionPatternID.Text);
                griJunctionList.UpdateData();
            }
        }

        //소블록 변경 이벤트
        private void comJunctionSftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (griJunctionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griJunctionList.DataSource).Rows[griJunctionList.Selected.Rows[0].Index].SetField("SFTRIDN", comJunctionSftridn.SelectedValue);
                ((DataTable)griJunctionList.DataSource).Rows[griJunctionList.Selected.Rows[0].Index].SetField("SFTR_NAME", comJunctionSftridn.Text);
                griJunctionList.UpdateData();
            }
        }

        //Remark필드 입력 이벤트
        private void texJunctionRemark_TextChanged(object sender, EventArgs e)
        {
            if (griJunctionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griJunctionList.DataSource).Rows[griJunctionList.Selected.Rows[0].Index].SetField("REMARK", texJunctionRemark.Text);
                griJunctionList.UpdateData();
            }
        }

        #endregion

        #region Reservoir 이벤트

        //추가버튼 클릭 이벤트
        private void butAddReservoir_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griReservoirList.DataSource).Rows.Count;
                griReservoirList.ActiveRow = null;

                DataRow row = ((DataTable)griReservoirList.DataSource).NewRow();
                ((DataTable)griReservoirList.DataSource).Rows.Add(row);

                griReservoirList.Rows[count].Selected = true;
                griReservoirList.ActiveRowScrollRegion.ScrollRowIntoView(griReservoirList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveReservoir_Click(object sender, EventArgs e)
        {
            if (griReservoirList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griReservoirList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveReservoirDataList(conditions);
                    griReservoirList.DataSource = result.Tables["WH_RESERVOIRS"];

                    if (griReservoirList.Rows.Count > 0)
                    {
                        griReservoirList.Rows[0].Selected = true;
                        griReservoirList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteReservoir_Click(object sender, EventArgs e)
        {
            if (griReservoirList.Selected.Rows.Count != 0)
            {
                int idx = griReservoirList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griReservoirList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griReservoirList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griReservoirList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griReservoirList.DataSource).Rows.Count;

                        griReservoirList.Rows[count-1].Selected = true;
                        griReservoirList.ActiveRowScrollRegion.ScrollRowIntoView(griReservoirList.Rows[count-1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texReservoirId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteReservoirData(conditions);
                            griReservoirList.DataSource = dSet.Tables["WH_RESERVOIRS"];

                            if (griReservoirList.Rows.Count > 0)
                            {
                                griReservoirList.Rows[0].Selected = true;
                                griReservoirList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texReservoirId.ReadOnly = false;

                                texReservoirId.Text = "";
                                texReservoirHead.Text = "";
                                texReservoirPatternId.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }   
        }

        //ID필드 입력 이벤트
        private void texReservoirId_TextChanged(object sender, EventArgs e)
        {
            if (griReservoirList.Selected.Rows.Count != 0)
            {
                ((DataTable)griReservoirList.DataSource).Rows[griReservoirList.Selected.Rows[0].Index].SetField("ID", texReservoirId.Text);
                griReservoirList.UpdateData();
            }
        }

        //HEAD필드 입력 이벤트
        private void texReservoirHead_TextChanged(object sender, EventArgs e)
        {
            if (griReservoirList.Selected.Rows.Count != 0)
            {
                ((DataTable)griReservoirList.DataSource).Rows[griReservoirList.Selected.Rows[0].Index].SetField("HEAD", texReservoirHead.Text);
                griReservoirList.UpdateData();
            }
        }

        //PATTERN필드 입력 이벤트
        private void texReservoirPatternId_TextChanged(object sender, EventArgs e)
        {
            if (griReservoirList.Selected.Rows.Count != 0)
            {
                ((DataTable)griReservoirList.DataSource).Rows[griReservoirList.Selected.Rows[0].Index].SetField("PATTERN_ID", texReservoirPatternId.Text);
                griReservoirList.UpdateData();
            }
        }

        #endregion

        #region Tank 이벤트

        //추가버튼 클릭 이벤트
        private void butAddTank_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griTankList.DataSource).Rows.Count;
                griTankList.ActiveRow = null;

                DataRow row = ((DataTable)griTankList.DataSource).NewRow();
                ((DataTable)griTankList.DataSource).Rows.Add(row);

                griTankList.Rows[count].Selected = true;
                griTankList.ActiveRowScrollRegion.ScrollRowIntoView(griTankList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveTank_Click(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griTankList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveTankDataList(conditions);
                    griTankList.DataSource = result.Tables["WH_TANK"];

                    if (griTankList.Rows.Count > 0)
                    {
                        griTankList.Rows[0].Selected = true;
                        griTankList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteTank_Click(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                int idx = griTankList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griTankList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griTankList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griTankList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griTankList.DataSource).Rows.Count;

                        griTankList.Rows[count - 1].Selected = true;
                        griTankList.ActiveRowScrollRegion.ScrollRowIntoView(griTankList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texTankId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteTankData(conditions);
                            griTankList.DataSource = dSet.Tables["WH_TANK"];

                            if (griTankList.Rows.Count > 0)
                            {
                                griTankList.Rows[0].Selected = true;
                                griTankList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texTankId.ReadOnly = false;

                                texTankId.Text = "";
                                texTankElev.Text = "";
                                texTankInitlvl.Text = "";
                                texTankMinLvl.Text = "";
                                texTankMaxlvl.Text = "";
                                texTankDiam.Text = "";
                                texTankMinvol.Text = "";
                                texTankVolCurveId.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //ID필드 입력 이벤트
        private void texTankId_TextChanged(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTankList.DataSource).Rows[griTankList.Selected.Rows[0].Index].SetField("ID", texTankId.Text);
                griTankList.UpdateData();
            }
        }

        //Elev필드 입력 이벤트
        private void texTankElev_TextChanged(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTankList.DataSource).Rows[griTankList.Selected.Rows[0].Index].SetField("ELEV", texTankElev.Text);
                griTankList.UpdateData();
            }
        }

        //Initlvl필드 입력 이벤트
        private void texTankInitlvl_TextChanged(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTankList.DataSource).Rows[griTankList.Selected.Rows[0].Index].SetField("INITLVL", texTankInitlvl.Text);
                griTankList.UpdateData();
            }
        }

        //MinLvl필드 입력 이벤트
        private void texTankMinLvl_TextChanged(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTankList.DataSource).Rows[griTankList.Selected.Rows[0].Index].SetField("MINLVL", texTankMinLvl.Text);
                griTankList.UpdateData();
            }
        }

        //Maxlvl필드 입력 이벤트
        private void texTankMaxlvl_TextChanged(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTankList.DataSource).Rows[griTankList.Selected.Rows[0].Index].SetField("MAXLVL", texTankMaxlvl.Text);
                griTankList.UpdateData();
            }
        }

        //Diam필드 입력 이벤트
        private void texTankDiam_TextChanged(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTankList.DataSource).Rows[griTankList.Selected.Rows[0].Index].SetField("DIAM", texTankDiam.Text);
                griTankList.UpdateData();
            }
        }

        //Minvol필드 입력 이벤트
        private void texTankMinvol_TextChanged(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTankList.DataSource).Rows[griTankList.Selected.Rows[0].Index].SetField("MINVOL", texTankMinvol.Text);
                griTankList.UpdateData();
            }
        }

        //VolCurveId필드 입력 이벤트
        private void texTankVolCurveId_TextChanged(object sender, EventArgs e)
        {
            if (griTankList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTankList.DataSource).Rows[griTankList.Selected.Rows[0].Index].SetField("VOLCURVE_ID", texTankVolCurveId.Text);
                griTankList.UpdateData();
            }
        }

        #endregion

        #region Quality 이벤트

        //추가버튼 클릭 이벤트
        private void butAddQuality_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griQualityList.DataSource).Rows.Count;
                griQualityList.ActiveRow = null;

                DataRow row = ((DataTable)griQualityList.DataSource).NewRow();
                ((DataTable)griQualityList.DataSource).Rows.Add(row);

                griQualityList.Rows[count].Selected = true;
                griQualityList.ActiveRowScrollRegion.ScrollRowIntoView(griQualityList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveQuality_Click(object sender, EventArgs e)
        {
            if (griQualityList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griQualityList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveQualityDataList(conditions);
                    griQualityList.DataSource = result.Tables["WH_QUALITY"];

                    if (griQualityList.Rows.Count > 0)
                    {
                        griQualityList.Rows[0].Selected = true;
                        griQualityList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteQuality_Click(object sender, EventArgs e)
        {
            if (griQualityList.Selected.Rows.Count != 0)
            {
                int idx = griQualityList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griQualityList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griQualityList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griQualityList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griQualityList.DataSource).Rows.Count;

                        griQualityList.Rows[count - 1].Selected = true;
                        griQualityList.ActiveRowScrollRegion.ScrollRowIntoView(griQualityList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texQualityId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteQualityData(conditions);
                            griQualityList.DataSource = dSet.Tables["WH_QUALITY"];

                            if (griQualityList.Rows.Count > 0)
                            {
                                griQualityList.Rows[0].Selected = true;
                                griQualityList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texQualityId.ReadOnly = false;

                                texQualityId.Text = "";
                                texQualityInitqual.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //Id필드 입력 이벤트
        private void texQualityId_TextChanged(object sender, EventArgs e)
        {
            if (griQualityList.Selected.Rows.Count != 0)
            {
                ((DataTable)griQualityList.DataSource).Rows[griQualityList.Selected.Rows[0].Index].SetField("ID", texQualityId.Text);
                griQualityList.UpdateData();
            }
        }

        //Initqual필드 입력 이벤트
        private void texQualityInitqual_TextChanged(object sender, EventArgs e)
        {
            if (griQualityList.Selected.Rows.Count != 0)
            {
                ((DataTable)griQualityList.DataSource).Rows[griQualityList.Selected.Rows[0].Index].SetField("INITQUAL", texQualityInitqual.Text);
                griQualityList.UpdateData();
            }
        }

        #endregion

        #region Source 이벤트

        //추가버튼 클릭 이벤트
        private void butAddSource_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griSourceList.DataSource).Rows.Count;
                griSourceList.ActiveRow = null;

                DataRow row = ((DataTable)griSourceList.DataSource).NewRow();
                ((DataTable)griSourceList.DataSource).Rows.Add(row);

                griSourceList.Rows[count].Selected = true;
                griSourceList.ActiveRowScrollRegion.ScrollRowIntoView(griSourceList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveSource_Click(object sender, EventArgs e)
        {
            if (griSourceList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griSourceList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["TYPE"]))
                    {
                        MessageBox.Show("TYPE이 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveSourceDataList(conditions);
                    griSourceList.DataSource = result.Tables["WH_SOURCE"];

                    if (griSourceList.Rows.Count > 0)
                    {
                        griSourceList.Rows[0].Selected = true;
                        griSourceList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteSource_Click(object sender, EventArgs e)
        {
            if (griSourceList.Selected.Rows.Count != 0)
            {
                int idx = griSourceList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griSourceList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griSourceList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griSourceList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griSourceList.DataSource).Rows.Count;

                        griSourceList.Rows[count - 1].Selected = true;
                        griSourceList.ActiveRowScrollRegion.ScrollRowIntoView(griSourceList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texSourceId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);
                        conditions.Add("TYPE", texSourceType.Text);

                        try
                        {
                            DataSet dSet = work.DeleteSourceData(conditions);
                            griSourceList.DataSource = dSet.Tables["WH_SOURCE"];

                            if (griSourceList.Rows.Count > 0)
                            {
                                griSourceList.Rows[0].Selected = true;
                                griSourceList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texSourceId.ReadOnly = false;
                                texSourceType.ReadOnly = false;

                                texSourceId.Text = "";
                                texSourceType.Text = "";
                                texSourceStrength.Text = "";
                                texSourcePatternId.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //Id필드 입력 이벤트
        private void texSourceId_TextChanged(object sender, EventArgs e)
        {
            if (griSourceList.Selected.Rows.Count != 0)
            {
                ((DataTable)griSourceList.DataSource).Rows[griSourceList.Selected.Rows[0].Index].SetField("ID", texSourceId.Text);
                griSourceList.UpdateData();
            }
        }

        //Type필드 입력 이벤트
        private void texSourceType_TextChanged(object sender, EventArgs e)
        {
            if (griSourceList.Selected.Rows.Count != 0)
            {
                ((DataTable)griSourceList.DataSource).Rows[griSourceList.Selected.Rows[0].Index].SetField("TYPE", texSourceType.Text);
                griSourceList.UpdateData();
            }
        }

        //Strength필드 입력 이벤트
        private void texSourceStrength_TextChanged(object sender, EventArgs e)
        {
            if (griSourceList.Selected.Rows.Count != 0)
            {
                ((DataTable)griSourceList.DataSource).Rows[griSourceList.Selected.Rows[0].Index].SetField("STRENGTH", texSourceStrength.Text);
                griSourceList.UpdateData();
            }
        }

        //PatternId필드 입력 이벤트
        private void texSourcePatternId_TextChanged(object sender, EventArgs e)
        {
            if (griSourceList.Selected.Rows.Count != 0)
            {
                ((DataTable)griSourceList.DataSource).Rows[griSourceList.Selected.Rows[0].Index].SetField("PATTERN_ID", texSourcePatternId.Text);
                griSourceList.UpdateData();
            }
        }

        #endregion

        #region Emitter 이벤트

        //추가버튼 클릭 이벤트
        private void butAddEmitter_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griEmitterList.DataSource).Rows.Count;
                griEmitterList.ActiveRow = null;

                DataRow row = ((DataTable)griEmitterList.DataSource).NewRow();
                ((DataTable)griEmitterList.DataSource).Rows.Add(row);

                griEmitterList.Rows[count].Selected = true;
                griEmitterList.ActiveRowScrollRegion.ScrollRowIntoView(griEmitterList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveEmitters_Click(object sender, EventArgs e)
        {
            if (griEmitterList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griEmitterList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["FLOW_COFFICIENT"]))
                    {
                        MessageBox.Show("FLOW COFFICIENT가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveEmitterDataList(conditions);
                    griEmitterList.DataSource = result.Tables["WH_EMITTERS"];

                    if (griEmitterList.Rows.Count > 0)
                    {
                        griEmitterList.Rows[0].Selected = true;
                        griEmitterList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteEmitter_Click(object sender, EventArgs e)
        {
            if (griEmitterList.Selected.Rows.Count != 0)
            {
                int idx = griEmitterList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griEmitterList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griEmitterList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griEmitterList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griEmitterList.DataSource).Rows.Count;

                        griEmitterList.Rows[count - 1].Selected = true;
                        griEmitterList.ActiveRowScrollRegion.ScrollRowIntoView(griEmitterList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texEmitterId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteEmitterData(conditions);
                            griEmitterList.DataSource = dSet.Tables["WH_EMITTERS"];

                            if (griEmitterList.Rows.Count > 0)
                            {
                                griEmitterList.Rows[0].Selected = true;
                                griEmitterList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texEmitterId.ReadOnly = false;
                                texEmitterId.Text = "";
                                texEmitterFlowCofficient.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //Junction필드 입력 이벤트
        private void texEmitterId_TextChanged(object sender, EventArgs e)
        {
            if (griEmitterList.Selected.Rows.Count != 0)
            {
                ((DataTable)griEmitterList.DataSource).Rows[griEmitterList.Selected.Rows[0].Index].SetField("ID", texEmitterId.Text);
                griEmitterList.UpdateData();
            }
        }

        //Coefficient필드 입력 이벤트
        private void texEmitterFlowCofficient_TextChanged(object sender, EventArgs e)
        {
            if (griEmitterList.Selected.Rows.Count != 0)
            {
                ((DataTable)griEmitterList.DataSource).Rows[griEmitterList.Selected.Rows[0].Index].SetField("FLOW_COFFICIENT", texEmitterFlowCofficient.Text);
                griEmitterList.UpdateData();
            }
        }

        #endregion

        #region Label 이벤트

        //추가버튼 클릭 이벤트
        private void butAddLabel_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griLabelList.DataSource).Rows.Count;
                griLabelList.ActiveRow = null;

                DataRow row = ((DataTable)griLabelList.DataSource).NewRow();
                ((DataTable)griLabelList.DataSource).Rows.Add(row);

                griLabelList.Rows[count].Selected = true;
                griLabelList.ActiveRowScrollRegion.ScrollRowIntoView(griLabelList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveLabel_Click(object sender, EventArgs e)
        {
            if (griLabelList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griLabelList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["X"]))
                    {
                        MessageBox.Show("X좌표가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["Y"]))
                    {
                        MessageBox.Show("Y좌표가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["REMARK"]))
                    {
                        MessageBox.Show("REMARK가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveLabelDataList(conditions);
                    griLabelList.DataSource = result.Tables["WH_LABELS"];

                    if (griLabelList.Rows.Count > 0)
                    {
                        griLabelList.Rows[0].Selected = true;
                        griLabelList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteLabel_Click(object sender, EventArgs e)
        {
            if (griLabelList.Selected.Rows.Count != 0)
            {
                int idx = griLabelList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griLabelList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griLabelList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griLabelList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griLabelList.DataSource).Rows.Count;

                        griLabelList.Rows[count - 1].Selected = true;
                        griLabelList.ActiveRowScrollRegion.ScrollRowIntoView(griLabelList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("IDX", texLabelIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteLabelData(conditions);
                            griLabelList.DataSource = dSet.Tables["WH_LABELS"];

                            if (griLabelList.Rows.Count > 0)
                            {
                                griLabelList.Rows[0].Selected = true;
                                griLabelList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texLabelIdx.ReadOnly = false;

                                texLabelIdx.Text = "";
                                texLabelX.Text = "";
                                texLabelY.Text = "";
                                texLabelRemark.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //IDX필드 입력 이벤트
        private void texLabelIdx_TextChanged(object sender, EventArgs e)
        {
            if (griLabelList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLabelList.DataSource).Rows[griLabelList.Selected.Rows[0].Index].SetField("IDX", texLabelIdx.Text);
                griLabelList.UpdateData();
            }
        }

        //X-Coord필드 입력 이벤트
        private void texLabelX_TextChanged(object sender, EventArgs e)
        {
            if (griLabelList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLabelList.DataSource).Rows[griLabelList.Selected.Rows[0].Index].SetField("X", texLabelX.Text);
                griLabelList.UpdateData();
            }
        }

        //Y-Coord필드 입력 이벤트
        private void texLabelY_TextChanged(object sender, EventArgs e)
        {
            if (griLabelList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLabelList.DataSource).Rows[griLabelList.Selected.Rows[0].Index].SetField("Y", texLabelY.Text);
                griLabelList.UpdateData();
            }
        }

        //Remark필드 입력 이벤트
        private void texLabelRemark_TextChanged(object sender, EventArgs e)
        {
            if (griLabelList.Selected.Rows.Count != 0)
            {
                ((DataTable)griLabelList.DataSource).Rows[griLabelList.Selected.Rows[0].Index].SetField("REMARK", texLabelRemark.Text);
                griLabelList.UpdateData();
            }
        }

        #endregion

        #region Demand 이벤트

        //추가버튼 클릭 이벤트
        private void butAddDemand_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griDemandList.DataSource).Rows.Count;
                griDemandList.ActiveRow = null;

                DataRow row = ((DataTable)griDemandList.DataSource).NewRow();
                ((DataTable)griDemandList.DataSource).Rows.Add(row);

                griDemandList.Rows[count].Selected = true;
                griDemandList.ActiveRowScrollRegion.ScrollRowIntoView(griDemandList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveDemand_Click(object sender, EventArgs e)
        {
            if (griDemandList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griDemandList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("Junction ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["CATEGORY"]))
                    {
                        MessageBox.Show("CATEGORY가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["DEMAND"]))
                    {
                        MessageBox.Show("DEMAND가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveDemandDataList(conditions);
                    griDemandList.DataSource = result.Tables["WH_DEMANDS"];

                    if (griDemandList.Rows.Count > 0)
                    {
                        griDemandList.Rows[0].Selected = true;
                        griDemandList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteDemand_Click(object sender, EventArgs e)
        {
            if (griDemandList.Selected.Rows.Count != 0)
            {
                int idx = griDemandList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griDemandList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griDemandList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griDemandList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griDemandList.DataSource).Rows.Count;

                        griDemandList.Rows[count - 1].Selected = true;
                        griDemandList.ActiveRowScrollRegion.ScrollRowIntoView(griDemandList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texDemandId.Text);
                        conditions.Add("CATEGORY", texDemandCategory.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteDemandData(conditions);
                            griDemandList.DataSource = dSet.Tables["WH_DEMANDS"];

                            if (griDemandList.Rows.Count > 0)
                            {
                                griDemandList.Rows[0].Selected = true;
                                griDemandList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texDemandId.ReadOnly = false;
                                texDemandCategory.ReadOnly = false;

                                texDemandId.Text        = "";
                                texDemandCategory.Text  = "";
                                texDemandDemand.Text    = "";
                                texDemandPatternId.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //Junction ID필드 입력 이벤트
        private void texDemandId_TextChanged(object sender, EventArgs e)
        {
            if (griDemandList.Selected.Rows.Count != 0)
            {
                ((DataTable)griDemandList.DataSource).Rows[griDemandList.Selected.Rows[0].Index].SetField("ID", texDemandId.Text);
                griDemandList.UpdateData();
            }
        }

        //Demand 필드 입력 이벤트
        private void texDemandDemand_TextChanged(object sender, EventArgs e)
        {
            if (griDemandList.Selected.Rows.Count != 0)
            {
                ((DataTable)griDemandList.DataSource).Rows[griDemandList.Selected.Rows[0].Index].SetField("DEMAND", texDemandDemand.Text);
                griDemandList.UpdateData();
            }
        }

        //Pattern ID필드 입력 이벤트
        private void texDemandPatternId_TextChanged(object sender, EventArgs e)
        {
            if (griDemandList.Selected.Rows.Count != 0)
            {
                ((DataTable)griDemandList.DataSource).Rows[griDemandList.Selected.Rows[0].Index].SetField("PATTERN_ID", texDemandPatternId.Text);
                griDemandList.UpdateData();
            }
        }

        //Category필드 입력 이벤트
        private void texDemandCategory_TextChanged(object sender, EventArgs e)
        {
            if (griDemandList.Selected.Rows.Count != 0)
            {
                ((DataTable)griDemandList.DataSource).Rows[griDemandList.Selected.Rows[0].Index].SetField("CATEGORY", texDemandCategory.Text);
                griDemandList.UpdateData();
            }
        }

        #endregion

        #region Mixing 이벤트

        //추가버튼 클릭 이벤트
        private void butAddMixing_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griMixingList.DataSource).Rows.Count;
                griMixingList.ActiveRow = null;

                DataRow row = ((DataTable)griMixingList.DataSource).NewRow();
                ((DataTable)griMixingList.DataSource).Rows.Add(row);

                griMixingList.Rows[count].Selected = true;
                griMixingList.ActiveRowScrollRegion.ScrollRowIntoView(griMixingList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveMixing_Click(object sender, EventArgs e)
        {
            if (griMixingList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griMixingList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("Tank ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveMixingDataList(conditions);
                    griMixingList.DataSource = result.Tables["WH_MIXING"];

                    if (griMixingList.Rows.Count > 0)
                    {
                        griMixingList.Rows[0].Selected = true;
                        griMixingList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteMixing_Click(object sender, EventArgs e)
        {
            if (griMixingList.Selected.Rows.Count != 0)
            {
                int idx = griMixingList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griMixingList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griMixingList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griMixingList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griMixingList.DataSource).Rows.Count;

                        griMixingList.Rows[count - 1].Selected = true;
                        griMixingList.ActiveRowScrollRegion.ScrollRowIntoView(griMixingList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texMixingId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteMixingData(conditions);
                            griMixingList.DataSource = dSet.Tables["WH_MIXING"];

                            if (griMixingList.Rows.Count > 0)
                            {
                                griMixingList.Rows[0].Selected = true;
                                griMixingList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texMixingId.ReadOnly = false;

                                texMixingId.Text = "";
                                texMixingModel.Text = "";
                                texMixingFlaction.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //Tank ID 필드 입력 이벤트
        private void texMixingId_TextChanged(object sender, EventArgs e)
        {
            if (griMixingList.Selected.Rows.Count != 0)
            {
                ((DataTable)griMixingList.DataSource).Rows[griMixingList.Selected.Rows[0].Index].SetField("ID", texMixingId.Text);
                griMixingList.UpdateData();
            }
        }

        //Model 필드 입력 이벤트
        private void texMixingModel_TextChanged(object sender, EventArgs e)
        {
            if (griMixingList.Selected.Rows.Count != 0)
            {
                ((DataTable)griMixingList.DataSource).Rows[griMixingList.Selected.Rows[0].Index].SetField("MODEL", texMixingModel.Text);
                griMixingList.UpdateData();
            }
        }

        //Flaction 필드 입력 이벤트
        private void texMixingFlaction_TextChanged(object sender, EventArgs e)
        {
            if (griMixingList.Selected.Rows.Count != 0)
            {
                ((DataTable)griMixingList.DataSource).Rows[griMixingList.Selected.Rows[0].Index].SetField("FLACTION", texMixingFlaction.Text);
                griMixingList.UpdateData();
            }
        }

        #endregion

        #region Pipe 이벤트

        //추가버튼 클릭 이벤트
        private void butAddPipe_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griPipeList.DataSource).Rows.Count;
                griPipeList.ActiveRow = null;

                DataRow row = ((DataTable)griPipeList.DataSource).NewRow();
                ((DataTable)griPipeList.DataSource).Rows.Add(row);

                griPipeList.Rows[count].Selected = true;
                griPipeList.ActiveRowScrollRegion.ScrollRowIntoView(griPipeList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSavePipe_Click(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griPipeList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("Pipe ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SavePipeDataList(conditions);
                    griPipeList.DataSource = result.Tables["WH_PIPES"];

                    if (griPipeList.Rows.Count > 0)
                    {
                        griPipeList.Rows[0].Selected = true;
                        griPipeList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeletePipe_Click(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                int idx = griPipeList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griPipeList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griPipeList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griPipeList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griPipeList.DataSource).Rows.Count;

                        griPipeList.Rows[count - 1].Selected = true;
                        griPipeList.ActiveRowScrollRegion.ScrollRowIntoView(griPipeList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texPipeId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeletePipeData(conditions);
                            griPipeList.DataSource = dSet.Tables["WH_PIPES"];

                            if (griPipeList.Rows.Count > 0)
                            {
                                griPipeList.Rows[0].Selected = true;
                                griPipeList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texPipeId.ReadOnly = false;

                                texPipeId.Text = "";
                                texPipeNode1.Text = "";
                                texPipeNode2.Text = "";
                                texPipeLength.Text = "";
                                texPipeDiam.Text = "";
                                texPipeLeakageCoefficient.Text = "";
                                texPipeRoughness.Text = "";
                                texPipeMloss.Text = "";
                                texPipeStatus.Text = "";
                                texPipeRemark.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //ID 필드 입력 이벤트
        private void texPipeId_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("ID", texPipeId.Text);
                griPipeList.UpdateData();
            }
        }

        //Node1 필드 입력 이벤트
        private void texPipeNode1_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("NODE1", texPipeNode1.Text);
                griPipeList.UpdateData();
            }
        }

        //Node2 필드 입력 이벤트
        private void texPipeNode2_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("NODE2", texPipeNode2.Text);
                griPipeList.UpdateData();
            }
        }

        //Length 필드 입력 이벤트
        private void texPipeLength_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("LENGTH", texPipeLength.Text);
                griPipeList.UpdateData();
            }
        }

        //Diameter 필드 입력 이벤트
        private void texPipeDiam_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("DIAM", texPipeDiam.Text);
                griPipeList.UpdateData();
            }
        }

        //LeakageCoefficient 필드 입력 이벤트
        private void texPipeLeakageCoefficient_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("LEAKAGE_COEFFICIENT", texPipeLeakageCoefficient.Text);
                griPipeList.UpdateData();
            }
        }

        //Roughness 필드 입력 이벤트
        private void texPipeRoughness_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("ROUGHNESS", texPipeRoughness.Text);
                griPipeList.UpdateData();
            }
        }

        //Minor Loss 필드 입력 이벤트
        private void texPipeMloss_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("MLOSS", texPipeMloss.Text);
                griPipeList.UpdateData();
            }
        }

        //Status 필드 입력 이벤트
        private void texPipeStatus_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("STATUS", texPipeStatus.Text);
                griPipeList.UpdateData();
            }
        }

        //Remark 필드 입력 이벤트
        private void texPipeRemark_TextChanged(object sender, EventArgs e)
        {
            if (griPipeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index].SetField("REMARK", texPipeRemark.Text);
                griPipeList.UpdateData();
            }
        }

        #endregion

        #region Coordinate 이벤트

        //추가버튼 클릭 이벤트
        private void butAddCoordinate_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griCoordinateList.DataSource).Rows.Count;
                griCoordinateList.ActiveRow = null;

                DataRow row = ((DataTable)griCoordinateList.DataSource).NewRow();
                ((DataTable)griCoordinateList.DataSource).Rows.Add(row);

                griCoordinateList.Rows[count].Selected = true;
                griCoordinateList.ActiveRowScrollRegion.ScrollRowIntoView(griCoordinateList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveCoordinate_Click(object sender, EventArgs e)
        {
            if (griCoordinateList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griCoordinateList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("Node ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["X"]))
                    {
                        MessageBox.Show("X좌표가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["Y"]))
                    {
                        MessageBox.Show("Y좌표가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveCoordinateDataList(conditions);
                    griCoordinateList.DataSource = result.Tables["WH_COORDINATES"];

                    if (griCoordinateList.Rows.Count > 0)
                    {
                        griCoordinateList.Rows[0].Selected = true;
                        griCoordinateList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteCoordinate_Click(object sender, EventArgs e)
        {
            if (griCoordinateList.Selected.Rows.Count != 0)
            {
                int idx = griCoordinateList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griCoordinateList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griCoordinateList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griCoordinateList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griCoordinateList.DataSource).Rows.Count;

                        griCoordinateList.Rows[count - 1].Selected = true;
                        griCoordinateList.ActiveRowScrollRegion.ScrollRowIntoView(griCoordinateList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texCoordinateId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteCoordinateData(conditions);
                            griCoordinateList.DataSource = dSet.Tables["WH_COORDINATES"];

                            if (griCoordinateList.Rows.Count > 0)
                            {
                                griCoordinateList.Rows[0].Selected = true;
                                griCoordinateList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texCoordinateId.ReadOnly = false;

                                texCoordinateId.Text = "";
                                texCoordinateX.Text = "";
                                texCoordinateY.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //Node 필드 입력 이벤트
        private void texCoordinateId_TextChanged(object sender, EventArgs e)
        {
            if (griCoordinateList.Selected.Rows.Count != 0)
            {
                ((DataTable)griCoordinateList.DataSource).Rows[griCoordinateList.Selected.Rows[0].Index].SetField("ID", texCoordinateId.Text);
                griCoordinateList.UpdateData();
            }
        }

        //X-Coord 필드 입력 이벤트
        private void texCoordinateX_TextChanged(object sender, EventArgs e)
        {
            if (griCoordinateList.Selected.Rows.Count != 0)
            {
                ((DataTable)griCoordinateList.DataSource).Rows[griCoordinateList.Selected.Rows[0].Index].SetField("X", texCoordinateX.Text);
                griCoordinateList.UpdateData();
            }
        }

        //Y-Coord 필드 입력 이벤트
        private void texCoordinateY_TextChanged(object sender, EventArgs e)
        {
            if (griCoordinateList.Selected.Rows.Count != 0)
            {
                ((DataTable)griCoordinateList.DataSource).Rows[griCoordinateList.Selected.Rows[0].Index].SetField("Y", texCoordinateY.Text);
                griCoordinateList.UpdateData();
            }
        }

        #endregion

        #region Vertices 이벤트

        //추가버튼 클릭 이벤트
        private void butAddVertices_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griVerticesList.DataSource).Rows.Count;
                griVerticesList.ActiveRow = null;

                DataRow row = ((DataTable)griVerticesList.DataSource).NewRow();
                ((DataTable)griVerticesList.DataSource).Rows.Add(row);

                griVerticesList.Rows[count].Selected = true;
                griVerticesList.ActiveRowScrollRegion.ScrollRowIntoView(griVerticesList.Rows[count]);
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveVertices_Click(object sender, EventArgs e)
        {
            if (griVerticesList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griVerticesList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("Node ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["X"]))
                    {
                        MessageBox.Show("X좌표가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["Y"]))
                    {
                        MessageBox.Show("Y좌표가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveVerticesDataList(conditions);
                    griVerticesList.DataSource = result.Tables["WH_VERTICES"];

                    if (griVerticesList.Rows.Count > 0)
                    {
                        griVerticesList.Rows[0].Selected = true;
                        griVerticesList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteVertices_Click(object sender, EventArgs e)
        {
            if (griVerticesList.Selected.Rows.Count != 0)
            {
                int idx = griVerticesList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griVerticesList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griVerticesList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griVerticesList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griVerticesList.DataSource).Rows.Count;

                        griVerticesList.Rows[count - 1].Selected = true;
                        griVerticesList.ActiveRowScrollRegion.ScrollRowIntoView(griVerticesList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texVerticesId.Text);
                        conditions.Add("IDX", texVerticesIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteVerticesData(conditions);
                            griVerticesList.DataSource = dSet.Tables["WH_VERTICES"];

                            if (griVerticesList.Rows.Count > 0)
                            {
                                griVerticesList.Rows[0].Selected = true;
                                griVerticesList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texVerticesId.ReadOnly = false;

                                texVerticesId.Text = "";
                                texVerticesIdx.Text = "";
                                texVerticesX.Text = "";
                                texVerticesY.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //Link 필드 입력 이벤트
        private void texVerticesId_TextChanged(object sender, EventArgs e)
        {
            if (griVerticesList.Selected.Rows.Count != 0)
            {
                ((DataTable)griVerticesList.DataSource).Rows[griVerticesList.Selected.Rows[0].Index].SetField("ID", texVerticesId.Text);
                griVerticesList.UpdateData();
            }
        }

        //Index 필드 입력 이벤트
        private void texVerticesIdx_TextChanged(object sender, EventArgs e)
        {
            if (griVerticesList.Selected.Rows.Count != 0)
            {
                ((DataTable)griVerticesList.DataSource).Rows[griVerticesList.Selected.Rows[0].Index].SetField("IDX", texVerticesIdx.Text);
                griVerticesList.UpdateData();
            }
        }

        //X-Coord 필드 입력 이벤트
        private void texVerticesX_TextChanged(object sender, EventArgs e)
        {
            if (griVerticesList.Selected.Rows.Count != 0)
            {
                ((DataTable)griVerticesList.DataSource).Rows[griVerticesList.Selected.Rows[0].Index].SetField("X", texVerticesX.Text);
                griVerticesList.UpdateData();
            }
        }

        //Y-Coord 필드 입력 이벤트
        private void texVerticesY_TextChanged(object sender, EventArgs e)
        {
            if (griVerticesList.Selected.Rows.Count != 0)
            {
                ((DataTable)griVerticesList.DataSource).Rows[griVerticesList.Selected.Rows[0].Index].SetField("Y", texVerticesY.Text);
                griVerticesList.UpdateData();
            }
        }

        #endregion

        #region Pump 이벤트

        //추가버튼 클릭 이벤트
        private void butAddPumps_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griPumpList.DataSource).Rows.Count;
                griPumpList.ActiveRow = null;

                DataRow row = ((DataTable)griPumpList.DataSource).NewRow();
                ((DataTable)griPumpList.DataSource).Rows.Add(row);

                griPumpList.Rows[count].Selected = true;
                griPumpList.ActiveRowScrollRegion.ScrollRowIntoView(griPumpList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeletePumps_Click(object sender, EventArgs e)
        {
            if (griPumpList.Selected.Rows.Count != 0)
            {
                int idx = griPumpList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griPumpList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griPumpList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griPumpList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griPumpList.DataSource).Rows.Count;

                        griPumpList.Rows[count - 1].Selected = true;
                        griPumpList.ActiveRowScrollRegion.ScrollRowIntoView(griPumpList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texPumpId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeletePumpData(conditions);
                            griPumpList.DataSource = dSet.Tables["WH_PUMPS"];

                            if (griPumpList.Rows.Count > 0)
                            {
                                griPumpList.Rows[0].Selected = true;
                                griPumpList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texPumpId.ReadOnly = false;

                                texPumpId.Text = "";
                                texPumpNode1.Text = "";
                                texPumpNode2.Text = "";
                                texPumpProperties.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSavePumps_Click(object sender, EventArgs e)
        {
            if (griPumpList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griPumpList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("Pump ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["NODE1"]))
                    {
                        MessageBox.Show("시작점 Node ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["NODE2"]))
                    {
                        MessageBox.Show("종료점 Node ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SavePumpDataList(conditions);
                    griPumpList.DataSource = result.Tables["WH_PUMPS"];

                    if (griPumpList.Rows.Count > 0)
                    {
                        griPumpList.Rows[0].Selected = true;
                        griPumpList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //ID 필드 입력 이벤트
        private void texPumpId_TextChanged(object sender, EventArgs e)
        {
            if (griPumpList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPumpList.DataSource).Rows[griPumpList.Selected.Rows[0].Index].SetField("ID", texPumpId.Text);
                griPumpList.UpdateData();
            }
        }

        //Node1 필드 입력 이벤트
        private void texPumpNode1_TextChanged(object sender, EventArgs e)
        {
            if (griPumpList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPumpList.DataSource).Rows[griPumpList.Selected.Rows[0].Index].SetField("NODE1", texPumpNode1.Text);
                griPumpList.UpdateData();
            }
        }

        //Node2 필드 입력 이벤트
        private void texPumpNode2_TextChanged(object sender, EventArgs e)
        {
            if (griPumpList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPumpList.DataSource).Rows[griPumpList.Selected.Rows[0].Index].SetField("NODE2", texPumpNode2.Text);
                griPumpList.UpdateData();
            }
        }

        //Properties 필드 입력 이벤트
        private void texPumpProperties_TextChanged(object sender, EventArgs e)
        {
            if (griPumpList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPumpList.DataSource).Rows[griPumpList.Selected.Rows[0].Index].SetField("PROPERTIES", texPumpProperties.Text);
                griPumpList.UpdateData();
            }
        }

        #endregion

        #region Valve 이벤트

        //추가버튼 클릭 이벤트
        private void butAddValve_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griValveList.DataSource).Rows.Count;
                griValveList.ActiveRow = null;

                DataRow row = ((DataTable)griValveList.DataSource).NewRow();
                ((DataTable)griValveList.DataSource).Rows.Add(row);

                griValveList.Rows[count].Selected = true;
                griValveList.ActiveRowScrollRegion.ScrollRowIntoView(griValveList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteValve_Click(object sender, EventArgs e)
        {
            if (griValveList.Selected.Rows.Count != 0)
            {
                int idx = griValveList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griValveList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griValveList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griValveList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griValveList.DataSource).Rows.Count;

                        griValveList.Rows[count - 1].Selected = true;
                        griValveList.ActiveRowScrollRegion.ScrollRowIntoView(griValveList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texValveId.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteValveData(conditions);
                            griValveList.DataSource = dSet.Tables["WH_VALVES"];

                            if (griValveList.Rows.Count > 0)
                            {
                                griValveList.Rows[0].Selected = true;
                                griValveList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texValveId.ReadOnly = false;

                                texValveId.Text = "";
                                texValveNode1.Text = "";
                                texValveNode2.Text = "";
                                texValveDiameter.Text = "";
                                texValveType.Text = "";
                                texValveSetting.Text = "";
                                texValveMinorLoss.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveValve_Click(object sender, EventArgs e)
        {
            if (griValveList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griValveList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("Valve ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["NODE1"]))
                    {
                        MessageBox.Show("시작점 Node ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["NODE2"]))
                    {
                        MessageBox.Show("종료점 Node ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["DIAMETER"]))
                    {
                        MessageBox.Show("DIAMETER가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["TYPE"]))
                    {
                        MessageBox.Show("TYPE이 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveValveDataList(conditions);
                    griValveList.DataSource = result.Tables["WH_VALVES"];

                    if (griValveList.Rows.Count > 0)
                    {
                        griValveList.Rows[0].Selected = true;
                        griValveList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //ID 필드 입력 이벤트
        private void texValveId_TextChanged(object sender, EventArgs e)
        {
            if (griValveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griValveList.DataSource).Rows[griValveList.Selected.Rows[0].Index].SetField("ID", texValveId.Text);
                griValveList.UpdateData();
            }
        }

        //Node1 필드 입력 이벤트
        private void texValveNode1_TextChanged(object sender, EventArgs e)
        {
            if (griValveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griValveList.DataSource).Rows[griValveList.Selected.Rows[0].Index].SetField("NODE1", texValveNode1.Text);
                griValveList.UpdateData();
            }
        }

        //Node2 필드 입력 이벤트
        private void texValveNode2_TextChanged(object sender, EventArgs e)
        {
            if (griValveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griValveList.DataSource).Rows[griValveList.Selected.Rows[0].Index].SetField("NODE2", texValveNode2.Text);
                griValveList.UpdateData();
            }
        }

        //Diameter 필드 입력 이벤트
        private void texValveDiameter_TextChanged(object sender, EventArgs e)
        {
            if (griValveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griValveList.DataSource).Rows[griValveList.Selected.Rows[0].Index].SetField("DIAMETER", texValveDiameter.Text);
                griValveList.UpdateData();
            }
        }

        //Type 필드 입력 이벤트
        private void texValveType_TextChanged(object sender, EventArgs e)
        {
            if (griValveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griValveList.DataSource).Rows[griValveList.Selected.Rows[0].Index].SetField("TYPE", texValveType.Text);
                griValveList.UpdateData();
            }
        }

        //Setting 필드 입력 이벤트
        private void texValveSetting_TextChanged(object sender, EventArgs e)
        {
            if (griValveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griValveList.DataSource).Rows[griValveList.Selected.Rows[0].Index].SetField("SETTING", texValveSetting.Text);
                griValveList.UpdateData();
            }
        }

        //MinorLoss 필드 입력 이벤트
        private void texValveMinorLoss_TextChanged(object sender, EventArgs e)
        {
            if (griValveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griValveList.DataSource).Rows[griValveList.Selected.Rows[0].Index].SetField("MINORLOSS", texValveMinorLoss.Text);
                griValveList.UpdateData();
            }

        }

        #endregion

        #region Energy 이벤트

        //추가버튼 클릭 이벤트
        private void butAddEnergy_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griEnergyList.DataSource).Rows.Count;
                griEnergyList.ActiveRow = null;

                DataRow row = ((DataTable)griEnergyList.DataSource).NewRow();
                ((DataTable)griEnergyList.DataSource).Rows.Add(row);

                griEnergyList.Rows[count].Selected = true;
                griEnergyList.ActiveRowScrollRegion.ScrollRowIntoView(griEnergyList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteEnergy_Click(object sender, EventArgs e)
        {
            if (griEnergyList.Selected.Rows.Count != 0)
            {
                int idx = griEnergyList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griEnergyList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griEnergyList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griEnergyList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griEnergyList.DataSource).Rows.Count;

                        griEnergyList.Rows[count - 1].Selected = true;
                        griEnergyList.ActiveRowScrollRegion.ScrollRowIntoView(griEnergyList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("IDX", texEnergyIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteEnergyData(conditions);
                            griEnergyList.DataSource = dSet.Tables["WH_ENERGY"];

                            if (griEnergyList.Rows.Count > 0)
                            {
                                griEnergyList.Rows[0].Selected = true;
                                griEnergyList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texEnergyIdx.ReadOnly = false;

                                texEnergyIdx.Text = "";
                                texEnergyEnergyStatement.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveEnergy_Click(object sender, EventArgs e)
        {
            if (griEnergyList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griEnergyList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["ENERGY_STATEMENT"]))
                    {
                        MessageBox.Show("Statement가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveEnergyDataList(conditions);
                    griEnergyList.DataSource = result.Tables["WH_ENERGY"];

                    if (griEnergyList.Rows.Count > 0)
                    {
                        griEnergyList.Rows[0].Selected = true;
                        griEnergyList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //Idx 필드 입력 이벤트
        private void texEnergyIdx_TextChanged(object sender, EventArgs e)
        {
            if (griEnergyList.Selected.Rows.Count != 0)
            {
                ((DataTable)griEnergyList.DataSource).Rows[griEnergyList.Selected.Rows[0].Index].SetField("IDX", texEnergyIdx.Text);
                griEnergyList.UpdateData();
            }
        }

        //Statement 필드 입력 이벤트
        private void texEnergyEnergyStatement_TextChanged(object sender, EventArgs e)
        {
            if (griEnergyList.Selected.Rows.Count != 0)
            {
                ((DataTable)griEnergyList.DataSource).Rows[griEnergyList.Selected.Rows[0].Index].SetField("ENERGY_STATEMENT", texEnergyEnergyStatement.Text);
                griEnergyList.UpdateData();
            }
        }

        #endregion

        #region Curves 이벤트

        //추가버튼 클릭 이벤트
        private void butAddCurve_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griCurveList.DataSource).Rows.Count;
                griCurveList.ActiveRow = null;

                DataRow row = ((DataTable)griCurveList.DataSource).NewRow();
                ((DataTable)griCurveList.DataSource).Rows.Add(row);

                griCurveList.Rows[count].Selected = true;
                griCurveList.ActiveRowScrollRegion.ScrollRowIntoView(griCurveList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteCurve_Click(object sender, EventArgs e)
        {
            if (griCurveList.Selected.Rows.Count != 0)
            {
                int idx = griCurveList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griCurveList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griCurveList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griCurveList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griCurveList.DataSource).Rows.Count;

                        griCurveList.Rows[count - 1].Selected = true;
                        griCurveList.ActiveRowScrollRegion.ScrollRowIntoView(griCurveList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texCurveId.Text);
                        conditions.Add("IDX", texCurveIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteCurveData(conditions);
                            griCurveList.DataSource = dSet.Tables["WH_CURVES"];

                            if (griCurveList.Rows.Count > 0)
                            {
                                griCurveList.Rows[0].Selected = true;
                                griCurveList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texCurveId.ReadOnly = false;

                                texCurveId.Text = "";
                                texCurveIdx.Text = "";
                                texCurveX.Text = "";
                                texCurveY.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveCurve_Click(object sender, EventArgs e)
        {
            if (griCurveList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griCurveList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("Curve ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["X"]))
                    {
                        MessageBox.Show("X값이 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["Y"]))
                    {
                        MessageBox.Show("Y값이 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveCurveDataList(conditions);
                    griCurveList.DataSource = result.Tables["WH_CURVES"];

                    if (griCurveList.Rows.Count > 0)
                    {
                        griCurveList.Rows[0].Selected = true;
                        griCurveList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //ID 필드 입력 이벤트
        private void texCurveId_TextChanged(object sender, EventArgs e)
        {
            if (griCurveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griCurveList.DataSource).Rows[griCurveList.Selected.Rows[0].Index].SetField("ID", texCurveId.Text);
                griCurveList.UpdateData();
            }
        }

        //Index 필드 입력 이벤트
        private void texCurveIdx_TextChanged(object sender, EventArgs e)
        {
            if (griCurveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griCurveList.DataSource).Rows[griCurveList.Selected.Rows[0].Index].SetField("IDX", texCurveIdx.Text);
                griCurveList.UpdateData();
            }
        }

        //X value 필드 입력 이벤트
        private void texCurveX_TextChanged(object sender, EventArgs e)
        {
            if (griCurveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griCurveList.DataSource).Rows[griCurveList.Selected.Rows[0].Index].SetField("X", texCurveX.Text);
                griCurveList.UpdateData();
            }
        }

        //Y value 필드 입력 이벤트
        private void texCurveY_TextChanged(object sender, EventArgs e)
        {
            if (griCurveList.Selected.Rows.Count != 0)
            {
                ((DataTable)griCurveList.DataSource).Rows[griCurveList.Selected.Rows[0].Index].SetField("Y", texCurveY.Text);
                griCurveList.UpdateData();
            }
        }

        #endregion

        #region Pattern 이벤트

        //추가버튼 클릭 이벤트
        private void butAddPattern_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griPatternList.DataSource).Rows.Count;
                griPatternList.ActiveRow = null;

                DataRow row = ((DataTable)griPatternList.DataSource).NewRow();
                ((DataTable)griPatternList.DataSource).Rows.Add(row);

                griPatternList.Rows[count].Selected = true;
                griPatternList.ActiveRowScrollRegion.ScrollRowIntoView(griPatternList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeletePattern_Click(object sender, EventArgs e)
        {
            if (griPatternList.Selected.Rows.Count != 0)
            {
                int idx = griPatternList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griPatternList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griPatternList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griPatternList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griPatternList.DataSource).Rows.Count;

                        griPatternList.Rows[count - 1].Selected = true;
                        griPatternList.ActiveRowScrollRegion.ScrollRowIntoView(griPatternList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("PATTERN_ID", texPatternPatternId.Text);
                        conditions.Add("IDX", texPatternIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeletePatternData(conditions);
                            griPatternList.DataSource = dSet.Tables["WH_PATTERNS"];

                            if (griPatternList.Rows.Count > 0)
                            {
                                griPatternList.Rows[0].Selected = true;
                                griPatternList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texPatternPatternId.ReadOnly = false;
                                texPatternIdx.ReadOnly = false;

                                texPatternPatternId.Text = "";
                                texPatternIdx.Text = "";
                                texPatternMultiplier.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void buSavePattern_Click(object sender, EventArgs e)
        {
            if (griPatternList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griPatternList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["PATTERN_ID"]))
                    {
                        MessageBox.Show("Pattern ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["MULTIPLIER"]))
                    {
                        MessageBox.Show("Multiplier가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SavePatternDataList(conditions);
                    griPatternList.DataSource = result.Tables["WH_PATTERNS"];

                    if (griPatternList.Rows.Count > 0)
                    {
                        griPatternList.Rows[0].Selected = true;
                        griPatternList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //ID 필드 입력 이벤트
        private void texPatternPatternId_TextChanged(object sender, EventArgs e)
        {
            if (griPatternList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPatternList.DataSource).Rows[griPatternList.Selected.Rows[0].Index].SetField("PATTERN_ID", texPatternPatternId.Text);
                griPatternList.UpdateData();
            }
        }

        //Index 필드 입력 이벤트
        private void texPatternIdx_TextChanged(object sender, EventArgs e)
        {
            if (griPatternList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPatternList.DataSource).Rows[griPatternList.Selected.Rows[0].Index].SetField("IDX", texPatternIdx.Text);
                griPatternList.UpdateData();
            }
        }

        //Multiplier 필드 입력 이벤트
        private void texPatternMultiplier_TextChanged(object sender, EventArgs e)
        {
            if (griPatternList.Selected.Rows.Count != 0)
            {
                ((DataTable)griPatternList.DataSource).Rows[griPatternList.Selected.Rows[0].Index].SetField("MULTIPLIER", texPatternMultiplier.Text);
                griPatternList.UpdateData();
            }
        }

        #endregion

        #region Status 이벤트

        //추가버튼 클릭 이벤트
        private void butAddStatus_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griStatusList.DataSource).Rows.Count;
                griStatusList.ActiveRow = null;

                DataRow row = ((DataTable)griStatusList.DataSource).NewRow();
                ((DataTable)griStatusList.DataSource).Rows.Add(row);

                griStatusList.Rows[count].Selected = true;
                griStatusList.ActiveRowScrollRegion.ScrollRowIntoView(griStatusList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteStatus_Click(object sender, EventArgs e)
        {
            if (griStatusList.Selected.Rows.Count != 0)
            {
                int idx = griStatusList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griStatusList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griStatusList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griStatusList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griStatusList.DataSource).Rows.Count;

                        griStatusList.Rows[count - 1].Selected = true;
                        griStatusList.ActiveRowScrollRegion.ScrollRowIntoView(griStatusList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("ID", texStatusId.Text);
                        conditions.Add("IDX", texStatusIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteStatusData(conditions);
                            griStatusList.DataSource = dSet.Tables["WH_STATUS"];

                            if (griStatusList.Rows.Count > 0)
                            {
                                griStatusList.Rows[0].Selected = true;
                                griStatusList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texStatusId.ReadOnly = false;
                                texStatusIdx.ReadOnly = false;

                                texStatusId.Text = "";
                                texStatusIdx.Text = "";
                                texStatusStatusSetting.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveStatus_Click(object sender, EventArgs e)
        {
            if (griStatusList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griStatusList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["ID"]))
                    {
                        MessageBox.Show("Status ID가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["STATUS_SETTING"]))
                    {
                        MessageBox.Show("Status/Setting이 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveStatusDataList(conditions);
                    griStatusList.DataSource = result.Tables["WH_STATUS"];

                    if (griStatusList.Rows.Count > 0)
                    {
                        griStatusList.Rows[0].Selected = true;
                        griStatusList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //ID 필드 입력 이벤트
        private void texStatusId_TextChanged(object sender, EventArgs e)
        {
            if (griStatusList.Selected.Rows.Count != 0)
            {
                ((DataTable)griStatusList.DataSource).Rows[griStatusList.Selected.Rows[0].Index].SetField("ID", texStatusId.Text);
                griStatusList.UpdateData();
            }
        }

        //IDX 필드 입력 이벤트
        private void texStatusIdx_TextChanged(object sender, EventArgs e)
        {
            if (griStatusList.Selected.Rows.Count != 0)
            {
                ((DataTable)griStatusList.DataSource).Rows[griStatusList.Selected.Rows[0].Index].SetField("IDX", texStatusIdx.Text);
                griStatusList.UpdateData();
            }
        }

        //Status/Setting 필드 입력 이벤트
        private void texStatusStatusSetting_TextChanged(object sender, EventArgs e)
        {
            if (griStatusList.Selected.Rows.Count != 0)
            {
                ((DataTable)griStatusList.DataSource).Rows[griStatusList.Selected.Rows[0].Index].SetField("STATUS_SETTING", texStatusStatusSetting.Text);
                griStatusList.UpdateData();
            }
        }

        #endregion

        #region Control 이벤트

        //추가버튼 클릭 이벤트
        private void butAddControl_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griControlList.DataSource).Rows.Count;
                griControlList.ActiveRow = null;

                DataRow row = ((DataTable)griControlList.DataSource).NewRow();
                ((DataTable)griControlList.DataSource).Rows.Add(row);

                griControlList.Rows[count].Selected = true;
                griControlList.ActiveRowScrollRegion.ScrollRowIntoView(griControlList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteControl_Click(object sender, EventArgs e)
        {
            if (griControlList.Selected.Rows.Count != 0)
            {
                int idx = griControlList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griControlList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griControlList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griControlList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griControlList.DataSource).Rows.Count;

                        griControlList.Rows[count - 1].Selected = true;
                        griControlList.ActiveRowScrollRegion.ScrollRowIntoView(griControlList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("IDX", texControlIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteControlData(conditions);
                            griControlList.DataSource = dSet.Tables["WH_CONTROLS"];

                            if (griControlList.Rows.Count > 0)
                            {
                                griControlList.Rows[0].Selected = true;
                                griControlList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texControlIdx.ReadOnly = false;

                                texControlIdx.Text = "";
                                texControlControlsStatement.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveControl_Click(object sender, EventArgs e)
        {
            if (griControlList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griControlList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["CONTROLS_STATEMENT"]))
                    {
                        MessageBox.Show("Statement가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveControlDataList(conditions);
                    griControlList.DataSource = result.Tables["WH_CONTROLS"];

                    if (griControlList.Rows.Count > 0)
                    {
                        griControlList.Rows[0].Selected = true;
                        griControlList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //Idx 필드 입력 이벤트
        private void texControlIdx_TextChanged(object sender, EventArgs e)
        {
            if (griControlList.Selected.Rows.Count != 0)
            {
                ((DataTable)griControlList.DataSource).Rows[griControlList.Selected.Rows[0].Index].SetField("IDX", texControlIdx.Text);
                griControlList.UpdateData();
            }
        }

        //Statement 필드 입력 이벤트
        private void texControlControlsStatement_TextChanged(object sender, EventArgs e)
        {
            if (griControlList.Selected.Rows.Count != 0)
            {
                ((DataTable)griControlList.DataSource).Rows[griControlList.Selected.Rows[0].Index].SetField("CONTROLS_STATEMENT", texControlControlsStatement.Text);
                griControlList.UpdateData();
            }
        }

        #endregion

        #region Rule 이벤트

        //추가버튼 클릭 이벤트
        private void butAddRule_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griRuleList.DataSource).Rows.Count;
                griRuleList.ActiveRow = null;

                DataRow row = ((DataTable)griRuleList.DataSource).NewRow();
                ((DataTable)griRuleList.DataSource).Rows.Add(row);

                griRuleList.Rows[count].Selected = true;
                griRuleList.ActiveRowScrollRegion.ScrollRowIntoView(griRuleList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteRule_Click(object sender, EventArgs e)
        {
            if (griRuleList.Selected.Rows.Count != 0)
            {
                int idx = griRuleList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griRuleList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griRuleList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griRuleList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griRuleList.DataSource).Rows.Count;

                        griRuleList.Rows[count - 1].Selected = true;
                        griRuleList.ActiveRowScrollRegion.ScrollRowIntoView(griRuleList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("IDX", texRuleIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteRuleData(conditions);
                            griRuleList.DataSource = dSet.Tables["WH_RULES"];

                            if (griRuleList.Rows.Count > 0)
                            {
                                griRuleList.Rows[0].Selected = true;
                                griRuleList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texRuleIdx.ReadOnly = false;

                                texRuleIdx.Text = "";
                                texRuleRulesStatement.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveRule_Click(object sender, EventArgs e)
        {
            if (griRuleList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griRuleList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["RULES_STATEMENT"]))
                    {
                        MessageBox.Show("Statement가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveRuleDataList(conditions);
                    griRuleList.DataSource = result.Tables["WH_RULES"];

                    if (griRuleList.Rows.Count > 0)
                    {
                        griRuleList.Rows[0].Selected = true;
                        griRuleList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //Idx 필드 입력 이벤트
        private void texRuleIdx_TextChanged(object sender, EventArgs e)
        {
            if (griRuleList.Selected.Rows.Count != 0)
            {
                ((DataTable)griRuleList.DataSource).Rows[griRuleList.Selected.Rows[0].Index].SetField("IDX", texRuleIdx.Text);
                griRuleList.UpdateData();
            }
        }

        //Statement 필드 입력 이벤트
        private void texRuleRulesStatement_TextChanged(object sender, EventArgs e)
        {
            if (griRuleList.Selected.Rows.Count != 0)
            {
                ((DataTable)griRuleList.DataSource).Rows[griRuleList.Selected.Rows[0].Index].SetField("RULES_STATEMENT", texRuleRulesStatement.Text);
                griRuleList.UpdateData();
            }
        }

        #endregion

        #region Reaction 이벤트

        //추가버튼 클릭 이벤트
        private void butAddReaction_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griReactionList.DataSource).Rows.Count;
                griReactionList.ActiveRow = null;

                DataRow row = ((DataTable)griReactionList.DataSource).NewRow();
                ((DataTable)griReactionList.DataSource).Rows.Add(row);

                griReactionList.Rows[count].Selected = true;
                griReactionList.ActiveRowScrollRegion.ScrollRowIntoView(griReactionList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteReaction_Click(object sender, EventArgs e)
        {
            if (griReactionList.Selected.Rows.Count != 0)
            {
                int idx = griReactionList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griReactionList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griReactionList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griReactionList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griReactionList.DataSource).Rows.Count;

                        griReactionList.Rows[count - 1].Selected = true;
                        griReactionList.ActiveRowScrollRegion.ScrollRowIntoView(griReactionList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("IDX", texReactionIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteReactionData(conditions);
                            griReactionList.DataSource = dSet.Tables["WH_REACTIONS"];

                            if (griReactionList.Rows.Count > 0)
                            {
                                griReactionList.Rows[0].Selected = true;
                                griReactionList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texReactionIdx.ReadOnly = false;

                                texReactionIdx.Text = "";
                                texReactionReactionStatement.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveReaction_Click(object sender, EventArgs e)
        {
            if (griReactionList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griReactionList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["REACTION_STATEMENT"]))
                    {
                        MessageBox.Show("Statement가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveReactionDataList(conditions);
                    griReactionList.DataSource = result.Tables["WH_REACTIONS"];

                    if (griReactionList.Rows.Count > 0)
                    {
                        griReactionList.Rows[0].Selected = true;
                        griReactionList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //Idx 필드 입력 이벤트
        private void texReactionIdx_TextChanged(object sender, EventArgs e)
        {
            if (griReactionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griReactionList.DataSource).Rows[griReactionList.Selected.Rows[0].Index].SetField("IDX", texReactionIdx.Text);
                griReactionList.UpdateData();
            }
        }

        //Statement 필드 입력 이벤트
        private void texReactionReactionStatement_TextChanged(object sender, EventArgs e)
        {
            if (griReactionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griReactionList.DataSource).Rows[griReactionList.Selected.Rows[0].Index].SetField("REACTION_STATEMENT", texReactionReactionStatement.Text);
                griReactionList.UpdateData();
            }
        }

        #endregion

        #region Option 이벤트

        //추가버튼 클릭 이벤트
        private void butAddOption_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griOptionList.DataSource).Rows.Count;
                griOptionList.ActiveRow = null;

                DataRow row = ((DataTable)griOptionList.DataSource).NewRow();
                ((DataTable)griOptionList.DataSource).Rows.Add(row);

                griOptionList.Rows[count].Selected = true;
                griOptionList.ActiveRowScrollRegion.ScrollRowIntoView(griOptionList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteOption_Click(object sender, EventArgs e)
        {
            if (griOptionList.Selected.Rows.Count != 0)
            {
                int idx = griOptionList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griOptionList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griOptionList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griOptionList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griOptionList.DataSource).Rows.Count;

                        griOptionList.Rows[count - 1].Selected = true;
                        griOptionList.ActiveRowScrollRegion.ScrollRowIntoView(griOptionList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("IDX", texOptionIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteOptionData(conditions);
                            griOptionList.DataSource = dSet.Tables["WH_OPTIONS"];

                            if (griOptionList.Rows.Count > 0)
                            {
                                griOptionList.Rows[0].Selected = true;
                                griOptionList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texOptionIdx.ReadOnly = false;

                                texOptionIdx.Text = "";
                                texOptionOptionsStatement.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveOption_Click(object sender, EventArgs e)
        {
            if (griOptionList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griOptionList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["OPTIONS_STATEMENT"]))
                    {
                        MessageBox.Show("Statement가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveOptionDataList(conditions);
                    griOptionList.DataSource = result.Tables["WH_OPTIONS"];

                    if (griOptionList.Rows.Count > 0)
                    {
                        griOptionList.Rows[0].Selected = true;
                        griOptionList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //Idx 필드 입력 이벤트
        private void texOptionIdx_TextChanged(object sender, EventArgs e)
        {
            if (griOptionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griOptionList.DataSource).Rows[griOptionList.Selected.Rows[0].Index].SetField("IDX", texOptionIdx.Text);
                griOptionList.UpdateData();
            }
        }

        //Statement 필드 입력 이벤트
        private void texOptionOptionsStatement_TextChanged(object sender, EventArgs e)
        {
            if (griOptionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griOptionList.DataSource).Rows[griOptionList.Selected.Rows[0].Index].SetField("OPTIONS_STATEMENT", texOptionOptionsStatement.Text);
                griOptionList.UpdateData();
            }
        }

        #endregion

        #region Time Option 이벤트

        //추가버튼 클릭 이벤트
        private void butAddTime_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griTimeList.DataSource).Rows.Count;
                griTimeList.ActiveRow = null;

                DataRow row = ((DataTable)griTimeList.DataSource).NewRow();
                ((DataTable)griTimeList.DataSource).Rows.Add(row);

                griTimeList.Rows[count].Selected = true;
                griTimeList.ActiveRowScrollRegion.ScrollRowIntoView(griTimeList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteTime_Click(object sender, EventArgs e)
        {
            if (griTimeList.Selected.Rows.Count != 0)
            {
                int idx = griTimeList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griTimeList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griTimeList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griTimeList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griTimeList.DataSource).Rows.Count;

                        griTimeList.Rows[count - 1].Selected = true;
                        griTimeList.ActiveRowScrollRegion.ScrollRowIntoView(griTimeList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("IDX", texTimeIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteTimeOptionData(conditions);
                            griTimeList.DataSource = dSet.Tables["WH_TIMES"];

                            if (griTimeList.Rows.Count > 0)
                            {
                                griTimeList.Rows[0].Selected = true;
                                griTimeList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texTimeIdx.ReadOnly = false;

                                texTimeIdx.Text = "";
                                texTimeTimesStatement.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveTime_Click(object sender, EventArgs e)
        {
            if (griTimeList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griTimeList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["TIMES_STATEMENT"]))
                    {
                        MessageBox.Show("Statement가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveTimeOptionDataList(conditions);
                    griTimeList.DataSource = result.Tables["WH_TIMES"];

                    if (griTimeList.Rows.Count > 0)
                    {
                        griTimeList.Rows[0].Selected = true;
                        griTimeList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //Idx 필드 입력 이벤트
        private void texTimeIdx_TextChanged(object sender, EventArgs e)
        {
            if (griTimeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTimeList.DataSource).Rows[griTimeList.Selected.Rows[0].Index].SetField("IDX", texTimeIdx.Text);
                griTimeList.UpdateData();
            }
        }

        //Statement 필드 입력 이벤트
        private void texTimeTimesStatement_TextChanged(object sender, EventArgs e)
        {
            if (griTimeList.Selected.Rows.Count != 0)
            {
                ((DataTable)griTimeList.DataSource).Rows[griTimeList.Selected.Rows[0].Index].SetField("TIMES_STATEMENT", texTimeTimesStatement.Text);
                griTimeList.UpdateData();
            }
        }

        #endregion

        #region Report Option 이벤트

        //추가버튼 클릭 이벤트
        private void butAddReport_Click(object sender, EventArgs e)
        {
            //Master가 조회되지 않은경우에는 추가하지 않는다.
            if (!"".Equals(texDetailINPNumber.Text))
            {
                int count = ((DataTable)griRptOptionList.DataSource).Rows.Count;
                griRptOptionList.ActiveRow = null;

                DataRow row = ((DataTable)griRptOptionList.DataSource).NewRow();
                ((DataTable)griRptOptionList.DataSource).Rows.Add(row);

                griRptOptionList.Rows[count].Selected = true;
                griRptOptionList.ActiveRowScrollRegion.ScrollRowIntoView(griRptOptionList.Rows[count]);
            }
        }

        //삭제버튼 클릭 이벤트
        private void butDeleteReport_Click(object sender, EventArgs e)
        {
            if (griRptOptionList.Selected.Rows.Count != 0)
            {
                int idx = griRptOptionList.Selected.Rows[0].Index;

                //DataRow의 상태확인
                if (((DataTable)griRptOptionList.DataSource).Rows[idx].RowState == DataRowState.Added)
                {
                    //추가인 경우 그냥 삭제만 한다.
                    ((DataTable)griRptOptionList.DataSource).Rows.RemoveAt(idx);

                    //삭제하고 행이 남았을 경우만 포커싱
                    if (griRptOptionList.Rows.Count > 0)
                    {
                        int count = ((DataTable)griRptOptionList.DataSource).Rows.Count;

                        griRptOptionList.Rows[count - 1].Selected = true;
                        griRptOptionList.ActiveRowScrollRegion.ScrollRowIntoView(griRptOptionList.Rows[count - 1]);
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("삭제하시겠습니까?", "데이터 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        Hashtable conditions = new Hashtable();
                        conditions.Add("IDX", texReportIdx.Text);
                        conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                        try
                        {
                            DataSet dSet = work.DeleteReportOptionData(conditions);
                            griRptOptionList.DataSource = dSet.Tables["WH_RPT_OPTIONS"];

                            if (griRptOptionList.Rows.Count > 0)
                            {
                                griRptOptionList.Rows[0].Selected = true;
                                griRptOptionList.ActiveRowScrollRegion.ScrollPosition = 0;
                            }
                            else
                            {
                                texReportIdx.ReadOnly = false;

                                texReportIdx.Text = "";
                                texReportReportStatement.Text = "";
                            }

                            MessageBox.Show("정상적으로 처리되었습니다.");
                        }
                        catch (Exception e1)
                        {
                            MessageBox.Show(e1.Message);
                        }
                    }
                }
            }
        }

        //저장버튼 클릭 이벤트
        private void butSaveReport_Click(object sender, EventArgs e)
        {
            if (griRptOptionList.Selected.Rows.Count != 0)
            {
                DataTable changedTable = ((DataTable)griRptOptionList.DataSource).GetChanges();
                Hashtable conditions = new Hashtable();

                if (changedTable == null)
                {
                    return;
                }

                //입력값 validation
                foreach (DataRow row in changedTable.Rows)
                {
                    if ("".Equals(row["IDX"]))
                    {
                        MessageBox.Show("Index가 입력되지 않은 행이 있습니다.");
                        return;
                    }

                    if ("".Equals(row["REPORT_STATEMENT"]))
                    {
                        MessageBox.Show("Statement가 입력되지 않은 행이 있습니다.");
                        return;
                    }
                }

                try
                {
                    conditions.Add("changedTable", changedTable);
                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    DataSet result = work.SaveReportOptionDataList(conditions);
                    griRptOptionList.DataSource = result.Tables["WH_RPT_OPTIONS"];

                    if (griRptOptionList.Rows.Count > 0)
                    {
                        griRptOptionList.Rows[0].Selected = true;
                        griRptOptionList.ActiveRowScrollRegion.ScrollPosition = 0;
                    }

                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                    Console.WriteLine(e1.ToString());
                }
            }
        }

        //Idx 필드 입력 이벤트
        private void texReportIdx_TextChanged(object sender, EventArgs e)
        {
            if (griRptOptionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griRptOptionList.DataSource).Rows[griRptOptionList.Selected.Rows[0].Index].SetField("IDX", texReportIdx.Text);
                griRptOptionList.UpdateData();
            }
        }

        //Statement 필드 입력 이벤트
        private void texReportReportStatement_TextChanged(object sender, EventArgs e)
        {
            if (griRptOptionList.Selected.Rows.Count != 0)
            {
                ((DataTable)griRptOptionList.DataSource).Rows[griRptOptionList.Selected.Rows[0].Index].SetField("REPORT_STATEMENT", texReportReportStatement.Text);
                griRptOptionList.UpdateData();
            }
        }

        #endregion

        #endregion

        #region 시스템 이벤트

        //INP 리스트 그리드 행 선택이 바뀌었을 경우
        private void griINPList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griINPList.Selected.Rows.Count != 0)
            {
                texDetailINPNumber.Text = Utils.nts(griINPList.Selected.Rows[0].GetCellValue("INP_NUMBER"));
                texDetailTitle.Text = Utils.nts(griINPList.Selected.Rows[0].GetCellValue("TITLE"));
                texDetailRemark.Text = Utils.nts(griINPList.Selected.Rows[0].GetCellValue("REMARK"));

                comDetailUseGbn.SelectedValue = Utils.nts(griINPList.Selected.Rows[0].GetCellValue("USE_GBN"));
                comDetailLftridn.SelectedValue = Utils.nts(griINPList.Selected.Rows[0].GetCellValue("LFTRIDN"));
                comDetailMftridn.SelectedValue = Utils.nts(griINPList.Selected.Rows[0].GetCellValue("MFTRIDN"));
                comDetailSftridn.SelectedValue = Utils.nts(griINPList.Selected.Rows[0].GetCellValue("SFTRIDN"));

                Hashtable conditions = new Hashtable();
                conditions.Add("INP_NUMBER", Utils.nts(griINPList.Selected.Rows[0].GetCellValue("INP_NUMBER")));

                //Section Data 조회및 할당
                Hashtable sectionDatas = work.SelectSectionLists(conditions);

                griJunctionList.DataSource      = ((DataSet)sectionDatas["junction"]).Tables["WH_JUNCTIONS"];               //Junction Grid 할당    
                griReservoirList.DataSource     = ((DataSet)sectionDatas["reservoir"]).Tables["WH_RESERVOIRS"];             //Reservoir Grid 할당 
                griTankList.DataSource          = ((DataSet)sectionDatas["tank"]).Tables["WH_TANK"];                        //Tank Grid 할당    
                griQualityList.DataSource       = ((DataSet)sectionDatas["quality"]).Tables["WH_QUALITY"];                  //Quality Grid 할당    
                griSourceList.DataSource        = ((DataSet)sectionDatas["source"]).Tables["WH_SOURCE"];                    //Source Grid 할당  
                griEmitterList.DataSource       = ((DataSet)sectionDatas["emitter"]).Tables["WH_EMITTERS"];                 //Emitter Grid 할당
                griLabelList.DataSource         = ((DataSet)sectionDatas["label"]).Tables["WH_LABELS"];                     //Label Grid 할당
                griDemandList.DataSource        = ((DataSet)sectionDatas["demand"]).Tables["WH_DEMANDS"];                   //Demand Grid 할당
                griMixingList.DataSource        = ((DataSet)sectionDatas["mixing"]).Tables["WH_MIXING"];                    //Mixing Grid 할당
                griPipeList.DataSource          = ((DataSet)sectionDatas["pipe"]).Tables["WH_PIPES"];                       //Pipe Grid 할당
                griCoordinateList.DataSource    = ((DataSet)sectionDatas["coordinate"]).Tables["WH_COORDINATES"];           //Coordinate Grid 할당
                griVerticesList.DataSource      = ((DataSet)sectionDatas["vertices"]).Tables["WH_VERTICES"];                //Vertices Grid 할당
                griPumpList.DataSource          = ((DataSet)sectionDatas["pump"]).Tables["WH_PUMPS"];                       //Pump Grid 할당
                griValveList.DataSource         = ((DataSet)sectionDatas["valve"]).Tables["WH_VALVES"];                     //Valve Grid 할당
                griEnergyList.DataSource        = ((DataSet)sectionDatas["energy"]).Tables["WH_ENERGY"];                    //Energy Grid 할당
                griCurveList.DataSource         = ((DataSet)sectionDatas["curve"]).Tables["WH_CURVES"];                     //Curve Grid 할당
                griPatternList.DataSource       = ((DataSet)sectionDatas["pattern"]).Tables["WH_PATTERNS"];                 //Pattern Grid 할당
                griStatusList.DataSource        = ((DataSet)sectionDatas["status"]).Tables["WH_STATUS"];                    //Status Grid 할당
                griControlList.DataSource       = ((DataSet)sectionDatas["control"]).Tables["WH_CONTROLS"];                 //Control Grid 할당
                griRuleList.DataSource          = ((DataSet)sectionDatas["rule"]).Tables["WH_RULES"];                       //Rule Grid 할당
                griReactionList.DataSource      = ((DataSet)sectionDatas["reaction"]).Tables["WH_REACTIONS"];               //Reaction Grid 할당
                griOptionList.DataSource        = ((DataSet)sectionDatas["option"]).Tables["WH_OPTIONS"];                   //Option Grid 할당
                griTimeList.DataSource          = ((DataSet)sectionDatas["time"]).Tables["WH_TIMES"];                       //Time Grid 할당
                griRptOptionList.DataSource     = ((DataSet)sectionDatas["report"]).Tables["WH_RPT_OPTIONS"];               //Report Grid 할당

                FormManager.SetGridStyle_PerformAutoResize(griJunctionList);
                FormManager.SetGridStyle_PerformAutoResize(griReservoirList);
                FormManager.SetGridStyle_PerformAutoResize(griTankList);
                FormManager.SetGridStyle_PerformAutoResize(griQualityList);
                FormManager.SetGridStyle_PerformAutoResize(griSourceList);
                FormManager.SetGridStyle_PerformAutoResize(griEmitterList);
                FormManager.SetGridStyle_PerformAutoResize(griLabelList);
                FormManager.SetGridStyle_PerformAutoResize(griDemandList);
                FormManager.SetGridStyle_PerformAutoResize(griMixingList);
                FormManager.SetGridStyle_PerformAutoResize(griPipeList);
                FormManager.SetGridStyle_PerformAutoResize(griCoordinateList);
                FormManager.SetGridStyle_PerformAutoResize(griVerticesList);
                FormManager.SetGridStyle_PerformAutoResize(griPumpList);
                FormManager.SetGridStyle_PerformAutoResize(griValveList);
                FormManager.SetGridStyle_PerformAutoResize(griEnergyList);
                FormManager.SetGridStyle_PerformAutoResize(griCurveList);
                FormManager.SetGridStyle_PerformAutoResize(griPatternList);
                FormManager.SetGridStyle_PerformAutoResize(griStatusList);
                FormManager.SetGridStyle_PerformAutoResize(griControlList);
                FormManager.SetGridStyle_PerformAutoResize(griRuleList);
                FormManager.SetGridStyle_PerformAutoResize(griReactionList);
                FormManager.SetGridStyle_PerformAutoResize(griOptionList);
                FormManager.SetGridStyle_PerformAutoResize(griTimeList);
                FormManager.SetGridStyle_PerformAutoResize(griRptOptionList);

                //Section Tab별 Grid 조회결과를 최초행에 위치
                SetGridFirstFocusing(tabINPSection);
            }
        }

        //Junction 리스트 그리드 행 선택이 바뀌었을 경우
        private void griJunctionList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griJunctionList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griJunctionList.DataSource).Rows[griJunctionList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texJunctionId.ReadOnly = true;
                }
                else
                {
                    texJunctionId.ReadOnly = false;
                }

                texJunctionId.Text = Utils.nts(griJunctionList.Selected.Rows[0].GetCellValue("ID"));
                texJunctionElev.Text = Utils.nts(griJunctionList.Selected.Rows[0].GetCellValue("ELEV"));
                texJunctionDemand.Text = Utils.nts(griJunctionList.Selected.Rows[0].GetCellValue("DEMAND"));
                texJunctionPatternID.Text = Utils.nts(griJunctionList.Selected.Rows[0].GetCellValue("PATTERN_ID"));
                comJunctionSftridn.SelectedValue = Utils.nts(griJunctionList.Selected.Rows[0].GetCellValue("SFTRIDN"));
                texJunctionRemark.Text = Utils.nts(griJunctionList.Selected.Rows[0].GetCellValue("REMARK"));
            }
            else
            {
                texJunctionId.Text = "";
                texJunctionElev.Text = "";
                texJunctionDemand.Text = "";
                texJunctionPatternID.Text = "";
                comJunctionSftridn.SelectedValue = "";
                texJunctionRemark.Text = "";
            }
        }

        //Reservoir 리스트 그리드 행 선택이 바뀌었을 경우
        private void griReservoirList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griReservoirList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griReservoirList.DataSource).Rows[griReservoirList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texReservoirId.ReadOnly = true;
                }
                else
                {
                    texReservoirId.ReadOnly = false;
                }

                texReservoirId.Text = Utils.nts(griReservoirList.Selected.Rows[0].GetCellValue("ID"));
                texReservoirHead.Text = Utils.nts(griReservoirList.Selected.Rows[0].GetCellValue("HEAD"));
                texReservoirPatternId.Text = Utils.nts(griReservoirList.Selected.Rows[0].GetCellValue("PATTERN_ID"));
            }
            else
            {
                texReservoirId.Text = "";
                texReservoirHead.Text = "";
                texReservoirPatternId.Text = "";
            }
        }

        //Tank 리스트 그리드 행 선택이 바뀌었을 경우
        private void griTankList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griTankList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griTankList.DataSource).Rows[griTankList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texTankId.ReadOnly = true;
                }
                else
                {
                    texTankId.ReadOnly = false;
                }

                texTankId.Text = Utils.nts(griTankList.Selected.Rows[0].GetCellValue("ID"));
                texTankElev.Text = Utils.nts(griTankList.Selected.Rows[0].GetCellValue("ELEV"));
                texTankInitlvl.Text = Utils.nts(griTankList.Selected.Rows[0].GetCellValue("INITLVL"));
                texTankMinLvl.Text = Utils.nts(griTankList.Selected.Rows[0].GetCellValue("MINLVL"));
                texTankMaxlvl.Text = Utils.nts(griTankList.Selected.Rows[0].GetCellValue("MAXLVL"));
                texTankDiam.Text = Utils.nts(griTankList.Selected.Rows[0].GetCellValue("DIAM"));
                texTankMinvol.Text = Utils.nts(griTankList.Selected.Rows[0].GetCellValue("MINVOL"));
                texTankVolCurveId.Text = Utils.nts(griTankList.Selected.Rows[0].GetCellValue("VOLCURVE_ID"));
            }
            else
            {
                texTankId.Text = "";
                texTankElev.Text = "";
                texTankInitlvl.Text = "";
                texTankMinLvl.Text = "";
                texTankMaxlvl.Text = "";
                texTankDiam.Text = "";
                texTankMinvol.Text = "";
                texTankVolCurveId.Text = "";
            }
        }

        //Quality 리스트 그리드 행 선택이 바뀌었을 경우
        private void griQualityList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griQualityList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griQualityList.DataSource).Rows[griQualityList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texQualityId.ReadOnly = true;
                }
                else
                {
                    texQualityId.ReadOnly = false;
                }

                texQualityId.Text = Utils.nts(griQualityList.Selected.Rows[0].GetCellValue("ID"));
                texQualityInitqual.Text = Utils.nts(griQualityList.Selected.Rows[0].GetCellValue("INITQUAL"));
            }
            else
            {
                texQualityId.Text = "";
                texQualityInitqual.Text = "";
            }
        }

        //Source 리스트 그리드 행 선택이 바뀌었을 경우
        private void griSourceList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griSourceList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griSourceList.DataSource).Rows[griSourceList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texSourceId.ReadOnly = true;
                    texSourceType.ReadOnly = true;
                }
                else
                {
                    texSourceId.ReadOnly = false;
                    texSourceType.ReadOnly = false;
                }

                texQualityId.Text = Utils.nts(griQualityList.Selected.Rows[0].GetCellValue("ID"));
                texQualityInitqual.Text = Utils.nts(griQualityList.Selected.Rows[0].GetCellValue("INITQUAL"));

                texSourceId.Text = Utils.nts(griSourceList.Selected.Rows[0].GetCellValue("ID"));
                texSourceType.Text = Utils.nts(griSourceList.Selected.Rows[0].GetCellValue("TYPE"));
                texSourceStrength.Text = Utils.nts(griSourceList.Selected.Rows[0].GetCellValue("STRENGTH"));
                texSourcePatternId.Text = Utils.nts(griSourceList.Selected.Rows[0].GetCellValue("PATTERN_ID"));
            }
            else
            {
                texQualityId.Text = "";
                texQualityInitqual.Text = "";

                texSourceId.Text = "";
                texSourceType.Text = "";
                texSourceStrength.Text = "";
                texSourcePatternId.Text = "";
            }
        }

        //Emitter 리스트 그리드 행 선택이 바뀌었을 경우
        private void griEmitterList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griEmitterList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griEmitterList.DataSource).Rows[griEmitterList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texEmitterId.ReadOnly = true;
                }
                else
                {
                    texEmitterId.ReadOnly = false;
                }

                texEmitterId.Text = Utils.nts(griEmitterList.Selected.Rows[0].GetCellValue("ID"));
                texEmitterFlowCofficient.Text = Utils.nts(griEmitterList.Selected.Rows[0].GetCellValue("ID"));
            }
            else
            {
                texEmitterId.Text = "";
                texEmitterFlowCofficient.Text = "";
            }
        }

        //Label 리스트 그리드 행 선택이 바뀌었을 경우
        private void griLabelList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griLabelList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griLabelList.DataSource).Rows[griLabelList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texLabelIdx.ReadOnly = true;
                }
                else
                {
                    texLabelIdx.ReadOnly = false;
                }

                texLabelIdx.Text = Utils.nts(griLabelList.Selected.Rows[0].GetCellValue("IDX"));
                texLabelX.Text = Utils.nts(griLabelList.Selected.Rows[0].GetCellValue("X"));
                texLabelY.Text = Utils.nts(griLabelList.Selected.Rows[0].GetCellValue("Y"));
                texLabelRemark.Text = Utils.nts(griLabelList.Selected.Rows[0].GetCellValue("REMARK"));
            }
            else
            {
                texLabelIdx.Text = "";
                texLabelX.Text = "";
                texLabelY.Text = "";
                texLabelRemark.Text = "";
            }
        }

        //Demand 리스트 그리드 행 선택이 바뀌었을 경우
        private void griDemandList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griDemandList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griDemandList.DataSource).Rows[griDemandList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texDemandId.ReadOnly = true;
                    texDemandCategory.ReadOnly = true;

                }
                else
                {
                    texDemandId.ReadOnly = false;
                    texDemandCategory.ReadOnly = false;
                }

                texDemandId.Text = Utils.nts(griDemandList.Selected.Rows[0].GetCellValue("ID"));
                texDemandCategory.Text = Utils.nts(griDemandList.Selected.Rows[0].GetCellValue("CATEGORY"));
                texDemandDemand.Text = Utils.nts(griDemandList.Selected.Rows[0].GetCellValue("DEMAND"));
                texDemandPatternId.Text = Utils.nts(griDemandList.Selected.Rows[0].GetCellValue("PATTERN_ID"));
            }
            else
            {
                texDemandId.Text = "";
                texDemandCategory.Text = "";
                texDemandDemand.Text = "";
                texDemandPatternId.Text = "";
            }
        }

        //Mixing 리스트 그리드 행 선택이 바뀌었을 경우
        private void griMixingList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griMixingList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griMixingList.DataSource).Rows[griMixingList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texMixingId.ReadOnly = true;
                }
                else
                {
                    texMixingId.ReadOnly = false;
                }

                texMixingId.Text = Utils.nts(griMixingList.Selected.Rows[0].GetCellValue("ID"));
                texMixingModel.Text = Utils.nts(griMixingList.Selected.Rows[0].GetCellValue("MODEL"));
                texMixingFlaction.Text = Utils.nts(griMixingList.Selected.Rows[0].GetCellValue("FLACTION"));
            }
            else
            {
                texMixingId.Text = "";
                texMixingModel.Text = "";
                texMixingFlaction.Text = "";
            }
        }

        //Pipe 리스트 그리드 행 선택이 바뀌었을 경우
        private void griPipeList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griPipeList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griPipeList.DataSource).Rows[griPipeList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texPipeId.ReadOnly = true;
                }
                else
                {
                    texPipeId.ReadOnly = false;
                }

                texPipeId.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("ID"));
                texPipeNode1.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("NODE1"));
                texPipeNode2.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("NODE2"));
                texPipeLength.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("LENGTH"));
                texPipeDiam.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("DIAM"));
                texPipeLeakageCoefficient.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("LEAKAGE_COEFFICIENT"));
                texPipeRoughness.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("ROUGHNESS"));
                texPipeMloss.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("MLOSS"));
                texPipeStatus.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("STATUS"));
                texPipeRemark.Text = Utils.nts(griPipeList.Selected.Rows[0].GetCellValue("REMARK"));
            }
            else
            {
                texPipeId.Text = "";
                texPipeNode1.Text = "";
                texPipeNode2.Text = "";
                texPipeLength.Text = "";
                texPipeDiam.Text = "";
                texPipeLeakageCoefficient.Text = "";
                texPipeRoughness.Text = "";
                texPipeMloss.Text = "";
                texPipeStatus.Text = "";
                texPipeRemark.Text = "";
            }
        }

        //Coordiante 리스트 그리드 행 선택이 바뀌었을 경우
        private void griCoordinateList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griCoordinateList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griCoordinateList.DataSource).Rows[griCoordinateList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texCoordinateId.ReadOnly = true;
                }
                else
                {
                    texCoordinateId.ReadOnly = false;
                }

                texCoordinateId.Text = Utils.nts(griCoordinateList.Selected.Rows[0].GetCellValue("ID"));
                texCoordinateX.Text = Utils.nts(griCoordinateList.Selected.Rows[0].GetCellValue("X"));
                texCoordinateY.Text = Utils.nts(griCoordinateList.Selected.Rows[0].GetCellValue("Y"));
            }
            else
            {
                texCoordinateId.Text = "";
                texCoordinateX.Text = "";
                texCoordinateY.Text = "";
            }
        }

        //Vertices 리스트 그리드 행 선택이 바뀌었을 경우
        private void griVerticesList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griVerticesList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griVerticesList.DataSource).Rows[griVerticesList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texVerticesId.ReadOnly = true;
                    texVerticesIdx.ReadOnly = true;
                }
                else
                {
                    texVerticesId.ReadOnly = false;
                    texVerticesIdx.ReadOnly = false;
                }

                texVerticesId.Text = Utils.nts(griVerticesList.Selected.Rows[0].GetCellValue("ID"));
                texVerticesIdx.Text = Utils.nts(griVerticesList.Selected.Rows[0].GetCellValue("IDX"));
                texVerticesX.Text = Utils.nts(griVerticesList.Selected.Rows[0].GetCellValue("X"));
                texVerticesY.Text = Utils.nts(griVerticesList.Selected.Rows[0].GetCellValue("Y"));
            }
            else
            {
                texVerticesId.Text = "";
                texVerticesIdx.Text = "";
                texVerticesX.Text = "";
                texVerticesY.Text = "";
            }
        }

        //Pump 리스트 그리드 행 선택이 바뀌었을 경우
        private void griPumpList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griPumpList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griPumpList.DataSource).Rows[griPumpList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texPumpId.ReadOnly = true;
                }
                else
                {
                    texPumpId.ReadOnly = false;
                }

                texPumpId.Text = Utils.nts(griPumpList.Selected.Rows[0].GetCellValue("ID"));
                texPumpNode1.Text = Utils.nts(griPumpList.Selected.Rows[0].GetCellValue("NODE1"));
                texPumpNode2.Text = Utils.nts(griPumpList.Selected.Rows[0].GetCellValue("NODE2"));
                texPumpProperties.Text = Utils.nts(griPumpList.Selected.Rows[0].GetCellValue("PROPERTIES"));
            }
            else
            {
                texPumpId.Text ="";
                texPumpNode1.Text = "";
                texPumpNode2.Text = "";
                texPumpProperties.Text = "";
            }
        }

        //Valve 리스트 그리드 행 선택이 바뀌었을 경우
        private void griValveList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griValveList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griValveList.DataSource).Rows[griValveList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texValveId.ReadOnly = true;
                }
                else
                {
                    texValveId.ReadOnly = false;
                }

                texValveId.Text = Utils.nts(griValveList.Selected.Rows[0].GetCellValue("ID"));
                texValveNode1.Text = Utils.nts(griValveList.Selected.Rows[0].GetCellValue("NODE1"));
                texValveNode2.Text = Utils.nts(griValveList.Selected.Rows[0].GetCellValue("NODE2"));
                texValveDiameter.Text = Utils.nts(griValveList.Selected.Rows[0].GetCellValue("DIAMETER"));
                texValveType.Text = Utils.nts(griValveList.Selected.Rows[0].GetCellValue("TYPE"));
                texValveSetting.Text = Utils.nts(griValveList.Selected.Rows[0].GetCellValue("SETTING"));
                texValveMinorLoss.Text = Utils.nts(griValveList.Selected.Rows[0].GetCellValue("MINORLOSS"));
            }
            else
            {
                texValveId.Text = "";
                texValveNode1.Text = "";
                texValveNode2.Text = "";
                texValveDiameter.Text = "";
                texValveType.Text = "";
                texValveSetting.Text = "";
                texValveMinorLoss.Text = "";
            }
        }

        //Energy 리스트 그리드 행 선택이 바뀌었을 경우
        private void griEnergyList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griEnergyList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griEnergyList.DataSource).Rows[griEnergyList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texEnergyIdx.ReadOnly = true;
                }
                else
                {
                    texEnergyIdx.ReadOnly = false;
                }
                
                texEnergyIdx.Text = Utils.nts(griEnergyList.Selected.Rows[0].GetCellValue("IDX"));
                texEnergyEnergyStatement.Text = Utils.nts(griEnergyList.Selected.Rows[0].GetCellValue("ENERGY_STATEMENT"));
            }
        }

        //Curve 리스트 그리드 행 선택이 바뀌었을 경우
        private void griCurveList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griCurveList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griCurveList.DataSource).Rows[griCurveList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texCurveId.ReadOnly = true;
                    texCurveIdx.ReadOnly = true;
                }
                else
                {
                    texCurveId.ReadOnly = false;
                    texCurveIdx.ReadOnly = false;
                }

                texCurveId.Text = Utils.nts(griCurveList.Selected.Rows[0].GetCellValue("ID"));
                texCurveIdx.Text = Utils.nts(griCurveList.Selected.Rows[0].GetCellValue("IDX"));
                texCurveX.Text = Utils.nts(griCurveList.Selected.Rows[0].GetCellValue("X"));
                texCurveY.Text = Utils.nts(griCurveList.Selected.Rows[0].GetCellValue("Y"));
            }
            else
            {
                texCurveId.Text = "";
                texCurveIdx.Text = "";
                texCurveX.Text = "";
                texCurveY.Text = "";
            }
        }

        //Pattern 리스트 그리드 행 선택이 바뀌었을 경우
        private void griPatternList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griPatternList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griPatternList.DataSource).Rows[griPatternList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texPatternPatternId.ReadOnly = true;
                    texPatternIdx.ReadOnly = true;
                }
                else
                {
                    texPatternPatternId.ReadOnly = false;
                    texPatternIdx.ReadOnly = false;
                }

                texPatternPatternId.Text = Utils.nts(griPatternList.Selected.Rows[0].GetCellValue("PATTERN_ID"));
                texPatternIdx.Text = Utils.nts(griPatternList.Selected.Rows[0].GetCellValue("IDX"));
                texPatternMultiplier.Text = Utils.nts(griPatternList.Selected.Rows[0].GetCellValue("MULTIPLIER"));
            }
            else
            {
                texPatternPatternId.Text = "";
                texPatternIdx.Text = "";
                texPatternMultiplier.Text = "";
            }
        }

        //Status 리스트 그리드 행 선택이 바뀌었을 경우
        private void griStatusList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griStatusList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griStatusList.DataSource).Rows[griStatusList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texStatusId.ReadOnly = true;
                    texStatusIdx.ReadOnly = true;
                }
                else
                {
                    texStatusId.ReadOnly = false;
                    texStatusIdx.ReadOnly = false;
                }

                texStatusId.Text = Utils.nts(griStatusList.Selected.Rows[0].GetCellValue("ID"));
                texStatusIdx.Text = Utils.nts(griStatusList.Selected.Rows[0].GetCellValue("IDX"));
                texStatusStatusSetting.Text = Utils.nts(griStatusList.Selected.Rows[0].GetCellValue("STATUS_SETTING"));
            }
            else
            {
                texStatusId.Text = "";
                texStatusIdx.Text = "";
                texStatusStatusSetting.Text = "";
            }
        }

        //Control 리스트 그리드 행 선택이 바뀌었을 경우
        private void griControlList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griControlList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griControlList.DataSource).Rows[griControlList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texControlIdx.ReadOnly = true;
                }
                else
                {
                    texControlIdx.ReadOnly = false;
                }

                texControlIdx.Text = Utils.nts(griControlList.Selected.Rows[0].GetCellValue("IDX"));
                texControlControlsStatement.Text = Utils.nts(griControlList.Selected.Rows[0].GetCellValue("CONTROLS_STATEMENT"));
            }
            else
            {
                texControlIdx.Text = "";
                texControlControlsStatement.Text = "";
            }
        }

        //Rule 리스트 그리드 행 선택이 바뀌었을 경우
        private void griRuleList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griRuleList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griRuleList.DataSource).Rows[griRuleList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texRuleIdx.ReadOnly = true;
                }
                else
                {
                    texRuleIdx.ReadOnly = false;
                }

                texRuleIdx.Text = Utils.nts(griRuleList.Selected.Rows[0].GetCellValue("IDX"));
                texRuleRulesStatement.Text = Utils.nts(griRuleList.Selected.Rows[0].GetCellValue("RULES_STATEMENT"));
            }
            else
            {
                texRuleIdx.Text = "";
                texRuleRulesStatement.Text = "";
            }
        }

        //Reaction 리스트 그리드 행 선택이 바뀌었을 경우
        private void griReactionList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griReactionList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griReactionList.DataSource).Rows[griReactionList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texReactionIdx.ReadOnly = true;
                }
                else
                {
                    texReactionIdx.ReadOnly = false;
                }

                texReactionIdx.Text = Utils.nts(griReactionList.Selected.Rows[0].GetCellValue("IDX"));
                texReactionReactionStatement.Text = Utils.nts(griReactionList.Selected.Rows[0].GetCellValue("REACTION_STATEMENT"));
            }
        }

        //Option 리스트 그리드 행 선택이 바뀌었을 경우
        private void griOptionList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griOptionList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griOptionList.DataSource).Rows[griOptionList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texOptionIdx.ReadOnly = true;
                }
                else
                {
                    texOptionIdx.ReadOnly = false;
                }

                texOptionIdx.Text = Utils.nts(griOptionList.Selected.Rows[0].GetCellValue("IDX"));
                texOptionOptionsStatement.Text = Utils.nts(griOptionList.Selected.Rows[0].GetCellValue("OPTIONS_STATEMENT"));
            }
            else
            {
                texOptionIdx.Text = "";
                texOptionOptionsStatement.Text = "";
            }
        }

        //Time 리스트 그리드 행 선택이 바뀌었을 경우
        private void griTimeList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griTimeList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griTimeList.DataSource).Rows[griTimeList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texTimeIdx.ReadOnly = true;
                }
                else
                {
                    texTimeIdx.ReadOnly = false;
                }

                texTimeIdx.Text = Utils.nts(griTimeList.Selected.Rows[0].GetCellValue("IDX"));
                texTimeTimesStatement.Text = Utils.nts(griTimeList.Selected.Rows[0].GetCellValue("TIMES_STATEMENT"));
            }
            else
            {
                texTimeIdx.Text = "";
                texTimeTimesStatement.Text = "";
            }
        }

        //Report 리스트 그리드 행 선택이 바뀌었을 경우
        private void griRptOptionList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //Grid에서 Multi Select를 막은 관계로 인덱스는 최초행인 0임
            if (griRptOptionList.Selected.Rows.Count != 0)
            {
                DataRow row = ((DataRow)((DataTable)griRptOptionList.DataSource).Rows[griRptOptionList.Selected.Rows[0].Index]);

                //추가행을 제외하고 읽기전용 처리
                if (row.RowState != DataRowState.Added)
                {
                    texReportIdx.ReadOnly = true;
                }
                else
                {
                    texReportIdx.ReadOnly = false;
                }

                texReportIdx.Text = Utils.nts(griRptOptionList.Selected.Rows[0].GetCellValue("IDX"));
                texReportReportStatement.Text = Utils.nts(griRptOptionList.Selected.Rows[0].GetCellValue("REPORT_STATEMENT"));
            }
            else
            {
                texReportIdx.Text = "";
                texReportReportStatement.Text = "";
            }
        }

        //대블록 콤보박스가 변경된 경우
        private void comLftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comLftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if(dSet != null)
            {
                comMftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }

        //중블록 콤보박스가 변경된 경우
        private void comMftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comMftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comSftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }

        #endregion

        #region 기타
        //조회 종료 시 Grid 첫행에 focusing
        private void SetGridFirstFocusing(Control ctrs)
        {
            foreach (Control ctr in ctrs.Controls)
            {
                if (ctr.Controls.Count > 0)
                {
                    SetGridFirstFocusing(ctr);
                }

                if ("Infragistics.Win.UltraWinGrid.UltraGrid".Equals(ctr.GetType().ToString()))
                {
                    //그리드 초기화
                    if (((UltraGrid)ctr).Rows.Count > 0)
                    {
                        ((UltraGrid)ctr).Rows[0].Selected = true;
                    }
                }
            }
        }

        //INP 파일 조회
        public void SelectInpList()
        {
            //결과표출 컨트롤 초기화
            ClearData(tabDetailContents);
            ClearData(tabINPSection);

            //검색조건 할당
            Hashtable conditions = new Hashtable();

            if (!"".Equals(comUseGbn.SelectedValue))
            {
                conditions.Add("USE_GBN", comUseGbn.SelectedValue);
            }

            if (!"".Equals(comLftridn.SelectedValue))
            {
                conditions.Add("LFTRIDN", comLftridn.SelectedValue);
            }

            if (!"".Equals(comMftridn.SelectedValue))
            {
                conditions.Add("MFTRIDN", comMftridn.SelectedValue);
            }

            if (!"".Equals(comSftridn.SelectedValue))
            {
                conditions.Add("SFTRIDN", comSftridn.SelectedValue);
            }

            if (!"".Equals(texTitle.Text))
            {
                conditions.Add("TITLE", texTitle.Text);
            }

            //conditions.Add("startDate", datStartDate.DateTime.Year + Convert.ToString(datStartDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datStartDate.DateTime.Day).PadLeft(2, '0'));
            //conditions.Add("endDate", datEndDate.DateTime.Year + Convert.ToString(datEndDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datEndDate.DateTime.Day).PadLeft(2, '0'));

            try
            {
                DataTable dTable = work.SelectINPList(conditions).Tables["WH_TITLE"];

                griINPList.DataSource = dTable;

                FormManager.SetGridStyle_PerformAutoResize(griINPList);

                if (griINPList.Rows.Count > 0)
                {
                    //조회 종료 시 첫행에 focus를 주고 해당 내용을 채움
                    griINPList.Rows[0].Selected = true;
                }
                else
                {
                    //검색결과가 없다면 아래 grid를 초기화
                    ((DataTable)griJunctionList.DataSource).Clear() ;
                    ((DataTable)griReservoirList.DataSource).Clear();
                    ((DataTable)griTankList.DataSource).Clear();
                    ((DataTable)griQualityList.DataSource).Clear();
                    ((DataTable)griSourceList.DataSource).Clear();
                    ((DataTable)griEmitterList.DataSource).Clear();
                    ((DataTable)griLabelList.DataSource).Clear();
                    ((DataTable)griDemandList.DataSource).Clear();
                    ((DataTable)griMixingList.DataSource).Clear();
                    ((DataTable)griPipeList.DataSource).Clear();
                    ((DataTable)griCoordinateList.DataSource).Clear();
                    ((DataTable)griVerticesList.DataSource).Clear();
                    ((DataTable)griPumpList.DataSource).Clear();
                    ((DataTable)griValveList.DataSource).Clear();
                    ((DataTable)griEnergyList.DataSource).Clear();
                    ((DataTable)griCurveList.DataSource).Clear();
                    ((DataTable)griPatternList.DataSource).Clear();
                    ((DataTable)griStatusList.DataSource).Clear();
                    ((DataTable)griControlList.DataSource).Clear();
                    ((DataTable)griRuleList.DataSource).Clear();
                    ((DataTable)griReactionList.DataSource).Clear();
                    ((DataTable)griOptionList.DataSource).Clear();
                    ((DataTable)griTimeList.DataSource).Clear();
                    ((DataTable)griRptOptionList.DataSource).Clear();
                }
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.ToString());
                //Console.WriteLine(e1.StackTrace);
            }
        }

        //INP파일 내보내기
        private void butExpInpData_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (!"".Equals(texDetailINPNumber.Text))
                {
                    DialogResult result = MessageBox.Show("EPANET INP 파일을 내보내기 하시겠습니까?", "INP유형선택", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.No)
                    {
                        return;
                    }

                    Hashtable conditions = new Hashtable();

                    //if (result == DialogResult.Yes)
                    //{
                    //    //EPANET K용 INP파일
                    //    conditions.Add("type", "K");
                    //}
                    //else if (result == DialogResult.No)
                    //{
                    //    //일반 INP 파일
                    //    conditions.Add("type", "N");
                    //}
                    conditions.Add("type", string.Empty);

                    conditions.Add("INP_NUMBER", texDetailINPNumber.Text);

                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.Filter = "epanet file(*.inp)|*.inp";
                    saveFileDialog.ShowDialog();

                    if (!"".Equals(saveFileDialog.FileName))
                    {
                        conditions.Add("fileName", saveFileDialog.FileName);
                        work.GenerateINPFile(conditions);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //조회결과 표출 컨트롤 초기화
        private void ClearData(Control ctrs)
        {
            foreach (Control ctr in ctrs.Controls)
            {
                if (ctr.Controls.Count > 0)
                {
                    ClearData(ctr);
                }

                if ("System.Windows.Forms.TextBox".Equals(ctr.GetType().ToString()))
                {
                    //텍스트박스 초기화
                    ((TextBox)ctr).Text = "";
                }
                else if ("System.Windows.Forms.ComboBox".Equals(ctr.GetType().ToString()))
                {
                    //콤보박스 초기화
                    ((ComboBox)ctr).SelectedValue = "";
                }
                else if ("Infragistics.Win.UltraWinGrid.UltraGrid".Equals(ctr.GetType().ToString()))
                {
                    //그리드 초기화
                    if (((UltraGrid)ctr).Rows.Count > 0)
                    {
                        ((UltraGrid)ctr).Rows.Dispose();
                    }
                }
            }
        }
        #endregion
    }
}
