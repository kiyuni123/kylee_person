﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

using WaterNet.WaterNetCore;

using WaterNet.WH_Common.utils;
using WaterNet.WH_Common.work;

using WaterNet.WH_INPManage.work;
using EMFrame.log;

namespace WaterNet.WH_INPManage.form
{
    public partial class frmAnalysisJobExport : Form
    {
        private INPManagetWork work = null;
        private CommonWork cWork = null;
        //public frmAnalysisScheduleManage parentForm = null;

        public frmAnalysisJobExport()
        {
            InitializeComponent();
            InitializeSetting();
        }

        //화면초기화
        private void InitializeSetting()
        {
            work = INPManagetWork.GetInstance();
            cWork = CommonWork.GetInstance();

            //메인 그리드 초기화
            UltraGridColumn masterColumn;

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "INP_NUMBER";
            masterColumn.Header.Caption = "모델일련번호";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Center;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 180;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "TITLE";
            masterColumn.Header.Caption = "제목";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 250;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "USE_GBN";
            masterColumn.Header.Caption = "사용구분";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "USE_GBN_NAME";
            masterColumn.Header.Caption = "사용구분";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "LFTRIDN";
            masterColumn.Header.Caption = "대블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "LFTR_NAME";
            masterColumn.Header.Caption = "대블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "MFTRIDN";
            masterColumn.Header.Caption = "중블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "MFTR_NAME";
            masterColumn.Header.Caption = "중블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "SFTRIDN";
            masterColumn.Header.Caption = "소블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "SFTR_NAME";
            masterColumn.Header.Caption = "소블록";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 120;
            masterColumn.Hidden = false;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "INS_DATE";
            masterColumn.Header.Caption = "등록일자";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Center;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 180;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "FILE_NAME";
            masterColumn.Header.Caption = "파일명";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 200;
            masterColumn.Hidden = true;   //필드 보이기

            masterColumn = griINPList.DisplayLayout.Bands[0].Columns.Add();
            masterColumn.Key = "REMARK";
            masterColumn.Header.Caption = "비고";
            masterColumn.CellActivation = Activation.NoEdit;
            masterColumn.CellClickAction = CellClickAction.RowSelect;
            masterColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            masterColumn.CellAppearance.TextHAlign = HAlign.Left;
            masterColumn.CellAppearance.TextVAlign = VAlign.Middle;
            masterColumn.Width = 300;
            masterColumn.Hidden = true;   //필드 보이기

            //INP리스트 스타일 확정
            WaterNetCore.FormManager.SetGridStyle(griINPList);
        }

        private void frmAnalysisJobExport_Load(object sender, EventArgs e)
        {
            //검색일자 설정
            //datStartDate.Value = Utils.GetCalcTime("mm", -1)["yyyymmdd"];
            //datEndDate.Value = Utils.GetTime()["yyyymmdd"];

            //사용구분정보 할당(검색조건)
            comUseGbn.ValueMember = "CODE";
            comUseGbn.DisplayMember = "CODE_NAME";

            Hashtable conditions = new Hashtable();
            conditions.Add("PCODE", "1001");

            DataSet useGbnDataSet = cWork.GetCodeList(conditions);

            if (useGbnDataSet != null)
            {
                comUseGbn.DataSource = useGbnDataSet.Tables["CM_CODE"];
            }

            //블록정보 할당(검색조건)
            comLftridn.ValueMember = "LOC_CODE";
            comLftridn.DisplayMember = "LOC_NAME";
            comMftridn.ValueMember = "LOC_CODE";
            comMftridn.DisplayMember = "LOC_NAME";
            comSftridn.ValueMember = "LOC_CODE";
            comSftridn.DisplayMember = "LOC_NAME";

            DataSet blockDataSet = cWork.GetLblockDataList(new Hashtable());

            if (blockDataSet != null)
            {
                comLftridn.DataSource = blockDataSet.Tables["CM_LOCATION"];
            }

            //조회버튼에 최초 포커스
            butSelectINPList.Focus();
        }

        //------------------------------------- 이벤트 정의 시작 -------------------------------------

        //대블록 콤보박스가 변경된 경우
        private void comLftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comLftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comMftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }

        //중블록 콤보박스가 변경된 경우
        private void comMftridn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Hashtable conditions = new Hashtable();
            conditions.Add("PLOC_CODE", comMftridn.SelectedValue);

            DataSet dSet = cWork.GetBlockDataList(conditions);

            if (dSet != null)
            {
                comSftridn.DataSource = dSet.Tables["CM_LOCATION"];
            }
        }

        //조회버튼 클릭 시
        private void butSelectINPList_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //검색조건 할당
                Hashtable conditions = new Hashtable();

                if (!"".Equals(comUseGbn.SelectedValue))
                {
                    conditions.Add("USE_GBN", comUseGbn.SelectedValue);
                }

                if (!"".Equals(comLftridn.SelectedValue))
                {
                    conditions.Add("LFTRIDN", comLftridn.SelectedValue);
                }

                if (!"".Equals(comMftridn.SelectedValue))
                {
                    conditions.Add("MFTRIDN", comMftridn.SelectedValue);
                }

                if (!"".Equals(comSftridn.SelectedValue))
                {
                    conditions.Add("SFTRIDN", comSftridn.SelectedValue);
                }

                if (!"".Equals(texTitle.Text))
                {
                    conditions.Add("TITLE", texTitle.Text);
                }

                //conditions.Add("startDate", datStartDate.DateTime.Year + Convert.ToString(datStartDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datStartDate.DateTime.Day).PadLeft(2, '0'));
                //conditions.Add("endDate", datEndDate.DateTime.Year + Convert.ToString(datEndDate.DateTime.Month).PadLeft(2, '0') + Convert.ToString(datEndDate.DateTime.Day).PadLeft(2, '0'));

                try
                {
                    DataTable dTable = work.SelectExportINPList(conditions).Tables["WH_TITLE"];

                    griINPList.DataSource = dTable;

                    //조회 종료 시 첫행에 focus를 주고 해당 내용을 채움
                    if (griINPList.Rows.Count > 0)
                    {
                        griINPList.Rows[0].Selected = true;
                    }
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.ToString());
                    Console.WriteLine(e1.StackTrace);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //EPANET K로 INP파일을 전송한다.
        private void butExport_Click(object sender, EventArgs e)
        {
            if (griINPList.Selected.Rows.Count != 0)
            {
                string strFilePath = Application.StartupPath + @"\Epanet2w.exe";
                string inpNumber = griINPList.Selected.Rows[0].GetCellValue("INP_NUMBER").ToString();

                this.StartInstallationProcess(strFilePath, "EPANET", 0, inpNumber);
            }
        }

        /// <summary>
        /// 설치프로그램을 실행하고 설치프로그램이 종료될 때까지 Wait한다.
        /// </summary>
        /// <param name="strFilePath">설치프로그램의 Full Path</param>
        /// <param name="strFilePath">설치프로그램의 Name</param>
        /// <param name="iWinStyle">설치프로그램의 시작되는 스타일 0 : Normal, 1 : Hidden</param>
        private void StartInstallationProcess(string strFilePath, string strPGMName, int iWinStyle, string inpNumber)
        {
            if (File.Exists(strFilePath) != true)
            {
                MessageBox.Show(strPGMName + " 프로그램 파일이 없습니다.", "확인", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            ProcessStartInfo pStartInfo = new ProcessStartInfo();
            pStartInfo.FileName = strFilePath;

            string strINP_NO = inpNumber;
            string strINP_PATH = @"C:\Water-NET\TEMP\SIMULATE\" + strINP_NO + ".INP";

            if (FunctionManager.IsDirectoryExists(@"C:\Water-NET\TEMP\SIMULATE\") != true) FunctionManager.CreateDirectory(@"C:\Water-NET\TEMP\SIMULATE\");

            Hashtable conditions = new Hashtable();

            //EPANET K용 INP파일
            //conditions.Add("type", "K");
            conditions.Add("type", string.Empty);
            conditions.Add("INP_NUMBER", strINP_NO);
            conditions.Add("fileName", strINP_PATH);

            WH_INPManage.work.INPManagetWork work = WH_INPManage.work.INPManagetWork.GetInstance();
            work.GenerateINPFile(conditions);

            pStartInfo.Arguments = string.Format(" " + strINP_PATH + "{0}", "");

            Process Proc = new Process();

            Proc.StartInfo = pStartInfo;

            Proc.Start();

            Proc.Dispose();

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }
        //------------------------------------- 이벤트 정의 종료 -------------------------------------
    }
}
