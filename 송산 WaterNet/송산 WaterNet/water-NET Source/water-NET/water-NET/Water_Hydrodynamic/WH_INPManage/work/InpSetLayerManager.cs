﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Oracle.DataAccess.Client;

using WaterNet.WaterAOCore;
using WaterNet.WaterNetCore;

using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Display;

namespace WaterNet.WH_INPManage.work
{
    public class InpSetLayerManager
    {
        #region 멤버 변수 및 상수
        private string m_QuerySelectJUNCTION = string.Empty;
        private string m_QuerySelectRESERVOIR = string.Empty;
        private string m_QuerySelectTANK = string.Empty;
        private string m_QuerySelectPIPE = string.Empty;
        private string m_QuerySelectPUMP = string.Empty;
        private string m_QuerySelectVALVE = string.Empty;
        private string m_QuerySelectVertics = string.Empty;

        private IFeatureLayer m_JUNCTION = new FeatureLayerClass();
        private IFeatureLayer m_RESERVOIR = new FeatureLayerClass();
        private IFeatureLayer m_TANK = new FeatureLayerClass();
        private IFeatureLayer m_PIPE = new FeatureLayerClass();
        private IFeatureLayer m_PUMP = new FeatureLayerClass();
        private IFeatureLayer m_VALVE = new FeatureLayerClass();

        private IWorkspace m_Workspace = default(IWorkspace);
        private OracleDBManager m_oDBManager = null;
        private string m_INP_No = string.Empty;
        #endregion

        /// <summary>
        /// 기본생성자
        /// </summary>
        //public InpSetLayerManager()
        //{
        //    InitializeSetting();
        //}

        public InpSetLayerManager(OracleDBManager oDBManager, string strInpNo)
        {
            m_oDBManager = oDBManager;
            m_INP_No = strInpNo;
            InitializeSetting();

            string strWorkspace = VariableManager.m_INPgraphicRootDirectory + "\\" + strInpNo;
            m_Workspace = ArcManager.getShapeWorkspace(strWorkspace);
            if (m_Workspace != null)  return;

            m_Workspace = ArcManager.CreateShapeWorkspace(VariableManager.m_INPgraphicRootDirectory, strInpNo);
        }

        private void InitializeSetting()
        {
            StringBuilder oStringBuilder = new StringBuilder();
            #region SelectQuery String
            ///--------Junction
            oStringBuilder.Remove(0,oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    A.ID                 AS NODE_ID ");
            oStringBuilder.AppendLine("      ,   C.X                  AS X");
            oStringBuilder.AppendLine("      ,   C.Y                  AS Y");
            oStringBuilder.AppendLine("      ,   D.POSITION_INFO      AS POSITION_INFO");
            oStringBuilder.AppendLine("FROM    WH_JUNCTIONS           A");
            oStringBuilder.AppendLine("       ,WH_COORDINATES         C");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'NODE'                    ");
            oStringBuilder.AppendLine("         ) D                                       ");
            oStringBuilder.AppendLine("WHERE   A.ID    = C.ID ");
            oStringBuilder.AppendLine("  AND   A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   C.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   A.ID = D.ID(+) ");
            m_QuerySelectJUNCTION = oStringBuilder.ToString();

            ///--------RESERVOIR
            oStringBuilder.Remove(0,oStringBuilder.Length);
            
            oStringBuilder.AppendLine("SELECT    A.ID                 AS NODE_ID ");
            oStringBuilder.AppendLine("      ,   C.X                  AS X");
            oStringBuilder.AppendLine("      ,   C.Y                  AS Y");
            oStringBuilder.AppendLine("      ,   D.POSITION_INFO      AS POSITION_INFO");
            oStringBuilder.AppendLine("FROM    WH_RESERVOIRS           A");
            oStringBuilder.AppendLine("       ,WH_COORDINATES         C");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'NODE'                    ");
            oStringBuilder.AppendLine("         ) D                                       ");
            oStringBuilder.AppendLine("WHERE   A.ID    = C.ID ");
            oStringBuilder.AppendLine("  AND   A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   C.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   A.ID = D.ID(+) ");
            m_QuerySelectRESERVOIR = oStringBuilder.ToString();

            ///-------TANK
            oStringBuilder.Remove(0, oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT    A.ID                 AS NODE_ID ");
            oStringBuilder.AppendLine("      ,   C.X                  AS X");
            oStringBuilder.AppendLine("      ,   C.Y                  AS Y");
            oStringBuilder.AppendLine("      ,   D.POSITION_INFO      AS POSITION_INFO");
            oStringBuilder.AppendLine("FROM    WH_TANK           A");
            oStringBuilder.AppendLine("       ,WH_COORDINATES         C");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'NODE'                    ");
            oStringBuilder.AppendLine("         ) D                                       ");
            oStringBuilder.AppendLine("WHERE   A.ID    = C.ID ");
            oStringBuilder.AppendLine("  AND   A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   C.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("  AND   A.ID = D.ID(+) ");
            m_QuerySelectTANK = oStringBuilder.ToString();
        
            ///-------PIPE
            oStringBuilder.Remove(0,oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT   LINK_ID,");
            oStringBuilder.AppendLine("         ST_NODE,");
            oStringBuilder.AppendLine("         ED_NODE,");
            oStringBuilder.AppendLine("         POSITION_INFO");
            oStringBuilder.AppendLine("FROM");
            oStringBuilder.AppendLine("   (SELECT  A.ID          AS LINK_ID");
            oStringBuilder.AppendLine("          ,C.X      || ',' || C.Y    AS ST_NODE");
            oStringBuilder.AppendLine("          ,D.X      || ',' || D.Y    AS ED_NODE");
            oStringBuilder.AppendLine("          ,E.POSITION_INFO           AS POSITION_INFO");
            oStringBuilder.AppendLine("   FROM    WH_PIPES                A");
            oStringBuilder.AppendLine("          ,WH_COORDINATES         C");
            oStringBuilder.AppendLine("          ,WH_COORDINATES         D");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'LINK'                    ");
            oStringBuilder.AppendLine("         ) E                                       ");
            oStringBuilder.AppendLine("   WHERE  A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.NODE1 = C.ID AND C.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.NODE2 = D.ID AND D.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.ID = E.ID(+))"); 

            m_QuerySelectPIPE = oStringBuilder.ToString();

            ///-------PUMP
            oStringBuilder.Remove(0,oStringBuilder.Length);
           
            oStringBuilder.AppendLine("SELECT   LINK_ID,");
            oStringBuilder.AppendLine("         ST_NODE,");
            oStringBuilder.AppendLine("         ED_NODE,");
            oStringBuilder.AppendLine("         POSITION_INFO");
            oStringBuilder.AppendLine("FROM");
            oStringBuilder.AppendLine("   (SELECT  A.ID          AS LINK_ID");
            oStringBuilder.AppendLine("          ,C.X      || ',' || C.Y    AS ST_NODE");
            oStringBuilder.AppendLine("          ,D.X      || ',' || D.Y    AS ED_NODE");
            oStringBuilder.AppendLine("          ,E.POSITION_INFO           AS POSITION_INFO");
            oStringBuilder.AppendLine("   FROM    WH_PUMPS                A");
            oStringBuilder.AppendLine("          ,WH_COORDINATES         C");
            oStringBuilder.AppendLine("          ,WH_COORDINATES         D");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'LINK'                    ");
            oStringBuilder.AppendLine("         ) E                                       ");
            oStringBuilder.AppendLine("   WHERE  A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.NODE1 = C.ID AND C.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.NODE2 = D.ID AND D.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.ID = E.ID(+))"); 
            m_QuerySelectPUMP = oStringBuilder.ToString();

            ///-------VALVE
            oStringBuilder.Remove(0,oStringBuilder.Length);
       
            oStringBuilder.AppendLine("SELECT   LINK_ID,");
            oStringBuilder.AppendLine("         ST_NODE,");
            oStringBuilder.AppendLine("         ED_NODE,");
            oStringBuilder.AppendLine("         POSITION_INFO");
            oStringBuilder.AppendLine("FROM");
            oStringBuilder.AppendLine("   (SELECT  A.ID          AS LINK_ID");
            oStringBuilder.AppendLine("          ,C.X      || ',' || C.Y    AS ST_NODE");
            oStringBuilder.AppendLine("          ,D.X      || ',' || D.Y    AS ED_NODE");
            oStringBuilder.AppendLine("          ,E.POSITION_INFO           AS POSITION_INFO");
            oStringBuilder.AppendLine("   FROM    WH_VALVES                A");
            oStringBuilder.AppendLine("          ,WH_COORDINATES         C");
            oStringBuilder.AppendLine("          ,WH_COORDINATES         D");
            oStringBuilder.AppendLine("       ,(                                          ");
            oStringBuilder.AppendLine("         SELECT   ID, POSITION_INFO                ");
            oStringBuilder.AppendLine("           FROM   WH_TAGS                          ");
            oStringBuilder.AppendLine("          WHERE   INP_NUMBER = '" + m_INP_No + "'  ");
            oStringBuilder.AppendLine("            AND   TYPE = 'LINK'                    ");
            oStringBuilder.AppendLine("         ) E                                       ");
            oStringBuilder.AppendLine("   WHERE  A.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.NODE1 = C.ID AND C.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.NODE2 = D.ID AND D.INP_NUMBER = '" + m_INP_No + "'");
            oStringBuilder.AppendLine("     AND     A.ID = E.ID(+))"); 
            m_QuerySelectVALVE = oStringBuilder.ToString();
        
            ///-------Vertics
            oStringBuilder.Remove(0,oStringBuilder.Length);
            oStringBuilder.AppendLine("SELECT  ID, IDX, X, Y");
            oStringBuilder.AppendLine("FROM    WH_VERTICES");
            oStringBuilder.AppendLine("WHERE INP_NUMBER = '" + m_INP_No + "'");
            m_QuerySelectVertics = oStringBuilder.ToString();
            #endregion

        }

        /// <summary>
        /// Workspace
        /// </summary>
        public IWorkspace Worksapce
        {
            get
            {
                return this.m_Workspace;
            }
            set
            {
                this.m_Workspace = value;
            }
        }

        /// <summary>
        /// Shape 생성
        /// </summary>
        public void CreateINP_Shape()
        {
            CreateINP_Shape(FunctionManager.INPShapeType.JUNCTIONS);
            CreateINP_Shape(FunctionManager.INPShapeType.PIPES);
            CreateINP_Shape(FunctionManager.INPShapeType.PUMPS);
            CreateINP_Shape(FunctionManager.INPShapeType.RESERVOIRS);
            CreateINP_Shape(FunctionManager.INPShapeType.TANKS);
            CreateINP_Shape(FunctionManager.INPShapeType.VALVES);

            CreateINP_Point_Feature(m_JUNCTION);
            CreateINP_Line_Feature(m_PIPE);
            CreateINP_Line_Feature(m_PUMP);
            CreateINP_Point_Feature(m_RESERVOIR);
            CreateINP_Point_Feature(m_TANK);
            CreateINP_Line_Feature(m_VALVE);
        }

        private void CreateINP_Line_Feature(IFeatureLayer pFeatureLayer)
        {

            OracleDataReader oReader = null;
            try
            {
                IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;
                if (pFeatureClass.AliasName.ToUpper() == "PIPE")
                {
                    if (string.IsNullOrEmpty(m_QuerySelectPIPE)) return;
                    Clipboard.SetText(m_QuerySelectPIPE);
                    oReader = m_oDBManager.ExecuteScriptDataReader(m_QuerySelectPIPE, null);
                }
                else if (pFeatureClass.AliasName.ToUpper() == "PUMP")
                {
                    if (string.IsNullOrEmpty(m_QuerySelectPUMP)) return;
                    Clipboard.SetText(m_QuerySelectPUMP);
                    oReader = m_oDBManager.ExecuteScriptDataReader(m_QuerySelectPUMP, null);
                }
                else if (pFeatureClass.AliasName.ToUpper() == "VALVE")
                {
                    if (string.IsNullOrEmpty(m_QuerySelectVALVE)) return;
                    Clipboard.SetText(m_QuerySelectVALVE);
                    oReader = m_oDBManager.ExecuteScriptDataReader(m_QuerySelectVALVE, null);
                }
                if (oReader == null) return;
                if (!oReader.HasRows) return;

                //객체 추가      //--PointCollection 생성
                IPointCollection pPointCollection = default(IPointCollection);
                try
                {
                    while (oReader.Read())
                    {
                        string strvertics = getVerticsPositions(oReader["LINK_ID"].ToString());
                        string strStNode = oReader["ST_NODE"].ToString();
                        string strEdNode = oReader["ED_NODE"].ToString();
                        string strPositions = string.Empty;
                        if (strvertics.Length < 1)
                        {
                            strPositions = strStNode + "|" + strEdNode;
                        }
                        else
                        {
                            strPositions = strStNode + "|" + strvertics + "|" + strEdNode;
                        }
                        //Console.WriteLine(oReader["LINK_ID"].ToString() + " => " + strPositions);
                        pPointCollection = ArcManager.getPointCollection(strPositions);

                        IGeometry pGeom = pPointCollection as IGeometry;
                        if (pGeom.IsEmpty) continue;

                        IFeature pFeature = pFeatureClass.CreateFeature();
                        ArcManager.SetValue(pFeature, "ID", oReader["LINK_ID"].ToString());
                        ArcManager.SetValue(pFeature, "POSITION_INFO", oReader["POSITION_INFO"].ToString());

                        pFeature.Shape = pGeom;
                        pFeature.Store();
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                    Console.WriteLine(ex.Message);
                }
            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.StackTrace); //oException.InnerException.Message);
            }
            finally
            {
                if (oReader != null) oReader.Close();
            }
        }

        private string getVerticsPositions(string sID)
        {
            OracleDataReader oReader = null;
            string sWhere = " AND ID = '" + sID + "'";
            string sOrder = " ORDER BY  TO_NUMBER(IDX)";
            StringBuilder oStringBuilder = new StringBuilder();
            try
            {
                //Clipboard.SetText(m_QuerySelectVertics + sWhere);
                oReader = m_oDBManager.ExecuteScriptDataReader(m_QuerySelectVertics + sWhere + sOrder, null);

                if (oReader == null) return string.Empty;
                if (!oReader.HasRows) return string.Empty;

                while (oReader.Read())
                {
                    oStringBuilder.Append(oReader["X"].ToString() + "," + oReader["Y"].ToString() + "|");
                }
                oStringBuilder.Remove(oStringBuilder.Length - 1, 1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oReader != null) oReader.Close();
            }

            return oStringBuilder.ToString();
        }

        private void CreateINP_Point_Feature(IFeatureLayer pFeatureLayer)
        {
            OracleDataReader oReader = null;
            try
            {
                IFeatureClass pFeatureClass = pFeatureLayer.FeatureClass;
                if (pFeatureClass.AliasName.ToUpper() == "JUNCTION")
                {
                    if (string.IsNullOrEmpty(m_QuerySelectJUNCTION)) return;
                    oReader = m_oDBManager.ExecuteScriptDataReader(m_QuerySelectJUNCTION, null);
                }
                else if (pFeatureClass.AliasName.ToUpper() == "RESERVOIR")
                {
                    if (string.IsNullOrEmpty(m_QuerySelectRESERVOIR)) return;
                    oReader = m_oDBManager.ExecuteScriptDataReader(m_QuerySelectRESERVOIR, null);
                }
                else if (pFeatureClass.AliasName.ToUpper() == "TANK")
                {
                    if (string.IsNullOrEmpty(m_QuerySelectTANK)) return;
                    oReader = m_oDBManager.ExecuteScriptDataReader(m_QuerySelectTANK, null);
                }

                if (oReader == null) return;
                if (!oReader.HasRows) return;

                try
                {
                    while (oReader.Read())
                    {
                        IPoint point = new PointClass();
                        point.PutCoords(Convert.ToDouble(oReader["X"]), Convert.ToDouble(oReader["Y"]));

                        IGeometry pGeom = point as IGeometry;
                        if (pGeom.IsEmpty) continue;

                        IFeature pFeature = pFeatureClass.CreateFeature();
                        ArcManager.SetValue(pFeature, "ID", oReader["NODE_ID"].ToString());
                        ArcManager.SetValue(pFeature, "POSITION_INFO", oReader["POSITION_INFO"].ToString());
                        pFeature.Shape = pGeom;
                        pFeature.Store();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    Console.WriteLine(ex.Message);
                }

            }
            catch (Exception oException)
            {
                throw new ExceptionManager(this.GetType().Namespace, this.GetType().Name, oException.Source, oException.Message, oException.StackTrace); //oException.InnerException.Message);
            }
            finally
            {
                if (oReader != null) oReader.Close();
            }
        }

        private void CreateINP_Shape(FunctionManager.INPShapeType ShapeType)
        {
            IFeatureClass pFeatureClass = null;
            switch (ShapeType)
            {
                case FunctionManager.INPShapeType.JUNCTIONS:
                    pFeatureClass = CreateShape_Point("JUNCTION");
                    if (pFeatureClass != null)  m_JUNCTION.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.RESERVOIRS:
                    pFeatureClass = CreateShape_Point("RESERVOIR");
                    if (pFeatureClass != null) m_RESERVOIR.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.TANKS:
                    pFeatureClass = CreateShape_Point("TANK");
                    if (pFeatureClass != null) m_TANK.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.PIPES:
                    pFeatureClass = CreateShape_Line("PIPE");
                    if (pFeatureClass != null) m_PIPE.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.PUMPS:
                    pFeatureClass = CreateShape_Line("PUMP");
                    if (pFeatureClass != null) m_PUMP.FeatureClass = pFeatureClass;
                    break;
                case FunctionManager.INPShapeType.VALVES:
                    pFeatureClass = CreateShape_Line("VALVE");
                    if (pFeatureClass != null) m_VALVE.FeatureClass = pFeatureClass;
                    break;
            }
            
        }

        private IFeatureClass CreateShape_Point(string sName)
        {
            IFeatureClass pFeatureClass = null;

            //Shape Field 구성
            WaterNet.WaterAOCore.VariableManager.FieldStruct[] structFields = new WaterNet.WaterAOCore.VariableManager.FieldStruct[26];
            structFields[0].sFieldName = "ID";
            structFields[1].sFieldName = "EN_ELEVATION";
            structFields[2].sFieldName = "EN_BASEDEMAND";
            structFields[3].sFieldName = "EN_PATTERN";
            structFields[4].sFieldName = "EN_EMITTER";
            structFields[5].sFieldName = "EN_INITQUAL";
            structFields[6].sFieldName = "EN_SOURCEQUAL";
            structFields[7].sFieldName = "EN_SOURCEPAT";
            structFields[8].sFieldName = "EN_SOURCETYPE";
            structFields[9].sFieldName = "EN_TANKLEVEL";
            structFields[10].sFieldName = "EN_DEMAND";
            structFields[11].sFieldName = "EN_HEAD";
            structFields[12].sFieldName = "EN_PRESSURE";
            structFields[13].sFieldName = "EN_QUALITY";
            structFields[14].sFieldName = "EN_SOURCEMASS";
            structFields[15].sFieldName = "EN_INITVOLUME";
            structFields[16].sFieldName = "EN_MIXMODEL";
            structFields[17].sFieldName = "EN_MIXZONEVOL";
            structFields[18].sFieldName = "EN_TANKDIAM";
            structFields[19].sFieldName = "EN_MINVOLUME";
            structFields[20].sFieldName = "EN_VOLCURV";
            structFields[21].sFieldName = "EN_MINLEVEL";
            structFields[22].sFieldName = "EN_MAXLEVEL";
            structFields[23].sFieldName = "EN_MIXFRACTION";
            structFields[24].sFieldName = "EN_TANK_KBULK";
            structFields[25].sFieldName = "POSITION_INFO";
            //------------------------------------------------
            structFields[0].sAliasName = "ID";
            structFields[1].sAliasName = "EN_ELEVATION";
            structFields[2].sAliasName = "EN_BASEDEMAND";
            structFields[3].sAliasName = "EN_PATTERN";
            structFields[4].sAliasName = "EN_EMITTER";
            structFields[5].sAliasName = "EN_INITQUAL";
            structFields[6].sAliasName = "EN_SOURCEQUAL";
            structFields[7].sAliasName = "EN_SOURCEPAT";
            structFields[8].sAliasName = "EN_SOURCETYPE";
            structFields[9].sAliasName = "EN_TANKLEVEL";
            structFields[10].sAliasName = "EN_DEMAND";
            structFields[11].sAliasName = "EN_HEAD";
            structFields[12].sAliasName = "EN_PRESSURE";
            structFields[13].sAliasName = "EN_QUALITY";
            structFields[14].sAliasName = "EN_SOURCEMASS";
            structFields[15].sAliasName = "EN_INITVOLUME";
            structFields[16].sAliasName = "EN_MIXMODEL";
            structFields[17].sAliasName = "EN_MIXZONEVOL";
            structFields[18].sAliasName = "EN_TANKDIAM";
            structFields[19].sAliasName = "EN_MINVOLUME";
            structFields[20].sAliasName = "EN_VOLCURV";
            structFields[21].sAliasName = "EN_MINLEVEL";
            structFields[22].sAliasName = "EN_MAXLEVEL";
            structFields[23].sAliasName = "EN_MIXFRACTION";
            structFields[24].sAliasName = "EN_TANK_KBULK";
            structFields[25].sAliasName = "POSITION_INFO";
            //-------------------------------------------------------
            structFields[0].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;
            structFields[1].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[2].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[3].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[4].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[5].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[6].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[7].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[8].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[9].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[10].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[11].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[12].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[13].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[14].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[15].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[16].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[17].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[18].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[19].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[20].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[21].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[22].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[23].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[24].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[25].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;

            //---------------------------------------------
            structFields[0].sFieldLength = 100;
            structFields[1].sFieldLength = 32;
            structFields[1].sScale = 2;
            structFields[2].sFieldLength = 32;
            structFields[2].sScale = 2;
            structFields[3].sFieldLength = 32;
            structFields[3].sScale = 2;
            structFields[4].sFieldLength = 32;
            structFields[4].sScale = 2;
            structFields[5].sFieldLength = 32;
            structFields[5].sScale = 2;
            structFields[6].sFieldLength = 32;
            structFields[6].sScale = 2;
            structFields[7].sFieldLength = 32;
            structFields[7].sScale = 2;
            structFields[8].sFieldLength = 32;
            structFields[8].sScale = 2;
            structFields[9].sFieldLength = 32;
            structFields[9].sScale = 2;
            structFields[10].sFieldLength = 32;
            structFields[10].sScale = 2;
            structFields[11].sFieldLength = 32;
            structFields[11].sScale = 2;
            structFields[12].sFieldLength = 32;
            structFields[12].sScale = 2;
            structFields[13].sFieldLength = 32;
            structFields[13].sScale = 2;
            structFields[14].sFieldLength = 32;
            structFields[14].sScale = 2;
            structFields[15].sFieldLength = 32;
            structFields[15].sScale = 2;
            structFields[16].sFieldLength = 32;
            structFields[16].sScale = 2;
            structFields[17].sFieldLength = 32;
            structFields[17].sScale = 2;
            structFields[18].sFieldLength = 32;
            structFields[18].sScale = 2;
            structFields[19].sFieldLength = 32;
            structFields[19].sScale = 2;
            structFields[20].sFieldLength = 32;
            structFields[20].sScale = 2;
            structFields[21].sFieldLength = 32;
            structFields[21].sScale = 2;
            structFields[22].sFieldLength = 32;
            structFields[22].sScale = 2;
            structFields[23].sFieldLength = 32;
            structFields[23].sScale = 2;
            structFields[24].sFieldLength = 32;
            structFields[24].sScale = 2;
            structFields[25].sFieldLength = 500;
            //--------------------------------------------------


            IFields pFields = WaterNet.WaterAOCore.ArcManager.CreateFields(WaterNet.WaterAOCore.VariableManager.ShapeType.ShapePoint, structFields);

            //좌표계 구성 --------axMap.spatialReference
            ISpatialReference spatialReference = WaterNet.WaterAOCore.ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");

            //Shape FeatreClass생성
            //if (WaterNet.WaterAOCore.ArcManager.getShapeFeatureClass(pWorkspace, sName) != null)
            //{
            //    MessageManager.ShowInformationMessage(pWorkspace.PathName + " 경로에 이미 [" + sName + "] 레이어가 있습니다.");
            //    return null;
            //}

            pFeatureClass = WaterNet.WaterAOCore.ArcManager.CreateFeatureClass(m_Workspace, sName, spatialReference, esriFeatureType.esriFTSimple,
                                                                                                     esriGeometryType.esriGeometryPoint, pFields, null, null, "");

            return pFeatureClass;
        }

        private IFeatureClass CreateShape_Line(string sName)
        {
            IFeatureClass pFeatureClass = null;

            //Shape Field 구성
            WaterNet.WaterAOCore.VariableManager.FieldStruct[] structFields = new WaterNet.WaterAOCore.VariableManager.FieldStruct[16];
            structFields[0].sFieldName = "ID";
            structFields[1].sFieldName =  "EN_DIAMETER";
            structFields[2].sFieldName =  "EN_LENGTH";
            structFields[3].sFieldName =  "EN_ROUGHNESS";
            structFields[4].sFieldName =  "EN_MINORLOSS";
            structFields[5].sFieldName =  "EN_INITSTATUS";
            structFields[6].sFieldName =  "EN_INITSETTING";
            structFields[7].sFieldName =  "EN_KBULK";
            structFields[8].sFieldName =  "EN_KWALL";
            structFields[9].sFieldName =  "EN_FLOW";
            structFields[10].sFieldName = "EN_VELOCITY";
            structFields[11].sFieldName = "EN_HEADLOSS";
            structFields[12].sFieldName = "EN_STATUS";
            structFields[13].sFieldName = "EN_SETTING";
            structFields[14].sFieldName = "EN_ENERGY";
            structFields[15].sFieldName = "POSITION_INFO";
            //------------------------------------------------
            structFields[0].sAliasName = "ID";
            structFields[1].sAliasName =  "EN_DIAMETER";
            structFields[2].sAliasName =  "EN_LENGTH";
            structFields[3].sAliasName =  "EN_ROUGHNESS";
            structFields[4].sAliasName =  "EN_MINORLOSS";
            structFields[5].sAliasName =  "EN_INITSTATUS";
            structFields[6].sAliasName =  "EN_INITSETTING";
            structFields[7].sAliasName =  "EN_KBULK";
            structFields[8].sAliasName =  "EN_KWALL";
            structFields[9].sAliasName =  "EN_FLOW";
            structFields[10].sAliasName = "EN_VELOCITY";
            structFields[11].sAliasName = "EN_HEADLOSS";
            structFields[12].sAliasName = "EN_STATUS";
            structFields[13].sAliasName = "EN_SETTING";
            structFields[14].sAliasName = "EN_ENERGY";
            structFields[15].sAliasName = "POSITION_INFO";

            //-------------------------------------------------------
            structFields[0].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;
            structFields[1].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[2].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[3].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[4].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[5].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[6].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[7].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[8].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[9].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[10].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[11].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[12].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[13].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[14].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.DoubleField;
            structFields[15].sFieldType = WaterNet.WaterAOCore.VariableManager.FieldType.StringField;

            //---------------------------------------------
            structFields[0].sFieldLength = 100;
            structFields[1].sFieldLength = 32;
            structFields[1].sScale = 2;
            structFields[2].sFieldLength = 32;
            structFields[2].sScale = 2;
            structFields[3].sFieldLength = 32;
            structFields[3].sScale = 2;
            structFields[4].sFieldLength = 32;
            structFields[4].sScale = 2;
            structFields[5].sFieldLength = 32;
            structFields[5].sScale = 2;
            structFields[6].sFieldLength = 32;
            structFields[6].sScale = 2;
            structFields[7].sFieldLength = 32;
            structFields[7].sScale = 2;
            structFields[8].sFieldLength = 32;
            structFields[8].sScale = 2;
            structFields[9].sFieldLength = 32;
            structFields[9].sScale = 2;
            structFields[10].sFieldLength = 32;
            structFields[10].sScale = 2;
            structFields[11].sFieldLength = 32;
            structFields[11].sScale = 2;
            structFields[12].sFieldLength = 32;
            structFields[12].sScale = 2;
            structFields[13].sFieldLength = 32;
            structFields[13].sScale = 2;
            structFields[14].sFieldLength = 32;
            structFields[14].sScale = 2;
            structFields[15].sFieldLength = 500;
            //--------------------------------------------------

            IFields pFields = WaterNet.WaterAOCore.ArcManager.CreateFields(WaterNet.WaterAOCore.VariableManager.ShapeType.ShapePolyline, structFields);

            //좌표계 구성 --------axMap.spatialReference
            ISpatialReference spatialReference = WaterNet.WaterAOCore.ArcManager.MakeSpatialReference("PCS_ITRF2000_TM.prj");

            //Shape FeatreClass생성
            //if (WaterNet.WaterAOCore.ArcManager.getShapeFeatureClass(pWorkspace, sName) != null)
            //{
            //    MessageManager.ShowInformationMessage(pWorkspace.PathName + " 경로에 이미 [" + sName + "] 레이어가 있습니다.");
            //    return null;
            //}

            pFeatureClass = WaterNet.WaterAOCore.ArcManager.CreateFeatureClass(m_Workspace, sName, spatialReference, esriFeatureType.esriFTSimple, esriGeometryType.esriGeometryPolyline, pFields, null, null, "");

            return pFeatureClass;
        }

        /// <summary>
        /// ArcEditor StartEdit
        /// </summary>
        /// <param name="pWorkSpace"></param>
        /// <remarks></remarks>
        public bool StartEditor()
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)m_Workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.StartEditOperation();
                return true;
            }
            else
            {
                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();
                return false;
            }

        }

        /// <summary>
        /// ArcEditor StopEdit
        /// </summary>
        /// <param name="pWorkSpace"></param>
        /// <param name="pSave"></param>
        /// <remarks></remarks>
        public void StopEditor(bool pSave)
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)m_Workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(pSave);
            }

        }

        /// <summary>
        /// ArcEditor AbortEditor
        /// </summary>
        public void AbortEditor()
        {
            IWorkspaceEdit pWorkspaceEdit = (IWorkspaceEdit)m_Workspace;

            if (pWorkspaceEdit.IsBeingEdited())
            {
                pWorkspaceEdit.AbortEditOperation();
            }
        }
    }
}
