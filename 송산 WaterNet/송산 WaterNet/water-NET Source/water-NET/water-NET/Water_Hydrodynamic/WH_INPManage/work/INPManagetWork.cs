﻿/**************************************************************************
 * 파일명   : WHINPManageDao.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.09.29
 * 설명     : INP파일 내용을 DB에 저장하기 위한 Data Access Object
 * 변경이력 : 2010.09.29 - 최초생성
 **************************************************************************/
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WaterNet.WaterNetCore;
using WaterNet.WH_INPManage.dao;
using WaterNet.WH_AnalysisScheduleManage.dao;

using WaterNet.WaterAOCore;

using System.Data;

namespace WaterNet.WH_INPManage.work
{
    public class INPManagetWork
    {
        private static INPManagetWork work = null;
        private INPManageDao dao = null;

        private AnalysisScheduleManageDao aDao = null;

        private INPManagetWork()
        {
            dao = INPManageDao.GetInstance();
            aDao = AnalysisScheduleManageDao.GetInstance();
        }

        //인스턴스 생성
        public static INPManagetWork GetInstance()
        {
            if (work == null)
            {
                work = new INPManagetWork();
            }

            return work;
        }

        //INP파일정보 등록
        public void RegistINPData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            InpSetLayerManager oInpSetLayer = null;
            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                dbManager.BeginTransaction();

                //수리/수질인 경우 해당 블록에 이미 모델이 등록되었는지 확인
                //if ("WH".Equals(conditions["USE_GBN"].ToString()) || "WQ".Equals(conditions["USE_GBN"].ToString()))
                if ("WH".Equals(conditions["USE_GBN"].ToString()))
                {
                    DataSet existModelDataSet = dao.IsExistINPData(dbManager, conditions);

                    if (existModelDataSet!=null && existModelDataSet.Tables["WH_TITLE"].Rows.Count != 0)
                    {
                        throw new Exception("이미 해당지역에 모델이 등록되어 있습니다.");
                    }
                }

                #region 마스터정보 등록
                dao.InsertINPMasterData(dbManager, conditions);
                Console.WriteLine("MASTER 정보 입력 완료...");

                Hashtable sectionData = (Hashtable)conditions["sectionData"];
                #endregion

                #region INP 섹션별 데이터 등록
                if (sectionData["junction"] != null)
                {
                    //Junction정보 등록
                    ArrayList junctionDataList = (ArrayList)sectionData["junction"];

                    for (int i = 0; i < junctionDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList junctionData = (ArrayList)junctionDataList[i];

                        junctionData.Insert(0, (string)conditions["INP_NUMBER"]);               //INP파일 번호 할당

                        dao.InsertJunctionData(dbManager, junctionData);
                    }

                    Console.WriteLine("JUNCTION 정보 입력 완료...");
                }

                if (sectionData["reservoir"] != null)
                {
                    //Reservior정보 등록
                    ArrayList reservoirDataList = (ArrayList)sectionData["reservoir"];

                    for (int i = 0; i < reservoirDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        if(i == 3)
                        {

                        }
                        
                        ArrayList reservoirData = (ArrayList)reservoirDataList[i];

                        reservoirData.Insert(0, (string)conditions["INP_NUMBER"]);               //INP파일 번호 할당

                        dao.InsertReservoirData(dbManager, reservoirData);
                    }

                    Console.WriteLine("RESERVOIR 정보 입력 완료...");
                }

                if (sectionData["tank"] != null)
                {
                    //Tank정보 등록
                    ArrayList tankDataList = (ArrayList)sectionData["tank"];

                    for (int i = 0; i < tankDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList tankData = (ArrayList)tankDataList[i];

                        tankData.Insert(0, (string)conditions["INP_NUMBER"]);                       //INP파일 번호 할당

                        dao.InsertTankData(dbManager, tankData);
                    }

                    Console.WriteLine("TANK 정보 입력 완료...");
                }

                if (sectionData["pipe"] != null)
                {
                    //Pipe정보 등록
                    ArrayList pipeDataList = (ArrayList)sectionData["pipe"];

                    for (int i = 0; i < pipeDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList pipeData = (ArrayList)pipeDataList[i];

                        if ("".Equals(pipeData[8].ToString())) pipeData.Insert(5, "0");                 //누수계수가 없는경우 "0"할당 2014.10.14 KHB
                        pipeData.Insert(0, (string)conditions["INP_NUMBER"]);                           //INP파일 번호 할당

                        dao.InsertPipeData(dbManager, pipeData, i.ToString());
                    }
                    
                    Console.WriteLine("PIPE 정보 입력 완료...");
                }

                if (sectionData["pump"] != null)
                {
                    //Pump정보 등록
                    ArrayList pumpDataList = (ArrayList)sectionData["pump"];

                    for (int i = 0; i < pumpDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList pumpData = (ArrayList)pumpDataList[i];

                        //Properties 속성은 뒤에 pump id가 붙는경우가 있어서 conditions[3]이후 값이 빈값이 아니라면 붙여준다.
                        for (int j = 4; j < pumpData.Count; j++)
                        {
                            if (pumpData[j] != null)
                            {
                                pumpData[3] = (string)pumpData[3] + " " + (string)pumpData[j];
                            }
                        }

                        pumpData.Insert(0, (string)conditions["INP_NUMBER"]);                           //INP파일 번호 할당

                        dao.InsertPumpData(dbManager, pumpData);
                    }

                    Console.WriteLine("PUMP 정보 입력 완료...");
                }

                if (sectionData["valve"] != null)
                {
                    //Valve정보 등록
                    ArrayList valveDataList = (ArrayList)sectionData["valve"];

                    for (int i = 0; i < valveDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList valveData = (ArrayList)valveDataList[i];

                        valveData.Insert(0, (string)conditions["INP_NUMBER"]);                           //INP파일 번호 할당

                        dao.InsertValveData(dbManager, valveData);
                    }

                    Console.WriteLine("VALVE 정보 입력 완료...");
                }

                if (sectionData["demand"] != null)
                {
                    //Demand정보 등록
                    ArrayList demandDataList = (ArrayList)sectionData["demand"];

                    for (int i = 0; i < demandDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList demandData = (ArrayList)demandDataList[i];

                        demandData.Insert(0, (string)conditions["INP_NUMBER"]);                           //INP파일 번호 할당

                        dao.InsertDemandData(dbManager, demandData);
                    }

                    Console.WriteLine("DEMAND 정보 입력 완료...");
                }

                if (sectionData["status"] != null)
                {
                    //Status정보 등록
                    ArrayList statusDataList = (ArrayList)sectionData["status"];

                    for (int i = 0; i < statusDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList statusData = (ArrayList)statusDataList[i];

                        //인덱스자리에 i값을 끼워넣는다.
                        statusData.Insert(1, Convert.ToString(i));

                        statusData.Insert(0, (string)conditions["INP_NUMBER"]);                           //INP파일 번호 할당

                        dao.InsertStatusData(dbManager, statusData);
                    }

                    Console.WriteLine("STATUS 정보 입력 완료...");
                }

                if (sectionData["curve"] != null)
                {
                    //Status정보 등록
                    ArrayList curveDataList = (ArrayList)sectionData["curve"];

                    Hashtable tmpIdx = new Hashtable();     //ID별로 index를 따로 진행시키기위한 Hashtable

                    for (int i = 0; i < curveDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList curveData = (ArrayList)curveDataList[i];

                        int idx = 0;

                        if (tmpIdx[(string)curveData[0]] != null)
                        {
                            //tmpIdx에 이미 존재하는 ID값이라면 발췌하여 +1을 하여 재할당
                            idx = ((int)tmpIdx[(string)curveData[0]]) + 1;
                            tmpIdx[(string)curveData[0]] = idx;
                        }
                        else
                        {
                            //tmpIdx에 존재하지 않는다면 0을 할당
                            tmpIdx.Add((string)curveData[0], idx);
                        }

                        //인덱스자리에 i값을 끼워넣는다.
                        curveData.Insert(1, Convert.ToString(idx));

                        curveData.Insert(0, (string)conditions["INP_NUMBER"]);                           //INP파일 번호 할당

                        dao.InsertCurveData(dbManager, curveData);
                    }

                    Console.WriteLine("CURVE 정보 입력 완료...");
                }

                if (sectionData["pattern"] != null)
                {
                    //2014.09.17 Bulk Insert로 교체 - 강현복
                    List<string> inpNumberList = new List<string>();
                    List<string> patternIdList = new List<string>();
                    List<string> idxList = new List<string>();
                    List<string> multiplierList = new List<string>();
                    List<string> remarkList = new List<string>();

                    ArrayList patternDataList = (ArrayList)sectionData["pattern"];

                    foreach (Hashtable patternData in patternDataList)
                    {
                        string id = patternData["id"].ToString();
                        ArrayList values = patternData["values"] as ArrayList;

                        for (int i = 0; i < values.Count; i++ )
                        {
                            inpNumberList.Add(conditions["INP_NUMBER"].ToString());
                            patternIdList.Add(id);
                            idxList.Add(i.ToString());
                            multiplierList.Add(values[i].ToString());
                            remarkList.Add("1");
                        }

                    }

                    Hashtable patBulkConditions = new Hashtable();
                    patBulkConditions.Add("inpNumberList", inpNumberList);
                    patBulkConditions.Add("patternIdList", patternIdList);
                    patBulkConditions.Add("idxList", idxList);
                    patBulkConditions.Add("multiplierList", multiplierList);
                    patBulkConditions.Add("remarkList", remarkList);

                    dao.InsertPatternBulkData(dbManager, patBulkConditions);
                    Console.WriteLine("PATTERN 정보 입력 완료...");

                    //ArrayList patternDataList = (ArrayList)sectionData["pattern"];

                    //for (int i = 0; i < patternDataList.Count; i++)
                    //{
                    //    Hashtable patternData = (Hashtable)patternDataList[i];

                    //    string patternId = (string)patternData["id"];
                    //    ArrayList patternValues = (ArrayList)patternData["values"];

                    //    Hashtable patternCond = new Hashtable();

                    //    patternCond.Add("INP_NUMBER", (string)conditions["INP_NUMBER"]);
                    //    patternCond.Add("PATTERN_ID", patternId);

                    //    for (int j = 0; j < patternValues.Count; j++)
                    //    {

                    //        patternCond.Add("IDX", Convert.ToString(j));
                    //        patternCond.Add("MULTIPLIER", Convert.ToString(patternValues[j]));

                    //        dao.InsertPatternData(dbManager, patternCond);

                    //        patternCond.Remove("IDX");
                    //        patternCond.Remove("MULTIPLIER");
                    //    }
                    //    patternCond.Remove("PATTERN_ID");
                    //}
                }

                if (sectionData["control"] != null)
                {
                    //Status정보 등록
                    ArrayList controlDataList = (ArrayList)sectionData["control"];

                    for (int i = 0; i < controlDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList controlData = (ArrayList)controlDataList[i];

                        ArrayList tmpList = new ArrayList();

                        tmpList.Add((string)conditions["INP_NUMBER"]);                      //INP파일 일련번호
                        tmpList.Add(Convert.ToString(i));                                   //IDX

                        string controlString = "";

                        //컬럼단위로 분해되서 나오는 cotrol제어문을 row단위로 문자열 하나로 붙여 관리한다.
                        for (int j = 0; j < controlData.Count; j++)
                        {
                            if (controlData[j] != null)
                            {
                                controlString = controlString + " " + controlData[j];
                            }
                        }

                        tmpList.Add(controlString);
                        tmpList.Add(controlData[10]);                   //주석은 11번째

                        dao.InsertControlData(dbManager, tmpList);
                    }

                    Console.WriteLine("CONTROL 정보 입력 완료...");
                }

                if (sectionData["rule"] != null)
                {
                    //Rule정보 등록
                    ArrayList ruleDataList = (ArrayList)sectionData["rule"];

                    for (int i = 0; i < ruleDataList.Count; i++)
                    {
                        //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                        ArrayList ruleData = (ArrayList)ruleDataList[i];

                        ArrayList tmpList = new ArrayList();

                        tmpList.Add((string)conditions["INP_NUMBER"]);                      //INP파일 일련번호
                        tmpList.Add(Convert.ToString(i));                                   //IDX

                        string ruleString = "";

                        //컬럼단위로 분해되서 나오는 cotrol제어문을 row단위로 문자열 하나로 붙여 관리한다.
                        for (int j = 0; j < ruleData.Count; j++)
                        {
                            if (ruleData[j] != null)
                            {
                                ruleString = ruleString + " " + ruleData[j];
                            }
                        }

                        tmpList.Add(ruleString);
                        tmpList.Add(ruleData[10]);                      //주석은 11번째

                        dao.InsertRuleData(dbManager, tmpList);
                    }

                    Console.WriteLine("RULE 정보 입력 완료...");
                }

                if (sectionData["energy"] != null)
                {
                    //Energy정보 등록
                    ArrayList energyDataList = (ArrayList)sectionData["energy"];

                    for (int i = 0; i < energyDataList.Count; i++)
                    {
                        //Dao로 반환할 입력정보
                        ArrayList tmpArray = new ArrayList();
                        tmpArray.Add((string)conditions["INP_NUMBER"]);             //inp번호 할당

                        ArrayList energyData = (ArrayList)energyDataList[i];

                        string energyString = "";

                        for (int j = 0; j < energyData.Count; j++)
                        {
                            if (energyData[j] != null)
                            {
                                energyString = energyString + " " + energyData[j];
                            }
                        }

                        tmpArray.Add(Convert.ToString(i));                          //index
                        tmpArray.Add(energyString);                                 //energyStatement

                        dao.InsertEnergyData(dbManager, tmpArray);
                    }

                    Console.WriteLine("ENERGY 정보 입력 완료...");
                }

                if (sectionData["emitter"] != null)
                {
                    //Emitter정보 등록
                    ArrayList emitterDataList = (ArrayList)sectionData["emitter"];

                    for (int i = 0; i < emitterDataList.Count; i++)
                    {
                        ArrayList emitterData = (ArrayList)emitterDataList[i];
                        emitterData.Insert(0, (string)conditions["INP_NUMBER"]);

                        dao.InsertEmitterData(dbManager, emitterData);
                    }

                    Console.WriteLine("EMITTER 정보 입력 완료...");
                }

                if (sectionData["quality"] != null)
                {
                    //Quality정보 등록
                    ArrayList qualityDataList = (ArrayList)sectionData["quality"];

                    for (int i = 0; i < qualityDataList.Count; i++)
                    {
                        ArrayList qualityData = (ArrayList)qualityDataList[i];
                        qualityData.Insert(0, (string)conditions["INP_NUMBER"]);

                        dao.InsertQualityData(dbManager, qualityData);
                    }

                    Console.WriteLine("QUALITY 정보 입력 완료...");
                }

                if (sectionData["source"] != null)
                {
                    //Emitter정보 등록
                    ArrayList sourceDataList = (ArrayList)sectionData["source"];

                    for (int i = 0; i < sourceDataList.Count; i++)
                    {
                        ArrayList sourceData = (ArrayList)sourceDataList[i];
                        sourceData.Insert(0, (string)conditions["INP_NUMBER"]);

                        dao.InsertSourceData(dbManager, sourceData);
                    }

                    Console.WriteLine("SOURCE 정보 입력 완료...");
                }

                if (sectionData["reaction"] != null)
                {
                    //Reaction정보 등록
                    List<string> inpNumberList = new List<string>();
                    List<string> idxList = new List<string>();
                    List<string> reactionStatementList = new List<string>();

                    ArrayList reactionDataList = (ArrayList)sectionData["reaction"];


                    //2014.10.14 메모리 오버플로우로 쪼개서 Bulk insert하는 로직 구현 (계속 오류남)
                    //if (reactionDataList.Count < 10000)
                    //{
                        for (int i = 0; i < reactionDataList.Count; i++)
                        {
                            ArrayList reactionData = (ArrayList)reactionDataList[i];

                            string statement = "";

                            foreach (string tmpStatement in reactionData)
                            {
                                if ("".Equals(statement)) statement = tmpStatement;
                                else statement = statement + "\t" + tmpStatement;
                            }

                            inpNumberList.Add((string)conditions["INP_NUMBER"]);
                            idxList.Add(i.ToString());
                            reactionStatementList.Add(statement);
                        }
                        if (reactionDataList.Count > 0)
                        {
                            Hashtable reaBulkConditions = new Hashtable();
                            reaBulkConditions.Add("inpNumberList", inpNumberList);
                            reaBulkConditions.Add("idxList", idxList);
                            reaBulkConditions.Add("reactionStatementList", reactionStatementList);

                            dao.InsertReactionBulkData(dbManager, reaBulkConditions);
                        }
                        Console.WriteLine("REACTION 정보 입력 완료...");
                    //}
                    //else
                    //{
                    //    int packCnt = reactionDataList.Count / 10000;
                    //    int remainCnt = reactionDataList.Count % 10000;

                    //    Console.WriteLine("10000개가 넘어 분할 입력 로직 수행 ( packCnt : " + packCnt + " remainCnt : " + remainCnt + " )");


                    //    for (int i = 1; i <= packCnt; i++ )
                    //    {
                    //        inpNumberList.Clear();
                    //        idxList.Clear();
                    //        reactionStatementList.Clear();

                    //        for (int j = (i - 1) * 10000; j < i * 10000; j++ )
                    //        {
                    //            ArrayList reactionData = (ArrayList)reactionDataList[j];

                    //            string statement = "";

                    //            foreach (string tmpStatement in reactionData)
                    //            {
                    //                if ("".Equals(statement)) statement = tmpStatement;
                    //                else statement = statement + "\t" + tmpStatement;
                    //            }

                    //            inpNumberList.Add((string)conditions["INP_NUMBER"]);
                    //            idxList.Add(j.ToString());
                    //            reactionStatementList.Add(statement);
                    //        }

                    //        Hashtable reaBulkConditions = new Hashtable();
                    //        reaBulkConditions.Add("inpNumberList", inpNumberList);
                    //        reaBulkConditions.Add("idxList", idxList);
                    //        reaBulkConditions.Add("reactionStatementList", reactionStatementList);

                    //        dao.InsertReactionBulkData(dbManager, reaBulkConditions);
                    //        Console.WriteLine("REACTION 정보 입력 완료..." + i);
                    //    }

                    //    //나머지 입력
                    //    if(remainCnt != 0)
                    //    {
                    //        inpNumberList.Clear();
                    //        idxList.Clear();
                    //        reactionStatementList.Clear();

                    //        for (int i = 1; i <= remainCnt; i++ )
                    //        {
                    //            ArrayList reactionData = (ArrayList)reactionDataList[(packCnt * 10000) + i];

                    //            string statement = "";

                    //            foreach (string tmpStatement in reactionData)
                    //            {
                    //                if ("".Equals(statement)) statement = tmpStatement;
                    //                else statement = statement + "\t" + tmpStatement;
                    //            }

                    //            inpNumberList.Add((string)conditions["INP_NUMBER"]);
                    //            idxList.Add((packCnt * 10000) + i.ToString());
                    //            reactionStatementList.Add(statement);
                    //        }

                    //        Hashtable reaBulkConditions = new Hashtable();
                    //        reaBulkConditions.Add("inpNumberList", inpNumberList);
                    //        reaBulkConditions.Add("idxList", idxList);
                    //        reaBulkConditions.Add("reactionStatementList", reactionStatementList);

                    //        dao.InsertReactionBulkData(dbManager, reaBulkConditions);
                    //        Console.WriteLine("REACTION 정보 입력 완료... (나머지)");
                    //    }
                    //}

                     
                    
                    

                    //ArrayList reactionDataList = (ArrayList)sectionData["reaction"];

                    //for (int i = 0; i < reactionDataList.Count; i++)
                    //{
                    //    //Dao로 반환할 입력정보
                    //    ArrayList tmpArray = new ArrayList();
                    //    tmpArray.Add((string)conditions["INP_NUMBER"]);             //inp번호 할당

                    //    ArrayList reactionData = (ArrayList)reactionDataList[i];

                    //    string reactionString = "";

                    //    for (int j = 0; j < reactionData.Count; j++)
                    //    {
                    //        if (reactionData[j] != null)
                    //        {
                    //            reactionString = reactionString + " " + reactionData[j];
                    //        }
                    //    }

                    //    tmpArray.Add(Convert.ToString(i));                          //index
                    //    tmpArray.Add(reactionString);                               //reactionStatement

                    //    dao.InsertReactionData(dbManager, tmpArray);
                    //}

                    //Console.WriteLine("REACTION 정보 입력 완료...");
                }

                if (sectionData["mixing"] != null)
                {
                    //Mixing정보 등록
                    ArrayList mixingDataList = (ArrayList)sectionData["mixing"];

                    for (int i = 0; i < mixingDataList.Count; i++)
                    {
                        ArrayList mixingData = (ArrayList)mixingDataList[i];
                        mixingData.Insert(0, (string)conditions["INP_NUMBER"]);

                        dao.InsertMixingData(dbManager, mixingData);
                    }

                    Console.WriteLine("MIXING 정보 입력 완료...");
                }

                if (sectionData["timeOption"] != null)
                {
                    //Time정보 등록
                    ArrayList timeDataList = (ArrayList)sectionData["timeOption"];

                    for (int i = 0; i < timeDataList.Count; i++)
                    {
                        //Dao로 반환할 입력정보
                        ArrayList tmpArray = new ArrayList();
                        tmpArray.Add((string)conditions["INP_NUMBER"]);             //inp번호 할당

                        ArrayList timeData = (ArrayList)timeDataList[i];

                        string timeString = "";

                        for (int j = 0; j < timeData.Count; j++)
                        {
                            if (timeData[j] != null)
                            {
                                timeString = timeString + " " + timeData[j];
                            }
                        }

                        tmpArray.Add(Convert.ToString(i));                          //index
                        tmpArray.Add(timeString);                                   //timeStatement

                        dao.InsertTimeOptionData(dbManager, tmpArray);
                    }

                    Console.WriteLine("TIME OPTION 정보 입력 완료...");
                }

                if (sectionData["reportOption"] != null)
                {
                    //Report Option정보 등록
                    ArrayList reportOptionDataList = (ArrayList)sectionData["reportOption"];

                    for (int i = 0; i < reportOptionDataList.Count; i++)
                    {
                        //Dao로 반환할 입력정보
                        ArrayList tmpArray = new ArrayList();
                        tmpArray.Add((string)conditions["INP_NUMBER"]);             //inp번호 할당

                        ArrayList reportOptionData = (ArrayList)reportOptionDataList[i];

                        string reportOptionString = "";

                        for (int j = 0; j < reportOptionData.Count; j++)
                        {
                            if (reportOptionData[j] != null)
                            {
                                reportOptionString = reportOptionString + " " + reportOptionData[j];
                            }
                        }

                        tmpArray.Add(Convert.ToString(i));                          //index
                        tmpArray.Add(reportOptionString);                           //reportStatement

                        dao.InsertReportOptionData(dbManager, tmpArray);
                    }

                    Console.WriteLine("REPORT OPTION 정보 입력 완료...");
                }

                if (sectionData["normalOption"] != null)
                {
                    //Normal Option 정보 등록
                    ArrayList normalOptionDataList = (ArrayList)sectionData["normalOption"];

                    for (int i = 0; i < normalOptionDataList.Count; i++)
                    {
                        //Dao로 반환할 입력정보
                        ArrayList tmpArray = new ArrayList();
                        tmpArray.Add((string)conditions["INP_NUMBER"]);             //inp번호 할당

                        ArrayList normalOptionData = (ArrayList)normalOptionDataList[i];

                        string normalOptionString = "";

                        for (int j = 0; j < normalOptionData.Count; j++)
                        {
                            if (normalOptionData[j] != null)
                            {
                                normalOptionString = normalOptionString + " " + normalOptionData[j];
                            }
                        }

                        tmpArray.Add(Convert.ToString(i));                          //index
                        tmpArray.Add(normalOptionString);                           //optionStatement

                        dao.InsertNormalOptionData(dbManager, tmpArray);
                    }

                    Console.WriteLine("NORMAL OPTION 정보 입력 완료...");
                }

                if (sectionData["coordinate"] != null)
                {
                    List<string> inpNumberList = new List<string>();
                    List<string> idList = new List<string>();
                    List<string> xList = new List<string>();
                    List<string> yList = new List<string>();

                    ArrayList coordinateDataList = (ArrayList)sectionData["coordinate"];

                    for (int i = 0; i < coordinateDataList.Count; i++ )
                    {
                        ArrayList coordinateData = (ArrayList)coordinateDataList[i];

                        inpNumberList.Add(conditions["INP_NUMBER"].ToString());
                        idList.Add(coordinateData[0].ToString());
                        xList.Add(coordinateData[1].ToString());
                        yList.Add(coordinateData[2].ToString());                        
                    }

                    Hashtable cooBulkConditions = new Hashtable();
                    cooBulkConditions.Add("inpNumberList", inpNumberList);
                    cooBulkConditions.Add("idList", idList);
                    cooBulkConditions.Add("xList", xList);
                    cooBulkConditions.Add("yList", yList);

                    dao.InsertCoordinateBulkData(dbManager, cooBulkConditions);

                    Console.WriteLine("COORDINATE 정보 입력 완료...");

                    //2014.09.17 Bulk Insert로 교체 - 강현복
                    //Coordinate정보 등록
                    //ArrayList coordinateDataList = (ArrayList)sectionData["coordinate"];

                    //ArrayList coordinateDatas = new ArrayList();
                    //for (int i = 0; i < coordinateDataList.Count; i++)
                    //{
                    //    ArrayList coordinateData = (ArrayList)coordinateDataList[i];
                    //    coordinateData.Insert(0, (string)conditions["INP_NUMBER"]);

                    //    coordinateDatas.Add(coordinateData);
                        
                    //}

                    //dao.InsertCoordinateData(dbManager, coordinateDatas);
                }

                if (sectionData["vertices"] != null)
                {
                    List<string> inpNumberList = new List<string>();
                    List<string> idList = new List<string>();
                    List<string> idxList = new List<string>();
                    List<string> xList = new List<string>();
                    List<string> yList = new List<string>();

                    ArrayList verticesDataList = (ArrayList)sectionData["vertices"];
                    Hashtable tmpIdx = new Hashtable();

                    for (int i = 0; i < verticesDataList.Count; i++)
                    {
                        ArrayList verticesData = (ArrayList)verticesDataList[i];

                        if (tmpIdx[verticesData[0].ToString()] == null)
                        {
                            tmpIdx.Add(verticesData[0].ToString(), 0);;
                            idxList.Add("0");
                            
                        }
                        else
                        {
                            int idx = int.Parse(tmpIdx[verticesData[0].ToString()].ToString());
                            idxList.Add((idx + 1).ToString());
                            tmpIdx[verticesData[0].ToString()] = (idx + 1).ToString();
                        }

                        inpNumberList.Add(conditions["INP_NUMBER"].ToString());
                        idList.Add(verticesData[0].ToString());
                        xList.Add(verticesData[1].ToString());
                        yList.Add(verticesData[2].ToString());
                    }

                    Hashtable verBulkConditions = new Hashtable();
                    verBulkConditions.Add("inpNumberList", inpNumberList);
                    verBulkConditions.Add("idList", idList);
                    verBulkConditions.Add("idxList", idxList);
                    verBulkConditions.Add("xList", xList);
                    verBulkConditions.Add("yList", yList);

                    dao.InsertVerticesBulkData(dbManager, verBulkConditions);

                    Console.WriteLine("VERTICES 정보 입력 완료...");

                    //2014.09.17 Bulk Insert로 교체 - 강현복
                    //Vertices정보 등록
                    //ArrayList verticesDataList = (ArrayList)sectionData["vertices"];

                    //Hashtable tmpIdx = new Hashtable();     //ID별로 index를 따로 진행시키기위한 Hashtable

                    //ArrayList verticesDatas = new ArrayList();
                    //for (int i = 0; i < verticesDataList.Count; i++)
                    //{
                    //    //Text parsing의 특수성으로 argument가 Hashtable이 아닌 ArrayList임
                    //    ArrayList verticesData = (ArrayList)verticesDataList[i];

                    //    int idx = 0;

                    //    if (tmpIdx[(string)verticesData[0]] != null)
                    //    {
                    //        //tmpIdx에 이미 존재하는 ID값이라면 발췌하여 +1을 하여 재할당
                    //        idx = ((int)tmpIdx[(string)verticesData[0]]) + 1;
                    //        tmpIdx[(string)verticesData[0]] = idx;
                    //    }
                    //    else
                    //    {
                    //        //tmpIdx에 존재하지 않는다면 0을 할당
                    //        tmpIdx.Add((string)verticesData[0], idx);
                    //    }

                    //    verticesData.Insert(0, (string)conditions["INP_NUMBER"]);               //INP파일 번호 할당
                    //    verticesData.Insert(2, Convert.ToString(idx));                          //인덱스자리에 idx값을 끼워넣는다.

                    //    verticesDatas.Add(verticesData);
                        
                    //}

                    //dao.InsertVerticesData(dbManager, verticesDatas);
                }

                if (sectionData["label"] != null)
                {
                    //label정보 등록
                    ArrayList labelDataList = (ArrayList)sectionData["label"];

                    for (int i = 0; i < labelDataList.Count; i++)
                    {
                        double d = double.MaxValue;
                        ArrayList labelData = (ArrayList)labelDataList[i];


                        if (!double.TryParse((string)labelData[0], out d))
                        {
                            labelData[2] = labelData[1];   

                            string[] split = ((string)labelData[0]).Trim().Split(new char[] { '\t',' ' });
                            labelData[0] = split[0];
                            labelData[1] = split[split.Length - 1];
                        }
                        
                        labelData.Insert(0, (string)conditions["INP_NUMBER"]);
                        labelData.Insert(1, i.ToString());

                        dao.InsertLabelData(dbManager, labelData);
                    }

                    Console.WriteLine("LABEL 정보 입력 완료...");
                }

                if (sectionData["tag"] != null)
                {
                    //tag정보 등록

                    List<string> inpNumberList = new List<string>();
                    List<string> typeList = new List<string>();
                    List<string> idList = new List<string>();
                    List<string> positionInfoList = new List<string>();

                    ArrayList tagDataList = (ArrayList)sectionData["tag"];

                    for (int i = 0; i < tagDataList.Count; i++)
                    {
                        ArrayList tagData = (ArrayList)tagDataList[i];

                        inpNumberList.Add(conditions["INP_NUMBER"].ToString());
                        typeList.Add(tagData[0].ToString());
                        idList.Add(tagData[1].ToString());
                        positionInfoList.Add(tagData[2].ToString());
                    }

                    Hashtable tagBulkConditions = new Hashtable();
                    tagBulkConditions.Add("inpNumberList", inpNumberList);
                    tagBulkConditions.Add("typeList", typeList);
                    tagBulkConditions.Add("idList", idList);
                    tagBulkConditions.Add("positionInfoList", positionInfoList);

                    dao.InsertTagBulkData(dbManager, tagBulkConditions);

                    Console.WriteLine("TAGS 정보 입력 완료...");

                    //for (int i = 0; i < positionInfoList.Count; i++ )
                    //{
                    //    Console.WriteLine(positionInfoList[i]);
                    //}


                    //2014.09.17 Bulk Insert로 변경 - 강현복
                    //ArrayList tagDataList = (ArrayList)sectionData["tag"];

                    //ArrayList tagDatas = new ArrayList();
                    //for (int i = 0; i < tagDataList.Count; i++)
                    //{
                    //    ArrayList tagData = (ArrayList)tagDataList[i];
                    //    tagData.Insert(0, (string)conditions["INP_NUMBER"]);

                    //    tagDatas.Add(tagData);
                        
                    //}

                    //dao.InsertTagData(dbManager, tagDatas);
                }
                #endregion

                #region 사용량배분
                //블록별 BaseDemand 총량 연산
                DataSet dSet = dao.SelectSumBaseDemand(dbManager, conditions);

                Hashtable baseDemandSumConditions = null;

                //블록별 BaseDemand 총량 입력
                foreach (DataRow row in dSet.Tables["WH_TAGS"].Rows)
                {
                    baseDemandSumConditions = new Hashtable();

                    baseDemandSumConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                    baseDemandSumConditions.Add("BSM_CDE", row["BSM_CDE"].ToString());
                    baseDemandSumConditions.Add("BSM_IDN", row["BSM_IDN"].ToString());
                    baseDemandSumConditions.Add("SUM_BASEDEMAND", row["SUM_BASEDEMAND"].ToString());

                    dao.InsertBlockSumUsage(dbManager, baseDemandSumConditions);
                }
                #endregion

                #region 해석결과표출범위 입력 (초기값 고정)
                //Elevation
                Hashtable elevationConditions = new Hashtable();

                elevationConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                elevationConditions.Add("RESULT_TYPE", "1004");
                elevationConditions.Add("RESULT_CODE", "000001");
                elevationConditions.Add("RANGE1", "25");
                elevationConditions.Add("RANGE2", "50");
                elevationConditions.Add("RANGE3", "75");
                elevationConditions.Add("RANGE4", "100");

                dao.InsertAnalysisResultRange(dbManager, elevationConditions);

                //BaseDemand
                Hashtable baseDemandConditions = new Hashtable();

                baseDemandConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                baseDemandConditions.Add("RESULT_TYPE", "1004");
                baseDemandConditions.Add("RESULT_CODE", "000002");
                baseDemandConditions.Add("RANGE1", "25");
                baseDemandConditions.Add("RANGE2", "50");
                baseDemandConditions.Add("RANGE3", "75");
                baseDemandConditions.Add("RANGE4", "100");

                dao.InsertAnalysisResultRange(dbManager, baseDemandConditions);

                //Initial Quality
                Hashtable initialQualityConditions = new Hashtable();

                initialQualityConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                initialQualityConditions.Add("RESULT_TYPE", "1004");
                initialQualityConditions.Add("RESULT_CODE", "000003");
                initialQualityConditions.Add("RANGE1", "2");
                initialQualityConditions.Add("RANGE2", "6");
                initialQualityConditions.Add("RANGE3", "12");
                initialQualityConditions.Add("RANGE4", "24");

                dao.InsertAnalysisResultRange(dbManager, initialQualityConditions);

                //Demand
                Hashtable demandConditions = new Hashtable();

                demandConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                demandConditions.Add("RESULT_TYPE", "1004");
                demandConditions.Add("RESULT_CODE", "000004");
                demandConditions.Add("RANGE1", "25");
                demandConditions.Add("RANGE2", "50");
                demandConditions.Add("RANGE3", "75");
                demandConditions.Add("RANGE4", "100");

                dao.InsertAnalysisResultRange(dbManager, demandConditions);

                //Head
                Hashtable headConditions = new Hashtable();

                headConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                headConditions.Add("RESULT_TYPE", "1004");
                headConditions.Add("RESULT_CODE", "000005");
                headConditions.Add("RANGE1", "25");
                headConditions.Add("RANGE2", "50");
                headConditions.Add("RANGE3", "75");
                headConditions.Add("RANGE4", "100");

                dao.InsertAnalysisResultRange(dbManager, headConditions);

                //Pressure
                Hashtable pressureConditions = new Hashtable();

                pressureConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                pressureConditions.Add("RESULT_TYPE", "1004");
                pressureConditions.Add("RESULT_CODE", "000006");
                pressureConditions.Add("RANGE1", "25");
                pressureConditions.Add("RANGE2", "50");
                pressureConditions.Add("RANGE3", "75");
                pressureConditions.Add("RANGE4", "100");

                dao.InsertAnalysisResultRange(dbManager, pressureConditions);

                //Length
                Hashtable lengthConditions = new Hashtable();

                lengthConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                lengthConditions.Add("RESULT_TYPE", "1005");
                lengthConditions.Add("RESULT_CODE", "000001");
                lengthConditions.Add("RANGE1", "100");
                lengthConditions.Add("RANGE2", "500");
                lengthConditions.Add("RANGE3", "1000");
                lengthConditions.Add("RANGE4", "5000");

                dao.InsertAnalysisResultRange(dbManager, lengthConditions);

                //Diameter
                Hashtable diameterConditions = new Hashtable();

                diameterConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                diameterConditions.Add("RESULT_TYPE", "1005");
                diameterConditions.Add("RESULT_CODE", "000002");
                diameterConditions.Add("RANGE1", "6");
                diameterConditions.Add("RANGE2", "12");
                diameterConditions.Add("RANGE3", "24");
                diameterConditions.Add("RANGE4", "36");

                dao.InsertAnalysisResultRange(dbManager, diameterConditions);

                //Roughness
                Hashtable roughnessConditions = new Hashtable();

                roughnessConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                roughnessConditions.Add("RESULT_TYPE", "1005");
                roughnessConditions.Add("RESULT_CODE", "000003");
                roughnessConditions.Add("RANGE1", "50");
                roughnessConditions.Add("RANGE2", "75");
                roughnessConditions.Add("RANGE3", "100");
                roughnessConditions.Add("RANGE4", "125");

                dao.InsertAnalysisResultRange(dbManager, roughnessConditions);

                //Bulk Coeff.
                Hashtable bulkCoeffConditions = new Hashtable();

                bulkCoeffConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                bulkCoeffConditions.Add("RESULT_TYPE", "1005");
                bulkCoeffConditions.Add("RESULT_CODE", "000004");
                bulkCoeffConditions.Add("RANGE1", "0.25");
                bulkCoeffConditions.Add("RANGE2", "0.50");
                bulkCoeffConditions.Add("RANGE3", "0.75");
                bulkCoeffConditions.Add("RANGE4", "1");

                dao.InsertAnalysisResultRange(dbManager, bulkCoeffConditions);

                //Wall Coeff.
                Hashtable wallCoeffConditions = new Hashtable();

                wallCoeffConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                wallCoeffConditions.Add("RESULT_TYPE", "1005");
                wallCoeffConditions.Add("RESULT_CODE", "000005");
                wallCoeffConditions.Add("RANGE1", "0.25");
                wallCoeffConditions.Add("RANGE2", "0.50");
                wallCoeffConditions.Add("RANGE3", "0.75");
                wallCoeffConditions.Add("RANGE4", "1");

                dao.InsertAnalysisResultRange(dbManager, wallCoeffConditions);

                //Flow
                Hashtable flowConditions = new Hashtable();

                flowConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                flowConditions.Add("RESULT_TYPE", "1005");
                flowConditions.Add("RESULT_CODE", "000006");
                flowConditions.Add("RANGE1", "25");
                flowConditions.Add("RANGE2", "50");
                flowConditions.Add("RANGE3", "75");
                flowConditions.Add("RANGE4", "100");

                dao.InsertAnalysisResultRange(dbManager, flowConditions);

                //Velocity
                Hashtable velocityConditions = new Hashtable();

                velocityConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                velocityConditions.Add("RESULT_TYPE", "1005");
                velocityConditions.Add("RESULT_CODE", "000007");
                velocityConditions.Add("RANGE1", "0.01");
                velocityConditions.Add("RANGE2", "0.1");
                velocityConditions.Add("RANGE3", "1");
                velocityConditions.Add("RANGE4", "2");

                dao.InsertAnalysisResultRange(dbManager, velocityConditions);

                //Unit Headloss
                Hashtable headlossConditions = new Hashtable();

                headlossConditions.Add("INP_NUMBER", conditions["INP_NUMBER"]);
                headlossConditions.Add("RESULT_TYPE", "1005");
                headlossConditions.Add("RESULT_CODE", "000008");
                headlossConditions.Add("RANGE1", "0.03");
                headlossConditions.Add("RANGE2", "0.05");
                headlossConditions.Add("RANGE3", "0.08");
                headlossConditions.Add("RANGE4", "0.1");

                dao.InsertAnalysisResultRange(dbManager, headlossConditions);

                #endregion

                #region 실시간 관망해석 세팅 입력
                Hashtable analysisSettingConditions = new Hashtable();
                
                analysisSettingConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                analysisSettingConditions.Add("ERROR_SAVE_YN", "N");
                analysisSettingConditions.Add("WARNING_SAVE_YN", "N");
                analysisSettingConditions.Add("LOW_PRESSURE", "0");

                dao.InsertAnalysisSettingData(dbManager, analysisSettingConditions);
                #endregion

                #region Shape파일 생성
                //oInpSetLayer = new InpSetLayerManager(dbManager, conditions["INP_NUMBER"].ToString());
                //oInpSetLayer.StartEditor();
                //oInpSetLayer.CreateINP_Shape();
                //oInpSetLayer.StopEditor(true);
                #endregion

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
                //Shape 파일 Abort()
                if (oInpSetLayer != null)
                {
                    oInpSetLayer.AbortEditor();
                }
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
                dbManager.Transaction.Dispose();
            }

        }

        //INP정보 삭제
        public void DeleteInpMasterData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteInpMasterData(dbManager, conditions);
                dao.DeleteJunctionData(dbManager, conditions);
                dao.DeleteReservoirData(dbManager, conditions);
                dao.DeleteTankData(dbManager, conditions);
                dao.DeleteQualityData(dbManager, conditions);
                dao.DeleteSourceData(dbManager, conditions);
                dao.DeleteEmitterData(dbManager, conditions);
                dao.DeleteLabelData(dbManager, conditions);
                dao.DeleteDemandData(dbManager, conditions);
                dao.DeleteMixingData(dbManager, conditions);
                dao.DeletePipeData(dbManager, conditions);
                dao.DeleteCoordinateData(dbManager, conditions);
                dao.DeleteVerticesData(dbManager, conditions);
                dao.DeletePumpData(dbManager, conditions);
                dao.DeleteValveData(dbManager, conditions);
                dao.DeleteEnergyData(dbManager, conditions);
                dao.DeleteCurveData(dbManager, conditions);
                dao.DeletePatternData(dbManager, conditions);
                dao.DeleteStatusData(dbManager, conditions);
                dao.DeleteControlData(dbManager, conditions);
                dao.DeleteRuleData(dbManager, conditions);
                dao.DeleteReactionData(dbManager, conditions);
                dao.DeleteOptionData(dbManager, conditions);
                dao.DeleteTimeOptionData(dbManager, conditions);
                dao.DeleteReportOptionData(dbManager, conditions);

                dao.DeleteLargeConsumerData(dbManager, conditions);
                dao.DeleteLargeConsumerTimeTableData(dbManager, conditions);
                dao.DeleteLargeConsumerActivateData(dbManager, conditions);
                dao.DeleteRealtimeCheckpointData(dbManager, conditions);
                dao.DeleteMeaserDumpMasterData(dbManager, conditions);
                dao.DeleteMeaserDumpData(dbManager, conditions);
                dao.DeleteAnalysisResultRangeData(dbManager, conditions);
                dao.DeleteBlockSumUsage(dbManager, conditions);
                dao.DeleteAnalysisSettingData(dbManager, conditions);
                dao.DeleteAveragePressurePointData(dbManager, conditions);

                dbManager.CommitTransaction();

                FunctionManager.RemoveDirectory(VariableManager.m_INPgraphicRootDirectory + "\\" + conditions["INP_NUMBER"], true);
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //INP파일 리스트 조회
        public DataSet SelectINPList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                result = dao.SelectINPMasterList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;

        }

        //DB에서 정보를 읽어 INP파일 조합
        public Hashtable GenerateINPFile(Hashtable conditions)
        {
            Hashtable result = new Hashtable();

            OracleDBManager dbManager = null;

            StreamWriter writer = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                string fineName = conditions["fileName"].ToString();
                //writer = new StreamWriter(fineName, true, Encoding.GetEncoding(51949));
                writer = new StreamWriter(fineName, false, Encoding.GetEncoding(51949));

                //Master 정보로부터 파일명을 발췌
                DataSet masterData = dao.SelectINPMasterData(dbManager, conditions);

                string title = (string)masterData.Tables["WH_TITLE"].Rows[0]["TITLE"];

                //Section별 데이터 조회
                DataSet junctionList = dao.SelectJunctionList(dbManager, conditions);
                DataSet reservoirList = dao.SelectReservoirList(dbManager, conditions);
                DataSet tankList = dao.SelectTankList(dbManager, conditions);
                DataSet pipeList = dao.SelectPipeList(dbManager, conditions);
                DataSet pumpList = dao.SelectPumpList(dbManager, conditions);
                DataSet valveList = dao.SelectValveList(dbManager, conditions);
                DataSet demandList = dao.SelectDemandList(dbManager, conditions);
                DataSet statusList = dao.SelectStatusList(dbManager, conditions);
                DataSet curveList = dao.SelectCurveList(dbManager, conditions);
                DataSet patternList = dao.SelectPatternList(dbManager, conditions);
                DataSet controlList = dao.SelectControlList(dbManager, conditions);
                DataSet ruleList = dao.SelectRuleList(dbManager, conditions);
                DataSet energyList = dao.SelectEnergyList(dbManager, conditions);
                DataSet emitterList = dao.SelectEmitterList(dbManager, conditions);
                DataSet qualityList = dao.SelectQualityList(dbManager, conditions);
                DataSet sourceList = dao.SelectSourceList(dbManager, conditions);
                DataSet reactionList = dao.SelectReactionList(dbManager, conditions);
                DataSet mixingList = dao.SelectMixingList(dbManager, conditions);
                DataSet timeList = dao.SelectTimeOptionList(dbManager, conditions);
                DataSet reportList = dao.SelectReportOptionList(dbManager, conditions);
                DataSet optionList = dao.SelectOptionList(dbManager, conditions);
                DataSet coordinateList = dao.SelectCoordinateList(dbManager, conditions);
                DataSet verticesList = dao.SelectVerticesList(dbManager, conditions);
                DataSet labelList = dao.SelectLabelList(dbManager, conditions);
                DataSet tagList = dao.SelectTagList(dbManager, conditions);

                //Section별로 조회된 데이터를 파일로 쓰기
                writer.WriteLine("[TITLE]");
                writer.WriteLine(title);

                writer.WriteLine("");
                writer.WriteLine("[JUNCTIONS]");
                foreach (DataRow junctionRow in junctionList.Tables["WH_JUNCTIONS"].Rows)
                {
                    writer.WriteLine(junctionRow["ID"] + "\t" + junctionRow["ELEV"] + "\t" + junctionRow["DEMAND"] + "\t" + junctionRow["PATTERN_ID"] + ";" + junctionRow["REMARK"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[RESERVOIRS]");
                foreach (DataRow reservoirRow in reservoirList.Tables["WH_RESERVOIRS"].Rows)
                {
                    writer.WriteLine(reservoirRow["ID"] + "\t" + reservoirRow["HEAD"] + "\t" + reservoirRow["PATTERN_ID"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[TANKS]");
                foreach (DataRow tankRow in tankList.Tables["WH_TANK"].Rows)
                {
                    writer.WriteLine(tankRow["ID"] + "\t" + tankRow["ELEV"] + "\t" + tankRow["INITLVL"] + "\t" + tankRow["MINLVL"] + "\t" + tankRow["MAXLVL"] + "\t" + tankRow["DIAM"] + "\t" + tankRow["MINVOL"] + "\t" + tankRow["VOLCURVE_ID"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[PIPES]");
                foreach (DataRow pipeRow in pipeList.Tables["WH_PIPES"].Rows)
                {
                    if ("N".Equals(conditions["type"].ToString()))
                    {
                        //통상적인 EPANET인 경우
                        writer.WriteLine(pipeRow["ID"] + "\t" + pipeRow["NODE1"] + "\t" + pipeRow["NODE2"] + "\t" + pipeRow["LENGTH"] + "\t" + pipeRow["DIAM"] + "\t" + pipeRow["LEAKAGE_COEFFICIENT"] + "\t" + pipeRow["ROUGHNESS"] + "\t" + pipeRow["MLOSS"] + "\t" + pipeRow["STATUS"] + ";" + pipeRow["REMARK"]);
                    }
                    else
                    {
                        //EPANET K인경우
                        writer.WriteLine(pipeRow["ID"] + "\t" + pipeRow["NODE1"] + "\t" + pipeRow["NODE2"] + "\t" + pipeRow["LENGTH"] + "\t" + pipeRow["DIAM"] + "\t" + pipeRow["ROUGHNESS"] + "\t" + pipeRow["MLOSS"] + "\t" + pipeRow["STATUS"] + ";" + pipeRow["REMARK"]);
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("[PUMPS]");
                foreach (DataRow pumpRow in pumpList.Tables["WH_PUMPS"].Rows)
                {
                    writer.WriteLine(pumpRow["ID"] + "\t" + pumpRow["NODE1"] + "\t" + pumpRow["NODE2"] + "\t" + pumpRow["PROPERTIES"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[VALVES]");
                foreach (DataRow valveRow in valveList.Tables["WH_VALVES"].Rows)
                {
                    writer.WriteLine(valveRow["ID"] + "\t" + valveRow["NODE1"] + "\t" + valveRow["NODE2"] + "\t" + valveRow["DIAMETER"] + "\t" + valveRow["TYPE"] + "\t" + valveRow["SETTING"] + "\t" + valveRow["MINORLOSS"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[DEMANDS]");
                foreach (DataRow demandRow in demandList.Tables["WH_DEMANDS"].Rows)
                {
                    writer.WriteLine(demandRow["ID"] + "\t" + demandRow["DEMAND"] + "\t" + demandRow["PATTERN_ID"] + "\t" + demandRow["CATEGORY"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[STATUS]");
                foreach (DataRow statusRow in statusList.Tables["WH_STATUS"].Rows)
                {
                    writer.WriteLine(statusRow["ID"] + "\t" + statusRow["STATUS_SETTING"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[CURVES]");
                foreach (DataRow curveRow in curveList.Tables["WH_CURVES"].Rows)
                {
                    writer.WriteLine(curveRow["ID"] + "\t" + curveRow["X"] + "\t" + curveRow["Y"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[PATTERNS]");
                foreach (DataRow patternRow in patternList.Tables["WH_PATTERNS"].Rows)
                {
                    writer.WriteLine(patternRow["PATTERN_ID"] + "\t" + patternRow["MULTIPLIER"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[CONTROLS]");
                foreach (DataRow controlRow in controlList.Tables["WH_CONTROLS"].Rows)
                {
                    writer.WriteLine(controlRow["CONTROLS_STATEMENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[RULES]");
                foreach (DataRow ruleRow in ruleList.Tables["WH_RULES"].Rows)
                {
                    writer.WriteLine(ruleRow["RULES_STATEMENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[ENERGY]");
                foreach (DataRow energyRow in energyList.Tables["WH_ENERGY"].Rows)
                {
                    writer.WriteLine(energyRow["ENERGY_STATEMENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[EMITTERS]");
                foreach (DataRow emitterRow in emitterList.Tables["WH_EMITTERS"].Rows)
                {
                    writer.WriteLine(emitterRow["ID"] + "\t" + emitterRow["FLOW_COFFICIENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[QUALITY]");
                foreach (DataRow qualityRow in qualityList.Tables["WH_QUALITY"].Rows)
                {
                    writer.WriteLine(qualityRow["ID"] + "\t" + qualityRow["INITQUAL"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[SOURCES]");
                foreach (DataRow sourceRow in sourceList.Tables["WH_SOURCE"].Rows)
                {
                    writer.WriteLine(sourceRow["ID"] + "\t" + sourceRow["TYPE"] + "\t" + sourceRow["STRENGTH"] + "\t" + sourceRow["PATTERN_ID"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[REACTIONS]");
                foreach (DataRow reactionRow in reactionList.Tables["WH_REACTIONS"].Rows)
                {
                    writer.WriteLine(reactionRow["REACTION_STATEMENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[MIXING]");
                foreach (DataRow mixingRow in mixingList.Tables["WH_MIXING"].Rows)
                {
                    writer.WriteLine(mixingRow["ID"] + "\t" + mixingRow["MODEL"] + "\t" + mixingRow["FLACTION"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[TIMES]");
                foreach (DataRow timeRow in timeList.Tables["WH_TIMES"].Rows)
                {
                    writer.WriteLine(timeRow["TIMES_STATEMENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[REPORT]");
                foreach (DataRow reportRow in reportList.Tables["WH_RPT_OPTIONS"].Rows)
                {
                    writer.WriteLine(reportRow["REPORT_STATEMENT"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[OPTIONS]");
                foreach (DataRow optionRow in optionList.Tables["WH_OPTIONS"].Rows)
                {
                    //EPANET K 전용일 경우 몇가지 항목을 제외하고 생성한다.
                    string statement = (optionRow["OPTIONS_STATEMENT"].ToString()).ToUpper();

                    if (statement.Contains("CHECKFREQ") || statement.Contains("MAXCHECK") || statement.Contains("DAMPLIMIT"))
                    {
                        if ("N".Equals(conditions["type"].ToString()))
                        {
                            writer.WriteLine(optionRow["OPTIONS_STATEMENT"]);
                        }
                    }
                    else
                    {
                        writer.WriteLine(optionRow["OPTIONS_STATEMENT"]);
                    }
                }

                writer.WriteLine("");
                writer.WriteLine("[COORDINATES]");
                foreach (DataRow coordinateRow in coordinateList.Tables["WH_COORDINATES"].Rows)
                {
                    writer.WriteLine(coordinateRow["ID"] + "\t" + coordinateRow["X"] + "\t" + coordinateRow["Y"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[VERTICES]");
                foreach (DataRow verticesRow in verticesList.Tables["WH_VERTICES"].Rows)
                {
                    writer.WriteLine(verticesRow["ID"] + "\t" + verticesRow["X"] + "\t" + verticesRow["Y"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[LABELS]");
                foreach (DataRow labelRow in labelList.Tables["WH_LABELS"].Rows)
                {
                    writer.WriteLine(labelRow["X"] + "\t" + labelRow["Y"] + "\t" + labelRow["REMARK"]);
                }

                writer.WriteLine("");
                writer.WriteLine("[TAGS]");
                foreach (DataRow tagRow in tagList.Tables["WH_TAGS"].Rows)
                {
                    writer.WriteLine(tagRow["TYPE"] + "\t" + tagRow["ID"] + "\t" + tagRow["POSITION_INFO"]);
                }
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.StackTrace);
                throw e1;
            }
            finally
            {
                dbManager.Close();
                writer.Close();
            }

            return null;
        }

        //Section 데이터 리스트 조회
        public Hashtable SelectSectionLists(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            Hashtable result = new Hashtable();

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();

                result.Add("junction", dao.SelectJunctionList(dbManager, conditions));
                result.Add("reservoir", dao.SelectReservoirList(dbManager, conditions));
                result.Add("tank", dao.SelectTankList(dbManager, conditions));
                result.Add("quality", dao.SelectQualityList(dbManager, conditions));
                result.Add("source", dao.SelectSourceList(dbManager, conditions));
                result.Add("emitter", dao.SelectEmitterList(dbManager, conditions));
                result.Add("label", dao.SelectLabelList(dbManager, conditions));
                result.Add("demand", dao.SelectDemandList(dbManager, conditions));
                result.Add("mixing", dao.SelectMixingList(dbManager, conditions));
                result.Add("pipe", dao.SelectPipeList(dbManager, conditions));
                result.Add("coordinate", dao.SelectCoordinateList(dbManager, conditions));
                result.Add("vertices", dao.SelectVerticesList(dbManager, conditions));
                result.Add("pump", dao.SelectPumpList(dbManager, conditions));
                result.Add("valve", dao.SelectValveList(dbManager, conditions));
                result.Add("energy", dao.SelectEnergyList(dbManager, conditions));
                result.Add("curve", dao.SelectCurveList(dbManager, conditions));
                result.Add("pattern", dao.SelectPatternList(dbManager, conditions));
                result.Add("status", dao.SelectStatusList(dbManager, conditions));
                result.Add("control", dao.SelectControlList(dbManager, conditions));
                result.Add("rule", dao.SelectRuleList(dbManager, conditions));
                result.Add("reaction", dao.SelectReactionList(dbManager, conditions));
                result.Add("option", dao.SelectOptionList(dbManager, conditions));
                result.Add("time", dao.SelectTimeOptionList(dbManager, conditions));
                result.Add("report", dao.SelectReportOptionList(dbManager, conditions));
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.StackTrace);
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //INP마스터 정보 수정
        public void UpdateInpMasterData(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["inpMasterData"];

            OracleDBManager dbManager = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                Hashtable scheduleConditions = null;
                Hashtable updateConditions = null;

                foreach (DataRow row in changedTable.Rows)
                {
                    scheduleConditions = new Hashtable();
                    scheduleConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());

                    DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, scheduleConditions);

                    //실시간 작업 등록여부 조회
                    if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                    {
                        throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오. (" + row["INP_NUMBER"] + ")");
                    }

                    updateConditions = new Hashtable();
                    updateConditions.Add("INP_NUMBER", row["INP_NUMBER"].ToString());
                    updateConditions.Add("USE_GBN", row["USE_GBN"].ToString());
                    updateConditions.Add("LFTRIDN", row["LFTRIDN"].ToString());
                    updateConditions.Add("MFTRIDN", row["MFTRIDN"].ToString());
                    updateConditions.Add("SFTRIDN", row["SFTRIDN"].ToString());
                    updateConditions.Add("TITLE", row["TITLE"].ToString());
                    updateConditions.Add("REMARK", row["REMARK"].ToString());

                    dao.UpdateInpMasterData(dbManager, updateConditions);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }
        }

        //Junction 변경정보 저장
        public DataSet SaveJunctionDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Junction정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistJunctionData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_JUNCTIONS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 ID의 JUNCTION이 존재합니다. (ID : " + row["ID"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["ELEV"].ToString());
                        insertConditions.Add(row["DEMAND"].ToString());
                        insertConditions.Add(row["PATTERN_ID"].ToString());
                        insertConditions.Add(row["SFTRIDN"].ToString());
                        insertConditions.Add("");
                        insertConditions.Add("");
                        insertConditions.Add("");
                        insertConditions.Add("");
                        insertConditions.Add(row["REMARK"].ToString());

                        dao.InsertJunctionData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("ELEV", row["ELEV"].ToString());
                        updateConditions.Add("DEMAND", row["DEMAND"].ToString());
                        updateConditions.Add("PATTERN_ID", row["PATTERN_ID"].ToString());
                        updateConditions.Add("SFTRIDN", row["SFTRIDN"].ToString());
                        updateConditions.Add("REMARK", row["REMARK"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdateJunctionData(dbManager, updateConditions);
                    }
                }

                //사용량 배분 다시연산
               
                //블록별 BaseDemand 총량 삭제
                dao.DeleteSumDemandData(dbManager, conditions);

                //블록별 BaseDemand 총량 연산
                DataSet dSet = dao.SelectSumBaseDemand(dbManager, conditions);

                Hashtable baseDemandSumConditions = null;

                //블록별 BaseDemand 총량 입력
                foreach (DataRow row in dSet.Tables["WH_TAGS"].Rows)
                {
                    baseDemandSumConditions = new Hashtable();

                    baseDemandSumConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                    baseDemandSumConditions.Add("BSM_CDE", row["BSM_CDE"].ToString());
                    baseDemandSumConditions.Add("BSM_IDN", row["BSM_IDN"].ToString());
                    baseDemandSumConditions.Add("SUM_BASEDEMAND", row["SUM_BASEDEMAND"].ToString());

                    dao.InsertBlockSumUsage(dbManager, baseDemandSumConditions);
                }

                result = dao.SelectJunctionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Junction 정보 삭제
        public DataSet DeleteJunctionData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteJunctionData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectJunctionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Reservoir 변경정보 저장
        public DataSet SaveReservoirDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Reservoir정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistReservoirData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_RESERVOIRS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 ID의 RESERVOIR가 존재합니다. (ID : " + row["ID"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["HEAD"].ToString());
                        insertConditions.Add(row["PATTERN_ID"].ToString());

                        dao.InsertReservoirData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("HEAD", row["HEAD"].ToString());
                        updateConditions.Add("PATTERN_ID", row["PATTERN_ID"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdateReservoirData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectReservoirList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Reservoir 정보 삭제
        public DataSet DeleteReservoirData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteReservoirData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectReservoirList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Tank 변경정보 저장
        public DataSet SaveTankDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Tank정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistTankData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_TANK"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 ID의 TANK가 존재합니다. (ID : " + row["ID"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["ELEV"].ToString());
                        insertConditions.Add(row["INITLVL"].ToString());
                        insertConditions.Add(row["MINLVL"].ToString());
                        insertConditions.Add(row["MAXLVL"].ToString());
                        insertConditions.Add(row["DIAM"].ToString());
                        insertConditions.Add(row["MINVOL"].ToString());
                        insertConditions.Add(row["VOLCURVE_ID"].ToString());

                        dao.InsertTankData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("ELEV", row["ELEV"].ToString());
                        updateConditions.Add("INITLVL", row["INITLVL"].ToString());
                        updateConditions.Add("MINLVL", row["MINLVL"].ToString());
                        updateConditions.Add("MAXLVL", row["MAXLVL"].ToString());
                        updateConditions.Add("DIAM", row["DIAM"].ToString());
                        updateConditions.Add("MINVOL", row["MINVOL"].ToString());
                        updateConditions.Add("VOLCURVE_ID", row["VOLCURVE_ID"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdateTankData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectTankList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Tank 정보 삭제
        public DataSet DeleteTankData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteTankData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectTankList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Quality 변경정보 저장
        public DataSet SaveQualityDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Quality정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistQualityData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_QUALITY"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 ID의 QUALITY가 존재합니다. (ID : " + row["ID"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["INITQUAL"].ToString());

                        dao.InsertQualityData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("INITQUAL", row["INITQUAL"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdateQualityData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectQualityList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Quality 정보 삭제
        public DataSet DeleteQualityData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteQualityData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectQualityList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Source 변경정보 저장
        public DataSet SaveSourceDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Source정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());
                        existConditions.Add("TYPE", row["TYPE"].ToString());

                        DataSet isExistResult = dao.IsExistSourceData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_SOURCE"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 ID, TYPE의 SOURCE가 존재합니다. (ID : " + row["ID"] + ", TYPE : " + row["TYPE"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["TYPE"].ToString());
                        insertConditions.Add(row["STRENGTH"].ToString());
                        insertConditions.Add(row["PATTERN_ID"].ToString());

                        dao.InsertSourceData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("STRENGTH", row["STRENGTH"].ToString());
                        updateConditions.Add("PATTERN_ID", row["PATTERN_ID"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());
                        updateConditions.Add("TYPE", row["TYPE"].ToString());

                        dao.UpdateSourceData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectSourceList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Source 정보 삭제
        public DataSet DeleteSourceData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteSourceData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectSourceList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Emitter 변경정보 저장
        public DataSet SaveEmitterDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Emitter정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistEmitterData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_EMITTERS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 ID의 EMITTER가 존재합니다. (ID : " + row["ID"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["FLOW_COFFICIENT"].ToString());

                        dao.InsertEmitterData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("FLOW_COFFICIENT", row["FLOW_COFFICIENT"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdateEmitterData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectEmitterList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Emitter 정보 삭제
        public DataSet DeleteEmitterData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteEmitterData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectEmitterList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Label 변경정보 저장
        public DataSet SaveLabelDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Label정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistLabelData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_LABELS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 IDX의 LABEL이 존재합니다. (IDX : " + row["IDX"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["X"].ToString());
                        insertConditions.Add(row["Y"].ToString());
                        insertConditions.Add(row["REMARK"].ToString());

                        dao.InsertLabelData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("X", row["X"].ToString());
                        updateConditions.Add("Y", row["Y"].ToString());
                        updateConditions.Add("REMARK", row["REMARK"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateLabelData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectLabelList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Label 정보 삭제
        public DataSet DeleteLabelData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteLabelData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectLabelList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Demand 변경정보 저장
        public DataSet SaveDemandDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Demand정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());
                        existConditions.Add("CATEGORY", row["CATEGORY"].ToString());

                        DataSet isExistResult = dao.IsExistDemandData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_DEMANDS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Junction/Category의 Demand가 존재합니다. (Junction : " + row["ID"] + ", Category :" + row["CATEGORY"] + " )");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["DEMAND"].ToString());
                        insertConditions.Add(row["PATTERN_ID"].ToString());
                        insertConditions.Add(row["CATEGORY"].ToString());

                        dao.InsertDemandData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("DEMAND", row["DEMAND"].ToString());
                        updateConditions.Add("PATTERN_ID", row["PATTERN_ID"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", conditions["ID"].ToString());
                        updateConditions.Add("CATEGORY", row["CATEGORY"].ToString());

                        dao.UpdateDemandData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectDemandList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Demand 정보 삭제
        public DataSet DeleteDemandData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteDemandData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectDemandList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Mixing 변경정보 저장
        public DataSet SaveMixingDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Mixing정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistMixingData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_MIXING"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Tank의 Mixing Model이 존재합니다. (Tank : " + row["ID"] + " )");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["MODEL"].ToString());
                        insertConditions.Add(row["FLACTION"].ToString());

                        dao.InsertMixingData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("MODEL", row["MODEL"].ToString());
                        updateConditions.Add("FLACTION", row["FLACTION"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", conditions["ID"].ToString());

                        dao.UpdateMixingData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectMixingList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Mixing 정보 삭제
        public DataSet DeleteMixingData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteMixingData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectMixingList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Pipe 변경정보 저장
        public DataSet SavePipeDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Pipe정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistPipeData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_PIPES"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Pipe가 존재합니다. (ID : " + row["ID"] + " )");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["NODE1"].ToString());
                        insertConditions.Add(row["NODE2"].ToString());
                        insertConditions.Add(row["LENGTH"].ToString());
                        insertConditions.Add(row["DIAM"].ToString());
                        insertConditions.Add(row["LEAKAGE_COEFFICIENT"].ToString());
                        insertConditions.Add(row["ROUGHNESS"].ToString());
                        insertConditions.Add(row["MLOSS"].ToString());
                        insertConditions.Add(row["STATUS"].ToString());
                        insertConditions.Add(row["REMARK"].ToString());

                        //dao.InsertPipeData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("NODE1", row["NODE1"].ToString());
                        updateConditions.Add("NODE2", row["NODE2"].ToString());
                        updateConditions.Add("LENGTH", row["LENGTH"].ToString());
                        updateConditions.Add("DIAM", row["DIAM"].ToString());
                        updateConditions.Add("ROUGHNESS", row["ROUGHNESS"].ToString());
                        updateConditions.Add("MLOSS", row["MLOSS"].ToString());
                        updateConditions.Add("STATUS", row["STATUS"].ToString());
                        updateConditions.Add("REMARK", row["REMARK"].ToString());
                        updateConditions.Add("LEAKAGE_COEFFICIENT", row["LEAKAGE_COEFFICIENT"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdatePipeData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectPipeList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Pipe 정보 삭제
        public DataSet DeletePipeData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeletePipeData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectPipeList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Coordinate 변경정보 저장
        public DataSet SaveCoordinateDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Coordinate정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistCoordinateData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_COORDINATES"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 좌표가 존재합니다. (ID : " + row["ID"] + " )");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["X"].ToString());
                        insertConditions.Add(row["Y"].ToString());

                        dao.InsertCoordinateData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("X", row["X"].ToString());
                        updateConditions.Add("Y", row["Y"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdateCoordinateData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectCoordinateList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Coordinate 정보 삭제
        public DataSet DeleteCoordinateData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteCoordinateData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectCoordinateList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Vertices 변경정보 저장
        public DataSet SaveVerticesDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Vertices정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistVerticesData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_VERTICES"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 좌표가 존재합니다. (ID : " + row["ID"] + " , IDX : " + row["IDX"] + " )");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["X"].ToString());
                        insertConditions.Add(row["Y"].ToString());

                        dao.InsertVerticesData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("X", row["X"].ToString());
                        updateConditions.Add("Y", row["Y"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateVerticesData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectVerticesList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Vertices 정보 삭제
        public DataSet DeleteVerticesData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteVerticesData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectVerticesList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Pump 변경정보 저장
        public DataSet SavePumpDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Pump정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistPumpData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_PUMPS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 펌프가 존재합니다. (ID : " + row["ID"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["NODE1"].ToString());
                        insertConditions.Add(row["NODE2"].ToString());
                        insertConditions.Add(row["PROPERTIES"].ToString());

                        dao.InsertPumpData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("NODE1", row["NODE1"].ToString());
                        updateConditions.Add("NODE2", row["NODE2"].ToString());
                        updateConditions.Add("PROPERTIES", row["PROPERTIES"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdatePumpData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectPumpList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Pump 정보 삭제
        public DataSet DeletePumpData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeletePumpData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectPumpList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Valve 변경정보 저장
        public DataSet SaveValveDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Valve정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());

                        DataSet isExistResult = dao.IsExistValveData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_VALVES"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 밸브가 존재합니다. (ID : " + row["ID"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["NODE1"].ToString());
                        insertConditions.Add(row["NODE2"].ToString());
                        insertConditions.Add(row["DIAMETER"].ToString());
                        insertConditions.Add(row["TYPE"].ToString());
                        insertConditions.Add(row["SETTING"].ToString());
                        insertConditions.Add(row["MINORLOSS"].ToString());

                        dao.InsertValveData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("NODE1", row["NODE1"].ToString());
                        updateConditions.Add("NODE2", row["NODE2"].ToString());
                        updateConditions.Add("DIAMETER", row["DIAMETER"].ToString());
                        updateConditions.Add("TYPE", row["TYPE"].ToString());
                        updateConditions.Add("SETTING", row["SETTING"].ToString());
                        updateConditions.Add("MINORLOSS", row["MINORLOSS"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());

                        dao.UpdateValveData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectValveList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Valve 정보 삭제
        public DataSet DeleteValveData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteValveData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectValveList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Energy 변경정보 저장
        public DataSet SaveEnergyDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Energy정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistEnergyData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_ENERGY"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Index의 Energy정보가 존재합니다. (IDX : " + row["IDX"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["ENERGY_STATEMENT"].ToString());

                        dao.InsertEnergyData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("ENERGY_STATEMENT", row["ENERGY_STATEMENT"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateEnergyData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectEnergyList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Energy 정보 삭제
        public DataSet DeleteEnergyData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteEnergyData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectEnergyList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Curve 변경정보 저장
        public DataSet SaveCurveDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Curve정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistCurveData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_CURVES"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 데이터가 존재합니다. (ID : " + row["ID"] + " , IDX : " + row["IDX"] + " )");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["X"].ToString());
                        insertConditions.Add(row["Y"].ToString());

                        dao.InsertCurveData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("X", row["X"].ToString());
                        updateConditions.Add("Y", row["Y"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateCurveData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectCurveList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Curve 정보 삭제
        public DataSet DeleteCurveData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteCurveData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectCurveList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Pattern 변경정보 저장
        public DataSet SavePatternDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                Hashtable insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Pattern정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("PATTERN_ID", row["PATTERN_ID"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistPatternData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_PATTERNS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 데이터가 존재합니다. (ID : " + row["PATTERN_ID"] + " , IDX : " + row["IDX"] + " )");
                        }

                        //추가인경우
                        insertConditions = new Hashtable();

                        insertConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        insertConditions.Add("PATTERN_ID", row["PATTERN_ID"].ToString());
                        insertConditions.Add("IDX", row["IDX"].ToString());
                        insertConditions.Add("MULTIPLIER", row["MULTIPLIER"].ToString());

                        dao.InsertPatternData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("MULTIPLIER", row["MULTIPLIER"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("PATTERN_ID", row["PATTERN_ID"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdatePatternData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectPatternList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Pattern 정보 삭제
        public DataSet DeletePatternData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeletePatternData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectPatternList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Status 변경정보 저장
        public DataSet SaveStatusDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Status정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("ID", row["ID"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistStatusData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_STATUS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 데이터가 존재합니다. (ID : " + row["ID"] + " , IDX : " + row["IDX"] + " )");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["ID"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["STATUS_SETTING"].ToString());

                        dao.InsertStatusData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("STATUS_SETTING", row["STATUS_SETTING"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("ID", row["ID"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateStatusData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectStatusList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Status 정보 삭제
        public DataSet DeleteStatusData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteStatusData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectStatusList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Control 변경정보 저장
        public DataSet SaveControlDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Control정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistControlData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_CONTROLS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Index의 Control정보가 존재합니다. (IDX : " + row["IDX"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["CONTROLS_STATEMENT"].ToString());
                        insertConditions.Add("");

                        dao.InsertControlData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("CONTROLS_STATEMENT", row["CONTROLS_STATEMENT"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateControlData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectControlList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Control 정보 삭제
        public DataSet DeleteControlData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteControlData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectControlList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Rule 변경정보 저장
        public DataSet SaveRuleDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Rule정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistRuleData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_RULES"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Index의 Rule정보가 존재합니다. (IDX : " + row["IDX"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["RULES_STATEMENT"].ToString());
                        insertConditions.Add("");

                        dao.InsertRuleData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("RULES_STATEMENT", row["RULES_STATEMENT"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateRuleData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectRuleList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;

        }

        //Rule 정보 삭제
        public DataSet DeleteRuleData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteRuleData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectRuleList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Reaction 변경정보 저장
        public DataSet SaveReactionDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Reaction정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistReactionData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_REACTIONS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Index의 Reaction정보가 존재합니다. (IDX : " + row["IDX"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["REACTION_STATEMENT"].ToString());
                        insertConditions.Add("");

                        dao.InsertReactionData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("REACTION_STATEMENT", row["REACTION_STATEMENT"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateReactionData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectReactionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Reaction 정보 삭제
        public DataSet DeleteReactionData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteReactionData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectReactionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Option 변경정보 저장
        public DataSet SaveOptionDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Option정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistOptionData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_OPTIONS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Index의 Option정보가 존재합니다. (IDX : " + row["IDX"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["OPTIONS_STATEMENT"].ToString());
                        insertConditions.Add("");

                        dao.InsertNormalOptionData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("OPTIONS_STATEMENT", row["OPTIONS_STATEMENT"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateOptionData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectOptionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e)
            {
                dbManager.RollbackTransaction();
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Option 정보 삭제
        public DataSet DeleteOptionData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteOptionData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectOptionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Time Option 변경정보 저장
        public DataSet SaveTimeOptionDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Time정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistTimeOptionData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_TIMES"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Index의 Time정보가 존재합니다. (IDX : " + row["IDX"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["TIMES_STATEMENT"].ToString());
                        insertConditions.Add("");

                        dao.InsertTimeOptionData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("TIMES_STATEMENT", row["TIMES_STATEMENT"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateTimeOptionData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectTimeOptionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Time Option 정보 삭제
        public DataSet DeleteTimeOptionData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteTimeOptionData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectTimeOptionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Report Option 변경정보 저장
        public DataSet SaveReportOptionDataList(Hashtable conditions)
        {
            DataTable changedTable = (DataTable)conditions["changedTable"];

            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                ArrayList insertConditions = null;
                Hashtable updateConditions = null;
                Hashtable existConditions = null;

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                foreach (DataRow row in changedTable.Rows)
                {
                    if (row.RowState == DataRowState.Added)
                    {
                        //Report정보 존재여부 확인
                        existConditions = new Hashtable();
                        existConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        existConditions.Add("IDX", row["IDX"].ToString());

                        DataSet isExistResult = dao.IsExistReportOptionData(dbManager, existConditions);

                        if (((DataTable)isExistResult.Tables["WH_RPT_OPTIONS"]).Rows.Count != 0)
                        {
                            throw new Exception("이미 같은 Index의 Report정보가 존재합니다. (IDX : " + row["IDX"] + ")");
                        }

                        //추가인경우
                        insertConditions = new ArrayList();

                        //inp parsing 모듈과 같이 사용하기 위해 ArrayList형태로 인자를 만들어야 함 (Remark는 마지막에 붙는 관계로 그사이에 빈값을 채워줌)
                        insertConditions.Add(conditions["INP_NUMBER"].ToString());
                        insertConditions.Add(row["IDX"].ToString());
                        insertConditions.Add(row["REPORT_STATEMENT"].ToString());
                        insertConditions.Add("");

                        dao.InsertReportOptionData(dbManager, insertConditions);
                    }
                    else if (row.RowState == DataRowState.Modified)
                    {
                        //수정인경우
                        updateConditions = new Hashtable();

                        updateConditions.Add("REPORT_STATEMENT", row["REPORT_STATEMENT"].ToString());
                        updateConditions.Add("INP_NUMBER", conditions["INP_NUMBER"].ToString());
                        updateConditions.Add("IDX", row["IDX"].ToString());

                        dao.UpdateReportOptionData(dbManager, updateConditions);
                    }
                }

                result = dao.SelectReportOptionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Report Option 정보 삭제
        public DataSet DeleteReportOptionData(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet scheduleJobList = aDao.SelectRegistedJobList(dbManager, conditions);

                //실시간 작업 등록여부 조회
                if (scheduleJobList.Tables["WH_HYDRA_SCHEDULE"].Rows.Count != 0)
                {
                    throw new Exception("실시간 작업에 등록된 모델입니다. 실시간 작업 등록삭제 후 수정하십시오.");
                }

                dao.DeleteReportOptionData(dbManager, conditions);

                //삭제 후 리스트 재조회
                result = dao.SelectReportOptionList(dbManager, conditions);

                dbManager.CommitTransaction();
            }
            catch (Exception e1)
            {
                dbManager.RollbackTransaction();
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //EPANET K와의 연동을 위해 Export할 INP파일 리스트를 조회
        public DataSet SelectExportINPList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();
                result = dao.SelectExportINPList(dbManager, conditions);
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.StackTrace);
                throw e1;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //단위변환
        //private string ConvertUnit(string unitOption, string value)
        //{
        //    string result = value;

        //    if ("1".Equals(unitOption))
        //    {
        //        //Cubic feet per second
        //        result = (double.Parse(value) * 0.3048 * 60 * 60 * 24).ToString();
        //    }
        //    else if ("2".Equals(unitOption))
        //    {
        //        //Gallons per minute
        //        result = (double.Parse(value) * 0.003785 * 60 * 24).ToString();
        //    }
        //    else if ("3".Equals(unitOption))
        //    {
        //        //Million Gallons per day
        //        result = (double.Parse(value) * 0.003785 * 1000000).ToString();
        //    }
        //    else if ("4".Equals(unitOption))
        //    {
        //        //Imperial MGD
        //        result = (double.Parse(value) * 0.004546 * 1000000).ToString();
        //    }
        //    else if ("5".Equals(unitOption))
        //    {
        //        //Imperial MGD
        //        result = (double.Parse(value) * 0.004546 * 1000000).ToString();
        //    }
        //    else if ("6".Equals(unitOption))
        //    {
        //        //Acre-feet per day
        //        result = (double.Parse(value) * 1233).ToString();
        //    }
        //    else if ("7".Equals(unitOption))
        //    {
        //        //Liter per second
        //        result = (double.Parse(value) * 0.001 * 60 * 60 * 24).ToString();
        //    }
        //    else if ("8".Equals(unitOption))
        //    {
        //        //Million Liter per day
        //        result = (double.Parse(value) * 0.001 * 1000000).ToString();
        //    }
        //    else if ("9".Equals(unitOption))
        //    {
        //        //Cubic meters per hour
        //        result = (double.Parse(value) * 24).ToString();
        //    }

        //    return result;
        //}
    }
}
