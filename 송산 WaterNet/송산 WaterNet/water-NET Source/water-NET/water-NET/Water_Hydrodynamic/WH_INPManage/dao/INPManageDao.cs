﻿/**************************************************************************
 * 파일명   : WHINPManageDao.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.09.29
 * 설명     : INP파일 내용을 핸들링하는 Data Access Object
 * 변경이력 : 2010.09.29 - 최초생성
 **************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using WaterNet.WaterNetCore;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace WaterNet.WH_INPManage.dao
{
    public class INPManageDao
    {
        //외부에서 사용될 참조변수
        private static INPManageDao dao = null;

        //대량의 입력자료가 존재하는 경우 빈번한 객체생성은 메모리 오버플로우의 원인이 된다.
        private StringBuilder queryString = new StringBuilder();

        //기본생성자
        private INPManageDao()
        {
        }

        //참조변수를 반환
        public static INPManageDao GetInstance()
        {
            if(dao == null)
            {
                dao = new INPManageDao();
            }

            return dao;
        }

        //해당블록에 등록된 실시간 관망해석 모델이 있는지 확인
        public DataSet IsExistINPData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      INP_NUMBER                                      ");
            queryString.AppendLine("from        WH_TITLE                                        ");
            queryString.AppendLine("where       USE_GBN     = '" + conditions["USE_GBN"] + "'   ");
            queryString.AppendLine("and         LFTRIDN     = '" + conditions["LFTRIDN"] + "'   ");
            queryString.AppendLine("and         MFTRIDN     = '" + conditions["MFTRIDN"] + "'   ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //INP Master정보 입력
        public void InsertINPMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder   queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_TITLE   (                                                        ");
            queryString.AppendLine("                            INP_NUMBER                                          ");
            queryString.AppendLine("                            ,USE_GBN                                            ");
            queryString.AppendLine("                            ,WSP_NAM                                            ");
            queryString.AppendLine("                            ,LFTRIDN                                            ");
            queryString.AppendLine("                            ,MFTRIDN                                            ");
            queryString.AppendLine("                            ,SFTRIDN                                            ");
            queryString.AppendLine("                            ,INS_DATE                                           ");
            queryString.AppendLine("                            ,TITLE                                              ");
            queryString.AppendLine("                            ,FILE_NAME                                          ");
            queryString.AppendLine("                            ,REMARK                                             ");
            queryString.AppendLine("                            ,ENERGY_GBN                                         ");
            queryString.AppendLine("                        )                                                       ");
            queryString.AppendLine("values                  (                                                       ");
            queryString.AppendLine("                            :1                                                  ");
            queryString.AppendLine("                            ,:2                                                 ");
            queryString.AppendLine("                            ,:3                                                 ");
            queryString.AppendLine("                            ,:4                                                 ");
            queryString.AppendLine("                            ,:5                                                 ");
            queryString.AppendLine("                            ,:6                                                 ");
            queryString.AppendLine("                            ,to_char(sysdate,'yyyymmddhh24miss')                ");
            queryString.AppendLine("                            ,:8                                                 ");
            queryString.AppendLine("                            ,:9                                                 ");
            queryString.AppendLine("                            ,:10                                                ");
            queryString.AppendLine("                            ,:11                                                ");
            queryString.AppendLine("                        )                                                       ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
                ,new OracleParameter("10", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["USE_GBN"];
            parameters[2].Value = (string)conditions["WSP_NAM"];
            parameters[3].Value = (string)conditions["LFTRIDN"];
            parameters[4].Value = (string)conditions["MFTRIDN"];
            parameters[5].Value = (string)conditions["SFTRIDN"];
            parameters[6].Value = (string)conditions["TITLE"];
            parameters[7].Value = (string)conditions["FILE_NAME"];
            parameters[8].Value = (string)conditions["REMARK"];
            parameters[9].Value = (string)conditions["ENERGY_GBN"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //INP Master리스트 조회
        public DataSet SelectINPMasterList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.INP_NUMBER                                                                                        ");
            queryString.AppendLine("            ,a.USE_GBN                                                                                          ");
            queryString.AppendLine("            ,e.CODE_NAME       as USE_GBN_NAME                                                                  ");
            queryString.AppendLine("            ,a.LFTRIDN                                                                                          ");
            queryString.AppendLine("            ,b.LOC_NAME        as LFTR_NAME                                                                     ");
            queryString.AppendLine("            ,a.MFTRIDN                                                                                          ");
            queryString.AppendLine("            ,c.LOC_NAME        as MFTR_NAME                                                                     ");
            queryString.AppendLine("            ,a.SFTRIDN                                                                                          ");
            queryString.AppendLine("            ,d.LOC_NAME        as SFTR_NAME                                                                     ");
            queryString.AppendLine("            ,to_char(to_date(a.INS_DATE,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')      as INS_DATE      ");
            queryString.AppendLine("            ,a.TITLE                                                                                            ");
            queryString.AppendLine("            ,a.REMARK                                                                                           ");
            //queryString.AppendLine("            ,a.FILE_NAME                                                                                        ");
            queryString.AppendLine("from        WH_TITLE                 a                                                                          ");
            queryString.AppendLine("            ,CM_LOCATION             b                                                                          ");
            queryString.AppendLine("            ,CM_LOCATION             c                                                                          ");
            queryString.AppendLine("            ,CM_LOCATION             d                                                                          ");
            queryString.AppendLine("            ,CM_CODE                 e                                                                          ");
            queryString.AppendLine("where       1 = 1                                                                                               ");

            if (conditions["USE_GBN"] != null)
            {
                queryString.AppendLine("and     a.USE_GBN     = '" + conditions["USE_GBN"] + "'                                                     ");
            }

            if (conditions["LFTRIDN"] != null)
            {
                queryString.AppendLine("and     a.LFTRIDN     = '" + conditions["LFTRIDN"] + "'                                                     ");
            }

            if (conditions["MFTRIDN"] != null)
            {
                queryString.AppendLine("and     a.MFTRIDN     = '" + conditions["MFTRIDN"] + "'                                                     ");
            }

            if (conditions["SFTRIDN"] != null)
            {
                queryString.AppendLine("and     a.SFTRIDN     = '" + conditions["SFTRIDN"] + "'                                                     ");
            }

            //if (conditions["startDate"] != null && conditions["endDate"] != null)
            //{
            //    queryString.AppendLine("and         a.INS_DATE                                                                                      ");
            //    queryString.AppendLine("between     '" + conditions["startDate"] + "'                                                               ");
            //    queryString.AppendLine("and         '" + conditions["endDate"] + "235959'                                                           ");
            //}
 
            if (conditions["TITLE"] != null)
            {
                queryString.AppendLine("and     a.TITLE       like '%" + conditions["TITLE"] + "%'                                                  ");
            }

            queryString.AppendLine("and         a.LFTRIDN   = b.LOC_CODE(+)                                                                         ");
            queryString.AppendLine("and         a.MFTRIDN   = c.LOC_CODE(+)                                                                         ");
            queryString.AppendLine("and         a.SFTRIDN   = d.LOC_CODE(+)                                                                         ");
            queryString.AppendLine("and         a.USE_GBN   = e.CODE(+)                                                                             ");
            queryString.AppendLine("and         e.PCODE(+)     = '1001'                                                                             ");
            queryString.AppendLine("order by    to_date(a.INS_DATE,'yyyy-mm-dd hh24:mi:ss')                                                         ");
            queryString.AppendLine("desc                                                                                                            ");
            queryString.AppendLine("            ,a.LFTRIDN                                                                                          ");
            queryString.AppendLine("            ,a.MFTRIDN                                                                                          ");
            queryString.AppendLine("            ,a.SFTRIDN                                                                                          ");
            queryString.AppendLine("asc                                                                                                             ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //INP Master 정보 조회
        public DataSet SelectINPMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      INP_NUMBER                                                                                      ");
            queryString.AppendLine("            ,USE_GBN                                                                                        ");
            queryString.AppendLine("            ,LFTRIDN                                                                                        ");
            queryString.AppendLine("            ,MFTRIDN                                                                                        ");
            queryString.AppendLine("            ,SFTRIDN                                                                                        ");
            queryString.AppendLine("            ,to_char(to_date(INS_DATE,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')      as INS_DATE    ");
            queryString.AppendLine("            ,TITLE                                                                                          ");
            queryString.AppendLine("            ,REMARK                                                                                         ");
            queryString.AppendLine("            ,FILE_NAME                                                                                      ");
            queryString.AppendLine("from        WH_TITLE                                                                                        ");
            queryString.AppendLine("where       1 = 1                                                                                           ");
            queryString.AppendLine("and         INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'                                                ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //INP Master 정보 수정
        public void UpdateInpMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update      WH_TITLE set                                    ");
            queryString.AppendLine("		    USE_GBN	= '" + conditions["USE_GBN"] + "'		");
            queryString.AppendLine("		    ,LFTRIDN	= '" + conditions["LFTRIDN"] + "'	");
            queryString.AppendLine("		    ,MFTRIDN	= '" + conditions["MFTRIDN"] + "'	");
            queryString.AppendLine("		    ,SFTRIDN	= '" + conditions["SFTRIDN"] + "'	");
            queryString.AppendLine("		    ,TITLE		= '" + conditions["TITLE"] + "'		");
            queryString.AppendLine("		    ,REMARK		= '" + conditions["REMARK"] + "'	");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //INP Master 정보 삭제
        public void DeleteInpMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_TITLE                                        ");
            queryString.AppendLine("where       INP_NUMBER = '" + conditions["INP_NUMBER"] + "' ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //junction 정보 입력 ###블럭정보 입력해야함
        public void InsertJunctionData(OracleDBManager dbManager, ArrayList conditions)
        {
            //StringBuilder   queryString = new StringBuilder();
            queryString.Remove(0, queryString.Length);

            queryString.AppendLine("insert into WH_JUNCTIONS    (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,ID                     ");
            queryString.AppendLine("                                ,ELEV                   ");
            queryString.AppendLine("                                ,DEMAND                 ");
            //queryString.AppendLine("                                ,DEVIDED_DEMAND         ");
            queryString.AppendLine("                                ,PATTERN_ID             ");
            queryString.AppendLine("                                ,SFTRIDN                ");
            queryString.AppendLine("                                ,REMARK                 ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                                ,:5                     ");
            queryString.AppendLine("                                ,:6                     ");
            queryString.AppendLine("                                ,:7                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim().Length < 16 ? ((string)conditions[3]).Trim() : ((string)conditions[3]).Trim().Substring(0, 15);
            //parameters[4].Value = "0";
            parameters[4].Value = ((string)conditions[4]).Trim();
            parameters[5].Value = (string)conditions[5];            //블록코드는 5번째
            parameters[6].Value = (string)conditions[10];           //주석은 10번째 위치

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //junction 리스트 조회
        public DataSet SelectJunctionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.ID				                                ");
            queryString.AppendLine("            ,a.ELEV				                                ");
            queryString.AppendLine("            ,a.DEMAND				                            ");
            queryString.AppendLine("            ,a.PATTERN_ID			                            ");
            queryString.AppendLine("            ,a.SFTRIDN			                                ");
            queryString.AppendLine("            ,b.LOC_NAME             as SFTR_NAME			    ");
            queryString.AppendLine("            ,a.REMARK				                            ");
            queryString.AppendLine("            ,''                     as ACTION                   ");
            queryString.AppendLine("from        WH_JUNCTIONS            a		                    ");
            queryString.AppendLine("            ,CM_LOCATION            b                           ");
            queryString.AppendLine("where       b.FTR_CODE(+)   = 'BZ003'                           ");
            queryString.AppendLine("and         a.SFTRIDN       = b.FTR_IDN(+)                      ");
            queryString.AppendLine("and         INP_NUMBER      = '"+ conditions["INP_NUMBER"] +"'	");
            
            //queryString.AppendLine("and         a.DEMAND ='a'");
            
            queryString.AppendLine("order by    ID				                                    ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_JUNCTIONS");
        }

        //Junction 정보 존재여부 확인
        public DataSet IsExistJunctionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_JUNCTIONS                                        ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_JUNCTIONS");
        }

        //junction 수정
        public void UpdateJunctionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_JUNCTIONS    set                                     ");
            queryString.AppendLine("        ELEV            = '" + conditions["ELEV"] + "'          ");
            queryString.AppendLine("        ,DEMAND         = '" + conditions["DEMAND"] + "'        ");
            queryString.AppendLine("        ,PATTERN_ID     = '" + conditions["PATTERN_ID"] + "'    ");
            queryString.AppendLine("        ,SFTRIDN        = '" + conditions["SFTRIDN"] + "'       ");
            queryString.AppendLine("        ,REMARK         = '" + conditions["REMARK"] + "'        ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"]+"'      ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"]+ "'             ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Junction 삭제
        public void DeleteJunctionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_JUNCTIONS                                        ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if(conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //RESERVOIR 정보 입력
        public void InsertReservoirData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RESERVOIRS   (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,ID                     ");
            queryString.AppendLine("                                ,HEAD                   ");
            queryString.AppendLine("                                ,PATTERN_ID             ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //RESERVOIR 리스트 조회
        public DataSet SelectReservoirList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,HEAD                                                   ");
            queryString.AppendLine("            ,PATTERN_ID                                             ");
            queryString.AppendLine("from        WH_RESERVOIRS                                           ");
            queryString.AppendLine("where       INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RESERVOIRS");
        }

        //RESERVOIR 정보 존재여부 확인
        public DataSet IsExistReservoirData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_RESERVOIRS                                       ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RESERVOIRS");
        }

        //RESERVOIR 정보 수정
        public void UpdateReservoirData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_RESERVOIRS    set                                    ");
            queryString.AppendLine("        HEAD            = '" + conditions["HEAD"] + "'          ");
            queryString.AppendLine("        ,PATTERN_ID     = '" + conditions["PATTERN_ID"] + "'    ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //RESERVOIR 정보 삭제
        public void DeleteReservoirData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_RESERVOIRS                                        ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //TANK 정보 입력
        public void InsertTankData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_TANK (                           ");
            queryString.AppendLine("                        INP_NUMBER              ");
            queryString.AppendLine("                        ,ID                     ");
            queryString.AppendLine("                        ,ELEV                   ");
            queryString.AppendLine("                        ,INITLVL                ");
            queryString.AppendLine("                        ,MINLVL                 ");
            queryString.AppendLine("                        ,MAXLVL                 ");
            queryString.AppendLine("                        ,DIAM                   ");
            queryString.AppendLine("                        ,MINVOL                 ");
            queryString.AppendLine("                        ,VOLCURVE_ID            ");
            queryString.AppendLine("                    )                           ");
            queryString.AppendLine("values              (                           ");
            queryString.AppendLine("                        :1                      ");
            queryString.AppendLine("                        ,:2                     ");
            queryString.AppendLine("                        ,:3                     ");
            queryString.AppendLine("                        ,:4                     ");
            queryString.AppendLine("                        ,:5                     ");
            queryString.AppendLine("                        ,:6                     ");
            queryString.AppendLine("                        ,:7                     ");
            queryString.AppendLine("                        ,:8                     ");
            queryString.AppendLine("                        ,:9                     ");
            queryString.AppendLine("                    )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();
            parameters[4].Value = ((string)conditions[4]).Trim();
            parameters[5].Value = ((string)conditions[5]).Trim();
            parameters[6].Value = ((string)conditions[6]).Trim();
            parameters[7].Value = ((string)conditions[7]).Trim();
            parameters[8].Value = ((string)conditions[8]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //TANK 리스트 조회
        public DataSet SelectTankList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,ELEV                                                   ");
            queryString.AppendLine("            ,INITLVL                                                ");
            queryString.AppendLine("            ,MINLVL                                                 ");
            queryString.AppendLine("            ,MAXLVL                                                 ");
            queryString.AppendLine("            ,DIAM                                                   ");
            queryString.AppendLine("            ,MINVOL                                                 ");
            queryString.AppendLine("            ,VOLCURVE_ID                                            ");
            queryString.AppendLine("from        WH_TANK                                                 ");
            queryString.AppendLine("where       INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TANK");
        }

        //TANK 정보 존재여부 확인
        public DataSet IsExistTankData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_TANK                                             ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TANK");
        }

        //TANK 정보 수정
        public void UpdateTankData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_TANK    set                                          ");
            queryString.AppendLine("        ELEV            = '" + conditions["ELEV"] + "'          ");
            queryString.AppendLine("        ,INITLVL        = '" + conditions["INITLVL"] + "'       ");
            queryString.AppendLine("        ,MINLVL         = '" + conditions["MINLVL"] + "'        ");
            queryString.AppendLine("        ,MAXLVL         = '" + conditions["MAXLVL"] + "'        ");
            queryString.AppendLine("        ,DIAM           = '" + conditions["DIAM"] + "'          ");
            queryString.AppendLine("        ,MINVOL         = '" + conditions["MINVOL"] + "'        ");
            queryString.AppendLine("        ,VOLCURVE_ID    = '" + conditions["VOLCURVE_ID"] + "'   ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //TANK 정보 삭제
        public void DeleteTankData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_TANK                                             ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Pipe 정보 입력
        public void InsertPipeData(OracleDBManager dbManager, ArrayList conditions, string idx)
        {
            //StringBuilder queryString = new StringBuilder();
            queryString.Remove(0, queryString.Length);

            //누수계수가 기본으로 적용된 inp만 등록이 가능하다.
            queryString.AppendLine("insert into WH_PIPES    (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,NODE1                  ");
            queryString.AppendLine("                            ,NODE2                  ");
            queryString.AppendLine("                            ,LENGTH                 ");
            queryString.AppendLine("                            ,DIAM                   ");
            queryString.AppendLine("                            ,LEAKAGE_COEFFICIENT    ");
            queryString.AppendLine("                            ,ROUGHNESS              ");
            queryString.AppendLine("                            ,MLOSS                  ");
            queryString.AppendLine("                            ,STATUS                 ");
            queryString.AppendLine("                            ,REMARK                 ");
            queryString.AppendLine("                            ,SFTRIDN                ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                            ,:6                     ");
            queryString.AppendLine("                            ,:7                     ");
            queryString.AppendLine("                            ,:8                     ");
            queryString.AppendLine("                            ,:9                     ");
            queryString.AppendLine("                            ,:10                    ");
            queryString.AppendLine("                            ,:11                    ");
            queryString.AppendLine("                            ,:12                    ");
            queryString.AppendLine("                            ,:13                    ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
                ,new OracleParameter("10", OracleDbType.Varchar2)
                ,new OracleParameter("11", OracleDbType.Varchar2)
                ,new OracleParameter("12", OracleDbType.Varchar2)
                ,new OracleParameter("13", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();
            parameters[4].Value = ((string)conditions[4]).Trim();
            parameters[5].Value = ((string)conditions[5]).Trim();
            parameters[6].Value = ((string)conditions[6]).Trim();
            parameters[7].Value = ((string)conditions[7]).Trim();
            parameters[8].Value = ((string)conditions[8]).Trim();
            parameters[9].Value = ((string)conditions[9]).Trim();
            parameters[10].Value = ((string)conditions[10]).Trim();                  //주석은 12번째 위치
            parameters[11].Value = "소블록";
            parameters[12].Value = idx;

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Pipe 리스트 조회
        public DataSet SelectPipeList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,LENGTH                                                 ");
            queryString.AppendLine("            ,DIAM                                                   ");
            queryString.AppendLine("            ,ROUGHNESS                                              ");
            queryString.AppendLine("            ,MLOSS                                                  ");
            queryString.AppendLine("            ,STATUS                                                 ");
            queryString.AppendLine("            ,REMARK                                                 ");
            //queryString.AppendLine("            ,SFTRIDN                                                ");
            queryString.AppendLine("            ,LEAKAGE_COEFFICIENT                                    ");
            queryString.AppendLine("from        WH_PIPES                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PIPES");
        }  

        //Pipe 정보 존재여부 확인
        public DataSet IsExistPipeData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_PIPES                                            ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PIPES");
        }

        //Pipe 정보 수정
        public void UpdatePipeData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_PIPES    set                                                         ");
            queryString.AppendLine("        NODE1                   = '" + conditions["NODE1"] + "'                 ");
            queryString.AppendLine("        ,NODE2                  = '" + conditions["NODE2"] + "'                 ");
            queryString.AppendLine("        ,LENGTH                 = '" + conditions["LENGTH"] + "'                ");
            queryString.AppendLine("        ,DIAM                   = '" + conditions["DIAM"] + "'                  ");
            queryString.AppendLine("        ,ROUGHNESS              = '" + conditions["ROUGHNESS"] + "'             ");
            queryString.AppendLine("        ,MLOSS                  = '" + conditions["MLOSS"] + "'                 ");
            queryString.AppendLine("        ,STATUS                 = '" + conditions["STATUS"] + "'                ");
            queryString.AppendLine("        ,REMARK                 = '" + conditions["REMARK"] + "'                ");
            queryString.AppendLine("        ,LEAKAGE_COEFFICIENT    = '" + conditions["LEAKAGE_COEFFICIENT"] + "'   ");
            queryString.AppendLine("where   INP_NUMBER              = '" + conditions["INP_NUMBER"] + "'            ");
            queryString.AppendLine("and     ID                      = '" + conditions["ID"] + "'                    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Pipe 정보 삭제
        public void DeletePipeData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_PIPES                                           ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Pump 정보 입력
        public void InsertPumpData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_PUMPS    (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,NODE1                  ");
            queryString.AppendLine("                            ,NODE2                  ");
            queryString.AppendLine("                            ,PROPERTIES             ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();
            parameters[4].Value = ((string)conditions[4]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Pump 리스트 조회
        public DataSet SelectPumpList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,PROPERTIES                                             ");
            queryString.AppendLine("from        WH_PUMPS                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PUMPS");
        }

        //Pump 정보 존재여부 확인
        public DataSet IsExistPumpData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_PUMPS                                            ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PUMPS");
        }

        //Pump 정보 수정
        public void UpdatePumpData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_PUMPS    set                                                         ");
            queryString.AppendLine("        NODE1                   = '" + conditions["NODE1"] + "'                 ");
            queryString.AppendLine("        ,NODE2                  = '" + conditions["NODE2"] + "'                 ");
            queryString.AppendLine("        ,PROPERTIES             = '" + conditions["PROPERTIES"] + "'            ");
            queryString.AppendLine("where   INP_NUMBER              = '" + conditions["INP_NUMBER"] + "'            ");
            queryString.AppendLine("and     ID                      = '" + conditions["ID"] + "'                    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Pump 정보 삭제
        public void DeletePumpData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_PUMPS                                           ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Valve 정보 입력
        public void InsertValveData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_VALVES   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,NODE1                  ");
            queryString.AppendLine("                            ,NODE2                  ");
            queryString.AppendLine("                            ,DIAMETER               ");
            queryString.AppendLine("                            ,TYPE                   ");
            queryString.AppendLine("                            ,SETTING                ");
            queryString.AppendLine("                            ,MINORLOSS              ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                            ,:6                     ");
            queryString.AppendLine("                            ,:7                     ");
            queryString.AppendLine("                            ,:8                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();
            parameters[4].Value = ((string)conditions[4]).Trim();
            parameters[5].Value = ((string)conditions[5]).Trim();
            parameters[6].Value = ((string)conditions[6]).Trim();
            parameters[7].Value = ((string)conditions[7]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Valve 리스트 조회
        public DataSet SelectValveList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,DIAMETER                                               ");
            queryString.AppendLine("            ,TYPE                                                   ");
            queryString.AppendLine("            ,SETTING                                                ");
            queryString.AppendLine("            ,MINORLOSS                                              ");
            queryString.AppendLine("from        WH_VALVES                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_VALVES");
        }

        //Valve 정보 존재여부 확인
        public DataSet IsExistValveData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_VALVES                                           ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_VALVES");
        }

        //Valve 정보 수정
        public void UpdateValveData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_VALVES    set                                        ");
            queryString.AppendLine("        NODE1           = '" + conditions["NODE1"] + "'         ");
            queryString.AppendLine("        ,NODE2          = '" + conditions["NODE2"] + "'         ");
            queryString.AppendLine("        ,DIAMETER       = '" + conditions["DIAMETER"] + "'      ");
            queryString.AppendLine("        ,TYPE           = '" + conditions["TYPE"] + "'          ");
            queryString.AppendLine("        ,SETTING        = '" + conditions["SETTING"] + "'       ");
            queryString.AppendLine("        ,MINORLOSS      = '" + conditions["MINORLOSS"] + "'     ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Valve 정보 삭제
        public void DeleteValveData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_VALVES                                           ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Demand 정보 입력
        public void InsertDemandData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_DEMANDS  (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,DEMAND                 ");
            queryString.AppendLine("                            ,PATTERN_ID             ");
            queryString.AppendLine("                            ,CATEGORY               ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();
            parameters[4].Value = ((string)conditions[4]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Demand 리스트 조회
        public DataSet SelectDemandList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,CATEGORY                                               ");
            queryString.AppendLine("            ,DEMAND                                                 ");
            queryString.AppendLine("            ,PATTERN_ID                                             ");
            queryString.AppendLine("from        WH_DEMANDS                                              ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_DEMANDS");
        }

        //Demand 정보 존재여부 확인
        public DataSet IsExistDemandData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("                ,CATEGORY                                           ");
            queryString.AppendLine("from            WH_DEMANDS                                          ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("and             CATEGORY    = '" + conditions["CATEGORY"] + "'      ");


            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_DEMANDS");
        }

        //Demand 정보 수정
        public void UpdateDemandData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_DEMANDS    set                                       ");
            queryString.AppendLine("        DEMAND          = '" + conditions["DEMAND"] + "'        ");
            queryString.AppendLine("        ,PATTERN_ID     = '" + conditions["PATTERN_ID"] + "'    ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("and     CATEGORY        = '" + conditions["CATEGORY"] + "'      ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Demand 정보 삭제
        public void DeleteDemandData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_DEMANDS                                          ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID,CATEGORY는 넘어오지 않는다.
            if (conditions["ID"] != null && conditions["CATEGORY"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
                queryString.AppendLine("and     CATEGORY    = '" + conditions["CATEGORY"] + "'      ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Status 정보 입력
        public void InsertStatusData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_STATUS   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,STATUS_SETTING         ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Status 정보 조회
        public DataSet SelectStatusList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,IDX                                                    ");
            queryString.AppendLine("            ,STATUS_SETTING                                         ");
            queryString.AppendLine("from        WH_STATUS                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_STATUS");
        }

        //Status 정보 존재여부 확인
        public DataSet IsExistStatusData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("                ,IDX                                                ");
            queryString.AppendLine("from            WH_STATUS                                           ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_STATUS");
        }

        //Status 정보 수정
        public void UpdateStatusData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_STATUS    set                                            ");
            queryString.AppendLine("        STATUS_SETTING  = '" + conditions["STATUS_SETTING"] + "'    ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'        ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'                ");
            queryString.AppendLine("and     IDX             = '" + conditions["IDX"] + "'               ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Status 정보 삭제
        public void DeleteStatusData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_STATUS                                           ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null && conditions["IDX"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Curve 정보 입력
        public void InsertCurveData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_CURVES   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,X                      ");
            queryString.AppendLine("                            ,Y                      ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = ((string)conditions[3]).Trim();
            parameters[4].Value = ((string)conditions[4]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Curve 리스트 조회
        public DataSet SelectCurveList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,IDX                                                    ");
            queryString.AppendLine("            ,X                                                      ");
            queryString.AppendLine("            ,Y                                                      ");
            queryString.AppendLine("from        WH_CURVES                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");
            queryString.AppendLine("            ,to_number(IDX)                                         ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_CURVES");
        }

        //Curve 정보 존재여부 확인
        public DataSet IsExistCurveData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("                ,IDX                                                ");
            queryString.AppendLine("from            WH_CURVES                                           ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_CURVES");
        }

        //Curve 정보 수정
        public void UpdateCurveData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_CURVES    set                                        ");
            queryString.AppendLine("        X               = '" + conditions["X"] + "'             ");
            queryString.AppendLine("        ,Y              = '" + conditions["Y"] + "'             ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("and     IDX             = '" + conditions["IDX"] + "'           ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Curve 정보 삭제
        public void DeleteCurveData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_CURVES                                           ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null && conditions["IDX"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Pattern 정보 입력
        public void InsertPatternData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_PATTERNS     (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,PATTERN_ID             ");
            queryString.AppendLine("                                ,IDX                    ");
            queryString.AppendLine("                                ,MULTIPLIER             ");
            queryString.AppendLine("                                ,REMARK                 ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                                ,:5                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = ((string)conditions["PATTERN_ID"]).Trim();
            parameters[2].Value = (string)conditions["IDX"];
            parameters[3].Value = ((string)conditions["MULTIPLIER"]).Trim();
            parameters[4].Value = "1";

            //Console.WriteLine(conditions["INP_NUMBER"] + "," + (string)conditions["PATTERN_ID"] + "," + (string)conditions["IDX"] + "," + (string)conditions["MULTIPLIER"]);

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //패턴 데이터 Bulk Insert
        public void InsertPatternBulkData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_PATTERNS     (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,PATTERN_ID             ");
            queryString.AppendLine("                                ,IDX                    ");
            queryString.AppendLine("                                ,MULTIPLIER             ");
            queryString.AppendLine("                                ,REMARK                 ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                                ,:5                     ");
            queryString.AppendLine("                            )                           ");

            OracleCommand commandOracle = new OracleCommand();
            commandOracle.CommandText = queryString.ToString();
            commandOracle.Connection = dbManager.Connection;
            commandOracle.ArrayBindCount = ((List<string>)conditions["inpNumberList"]).Count;

            OracleParameter prmInpNumberList = new OracleParameter("1", OracleDbType.Varchar2);
            prmInpNumberList.Direction = ParameterDirection.Input;
            prmInpNumberList.Value = ((List<string>)conditions["inpNumberList"]).ToArray();
            commandOracle.Parameters.Add(prmInpNumberList);

            OracleParameter prmPatternIdList = new OracleParameter("2", OracleDbType.Varchar2);
            prmPatternIdList.Direction = ParameterDirection.Input;
            prmPatternIdList.Value = ((List<string>)conditions["patternIdList"]).ToArray();
            commandOracle.Parameters.Add(prmPatternIdList);

            OracleParameter prmIdxList = new OracleParameter("3", OracleDbType.Varchar2);
            prmIdxList.Direction = ParameterDirection.Input;
            prmIdxList.Value = ((List<string>)conditions["idxList"]).ToArray();
            commandOracle.Parameters.Add(prmIdxList);

            OracleParameter prmMultiplierList = new OracleParameter("4", OracleDbType.Varchar2);
            prmMultiplierList.Direction = ParameterDirection.Input;
            prmMultiplierList.Value = ((List<string>)conditions["multiplierList"]).ToArray();
            commandOracle.Parameters.Add(prmMultiplierList);

            OracleParameter prmRemarkList = new OracleParameter("5", OracleDbType.Varchar2);
            prmRemarkList.Direction = ParameterDirection.Input;
            prmRemarkList.Value = ((List<string>)conditions["remarkList"]).ToArray();
            commandOracle.Parameters.Add(prmRemarkList);

            commandOracle.ExecuteNonQuery();
        }

        //Pattern 리스트 조회
        public DataSet SelectPatternList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      PATTERN_ID                                              ");
            queryString.AppendLine("            ,IDX                                                    ");
            queryString.AppendLine("            ,MULTIPLIER                                             ");
            queryString.AppendLine("from        WH_PATTERNS                                             ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    PATTERN_ID                                              ");
            queryString.AppendLine("            ,to_number(IDX)                                         ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PATTERNS");
        }

        //Pattern 정보 존재여부 확인
        public DataSet IsExistPatternData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          PATTERN_ID                                          ");
            queryString.AppendLine("                ,IDX                                                ");
            queryString.AppendLine("from            WH_PATTERNS                                         ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             PATTERN_ID  = '" + conditions["PATTERN_ID"] + "'    ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PATTERNS");
        }

        //Pattern 정보 수정
        public void UpdatePatternData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_PATTERNS    set                                      ");
            queryString.AppendLine("        MULTIPLIER      = '" + conditions["MULTIPLIER"] + "'    ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     PATTERN_ID      = '" + conditions["PATTERN_ID"] + "'    ");
            queryString.AppendLine("and     IDX             = '" + conditions["IDX"] + "'           ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Pattern 정보 삭제
        public void DeletePatternData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_PATTERNS                                         ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["PATTERN_ID"] != null && conditions["IDX"] != null)
            {
                queryString.AppendLine("and     PATTERN_ID  = '" + conditions["PATTERN_ID"] + "'    ");
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Control 정보 입력
        public void InsertControlData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_CONTROLS (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,CONTROLS_STATEMENT     ");
            queryString.AppendLine("                            ,REMARK                 ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();            //주석 - work에서 argument를 재생성했기때문에 idx가 10이 아님

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Control 리스트 조회
        public DataSet SelectControlList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,CONTROLS_STATEMENT                                     ");
            queryString.AppendLine("from        WH_CONTROLS                                             ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_CONTROLS");
        }

        //Control 정보 존재여부 확인
        public DataSet IsExistControlData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          IDX                                                 ");
            queryString.AppendLine("from            WH_CONTROLS                                         ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_CONTROLS");
        }

        //Control 정보 수정
        public void UpdateControlData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_CONTROLS    set                                                      ");
            queryString.AppendLine("        CONTROLS_STATEMENT      = '" + conditions["CONTROLS_STATEMENT"] + "'    ");
            queryString.AppendLine("where   INP_NUMBER              = '" + conditions["INP_NUMBER"] + "'            ");
            queryString.AppendLine("and     IDX                     = '" + conditions["IDX"] + "'                   ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Control 정보 삭제
        public void DeleteControlData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_CONTROLS                                         ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["IDX"] != null)
            {
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Rule 정보 입력
        public void InsertRuleData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RULES    (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,RULES_STATEMENT        ");
            queryString.AppendLine("                            ,REMARK                 ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();            //주석 - work에서 argument를 재생성했기때문에 idx가 10이 아님

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Rule 리스트 조회
        public DataSet SelectRuleList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,RULES_STATEMENT                                        ");
            queryString.AppendLine("            ,REMARK                                                 ");
            queryString.AppendLine("from        WH_RULES                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RULES");
        }

        //Rule 정보 존재여부 확인
        public DataSet IsExistRuleData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          IDX                                                 ");
            queryString.AppendLine("from            WH_RULES                                            ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RULES");
        }

        //Rule 정보 수정
        public void UpdateRuleData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_RULES    set                                                 ");
            queryString.AppendLine("        RULES_STATEMENT     = '" + conditions["RULES_STATEMENT"] + "'   ");
            queryString.AppendLine("where   INP_NUMBER          = '" + conditions["INP_NUMBER"] + "'        ");
            queryString.AppendLine("and     IDX                 = '" + conditions["IDX"] + "'               ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Rule 정보 삭제
        public void DeleteRuleData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_RULES                                            ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["IDX"] != null)
            {
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Energy 정보 입력
        public void InsertEnergyData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_ENERGY   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,ENERGY_STATEMENT       ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = ((string)conditions[2]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Energy 리스트 조회
        public DataSet SelectEnergyList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,ENERGY_STATEMENT                                       ");
            queryString.AppendLine("from        WH_ENERGY                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_ENERGY");
        }

        //Energy 정보 존재여부 확인
        public DataSet IsExistEnergyData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          IDX                                                 ");
            queryString.AppendLine("from            WH_ENERGY                                           ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_ENERGY");
        }

        //Energy 정보 수정
        public void UpdateEnergyData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_ENERGY    set                                                ");
            queryString.AppendLine("        ENERGY_STATEMENT    = '" + conditions["ENERGY_STATEMENT"] + "'  ");
            queryString.AppendLine("where   INP_NUMBER          = '" + conditions["INP_NUMBER"] + "'        ");
            queryString.AppendLine("and     IDX                 = '" + conditions["IDX"] + "'               ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Energy 정보 삭제
        public void DeleteEnergyData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_ENERGY                                           ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["IDX"] != null)
            {
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }
        
        //Emitter 정보 입력
        public void InsertEmitterData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_EMITTERS (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,FLOW_COFFICIENT        ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Emitter 리스트 조회
        public DataSet SelectEmitterList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,FLOW_COFFICIENT                                        ");
            queryString.AppendLine("from        WH_EMITTERS                                             ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_EMITTERS");
        }

        //Emitter 존재여부 확인
        public DataSet IsExistEmitterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_EMITTERS                                         ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_EMITTERS");
        }

        //Emitter 정보 수정
        public void UpdateEmitterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_EMITTERS    set                                              ");
            queryString.AppendLine("        FLOW_COFFICIENT     = '" + conditions["FLOW_COFFICIENT"] + "'   ");
            queryString.AppendLine("where   INP_NUMBER          = '" + conditions["INP_NUMBER"] + "'        ");
            queryString.AppendLine("and     ID                  = '" + conditions["ID"] + "'                ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Emitter 정보 삭제
        public void DeleteEmitterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_EMITTERS                                         ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Quality 정보 입력
        public void InsertQualityData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_QUALITY  (                            ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,INITQUAL               ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Quality 리스트 조회
        public DataSet SelectQualityList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,INITQUAL                                               ");
            queryString.AppendLine("from        WH_QUALITY                                              ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_QUALITY");
        }

        //Quality 정보 존재여부 확인
        public DataSet IsExistQualityData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_QUALITY                                          ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_QUALITY");
        }

        //QUALITY 정보 수정
        public void UpdateQualityData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_QUALITY    set                                       ");
            queryString.AppendLine("        INITQUAL            = '" + conditions["INITQUAL"] + "'  ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Quality 정보 삭제
        public void DeleteQualityData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_QUALITY                                          ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Source 정보 입력
        public void InsertSourceData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_SOURCE   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,TYPE                   ");
            queryString.AppendLine("                            ,STRENGTH               ");
            queryString.AppendLine("                            ,PATTERN_ID             ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();
            parameters[4].Value = ((string)conditions[4]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Source 리스트 조회
        public DataSet SelectSourceList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,TYPE                                                   ");
            queryString.AppendLine("            ,STRENGTH                                               ");
            queryString.AppendLine("            ,PATTERN_ID                                             ");
            queryString.AppendLine("from        WH_SOURCE                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_SOURCE");
        }

        //Source 정보 존재여부 확인
        public DataSet IsExistSourceData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_SOURCE                                           ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("and             TYPE        = '" + conditions["TYPE"] + "'          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_SOURCE");
        }

        //Source 정보 수정
        public void UpdateSourceData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_SOURCE    set                                            ");
            queryString.AppendLine("        STRENGTH            = '" + conditions["STRENGTH"] + "'      ");
            queryString.AppendLine("        ,PATTERN_ID          = '" + conditions["PATTERN_ID"] + "'   ");
            queryString.AppendLine("where   INP_NUMBER          = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID                  = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("and     TYPE                = '" + conditions["TYPE"] + "'          ");


            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Quality 정보 삭제
        public void DeleteSourceData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_SOURCE                                           ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            //INP Master를 삭제할 경우 TYPE은 넘어오지 않는다.
            if (conditions["TYPE"] != null)
            {
                queryString.AppendLine("and     TYPE        = '" + conditions["TYPE"] + "'          ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Reaction 정보 입력
        public void InsertReactionData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_REACTIONS    (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,IDX                    ");
            queryString.AppendLine("                                ,REACTION_STATEMENT     ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = ((string)conditions[2]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Reaction 정보 입력 - Bulk insert
        public void InsertReactionBulkData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_REACTIONS    (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,IDX                    ");
            queryString.AppendLine("                                ,REACTION_STATEMENT     ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                            )                           ");

            OracleCommand commandOracle = new OracleCommand();
            commandOracle.CommandText = queryString.ToString();
            commandOracle.Connection = dbManager.Connection;
            commandOracle.ArrayBindCount = ((List<string>)conditions["inpNumberList"]).Count;

            OracleParameter prmInpNumberList = new OracleParameter("1", OracleDbType.Varchar2);
            prmInpNumberList.Direction = ParameterDirection.Input;
            prmInpNumberList.Value = ((List<string>)conditions["inpNumberList"]).ToArray();
            commandOracle.Parameters.Add(prmInpNumberList);

            OracleParameter prmIdxList = new OracleParameter("2", OracleDbType.Varchar2);
            prmIdxList.Direction = ParameterDirection.Input;
            prmIdxList.Value = ((List<string>)conditions["idxList"]).ToArray();
            commandOracle.Parameters.Add(prmIdxList);

            OracleParameter prmReactionStatementList = new OracleParameter("3", OracleDbType.Varchar2);
            prmReactionStatementList.Direction = ParameterDirection.Input;
            prmReactionStatementList.Value = ((List<string>)conditions["reactionStatementList"]).ToArray();
            commandOracle.Parameters.Add(prmReactionStatementList);

            commandOracle.ExecuteNonQuery();
        }

        //Reaction 리스트 조회
        public DataSet SelectReactionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX														");
            queryString.AppendLine("            ,REACTION_STATEMENT									    ");
            queryString.AppendLine("from        WH_REACTIONS											");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)										    ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_REACTIONS");
        }

        //Reaction 정보 존재여부 확인
        public DataSet IsExistReactionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          IDX                                                 ");
            queryString.AppendLine("from            WH_REACTIONS                                        ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_REACTIONS");
        }

        //Reaction 정보 수정
        public void UpdateReactionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_REACTIONS    set                                                     ");
            queryString.AppendLine("        REACTION_STATEMENT      = '" + conditions["REACTION_STATEMENT"] + "'    ");
            queryString.AppendLine("where   INP_NUMBER              = '" + conditions["INP_NUMBER"] + "'            ");
            queryString.AppendLine("and     IDX                     = '" + conditions["IDX"] + "'                   ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Reaction 정보 삭제
        public void DeleteReactionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_REACTIONS                                        ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["IDX"] != null)
            {
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Mixing 정보 입력
        public void InsertMixingData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_MIXING   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,MODEL                  ");
            queryString.AppendLine("                            ,FLACTION               ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = ((string)conditions[1]).Trim();
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Mixing 리스트 조회
        public DataSet SelectMixingList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,MODEL                                                  ");
            queryString.AppendLine("            ,FLACTION                                               ");
            queryString.AppendLine("from        WH_MIXING                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MIXING");
        }

        //Mixing 정보 존재여부 확인
        public DataSet IsExistMixingData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_MIXING                                           ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MIXING");
        }

        //Mixing 정보 수정
        public void UpdateMixingData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_MIXING    set                                        ");
            queryString.AppendLine("        MODEL           = '" + conditions["MODEL"] + "'         ");
            queryString.AppendLine("        ,FLACTION       = '" + conditions["FLACTION"] + "'      ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Mixing 정보 삭제
        public void DeleteMixingData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_MIXING                                           ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Time option 정보 입력
        public void InsertTimeOptionData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_TIMES    (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,TIMES_STATEMENT        ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = ((string)conditions[2]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Time option 리스트 조회
        public DataSet SelectTimeOptionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,TIMES_STATEMENT                                        ");
            queryString.AppendLine("from        WH_TIMES                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TIMES");
        }

        //Time option  정보 존재여부 확인
        public DataSet IsExistTimeOptionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          IDX                                                 ");
            queryString.AppendLine("from            WH_TIMES                                            ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TIMES");
        }

        //Time option  정보 수정
        public void UpdateTimeOptionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_TIMES    set                                                     ");
            queryString.AppendLine("        TIMES_STATEMENT     = '" + conditions["TIMES_STATEMENT"] + "'       ");
            queryString.AppendLine("where   INP_NUMBER          = '" + conditions["INP_NUMBER"] + "'            ");
            queryString.AppendLine("and     IDX                 = '" + conditions["IDX"] + "'                   ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Time option  정보 삭제
        public void DeleteTimeOptionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_TIMES                                            ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["IDX"] != null)
            {
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Report Option 정보 입력
        public void InsertReportOptionData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RPT_OPTIONS  (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,IDX                    ");
            queryString.AppendLine("                                ,REPORT_STATEMENT       ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Report Option 리스트 조회
        public DataSet SelectReportOptionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,REPORT_STATEMENT                                       ");
            queryString.AppendLine("from        WH_RPT_OPTIONS                                          ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_OPTIONS");
        }

        //Report option  정보 존재여부 확인
        public DataSet IsExistReportOptionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          IDX                                                 ");
            queryString.AppendLine("from            WH_RPT_OPTIONS                                      ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_OPTIONS");
        }

        //Report option  정보 수정
        public void UpdateReportOptionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_RPT_OPTIONS    set                                               ");
            queryString.AppendLine("        REPORT_STATEMENT    = '" + conditions["REPORT_STATEMENT"] + "'      ");
            queryString.AppendLine("where   INP_NUMBER          = '" + conditions["INP_NUMBER"] + "'            ");
            queryString.AppendLine("and     IDX                 = '" + conditions["IDX"] + "'                   ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Report option  정보 삭제
        public void DeleteReportOptionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_RPT_OPTIONS                                      ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["IDX"] != null)
            {
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Option 정보 입력
        public void InsertNormalOptionData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_OPTIONS  (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,OPTIONS_STATEMENT      ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Option 리스트 조회
        public DataSet SelectOptionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,OPTIONS_STATEMENT                                      ");
            queryString.AppendLine("from        WH_OPTIONS                                              ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_OPTIONS");
        }

        //Option 정보 존재여부 확인
        public DataSet IsExistOptionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          IDX                                                 ");
            queryString.AppendLine("from            WH_OPTIONS                                          ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_OPTIONS");
        }

        //Option 정보 수정
        public void UpdateOptionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_OPTIONS    set                                                       ");
            queryString.AppendLine("        OPTIONS_STATEMENT       = '" + conditions["OPTIONS_STATEMENT"] + "'     ");
            queryString.AppendLine("where   INP_NUMBER              = '" + conditions["INP_NUMBER"] + "'            ");
            queryString.AppendLine("and     IDX                     = '" + conditions["IDX"] + "'                   ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Option 정보 삭제
        public void DeleteOptionData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_OPTIONS                                          ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["IDX"] != null)
            {
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Coordinate 정보 입력
        public void InsertCoordinateData(OracleDBManager dbManager, ArrayList conditions)
        {
            //StringBuilder queryString = new StringBuilder();
            queryString.Remove(0, queryString.Length);

            queryString.AppendLine("insert into WH_COORDINATES  (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,ID                     ");
            queryString.AppendLine("                                ,X                      ");
            queryString.AppendLine("                                ,Y                      ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                            )                           ");

            for (int i = 0; i < conditions.Count; i++)
            {
                IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                };

                ArrayList coordinateData = (ArrayList)conditions[i];

                parameters[0].Value = (string)coordinateData[0];
                parameters[1].Value = ((string)coordinateData[1]).Trim();
                parameters[2].Value = ((string)coordinateData[2]).Trim();
                parameters[3].Value = ((string)coordinateData[3]).Trim();

                dbManager.ExecuteScript(queryString.ToString(), parameters);
            }
        }

        //Coordinate 정보 입력 - Bulk insert
        public void InsertCoordinateBulkData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();
            
            queryString.AppendLine("insert into WH_COORDINATES  (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,ID                     ");
            queryString.AppendLine("                                ,X                      ");
            queryString.AppendLine("                                ,Y                      ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                            )                           ");

            OracleCommand commandOracle = new OracleCommand();
            commandOracle.CommandText = queryString.ToString();
            commandOracle.Connection = dbManager.Connection;
            commandOracle.ArrayBindCount = ((List<string>)conditions["inpNumberList"]).Count;

            OracleParameter prmInpNumberList = new OracleParameter("1", OracleDbType.Varchar2);
            prmInpNumberList.Direction = ParameterDirection.Input;
            prmInpNumberList.Value = ((List<string>)conditions["inpNumberList"]).ToArray();
            commandOracle.Parameters.Add(prmInpNumberList);

            OracleParameter prmIdList = new OracleParameter("2", OracleDbType.Varchar2);
            prmIdList.Direction = ParameterDirection.Input;
            prmIdList.Value = ((List<string>)conditions["idList"]).ToArray();
            commandOracle.Parameters.Add(prmIdList);

            OracleParameter prmXList = new OracleParameter("3", OracleDbType.Varchar2);
            prmXList.Direction = ParameterDirection.Input;
            prmXList.Value = ((List<string>)conditions["xList"]).ToArray();
            commandOracle.Parameters.Add(prmXList);

            OracleParameter prmYList = new OracleParameter("4", OracleDbType.Varchar2);
            prmYList.Direction = ParameterDirection.Input;
            prmYList.Value = ((List<string>)conditions["yList"]).ToArray();
            commandOracle.Parameters.Add(prmYList);

            commandOracle.ExecuteNonQuery();
        }

        //Coordinate 리스트 조회
        public DataSet SelectCoordinateList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,X                                                      ");
            queryString.AppendLine("            ,Y                                                      ");
            queryString.AppendLine("from        WH_COORDINATES                                          ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_COORDINATES");
        }

        //Coordinate 정보 존재여부 확인
        public DataSet IsExistCoordinateData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("from            WH_COORDINATES                                      ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_COORDINATES");
        }

        //Coordinate 정보 수정
        public void UpdateCoordinateData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_COORDINATES    set                                   ");
            queryString.AppendLine("        X               = '" + conditions["X"] + "'             ");
            queryString.AppendLine("        ,Y              = '" + conditions["Y"] + "'             ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'            ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Coordinate 정보 삭제
        public void DeleteCoordinateData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_COORDINATES                                      ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Vertices 정보 입력
        public void InsertVerticesData(OracleDBManager dbManager, ArrayList conditions)
        {
            //StringBuilder queryString = new StringBuilder();
            queryString.Remove(0, queryString.Length);

            queryString.AppendLine("insert into WH_VERTICES (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,X                      ");
            queryString.AppendLine("                            ,Y                      ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            for (int i = 0; i < conditions.Count; i++)
            {
                IDataParameter[] parameters =  {
                     new OracleParameter("1", OracleDbType.Varchar2)
                    ,new OracleParameter("2", OracleDbType.Varchar2)
                    ,new OracleParameter("3", OracleDbType.Varchar2)
                    ,new OracleParameter("4", OracleDbType.Varchar2)
                    ,new OracleParameter("5", OracleDbType.Varchar2)
                };

                ArrayList verticesData = (ArrayList)conditions[i];

                parameters[0].Value = (string)verticesData[0];
                parameters[1].Value = ((string)verticesData[1]).Trim();
                parameters[2].Value = (string)verticesData[2];
                parameters[3].Value = ((string)verticesData[3]).Trim();
                parameters[4].Value = ((string)verticesData[4]).Trim();

                dbManager.ExecuteScript(queryString.ToString(), parameters);                
            }

        }

        //Vertices 정보 입력 - Bulk Insert
        public void InsertVerticesBulkData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_VERTICES (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,X                      ");
            queryString.AppendLine("                            ,Y                      ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            OracleCommand commandOracle = new OracleCommand();
            commandOracle.CommandText = queryString.ToString();
            commandOracle.Connection = dbManager.Connection;
            commandOracle.ArrayBindCount = ((List<string>)conditions["inpNumberList"]).Count;

            OracleParameter prmInpNumberList = new OracleParameter("1", OracleDbType.Varchar2);
            prmInpNumberList.Direction = ParameterDirection.Input;
            prmInpNumberList.Value = ((List<string>)conditions["inpNumberList"]).ToArray();
            commandOracle.Parameters.Add(prmInpNumberList);

            OracleParameter prmIdList = new OracleParameter("2", OracleDbType.Varchar2);
            prmIdList.Direction = ParameterDirection.Input;
            prmIdList.Value = ((List<string>)conditions["idList"]).ToArray();
            commandOracle.Parameters.Add(prmIdList);

            OracleParameter prmIdxList = new OracleParameter("3", OracleDbType.Varchar2);
            prmIdxList.Direction = ParameterDirection.Input;
            prmIdxList.Value = ((List<string>)conditions["idxList"]).ToArray();
            commandOracle.Parameters.Add(prmIdxList);

            OracleParameter prmXList = new OracleParameter("4", OracleDbType.Varchar2);
            prmXList.Direction = ParameterDirection.Input;
            prmXList.Value = ((List<string>)conditions["xList"]).ToArray();
            commandOracle.Parameters.Add(prmXList);

            OracleParameter prmYList = new OracleParameter("5", OracleDbType.Varchar2);
            prmYList.Direction = ParameterDirection.Input;
            prmYList.Value = ((List<string>)conditions["yList"]).ToArray();
            commandOracle.Parameters.Add(prmYList);

            commandOracle.ExecuteNonQuery();
        }

        //Vertices 리스트 조회
        public DataSet SelectVerticesList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,IDX                                                    ");
            queryString.AppendLine("            ,X                                                      ");
            queryString.AppendLine("            ,Y                                                      ");
            queryString.AppendLine("from        WH_VERTICES                                             ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");
            queryString.AppendLine("            ,to_number(IDX)                                         ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_VERTICES");
        }

        //Vertices 정보 존재여부 확인
        public DataSet IsExistVerticesData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ID                                                  ");
            queryString.AppendLine("                ,IDX                                                ");
            queryString.AppendLine("from            WH_VERTICES                                         ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             ID          = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("and             IDX         = '" + conditions["IDX"] + "'           ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_VERTICES");
        }

        //Vertices 정보 수정
        public void UpdateVerticesData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_VERTICES    set                                      ");
            queryString.AppendLine("        X               = '" + conditions["X"] + "'             ");
            queryString.AppendLine("        ,Y              = '" + conditions["Y"] + "'             ");
            queryString.AppendLine("where   INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and     ID              = '" + conditions["ID"] + "'            ");
            queryString.AppendLine("and     IDX             = '" + conditions["IDX"] + "'           ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Vertices 정보 삭제
        public void DeleteVerticesData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_VERTICES                                         ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            //INP Master를 삭제할 경우 ID는 넘어오지 않는다.
            if (conditions["ID"] != null && conditions["IDX"] != null)
            {
                queryString.AppendLine("and     ID          = '" + conditions["ID"] + "'            ");
                queryString.AppendLine("and     IDX         = '" + conditions["IDX"] + "'           ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Label 정보 입력
        public void InsertLabelData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_LABELS   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,X                      ");
            queryString.AppendLine("                            ,Y                      ");
            queryString.AppendLine("                            ,REMARK                 ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = ((string)conditions[2]).Trim();
            parameters[3].Value = ((string)conditions[3]).Trim();
            parameters[4].Value = ((string)conditions[4]).Trim();

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Label 리스트 조회
        public DataSet SelectLabelList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,X                                                      ");
            queryString.AppendLine("            ,Y                                                      ");
            queryString.AppendLine("            ,REMARK                                                 ");
            queryString.AppendLine("from        WH_LABELS                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LABELS");
        }

        //Label 정보 존재여부 확인
        public DataSet IsExistLabelData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          IDX                                                 ");
            queryString.AppendLine("from            WH_LABELS                                           ");
            queryString.AppendLine("where           INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and             IDX          = '" + conditions["IDX"] + "'          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LABELS");
        }

        //Label 정보 수정
        public void UpdateLabelData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("update  WH_LABELS   set                                                 ");
            queryString.AppendLine("        X                   = '" + conditions["X"] + "'                 ");
            queryString.AppendLine("        ,Y                  = '" + conditions["Y"] + "'                 ");
            queryString.AppendLine("        ,REMARK             = '" + conditions["REMARK"] + "'            ");
            queryString.AppendLine("where   INP_NUMBER          = '" + conditions["INP_NUMBER"] + "'        ");
            queryString.AppendLine("and     IDX                 = '" + conditions["IDX"] + "'               ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Label 정보 삭제
        public void DeleteLabelData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_LABELS                                                   ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'            ");

            //INP Master를 삭제할 경우 IDX는 넘어오지 않는다.
            if (conditions["IDX"] != null)
            {
                queryString.AppendLine("and     IDX          = '" + conditions["IDX"] + "'                  ");
            }

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Tag 정보 입력
        public void InsertTagData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_TAGS (                   ");
            queryString.AppendLine("                        INP_NUMBER      ");
            queryString.AppendLine("                        ,TYPE           ");
            queryString.AppendLine("                        ,ID             ");
            queryString.AppendLine("                        ,POSITION_INFO  ");
            queryString.AppendLine("                    ) values (          ");
            queryString.AppendLine("                        :1              ");
            queryString.AppendLine("                        ,:2             ");
            queryString.AppendLine("                        ,:3             ");
            queryString.AppendLine("                        ,:4             ");
            queryString.AppendLine("                    )                   ");

            ArrayList tagData;
            for (int i = 0; i < conditions.Count; i++)
            {
                IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                };

                tagData = (ArrayList)conditions[i];
                parameters[0].Value = (string)tagData[0];
                parameters[1].Value = ((string)tagData[1]).Trim();
                parameters[2].Value = ((string)tagData[2]).Trim();
                parameters[3].Value = ((string)tagData[3]).Trim();

                Console.WriteLine(conditions.Count.ToString() + " , " +i.ToString());
                dbManager.ExecuteScript(queryString.ToString(), parameters);
            }


        }

        //Tag 정보 입력 - Bulk Insert
        public void InsertTagBulkData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_TAGS (                   ");
            queryString.AppendLine("                        INP_NUMBER      ");
            queryString.AppendLine("                        ,TYPE           ");
            queryString.AppendLine("                        ,ID             ");
            queryString.AppendLine("                        ,POSITION_INFO  ");
            queryString.AppendLine("                    ) values (          ");
            queryString.AppendLine("                        :1              ");
            queryString.AppendLine("                        ,:2             ");
            queryString.AppendLine("                        ,:3             ");
            queryString.AppendLine("                        ,:4             ");
            queryString.AppendLine("                    )                   ");

            OracleCommand commandOracle = new OracleCommand();
            commandOracle.CommandText = queryString.ToString();
            commandOracle.Connection = dbManager.Connection;
            commandOracle.ArrayBindCount = ((List<string>)conditions["inpNumberList"]).Count;

            OracleParameter prmInpNumberList = new OracleParameter("1", OracleDbType.Varchar2);
            prmInpNumberList.Direction = ParameterDirection.Input;
            prmInpNumberList.Value = ((List<string>)conditions["inpNumberList"]).ToArray();
            commandOracle.Parameters.Add(prmInpNumberList);

            OracleParameter prmTypeList = new OracleParameter("2", OracleDbType.Varchar2);
            prmTypeList.Direction = ParameterDirection.Input;
            prmTypeList.Value = ((List<string>)conditions["typeList"]).ToArray();
            commandOracle.Parameters.Add(prmTypeList);

            OracleParameter prmIdList = new OracleParameter("3", OracleDbType.Varchar2);
            prmIdList.Direction = ParameterDirection.Input;
            prmIdList.Value = ((List<string>)conditions["idList"]).ToArray();
            commandOracle.Parameters.Add(prmIdList);

            OracleParameter prmPositionInfoList = new OracleParameter("4", OracleDbType.Varchar2);
            prmPositionInfoList.Direction = ParameterDirection.Input;
            prmPositionInfoList.Value = ((List<string>)conditions["positionInfoList"]).ToArray();
            commandOracle.Parameters.Add(prmPositionInfoList);

            commandOracle.ExecuteNonQuery();
        }


        //Tag 리스트 조회
        public DataSet SelectTagList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,TYPE                                                   ");
            queryString.AppendLine("            ,POSITION_INFO                                          ");
            queryString.AppendLine("from        WH_TAGS                                                 ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    TYPE                                                    ");
            queryString.AppendLine("            ,ID                                                     ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TAGS");
        }

        //블록별 BaseDemand 총량 연산
        public DataSet SelectSumBaseDemand(OracleDBManager dbManager, Hashtable conditions)
        {
            queryString.Remove(0, queryString.Length);

            //queryString.AppendLine("select          a.POSITION_INFO                                                                                                                                             ");
            queryString.AppendLine("select          substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 2)+1,length(a.POSITION_INFO) - instr(a.POSITION_INFO, '|', 2, 1))                 as BSM_IDN          ");
            queryString.AppendLine("                ,substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 1)+1,instr(a.POSITION_INFO, '|', 1, 2)-1 - instr(a.POSITION_INFO, '|', 1, 1))    as BSM_CDE          ");
            queryString.AppendLine("                ,sum(to_number(b.DEMAND))                               as SUM_BASEDEMAND                                                                                   ");
            queryString.AppendLine("from            WH_TAGS         a                                                                                                                                           ");
            queryString.AppendLine("                ,WH_JUNCTIONS   b                                                                                                                                           ");
            queryString.AppendLine("where           a.TYPE          = 'NODE'                                                                                                                                    ");
            queryString.AppendLine("and             a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                                                                        ");
            queryString.AppendLine("and             b.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'                                                                                                        ");
            queryString.AppendLine("and             a.ID            = b.ID                                                                                                                                      ");
            queryString.AppendLine("and             substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 2)+1,length(a.POSITION_INFO) - instr(a.POSITION_INFO, '|', 2, 1))             is not null             ");
            queryString.AppendLine("and             substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 1)+1,instr(a.POSITION_INFO, '|', 1, 2)-1 - instr(a.POSITION_INFO, '|', 1, 1)) is not null             ");
            queryString.AppendLine("group by        substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 2)+1,length(a.POSITION_INFO) - instr(a.POSITION_INFO, '|', 2, 1))                                     ");
            queryString.AppendLine("                ,substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 1)+1,instr(a.POSITION_INFO, '|', 1, 2)-1 - instr(a.POSITION_INFO, '|', 1, 1))                        ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TAGS");
        }

        //블록별 BaseDemand 총량 입력
        public void InsertBlockSumUsage(OracleDBManager dbManager, Hashtable conditions)
        {
            queryString.Remove(0, queryString.Length);

            queryString.AppendLine("insert into     WH_BLOCK_SUM_USEAGE (                       ");
            queryString.AppendLine("                                        INP_NUMBER          ");
            queryString.AppendLine("                                        ,BSM_CDE            ");
            queryString.AppendLine("                                        ,BSM_IDN            ");
            queryString.AppendLine("                                        ,SUM_BASEDEMAND     ");
            queryString.AppendLine("                                    ) values (              ");
            queryString.AppendLine("                                        :1                  ");
            queryString.AppendLine("                                        ,:2                 ");
            queryString.AppendLine("                                        ,:3                 ");
            queryString.AppendLine("                                        ,:4                 ");
            queryString.AppendLine("                                    )                       ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = conditions["INP_NUMBER"];
            parameters[1].Value = conditions["BSM_CDE"];
            parameters[2].Value = conditions["BSM_IDN"];
            parameters[3].Value = ((string)conditions["SUM_BASEDEMAND"]).Trim().Length < 16 ? ((string)conditions["SUM_BASEDEMAND"]).Trim() : ((string)conditions["SUM_BASEDEMAND"]).Trim().Substring(0,15);

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //해석결과표출범위 입력
        public void InsertAnalysisResultRange(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_ANALYSIS_RESULT_RANGE (                  ");
            queryString.AppendLine("                                        INP_NUMBER      ");
            queryString.AppendLine("                                        ,RESULT_TYPE    ");
            queryString.AppendLine("                                        ,RESULT_CODE    ");
            queryString.AppendLine("                                        ,RANGE1         ");
            queryString.AppendLine("                                        ,RANGE2         ");
            queryString.AppendLine("                                        ,RANGE3         ");
            queryString.AppendLine("                                        ,RANGE4         ");
            queryString.AppendLine("                                      ) values (        ");
            queryString.AppendLine("                                        :1              ");
            queryString.AppendLine("                                        ,:2             ");
            queryString.AppendLine("                                        ,:3             ");
            queryString.AppendLine("                                        ,:4             ");
            queryString.AppendLine("                                        ,:5             ");
            queryString.AppendLine("                                        ,:6             ");
            queryString.AppendLine("                                        ,:7             ");
            queryString.AppendLine("                                      )                 ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["RESULT_TYPE"];
            parameters[2].Value = (string)conditions["RESULT_CODE"];
            parameters[3].Value = (string)conditions["RANGE1"];
            parameters[4].Value = (string)conditions["RANGE2"];
            parameters[5].Value = (string)conditions["RANGE3"];
            parameters[6].Value = (string)conditions["RANGE4"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //대수용가 정보 삭제
        public void DeleteLargeConsumerData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_LCONSUMER_INFO                                   ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //대수용가 적용 타임테이블 정보 삭제
        public void DeleteLargeConsumerTimeTableData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_LCONSUMER_TIME                                   ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //실측지점데이터 삭제
        public void DeleteRealtimeCheckpointData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_MEASURE_CHECKPOINT                               ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //이동식 계측기 마스터정보 삭제
        public void DeleteMeaserDumpMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_MEASURE_DUMP_MASTER                              ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //이동식 계측기 데이터 삭제
        public void DeleteMeaserDumpData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_MEASURE_DUMP_DATA                                ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //해석결과 표출범위 삭제
        public void DeleteAnalysisResultRangeData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_ANALYSIS_RESULT_RANGE                            ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //블록평균사용량 삭제
        public void DeleteBlockSumUsage(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_BLOCK_SUM_USEAGE                                 ");
            queryString.AppendLine("where       INP_NUMBER  = '" + conditions["INP_NUMBER"] + "'    ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //실시간 관망해석 설정 입력
        public void InsertAnalysisSettingData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_ANALYSIS_SETTING (                       ");
            queryString.AppendLine("                                    INP_NUMBER          ");
            queryString.AppendLine("                                    ,ERROR_SAVE_YN      ");
            queryString.AppendLine("                                    ,LOW_PRESSURE       ");
            queryString.AppendLine("                                ) values (              ");
            queryString.AppendLine("                                    :1                  ");
            queryString.AppendLine("                                    ,:2                 ");
            queryString.AppendLine("                                    ,:3                 ");
            queryString.AppendLine("                                )                       ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["ERROR_SAVE_YN"];
            parameters[2].Value = (string)conditions["LOW_PRESSURE"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //실시간 관망해석 설정 삭제
        public void DeleteAnalysisSettingData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_ANALYSIS_SETTING                             ");
            queryString.AppendLine("where       INP_NUMBER = '" + conditions["INP_NUMBER"] + "' ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //EPANET K와의 연동을 위해 Export할 INP파일 리스트를 조회
        public DataSet SelectExportINPList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      a.INP_NUMBER                                                                                        ");
            queryString.AppendLine("            ,a.USE_GBN                                                                                          ");
            queryString.AppendLine("            ,e.CODE_NAME       as USE_GBN_NAME                                                                  ");
            queryString.AppendLine("            ,a.LFTRIDN                                                                                          ");
            queryString.AppendLine("            ,b.LOC_NAME        as LFTR_NAME                                                                     ");
            queryString.AppendLine("            ,a.MFTRIDN                                                                                          ");
            queryString.AppendLine("            ,c.LOC_NAME        as MFTR_NAME                                                                     ");
            queryString.AppendLine("            ,a.SFTRIDN                                                                                          ");
            queryString.AppendLine("            ,d.LOC_NAME        as SFTR_NAME                                                                     ");
            queryString.AppendLine("            ,to_char(to_date(a.INS_DATE,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')      as INS_DATE      ");
            queryString.AppendLine("            ,a.TITLE                                                                                            ");
            queryString.AppendLine("            ,a.REMARK                                                                                           ");
            queryString.AppendLine("            ,a.FILE_NAME                                                                                        ");
            queryString.AppendLine("from        WH_TITLE                 a                                                                          ");
            queryString.AppendLine("            ,CM_LOCATION             b                                                                          ");
            queryString.AppendLine("            ,CM_LOCATION             c                                                                          ");
            queryString.AppendLine("            ,CM_LOCATION             d                                                                          ");
            queryString.AppendLine("            ,CM_CODE                 e                                                                          ");
            queryString.AppendLine("where       1 = 1                                                                                               ");

            if (conditions["USE_GBN"] != null)
            {
                queryString.AppendLine("and     a.USE_GBN     = '" + conditions["USE_GBN"] + "'                                                     ");
            }

            if (conditions["LFTRIDN"] != null)
            {
                queryString.AppendLine("and     a.LFTRIDN     = '" + conditions["LFTRIDN"] + "'                                                     ");
            }

            if (conditions["MFTRIDN"] != null)
            {
                queryString.AppendLine("and     a.MFTRIDN     = '" + conditions["MFTRIDN"] + "'                                                     ");
            }

            if (conditions["SFTRIDN"] != null)
            {
                queryString.AppendLine("and     a.SFTRIDN     = '" + conditions["SFTRIDN"] + "'                                                     ");
            }

            //if (conditions["startDate"] != null && conditions["endDate"] != null)
            //{
            //    queryString.AppendLine("and         a.INS_DATE                                                                                      ");
            //    queryString.AppendLine("between     '" + conditions["startDate"] + "'                                                               ");
            //    queryString.AppendLine("and         '" + conditions["endDate"] + "235959'                                                           ");
            //}

            if (conditions["TITLE"] != null)
            {
                queryString.AppendLine("and     a.TITLE       like '%" + conditions["TITLE"] + "%'                                                  ");
            }

            queryString.AppendLine("and         a.LFTRIDN   = b.LOC_CODE(+)                                                                         ");
            queryString.AppendLine("and         a.MFTRIDN   = c.LOC_CODE(+)                                                                         ");
            queryString.AppendLine("and         a.SFTRIDN   = d.LOC_CODE(+)                                                                         ");
            queryString.AppendLine("and         a.USE_GBN   = e.CODE(+)                                                                             ");
            queryString.AppendLine("and         e.PCODE(+)     = '1001'                                                                             ");
            queryString.AppendLine("order by    to_date(a.INS_DATE,'yyyy-mm-dd hh24:mi:ss')                                                         ");
            queryString.AppendLine("desc                                                                                                            ");
            queryString.AppendLine("            ,a.LFTRIDN                                                                                          ");
            queryString.AppendLine("            ,a.MFTRIDN                                                                                          ");
            queryString.AppendLine("            ,a.SFTRIDN                                                                                          ");
            queryString.AppendLine("asc                                                                                                             ");

            //Console.WriteLine(queryString);

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //모델의 블록별 사용량 합산 삭제
        public void DeleteSumDemandData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_BLOCK_SUM_USEAGE                             ");
            queryString.AppendLine("where       INP_NUMBER = '" + conditions["INP_NUMBER"] + "' ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //평균수압지점 삭제
        public void DeleteAveragePressurePointData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_AVG_PRES_NODE                                    ");
            queryString.AppendLine("where       INP_NUMBER = '" + conditions["INP_NUMBER"] + "'     ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //대수용가 동작정보 삭제
        public void DeleteLargeConsumerActivateData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_LCONSUMER_ACTIVATE                               ");
            queryString.AppendLine("where       INP_NUMBER = '" + conditions["INP_NUMBER"] + "'     ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }
    }
}