﻿/**************************************************************************
 * 파일명   : EPANETMethodDefinition.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.09.30
 * 설명     : EPANET Toolkit을 확장한 강성찬과장의 dll (xnet_dll.dll)에 
 *            정의된 함수원형 정의
 * 변경이력 : 2010.09.30 - 최초생성
 **************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;

namespace WaterNet.WH_PipingNetworkAnalysis.epanet
{
    public class EPANETMethodDefinition
    {
/*******************************************************************************
 * 강성찬 과장 Custom 함수 (inp파일 parsing을 위해 사용)
 *******************************************************************************/
        //Epanet Toolkit loading 함수 원형
        //[DllImport("xnet_dll.dll", CallingConvention = CallingConvention.StdCall)]
        [DllImport("xnet_dll.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "LoadNet")]
        public static extern void LoadNet();

        //Epanet Toolkit unloading 함수 원형
        //[DllImport("xnet_dll.dll")]
        [DllImport("xnet_dll.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void UnloadNet();

        //.inp파일을 읽는 함수 원형
        //[DllImport("xnet_dll.dll", CharSet = CharSet.Unicode)]
        [DllImport("xnet_dll.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void ReadInpFile(StringBuilder fname, bool leak);

        //섹션의 위치값을 반환하는 함수 원형
        //[DllImport("xnet_dll.dll", CharSet = CharSet.Unicode)]
        [DllImport("xnet_dll.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean SectionBetween(StringBuilder sec, ref int sIdx, ref int eIdx);

        //섹션의 위치에 따른 항목값을 반환하는 함수 원형
        [DllImport("xnet_dll.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean GetSectionValue1(int idx,
                                                       ref StringBuilder v1,
                                                       ref StringBuilder v2,
                                                       ref StringBuilder v3,
                                                       ref StringBuilder v4,
                                                       ref StringBuilder v5,
                                                       ref StringBuilder v6,
                                                       ref StringBuilder v7,
                                                       ref StringBuilder v8,
                                                       ref StringBuilder v9,
                                                       ref StringBuilder v10);

        //해당 섹션의 인덱스에 존재하는 주석을 조회한다.
        //[DllImport("xnet_dll.dll", CharSet = CharSet.Unicode)]
        //public static extern StringBuilder GetSectionComment(int idx);
        [DllImport("xnet_dll.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void GetSectionComment(int idx, ref StringBuilder value);


        //Pattern항목의 카운트를 반환
        [DllImport("xnet_dll.dll", CharSet = CharSet.Unicode)]
        public static extern int CountAllPattern();

        //Pattern항목의 ID를 반환
        //[DllImport("xnet_dll.dll", CharSet = CharSet.Unicode)]
        //public static extern StringBuilder GetPatternId(int idx);
        [DllImport("xnet_dll.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void GetPatternId(int idx, ref StringBuilder value);

        //해당 패턴의 구성요소 카운트를 반환
        [DllImport("xnet_dll.dll", CharSet = CharSet.Unicode)]
        public static extern int CountPattern(int idx);

        //해당 패턴의 인덱스별 값을 반환
        [DllImport("xnet_dll.dll", CharSet = CharSet.Unicode)]
        public static extern double GetPatternValue(int idx1, int idx2);

/*******************************************************************************
 * Epanet Toolkit 원본 함수 
 *******************************************************************************/
        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENopen(StringBuilder f1, StringBuilder f2, StringBuilder f3);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENopenH();

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENopenQ();

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENinitH(int saveFlag);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENinitQ(int saveflag);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENrunH(ref long t);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENrunQ(ref long t);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgetnodevalue(int index, int paramcode, ref float value);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgetnodeid(int index, StringBuilder id);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgetnodetype(int index, ref int typecode);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgetlinkvalue(int index, int paramcode, ref float value);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgetlinkid(int index, StringBuilder id);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgetlinktype(int index, ref int typecode);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgetNodeIndex(StringBuilder id, ref int index);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgetpatternindex(StringBuilder id, ref int index);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENsetnodevalue(int index, int paramcode, float value);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENsetpatternvalue(int index, int period, float value);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENcloseH();

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENcloseQ();

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENclose();

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgetcount(int countcode, ref int count);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENnextH(ref long tstep);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENnextQ(ref long tstep);

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENsolveH();

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENsolveQ();

        //[DllImport("epanet_leak.dll", CharSet = CharSet.Ansi)]
        //public static extern int ENgettimeparam(int paramcode, ref long timevalue);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENopen(StringBuilder f1, StringBuilder f2, StringBuilder f3);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENopenH();

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENopenQ();

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENinitH(int saveFlag);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENinitQ(int saveflag);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENrunH(ref long t);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENrunQ(ref long t);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgetnodevalue(int index, int paramcode, ref float value);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgetnodeid(int index, StringBuilder id);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgetnodetype(int index, ref int typecode);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgetlinkvalue(int index, int paramcode, ref float value);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgetlinkid(int index, StringBuilder id);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgetlinktype(int index, ref int typecode);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgetNodeIndex(StringBuilder id, ref int index);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgetpatternindex(StringBuilder id, ref int index);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENsetnodevalue(int index, int paramcode, float value);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENsetpatternvalue(int index, int period, float value);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENcloseH();

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENcloseQ();

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENclose();

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgetcount(int countcode, ref int count);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENnextH(ref long tstep);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENnextQ(ref long tstep);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENstepQ(ref long tleft);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENsolveH();

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENsolveQ();

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENgettimeparam(int paramcode, ref long timevalue);

        [DllImport("epanet2.dll", CharSet = CharSet.Ansi)]
        public static extern int ENsetqualtype(int qualcode, StringBuilder chemname, StringBuilder chemunits, StringBuilder tracenode);
    }
}
