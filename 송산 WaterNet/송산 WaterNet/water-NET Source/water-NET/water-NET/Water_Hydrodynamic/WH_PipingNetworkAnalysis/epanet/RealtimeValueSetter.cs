﻿/**************************************************************************
 * 파일명   : RealtimeValueSetterByJob.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.20
 * 설명     : 실시간 관망해석 시 주어진 시간마다 재설정될 값을 정의
 * 변경이력 : 2010.10.20 - 최초생성
 **************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WH_PipingNetworkAnalysis.epanet
{
    class RealtimeValueSetterByJob
    {
        public RealtimeValueSetterByJob()
        {
        }

        //실시간 재설정값 조회
        public Hashtable GetHydroRealtimeValue(string inpNumber)
        {
            Hashtable result = new Hashtable();

            if("WH".Equals(inpNumber.Substring(0,2)))
            {
                //수리

                //실시간 유량 조회




                result.Add("hydro",null);
            }
            else if ("WE".Equals(inpNumber.Substring(0, 2)))
            {
                //에너지

                result.Add("energy", null);
            }

            return result;
        }
    }
}
