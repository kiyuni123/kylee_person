﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterNet.WH_PipingNetworkAnalysis.tmpUtils
{
    class TmpVariables
    {
        public static string INP_FILE_PATH = System.Windows.Forms.Application.StartupPath + @"\TEMP\ANALYSIS\";              //관망해석 요청 시 INP파일을 생성할 위치
        public static string RPT_FILE_PATH = System.Windows.Forms.Application.StartupPath + @"\TEMP\ANALYSIS\REPORT\";       //관망해석 결과파일을 생성 할 경우 위치
        public static string REAL_INP_FILE_PATH = System.Windows.Forms.Application.StartupPath + @"\TEMP\ANALYSIS\REAL\";    //실시간 관망해석 시 실시간 데이터를 이용해 재생성할 INP 파일의 위치
    }
}
