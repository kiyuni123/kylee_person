﻿/**************************************************************************
 * 파일명   : Utils.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.07
 * 설명     : 필요한 utility를 임시로 정의한 클래스
 * 변경이력 : 2010.10.07 - 최초생성
 **************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace WaterNet.WH_PipingNetworkAnalysis.tmpUtils
{
    public class Utils
    {
        private static Utils utils = null;

        private Utils()
        {
        }

        public Utils GetInsatance()
        {
            if(utils == null)
            {
                utils = new Utils();
            }

            return utils;
        }

        //일련번호 생성
        public static String GetSerialNumber(string workFlag)
        {
            Random random = new Random();

            return workFlag
                    + DateTime.Now.Year.ToString() 
                    + (DateTime.Now.Month.ToString()).PadLeft(2, '0') 
                    + (DateTime.Now.Day.ToString()).PadLeft(2, '0') 
                    + (DateTime.Now.Second.ToString()).PadLeft(2, '0') 
                    + (DateTime.Now.Millisecond.ToString()).PadLeft(3, '0') 
                    + (random.Next(1, 9999).ToString()).PadLeft(4,'0');
        }

        //넘겨받은 DataSet을 해체하여 Console에 출력
        public static void ShowDataSet(DataSet dSet)
        {
            IEnumerator tableEnu = dSet.Tables.GetEnumerator();

            while(tableEnu.MoveNext())
            {
                string tableName = tableEnu.Current.ToString();

                Console.WriteLine("테이블명 : "+tableName);

                for(int i = 0; i < (dSet.Tables[tableName].Rows.Count); i++)
                {
                    //DataRow row = dSet.Tables[tableName].Rows[i];

                    Console.WriteLine("=================" + i + "번째=================");

                    Object[] obj = dSet.Tables[tableName].Rows[i].ItemArray;

                    for (int j = 0; j < obj.Length; j++ )
                    {
                        Console.WriteLine(obj[j]);
                    }
                }
            }
        }

        //null to string
        public static string nts(object arg)
        {
            string result = "";
       

            if (arg != DBNull.Value && arg != null)
            {
                result = (string)arg;
            }

            return result;
        }
    }
}
