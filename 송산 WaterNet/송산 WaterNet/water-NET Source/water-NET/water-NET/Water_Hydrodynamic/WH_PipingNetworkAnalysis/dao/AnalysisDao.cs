﻿/**************************************************************************
 * 파일명   : AnalysisDao.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.25
 * 설명     : 관망해석에 관련된 Data Handling용 Data Access Object
 * 변경이력 : 2010.10.25 - 최초생성
 **************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using WaterNet.WaterNetCore;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace WaterNet.WH_PipingNetworkAnalysis.dao
{
    public class AnalysisDao
    {
        private static AnalysisDao dao = null;
        //private StringBuilder queryString = new StringBuilder();

        private AnalysisDao()
        {
        }

        public static AnalysisDao GetInstance()
        {
            if(dao == null)
            {
                dao = new AnalysisDao();
            }

            return dao;
        }

        //해석결과 Master 저장
        public void InsertReportMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            //queryString.Remove(0, queryString.Length);
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RPT_MASTER   (                                                       ");
            queryString.AppendLine("                                RPT_NUMBER                                          ");
            queryString.AppendLine("                                ,WSP_NAM                                            ");
            queryString.AppendLine("                                ,LFTRIDN                                            ");
            queryString.AppendLine("                                ,MFTRIDN                                            ");
            queryString.AppendLine("                                ,SFTRIDN                                            ");
            queryString.AppendLine("                                ,INP_NUMBER                                         ");
            queryString.AppendLine("                                ,RPT_DATE                                           ");
            queryString.AppendLine("                                ,TITLE                                              ");
            queryString.AppendLine("                                ,AUTO_MANUAL                                        ");
            queryString.AppendLine("                                ,TARGET_DATE                                        ");
            queryString.AppendLine("                            ) values (                                              ");
            queryString.AppendLine("                                :1                                                  ");
            queryString.AppendLine("                                ,:2                                                 ");
            queryString.AppendLine("                                ,:3                                                 ");
            queryString.AppendLine("                                ,:4                                                 ");
            queryString.AppendLine("                                ,:5                                                 ");
            queryString.AppendLine("                                ,:6                                                 ");
            queryString.AppendLine("                                ,to_char(sysdate,'yyyymmddhh24miss')                ");
            queryString.AppendLine("                                ,:7                                                 ");
            queryString.AppendLine("                                ,:8                                                 ");
            queryString.AppendLine("                                ,:9                                                 ");
            queryString.AppendLine("                            )                                                       ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["RPT_NUMBER"];
            parameters[1].Value = (string)conditions["WSP_NAM"];
            parameters[2].Value = (string)conditions["LFTRIDN"];
            parameters[3].Value = (string)conditions["MFTRIDN"];
            parameters[4].Value = (string)conditions["SFTRIDN"];
            parameters[5].Value = (string)conditions["INP_NUMBER"];
            parameters[6].Value = (string)conditions["TITLE"];
            parameters[7].Value = (string)conditions["AUTO_MANUAL"];
            parameters[8].Value = (string)conditions["TARGET_DATE"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Junction 해석결과 저장
        public void InsertNodeReportData(OracleDBManager dbManager, Hashtable conditions)
        {
            //queryString.Remove(0, queryString.Length);
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RPT_NODES        (                                   ");
            queryString.AppendLine("									RPT_NUMBER						");
            queryString.AppendLine("									,INP_NUMBER						");
            queryString.AppendLine("									,NODE_ID						");
            queryString.AppendLine("									,ANALYSIS_TIME					");
            queryString.AppendLine("									,ELEVATION						");
            queryString.AppendLine("									,BASEDEMAND						");
            queryString.AppendLine("									,PATTERN						");
            queryString.AppendLine("									,EMITTER						");
            queryString.AppendLine("									,INITQUAL						");
            queryString.AppendLine("									,SOURCEQUAL						");
            queryString.AppendLine("									,SOURCEPAT						");
            queryString.AppendLine("									,SOURCETYPE						");
            queryString.AppendLine("									,TANKLEVEL						");
            queryString.AppendLine("									,DEMAND							");
            queryString.AppendLine("									,HEAD							");
            queryString.AppendLine("									,PRESSURE						");
            queryString.AppendLine("									,QUALITY						");
            queryString.AppendLine("									,SOURCEMASS						");
            queryString.AppendLine("									,INITVOLUME						");
            queryString.AppendLine("									,MIXMODEL						");
            queryString.AppendLine("									,MIXZONEVOL						");
            queryString.AppendLine("									,TANKDIAM						");
            queryString.AppendLine("									,MINVOLUME						");
            queryString.AppendLine("									,VOLCURVE						");
            queryString.AppendLine("									,MINLEVEL						");
            queryString.AppendLine("									,MAXLEVEL						");
            queryString.AppendLine("									,MIXFRACTION					");
            queryString.AppendLine("									,TANK_KBULK						");
            queryString.AppendLine("									,ANALYSIS_TYPE					");
            queryString.AppendLine("                                ) values (                          ");
            queryString.AppendLine("                                    :1                              ");
            queryString.AppendLine("                                    ,:2                             ");
            queryString.AppendLine("                                    ,:3                             ");
            queryString.AppendLine("                                    ,:4                             ");
            queryString.AppendLine("                                    ,:5                             ");
            queryString.AppendLine("                                    ,:6                             ");
            queryString.AppendLine("                                    ,:7                             ");
            queryString.AppendLine("                                    ,:8                             ");
            queryString.AppendLine("                                    ,:9                             ");
            queryString.AppendLine("                                    ,:10                            ");
            queryString.AppendLine("                                    ,:11                            ");
            queryString.AppendLine("                                    ,:12                            ");
            queryString.AppendLine("                                    ,:13                            ");
            queryString.AppendLine("                                    ,:14                            ");
            queryString.AppendLine("                                    ,:15                            ");
            queryString.AppendLine("                                    ,:16                            ");
            queryString.AppendLine("                                    ,:17                            ");
            queryString.AppendLine("                                    ,:18                            ");
            queryString.AppendLine("                                    ,:19                            ");
            queryString.AppendLine("                                    ,:20                            ");
            queryString.AppendLine("                                    ,:21                            ");
            queryString.AppendLine("                                    ,:22                            ");
            queryString.AppendLine("                                    ,:23                            ");
            queryString.AppendLine("                                    ,:24                            ");
            queryString.AppendLine("                                    ,:25                            ");
            queryString.AppendLine("                                    ,:26                            ");
            queryString.AppendLine("                                    ,:27                            ");
            queryString.AppendLine("                                    ,:28                            ");
            queryString.AppendLine("                                    ,:29                            ");
            queryString.AppendLine("                                )                                   ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
                ,new OracleParameter("10", OracleDbType.Varchar2)
                ,new OracleParameter("11", OracleDbType.Varchar2)
                ,new OracleParameter("12", OracleDbType.Varchar2)
                ,new OracleParameter("13", OracleDbType.Varchar2)
                ,new OracleParameter("14", OracleDbType.Varchar2)
                ,new OracleParameter("15", OracleDbType.Varchar2)
                ,new OracleParameter("16", OracleDbType.Varchar2)
                ,new OracleParameter("17", OracleDbType.Varchar2)
                ,new OracleParameter("18", OracleDbType.Varchar2)
                ,new OracleParameter("19", OracleDbType.Varchar2)
                ,new OracleParameter("20", OracleDbType.Varchar2)
                ,new OracleParameter("21", OracleDbType.Varchar2)
                ,new OracleParameter("22", OracleDbType.Varchar2)
                ,new OracleParameter("23", OracleDbType.Varchar2)
                ,new OracleParameter("24", OracleDbType.Varchar2)
                ,new OracleParameter("25", OracleDbType.Varchar2)
                ,new OracleParameter("26", OracleDbType.Varchar2)
                ,new OracleParameter("27", OracleDbType.Varchar2)
                ,new OracleParameter("28", OracleDbType.Varchar2)
                ,new OracleParameter("29", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["RPT_NUMBER"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];
            parameters[2].Value = (string)conditions["NODE_ID"];
            parameters[3].Value = (string)conditions["ANALYSIS_TIME"];
            parameters[4].Value = (string)conditions["ELEVATION"];
            parameters[5].Value = (string)conditions["BASEDEMAND"];
            parameters[6].Value = (string)conditions["PATTERN"];
            parameters[7].Value = (string)conditions["EMITTER"];
            parameters[8].Value = (string)conditions["INITQUAL"];
            parameters[9].Value = (string)conditions["SOURCEQUAL"];
            parameters[10].Value = (string)conditions["SOURCEPAT"];
            parameters[11].Value = (string)conditions["SOURCETYPE"];
            parameters[12].Value = (string)conditions["TANKLEVEL"];
            parameters[13].Value = (string)conditions["DEMAND"];
            parameters[14].Value = (string)conditions["HEAD"];
            parameters[15].Value = (string)conditions["PRESSURE"];
            parameters[16].Value = (string)conditions["QUALITY"];
            parameters[17].Value = (string)conditions["SOURCEMASS"];
            parameters[18].Value = (string)conditions["INITVOLUME"];
            parameters[19].Value = (string)conditions["MIXMODEL"];
            parameters[20].Value = (string)conditions["MIXZONEVOL"];
            parameters[21].Value = (string)conditions["TANKDIAM"];
            parameters[22].Value = (string)conditions["MINVOLUME"];
            parameters[23].Value = (string)conditions["VOLCURVE"];
            parameters[24].Value = (string)conditions["MINLEVEL"];
            parameters[25].Value = (string)conditions["MAXLEVEL"];
            parameters[26].Value = (string)conditions["MIXFRACTION"];
            parameters[27].Value = (string)conditions["TANK_KBULK"];
            parameters[28].Value = (string)conditions["ANALYSIS_TYPE"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Link 해석결과 저장
        public void InsertLinkReportData(OracleDBManager dbManager, Hashtable conditions)
        {
            //queryString.Remove(0, queryString.Length);
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RPT_LINKS    (                                   ");
            queryString.AppendLine("								RPT_NUMBER						");
            queryString.AppendLine("								,INP_NUMBER						");
            queryString.AppendLine("								,LINK_ID						");
            queryString.AppendLine("								,ANALYSIS_TIME					");
            queryString.AppendLine("								,ANALYSIS_TYPE					");
            queryString.AppendLine("								,DIAMETER						");
            queryString.AppendLine("								,LENGTH							");
            queryString.AppendLine("								,ROUGHNESS						");
            queryString.AppendLine("								,MINORLOSS						");
            queryString.AppendLine("								,INITSTATUS						");
            queryString.AppendLine("								,INITSETTING					");
            queryString.AppendLine("								,KBULK							");
            queryString.AppendLine("								,KWALL							");
            queryString.AppendLine("								,FLOW							");
            queryString.AppendLine("								,VELOCITY						");
            queryString.AppendLine("								,HEADLOSS						");
            queryString.AppendLine("								,STATUS							");
            queryString.AppendLine("								,SETTING						");
            queryString.AppendLine("								,ENERGY							");
            queryString.AppendLine("                            ) values (                          ");
            queryString.AppendLine("                                :1                              ");
            queryString.AppendLine("                                ,:2                             ");
            queryString.AppendLine("                                ,:3                             ");
            queryString.AppendLine("                                ,:4                             ");
            queryString.AppendLine("                                ,:5                             ");
            queryString.AppendLine("                                ,:6                             ");
            queryString.AppendLine("                                ,:7                             ");
            queryString.AppendLine("                                ,:8                             ");
            queryString.AppendLine("                                ,:9                             ");
            queryString.AppendLine("                                ,:10                            ");
            queryString.AppendLine("                                ,:11                            ");
            queryString.AppendLine("                                ,:12                            ");
            queryString.AppendLine("                                ,:13                            ");
            queryString.AppendLine("                                ,:14                            ");
            queryString.AppendLine("                                ,:15                            ");
            queryString.AppendLine("                                ,:16                            ");
            queryString.AppendLine("                                ,:17                            ");
            queryString.AppendLine("                                ,:18                            ");
            queryString.AppendLine("                                ,:19                            ");
            queryString.AppendLine("                            )                                   ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
                ,new OracleParameter("10", OracleDbType.Varchar2)
                ,new OracleParameter("11", OracleDbType.Varchar2)
                ,new OracleParameter("12", OracleDbType.Varchar2)
                ,new OracleParameter("13", OracleDbType.Varchar2)
                ,new OracleParameter("14", OracleDbType.Varchar2)
                ,new OracleParameter("15", OracleDbType.Varchar2)
                ,new OracleParameter("16", OracleDbType.Varchar2)
                ,new OracleParameter("17", OracleDbType.Varchar2)
                ,new OracleParameter("18", OracleDbType.Varchar2)
                ,new OracleParameter("19", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["RPT_NUMBER"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];
            parameters[2].Value = (string)conditions["LINK_ID"];
            parameters[3].Value = (string)conditions["ANALYSIS_TIME"];
            parameters[4].Value = (string)conditions["ANALYSIS_TYPE"];
            parameters[5].Value = (string)conditions["DIAMETER"];
            parameters[6].Value = (string)conditions["LENGTH"];
            parameters[7].Value = (string)conditions["ROUGHNESS"];
            parameters[8].Value = (string)conditions["MINORLOSS"];
            parameters[9].Value = (string)conditions["INITSTATUS"];
            parameters[10].Value = (string)conditions["INITSETTING"];
            parameters[11].Value = (string)conditions["KBULK"];
            parameters[12].Value = (string)conditions["KWALL"];
            parameters[13].Value = (string)conditions["FLOW"];
            parameters[14].Value = (string)conditions["VELOCITY"];
            parameters[15].Value = (string)conditions["HEADLOSS"];
            parameters[16].Value = (string)conditions["STATUS"];
            parameters[17].Value = (string)conditions["SETTING"];
            parameters[18].Value = (string)conditions["ENERGY"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        public void InsertNodeAnalysisResultBulk(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RPT_NODES        (                                   ");
            queryString.AppendLine("									RPT_NUMBER						");
            queryString.AppendLine("									,INP_NUMBER						");
            queryString.AppendLine("									,NODE_ID						");
            queryString.AppendLine("									,ANALYSIS_TIME					");
            queryString.AppendLine("									,ELEVATION						");
            queryString.AppendLine("									,BASEDEMAND						");
            queryString.AppendLine("									,PATTERN						");
            queryString.AppendLine("									,EMITTER						");
            queryString.AppendLine("									,INITQUAL						");
            queryString.AppendLine("									,SOURCEQUAL						");
            queryString.AppendLine("									,SOURCEPAT						");
            queryString.AppendLine("									,SOURCETYPE						");
            queryString.AppendLine("									,TANKLEVEL						");
            queryString.AppendLine("									,DEMAND							");
            queryString.AppendLine("									,HEAD							");
            queryString.AppendLine("									,PRESSURE						");
            queryString.AppendLine("									,QUALITY						");
            queryString.AppendLine("									,SOURCEMASS						");
            queryString.AppendLine("									,INITVOLUME						");
            queryString.AppendLine("									,MIXMODEL						");
            queryString.AppendLine("									,MIXZONEVOL						");
            queryString.AppendLine("									,TANKDIAM						");
            queryString.AppendLine("									,MINVOLUME						");
            queryString.AppendLine("									,VOLCURVE						");
            queryString.AppendLine("									,MINLEVEL						");
            queryString.AppendLine("									,MAXLEVEL						");
            queryString.AppendLine("									,MIXFRACTION					");
            queryString.AppendLine("									,TANK_KBULK						");
            queryString.AppendLine("									,ANALYSIS_TYPE					");
            queryString.AppendLine("                                ) values (                          ");
            queryString.AppendLine("                                    :1                              ");
            queryString.AppendLine("                                    ,:2                             ");
            queryString.AppendLine("                                    ,:3                             ");
            queryString.AppendLine("                                    ,:4                             ");
            queryString.AppendLine("                                    ,:5                             ");
            queryString.AppendLine("                                    ,:6                             ");
            queryString.AppendLine("                                    ,:7                             ");
            queryString.AppendLine("                                    ,:8                             ");
            queryString.AppendLine("                                    ,:9                             ");
            queryString.AppendLine("                                    ,:10                            ");
            queryString.AppendLine("                                    ,:11                            ");
            queryString.AppendLine("                                    ,:12                            ");
            queryString.AppendLine("                                    ,:13                            ");
            queryString.AppendLine("                                    ,:14                            ");
            queryString.AppendLine("                                    ,:15                            ");
            queryString.AppendLine("                                    ,:16                            ");
            queryString.AppendLine("                                    ,:17                            ");
            queryString.AppendLine("                                    ,:18                            ");
            queryString.AppendLine("                                    ,:19                            ");
            queryString.AppendLine("                                    ,:20                            ");
            queryString.AppendLine("                                    ,:21                            ");
            queryString.AppendLine("                                    ,:22                            ");
            queryString.AppendLine("                                    ,:23                            ");
            queryString.AppendLine("                                    ,:24                            ");
            queryString.AppendLine("                                    ,:25                            ");
            queryString.AppendLine("                                    ,:26                            ");
            queryString.AppendLine("                                    ,:27                            ");
            queryString.AppendLine("                                    ,:28                            ");
            queryString.AppendLine("                                    ,:29                            ");
            queryString.AppendLine("                                )                                   ");

            OracleCommand commandOracle = new OracleCommand();
            commandOracle.CommandText = queryString.ToString();
            commandOracle.CommandTimeout = 1000 * 60 * 10;
            commandOracle.Connection = dbManager.Connection;
            commandOracle.CommandType = CommandType.Text;
            commandOracle.ArrayBindCount = ((List<string>)conditions["nodeRptNumberList"]).Count;

            OracleParameter prmNodeRptNumberList = new OracleParameter("1", OracleDbType.Varchar2);
            prmNodeRptNumberList.Direction = ParameterDirection.Input;
            prmNodeRptNumberList.Value = ((List<string>)conditions["nodeRptNumberList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeRptNumberList);

            OracleParameter prmNodeInpNumberList = new OracleParameter("2", OracleDbType.Varchar2);
            prmNodeInpNumberList.Direction = ParameterDirection.Input;
            prmNodeInpNumberList.Value = ((List<string>)conditions["nodeInpNumberList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeInpNumberList);

            OracleParameter prmNodeNodeIdList = new OracleParameter("3", OracleDbType.Varchar2);
            prmNodeNodeIdList.Direction = ParameterDirection.Input;
            prmNodeNodeIdList.Value = ((List<string>)conditions["nodeNodeIdList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeNodeIdList);

            OracleParameter prmNodeAnalysisTimeList = new OracleParameter("4", OracleDbType.Varchar2);
            prmNodeAnalysisTimeList.Direction = ParameterDirection.Input;
            prmNodeAnalysisTimeList.Value = ((List<string>)conditions["nodeAnalysisTimeList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeAnalysisTimeList);

            OracleParameter prmNodeElevationList = new OracleParameter("5", OracleDbType.Varchar2);
            prmNodeElevationList.Direction = ParameterDirection.Input;
            prmNodeElevationList.Value = ((List<string>)conditions["nodeElevationList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeElevationList);

            OracleParameter prmNodeBasedemandList = new OracleParameter("6", OracleDbType.Varchar2);
            prmNodeBasedemandList.Direction = ParameterDirection.Input;
            prmNodeBasedemandList.Value = ((List<string>)conditions["nodeBasedemandList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeBasedemandList);

            OracleParameter prmNodePatternList = new OracleParameter("7", OracleDbType.Varchar2);
            prmNodePatternList.Direction = ParameterDirection.Input;
            prmNodePatternList.Value = ((List<string>)conditions["nodePatternList"]).ToArray();
            commandOracle.Parameters.Add(prmNodePatternList);

            OracleParameter prmNodeEmitterList = new OracleParameter("8", OracleDbType.Varchar2);
            prmNodeEmitterList.Direction = ParameterDirection.Input;
            prmNodeEmitterList.Value = ((List<string>)conditions["nodeEmitterList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeEmitterList);

            OracleParameter prmNodeInitqualList = new OracleParameter("9", OracleDbType.Varchar2);
            prmNodeInitqualList.Direction = ParameterDirection.Input;
            prmNodeInitqualList.Value = ((List<string>)conditions["nodeInitqualList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeInitqualList);

            OracleParameter prmNodeSourcequalList = new OracleParameter("10", OracleDbType.Varchar2);
            prmNodeSourcequalList.Direction = ParameterDirection.Input;
            prmNodeSourcequalList.Value = ((List<string>)conditions["nodeSourcequalList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeSourcequalList);

            OracleParameter prmNodeSourcepatList = new OracleParameter("11", OracleDbType.Varchar2);
            prmNodeSourcepatList.Direction = ParameterDirection.Input;
            prmNodeSourcepatList.Value = ((List<string>)conditions["nodeSourcepatList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeSourcepatList);

            OracleParameter prmNodeSourcetypeList = new OracleParameter("12", OracleDbType.Varchar2);
            prmNodeSourcetypeList.Direction = ParameterDirection.Input;
            prmNodeSourcetypeList.Value = ((List<string>)conditions["nodeSourcetypeList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeSourcetypeList);

            OracleParameter prmNodeTanklevelList = new OracleParameter("13", OracleDbType.Varchar2);
            prmNodeTanklevelList.Direction = ParameterDirection.Input;
            prmNodeTanklevelList.Value = ((List<string>)conditions["nodeTanklevelList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeTanklevelList);

            OracleParameter prmNodeDemandList = new OracleParameter("14", OracleDbType.Varchar2);
            prmNodeDemandList.Direction = ParameterDirection.Input;
            prmNodeDemandList.Value = ((List<string>)conditions["nodeDemandList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeDemandList);

            OracleParameter prmNodeHeadList = new OracleParameter("15", OracleDbType.Varchar2);
            prmNodeHeadList.Direction = ParameterDirection.Input;
            prmNodeHeadList.Value = ((List<string>)conditions["nodeHeadList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeHeadList);

            OracleParameter prmNodePressureList = new OracleParameter("16", OracleDbType.Varchar2);
            prmNodePressureList.Direction = ParameterDirection.Input;
            prmNodePressureList.Value = ((List<string>)conditions["nodePressureList"]).ToArray();
            commandOracle.Parameters.Add(prmNodePressureList);

            OracleParameter prmNodeQualityList = new OracleParameter("17", OracleDbType.Varchar2);
            prmNodeQualityList.Direction = ParameterDirection.Input;
            prmNodeQualityList.Value = ((List<string>)conditions["nodeQualityList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeQualityList);

            OracleParameter prmNodeSourcemassList = new OracleParameter("18", OracleDbType.Varchar2);
            prmNodeSourcemassList.Direction = ParameterDirection.Input;
            prmNodeSourcemassList.Value = ((List<string>)conditions["nodeSourcemassList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeSourcemassList);

            OracleParameter prmNodeInitvolumeList = new OracleParameter("19", OracleDbType.Varchar2);
            prmNodeInitvolumeList.Direction = ParameterDirection.Input;
            prmNodeInitvolumeList.Value = ((List<string>)conditions["nodeInitvolumeList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeInitvolumeList);

            OracleParameter prmNodeMixmodelList = new OracleParameter("20", OracleDbType.Varchar2);
            prmNodeMixmodelList.Direction = ParameterDirection.Input;
            prmNodeMixmodelList.Value = ((List<string>)conditions["nodeMixmodelList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeMixmodelList);

            OracleParameter prmNodeMixzonevolList = new OracleParameter("21", OracleDbType.Varchar2);
            prmNodeMixzonevolList.Direction = ParameterDirection.Input;
            prmNodeMixzonevolList.Value = ((List<string>)conditions["nodeMixzonevolList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeMixzonevolList);

            OracleParameter prmNodeTankdiamList = new OracleParameter("22", OracleDbType.Varchar2);
            prmNodeTankdiamList.Direction = ParameterDirection.Input;
            prmNodeTankdiamList.Value = ((List<string>)conditions["nodeTankdiamList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeTankdiamList);

            OracleParameter prmNodeMinvolumeList = new OracleParameter("23", OracleDbType.Varchar2);
            prmNodeMinvolumeList.Direction = ParameterDirection.Input;
            prmNodeMinvolumeList.Value = ((List<string>)conditions["nodeMinvolumeList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeMinvolumeList);

            OracleParameter prmNodeVolcurveList = new OracleParameter("24", OracleDbType.Varchar2);
            prmNodeVolcurveList.Direction = ParameterDirection.Input;
            prmNodeVolcurveList.Value = ((List<string>)conditions["nodeVolcurveList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeVolcurveList);

            OracleParameter prmNodeMinlevelList = new OracleParameter("25", OracleDbType.Varchar2);
            prmNodeMinlevelList.Direction = ParameterDirection.Input;
            prmNodeMinlevelList.Value = ((List<string>)conditions["nodeMinlevelList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeMinlevelList);

            OracleParameter prmNodeMaxlevelList = new OracleParameter("26", OracleDbType.Varchar2);
            prmNodeMaxlevelList.Direction = ParameterDirection.Input;
            prmNodeMaxlevelList.Value = ((List<string>)conditions["nodeMaxlevelList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeMaxlevelList);

            OracleParameter prmNodeMixfractionList = new OracleParameter("27", OracleDbType.Varchar2);
            prmNodeMixfractionList.Direction = ParameterDirection.Input;
            prmNodeMixfractionList.Value = ((List<string>)conditions["nodeMixfractionList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeMixfractionList);

            OracleParameter prmNodeTankKbulkList = new OracleParameter("28", OracleDbType.Varchar2);
            prmNodeTankKbulkList.Direction = ParameterDirection.Input;
            prmNodeTankKbulkList.Value = ((List<string>)conditions["nodeTankKbulkList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeTankKbulkList);

            OracleParameter prmNodeAnalysisTypeList = new OracleParameter("29", OracleDbType.Varchar2);
            prmNodeAnalysisTypeList.Direction = ParameterDirection.Input;
            prmNodeAnalysisTypeList.Value = ((List<string>)conditions["nodeAnalysisTypeList"]).ToArray();
            commandOracle.Parameters.Add(prmNodeAnalysisTypeList);

            try
            {
                commandOracle.ExecuteNonQuery();
                foreach (OracleParameter parameter in commandOracle.Parameters) parameter.Dispose();
                commandOracle.Parameters.Clear();
                commandOracle.Dispose();
                queryString.Remove(0, queryString.Length);
            }
            catch (Exception ex)
            {

                System.Diagnostics.Debug.Print(ex.Source);
            }

        }

        public void InsertLinkAnalysisResultBulk(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RPT_LINKS    (                                   ");
            queryString.AppendLine("								RPT_NUMBER						");
            queryString.AppendLine("								,INP_NUMBER						");
            queryString.AppendLine("								,LINK_ID						");
            queryString.AppendLine("								,ANALYSIS_TIME					");
            queryString.AppendLine("								,ANALYSIS_TYPE					");
            queryString.AppendLine("								,DIAMETER						");
            queryString.AppendLine("								,LENGTH							");
            queryString.AppendLine("								,ROUGHNESS						");
            queryString.AppendLine("								,MINORLOSS						");
            queryString.AppendLine("								,INITSTATUS						");
            queryString.AppendLine("								,INITSETTING					");
            queryString.AppendLine("								,KBULK							");
            queryString.AppendLine("								,KWALL							");
            queryString.AppendLine("								,FLOW							");
            queryString.AppendLine("								,VELOCITY						");
            queryString.AppendLine("								,HEADLOSS						");
            queryString.AppendLine("								,STATUS							");
            queryString.AppendLine("								,SETTING						");
            queryString.AppendLine("								,ENERGY							");
            queryString.AppendLine("                            ) values (                          ");
            queryString.AppendLine("                                :1                              ");
            queryString.AppendLine("                                ,:2                             ");
            queryString.AppendLine("                                ,:3                             ");
            queryString.AppendLine("                                ,:4                             ");
            queryString.AppendLine("                                ,:5                             ");
            queryString.AppendLine("                                ,:6                             ");
            queryString.AppendLine("                                ,:7                             ");
            queryString.AppendLine("                                ,:8                             ");
            queryString.AppendLine("                                ,:9                             ");
            queryString.AppendLine("                                ,:10                            ");
            queryString.AppendLine("                                ,:11                            ");
            queryString.AppendLine("                                ,:12                            ");
            queryString.AppendLine("                                ,:13                            ");
            queryString.AppendLine("                                ,:14                            ");
            queryString.AppendLine("                                ,:15                            ");
            queryString.AppendLine("                                ,:16                            ");
            queryString.AppendLine("                                ,:17                            ");
            queryString.AppendLine("                                ,:18                            ");
            queryString.AppendLine("                                ,:19                            ");
            queryString.AppendLine("                            )                                   ");

            OracleCommand commandOracle = new OracleCommand();
            commandOracle.CommandText = queryString.ToString();
            commandOracle.CommandTimeout = 1000 * 60 * 10;
            commandOracle.Connection = dbManager.Connection;
            commandOracle.CommandType = CommandType.Text;
            commandOracle.ArrayBindCount = ((List<string>)conditions["linkRptNumber"]).Count;

            OracleParameter prmLinkRptNumber = new OracleParameter("1", OracleDbType.Varchar2);
            prmLinkRptNumber.Direction = ParameterDirection.Input;
            prmLinkRptNumber.Value = ((List<string>)conditions["linkRptNumber"]).ToArray();
            commandOracle.Parameters.Add(prmLinkRptNumber);

            OracleParameter prmLinkInpNumber = new OracleParameter("2", OracleDbType.Varchar2);
            prmLinkInpNumber.Direction = ParameterDirection.Input;
            prmLinkInpNumber.Value = ((List<string>)conditions["linkInpNumber"]).ToArray();
            commandOracle.Parameters.Add(prmLinkInpNumber);

            OracleParameter prmLinkLinkId = new OracleParameter("3", OracleDbType.Varchar2);
            prmLinkLinkId.Direction = ParameterDirection.Input;
            prmLinkLinkId.Value = ((List<string>)conditions["linkLinkId"]).ToArray();
            commandOracle.Parameters.Add(prmLinkLinkId);

            OracleParameter prmLinkAnalysisTime = new OracleParameter("4", OracleDbType.Varchar2);
            prmLinkAnalysisTime.Direction = ParameterDirection.Input;
            prmLinkAnalysisTime.Value = ((List<string>)conditions["linkAnalysisTime"]).ToArray();
            commandOracle.Parameters.Add(prmLinkAnalysisTime);

            OracleParameter prmLinkAnalysisType = new OracleParameter("5", OracleDbType.Varchar2);
            prmLinkAnalysisType.Direction = ParameterDirection.Input;
            prmLinkAnalysisType.Value = ((List<string>)conditions["linkAnalysisType"]).ToArray();
            commandOracle.Parameters.Add(prmLinkAnalysisType);

            OracleParameter prmLinkDiameter = new OracleParameter("6", OracleDbType.Varchar2);
            prmLinkDiameter.Direction = ParameterDirection.Input;
            prmLinkDiameter.Value = ((List<string>)conditions["linkDiameter"]).ToArray();
            commandOracle.Parameters.Add(prmLinkDiameter);

            OracleParameter prmLinkLength = new OracleParameter("7", OracleDbType.Varchar2);
            prmLinkLength.Direction = ParameterDirection.Input;
            prmLinkLength.Value = ((List<string>)conditions["linkLength"]).ToArray();
            commandOracle.Parameters.Add(prmLinkLength);

            OracleParameter prmLinkRoughness = new OracleParameter("8", OracleDbType.Varchar2);
            prmLinkRoughness.Direction = ParameterDirection.Input;
            prmLinkRoughness.Value = ((List<string>)conditions["linkRoughness"]).ToArray();
            commandOracle.Parameters.Add(prmLinkRoughness);

            OracleParameter prmLinkMinorloss = new OracleParameter("9", OracleDbType.Varchar2);
            prmLinkMinorloss.Direction = ParameterDirection.Input;
            prmLinkMinorloss.Value = ((List<string>)conditions["linkMinorloss"]).ToArray();
            commandOracle.Parameters.Add(prmLinkMinorloss);

            OracleParameter prmLinkInitstatus = new OracleParameter("10", OracleDbType.Varchar2);
            prmLinkInitstatus.Direction = ParameterDirection.Input;
            prmLinkInitstatus.Value = ((List<string>)conditions["linkInitstatus"]).ToArray();
            commandOracle.Parameters.Add(prmLinkInitstatus);

            OracleParameter prmLinkInitsetting = new OracleParameter("11", OracleDbType.Varchar2);
            prmLinkInitsetting.Direction = ParameterDirection.Input;
            prmLinkInitsetting.Value = ((List<string>)conditions["linkInitsetting"]).ToArray();
            commandOracle.Parameters.Add(prmLinkInitsetting);

            OracleParameter prmLinkKbulk = new OracleParameter("12", OracleDbType.Varchar2);
            prmLinkKbulk.Direction = ParameterDirection.Input;
            prmLinkKbulk.Value = ((List<string>)conditions["linkKbulk"]).ToArray();
            commandOracle.Parameters.Add(prmLinkKbulk);

            OracleParameter prmLinkKwall = new OracleParameter("13", OracleDbType.Varchar2);
            prmLinkKwall.Direction = ParameterDirection.Input;
            prmLinkKwall.Value = ((List<string>)conditions["linkKwall"]).ToArray();
            commandOracle.Parameters.Add(prmLinkKwall);

            OracleParameter prmLinkFlow = new OracleParameter("14", OracleDbType.Varchar2);
            prmLinkFlow.Direction = ParameterDirection.Input;
            prmLinkFlow.Value = ((List<string>)conditions["linkFlow"]).ToArray();
            commandOracle.Parameters.Add(prmLinkFlow);

            OracleParameter prmLinkVelocity = new OracleParameter("15", OracleDbType.Varchar2);
            prmLinkVelocity.Direction = ParameterDirection.Input;
            prmLinkVelocity.Value = ((List<string>)conditions["linkVelocity"]).ToArray();
            commandOracle.Parameters.Add(prmLinkVelocity);

            OracleParameter prmLinkHeadloss = new OracleParameter("16", OracleDbType.Varchar2);
            prmLinkHeadloss.Direction = ParameterDirection.Input;
            prmLinkHeadloss.Value = ((List<string>)conditions["linkHeadloss"]).ToArray();
            commandOracle.Parameters.Add(prmLinkHeadloss);

            OracleParameter prmLinkStatus = new OracleParameter("17", OracleDbType.Varchar2);
            prmLinkStatus.Direction = ParameterDirection.Input;
            prmLinkStatus.Value = ((List<string>)conditions["linkStatus"]).ToArray();
            commandOracle.Parameters.Add(prmLinkStatus);

            OracleParameter prmLinkSetting = new OracleParameter("18", OracleDbType.Varchar2);
            prmLinkSetting.Direction = ParameterDirection.Input;
            prmLinkSetting.Value = ((List<string>)conditions["linkSetting"]).ToArray();
            commandOracle.Parameters.Add(prmLinkSetting);

            OracleParameter prmLinkEnergy = new OracleParameter("19", OracleDbType.Varchar2);
            prmLinkEnergy.Direction = ParameterDirection.Input;
            prmLinkEnergy.Value = ((List<string>)conditions["linkEnergy"]).ToArray();
            commandOracle.Parameters.Add(prmLinkEnergy);

            try
            {
                commandOracle.ExecuteNonQuery();
                foreach (OracleParameter parameter in commandOracle.Parameters) parameter.Dispose();
                commandOracle.Parameters.Clear();
                commandOracle.Dispose();
                queryString.Remove(0, queryString.Length);
            }
            catch (Exception ex)
            {

                System.Diagnostics.Debug.Print(ex.Source);
            }
        }

        //해석결과 마스터 삭제
        public void DeleteReportMaster(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_RPT_MASTER                                       ");
            queryString.AppendLine("where       RPT_NUMBER = '" + conditions["RPT_NUMBER"] + "'     ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Node 해석결과 삭제
        public void DeleteNodeReport(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_RPT_NODES                                        ");
            queryString.AppendLine("where       RPT_NUMBER = '" + conditions["RPT_NUMBER"] + "'     ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //Link 해석결과 삭제
        public void DeleteLinkReport(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("delete from WH_RPT_LINKS                                        ");
            queryString.AppendLine("where       RPT_NUMBER = '" + conditions["RPT_NUMBER"] + "'     ");

            dbManager.ExecuteScript(queryString.ToString(), null);
        }

        //해석오류 입력
        public void InsertRealtimeAnalysisError(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_REALTIME_ANALYSIS_ERROR  (                   ");
            queryString.AppendLine("                                            IDX             ");
            queryString.AppendLine("                                            ,INP_NUMBER     ");
            queryString.AppendLine("                                            ,ERROR_CODE     ");
            queryString.AppendLine("                                            ,INCREASE_DATE  ");
            queryString.AppendLine("                                        ) values (          ");
            queryString.AppendLine("                                            :1              ");
            queryString.AppendLine("                                            ,:2             ");
            queryString.AppendLine("                                            ,:3             ");
            queryString.AppendLine("                                            ,:4             ");
            queryString.AppendLine("                                        )                   ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["IDX"];
            parameters[1].Value = (string)conditions["INP_NUMBER"];
            parameters[2].Value = (string)conditions["ERROR_CODE"];
            parameters[3].Value = (string)conditions["INCREASE_DATE"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //관망해석 설정조회
        public DataSet SelectAnalysisSetting(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          ERROR_SAVE_YN                                       ");
            queryString.AppendLine("from            WH_ANALYSIS_SETTING                                 ");
            queryString.AppendLine("where           INP_NUMBER = '" + conditions["INP_NUMBER"] + "'     ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_ANALYSIS_SETTING");
        }
    }
}
