﻿/**************************************************************************
 * 파일명   : WHINPManageDao.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.17
 * 설명     : INP파일 내용을 핸들링하는 Data Access Object 
 *            (외부에서 사용되는 관계로 커플링을 피하기위해 INPManager와 내용은 같으나 별도로 구성)
 * 변경이력 : 2010.10.17 - 최초생성
 **************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using WaterNet.WaterNetCore;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace WaterNet.WH_PipingNetworkAnalysis.dao
{
    public class INPManageDao
    {
        //외부에서 사용될 참조변수
        private static INPManageDao dao = null;

        //대량의 입력자료가 존재하는 경우 빈번한 객체생성은 메모리 오버플로우의 원인이 된다.
        private StringBuilder queryString = new StringBuilder();

        //기본생성자
        private INPManageDao()
        {
        }

        //참조변수를 반환
        public static INPManageDao GetInstance()
        {
            if(dao == null)
            {
                dao = new INPManageDao();
            }

            return dao;
        }

        //INP Master정보 입력
        public void InsertINPMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder   queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_TITLE   (                                                        ");
            queryString.AppendLine("                            INP_NUMBER                                          ");
            queryString.AppendLine("                            ,USE_GBN                                            ");
            queryString.AppendLine("                            ,WSP_NAM                                            ");
            queryString.AppendLine("                            ,LFTRIDN                                            ");
            queryString.AppendLine("                            ,MFTRIDN                                            ");
            queryString.AppendLine("                            ,SFTRIDN                                            ");
            queryString.AppendLine("                            ,INS_DATE                                           ");
            queryString.AppendLine("                            ,TITLE                                              ");
            queryString.AppendLine("                            ,FILE_NAME                                          ");
            queryString.AppendLine("                            ,REMARK                                             ");
            queryString.AppendLine("                        )                                                       ");
            queryString.AppendLine("values                  (                                                       ");
            queryString.AppendLine("                            :1                                                  ");
            queryString.AppendLine("                            ,:2                                                 ");
            queryString.AppendLine("                            ,:3                                                 ");
            queryString.AppendLine("                            ,:4                                                 ");
            queryString.AppendLine("                            ,:5                                                 ");
            queryString.AppendLine("                            ,:6                                                 ");
            queryString.AppendLine("                            ,to_char(sysdate,'yyyymmddhh24miss')                ");
            queryString.AppendLine("                            ,:8                                                 ");
            queryString.AppendLine("                            ,:9                                                 ");
            queryString.AppendLine("                            ,:10                                                ");
            queryString.AppendLine("                        )                                                       ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["USE_GBN"];
            parameters[2].Value = (string)conditions["WSP_NAM"];
            parameters[3].Value = (string)conditions["LFTRIDN"];
            parameters[4].Value = (string)conditions["MFTRIDN"];
            parameters[5].Value = (string)conditions["SFTRIDN"];
            parameters[6].Value = (string)conditions["TITLE"];
            parameters[7].Value = (string)conditions["FILE_NAME"];
            parameters[8].Value = (string)conditions["REMARK"];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //INP Master리스트 조회
        public DataSet SelectINPMasterList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.Append("select      INP_NUMBER                                                                                      ");
            queryString.Append("            ,USE_GBN                                                                                        ");
            //queryString.Append("            ,WSP_NAM                                                                                      ");           //배수계통 - 사용보류
            queryString.Append("            ,LFTRIDN                                                                                        ");
            queryString.Append("            ,MFTRIDN                                                                                        ");
            queryString.Append("            ,SFTRIDN                                                                                        ");
            queryString.Append("            ,to_char(to_date(INS_DATE,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')      as INS_DATE    ");
            queryString.Append("            ,TITLE                                                                                          ");
            queryString.Append("            ,REMARK                                                                                         ");
            queryString.Append("            ,FILE_NAME                                                                                      ");
            queryString.Append("from        WH_TITLE                                                                                        ");
            queryString.Append("where       1 = 1                                                                                           ");

            if (!"".Equals(conditions["USE_GBN"]))
            {
                queryString.Append("and     USE_GBN     = '" + conditions["USE_GBN"] + "'                                                   ");
            }

            if (!"".Equals(conditions["LFTRIDN"]))
            {
                queryString.Append("and     LFTRIDN     = '" + conditions["LFTRIDN"] + "'                                                   ");
            }

            if (!"".Equals(conditions["MFTRIDN"]))
            {
                queryString.Append("and     MFTRIDN     = '" + conditions["MFTRIDN"] + "'                                                   ");
            }

            if (!"".Equals(conditions["SFTRIDN"]))
            {
                queryString.Append("and     SFTRIDN     = '" + conditions["SFTRIDN"] + "'                                                   ");
            }

            //날짜는 늘 넘어오므로 기본할당.
            queryString.Append("and         INS_DATE                                                                                        ");
            queryString.Append("between     '" + conditions["startDate"] + "'                                                               ");
            queryString.Append("and         '" + conditions["endDate"] + "235959'                                                           ");

            if (!"".Equals(conditions["TITLE"]))
            {
                queryString.Append("and     TITLE       like '%" + conditions["TITLE"] + "%'                                                ");
            }

            queryString.Append("order by    to_date(INS_DATE,'yyyy-mm-dd hh24:mi:ss')                                                       ");
            queryString.Append("desc                                                                                                        ");
            queryString.Append("            ,LFTRIDN                                                                                        ");
            queryString.Append("            ,MFTRIDN                                                                                        ");
            queryString.Append("            ,SFTRIDN                                                                                        ");
            queryString.Append("asc                                                                                                         ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //INP Master 정보 조회
        public DataSet SelectINPMasterData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.Append("select      INP_NUMBER                                                                                      ");
            queryString.Append("            ,USE_GBN                                                                                        ");
            queryString.Append("            ,LFTRIDN                                                                                        ");
            queryString.Append("            ,MFTRIDN                                                                                        ");
            queryString.Append("            ,SFTRIDN                                                                                        ");
            queryString.Append("            ,to_char(to_date(INS_DATE,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')      as INS_DATE    ");
            queryString.Append("            ,TITLE                                                                                          ");
            queryString.Append("            ,REMARK                                                                                         ");
            queryString.Append("            ,FILE_NAME                                                                                      ");
            queryString.Append("from        WH_TITLE                                                                                        ");
            queryString.Append("where       1 = 1                                                                                           ");
            queryString.Append("and         INP_NUMBER  = '"+conditions["INP_NUMBER"]+"'                                                    ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //JUNCTION 정보 입력
        public void InsertJunctionData(OracleDBManager dbManager, ArrayList conditions)
        {
            //StringBuilder   queryString = new StringBuilder();
            queryString.Remove(0, queryString.Length);

            queryString.AppendLine("insert into WH_JUNCTIONS    (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,ID                     ");
            queryString.AppendLine("                                ,ELEV                   ");
            queryString.AppendLine("                                ,DEMAND                 ");
            queryString.AppendLine("                                ,DEVIDED_DEMAND         ");
            queryString.AppendLine("                                ,PATTERN_ID             ");
            queryString.AppendLine("                                ,SFTRIDN                ");
            queryString.AppendLine("                                ,REMARK                 ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                                ,:5                     ");
            queryString.AppendLine("                                ,:6                     ");
            queryString.AppendLine("                                ,:7                     ");
            queryString.AppendLine("                                ,:8                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];
            parameters[4].Value = "0";
            parameters[5].Value = (string)conditions[4];
            parameters[6].Value = "1";
            parameters[7].Value = (string)conditions[11];            //주석은 11번째 위치

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //JUNCTION 리스트 조회
        public DataSet SelectJunctionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            //queryString.AppendLine("select      INP_NUMBER			                                    ");
            queryString.AppendLine("select      ID				                                        ");
            queryString.AppendLine("            ,ELEV				                                    ");
            queryString.AppendLine("            ,DEMAND				                                    ");
            //queryString.AppendLine("            ,DEVIDED_DEMAND		                                    ");
            if (conditions["WQ_MODEL_MAKE"] != null)
            {
                queryString.AppendLine("            ,NULL as PATTERN_ID	                                    ");
            }
            else
            {
                queryString.AppendLine("            ,PATTERN_ID			                                    ");
            }
            queryString.AppendLine("            ,SFTRIDN			                                    ");
            queryString.AppendLine("            ,REMARK				                                    ");
            queryString.AppendLine("from        WH_JUNCTIONS		                                    ");
            queryString.AppendLine("where       INP_NUMBER        = '"+ conditions["INP_NUMBER"] +"'	");
            queryString.AppendLine("order by    ID				                                        ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_JUNCTIONS");
        }

        //RESERVOIR 정보 입력
        public void InsertReservoirData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RESERVOIRS   (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,ID                     ");
            queryString.AppendLine("                                ,HEAD                   ");
            queryString.AppendLine("                                ,PATTERN_ID             ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //RESERVOIR 리스트 조회
        public DataSet SelectReservoirList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,HEAD                                                   ");
            queryString.AppendLine("            ,PATTERN_ID                                             ");
            queryString.AppendLine("from        WH_RESERVOIRS                                           ");
            queryString.AppendLine("where       INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RESERVOIRS");
        }

        //TANK 정보 입력
        public void InsertTankData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_TANK (                           ");
            queryString.AppendLine("                        INP_NUMBER              ");
            queryString.AppendLine("                        ,ID                     ");
            queryString.AppendLine("                        ,ELEV                   ");
            queryString.AppendLine("                        ,INITLVL                ");
            queryString.AppendLine("                        ,MINLVL                 ");
            queryString.AppendLine("                        ,MAXLVL                 ");
            queryString.AppendLine("                        ,DIAM                   ");
            queryString.AppendLine("                        ,MINVOL                 ");
            queryString.AppendLine("                        ,VOLCURVE_ID            ");
            queryString.AppendLine("                    )                           ");
            queryString.AppendLine("values              (                           ");
            queryString.AppendLine("                        :1                      ");
            queryString.AppendLine("                        ,:2                     ");
            queryString.AppendLine("                        ,:3                     ");
            queryString.AppendLine("                        ,:4                     ");
            queryString.AppendLine("                        ,:5                     ");
            queryString.AppendLine("                        ,:6                     ");
            queryString.AppendLine("                        ,:7                     ");
            queryString.AppendLine("                        ,:8                     ");
            queryString.AppendLine("                        ,:9                     ");
            queryString.AppendLine("                    )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];
            parameters[4].Value = (string)conditions[4];
            parameters[5].Value = (string)conditions[5];
            parameters[6].Value = (string)conditions[6];
            parameters[7].Value = (string)conditions[7];
            parameters[8].Value = (string)conditions[8];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //TANK 리스트 조회
        public DataSet SelectTankList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,ELEV                                                   ");
            queryString.AppendLine("            ,INITLVL                                                ");
            queryString.AppendLine("            ,MINLVL                                                 ");
            queryString.AppendLine("            ,MAXLVL                                                 ");
            queryString.AppendLine("            ,DIAM                                                   ");
            queryString.AppendLine("            ,MINVOL                                                 ");
            queryString.AppendLine("            ,VOLCURVE_ID                                            ");
            queryString.AppendLine("from        WH_TANK                                                 ");
            queryString.AppendLine("where       INP_NUMBER      = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TANK");
        }

        //Pipe 정보 입력
        public void InsertPipeData(OracleDBManager dbManager, ArrayList conditions)
        {
            //StringBuilder queryString = new StringBuilder();
            queryString.Remove(0, queryString.Length);

            //누수계수가 기본으로 적용된 inp만 등록이 가능하다.
            queryString.AppendLine("insert into WH_PIPES    (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,NODE1                  ");
            queryString.AppendLine("                            ,NODE2                  ");
            queryString.AppendLine("                            ,LENGTH                 ");
            queryString.AppendLine("                            ,DIAM                   ");
            queryString.AppendLine("                            ,LEAKAGE_COEFFICIENT    ");
            queryString.AppendLine("                            ,ROUGHNESS              ");
            queryString.AppendLine("                            ,MLOSS                  ");
            queryString.AppendLine("                            ,STATUS                 ");
            queryString.AppendLine("                            ,REMARK                 ");
            queryString.AppendLine("                            ,SFTRIDN                ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                            ,:6                     ");
            queryString.AppendLine("                            ,:7                     ");
            queryString.AppendLine("                            ,:8                     ");
            queryString.AppendLine("                            ,:9                     ");
            queryString.AppendLine("                            ,:10                    ");
            queryString.AppendLine("                            ,:11                    ");
            queryString.AppendLine("                            ,:12                    ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
                ,new OracleParameter("9", OracleDbType.Varchar2)
                ,new OracleParameter("10", OracleDbType.Varchar2)
                ,new OracleParameter("11", OracleDbType.Varchar2)
                ,new OracleParameter("12", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0]; ;
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];
            parameters[4].Value = (string)conditions[4];
            parameters[5].Value = (string)conditions[5];
            parameters[6].Value = (string)conditions[6];
            parameters[7].Value = (string)conditions[7];
            parameters[8].Value = (string)conditions[8];
            parameters[9].Value = (string)conditions[9];
            parameters[10].Value = (string)conditions[11];                  //주석은 12번째 위치
            parameters[11].Value = "소블록";

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Pipe 리스트 조회
        public DataSet SelectPipeList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,LENGTH                                                 ");
            queryString.AppendLine("            ,DIAM                                                   ");
            queryString.AppendLine("            ,ROUGHNESS                                              ");
            queryString.AppendLine("            ,MLOSS                                                  ");
            queryString.AppendLine("            ,STATUS                                                 ");
            queryString.AppendLine("            ,REMARK                                                 ");
            queryString.AppendLine("            ,SFTRIDN                                                ");
            queryString.AppendLine("            ,LEAKAGE_COEFFICIENT                                    ");
            queryString.AppendLine("from        WH_PIPES                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PIPES");
        }

        //Pump 정보 입력
        public void InsertPumpData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_PUMPS    (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,NODE1                  ");
            queryString.AppendLine("                            ,NODE2                  ");
            queryString.AppendLine("                            ,PROPERTIES             ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];
            parameters[4].Value = (string)conditions[4];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Pump 리스트 조회
        public DataSet SelectPumpList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,PROPERTIES                                             ");
            queryString.AppendLine("from        WH_PUMPS                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PUMPS");
        }

        //Valve 정보 입력
        public void InsertValveData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_VALVES   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,NODE1                  ");
            queryString.AppendLine("                            ,NODE2                  ");
            queryString.AppendLine("                            ,DIAMETER               ");
            queryString.AppendLine("                            ,TYPE                   ");
            queryString.AppendLine("                            ,SETTING                ");
            queryString.AppendLine("                            ,MINORLOSS              ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                            ,:6                     ");
            queryString.AppendLine("                            ,:7                     ");
            queryString.AppendLine("                            ,:8                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
                ,new OracleParameter("6", OracleDbType.Varchar2)
                ,new OracleParameter("7", OracleDbType.Varchar2)
                ,new OracleParameter("8", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];
            parameters[4].Value = (string)conditions[4];
            parameters[5].Value = (string)conditions[5];
            parameters[6].Value = (string)conditions[6];
            parameters[7].Value = (string)conditions[7];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Valve 리스트 조회
        public DataSet SelectValveList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,NODE1                                                  ");
            queryString.AppendLine("            ,NODE2                                                  ");
            queryString.AppendLine("            ,DIAMETER                                               ");
            queryString.AppendLine("            ,TYPE                                                   ");
            queryString.AppendLine("            ,SETTING                                                ");
            queryString.AppendLine("            ,MINORLOSS                                              ");
            queryString.AppendLine("from        WH_VALVES                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_VALVES");
        }

        //Demand 정보 입력
        public void InsertDemandData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_DEMANDS  (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,DEMAND                 ");
            queryString.AppendLine("                            ,PATTERN_ID             ");
            queryString.AppendLine("                            ,CATEGORY               ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];
            parameters[4].Value = (string)conditions[4];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Demand 리스트 조회
        public DataSet SelectDemandList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,CATEGORY                                               ");
            queryString.AppendLine("            ,DEMAND                                                 ");
            queryString.AppendLine("            ,PATTERN_ID                                             ");
            queryString.AppendLine("from        WH_DEMANDS                                              ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_DEMANDS");
        }

        //Status 정보 입력
        public void InsertStatusData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_STATUS   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,STATUS_SETTING         ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Status 정보 조회
        public DataSet SelectStatusList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,IDX                                                    ");
            queryString.AppendLine("            ,STATUS_SETTING                                         ");
            queryString.AppendLine("from        WH_STATUS                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");
            queryString.AppendLine("            ,to_number(IDX)                                         ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_STATUS");
        }

        //Curve 정보 입력
        public void InsertCurveData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_CURVES   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,X                      ");
            queryString.AppendLine("                            ,Y                      ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];
            parameters[4].Value = (string)conditions[4];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Curve 리스트 조회
        public DataSet SelectCurveList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,IDX                                                    ");
            queryString.AppendLine("            ,X                                                      ");
            queryString.AppendLine("            ,Y                                                      ");
            queryString.AppendLine("from        WH_CURVES                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");
            queryString.AppendLine("            ,to_number(IDX)                                         ");

            //Console.WriteLine(queryString.ToString());
            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_CURVES");
        }

        //Pattern 정보 입력
        public void InsertPatternData(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_PATTERNS     (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,PATTERN_ID             ");
            queryString.AppendLine("                                ,IDX                    ");
            queryString.AppendLine("                                ,MULTIPLIER             ");
            queryString.AppendLine("                                ,REMARK                 ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                                ,:5                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions["INP_NUMBER"];
            parameters[1].Value = (string)conditions["PATTERN_ID"];
            parameters[2].Value = (string)conditions["IDX"];
            parameters[3].Value = (string)conditions["MULTIPLIER"];
            parameters[4].Value = "1";

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Pattern 리스트 조회
        public DataSet SelectPatternList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      PATTERN_ID                                              ");
            queryString.AppendLine("            ,IDX                                                    ");
            queryString.AppendLine("            ,MULTIPLIER                                             ");
            queryString.AppendLine("            ,REMARK                                                 ");
            queryString.AppendLine("from        WH_PATTERNS                                             ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    PATTERN_ID                                              ");
            queryString.AppendLine("            ,to_number(IDX)                                         ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_PATTERNS");
        }

        //Control 정보 입력
        public void InsertControlData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_CONTROLS (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,CONTROLS_STATEMENT     ");
            queryString.AppendLine("                            ,REMARK                 ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];            //주석 - work에서 argument를 재생성했기때문에 idx가 10이 아님

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Control 리스트 조회
        public DataSet SelectControlList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,CONTROLS_STATEMENT                                     ");
            queryString.AppendLine("            ,REMARK                                                 ");
            queryString.AppendLine("from        WH_CONTROLS                                             ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_CONTROLS");
        }

        //Rule 정보 입력
        public void InsertRuleData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RULES    (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,RULES_STATEMENT        ");
            queryString.AppendLine("                            ,REMARK                 ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];            //주석 - work에서 argument를 재생성했기때문에 idx가 10이 아님

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Rule 리스트 조회
        public DataSet SelectRuleList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,RULES_STATEMENT                                        ");
            queryString.AppendLine("            ,REMARK                                                 ");
            queryString.AppendLine("from        WH_RULES                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RULES");
        }

        //Energy 정보 입력
        public void InsertEnergyData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_ENERGY   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,ENERGY_STATEMENT       ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Energy 리스트 조회
        public DataSet SelectEnergyList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,ENERGY_STATEMENT                                       ");
            queryString.AppendLine("from        WH_ENERGY                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_ENERGY");
        }
        
        //Emitter 정보 입력
        public void InsertEmitterData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into EH_EMITTERS (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,FLOW_COFFICIENT        ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Emitter 리스트 조회
        public DataSet SelectEmitterList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,FLOW_COFFICIENT                                        ");
            queryString.AppendLine("from        WH_EMITTERS                                             ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_EMITTERS");
        }

        //Quality 정보 입력
        public void InsertQualityData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_QUALITY  (                            ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,INITQUAL               ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Quality 리스트 조회
        public DataSet SelectQualityList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,INITQUAL                                               ");
            queryString.AppendLine("from        WH_QUALITY                                              ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_QUALITY");
        }

        //Source 정보 입력
        public void InsertSourceData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_SOURCE   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,TYPE                   ");
            queryString.AppendLine("                            ,STRENGTH               ");
            queryString.AppendLine("                            ,PATTERN_ID             ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];
            parameters[4].Value = (string)conditions[4];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Source 리스트 조회
        public DataSet SelectSourceList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,TYPE                                                   ");
            queryString.AppendLine("            ,STRENGTH                                               ");
            queryString.AppendLine("            ,PATTERN_ID                                             ");
            queryString.AppendLine("from        WH_SOURCE                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_SOURCE");
        }

        //Reaction 정보 입력
        public void InsertReactionData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_REACTIONS    (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,IDX                    ");
            queryString.AppendLine("                                ,REACTIONS_STATEMENT    ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Reaction 리스트 조회
        public DataSet SelectReactionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX														");
            queryString.AppendLine("            ,REACTION_STATEMENT									    ");
            queryString.AppendLine("from        WH_REACTIONS											");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)										    ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_REACTIONS");
        }

        //Mixing 정보 입력
        public void InsertMixingData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_MIXING   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,MODEL                  ");
            queryString.AppendLine("                            ,FLACTION               ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Mixing 리스트 조회
        public DataSet SelectMixingList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,MODEL                                                  ");
            queryString.AppendLine("            ,FLACTION                                               ");
            queryString.AppendLine("from        WH_MIXING                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_MIXING");
        }

        //Time option 정보 입력
        public void InsertTimeOptionData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_TIMES    (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,TIMES_STATEMENT        ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Time option 리스트 조회
        public DataSet SelectTimeOptionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,TIMES_STATEMENT                                        ");
            queryString.AppendLine("from        WH_TIMES                                                ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TIMES");
        }

        //Report Option 정보 입력
        public void InsertReportOptionData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_RPT_OPTIONS  (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,IDX                    ");
            queryString.AppendLine("                                ,REPORT_STATEMENT       ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Report Option 리스트 조회
        public DataSet SelectReportOptionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,REPORT_STATEMENT                                       ");
            queryString.AppendLine("from        WH_RPT_OPTIONS                                          ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_RPT_OPTIONS");
        }

        //Option 정보 입력
        public void InsertNormalOptionData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_OPTIONS  (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,OPTIONS_STATEMENT      ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Option 리스트 조회
        public DataSet SelectOptionList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      IDX                                                     ");
            queryString.AppendLine("            ,OPTIONS_STATEMENT                                      ");
            queryString.AppendLine("from        WH_OPTIONS                                              ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    to_number(IDX)                                          ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_OPTIONS");
        }

        //Coordinate 정보 입력
        public void InsertCoordinateData(OracleDBManager dbManager, ArrayList conditions)
        {
            //StringBuilder queryString = new StringBuilder();
            queryString.Remove(0, queryString.Length);

            queryString.AppendLine("insert into WH_COORDINATES  (                           ");
            queryString.AppendLine("                                INP_NUMBER              ");
            queryString.AppendLine("                                ,ID                     ");
            queryString.AppendLine("                                ,X                      ");
            queryString.AppendLine("                                ,Y                      ");
            queryString.AppendLine("                            )                           ");
            queryString.AppendLine("values                      (                           ");
            queryString.AppendLine("                                :1                      ");
            queryString.AppendLine("                                ,:2                     ");
            queryString.AppendLine("                                ,:3                     ");
            queryString.AppendLine("                                ,:4                     ");
            queryString.AppendLine("                            )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Coordinate 리스트 조회
        public DataSet SelectCoordinateList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,X                                                      ");
            queryString.AppendLine("            ,Y                                                      ");
            queryString.AppendLine("from        WH_COORDINATES                                          ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_COORDINATES");
        }

        //Vertices 정보 입력
        public void InsertVerticesData(OracleDBManager dbManager, ArrayList conditions)
        {
            //StringBuilder queryString = new StringBuilder();
            queryString.Remove(0, queryString.Length);

            queryString.AppendLine("insert into WH_VERTICES (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,ID                     ");
            queryString.AppendLine("                            ,IDX                    ");
            queryString.AppendLine("                            ,X                      ");
            queryString.AppendLine("                            ,Y                      ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                            ,:5                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
                ,new OracleParameter("5", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];
            parameters[4].Value = (string)conditions[4];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Vertices 리스트 조회
        public DataSet SelectVerticesList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      ID                                                      ");
            queryString.AppendLine("            ,IDX                                                    ");
            queryString.AppendLine("            ,X                                                      ");
            queryString.AppendLine("            ,Y                                                      ");
            queryString.AppendLine("from        WH_VERTICES                                             ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    ID                                                      ");
            queryString.AppendLine("            ,to_number(IDX)                                         ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_VERTICES");
        }

        //Label 정보 입력
        public void InsertLabelData(OracleDBManager dbManager, ArrayList conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("insert into WH_LABELS   (                           ");
            queryString.AppendLine("                            INP_NUMBER              ");
            queryString.AppendLine("                            ,X                      ");
            queryString.AppendLine("                            ,Y                      ");
            queryString.AppendLine("                            ,REMARK                 ");
            queryString.AppendLine("                        )                           ");
            queryString.AppendLine("values                  (                           ");
            queryString.AppendLine("                            :1                      ");
            queryString.AppendLine("                            ,:2                     ");
            queryString.AppendLine("                            ,:3                     ");
            queryString.AppendLine("                            ,:4                     ");
            queryString.AppendLine("                        )                           ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                ,new OracleParameter("2", OracleDbType.Varchar2)
                ,new OracleParameter("3", OracleDbType.Varchar2)
                ,new OracleParameter("4", OracleDbType.Varchar2)
            };

            parameters[0].Value = (string)conditions[0];
            parameters[1].Value = (string)conditions[1];
            parameters[2].Value = (string)conditions[2];
            parameters[3].Value = (string)conditions[3];

            dbManager.ExecuteScript(queryString.ToString(), parameters);
        }

        //Label 리스트 조회
        public DataSet SelectLabelList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      X                                                       ");
            queryString.AppendLine("            ,Y                                                      ");
            queryString.AppendLine("            ,REMARK                                                 ");
            queryString.AppendLine("from        WH_LABELS                                               ");
            queryString.AppendLine("where       INP_NUMBER        = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by    REMARK                                                  ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_LABELS");
        }

        //Tag 리스트 조회
        public DataSet SelectTagList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      TYPE	                                        ");
            queryString.AppendLine("            , ID	                                        ");
            queryString.AppendLine("            , POSITION_INFO 	                            ");
            queryString.AppendLine("from        WH_TAGS	                                        ");
            queryString.AppendLine("where      INP_NUMBER = '" + conditions["INP_NUMBER"] + "'	");
            queryString.AppendLine("order by   TYPE, ID                                         ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TAGS");
        }
    }
}