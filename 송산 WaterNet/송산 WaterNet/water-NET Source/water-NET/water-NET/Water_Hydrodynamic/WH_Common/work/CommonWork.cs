﻿/**************************************************************************
 * 파일명   : CommonWork.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.25
 * 설명     : 공통작업에 대한 Business Logic
 * 변경이력 : 2010.10.25 - 최초생성
 **************************************************************************/
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WaterNet.WH_Common.dao;
using WaterNet.WaterNetCore;

namespace WaterNet.WH_Common.work
{
    public class CommonWork
    {
        private static CommonWork work = null;
        private CommonDao dao = null;

        private CommonWork()
        {
            dao = CommonDao.GetInstance();
        }

        public static CommonWork GetInstance()
        {
            if(work == null)
            {
                work = new CommonWork();
            }

            return work;
        }

        //대블록 정보 리스트들을 조회한다. (폼 최초로딩 시)
        public DataSet GetLblockDataList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                //대블록 정보 조회
                result = dao.GetLblockDataList(dbManager, conditions);

                //최상위에 Blank 추가
                if (result != null)
                {
                    DataRow row = result.Tables["CM_LOCATION"].NewRow();
                    row["LOC_CODE"] = "";
                    row["LOC_NAME"] = "선택";

                    result.Tables["CM_LOCATION"].Rows.InsertAt(row, 0);
                }
            }
            catch(Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //상위블록코드로 하위블록리스트를 조회
        public DataSet GetBlockDataList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = dao.GetBlockDataList(dbManager, conditions);

                //최상위에 Blank 추가
                if(result != null)
                {
                    DataRow row = result.Tables["CM_LOCATION"].NewRow();
                    row["LOC_CODE"] = "";
                    row["LOC_NAME"] = "선택";

                    result.Tables["CM_LOCATION"].Rows.InsertAt(row, 0);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //Shape 소블록 리스트 조회
        public DataSet GetShapeSblockDataList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = dao.GetShapeSblockDataList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //공통코드 조회
        public DataSet GetCodeList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                result = dao.GetCodeList(dbManager, conditions);

                string removeAllSelect = "N";

                if(conditions["removeAllSelect"]!=null)
                {
                    removeAllSelect = (string)conditions["removeAllSelect"];
                }

                if(result != null&&"N".Equals(removeAllSelect))
                {
                    DataRow row = result.Tables["CM_CODE"].NewRow();

                    row["CODE"] = "";
                    row["CODE_NAME"] = "전체";

                    result.Tables["CM_CODE"].Rows.InsertAt(row, 0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //계통(중블록)코드를 조회한다
        public DataSet GetMblockDataList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                //계통(중블록) 데이터 조회
                result = dao.GetMblockList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //해당 모델의 소블록코드 조회
        public DataSet SelectSblockInModel(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataSet result = null;

            try
            {
                dbManager = new OracleDBManager();                                                   //DB작업 관리자 생성
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();     //DB연결정보 할당

                //DB작업
                dbManager.Open();

                //계통(중블록) 데이터 조회
                result = dao.SelectSblockInModel(dbManager, conditions);

                DataRow row = result.Tables["WH_TITLE"].NewRow();

                row["LOC_CODE"] = "";
                row["FTR_IDN"] = "";
                row["LOC_NAME"] = "전체";

                result.Tables["WH_TITLE"].Rows.InsertAt(row, 0);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }
    }
}
