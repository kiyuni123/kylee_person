﻿/**************************************************************************
 * 파일명   : CommonDao.cs
 * 작성자   : kang hyun bok
 * 작성일자 : 2010.10.25
 * 설명     : 공통정보를 조회하기 위한 Data Access Object
 * 변경이력 : 2010.09.29 - 최초생성
 **************************************************************************/

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using WaterNet.WaterNetCore;

namespace WaterNet.WH_Common.dao
{
    public class CommonDao
    {
        private static CommonDao dao = null;

        private CommonDao()
        {
        }

        public static CommonDao GetInstance()
        {
            if(dao == null)
            {
                dao = new CommonDao();
            }

            return dao;
        }

        //대블록정보 조회
        public DataSet GetLblockDataList(OracleDBManager dbManager, Hashtable conditons)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          LOC_CODE			    ");
            queryString.AppendLine("                ,PLOC_CODE			    ");
            queryString.AppendLine("                ,LOC_GBN			    ");
            queryString.AppendLine("                ,LOC_NAME			    ");
            queryString.AppendLine("from            CM_LOCATION			    ");
            queryString.AppendLine("where           1 = 1			        ");
            //queryString.AppendLine("and             LOC_CODE = '000012'		");
            queryString.AppendLine("and             FTR_CODE = 'BZ001'		");
            queryString.AppendLine("order by        LOC_NAME                ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_LOCATION");
        }

        //중,소블록정보 조회
        public DataSet GetBlockDataList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          LOC_CODE			                            ");
            queryString.AppendLine("                ,PLOC_CODE			                            ");
            queryString.AppendLine("                ,LOC_GBN			                            ");
            queryString.AppendLine("                ,LOC_NAME			                            ");
            queryString.AppendLine("from            CM_LOCATION			                            ");
            queryString.AppendLine("where           1 = 1                                           ");
            queryString.AppendLine("and             PLOC_CODE   = '" + conditions["PLOC_CODE"] + "' ");
            queryString.AppendLine("order by        LOC_NAME                                        ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_LOCATION");
        }

        //계통(중블록)코드 조회
        public DataSet GetMblockList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          LOC_CODE            ");
            queryString.AppendLine("                ,LOC_NAME           ");
            queryString.AppendLine("from            CM_LOCATION         ");
            queryString.AppendLine("where           FTR_CODE = 'BZ002'  ");
            queryString.AppendLine("order by        LOC_NAME            ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_LOCATION");
        }

        //소블록 Shape Code List 조회
        public DataSet GetShapeSblockDataList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          LOC_CODE                ");
            queryString.AppendLine("                ,PLOC_CODE              ");
            queryString.AppendLine("                ,LOC_NAME               ");
            queryString.AppendLine("                ,FTR_IDN                ");
            queryString.AppendLine("from            CM_LOCATION             ");
            queryString.AppendLine("where           1 = 1                   ");
            queryString.AppendLine("and             FTR_CODE    = 'BZ003'   ");
            queryString.AppendLine("order by        LOC_NAME                ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_LOCATION");
        }

        //해당 모델의 소블록코드 조회
        public DataSet SelectSblockInModel(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      LOC_CODE                                                ");
            queryString.AppendLine("            ,LOC_NAME                                               ");
            queryString.AppendLine("            ,FTR_IDN                                                ");
            queryString.AppendLine("from        WH_TITLE        a                                       ");
            queryString.AppendLine("            ,CM_LOCATION    b                                       ");
            queryString.AppendLine("where       a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'    ");
            queryString.AppendLine("and         a.MFTRIDN       = b.PLOC_CODE                           ");
            queryString.AppendLine("order by    b.LOC_NAME                                              ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TITLE");
        }

        //공통코드 조회
        public DataSet GetCodeList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select          CODE                                        ");
            queryString.AppendLine("                ,CODE_NAME                                  ");
            queryString.AppendLine("from            CM_CODE                                     ");
            queryString.AppendLine("where           PCODE   = '" + conditions["PCODE"] + "'     ");
            queryString.AppendLine("order by        CODE                                        ");

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "CM_CODE");
        }

        //JUNCTION/NODE ID로 지역정보 조회
        public DataSet GetLocationInfo(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("select      b.LOC_CODE 			                                                                                                                                        ");
            queryString.AppendLine("from        WH_TAGS         a			                                                                                                                                ");
            queryString.AppendLine("            ,CM_LOCATION    b			                                                                                                                                ");
            queryString.AppendLine("where       a.INP_NUMBER    = '" + conditions["INP_NUMBER"] + "'			                                                                                            ");
            queryString.AppendLine("and         a.ID            = '" + conditions["ID"] + "'			                                                                                                    ");
            
            if("NODE".Equals(conditions["TYPE"].ToString()))
            {
                queryString.AppendLine("and     a.TYPE          = 'NODE'			");
                queryString.AppendLine("and     substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 1)+1,instr(a.POSITION_INFO, '|', 1, 2)-1 - instr(a.POSITION_INFO, '|', 1, 1)) = b.FTR_CODE			");
                queryString.AppendLine("and     substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 2)+1,length(a.POSITION_INFO) - instr(a.POSITION_INFO, '|', 1, 2))             = b.FTR_IDN             ");
            }
            else if ("LINK".Equals(conditions["TYPE"].ToString()))
            {
                queryString.AppendLine("and     a.TYPE          = 'LINK'			");
                queryString.AppendLine("and     substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 5)+1,instr(a.POSITION_INFO, '|', 1, 6)-1 - instr(a.POSITION_INFO, '|', 1, 5)) = b.FTR_CODE			");
                queryString.AppendLine("and     substr(a.POSITION_INFO,instr(a.POSITION_INFO, '|', 1, 6)+1,length(a.POSITION_INFO) - instr(a.POSITION_INFO, '|', 1, 6))             = b.FTR_IDN             ");
            }

            return dbManager.ExecuteScriptDataSet(queryString.ToString(), null, "WH_TAGS");
        }
    }
}
