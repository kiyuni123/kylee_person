﻿using System;
using WaterNet.WV_MeterChangeInformation.dao;
using WaterNet.WV_Common.work;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_MeterChangeInformation.work
{
    public class MeterChangeInformationWork : BaseWork
    {
        private static MeterChangeInformationWork work = null;
        private MeterChangeInformationDao dao = null;

        public static MeterChangeInformationWork GetInstance()
        {
            if (work == null)
            {
                work = new MeterChangeInformationWork();
            }
            return work;
        }

        private MeterChangeInformationWork()
        {
            dao = MeterChangeInformationDao.GetInstance();
        }

        public DataSet SelectMeterChangeInformation(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                this.dao.SelectMeterChangeInformation(DataBaseManager, result, "RESULT", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public void InsertMeterChangeInformationDetail(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                string mtrchgserno = this.dao.GetMeterChangeNo(DataBaseManager, parameter).ToString();

                if (mtrchgserno != null)
                {
                    if (parameter.ContainsKey("MTRCHGSERNO"))
                    {
                        parameter.Remove("MTRCHGSERNO");
                    }
                    if (parameter.ContainsKey("MDFY_YN"))
                    {
                        parameter.Remove("MDFY_YN");
                    }

                    parameter.Add("MDFY_YN", "Y");
                    parameter.Add("MTRCHGSERNO", mtrchgserno);
                    this.dao.InsertMeterChangeInformationDetail(DataBaseManager, parameter);
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void UpdateMeterChangeInformationDetail(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                if (parameter.ContainsKey("MDFY_YN"))
                {
                    parameter.Remove("MDFY_YN");
                }

                parameter.Add("MDFY_YN", "Y");
                this.dao.UpdateMeterChangeInformationDetail(DataBaseManager, parameter);
                MessageBox.Show("정상적으로 처리되었습니다.");
                
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void DeleteMeterChangeInformationDetail(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                if (parameter.ContainsKey("MDFY_YN"))
                {
                    parameter.Remove("MDFY_YN");
                }

                parameter.Add("MDFY_YN", "Y");
                this.dao.DeleteMeterChangeInformationDetail(DataBaseManager, parameter);
                MessageBox.Show("정상적으로 처리되었습니다.");

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public DataSet SelectDM(Hashtable parameter)
        {
            DataSet result = new DataSet();

            try
            {
                ConnectionOpen();
                BeginTransaction();

                this.dao.SelectDM(DataBaseManager, result, "RESULT", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }

            return result;
        }

        public DataSet UpdateExcelImport(DataTable data)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (DataRow row in data.Rows)
                {
                    this.dao.UpdateExcelImport(DataBaseManager, Utils.ConverToHashtable(row));
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
