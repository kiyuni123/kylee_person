﻿namespace WaterNet.WV_MeterChangeInformation.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SGCCD");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MDDT");
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MTRCHGSERNO");
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn28 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MDFY_YN");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNO");
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DMNM");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NRDADDR");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn29 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TRANSCLASS");
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MRYM_Y");
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MRYM_M");
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OMFRNO");
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OWSPIPESZCD");
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSRMVNDL");
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NMFRNO");
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NWSPIPESZCD");
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WSATTNDL");
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn30 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TRANSRS");
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.importBtn = new System.Windows.Forms.Button();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.insertBtn = new System.Windows.Forms.Button();
            this.updateBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.transclass = new System.Windows.Forms.TextBox();
            this.mtrchgserno = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.nwspipeszcd = new System.Windows.Forms.ComboBox();
            this.owspipeszcd = new System.Windows.Forms.ComboBox();
            this.mrym_m = new System.Windows.Forms.ComboBox();
            this.mrym_y = new System.Windows.Forms.ComboBox();
            this.MonthToMonthIntervalText = new System.Windows.Forms.Label();
            this.wsattndl = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.label13 = new System.Windows.Forms.Label();
            this.nmfrno = new System.Windows.Forms.TextBox();
            this.wsrmvndl = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.omfrno = new System.Windows.Forms.TextBox();
            this.transrs = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.mddt = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dmSearchBtn = new System.Windows.Forms.Button();
            this.nrdaddr = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dmnm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dmno = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.sblock = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mblock = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblock = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mddt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Location = new System.Drawing.Point(1062, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 553);
            this.pictureBox4.TabIndex = 26;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 553);
            this.pictureBox3.TabIndex = 25;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 563);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1072, 10);
            this.pictureBox2.TabIndex = 24;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1072, 10);
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.importBtn);
            this.panel1.Controls.Add(this.deleteBtn);
            this.panel1.Controls.Add(this.insertBtn);
            this.panel1.Controls.Add(this.updateBtn);
            this.panel1.Controls.Add(this.searchBtn);
            this.panel1.Controls.Add(this.excelBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 124);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel1.Size = new System.Drawing.Size(1052, 30);
            this.panel1.TabIndex = 47;
            // 
            // importBtn
            // 
            this.importBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.importBtn.Location = new System.Drawing.Point(759, 2);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(63, 25);
            this.importBtn.TabIndex = 53;
            this.importBtn.TabStop = false;
            this.importBtn.Text = "Import";
            this.importBtn.UseVisualStyleBackColor = true;
            // 
            // deleteBtn
            // 
            this.deleteBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.deleteBtn.AutoSize = true;
            this.deleteBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.deleteBtn.Location = new System.Drawing.Point(966, 2);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(40, 25);
            this.deleteBtn.TabIndex = 51;
            this.deleteBtn.TabStop = false;
            this.deleteBtn.Text = "삭제";
            this.deleteBtn.UseVisualStyleBackColor = true;
            // 
            // insertBtn
            // 
            this.insertBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.insertBtn.AutoSize = true;
            this.insertBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.insertBtn.Location = new System.Drawing.Point(874, 2);
            this.insertBtn.Name = "insertBtn";
            this.insertBtn.Size = new System.Drawing.Size(40, 25);
            this.insertBtn.TabIndex = 50;
            this.insertBtn.TabStop = false;
            this.insertBtn.Text = "추가";
            this.insertBtn.UseVisualStyleBackColor = true;
            // 
            // updateBtn
            // 
            this.updateBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.updateBtn.AutoSize = true;
            this.updateBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.updateBtn.Location = new System.Drawing.Point(920, 2);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(40, 25);
            this.updateBtn.TabIndex = 48;
            this.updateBtn.TabStop = false;
            this.updateBtn.Text = "저장";
            this.updateBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(1012, 2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 30;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(828, 2);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 31;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // ultraGrid1
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance37;
            ultraGridColumn1.Header.Caption = "지역관리코드";
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.Hidden = true;
            ultraGridColumn4.Header.Caption = "센터";
            ultraGridColumn4.Header.VisiblePosition = 1;
            ultraGridColumn4.Hidden = true;
            ultraGridColumn8.Header.Caption = "대블록";
            ultraGridColumn8.Header.VisiblePosition = 2;
            ultraGridColumn8.Width = 60;
            ultraGridColumn9.Header.Caption = "중블록";
            ultraGridColumn9.Header.VisiblePosition = 3;
            ultraGridColumn9.Width = 60;
            ultraGridColumn10.Header.Caption = "소블록";
            ultraGridColumn10.Header.VisiblePosition = 4;
            ultraGridColumn10.Width = 60;
            ultraGridColumn11.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance1.TextHAlignAsString = "Center";
            ultraGridColumn11.CellAppearance = appearance1;
            ultraGridColumn11.Header.Caption = "교체일자";
            ultraGridColumn11.Header.VisiblePosition = 5;
            ultraGridColumn11.Width = 105;
            appearance2.TextHAlignAsString = "Center";
            ultraGridColumn14.CellAppearance = appearance2;
            ultraGridColumn14.Header.Caption = "교체순번";
            ultraGridColumn14.Header.VisiblePosition = 6;
            ultraGridColumn14.Width = 77;
            ultraGridColumn28.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance3.TextHAlignAsString = "Center";
            ultraGridColumn28.CellAppearance = appearance3;
            ultraGridColumn28.Header.Caption = "수정가능여부";
            ultraGridColumn28.Header.VisiblePosition = 7;
            ultraGridColumn28.Width = 100;
            ultraGridColumn12.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance4.TextHAlignAsString = "Center";
            ultraGridColumn12.CellAppearance = appearance4;
            ultraGridColumn12.Header.Caption = "수용가번호";
            ultraGridColumn12.Header.VisiblePosition = 9;
            ultraGridColumn13.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn13.Header.Caption = "수용가명";
            ultraGridColumn13.Header.VisiblePosition = 10;
            ultraGridColumn2.Header.Caption = "주소";
            ultraGridColumn2.Header.VisiblePosition = 8;
            ultraGridColumn2.Hidden = true;
            ultraGridColumn29.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance5.TextHAlignAsString = "Center";
            ultraGridColumn29.CellAppearance = appearance5;
            ultraGridColumn29.Header.Caption = "교체구분";
            ultraGridColumn29.Header.VisiblePosition = 11;
            appearance6.TextHAlignAsString = "Center";
            ultraGridColumn3.CellAppearance = appearance6;
            ultraGridColumn3.Header.Caption = "적용검침년도";
            ultraGridColumn3.Header.VisiblePosition = 12;
            ultraGridColumn3.Hidden = true;
            appearance7.TextHAlignAsString = "Center";
            ultraGridColumn15.CellAppearance = appearance7;
            ultraGridColumn15.Header.Caption = "적용검침월";
            ultraGridColumn15.Header.VisiblePosition = 13;
            ultraGridColumn15.Hidden = true;
            appearance8.TextHAlignAsString = "Center";
            ultraGridColumn16.CellAppearance = appearance8;
            ultraGridColumn16.Header.Caption = "교체전계량기제작번호";
            ultraGridColumn16.Header.VisiblePosition = 14;
            ultraGridColumn16.Hidden = true;
            appearance9.TextHAlignAsString = "Center";
            ultraGridColumn17.CellAppearance = appearance9;
            ultraGridColumn17.Header.Caption = "교체전상수도구경";
            ultraGridColumn17.Header.VisiblePosition = 15;
            ultraGridColumn17.Hidden = true;
            appearance10.TextHAlignAsString = "Center";
            ultraGridColumn18.CellAppearance = appearance10;
            ultraGridColumn18.Header.Caption = "계량기철거지침";
            ultraGridColumn18.Header.VisiblePosition = 16;
            ultraGridColumn18.Hidden = true;
            appearance11.TextHAlignAsString = "Center";
            ultraGridColumn19.CellAppearance = appearance11;
            ultraGridColumn19.Header.Caption = "교체후계량기제작번호";
            ultraGridColumn19.Header.VisiblePosition = 17;
            ultraGridColumn19.Hidden = true;
            appearance12.TextHAlignAsString = "Center";
            ultraGridColumn20.CellAppearance = appearance12;
            ultraGridColumn20.Header.Caption = "교체후상수도구경";
            ultraGridColumn20.Header.VisiblePosition = 18;
            ultraGridColumn20.Hidden = true;
            appearance13.TextHAlignAsString = "Center";
            ultraGridColumn21.CellAppearance = appearance13;
            ultraGridColumn21.Header.Caption = "계량기설치지침";
            ultraGridColumn21.Header.VisiblePosition = 19;
            ultraGridColumn21.Hidden = true;
            ultraGridColumn30.Header.Caption = "교체사유";
            ultraGridColumn30.Header.VisiblePosition = 20;
            ultraGridColumn30.Width = 201;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn4,
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn14,
            ultraGridColumn28,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn2,
            ultraGridColumn29,
            ultraGridColumn3,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21,
            ultraGridColumn30});
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.GroupByBox.Hidden = true;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance44;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance47;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(3, 3);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(1046, 157);
            this.ultraGrid1.TabIndex = 48;
            this.ultraGrid1.TabStop = false;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(10, 154);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1052, 409);
            this.panel2.TabIndex = 50;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ultraGrid1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1052, 409);
            this.tableLayoutPanel1.TabIndex = 50;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 166);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1046, 240);
            this.panel3.TabIndex = 49;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.transclass);
            this.groupBox2.Controls.Add(this.mtrchgserno);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.nwspipeszcd);
            this.groupBox2.Controls.Add(this.owspipeszcd);
            this.groupBox2.Controls.Add(this.mrym_m);
            this.groupBox2.Controls.Add(this.mrym_y);
            this.groupBox2.Controls.Add(this.MonthToMonthIntervalText);
            this.groupBox2.Controls.Add(this.wsattndl);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.ultraLabel2);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.nmfrno);
            this.groupBox2.Controls.Add(this.wsrmvndl);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.ultraLabel1);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.omfrno);
            this.groupBox2.Controls.Add(this.transrs);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.mddt);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1046, 128);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "계량기 교체 정보";
            // 
            // transclass
            // 
            this.transclass.Location = new System.Drawing.Point(87, 28);
            this.transclass.Name = "transclass";
            this.transclass.Size = new System.Drawing.Size(121, 21);
            this.transclass.TabIndex = 37;
            // 
            // mtrchgserno
            // 
            this.mtrchgserno.Location = new System.Drawing.Point(756, 69);
            this.mtrchgserno.Name = "mtrchgserno";
            this.mtrchgserno.Size = new System.Drawing.Size(121, 21);
            this.mtrchgserno.TabIndex = 36;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(694, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 35;
            this.label14.Text = "교체순번";
            // 
            // nwspipeszcd
            // 
            this.nwspipeszcd.FormattingEnabled = true;
            this.nwspipeszcd.Location = new System.Drawing.Point(297, 111);
            this.nwspipeszcd.Name = "nwspipeszcd";
            this.nwspipeszcd.Size = new System.Drawing.Size(121, 20);
            this.nwspipeszcd.TabIndex = 34;
            // 
            // owspipeszcd
            // 
            this.owspipeszcd.FormattingEnabled = true;
            this.owspipeszcd.Location = new System.Drawing.Point(297, 69);
            this.owspipeszcd.Name = "owspipeszcd";
            this.owspipeszcd.Size = new System.Drawing.Size(121, 20);
            this.owspipeszcd.TabIndex = 33;
            // 
            // mrym_m
            // 
            this.mrym_m.FormattingEnabled = true;
            this.mrym_m.Location = new System.Drawing.Point(829, 111);
            this.mrym_m.Name = "mrym_m";
            this.mrym_m.Size = new System.Drawing.Size(50, 20);
            this.mrym_m.TabIndex = 32;
            // 
            // mrym_y
            // 
            this.mrym_y.FormattingEnabled = true;
            this.mrym_y.Location = new System.Drawing.Point(756, 111);
            this.mrym_y.Name = "mrym_y";
            this.mrym_y.Size = new System.Drawing.Size(70, 20);
            this.mrym_y.TabIndex = 31;
            // 
            // MonthToMonthIntervalText
            // 
            this.MonthToMonthIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MonthToMonthIntervalText.AutoSize = true;
            this.MonthToMonthIntervalText.Location = new System.Drawing.Point(670, 114);
            this.MonthToMonthIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.MonthToMonthIntervalText.Name = "MonthToMonthIntervalText";
            this.MonthToMonthIntervalText.Size = new System.Drawing.Size(77, 12);
            this.MonthToMonthIntervalText.TabIndex = 30;
            this.MonthToMonthIntervalText.Text = "검침적용년월";
            // 
            // wsattndl
            // 
            this.wsattndl.Location = new System.Drawing.Point(516, 111);
            this.wsattndl.Name = "wsattndl";
            this.wsattndl.Size = new System.Drawing.Size(121, 21);
            this.wsattndl.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(457, 114);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 28;
            this.label12.Text = "설치지침";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(27, 105);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(60, 27);
            this.ultraLabel2.TabIndex = 27;
            this.ultraLabel2.Text = "교체후   제작번호";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(226, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 26;
            this.label13.Text = "교체후구경";
            // 
            // nmfrno
            // 
            this.nmfrno.Location = new System.Drawing.Point(87, 111);
            this.nmfrno.Name = "nmfrno";
            this.nmfrno.Size = new System.Drawing.Size(121, 21);
            this.nmfrno.TabIndex = 24;
            // 
            // wsrmvndl
            // 
            this.wsrmvndl.Location = new System.Drawing.Point(516, 69);
            this.wsrmvndl.Name = "wsrmvndl";
            this.wsrmvndl.Size = new System.Drawing.Size(121, 21);
            this.wsrmvndl.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(457, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 22;
            this.label10.Text = "철거지침";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(27, 63);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(60, 27);
            this.ultraLabel1.TabIndex = 21;
            this.ultraLabel1.Text = "교체전   제작번호";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(226, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 20;
            this.label11.Text = "교체전구경";
            // 
            // omfrno
            // 
            this.omfrno.Location = new System.Drawing.Point(87, 69);
            this.omfrno.Name = "omfrno";
            this.omfrno.Size = new System.Drawing.Size(121, 21);
            this.omfrno.TabIndex = 14;
            // 
            // transrs
            // 
            this.transrs.Location = new System.Drawing.Point(516, 27);
            this.transrs.Name = "transrs";
            this.transrs.Size = new System.Drawing.Size(506, 21);
            this.transrs.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(457, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 17;
            this.label9.Text = "교체사유";
            // 
            // mddt
            // 
            this.mddt.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.mddt.Location = new System.Drawing.Point(297, 27);
            this.mddt.Name = "mddt";
            this.mddt.Size = new System.Drawing.Size(121, 21);
            this.mddt.TabIndex = 16;
            this.mddt.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(238, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "교체일자";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 14;
            this.label7.Text = "교체구분";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(0, 102);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(1046, 10);
            this.pictureBox5.TabIndex = 24;
            this.pictureBox5.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dmSearchBtn);
            this.groupBox1.Controls.Add(this.nrdaddr);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dmnm);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dmno);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.sblock);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.mblock);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblock);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1046, 102);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "수용가 정보";
            // 
            // dmSearchBtn
            // 
            this.dmSearchBtn.Location = new System.Drawing.Point(672, 21);
            this.dmSearchBtn.Name = "dmSearchBtn";
            this.dmSearchBtn.Size = new System.Drawing.Size(75, 23);
            this.dmSearchBtn.TabIndex = 3;
            this.dmSearchBtn.Text = "수용가검색";
            this.dmSearchBtn.UseVisualStyleBackColor = true;
            // 
            // nrdaddr
            // 
            this.nrdaddr.Location = new System.Drawing.Point(516, 58);
            this.nrdaddr.Name = "nrdaddr";
            this.nrdaddr.Size = new System.Drawing.Size(506, 21);
            this.nrdaddr.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(445, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "수용가주소";
            // 
            // dmnm
            // 
            this.dmnm.Location = new System.Drawing.Point(297, 58);
            this.dmnm.Name = "dmnm";
            this.dmnm.Size = new System.Drawing.Size(121, 21);
            this.dmnm.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(238, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "수용가명";
            // 
            // dmno
            // 
            this.dmno.Location = new System.Drawing.Point(85, 58);
            this.dmno.Name = "dmno";
            this.dmno.Size = new System.Drawing.Size(121, 21);
            this.dmno.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "수용가번호";
            // 
            // sblock
            // 
            this.sblock.FormattingEnabled = true;
            this.sblock.Location = new System.Drawing.Point(516, 23);
            this.sblock.Name = "sblock";
            this.sblock.Size = new System.Drawing.Size(121, 20);
            this.sblock.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(469, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "소블록";
            // 
            // mblock
            // 
            this.mblock.FormattingEnabled = true;
            this.mblock.Location = new System.Drawing.Point(297, 22);
            this.mblock.Name = "mblock";
            this.mblock.Size = new System.Drawing.Size(121, 20);
            this.mblock.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "중블록";
            // 
            // lblock
            // 
            this.lblock.FormattingEnabled = true;
            this.lblock.Location = new System.Drawing.Point(85, 23);
            this.lblock.Name = "lblock";
            this.lblock.Size = new System.Drawing.Size(121, 20);
            this.lblock.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "대블록";
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1052, 114);
            this.searchBox1.TabIndex = 27;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 573);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.MinimumSize = new System.Drawing.Size(600, 600);
            this.Name = "frmMain";
            this.Text = "계량기 교체 관리";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mddt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private WaterNet.WV_Common.form.SearchBox searchBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button insertBtn;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button dmSearchBtn;
        private System.Windows.Forms.ComboBox lblock;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox sblock;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox mblock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.TextBox nrdaddr;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox dmnm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox dmno;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox omfrno;
        private System.Windows.Forms.TextBox transrs;
        private System.Windows.Forms.Label label9;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor mddt;
        private System.Windows.Forms.TextBox wsattndl;
        private System.Windows.Forms.Label label12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox nmfrno;
        private System.Windows.Forms.TextBox wsrmvndl;
        private System.Windows.Forms.Label label10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox mrym_m;
        private System.Windows.Forms.ComboBox mrym_y;
        private System.Windows.Forms.Label MonthToMonthIntervalText;
        private System.Windows.Forms.ComboBox nwspipeszcd;
        private System.Windows.Forms.ComboBox owspipeszcd;
        private System.Windows.Forms.Button deleteBtn;
        private System.Windows.Forms.TextBox mtrchgserno;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox transclass;
        private System.Windows.Forms.Button importBtn;
    }
}