﻿using System;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.form;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WV_MeterChangeInformation.work;
using ESRI.ArcGIS.Carto;
using WaterNet.WaterAOCore;
using ESRI.ArcGIS.Geodatabase;
using WaterNet.WV_Common.control;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.util;
using EMFrame.log;
using WaterNet.WV_Common.excel;

namespace WaterNet.WV_MeterChangeInformation.form
{
    public partial class frmMain : Form, ExcelImport
    {
        private IMapControlWV mainMap = null;
        private UltraGridManager gridManager = null;
        private BlockComboboxControl detailblock = null;
        private ExcelManager excelManager = new ExcelManager();

        //수도계량기 레이어
        private ILayer pLayer = null;

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 데이터 확인용폼으로 사용될 경우
        /// </summary>
        public bool UsePlan
        {
            set
            {
                if (value == true)
                {
                    this.searchBox1.Visible = false;
                    this.panel1.Visible = false;
                    this.searchBox1.InitializeParameter();
                    this.SelectMeterChangeInformation(this.searchBox1.Parameters);
                }
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        private bool DetailEnabled
        {
            get
            {
                bool enabled = false;

                UltraGridRow row = null;

                if (this.ultraGrid1.Selected.Rows.Count > 0)
                {
                    row = this.ultraGrid1.Selected.Rows[0];
                }

                if (row == null)
                {
                    enabled = true;
                }

                if (row != null)
                {
                    if (row.Cells["MDFY_YN"].Value.ToString() == "Y")
                    {
                        enabled = true;
                    }
                    else if (row.Cells["MDFY_YN"].Value.ToString() == "N")
                    {
                        enabled = false;
                    }
                }
                return enabled;
            }
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["계량기교체관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.insertBtn.Enabled = false;
                this.updateBtn.Enabled = false;
                this.deleteBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 폼 초기화 설정
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.Text = "검색조건";
            this.searchBox1.IntervalType = INTERVAL_TYPE.DATE;
            this.searchBox1.IntervalText = "검색기간";
            this.searchBox1.StartDateObject.Value = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
            this.searchBox1.EndDateObject.Value = DateTime.Now.ToString("yyyy-MM-dd");

            this.detailblock = new BlockComboboxControl(this.lblock, this.mblock, this.sblock);
            this.detailblock.AddDefaultOption(LOCATION_TYPE.All, false);

            this.lblock.Enabled = false;
            this.mblock.Enabled = false;
            this.sblock.Enabled = false;
            this.dmno.Enabled = false;
            this.dmnm.Enabled = false;
            this.nrdaddr.Enabled = false;
            this.mtrchgserno.Enabled = false;

            this.detailblock.StopEvent = true;
        }

        /// <summary>
        /// 상세폼의 객체 활성/ 비활성 제어
        /// </summary>
        /// <param name="isEnabled"></param>
        private void SetEnabledDetailForm(bool isEnabled)
        {
            this.deleteBtn.Enabled = isEnabled;
            this.updateBtn.Enabled = isEnabled;
        }

        /// <summary>
        /// 그리드 초기화 설정
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //대블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            //수정가능여부코드,
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MDFY_YN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MDFY_YN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2011");
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MDFY_YN"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MDFY_YN"];

            Utils.SetValueList(this.mrym_y, VALUELIST_TYPE.SINGLE_YEAR, null);
            Utils.SetValueList(this.mrym_m, VALUELIST_TYPE.SINGLE_MONTH, null);
            Utils.SetValueList(this.owspipeszcd, VALUELIST_TYPE.CODE, "2012");
            Utils.SetValueList(this.nwspipeszcd, VALUELIST_TYPE.CODE, "2012");
        }

        /// <summary>
        /// 이벤트 초기화 설정
        /// </summary>
        private void InitializeEvent()
        {
            this.importBtn.Click += new EventHandler(importBtn_Click);

            this.excelBtn.Click += new EventHandler(ExportExcel_EventHandler);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.insertBtn.Click += new EventHandler(insertBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.deleteBtn.Click += new EventHandler(deleteBtn_Click);
            this.dmSearchBtn.Click += new EventHandler(dmSearchBtn_Click);

            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);
            this.Disposed += new EventHandler(frmMain_Disposed);
        }
        
        private void importBtn_Click(object sender, EventArgs e)
        {
            frmImport import = new frmImport();
            import.Items.Add(new ImportItem("DMNO", "수용가번호", typeof(string), true));
            import.Items.Add(new ImportItem("MTRCHGSERNO", "교체순번", typeof(double), true));
            import.Items.Add(new ImportItem("MDFY_YN", "수정가능여부", typeof(string), true));
            import.Items.Add(new ImportItem("TRANSCLASS", "교체구분", typeof(string), false));
            import.Items.Add(new ImportItem("MRYM", "적용검침년월", typeof(string), false));
            import.Items.Add(new ImportItem("MDDT", "교체일자", typeof(string), true));
            import.Items.Add(new ImportItem("OMFRNO", "교체전계량기제작번호", typeof(string), false));
            import.Items.Add(new ImportItem("OWSPIPESZCD", "교체전상수도구경", typeof(string), false));
            import.Items.Add(new ImportItem("WSRMVNDL", "계량기철거지침", typeof(double), false));
            import.Items.Add(new ImportItem("NMFRNO", "교체후계량기제작번호", typeof(string), false));
            import.Items.Add(new ImportItem("NWSPIPESZCD", "교체후상수도구경", typeof(string), false));
            import.Items.Add(new ImportItem("WSATTNDL", "계량기설치지침", typeof(double), false));
            import.Items.Add(new ImportItem("TRANSRS", "변경사유", typeof(string), false));
            import.Items.Add(new ImportItem("RFLYM", "반영년월", typeof(string), false));
            import.UpdateExcel = this;
            import.ShowDialog();
        }

        /// <summary>
        /// 엑셀파일 다운로드이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportExcel_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.ultraGrid1, "계량기교체정보");
                this.excelManager.Save("계량기교체정보", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 검색버튼 클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectMeterChangeInformation(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 추가버튼 클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void insertBtn_Click(object sender, EventArgs e)
        {
            this.InsertMeterChangeInformationDetail();
        }

        /// <summary>
        /// 저장버튼 클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            this.UpdateMeterChangeInformationDetail();
        }

        /// <summary>
        /// 삭제버튼 클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                this.DeleteMeterChangeInformationDetail();
            }
        }

        /// <summary>
        /// 수용가검색버튼 클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dmSearchBtn_Click(object sender, EventArgs e)
        {
            frmDMSearch form = new frmDMSearch();
            form.Owner = this;
            form.ShowDialog();
        }

        /// <summary>
        /// 검색된 계량기를 선택 상태로 만든다.
        /// </summary>
        private void SelectedWaterMeter()
        {
            //레이어를 가져온다.
            ILayer pLayer = this.getLayerInstance();

            //레이어가 없으면 넘어감,
            if (pLayer == null)
            {
                return;
            }

            IFeatureSelection selection = (IFeatureSelection)pLayer;
            selection.Clear();

            //검색내용이 없으면 넘어감,
            if (this.ultraGrid1.Rows.Count == 0)
            {
                this.mainMap.IMapControl3.ActiveView.Refresh();
                return;
            }

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                if (!gridRow.Hidden)
                {
                    string strWhere = "DMNO = '" + gridRow.Cells["DMNO"].Value.ToString() + "'";
                    IFeature feature = ArcManager.GetFeature(pLayer, strWhere);
                    if (feature != null)
                    {
                        selection.Add(feature);
                    }
                }
            }
            this.mainMap.IMapControl3.MapScale = 1000;
            this.mainMap.IMapControl3.ActiveView.Refresh();
        }

        /// <summary>
        /// 활성화된 로우에 클릭 이벤트를 준다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Selected = true;
            }
        }

        /// <summary>
        /// 그리드 로우 선택 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //레이어를 가져온다.
            ILayer pLayer = this.getLayerInstance();

            IFeatureSelection selection = (IFeatureSelection)pLayer;
            selection.Clear();
            
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                this.mainMap.IMapControl3.ActiveView.Refresh();
                return;
            }

            UltraGridRow gridRow = this.ultraGrid1.Selected.Rows[0];

            string strWhere = "DMNO = '" + gridRow.Cells["DMNO"].Value.ToString() + "'"; 

            IFeature feature = ArcManager.GetFeature(pLayer, strWhere);

            if (feature != null)
            {
                selection.Add(feature);
                this.mainMap.IMapControl3.MapScale = 1000;
                ArcManager.MoveCenterAt(this.mainMap.IMapControl3, feature.Shape);
            }

            this.SetEnabledDetailForm(this.DetailEnabled);
            this.detailblock.StopEvent = true;
            this.SelectMeterChangeInformationDetail();
        }

        /// <summary>
        /// 목록조회
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectMeterChangeInformation(Hashtable parameter)
        {
            //최종선택한 내용을 저장함,
            this.mainMap.MoveToBlock(parameter);
            this.mainMap.SelectedFeatureByBlock(parameter);
            this.ultraGrid1.DataSource = MeterChangeInformationWork.GetInstance().SelectMeterChangeInformation(parameter).Tables[0];
        }

        /// <summary>
        /// 상세조회
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectMeterChangeInformationDetail()
        {
            Utils.ClearControls(this.panel2);

            UltraGridRow row = this.ultraGrid1.Selected.Rows[0];
            if (row == null)
            {
                return;
            }

            Hashtable parameter = Utils.ConverToHashtable(row);
            Utils.SetControlDataSetting(parameter, this.panel2);
        }

        /// <summary>
        /// 등록
        /// </summary>
        /// <param name="parameter"></param>
        private void InsertMeterChangeInformationDetail()
        {
            this.ultraGrid1.Selected.Rows.Clear();
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Activated = false;
            }
            Utils.ClearControls(this.panel2);
            this.SetEnabledDetailForm(this.DetailEnabled);

            if (this.ultraGrid1.Rows.Count > 0)
            {
                this.ultraGrid1.ActiveRowScrollRegion.ScrollRowIntoView(this.ultraGrid1.Rows[0]);
            }
        }

        /// <summary>
        /// 수정
        /// </summary>
        /// <param name="parameter"></param>
        private void UpdateMeterChangeInformationDetail()
        {
            Hashtable parameter = Utils.ConverToHashtable(this.panel2);

            if (parameter.ContainsKey("DMNO"))
            {
                if (parameter["DMNO"].ToString() == string.Empty)
                {
                    MessageBox.Show("수용가를 선택해야 합니다.");
                    return;
                }
            }

            if (this.ultraGrid1.Selected.Rows.Count > 0)
            {
                MeterChangeInformationWork.GetInstance().UpdateMeterChangeInformationDetail(parameter);
            }
            else if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                MeterChangeInformationWork.GetInstance().InsertMeterChangeInformationDetail(parameter);
            }
            
            this.SelectMeterChangeInformation(this.searchBox1.Parameters);
            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                if (row.Cells["DMNO"].Value.ToString() == parameter["DMNO"].ToString() &&
                    row.Cells["MTRCHGSERNO"].Value.ToString() == parameter["MTRCHGSERNO"].ToString() &&
                    row.Cells["MDFY_YN"].Value.ToString() == "Y")
                {
                    if (this.ultraGrid1.ActiveRow != null)
                    {
                        this.ultraGrid1.ActiveRow.Activated = false;
                    }
                    this.ultraGrid1.ActiveRow = row;
                    this.ultraGrid1.ActiveRowScrollRegion.ScrollRowIntoView(row);
                }
            }
        }

        /// <summary>
        /// 등록인경우 검색조건과 맞춰서 재검색할수있게 검색조건을 세팅한다.
        /// 
        /// 1.검색이전만 세팅함.
        /// </summary>
        /// <param name="parameter"></param>
        private void ValidateSearchBox(Hashtable parameter)
        {
            if (this.searchBox1.Parameters == null)
            {
                this.searchBox1.StartDateObject.Value = this.mddt.Value;
                this.searchBox1.EndDateObject.Value = this.mddt.Value;
                this.searchBox1.LargeBlockObject.SelectedValue = this.lblock.SelectedValue;
                this.searchBox1.MiddleBlockObject.SelectedValue = this.mblock.SelectedValue;
                this.searchBox1.SmallBlockObject.SelectedValue = this.sblock.SelectedValue;
                this.searchBox1.InitializeParameter();
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        /// <param name="parameter"></param>
        private void DeleteMeterChangeInformationDetail()
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                return;
            }

            UltraGridRow row = this.ultraGrid1.Selected.Rows[0];
            if (row == null)
            {
                return;
            }

            Hashtable parameter = Utils.ConverToHashtable(row);
            MeterChangeInformationWork.GetInstance().DeleteMeterChangeInformationDetail(parameter);
            this.SelectMeterChangeInformation(this.searchBox1.Parameters);
        }

        /// <summary>
        /// 수도계량기 레이어를 반환
        /// </summary>
        /// <returns></returns>
        private ILayer getLayerInstance()
        {
            if (this.pLayer == null)
            {
                this.pLayer = ArcManager.GetMapLayer(this.mainMap.IMapControl3, "수도계량기");
            }
            return this.pLayer;
        }

        /// <summary>
        /// 선택한 수용가의 데이터를 설정한다.
        /// </summary>
        /// <param name="parameter"></param>
        public void SetDMInfo(Hashtable parameter)
        {
            Utils.SetControlDataSetting(parameter, this.panel2);
        }

        /// <summary>
        /// 폼이 닫힌 후 처리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Disposed(object sender, EventArgs e)
        {
            
        }

        #region ExcelImport 멤버

        public void update(System.Data.DataTable data)
        {
            MeterChangeInformationWork.GetInstance().UpdateExcelImport(data);
        }

        #endregion
    }
}
