﻿using System;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using System.Collections;
using WaterNet.WV_MeterChangeInformation.work;
using WaterNet.WV_Common.util;
using Infragistics.Win;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.form;
using EMFrame.log;

namespace WaterNet.WV_MeterChangeInformation.form
{
    public partial class frmDMSearch : Form
    {
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        public frmDMSearch()
        {
            InitializeComponent();
            Load += new EventHandler(frmDMSearch_Load);
        }

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// 데이터 확인용폼으로 사용될 경우
        /// </summary>
        public bool UsePlan
        {
            set
            {
                if (value == true)
                {
                    this.searchBox1.Visible = false;
                    this.panel1.Visible = false;
                    this.searchBox1.InitializeParameter();
                    this.SelectDM(this.searchBox1.Parameters);
                }
            }
        }

        private void frmDMSearch_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalType = WaterNet.WV_Common.enum1.INTERVAL_TYPE.NONE;
            this.selectBtn.Enabled = false;
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
        }

        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
        }

        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.selectBtn.Click += new EventHandler(selectBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);

            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid1.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.ultraGrid1, "수용가정보");
                this.excelManager.Save("수용가정보", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void selectBtn_Click(object sender, EventArgs e)
        {
            frmMain owner = this.Owner as frmMain;

            if (owner != null)
            {
                Hashtable parameter = null;

                if (this.ultraGrid1.Selected.Rows.Count == 0)
                {
                    return;
                }

                if (this.ultraGrid1.Selected.Rows.Count > 0)
                {
                    parameter = Utils.ConverToHashtable(this.ultraGrid1.Selected.Rows[0]);
                }
                owner.SetDMInfo(parameter);
                this.Close();
            }
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectDM(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void SelectDM(Hashtable parameter)
        {
            this.ultraGrid1.DataSource = MeterChangeInformationWork.GetInstance().SelectDM(parameter).Tables[0];
        }

        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Selected = true;
            }
        }

        private void ultraGrid1_AfterSelectChange(object sender, Infragistics.Win.UltraWinGrid.AfterSelectChangeEventArgs e)
        {
            if (this.ultraGrid1.Selected.Rows.Count > 0)
            {
                this.selectBtn.Enabled = true;
            }
            else if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                this.selectBtn.Enabled = false;
            }
        }
    }
}
