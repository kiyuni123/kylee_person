﻿using System.Text;
using System.Data;
using WaterNet.WaterNetCore;
using System.Collections;
using WaterNet.WV_Common.dao;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_MeterChangeInformation.dao
{
    public class MeterChangeInformationDao : BaseDao
    {
        private static MeterChangeInformationDao dao = null;

        private MeterChangeInformationDao() { }

        public static MeterChangeInformationDao GetInstance()
        {
            if (dao == null)
            {
                dao = new MeterChangeInformationDao();
            }
            return dao;
        }

        //계량기교체 조회
        public void SelectMeterChangeInformation(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with                                                                                                                             ");
            query.AppendLine("loc as                                                                                                                           ");
            query.AppendLine("(                                                                                                                                ");
            query.AppendLine("select c1.loc_code                                                                                                               ");
            query.AppendLine("      ,c1.sgccd                                                                                                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))  ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                          ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                          ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                           ");
            query.AppendLine("      ,c1.ord                                                                                                                    ");
            query.AppendLine("  from                                                                                                                           ");
            query.AppendLine("      (                                                                                                                          ");
            query.AppendLine("       select sgccd                                                                                                              ");
            query.AppendLine("             ,loc_code                                                                                                           ");
            query.AppendLine("             ,ploc_code                                                                                                          ");
            query.AppendLine("             ,loc_name                                                                                                           ");
            query.AppendLine("             ,ftr_idn                                                                                                            ");
            query.AppendLine("             ,ftr_code                                                                                                           ");
            query.AppendLine("             ,rel_loc_name                                                                                                       ");
            query.AppendLine("             ,rownum ord                                                                                                         ");
            query.AppendLine("         from cm_location                                                                                                        ");
            query.AppendLine("        where 1 = 1                                                                                                              ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                          ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                    ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                ");
            query.AppendLine("      ) c1                                                                                                                       ");
            query.AppendLine("       ,cm_location c2                                                                                                           ");
            query.AppendLine("       ,cm_location c3                                                                                                           ");
            query.AppendLine(" where 1 = 1                                                                                                                     ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                             ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                             ");
            query.AppendLine(" order by c1.ord                                                                                                                 ");
            query.AppendLine(")                                                                                                                                ");
            query.AppendLine("select loc.loc_code                                                                                                              ");
            query.AppendLine("      ,loc.sgccd                                                                                                                 ");
            query.AppendLine("      ,loc.lblock                                                                                                                ");
            query.AppendLine("      ,loc.mblock                                                                                                                ");
            query.AppendLine("      ,loc.sblock                                                                                                                ");
            query.AppendLine("      ,dmi.dmno                                                                                                                  ");
            query.AppendLine("      ,dmi.dmnm                                                                                                                  ");
            //query.AppendLine("      ,dmi.nrdaddr                                                                                                               ");
            query.AppendLine("      ,mtr.mtrchgserno                                                                                                           ");
            query.AppendLine("      ,mtr.mdfy_yn                                                                                                               ");
            query.AppendLine("      ,mtr.transclass                                                                                                            ");
            query.AppendLine("      ,mtr.mrym_y                                                                                                                ");
            query.AppendLine("      ,mtr.mrym_m                                                                                                                ");
            query.AppendLine("      ,to_char(mtr.mddt,'yyyy-mm-dd') mddt                                                                                       ");
            query.AppendLine("      ,mtr.omfrno                                                                                                                ");
            query.AppendLine("      ,mtr.owspipeszcd                                                                                                           ");
            query.AppendLine("      ,mtr.wsrmvndl                                                                                                              ");
            query.AppendLine("      ,mtr.nmfrno                                                                                                                ");
            query.AppendLine("      ,mtr.nwspipeszcd                                                                                                           ");
            query.AppendLine("      ,mtr.wsattndl                                                                                                              ");
            query.AppendLine("      ,mtr.transrs                                                                                                               ");
            query.AppendLine("  from loc                                                                                                                       ");
            query.AppendLine("      ,wi_dminfo dmi                                                                                                                ");
            query.AppendLine("    ,(                                                                                                                           ");
            query.AppendLine("    select dmno                                                                                                                  ");
            query.AppendLine("          ,mtrchgserno                                                                                                           ");
            query.AppendLine("          ,'N' mdfy_yn                                                                                                           ");
            query.AppendLine("          ,transclass                                                                                                            ");
            query.AppendLine("          ,to_char(to_date(mrym,'yyyymm'), 'yyyy') mrym_y                                                                        ");
            query.AppendLine("          ,to_char(to_date(mrym,'yyyymm'), 'mm') mrym_m                                                                          ");
            query.AppendLine("          ,to_date(mddt,'yyyymmdd') mddt                                                                                         ");
            query.AppendLine("          ,omfrno                                                                                                                ");
            query.AppendLine("          ,owspipeszcd                                                                                                           ");
            query.AppendLine("          ,wsrmvndl                                                                                                              ");
            query.AppendLine("          ,nmfrno                                                                                                                ");
            query.AppendLine("          ,nwspipeszcd                                                                                                           ");
            query.AppendLine("          ,wsattndl                                                                                                              ");
            query.AppendLine("          ,transrs                                                                                                               ");
            query.AppendLine("      from wi_mtrchginfo                                                                                                            ");
            query.AppendLine("     where mddt between :STARTDATE and :ENDDATE                                                                                  ");
            query.AppendLine("    union all                                                                                                                    ");
            query.AppendLine("    select dmno                                                                                                                  ");
            query.AppendLine("          ,mtrchgserno                                                                                                           ");
            query.AppendLine("          ,'Y' mdfy_yn                                                                                                           ");
            query.AppendLine("          ,transclass                                                                                                            ");
            query.AppendLine("          ,to_char(to_date(mrym,'yyyymm'), 'yyyy') mrym_y                                                                        ");
            query.AppendLine("          ,to_char(to_date(mrym,'yyyymm'), 'mm') mrym_m                                                                          ");
            query.AppendLine("          ,to_date(mddt,'yyyymmdd') mddt                                                                                         ");
            query.AppendLine("          ,omfrno                                                                                                                ");
            query.AppendLine("          ,owspipeszcd                                                                                                           ");
            query.AppendLine("          ,wsrmvndl                                                                                                              ");
            query.AppendLine("          ,nmfrno                                                                                                                ");
            query.AppendLine("          ,nwspipeszcd                                                                                                           ");
            query.AppendLine("          ,wsattndl                                                                                                              ");
            query.AppendLine("          ,transrs                                                                                                               ");
            query.AppendLine("      from wv_mtrchg                                                                                                             ");
            query.AppendLine("     where mddt between :STARTDATE and :ENDDATE                                                                                  ");
            query.AppendLine("    ) mtr                                                                                                                        ");
            query.AppendLine(" where 1 = 1                                                                                                                     ");
            query.AppendLine("   and dmi.sgccd = loc.sgccd                                                                                                     ");
            query.AppendLine("   and dmi.lftridn = loc.lftridn                                                                                                 ");
            query.AppendLine("   and dmi.mftridn = loc.mftridn                                                                                                 ");
            query.AppendLine("   and dmi.sftridn = loc.sftridn                                                                                                 ");
            query.AppendLine("   and mtr.dmno = dmi.dmno                                                                                                       ");
            query.AppendLine(" order by                                                                                                                        ");
            query.AppendLine("       loc.ord                                                                                                                   ");
            query.AppendLine("      ,mtr.mddt                                                                                                                  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];

            //println(query);

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //계량기교체 수정
        public void UpdateMeterChangeInformationDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update wv_mtrchg a                                                               ");
            query.AppendLine("   set (dmno                                                                     ");
            query.AppendLine("      ,mtrchgserno                                                               ");
            query.AppendLine("      ,mdfy_yn                                                                   ");
            query.AppendLine("      ,transclass                                                                ");
            query.AppendLine("      ,mrym                                                                      ");
            query.AppendLine("      ,mddt                                                                      ");
            query.AppendLine("      ,omfrno                                                                    ");
            query.AppendLine("      ,owspipeszcd                                                               ");
            query.AppendLine("      ,wsrmvndl                                                                  ");
            query.AppendLine("      ,nmfrno                                                                    ");
            query.AppendLine("      ,nwspipeszcd                                                               ");
            query.AppendLine("      ,wsattndl                                                                  ");
            query.AppendLine("      ,transrs ) =                                                               ");
            query.AppendLine("       (                                                                         ");
            query.AppendLine("       select                                                                    ");
            query.AppendLine("              :DMNO                                                              ");
            query.AppendLine("             ,:MTRCHGSERNO                                                       ");
            query.AppendLine("             ,:MDFY_YN                                                           ");
            query.AppendLine("             ,:TRANSCLASS                                                        ");
            query.AppendLine("             ,:MRYM_Y||:MRYM_M                                                   ");
            query.AppendLine("             ,:MDDT                                                              ");
            query.AppendLine("             ,:OMFRNO                                                            ");
            query.AppendLine("             ,:OWSPIPESZCD                                                       ");
            query.AppendLine("             ,:WSRMVNDL                                                          ");
            query.AppendLine("             ,:NMFRNO                                                            ");
            query.AppendLine("             ,:NWSPIPESZCD                                                       ");
            query.AppendLine("             ,:WSATTNDL                                                          ");
            query.AppendLine("             ,:TRANSRS                                                           ");
            query.AppendLine("         from dual                                                               ");
            query.AppendLine("       )                                                                         ");
            query.AppendLine(" where 1 = 1                                                                     ");
            query.AppendLine("   and a.dmno = :DMNO                                                            ");
            query.AppendLine("   and a.mtrchgserno = :MTRCHGSERNO                                              ");
            query.AppendLine("   and a.mdfy_yn = :MDFY_YN                                                      ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("DMNO", OracleDbType.Varchar2)
                    ,new OracleParameter("MTRCHGSERNO", OracleDbType.Varchar2)
	                ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                    ,new OracleParameter("TRANSCLASS", OracleDbType.Varchar2)
                    ,new OracleParameter("MRYM_Y", OracleDbType.Varchar2)
                    ,new OracleParameter("MRYM_M", OracleDbType.Varchar2)
                    ,new OracleParameter("MDDT", OracleDbType.Varchar2)
                    ,new OracleParameter("OMFRNO", OracleDbType.Varchar2)
                    ,new OracleParameter("OWSPIPESZCD", OracleDbType.Varchar2)
                    ,new OracleParameter("WSRMVNDL", OracleDbType.Varchar2)
                    ,new OracleParameter("NMFRNO", OracleDbType.Varchar2)
                    ,new OracleParameter("NWSPIPESZCD", OracleDbType.Varchar2)
                    ,new OracleParameter("WSATTNDL", OracleDbType.Varchar2)
                    ,new OracleParameter("TRANSRS", OracleDbType.Varchar2)
                    ,new OracleParameter("DMNO", OracleDbType.Varchar2)
                    ,new OracleParameter("MTRCHGSERNO", OracleDbType.Varchar2)
                    ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["DMNO"];
            parameters[1].Value = parameter["MTRCHGSERNO"];
            parameters[2].Value = parameter["MDFY_YN"];
            parameters[3].Value = parameter["TRANSCLASS"];
            parameters[4].Value = parameter["MRYM_Y"];
            parameters[5].Value = parameter["MRYM_M"];
            parameters[6].Value = parameter["MDDT"];
            parameters[7].Value = parameter["OMFRNO"];
            parameters[8].Value = parameter["OWSPIPESZCD"];
            parameters[9].Value = parameter["WSRMVNDL"];
            parameters[10].Value = parameter["NMFRNO"];
            parameters[11].Value = parameter["NWSPIPESZCD"];
            parameters[12].Value = parameter["WSATTNDL"];
            parameters[13].Value = parameter["TRANSRS"];
            parameters[14].Value = parameter["DMNO"];
            parameters[15].Value = parameter["MTRCHGSERNO"];
            parameters[16].Value = parameter["MDFY_YN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //계량기교체 등록
        public void InsertMeterChangeInformationDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("insert into wv_mtrchg a                                                          ");
            query.AppendLine("select                                                                           ");
            query.AppendLine("       :DMNO                                                                     ");
            query.AppendLine("      ,:MTRCHGSERNO                                                              ");
            query.AppendLine("      ,:MDFY_YN                                                                  ");
            query.AppendLine("      ,:TRANSCLASS                                                               ");
            query.AppendLine("      ,:MRYM_Y||:MRYM_M                                                          ");
            query.AppendLine("      ,:MDDT                                                                     ");
            query.AppendLine("      ,:OMFRNO                                                                   ");
            query.AppendLine("      ,:OWSPIPESZCD                                                              ");
            query.AppendLine("      ,:WSRMVNDL                                                                 ");
            query.AppendLine("      ,:NMFRNO                                                                   ");
            query.AppendLine("      ,:NWSPIPESZCD                                                              ");
            query.AppendLine("      ,:WSATTNDL                                                                 ");
            query.AppendLine("      ,:TRANSRS                                                                  ");
            query.AppendLine("      ,''                                                                        ");
            query.AppendLine("  from dual                                                                      ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("DMNO", OracleDbType.Varchar2)
                    ,new OracleParameter("MTRCHGSERNO", OracleDbType.Varchar2)
	                ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                    ,new OracleParameter("TRANSCLASS", OracleDbType.Varchar2)
                    ,new OracleParameter("MRYM_Y", OracleDbType.Varchar2)
                    ,new OracleParameter("MRYM_M", OracleDbType.Varchar2)
                    ,new OracleParameter("MDDT", OracleDbType.Varchar2)
                    ,new OracleParameter("OMFRNO", OracleDbType.Varchar2)
                    ,new OracleParameter("OWSPIPESZCD", OracleDbType.Varchar2)
                    ,new OracleParameter("WSRMVNDL", OracleDbType.Varchar2)
                    ,new OracleParameter("NMFRNO", OracleDbType.Varchar2)
                    ,new OracleParameter("NWSPIPESZCD", OracleDbType.Varchar2)
                    ,new OracleParameter("WSATTNDL", OracleDbType.Varchar2)
                    ,new OracleParameter("TRANSRS", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["DMNO"];
            parameters[1].Value = parameter["MTRCHGSERNO"];
            parameters[2].Value = parameter["MDFY_YN"];
            parameters[3].Value = parameter["TRANSCLASS"];
            parameters[4].Value = parameter["MRYM_Y"];
            parameters[5].Value = parameter["MRYM_M"];
            parameters[6].Value = parameter["MDDT"];
            parameters[7].Value = parameter["OMFRNO"];
            parameters[8].Value = parameter["OWSPIPESZCD"];
            parameters[9].Value = parameter["WSRMVNDL"];
            parameters[10].Value = parameter["NMFRNO"];
            parameters[11].Value = parameter["NWSPIPESZCD"];
            parameters[12].Value = parameter["WSATTNDL"];
            parameters[13].Value = parameter["TRANSRS"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //계량기교체 삭제
        public void DeleteMeterChangeInformationDetail(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete from wv_mtrchg                                                           "); 
            query.AppendLine(" where 1 = 1                                                                    ");
            query.AppendLine("   and dmno = :DMNO                                                             ");
            query.AppendLine("   and mtrchgserno = :MTRCHGSERNO                                               ");
            query.AppendLine("   and mdfy_yn = :MDFY_YN                                                       ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("DMNO", OracleDbType.Varchar2)
                    ,new OracleParameter("MTRCHGSERNO", OracleDbType.Varchar2)
	                ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["DMNO"];
            parameters[1].Value = parameter["MTRCHGSERNO"];
            parameters[2].Value = parameter["MDFY_YN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //계량기 교체순번 조회
        public object GetMeterChangeNo(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("select nvl(max(to_number(mtrchgserno)),0) + 1                                     ");
            query.AppendLine("  from wv_mtrchg                                                                  ");
            query.AppendLine(" where dmno = :DMNO                                                               ");
            query.AppendLine("   and mdfy_yn = 'Y'                                                              ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("DMNO", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["DMNO"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //블록별 수용가 조회
        public void SelectDM(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with                                                                                                                             ");
            query.AppendLine("loc as                                                                                                                           ");
            query.AppendLine("(                                                                                                                                ");
            query.AppendLine("select c1.loc_code                                                                                                               ");
            query.AppendLine("      ,c1.sgccd                                                                                                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))  ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                          ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn                                          ");
            query.AppendLine("      ,c1.rel_loc_name                                                                                                           ");
            query.AppendLine("      ,c1.ord                                                                                                                    ");
            query.AppendLine("  from                                                                                                                           ");
            query.AppendLine("      (                                                                                                                          ");
            query.AppendLine("       select sgccd                                                                                                              ");
            query.AppendLine("             ,loc_code                                                                                                           ");
            query.AppendLine("             ,ploc_code                                                                                                          ");
            query.AppendLine("             ,loc_name                                                                                                           ");
            query.AppendLine("             ,ftr_idn                                                                                                            ");
            query.AppendLine("             ,ftr_code                                                                                                           ");
            query.AppendLine("             ,rel_loc_name                                                                                                       ");
            query.AppendLine("             ,rownum ord                                                                                                         ");
            query.AppendLine("         from cm_location                                                                                                        ");
            query.AppendLine("        where 1 = 1                                                                                                              ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                          ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                    ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                     ");
            query.AppendLine("      ) c1                                                                                                                       ");
            query.AppendLine("       ,cm_location c2                                                                                                           ");
            query.AppendLine("       ,cm_location c3                                                                                                           ");
            query.AppendLine(" where 1 = 1                                                                                                                     ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                             ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                             ");
            query.AppendLine(" order by c1.ord                                                                                                                 ");
            query.AppendLine(")                                                                                                                                ");
            query.AppendLine("select loc.loc_code                                                                                                              ");
            query.AppendLine("      ,loc.lblock                                                                                                                ");
            query.AppendLine("      ,loc.mblock                                                                                                                ");
            query.AppendLine("      ,loc.sblock                                                                                                                ");
            query.AppendLine("      ,dmi.sgccd                                                                                                                 ");
            //query.AppendLine("      ,dmi.sggcd                                                                                                                 ");
            query.AppendLine("      ,dmi.umdcd                                                                                                                 ");
            //query.AppendLine("      ,dmi.bgcd                                                                                                                  ");
            query.AppendLine("      ,dmi.dmno                                                                                                                  ");
            query.AppendLine("      ,dmi.dmnm                                                                                                                  ");
            //query.AppendLine("      ,dmi.lotno                                                                                                                 ");
            query.AppendLine("      ,dmi.hydrntno                                                                                                              ");
            //query.AppendLine("      ,dmi.nrdaddr                                                                                                               ");
            query.AppendLine("  from loc                                                                                                                       ");
            query.AppendLine("      ,wi_dminfo dmi                                                                                                                ");
            query.AppendLine(" where 1 = 1                                                                                                                     ");
            query.AppendLine("   and dmi.sgccd = loc.sgccd                                                                                                     ");
            query.AppendLine("   and dmi.lftridn = loc.lftridn                                                                                                 ");
            query.AppendLine("   and dmi.mftridn = loc.mftridn                                                                                                 ");
            query.AppendLine("   and dmi.sftridn = loc.sftridn                                                                                                 ");
            query.AppendLine(" order by                                                                                                                        ");
            query.AppendLine("       loc.ord                                                                                                                   ");
            query.AppendLine("      ,dmi.dmno                                                                                                                  ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public void UpdateExcelImport(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO WV_MTRCHG A ");
            query.AppendLine("USING ");
            query.AppendLine("( ");
            query.AppendLine("      SELECT :DMNO DMNO ");
            query.AppendLine("            ,:MTRCHGSERNO MTRCHGSERNO ");
            query.AppendLine("            ,:MDFY_YN MDFY_YN ");
            query.AppendLine("            ,:TRANSCLASS TRANSCLASS ");
            query.AppendLine("            ,:MRYM MRYM ");
            query.AppendLine("            ,:MDDT MDDT ");
            query.AppendLine("            ,:OMFRNO OMFRNO ");
            query.AppendLine("            ,:OWSPIPESZCD OWSPIPESZCD ");
            query.AppendLine("            ,:WSRMVNDL WSRMVNDL ");
            query.AppendLine("            ,:NMFRNO NMFRNO ");
            query.AppendLine("            ,:NWSPIPESZCD NWSPIPESZCD ");
            query.AppendLine("            ,:WSATTNDL WSATTNDL ");
            query.AppendLine("            ,:TRANSRS TRANSRS ");
            query.AppendLine("            ,:RFLYM RFLYM ");
            query.AppendLine("        FROM DUAL ");
            query.AppendLine(") B ");
            query.AppendLine("   ON ");
            query.AppendLine("( ");
            query.AppendLine("             A.DMNO = B.DMNO ");
            query.AppendLine("         AND A.MTRCHGSERNO = B.MTRCHGSERNO ");
            query.AppendLine("         AND A.MDFY_YN = B.MDFY_YN ");
            query.AppendLine(")  ");
            query.AppendLine("WHEN MATCHED THEN ");
            query.AppendLine("     UPDATE SET A.TRANSCLASS = B.TRANSCLASS ");
            query.AppendLine("               ,A.MRYM = B.MRYM ");
            query.AppendLine("               ,A.MDDT = B.MDDT ");
            query.AppendLine("               ,A.OMFRNO = B.OMFRNO ");
            query.AppendLine("               ,A.OWSPIPESZCD = B.OWSPIPESZCD ");
            query.AppendLine("               ,A.WSRMVNDL = B.WSRMVNDL ");
            query.AppendLine("               ,A.NMFRNO = B.NMFRNO ");
            query.AppendLine("               ,A.NWSPIPESZCD = B.NWSPIPESZCD ");
            query.AppendLine("               ,A.WSATTNDL = B.WSATTNDL ");
            query.AppendLine("               ,A.TRANSRS = B.TRANSRS ");
            query.AppendLine("               ,A.RFLYM = B.RFLYM ");
            query.AppendLine("WHEN NOT MATCHED THEN ");
            query.AppendLine("     INSERT (A.DMNO, A.MTRCHGSERNO, A.MDFY_YN, A.TRANSCLASS, A.MRYM, A.MDDT, A.OMFRNO, A.OWSPIPESZCD, A.WSRMVNDL, A.NMFRNO, A.NWSPIPESZCD, A.WSATTNDL, A.TRANSRS, A.RFLYM) ");
            query.AppendLine("     VALUES (B.DMNO, B.MTRCHGSERNO, B.MDFY_YN, B.TRANSCLASS, B.MRYM, B.MDDT, B.OMFRNO, B.OWSPIPESZCD, B.WSRMVNDL, B.NMFRNO, B.NWSPIPESZCD, B.WSATTNDL, B.TRANSRS, B.RFLYM) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("DMNO", OracleDbType.Varchar2)
                    ,new OracleParameter("MTRCHGSERNO", OracleDbType.Varchar2)
	                ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                    ,new OracleParameter("TRANSCLASS", OracleDbType.Varchar2)
                    ,new OracleParameter("MRYM", OracleDbType.Varchar2)
                    ,new OracleParameter("MDDT", OracleDbType.Varchar2)
                    ,new OracleParameter("OMFRNO", OracleDbType.Varchar2)
                    ,new OracleParameter("OWSPIPESZCD", OracleDbType.Varchar2)
                    ,new OracleParameter("WSRMVNDL", OracleDbType.Varchar2)
                    ,new OracleParameter("NMFRNO", OracleDbType.Varchar2)
                    ,new OracleParameter("NWSPIPESZCD", OracleDbType.Varchar2)
                    ,new OracleParameter("WSATTNDL", OracleDbType.Varchar2)
                    ,new OracleParameter("TRANSRS", OracleDbType.Varchar2)
                    ,new OracleParameter("RFLYM", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["DMNO"];
            parameters[1].Value = parameter["MTRCHGSERNO"];
            parameters[2].Value = parameter["MDFY_YN"];
            parameters[3].Value = parameter["TRANSCLASS"];
            parameters[4].Value = parameter["MRYM"];
            parameters[5].Value = parameter["MDDT"];
            parameters[6].Value = parameter["OMFRNO"];
            parameters[7].Value = parameter["OWSPIPESZCD"];
            parameters[8].Value = parameter["WSRMVNDL"];
            parameters[9].Value = parameter["NMFRNO"];
            parameters[10].Value = parameter["NWSPIPESZCD"];
            parameters[11].Value = parameter["WSATTNDL"];
            parameters[12].Value = parameter["TRANSRS"];
            parameters[13].Value = parameter["RFLYM"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
