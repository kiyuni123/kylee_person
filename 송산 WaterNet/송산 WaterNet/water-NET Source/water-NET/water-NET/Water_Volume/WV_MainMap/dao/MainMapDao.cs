﻿using System.Text;
using WaterNet.WV_Common.dao;
using System.Data;
using WaterNet.WaterNetCore;

namespace WaterNet.WV_MainMap.dao
{
    public class MainMapDao : BaseDao
    {
        private static MainMapDao dao = null;

        private MainMapDao() { }
        public static MainMapDao GetInstance()
        {
            if (dao == null)
            {
                dao = new MainMapDao();
            }
            return dao;
        }

        public void SelectSmallBlock(OracleDBManager manager, DataSet dataSet, string dataMember)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select c1.loc_code                                                                                                                   ");
            query.AppendLine("      ,c1.sgccd																													   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))	   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock									   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock									   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock											   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn)))	   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.ftr_idn), c3.ftr_idn)) lftridn										   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.ftr_idn), c2.ftr_idn)) mftridn										   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.ftr_idn)) sftridn											   ");
            query.AppendLine("      ,c1.rel_loc_name																											   ");
            query.AppendLine("      ,c1.ord																														   ");
            query.AppendLine("  from																															   ");
            query.AppendLine("      (																															   ");
            query.AppendLine("       select sgccd																												   ");
            query.AppendLine("             ,loc_code																											   ");
            query.AppendLine("             ,ploc_code																											   ");
            query.AppendLine("             ,loc_name																											   ");
            query.AppendLine("             ,ftr_idn																												   ");
            query.AppendLine("             ,ftr_code																											   ");
            query.AppendLine("             ,rel_loc_name																										   ");
            query.AppendLine("             ,rownum ord																											   ");
            query.AppendLine("         from cm_location																											   ");
            query.AppendLine("        where 1 = 1																												   ");
            query.AppendLine("          and ftr_code = 'BZ003'																									   ");
            query.AppendLine("        start with ftr_code = 'BZ001'																								   ");
            query.AppendLine("        connect by prior loc_code = ploc_code																						   ");
            query.AppendLine("        order SIBLINGS by ftr_idn																						   ");
            query.AppendLine("      ) c1																														   ");
            query.AppendLine("       ,cm_location c2																											   ");
            query.AppendLine("       ,cm_location c3																											   ");
            query.AppendLine(" where 1 = 1																														   ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																								   ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																								   ");
            query.AppendLine(" order by c1.ord																													   ");

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), null);
        }
    }
}
