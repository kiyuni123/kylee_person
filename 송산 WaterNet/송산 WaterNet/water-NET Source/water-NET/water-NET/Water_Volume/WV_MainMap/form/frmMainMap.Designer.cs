﻿namespace WaterNet.WV_MainMap.form
{
    partial class frmMainMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainMap));
            this.contextMenuStripPopup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.누수감시toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.누수감시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.자동누수감시결과ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.누수량산정결과조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.집중관리우선순위조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.공급량야간최소유량분석조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.야간최소유량산정방식별조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유수율분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.블록별공급량수압트랜드조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유수율현황조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.월유수율분석보고서ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유수율상호분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.총괄수량수지분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유수율제고사업추진관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.계량기교체관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.누수지점관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.관개대체관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.단수작업관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.사업추진효과분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.블록별일반현황정보조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.블록기본설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spcContents.Panel1.SuspendLayout();
            this.spcContents.Panel2.SuspendLayout();
            this.spcContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).BeginInit();
            this._frmMapAutoHideControl.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.tabTOC.SuspendLayout();
            this.spcTOC.Panel2.SuspendLayout();
            this.spcTOC.SuspendLayout();
            this.tabPageTOC.SuspendLayout();
            this.spcIndexMap.Panel1.SuspendLayout();
            this.spcIndexMap.Panel2.SuspendLayout();
            this.spcIndexMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).BeginInit();
            this.contextMenuStripPopup.SuspendLayout();
            this.SuspendLayout();
            // 
            // spcContents
            // 
            // 
            // axToolbar
            // 
            this.axToolbar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axToolbar.OcxState")));
            // 
            // DockManagerCommand
            // 
            this.DockManagerCommand.DefaultGroupSettings.ForceSerialization = true;
            this.DockManagerCommand.DefaultPaneSettings.ForceSerialization = true;
            // 
            // _frmMapAutoHideControl
            // 
            this._frmMapAutoHideControl.Size = new System.Drawing.Size(11, 583);
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.Location = new System.Drawing.Point(-10000, 0);
            this.dockableWindow1.TabIndex = 12;
            // 
            // spcTOC
            // 
            // 
            // tvBlock
            // 
            this.tvBlock.LineColor = System.Drawing.Color.Black;
            // 
            // spcIndexMap
            // 
            // 
            // axTOC
            // 
            this.axTOC.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOC.OcxState")));
            // 
            // axMap
            // 
            this.axMap.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMap.OcxState")));
            this.axMap.Size = new System.Drawing.Size(661, 524);
            // 
            // contextMenuStripPopup
            // 
            this.contextMenuStripPopup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.누수감시toolStripMenuItem1,
            this.toolStripSeparator1,
            this.누수감시ToolStripMenuItem,
            this.유수율분석ToolStripMenuItem,
            this.유수율제고사업추진관리ToolStripMenuItem,
            this.블록별일반현황정보조회ToolStripMenuItem,
            this.toolStripSeparator2,
            this.블록기본설정ToolStripMenuItem});
            this.contextMenuStripPopup.Name = "contextMenuStripPopup";
            this.contextMenuStripPopup.Size = new System.Drawing.Size(219, 148);
            this.contextMenuStripPopup.Text = "누수감시";
            // 
            // 누수감시toolStripMenuItem1
            // 
            this.누수감시toolStripMenuItem1.CheckOnClick = true;
            this.누수감시toolStripMenuItem1.Name = "누수감시toolStripMenuItem1";
            this.누수감시toolStripMenuItem1.Size = new System.Drawing.Size(218, 22);
            this.누수감시toolStripMenuItem1.Text = "누수감시 결과 GIS 전환";
            this.누수감시toolStripMenuItem1.Click += new System.EventHandler(this.누수감시toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(215, 6);
            // 
            // 누수감시ToolStripMenuItem
            // 
            this.누수감시ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.자동누수감시결과ToolStripMenuItem,
            this.누수량산정결과조회ToolStripMenuItem,
            this.집중관리우선순위조회ToolStripMenuItem,
            this.공급량야간최소유량분석조회ToolStripMenuItem,
            this.야간최소유량산정방식별조회ToolStripMenuItem});
            this.누수감시ToolStripMenuItem.Name = "누수감시ToolStripMenuItem";
            this.누수감시ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.누수감시ToolStripMenuItem.Text = "누수감시";
            // 
            // 자동누수감시결과ToolStripMenuItem
            // 
            this.자동누수감시결과ToolStripMenuItem.Name = "자동누수감시결과ToolStripMenuItem";
            this.자동누수감시결과ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.자동누수감시결과ToolStripMenuItem.Text = "자동 누수감시 결과";
            this.자동누수감시결과ToolStripMenuItem.Click += new System.EventHandler(this.누수감시결과toolStripMenuItem1_Click);
            // 
            // 누수량산정결과조회ToolStripMenuItem
            // 
            this.누수량산정결과조회ToolStripMenuItem.Name = "누수량산정결과조회ToolStripMenuItem";
            this.누수량산정결과조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.누수량산정결과조회ToolStripMenuItem.Text = "누수량 산정 결과 조회";
            this.누수량산정결과조회ToolStripMenuItem.Click += new System.EventHandler(this.누수량관리toolStripMenuItem_Click);
            // 
            // 집중관리우선순위조회ToolStripMenuItem
            // 
            this.집중관리우선순위조회ToolStripMenuItem.Name = "집중관리우선순위조회ToolStripMenuItem";
            this.집중관리우선순위조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.집중관리우선순위조회ToolStripMenuItem.Text = "집중관리 블록 우선 순위 조회";
            this.집중관리우선순위조회ToolStripMenuItem.Click += new System.EventHandler(this.블록운영분석ToolStripMenuItem_Click);
            // 
            // 공급량야간최소유량분석조회ToolStripMenuItem
            // 
            this.공급량야간최소유량분석조회ToolStripMenuItem.Name = "공급량야간최소유량분석조회ToolStripMenuItem";
            this.공급량야간최소유량분석조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.공급량야간최소유량분석조회ToolStripMenuItem.Text = "공급량/ 야간최소유량 분석 조회";
            this.공급량야간최소유량분석조회ToolStripMenuItem.Click += new System.EventHandler(this.블록별공급량야간최소유량분석toolStripMenuItem_Click);
            // 
            // 야간최소유량산정방식별조회ToolStripMenuItem
            // 
            this.야간최소유량산정방식별조회ToolStripMenuItem.Name = "야간최소유량산정방식별조회ToolStripMenuItem";
            this.야간최소유량산정방식별조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.야간최소유량산정방식별조회ToolStripMenuItem.Text = "야간최소유량 산정 방식별 조회";
            this.야간최소유량산정방식별조회ToolStripMenuItem.Click += new System.EventHandler(this.야간최소유량필터링ToolStripMenuItem_Click);
            // 
            // 유수율분석ToolStripMenuItem
            // 
            this.유수율분석ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.블록별공급량수압트랜드조회ToolStripMenuItem,
            this.유수율현황조회ToolStripMenuItem,
            this.월유수율분석보고서ToolStripMenuItem,
            this.유수율상호분석ToolStripMenuItem,
            this.총괄수량수지분석ToolStripMenuItem});
            this.유수율분석ToolStripMenuItem.Name = "유수율분석ToolStripMenuItem";
            this.유수율분석ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.유수율분석ToolStripMenuItem.Text = "유수율 분석";
            // 
            // 블록별공급량수압트랜드조회ToolStripMenuItem
            // 
            this.블록별공급량수압트랜드조회ToolStripMenuItem.Name = "블록별공급량수압트랜드조회ToolStripMenuItem";
            this.블록별공급량수압트랜드조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.블록별공급량수압트랜드조회ToolStripMenuItem.Text = "블록별 공급량/ 수압트랜드 조회";
            this.블록별공급량수압트랜드조회ToolStripMenuItem.Click += new System.EventHandler(this.유량패턴현황ToolStripMenuItem_Click);
            // 
            // 유수율현황조회ToolStripMenuItem
            // 
            this.유수율현황조회ToolStripMenuItem.Name = "유수율현황조회ToolStripMenuItem";
            this.유수율현황조회ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.유수율현황조회ToolStripMenuItem.Text = "유수율 현황 조회";
            this.유수율현황조회ToolStripMenuItem.Click += new System.EventHandler(this.유수율현황ToolStripMenuItem_Click);
            // 
            // 월유수율분석보고서ToolStripMenuItem
            // 
            this.월유수율분석보고서ToolStripMenuItem.Name = "월유수율분석보고서ToolStripMenuItem";
            this.월유수율분석보고서ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.월유수율분석보고서ToolStripMenuItem.Text = "월유수율 분석 보고서";
            this.월유수율분석보고서ToolStripMenuItem.Click += new System.EventHandler(this.월유수율분석보고서ToolStripMenuItem_Click);
            // 
            // 유수율상호분석ToolStripMenuItem
            // 
            this.유수율상호분석ToolStripMenuItem.Name = "유수율상호분석ToolStripMenuItem";
            this.유수율상호분석ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.유수율상호분석ToolStripMenuItem.Text = "유수율 상호 분석";
            this.유수율상호분석ToolStripMenuItem.Click += new System.EventHandler(this.유수율상호분석ToolStripMenuItem_Click);
            // 
            // 총괄수량수지분석ToolStripMenuItem
            // 
            this.총괄수량수지분석ToolStripMenuItem.Name = "총괄수량수지분석ToolStripMenuItem";
            this.총괄수량수지분석ToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.총괄수량수지분석ToolStripMenuItem.Text = "총괄수량수지 분석";
            this.총괄수량수지분석ToolStripMenuItem.Click += new System.EventHandler(this.총괄수량수지분석ToolStripMenuItem_Click);
            // 
            // 유수율제고사업추진관리ToolStripMenuItem
            // 
            this.유수율제고사업추진관리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.계량기교체관리ToolStripMenuItem,
            this.누수지점관리ToolStripMenuItem,
            this.관개대체관리ToolStripMenuItem,
            this.단수작업관리ToolStripMenuItem,
            this.사업추진효과분석ToolStripMenuItem});
            this.유수율제고사업추진관리ToolStripMenuItem.Name = "유수율제고사업추진관리ToolStripMenuItem";
            this.유수율제고사업추진관리ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.유수율제고사업추진관리ToolStripMenuItem.Text = "유수율제고 사업추진관리";
            // 
            // 계량기교체관리ToolStripMenuItem
            // 
            this.계량기교체관리ToolStripMenuItem.Name = "계량기교체관리ToolStripMenuItem";
            this.계량기교체관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.계량기교체관리ToolStripMenuItem.Text = "계량기 교체 관리";
            this.계량기교체관리ToolStripMenuItem.Click += new System.EventHandler(this.계량기교체관리ToolStripMenuItem_Click);
            // 
            // 누수지점관리ToolStripMenuItem
            // 
            this.누수지점관리ToolStripMenuItem.Name = "누수지점관리ToolStripMenuItem";
            this.누수지점관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.누수지점관리ToolStripMenuItem.Text = "누수지점 관리";
            this.누수지점관리ToolStripMenuItem.Click += new System.EventHandler(this.누수지점관리ToolStripMenuItem_Click);
            // 
            // 관개대체관리ToolStripMenuItem
            // 
            this.관개대체관리ToolStripMenuItem.Name = "관개대체관리ToolStripMenuItem";
            this.관개대체관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.관개대체관리ToolStripMenuItem.Text = "관개대체 관리";
            this.관개대체관리ToolStripMenuItem.Click += new System.EventHandler(this.관개대체관리ToolStripMenuItem_Click);
            // 
            // 단수작업관리ToolStripMenuItem
            // 
            this.단수작업관리ToolStripMenuItem.Name = "단수작업관리ToolStripMenuItem";
            this.단수작업관리ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.단수작업관리ToolStripMenuItem.Text = "단수작업 관리";
            this.단수작업관리ToolStripMenuItem.Click += new System.EventHandler(this.기타단수작업관리ToolStripMenuItem_Click);
            // 
            // 사업추진효과분석ToolStripMenuItem
            // 
            this.사업추진효과분석ToolStripMenuItem.Name = "사업추진효과분석ToolStripMenuItem";
            this.사업추진효과분석ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.사업추진효과분석ToolStripMenuItem.Text = "사업추진 효과 분석";
            this.사업추진효과분석ToolStripMenuItem.Click += new System.EventHandler(this.사업추진효과분석toolStripMenuItem_Click);
            // 
            // 블록별일반현황정보조회ToolStripMenuItem
            // 
            this.블록별일반현황정보조회ToolStripMenuItem.Name = "블록별일반현황정보조회ToolStripMenuItem";
            this.블록별일반현황정보조회ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.블록별일반현황정보조회ToolStripMenuItem.Text = "블록별 일반현황 정보 조회";
            this.블록별일반현황정보조회ToolStripMenuItem.Click += new System.EventHandler(this.일반현황관리toolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(215, 6);
            // 
            // 블록기본설정ToolStripMenuItem
            // 
            this.블록기본설정ToolStripMenuItem.Name = "블록기본설정ToolStripMenuItem";
            this.블록기본설정ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.블록기본설정ToolStripMenuItem.Text = "블록별 수량관리 기본 설정";
            this.블록기본설정ToolStripMenuItem.Click += new System.EventHandler(this.블록기본설정ToolStripMenuItem_Click);
            // 
            // frmMainMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 583);
            this.Name = "frmMainMap";
            this.Text = "frmMainMap";
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaLeft, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaRight, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaBottom, 0);
            this.Controls.SetChildIndex(this._frmMapUnpinnedTabAreaTop, 0);
            this.Controls.SetChildIndex(this.windowDockingArea1, 0);
            this.Controls.SetChildIndex(this.spcContents, 0);
            this.Controls.SetChildIndex(this._frmMapAutoHideControl, 0);
            this.spcContents.Panel1.ResumeLayout(false);
            this.spcContents.Panel2.ResumeLayout(false);
            this.spcContents.Panel2.PerformLayout();
            this.spcContents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axIndexMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axToolbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DockManagerCommand)).EndInit();
            this._frmMapAutoHideControl.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.tabTOC.ResumeLayout(false);
            this.spcTOC.Panel2.ResumeLayout(false);
            this.spcTOC.ResumeLayout(false);
            this.tabPageTOC.ResumeLayout(false);
            this.spcIndexMap.Panel1.ResumeLayout(false);
            this.spcIndexMap.Panel2.ResumeLayout(false);
            this.spcIndexMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axTOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsLayer)).EndInit();
            this.contextMenuStripPopup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStripPopup;
        private System.Windows.Forms.ToolStripMenuItem 누수감시toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 블록기본설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 누수감시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 자동누수감시결과ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 공급량야간최소유량분석조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 누수량산정결과조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 야간최소유량산정방식별조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유수율분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 블록별공급량수압트랜드조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유수율현황조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 월유수율분석보고서ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유수율상호분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 총괄수량수지분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유수율제고사업추진관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 계량기교체관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 누수지점관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 관개대체관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 단수작업관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 사업추진효과분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 블록별일반현황정보조회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 집중관리우선순위조회ToolStripMenuItem;
    }
}