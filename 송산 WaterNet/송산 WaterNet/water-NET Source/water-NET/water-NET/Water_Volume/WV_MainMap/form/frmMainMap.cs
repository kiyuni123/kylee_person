﻿using System;
using System.Windows.Forms;
using WaterNet.WaterAOCore;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Controls;
using WaterNet.WV_Common.work;
using System.Collections;
using System.Data;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.form;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_LeakageManage.work;
using WaterNet.WV_Common.util;
using ESRI.ArcGIS.Geometry;

namespace WaterNet.WV_MainMap.form
{
    /// <summary>
    /// 메인지도의 화면이며 수량관리에서 공통적으로 사용된다.
    /// 공통적으로 사용될 지도관련 기능을 구현하며 각 업무화면은 해당객체의 인스턴스를 가지고 기능을 제어한다.
    /// 각 업무폼에서의 인스턴스이름은 MainMap이다.
    /// </summary>
    public partial class frmMainMap : WaterNet.WaterAOCore.frmMap, WaterNetCore.IForminterface, WV_Common.interface1.IMapControlWV
    {
        private IMapControl3 imapControl3 = null;
        private ILayer largeBlock = null;
        private ILayer middleBlock = null;
        private ILayer smallBlock = null;

        public frmMainMap()
        {
            InitializeComponent();
            InitializeSetting();
        }

        public IMapControl3 IMapControl3
        {
            get
            {
                return this.imapControl3;
            }
        }

        public AxMapControl AxMap
        {
            get
            {
                return base.axMap;
            }
        }

        public ToolStripButton ToolActionCommand
        {
            get
            {
                return base.toolActionCommand;
            }
        }

        /// <summary>
        /// Map의 포커스를 특정 블록으로 변경시켜준다.
        /// </summary>
        /// <param name="blockNode"></param>
        public void MoveToBlock(Hashtable parameter)
        {
            if (parameter != null)
            {
                string layerName = null;
                string strWhere = "FTR_IDN = '" + parameter["FTR_IDN"].ToString()+"'";

                switch (parameter["FTR_CODE"].ToString())
                {
                    case "BZ001":
                        layerName = "대블록";
                        break;
                    case "BZ002":
                        layerName = "중블록";
                        break;
                    case "BZ003":
                        layerName = "소블록";
                        break;
                }

                this.MoveFocusAt(layerName, strWhere);
            }
        }

        public override void Open()
        {
            base.Open();
            this.MapObjectSetting();
        }

        protected override void InitializeSetting()
        {
            base.InitializeSetting();
            this.InitializeEvent();
        }

        /// <summary>
        /// 이벤트 초기 설정
        /// </summary>
        private void InitializeEvent()
        {
            base.tvBlock.AfterSelect += new TreeViewEventHandler(tvBlockAfterSelect_EventHandler);
            base.axMap.OnMouseDown += new IMapControlEvents2_Ax_OnMouseDownEventHandler(axMapOnMouseDown_EventHandler);

            this.Load += new EventHandler(frmMainMap_Load);
        }

        private void frmMainMap_Load(object sender, EventArgs e)
        {
            //초기 전입시 누수감시 전환.
            this.누수감시toolStripMenuItem1.Checked = true;
            this.누수감시toolStripMenuItem1_Click(this.누수감시toolStripMenuItem1, new EventArgs());

            ILayer layer = ArcManager.GetShapeLayer(VariableManager.m_Pipegraphic, "누수지점");

            if (layer != null)
            {
                ArcManager.SetDefaultRenderer(axMap.ActiveView.FocusMap, (IFeatureLayer)layer);
                layer.Visible = true;
                base.axMap.AddLayer(layer);
            }
        }

        public void LeakageMonitering()
        {
            if (!this.누수감시toolStripMenuItem1.Checked)
            {
                this.누수감시toolStripMenuItem1.Checked = true;
                this.누수감시toolStripMenuItem1_Click(this.누수감시toolStripMenuItem1, new EventArgs());
            }

            for (int i = 0; i < base.axToolbar.CommandPool.Count; i++)
            {
                if (base.axToolbar.GetItem(i).Command.ToString() == "WaterNet.WaterFullExtent.CmdFullExtent")
                {
                    base.axToolbar.GetItem(i).Command.OnClick();
                }
            }
        }

        /// <summary>
        /// 마우스 우클릭시 컨텍스트 메뉴 활성화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void axMapOnMouseDown_EventHandler(object sender, IMapControlEvents2_OnMouseDownEvent e)
        {
            //1 == left
            //2 == right
            if (e.button == 2)
            {
                this.SelectedFeatureByBlock(e.mapX, e.mapY);
                System.Drawing.Point mousePoint = new System.Drawing.Point(e.x, e.y);
                this.contextMenuStripPopup.Show(axMap, mousePoint);
            }
        }

        private string selectedBlockKind = null;
        private string selectedBlockCode = null;
        private ESRI.ArcGIS.Geodatabase.IFeature selectedBlockFeature = null;

        /// <summary>
        /// 블록선택
        /// </summary>
        /// <param name="parameter"></param>
        public void SelectedFeatureByBlock(Hashtable parameter)
        {
            if (parameter != null)
            {
                ILayer layer = null;
                string strWhere = "FTR_IDN = '" + parameter["FTR_IDN"].ToString() + "'";

                switch (parameter["FTR_CODE"].ToString())
                {
                    case "BZ001":
                        layer = this.largeBlock;
                        break;
                    case "BZ002":
                        layer = this.middleBlock;
                        break;
                    case "BZ003":
                        layer = this.smallBlock;
                        break;
                }

                IFeatureSelection selection = (IFeatureSelection)layer;

                if (selection == null)
                {
                    return;
                }

                selection.Clear();
                selection.Add(ArcManager.GetFeature(layer, strWhere));
                this.axMap.Refresh();
            }
        }

        /// <summary>
        /// 블록선택
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void SelectedFeatureByBlock(double x, double y)
        {
            bool isToolbar = false;

            for (int i = 0; i < this.axToolbar.CommandPool.Count; i++)
            {
                if (this.axToolbar.CommandPool.get_Command(i).Checked)
                {
                    isToolbar = true;
                    break;
                }
            }
            if (isToolbar)
            {
                return;
            }

            //클릭 위치 정보
            ESRI.ArcGIS.Geometry.IPoint pPoint = new ESRI.ArcGIS.Geometry.PointClass();
            pPoint.X = x;
            pPoint.Y = y;
            pPoint.Z = 0;

            //Hashtable lInfo =
            //    ArcManager.GetSpatialFilterResultToFeatureHashtable(
            //    ((IFeatureLayer)this.largeBlock).FeatureClass,
            //    "FTR_IDN",
            //    (ESRI.ArcGIS.Geometry.IGeometry)pPoint,
            //    ESRI.ArcGIS.Geodatabase.esriSpatialRelEnum.esriSpatialRelIntersects);

            Hashtable mInfo =
                ArcManager.GetSpatialFilterResultToFeatureHashtable(
                ((IFeatureLayer)this.middleBlock).FeatureClass,
                "FTR_IDN",
                (ESRI.ArcGIS.Geometry.IGeometry)pPoint,
                ESRI.ArcGIS.Geodatabase.esriSpatialRelEnum.esriSpatialRelIntersects);

            Hashtable sInfo =
                ArcManager.GetSpatialFilterResultToFeatureHashtable(
                ((IFeatureLayer)this.smallBlock).FeatureClass,
                "FTR_IDN",
                (ESRI.ArcGIS.Geometry.IGeometry)pPoint,
                ESRI.ArcGIS.Geodatabase.esriSpatialRelEnum.esriSpatialRelIntersects);

            //현재축척
            double currentScale = this.imapControl3.MapScale;

            if (currentScale <= this.smallBlock.MinimumScale)
            {
                this.selectedBlockKind = string.Empty;

                //소블록 중블록 두가지 경우 체크,
                if (sInfo.Count > 0)
                {
                    foreach (string key in sInfo.Keys)
                    {
                        ESRI.ArcGIS.Geodatabase.IFeature block = sInfo[key] as ESRI.ArcGIS.Geodatabase.IFeature;

                        IFeatureSelection selection = (IFeatureSelection)this.smallBlock;
                        selection.Clear();
                        selection.Add(block);

                        this.selectedBlockFeature = block;

                        this.selectedBlockKind = "BZ003";
                        this.selectedBlockCode = key;

                        this.axMap.Refresh();
                    }
                }
                else if (mInfo.Count > 0)
                {
                    foreach (string key in mInfo.Keys)
                    {
                        ESRI.ArcGIS.Geodatabase.IFeature block = mInfo[key] as ESRI.ArcGIS.Geodatabase.IFeature;

                        IFeatureSelection selection = (IFeatureSelection)this.middleBlock;
                        selection.Clear();
                        selection.Add(block);

                        this.selectedBlockFeature = block;

                        this.selectedBlockKind = "BZ002";
                        this.selectedBlockCode = key;

                        this.axMap.Refresh();
                    }
                }
            }
            else
            {
                //중블록 체크
                if (mInfo.Count > 0)
                {
                    foreach (string key in mInfo.Keys)
                    {
                        ESRI.ArcGIS.Geodatabase.IFeature block = mInfo[key] as ESRI.ArcGIS.Geodatabase.IFeature;

                        IFeatureSelection selection = (IFeatureSelection)this.middleBlock;
                        selection.Clear();
                        selection.Add(block);

                        this.selectedBlockFeature = block;

                        this.selectedBlockKind = "BZ002";
                        this.selectedBlockCode = key;

                        this.axMap.Refresh();
                    }
                }
            //    //대블록 체크
            //    else if (lInfo.Count > 0)
            //    {
            //        foreach (string key in lInfo.Keys)
            //        {
            //            ESRI.ArcGIS.Geodatabase.IFeature block = lInfo[key] as ESRI.ArcGIS.Geodatabase.IFeature;

            //            IFeatureSelection selection = (IFeatureSelection)this.largeBlock;
            //            selection.Clear();
            //            selection.Add(block);

            //            this.selectedBlockFeature = block;

            //            this.selectedBlockKind = "BZ001";
            //            this.selectedBlockCode = key;

            //            this.axMap.Refresh();
            //        }
            //    }
            }
        }

        /// <summary>
        /// 좌측 트리 블럭 클릭시 이벤트 핸들러
        /// </summary>
        /// <param name="sender">트리 블럭</param>
        /// <param name="e">이벤트 인자</param>
        private void tvBlockAfterSelect_EventHandler(object sender, TreeViewEventArgs e)
        {
            if (e.Node == null)
            {
                return;
            }

            switch (e.Node.Name)
            {
                case "BZ002":
                    MapScale(100001);
                    break;
                case "BZ003":
                    MapScale(10000);
                    break;
            }
        }

        /// <summary>
        /// 축척변경
        /// </summary>
        /// <param name="scale">축척</param>
        private void MapScale(int scale)
        {
            base.axMap.MapScale = scale;
        }

        /// <summary>
        /// MapObjectSetting - 맵 제어에 필요한 객체 설정
        /// </summary>
        private void MapObjectSetting()
        {
            this.smallBlock = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "소블록");
            this.middleBlock = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "중블록");
            this.largeBlock = ArcManager.GetMapLayer((IMapControl3)axMap.Object, "대블록");
            this.imapControl3 = (IMapControl3)axMap.Object;
        }

        #region Implementation of ICollection IForminterface
        /// <summary>
        /// FormID : 탭에 보여지는 이름
        /// </summary>
        public string FormID
        {
            //WaterNet 탭에 보여질 이름
            get { return "수량관리"; }
        }
        /// <summary>
        /// FormKey : 현재 프로젝트 이름
        /// </summary>
        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion

        /// <summary>
        /// 레이어에서 특정 퓨처를 찾아 포커스를 이동시켜 준다. 
        /// </summary>
        /// <param name="layerName">레이어명</param>
        /// <param name="whereCase">조건</param>
        public void MoveFocusAt(string layerName, string whereCase)
        {
            ILayer layer = ArcManager.GetMapLayer(imapControl3, layerName);

            if (layer != null)
            {
                ESRI.ArcGIS.Geodatabase.IFeature feature = ArcManager.GetFeature(layer, whereCase);

                if (feature != null)
                {
                    axMap.Extent = feature.Shape.Envelope;
                    ArcManager.MoveCenterAt(this.imapControl3, feature);

                    if (layerName == "중블록")
                    {
                        this.MapScale(100001);
                    }
                }
            }
        }


        /// <summary>
        /// 레이어에서 특정 퓨처를 찾아 포커스를 이동시켜 준다. 
        /// </summary>
        /// <param name="layerName">레이어명</param>
        /// <param name="whereCase">조건</param>
        public void MoveFocusAt(string layerName, string whereCase, int scale)
        {
            ILayer layer = ArcManager.GetMapLayer(imapControl3, layerName);

            if (layer != null)
            {
                ESRI.ArcGIS.Geodatabase.IFeature feature = ArcManager.GetFeature(layer, whereCase);

                if (feature != null)
                {
                    ArcManager.MoveCenterAt(this.imapControl3, feature);
                    this.MapScale(scale);
                    Application.DoEvents();
                    ArcManager.FlashShape(axMap.ActiveView, (IGeometry)feature.Shape, 8, 100);
                }
                if (feature == null)
                {
                    MessageBox.Show("GIS에 선택한 객체가 존재하지 않습니다.");
                }
            }
        }

        /// <summary>
        /// 소블록의 누수감시 화면 전환
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 누수감시toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            if (item.Checked)
            {
                Hashtable parameter = new Hashtable();
                parameter["STARTDATE"] = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");

                this.SetLeakageMoniteringRenderer(parameter);

                this.middleBlock.Visible = false;
                this.smallBlock.MinimumScale = 999999;
                this.smallBlock.MaximumScale = 1;
            }
            else if (!item.Checked)
            {
                this.middleBlock.Visible = true;
                this.smallBlock.MinimumScale = 100000;
                this.smallBlock.MaximumScale = 1;

                ArcManager.SetDefaultRenderer(this.axMap.Map, (IFeatureLayer)this.smallBlock);
            }

            this.axMap.Refresh();
        }

        /// <summary>
        /// 현재 선택된 블럭을 업무화면의 검색조건과 일치시킨다.
        /// </summary>
        /// <param name="searchBox"></param>
        private void SetBlockBySearchBox(SearchBox searchBox)
        {
            if (this.selectedBlockFeature == null)
            {
                return;
            }

            Hashtable parameter = new Hashtable();
            parameter["FTR_CODE"] = ArcManager.GetValue(this.selectedBlockFeature, "FTR_CDE").ToString();
            parameter["FTR_IDN"] = ArcManager.GetValue(this.selectedBlockFeature, "FTR_IDN").ToString();

            //블럭정보,
            DataSet blockInfo = BlockWork.GetInstance().SelectBlockInfoByBlockCode(parameter, "RESULT");

            //값이없는경우 그냥 넘긴다.
            if (blockInfo.Tables.Count == 0)
            {
                return;
            }

            foreach (DataRow row in blockInfo.Tables["RESULT"].Rows)
            {
                if (row["LBLOCK"] != DBNull.Value)
                {
                    searchBox.SelectBlockItem(LOCATION_TYPE.LARGE_BLOCK, row["LBLOCK"].ToString());
                }

                if (row["MBLOCK"] != DBNull.Value)
                {
                    searchBox.SelectBlockItem(LOCATION_TYPE.MIDDLE_BLOCK, row["MBLOCK"].ToString());
                }

                if (row["SBLOCK"] != DBNull.Value)
                {
                    searchBox.SelectBlockItem(LOCATION_TYPE.SMALL_BLOCK, row["SBLOCK"].ToString());
                }
            }
        }

        /// <summary>
        /// 누수감시 결과를 화면에 표시한다.
        /// </summary>
        public void SetLeakageMoniteringRenderer(Hashtable parameter)
        {
            //Hashtable parameter = new Hashtable();
            
            ////감시결과 기준일자 설정
            //parameter["STARTDATE"] = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");

            if ( this.contextMenuStripPopup.Items.Count == 0 || !((ToolStripMenuItem)this.contextMenuStripPopup.Items[0]).Checked)
            {
                return;
            }

            DataTable dataTable = LeakageManageWork.GetInstance().SelectLeakageResult(parameter);

            if (dataTable == null || dataTable.Rows.Count == 0)
            {
                //MessageBox.Show("누수감시결과가 존재하지 않습니다.");
                return;
            }

            foreach (DataRow dataRow in dataTable.Rows)
            {
                IColor pColor = null;

                //데이터오류
                //if (dataRow["MONITOR_RESULT"].ToString() == "N" && dataRow["MONITOR_ALARM"].ToString() == "True")
                if (dataRow["MONITOR_RESULT"].ToString() == "N")
                {
                    pColor = WaterAOCore.ArcManager.GetColor(255, 165, 0);
                }
                //누수
                else if (dataRow["MONITOR_RESULT"].ToString() == "T" && dataRow["MONITOR_ALARM"].ToString() == "True")
                {
                    pColor = WaterAOCore.ArcManager.GetColor(255, 0, 0);
                }
                //확인필요
                else if (dataRow["MONITOR_RESULT"].ToString() == "A" && dataRow["MONITOR_ALARM"].ToString() == "True")
                {
                    pColor = WaterAOCore.ArcManager.GetColor(238, 238, 0);
                }
                //정상
                else// if (dataRow["MONITOR_RESULT"].ToString() == "F")
                {
                    pColor = WaterAOCore.ArcManager.GetColor(30, 144, 255);
                }

                IColor pOutColor = WaterAOCore.ArcManager.GetColor(100, 100, 200);
                ISymbol pLineSymbol = null;

                if (((IGeoFeatureLayer)smallBlock).Renderer is IUniqueValueRenderer)
                {
                    IUniqueValueRenderer pUniqueRenderer = ((IGeoFeatureLayer)smallBlock).Renderer as IUniqueValueRenderer;

                    pLineSymbol = WaterAOCore.ArcManager.MakeSimpleLineSymbol(esriSimpleLineStyle.esriSLSSolid, 0.01);
                    ISymbol oSymbol = ArcManager.MakeSimpleFillSymbol(esriSimpleFillStyle.esriSFSSolid, (ILineSymbol)pLineSymbol, pColor);

                    pUniqueRenderer.set_Symbol(dataRow["LOC_NAME"].ToString(), oSymbol);

                    ILayerEffects layereffect = smallBlock as ILayerEffects;
                    layereffect.Transparency = (short)60;
                }
                this.axMap.Refresh();
            }
        }

        //누수감시결과
        private void 누수감시결과toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_LeakageManage.form.frmLeakageResult)))
            {
                return;
            }

            WV_LeakageManage.form.frmLeakageResult oForm = new WaterNet.WV_LeakageManage.form.frmLeakageResult();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.SelectedBlock = this.selectedBlockCode;
            oForm.Show();
        }

        //블록운영분석
        private void 블록운영분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_OperationManage.form.frmMain)))
            {
                return;
            }

            WV_OperationManage.form.frmMain oForm = new WaterNet.WV_OperationManage.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
        }
        
        //블록별유량비교분석
        private void 블록별공급량야간최소유량분석toolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_BlockFlowAnalysis.form.BlockOperationAnalysis)))
            {
                return;
            }

            WV_BlockFlowAnalysis.form.BlockOperationAnalysis oForm = new WV_BlockFlowAnalysis.form.BlockOperationAnalysis();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //누수량관리
        private void 누수량관리toolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_LeakageManage.form.frmMain)))
            {
                return;
            }

            WV_LeakageManage.form.frmMain oForm = new WV_LeakageManage.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //야간최소유량필터링
        private void 야간최소유량필터링ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_BackgroundMinimumNightFlowFiltering.form.frmMain)))
            {
                return;
            }

            WV_BackgroundMinimumNightFlowFiltering.form.frmMain oForm = new WV_BackgroundMinimumNightFlowFiltering.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //사업추진효과분석
        private void 사업추진효과분석toolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_BusinessPromotionEffectAnalysis.form.frmMain)))
            {
                return;
            }

            WV_BusinessPromotionEffectAnalysis.form.frmMain oForm = new WV_BusinessPromotionEffectAnalysis.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //계량기교체관리
        private void 계량기교체관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_MeterChangeInformation.form.frmMain)))
            {
                return;
            }

            WV_MeterChangeInformation.form.frmMain oForm = new WV_MeterChangeInformation.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //누수지점관리
        private void 누수지점관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_LeakageRepairManage.form.frmMain)))
            {
                return;
            }

            WV_LeakageRepairManage.form.frmMain oForm = new WV_LeakageRepairManage.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //관개대체관리
        private void 관개대체관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_PipeChangeManage.form.frmMain)))
            {
                return;
            }

            WV_PipeChangeManage.form.frmMain oForm = new WV_PipeChangeManage.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //기타단수작업관리
        private void 기타단수작업관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_ShutOffManage.form.frmMain)))
            {
                return;
            }

            WV_ShutOffManage.form.frmMain oForm = new WV_ShutOffManage.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //유량패턴현황
        private void 유량패턴현황ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_FlowPatternStatus.form.frmMain)))
            {
                return;
            }

            WV_FlowPatternStatus.form.frmMain oForm = new WV_FlowPatternStatus.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //유수율현황
        private void 유수율현황ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_RevenueWaterRatioStatus.form.frmMain)))
            {
                return;
            }

            WV_RevenueWaterRatioStatus.form.frmMain oForm = new WV_RevenueWaterRatioStatus.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //유수율상호분석
        private void 유수율상호분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_RevenueWaterRatioAnalysis.form.frmMain)))
            {
                return;
            }

            WV_RevenueWaterRatioAnalysis.form.frmMain oForm = new WV_RevenueWaterRatioAnalysis.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //월유수율분석보고서
        private void 월유수율분석보고서ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_RevenueWaterRatioReport.form.frmMain)))
            {
                return;
            }

            WV_RevenueWaterRatioReport.form.frmMain oForm = new WV_RevenueWaterRatioReport.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //년간총괄수량수지분석
        private void 총괄수량수지분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_WaterBalanceAnalysis.form.frmMain)))
            {
                return;
            }

            WV_WaterBalanceAnalysis.form.frmMain oForm = new WV_WaterBalanceAnalysis.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //일반현황관리
        private void 일반현황관리toolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_GeneralStatus.form.frmMain)))
            {
                return;
            }

            WV_GeneralStatus.form.frmMain oForm = new WV_GeneralStatus.form.frmMain();
            oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
            this.SetBlockBySearchBox(oForm.SearchBox);
        }

        //블록기본설정
        private void 블록기본설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_BlockManage.form.frmMain)))
            {
                return;
            }

            WV_BlockManage.form.frmMain oForm = new WV_BlockManage.form.frmMain();
            //oForm.MainMap = this as IMapControlWV;
            oForm.Owner = this;
            oForm.Show();
        }
    }
}
