﻿using System;
using WaterNet.WV_Common.work;
using WaterNet.WV_MainMap.dao;
using System.Data;

namespace WaterNet.WV_MainMap.work
{
    public class MainMapWork : BaseWork
    {
        private static MainMapWork work = null;
        private MainMapDao dao = null;

        public static MainMapWork GetInstance()
        {
            if (work == null)
            {
                work = new MainMapWork();
            }
            return work;
        }

        private MainMapWork()
        {
            this.dao = MainMapDao.GetInstance();
        }

        public DataSet SelectSmallBlock()
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectSmallBlock(base.DataBaseManager, result, "RESULT");

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
