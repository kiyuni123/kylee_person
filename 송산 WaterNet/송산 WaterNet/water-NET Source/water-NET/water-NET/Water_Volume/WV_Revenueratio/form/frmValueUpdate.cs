﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using WaterNet.WV_Revenueratio.work;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace WaterNet.WV_Revenueratio.form
{
    public partial class frmValueUpdate : Form
    {
        private UltraGridManager gridManager = null;
        private string date = null;
        private string time = null;
        private string time2 = null;
        private string loc_code = null;
        private double value = 0.0;
        private bool totalChangeYn = false;
        private Hashtable parameters = null;

        public frmValueUpdate(string date,string loc_code)
        {
            InitializeComponent();
            Load += new EventHandler(frmValueUpdate_Load);
            this.date = date;
            this.loc_code = loc_code;
            
        }
        // yyyy-MM-dd HH:mm
        public string Time
        {
            get
            {
                return time;
            }
        }
        // yyyyMMddHH
        public string Time2
        {
            get
            {
                return time2;
            }
        }

        public double Value
        {
            get
            {
                return value;
            }
        }

        public bool TotalChangeYn
        {
            get
            {
                return totalChangeYn;
            }
        }

        private void frmValueUpdate_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();            
            this.InitializeColumn();
            this.InitializeEvent();
            this.searchBtn_Click(this.searchBtn, new EventArgs());

            // 시작시 처음열을 선택 (index 에러 방지)
            //this.ultraGrid1.Rows[0].Selected = true;
        }

        private void InitializeForm()
        {            
            this.ud_date.Value = date;
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);            
            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
        }

        #region 컬럼설정
        private void InitializeColumn()
        {
            UltraGridColumn column;
            UltraGridBand band = this.ultraGrid1.DisplayLayout.Bands[0];

            column = band.Columns.Add();
            column.Key = "LOC_CODE";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.Hidden = true;

            column = band.Columns.Add();
            column.Key = "TAGNAME";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.Hidden = true;

            column = band.Columns.Add();
            column.Key = "TIMESTAMP";
            column.Header.Caption = "일시";
            //column.CellActivation = Activation.AllowEdit;
            column.CellActivation = Activation.NoEdit;
            column.Format = "yyyy-MM-dd HH:mm";
            column.CellClickAction = CellClickAction.RowSelect;
            //column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.Width = 120;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;

            column = band.Columns.Add();
            column.Key = "VALUE";
            column.Header.Caption = "값";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.Format = "###,###,##0.00";
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
        }
        #endregion

        private void InitializeEvent()
        {
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            //this.ultraGrid1.ClickCellButton += new CellEventHandler(ultraGrid1_ClickCellButton);
        }

        // 2017-02-06 컬럼버튼 선택이 아닌 상단버튼 선택으로 변경 (ultraGrid1_ClickCellButton => changeBtn_Click, allChangeBtn_Click)
        private void ultraGrid1_ClickCellButton(object sender, CellEventArgs e)
        {
            time = ((DateTime)e.Cell.Value).ToString("yyyy-MM-dd HH:mm");
            value = Convert.ToDouble(e.Cell.Row.Cells["VALUE"].Value.ToString());
            
            this.DialogResult = System.Windows.Forms.DialogResult.OK;

            this.Close();
        }
        // 선택된 블록의 적산수정 (totalChangeYn = false)
        private void changeBtn_Click(object sender, EventArgs e)
        {
            if(this.ultraGrid1.Selected.Rows.Count == 0)
            {
                MessageBox.Show("해당 시간을 선택하여 주십시오.");
                return;
            }

            totalChangeYn = false;
            time = ((DateTime)this.ultraGrid1.Selected.Rows[0].Cells["TIMESTAMP"].Value).ToString("yyyy-MM-dd HH:mm");
            value = Convert.ToDouble(this.ultraGrid1.Selected.Rows[0].Cells["VALUE"].Value.ToString());           
            this.DialogResult = System.Windows.Forms.DialogResult.OK;

            this.Close();
        }
        // 모든 블록의 적산수정 (totalChangeYn = true)
        private void allChangeBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                MessageBox.Show("해당 시간을 선택하여 주십시오.");
                return;
            }

            totalChangeYn = true;
            time = ((DateTime)this.ultraGrid1.Selected.Rows[0].Cells["TIMESTAMP"].Value).ToString("yyyy-MM-dd HH:mm");
            time2 = ((DateTime)this.ultraGrid1.Selected.Rows[0].Cells["TIMESTAMP"].Value).ToString("yyyyMMddHHmm");
            this.DialogResult = System.Windows.Forms.DialogResult.OK;

            this.Close();
        }


        private void searchBtn_Click(object sender, EventArgs e)
        {
            if (parameters == null)
            {
                parameters = new Hashtable();
            }
            parameters["TIME"] = ((DateTime)this.ud_date.Value).ToString("yyyyMMddHH");
            parameters["LOC_CODE"] = loc_code;
            this.searchValues(parameters);
        }

        private void searchValues(Hashtable parameter)
        {
            this.ultraGrid1.DataSource = RevenueratioWork.GetInstance().SelectValues(parameters);
        }
    }
}

