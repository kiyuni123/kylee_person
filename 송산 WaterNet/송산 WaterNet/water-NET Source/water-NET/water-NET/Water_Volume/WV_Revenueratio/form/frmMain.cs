﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.util;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WV_Revenueratio.work;
using EMFrame.log;
using Infragistics.Win;
using Infragistics.Excel;
using System.Diagnostics;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace WaterNet.WV_Revenueratio.form
{
    public partial class frmMain : Form, WaterNet.WaterNetCore.IForminterface
    {
        private UltraGridManager gridManager = null;
        private Hashtable parameters = null;
        private ExcelManager excelManager = new ExcelManager();
        private object start_time = null;
        private object end_time = null;

        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeColumn();
            this.InitializeEvent();
            this.searchBtn_Click(this.searchBtn, new EventArgs());
        }

        #region 컬럼설정
        private void InitializeColumn()
        {
            UltraGridColumn column;
            UltraGridBand band = this.ultraGrid1.DisplayLayout.Bands[0];
            band.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            band.ColHeaderLines = 2;

            UltraGridGroup group = null;
            UltraGridGroup group2 = null;
            UltraGridGroup group3 = null;
            UltraGridGroup group4 = null;
            UltraGridGroup group5 = null;
            UltraGridGroup group6 = null;

            band.Groups.Clear();

            group = band.Groups.Add("BLOCK", "블록");
            group.Header.Appearance.TextHAlign = HAlign.Center;
            group.RowLayoutGroupInfo.OriginX = 0;
            group.RowLayoutGroupInfo.OriginY = 0;
            group.RowLayoutGroupInfo.SpanY = 1;
            group.RowLayoutGroupInfo.SpanX = 3;

            group2 = band.Groups.Add("ST_FRQ", "공급량 시점 적산");
            group2.Header.Appearance.TextHAlign = HAlign.Center;
            group2.RowLayoutGroupInfo.OriginX = 3;
            group2.RowLayoutGroupInfo.OriginY = 0;
            group2.RowLayoutGroupInfo.SpanY = 1;
            group2.RowLayoutGroupInfo.SpanX = 2;

            group3 = band.Groups.Add("ED_FRQ", "공급량 종점 적산");
            group3.Header.Appearance.TextHAlign = HAlign.Center;
            group3.RowLayoutGroupInfo.OriginX = 5;
            group3.RowLayoutGroupInfo.OriginY = 0;
            group3.RowLayoutGroupInfo.SpanY = 1;
            group3.RowLayoutGroupInfo.SpanX = 2;

            group4 = band.Groups.Add("SUPPLIED", "공급량(㎥)");
            group4.Header.Appearance.TextHAlign = HAlign.Center;
            group4.RowLayoutGroupInfo.OriginX = 7;
            group4.RowLayoutGroupInfo.OriginY = 0;
            group4.RowLayoutGroupInfo.SpanY = 1;
            group4.RowLayoutGroupInfo.SpanX = 2;

            group5 = band.Groups.Add("USED", "사용량(㎥)");
            group5.Header.Appearance.TextHAlign = HAlign.Center;
            group5.RowLayoutGroupInfo.OriginX = 9;
            group5.RowLayoutGroupInfo.OriginY = 0;
            group5.RowLayoutGroupInfo.SpanY = 1;
            group5.RowLayoutGroupInfo.SpanX = 2;

            group6 = band.Groups.Add("REVENUE", "유수율(%)");
            group6.Header.Appearance.TextHAlign = HAlign.Center;
            group6.RowLayoutGroupInfo.OriginX = 11;
            group6.RowLayoutGroupInfo.OriginY = 0;
            group6.RowLayoutGroupInfo.SpanY = 1;
            group6.RowLayoutGroupInfo.SpanX = 2;

            //ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;

            column = band.Columns.Add();
            column.Key = "LOC_CODE";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.Hidden = true;

            column = band.Columns.Add();
            column.Key = "LBLOCK";
            column.Header.Caption = "대블록";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Width = 120;
            column.RowLayoutColumnInfo.OriginX = 0;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "MBLOCK";
            column.Header.Caption = "중블록";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.RowLayoutColumnInfo.OriginX = 1;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "SBLOCK";
            column.Header.Caption = "소블록";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.RowLayoutColumnInfo.OriginX = 2;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "START_TIME";
            column.Header.Caption = "일시";
            column.CellActivation = Activation.AllowEdit;
            column.Format = "yyyy-MM-dd HH:mm";
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Width = 120;
            column.RowLayoutColumnInfo.OriginX = 3;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "START_VALUE";
            column.Header.Caption = "값";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Format = "###,###,##0.00";
            column.Width = 100;
            column.RowLayoutColumnInfo.OriginX = 4;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "END_TIME";
            column.Header.Caption = "일시";
            column.CellActivation = Activation.AllowEdit;
            column.Format = "yyyy-MM-dd HH:mm";
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Width = 120;
            column.RowLayoutColumnInfo.OriginX = 5;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "END_VALUE";
            column.Header.Caption = "값";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Format = "###,###,##0.00";
            column.Width = 100;
            column.RowLayoutColumnInfo.OriginX = 6;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "AUTO_SUPPLIED";
            column.Header.Caption = "자동";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Format = "###,###,##0.00";
            column.Width = 100;
            column.RowLayoutColumnInfo.OriginX = 7;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "MANUAL_SUPPLIED";
            column.Header.Caption = "수동(검침)";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Format = "###,###,##0.00";
            column.Width = 100;
            column.RowLayoutColumnInfo.OriginX = 8;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "USED";
            column.Header.Caption = "Water-INFOS";
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Format = "###,###,##0";
            column.RowLayoutColumnInfo.OriginX = 9;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "AUTO_REVENUE";
            column.Header.Caption = "자동";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Format = "###,###,##0.00";
            column.RowLayoutColumnInfo.OriginX = 11;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "MANUAL_REVENUE";
            column.Header.Caption = "수정";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Right;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Format = "###,###,##0.00";
            column.RowLayoutColumnInfo.OriginX = 12;
            column.RowLayoutColumnInfo.OriginY = 1;
            column.RowLayoutColumnInfo.SpanY = 1;
            column.RowLayoutColumnInfo.SpanX = 1;

            column = band.Columns.Add();
            column.Key = "TM";
            column.Header.Caption = "TM전송";
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            column.CellAppearance.TextHAlign = HAlign.Center;
            column.CellAppearance.TextVAlign = VAlign.Middle;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.Width = 50;
            column.RowLayoutColumnInfo.OriginX = 13;
            column.RowLayoutColumnInfo.OriginY = 0;
            column.RowLayoutColumnInfo.SpanY = 2;
            column.RowLayoutColumnInfo.SpanX = 1;
            
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].TabIndex = 0;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].TabIndex = 1;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].TabIndex = 2;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["START_TIME"].TabIndex = 3;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["START_VALUE"].TabIndex = 4;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["END_TIME"].TabIndex = 5;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["END_VALUE"].TabIndex = 6;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["AUTO_SUPPLIED"].TabIndex = 7;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MANUAL_SUPPLIED"].TabIndex = 8;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["USED"].TabIndex = 9;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["AUTO_REVENUE"].TabIndex = 10;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MANUAL_REVENUE"].TabIndex = 11;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["TM"].TabIndex = 12;
            

        }
        #endregion

        private void InitializeForm()
        {
            DateTime Now = DateTime.Now;

            ComboBoxUtils.SetMember(this.cb_R_Year, "CODE", "CODE_NAME");
            ComboBoxUtils.SetMember(this.cb_R_Mon, "CODE", "CODE_NAME");

            this.cb_R_Year.DataSource = DateUtils.GetYear(-10, "CODE", "CODE_NAME");
            this.cb_R_Mon.DataSource = DateUtils.GetMonth("CODE", "CODE_NAME");

            this.cb_R_Mon.SelectedValue = Now.AddMonths(-1).ToString("MM");
            if (cb_R_Mon.SelectedValue.ToString() == "12")
                this.cb_R_Year.SelectedValue = Now.Year - 1;
            this.ud_start.Value = DateTime.ParseExact(cb_R_Year.SelectedValue.ToString() + this.cb_R_Mon.SelectedValue.ToString() + "01", "yyyyMMdd", null);
            this.ud_end.Value = DateTime.ParseExact(cb_R_Year.SelectedValue.ToString() + this.cb_R_Mon.SelectedValue.ToString() + "01", "yyyyMMdd", null).AddMonths(1);
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid1.DisplayLayout.Override.DefaultRowHeight = 30;
        }

        private void InitializeEvent()
        {
            this.deleteBtn.Click += new EventHandler(deleteBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.cb_R_Year.SelectedValueChanged += new EventHandler(cb_R_Year_SelectedValueChanged);
            this.cb_R_Mon.SelectedValueChanged += new EventHandler(cb_R_Mon_SelectedValueChanged);
            this.ultraGrid1.ClickCellButton += new CellEventHandler(ultraGrid1_ClickCellButton);
            this.ultraGrid1.AfterCellUpdate += new CellEventHandler(ultraGrid1_AfterCellUpdate);
            this.ultraGrid1.DoubleClickCell += new DoubleClickCellEventHandler(ultraGrid1_DoubleClickCell);
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("초기화하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                Cursor currentCursor = this.Cursor;
                this.Cursor = Cursors.WaitCursor;

                try
                {
                    foreach (UltraGridRow row in this.ultraGrid1.Rows)
                    {
                        Hashtable parameter = new Hashtable();
                        parameter["LBLOCK"] = row.Cells["LBLOCK"].Value;
                        parameter["LOC_CODE"] = row.Cells["LOC_CODE"].Value;
                        parameter["YEAR_MON"] = cb_R_Year.SelectedValue.ToString() + cb_R_Mon.SelectedValue.ToString();

                        RevenueratioWork.GetInstance().DeleteRevenueRatio(parameter);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    this.Cursor = currentCursor;
                }
                this.searchBtn.PerformClick();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
        }

        private void ultraGrid1_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            string loc_code = string.Empty;

            switch (e.Cell.Column.Key)
            {
                case "LBLOCK":
                    if (e.Cell.Value != DBNull.Value)
                        loc_code = e.Cell.Row.Cells["LOC_CODE"].Value.ToString();
                    else
                        loc_code = string.Empty;
                    break;
                case "MBLOCK":
                    if (e.Cell.Value != DBNull.Value)
                        loc_code = e.Cell.Row.Cells["LOC_CODE"].Value.ToString();
                    else
                        loc_code = string.Empty;
                    break;
                case "SBLOCK":
                    if (e.Cell.Value != DBNull.Value)
                        loc_code = e.Cell.Row.Cells["LOC_CODE"].Value.ToString();
                    else
                        loc_code = string.Empty;
                    break;
            }
            if (string.IsNullOrEmpty(loc_code) || loc_code.IndexOf("SUM") > -1) return;
            if (!tagData(loc_code))
            {
                MessageBox.Show("해당블록의 태그가 존재하지않습니다.");
                return;
            }

            WV_Common.form.frmTagDescription form = new WaterNet.WV_Common.form.frmTagDescription(loc_code, "TD");
            form.Show();
        }

        private bool tagData(string loc_code)
        {
            bool result = false;
            object tagname = null;

            tagname = RevenueratioWork.GetInstance().SelectTagname(loc_code);

            if (tagname != null)
            {
                result = true;
            }

            return result;
        }

        private bool isUserCellUpdate = false;

        private void ultraGrid1_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (!isUserCellUpdate ||
                this.ultraGrid1.DataSource == null)
            {
                return;
            }
            string used = null;
            string m_supplied = null;
            string m_revenue = null;

            if (e.Cell.Row.Cells["USED"].Value != DBNull.Value)
                used = Convert.ToDouble(e.Cell.Row.Cells["USED"].Value).ToString("N2");

            if (e.Cell.Row.Cells["AUTO_SUPPLIED"].Value != DBNull.Value)
            {
                string a_supplied = Convert.ToDouble(e.Cell.Row.Cells["AUTO_SUPPLIED"].Value).ToString("N2");


                if (e.Cell.Row.Cells["MANUAL_SUPPLIED"].Value.ToString() == "")
                {
                    m_revenue = (Convert.ToDouble(used) / Convert.ToDouble(a_supplied) * 100).ToString("N2");
                }
                else
                {
                    m_supplied = Convert.ToDouble(e.Cell.Row.Cells["MANUAL_SUPPLIED"].Value).ToString("N2");
                    m_revenue = (Convert.ToDouble(used) / Convert.ToDouble(m_supplied) * 100).ToString("N2");
                    e.Cell.Row.Cells["MANUAL_SUPPLIED"].Appearance.BackColor = Color.PeachPuff;

                }
            }
            else if (e.Cell.Row.Cells["MANUAL_SUPPLIED"].Value != DBNull.Value)
            {
                m_supplied = Convert.ToDouble(e.Cell.Row.Cells["MANUAL_SUPPLIED"].Value).ToString("N2");
                m_revenue = (Convert.ToDouble(used) / Convert.ToDouble(m_supplied) * 100).ToString("N2");
                e.Cell.Row.Cells["MANUAL_SUPPLIED"].Appearance.BackColor = Color.PeachPuff;
            }
            else
            {
                return;
            }
            isUserCellUpdate = false;
            e.Cell.Row.Cells["MANUAL_REVENUE"].Value = m_revenue;
            e.Cell.Row.Cells["MANUAL_REVENUE"].Appearance.BackColor = Color.PeachPuff;
            e.Cell.Appearance.BackColor = Color.PeachPuff;
            isUserCellUpdate = true;

        }

        private void ultraGrid1_ClickCellButton(object sender, CellEventArgs e)
        {
            Form popupForm = null;

            switch (e.Cell.Column.Key)
            {
                case "START_TIME":
                    if (e.Cell.Row.Cells["START_TIME"].Value != DBNull.Value)
                        popupForm = new frmValueUpdate((string)e.Cell.Row.Cells["START_TIME"].Value, (string)e.Cell.Row.Cells["LOC_CODE"].Value);
                    else
                        popupForm = new frmValueUpdate(ud_start.Value.ToString(), (string)e.Cell.Row.Cells["LOC_CODE"].Value);

                    if (popupForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        // 2017-02-06 일괄수정 / 수정 If문 추가
                        if (((frmValueUpdate)popupForm).TotalChangeYn)
                        {
                            // 일괄수정일 경우 모든 날짜/값 업데이트
                            foreach(UltraGridRow row in ultraGrid1.Rows )
                            {
                                if (row.Cells["START_TIME"].Value == DBNull.Value)
                                    continue;

                                // 해당Row의 값 조회
                                parameters["STARTDATE"] = ((frmValueUpdate)popupForm).Time2;
                                parameters["LOC_CODE"] = row.Cells["LOC_CODE"].Value;

                                row.Cells["START_TIME"].Value = ((frmValueUpdate)popupForm).Time;
                                row.Cells["START_VALUE"].Value = Convert.ToDouble(RevenueratioWork.GetInstance().SelectValue(parameters));
                                row.Cells["START_VALUE"].Appearance.BackColor = Color.SkyBlue;
                                row.Cells["START_TIME"].Appearance.BackColor = Color.SkyBlue;

                                if (row.Cells["END_VALUE"].Value != DBNull.Value)
                                {
                                    row.Cells["AUTO_SUPPLIED"].Value = Convert.ToDouble(row.Cells["END_VALUE"].Value) - Convert.ToDouble(row.Cells["START_VALUE"].Value);
                                    row.Cells["AUTO_SUPPLIED"].Appearance.BackColor = Color.SkyBlue;
                                }
                            }
                        }
                        else
                        {
                            // 선택된 열의 값만 변경
                            e.Cell.Value = ((frmValueUpdate)popupForm).Time;
                            e.Cell.Row.Cells["START_VALUE"].Value = ((frmValueUpdate)popupForm).Value;
                            e.Cell.Row.Cells["START_VALUE"].Appearance.BackColor = Color.SkyBlue;
                            e.Cell.Row.Cells["START_TIME"].Appearance.BackColor = Color.SkyBlue;
                            if (e.Cell.Row.Cells["END_VALUE"].Value != DBNull.Value)
                            {
                                e.Cell.Row.Cells["AUTO_SUPPLIED"].Value = Convert.ToDouble(e.Cell.Row.Cells["END_VALUE"].Value) - Convert.ToDouble(e.Cell.Row.Cells["START_VALUE"].Value);
                                e.Cell.Row.Cells["AUTO_SUPPLIED"].Appearance.BackColor = Color.SkyBlue;
                            }
                        }
                    }
                    break;
                case "END_TIME":
                    if (e.Cell.Row.Cells["END_TIME"].Value != DBNull.Value)
                        popupForm = new frmValueUpdate((string)e.Cell.Row.Cells["END_TIME"].Value, (string)e.Cell.Row.Cells["LOC_CODE"].Value);
                    else
                        popupForm = new frmValueUpdate(ud_end.Value.ToString(), (string)e.Cell.Row.Cells["LOC_CODE"].Value);
                    if (popupForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        // 2017-02-06 일괄수정 / 수정 If문 추가
                        if (((frmValueUpdate)popupForm).TotalChangeYn)
                        {
                            // 일괄수정일 경우 모든 날짜/값 업데이트
                            foreach (UltraGridRow row in ultraGrid1.Rows)
                            {
                                if (row.Cells["END_TIME"].Value == DBNull.Value)
                                    continue;

                                // 해당Row의 값 조회
                                parameters["STARTDATE"] = ((frmValueUpdate)popupForm).Time2;
                                parameters["LOC_CODE"] = row.Cells["LOC_CODE"].Value;

                                row.Cells["END_TIME"].Value = ((frmValueUpdate)popupForm).Time;
                                row.Cells["END_VALUE"].Value = Convert.ToDouble(RevenueratioWork.GetInstance().SelectValue(parameters));
                                row.Cells["END_VALUE"].Appearance.BackColor = Color.SkyBlue;
                                row.Cells["END_TIME"].Appearance.BackColor = Color.SkyBlue;

                                if (row.Cells["START_VALUE"].Value != DBNull.Value)
                                {
                                    row.Cells["AUTO_SUPPLIED"].Value = Convert.ToDouble(row.Cells["END_VALUE"].Value) - Convert.ToDouble(row.Cells["START_VALUE"].Value);
                                    row.Cells["AUTO_SUPPLIED"].Appearance.BackColor = Color.SkyBlue;
                                }
                            }
                        }
                        else
                        {
                            // 선택된 열의 값만 변경
                            e.Cell.Value = ((frmValueUpdate)popupForm).Time;
                            e.Cell.Row.Cells["END_VALUE"].Value = ((frmValueUpdate)popupForm).Value;
                            e.Cell.Row.Cells["END_VALUE"].Appearance.BackColor = Color.SkyBlue;
                            e.Cell.Row.Cells["END_TIME"].Appearance.BackColor = Color.SkyBlue;
                            if (e.Cell.Row.Cells["START_VALUE"].Value != DBNull.Value)
                            {
                                e.Cell.Row.Cells["AUTO_SUPPLIED"].Value = Convert.ToDouble(e.Cell.Row.Cells["END_VALUE"].Value) - Convert.ToDouble(e.Cell.Row.Cells["START_VALUE"].Value);
                                e.Cell.Row.Cells["AUTO_SUPPLIED"].Appearance.BackColor = Color.SkyBlue;
                            }
                        }
                    }
                    break;
            }
            this.isUserCellUpdate = true;

        }


        private void cb_R_Mon_SelectedValueChanged(object sender, EventArgs e)
        {
            DateTime time = DateTime.Now;
            this.ud_start.Value = DateTime.ParseExact(cb_R_Year.SelectedValue.ToString() + this.cb_R_Mon.SelectedValue.ToString() + "0100", "yyyyMMddHH", null);
            time = (DateTime)this.ud_start.Value;
            this.ud_end.Value = time.AddMonths(1);
        }

        private void cb_R_Year_SelectedValueChanged(object sender, EventArgs e)
        {
            DateTime time = DateTime.Now;
            //if (manualData(parameters))
            //{
            //    start_time = RevenueratioWork.GetInstance().SelectStartTime(parameters);
            //    end_time = RevenueratioWork.GetInstance().SelectEndTime(parameters);
            //    this.ud_start.Value = DateTime.ParseExact(start_time.ToString(), "yyyyMMddHH", null);
            //    this.ud_end.Value = DateTime.ParseExact(end_time.ToString(), "yyyyMMddHH", null);
            //}
            //else
            //{
            this.ud_start.Value = DateTime.ParseExact(cb_R_Year.SelectedValue.ToString() + this.cb_R_Mon.SelectedValue.ToString() + "0100", "yyyyMMddHH", null);
            time = (DateTime)this.ud_start.Value;
            this.ud_end.Value = time.AddMonths(1);
            //}
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                Cursor currentCursor = this.Cursor;
                this.Cursor = Cursors.WaitCursor;

                try
                {
                    foreach (UltraGridRow row in this.ultraGrid1.Rows)
                    {
                        Hashtable parameter = new Hashtable();
                        parameter["LBLOCK"] = row.Cells["LBLOCK"].Value;
                        parameter["LOC_CODE"] = row.Cells["LOC_CODE"].Value;
                        parameter["YEAR_MON"] = cb_R_Year.SelectedValue.ToString() + cb_R_Mon.SelectedValue.ToString();
                        if (row.Cells["START_TIME"].Value != DBNull.Value)
                            parameter["START_TIME"] = Convert.ToDateTime(row.Cells["START_TIME"].Value).ToString("yyyyMMddHHmm");
                        else
                            parameter["START_TIME"] = Convert.ToDateTime(ud_start.Value).ToString("yyyyMMddHHmm");
                        parameter["START_VALUE"] = row.Cells["START_VALUE"].Value;
                        if (row.Cells["END_TIME"].Value != DBNull.Value)
                            parameter["END_TIME"] = Convert.ToDateTime(row.Cells["END_TIME"].Value).ToString("yyyyMMddHHmm");
                        else
                            parameter["END_TIME"] = Convert.ToDateTime(ud_end.Value).ToString("yyyyMMddHHmm");
                        parameter["END_VALUE"] = row.Cells["END_VALUE"].Value;
                        parameter["AUTO_SUPPLIED"] = row.Cells["AUTO_SUPPLIED"].Value;
                        parameter["MANUAL_SUPPLIED"] = row.Cells["MANUAL_SUPPLIED"].Value;
                        parameter["USED"] = row.Cells["USED"].Value;
                        parameter["M_START_TIME"] = ud_start.Value;
                        parameter["M_END_TIME"] = ud_end.Value;
                        RevenueratioWork.GetInstance().UpdateRevenueRatio(parameter);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    this.Cursor = currentCursor;
                }
                this.searchBtn.PerformClick();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            Cursor cursor = this.Cursor;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ExpoterExcel();
            }
            catch (Exception e1)
            {
                Logger.Error(e1);
            }
            finally
            {
                this.Cursor = cursor;
            }
        }

        private void ExpoterExcel()
        {
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.WorkbookBeforeClose += new Microsoft.Office.Interop.Excel.AppEvents_WorkbookBeforeCloseEventHandler(xlApp_WorkbookBeforeClose);

            if (xlApp == null)
            {
                return;
            }

            Microsoft.Office.Interop.Excel.Workbooks workbooks = xlApp.Workbooks;
            Microsoft.Office.Interop.Excel.Workbook workbook = workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];
            Microsoft.Office.Interop.Excel.PageSetup print = worksheet.PageSetup;   //프린트 설정
            Microsoft.Office.Interop.Excel.Range range = xlApp.get_Range("B2", "AL2");  //병합
            Microsoft.Office.Interop.Excel.Range rangeCell;

            DataSet dataSet = this.ultraGrid1.DataSource as DataSet;

            int columnCount = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count - 1;
            int rowCount = this.ultraGrid1.Rows.Count + 5;

            //틀 고정
            worksheet.Application.ActiveWindow.SplitRow = 5;
            worksheet.Application.ActiveWindow.FreezePanes = true;

            //타이틀 제목 병합
            rangeCell = worksheet.get_Range(worksheet.Cells[2, 2], worksheet.Cells[2, columnCount - 1]);
            rangeCell.Font.Size = 30;
            rangeCell.Font.Bold = true;

            rangeCell.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
            rangeCell.Font.Name = "돋음";
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데 정렬
            rangeCell.Merge(true);
            //구분 병합
            //rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, 1]);
            //rangeCell.ColumnWidth = 9;
            //rangeCell.Merge(false);

            // 전체 범위 설정
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 1], worksheet.Cells[rowCount, columnCount]);
            rangeCell.Font.Name = "돋움";
            rangeCell.RowHeight = 27;
            // 전체 범위안쪽 테두리 설정
            rangeCell.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            rangeCell.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데정렬

            // 전체 범위 바깥쪽 테두리설정
            rangeCell.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin,
                Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic);

            //데이터 범위설정
            //rangeCell = worksheet.get_Range(worksheet.Cells[7, 2], worksheet.Cells[rowCount , columnCount]);
            //rangeCell.NumberFormat = @"#,###,##0.00"; //숫자 범주
            //rangeCell.ColumnWidth = 6.44; //컬럼 넓이
            //rangeCell = worksheet.get_Range(worksheet.Cells[7, 1], worksheet.Cells[37, 1]);
            //rangeCell.NumberFormat = @"mm월dd일"; //날짜 범주

            //데이터 오른쪽정렬
            //rangeCell = worksheet.get_Range(worksheet.Cells[7, 2], worksheet.Cells[rowCount , columnCount - 1]);
            //rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlRight;

            //rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, columnCount]);
            //rangeCell.RowHeight = 13.5;   //구분 로우 높이
            //rangeCell = worksheet.get_Range(worksheet.Cells[4, 1], worksheet.Cells[4, columnCount]);
            //rangeCell.RowHeight = 0;

            //프린트 여백

            print.LeftMargin = 0;
            print.RightMargin = 0;
            print.TopMargin = 0;
            print.BottomMargin = 0;

            print.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;   //프린트 가로로 출력
            print.Zoom = AutoSize;    //프린트 비율
            print.CenterVertically = false;  //세로 자동맞춤
            print.CenterHorizontally = true;    //가로 자동맞춤

            // 타이틀 및 병합
            // 블록
            range.Cells[3, 0] = "블록";
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 1], worksheet.Cells[4, 3]);
            rangeCell.Merge(true);

            range.Cells[4, 0] = "대블록";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 1], worksheet.Cells[rowCount + 6, 1]);
            rangeCell.ColumnWidth = 13;            
            range.Cells[4, 1] = "중블록";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 2], worksheet.Cells[rowCount + 6, 2]);
            rangeCell.ColumnWidth = 13;
            range.Cells[4, 2] = "소블록";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 3], worksheet.Cells[rowCount + 6, 3]);
            rangeCell.ColumnWidth = 13;
            
            // 공급량 시점적산
            range.Cells[3, 3] = "공급량 시점적산";
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 4], worksheet.Cells[4, 5]);
            rangeCell.Merge(true);
            range.Cells[4, 3] = "일시";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 4], worksheet.Cells[rowCount + 6, 4]);
            rangeCell.ColumnWidth = 20;
            range.Cells[4, 4] = "값";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 5], worksheet.Cells[rowCount + 6, 5]);
            rangeCell.ColumnWidth = 15;

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 5], worksheet.Cells[rowCount + 6, 5]);
            rangeCell.NumberFormat = @"#,###,##0.00"; //숫자 범주
            
            // 공급량 종점적산
            range.Cells[3, 5] = "공급량 종점적산";
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 6], worksheet.Cells[4, 7]);
            rangeCell.Merge(true);
            range.Cells[4, 5] = "일시";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 6], worksheet.Cells[rowCount + 6, 6]);
            rangeCell.ColumnWidth = 20;
            range.Cells[4, 6] = "값";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 7], worksheet.Cells[rowCount + 6, 7]);
            rangeCell.ColumnWidth = 15;

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 7], worksheet.Cells[rowCount + 6, 7]);
            rangeCell.NumberFormat = @"#,###,##0.00"; //숫자 범주

            // 공급량
            range.Cells[3, 7] = "공급량";
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 8], worksheet.Cells[4, 9]);
            rangeCell.Merge(true);
            range.Cells[4, 7] = "자동";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 8], worksheet.Cells[rowCount + 6, 8]);
            rangeCell.ColumnWidth = 15;
            range.Cells[4, 8] = "수동(검침)";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 9], worksheet.Cells[rowCount + 6, 9]);
            rangeCell.ColumnWidth = 15;

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 7], worksheet.Cells[rowCount + 6, 7]);
            rangeCell.NumberFormat = @"#,###,##0.00"; //숫자 범주
            rangeCell = worksheet.get_Range(worksheet.Cells[5, 8], worksheet.Cells[rowCount + 6, 8]);
            rangeCell.NumberFormat = @"#,###,##0.00"; //숫자 범주

            // 사용량
            range.Cells[3, 9] = "사용량";
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 10], worksheet.Cells[5, 10]);
            rangeCell.Merge(false);
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 10], worksheet.Cells[rowCount + 6, 10]);
            rangeCell.ColumnWidth = 15;

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 9], worksheet.Cells[rowCount + 6, 9]);
            rangeCell.NumberFormat = @"#,###,##0.00"; //숫자 범주

            // 유수율
            range.Cells[3, 11] = "유수율";
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 11], worksheet.Cells[4, 12]);
            rangeCell.Merge(true);
            range.Cells[4, 10] = "자동";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 11], worksheet.Cells[rowCount + 6, 11]);
            rangeCell.ColumnWidth = 7;
            range.Cells[4, 11] = "수동";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 12], worksheet.Cells[rowCount + 6, 12]);
            rangeCell.ColumnWidth = 7;

            // TM
            range.Cells[3, 12] = "TM";
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 13], worksheet.Cells[rowCount + 6, 13]);
            rangeCell.ColumnWidth = 5;
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 13], worksheet.Cells[5, 13]);
            rangeCell.Merge(false);

            // 표헤더 색변경
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 1], worksheet.Cells[5, columnCount]);
            rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(204, 255, 204));

            // 제목,날짜
            range.Cells[2, 0] = "조회 날짜 " + ((DateTime)ud_start.Value).ToString("[yyyy/MM/dd]") + " ~ " + ((DateTime)ud_end.Value).ToString("[yyyy/MM/dd]");
            range.Cells[1, 1] = "월별 유수율 관리 조회";
            
            int row_index = 0;
            int column_index = 0;
            object columnValue = null;

            /*
            Infragistics.Excel.Workbook workbook = Infragistics.Excel.Workbook.Load(new MemoryStream(Properties.Resources.RevenueRatio));
            workbook.Worksheets[0].Rows[2].Cells[2].Value = cb_R_Year.SelectedValue.ToString() + "년" + this.cb_R_Mon.SelectedValue.ToString() + "월";
            */
            for (int i = 0; i < this.ultraGrid1.Rows.Count; i++)
            {
                worksheet.Cells[i+6, 1] = this.ultraGrid1.Rows[i].Cells["LBLOCK"].Value;
                worksheet.Cells[i+6, 2] = this.ultraGrid1.Rows[i].Cells["MBLOCK"].Value;
                worksheet.Cells[i+6, 3] = this.ultraGrid1.Rows[i].Cells["SBLOCK"].Value;
                worksheet.Cells[i+6, 4] = this.ultraGrid1.Rows[i].Cells["START_TIME"].Value;
                worksheet.Cells[i+6, 5] = this.ultraGrid1.Rows[i].Cells["START_VALUE"].Value;
                worksheet.Cells[i+6, 6] = this.ultraGrid1.Rows[i].Cells["END_TIME"].Value;
                worksheet.Cells[i+6, 7] = this.ultraGrid1.Rows[i].Cells["END_VALUE"].Value;
                worksheet.Cells[i+6, 8] = this.ultraGrid1.Rows[i].Cells["AUTO_SUPPLIED"].Value;
                worksheet.Cells[i+6, 9] = this.ultraGrid1.Rows[i].Cells["MANUAL_SUPPLIED"].Value;
                worksheet.Cells[i+6, 10] = this.ultraGrid1.Rows[i].Cells["USED"].Value;
                worksheet.Cells[i+6, 11] = this.ultraGrid1.Rows[i].Cells["AUTO_REVENUE"].Value;
                worksheet.Cells[i+6, 12] = this.ultraGrid1.Rows[i].Cells["MANUAL_REVENUE"].Value;
                worksheet.Cells[i+6, 13] = this.ultraGrid1.Rows[i].Cells["TM"].Value;
                /*
                if (this.ultraGrid1.Rows[i].Cells["START_VALUE"].Value != DBNull.Value)
                    workbook.Worksheets[0].Rows[6 + i].Cells[4].Value = Convert.ToDouble(this.ultraGrid1.Rows[i].Cells["START_VALUE"].Value);
                else workbook.Worksheets[0].Rows[6 + i].Cells[4].Value = null;

                workbook.Worksheets[0].Rows[6 + i].Cells[5].Value = this.ultraGrid1.Rows[i].Cells["END_TIME"].Value;

                if (this.ultraGrid1.Rows[i].Cells["END_VALUE"].Value != DBNull.Value)
                    workbook.Worksheets[0].Rows[6 + i].Cells[6].Value = Convert.ToDouble(this.ultraGrid1.Rows[i].Cells["END_VALUE"].Value);
                else workbook.Worksheets[0].Rows[6 + i].Cells[6].Value = null;
                if (this.ultraGrid1.Rows[i].Cells["AUTO_SUPPLIED"].Value != DBNull.Value)
                    workbook.Worksheets[0].Rows[6 + i].Cells[7].Value = Convert.ToDouble(this.ultraGrid1.Rows[i].Cells["AUTO_SUPPLIED"].Value);
                else workbook.Worksheets[0].Rows[6 + i].Cells[7].Value = null; ;
                if (this.ultraGrid1.Rows[i].Cells["MANUAL_SUPPLIED"].Value != DBNull.Value)
                    workbook.Worksheets[0].Rows[6 + i].Cells[8].Value = Convert.ToDouble(this.ultraGrid1.Rows[i].Cells["MANUAL_SUPPLIED"].Value);
                else workbook.Worksheets[0].Rows[6 + i].Cells[8].Value = null;
                if (this.ultraGrid1.Rows[i].Cells["USED"].Value != DBNull.Value)
                    workbook.Worksheets[0].Rows[6 + i].Cells[9].Value = Convert.ToDouble(this.ultraGrid1.Rows[i].Cells["USED"].Value);
                else workbook.Worksheets[0].Rows[6 + i].Cells[9].Value = null;
                if (this.ultraGrid1.Rows[i].Cells["AUTO_REVENUE"].Value != DBNull.Value)
                    workbook.Worksheets[0].Rows[6 + i].Cells[10].Value = Convert.ToDouble(this.ultraGrid1.Rows[i].Cells["AUTO_REVENUE"].Value);
                else workbook.Worksheets[0].Rows[6 + i].Cells[10].Value = null;
                if (this.ultraGrid1.Rows[i].Cells["MANUAL_REVENUE"].Value != DBNull.Value)
                    workbook.Worksheets[0].Rows[6 + i].Cells[11].Value = Convert.ToDouble(this.ultraGrid1.Rows[i].Cells["MANUAL_REVENUE"].Value);
                else workbook.Worksheets[0].Rows[6 + i].Cells[11].Value = null;
                 * */
            }

            xlApp.ScreenUpdating = true;
            xlApp.Visible = true;
            /*
            SaveFileDialog excelFileDialog = new SaveFileDialog();
            excelFileDialog.Filter = "EXCEL(*.xlsx)|*.xlsx";
            excelFileDialog.FilterIndex = 0;
            if (excelFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (excelFileDialog.FileName != string.Empty)
                    //workbook.SaveCopyAs(excelFileDialog.FileName);
                    workbook.SaveAs(excelFileDialog.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlExclusive, Type.Missing,Type.Missing,Type.Missing,Type.Missing,Type.Missing);

            }*/            
        }

        private void xlApp_WorkbookBeforeClose(Microsoft.Office.Interop.Excel.Workbook Wb, ref bool Cancel)
        {
            Process[] ExcelPros = Process.GetProcessesByName("EXCEL");

            for (int i = 0; i < ExcelPros.Length; i++)
            {
                Console.WriteLine(ExcelPros[i].MainWindowTitle);

                if (ExcelPros[i].MainWindowTitle == "")
                {
                    ExcelPros[i].Kill();
                }
            }
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (parameters == null)
                {
                    parameters = new Hashtable();
                }
                parameters["STARTDATE"] = ((DateTime)this.ud_start.Value).ToString("yyyyMMdd HH");
                parameters["ENDDATE"] = ((DateTime)this.ud_end.Value).ToString("yyyyMMdd HH");
                parameters["STYM"] = this.cb_R_Year.SelectedValue.ToString() + this.cb_R_Mon.SelectedValue.ToString();

                this.searchRevenueRatio(parameters);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void searchRevenueRatio(Hashtable parameters)
        {
            this.isUserCellUpdate = false;
            if (manualData(parameters))
            {
                this.start_time = RevenueratioWork.GetInstance().SelectStartTime(parameters);
                this.end_time = RevenueratioWork.GetInstance().SelectEndTime(parameters);
                this.ultraGrid1.DataSource = RevenueratioWork.GetInstance().SelectRevenueRatioEdit(parameters);
                this.SetGridColumns();
            }
            else
                this.ultraGrid1.DataSource = RevenueratioWork.GetInstance().SelectRevenueRatio(parameters);
            this.isUserCellUpdate = true;
        }

        private void SetGridColumns()
        {
            this.ud_start.Value = this.start_time;
            this.ud_end.Value = this.end_time;

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                //if (gridRow.Cells["START_TIME"].Value == DBNull.Value)
                //{

                //    gridRow.Cells["START_TIME"].Appearance.BackColor = Color.Red;
                //    gridRow.Cells["START_VALUE"].Appearance.BackColor = Color.Red;
                //    gridRow.Cells["START_TIME"].ToolTipText = "결측";
                //    gridRow.Cells["START_VALUE"].ToolTipText = "결측";
                //}

                //if (gridRow.Cells["END_TIME"].Value == DBNull.Value)
                //{
                //    gridRow.Cells["END_TIME"].Appearance.BackColor = Color.Red;
                //    gridRow.Cells["END_VALUE"].Appearance.BackColor = Color.Red;
                //    gridRow.Cells["END_TIME"].ToolTipText = "결측";
                //    gridRow.Cells["END_VALUE"].ToolTipText = "결측";

                //}           

                if (gridRow.Cells["MANUAL_SUPPLIED"].Value.ToString() != "")
                    gridRow.Cells["MANUAL_SUPPLIED"].Appearance.BackColor = Color.PeachPuff;
                if (gridRow.Cells["MANUAL_REVENUE"].Value.ToString() != "")
                    gridRow.Cells["MANUAL_REVENUE"].Appearance.BackColor = Color.PeachPuff;
                if (gridRow.Cells["START_TIME"].Value.ToString() != this.start_time.ToString())
                {
                    gridRow.Cells["START_TIME"].Appearance.BackColor = Color.SkyBlue;
                    gridRow.Cells["START_VALUE"].Appearance.BackColor = Color.SkyBlue;
                }
                if (gridRow.Cells["END_TIME"].Value.ToString() != this.end_time.ToString())
                {
                    gridRow.Cells["END_TIME"].Appearance.BackColor = Color.SkyBlue;
                    gridRow.Cells["END_VALUE"].Appearance.BackColor = Color.SkyBlue;
                }
            }

        }

        private bool manualData(Hashtable parameters)
        {
            bool result = false;
            object year_mon = null;

            year_mon = RevenueratioWork.GetInstance().SelectRevenueMaster(parameters);

            if (year_mon != null)
            {
                result = true;
            }

            return result;
        }

        public void SetRowClick(UltraGrid item, bool isClick)
        {
            if (isClick)
            {
                item.DisplayLayout.Override.ActiveRowAppearance.BackColor = SystemColors.Highlight;
                item.DisplayLayout.Override.ActiveRowAppearance.ForeColor = SystemColors.HighlightText;
            }

            if (!isClick)
            {
                item.DisplayLayout.Override.ActiveRowAppearance.BackColor = Color.Empty;
                item.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Empty;
            }
        }

        public void SetCellClick(UltraGrid item)
        {
            item.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;
            item.DisplayLayout.Override.SelectTypeCell = SelectType.Single;
            item.DisplayLayout.SelectionOverlayColor = Color.Empty;
            item.DisplayLayout.SelectionOverlayBorderColor = Color.Blue;
            item.DisplayLayout.Override.ActiveCellBorderThickness = 3;
            item.DisplayLayout.Override.ActiveCellAppearance.BorderColor = Color.Blue;
            item.DisplayLayout.Override.ActiveCellAppearance.BorderColor2 = Color.FromArgb(128, 128, 255);
        }

        private void ultraGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.V)
            {
                //선택된 셀이 없으면 동작하지 않는다.
                if (this.ultraGrid1.ActiveCell == null)
                {
                    return;
                }

                //클립보드의 엑셀 선택 영역을 가져온다..
                //엑셀이 아니라면 값은 null 이며 동작하지 않는다.
                string[,] range = WV_Common.excel.Clipboard.GetStringRange();
                if (range == null)
                {
                    return;
                }

                // 시작시간, 종료시간일 경우 EXCEL 붙여넣기를 하지 않는다.
                if (this.ultraGrid1.ActiveCell.Column.Key == "START_TIME" || this.ultraGrid1.ActiveCell.Column.Key == "END_TIME")
                {
                    return;
                }

                int start_row_idx = this.ultraGrid1.ActiveCell.Row.Index;
                int start_cell_idx = 0;
                int column_index = 0;
                Dictionary<int, string> columns = new Dictionary<int, string>();

                UltraGridColumn column = this.ultraGrid1.DisplayLayout.Bands[0].Columns[0];
                column = column.GetRelatedVisibleColumn(VisibleRelation.First);

                while (null != column)
                {
                    if (column != null)
                    {
                        columns[column_index] = column.Key;

                        if (this.ultraGrid1.ActiveCell.Column.Key == column.Key)
                        {
                            start_cell_idx = column_index;
                        }
                    }

                    column = column.GetRelatedVisibleColumn(VisibleRelation.Next);
                    column_index++;
                }

                int target_row_idx = 0;
                int target_cell_idx = 0;

                for (int i = 0; i < range.GetLength(0); i++)
                {
                    for (int j = 0; j < range.GetLength(1); j++)
                    {
                        double result = 0;
                        if (range[i, j] != null)
                        {
                            if (!Double.TryParse(range[i, j], out result))
                            {
                                MessageBox.Show("선택하신 영역내에 숫자가 아닌 값이 존재합니다.");
                                return;
                            }
                        }
                    }
                }

                this.ultraGrid1.PerformAction(UltraGridAction.ExitEditMode);

                for (int i = start_row_idx; i < this.ultraGrid1.Rows.Count; i++)
                {
                    target_cell_idx = 0;

                    if (target_row_idx > range.GetLength(0) - 1)
                    {
                        break;
                    }

                    for (int j = start_cell_idx; j < columns.Count; j++)
                    {
                        if (target_cell_idx > range.GetLength(1) - 1)
                        {
                            break;
                        }

                        if (this.ultraGrid1.Rows[i].Cells[columns[j]].Column.CellAppearance.BackColor == Color.Empty)
                        {
                            if (range[target_row_idx, target_cell_idx] == null)
                            {
                                target_cell_idx++;
                                continue;
                            }
                            else
                                this.ultraGrid1.Rows[i].Cells[columns[j]].Value = range[target_row_idx, target_cell_idx];
                        }

                        target_cell_idx++;
                    }

                    target_row_idx++;
                }
            }
        }
    }
}
