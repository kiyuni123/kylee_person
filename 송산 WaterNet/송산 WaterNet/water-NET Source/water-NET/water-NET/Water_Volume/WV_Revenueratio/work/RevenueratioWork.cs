﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.work;
using System.Data;
using System.Collections;
using WaterNet.WV_Revenueratio.dao;

namespace WaterNet.WV_Revenueratio.work
{
    public class RevenueratioWork : BaseWork
    {
        private static RevenueratioWork work = null;
        private RevenueratioDao dao = null;

        private RevenueratioWork()
        {
            dao = RevenueratioDao.GetInstance();
        }

        public static RevenueratioWork GetInstance()
        {
            if (work == null)
            {
                work = new RevenueratioWork();
            }
            return work;
        }

        //public DataSet SelectRevenueMaster(Hashtable parameter)
        //{
        //    DataSet result = null;
        //    try
        //    {
        //        ConnectionOpen();
        //        BeginTransaction();
        //        dao.SelectRevenueMaster(base.DataBaseManager, result, "result", parameter);
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        CloseTransaction();
        //        ConnectionClose();
        //    }
        //    return result;
        //}

        public object SelectRevenueMaster(Hashtable parameter)
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectRevenueMaster(base.DataBaseManager, parameter);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public object SelectStartTime(Hashtable parameter)
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectStartTime(base.DataBaseManager, parameter);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public object SelectEndTime(Hashtable parameter)
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectEndTime(base.DataBaseManager, parameter);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public object SelectTagname(string loc_code)
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectTagname(base.DataBaseManager, loc_code);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectRevenueRatio(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectRevenueRatio(base.DataBaseManager, result, "result", parameter);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectRevenueRatioEdit(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectRevenueRatioEdit(base.DataBaseManager, result, "result", parameter);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectValues(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectValues(base.DataBaseManager, result, "result", parameter);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public Object SelectValue(Hashtable parameter)
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();
                result = dao.SelectValue(base.DataBaseManager, parameter);

                if (result == null)
                {
                    result = 0;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public void UpdateRevenueRatio(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                if (parameter["LBLOCK"].ToString() != "")
                    dao.UpdateMaster(base.DataBaseManager, parameter);
                if (parameter["MANUAL_SUPPLIED"].ToString() != "")
                    dao.UpdateRevenueRatio(base.DataBaseManager, parameter);
                else
                    dao.UpdateRevenueRatio2(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void DeleteRevenueRatio(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.DeleteMaster(base.DataBaseManager, parameter);
                dao.DeleteRevenueRatio(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }
    }
}
