﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.dao;
using System.Data;
using System.Collections;
using WaterNet.WaterNetCore;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_Revenueratio.dao
{
    public class RevenueratioDao : BaseDao
    {
        private static RevenueratioDao dao = null;

        private RevenueratioDao() { }

        public static RevenueratioDao GetInstance()
        {
            if (dao == null)
            {
                dao = new RevenueratioDao();
            }
            return dao;
        }

        //public void SelectRevenueMaster(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        //{
        //    StringBuilder query = new StringBuilder();

        //    query.AppendLine("select   year_mon                                       ");
        //    query.AppendLine("         , to_char(start_time,'yyyyMMddHH') start_time  ");
        //    query.AppendLine("         , to_char(end_time,'yyyyMMddHH') end_time      ");
        //    query.AppendLine("  from   wv_revenue_master                              ");
        //    query.AppendLine(" where year_mon = :YEAR_MON                             ");

        //    IDataParameter[] parameters =  {
        //             new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
        //        };

        //    parameters[0].Value = parameter["STYM"];

        //    manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        //}

        public object SelectRevenueMaster(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select year_mon                          ");
            query.AppendLine("  from   wv_revenue_master               ");
            query.AppendLine(" where year_mon = :year_mon              ");

            IDataParameter[] parameters =  {
                     new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STYM"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectStartTime(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select to_char(start_time,'yyyy-mm-dd hh24:mi') START_TIME    ");
            query.AppendLine("  from   wv_revenue_master               ");
            query.AppendLine(" where year_mon = :year_mon              ");

            IDataParameter[] parameters =  {
                     new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STYM"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectEndTime(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select to_char(end_time,'yyyy-mm-dd hh24:mi') START_TIME  ");
            query.AppendLine("  from   wv_revenue_master               ");
            query.AppendLine(" where year_mon = :year_mon              ");

            IDataParameter[] parameters =  {
                     new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STYM"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectTagname(OracleDBManager manager, string loc_code)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select a.tagname from if_ihtags a, if_tag_gbn b   ");
            query.AppendLine("        where  a.tagname = b.tagname              ");
            query.AppendLine("        and    b.tag_gbn = 'TD'                   ");
            query.AppendLine("        and    a.loc_code = :LOC_CODE             ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = loc_code;

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public void SelectRevenueRatio(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select locdata.LOC_CODE	                                                                                                          ");
            query.AppendLine("      , locdata.lblock LBLOCK                                                                                                   ");
            query.AppendLine("      , locdata.mblock MBLOCK	                                                                                                  ");
            query.AppendLine("      , locdata.sblock SBLOCK     	                                                                                             ");
            query.AppendLine("      , to_char(locdata.timestamp1,'yyyy-mm-dd hh24:mi') START_TIME	                                                                ");
            query.AppendLine("      , round(locdata.value1,2)     START_VALUE	                                                                                                 ");
            query.AppendLine("      , to_char(locdata.timestamp2,'yyyy-mm-dd hh24:mi') END_TIME                                                                     ");
            query.AppendLine("      , round(locdata.value2,2)     END_VALUE	                                                                                                 ");
            query.AppendLine("      , round((locdata.value2-locdata.value1),2) AUTO_SUPPLIED	                                                                                     ");
            query.AppendLine("      , '' MANUAL_SUPPLIED	                                                                                                        ");
            query.AppendLine("      , case when l_used.use is not null then l_used.use                                                                              ");
            query.AppendLine("             when m_used.use is not null then m_used.use                                                                              ");
            query.AppendLine("             else s_used.use                                                                                                          ");
            query.AppendLine("             end as USED      	                                                                                                    ");
            query.AppendLine("      , case when l_used.use is not null then round((l_used.use/nullif((locdata.value2-locdata.value1),0) *100),2)                    ");
            query.AppendLine("             when m_used.use is not null then round((m_used.use/nullif((locdata.value2-locdata.value1),0)*100),2)                     ");
            query.AppendLine("             else round((s_used.use/nullif((locdata.value2-locdata.value1),0)*100),2)                                                 ");
            query.AppendLine("             end as AUTO_REVENUE                                                                                                      ");
            query.AppendLine("      , '' MANUAL_REVENUE	                                                                                                            ");
            query.AppendLine("      , decode(tag.tagname,null,null, 'TM') TM                                                                                                            ");
            query.AppendLine("      from	                                                                                                                        ");
            query.AppendLine("      (                                                                                                                               ");
            query.AppendLine("             select a.tagname, a.loc_code From if_ihtags a, if_tag_gbn b                                                                   ");
            query.AppendLine("             where a.tagname = b.tagname                                                                                              ");
            query.AppendLine("             and   b.tag_gbn ='TD'                                                                                                    ");
            query.AppendLine("             and loc_code is not null                                                                                                 ");
            query.AppendLine("      ) tag                                                                                                                           ");
            query.AppendLine("      ,                                                                                                                               ");
            query.AppendLine("      	  (select data.*, loc.* from                                                                                                ");
            query.AppendLine("             (select sd.tagname1, sd.timestamp1, sd.value1, sd.loc_code1, ed.tagname2, ed.timestamp2, ed.value2, ed.loc_code2 from    ");
            query.AppendLine("              (select a.tagname tagname1	                                                                                            ");
            query.AppendLine("        ,a.timestamp timestamp1	                                                                                                    ");
            query.AppendLine("        , a.value value1	                                                                                                 ");
            query.AppendLine("        , b.loc_code loc_code1 	                                                                                             ");
            query.AppendLine("        from 	                                                                                                             ");
            query.AppendLine("              if_gather_realtime a	                                                                                     ");
            query.AppendLine("              , if_ihtags b	                                                                                     ");
            query.AppendLine("              , if_tag_gbn c	                                                                                             ");
            query.AppendLine("        where a.tagname = b.tagname	                                                                             ");
            query.AppendLine("        and a.timestamp = to_date(:STARTDATE,'yyyymmddhh24')	                                                         ");
            query.AppendLine("        and c.tagname = b.tagname	                                                                                     ");
            query.AppendLine("        and b.loc_code is not null	                                                                                     ");
            query.AppendLine("        and b.use_yn = 'Y'	                                                                                     ");
            query.AppendLine("        and c.tag_gbn = 'TD') sd	                                                                                     ");
            query.AppendLine("        full outer join 	                                                                                                             ");
            query.AppendLine("        (select a.tagname	tagname2     ");
            query.AppendLine("        ,a.timestamp timestamp2	                                                                                                 ");
            query.AppendLine("        , a.value	value2                                                                                                     ");
            query.AppendLine("        , b.loc_code loc_code2	                                                                                             ");
            query.AppendLine("        from 	                                                                                                             ");
            query.AppendLine("              if_gather_realtime a	                                                                                     ");
            query.AppendLine("              , if_ihtags b	                                                                                     ");
            query.AppendLine("              , if_tag_gbn c	                                                                                             ");
            query.AppendLine("        where a.tagname = b.tagname	                                                                                     ");
            query.AppendLine("        and a.timestamp = to_date(:ENDDATE,'yyyymmddhh24')	                                                         ");
            query.AppendLine("        and c.tagname = b.tagname	                                                                                     ");
            query.AppendLine("        and b.loc_code is not null	                                                                                     ");
            query.AppendLine("        and b.use_yn = 'Y'	                                                                                     ");
            query.AppendLine("        and c.tag_gbn = 'TD') ed	        ");
            query.AppendLine("        on (sd.loc_code1 = ed.loc_code2)) data                                                                                    ");
            query.AppendLine("        full outer join	                                                                                                               ");
            query.AppendLine("        (select c1.loc_code                                                                                              ");
            query.AppendLine("                  ,decode(c1.ploc_code, null, decode(c2.loc_code, null,                                                  ");
            query.AppendLine("                  decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))) lblock                          ");
            query.AppendLine("                  ,decode(c1.ploc_code, null, decode(c2.loc_code, null	                                             ");
            query.AppendLine("                  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	                                 ");
            query.AppendLine("                  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name))) mblock	                           ");
            query.AppendLine("                  ,decode(c1.ploc_code, null, decode(c2.loc_code, null,                                                  ");
            query.AppendLine("                  decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))                                           ");
            query.AppendLine("                  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                        ");
            query.AppendLine("                  ,c1.ord          	                                                                                 ");
            query.AppendLine("                  ,c1.ftr_code	                                                                                         ");
            query.AppendLine("              from                                                                                 	             ");
            query.AppendLine("                  (                                                                                  	             ");
            query.AppendLine("                   select sgccd                                                                      	             ");
            query.AppendLine("                         ,loc_code                                                                   	                     ");
            query.AppendLine("                         ,ploc_code                                                               	                     ");
            query.AppendLine("                         ,loc_name                                                                	                     ");
            query.AppendLine("                         ,ftr_idn                                                                  	                     ");
            query.AppendLine("                         ,ftr_code                                                                 	                     ");
            query.AppendLine("                         ,rel_loc_name                                                             	                     ");
            query.AppendLine("                         ,kt_gbn                                                                  	             ");
            query.AppendLine("                         ,rownum ord                                                                 	                     ");
            query.AppendLine("                     from cm_location                                                             	                   ");
            query.AppendLine("                    where 1 = 1                                                                                          ");
            query.AppendLine("                    start with ftr_code = 'BZ001'                                                                          ");
            query.AppendLine("                    connect by prior loc_code = ploc_code                                         	                   ");
            query.AppendLine("                    order SIBLINGS by orderby                                                                              ");
            query.AppendLine("                  ) c1                                                                            	                   ");
            query.AppendLine("                   ,cm_location c2                                                                                       ");
            query.AppendLine("                   ,cm_location c3                                                                                 ");
            query.AppendLine("             where 1 = 1                                                                             	                     ");
            query.AppendLine("               and c1.ploc_code = c2.loc_code(+)                                                   	                     ");
            query.AppendLine("               and c2.ploc_code = c3.loc_code(+)                                                   	                 ");
            query.AppendLine("             order by c1.ord            	                                                                                 ");
            query.AppendLine("             ) loc	     ");
            query.AppendLine("             on loc.loc_code = decode(data.loc_code1, null, data.loc_code2, data.loc_code1)) locdata                                                                                     ");
            query.AppendLine("            ,	                                                                                                         ");
            query.AppendLine("             (	                                                                                                         ");
            query.AppendLine("             select c.loc_code	                                                                                 ");
            query.AppendLine("                     , sum(a.wsstvol) use	                                                                         ");
            query.AppendLine("                     , a.stym       	                                                                                 ");
            query.AppendLine("                     from wi_stwchrg a	                                                                             ");
            query.AppendLine("                          , wi_dminfo b	                                                                             ");
            query.AppendLine("                          , cm_location c	                                                                             ");
            query.AppendLine("                    where a.dmno = b.dmno	                                                                             ");
            query.AppendLine("                    and   b.sftridn = c.ftr_idn	                                                                     ");
            query.AppendLine("                    and   a.stym = :STYM	                                                                               ");
            query.AppendLine("                    group by c.loc_code, a.stym	                                                                       ");
            query.AppendLine("             ) s_used                                                                                                    ");
            query.AppendLine("             ,                                                                                                           ");
            query.AppendLine("              (                                                                                                          ");
            query.AppendLine("              select c.loc_code, sum(a.use) use                                                                          ");
            query.AppendLine("              from                                                                                                 ");
            query.AppendLine("                    (                                                                                                      ");
            query.AppendLine("                    select c.loc_code	                                                                                   ");
            query.AppendLine("                     , sum(a.wsstvol) use	                                                                           ");
            query.AppendLine("                     , a.stym                                                                                          ");
            query.AppendLine("                     , c.ploc_code                                                                                     ");
            query.AppendLine("                     from wi_stwchrg a	                                                                             ");
            query.AppendLine("                          , wi_dminfo b	                                                                             ");
            query.AppendLine("                          , cm_location c	                                                                             ");
            query.AppendLine("                    where a.dmno = b.dmno	                                                                             ");
            query.AppendLine("                    and   b.sftridn = c.ftr_idn	                                                                       ");
            query.AppendLine("                    and   a.stym = :STYM	                                                                               ");
            query.AppendLine("                    group by c.loc_code, a.stym,c.ploc_code                                                              ");
            query.AppendLine("                    )a, cm_location c                                                                                    ");
            query.AppendLine("                    where c.loc_code = a.ploc_code                                                                       ");
            query.AppendLine("                    group by c.loc_code                                                                                  ");
            query.AppendLine("              ) m_used                                                                                                   ");
            query.AppendLine("              ,                                                                                                          ");
            query.AppendLine("               (                                                                                                         ");
            query.AppendLine("                select c.loc_code, sum(a.use) use                                                                        ");
            query.AppendLine("                from                                                                                                     ");
            query.AppendLine("                   (                                                                                                     ");
            query.AppendLine("                    select c.loc_code, sum(a.use) use,c.ploc_code                                                        ");
            query.AppendLine("                    from                                                                                               ");
            query.AppendLine("                        (                                                                                              ");
            query.AppendLine("                        select c.loc_code	                                                                               ");
            query.AppendLine("                         , sum(a.wsstvol) use	                                                                       ");
            query.AppendLine("                         , a.stym                                                                                          ");
            query.AppendLine("                         , c.ploc_code                                                                                     ");
            query.AppendLine("                         from wi_stwchrg a	                                                                     ");
            query.AppendLine("                              , wi_dminfo b	                                                                     ");
            query.AppendLine("                              , cm_location c	                                                                             ");
            query.AppendLine("                        where a.dmno = b.dmno	                                                                     ");
            query.AppendLine("                        and   b.sftridn = c.ftr_idn	                                                                   ");
            query.AppendLine("                        and   a.stym = :STYM	                                                                           ");
            query.AppendLine("                        group by c.loc_code, a.stym,c.ploc_code                                                          ");
            query.AppendLine("                        )a, cm_location c                                                                                ");
            query.AppendLine("                        where c.loc_code = a.ploc_code                                                                   ");
            query.AppendLine("                        group by c.loc_code, c.ploc_code                                                                 ");
            query.AppendLine("                   )a , cm_location c                                                                                        ");
            query.AppendLine("                    where c.loc_code = a.ploc_code                                                                           ");
            query.AppendLine("                    group by c.loc_code                                                                                  ");
            query.AppendLine("               ) l_used                                                                                                  ");
            query.AppendLine("             where locdata.loc_code = s_used.loc_code(+)                                                                     ");
            query.AppendLine("             and   locdata.loc_code = m_used.loc_code(+)                                                                     ");
            query.AppendLine("             and   locdata.loc_code = l_used.loc_code(+)                                                                     ");
            query.AppendLine("             and   locdata.loc_code = tag.loc_code(+)                                                                        ");
            query.AppendLine("             order by locdata.ord       ");

            IDataParameter[] parameters =  {
                    new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STYM", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STYM"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public void SelectRevenueRatioEdit(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select loc.loc_code												                            ");
            query.AppendLine("       , loc.lblock LBLOCK											                        ");
            query.AppendLine("       , loc.mblock MBLOCK											                        ");
            query.AppendLine("       , loc.sblock SBLOCK											                        ");
            query.AppendLine("       , to_char(start_time,'yyyy-mm-dd hh24:mi') START_TIME							        ");
            query.AppendLine("       , round(start_value,2) START_VALUE									                    ");
            query.AppendLine("       , to_char(end_time,'yyyy-mm-dd hh24:mi') END_TIME							            ");
            query.AppendLine("       , round(end_value,2) END_VALUE										                    ");
            query.AppendLine("       , round(sum_auto,2) AUTO_SUPPLIED									                    ");
            query.AppendLine("       , round(sum_edit,2) MANUAL_SUPPLIED									                ");
            query.AppendLine("       , case when revenue is not null then round(revenue,2)			                        ");
            query.AppendLine("              when l_used.use is not null then l_used.use										                        ");
            query.AppendLine("              when m_used.use is not null then m_used.use										                        ");
            query.AppendLine("              else s_used.use										                        ");
            query.AppendLine("              end  USED														                        ");            
            query.AppendLine("       , to_char(decode(sum_auto,0,null,round(revenue/sum_auto*100,2)),'FM99999990D99') AUTO_REVENUE                       					");
            query.AppendLine("       , case when sum_edit is not null then to_char(decode(sum_edit,0,null,round((revenue/sum_edit*100),2)),'FM99999990D99')					");
            query.AppendLine("              else null								                                        ");
            query.AppendLine("              end as MANUAL_REVENUE										                    ");
            query.AppendLine("      , decode(tag.tagname,null,null, 'TM') TM                                                ");
            query.AppendLine("       from wv_revenue_ratio  wrr										                        ");            
            query.AppendLine("	  ,													                                        ");
            query.AppendLine("      (                                                                                                                               ");
            query.AppendLine("             select a.tagname, a.loc_code From if_ihtags a, if_tag_gbn b                                                                   ");
            query.AppendLine("             where a.tagname = b.tagname                                                                                              ");
            query.AppendLine("             and   b.tag_gbn ='TD'                                                                                                    ");
            query.AppendLine("             and loc_code is not null                                                                                                 ");
            query.AppendLine("      ) tag                                                                                                                           ");
            query.AppendLine("	  ,(select c1.loc_code											                            ");
            query.AppendLine("		    ,decode(c1.ploc_code, null, decode(c2.loc_code, null,					            ");
            query.AppendLine("		    decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))) lblock			");
            query.AppendLine("		    ,decode(c1.ploc_code, null, decode(c2.loc_code, null					            ");
            query.AppendLine("		    ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))				        ");
            query.AppendLine("		    ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name))) mblock			");
            query.AppendLine("		    ,decode(c1.ploc_code, null, decode(c2.loc_code, null,					            ");
            query.AppendLine("		    decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))				        ");
            query.AppendLine("		    ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock	");
            query.AppendLine("		    ,c1.ord											                                    ");
            query.AppendLine("		    ,c1.ftr_code										                                ");
            query.AppendLine("		from												                                    ");            
            query.AppendLine("		    (												                                    ");
            query.AppendLine("		     select sgccd									                                    ");
            query.AppendLine("			   ,loc_code									                                    ");
            query.AppendLine("			   ,ploc_code									                                    ");
            query.AppendLine("			   ,loc_name									                                    ");
            query.AppendLine("			   ,ftr_idn										                                    ");
            query.AppendLine("			   ,ftr_code									                                    ");
            query.AppendLine("			   ,rel_loc_name								                                    ");
            query.AppendLine("			   ,kt_gbn										                                    ");
            query.AppendLine("			   ,rownum ord									                                    ");
            query.AppendLine("		       from cm_location								                                    ");
            query.AppendLine("		      where 1 = 1									                                    ");
            query.AppendLine("		      start with ftr_code = 'BZ001'					                                    ");
            query.AppendLine("		      connect by prior loc_code = ploc_code			                                    ");
            query.AppendLine("		      order SIBLINGS by orderby						                                    ");
            query.AppendLine("		    ) c1											                                    ");
            query.AppendLine("		     ,cm_location c2								                                    ");
            query.AppendLine("		     ,cm_location c3								                                    ");
            query.AppendLine("	       where 1 = 1										                                    ");
            query.AppendLine("		 and c1.ploc_code = c2.loc_code(+)					                                    ");
            query.AppendLine("		 and c2.ploc_code = c3.loc_code(+)					                                    ");
            query.AppendLine("	       order by c1.ord									                                    ");
            query.AppendLine("	       ) loc											                                    ");
            query.AppendLine("            ,	                                                                                                         ");
            query.AppendLine("             (	                                                                                                         ");
            query.AppendLine("             select c.loc_code	                                                                                 ");
            query.AppendLine("                     , sum(a.wsstvol) use	                                                                         ");
            query.AppendLine("                     , a.stym       	                                                                                 ");
            query.AppendLine("                     from wi_stwchrg a	                                                                             ");
            query.AppendLine("                          , wi_dminfo b	                                                                             ");
            query.AppendLine("                          , cm_location c	                                                                             ");
            query.AppendLine("                    where a.dmno = b.dmno	                                                                             ");
            query.AppendLine("                    and   b.sftridn = c.ftr_idn	                                                                     ");
            query.AppendLine("                    and   a.stym = :STYM	                                                                               ");
            query.AppendLine("                    group by c.loc_code, a.stym	                                                                       ");
            query.AppendLine("             ) s_used                                                                                                    ");
            query.AppendLine("             ,                                                                                                           ");
            query.AppendLine("              (                                                                                                          ");
            query.AppendLine("              select c.loc_code, sum(a.use) use                                                                          ");
            query.AppendLine("              from                                                                                                 ");
            query.AppendLine("                    (                                                                                                      ");
            query.AppendLine("                    select c.loc_code	                                                                                   ");
            query.AppendLine("                     , sum(a.wsstvol) use	                                                                           ");
            query.AppendLine("                     , a.stym                                                                                          ");
            query.AppendLine("                     , c.ploc_code                                                                                     ");
            query.AppendLine("                     from wi_stwchrg a	                                                                             ");
            query.AppendLine("                          , wi_dminfo b	                                                                             ");
            query.AppendLine("                          , cm_location c	                                                                             ");
            query.AppendLine("                    where a.dmno = b.dmno	                                                                             ");
            query.AppendLine("                    and   b.sftridn = c.ftr_idn	                                                                       ");
            query.AppendLine("                    and   a.stym = :STYM	                                                                               ");
            query.AppendLine("                    group by c.loc_code, a.stym,c.ploc_code                                                              ");
            query.AppendLine("                    )a, cm_location c                                                                                    ");
            query.AppendLine("                    where c.loc_code = a.ploc_code                                                                       ");
            query.AppendLine("                    group by c.loc_code                                                                                  ");
            query.AppendLine("              ) m_used                                                                                                   ");
            query.AppendLine("              ,                                                                                                          ");
            query.AppendLine("               (                                                                                                         ");
            query.AppendLine("                select c.loc_code, sum(a.use) use                                                                        ");
            query.AppendLine("                from                                                                                                     ");
            query.AppendLine("                   (                                                                                                     ");
            query.AppendLine("                    select c.loc_code, sum(a.use) use,c.ploc_code                                                        ");
            query.AppendLine("                    from                                                                                               ");
            query.AppendLine("                        (                                                                                              ");
            query.AppendLine("                        select c.loc_code	                                                                               ");
            query.AppendLine("                         , sum(a.wsstvol) use	                                                                       ");
            query.AppendLine("                         , a.stym                                                                                          ");
            query.AppendLine("                         , c.ploc_code                                                                                     ");
            query.AppendLine("                         from wi_stwchrg a	                                                                     ");
            query.AppendLine("                              , wi_dminfo b	                                                                     ");
            query.AppendLine("                              , cm_location c	                                                                             ");
            query.AppendLine("                        where a.dmno = b.dmno	                                                                     ");
            query.AppendLine("                        and   b.sftridn = c.ftr_idn	                                                                   ");
            query.AppendLine("                        and   a.stym = :STYM	                                                                           ");
            query.AppendLine("                        group by c.loc_code, a.stym,c.ploc_code                                                          ");
            query.AppendLine("                        )a, cm_location c                                                                                ");
            query.AppendLine("                        where c.loc_code = a.ploc_code                                                                   ");
            query.AppendLine("                        group by c.loc_code, c.ploc_code                                                                 ");
            query.AppendLine("                   )a , cm_location c                                                                                        ");
            query.AppendLine("                    where c.loc_code = a.ploc_code                                                                           ");
            query.AppendLine("                    group by c.loc_code                                                                                  ");
            query.AppendLine("               ) l_used                                                                                                  ");
            query.AppendLine("	       where wrr.loc_code = loc.loc_code				                                    ");
            query.AppendLine("	       and   loc.loc_code = s_used.loc_code(+)						                                    ");
            query.AppendLine("	       and   loc.loc_code = m_used.loc_code(+)						                                    ");
            query.AppendLine("	       and   loc.loc_code = l_used.loc_code(+)						                                    ");
            query.AppendLine("	       and   wrr.year_mon = :STYM						                                    ");
            query.AppendLine("         and   loc.loc_code = tag.loc_code(+)                                                 ");
            query.AppendLine("	       order by loc.ord									                                    ");


            IDataParameter[] parameters =  {                    
                    new OracleParameter("STYM", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STYM"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public void SelectValues(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select  a.tagname	                                                                                        ");
            query.AppendLine("        ,a.timestamp	                                                                                    ");
            query.AppendLine("        , a.value	                                                                                        ");
            query.AppendLine("        , b.loc_code                                                                                      ");
            query.AppendLine("        from 	                                                                                            ");
            query.AppendLine("              if_gather_realtime a	                                                                    ");
            query.AppendLine("              , if_ihtags b	                                                                            ");
            query.AppendLine("              , if_tag_gbn c	                                                                            ");
            query.AppendLine("        where a.tagname = b.tagname	                                                                    ");
            query.AppendLine("        and a.timestamp between to_date(concat(:STARTDATE,'00'),'yyyymmddhh24mi')                         ");
            query.AppendLine("                        and to_date(concat(:STARTDATE,'59'), 'yyyymmddhh24mi')	                        ");
            query.AppendLine("        and c.tagname = b.tagname	                                                                        ");
            query.AppendLine("        and c.tag_gbn = 'TD'									                                            ");
            query.AppendLine("        and loc_code = :LOC_CODE										                                	");

            IDataParameter[] parameters =  {
                    new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)                    
                };

            parameters[0].Value = parameter["TIME"];
            parameters[1].Value = parameter["TIME"];
            parameters[2].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public object SelectValue(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("            select a.value	                                                                            ");
            query.AppendLine("              from 	                                                                                    ");        
            query.AppendLine("                  if_gather_realtime a	                                                                ");   
            query.AppendLine("                  , if_ihtags b	                                                                        ");    
            query.AppendLine("                  , if_tag_gbn c	                                                                        ");    
            query.AppendLine("            where a.tagname = b.tagname	                                                                ");
            query.AppendLine("            and a.timestamp = to_date(:STARTDATE,'yyyymmddhh24mi')                                        ");
            query.AppendLine("            and c.tagname = b.tagname	                                                                    ");    
            query.AppendLine("            and c.tag_gbn = 'TD'									                                        ");
            query.AppendLine("            and loc_code = :LOC_CODE										                                ");	

            IDataParameter[] parameters =  {
                    new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)                    
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["LOC_CODE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public void UpdateMaster(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_master wrm                           ");
            query.AppendLine("using (select :YEAR_MON YEAR_MON from dual) a             ");
            query.AppendLine("  on (wrm.YEAR_MON = a.YEAR_MON)                           ");
            query.AppendLine("when matched then                                         ");
            query.AppendLine("     update set START_TIME = :START_TIME                  ");
            query.AppendLine("              , END_TIME = :END_TIME                      ");
            query.AppendLine("when not matched then                                     ");
            query.AppendLine("     insert (YEAR_MON, START_TIME, END_TIME)              ");
            query.AppendLine("     values (a.YEAR_MON, :START_TIME, :END_TIME)          ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("START_TIME", OracleDbType.Date)
                     ,new OracleParameter("END_TIME", OracleDbType.Date)
                     ,new OracleParameter("START_TIME", OracleDbType.Date)
                     ,new OracleParameter("END_TIME", OracleDbType.Date)                     
                };

            parameters[0].Value = parameter["YEAR_MON"];
            parameters[1].Value = parameter["M_START_TIME"];
            parameters[2].Value = parameter["M_END_TIME"];
            parameters[3].Value = parameter["M_START_TIME"];
            parameters[4].Value = parameter["M_END_TIME"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeleteMaster(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete from wv_revenue_master where year_mon = :YEAR_MON  ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["YEAR_MON"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void UpdateRevenueRatio(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio wrr                                                                                                                   ");
            query.AppendLine("using (select :LOC_CODE LOC_CODE, :YEAR_MON YEAR_MON from dual) a                                                                                 ");
            query.AppendLine("  on (wrr.LOC_CODE = a.LOC_CODE                                                                                                                   ");
            query.AppendLine("      and wrr.YEAR_MON = a.YEAR_MON)                                                                                                              ");
            query.AppendLine("when matched then                                                                                                                                 ");
            query.AppendLine("     update set WATER_SUPPLIED = :MANUAL_SUPPLIED                                                                                                 ");
            query.AppendLine("              , REVENUE = :USED                                                                                                                   ");
            query.AppendLine("              , START_TIME = to_date(:START_TIME,'yyyyMMdd hh24:mi')                                                                                                          ");
            query.AppendLine("              , START_VALUE = :START_VALUE                                                                                                        ");
            query.AppendLine("              , END_TIME = to_date(:END_TIME,'yyyyMMdd hh24:mi')                                                                                                              ");
            query.AppendLine("              , END_VALUE = :END_VALUE                                                                                                            ");
            query.AppendLine("              , SUM_AUTO = :AUTO_SUPPLIED                                                                                                         ");
            query.AppendLine("              , SUM_EDIT = :MANUAL_SUPPLIED                                                                                                       ");
            query.AppendLine("when not matched then                                                                                                                             ");
            query.AppendLine("     insert (LOC_CODE, YEAR_MON, WATER_SUPPLIED, REVENUE, START_TIME, START_VALUE, END_TIME,END_VALUE, SUM_AUTO, SUM_EDIT)                        ");
            query.AppendLine("     values (a.LOC_CODE, a.YEAR_MON, :MANUAL_SUPPLIED, :USED, to_date(:START_TIME,'yyyyMMdd hh24:mi'), :START_VALUE, to_date(:END_TIME,'yyyyMMdd hh24:mi'), :END_VALUE, :AUTO_SUPPLIED, :MANUAL_SUPPLIED) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("MANUAL_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("USED", OracleDbType.Varchar2)
                     ,new OracleParameter("START_TIME", OracleDbType.Varchar2)
                     ,new OracleParameter("START_VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("END_TIME", OracleDbType.Varchar2)
                     ,new OracleParameter("END_VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("AUTO_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("MANUAL_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("MANUAL_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("USED", OracleDbType.Varchar2)
                     ,new OracleParameter("START_TIME", OracleDbType.Varchar2)
                     ,new OracleParameter("START_VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("END_TIME", OracleDbType.Varchar2)
                     ,new OracleParameter("END_VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("AUTO_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("MANUAL_SUPPLIED", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["YEAR_MON"];
            parameters[2].Value = parameter["MANUAL_SUPPLIED"];
            parameters[3].Value = parameter["USED"];
            parameters[4].Value = parameter["START_TIME"];
            parameters[5].Value = parameter["START_VALUE"];
            parameters[6].Value = parameter["END_TIME"];
            parameters[7].Value = parameter["END_VALUE"];
            parameters[8].Value = parameter["AUTO_SUPPLIED"];
            parameters[9].Value = parameter["MANUAL_SUPPLIED"];
            parameters[10].Value = parameter["MANUAL_SUPPLIED"];
            parameters[11].Value = parameter["USED"];
            parameters[12].Value = parameter["START_TIME"];
            parameters[13].Value = parameter["START_VALUE"];
            parameters[14].Value = parameter["END_TIME"];
            parameters[15].Value = parameter["END_VALUE"];
            parameters[16].Value = parameter["AUTO_SUPPLIED"];
            parameters[17].Value = parameter["MANUAL_SUPPLIED"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void UpdateRevenueRatio2(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio wrr                                                                                                                   ");
            query.AppendLine("using (select :LOC_CODE LOC_CODE, :YEAR_MON YEAR_MON from dual) a                                                                                 ");
            query.AppendLine("  on (wrr.LOC_CODE = a.LOC_CODE                                                                                                                   ");
            query.AppendLine("      and wrr.YEAR_MON = a.YEAR_MON)                                                                                                              ");
            query.AppendLine("when matched then                                                                                                                                 ");
            query.AppendLine("     update set WATER_SUPPLIED = :AUTO_SUPPLIED                                                                                                   ");
            query.AppendLine("              , REVENUE = :USED                                                                                                                   ");
            query.AppendLine("              , START_TIME = to_date(:START_TIME,'yyyyMMdd hh24:mi')                                                                                                          ");
            query.AppendLine("              , START_VALUE = :START_VALUE                                                                                                        ");
            query.AppendLine("              , END_TIME = to_date(:END_TIME,'yyyyMMdd hh24:mi')                                                                                                              ");
            query.AppendLine("              , END_VALUE = :END_VALUE                                                                                                            ");
            query.AppendLine("              , SUM_AUTO = :AUTO_SUPPLIED                                                                                                         ");
            query.AppendLine("              , SUM_EDIT = :MANUAL_SUPPLIED                                                                                                       ");
            query.AppendLine("when not matched then                                                                                                                             ");
            query.AppendLine("     insert (LOC_CODE, YEAR_MON, WATER_SUPPLIED, REVENUE, START_TIME, START_VALUE, END_TIME,END_VALUE, SUM_AUTO, SUM_EDIT)                        ");
            query.AppendLine("     values (a.LOC_CODE, a.YEAR_MON, :AUTO_SUPPLIED, :USED, to_date(:START_TIME,'yyyyMMdd hh24:mi'), :START_VALUE, to_date(:END_TIME,'yyyyMMdd hh24:mi'), :END_VALUE, :AUTO_SUPPLIED, :MANUAL_SUPPLIED)   ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("AUTO_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("USED", OracleDbType.Varchar2)
                     ,new OracleParameter("START_TIME", OracleDbType.Varchar2)
                     ,new OracleParameter("START_VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("END_TIME", OracleDbType.Varchar2)
                     ,new OracleParameter("END_VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("AUTO_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("MANUAL_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("AUTO_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("USED", OracleDbType.Varchar2)
                     ,new OracleParameter("START_TIME", OracleDbType.Varchar2)
                     ,new OracleParameter("START_VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("END_TIME", OracleDbType.Varchar2)
                     ,new OracleParameter("END_VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("AUTO_SUPPLIED", OracleDbType.Varchar2)
                     ,new OracleParameter("MANUAL_SUPPLIED", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["YEAR_MON"];
            parameters[2].Value = parameter["AUTO_SUPPLIED"];
            parameters[3].Value = parameter["USED"];
            parameters[4].Value = parameter["START_TIME"];
            parameters[5].Value = parameter["START_VALUE"];
            parameters[6].Value = parameter["END_TIME"];
            parameters[7].Value = parameter["END_VALUE"];
            parameters[8].Value = parameter["AUTO_SUPPLIED"];
            parameters[9].Value = parameter["MANUAL_SUPPLIED"];
            parameters[10].Value = parameter["AUTO_SUPPLIED"];
            parameters[11].Value = parameter["USED"];
            parameters[12].Value = parameter["START_TIME"];
            parameters[13].Value = parameter["START_VALUE"];
            parameters[14].Value = parameter["END_TIME"];
            parameters[15].Value = parameter["END_VALUE"];
            parameters[16].Value = parameter["AUTO_SUPPLIED"];
            parameters[17].Value = parameter["MANUAL_SUPPLIED"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        public void DeleteRevenueRatio(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete from wv_revenue_ratio where year_mon = :YEAR_MON  ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["YEAR_MON"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }



}
