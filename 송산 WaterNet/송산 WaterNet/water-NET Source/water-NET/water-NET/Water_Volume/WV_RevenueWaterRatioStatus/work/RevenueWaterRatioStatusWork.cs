﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_RevenueWaterRatioStatus.dao;
using WaterNet.WV_Common.work;
using System.Data;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;
using System.Windows.Forms;
using WaterNet.WV_RevenueWaterRatioStatus.vo;

namespace WaterNet.WV_RevenueWaterRatioStatus.work
{
    public class RevenueWaterRatioStatusWork : BaseWork
    {
        private static RevenueWaterRatioStatusWork work = null;
        private RevenueWaterRatioStatusDao dao = null;

        private RevenueWaterRatioStatusWork()
        {
            dao = RevenueWaterRatioStatusDao.GetInstance();
        }

        public static RevenueWaterRatioStatusWork GetInstance()
        {
            if (work == null)
            {
                work = new RevenueWaterRatioStatusWork();
            }
            return work;
        }

        public DataSet SelectTotalRevenueWaterRatioStatus(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectTotalRevenueWaterRatioStatus(base.DataBaseManager, result, "result", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public IList<RevenueWaterRatioStatusVO> SelectBlockRevenueWaterRatioStatus(Hashtable parameter)
        {
            DataSet result = new DataSet();
            IList<RevenueWaterRatioStatusVO> StatusList = new List<RevenueWaterRatioStatusVO>();

            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectBlockRevenueWaterRatioStatus(base.DataBaseManager, result, "result", parameter);

                if (result.Tables.Count == 0)
                {
                    return StatusList;
                }

                foreach (DataRow dataRow in result.Tables[0].Rows)
                {
                    StatusList.Add(new RevenueWaterRatioStatusVO(dataRow));
                }


                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return StatusList;
        }


        public void UpdateBlockRevenueWaterRatioStatus(RowsCollection rows)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (UltraGridRow gridRow in rows)
                {
                    Hashtable parameter = Utils.ConverToHashtable(gridRow);

                    parameter["YEAR_MON"] = Convert.ToDateTime(gridRow.Cells["YEAR_MON"].Value).ToString("yyyyMM");

                    dao.UpdateRevenueWaterRatioStatus(base.DataBaseManager, parameter);
                }

                dao.UpdateAddRevenue_Middle(base.DataBaseManager);
                dao.UpdateAddRevenue_Large(base.DataBaseManager);

                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }
    }
}
