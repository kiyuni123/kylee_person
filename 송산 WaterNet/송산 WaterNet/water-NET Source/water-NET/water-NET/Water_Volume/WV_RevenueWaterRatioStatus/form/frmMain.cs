﻿using System;
using System.Data;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.form;
using WaterNet.WV_Common.util;
using ChartFX.WinForms;
using System.Collections;
using WaterNet.WV_RevenueWaterRatioStatus.work;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.enum1;
using Infragistics.Win;
using WaterNet.WV_RevenueWaterRatioStatus.vo;
using System.Collections.Generic;
using EMFrame.log;

namespace WaterNet.WV_RevenueWaterRatioStatus.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private TableLayout tableLayout = null;
        private UltraGridManager gridManager = null;
        private ChartManager chartManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================유수율현황조회

            object o = EMFrame.statics.AppStatic.USER_MENU["유수율현황조회ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeValueList();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.tableLayout = new TableLayout(tableLayoutPanel1, chartPanel1, tablePanel1, chartVisibleBtn, tableVisibleBtn);

            this.searchBox1.BlockControl.AddDefaultOption(LOCATION_TYPE.LARGE_BLOCK, false);

            //확정일자를 다음달 10일이라고 가정할 경우,
            //현재 4.5일 이라면.
            //3월을 선택하는게 아니라, 4월 10일 이전이므로 2월을 선택한다.
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(this.chart1, "yyyy-MM");
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);

            this.gridManager.SetCellClick(this.ultraGrid2);
            this.gridManager.SetRowClick(this.ultraGrid2, false);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.ultraGrid2.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid2_InitializeLayout);

            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.excelBtn.Click += new EventHandler(ExportExcel_EventHandler);
            this.searchBtn.Click += new EventHandler(SelectTotalRevenueWaterRatioStatus_EventHandler);

            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);
        }


        //추가 급수량, 추가 유수수량 저장.
        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.ultraGrid2.DataSource == null || this.ultraGrid2.Rows.Count == 0)
                {
                    return;
                }

                RevenueWaterRatioStatusWork.GetInstance().UpdateBlockRevenueWaterRatioStatus(this.ultraGrid2.Rows);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            if (e.Layout.Rows.Count == 0)
            {
                return;
            }
            Hashtable row = Utils.ConverToHashtable(e.Layout.Rows[0]);

            if (row["FTR_CODE"].ToString() == "BZ001")
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].Hidden = false;
            }

            if (row["FTR_CODE"].ToString() == "BZ002")
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].Hidden = true;
            }

            if (row["FTR_CODE"].ToString() == "BZ003")
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].Hidden = true;
            }
        }

        //검색조건에 따라 추가 유수수량, 추가 무수수량의 입력여부를 수정한다.
        private void ultraGrid2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            if (e.Layout.Rows.Count == 0)
            {
                return;
            }
            Hashtable row = Utils.ConverToHashtable(e.Layout.Rows[0]);

            if (row["FTR_CODE"].ToString() == "BZ001")
            {
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_WATER_SUPPLIED"].CellActivation = Activation.NoEdit;
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_WATER_SUPPLIED"].CellClickAction = CellClickAction.CellSelect;
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].CellActivation = Activation.AllowEdit;
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].CellClickAction = CellClickAction.Edit;

                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].Hidden = false;
            }

            if (row["FTR_CODE"].ToString() == "BZ002")
            {
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_WATER_SUPPLIED"].CellActivation = Activation.NoEdit;
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_WATER_SUPPLIED"].CellClickAction = CellClickAction.CellSelect;
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].CellActivation = Activation.NoEdit;
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].CellClickAction = CellClickAction.CellSelect;

                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].Hidden = true;
            }

            if (row["FTR_CODE"].ToString() == "BZ003")
            {
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_WATER_SUPPLIED"].CellActivation = Activation.AllowEdit;
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_WATER_SUPPLIED"].CellClickAction = CellClickAction.Edit;
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].CellActivation = Activation.NoEdit;
                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].CellClickAction = CellClickAction.CellSelect;

                this.ultraGrid2.DisplayLayout.Bands[0].Columns["ADD_REVENUE"].Hidden = true;
            }
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //대블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            //대블록
            if (!this.ultraGrid2.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid2.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid2.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid2.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid2.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid2.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];

            this.ultraGrid2.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid2.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid2.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid2.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid2.DisplayLayout.ValueLists["SBLOCK"];
        }

        /// <summary>
        /// 엑셀파일 다운로드이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportExcel_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.chart1, "유수율현황", 0, 0, 9, 20);
                this.excelManager.AddWorksheet(this.ultraGrid1, "유수율현황", 10, 0);

                int row = this.ultraGrid1.Rows.Count + 2;

                this.excelManager.AddWorksheet(this.ultraGrid2, "유수율현황", 10 + row, 0);
                this.excelManager.Save("유수율현황", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 검색버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectTotalRevenueWaterRatioStatus_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectTotalRevenueWaterRatioStatus(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 유수율현황 검색(블록비교기준)
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectTotalRevenueWaterRatioStatus(Hashtable parameter)
        {
            this.mainMap.MoveToBlock(parameter);
            this.ultraGrid1.DataSource = RevenueWaterRatioStatusWork.GetInstance().SelectTotalRevenueWaterRatioStatus(parameter);
        }

        /// <summary>
        /// 로우활성화 이벤트 핸들러
        /// 블록별 우선순위에서 로우를 클릭했을때..
        /// 선택한 블록을 선택상태로 지정해준다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            grid.Selected.Rows.Clear();
            grid.ActiveRow.Selected = true;
        }

        /// <summary>
        /// 블록별 현황에서 블럭이 선택 되었을떄 이벤트핸들러
        /// 블록의 상세내역과 차트를 랜더링한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            if (grid.Selected.Rows.Contains(grid.ActiveRow))
            {
                UltraGridRow row = grid.Selected.Rows[0];

                Hashtable parameter = this.searchBox1.Parameters;

                if (parameter.ContainsKey("SUB_LOC_CODE"))
                {
                    parameter.Remove("SUB_LOC_CODE");
                }
                parameter["SUB_LOC_CODE"] = row.Cells["LOC_CODE"].Value.ToString();
                this.SelectRevenueWaterRatioStatus(parameter);
            }
        }

        /// <summary>
        /// 블록 세부 유수율현황 검색
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectRevenueWaterRatioStatus(Hashtable parameter)
        {
            this.ultraGrid2.DataSource = RevenueWaterRatioStatusWork.GetInstance().SelectBlockRevenueWaterRatioStatus(parameter);
            this.InitializeChartSetting();
        }

        /// <summary>
        /// 검색 데이터 변경시 차트 세팅/ 데이터 초기화 
        /// </summary>
        private void InitializeChartSetting()
        {
            IList<RevenueWaterRatioStatusVO> statusList = (List<RevenueWaterRatioStatusVO>)this.ultraGrid2.DataSource;

            Chart chart = this.chartManager.Items[0];

            chart.Data.Series = 6;
            chart.Data.Points = statusList.Count;

            foreach (RevenueWaterRatioStatusVO status in statusList)
            {
                int pointIndex = statusList.IndexOf(status);
                chart.AxisX.Labels[pointIndex] = status.YEAR_MON.ToString("yyyy-MM");
                
                chart.Data[0, pointIndex] = status.WATER_SUPPLIED;
                chart.Data[1, pointIndex] = status.REVENUE;
                chart.Data[2, pointIndex] = status.NON_REVENUE;
                
                if (Utils.ToDouble(status.ADD_WATER_SUPPLIED) != 0)
                {
                    chart.Data[3, pointIndex] = status.ADD_WATER_SUPPLIED;
                }
                if (status.ADD_REVENUE != 0)
                {
                    chart.Data[4, pointIndex] = status.ADD_REVENUE;
                }
                if (Utils.ToDouble(status.REVENUE_RATIO) != 0)
                {
                    chart.Data[5, pointIndex] = status.REVENUE_RATIO;
                }
            }

            chart.Series[0].AxisY = chart1.AxisY2;
            chart.Series[0].Gallery = Gallery.Lines;
            chart.Series[0].Line.Width = 3;
            chart.Series[0].MarkerSize = 3;

            chart.Series[1].AxisY = chart1.AxisY2;
            chart.Series[1].Gallery = Gallery.Lines;
            chart.Series[1].Line.Width = 3;
            chart.Series[1].MarkerSize = 3;

            chart.Series[2].AxisY = chart1.AxisY2;
            chart.Series[2].Gallery = Gallery.Lines;
            chart.Series[2].Line.Width = 3;
            chart.Series[2].MarkerSize = 3;

            chart.Series[3].AxisY = chart1.AxisY2;
            chart.Series[3].Gallery = Gallery.Lines;
            chart.Series[3].Line.Width = 3;
            chart.Series[3].MarkerSize = 3;

            chart.Series[4].AxisY = chart1.AxisY2;
            chart.Series[4].Gallery = Gallery.Lines;
            chart.Series[4].Line.Width = 3;
            chart.Series[4].MarkerSize = 3;

            chart.Series[5].Gallery = Gallery.Bar;
            chart.Series[5].Line.Width = 1;
            chart.Series[5].MarkerSize = 0;

            chart.Series[0].Text = "급수량(㎥/월)";
            chart.Series[1].Text = "유수수량(㎥/월)";
            chart.Series[2].Text = "무수수량(㎥/월)";
            chart.Series[3].Text = "추가급수량(㎥/월)";
            chart.Series[4].Text = "도수및손괴(㎥/월)";
            chart.Series[5].Text = "유수율(%)";

            chart.AxesY[0].Title.Text = "유수율(%)";
            chart.AxesY[1].Title.Text = "유량(㎥/월)";
            chart.AxesY[1].DataFormat.Format = AxisFormat.Number;
            chart.AxesY[1].DataFormat.CustomFormat = "###,###,###";
            chart.AxesY[1].LabelsFormat.Format = AxisFormat.Number;
            chart.AxesY[1].LabelsFormat.CustomFormat = "###,###,###";

            this.chartManager.AllShowSeries(chart);

            chart.Refresh();
        }

       
    }
}
