﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using WaterNet.WaterNetCore;
using WaterNet.WV_Common.dao;
using WaterNet.WV_Common.helper;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_RevenueWaterRatioStatus.dao
{
    public class RevenueWaterRatioStatusDao : BaseDao
    {
        private static RevenueWaterRatioStatusDao dao = null;

        public static RevenueWaterRatioStatusDao GetInstance()
        {
            if (dao == null)
            {
                dao = new RevenueWaterRatioStatusDao();
            }
            return dao;
        }

        public void SelectTotalRevenueWaterRatioStatus(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                  ");
            query.AppendLine("(																															   ");
            query.AppendLine("select c1.loc_code	     																								   ");
            query.AppendLine("	  ,c1.sgccd																												   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock								   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock								   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock										   ");
            query.AppendLine("	  ,c1.ord																												   ");
            query.AppendLine("	  ,c1.ftr_code	    																									   ");
            query.AppendLine("  from																													   ");
            query.AppendLine("	  (																														   ");
            query.AppendLine("	   select sgccd																											   ");
            query.AppendLine("			 ,loc_code																										   ");
            query.AppendLine("			 ,ploc_code																										   ");
            query.AppendLine("			 ,loc_name																										   ");
            query.AppendLine("			 ,ftr_idn																										   ");
            query.AppendLine("			 ,ftr_code																										   ");
            query.AppendLine("			 ,rel_loc_name																									   ");
            query.AppendLine("			 ,kt_gbn																										   ");
            query.AppendLine("			 ,rownum ord																									   ");
            query.AppendLine("		 from cm_location																									   ");
            query.AppendLine("		where 1 = 1																											   ");

            if (parameter["LOC_CODE"].ToString() == "O")
            {
                query.AppendLine("          and ftr_code = decode(:FTR_CODE, null, 'BZ001', decode(:FTR_CODE, 'BZ001', 'BZ002', 'BZ003'))                   ");
                query.AppendLine("		start with ftr_code = decode(:LOC_CODE, 'O', 'BZ001')                                                                  ");
            }
            else
            {
                query.AppendLine("          and ftr_code = decode(:FTR_CODE, 'BZ000', 'BZ001', decode(:FTR_CODE, 'BZ001', 'BZ002', 'BZ003'))                   ");
                query.AppendLine("		start with loc_code = :LOC_CODE                                                                                        ");
            }
            
            query.AppendLine("		connect by prior loc_code = ploc_code																				   ");
            query.AppendLine("		order SIBLINGS by ftr_idn																							   ");
            query.AppendLine("	  ) c1																													   ");
            query.AppendLine("	   ,cm_location c2																										   ");
            query.AppendLine("	   ,cm_location c3																										   ");
            query.AppendLine(" where 1 = 1																												   ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																						   ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																						   ");
            query.AppendLine(" order by c1.ord																											   ");
            query.AppendLine(")																															   ");
            query.AppendLine(" select loc.loc_code																										   ");
            query.AppendLine("	  ,loc.lblock																											   ");
            query.AppendLine("	  ,loc.mblock																											   ");
            query.AppendLine("	  ,loc.sblock																											   ");
            query.AppendLine("	  ,loc.ftr_code																											   ");
            query.AppendLine("	  ,round(avg(to_number(wpl.pip_len/1000)),6) pip_len																			   ");
            query.AppendLine("	  ,round(avg(to_number(wcd.gupsujunsu)),6) gupsujunsu																	   ");
            query.AppendLine("	  ,round(avg(to_number(wrr.water_supplied)),2) water_supplied															   ");
            query.AppendLine("	  ,round(avg(to_number(wrr.add_water_supplied)),2) add_water_supplied													   ");
            query.AppendLine("	  ,round(avg(to_number(wrr.revenue)),2) revenue																			   ");
            query.AppendLine("	  ,round(avg(to_number(wrr.add_revenue)),2) add_revenue																	   ");

            query.AppendLine("	  ,round(avg(to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0))-to_number(wrr.revenue+nvl(wrr.add_revenue,0))),2) non_revenue										   ");
            query.AppendLine("	  ,decode(avg(to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0))),0,0,round(((avg(to_number(wrr.revenue+nvl(wrr.add_revenue,0))) 									   ");
            query.AppendLine("      / avg(to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0)))) * 100),2)) revenue_ratio														   ");
            query.AppendLine("	  ,round(avg(to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0))-to_number(wrr.revenue+nvl(wrr.add_revenue,0)))/avg(wpl.pip_len/1000),6) pip_len_nonrevenue				   ");
            query.AppendLine("	  ,round(avg(to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0))-to_number(wrr.revenue+nvl(wrr.add_revenue,0)))/avg(wcd.gupsujunsu),6) gupsujunsu_nonrevenue			   ");
            
            query.AppendLine("  from loc																												   ");
            query.AppendLine("	  ,wv_revenue_ratio wrr																									   ");
            query.AppendLine("	  ,(																													   ");
            query.AppendLine("	   select loc_code																										   ");
            query.AppendLine("			 ,to_char(to_date(datee,'yyyymmdd'),'yyyymm') year_mon															   ");
            query.AppendLine("			 ,avg(pip_len) pip_len																							   ");
            query.AppendLine("		 from wv_pipe_lm_day																								   ");
            query.AppendLine("		where to_char(to_date(datee,'yyyymmdd'),'yyyymm') between :STARTDATE and :ENDDATE									   ");
            query.AppendLine("		  and saa_cde = 'SAA004'																							   ");
            query.AppendLine("		group by																											   ");
            query.AppendLine("			  loc_code																										   ");
            query.AppendLine("			 ,to_char(to_date(datee,'yyyymmdd'),'yyyymm')																	   ");
            query.AppendLine("	   ) wpl																												   ");
            query.AppendLine("	  ,(																													   ");
            query.AppendLine("	   select wcd.loc_code																									   ");
            query.AppendLine("			 ,to_char(to_date(wcd.datee,'yyyymmdd'),'yyyymm') year_mon														   ");
            query.AppendLine("			 ,round(avg(wcd.gupsujunsu),6) gupsujunsu																		   ");
            query.AppendLine("		 from wv_consumer_day wcd																							   ");
            query.AppendLine("		where to_char(to_date(wcd.datee,'yyyymmdd'),'yyyymm') between :STARTDATE and :ENDDATE								   ");
            query.AppendLine("		group by wcd.loc_code , to_char(to_date(wcd.datee,'yyyymmdd'),'yyyymm')												   ");
            query.AppendLine("	  ) wcd																													   ");
            query.AppendLine(" where 1 = 1																												   ");
            query.AppendLine("   and wrr.loc_code = loc.loc_code																						   ");
            query.AppendLine("   and wrr.year_mon between :STARTDATE and :ENDDATE																		   ");
            query.AppendLine("   and wpl.loc_code(+) = wrr.loc_code																						   ");
            query.AppendLine("   and wpl.year_mon(+) = wrr.year_mon																						   ");
            query.AppendLine("   and wcd.loc_code(+) = wrr.loc_code																						   ");
            query.AppendLine("   and wcd.year_mon(+) = wrr.year_mon																						   ");
            query.AppendLine(" group by																													   ");
            query.AppendLine("	   loc.ord																												   ");
            query.AppendLine("	  ,loc.loc_code																											   ");
            query.AppendLine("	  ,loc.lblock																											   ");
            query.AppendLine("	  ,loc.mblock																											   ");
            query.AppendLine("	  ,loc.sblock																											   ");
            query.AppendLine("	  ,loc.ftr_code																											   ");
            query.AppendLine(" order by																													   ");
            query.AppendLine("	   loc.ord																												   ");


            IDataParameter[] parameters =  {
                     new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("FTR_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["FTR_CODE"];
            parameters[1].Value = parameter["FTR_CODE"];
            parameters[2].Value = parameter["LOC_CODE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];
            parameters[5].Value = parameter["STARTDATE"];
            parameters[6].Value = parameter["ENDDATE"];
            parameters[7].Value = parameter["STARTDATE"];
            parameters[8].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        public void SelectBlockRevenueWaterRatioStatus(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                   ");
            query.AppendLine("(																																");
            query.AppendLine("select c1.loc_code																											");
            query.AppendLine("	  ,c1.sgccd																													");
            query.AppendLine("	  ,c1.ftr_code																													");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))	");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock									");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock											");
            query.AppendLine("	  ,c1.ord																													");
            query.AppendLine("  from																														");
            query.AppendLine("	  (																															");
            query.AppendLine("	   select sgccd																												");
            query.AppendLine("			 ,loc_code																											");
            query.AppendLine("			 ,ploc_code																											");
            query.AppendLine("			 ,loc_name																											");
            query.AppendLine("			 ,ftr_idn																											");
            query.AppendLine("			 ,ftr_code																											");
            query.AppendLine("			 ,rel_loc_name																										");
            query.AppendLine("			 ,kt_gbn																											");
            query.AppendLine("			 ,rownum ord																										");
            query.AppendLine("		 from cm_location																										");
            query.AppendLine("		where 1 = 1																												");
            query.AppendLine("          and loc_code = :SUB_LOC_CODE																						");
            query.AppendLine("		start with loc_code = :SUB_LOC_CODE																						");
            query.AppendLine("		connect by prior loc_code = ploc_code																					");
            query.AppendLine("		order SIBLINGS by ftr_idn																								");
            query.AppendLine("	  ) c1																														");
            query.AppendLine("	   ,cm_location c2																											");
            query.AppendLine("	   ,cm_location c3																											");
            query.AppendLine(" where 1 = 1																													");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							");
            query.AppendLine(" order by c1.ord																												");
            query.AppendLine(")																																");
            query.AppendLine(" select loc.loc_code																											");
            query.AppendLine("	  ,loc.ftr_code																												");
            query.AppendLine("	  ,loc.lblock																												");
            query.AppendLine("	  ,loc.mblock																												");
            query.AppendLine("	  ,loc.sblock																												");
            query.AppendLine("	  ,to_date(wrr.year_mon,'yyyymm') year_mon																					");
            query.AppendLine("	  ,to_number(round(wpl.pip_len/1000,6)) pip_len																					");
            query.AppendLine("	  ,to_number(round(wcd.gupsujunsu,6)) gupsujunsu																			");
            query.AppendLine("	  ,to_number(wrr.water_supplied) water_supplied																				");
            query.AppendLine("	  ,to_number(wrr.add_water_supplied) add_water_supplied																		");
            query.AppendLine("	  ,to_number(wrr.revenue) revenue																							");
            query.AppendLine("	  ,to_number(wrr.add_revenue) add_revenue																					");

            //query.AppendLine("	  ,to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0))-to_number(wrr.revenue+nvl(wrr.add_revenue,0)) non_revenue															");
            //query.AppendLine("	  ,decode(to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0)),0,0,round(((to_number(wrr.revenue+nvl(wrr.add_revenue,0)) 													");
            //query.AppendLine("       / to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0))) * 100),2)) revenue_ratio																");
            //query.AppendLine("	  ,round((to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0))-to_number(wrr.revenue+nvl(wrr.add_revenue,0)))/to_number(wpl.pip_len/1000),6) pip_len_nonrevenue				");
            //query.AppendLine("	  ,round((to_number(wrr.water_supplied+nvl(wrr.add_water_supplied,0))-to_number(wrr.revenue+nvl(wrr.add_revenue,0)))/to_number(wcd.gupsujunsu),6) gupsujunsu_nonrevenue			");
            
            query.AppendLine("  from loc																													");
            query.AppendLine("	  ,wv_revenue_ratio wrr																										");
            query.AppendLine("	  ,(																														");
            query.AppendLine("	   select datee																												");
            query.AppendLine("			 ,avg(pip_len) pip_len																								");
            query.AppendLine("		 from (																													");
            query.AppendLine("			  select loc.loc_code																								");
            query.AppendLine("					,to_char(to_date(wpl.datee,'yyyymmdd'),'yyyymm') datee														");
            query.AppendLine("					,round(avg(pip_len),6) pip_len																				");
            query.AppendLine("				from wv_pipe_lm_day wpl																							");
            query.AppendLine("					,(																											");
            query.AppendLine("					 select loc_code																							");
            query.AppendLine("					   from cm_location																							");
            query.AppendLine("					  where 1 = 1																								");
            query.AppendLine("					  start with loc_code = :SUB_LOC_CODE																		");
            query.AppendLine("					 connect by prior loc_code = ploc_code																		");
            query.AppendLine("					) loc																										");
            query.AppendLine("			   where wpl.loc_code = loc.loc_code																				");
            query.AppendLine("				 and to_char(to_date(wpl.datee,'yyyymmdd'),'yyyymm') between :STARTDATE and :ENDDATE							");
            query.AppendLine("				 and wpl.saa_cde = 'SAA004'																						");
            query.AppendLine("			   group by loc.loc_code , to_char(to_date(wpl.datee,'yyyymmdd'),'yyyymm')											");
            query.AppendLine("			  )																													");
            query.AppendLine("		group by datee																											");
            query.AppendLine("	  ) wpl																														");
            query.AppendLine("	  ,(																														");
            query.AppendLine("	   select datee																												");
            query.AppendLine("			 ,sum(gupsujunsu) gupsujunsu																						");
            query.AppendLine("		 from (																													");
            query.AppendLine("			  select loc.loc_code																								");
            query.AppendLine("					,to_char(to_date(wcd.datee,'yyyymmdd'),'yyyymm') datee														");
            query.AppendLine("					,round(avg(wcd.gupsujunsu),6) gupsujunsu																	");
            query.AppendLine("				from wv_consumer_day wcd																						");
            query.AppendLine("					,(																											");
            query.AppendLine("					 select loc_code																							");
            query.AppendLine("					   from cm_location																							");
            query.AppendLine("					  where 1 = 1																								");
            query.AppendLine("					  start with loc_code = :SUB_LOC_CODE																		");
            query.AppendLine("					 connect by prior loc_code = ploc_code																		");
            query.AppendLine("					) loc																										");
            query.AppendLine("			   where wcd.loc_code = loc.loc_code																				");
            query.AppendLine("				 and to_char(to_date(wcd.datee,'yyyymmdd'),'yyyymm') between :STARTDATE and :ENDDATE							");
            query.AppendLine("			   group by loc.loc_code , to_char(to_date(wcd.datee,'yyyymmdd'),'yyyymm')											");
            query.AppendLine("			 )																													");
            query.AppendLine("			 group by datee																										");
            query.AppendLine("	  ) wcd																														");
            query.AppendLine(" where 1 = 1																													");
            query.AppendLine("   and wrr.loc_code = loc.loc_code																							");
            query.AppendLine("   and wrr.year_mon between :STARTDATE and :ENDDATE																			");
            query.AppendLine("   and wpl.datee(+) = wrr.year_mon																							");
            query.AppendLine("   and wcd.datee(+) = wrr.year_mon																							");
            query.AppendLine(" order by 																													");
            query.AppendLine("       loc.ord																												");
            query.AppendLine("      ,wrr.year_mon																											");


            IDataParameter[] parameters =  {
                     new OracleParameter("SUB_LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("SUB_LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("SUB_LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("SUB_LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["SUB_LOC_CODE"];
            parameters[1].Value = parameter["SUB_LOC_CODE"];
            parameters[2].Value = parameter["SUB_LOC_CODE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];
            parameters[5].Value = parameter["SUB_LOC_CODE"];
            parameters[6].Value = parameter["STARTDATE"];
            parameters[7].Value = parameter["ENDDATE"];
            parameters[8].Value = parameter["STARTDATE"];
            parameters[9].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        public void UpdateRevenueWaterRatioStatus(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update wv_revenue_ratio a                                           ");
            query.AppendLine("  set a.add_water_supplied = :ADD_WATER_SUPPLIED					  ");
            query.AppendLine("     ,a.add_revenue = :ADD_REVENUE								  ");
            query.AppendLine(" where a.loc_code = :LOC_CODE										  ");
            query.AppendLine("   and a.year_mon = :YEAR_MON										  ");

            IDataParameter[] parameters =  {
                     new OracleParameter("ADD_WATER_SUPPLIED", OracleDbType.Varchar2)
                    ,new OracleParameter("ADD_REVENUE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["ADD_WATER_SUPPLIED"];
            parameters[1].Value = parameter["ADD_REVENUE"];
            parameters[2].Value = parameter["LOC_CODE"];
            parameters[3].Value = parameter["YEAR_MON"];

            manager.ExecuteScript(query.ToString(), parameters);
        }


        public void UpdateAddRevenue_Middle(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio a                                                    ");
            query.AppendLine("using (																		   ");
            query.AppendLine("select loc.ploc_code loc_code													   ");
            query.AppendLine("	  ,wrr.year_mon year_mon													   ");
            query.AppendLine("	  ,sum(wrr.add_water_supplied) add_water_supplied							   ");
            query.AppendLine("  from wv_revenue_ratio wrr													   ");
            query.AppendLine("	  ,(																		   ");
            query.AppendLine("	   select ploc_code, loc_code												   ");
            query.AppendLine("			 ,ftr_idn															   ");
            query.AppendLine("		 from cm_location														   ");
            query.AppendLine("		where ftr_code = 'BZ003'												   ");
            query.AppendLine("	   ) loc																	   ");
            query.AppendLine("  where wrr.loc_code = loc.loc_code											   ");
            query.AppendLine("  group by loc.ploc_code, wrr.year_mon										   ");
            query.AppendLine(") b																			   ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)					   ");
            query.AppendLine(" when matched then															   ");
            query.AppendLine("	  update set a.add_water_supplied = b.add_water_supplied					   ");
            query.AppendLine(" when not matched then														   ");
            query.AppendLine("	  insert (a.loc_code, a.year_mon, a.add_water_supplied)						   ");
            query.AppendLine("	  values (b.loc_code, b.year_mon, b.add_water_supplied)						   ");

            manager.ExecuteScript(query.ToString(), null);
        }

        public void UpdateAddRevenue_Large(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio a													   ");
            query.AppendLine("using (																		   ");
            query.AppendLine("select loc.ploc_code loc_code													   ");
            query.AppendLine("	  ,wrr.year_mon year_mon													   ");
            query.AppendLine("	  ,sum(wrr.add_water_supplied) add_water_supplied							   ");
            query.AppendLine("  from wv_revenue_ratio wrr													   ");
            query.AppendLine("	  ,(																		   ");
            query.AppendLine("	   select ploc_code, loc_code												   ");
            query.AppendLine("			 ,ftr_idn															   ");
            query.AppendLine("		 from cm_location														   ");
            query.AppendLine("		where ftr_code = 'BZ002'												   ");
            query.AppendLine("	   ) loc																	   ");
            query.AppendLine("  where wrr.loc_code = loc.loc_code											   ");
            query.AppendLine("  group by loc.ploc_code, wrr.year_mon										   ");
            query.AppendLine(") b																			   ");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)					   ");
            query.AppendLine(" when matched then															   ");
            query.AppendLine("	  update set a.add_water_supplied = b.add_water_supplied					   ");
            query.AppendLine(" when not matched then														   ");
            query.AppendLine("	  insert (a.loc_code, a.year_mon, a.add_water_supplied)						   ");
            query.AppendLine("	  values (b.loc_code, b.year_mon, b.add_water_supplied)						   ");

            manager.ExecuteScript(query.ToString(), null);
        }
    }
}
