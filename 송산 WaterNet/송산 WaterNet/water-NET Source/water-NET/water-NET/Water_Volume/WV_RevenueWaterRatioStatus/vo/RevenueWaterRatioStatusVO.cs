﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_RevenueWaterRatioStatus.vo
{
    public class RevenueWaterRatioStatusVO
    {
        private string loc_code = string.Empty;
        private string ftr_code = string.Empty;
        private string lblock = string.Empty;
        private string mblock = string.Empty;
        private string sblock = string.Empty;
        private DateTime year_mon;
        private double water_supplied = 0;
        private double add_water_supplied = 0;
        private double revenue = 0;
        private double add_revenue = 0;
        //private double non_revenue = 0;
        //private double revenue_ratio = 0;
        private double pip_len = 0;
        private double gupsujunsu = 0;
        //private double pip_len_nonrevenue = 0;
        //private double gupsujunsu_nonrevenue = 0;

        public RevenueWaterRatioStatusVO(DataRow RWStatus)
        {
            this.loc_code = RWStatus["LOC_CODE"].ToString();
            this.ftr_code = RWStatus["FTR_CODE"].ToString();
            this.lblock = RWStatus["LBLOCK"].ToString();
            this.mblock = RWStatus["MBLOCK"].ToString();
            this.sblock = RWStatus["SBLOCK"].ToString();
            this.year_mon = Convert.ToDateTime(RWStatus["YEAR_MON"]);
            this.water_supplied = Utils.ToDouble(RWStatus["WATER_SUPPLIED"]);
            this.add_water_supplied = Utils.ToDouble(RWStatus["ADD_WATER_SUPPLIED"]);
            this.revenue = Utils.ToDouble(RWStatus["REVENUE"]);
            this.add_revenue = Utils.ToDouble(RWStatus["ADD_REVENUE"]);
            this.pip_len = Utils.ToDouble(RWStatus["PIP_LEN"]);
            this.gupsujunsu = Utils.ToDouble(RWStatus["GUPSUJUNSU"]);
        }

        public string LOC_CODE
        {
            get
            {
                return this.loc_code;
            }
            set
            {
                this.loc_code = value;
            }
        }

        public string FTR_CODE
        {
            get
            {
                return this.ftr_code;
            }
            set
            {
                this.ftr_code = value;
            }
        }

        public string LBLOCK
        {
            get
            {
                return this.lblock;
            }
            set
            {
                this.lblock = value;
            }
        }

        public string MBLOCK
        {
            get
            {
                return this.mblock;
            }
            set
            {
                this.mblock = value;
            }
        }

        public string SBLOCK
        {
            get
            {
                return this.sblock;
            }
            set
            {
                this.sblock = value;
            }
        }

        public DateTime YEAR_MON
        {
            get
            {
                return this.year_mon;
            }
            set
            {
                this.year_mon = value;
            }
        }

        public double WATER_SUPPLIED
        {
            get
            {
                return this.water_supplied;
            }
            set
            {
                this.water_supplied = value;
            }
        }

        public double ADD_WATER_SUPPLIED
        {
            get
            {
                return this.add_water_supplied;
            }
            set
            {
                this.add_water_supplied = value;
            }
        }

        public double REVENUE
        {
            get
            {
                return this.revenue;
            }
            set
            {
                this.revenue = value;
            }
        }

        public double ADD_REVENUE
        {
            get
            {
                return this.add_revenue;
            }
            set
            {
                this.add_revenue = value;
            }
        }

        public double NON_REVENUE
        {
            get
            {
                double result = 0;

                result = (this.WATER_SUPPLIED + this.ADD_WATER_SUPPLIED) - (this.REVENUE + this.ADD_REVENUE);

                return result ;
            }
            //set
            //{
            //    this.non_revenue = value;
            //}
        }

        public double REVENUE_RATIO
        {
            get
            {
                double result = 0;

                result = ((this.REVENUE + this.ADD_REVENUE) / (this.WATER_SUPPLIED + this.ADD_WATER_SUPPLIED)) * 100;

                return result;
            }
            //set
            //{
            //    this.revenue_ratio = value;
            //}
        }

        public double PIP_LEN
        {
            get
            {
                return this.pip_len;
            }
            set
            {
                this.pip_len = value;
            }
        }

        public double GUPSUJUNSU
        {
            get
            {
                return this.gupsujunsu;
            }
            set
            {
                this.gupsujunsu = value;
            }
        }

        public double PIP_LEN_NONREVENUE
        {
            get
            {
                double result = 0;

                result = this.NON_REVENUE / this.PIP_LEN;

                return result;
            }
            //set
            //{
            //    this.pip_len_nonrevenue = value;
            //}
        }

        public double GUPSUJUNSU_NONREVENUE
        {
            get
            {
                double result = 0;

                result = this.NON_REVENUE / this.GUPSUJUNSU;

                return result;
            }
            //set
            //{
            //    this.gupsujunsu_nonrevenue = value;
            //}
        }
    }
}
