﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_Common.work;

namespace WaterNet.WV_RealTimeDataInformation.work
{
    public class RealTimeDataWork : BaseWork
    {
        private static RealTimeDataWork work = null;
        private dao.RealTimeDataDao Dao = null;

        public static RealTimeDataWork GetInstance()
        {
            if (work == null)
            {
                work = new RealTimeDataWork();
            }
            return work;
        }

        private RealTimeDataWork()
        {
            Dao = dao.RealTimeDataDao.GetInstance();
        }

        public System.Data.DataSet selectRealtime(System.Collections.Hashtable parameter)
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                result = this.Dao.selectRealtimeDao(DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result as System.Data.DataSet;
        }

        
    }
}
