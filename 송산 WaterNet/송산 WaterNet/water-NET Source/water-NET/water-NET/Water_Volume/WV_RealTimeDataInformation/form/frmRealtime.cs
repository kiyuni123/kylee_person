﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.manager;

namespace WaterNet.WV_RealTimeDataInformation.form
{
    public partial class frmRealtime : Form, WaterNetCore.IForminterface
    {
        private UltraGridManager gridManager = null;
        
        /// <summary>
        /// 생성자
        /// </summary>
        public frmRealtime()
        {   
            InitializeComponent();
            this.Load += new EventHandler(frmRealtime_Load);
            // 검색조건부분 사이즈 변경 이벤트 등록
            flowLayoutPanel1.SizeChanged += new EventHandler(flowLayoutPanel1_SizeChanged);
        }

#region IForminterface 멤버
        string WaterNetCore.IForminterface.FormID
        {
            get { return this.Text.ToString(); }
        }
        string WaterNetCore.IForminterface.FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
#endregion

        /// <summary>
        /// DB 클래스
        /// </summary>
        //ListWork lw = new ListWork();
        //WaterNETServer.dao.OracleWork ow = new WaterNETServer.dao.OracleWork();

        int exeCnt = 0;

        /// <summary>
        /// 폼 사이즈 변경시 검색조건부분 리사이징
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void flowLayoutPanel1_SizeChanged(object sender, EventArgs e)
        {
            this.panel1.Height = this.flowLayoutPanel1.Height + 34;
            this.groupBox1.Height = this.panel1.Height + 20;
            this.splitContainer1.SplitterDistance = this.groupBox1.Height;
        }

        /// <summary>
        /// 폼 로드시 실행
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmRealtime_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeForm();

            // 폼 로드시 검색 부분 사이즈 초기화
            flowLayoutPanel1_SizeChanged(sender, e);

            // 일단 검색 보여줘
            Search();
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);

            this.ultraGrid1.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.button1.Click +=new EventHandler(button1_Click);

        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.radioButton1.Checked = true;
            this.dateTimePicker2.Value = DateTime.Today.AddDays(1);
        }

        /// <summary>
        /// 검색
        /// </summary>
        private void Search()
        {
            // 검색 조건 세팅
            Hashtable sCondition = new Hashtable();
            sCondition.Add("textbox1", textBox1.Text);
            sCondition.Add("textbox2", textBox2.Text);
            sCondition.Add("datatype", this.radioButton1.Checked ? "Realtime" : "Integrate");
            sCondition.Add("dateTimePicker1", dateTimePicker1.Value.ToString("yyyyMMdd"));
            sCondition.Add("dateTimePicker2", dateTimePicker2.Value.ToString("yyyyMMdd"));
            sCondition.Add("cnt", textBox3.Text);

            // 검색
            DataSet dSet = WaterNet.WV_RealTimeDataInformation.work.RealTimeDataWork.GetInstance().selectRealtime(sCondition);
            foreach (DataTable dt in dSet.Tables)
            {
                DataColumnCollection dcc = dSet.Tables[dt.TableName].Columns;
                // 첫 검색일 때 그리드 컬럼명을 세팅
                //if (exeCnt == 0) fu.InitializeSettingTagColumns(ultraGrid1, dcc);
                // 검색 결과 그리드에 표시
                ultraGrid1.DataSource = dSet.Tables[dt.TableName];
            }

            // 검색 카운트 증가 (첫 검색 판단에만 사용)
            //exeCnt++;

            ultraGrid1.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
            // 수정 컬럼 설정
            //cm.ColumeAllowEdit(ultraGrid1, 1, 12);
            //cm.ColumeAllowEdit(ultraGrid1, 13, 16);
        }


#region 검색 조건 함수 (콤보 박스 완성)

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

#endregion 검색 조건 함수 완료

#region 이벤트 모음 시작

        /// <summary>
        /// 검색버튼 클릭
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            Search();
        }

#endregion 이벤트 모음 종료

    }
}
