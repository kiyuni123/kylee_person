﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WaterNetCore;

namespace WaterNet.WV_RealTimeDataInformation.dao
{
    public class RealTimeDataDao
    {
        private static RealTimeDataDao dao = null;

        private RealTimeDataDao() { }

        public static RealTimeDataDao GetInstance()
        {
            if (dao == null)
            {
                dao = new RealTimeDataDao();
            }
            return dao;
        }

        /// <summary>
        /// 실시간 수집 태그 목록 전체를 가져온다.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="dataset"></param>
        /// <param name="tableName"></param>
        public object selectRealtimeDao(OracleDBManager manager, System.Collections.Hashtable param)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select /*+ index_desc( a if_gather_realtime_pk ) index_desc( b xpkif_ihtags )*/");
            query.AppendLine("       a.tagname");
            query.AppendLine("     , to_char(a.timestamp, 'yyyy/mm/dd hh24:mi:ss') timestamp");
            query.AppendLine("     , b.description");
            query.AppendLine("     , round(a.value, 2) value");
            if (((string)param["datatype"]).Contains("Realtime"))
            {
                query.AppendLine("  from if_gather_realtime a");
            }
            else
            {
                query.AppendLine("  from if_accumulation_hour a");
            }
            
            query.AppendLine("     , (select *");
            query.AppendLine("          from if_ihtags");
            query.AppendLine("         where 1 = 1");

            if (!param["textbox1"].ToString().Equals(""))
            {
                query.AppendLine("           and tagname like '%" + param["textbox1"].ToString() + "%'");
            }
            if (!param["textbox2"].ToString().Equals(""))
            {
                query.AppendLine("           and description like '%" + param["textbox2"].ToString() + "%'");
            }

            query.AppendLine("       ) b");
            query.AppendLine(" where a.tagname = b.tagname");

            if (!param["dateTimePicker1"].ToString().Equals(""))
            {
                query.Append("   and a.timestamp between to_date('" + param["dateTimePicker1"].ToString() + "', 'yyyymmdd')");
            }
            if (!param["dateTimePicker2"].ToString().Equals(""))
            {
                query.AppendLine(" and to_date('" + param["dateTimePicker2"].ToString() + "', 'yyyymmdd')");
            }

            query.AppendLine("   and rownum <= " + param["cnt"].ToString() + "");

            return  manager.ExecuteScriptDataSet(query.ToString(), new System.Data.IDataParameter[] { });
        }
    }
}
