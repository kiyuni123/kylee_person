﻿using System;
using WaterNet.WV_Common.work;
using WaterNet.WV_Report.dao;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using WaterNet.WV_Common.util;
using Infragistics.Win.UltraWinGrid;
using System.Drawing;

namespace WaterNet.WV_Report.work
{
    public class ReportWork : BaseWork
    {
        private static ReportWork work = null;
        private ReportDao dao = null;

        public static ReportWork GetInstance()
        {
            if (work == null)
            {
                work = new ReportWork();
            }
            return work;
        }

        private ReportWork()
        {
            dao = ReportDao.GetInstance();
        }

        public object SelectTagCount(Hashtable parameter)
        {
            object result = null;
            try
            {
                ConnectionOpen();
                BeginTransaction();

                result = dao.SelectTagCount(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectDailyReport(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectDailyReport_LargeInfo(base.DataBaseManager, result, "LARGE_INFO", parameter);
                dao.SelectDailyReport_MiddleInfo(base.DataBaseManager, result, "MIDDLE_INFO", parameter);
                dao.SelectDailyReport_SmallInfo(base.DataBaseManager, result, "SMALL_INFO", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        

        public DataSet SelectValueEdit(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                if (parameter["TAG_GBN"].ToString() == "MNF")
                {
                    dao.SelectMNF(base.DataBaseManager, result, "RESULT", parameter);
                }

                if (parameter["TAG_GBN"].ToString() == "FRQ" || parameter["TAG_GBN"].ToString() == "FRQ_O" ||
                    parameter["TAG_GBN"].ToString() == "SPL_D" || parameter["TAG_GBN"].ToString() == "SPL_O" || parameter["TAG_GBN"].ToString() == "SPL_I")
                {
                    dao.SelectFlow(base.DataBaseManager, result, "RESULT", parameter);
                }

                if (parameter["TAG_GBN"].ToString() == "YD")
                {
                    dao.SelectFlow_YD(base.DataBaseManager, result, "RESULT", parameter);
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public void UpdateValueEdit(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                if (parameter["TAG_GBN"].ToString() == "MNF")
                {
                    if (parameter["TAGNAME"] == null)
                    {
                        parameter["TAGNAME"] = dao.SelectTagName(base.DataBaseManager, parameter);
                    }

                    dao.UpdateMNF(base.DataBaseManager, parameter);
                }

                if (parameter["TAG_GBN"].ToString() == "FRQ" || parameter["TAG_GBN"].ToString() == "FRQ_O" ||
                    parameter["TAG_GBN"].ToString() == "SPL_D" || parameter["TAG_GBN"].ToString() == "SPL_O" || parameter["TAG_GBN"].ToString() == "SPL_I")
                {
                    if (parameter["TAGNAME"] == null)
                    {
                        parameter["TAGNAME"] = dao.SelectTagName(base.DataBaseManager, parameter);
                    }

                    //DATEE를 시간으로 구분해서 00시인경우 하루를 빼줘야한다.
                    DateTime DATEE = DateTime.ParseExact(parameter["DATEE"].ToString(), "yyyyMMddHHmm", null);

                    if (DATEE.Hour.ToString() == "0")
                    {
                        parameter["DATEE"] = DATEE.AddDays(-1).ToString("yyyyMMddHHmm");
                    }

                    //시공급량 수정
                    dao.UpdateFlow(base.DataBaseManager, parameter);

                    //일공급량 수정
                    dao.UpdateDayFlow(base.DataBaseManager, parameter);

                    //태그명으로 검색한다.
                    //현재의 태그명으로 FRQ 구분이 있으면 블록의 유입공급량이 수정된걸로 판단하고 유수율분석의 수정 검증 로직을 수행한다.
                    //FRQ로 파생된 태그가 아니면 로직을 빠져나간다.
                    if (Utils.ToDouble(dao.SelectFRQCount(base.DataBaseManager, parameter)) == 0)
                    {
                        CommitTransaction();
                        return;
                    }

                    //월공급량 수정
                    //시작일자 / 종료일자 설정
                    parameter["YEAR_MON"] =
                        Utils.GetSuppliedMonth(parameter["DATEE"].ToString(), "yyyyMMddHHmm", parameter["LOC_CODE"].ToString());

                    parameter["STARTDATE"] =
                        Utils.GetSuppliedStartdate(parameter["DATEE"].ToString(), "yyyyMMddHHmm", parameter["LOC_CODE"].ToString());

                    parameter["ENDDATE"] =
                        Utils.GetSuppliedEnddate(parameter["DATEE"].ToString(), "yyyyMMddHHmm", parameter["LOC_CODE"].ToString());

                    //월공급량 수정전, 현재일자가 급수량 선정일자 및 유수수량 확정일자를 충족하는지 확인한다.
                    //if (Utils.CanRevenueUpdate(parameter["YEAR_MON"].ToString(), "yyyyMM", parameter["LOC_CODE"].ToString()))
                    //{
                    //    dao.UpdateMonthFlow_Small(base.DataBaseManager, parameter);
                    //    dao.UpdateMonthFlow_Middle(base.DataBaseManager, parameter);
                    //    dao.UpdateMonthFlow_Large(base.DataBaseManager, parameter);
                    //}
                }

                if (parameter["TAG_GBN"].ToString() == "YD")
                {
                    dao.UpdateFlow_YD(base.DataBaseManager, parameter);

                    //월공급량 수정
                    //시작일자 / 종료일자 설정
                    parameter["YEAR_MON"] =
                        Utils.GetSuppliedMonth(parameter["DATEE"].ToString(), "yyyyMMdd", parameter["LOC_CODE"].ToString());

                    parameter["STARTDATE"] =
                        Utils.GetSuppliedStartdate(parameter["DATEE"].ToString(), "yyyyMMdd", parameter["LOC_CODE"].ToString());

                    parameter["ENDDATE"] =
                        Utils.GetSuppliedEnddate(parameter["DATEE"].ToString(), "yyyyMMdd", parameter["LOC_CODE"].ToString());

                                        //월공급량 수정전, 현재일자가 급수량 선정일자 및 유수수량 확정일자를 충족하는지 확인한다.
                    //if (Utils.CanRevenueUpdate(parameter["YEAR_MON"].ToString(), "yyyyMM", parameter["LOC_CODE"].ToString()))
                    //{
                    //    dao.UpdateMonthFlow_Small(base.DataBaseManager, parameter);
                    //    dao.UpdateMonthFlow_Middle(base.DataBaseManager, parameter);
                    //    dao.UpdateMonthFlow_Large(base.DataBaseManager, parameter);
                    //}
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public DataSet SelectDayMNF(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectMNF_TotalInfo(base.DataBaseManager, result, "TOTAL_INFO", parameter);
                dao.SelectMNF_MiddleInfo(base.DataBaseManager, result, "MIDDLE_INFO", parameter);
                dao.SelectMNF_SmallInfo(base.DataBaseManager, result, "SMALL_INFO", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectDaySupply(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectDaySupply_TotalInfo(base.DataBaseManager, result, "TOTAL_INFO", parameter);
                dao.SelectDaySupply_MiddleInfo(base.DataBaseManager, result, "MIDDLE_INFO", parameter);
                dao.SelectDaySupply_SmallInfo(base.DataBaseManager, result, "SMALL_INFO", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }


        public DataSet SelectTimeSupply(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectTimeSupply_TotalInfo(base.DataBaseManager, result, "TOTAL_INFO", parameter);
                dao.SelectTimeSupply_MiddleInfo(base.DataBaseManager, result, "MIDDLE_INFO", parameter);
                dao.SelectTimeSupply_SmallInfo(base.DataBaseManager, result, "SMALL_INFO", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        //20140512 승일추가
        //시단위공급량2 DB쿼리 사용하기 위해 선언
        public DataSet SelectTimeSupply2(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectTimeSupply_TotalInfo2(base.DataBaseManager, result, "BLOCK_INFO", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }


        public DataSet SelectSystemTimeSupply(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectSystemTimeSupply_TotalInfo(base.DataBaseManager, result, "TOTAL_INFO", parameter);
                dao.SelectSystemTimeSupply_MiddleInfo(base.DataBaseManager, result, "MIDDLE_INFO", parameter);
                dao.SelectSystemTimeSupply_SmallInfo(base.DataBaseManager, result, "SMALL_INFO", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectSystemDailyReport(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectSystemDailyReport_TotalInfo(base.DataBaseManager, result, "TOTAL_INFO", parameter);
                dao.SelectSystemDailyReport_MiddleInfo(base.DataBaseManager, result, "MIDDLE_INFO", parameter);
                dao.SelectSystemDailyReport_SmallInfo(base.DataBaseManager, result, "SMALL_INFO", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }


        public DataSet SelectDailyLog(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.SelectDailyLog(base.DataBaseManager, result, "RESULT", parameter);
                dao.SelectDailyLogChart(base.DataBaseManager, result, "CHART", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public void UpdateDailyLog(RowsCollection gridRows)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach(UltraGridRow gridRow in gridRows)
                {
                    Hashtable parameter = Utils.ConverToHashtable(gridRow);
                    parameter.Remove("DATEE");
                    parameter["DATEE"] = Convert.ToDateTime( gridRow.Cells["DATEE"].Value).ToString("yyyyMMdd");

                    dao.UpdateDailyLog(base.DataBaseManager, parameter);
                }
                MessageBox.Show("정상적으로 처리되었습니다.");
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void DeleteDailyLog(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.DeleteDailyLog(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public DataTable getReservoirs(string flowMeasurePoint)
        {
            DataTable data = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();

                data = dao.getReservoirs(base.DataBaseManager, flowMeasurePoint);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }

            return data;
        }

        public DataTable getAccumulationHourFlows(Hashtable parameter)
        {
            DataTable data = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();

                data = dao.getAccumulationHourFlows(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }

            return data;
        }

        public void updateAccumulationHourFlows(DataTable data)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                Hashtable parameter = new Hashtable();
                foreach (DataRow row in data.Rows)
                {
                    parameter["TAGNAME"] = row["TAGNAME"];
                    parameter["TIMESTAMP"] = Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyyMMddHH");
                    parameter["VALUE"] = row["VALUE"];
                    dao.updateAccumulationHourFlows(base.DataBaseManager, parameter);
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }
    }
}
