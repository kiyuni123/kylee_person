﻿using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using WaterNet.WaterNetCore;
using System.Collections;
using WaterNet.WV_Common.dao;

namespace WaterNet.WV_Report.dao
{
    public class ReportDao : BaseDao
    {
        private static ReportDao dao = null;
        private ReportDao() { }

        public static ReportDao GetInstance()
        {
            if (dao == null)
            {
                dao = new ReportDao();
            }
            return dao;
            
        }

        public object SelectTagCount(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select count(*)                                               ");
            query.AppendLine("  from if_ihtags iih                                          ");
            query.AppendLine("      ,if_tag_gbn itg                                         ");
            query.AppendLine(" where iih.loc_code2 = :LOC_CODE                              ");
            query.AppendLine("   and itg.tagname = iih.tagname                              ");
            query.AppendLine("   and itg.tag_gbn = :TAG_GBN                                 ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("TAG_GBN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["TAG_GBN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectTagName(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select itg.tagname                                            ");
            query.AppendLine("  from if_ihtags iih                                          ");
            query.AppendLine("      ,if_tag_gbn itg                                         ");
            query.AppendLine(" where iih.loc_code2 = :LOC_CODE                              ");
            query.AppendLine("   and itg.tagname = iih.tagname                              ");
            query.AppendLine("   and itg.tag_gbn = :TAG_GBN                                 ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("TAG_GBN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["TAG_GBN"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public object SelectFRQCount(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select count(*)                                               ");
            query.AppendLine("  from if_tag_gbn												");
            query.AppendLine(" where tag_gbn = 'FRQ'										");
            query.AppendLine("   and tagname = :TAGNAME										");

            IDataParameter[] parameters =  {
                     new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TAGNAME"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        //관망운영일보 대블록 정보
        public void SelectDailyReport_LargeInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                    ");
            //query.AppendLine("(																																 ");
            //query.AppendLine("select c1.loc_code																											 ");
            //query.AppendLine("      ,c1.sgccd																												 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock								 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock								 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock										 ");
            //query.AppendLine("      ,c1.ftr_code																											 ");
            //query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code	 ");
            //query.AppendLine("      ,c1.ord																													 ");
            //query.AppendLine("  from																														 ");
            //query.AppendLine("      (																														 ");
            //query.AppendLine("       select sgccd																											 ");
            //query.AppendLine("             ,loc_code																										 ");
            //query.AppendLine("             ,ploc_code																										 ");
            //query.AppendLine("             ,loc_name																										 ");
            //query.AppendLine("             ,ftr_idn																											 ");
            //query.AppendLine("             ,ftr_code																										 ");
            //query.AppendLine("             ,kt_gbn																											 ");
            //query.AppendLine("             ,rownum ord																										 ");
            //query.AppendLine("         from cm_location																										 ");
            //query.AppendLine("        where 1 = 1																											 ");
            //query.AppendLine("          and loc_code = :LOC_CODE																							 ");
            //query.AppendLine("        start with ftr_code = 'BZ001'																							 ");
            //query.AppendLine("        connect by prior loc_code = ploc_code																					 ");
            //query.AppendLine("        order SIBLINGS by ftr_idn																								 ");
            //query.AppendLine("      ) c1																													 ");
            //query.AppendLine("       ,cm_location c2																										 ");
            //query.AppendLine("       ,cm_location c3																										 ");
            //query.AppendLine(" where 1 = 1																													 ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            //query.AppendLine(" order by c1.ord																												 ");
            //query.AppendLine(")																																 ");
            //query.AppendLine(",sub_loc as																													 ");
            //query.AppendLine("(																																 ");
            //query.AppendLine("select c1.sgccd																								                 ");
            //query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) loc_code	     ");
            //query.AppendLine("      ,c1.ftr_idn                                                                                                              ");
            //query.AppendLine("  from cm_location c1																											 ");
            //query.AppendLine(" where 1 = 1																													 ");
            //query.AppendLine("   and (c1.ftr_code = 'BZ002' or (c1.ftr_code = 'BZ003'	and c1.kt_gbn = '003'))												 ");
            //query.AppendLine(" start with c1.loc_code = :LOC_CODE																							 ");
            //query.AppendLine("connect by prior c1.loc_code = c1.ploc_code																					 ");
            //query.AppendLine(")																																 ");
            //query.AppendLine("select a.jungsu_out_day_flow																									 ");
            //query.AppendLine("      ,a.jungsu_out_month_flow																								 ");
            //query.AppendLine("      ,a.jungsu_out_day_leakage																								 ");
            //query.AppendLine("      ,a.jungsu_out_month_leakage																								 ");
            //query.AppendLine("      ,a.jungsu_ratio																											 ");
            //query.AppendLine("      ,b.total_in_day_flow																									 ");
            //query.AppendLine("      ,b.total_in_month_flow																									 ");
            //query.AppendLine("      ,b.total_in_day_leakage																									 ");
            //query.AppendLine("      ,b.total_in_month_leakage																								 ");
            //query.AppendLine("      ,b.total_ratio																											 ");
            //query.AppendLine("  from																														 ");
            //query.AppendLine("      (																														 ");
            //query.AppendLine("      select sum(decode(iay.timestamp, to_date(:ENDDATE,'yyyymmdd'), value)) jungsu_out_day_flow								 ");
            //query.AppendLine("            ,sum(value) jungsu_out_month_flow																					 ");
            //query.AppendLine("            ,'평균' jungsu_out_day_leakage																					 ");
            //query.AppendLine("            ,'' jungsu_out_month_leakage																						 ");
            //query.AppendLine("            ,'' jungsu_ratio																									 ");
            //query.AppendLine("        from loc																												 ");
            //query.AppendLine("            ,if_ihtags iih																									 ");
            //query.AppendLine("            ,if_tag_gbn itg																									 ");
            //query.AppendLine("            ,if_accumulation_yesterday iay																					 ");
            //query.AppendLine("       where iih.loc_code = loc.tag_loc_code																					 ");
            //query.AppendLine("         and itg.tagname = iih.tagname																						 ");
            //query.AppendLine("         and itg.tag_gbn = 'YD'																								 ");
            //query.AppendLine("         and iay.tagname = itg.tagname																						 ");
            //query.AppendLine("         and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')							 ");
            //query.AppendLine("      ) a																														 ");
            //query.AppendLine("      ,(																														 ");
            //query.AppendLine("      select sum(decode(iay.timestamp, to_date(:ENDDATE,'yyyymmdd'), value)) total_in_day_flow								 ");
            //query.AppendLine("            ,sum(value) total_in_month_flow																					 ");
            //query.AppendLine("            ,floor(sum(value) /																								 ");
            //query.AppendLine("            (to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 1)) total_in_day_leakage							 ");
            //query.AppendLine("            ,'' total_in_month_leakage																						 ");
            //query.AppendLine("            ,'' total_ratio																									 ");
            //query.AppendLine("        from sub_loc loc																										 ");
            //query.AppendLine("            ,if_ihtags iih																									 ");
            //query.AppendLine("            ,if_tag_gbn itg																									 ");
            //query.AppendLine("            ,if_accumulation_yesterday iay																					 ");
            //query.AppendLine("       where iih.loc_code = loc.loc_code																						 ");
            //query.AppendLine("         and itg.tagname = iih.tagname																						 ");
            //query.AppendLine("         and itg.tag_gbn = 'YD'																								 ");
            //query.AppendLine("         and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                        ");
            //query.AppendLine("         and iay.tagname = itg.tagname																						 ");
            //query.AppendLine("         and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')							 ");
            //query.AppendLine("      ) b																														 ");

            query.AppendLine("WITH LOC AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT C1.SGCCD ");
            query.AppendLine("      ,C1.LOC_CODE ");
            query.AppendLine("      ,DECODE(C1.KT_GBN ");
            query.AppendLine("             ,'002' ");
            query.AppendLine("             ,(SELECT LOC_CODE FROM CM_LOCATION WHERE PLOC_CODE = C1.LOC_CODE) ");
            query.AppendLine("             ,C1.LOC_CODE) TAG_LOC_CODE ");
            query.AppendLine("      ,C1.ORD ");
            query.AppendLine("      ,C1.KT_GBN ");
            query.AppendLine("  FROM ");
            query.AppendLine("      ( ");
            query.AppendLine("       SELECT SGCCD ");
            query.AppendLine("             ,LOC_CODE ");
            query.AppendLine("             ,PLOC_CODE ");
            query.AppendLine("             ,KT_GBN ");
            query.AppendLine("             ,ROWNUM ORD ");
            query.AppendLine("         FROM CM_LOCATION2 ");
            query.AppendLine("        WHERE 1 = 1 ");
            query.AppendLine("          AND LOC_CODE = :LOC_CODE ");
            query.AppendLine("        START WITH FTR_CODE = 'BZ001' ");
            query.AppendLine("        CONNECT BY PRIOR LOC_CODE = PLOC_CODE ");
            query.AppendLine("        ORDER SIBLINGS BY FTR_IDN ");
            query.AppendLine("      ) C1 ");
            query.AppendLine("       ,CM_LOCATION C2 ");
            query.AppendLine("       ,CM_LOCATION C3 ");
            query.AppendLine(" WHERE 1 = 1 ");
            query.AppendLine("   AND C1.PLOC_CODE = C2.LOC_CODE(+) ");
            query.AppendLine("   AND C2.PLOC_CODE = C3.LOC_CODE(+) ");
            query.AppendLine(" ORDER BY C1.ORD ");
            query.AppendLine(")  ");
            query.AppendLine(",SUB_LOC AS ");
            query.AppendLine("(  ");
            query.AppendLine("SELECT A.SGCCD ");
            query.AppendLine("      ,DECODE(A.KT_GBN, '002' ");
            query.AppendLine("            ,(SELECT LOC_CODE FROM CM_LOCATION WHERE PLOC_CODE = A.LOC_CODE) ");
            query.AppendLine("            ,A.LOC_CODE) LOC_CODE ");
            query.AppendLine("      ,A.KT_GBN ");
            query.AppendLine("  FROM ");
            query.AppendLine("      ( ");
            query.AppendLine("       SELECT SGCCD ");
            query.AppendLine("             ,LOC_CODE ");
            query.AppendLine("             ,PLOC_CODE ");
            query.AppendLine("             ,KT_GBN ");
            query.AppendLine("             ,ROWNUM ORD ");
            query.AppendLine("         FROM CM_LOCATION2 ");
            query.AppendLine("        WHERE 1 = 1 ");
            query.AppendLine("        START WITH LOC_CODE = :LOC_CODE ");
            query.AppendLine("        CONNECT BY PRIOR LOC_CODE = PLOC_CODE ");
            query.AppendLine("        ORDER SIBLINGS BY FTR_IDN ");
            query.AppendLine("      ) A ");
            query.AppendLine("      ,WV_SYSTEM_MASTER B ");
            query.AppendLine(" WHERE 1 = 1 ");
            query.AppendLine("   AND A.LOC_CODE = B.LOC_CODE ");
            query.AppendLine(" UNION ALL ");
            query.AppendLine(" SELECT SGCCD, LOC_CODE, KT_GBN FROM CM_LOCATION2 WHERE KT_GBN = '003'");
            query.AppendLine(") ");
            query.AppendLine("SELECT A.JUNGSU_OUT_DAY_FLOW ");
            query.AppendLine("      ,A.JUNGSU_OUT_MONTH_FLOW ");
            query.AppendLine("      ,A.JUNGSU_OUT_DAY_LEAKAGE ");
            query.AppendLine("      ,A.JUNGSU_OUT_MONTH_LEAKAGE ");
            query.AppendLine("      ,A.JUNGSU_RATIO ");
            query.AppendLine("      ,B.TOTAL_IN_DAY_FLOW ");
            query.AppendLine("      ,B.TOTAL_IN_MONTH_FLOW ");
            query.AppendLine("      ,B.TOTAL_IN_DAY_LEAKAGE ");
            query.AppendLine("      ,B.TOTAL_IN_MONTH_LEAKAGE ");
            query.AppendLine("      ,B.TOTAL_RATIO ");
            query.AppendLine("  FROM ");
            query.AppendLine("      ( ");
            query.AppendLine("      SELECT SUM(DECODE(TO_CHAR(IAY.TIMESTAMP - 1/24,'YYYYMMDD'), :ENDDATE, VALUE)) JUNGSU_OUT_DAY_FLOW ");
            query.AppendLine("            ,SUM(VALUE) JUNGSU_OUT_MONTH_FLOW ");
            query.AppendLine("            ,'평균' JUNGSU_OUT_DAY_LEAKAGE ");
            query.AppendLine("            ,'' JUNGSU_OUT_MONTH_LEAKAGE ");
            query.AppendLine("            ,'' JUNGSU_RATIO ");
            query.AppendLine("        FROM LOC ");
            query.AppendLine("            ,IF_IHTAGS IIH ");
            query.AppendLine("            ,IF_TAG_GBN ITG ");
            query.AppendLine("            ,IF_ACCUMULATION_HOUR IAY ");
            query.AppendLine("       WHERE IIH.LOC_CODE = LOC.TAG_LOC_CODE ");
            query.AppendLine("         AND ITG.TAGNAME = IIH.TAGNAME ");
            query.AppendLine("         AND ITG.TAG_GBN = 'FRQ' ");
            query.AppendLine("         AND IAY.TAGNAME = ITG.TAGNAME ");
            query.AppendLine("         AND IAY.TIMESTAMP BETWEEN TO_DATE(:STARTDATE||'01','YYYYMMDDHH24') AND TO_DATE(:ENDDATE,'YYYYMMDD') + 1 ");
            query.AppendLine("      ) A ");
            query.AppendLine("      ,( ");
            query.AppendLine("      SELECT SUM(DECODE(TO_CHAR(IAY.TIMESTAMP - 1/24,'YYYYMMDD'), :ENDDATE, VALUE)) TOTAL_IN_DAY_FLOW ");
            query.AppendLine("            ,SUM(VALUE) TOTAL_IN_MONTH_FLOW ");
            query.AppendLine("            ,FLOOR(SUM(VALUE) / ");
            query.AppendLine("            (TO_DATE(:ENDDATE,'YYYYMMDD') - TO_DATE(:STARTDATE,'YYYYMMDD') + 1)) TOTAL_IN_DAY_LEAKAGE ");
            query.AppendLine("            ,'' TOTAL_IN_MONTH_LEAKAGE ");
            query.AppendLine("            ,'' TOTAL_RATIO ");
            query.AppendLine("        FROM SUB_LOC LOC ");
            query.AppendLine("            ,IF_IHTAGS IIH ");
            query.AppendLine("            ,IF_TAG_GBN ITG ");
            query.AppendLine("            ,IF_ACCUMULATION_HOUR IAY ");
            query.AppendLine("       WHERE IIH.LOC_CODE = LOC.LOC_CODE ");
            query.AppendLine("         AND ITG.TAGNAME = IIH.TAGNAME ");
            query.AppendLine("         AND ITG.TAG_GBN = DECODE(LOC.KT_GBN, '001', 'SPL_D', 'FRQ') ");
            query.AppendLine("         AND ITG.TAGNAME NOT IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'YD_R') ");
            query.AppendLine("         AND IAY.TAGNAME = ITG.TAGNAME ");
            query.AppendLine("         AND IAY.TIMESTAMP BETWEEN TO_DATE(:STARTDATE||'01','YYYYMMDDHH24') AND TO_DATE(:ENDDATE,'YYYYMMDD') + 1 ");
            query.AppendLine("      ) B ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];
            parameters[5].Value = parameter["ENDDATE"];
            parameters[6].Value = parameter["ENDDATE"];
            parameters[7].Value = parameter["STARTDATE"];
            parameters[8].Value = parameter["STARTDATE"];
            parameters[9].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //관망운영일보 중블록 정보
        public void SelectDailyReport_MiddleInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                    ");
            //query.AppendLine("(																																 ");
            //query.AppendLine("select c1.loc_code																											 ");
            //query.AppendLine("      ,c1.sgccd																												 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock								 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock								 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock										 ");
            //query.AppendLine("      ,c1.ftr_code																											 ");
            //query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code	 ");
            //query.AppendLine("      ,c1.kt_gbn																												 ");
            //query.AppendLine("      ,c1.loc_name																											 ");
            //query.AppendLine("      ,decode(c1.kt_gbn, '002', '면지역', c1.loc_name) middle_name                                                             ");
            //query.AppendLine("      ,c1.ord																													 ");
            //query.AppendLine("  from																														 ");
            //query.AppendLine("      (																														 ");
            //query.AppendLine("       select sgccd																											 ");
            //query.AppendLine("             ,loc_code																										 ");
            //query.AppendLine("             ,ploc_code																										 ");
            //query.AppendLine("             ,loc_name																										 ");
            //query.AppendLine("             ,ftr_idn																											 ");
            //query.AppendLine("             ,ftr_code																										 ");
            //query.AppendLine("             ,kt_gbn																											 ");
            //query.AppendLine("             ,rownum ord																										 ");
            //query.AppendLine("         from cm_location																										 ");
            //query.AppendLine("        where 1 = 1																											 ");
            //query.AppendLine("          and ftr_code = 'BZ002'																								 ");
            //query.AppendLine("        start with loc_code = :LOC_CODE																						 ");
            //query.AppendLine("        connect by prior loc_code = ploc_code																					 ");
            //query.AppendLine("        order SIBLINGS by ftr_idn																								 ");
            //query.AppendLine("      ) c1																													 ");
            //query.AppendLine("       ,cm_location c2																										 ");
            //query.AppendLine("       ,cm_location c3																										 ");
            //query.AppendLine(" where 1 = 1																													 ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            //query.AppendLine(" order by c1.ord																												 ");
            //query.AppendLine(")																																 ");
            //query.AppendLine(",sub_loc as																													 ");
            //query.AppendLine("(																																 ");
            //query.AppendLine("select a.loc_code																												 ");
            //query.AppendLine("      ,(select decode(kt_gbn, '002', '면지역', loc_name) from cm_location where loc_code = a.ploc_code) loc_name				 ");
            //query.AppendLine("  from cm_location a																											 ");
            //query.AppendLine(" where 1 = 1																													 ");
            //query.AppendLine("   and a.ftr_code = 'BZ003'																									 ");
            //query.AppendLine("   and (a.kt_gbn != '003'																										 ");
            //query.AppendLine("    or a.kt_gbn is null)																										 ");
            //query.AppendLine(" start with a.loc_code = :LOC_CODE																							 ");
            //query.AppendLine("connect by prior a.loc_code = a.ploc_code																						 ");
            //query.AppendLine(")																																 ");
            //query.AppendLine("select middle_name																											 ");
            //query.AppendLine("      ,branchoff_day_flow																										 ");
            //query.AppendLine("      ,branchoff_month_flow																									 ");
            //query.AppendLine("      ,branchoff_day_leakage																									 ");
            //query.AppendLine("      ,branchoff_month_leakage																								 ");
            //query.AppendLine("      ,branchoff_ratio																										 ");
            //query.AppendLine("      ,reservoir_day_flow																										 ");
            //query.AppendLine("      ,reservoir_month_flow																									 ");
            //query.AppendLine("      ,(branchoff_day_flow - reservoir_day_flow) reservoir_day_leakage														 ");
            //query.AppendLine("      ,(branchoff_month_flow - reservoir_month_flow) reservoir_month_leakage													 ");
            //query.AppendLine("      ,round(((decode(branchoff_day_flow, 0, 0, reservoir_day_flow/branchoff_day_flow))*100),1) reservoir_ratio													 ");
            //query.AppendLine("      ,block_day_flow																											 ");
            //query.AppendLine("      ,block_month_flow																										 ");
            //query.AppendLine("      ,(reservoir_day_flow - block_day_flow) block_day_leakage																 ");
            //query.AppendLine("      ,(reservoir_month_flow - block_month_flow) block_month_leakage															 ");
            //query.AppendLine("      ,round(((decode(reservoir_day_flow, 0, 0, block_day_flow/reservoir_day_flow))*100),1) block_ratio															 ");
            //query.AppendLine("																																 ");
            //query.AppendLine("  from																														 ");
            //query.AppendLine("      (																														 ");
            //query.AppendLine("      select loc.middle_name																									 ");
            //query.AppendLine("            ,max(a.branchoff_day_flow) branchoff_day_flow																		 ");
            //query.AppendLine("            ,max(a.branchoff_month_flow) branchoff_month_flow																	 ");
            //query.AppendLine("            ,max(a.branchoff_day_leakage) branchoff_day_leakage																 ");
            //query.AppendLine("            ,max(a.branchoff_month_leakage) branchoff_month_leakage															 ");
            //query.AppendLine("            ,max(a.branchoff_ratio) branchoff_ratio																			 ");
            //query.AppendLine("            ,max(b.reservoir_day_flow) reservoir_day_flow																		 ");
            //query.AppendLine("            ,max(b.reservoir_month_flow) reservoir_month_flow																	 ");
            //query.AppendLine("            ,max(b.reservoir_day_leakage) reservoir_day_leakage																 ");
            //query.AppendLine("            ,max(b.reservoir_month_leakage) reservoir_month_leakage															 ");
            //query.AppendLine("            ,max(b.reservoir_ratio) reservoir_ratio																			 ");
            //query.AppendLine("            ,max(c.block_day_flow) block_day_flow																				 ");
            //query.AppendLine("            ,max(c.block_month_flow) block_month_flow																			 ");
            //query.AppendLine("            ,max(c.block_day_leakage) block_day_leakage																		 ");
            //query.AppendLine("            ,max(c.block_month_leakage) block_month_leakage																	 ");
            //query.AppendLine("            ,max(c.block_ratio) block_ratio																					 ");
            //query.AppendLine("        from																													 ");
            //query.AppendLine("            (																													 ");
            //query.AppendLine("            select loc.middle_name												                                             ");
            //query.AppendLine("                  ,sum(decode(iay.timestamp, to_date(:ENDDATE,'yyyymmdd'), value)) branchoff_day_flow							 ");
            //query.AppendLine("                  ,sum(value) branchoff_month_flow																			 ");
            //query.AppendLine("                  ,'' branchoff_day_leakage																					 ");
            //query.AppendLine("                  ,'' branchoff_month_leakage																					 ");
            //query.AppendLine("                  ,'' branchoff_ratio																							 ");
            //query.AppendLine("              from loc																										 ");
            //query.AppendLine("                  ,if_ihtags iih																								 ");
            //query.AppendLine("                  ,if_tag_gbn itg																								 ");
            //query.AppendLine("                  ,if_accumulation_yesterday iay																				 ");
            //query.AppendLine("             where iih.loc_code = loc.tag_loc_code																			 ");
            //query.AppendLine("               and itg.tagname = iih.tagname																					 ");
            //query.AppendLine("               and itg.tag_gbn = 'YD'																							 ");
            //query.AppendLine("               and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                  ");
            //query.AppendLine("               and iay.tagname = itg.tagname																					 ");
            //query.AppendLine("               and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')						 ");
            //query.AppendLine("             group by loc.middle_name																		                     ");
            //query.AppendLine("            ) a																												 ");
            //query.AppendLine("            ,(																												 ");
            //query.AppendLine("            select decode(loc.kt_gbn, '002', '면지역', loc.loc_name) middle_name												 ");
            //query.AppendLine("                  ,decode(loc.kt_gbn, '002', null, sum(decode(iay.timestamp, to_date(:ENDDATE,'yyyymmdd'), value))) reservoir_day_flow							 ");
            //query.AppendLine("                  ,decode(loc.kt_gbn, '002', null, sum(value)) reservoir_month_flow											 ");
            //query.AppendLine("                  ,'' reservoir_day_leakage																					 ");
            //query.AppendLine("                  ,'' reservoir_month_leakage																					 ");
            //query.AppendLine("                  ,'' reservoir_ratio																							 ");
            //query.AppendLine("              from loc																										 ");
            //query.AppendLine("                  ,if_ihtags iih																								 ");
            //query.AppendLine("                  ,if_tag_gbn itg																								 ");
            //query.AppendLine("                  ,if_accumulation_yesterday iay																				 ");
            //query.AppendLine("             where iih.loc_code = loc.loc_code																				 ");
            //query.AppendLine("               and itg.tagname = iih.tagname																					 ");
            //query.AppendLine("               and itg.tag_gbn = 'YD_R'																						 ");
            //query.AppendLine("               and iay.tagname = itg.tagname																					 ");
            //query.AppendLine("               and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')						 ");
            //query.AppendLine("             group by loc.loc_name, loc.kt_gbn																				 ");
            //query.AppendLine("            ) b																												 ");
            //query.AppendLine("            ,(																												 ");
            //query.AppendLine("            select loc.loc_name middle_name																					 ");
            //query.AppendLine("                  ,sum(decode(iay.timestamp, to_date(:ENDDATE,'yyyymmdd'), value)) block_day_flow								 ");
            //query.AppendLine("                  ,sum(value) block_month_flow																				 ");
            //query.AppendLine("                  ,'' block_day_leakage																						 ");
            //query.AppendLine("                  ,'' block_month_leakage																						 ");
            //query.AppendLine("                  ,'' block_ratio																								 ");
            //query.AppendLine("              from sub_loc loc																								 ");
            //query.AppendLine("                  ,if_ihtags iih																								 ");
            //query.AppendLine("                  ,if_tag_gbn itg																								 ");
            //query.AppendLine("                  ,if_accumulation_yesterday iay																				 ");
            //query.AppendLine("             where iih.loc_code = loc.loc_code																				 ");
            //query.AppendLine("               and itg.tagname = iih.tagname																					 ");
            //query.AppendLine("               and itg.tag_gbn = 'YD'																							 ");
            //query.AppendLine("               and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                  ");
            //query.AppendLine("               and iay.tagname = itg.tagname																					 ");
            //query.AppendLine("               and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')						 ");
            //query.AppendLine("             group by loc.loc_name																							 ");
            //query.AppendLine("            ) c																												 ");
            //query.AppendLine("            ,loc loc                                                                                                           ");
            //query.AppendLine("       where a.middle_name(+) = loc.middle_name																			     ");
            //query.AppendLine("         and b.middle_name(+) = loc.middle_name																				 ");
            //query.AppendLine("         and c.middle_name(+) = loc.middle_name																				 ");
            //query.AppendLine("       group by loc.middle_name																								 ");
            //query.AppendLine("      ) a																														 ");
            //query.AppendLine(" order by (select orderby from cm_location where loc_name = a.middle_name) ");

            //IDataParameter[] parameters =  {
            //         new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            //         ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            //         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
            //         ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["LOC_CODE"];
            //parameters[1].Value = parameter["LOC_CODE"];
            //parameters[2].Value = parameter["ENDDATE"];
            //parameters[3].Value = parameter["STARTDATE"];
            //parameters[4].Value = parameter["ENDDATE"];
            //parameters[5].Value = parameter["ENDDATE"];
            //parameters[6].Value = parameter["STARTDATE"];
            //parameters[7].Value = parameter["ENDDATE"];
            //parameters[8].Value = parameter["ENDDATE"];
            //parameters[9].Value = parameter["STARTDATE"];
            //parameters[10].Value = parameter["ENDDATE"];


            query.AppendLine("WITH LOC AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT A.SGCCD ");
            query.AppendLine("      ,A.LOC_CODE ");
            query.AppendLine("      ,DECODE(A.KT_GBN, '002'  ");
            query.AppendLine("            ,(SELECT LOC_CODE FROM CM_LOCATION WHERE PLOC_CODE = A.LOC_CODE)  ");
            query.AppendLine("            ,A.LOC_CODE) TAG_LOC_CODE  ");
            query.AppendLine("      ,B.SYSTEM_NAME MIDDLE_NAME ");
            query.AppendLine("      ,A.KT_GBN ");
            query.AppendLine("      ,B.ORDERBY ");
            query.AppendLine("  FROM  ");
            query.AppendLine("      (  ");
            query.AppendLine("       SELECT SGCCD  ");
            query.AppendLine("             ,LOC_CODE  ");
            query.AppendLine("             ,PLOC_CODE  ");
            query.AppendLine("             ,KT_GBN  ");
            query.AppendLine("             ,ROWNUM ORD  ");
            query.AppendLine("         FROM CM_LOCATION2  ");
            query.AppendLine("        WHERE 1 = 1  ");
            query.AppendLine("        START WITH LOC_CODE = :LOC_CODE  ");
            query.AppendLine("        CONNECT BY PRIOR LOC_CODE = PLOC_CODE  ");
            query.AppendLine("        ORDER SIBLINGS BY FTR_IDN  ");
            query.AppendLine("      ) A  ");
            query.AppendLine("      ,WV_SYSTEM_MASTER B  ");
            query.AppendLine(" WHERE 1 = 1  ");
            query.AppendLine("   AND A.LOC_CODE = B.LOC_CODE  ");
            query.AppendLine(") ");
            query.AppendLine(",SUB_LOC AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT B.LOC_CODE ");
            query.AppendLine("      ,A.SYSTEM_NAME LOC_NAME ");
            query.AppendLine("  FROM WV_SYSTEM_MASTER A ");
            query.AppendLine("      ,WV_SYSTEM_MAPPING B ");
            query.AppendLine("      ,(  ");
            query.AppendLine("       SELECT SGCCD  ");
            query.AppendLine("             ,LOC_CODE  ");
            query.AppendLine("             ,PLOC_CODE  ");
            query.AppendLine("             ,KT_GBN  ");
            query.AppendLine("             ,ROWNUM ORD  ");
            query.AppendLine("         FROM CM_LOCATION2  ");
            query.AppendLine("        WHERE 1 = 1  ");
            query.AppendLine("          AND (KT_GBN != '003' OR KT_GBN IS NULL)");
            query.AppendLine("        START WITH LOC_CODE = :LOC_CODE  ");
            query.AppendLine("        CONNECT BY PRIOR LOC_CODE = PLOC_CODE  ");
            query.AppendLine("        ORDER SIBLINGS BY FTR_IDN  ");
            query.AppendLine("      ) C  ");
            query.AppendLine(" WHERE 1 = 1 ");
            query.AppendLine("   AND B.SYSTEM_CODE = A.SYSTEM_CODE ");
            query.AppendLine("   AND C.LOC_CODE = B.LOC_CODE ");
            query.AppendLine(" GROUP BY B.LOC_CODE, A.SYSTEM_NAME ");
            query.AppendLine(") ");
            query.AppendLine("SELECT MIDDLE_NAME ");
            query.AppendLine("      ,BRANCHOFF_DAY_FLOW ");
            query.AppendLine("      ,BRANCHOFF_MONTH_FLOW ");
            query.AppendLine("      ,BRANCHOFF_DAY_LEAKAGE ");
            query.AppendLine("      ,BRANCHOFF_MONTH_LEAKAGE ");
            query.AppendLine("      ,BRANCHOFF_RATIO ");
            query.AppendLine("      ,RESERVOIR_DAY_FLOW ");
            query.AppendLine("      ,RESERVOIR_MONTH_FLOW ");
            query.AppendLine("      ,(BRANCHOFF_DAY_FLOW - RESERVOIR_DAY_FLOW) RESERVOIR_DAY_LEAKAGE ");
            query.AppendLine("      ,(BRANCHOFF_MONTH_FLOW - RESERVOIR_MONTH_FLOW) RESERVOIR_MONTH_LEAKAGE ");
            query.AppendLine("      ,ROUND(((DECODE(BRANCHOFF_DAY_FLOW, 0, 0, RESERVOIR_DAY_FLOW/BRANCHOFF_DAY_FLOW))*100),1) RESERVOIR_RATIO ");
            query.AppendLine("      ,BLOCK_DAY_FLOW ");
            query.AppendLine("      ,BLOCK_MONTH_FLOW ");
            query.AppendLine("      ,(RESERVOIR_DAY_FLOW - BLOCK_DAY_FLOW) BLOCK_DAY_LEAKAGE ");
            query.AppendLine("      ,(RESERVOIR_MONTH_FLOW - BLOCK_MONTH_FLOW) BLOCK_MONTH_LEAKAGE ");
            query.AppendLine("      ,ROUND(((DECODE(RESERVOIR_DAY_FLOW, 0, 0, BLOCK_DAY_FLOW/RESERVOIR_DAY_FLOW))*100),1) BLOCK_RATIO ");
            query.AppendLine("  FROM ");
            query.AppendLine("      ( ");
            query.AppendLine("      SELECT LOC.MIDDLE_NAME ");
            query.AppendLine("            ,MAX(A.BRANCHOFF_DAY_FLOW) BRANCHOFF_DAY_FLOW ");
            query.AppendLine("            ,MAX(A.BRANCHOFF_MONTH_FLOW) BRANCHOFF_MONTH_FLOW ");
            query.AppendLine("            ,MAX(A.BRANCHOFF_DAY_LEAKAGE) BRANCHOFF_DAY_LEAKAGE ");
            query.AppendLine("            ,MAX(A.BRANCHOFF_MONTH_LEAKAGE) BRANCHOFF_MONTH_LEAKAGE ");
            query.AppendLine("            ,MAX(A.BRANCHOFF_RATIO) BRANCHOFF_RATIO ");
            query.AppendLine("            ,MAX(B.RESERVOIR_DAY_FLOW) RESERVOIR_DAY_FLOW ");
            query.AppendLine("            ,MAX(B.RESERVOIR_MONTH_FLOW) RESERVOIR_MONTH_FLOW ");
            query.AppendLine("            ,MAX(B.RESERVOIR_DAY_LEAKAGE) RESERVOIR_DAY_LEAKAGE ");
            query.AppendLine("            ,MAX(B.RESERVOIR_MONTH_LEAKAGE) RESERVOIR_MONTH_LEAKAGE ");
            query.AppendLine("            ,MAX(B.RESERVOIR_RATIO) RESERVOIR_RATIO ");
            query.AppendLine("            ,MAX(C.BLOCK_DAY_FLOW) BLOCK_DAY_FLOW ");
            query.AppendLine("            ,MAX(C.BLOCK_MONTH_FLOW) BLOCK_MONTH_FLOW ");
            query.AppendLine("            ,MAX(C.BLOCK_DAY_LEAKAGE) BLOCK_DAY_LEAKAGE ");
            query.AppendLine("            ,MAX(C.BLOCK_MONTH_LEAKAGE) BLOCK_MONTH_LEAKAGE ");
            query.AppendLine("            ,MAX(C.BLOCK_RATIO) BLOCK_RATIO ");
            query.AppendLine("        FROM ");
            query.AppendLine("            ( ");
            query.AppendLine("            SELECT LOC.MIDDLE_NAME ");
            query.AppendLine("                  ,SUM(DECODE(TO_CHAR(IAY.TIMESTAMP - 1/24,'YYYYMMDD'), :ENDDATE, VALUE)) BRANCHOFF_DAY_FLOW");
            query.AppendLine("                  ,SUM(VALUE) BRANCHOFF_MONTH_FLOW ");
            query.AppendLine("                  ,'' BRANCHOFF_DAY_LEAKAGE ");
            query.AppendLine("                  ,'' BRANCHOFF_MONTH_LEAKAGE ");
            query.AppendLine("                  ,'' BRANCHOFF_RATIO ");
            query.AppendLine("              FROM LOC ");
            query.AppendLine("                  ,IF_IHTAGS IIH ");
            query.AppendLine("                  ,IF_TAG_GBN ITG ");
            query.AppendLine("                  ,IF_ACCUMULATION_HOUR IAY ");
            query.AppendLine("             WHERE IIH.LOC_CODE = LOC.TAG_LOC_CODE ");
            query.AppendLine("               AND ITG.TAGNAME = IIH.TAGNAME ");
            query.AppendLine("               AND ITG.TAG_GBN = DECODE(LOC.KT_GBN, '001', 'SPL_D', 'FRQ') ");
            query.AppendLine("               AND iTG.TAGNAME NOT IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'YD_R') ");
            query.AppendLine("               AND IAY.TAGNAME = ITG.TAGNAME ");
            query.AppendLine("               AND IAY.TIMESTAMP BETWEEN TO_DATE(:STARTDATE||'01','YYYYMMDDHH24') AND TO_DATE(:ENDDATE,'YYYYMMDD') + 1 ");
            query.AppendLine("             GROUP BY LOC.MIDDLE_NAME ");
            query.AppendLine("            ) A ");
            query.AppendLine("            ,( ");
            query.AppendLine("            SELECT LOC.MIDDLE_NAME MIDDLE_NAME ");
            query.AppendLine("                  ,DECODE(LOC.KT_GBN, '002', NULL, SUM(DECODE(TO_CHAR(IAY.TIMESTAMP - 1/24,'YYYYMMDD'), :ENDDATE, VALUE))) RESERVOIR_DAY_FLOW ");
            query.AppendLine("                  ,DECODE(LOC.KT_GBN, '002', NULL, SUM(VALUE)) RESERVOIR_MONTH_FLOW ");
            query.AppendLine("                  ,'' RESERVOIR_DAY_LEAKAGE ");
            query.AppendLine("                  ,'' RESERVOIR_MONTH_LEAKAGE ");
            query.AppendLine("                  ,'' RESERVOIR_RATIO ");
            query.AppendLine("              FROM LOC ");
            query.AppendLine("                  ,IF_IHTAGS IIH ");
            query.AppendLine("                  ,IF_TAG_GBN ITG ");
            query.AppendLine("                  ,IF_ACCUMULATION_HOUR IAY ");
            query.AppendLine("             WHERE IIH.LOC_CODE = LOC.TAG_LOC_CODE ");
            query.AppendLine("               AND ITG.TAGNAME = IIH.TAGNAME ");
            query.AppendLine("               AND ITG.TAG_GBN = 'SPL_O' ");
            query.AppendLine("               AND IAY.TAGNAME = ITG.TAGNAME ");
            query.AppendLine("               AND IAY.TIMESTAMP BETWEEN TO_DATE(:STARTDATE||'01','YYYYMMDDHH24') AND TO_DATE(:ENDDATE,'YYYYMMDD') + 1 ");
            query.AppendLine("             GROUP BY LOC.MIDDLE_NAME, LOC.KT_GBN ");
            query.AppendLine("            ) B ");
            query.AppendLine("            ,( ");
            query.AppendLine("            SELECT LOC.LOC_NAME MIDDLE_NAME ");
            query.AppendLine("                  ,SUM(DECODE(TO_CHAR(IAY.TIMESTAMP - 1/24,'YYYYMMDD'), :ENDDATE, VALUE)) BLOCK_DAY_FLOW ");
            query.AppendLine("                  ,SUM(VALUE) BLOCK_MONTH_FLOW ");
            query.AppendLine("                  ,'' BLOCK_DAY_LEAKAGE ");
            query.AppendLine("                  ,'' BLOCK_MONTH_LEAKAGE ");
            query.AppendLine("                  ,'' BLOCK_RATIO ");
            query.AppendLine("              FROM SUB_LOC LOC ");
            query.AppendLine("                  ,IF_IHTAGS IIH ");
            query.AppendLine("                  ,IF_TAG_GBN ITG ");
            query.AppendLine("                  ,IF_ACCUMULATION_HOUR IAY ");
            query.AppendLine("             WHERE IIH.LOC_CODE = LOC.LOC_CODE ");
            query.AppendLine("               AND ITG.TAGNAME = IIH.TAGNAME ");
            query.AppendLine("               AND ITG.TAG_GBN = 'FRQ' ");
            query.AppendLine("               AND ITG.TAGNAME NOT IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'YD_R') ");
            query.AppendLine("               AND IAY.TAGNAME = ITG.TAGNAME ");
            query.AppendLine("               AND IAY.TIMESTAMP BETWEEN TO_DATE(:STARTDATE||'01','YYYYMMDDHH24') AND TO_DATE(:ENDDATE,'YYYYMMDD') + 1");
            query.AppendLine("             GROUP BY LOC.LOC_NAME ");
            query.AppendLine("            ) C ");
            query.AppendLine("            ,LOC LOC ");
            query.AppendLine("       WHERE A.MIDDLE_NAME(+) = LOC.MIDDLE_NAME ");
            query.AppendLine("         AND B.MIDDLE_NAME(+) = LOC.MIDDLE_NAME ");
            query.AppendLine("         AND C.MIDDLE_NAME(+) = LOC.MIDDLE_NAME ");
            query.AppendLine("       GROUP BY LOC.MIDDLE_NAME ");
            query.AppendLine("      ) A ");
            query.AppendLine(" ORDER BY (SELECT MAX(ORDERBY) FROM WV_SYSTEM_MASTER WHERE SYSTEM_NAME = A.MIDDLE_NAME) ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];
            parameters[5].Value = parameter["ENDDATE"];
            parameters[6].Value = parameter["STARTDATE"];
            parameters[7].Value = parameter["ENDDATE"];
            parameters[8].Value = parameter["ENDDATE"];
            parameters[9].Value = parameter["STARTDATE"];
            parameters[10].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //관망운영일보 소블록 정보
        public void SelectDailyReport_SmallInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                    ");
            //query.AppendLine("(																																 ");
            //query.AppendLine("select c1.loc_code																											 ");
            //query.AppendLine("      ,c1.sgccd																												 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock								 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock								 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock										 ");
            //query.AppendLine("      ,c1.ftr_code																											 ");
            //query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code	 ");
            //query.AppendLine("      ,c1.kt_gbn																												 ");
            //query.AppendLine("      ,(select decode(kt_gbn, '002', '면지역', loc_name) from cm_location where loc_code = c1.ploc_code) ploc_name			 ");
            //query.AppendLine("      ,c1.loc_name																											 ");
            //query.AppendLine("      ,c1.ord																													 ");
            //query.AppendLine("  from																														 ");
            //query.AppendLine("      (																														 ");
            //query.AppendLine("       select sgccd																											 ");
            //query.AppendLine("             ,loc_code																										 ");
            //query.AppendLine("             ,ploc_code																										 ");
            //query.AppendLine("             ,loc_name																										 ");
            //query.AppendLine("             ,ftr_idn																											 ");
            //query.AppendLine("             ,ftr_code																										 ");
            //query.AppendLine("             ,kt_gbn																											 ");
            //query.AppendLine("             ,rownum ord																										 ");
            //query.AppendLine("         from cm_location																										 ");
            //query.AppendLine("        where 1 = 1																											 ");
            //query.AppendLine("          and ftr_code = 'BZ003'																								 ");
            //query.AppendLine("        start with loc_code = :LOC_CODE																						 ");
            //query.AppendLine("        connect by prior loc_code = ploc_code																					 ");
            //query.AppendLine("        order SIBLINGS by ftr_idn																								 ");
            //query.AppendLine("      ) c1																													 ");
            //query.AppendLine("       ,cm_location c2																										 ");
            //query.AppendLine("       ,cm_location c3																										 ");
            //query.AppendLine(" where 1 = 1																													 ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            //query.AppendLine(" order by c1.ord																												 ");
            //query.AppendLine(")																																 ");
            //query.AppendLine("select middle_name																											 ");
            //query.AppendLine("      ,regexp_replace(small_name, '_(.*)', '') small_name																		 ");
            //query.AppendLine("      ,day_flow																												 ");
            //query.AppendLine("      ,month_flow																												 ");
            //query.AppendLine("  from																														 ");
            //query.AppendLine("      (																														 ");
            //query.AppendLine("      select loc.ploc_name middle_name																						 ");
            //query.AppendLine("            ,loc.loc_name||'_'||loc.kt_gbn small_name																			 ");
            //query.AppendLine("            ,sum(decode(iay.timestamp, to_date(:ENDDATE,'yyyymmdd'), value)) day_flow											 ");
            //query.AppendLine("            ,sum(iay.value) month_flow																						 ");
            //query.AppendLine("            ,loc.kt_gbn																										 ");
            //query.AppendLine("        from loc																												 ");
            //query.AppendLine("            ,if_ihtags iih																									 ");
            //query.AppendLine("            ,if_tag_gbn itg																									 ");
            //query.AppendLine("            ,if_accumulation_yesterday iay																					 ");
            //query.AppendLine("       where 1 = 1																											 ");
            //query.AppendLine("         and iih.loc_code = loc.loc_code																						 ");
            //query.AppendLine("         and itg.tagname = iih.tagname																						 ");
            //query.AppendLine("         and itg.tag_gbn = 'YD'																								 ");
            //query.AppendLine("         and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                        ");
            //query.AppendLine("         and iay.tagname = itg.tagname																						 ");
            //query.AppendLine("         and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')							 ");
            //query.AppendLine("      group by loc.ploc_name, loc.loc_name, loc.kt_gbn																		 ");
            //query.AppendLine("      )																														 ");
            //query.AppendLine(" model																														 ");
            //query.AppendLine(" partition by (middle_name)																									 ");
            //query.AppendLine(" dimension by (small_name)																									 ");
            //query.AppendLine(" measures (day_flow, month_flow)																								 ");
            //query.AppendLine(" ignore nav																													 ");
            //query.AppendLine(" rules ( day_flow['소계'] =																									 ");
            //query.AppendLine("         sum(day_flow)[regexp_replace(small_name, '(.*)_', '') != '003' or regexp_replace(small_name, '(.*)_', '') is null]	 ");
            //query.AppendLine("        ,month_flow['소계'] =																									 ");
            //query.AppendLine("         sum(month_flow)[regexp_replace(small_name, '(.*)_', '') != '003' or regexp_replace(small_name, '(.*)_', '') is null]) ");
            //query.AppendLine(" order by middle_name, decode(small_name, '소계', '0' , small_name)															 ");

            //query.AppendLine("with loc as                                                                                                                    ");
            //query.AppendLine("(																																 ");
            //query.AppendLine("select c1.loc_code																											 ");
            //query.AppendLine("      ,c1.sgccd																												 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock								 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock								 ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))		 ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock										 ");
            //query.AppendLine("      ,c1.ftr_code																											 ");
            //query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location2 where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code	 ");
            //query.AppendLine("      ,c1.kt_gbn																												 ");
            //query.AppendLine("      ,(select decode(kt_gbn, '002', '면지역', loc_name) from cm_location2 where loc_code = c1.ploc_code) ploc_name			 ");
            //query.AppendLine("      ,c1.loc_name																											 ");
            //query.AppendLine("      ,c1.ord																													 ");
            //query.AppendLine("  from																														 ");
            //query.AppendLine("      (																														 ");
            //query.AppendLine("       select sgccd																											 ");
            //query.AppendLine("             ,loc_code																										 ");
            //query.AppendLine("             ,ploc_code																										 ");
            //query.AppendLine("             ,loc_name																										 ");
            //query.AppendLine("             ,ftr_idn																											 ");
            //query.AppendLine("             ,ftr_code																										 ");
            //query.AppendLine("             ,kt_gbn																											 ");
            //query.AppendLine("             ,rownum ord																										 ");
            //query.AppendLine("         from cm_location2																										 ");
            //query.AppendLine("        where 1 = 1																											 ");
            //query.AppendLine("          and ftr_code = 'BZ003'																								 ");
            //query.AppendLine("        start with loc_code = :LOC_CODE																						 ");
            //query.AppendLine("        connect by prior loc_code = ploc_code																					 ");
            //query.AppendLine("        order SIBLINGS by ftr_idn																								 ");
            //query.AppendLine("      ) c1																													 ");
            //query.AppendLine("       ,cm_location2 c2																										 ");
            //query.AppendLine("       ,cm_location2 c3																										 ");
            //query.AppendLine(" where 1 = 1																													 ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																							 ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																							 ");
            //query.AppendLine(" order by c1.ord																												 ");
            //query.AppendLine(")																																 ");
            //query.AppendLine("select middle_name																											 ");
            //query.AppendLine("      ,regexp_replace(small_name, '_(.*)', '') small_name																		 ");
            //query.AppendLine("      ,day_flow																												 ");
            //query.AppendLine("      ,month_flow																												 ");
            //query.AppendLine("  from																														 ");
            //query.AppendLine("      (																														 ");
            //query.AppendLine("      select loc.ploc_name middle_name																						 ");
            //query.AppendLine("            ,loc.loc_name||'_'||loc.kt_gbn small_name																			 ");
            //query.AppendLine("            ,sum(decode(iay.timestamp, to_date(:ENDDATE,'yyyymmdd'), value)) day_flow											 ");
            //query.AppendLine("            ,sum(iay.value) month_flow																						 ");
            //query.AppendLine("            ,loc.kt_gbn																										 ");
            //query.AppendLine("        from loc																												 ");
            //query.AppendLine("            ,if_ihtags iih																									 ");
            //query.AppendLine("            ,if_tag_gbn itg																									 ");
            //query.AppendLine("            ,if_accumulation_yesterday iay																					 ");
            //query.AppendLine("       where 1 = 1																											 ");
            //query.AppendLine("         and iih.loc_code2 = loc.loc_code																						 ");
            //query.AppendLine("         and itg.tagname = iih.tagname																						 ");
            //query.AppendLine("         and itg.tag_gbn = 'YD'																								 ");
            //query.AppendLine("         and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                        ");
            //query.AppendLine("         and iay.tagname = itg.tagname																						 ");
            //query.AppendLine("         and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')							 ");
            //query.AppendLine("      group by loc.ploc_name, loc.loc_name, loc.kt_gbn																		 ");
            //query.AppendLine("      )																														 ");
            //query.AppendLine(" model																														 ");
            //query.AppendLine(" partition by (middle_name)																									 ");
            //query.AppendLine(" dimension by (small_name)																									 ");
            //query.AppendLine(" measures (day_flow, month_flow)																								 ");
            //query.AppendLine(" ignore nav																													 ");
            //query.AppendLine(" rules ( day_flow['소계'] =																									 ");
            //query.AppendLine("         sum(day_flow)[regexp_replace(small_name, '(.*)_', '') != '003' or regexp_replace(small_name, '(.*)_', '') is null]	 ");
            //query.AppendLine("        ,month_flow['소계'] =																									 ");
            //query.AppendLine("         sum(month_flow)[regexp_replace(small_name, '(.*)_', '') != '003' or regexp_replace(small_name, '(.*)_', '') is null]) ");
            //query.AppendLine(" order by middle_name, decode(small_name, '소계', '0' , small_name)															 ");

            query.AppendLine("WITH LOC AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT B.LOC_CODE  ");
            query.AppendLine("      ,A.SYSTEM_NAME PLOC_NAME ");
            query.AppendLine("      ,C.LOC_NAME ");
            query.AppendLine("      ,C.KT_GBN ");
            query.AppendLine("      ,B.ORDERBY ");
            query.AppendLine("  FROM WV_SYSTEM_MASTER A  ");
            query.AppendLine("      ,WV_SYSTEM_MAPPING B  ");
            query.AppendLine("      ,(   ");
            query.AppendLine("       SELECT SGCCD   ");
            query.AppendLine("             ,LOC_CODE ");
            query.AppendLine("             ,LOC_NAME ");
            query.AppendLine("             ,PLOC_CODE   ");
            query.AppendLine("             ,KT_GBN   ");
            query.AppendLine("             ,ROWNUM ORD   ");
            query.AppendLine("         FROM CM_LOCATION2   ");
            query.AppendLine("        WHERE 1 = 1  ");
            query.AppendLine("          AND FTR_CODE = 'BZ003' ");
            query.AppendLine("        START WITH LOC_CODE = :LOC_CODE   ");
            query.AppendLine("        CONNECT BY PRIOR LOC_CODE = PLOC_CODE   ");
            query.AppendLine("        ORDER SIBLINGS BY FTR_IDN   ");
            query.AppendLine("      ) C   ");
            query.AppendLine(" WHERE 1 = 1  ");
            query.AppendLine("   AND A.SYSTEM_CODE = B.SYSTEM_CODE  ");
            query.AppendLine("   AND B.LOC_CODE = C.LOC_CODE  ");
            query.AppendLine(" GROUP BY B.LOC_CODE, A.SYSTEM_NAME, C.LOC_NAME, C.KT_GBN, B.ORDERBY ");
            query.AppendLine(") ");
            query.AppendLine("SELECT MIDDLE_NAME ");
            query.AppendLine("      ,REGEXP_REPLACE(SMALL_NAME, '_(.*)', '') SMALL_NAME ");
            query.AppendLine("      ,DAY_FLOW ");
            query.AppendLine("      ,MONTH_FLOW ");
            query.AppendLine("  FROM ");
            query.AppendLine("      ( ");
            query.AppendLine("      SELECT LOC.PLOC_NAME MIDDLE_NAME ");
            query.AppendLine("            ,LOC.LOC_NAME||'_'||LOC.KT_GBN SMALL_NAME ");
            query.AppendLine("            ,SUM(DECODE(TO_CHAR(IAY.TIMESTAMP - 1/24,'YYYYMMDD'), :ENDDATE, VALUE)) DAY_FLOW ");
            query.AppendLine("            ,SUM(IAY.VALUE) MONTH_FLOW ");
            query.AppendLine("            ,LOC.KT_GBN ");
            query.AppendLine("            ,LOC.ORDERBY ");
            query.AppendLine("        FROM LOC ");
            query.AppendLine("            ,IF_IHTAGS IIH ");
            query.AppendLine("            ,IF_TAG_GBN ITG ");
            query.AppendLine("            ,IF_ACCUMULATION_HOUR IAY ");
            query.AppendLine("       WHERE 1 = 1 ");
            query.AppendLine("         AND IIH.LOC_CODE2 = LOC.LOC_CODE ");
            query.AppendLine("         AND ITG.TAGNAME = IIH.TAGNAME ");
            query.AppendLine("         AND ITG.TAG_GBN = 'FRQ' ");
            query.AppendLine("         AND ITG.TAGNAME NOT IN (SELECT TAGNAME FROM IF_TAG_GBN WHERE TAG_GBN = 'YD_R') ");
            query.AppendLine("         AND IAY.TAGNAME = ITG.TAGNAME ");
            query.AppendLine("         AND IAY.TIMESTAMP BETWEEN TO_DATE(:STARTDATE||'01','YYYYMMDDHH24') AND TO_DATE(:ENDDATE,'YYYYMMDD') + 1 ");
            query.AppendLine("      GROUP BY LOC.PLOC_NAME, LOC.LOC_NAME, LOC.KT_GBN ,LOC.ORDERBY ");
            query.AppendLine("      ) ");
            query.AppendLine(" MODEL ");
            query.AppendLine(" PARTITION BY (MIDDLE_NAME) ");
            query.AppendLine(" DIMENSION BY (SMALL_NAME) ");
            query.AppendLine(" MEASURES (DAY_FLOW, MONTH_FLOW) ");
            query.AppendLine(" IGNORE NAV ");
            query.AppendLine(" RULES ( DAY_FLOW['소계'] = ");
            query.AppendLine("         SUM(DAY_FLOW)[REGEXP_REPLACE(SMALL_NAME, '(.*)_', '') != '003' OR REGEXP_REPLACE(SMALL_NAME, '(.*)_', '') IS NULL] ");
            query.AppendLine("        ,MONTH_FLOW['소계'] = ");
            query.AppendLine("         SUM(MONTH_FLOW)[REGEXP_REPLACE(SMALL_NAME, '(.*)_', '') != '003' OR REGEXP_REPLACE(SMALL_NAME, '(.*)_', '') IS NULL]) ");
            query.AppendLine(" ORDER BY MIDDLE_NAME, DECODE(SMALL_NAME, '소계', '0' , SMALL_NAME) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        //특이사항기록 검색
        public void SelectDailyLog(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                     ");
            query.AppendLine("(                                                                                                                               ");
            query.AppendLine("select c1.loc_code                                                                                                              ");
            query.AppendLine("      ,c1.sgccd                                                                                                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                         ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                             ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                     ");
            query.AppendLine("      ,c1.ord                                                                                                                   ");
            query.AppendLine("  from                                                                                                                          ");
            query.AppendLine("      (                                                                                                                         ");
            query.AppendLine("       select sgccd                                                                                                             ");
            query.AppendLine("             ,loc_code                                                                                                          ");
            query.AppendLine("             ,ploc_code                                                                                                         ");
            query.AppendLine("             ,loc_name                                                                                                          ");
            query.AppendLine("             ,ftr_idn                                                                                                           ");
            query.AppendLine("             ,ftr_code                                                                                                          ");
            query.AppendLine("             ,res_code                                                                                                          ");
            query.AppendLine("             ,rownum ord                                                                                                        ");
            query.AppendLine("         from cm_location                                                                                                       ");
            query.AppendLine("        where 1 = 1                                                                                                             ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                         ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                   ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                               ");
            query.AppendLine("      ) c1                                                                                                                      ");
            query.AppendLine("       ,cm_location c2                                                                                                          ");
            query.AppendLine("       ,cm_location c3                                                                                                          ");
            query.AppendLine(" where 1 = 1                                                                                                                    ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                            ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                            ");
            query.AppendLine(" order by c1.ord                                                                                                                ");
            query.AppendLine(")                                                                                                                               ");
            query.AppendLine("select loc.loc_code                                                                                                             ");
            query.AppendLine("      ,loc.lblock                                                                                                               ");
            query.AppendLine("      ,loc.mblock                                                                                                               ");
            query.AppendLine("      ,loc.sblock                                                                                                               ");
            query.AppendLine("      ,to_date(wdl.datee,'yyyymmdd') datee                                                                                      ");
            query.AppendLine("      ,wdl.log_no                                                                                                               ");
            query.AppendLine("      ,wdl.content                                                                                                              ");
            query.AppendLine("  from loc loc                                                                                                                  ");
            query.AppendLine("      ,wv_daily_log wdl                                                                                                         ");
            query.AppendLine(" where wdl.loc_Code = loc.loc_code                                                                                              ");
            query.AppendLine("   and wdl.datee between :STARTDATE and :ENDDATE                                                                                ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //특이사항기록 수정 및 등록
        public void UpdateDailyLog(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_daily_log a                                                                                          ");
            query.AppendLine("using (select decode(:LOG_NO, null, (select nvl(max(log_no),0)+1 from wv_daily_log), :LOG_NO) log_no               ");
            query.AppendLine("         from dual a                                                                                               ");
            query.AppendLine("      ) b                                                                                                          ");
            query.AppendLine("   on (       a.log_no = b.log_no                                                                                  ");
            query.AppendLine("      )                                                                                                            ");
            query.AppendLine("when matched then                                                                                                  ");
            query.AppendLine("   update set a.loc_code = :SBLOCK                                                                                 ");
            query.AppendLine("             ,a.datee = :DATEE                                                                                     ");
            query.AppendLine("             ,a.content = :CONTENT                                                                                 ");
            query.AppendLine("when not matched then                                                                                              ");
            query.AppendLine("      insert (a.loc_code, a.log_no, a.datee, a.content)                                                            ");
            query.AppendLine("      values (:SBLOCK, b.log_no, :DATEE, :CONTENT)                                                                 ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOG_NO", OracleDbType.Varchar2)
                     ,new OracleParameter("LOG_NO", OracleDbType.Varchar2)
                     ,new OracleParameter("SBLOCK", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("CONTENT", OracleDbType.Varchar2)
                     ,new OracleParameter("SBLOCK", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("CONTENT", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOG_NO"];
            parameters[1].Value = parameter["LOG_NO"];
            parameters[2].Value = parameter["SBLOCK"];
            parameters[3].Value = parameter["DATEE"];
            parameters[4].Value = parameter["CONTENT"];
            parameters[5].Value = parameter["SBLOCK"];
            parameters[6].Value = parameter["DATEE"];
            parameters[7].Value = parameter["CONTENT"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //특이사항기록 삭제
        public void DeleteDailyLog(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete wv_daily_log where log_no = :LOG_NO                                                                         "); 
            IDataParameter[] parameters =  {
	                 new OracleParameter("LOG_NO", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOG_NO"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //특이사항기록 검색
        public void SelectDailyLogChart(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            
            query.AppendLine("with loc as                                                                                                                       ");
            query.AppendLine("(																																	");
            query.AppendLine("select c1.loc_code																												");
            query.AppendLine("	  ,c1.sgccd																														");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))		");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock										");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock										");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock												");
            query.AppendLine("	  ,c1.ftr_code																													");
            query.AppendLine("	  ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code			");
            query.AppendLine("	  ,c1.ord																														");
            query.AppendLine("  from																															");
            query.AppendLine("	  (																																");
            query.AppendLine("	   select sgccd																													");
            query.AppendLine("			 ,loc_code																												");
            query.AppendLine("			 ,ploc_code																												");
            query.AppendLine("			 ,loc_name																												");
            query.AppendLine("			 ,ftr_idn																												");
            query.AppendLine("			 ,ftr_code																												");
            query.AppendLine("			 ,kt_gbn																												");
            query.AppendLine("			 ,rownum ord																											");
            query.AppendLine("		 from cm_location																											");
            query.AppendLine("		where 1 = 1																													");
            query.AppendLine("          and loc_code = :LOC_CODE																								");
            query.AppendLine("		start with loc_code = :LOC_CODE																								");
            query.AppendLine("		connect by prior loc_code = ploc_code																						");
            query.AppendLine("		order SIBLINGS by ftr_idn																									");
            query.AppendLine("	  ) c1																															");
            query.AppendLine("	   ,cm_location c2																												");
            query.AppendLine("	   ,cm_location c3																												");
            query.AppendLine(" where 1 = 1																														");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																								");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																								");
            query.AppendLine(" order by c1.ord																													");
            query.AppendLine(")																																	");
            query.AppendLine(",sub_loc as																														");
            query.AppendLine("(																																	");
            query.AppendLine("select loc_code																													");
            query.AppendLine("  from cm_location																												");
            query.AppendLine(" where 1 = 1																														");
            query.AppendLine("   and ftr_code = 'BZ003'																											");
            query.AppendLine(" start with loc_code = :LOC_CODE																									");
            query.AppendLine(" connect by prior loc_code = ploc_code																							");
            query.AppendLine(" order SIBLINGS by ftr_idn																										");
            query.AppendLine(")																																	");
            query.AppendLine("select to_date(iwm.datee,'yyyymmdd') datee																						");
            query.AppendLine("	  ,sum(iay.flow) flow																											");
            query.AppendLine("	  ,round(sum(iwm.mnf),2) mnf																									");
            query.AppendLine("	  ,sum(minleak.count) minleak_count																								");
            query.AppendLine("	  ,sum(tamleak.count) tamleak_count																								");
            query.AppendLine("  from																															");
            query.AppendLine("	  (																																");
            query.AppendLine("	  select datee, sum(mnf) mnf																									");
            query.AppendLine("		from																														");
            query.AppendLine("			(																														");
            query.AppendLine("			select to_char(iwm.timestamp,'yyyymmdd') datee																			");
            query.AppendLine("				  ,round(to_number(iwm.filtering),1) mnf																			");
            query.AppendLine("			  from loc																												");
            query.AppendLine("				  ,if_ihtags iih																									");
            query.AppendLine("				  ,if_tag_gbn itg																									");
            query.AppendLine("				  ,if_wv_mnf iwm																									");
            query.AppendLine("			 where 1 = 1																											");
            query.AppendLine("			   and iih.loc_code = loc.tag_loc_code																					");
            query.AppendLine("			   and itg.tagname = iih.tagname																						");
            query.AppendLine("			   and itg.tag_gbn = 'MNF'																								");
            query.AppendLine("			   and iwm.tagname = itg.tagname																						");
            query.AppendLine("			   and iwm.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')");
            query.AppendLine("			 union all																												");
            query.AppendLine("			select to_char(to_date(:STARTDATE, 'yyyymmdd') + rownum -1,'yyyymmdd') datee , 0 from dual								");
            query.AppendLine("			connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2									");
            query.AppendLine("			)																														");
            query.AppendLine("	  group by datee																												");
            query.AppendLine("	  ) iwm																															");
            query.AppendLine("	  ,(																															");
            query.AppendLine("	  select to_char(iay.timestamp,'yyyymmdd') datee																				");
            query.AppendLine("			,iay.value flow																											");
            query.AppendLine("		from loc																													");
            query.AppendLine("			,if_ihtags iih																											");
            query.AppendLine("			,if_tag_gbn itg																											");
            query.AppendLine("			,if_accumulation_yesterday iay																							");
            query.AppendLine("	   where 1 = 1																													");
            query.AppendLine("		 and iih.loc_code = loc.tag_loc_code																						");
            query.AppendLine("		 and itg.tagname = iih.tagname																								");
            query.AppendLine("		 and itg.tag_gbn = 'YD'																										");
            query.AppendLine("		 and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')												");
            query.AppendLine("         and iay.tagname = itg.tagname																							");
            query.AppendLine("		 and iay.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and to_date(:ENDDATE||'2359','yyyymmddhh24mi')		");
            query.AppendLine("	  ) iay																															");
            query.AppendLine("	  ,(																															");
            query.AppendLine("	  select sum(1) count																											");
            query.AppendLine("			,wlr.ree_ymd datee																										");
            query.AppendLine("		from sub_loc loc																											");
            query.AppendLine("			,wv_leak_restore wlr																									");
            query.AppendLine("	   where wlr.loc_code = loc.loc_code																							");
            query.AppendLine("		 and wlr.ree_ymd between :STARTDATE and :ENDDATE																			");
            query.AppendLine("		 and wlr.lek_cde = 'BEG001'																									");
            query.AppendLine("	  group by wlr.ree_ymd																											");
            query.AppendLine("	  ) minleak																														");
            query.AppendLine("	  ,(																															");
            query.AppendLine("	  select sum(1) count																											");
            query.AppendLine("			,wlr.ree_ymd datee																										");
            query.AppendLine("		from sub_loc loc																											");
            query.AppendLine("			,wv_leak_restore wlr																									");
            query.AppendLine("	   where wlr.loc_code = loc.loc_code																							");
            query.AppendLine("		 and wlr.ree_ymd between :STARTDATE and :ENDDATE																			");
            query.AppendLine("		 and wlr.lek_cde != 'BEG001'																								");
            query.AppendLine("	  group by wlr.ree_ymd																											");
            query.AppendLine("	  ) tamleak																														");
            query.AppendLine(" where 1 = 1																														");
            query.AppendLine("   and iay.datee(+) = iwm.datee																									");
            query.AppendLine("   and minleak.datee(+) = iwm.datee																								");
            query.AppendLine("   and tamleak.datee(+) = iwm.datee																								");
            query.AppendLine(" group by iwm.datee																												");
            query.AppendLine(" order by datee																													");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["LOC_CODE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];
            parameters[5].Value = parameter["STARTDATE"];
            parameters[6].Value = parameter["ENDDATE"];
            parameters[7].Value = parameter["STARTDATE"];
            parameters[8].Value = parameter["STARTDATE"];
            parameters[9].Value = parameter["ENDDATE"];
            parameters[10].Value = parameter["STARTDATE"];
            parameters[11].Value = parameter["ENDDATE"];
            parameters[12].Value = parameter["STARTDATE"];
            parameters[13].Value = parameter["ENDDATE"];
            
            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //야간최소유량 값 검색
        public void SelectMNF(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                        ");
            query.AppendLine("(                                                                                                                                  ");
            query.AppendLine("select c1.loc_code                                                                                                                 ");
            query.AppendLine("      ,c1.sgccd                                                                                                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_name)) lblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_name)) mblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                            ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                                ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                        ");
            query.AppendLine("      ,c1.ord                                                                                                                      ");
            query.AppendLine("  from                                                                                                                             ");
            query.AppendLine("      (                                                                                                                            ");
            query.AppendLine("       select sgccd                                                                                                                ");
            query.AppendLine("             ,loc_code                                                                                                             ");
            query.AppendLine("             ,ploc_code                                                                                                            ");
            query.AppendLine("             ,loc_name                                                                                                             ");
            query.AppendLine("             ,ftr_idn                                                                                                              ");
            query.AppendLine("             ,ftr_code                                                                                                             ");
            query.AppendLine("             ,res_code                                                                                                             ");
            query.AppendLine("             ,rownum ord                                                                                                           ");
            query.AppendLine("         from cm_location2                                                                                                          ");
            query.AppendLine("        where 1 = 1                                                                                                                ");
            query.AppendLine("          and ftr_code = 'BZ003'                                                                                                   ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                            ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                      ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                  ");
            query.AppendLine("       ) c1                                                                                                                        ");
            query.AppendLine("       ,cm_location2 c2                                                                                                             ");
            query.AppendLine("       ,cm_location2 c3                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                       ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            query.AppendLine(" order by c1.ord                                                                                                                   ");
            query.AppendLine(")                                                                                                                                  ");
            query.AppendLine("select loc.loc_code                                                                                                                ");
            query.AppendLine("      ,loc.lblock                                                                                                                  ");
            query.AppendLine("      ,loc.mblock                                                                                                                  ");
            query.AppendLine("      ,loc.sblock                                                                                                                  ");
            query.AppendLine("      ,itg.tag_gbn                                                                                                                 ");
            query.AppendLine("      ,itg.tagname                                                                                                                 ");
            query.AppendLine("      ,to_date(tmp.timestamp,'yyyymmdd') datee                                                                                     ");
            query.AppendLine("      ,(                                                                                                                           ");
            query.AppendLine("      select round(to_number(filtering),2) value                                                                                   ");
            query.AppendLine("        from if_wv_mnf                                                                                                             ");
            query.AppendLine("       where 1 = 1                                                                                                                 ");
            query.AppendLine("         and tagname = itg.tagname                                                                                                 ");
            query.AppendLine("         and timestamp between to_date(tmp.timestamp||'0000','yyyymmddhh24mi') and to_date(tmp.timestamp||'2359','yyyymmddhh24mi') ");
            query.AppendLine("      ) value                                                                                                                      ");
            query.AppendLine("  from loc                                                                                                                         ");
            query.AppendLine("      ,if_ihtags iih                                                                                                               ");
            query.AppendLine("      ,if_tag_gbn itg                                                                                                              ");
            query.AppendLine("      ,(select :DATEE timestamp from dual ) tmp                                                                                    ");
            query.AppendLine("      ,if_wv_mnf iwm                                                                                                               ");
            query.AppendLine(" where iih.loc_code2 = loc.loc_code                                                                                                 ");
            query.AppendLine("   and iih.tagname = itg.tagname                                                                                                   ");
            query.AppendLine("   and itg.tag_gbn = 'MNF'                                                                                                         ");
            query.AppendLine(" group by                                                                                                                          ");
            query.AppendLine("       loc.loc_code                                                                                                                ");
            query.AppendLine("      ,loc.lblock                                                                                                                  ");
            query.AppendLine("      ,loc.mblock                                                                                                                  ");
            query.AppendLine("      ,loc.sblock                                                                                                                  ");
            query.AppendLine("      ,itg.tag_gbn                                                                                                                 ");
            query.AppendLine("      ,itg.tagname                                                                                                                 ");
            query.AppendLine("      ,to_date(tmp.timestamp,'yyyymmdd')                                                                                           ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //2014_05_12 대블록, 중블록 이름 나오게 하려 수정
        //시간단위 유량 값 검색
        public void SelectFlow(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                       ");
            query.AppendLine("(                                                                                                                                 ");
            query.AppendLine("select c1.loc_code                                                                                                                ");
            query.AppendLine("      ,c1.sgccd                                                                                                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name)))   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_name), c3.loc_name)) lblock                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) mblock                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                           ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                               ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                       ");
            query.AppendLine("      ,c1.ord                                                                                                                     ");
            query.AppendLine("  from                                                                                                                            ");
            query.AppendLine("      (                                                                                                                           ");
            query.AppendLine("       select sgccd                                                                                                               ");
            query.AppendLine("             ,loc_code                                                                                                            ");
            query.AppendLine("             ,ploc_code                                                                                                           ");
            query.AppendLine("             ,loc_name                                                                                                            ");
            query.AppendLine("             ,ftr_idn                                                                                                             ");
            query.AppendLine("             ,ftr_code                                                                                                            ");
            query.AppendLine("             ,res_code                                                                                                            ");
            query.AppendLine("             ,rownum ord                                                                                                          ");
            query.AppendLine("         from cm_location2                                                                                                         ");
            query.AppendLine("        where 1 = 1                                                                                                               ");
            //query.AppendLine("          and ftr_code = 'BZ003'                                                                                                  ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                                                  ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                           ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                     ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                 ");
            query.AppendLine("       ) c1                                                                                                                       ");
            query.AppendLine("       ,cm_location2 c2                                                                                                            ");
            query.AppendLine("       ,cm_location2 c3                                                                                                            ");
            query.AppendLine(" where 1 = 1                                                                                                                      ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                              ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                              ");
            query.AppendLine(" order by c1.ord                                                                                                                  ");
            query.AppendLine(")                                                                                                                                 ");
            query.AppendLine("select loc.loc_code                                                                                                               ");
            query.AppendLine("      ,loc.lblock                                                                                                                 ");
            query.AppendLine("      ,loc.mblock                                                                                                                 ");
            query.AppendLine("      ,loc.sblock                                                                                                                 ");
            query.AppendLine("      ,itg.tag_gbn                                                                                                                ");
            query.AppendLine("      ,itg.tagname                                                                                                                ");
            query.AppendLine("      ,to_date(tmp.timestamp,'yyyymmddhh24mi') datee                                                                              ");
            query.AppendLine("      ,(                                                                                                                          ");
            query.AppendLine("      select nvl(round(avg(to_number(value))),0) value                                                                            ");
            query.AppendLine("        from if_accumulation_hour                                                                                                 ");
            query.AppendLine("       where 1 = 1                                                                                                                ");
            query.AppendLine("         and tagname = itg.tagname                                                                                                ");
//            query.AppendLine("         and timestamp between to_date(tmp.timestamp,'yyyymmddhh24mi')                                                            ");
//            query.AppendLine("         and     decode(substr(replace(:E_TIME,':',''),0,2), '24', to_date(substr(tmp.timestamp,0,8),'yyyymmddhh24mi')+1          ");
//            query.AppendLine("                        , to_date(concat(substr(tmp.timestamp,0,8),replace(:STARTDATE,':','')),'yyyymmddhh24mi'))                    ");
            query.AppendLine("          and timestamp between :V_TIME and :E_TIME                                                                               ");
            query.AppendLine("      ) value                                                                                                                     ");
            query.AppendLine("      ,:V_TIME v_time                                                                                                             ");
            query.AppendLine("      ,:E_TIME e_time                                                                                                             ");
            query.AppendLine("  from loc                                                                                                                        ");
            query.AppendLine("      ,if_ihtags iih                                                                                                              ");
            query.AppendLine("      ,if_tag_gbn itg                                                                                                             ");
            query.AppendLine("      ,(select :DATEE timestamp from dual ) tmp                                                                                   ");
            query.AppendLine(" where iih.loc_code2 = loc.loc_code                                                                                                ");
            query.AppendLine("   and iih.tagname = itg.tagname                                                                                                  ");
            query.AppendLine("   and itg.tag_gbn = :TAG_GBN                                                                                                        ");
            query.AppendLine(" group by                                                                                                                         ");
            query.AppendLine("       loc.loc_code                                                                                                               ");
            query.AppendLine("      ,loc.lblock                                                                                                                 ");
            query.AppendLine("      ,loc.mblock                                                                                                                 ");
            query.AppendLine("      ,loc.sblock                                                                                                                 ");
            query.AppendLine("      ,itg.tag_gbn                                                                                                                ");
            query.AppendLine("      ,itg.tagname                                                                                                                ");
            query.AppendLine("      ,to_date(tmp.timestamp,'yyyymmddhh24mi')                                                                                    ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("V_TIME", OracleDbType.Date)                     
                     ,new OracleParameter("E_TIME", OracleDbType.Date)
                     ,new OracleParameter("V_TIME", OracleDbType.Date)
                     ,new OracleParameter("E_TIME", OracleDbType.Date)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("TAG_GBN", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["V_TIME"];
            parameters[3].Value = parameter["E_TIME"];
            parameters[4].Value = parameter["V_TIME"];
            parameters[5].Value = parameter["E_TIME"];
            parameters[6].Value = parameter["STARTDATE"];
            parameters[7].Value = parameter["TAG_GBN"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //일단위 유량 값 검색
        public void SelectFlow_YD(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                        ");
            query.AppendLine("(                                                                                                                                  ");
            query.AppendLine("select c1.loc_code                                                                                                                 ");
            query.AppendLine("      ,c1.sgccd                                                                                                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_name)) lblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_name)) mblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) sblock                                            ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                                ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                        ");
            query.AppendLine("      ,c1.ord                                                                                                                      ");
            query.AppendLine("  from                                                                                                                             ");
            query.AppendLine("      (                                                                                                                            ");
            query.AppendLine("       select sgccd                                                                                                                ");
            query.AppendLine("             ,loc_code                                                                                                             ");
            query.AppendLine("             ,ploc_code                                                                                                            ");
            query.AppendLine("             ,loc_name                                                                                                             ");
            query.AppendLine("             ,ftr_idn                                                                                                              ");
            query.AppendLine("             ,ftr_code                                                                                                             ");
            query.AppendLine("             ,res_code                                                                                                             ");
            query.AppendLine("             ,rownum ord                                                                                                           ");
            query.AppendLine("         from cm_location2                                                                                                          ");
            query.AppendLine("        where 1 = 1                                                                                                                ");
            query.AppendLine("          and ftr_code = 'BZ003'                                                                                                   ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                            ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                      ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                  ");
            query.AppendLine("       ) c1                                                                                                                        ");
            query.AppendLine("       ,cm_location2 c2                                                                                                             ");
            query.AppendLine("       ,cm_location2 c3                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                       ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            query.AppendLine(" order by c1.ord                                                                                                                   ");
            query.AppendLine(")                                                                                                                                  ");
            query.AppendLine("select loc.loc_code                                                                                                                ");
            query.AppendLine("      ,loc.lblock                                                                                                                  ");
            query.AppendLine("      ,loc.mblock                                                                                                                  ");
            query.AppendLine("      ,loc.sblock                                                                                                                  ");
            query.AppendLine("      ,itg.tag_gbn                                                                                                                 ");
            query.AppendLine("      ,itg.tagname                                                                                                                 ");
            query.AppendLine("      ,to_date(tmp.timestamp,'yyyymmdd') datee                                                                                     ");
            query.AppendLine("      ,(                                                                                                                           ");
            query.AppendLine("      select to_number(value) value                                                                                                ");
            query.AppendLine("        from if_accumulation_yesterday                                                                                             ");
            query.AppendLine("       where 1 = 1                                                                                                                 ");
            query.AppendLine("         and tagname = itg.tagname                                                                                                 ");
            query.AppendLine("         and timestamp between to_date(tmp.timestamp||'0000','yyyymmddhh24mi') and to_date(tmp.timestamp||'2359','yyyymmddhh24mi') ");
            query.AppendLine("      ) value                                                                                                                      ");
            query.AppendLine("  from loc                                                                                                                         ");
            query.AppendLine("      ,if_ihtags iih                                                                                                               ");
            query.AppendLine("      ,if_tag_gbn itg                                                                                                              ");
            query.AppendLine("      ,(select :DATEE timestamp from dual ) tmp                                                                                    ");
            query.AppendLine("      ,if_accumulation_yesterday iay                                                                                               ");
            query.AppendLine(" where iih.loc_code2 = loc.loc_code                                                                                                 ");
            query.AppendLine("   and iih.tagname = itg.tagname                                                                                                   ");
            query.AppendLine("   and itg.tag_gbn = 'YD'                                                                                                          ");
            query.AppendLine(" group by                                                                                                                          ");
            query.AppendLine("       loc.loc_code                                                                                                                ");
            query.AppendLine("      ,loc.lblock                                                                                                                  ");
            query.AppendLine("      ,loc.mblock                                                                                                                  ");
            query.AppendLine("      ,loc.sblock                                                                                                                  ");
            query.AppendLine("      ,itg.tag_gbn                                                                                                                 ");
            query.AppendLine("      ,itg.tagname                                                                                                                 ");
            query.AppendLine("      ,to_date(tmp.timestamp,'yyyymmdd')                                                                                           ");
            
            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //야간최소유량 수정 및 등록
        public void UpdateMNF(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into if_wv_mnf iwm                                                                                                      ");
            query.AppendLine("using (select :TAGNAME tagname, :DATEE timestamp from dual) a                                                                 ");
            query.AppendLine("  on (iwm.tagname = a.tagname and                                                                                             ");
            query.AppendLine("      iwm.timestamp between to_date(a.timestamp||'0000', 'yyyymmddhh24mi') and to_date(a.timestamp||'2359', 'yyyymmddhh24mi'))");
            query.AppendLine("when matched then                                                                                                             ");
            query.AppendLine("     update set filtering = :VALUE                                                                                            ");
            query.AppendLine("when not matched then                                                                                                         ");
            query.AppendLine("     insert (tagname, timestamp, filtering)                                                                                   ");
            query.AppendLine("     values (a.tagname, to_date(a.timestamp,'yyyymmdd'), :VALUE)                                                              ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("VALUE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["VALUE"];
            parameters[3].Value = parameter["VALUE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //시간단위 유량 수정 및 등록
        public void UpdateFlow(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("merge into if_accumulation_hour iah                                                         ");
            query.AppendLine("using (select :TAGNAME tagname                                                              ");
            //query.AppendLine("              ,to_date(:V_TIME,'yyyymmddhh24mi') + ((1/24)*(rownum-1)) timestamp          ");
            //query.AppendLine("         from dual connect by rownum <= ((to_date(:E_TIME,'yyyymmddhh24mi')               ");
            //query.AppendLine("                               - to_date(:V_TIME,'yyyymmddhh24mi'))*24)+1) a              ");
            query.AppendLine("                  ,:V_TIME  + ((1/24)*(rownum-1)) timestamp                                 ");
            query.AppendLine("              from dual connect by rownum <= ((:E_TIME - :V_TIME)*24)+1) a                  ");
            query.AppendLine("  on (iah.tagname = a.tagname and iah.timestamp = a.timestamp)                              ");
            query.AppendLine("when matched then                                                                           ");
            query.AppendLine("     update set value = :VALUE                                                              ");
            query.AppendLine("when not matched then                                                                       ");
            query.AppendLine("     insert (tagname, timestamp, value)                                                     ");
            query.AppendLine("     values (a.tagname, a.timestamp, :VALUE)                                                ");


            IDataParameter[] parameters =  {
	                 new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                     ,new OracleParameter("V_TIME", OracleDbType.Date)
                     ,new OracleParameter("E_TIME", OracleDbType.Date)
                     ,new OracleParameter("V_TIME", OracleDbType.Date)
                     ,new OracleParameter("VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("VALUE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["V_TIME"];
            parameters[2].Value = parameter["E_TIME"];
            parameters[3].Value = parameter["V_TIME"];
            parameters[4].Value = parameter["VALUE"];
            parameters[5].Value = parameter["VALUE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //시간단위 유량 수정 및 등록
        public void UpdateFlow_YD(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into if_accumulation_yesterday iay                                                                                      ");
            query.AppendLine("using (select :TAGNAME tagname, :DATEE timestamp from dual) a                                                                 ");
            query.AppendLine("  on (iay.tagname = a.tagname and iay.timestamp = to_date(a.timestamp, 'yyyymmdd'))                                           ");
            query.AppendLine("when matched then                                                                                                             ");
            query.AppendLine("     update set value = :VALUE                                                                                                ");
            query.AppendLine("when not matched then                                                                                                         ");
            query.AppendLine("     insert (tagname, timestamp, value)                                                                                       ");
            query.AppendLine("     values (a.tagname, to_date(a.timestamp,'yyyymmdd'), :VALUE)                                                              ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("VALUE", OracleDbType.Varchar2)
                     ,new OracleParameter("VALUE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["VALUE"];
            parameters[3].Value = parameter["VALUE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //일 유량 수정 및 등록
        public void UpdateDayFlow(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into if_accumulation_yesterday a                                                                                        ");
            query.AppendLine("using (select replace(:tagname, '_1H', '_YD') tagname                                                                         ");
            query.AppendLine("            , to_date(to_char(to_date(:DATEE, 'yyyymmddhh24mi'),'yyyymmdd'),'yyyymmdd') timestamp                             ");
            query.AppendLine("            , round(sum(a.value),2) value                                                                                     ");
            query.AppendLine("         from if_accumulation_hour a                                                                                          ");
            query.AppendLine("        where 1 = 1                                                                                                           ");
            query.AppendLine("          and a.tagname = :tagname                                                                                            ");
            query.AppendLine("          and a.timestamp between to_date(to_char(to_date(:DATEE, 'yyyymmddhh24mi'),'yyyymmdd')||'0100', 'yyyymmddhh24mi')    ");
            query.AppendLine("                              and to_date(to_char(to_date(:DATEE, 'yyyymmddhh24mi'),'yyyymmdd')||'0000', 'yyyymmddhh24mi')+1  ");
            query.AppendLine("        group by a.tagname                                                                                                    ");
            query.AppendLine("      ) b                                                                                                                     ");
            query.AppendLine("   on (       a.tagname   = b.tagname                                                                                         ");
            query.AppendLine("          and a.timestamp = b.timestamp                                                                                       ");
            query.AppendLine("      )                                                                                                                       ");
            query.AppendLine("when matched then                                                                                                             ");
            query.AppendLine("   update set a.value = b.value                                                                                               ");
            query.AppendLine("when not matched then                                                                                                         ");
            query.AppendLine("      insert (a.tagname, a.timestamp, a.value)                                                                                ");
            query.AppendLine("      values (b.tagname, b.timestamp, b.value)                                                                                ");
            
            IDataParameter[] parameters =  {
	                 new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                     ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["TAGNAME"];
            parameters[3].Value = parameter["DATEE"];
            parameters[4].Value = parameter["DATEE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //월 유량 수정 및 등록
        //public void UpdateMonthFlow(OracleDBManager manager, Hashtable parameter)
        //{
        //    StringBuilder query = new StringBuilder();

        //    query.AppendLine("merge into wv_revenue_ratio a                                                                                 ");
        //    query.AppendLine("using (																										");
        //    query.AppendLine("select loc.loc_code																							");
        //    query.AppendLine("      ,:YEAR_MON year_mon																						");
        //    query.AppendLine("      ,sum(iay.value) water_supplied																			");
        //    query.AppendLine("      ,(select amtuse																							");
        //    query.AppendLine("          from wv_consumer_mon																				");
        //    query.AppendLine("         where loc_code = loc.loc_code																		");
        //    query.AppendLine("           and year_mon = :YEAR_MON																			");
        //    query.AppendLine("       ) revenue																								");
        //    query.AppendLine("  from cm_location loc																						");
        //    query.AppendLine("      ,if_ihtags iih																							");
        //    query.AppendLine("      ,if_tag_gbn itg																							");
        //    query.AppendLine("      ,if_accumulation_yesterday iay																			");
        //    query.AppendLine(" where 1 = 1																									");
        //    query.AppendLine("   and iih.loc_code = decode(loc.kt_gbn, '002', (select loc_code from cm_location where ploc_code = loc.loc_code), loc.loc_code) 																			");
        //    query.AppendLine("   and itg.tagname = iih.tagname																				");
        //    query.AppendLine("   and itg.tag_gbn = 'YD'																						");
        //    query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                             ");
        //    query.AppendLine("   and iay.tagname = itg.tagname																				");
        //    query.AppendLine("   and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')					");
        //    query.AppendLine(" group by																										");
        //    query.AppendLine("       loc.loc_code																							");
        //    query.AppendLine(") b																											");
        //    query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)													");
        //    query.AppendLine(" when matched then																							");
        //    query.AppendLine("      update set a.water_supplied = b.water_supplied, a.revenue = b.revenue								    ");
        //    query.AppendLine(" when not matched then																						");
        //    query.AppendLine("      insert (a.loc_code, a.year_mon, a.water_supplied, a.revenue)											");
        //    query.AppendLine("      values (b.loc_code, b.year_mon, b.water_supplied, b.revenue)											");

        //    IDataParameter[] parameters =  {
        //             new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
        //             ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
        //             ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
        //             ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
        //        };

        //    parameters[0].Value = parameter["YEAR_MON"];
        //    parameters[1].Value = parameter["YEAR_MON"];
        //    parameters[2].Value = parameter["STARTDATE"];
        //    parameters[3].Value = parameter["ENDDATE"];

        //    manager.ExecuteScript(query.ToString(), parameters);
        //}

        //월 유량 수정 및 등록(소블록)
        public void UpdateMonthFlow_Small(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio a                                                                                 ");
            query.AppendLine("using (																										");
            query.AppendLine("select loc.loc_code																							");
            query.AppendLine("      ,:YEAR_MON year_mon																						");
            query.AppendLine("      ,round(sum(iay.value),2) water_supplied																			");
            query.AppendLine("      ,(select amtuse																							");
            query.AppendLine("          from wv_consumer_mon																				");
            query.AppendLine("         where loc_code = loc.loc_code																		");
            query.AppendLine("           and year_mon = :YEAR_MON																			");
            query.AppendLine("       ) revenue																								");
            query.AppendLine("  from cm_location loc																						");
            query.AppendLine("      ,if_ihtags iih																							");
            query.AppendLine("      ,if_tag_gbn itg																							");
            query.AppendLine("      ,if_accumulation_yesterday iay																			");
            query.AppendLine(" where 1 = 1																									");
            query.AppendLine("   and loc.ftr_code = 'BZ003'                                                                                 ");
            query.AppendLine("   and iih.loc_code = decode(loc.kt_gbn, '002', (select loc_code from cm_location where ploc_code = loc.loc_code), loc.loc_code) 																			");
            query.AppendLine("   and itg.tagname = iih.tagname																				");
            query.AppendLine("   and itg.tag_gbn = 'YD'																						");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                             ");
            query.AppendLine("   and iay.tagname = itg.tagname																				");
            query.AppendLine("   and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')					");
            query.AppendLine(" group by																										");
            query.AppendLine("       loc.loc_code																							");
            query.AppendLine(") b																											");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)													");
            query.AppendLine(" when matched then																							");
            query.AppendLine("      update set a.water_supplied = b.water_supplied, a.revenue = b.revenue							   	    ");
            query.AppendLine(" when not matched then																						");
            query.AppendLine("      insert (a.loc_code, a.year_mon, a.water_supplied, a.revenue)											");
            query.AppendLine("      values (b.loc_code, b.year_mon, b.water_supplied, b.revenue)											");

            IDataParameter[] parameters =  {
                     new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["YEAR_MON"];
            parameters[1].Value = parameter["YEAR_MON"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //월 유량 수정 및 등록(소블록)
        public void UpdateMonthFlow_Middle(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio a                                                                                 ");
            query.AppendLine("using (																										");
            query.AppendLine("select loc.loc_code																							");
            query.AppendLine("      ,:YEAR_MON year_mon																						");
            query.AppendLine("      ,round(sum(iay.value),2) water_supplied																			");
            query.AppendLine("      ,(select amtuse																							");
            query.AppendLine("          from wv_consumer_mon																				");
            query.AppendLine("         where loc_code = loc.loc_code																		");
            query.AppendLine("           and year_mon = :YEAR_MON																			");
            query.AppendLine("       ) revenue																								");
            query.AppendLine("  from cm_location loc																						");
            query.AppendLine("      ,if_ihtags iih																							");
            query.AppendLine("      ,if_tag_gbn itg																							");
            query.AppendLine("      ,if_accumulation_yesterday iay																			");
            query.AppendLine(" where 1 = 1																									");
            query.AppendLine("   and loc.ftr_code = 'BZ002'                                                                                 ");
            query.AppendLine("   and iih.loc_code = decode(loc.kt_gbn, '002', (select loc_code from cm_location where ploc_code = loc.loc_code), loc.loc_code) 																			");
            query.AppendLine("   and itg.tagname = iih.tagname																				");
            query.AppendLine("   and itg.tag_gbn = 'YD'																						");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                             ");
            query.AppendLine("   and iay.tagname = itg.tagname																				");
            query.AppendLine("   and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')					");
            query.AppendLine(" group by																										");
            query.AppendLine("       loc.loc_code																							");
            query.AppendLine(") b																											");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)													");
            query.AppendLine(" when matched then																							");
            query.AppendLine("      update set a.water_supplied = b.water_supplied, a.revenue = b.revenue							   	    ");
            query.AppendLine(" when not matched then																						");
            query.AppendLine("      insert (a.loc_code, a.year_mon, a.water_supplied, a.revenue)											");
            query.AppendLine("      values (b.loc_code, b.year_mon, b.water_supplied, b.revenue)											");

            IDataParameter[] parameters =  {
                     new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["YEAR_MON"];
            parameters[1].Value = parameter["YEAR_MON"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //월 유량 수정 및 등록(소블록)
        public void UpdateMonthFlow_Large(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_revenue_ratio a                                                                                 ");
            query.AppendLine("using (																										");
            query.AppendLine("select loc.loc_code                                                                                              ");
            query.AppendLine("      ,loc.year_mon																							  ");
            query.AppendLine("      ,round(sum(loc.water_supplied), 2) water_supplied																	  ");
            query.AppendLine("	  ,(select amtuse																							  ");
            query.AppendLine("		  from wv_consumer_mon																					  ");
            query.AppendLine("		 where loc_code = loc.loc_code																			  ");
            query.AppendLine("		   and year_mon = :YEAR_MON																				  ");
            query.AppendLine("	   ) revenue																								  ");
            query.AppendLine("  from																											  ");
            query.AppendLine("      (																										  ");
            query.AppendLine("select (select loc_code from cm_location where ftr_code = 'BZ001'												  ");
            query.AppendLine("		 start with loc_code = loc.loc_code																		  ");
            query.AppendLine("	   connect by loc_code = prior ploc_code																	  ");
            query.AppendLine("	   ) loc_code																								  ");
            query.AppendLine("	  ,:YEAR_MON year_mon																						  ");
            query.AppendLine("	  ,sum(iay.value) water_supplied																			  ");
            query.AppendLine("  from cm_location loc																							  ");
            query.AppendLine("	  ,if_ihtags iih																							  ");
            query.AppendLine("	  ,if_tag_gbn itg																							  ");
            query.AppendLine("	  ,if_accumulation_yesterday iay																			  ");
            query.AppendLine(" where 1 = 1																									  ");
            query.AppendLine("   and (loc.ftr_code = 'BZ002' or (loc.ftr_code = 'BZ003' and loc.kt_gbn = '003'))								  ");
            query.AppendLine("   and iih.loc_code =																							  ");
            query.AppendLine("	   decode(loc.kt_gbn, '002', (select loc_code from cm_location where ploc_code = loc.loc_code), loc.loc_code) ");
            query.AppendLine("   and itg.tagname = iih.tagname																				  ");
            query.AppendLine("   and itg.tag_gbn = 'YD'																						  ");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')								  ");
            query.AppendLine("   and iay.tagname = itg.tagname																				  ");
            query.AppendLine("   and iay.timestamp between to_date(:STARTDATE,'yyyymmdd') and to_date(:ENDDATE,'yyyymmdd')					  ");
            query.AppendLine(" group by loc.loc_code																							  ");
            query.AppendLine("      ) loc																									  ");
            query.AppendLine(" group by loc.loc_code, loc.year_mon																			  ");
            query.AppendLine(") b																											");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.year_mon = b.year_mon)													");
            query.AppendLine(" when matched then																							");
            query.AppendLine("      update set a.water_supplied = b.water_supplied, a.revenue = b.revenue							   	    ");
            query.AppendLine(" when not matched then																						");
            query.AppendLine("      insert (a.loc_code, a.year_mon, a.water_supplied, a.revenue)											");
            query.AppendLine("      values (b.loc_code, b.year_mon, b.water_supplied, b.revenue)											");

            IDataParameter[] parameters =  {
                     new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["YEAR_MON"];
            parameters[1].Value = parameter["YEAR_MON"];
            parameters[2].Value = parameter["STARTDATE"];
            parameters[3].Value = parameter["ENDDATE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //야간최소유량 날짜, 총계 목록
        public void SelectMNF_TotalInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                        ");
            query.AppendLine("(                                                                                                                                  ");
            query.AppendLine("select c1.loc_code                                                                                                                 ");
            query.AppendLine("      ,c1.sgccd                                                                                                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                            ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                                ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                        ");
            query.AppendLine("      ,c1.ord                                                                                                                      ");
            query.AppendLine("  from                                                                                                                             ");
            query.AppendLine("      (                                                                                                                            ");
            query.AppendLine("       select sgccd                                                                                                                ");
            query.AppendLine("             ,loc_code                                                                                                             ");
            query.AppendLine("             ,ploc_code                                                                                                            ");
            query.AppendLine("             ,loc_name                                                                                                             ");
            query.AppendLine("             ,ftr_idn                                                                                                              ");
            query.AppendLine("             ,ftr_code                                                                                                             ");
            query.AppendLine("             ,res_code                                                                                                             ");
            query.AppendLine("             ,rownum ord                                                                                                           ");
            query.AppendLine("         from cm_location                                                                                                          ");
            query.AppendLine("        where 1 = 1                                                                                                                ");
            query.AppendLine("          and ftr_code = 'BZ003'                                                                                                   ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                            ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                      ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                  ");
            query.AppendLine("      ) c1                                                                                                                         ");
            query.AppendLine("       ,cm_location c2                                                                                                             ");
            query.AppendLine("       ,cm_location c3                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                       ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            query.AppendLine(" order by c1.ord                                                                                                                   ");
            query.AppendLine(")                                                                                                                                  ");
            query.AppendLine("select datee                                                                                                                       ");
            query.AppendLine("      ,decode(max(total_sum),0, null, max(total_sum)) total_sum                                                                    ");
            query.AppendLine("  from                                                                                                                             ");
            query.AppendLine("      (                                                                                                                            ");
            query.AppendLine("       select to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd') datee                                                          ");
            query.AppendLine("             ,to_number(round(sum(iwm.filtering),2)) total_sum                                                                     ");
            query.AppendLine("         from loc                                                                                                                  ");
            query.AppendLine("             ,if_ihtags iih                                                                                                        ");
            query.AppendLine("             ,if_tag_gbn itg                                                                                                       ");
            query.AppendLine("             ,if_wv_mnf iwm                                                                                                        ");
            query.AppendLine("        where iih.loc_code = loc.loc_code                                                                                          ");
            query.AppendLine("          and iih.tagname = itg.tagname                                                                                            ");
            query.AppendLine("          and itg.tag_gbn = 'MNF'                                                                                                  ");
            query.AppendLine("          and iwm.tagname = iih.tagname                                                                                            ");
            query.AppendLine("          and iwm.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and  to_date(:ENDDATE||'2359','yyyymmddhh24mi')   ");
            query.AppendLine("        group by to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd')                                                             ");
            query.AppendLine("       union all                                                                                                                   ");
            query.AppendLine("       select to_date(:STARTDATE, 'yyyymmdd') + rownum -1 , 0 from dual                                                            ");
            query.AppendLine("      connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2                                        ");
            query.AppendLine("      )                                                                                                                            ");
            query.AppendLine(" group by datee order by datee                                                                                                     ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];
            parameters[5].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //야간최소유량 중_소블록 목록
        public void SelectMNF_MiddleInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                  ");
            //query.AppendLine("(																															   ");
            //query.AppendLine("select c1.loc_code																										   ");
            //query.AppendLine("	  ,c1.sgccd																												   ");
            //query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))");
            //query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock								   ");
            //query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            //query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock								   ");
            //query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            //query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock										   ");
            //query.AppendLine("	  ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															       ");
            //query.AppendLine("	  ,decode(c2.kt_gbn, '002', '면지역', c2.loc_name) middle_name															   ");
            //query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            //query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name									   ");
            //query.AppendLine("    ,c1.orderby s_orderby																									   ");
            //query.AppendLine("    ,c2.orderby m_orderby																									   ");
            //query.AppendLine("  from																													   ");
            //query.AppendLine("	  (																														   ");
            //query.AppendLine("	   select sgccd																											   ");
            //query.AppendLine("			 ,loc_code																										   ");
            //query.AppendLine("			 ,ploc_code																										   ");
            //query.AppendLine("			 ,loc_name																										   ");
            //query.AppendLine("			 ,ftr_idn																										   ");
            //query.AppendLine("			 ,ftr_code																										   ");
            //query.AppendLine("			 ,res_code																										   ");
            //query.AppendLine("           ,orderby																										   ");
            //query.AppendLine("			 ,rownum ord																									   ");
            //query.AppendLine("		 from cm_location																									   ");
            //query.AppendLine("		where 1 = 1																											   ");
            //query.AppendLine("		  and ftr_code = 'BZ003'																							   ");
            //query.AppendLine("		start with loc_code = :LOC_CODE																						   ");
            //query.AppendLine("		connect by prior loc_code = ploc_code																				   ");
            //query.AppendLine("		order SIBLINGS by ftr_idn																							   ");
            //query.AppendLine("	  ) c1																													   ");
            //query.AppendLine("	   ,cm_location c2																										   ");
            //query.AppendLine("	   ,cm_location c3																										   ");
            //query.AppendLine(" where 1 = 1																												   ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																						   ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																						   ");
            //query.AppendLine(" order by c1.ord																											   ");
            //query.AppendLine(")																															   ");
            //query.AppendLine("select loc.middle_code mblock, loc.sblock, loc.middle_name, loc.small_name												   ");
            //query.AppendLine("  from loc																												   ");
            //query.AppendLine("	    ,if_ihtags iih																										   ");
            //query.AppendLine("	    ,if_tag_gbn itg																										   ");
            //query.AppendLine(" where iih.loc_code = loc.loc_code																						   ");
            //query.AppendLine("   and itg.tagname = iih.tagname																							   ");
            //query.AppendLine("   and itg.tag_gbn = 'MNF'																								   ");
            //query.AppendLine("order by loc.m_orderby, loc.s_orderby																						   ");

            query.AppendLine("with loc as                                                                                                                  ");
            query.AppendLine("(																															   ");
            query.AppendLine("select c1.loc_code																										   ");
            query.AppendLine("	  ,c1.sgccd																												   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock								   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock								   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock										   ");
            query.AppendLine("	  ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															       ");
            query.AppendLine("	  ,decode(c2.kt_gbn, '002', '면지역', c2.loc_name) middle_name															   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))	   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name									   ");
            query.AppendLine("    ,c1.orderby s_orderby																									   ");
            query.AppendLine("    ,c2.orderby m_orderby																									   ");
            query.AppendLine("  from																													   ");
            query.AppendLine("	  (																														   ");
            query.AppendLine("	   select sgccd																											   ");
            query.AppendLine("			 ,loc_code																										   ");
            query.AppendLine("			 ,ploc_code																										   ");
            query.AppendLine("			 ,loc_name																										   ");
            query.AppendLine("			 ,ftr_idn																										   ");
            query.AppendLine("			 ,ftr_code																										   ");
            query.AppendLine("			 ,res_code																										   ");
            query.AppendLine("           ,orderby																										   ");
            query.AppendLine("			 ,rownum ord																									   ");
            query.AppendLine("		 from cm_location2																									   ");
            query.AppendLine("		where 1 = 1																											   ");
            query.AppendLine("		  and ftr_code = 'BZ003'																							   ");
            query.AppendLine("		start with loc_code = :LOC_CODE																						   ");
            query.AppendLine("		connect by prior loc_code = ploc_code																				   ");
            query.AppendLine("		order SIBLINGS by ftr_idn																							   ");
            query.AppendLine("	  ) c1																													   ");
            query.AppendLine("	   ,cm_location2 c2																										   ");
            query.AppendLine("	   ,cm_location2 c3																										   ");
            query.AppendLine(" where 1 = 1																												   ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																						   ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																						   ");
            query.AppendLine(" order by c1.ord																											   ");
            query.AppendLine(")																															   ");
            query.AppendLine("select loc.middle_code mblock, loc.sblock, loc.middle_name, loc.small_name												   ");
            query.AppendLine("  from loc																												   ");
            query.AppendLine("	    ,if_ihtags iih																										   ");
            query.AppendLine("	    ,if_tag_gbn itg																										   ");
            query.AppendLine(" where iih.loc_code2 = loc.loc_code																						   ");
            query.AppendLine("   and itg.tagname = iih.tagname																							   ");
            query.AppendLine("   and itg.tag_gbn = 'MNF'																								   ");
            query.AppendLine("order by loc.m_orderby, loc.s_orderby																						   ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //야간최소유량 값 검색
        public void SelectMNF_SmallInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select lblock                                                                                                                          ");
            //query.AppendLine("      ,mblock                                                                                                                          ");
            //query.AppendLine("      ,decode(sblock,'_SUM',mblock||sblock,sblock) sblock                                                                              ");
            //query.AppendLine("      ,datee                                                                                                                           ");
            //query.AppendLine("      ,mnf                                                                                                                             ");
            //query.AppendLine("      ,filtering                                                                                                                       ");
            //query.AppendLine("  from                                                                                                                                 ");
            //query.AppendLine("    (                                                                                                                                  ");
            //query.AppendLine("    with loc as                                                                                                                        ");
            //query.AppendLine("    (                                                                                                                                  ");
            //query.AppendLine("    select c1.loc_code                                                                                                                 ");
            //query.AppendLine("          ,c1.sgccd                                                                                                                    ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                     ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                     ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                            ");
            //query.AppendLine("	        ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															             ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                                ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                        ");
            //query.AppendLine("          ,c1.ord                                                                                                                      ");
            //query.AppendLine("      from                                                                                                                             ");
            //query.AppendLine("          (                                                                                                                            ");
            //query.AppendLine("           select sgccd                                                                                                                ");
            //query.AppendLine("                 ,loc_code                                                                                                             ");
            //query.AppendLine("                 ,ploc_code                                                                                                            ");
            //query.AppendLine("                 ,loc_name                                                                                                             ");
            //query.AppendLine("                 ,ftr_idn                                                                                                              ");
            //query.AppendLine("                 ,ftr_code                                                                                                             ");
            //query.AppendLine("                 ,res_code                                                                                                             ");
            //query.AppendLine("                 ,rownum ord                                                                                                           ");
            //query.AppendLine("             from cm_location                                                                                                          ");
            //query.AppendLine("            where 1 = 1                                                                                                                ");
            //query.AppendLine("              and ftr_code = 'BZ003'                                                                                                   ");
            //query.AppendLine("            start with loc_code = :LOC_CODE                                                                                            ");
            //query.AppendLine("            connect by prior loc_code = ploc_code                                                                                      ");
            //query.AppendLine("            order SIBLINGS by ftr_idn                                                                                                  ");
            //query.AppendLine("          ) c1                                                                                                                         ");
            //query.AppendLine("           ,cm_location c2                                                                                                             ");
            //query.AppendLine("           ,cm_location c3                                                                                                             ");
            //query.AppendLine("     where 1 = 1                                                                                                                       ");
            //query.AppendLine("       and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            //query.AppendLine("       and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            //query.AppendLine("     order by c1.ord                                                                                                                   ");
            //query.AppendLine("    )                                                                                                                                  ");
            //query.AppendLine("    select loc.lblock                                                                                                                  ");
            //query.AppendLine("          ,loc.middle_code mblock                                                                                                      ");
            //query.AppendLine("          ,loc.sblock                                                                                                                  ");
            //query.AppendLine("          ,to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd') datee                                                                 ");
            //query.AppendLine("          ,round(to_number(sum(iwm.mvavg_min)),2) mnf                                                                                  ");
            //query.AppendLine("          ,round(to_number(sum(iwm.filtering)),2) filtering                                                                            ");
            //query.AppendLine("      from loc                                                                                                                         ");
            //query.AppendLine("          ,if_ihtags iih                                                                                                               ");
            //query.AppendLine("          ,if_tag_gbn itg                                                                                                              ");
            //query.AppendLine("          ,if_wv_mnf iwm                                                                                                               ");
            //query.AppendLine("     where iih.loc_code = loc.loc_code                                                                                                 ");
            //query.AppendLine("       and iih.tagname = itg.tagname                                                                                                   ");
            //query.AppendLine("       and itg.tag_gbn = 'MNF'                                                                                                         ");
            //query.AppendLine("       and iwm.tagname = iih.tagname                                                                                                   ");
            //query.AppendLine("       and iwm.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and  to_date(:ENDDATE||'2359','yyyymmddhh24mi')          ");
            //query.AppendLine("     group by loc.lblock, loc.middle_code, loc.sblock, to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd')                           ");
            //query.AppendLine("    )                                                                                                                                  ");
            //query.AppendLine(" model                                                                                                                                 ");
            //query.AppendLine(" partition by (lblock, mblock, datee)                                                                                                  ");
            //query.AppendLine(" dimension by (sblock)                                                                                                                 ");
            //query.AppendLine(" measures (mnf, filtering)                                                                                                             ");
            //query.AppendLine(" ignore nav                                                                                                                            ");
            //query.AppendLine(" rules ( mnf['_SUM'] = sum(nvl(filtering, mnf))[sblock is not null] )                                                                  ");
            //query.AppendLine(" order by lblock, mblock, sblock, datee                                                                                                ");

            query.AppendLine("select lblock                                                                                                                          ");
            query.AppendLine("      ,mblock                                                                                                                          ");
            query.AppendLine("      ,decode(sblock,'_SUM',mblock||sblock,sblock) sblock                                                                              ");
            query.AppendLine("      ,datee                                                                                                                           ");
            query.AppendLine("      ,mnf                                                                                                                             ");
            query.AppendLine("      ,filtering                                                                                                                       ");
            query.AppendLine("  from                                                                                                                                 ");
            query.AppendLine("    (                                                                                                                                  ");
            query.AppendLine("    with loc as                                                                                                                        ");
            query.AppendLine("    (                                                                                                                                  ");
            query.AppendLine("    select c1.loc_code                                                                                                                 ");
            query.AppendLine("          ,c1.sgccd                                                                                                                    ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                     ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                     ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                            ");
            query.AppendLine("	        ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															             ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                                ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                        ");
            query.AppendLine("          ,c1.ord                                                                                                                      ");
            query.AppendLine("      from                                                                                                                             ");
            query.AppendLine("          (                                                                                                                            ");
            query.AppendLine("           select sgccd                                                                                                                ");
            query.AppendLine("                 ,loc_code                                                                                                             ");
            query.AppendLine("                 ,ploc_code                                                                                                            ");
            query.AppendLine("                 ,loc_name                                                                                                             ");
            query.AppendLine("                 ,ftr_idn                                                                                                              ");
            query.AppendLine("                 ,ftr_code                                                                                                             ");
            query.AppendLine("                 ,res_code                                                                                                             ");
            query.AppendLine("                 ,rownum ord                                                                                                           ");
            query.AppendLine("             from cm_location2                                                                                                          ");
            query.AppendLine("            where 1 = 1                                                                                                                ");
            query.AppendLine("              and ftr_code = 'BZ003'                                                                                                   ");
            query.AppendLine("            start with loc_code = :LOC_CODE                                                                                            ");
            query.AppendLine("            connect by prior loc_code = ploc_code                                                                                      ");
            query.AppendLine("            order SIBLINGS by ftr_idn                                                                                                  ");
            query.AppendLine("          ) c1                                                                                                                         ");
            query.AppendLine("           ,cm_location2 c2                                                                                                             ");
            query.AppendLine("           ,cm_location2 c3                                                                                                             ");
            query.AppendLine("     where 1 = 1                                                                                                                       ");
            query.AppendLine("       and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            query.AppendLine("       and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            query.AppendLine("     order by c1.ord                                                                                                                   ");
            query.AppendLine("    )                                                                                                                                  ");
            query.AppendLine("    select loc.lblock                                                                                                                  ");
            query.AppendLine("          ,loc.middle_code mblock                                                                                                      ");
            query.AppendLine("          ,loc.sblock                                                                                                                  ");
            query.AppendLine("          ,to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd') datee                                                                 ");
            query.AppendLine("          ,round(to_number(sum(iwm.mvavg_min)),2) mnf                                                                                  ");
            query.AppendLine("          ,round(to_number(sum(iwm.filtering)),2) filtering                                                                            ");
            query.AppendLine("      from loc                                                                                                                         ");
            query.AppendLine("          ,if_ihtags iih                                                                                                               ");
            query.AppendLine("          ,if_tag_gbn itg                                                                                                              ");
            query.AppendLine("          ,if_wv_mnf iwm                                                                                                               ");
            query.AppendLine("     where iih.loc_code2 = loc.loc_code                                                                                                 ");
            query.AppendLine("       and iih.tagname = itg.tagname                                                                                                   ");
            query.AppendLine("       and itg.tag_gbn = 'MNF'                                                                                                         ");
            query.AppendLine("       and iwm.tagname = iih.tagname                                                                                                   ");
            query.AppendLine("       and iwm.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and  to_date(:ENDDATE||'2359','yyyymmddhh24mi')          ");
            query.AppendLine("     group by loc.lblock, loc.middle_code, loc.sblock, to_date(to_char(iwm.timestamp,'yyyymmdd'),'yyyymmdd')                           ");
            query.AppendLine("    )                                                                                                                                  ");
            query.AppendLine(" model                                                                                                                                 ");
            query.AppendLine(" partition by (lblock, mblock, datee)                                                                                                  ");
            query.AppendLine(" dimension by (sblock)                                                                                                                 ");
            query.AppendLine(" measures (mnf, filtering)                                                                                                             ");
            query.AppendLine(" ignore nav                                                                                                                            ");
            query.AppendLine(" rules ( mnf['_SUM'] = sum(nvl(filtering, mnf))[sblock is not null] )                                                                  ");
            query.AppendLine(" order by lblock, mblock, sblock, datee                                                                                                ");


            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        ////////// 일별 공급량
        //일별공급량 날짜, 총계 목록
        public void SelectDaySupply_TotalInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                     ");
            query.AppendLine("(                                                                                                                               ");
            query.AppendLine("select c1.loc_code                                                                                                              ");
            query.AppendLine("      ,c1.sgccd                                                                                                                 ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code))) ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                  ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                         ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                             ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                     ");
            query.AppendLine("      ,c1.ord                                                                                                                   ");
            query.AppendLine("  from                                                                                                                          ");
            query.AppendLine("      (                                                                                                                         ");
            query.AppendLine("       select sgccd                                                                                                             ");
            query.AppendLine("             ,loc_code                                                                                                          ");
            query.AppendLine("             ,ploc_code                                                                                                         ");
            query.AppendLine("             ,loc_name                                                                                                          ");
            query.AppendLine("             ,ftr_idn                                                                                                           ");
            query.AppendLine("             ,ftr_code                                                                                                          ");
            query.AppendLine("             ,res_code                                                                                                          ");
            query.AppendLine("             ,rownum ord                                                                                                        ");
            query.AppendLine("         from cm_location                                                                                                       ");
            query.AppendLine("        where 1 = 1                                                                                                             ");
            query.AppendLine("          and ftr_code = 'BZ003'                                                                                                ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                         ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                   ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                               ");
            query.AppendLine("      ) c1                                                                                                                      ");
            query.AppendLine("       ,cm_location c2                                                                                                          ");
            query.AppendLine("       ,cm_location c3                                                                                                          ");
            query.AppendLine(" where 1 = 1                                                                                                                    ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                            ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                            ");
            query.AppendLine(" order by c1.ord                                                                                                                ");
            query.AppendLine(")                                                                                                                               ");
            query.AppendLine("select datee                                                                                                                    ");
            query.AppendLine("      ,to_number(decode(max(total_sum),0, null, max(total_sum))) total_sum                                                      ");
            query.AppendLine("  from                                                                                                                          ");
            query.AppendLine("      (                                                                                                                         ");
            query.AppendLine("       select to_date(to_char(iay.timestamp,'yyyymmdd'),'yyyymmdd') datee                                                       ");
            query.AppendLine("             ,round(sum(iay.value),2) total_sum                                                                                 ");
            query.AppendLine("         from loc                                                                                                               ");
            query.AppendLine("             ,if_ihtags iih                                                                                                     ");
            query.AppendLine("             ,if_tag_gbn itg                                                                                                    ");
            query.AppendLine("             ,if_accumulation_yesterday iay                                                                                     ");
            query.AppendLine("        where iih.loc_code = loc.loc_code                                                                                       ");
            query.AppendLine("          and iih.tagname = itg.tagname                                                                                         ");
            query.AppendLine("          and itg.tag_gbn = 'YD'                                                                                                ");
            query.AppendLine("          and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                        ");
            query.AppendLine("          and iay.tagname = iih.tagname                                                                                         ");
            query.AppendLine("          and iay.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and  to_date(:ENDDATE||'2359','yyyymmddhh24mi')");
            query.AppendLine("        group by to_date(to_char(iay.timestamp,'yyyymmdd'),'yyyymmdd')                                                          ");
            query.AppendLine("       union all                                                                                                                ");
            query.AppendLine("       select to_date(:STARTDATE, 'yyyymmdd') + rownum -1 , 0 from dual                                                         ");
            query.AppendLine("      connect by rownum < to_date(:ENDDATE,'yyyymmdd') - to_date(:STARTDATE,'yyyymmdd') + 2                                     ");
            query.AppendLine("      )                                                                                                                         ");
            query.AppendLine(" group by datee order by datee                                                                                                  ");
            
            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];
            parameters[5].Value = parameter["STARTDATE"];

            //println(query);

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        //일별공급량 중_소블록 목록
        public void SelectDaySupply_MiddleInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                       ");
            //query.AppendLine("(                                                                                                                                 ");
            //query.AppendLine("select c1.loc_code                                                                                                                ");
            //query.AppendLine("      ,c1.sgccd                                                                                                                   ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))   ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                    ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                    ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                           ");
            //query.AppendLine("	    ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															            ");
            //query.AppendLine("	    ,decode(c2.kt_gbn, '002', '면지역', c2.loc_name) middle_name															    ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                       ");
            //query.AppendLine("      ,c1.orderby s_orderby																									    ");
            //query.AppendLine("      ,c2.orderby m_orderby																									    ");
            //query.AppendLine("  from                                                                                                                            ");
            //query.AppendLine("      (                                                                                                                           ");
            //query.AppendLine("       select sgccd                                                                                                               ");
            //query.AppendLine("             ,loc_code                                                                                                            ");
            //query.AppendLine("             ,ploc_code                                                                                                           ");
            //query.AppendLine("             ,loc_name                                                                                                            ");
            //query.AppendLine("             ,ftr_idn                                                                                                             ");
            //query.AppendLine("             ,ftr_code                                                                                                            ");
            //query.AppendLine("             ,res_code                                                                                                            ");
            //query.AppendLine("             ,orderby																										        ");
            //query.AppendLine("             ,rownum ord                                                                                                          ");
            //query.AppendLine("         from cm_location                                                                                                         ");
            //query.AppendLine("        where 1 = 1                                                                                                               ");
            //query.AppendLine("          and ftr_code = 'BZ003'                                                                                                  ");
            //query.AppendLine("        start with loc_code = :LOC_CODE                                                                                           ");
            //query.AppendLine("        connect by prior loc_code = ploc_code                                                                                     ");
            //query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                 ");
            //query.AppendLine("      ) c1                                                                                                                        ");
            //query.AppendLine("       ,cm_location c2                                                                                                            ");
            //query.AppendLine("       ,cm_location c3                                                                                                            ");
            //query.AppendLine(" where 1 = 1                                                                                                                      ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                              ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                              ");
            //query.AppendLine(" order by c1.ord                                                                                                                  ");
            //query.AppendLine(")                                                                                                                                 ");
            //query.AppendLine("select loc.middle_code mblock, loc.sblock, loc.middle_name, loc.small_name                                                        ");
            //query.AppendLine("  from loc                                                                                                                        ");
            //query.AppendLine("      ,if_ihtags iih                                                                                                              ");
            //query.AppendLine("      ,if_tag_gbn itg                                                                                                             ");
            //query.AppendLine(" where iih.loc_code = loc.loc_code                                                                                                ");
            //query.AppendLine("   and itg.tagname = iih.tagname                                                                                                  ");
            //query.AppendLine("   and itg.tag_gbn = 'YD'                                                                                                         ");
            //query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                                 ");
            //query.AppendLine(" order by loc.m_orderby, loc.s_orderby																						    ");

            query.AppendLine("with loc as                                                                                                                       ");
            query.AppendLine("(                                                                                                                                 ");
            query.AppendLine("select c1.loc_code                                                                                                                ");
            query.AppendLine("      ,c1.sgccd                                                                                                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                           ");
            query.AppendLine("	    ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															            ");
            query.AppendLine("	    ,decode(c2.kt_gbn, '002', '면지역', c2.loc_name) middle_name															    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                       ");
            query.AppendLine("      ,c1.orderby s_orderby																									    ");
            query.AppendLine("      ,c2.orderby m_orderby																									    ");
            query.AppendLine("  from                                                                                                                            ");
            query.AppendLine("      (                                                                                                                           ");
            query.AppendLine("       select sgccd                                                                                                               ");
            query.AppendLine("             ,loc_code                                                                                                            ");
            query.AppendLine("             ,ploc_code                                                                                                           ");
            query.AppendLine("             ,loc_name                                                                                                            ");
            query.AppendLine("             ,ftr_idn                                                                                                             ");
            query.AppendLine("             ,ftr_code                                                                                                            ");
            query.AppendLine("             ,res_code                                                                                                            ");
            query.AppendLine("             ,orderby																										        ");
            query.AppendLine("             ,rownum ord                                                                                                          ");
            query.AppendLine("         from cm_location2                                                                                                         ");
            query.AppendLine("        where 1 = 1                                                                                                               ");
            query.AppendLine("          and ftr_code = 'BZ003'                                                                                                  ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                           ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                     ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                 ");
            query.AppendLine("      ) c1                                                                                                                        ");
            query.AppendLine("       ,cm_location2 c2                                                                                                            ");
            query.AppendLine("       ,cm_location2 c3                                                                                                            ");
            query.AppendLine(" where 1 = 1                                                                                                                      ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                              ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                              ");
            query.AppendLine(" order by c1.ord                                                                                                                  ");
            query.AppendLine(")                                                                                                                                 ");
            query.AppendLine("select loc.middle_code mblock, loc.sblock, loc.middle_name, loc.small_name                                                        ");
            query.AppendLine("  from loc                                                                                                                        ");
            query.AppendLine("      ,if_ihtags iih                                                                                                              ");
            query.AppendLine("      ,if_tag_gbn itg                                                                                                             ");
            query.AppendLine(" where iih.loc_code2 = loc.loc_code                                                                                                ");
            query.AppendLine("   and itg.tagname = iih.tagname                                                                                                  ");
            query.AppendLine("   and itg.tag_gbn = 'YD'                                                                                                         ");
            query.AppendLine("   and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                                 ");
            query.AppendLine(" group by loc.middle_code, loc.sblock, loc.middle_name, loc.small_name,loc.m_orderby, loc.s_orderby                               ");
            query.AppendLine(" order by loc.m_orderby, loc.s_orderby																						    ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //일별공급량 값 검색
        public void SelectDaySupply_SmallInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine(" select lblock                                                                                                                      ");
            //query.AppendLine("      ,mblock                                                                                                                       ");
            //query.AppendLine("      ,decode(sblock,'_SUM',mblock||sblock,sblock) sblock                                                                           ");
            //query.AppendLine("      ,datee                                                                                                                        ");
            //query.AppendLine("      ,flow                                                                                                                         ");
            //query.AppendLine("  from                                                                                                                              ");
            //query.AppendLine("    (                                                                                                                               ");
            //query.AppendLine("    with loc as                                                                                                                     ");
            //query.AppendLine("    (                                                                                                                               ");
            //query.AppendLine("    select c1.loc_code                                                                                                              ");
            //query.AppendLine("          ,c1.sgccd                                                                                                                 ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code))) ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                  ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                  ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                         ");
            //query.AppendLine("	        ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															          ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                             ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                     ");
            //query.AppendLine("          ,c1.ord                                                                                                                   ");
            //query.AppendLine("      from                                                                                                                          ");
            //query.AppendLine("          (                                                                                                                         ");
            //query.AppendLine("           select sgccd                                                                                                             ");
            //query.AppendLine("                 ,loc_code                                                                                                          ");
            //query.AppendLine("                 ,ploc_code                                                                                                         ");
            //query.AppendLine("                 ,loc_name                                                                                                          ");
            //query.AppendLine("                 ,ftr_idn                                                                                                           ");
            //query.AppendLine("                 ,ftr_code                                                                                                          ");
            //query.AppendLine("                 ,res_code                                                                                                          ");
            //query.AppendLine("                 ,rownum ord                                                                                                        ");
            //query.AppendLine("             from cm_location                                                                                                       ");
            //query.AppendLine("            where 1 = 1                                                                                                             ");
            //query.AppendLine("              and ftr_code = 'BZ003'                                                                                                ");
            //query.AppendLine("            start with loc_code = :LOC_CODE                                                                                         ");
            //query.AppendLine("            connect by prior loc_code = ploc_code                                                                                   ");
            //query.AppendLine("            order SIBLINGS by ftr_idn                                                                                               ");
            //query.AppendLine("          ) c1                                                                                                                      ");
            //query.AppendLine("           ,cm_location c2                                                                                                          ");
            //query.AppendLine("           ,cm_location c3                                                                                                          ");
            //query.AppendLine("     where 1 = 1                                                                                                                    ");
            //query.AppendLine("       and c1.ploc_code = c2.loc_code(+)                                                                                            ");
            //query.AppendLine("       and c2.ploc_code = c3.loc_code(+)                                                                                            ");
            //query.AppendLine("     order by c1.ord                                                                                                                ");
            //query.AppendLine("    )                                                                                                                               ");
            //query.AppendLine("    select loc.lblock                                                                                                               ");
            //query.AppendLine("          ,loc.middle_code mblock                                                                                                   ");
            //query.AppendLine("          ,loc.sblock                                                                                                               ");
            //query.AppendLine("          ,to_date(to_char(iay.timestamp,'yyyymmdd'),'yyyymmdd') datee                                                              ");
            //query.AppendLine("          ,to_number(sum(iay.value)) flow                                                                                           ");
            //query.AppendLine("      from loc                                                                                                                      ");
            //query.AppendLine("          ,if_ihtags iih                                                                                                            ");
            //query.AppendLine("          ,if_tag_gbn itg                                                                                                           ");
            //query.AppendLine("          ,if_accumulation_yesterday iay                                                                                            ");
            //query.AppendLine("     where iih.loc_code = loc.loc_code                                                                                              ");
            //query.AppendLine("       and iih.tagname = itg.tagname                                                                                                ");
            //query.AppendLine("       and itg.tag_gbn = 'YD'                                                                                                       ");
            //query.AppendLine("       and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                               ");
            //query.AppendLine("       and iay.tagname = iih.tagname                                                                                                ");
            //query.AppendLine("       and iay.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and  to_date(:ENDDATE||'2359','yyyymmddhh24mi')       ");
            //query.AppendLine("     group by loc.lblock, loc.middle_code, loc.sblock, to_date(to_char(iay.timestamp,'yyyymmdd'),'yyyymmdd')                        ");
            //query.AppendLine("    )                                                                                                                               ");
            //query.AppendLine(" model                                                                                                                              ");
            //query.AppendLine(" partition by (lblock, mblock, datee)                                                                                               ");
            //query.AppendLine(" dimension by (sblock)                                                                                                              ");
            //query.AppendLine(" measures (flow)                                                                                                                    ");
            //query.AppendLine(" ignore nav                                                                                                                         ");
            //query.AppendLine(" rules ( flow['_SUM'] = sum(flow)[sblock is not null] )                                                                             ");
            //query.AppendLine(" order by lblock, mblock, sblock, datee                                                                                             ");

            query.AppendLine(" select lblock                                                                                                                      ");
            query.AppendLine("      ,mblock                                                                                                                       ");
            query.AppendLine("      ,decode(sblock,'_SUM',mblock||sblock,sblock) sblock                                                                           ");
            query.AppendLine("      ,datee                                                                                                                        ");
            query.AppendLine("      ,flow                                                                                                                         ");
            query.AppendLine("  from                                                                                                                              ");
            query.AppendLine("    (                                                                                                                               ");
            query.AppendLine("    with loc as                                                                                                                     ");
            query.AppendLine("    (                                                                                                                               ");
            query.AppendLine("    select c1.loc_code                                                                                                              ");
            query.AppendLine("          ,c1.sgccd                                                                                                                 ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code))) ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                  ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                  ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                         ");
            query.AppendLine("	        ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															          ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                             ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))        ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                     ");
            query.AppendLine("          ,c1.ord                                                                                                                   ");
            query.AppendLine("      from                                                                                                                          ");
            query.AppendLine("          (                                                                                                                         ");
            query.AppendLine("           select sgccd                                                                                                             ");
            query.AppendLine("                 ,loc_code                                                                                                          ");
            query.AppendLine("                 ,ploc_code                                                                                                         ");
            query.AppendLine("                 ,loc_name                                                                                                          ");
            query.AppendLine("                 ,ftr_idn                                                                                                           ");
            query.AppendLine("                 ,ftr_code                                                                                                          ");
            query.AppendLine("                 ,res_code                                                                                                          ");
            query.AppendLine("                 ,rownum ord                                                                                                        ");
            query.AppendLine("             from cm_location2                                                                                                       ");
            query.AppendLine("            where 1 = 1                                                                                                             ");
            query.AppendLine("              and ftr_code = 'BZ003'                                                                                                ");
            query.AppendLine("            start with loc_code = :LOC_CODE                                                                                         ");
            query.AppendLine("            connect by prior loc_code = ploc_code                                                                                   ");
            query.AppendLine("            order SIBLINGS by ftr_idn                                                                                               ");
            query.AppendLine("          ) c1                                                                                                                      ");
            query.AppendLine("           ,cm_location2 c2                                                                                                          ");
            query.AppendLine("           ,cm_location2 c3                                                                                                          ");
            query.AppendLine("     where 1 = 1                                                                                                                    ");
            query.AppendLine("       and c1.ploc_code = c2.loc_code(+)                                                                                            ");
            query.AppendLine("       and c2.ploc_code = c3.loc_code(+)                                                                                            ");
            query.AppendLine("     order by c1.ord                                                                                                                ");
            query.AppendLine("    )                                                                                                                               ");
            query.AppendLine("    select loc.lblock                                                                                                               ");
            query.AppendLine("          ,loc.middle_code mblock                                                                                                   ");
            query.AppendLine("          ,loc.sblock                                                                                                               ");
            query.AppendLine("          ,to_date(to_char(iay.timestamp,'yyyymmdd'),'yyyymmdd') datee                                                              ");
            query.AppendLine("          ,to_number(sum(iay.value)) flow                                                                                           ");
            query.AppendLine("      from loc                                                                                                                      ");
            query.AppendLine("          ,if_ihtags iih                                                                                                            ");
            query.AppendLine("          ,if_tag_gbn itg                                                                                                           ");
            query.AppendLine("          ,if_accumulation_yesterday iay                                                                                            ");
            query.AppendLine("     where iih.loc_code2 = loc.loc_code                                                                                              ");
            query.AppendLine("       and iih.tagname = itg.tagname                                                                                                ");
            query.AppendLine("       and itg.tag_gbn = 'YD'                                                                                                       ");
            query.AppendLine("       and itg.tagname not in (select tagname from if_tag_gbn where tag_gbn = 'YD_R')                                               ");
            query.AppendLine("       and iay.tagname = iih.tagname                                                                                                ");
            query.AppendLine("       and iay.timestamp between to_date(:STARTDATE||'0000','yyyymmddhh24mi') and  to_date(:ENDDATE||'2359','yyyymmddhh24mi')       ");
            query.AppendLine("     group by loc.lblock, loc.middle_code, loc.sblock, to_date(to_char(iay.timestamp,'yyyymmdd'),'yyyymmdd')                        ");
            query.AppendLine("    )                                                                                                                               ");
            query.AppendLine(" model                                                                                                                              ");
            query.AppendLine(" partition by (lblock, mblock, datee)                                                                                               ");
            query.AppendLine(" dimension by (sblock)                                                                                                              ");
            query.AppendLine(" measures (flow)                                                                                                                    ");
            query.AppendLine(" ignore nav                                                                                                                         ");
            query.AppendLine(" rules ( flow['_SUM'] = sum(flow)[sblock is not null] )                                                                             ");
            query.AppendLine(" order by lblock, mblock, sblock, datee                                                                                             ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        ////////// 시단위 공급량
        //시단위공급량 날짜, 총계 목록
        public void SelectTimeSupply_TotalInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                          ");
            query.AppendLine("(																																	   ");
            query.AppendLine("select c1.loc_code																												   ");
            query.AppendLine("	  ,c1.sgccd																														   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))		   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock										   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock										   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock												   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name									   ");
            query.AppendLine("	  ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))			   ");
            query.AppendLine("	  ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name											   ");
            query.AppendLine("	  ,c1.ord																														   ");
            query.AppendLine("  from																															   ");
            query.AppendLine("	  (																																   ");
            query.AppendLine("	   select sgccd																													   ");
            query.AppendLine("			 ,loc_code																												   ");
            query.AppendLine("			 ,ploc_code																												   ");
            query.AppendLine("			 ,loc_name																												   ");
            query.AppendLine("			 ,ftr_idn																												   ");
            query.AppendLine("			 ,ftr_code																												   ");
            query.AppendLine("			 ,res_code																												   ");
            query.AppendLine("			 ,rownum ord																											   ");
            query.AppendLine("		 from cm_location																											   ");
            query.AppendLine("		where 1 = 1																													   ");
            query.AppendLine("		  and ftr_code = 'BZ003'																									   ");
            query.AppendLine("		start with loc_code = :LOC_CODE																								   ");
            query.AppendLine("		connect by prior loc_code = ploc_code																						   ");
            query.AppendLine("		order SIBLINGS by ftr_idn																									   ");
            query.AppendLine("	  ) c1																															   ");
            query.AppendLine("	   ,cm_location c2																												   ");
            query.AppendLine("	   ,cm_location c3																												   ");
            query.AppendLine(" where 1 = 1																														   ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																								   ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																								   ");
            query.AppendLine(" order by c1.ord																													   ");
            query.AppendLine(")																																	   ");
            query.AppendLine("select datee																														   ");
            //query.AppendLine("      ,decode(to_char(datee,'hh24'),'00','24',to_char(datee,'hh24'))||':00' v_time												   ");
            query.AppendLine("      ,datee v_time												   ");
            query.AppendLine("	  ,to_number(decode(max(total_sum),0, null, max(total_sum))) total_sum															   ");
            query.AppendLine("  from																															   ");
            query.AppendLine("	  (																																   ");
            query.AppendLine("	   select to_date(to_char(iah.timestamp,'yyyymmddhh24')||'00','yyyymmddhh24mi') datee											   ");
            query.AppendLine("		   ,'' v_time																												   ");
            query.AppendLine("			 ,sum(iah.value) total_sum																								   ");
            query.AppendLine("		 from loc																													   ");
            query.AppendLine("			 ,if_ihtags iih																											   ");
            query.AppendLine("			 ,if_tag_gbn itg																										   ");
            query.AppendLine("			 ,if_accumulation_hour iah																								   ");
            query.AppendLine("		where iih.loc_code = loc.loc_code																							   ");
            query.AppendLine("		  and iih.tagname = itg.tagname																								   ");
            query.AppendLine("		  and itg.tag_gbn = 'FRQ'																									   ");
            query.AppendLine("		  and iah.tagname = iih.tagname																								   ");
            query.AppendLine("		  and iah.timestamp between to_date(:STARTDATE||'0100','yyyymmddhh24mi') and  to_date(:ENDDATE||'0000','yyyymmddhh24mi')       ");
            query.AppendLine("		group by to_date(to_char(iah.timestamp,'yyyymmddhh24')||'00','yyyymmddhh24mi')												   ");
            query.AppendLine("	   union all																													   ");
            query.AppendLine("	   select (to_date(:STARTDATE || '01', 'yyyymmddhh24') + (LEVEL-1)/24) , '', 0 from dual                                           ");
            query.AppendLine("	            connect by LEVEL <=  (TO_DATE(:ENDDATE || '2359', 'yyyymmddhh24mi') - TO_DATE(:STARTDATE, 'YYYYMMDD'))*(24)			   ");
            query.AppendLine("	  )																																   ");
            query.AppendLine(" group by datee order by datee																									   ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //20140512_승일수정
        //메소드(SelectTimeSupply_TotalInfo2) 추가 및 쿼리 추가
        public void SelectTimeSupply_TotalInfo2(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("WITH LOC AS");
            query.AppendLine("            (");
            query.AppendLine("            SELECT C1.LOC_CODE");
            query.AppendLine("                 , DECODE(C1.PLOC_CODE, NULL, DECODE(C2.LOC_CODE, NULL, DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, C1.LOC_NAME)))");
            query.AppendLine("                 , DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, C2.LOC_NAME), C3.LOC_NAME)) LBLOCK");
            query.AppendLine("                 , DECODE(C1.PLOC_CODE, NULL, DECODE(C2.LOC_CODE, NULL, DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, NULL)))");
            query.AppendLine("                 , DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, C1.LOC_NAME), C2.LOC_NAME)) MBLOCK");
            query.AppendLine("                 , DECODE(C1.PLOC_CODE, NULL, DECODE(C2.LOC_CODE, NULL, DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, NULL)))");
            query.AppendLine("                 , DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, NULL), C1.LOC_NAME)) SBLOCK");
            query.AppendLine("                 , DECODE(C1.KT_GBN, '002', (SELECT LOC_CODE FROM CM_LOCATION WHERE PLOC_CODE = C1.LOC_CODE), C1.LOC_CODE) TAG_LOC_CODE");
            query.AppendLine("                 , C1.FTR_CODE");
            query.AppendLine("                 , C4.TAG_GBN");
            query.AppendLine("                 , C1.LOC_IDX");
            query.AppendLine("                 --, decode(c4.tag_gbn, 'SPL_D', '1', 'FRQ', '2', 'SPL_I', '3', 'SPL_O', '4') ord");
            query.AppendLine("             FROM (");
            query.AppendLine("                   SELECT LOC_CODE");
            query.AppendLine("                        , PLOC_CODE");
            query.AppendLine("                        , LOC_NAME");
            query.AppendLine("                        , KT_GBN");
            query.AppendLine("                        , FTR_CODE");
            query.AppendLine("                        , ROWNUM LOC_IDX");
            query.AppendLine("                     FROM CM_LOCATION2");
            query.AppendLine("                    START WITH FTR_CODE = 'BZ001'");
            query.AppendLine("                  CONNECT BY PRIOR LOC_CODE = PLOC_CODE");
            query.AppendLine("                  ORDER SIBLINGS BY ORDERBY");
            query.AppendLine("                  ) C1");
            query.AppendLine("            	  , CM_LOCATION2 C2");
            query.AppendLine("            	  , CM_LOCATION2 C3");
            query.AppendLine("                , (");
            query.AppendLine("            SELECT 'BZ001' FTR_CODE, 'FRQ' TAG_GBN FROM DUAL ");
            query.AppendLine("             UNION ALL");
            query.AppendLine("            SELECT 'BZ001' FTR_CODE, 'SPL_O' TAG_GBN FROM DUAL");
            query.AppendLine("             UNION ALL");
            query.AppendLine("            SELECT 'BZ002' FTR_CODE, 'SPL_D' TAG_GBN FROM DUAL ");
            query.AppendLine("             UNION ALL");
            query.AppendLine("            SELECT 'BZ002' FTR_CODE, 'SPL_I' TAG_GBN FROM DUAL ");
            query.AppendLine("             UNION ALL");
            query.AppendLine("            SELECT 'BZ002' FTR_CODE, 'FRQ' TAG_GBN FROM DUAL ");
            query.AppendLine("             UNION ALL");
            query.AppendLine("            SELECT 'BZ002' FTR_CODE, 'SPL_O' TAG_GBN FROM DUAL");
            query.AppendLine("             UNION ALL");
            query.AppendLine("            SELECT 'BZ003' FTR_CODE, 'FRQ' TAG_GBN FROM DUAL");
            query.AppendLine("                 ) C4");
            query.AppendLine("             WHERE 1 = 1");
            query.AppendLine("               AND C1.PLOC_CODE = C2.LOC_CODE(+)");
            query.AppendLine("               AND C2.PLOC_CODE = C3.LOC_CODE(+)");
            query.AppendLine("               AND C4.FTR_CODE = C1.FTR_CODE");
            query.AppendLine("            )");
            query.AppendLine("            SELECT A.LOC_CODE");
            query.AppendLine("                 --, B.TAGNAME");
            query.AppendLine("                 , A.LBLOCK");
            query.AppendLine("                 , A.MBLOCK");
            query.AppendLine("                 , A.SBLOCK             ");
            query.AppendLine("                 , DECODE(A.TAG_GBN, 'FRQ', '블록') TAG_GBN --삭제");
            query.AppendLine("                 --, DECODE(A.TAG_GBN, 'SPL_O', '(배)유출', DECODE(A.TAG_GBN, 'FRQ', '블록', DECODE(A.TAG_GBN, 'SPL_I', '(배)유입', DECODE(A.TAG_GBN,'SPL_D','분기', NULL)))) TAG_GBN");
            query.AppendLine("                 , SUM(C.VALUE) DAYSUM");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '01', C.VALUE)) HH01");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '02', C.VALUE)) HH02");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '03', C.VALUE)) HH03");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '04', C.VALUE)) HH04");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '05', C.VALUE)) HH05");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '06', C.VALUE)) HH06");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '07', C.VALUE)) HH07");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '08', C.VALUE)) HH08");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '09', C.VALUE)) HH09");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '10', C.VALUE)) HH10");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '11', C.VALUE)) HH11");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '12', C.VALUE)) HH12");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '13', C.VALUE)) HH13");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '14', C.VALUE)) HH14");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '15', C.VALUE)) HH15");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '16', C.VALUE)) HH16");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '17', C.VALUE)) HH17");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '18', C.VALUE)) HH18");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '19', C.VALUE)) HH19");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '20', C.VALUE)) HH20");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '21', C.VALUE)) HH21");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '22', C.VALUE)) HH22");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '23', C.VALUE)) HH23");
            query.AppendLine("                 , SUM(DECODE(TO_CHAR(C.TIMESTAMP,'HH24'), '00', C.VALUE)) HH24");
            query.AppendLine("              FROM LOC A");
            query.AppendLine("                 , (");
            query.AppendLine("                   SELECT A.LOC_CODE2");
            query.AppendLine("                        , B.TAG_GBN");
            query.AppendLine("                        , A.TAGNAME");
            query.AppendLine("                     FROM IF_IHTAGS A");
            query.AppendLine("                        , IF_TAG_GBN B");
            query.AppendLine("                     WHERE B.TAG_GBN IN ('FRQ') --삭제");
            query.AppendLine("                   -- WHERE B.TAG_GBN IN ('FRQ', 'SPL_O', 'SPL_I', 'SPL_D')");
            query.AppendLine("                      AND A.TAGNAME = B.TAGNAME");
            query.AppendLine("                      AND A.LOC_CODE2 IS NOT NULL");
            query.AppendLine("                   ) B");
            query.AppendLine("                  , IF_ACCUMULATION_HOUR C");
            query.AppendLine("             WHERE B.LOC_CODE2 = A.LOC_CODE(+)");
            query.AppendLine("               AND B.TAG_GBN = A.TAG_GBN(+)");
            query.AppendLine("               AND C.TAGNAME(+) = B.TAGNAME");
            query.AppendLine("               AND C.TIMESTAMP(+) >= TO_DATE(:STARTDATE||'01','YYYYMMDDHH24')");
            query.AppendLine("               AND C.TIMESTAMP(+) <= TO_DATE(:STARTDATE||'00','YYYYMMDDHH24') + 1");
            query.AppendLine("             GROUP BY ");
            query.AppendLine("                   A.LOC_CODE");
            query.AppendLine("                 , A.LBLOCK");
            query.AppendLine("                 , A.MBLOCK");
            query.AppendLine("                 , A.SBLOCK                 ");
            query.AppendLine("                 , A.TAG_GBN");
            query.AppendLine("                 , A.LOC_IDX");
            query.AppendLine("                 --, B.TAGNAME");
            query.AppendLine("                 --, a.ord");
            query.AppendLine("             ORDER BY ");
            query.AppendLine("                   A.LOC_IDX");
            query.AppendLine("                 --, a.ord");

            IDataParameter[] parameters =  {
                      new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                };
                        
            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["STARTDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //시단위공급량 중_소블록 목록
        public void SelectTimeSupply_MiddleInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                       ");
            //query.AppendLine("(                                                                                                                                 ");
            //query.AppendLine("select c1.loc_code                                                                                                                ");
            //query.AppendLine("      ,c1.sgccd                                                                                                                   ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))   ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                    ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                    ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                           ");
            //query.AppendLine("	    ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															            ");
            //query.AppendLine("	    ,decode(c2.kt_gbn, '002', '면지역', c2.loc_name) middle_name                                                                ");
            //query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            //query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                       ");
            //query.AppendLine("      ,c1.orderby s_orderby																									    ");
            //query.AppendLine("      ,c2.orderby m_orderby																									    ");
            //query.AppendLine("  from                                                                                                                            ");
            //query.AppendLine("      (                                                                                                                           ");
            //query.AppendLine("       select sgccd                                                                                                               ");
            //query.AppendLine("             ,loc_code                                                                                                            ");
            //query.AppendLine("             ,ploc_code                                                                                                           ");
            //query.AppendLine("             ,loc_name                                                                                                            ");
            //query.AppendLine("             ,ftr_idn                                                                                                             ");
            //query.AppendLine("             ,ftr_code                                                                                                            ");
            //query.AppendLine("             ,res_code                                                                                                            ");
            //query.AppendLine("             ,orderby																										        ");
            //query.AppendLine("             ,rownum ord                                                                                                          ");
            //query.AppendLine("         from cm_location                                                                                                         ");
            //query.AppendLine("        where 1 = 1                                                                                                               ");
            //query.AppendLine("          and ftr_code = 'BZ003'                                                                                                  ");
            //query.AppendLine("        start with loc_code = :LOC_CODE                                                                                           ");
            //query.AppendLine("        connect by prior loc_code = ploc_code                                                                                     ");
            //query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                 ");
            //query.AppendLine("      ) c1                                                                                                                        ");
            //query.AppendLine("       ,cm_location c2                                                                                                            ");
            //query.AppendLine("       ,cm_location c3                                                                                                            ");
            //query.AppendLine(" where 1 = 1                                                                                                                      ");
            //query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                              ");
            //query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                              ");
            //query.AppendLine(" order by c1.ord                                                                                                                  ");
            //query.AppendLine(")                                                                                                                                 ");
            //query.AppendLine("select loc.middle_code mblock, loc.sblock, loc.middle_name, loc.small_name                                                        ");
            //query.AppendLine("  from loc                                                                                                                        ");
            //query.AppendLine("      ,if_ihtags iih                                                                                                              ");
            //query.AppendLine("      ,if_tag_gbn itg                                                                                                             ");
            //query.AppendLine(" where iih.loc_code = loc.loc_code                                                                                                ");
            //query.AppendLine("   and itg.tagname = iih.tagname                                                                                                  ");
            //query.AppendLine("   and itg.tag_gbn = 'FRQ'                                                                                                        ");
            //query.AppendLine(" order by loc.m_orderby, loc.s_orderby									 													    ");

            query.AppendLine("with loc as                                                                                                                       ");
            query.AppendLine("(                                                                                                                                 ");
            query.AppendLine("select c1.loc_code                                                                                                                ");
            query.AppendLine("      ,c1.sgccd                                                                                                                   ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))   ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                           ");
            query.AppendLine("	    ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code															            ");
            query.AppendLine("	    ,decode(c2.kt_gbn, '002', '면지역', c2.loc_name) middle_name                                                                ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))          ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                       ");
            query.AppendLine("      ,c1.orderby s_orderby																									    ");
            query.AppendLine("      ,c2.orderby m_orderby																									    ");
            query.AppendLine("  from                                                                                                                            ");
            query.AppendLine("      (                                                                                                                           ");
            query.AppendLine("       select sgccd                                                                                                               ");
            query.AppendLine("             ,loc_code                                                                                                            ");
            query.AppendLine("             ,ploc_code                                                                                                           ");
            query.AppendLine("             ,loc_name                                                                                                            ");
            query.AppendLine("             ,ftr_idn                                                                                                             ");
            query.AppendLine("             ,ftr_code                                                                                                            ");
            query.AppendLine("             ,res_code                                                                                                            ");
            query.AppendLine("             ,orderby																										        ");
            query.AppendLine("             ,rownum ord                                                                                                          ");
            query.AppendLine("         from cm_location2                                                                                                         ");
            query.AppendLine("        where 1 = 1                                                                                                               ");
            query.AppendLine("          and ftr_code = 'BZ003'                                                                                                  ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                           ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                     ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                 ");
            query.AppendLine("      ) c1                                                                                                                        ");
            query.AppendLine("       ,cm_location2 c2                                                                                                            ");
            query.AppendLine("       ,cm_location2 c3                                                                                                            ");
            query.AppendLine(" where 1 = 1                                                                                                                      ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                              ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                              ");
            query.AppendLine(" order by c1.ord                                                                                                                  ");
            query.AppendLine(")                                                                                                                                 ");
            query.AppendLine("select loc.middle_code mblock, loc.sblock, loc.middle_name, loc.small_name                                                        ");
            query.AppendLine("  from loc                                                                                                                        ");
            query.AppendLine("      ,if_ihtags iih                                                                                                              ");
            query.AppendLine("      ,if_tag_gbn itg                                                                                                             ");
            query.AppendLine(" where iih.loc_code2 = loc.loc_code                                                                                                ");
            query.AppendLine("   and itg.tagname = iih.tagname                                                                                                  ");
            query.AppendLine("   and itg.tag_gbn = 'FRQ'                                                                                                        ");
            query.AppendLine(" order by loc.m_orderby, loc.s_orderby									 													    ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        //시단위공급량 값 검색
        public void SelectTimeSupply_SmallInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("select lblock                                                                                                                        ");
            //query.AppendLine("      ,mblock                                                                                                                        ");
            //query.AppendLine("      ,decode(sblock,'_SUM',mblock||sblock,sblock) sblock                                                                            ");
            //query.AppendLine("      ,datee                                                                                                                         ");
            //query.AppendLine("      ,flow                                                                                                                          ");
            //query.AppendLine("  from                                                                                                                               ");
            //query.AppendLine("    (                                                                                                                                ");
            //query.AppendLine("    with loc as                                                                                                                      ");
            //query.AppendLine("    (                                                                                                                                ");
            //query.AppendLine("    select c1.loc_code                                                                                                               ");
            //query.AppendLine("          ,c1.sgccd                                                                                                                  ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))  ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                   ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                   ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                          ");
            //query.AppendLine("	        ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code		                                                               ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                              ");
            //query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            //query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                      ");
            //query.AppendLine("          ,c1.ord                                                                                                                    ");
            //query.AppendLine("      from                                                                                                                           ");
            //query.AppendLine("          (                                                                                                                          ");
            //query.AppendLine("           select sgccd                                                                                                              ");
            //query.AppendLine("                 ,loc_code                                                                                                           ");
            //query.AppendLine("                 ,ploc_code                                                                                                          ");
            //query.AppendLine("                 ,loc_name                                                                                                           ");
            //query.AppendLine("                 ,ftr_idn                                                                                                            ");
            //query.AppendLine("                 ,ftr_code                                                                                                           ");
            //query.AppendLine("                 ,res_code                                                                                                           ");
            //query.AppendLine("                 ,rownum ord                                                                                                         ");
            //query.AppendLine("             from cm_location                                                                                                        ");
            //query.AppendLine("            where 1 = 1                                                                                                              ");
            //query.AppendLine("              and ftr_code = 'BZ003'                                                                                                 ");
            //query.AppendLine("            start with loc_code = :LOC_CODE                                                                                          ");
            //query.AppendLine("            connect by prior loc_code = ploc_code                                                                                    ");
            //query.AppendLine("            order SIBLINGS by ftr_idn                                                                                                ");
            //query.AppendLine("          ) c1                                                                                                                       ");
            //query.AppendLine("           ,cm_location c2                                                                                                           ");
            //query.AppendLine("           ,cm_location c3                                                                                                           ");
            //query.AppendLine("     where 1 = 1                                                                                                                     ");
            //query.AppendLine("       and c1.ploc_code = c2.loc_code(+)                                                                                             ");
            //query.AppendLine("       and c2.ploc_code = c3.loc_code(+)                                                                                             ");
            //query.AppendLine("     order by c1.ord                                                                                                                 ");
            //query.AppendLine("    )                                                                                                                                ");
            //query.AppendLine("    select loc.lblock                                                                                                                ");
            //query.AppendLine("          ,loc.middle_code mblock                                                                                                    ");
            //query.AppendLine("          ,loc.sblock                                                                                                                ");
            //query.AppendLine("          ,to_date(to_char(iah.timestamp,'yyyymmddhh24')||'00','yyyymmddhh24mi') datee                                               ");
            //query.AppendLine("          ,to_number(sum(iah.value)) flow                                                                                            ");
            //query.AppendLine("      from loc                                                                                                                       ");
            //query.AppendLine("          ,if_ihtags iih                                                                                                             ");
            //query.AppendLine("          ,if_tag_gbn itg                                                                                                            ");
            //query.AppendLine("          ,if_accumulation_hour iah                                                                                                  ");
            //query.AppendLine("     where iih.loc_code = loc.loc_code                                                                                               ");
            //query.AppendLine("       and iih.tagname = itg.tagname                                                                                                 ");
            //query.AppendLine("       and itg.tag_gbn = 'FRQ'                                                                                                       ");
            //query.AppendLine("       and iah.tagname = iih.tagname                                                                                                 ");
            //query.AppendLine("       and iah.timestamp between to_date(:STARTDATE||'0100','yyyymmddhh24mi') and  to_date(:STARTDATE||'0000','yyyymmddhh24mi')+1    ");
            //query.AppendLine("     group by loc.lblock, loc.middle_code, loc.sblock, to_date(to_char(iah.timestamp,'yyyymmddhh24')||'00','yyyymmddhh24mi')         ");
            //query.AppendLine("    )                                                                                                                                ");
            //query.AppendLine(" model                                                                                                                               ");
            //query.AppendLine(" partition by (lblock, mblock, datee)                                                                                                ");
            //query.AppendLine(" dimension by (sblock)                                                                                                               ");
            //query.AppendLine(" measures (flow)                                                                                                                     ");
            //query.AppendLine(" ignore nav                                                                                                                          ");
            //query.AppendLine(" rules ( flow['_SUM'] = sum(flow)[sblock is not null] )                                                                              ");
            //query.AppendLine(" order by lblock, mblock, sblock, datee                                                                                              ");

            query.AppendLine("select lblock                                                                                                                        ");
            query.AppendLine("      ,mblock                                                                                                                        ");
            query.AppendLine("      ,decode(sblock,'_SUM',mblock||sblock,sblock) sblock                                                                            ");
            query.AppendLine("      ,datee                                                                                                                         ");
            query.AppendLine("      ,flow                                                                                                                          ");
            query.AppendLine("  from                                                                                                                               ");
            query.AppendLine("    (                                                                                                                                ");
            query.AppendLine("    with loc as                                                                                                                      ");
            query.AppendLine("    (                                                                                                                                ");
            query.AppendLine("    select c1.loc_code                                                                                                               ");
            query.AppendLine("          ,c1.sgccd                                                                                                                  ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))  ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                   ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                   ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                          ");
            query.AppendLine("	        ,decode(c2.kt_gbn, '002', 'X', c2.loc_code) middle_code		                                                               ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_name), c2.loc_name)) middle_name                              ");
            query.AppendLine("          ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))         ");
            query.AppendLine("          ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_name)) small_name                                      ");
            query.AppendLine("          ,c1.ord                                                                                                                    ");
            query.AppendLine("      from                                                                                                                           ");
            query.AppendLine("          (                                                                                                                          ");
            query.AppendLine("           select sgccd                                                                                                              ");
            query.AppendLine("                 ,loc_code                                                                                                           ");
            query.AppendLine("                 ,ploc_code                                                                                                          ");
            query.AppendLine("                 ,loc_name                                                                                                           ");
            query.AppendLine("                 ,ftr_idn                                                                                                            ");
            query.AppendLine("                 ,ftr_code                                                                                                           ");
            query.AppendLine("                 ,res_code                                                                                                           ");
            query.AppendLine("                 ,rownum ord                                                                                                         ");
            query.AppendLine("             from cm_location2                                                                                                        ");
            query.AppendLine("            where 1 = 1                                                                                                              ");
            query.AppendLine("              and ftr_code = 'BZ003'                                                                                                 ");
            query.AppendLine("            start with loc_code = :LOC_CODE                                                                                          ");
            query.AppendLine("            connect by prior loc_code = ploc_code                                                                                    ");
            query.AppendLine("            order SIBLINGS by ftr_idn                                                                                                ");
            query.AppendLine("          ) c1                                                                                                                       ");
            query.AppendLine("           ,cm_location2 c2                                                                                                           ");
            query.AppendLine("           ,cm_location2 c3                                                                                                           ");
            query.AppendLine("     where 1 = 1                                                                                                                     ");
            query.AppendLine("       and c1.ploc_code = c2.loc_code(+)                                                                                             ");
            query.AppendLine("       and c2.ploc_code = c3.loc_code(+)                                                                                             ");
            query.AppendLine("     order by c1.ord                                                                                                                 ");
            query.AppendLine("    )                                                                                                                                ");
            query.AppendLine("    select loc.lblock                                                                                                                ");
            query.AppendLine("          ,loc.middle_code mblock                                                                                                    ");
            query.AppendLine("          ,loc.sblock                                                                                                                ");
            query.AppendLine("          ,to_date(to_char(iah.timestamp,'yyyymmddhh24')||'00','yyyymmddhh24mi') datee                                               ");
            query.AppendLine("          ,to_number(sum(iah.value)) flow                                                                                            ");
            query.AppendLine("      from loc                                                                                                                       ");
            query.AppendLine("          ,if_ihtags iih                                                                                                             ");
            query.AppendLine("          ,if_tag_gbn itg                                                                                                            ");
            query.AppendLine("          ,if_accumulation_hour iah                                                                                                  ");
            query.AppendLine("     where iih.loc_code2 = loc.loc_code                                                                                               ");
            query.AppendLine("       and iih.tagname = itg.tagname                                                                                                 ");
            query.AppendLine("       and itg.tag_gbn = 'FRQ'                                                                                                       ");
            query.AppendLine("       and iah.tagname = iih.tagname                                                                                                 ");
            query.AppendLine("       and iah.timestamp between to_date(:STARTDATE||'0100','yyyymmddhh24mi') and  to_date(:ENDDATE||'0000','yyyymmddhh24mi')+1    ");
            query.AppendLine("     group by loc.lblock, loc.middle_code, loc.sblock, to_date(to_char(iah.timestamp,'yyyymmddhh24')||'00','yyyymmddhh24mi')         ");
            query.AppendLine("    )                                                                                                                                ");
            query.AppendLine(" model                                                                                                                               ");
            query.AppendLine(" partition by (lblock, mblock, datee)                                                                                                ");
            query.AppendLine(" dimension by (sblock)                                                                                                               ");
            query.AppendLine(" measures (flow)                                                                                                                     ");
            query.AppendLine(" ignore nav                                                                                                                          ");
            query.AppendLine(" rules ( flow['_SUM'] = sum(flow)[sblock is not null] )                                                                              ");
            query.AppendLine(" order by lblock, mblock, sblock, datee                                                                                              ");


            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }



        ////////// 시단위 공급량
        //시단위공급량 날짜 목록(계통별)
        public void SelectSystemTimeSupply_TotalInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select datee                                                                                         ");
//            query.AppendLine("	  ,decode(to_char(datee,'hh24'),'00','24',to_char(datee,'hh24'))||':00' v_time					   ");
//            query.AppendLine("  from																							   ");
//            query.AppendLine("	  (																								   ");
//            query.AppendLine("	   select to_date(:STARTDATE||'01','yyyymmddhh24') + (rownum-1)/24 datee from dual				   ");
//            query.AppendLine("	  connect by rownum < 25																		   ");

            query.AppendLine("          ,datee v_time                                                                               ");
            query.AppendLine("  from                                                                                                ");
            query.AppendLine("        (                                                                                             ");
            query.AppendLine("          select (to_date(:STARTDATE || '01', 'yyyymmddhh24') + (LEVEL-1)/24)  datee from dual                    ");
            query.AppendLine("          connect by LEVEL <=  (TO_DATE(:ENDDATE || '2359', 'yyyymmddhh24mi') - TO_DATE(:STARTDATE, 'YYYYMMDD'))*(24) ");
            query.AppendLine("	  )																								    ");
            query.AppendLine(" group by datee order by datee																	    ");


            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //시단위공급량 계통구분_계통명 목록(계통별)
        public void SelectSystemTimeSupply_MiddleInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select mblock                                                                         ");
            query.AppendLine("      ,mblock||'_'||sblock sblock 													");
            query.AppendLine("      ,middle_name																	");
            query.AppendLine("      ,small_name																		");
            query.AppendLine("  from 																				");
            query.AppendLine("       (																				");
            query.AppendLine("	    select '1' mblock																");
            query.AppendLine("             ,a.loc_code sblock														");
            query.AppendLine("             ,'정수장 유출' middle_name												");
            query.AppendLine("			 ,(select loc_name from cm_location where loc_code = a.res_code) small_name	");
            query.AppendLine("             ,a.orderby																");
            query.AppendLine("		 from cm_location a																");
            query.AppendLine("		where 1 = 1																		");
            query.AppendLine("		  and a.ftr_code = 'BZ001'														");
            query.AppendLine("		start with a.loc_code = :LOC_CODE												");
            query.AppendLine("		connect by prior a.loc_code = a.ploc_code										");
            query.AppendLine("        union all																		");
            query.AppendLine("	   select '2' mblock																");
            query.AppendLine("           ,a.loc_code sblock							                                ");
            query.AppendLine("           ,'분기점' middle_name													    ");
            query.AppendLine("			 ,a.loc_name small_name					                                    ");
            query.AppendLine("           ,a.orderby																    ");
            query.AppendLine("		 from cm_location a																");
            query.AppendLine("		where 1 = 1																		");
            query.AppendLine("		  and (a.kt_gbn = '001' or a.kt_gbn is null)  						 		    ");
            query.AppendLine("		  and a.ftr_code = 'BZ002'														");

            query.AppendLine("        and (select tag_gbn from if_tag_gbn where tagname in ( select tagname from if_ihtags where loc_code = a.loc_code) and tag_gbn = 'FRQ')  = 'FRQ' ");
            query.AppendLine("		start with a.loc_code = :LOC_CODE												");
            query.AppendLine("		connect by prior a.loc_code = a.ploc_code										");
            query.AppendLine("        union all																		");
            query.AppendLine("	   select '3' mblock																");
            query.AppendLine("             ,a.loc_code sblock														");
            query.AppendLine("             ,'배수지 유출' middle_name												");
            query.AppendLine("			 ,(select loc_name from cm_location where loc_code = a.res_code) small_name	");
            query.AppendLine("             ,a.orderby																");
            query.AppendLine("		 from cm_location a																");
            query.AppendLine("		where 1 = 1																		");
            query.AppendLine("		  and a.ftr_code = 'BZ002'														");
            query.AppendLine("        and a.kt_gbn = '001'														");
            //태그가 없으면 목록에서 제외
            query.AppendLine("        and (select tag_gbn from if_tag_gbn where tagname in ( select tagname from if_ihtags where loc_code = a.loc_code) and tag_gbn = 'FRQ_O')  = 'FRQ_O' ");

            query.AppendLine("		start with a.loc_code = :LOC_CODE												");
            query.AppendLine("		connect by prior a.loc_code = a.ploc_code										");
            query.AppendLine("       ) a																			");
            query.AppendLine("  group by mblock, sblock, middle_name, small_name									");
            query.AppendLine("  order by mblock, max(orderby)														");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        //시단위공급량 값 검색(계통별)
        public void SelectSystemTimeSupply_SmallInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select a.mblock                                                                                                                   ");
            query.AppendLine("      ,a.sblock																													");
            query.AppendLine("      ,a.datee																													");
            query.AppendLine("      ,a.flow																														");
            query.AppendLine("  from (																															");
            query.AppendLine("       with loc as																												");
            query.AppendLine("       (																															");
            query.AppendLine("       select mblock																												");
            query.AppendLine("             ,sblock																												");
            query.AppendLine("             ,middle_name																											");
            query.AppendLine("             ,small_name																											");
            query.AppendLine("         from																														");
            query.AppendLine("	         (																														");
            query.AppendLine("		      select '1' mblock																										");
            query.AppendLine("			        ,a.loc_code sblock																								");
            query.AppendLine("			        ,'정수장 유출' middle_name																						");
            query.AppendLine("			        ,(select loc_name from cm_location where loc_code = a.res_code) small_name										");
            query.AppendLine("			        ,a.orderby																										");
            query.AppendLine("		       from cm_location a																									");
            query.AppendLine("              where 1 = 1																											");
            query.AppendLine("                and a.ftr_code = 'BZ001'																							");
            query.AppendLine("              start with a.loc_code = :LOC_CODE																					");
            query.AppendLine("            connect by prior a.loc_code = a.ploc_code																				");
            query.AppendLine("              union all																											");
            query.AppendLine("             select '2' mblock																									");
            query.AppendLine("                   ,decode(a.kt_gbn, '002', (select loc_code from cm_location where ploc_code = a.loc_code), a.loc_code) sblock	");
            query.AppendLine("                   ,'블록 유입' middle_name																						");
            query.AppendLine("                   ,decode(a.kt_gbn, '001', a.loc_name, decode(a.kt_gbn, null, a.loc_name, '면지역')) small_name					");
            query.AppendLine("                   ,a.orderby																										");
            query.AppendLine("               from cm_location a																									");
            query.AppendLine("              where 1 = 1																											");
            query.AppendLine("		          and (a.kt_gbn = '001' or a.kt_gbn is null)												 		                    ");
            query.AppendLine("                and a.ftr_code = 'BZ002'																							");
            query.AppendLine("              start with a.loc_code = :LOC_CODE																					");
            query.AppendLine("            connect by prior a.loc_code = a.ploc_code																				");
            query.AppendLine("              union all																											");
            query.AppendLine("             select '3' mblock																									");
            query.AppendLine("                   ,a.loc_code sblock																								");
            query.AppendLine("                   ,'배수지 유출' middle_name																						");
            query.AppendLine("                   ,(select loc_name from cm_location where loc_code = a.res_code) small_name										");
            query.AppendLine("                  ,a.orderby																										");
            query.AppendLine("              from cm_location a																									");
            query.AppendLine("             where 1 = 1																											");
            query.AppendLine("               and a.ftr_code = 'BZ002'																							");
            query.AppendLine("               and a.kt_gbn = '001'																								");
            query.AppendLine("             start with a.loc_code = :LOC_CODE																					");
            query.AppendLine("           connect by prior a.loc_code = a.ploc_code																				");
            query.AppendLine("            ) a																													");
            query.AppendLine("       order by 																													");
            query.AppendLine("              mblock																												");
            query.AppendLine("             ,orderby																												");
            query.AppendLine("       )																															");
            query.AppendLine("select mblock																														");
            query.AppendLine("	  ,decode(sblock,'_SUM',mblock||sblock,sblock) sblock																			");
            query.AppendLine("	  ,datee																														");
            query.AppendLine("	  ,flow																															");
            query.AppendLine("  from																															");
            query.AppendLine("	 (																																");
            query.AppendLine("       select mblock																												");
            query.AppendLine("             ,mblock||'_'||sblock sblock																							");
            query.AppendLine("             ,datee																												");
            query.AppendLine("             ,sum(flow) flow																										");
            query.AppendLine("         from (																													");
            query.AppendLine("             select loc.mblock																									");
            query.AppendLine("                   ,decode(loc.small_name, '면지역', 'X', loc.sblock) sblock														");
            query.AppendLine("                   ,to_date(to_char(iah.timestamp,'yyyymmddhh24')||'00','yyyymmddhh24mi') datee									");
            query.AppendLine("                   ,iah.value flow																								");
            query.AppendLine("               from loc loc																										");
            query.AppendLine("	               ,if_ihtags iih																									");
            query.AppendLine("                   ,if_tag_gbn itg																								");
            query.AppendLine("                   ,if_accumulation_hour iah																						");
            query.AppendLine("              where iih.loc_code = loc.sblock																						");
            query.AppendLine("                and iih.tagname = itg.tagname																						");
            query.AppendLine("                and itg.tag_gbn = decode(loc.mblock, '3', 'FRQ_O', 'FRQ')															");
            query.AppendLine("                and iah.tagname = iih.tagname																						");
            query.AppendLine("                and iah.timestamp 																								");
            query.AppendLine("                    between to_date(:STARTDATE||'0100','yyyymmddhh24mi') 															");
            //query.AppendLine("                        and to_date(:STARTDATE||'0000','yyyymmddhh24mi')+1														");
            query.AppendLine("                        and to_date(:ENDDATE||'0000','yyyymmddhh24mi')+1														");
            query.AppendLine("              )																													");
            query.AppendLine("         group by 																												");
            query.AppendLine("               mblock																												");
            query.AppendLine("              ,sblock																												");
            query.AppendLine("              ,datee																												");
            query.AppendLine("	  )																																");
            query.AppendLine(" model																															");
            query.AppendLine(" partition by (mblock, datee)																										");
            query.AppendLine(" dimension by (sblock)																											");
            query.AppendLine(" measures (flow)																													");
            query.AppendLine(" ignore nav																														");
            query.AppendLine(" rules ( flow['_SUM'] = sum(flow)[sblock is not null] )																			");
            query.AppendLine(") a																																");
            query.AppendLine("order by																															");
            query.AppendLine("      a.mblock																													");
            query.AppendLine("     ,nvl((select orderby from cm_location where loc_code = a.sblock), decode(a.sblock, 'X', 888, 999))							");
            query.AppendLine("     ,a.datee																														");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["LOC_CODE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }



        ////////// 시단위 공급량
        //시단위공급량 날짜 목록(배수지)
        public void SelectSystemDailyReport_TotalInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select datee                                                                                         ");
            query.AppendLine("	  ,datee v_time                                                             					   ");
            query.AppendLine("  from																							   ");
            query.AppendLine("	  (																								   ");
            //query.AppendLine("	   select to_date(:STARTDATE||'01','yyyymmddhh24') + (rownum-1)/24 datee from dual				   ");
            //query.AppendLine("	  connect by rownum < 25																		   ");
            query.AppendLine("          select (to_date(:STARTDATE || '01', 'yyyymmddhh24') + (LEVEL-1)/24)  datee from dual                    ");
            query.AppendLine("          connect by LEVEL <=  (TO_DATE(:ENDDATE || '2359', 'yyyymmddhh24mi') - TO_DATE(:STARTDATE, 'YYYYMMDD'))*(24) ");
            query.AppendLine("	  )																								   ");
            query.AppendLine(" group by datee order by datee																	   ");


            IDataParameter[] parameters =  {
                     new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //시단위공급량 계통구분_계통명 목록(배수지)
        public void SelectSystemDailyReport_MiddleInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                     ");
            //query.AppendLine("(																																  ");
            //query.AppendLine("select decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) loc_code		  ");
            //query.AppendLine("      ,decode(c1.res_code, null, (select loc_name from cm_location where ploc_code = c1.loc_code), 							  ");
            //query.AppendLine("              (select loc_name from cm_location where loc_code = c1.res_code)) loc_name										  ");
            //query.AppendLine("      ,c1.res_code																											  ");
            //query.AppendLine("      ,c1.orderby																												  ");
            //query.AppendLine("  from cm_location c1																											  ");
            //query.AppendLine(" where c1.ftr_code = 'BZ002' or (c1.ftr_code = 'BZ003' and c1.res_code is not null)											  ");
            //query.AppendLine(" start with c1.loc_code = :LOC_CODE																							  ");
            //query.AppendLine("connect by prior c1.loc_code = c1.ploc_code																					  ");
            //query.AppendLine(")																																  ");
            //query.AppendLine("select loc.loc_code mblock																									  ");
            //query.AppendLine("      ,loc.loc_code||'_'||itg.tag_gbn sblock																					  ");
            //query.AppendLine("      ,loc.loc_name middle_name																								  ");
            //query.AppendLine("      ,decode(itg.tag_gbn, 'SPL_D', '분기점', decode(itg.tag_gbn, 'SPL_I', '유입', '유출')) small_name						  ");
            //query.AppendLine("  from loc																													  ");
            //query.AppendLine("      ,if_ihtags iih																											  ");
            //query.AppendLine("      ,if_tag_gbn itg																											  ");
            //query.AppendLine(" where iih.loc_code = loc.loc_code																							  ");
            //query.AppendLine("   and itg.tagname = iih.tagname																								  ");
            //query.AppendLine("   and itg.tag_gbn in ('SPL_D', 'SPL_I', 'SPL_O')																				  ");
            //query.AppendLine(" order by loc.orderby, itg.tag_gbn																							  ");

            //query.AppendLine("with loc as                                                                                                                     ");
            //query.AppendLine("(																																  ");
            //query.AppendLine("select decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) loc_code		  ");
            //query.AppendLine("      ,decode(c1.res_code, null, (select loc_name from cm_location where ploc_code = c1.loc_code), 							  ");
            //query.AppendLine("              (select loc_name from cm_location where loc_code = c1.res_code)) loc_name										  ");
            //query.AppendLine("      ,c1.res_code																											  ");
            //query.AppendLine("      ,c1.orderby																												  ");
            //query.AppendLine("  from cm_location2 c1																											  ");
            //query.AppendLine(" where c1.ftr_code = 'BZ002' or (c1.ftr_code = 'BZ003' and c1.res_code is not null)											  ");
            //query.AppendLine(" start with c1.loc_code = :LOC_CODE																							  ");
            //query.AppendLine("connect by prior c1.loc_code = c1.ploc_code																					  ");
            //query.AppendLine(")																																  ");
            //query.AppendLine("select loc.loc_code mblock																									  ");
            //query.AppendLine("      ,loc.loc_code||'_'||itg.tag_gbn sblock																					  ");
            //query.AppendLine("      ,loc.loc_name middle_name																								  ");
            //query.AppendLine("      ,decode(itg.tag_gbn, 'SPL_D', '분기점', decode(itg.tag_gbn, 'SPL_I', '유입', '유출')) small_name						  ");
            //query.AppendLine("  from loc																													  ");
            //query.AppendLine("      ,if_ihtags iih																											  ");
            //query.AppendLine("      ,if_tag_gbn itg																											  ");
            //query.AppendLine(" where iih.loc_code2 = loc.loc_code																							  ");
            //query.AppendLine("   and itg.tagname = iih.tagname																								  ");
            //query.AppendLine("   and itg.tag_gbn in ('SPL_D', 'SPL_I', 'SPL_O')																				  ");
            //query.AppendLine(" order by loc.orderby, itg.tag_gbn																							  ");


            query.AppendLine("with loc as                                                                                                                   ");
            query.AppendLine("(																																");
            query.AppendLine("select decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) loc_code		");
            query.AppendLine("      ,decode(c1.res_code, null, c1.loc_name,																					");
            query.AppendLine("             (select loc_name from cm_location where loc_code = c1.res_code)) loc_name										");
            query.AppendLine("      ,c1.res_code																											");
            query.AppendLine("      ,c1.orderby																												");
            query.AppendLine("  from																														");
            query.AppendLine("      (																														");
            query.AppendLine("      select c2.loc_code																										");
            query.AppendLine("            ,c2.loc_name																										");
            query.AppendLine("            ,c2.kt_gbn																										");
            query.AppendLine("            ,decode(c2.ftr_code, 'BZ003', 																					");
            query.AppendLine("                   nvl((select res_code from cm_location where loc_code = c2.ploc_code and kt_gbn != '001'), c2.res_code), 	");
            query.AppendLine("                   c2.res_code) res_code																						");
            query.AppendLine("            ,c2.orderby																										");
            query.AppendLine("        from cm_location c1																									");
            query.AppendLine("            ,cm_location2 c2																									");
            query.AppendLine("       where c1.loc_code(+) = c2.loc_code																						");
            query.AppendLine("         and c2.ftr_code != 'BZ001' ");
            query.AppendLine("         and c2.res_code is not null ");
            //query.AppendLine("         and (c2.ftr_code='BZ003' or (c2.ftr_code = 'BZ002' and c2.kt_gbn = '001'))											");
            //query.AppendLine("         and ((select kt_gbn from cm_location where loc_code = c2.ploc_code) != '001' or c2.res_code is not null)				");
            query.AppendLine("       start with c2.loc_code = :LOC_CODE																						");
            query.AppendLine("      connect by prior c2.loc_code = c2.ploc_code																				");
            query.AppendLine("      ) c1																													");
            query.AppendLine(")																																");
            query.AppendLine("select loc.loc_code mblock																									");
            query.AppendLine("	  ,loc.loc_code||'_'||itg.tag_gbn sblock																					");
            query.AppendLine("	  ,loc.loc_name middle_name																									");
            query.AppendLine("	  ,decode(itg.tag_gbn, 'SPL_D', '분기점', decode(itg.tag_gbn, 'SPL_I', '유입', '유출')) small_name							");
            query.AppendLine("  from loc																													");
            query.AppendLine("	  ,if_ihtags iih																											");
            query.AppendLine("	  ,if_tag_gbn itg																											");
            query.AppendLine(" where iih.loc_code2 = loc.loc_code																							");
            query.AppendLine("   and itg.tagname = iih.tagname																								");
            query.AppendLine("   and itg.tag_gbn in ('SPL_D', 'SPL_I', 'SPL_O')																				");
            query.AppendLine(" order by loc.orderby, itg.tag_gbn																							");


            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        //시단위공급량 값 검색(배수지)
        public void SelectSystemDailyReport_SmallInfo(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("with loc as                                                                                                                    ");
            //query.AppendLine("(																																 ");
            //query.AppendLine("select decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) loc_code		 ");
            //query.AppendLine("      ,decode(c1.res_code, null, (select loc_name from cm_location where ploc_code = c1.loc_code), 							 ");
            //query.AppendLine("	          (select loc_name from cm_location where loc_code = c1.res_code)) loc_name											 ");
            //query.AppendLine("      ,c1.res_code																											 ");
            //query.AppendLine("      ,c1.orderby																												 ");
            //query.AppendLine("  from cm_location c1																											 ");
            //query.AppendLine(" where c1.ftr_code = 'BZ002' or (c1.ftr_code = 'BZ003' and c1.res_code is not null)											 ");
            //query.AppendLine(" start with c1.loc_code = :LOC_CODE																							 ");
            //query.AppendLine("connect by prior c1.loc_code = c1.ploc_code																					 ");
            //query.AppendLine(")																																 ");
            //query.AppendLine("select loc.loc_code mblock																									 ");
            //query.AppendLine("      ,loc.loc_code||'_'||itg.tag_gbn sblock																					 ");
            //query.AppendLine("      ,iah.timestamp	datee																									 ");
            //query.AppendLine("      ,sum(iah.value) flow																									 ");
            //query.AppendLine("  from loc																													 ");
            //query.AppendLine("      ,if_ihtags iih																											 ");
            //query.AppendLine("      ,if_tag_gbn itg																											 ");
            //query.AppendLine("      ,if_accumulation_hour iah																								 ");
            //query.AppendLine(" where iih.loc_code = loc.loc_code																							 ");
            //query.AppendLine("   and itg.tagname = iih.tagname																								 ");
            //query.AppendLine("   and itg.tag_gbn in ('SPL_D', 'SPL_I', 'SPL_O')																				 ");
            //query.AppendLine("   and iah.tagname = itg.tagname																								 ");
            //query.AppendLine("   and iah.timestamp 																											 ");
            //query.AppendLine("       between to_date(:STARTDATE||'0100','yyyymmddhh24mi') 																	 ");
            //query.AppendLine("           and to_date(:STARTDATE||'0000','yyyymmddhh24mi')+1																	 ");
            //query.AppendLine(" group by loc.loc_code, itg.tag_gbn, iah.timestamp, loc.orderby																 ");
            //query.AppendLine(" order by loc.orderby, itg.tag_gbn, timestamp																					 ");

            //query.AppendLine("with loc as                                                                                                                    ");
            //query.AppendLine("(																																 ");
            //query.AppendLine("select decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) loc_code		 ");
            //query.AppendLine("      ,decode(c1.res_code, null, (select loc_name from cm_location where ploc_code = c1.loc_code), 							 ");
            //query.AppendLine("	          (select loc_name from cm_location where loc_code = c1.res_code)) loc_name											 ");
            //query.AppendLine("      ,c1.res_code																											 ");
            //query.AppendLine("      ,c1.orderby																												 ");
            //query.AppendLine("  from cm_location2 c1																											 ");
            //query.AppendLine(" where c1.ftr_code = 'BZ002' or (c1.ftr_code = 'BZ003' and c1.res_code is not null)											 ");
            //query.AppendLine(" start with c1.loc_code = :LOC_CODE																							 ");
            //query.AppendLine("connect by prior c1.loc_code = c1.ploc_code																					 ");
            //query.AppendLine(")																																 ");
            //query.AppendLine("select loc.loc_code mblock																									 ");
            //query.AppendLine("      ,loc.loc_code||'_'||itg.tag_gbn sblock																					 ");
            //query.AppendLine("      ,iah.timestamp	datee																									 ");
            //query.AppendLine("      ,sum(iah.value) flow																									 ");
            //query.AppendLine("  from loc																													 ");
            //query.AppendLine("      ,if_ihtags iih																											 ");
            //query.AppendLine("      ,if_tag_gbn itg																											 ");
            //query.AppendLine("      ,if_accumulation_hour iah																								 ");
            //query.AppendLine(" where iih.loc_code2 = loc.loc_code																							 ");
            //query.AppendLine("   and itg.tagname = iih.tagname																								 ");
            //query.AppendLine("   and itg.tag_gbn in ('SPL_D', 'SPL_I', 'SPL_O')																				 ");
            //query.AppendLine("   and iah.tagname = itg.tagname																								 ");
            //query.AppendLine("   and iah.timestamp 																											 ");
            //query.AppendLine("       between to_date(:STARTDATE||'0100','yyyymmddhh24mi') 																	 ");
            //query.AppendLine("           and to_date(:STARTDATE||'0000','yyyymmddhh24mi')+1																	 ");
            //query.AppendLine(" group by loc.loc_code, itg.tag_gbn, iah.timestamp, loc.orderby																 ");
            //query.AppendLine(" order by loc.orderby, itg.tag_gbn, timestamp																					 ");


            query.AppendLine("with loc as                                                                                                                    ");
            query.AppendLine("(																																");
            query.AppendLine("select decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) loc_code		");
            query.AppendLine("      ,decode(c1.res_code, null, c1.loc_name,																					");
            query.AppendLine("             (select loc_name from cm_location where loc_code = c1.res_code)) loc_name										");
            query.AppendLine("      ,c1.res_code																											");
            query.AppendLine("      ,c1.orderby																												");
            query.AppendLine("  from																														");
            query.AppendLine("      (																														");
            query.AppendLine("      select c2.loc_code																										");
            query.AppendLine("            ,c2.loc_name																										");
            query.AppendLine("            ,c2.kt_gbn																										");
            query.AppendLine("            ,decode(c2.ftr_code, 'BZ003', 																					");
            query.AppendLine("                   nvl((select res_code from cm_location where loc_code = c2.ploc_code and kt_gbn != '001'), c2.res_code), 	");
            query.AppendLine("                   c2.res_code) res_code																						");
            query.AppendLine("            ,c2.orderby																										");
            query.AppendLine("        from cm_location c1																									");
            query.AppendLine("            ,cm_location2 c2																									");
            query.AppendLine("       where c1.loc_code(+) = c2.loc_code																						");
            query.AppendLine("         and c2.ftr_code != 'BZ001' ");
            query.AppendLine("         and c2.res_code is not null ");
            //query.AppendLine("         and (c2.ftr_code='BZ003' or (c2.ftr_code = 'BZ002' and c2.kt_gbn = '001'))											");
            //query.AppendLine("         and ((select kt_gbn from cm_location where loc_code = c2.ploc_code) != '001' or c2.res_code is not null)				");
            query.AppendLine("       start with c2.loc_code = :LOC_CODE																						");
            query.AppendLine("      connect by prior c2.loc_code = c2.ploc_code																				");
            query.AppendLine("      ) c1																													");
            query.AppendLine(")																																");
            query.AppendLine("select loc.loc_code mblock																									 ");
            query.AppendLine("      ,loc.loc_code||'_'||itg.tag_gbn sblock																					 ");
            query.AppendLine("      ,iah.timestamp	datee																									 ");
            query.AppendLine("      ,sum(iah.value) flow																									 ");
            query.AppendLine("  from loc																													 ");
            query.AppendLine("      ,if_ihtags iih																											 ");
            query.AppendLine("      ,if_tag_gbn itg																											 ");
            query.AppendLine("      ,if_accumulation_hour iah																								 ");
            query.AppendLine(" where iih.loc_code2 = loc.loc_code																							 ");
            query.AppendLine("   and itg.tagname = iih.tagname																								 ");
            query.AppendLine("   and itg.tag_gbn in ('SPL_D', 'SPL_I', 'SPL_O')																				 ");
            query.AppendLine("   and iah.tagname = itg.tagname																								 ");
            query.AppendLine("   and iah.timestamp 																											 ");
            query.AppendLine("       between to_date(:STARTDATE||'0100','yyyymmddhh24mi') 																	 ");
            query.AppendLine("           and to_date(:ENDDATE||'0000','yyyymmddhh24mi')+1																	 ");
            query.AppendLine(" group by loc.loc_code, itg.tag_gbn, iah.timestamp, loc.orderby																 ");
            query.AppendLine(" order by loc.orderby, itg.tag_gbn, timestamp																					 ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //배수지목록검색
        public DataTable getReservoirs(OracleDBManager manager, string flowMeasurePoint)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("SELECT B.LOC_CODE CODE ");
            query.AppendLine("      ,A.LOC_NAME NAME ");
            query.AppendLine("  FROM CM_LOCATION A ");
            query.AppendLine("      ,CM_LOCATION B ");
            query.AppendLine("      ,IF_IHTAGS C ");
            query.AppendLine("      ,IF_TAG_GBN D ");
            query.AppendLine(" WHERE A.LOC_GBN = '배수지' ");
            query.AppendLine("   AND B.RES_CODE = A.LOC_CODE ");
            query.AppendLine("   AND C.LOC_CODE = B.LOC_CODE ");
            query.AppendLine("   AND D.TAGNAME = C.TAGNAME ");
            query.AppendLine("   AND D.TAG_GBN = :TAG_GBN ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("TAG_GBN", OracleDbType.Varchar2)
            };

            parameters[0].Value = flowMeasurePoint;

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        //적산차조회
        public DataTable getAccumulationHourFlows(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("WITH LOC AS ");
            query.AppendLine("( ");
            query.AppendLine("SELECT C1.LOC_CODE ");
            query.AppendLine("      ,C1.SGCCD ");
            query.AppendLine("      ,DECODE(C1.PLOC_CODE, NULL, DECODE(C2.LOC_CODE, NULL, DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, C1.LOC_CODE))) ");
            query.AppendLine("      ,DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, C2.LOC_CODE), C3.LOC_CODE)) LBLOCK ");
            query.AppendLine("      ,DECODE(C1.PLOC_CODE, NULL, DECODE(C2.LOC_CODE, NULL, DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, NULL))) ");
            query.AppendLine("      ,DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, C1.LOC_CODE), C2.LOC_CODE)) MBLOCK ");
            query.AppendLine("      ,DECODE(C1.PLOC_CODE, NULL, DECODE(C2.LOC_CODE, NULL, DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, NULL))) ");
            query.AppendLine("      ,DECODE(C2.PLOC_CODE, NULL, DECODE(C3.LOC_CODE, NULL, NULL), C1.LOC_CODE)) SBLOCK ");
            query.AppendLine("      ,C1.FTR_CODE ");
            query.AppendLine("      ,DECODE(C1.KT_GBN, '002', (SELECT LOC_CODE FROM CM_LOCATION WHERE PLOC_CODE = C1.LOC_CODE), C1.LOC_CODE) TAG_LOC_CODE ");
            query.AppendLine("      ,C1.REL_LOC_NAME ");
            query.AppendLine("      ,C1.ORD ");
            query.AppendLine("      ,C.TAG_GBN ");
            query.AppendLine("      ,C.TAGNAME ");
            query.AppendLine("      ,B.DESCRIPTION TAG_DESCRIPTION ");
            query.AppendLine("      ,TMP.TIMESTAMP ");
            query.AppendLine("  FROM ( ");
            query.AppendLine("       SELECT SGCCD ");
            query.AppendLine("             ,LOC_CODE ");
            query.AppendLine("             ,PLOC_CODE ");
            query.AppendLine("             ,LOC_NAME ");
            query.AppendLine("             ,FTR_IDN ");
            query.AppendLine("             ,FTR_CODE ");
            query.AppendLine("             ,RES_CODE ");
            query.AppendLine("             ,KT_GBN ");
            query.AppendLine("             ,REL_LOC_NAME ");
            query.AppendLine("             ,ROWNUM ORD ");
            query.AppendLine("         FROM CM_LOCATION ");
            query.AppendLine("        WHERE 1 = 1 ");
            query.AppendLine("          AND LOC_CODE = :LOC_CODE ");
            query.AppendLine("        START WITH FTR_CODE = 'BZ001' ");
            query.AppendLine("      CONNECT BY PRIOR LOC_CODE = PLOC_CODE ");
            query.AppendLine("        ORDER SIBLINGS BY FTR_IDN ");
            query.AppendLine("      ) C1 ");
            query.AppendLine("       ,CM_LOCATION C2 ");
            query.AppendLine("       ,CM_LOCATION C3 ");
            query.AppendLine("       ,( ");
            query.AppendLine("         SELECT TO_DATE(:STARTDATE,'YYYYMMDDHH24') + ((1/24)*(ROWNUM-1)) TIMESTAMP ");
            query.AppendLine("           FROM DUAL ");
            query.AppendLine("         CONNECT BY ROWNUM < ((TO_DATE(:ENDDATE,'YYYYMMDDHH24') ");
            query.AppendLine("                            -TO_DATE(:STARTDATE,'YYYYMMDDHH24')) * 24) + 2 ");
            query.AppendLine("      ) TMP ");
            query.AppendLine("      ,IF_IHTAGS B ");
            query.AppendLine("      ,IF_TAG_GBN C ");
            query.AppendLine(" WHERE 1 = 1 ");
            query.AppendLine("   AND C1.PLOC_CODE = C2.LOC_CODE(+) ");
            query.AppendLine("   AND C2.PLOC_CODE = C3.LOC_CODE(+) ");
            query.AppendLine("   AND B.LOC_CODE = C1.LOC_CODE ");
            query.AppendLine("   AND C.TAGNAME = B.TAGNAME ");
            query.AppendLine("   AND C.TAG_GBN = :TAG_GBN ");
            query.AppendLine(" ORDER BY C1.ORD ");
            query.AppendLine(") ");
            query.AppendLine("SELECT A.LOC_CODE ");
            query.AppendLine("      ,A.SGCCD ");
            query.AppendLine("      ,A.LBLOCK ");
            query.AppendLine("      ,A.MBLOCK ");
            query.AppendLine("      ,A.SBLOCK ");
            query.AppendLine("      ,A.REL_LOC_NAME ");
            query.AppendLine("      ,A.TAG_GBN ");
            query.AppendLine("      ,A.TAGNAME ");
            query.AppendLine("      ,A.TAG_DESCRIPTION ");
            query.AppendLine("      ,A.TIMESTAMP ");
            query.AppendLine("      ,B.VALUE ");
            query.AppendLine("  FROM LOC A ");
            query.AppendLine("      ,IF_ACCUMULATION_HOUR B ");
            query.AppendLine(" WHERE B.TAGNAME(+) = A.TAGNAME ");
            query.AppendLine("   AND B.TIMESTAMP(+) = A.TIMESTAMP ");
            query.AppendLine(" ORDER BY A.TIMESTAMP ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
                     ,new OracleParameter("TAG_GBN", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];
            parameters[3].Value = parameter["STARTDATE"];
            parameters[4].Value = parameter["TAG_GBN"];

            return manager.ExecuteScriptDataTable(query.ToString(), parameters);
        }

        //적산차저장
        public void updateAccumulationHourFlows(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO IF_ACCUMULATION_HOUR A ");
            query.AppendLine("USING ( ");
            query.AppendLine("       SELECT :TAGNAME TAGNAME ");
            query.AppendLine("             ,TO_DATE(:TIMESTAMP, 'YYYYMMDDHH24') TIMESTAMP ");
            query.AppendLine("             ,:VALUE VAL ");
            query.AppendLine("         FROM DUAL ");
            query.AppendLine("      ) B ");
            query.AppendLine("  ON (A.TAGNAME = B.TAGNAME AND A.TIMESTAMP = B.TIMESTAMP) ");
            query.AppendLine("WHEN MATCHED THEN ");
            query.AppendLine("     UPDATE SET A.VALUE = B.VAL ");
            query.AppendLine("WHEN NOT MATCHED THEN ");
            query.AppendLine("     INSERT (A.TAGNAME, A.TIMESTAMP, A.VALUE) ");
            query.AppendLine("     VALUES (B.TAGNAME, B.TIMESTAMP, B.VAL) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                     ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                     ,new OracleParameter("VALUE", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["TAGNAME"];
            parameters[1].Value = parameter["TIMESTAMP"];
            parameters[2].Value = parameter["VALUE"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
