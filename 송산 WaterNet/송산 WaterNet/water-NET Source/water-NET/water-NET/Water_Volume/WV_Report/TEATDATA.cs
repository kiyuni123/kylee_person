﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace WaterNet.WV_Report
{
    public class TEATDATA
    {

        private DataSet MNF()
        {
            DataSet dataSet = new DataSet();

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("DATEE", typeof(DateTime));
            dataTable.Columns.Add("TOTAL_SUM", typeof(double));
            DataRow dataRow = null;

            dataRow = dataTable.NewRow();
            dataRow["DATEE"] = new DateTime(2010, 11, 16);
            dataRow["TOTAL_SUM"] = 39.65;
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["DATEE"] = new DateTime(2010, 11, 17);
            dataRow["TOTAL_SUM"] = 91.16;
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["DATEE"] = new DateTime(2010, 11, 18);
            dataRow["TOTAL_SUM"] = 40.08;
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["DATEE"] = new DateTime(2010, 11, 19);
            dataRow["TOTAL_SUM"] = 72.69;
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["DATEE"] = new DateTime(2010, 11, 20);
            dataRow["TOTAL_SUM"] = 35.06;
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["DATEE"] = new DateTime(2010, 11, 21);
            dataRow["TOTAL_SUM"] = 88.02;
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["DATEE"] = new DateTime(2010, 11, 22);
            dataRow["TOTAL_SUM"] = 27.61;
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["DATEE"] = new DateTime(2010, 11, 23);
            dataRow["TOTAL_SUM"] = 38.57;
            dataTable.Rows.Add(dataRow);

            dataTable.TableName = "TOTAL_INFO";
            dataSet.Tables.Add(dataTable);

            DataTable dataTable2 = new DataTable();
            dataTable2.Columns.Add("LBLOCK", typeof(string));
            dataTable2.Columns.Add("MBLOCK", typeof(string));
            dataTable2.Columns.Add("SBLOCK", typeof(string));
            dataTable2.Columns.Add("DATEE", typeof(DateTime));
            dataTable2.Columns.Add("MNF", typeof(double));
            dataTable2.Columns.Add("FILTERING", typeof(double));

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000035";
            dataRow["DATEE"] = new DateTime(2010, 11, 16);
            dataRow["MNF"] = 27;
            dataRow["FILTERING"] = 28;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000035";
            dataRow["DATEE"] = new DateTime(2010, 11, 17);
            dataRow["MNF"] = DBNull.Value;
            dataRow["FILTERING"] = DBNull.Value;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000035";
            dataRow["DATEE"] = new DateTime(2010, 11, 18);
            dataRow["MNF"] = 28.60;
            dataRow["FILTERING"] = 28.60;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000035";
            dataRow["DATEE"] = new DateTime(2010, 11, 19);
            dataRow["MNF"] = 28;
            dataRow["FILTERING"] = 28;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000035";
            dataRow["DATEE"] = new DateTime(2010, 11, 20);
            dataRow["MNF"] = 23.70;
            dataRow["FILTERING"] = 23.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000035";
            dataRow["DATEE"] = new DateTime(2010, 11, 21);
            dataRow["MNF"] = 24.90;
            dataRow["FILTERING"] = 24.90;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000035";
            dataRow["DATEE"] = new DateTime(2010, 11, 22);
            dataRow["MNF"] = 15.70;
            dataRow["FILTERING"] = 15.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000035";
            dataRow["DATEE"] = new DateTime(2010, 11, 23);
            dataRow["MNF"] = 27.30;
            dataRow["FILTERING"] = 27.30;
            dataTable2.Rows.Add(dataRow);

            ///
            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000036";
            dataRow["DATEE"] = new DateTime(2010, 11, 16);
            dataRow["MNF"] = 1.65;
            dataRow["FILTERING"] = 1.65;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000036";
            dataRow["DATEE"] = new DateTime(2010, 11, 17);
            dataRow["MNF"] = 1.86;
            dataRow["FILTERING"] = 1.86;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000036";
            dataRow["DATEE"] = new DateTime(2010, 11, 18);
            dataRow["MNF"] = 1.98;
            dataRow["FILTERING"] = 1.98;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000036";
            dataRow["DATEE"] = new DateTime(2010, 11, 19);
            dataRow["MNF"] = 1.69;
            dataRow["FILTERING"] = 1.69;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000036";
            dataRow["DATEE"] = new DateTime(2010, 11, 20);
            dataRow["MNF"] = 1.96;
            dataRow["FILTERING"] = 1.96;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000036";
            dataRow["DATEE"] = new DateTime(2010, 11, 21);
            dataRow["MNF"] = 1.62;
            dataRow["FILTERING"] = 1.62;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000036";
            dataRow["DATEE"] = new DateTime(2010, 11, 22);
            dataRow["MNF"] = 1.61;
            dataRow["FILTERING"] = 1.61;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000036";
            dataRow["DATEE"] = new DateTime(2010, 11, 23);
            dataRow["MNF"] = 1.67;
            dataRow["FILTERING"] = 1.67;
            dataTable2.Rows.Add(dataRow);

            ///
            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000013_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 16);
            dataRow["MNF"] = 1.65;
            dataRow["FILTERING"] = 1.65;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000013_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 17);
            dataRow["MNF"] = 1.86;
            dataRow["FILTERING"] = 1.86;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000013_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 18);
            dataRow["MNF"] = 1.98;
            dataRow["FILTERING"] = 1.98;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000013_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 19);
            dataRow["MNF"] = 1.69;
            dataRow["FILTERING"] = 1.69;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000013_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 20);
            dataRow["MNF"] = 1.96;
            dataRow["FILTERING"] = 1.96;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000013_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 21);
            dataRow["MNF"] = 1.62;
            dataRow["FILTERING"] = 1.62;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000013_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 22);
            dataRow["MNF"] = 1.61;
            dataRow["FILTERING"] = 1.61;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["SBLOCK"] = "000013_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 23);
            dataRow["MNF"] = 1.67;
            dataRow["FILTERING"] = 1.67;
            dataTable2.Rows.Add(dataRow);
            ///

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 16);
            dataRow["MNF"] = 3.50;
            dataRow["FILTERING"] = 3.50;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 17);
            dataRow["MNF"] = 54.70;
            dataRow["FILTERING"] = 54.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 18);
            dataRow["MNF"] = 3.40;
            dataRow["FILTERING"] = 3.40;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 19);
            dataRow["MNF"] = 37.30;
            dataRow["FILTERING"] = 37.30;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 20);
            dataRow["MNF"] = 3.30;
            dataRow["FILTERING"] = 3.30;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 21);
            dataRow["MNF"] = 55.80;
            dataRow["FILTERING"] = 55.80;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 22);
            dataRow["MNF"] = 4.70;
            dataRow["FILTERING"] = 4.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 23);
            dataRow["MNF"] = 3.80;
            dataRow["FILTERING"] = 3.80;
            dataTable2.Rows.Add(dataRow);
            ///
            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 16);
            dataRow["MNF"] = 3.50;
            dataRow["FILTERING"] = 3.50;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 17);
            dataRow["MNF"] = 54.70;
            dataRow["FILTERING"] = 54.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 18);
            dataRow["MNF"] = 3.40;
            dataRow["FILTERING"] = 3.40;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 19);
            dataRow["MNF"] = 37.30;
            dataRow["FILTERING"] = 37.30;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 20);
            dataRow["MNF"] = 3.30;
            dataRow["FILTERING"] = 3.30;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 21);
            dataRow["MNF"] = 55.80;
            dataRow["FILTERING"] = 55.80;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 22);
            dataRow["MNF"] = 4.70;
            dataRow["FILTERING"] = 4.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000032";
            dataRow["DATEE"] = new DateTime(2010, 11, 23);
            dataRow["MNF"] = 3.80;
            dataRow["FILTERING"] = 3.80;
            dataTable2.Rows.Add(dataRow);
            ///


            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000037";
            dataRow["DATEE"] = new DateTime(2010, 11, 16);
            dataRow["MNF"] = 6.50;
            dataRow["FILTERING"] = 6.50;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000037";
            dataRow["DATEE"] = new DateTime(2010, 11, 17);
            dataRow["MNF"] = 5.80;
            dataRow["FILTERING"] = 5.80;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000037";
            dataRow["DATEE"] = new DateTime(2010, 11, 18);
            dataRow["MNF"] = 6.10;
            dataRow["FILTERING"] = 6.10;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000037";
            dataRow["DATEE"] = new DateTime(2010, 11, 19);
            dataRow["MNF"] = 5.70;
            dataRow["FILTERING"] = 5.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000037";
            dataRow["DATEE"] = new DateTime(2010, 11, 20);
            dataRow["MNF"] = 6.10;
            dataRow["FILTERING"] = 6.10;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000037";
            dataRow["DATEE"] = new DateTime(2010, 11, 21);
            dataRow["MNF"] = 5.70;
            dataRow["FILTERING"] = 5.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000037";
            dataRow["DATEE"] = new DateTime(2010, 11, 22);
            dataRow["MNF"] = 5.60;
            dataRow["FILTERING"] = 5.60;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000037";
            dataRow["DATEE"] = new DateTime(2010, 11, 23);
            dataRow["MNF"] = 5.80;
            dataRow["FILTERING"] = 5.80;
            dataTable2.Rows.Add(dataRow);
            ///
            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000022_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 16);
            dataRow["MNF"] = 6.50;
            dataRow["FILTERING"] = 6.50;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000022_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 17);
            dataRow["MNF"] = 5.80;
            dataRow["FILTERING"] = 5.80;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000022_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 18);
            dataRow["MNF"] = 6.10;
            dataRow["FILTERING"] = 6.10;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000022_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 19);
            dataRow["MNF"] = 5.70;
            dataRow["FILTERING"] = 5.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000022_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 20);
            dataRow["MNF"] = 6.10;
            dataRow["FILTERING"] = 6.10;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000022_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 21);
            dataRow["MNF"] = 5.70;
            dataRow["FILTERING"] = 5.70;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000022_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 22);
            dataRow["MNF"] = 5.60;
            dataRow["FILTERING"] = 5.60;
            dataTable2.Rows.Add(dataRow);

            dataRow = dataTable2.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["SBLOCK"] = "000022_SUM";
            dataRow["DATEE"] = new DateTime(2010, 11, 23);
            dataRow["MNF"] = 5.80;
            dataRow["FILTERING"] = 5.80;
            dataTable2.Rows.Add(dataRow);


            dataTable2.TableName = "SMALL_INFO";
            dataSet.Tables.Add(dataTable2);

            DataTable dataTable3 = new DataTable();
            dataTable3.Columns.Add("LBLOCK", typeof(string));
            dataTable3.Columns.Add("MBLOCK", typeof(string));
            dataTable3.Columns.Add("MIDDLE_NAME", typeof(string));
            dataTable3.Columns.Add("SBLOCK", typeof(string));
            dataTable3.Columns.Add("SMALL_NAME", typeof(string));

            dataRow = dataTable3.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["MIDDLE_NAME"] = "마곡계통";
            dataRow["SBLOCK"] = "000035";
            dataRow["SMALL_NAME"] = "마곡01";
            dataTable3.Rows.Add(dataRow);

            dataRow = dataTable3.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000013";
            dataRow["MIDDLE_NAME"] = "마곡계통";
            dataRow["SBLOCK"] = "000036";
            dataRow["SMALL_NAME"] = "마곡02";
            dataTable3.Rows.Add(dataRow);

            dataRow = dataTable3.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["MIDDLE_NAME"] = "장명계통";
            dataRow["SBLOCK"] = "000032";
            dataRow["SMALL_NAME"] = "장명01";
            dataTable3.Rows.Add(dataRow);

            dataRow = dataTable3.NewRow();
            dataRow["LBLOCK"] = "000012";
            dataRow["MBLOCK"] = "000022";
            dataRow["MIDDLE_NAME"] = "장명계통";
            dataRow["SBLOCK"] = "000037";
            dataRow["SMALL_NAME"] = "장명02";
            dataTable3.Rows.Add(dataRow);

            dataTable3.TableName = "MIDDLE_INFO";
            dataSet.Tables.Add(dataTable3);

            return dataSet;
        }


    }
}
