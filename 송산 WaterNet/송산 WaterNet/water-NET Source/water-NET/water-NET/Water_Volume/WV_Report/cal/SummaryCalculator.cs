﻿using System;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_Report.cal
{
    public class SummaryCalculator
    {
    }


    public class MonthAverage : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = 0; i < rows.Count; i++)
            {
                double value = Utils.ToDouble(rows[i].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }
        #endregion
    }

    public class Week4Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 28; i--)
            {
                double value = Utils.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

    public class Week3Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 21; i--)
            {
                double value = Utils.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

    public class Week2Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 14; i--)
            {
                double value = Utils.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

    public class Week1Average : ICustomSummaryCalculator
    {
        #region ICustomSummaryCalculator 멤버

        private double result = 0;

        public void AggregateCustomSummary(SummarySettings summarySettings, UltraGridRow row)
        {

        }

        public void BeginCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            this.result = 0;
        }

        public object EndCustomSummary(SummarySettings summarySettings, RowsCollection rows)
        {
            if (summarySettings.SourceColumn.DataType == typeof(DateTime) || summarySettings.SourceColumn.DataType == typeof(string))
            {
                return this.result;
            }

            int count = 0;

            for (int i = rows.Count; i > rows.Count - 7; i--)
            {
                double value = Utils.ToDouble(rows[i - 1].Cells[summarySettings.SourceColumn].Value);
                this.result += value;
                if (value > 0)
                {
                    count++;
                }
            }

            this.result = this.result / count;

            if (Double.IsNaN(this.result))
            {
                this.result = 0;
            }
            return this.result;
        }

        #endregion
    }

}
