﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.control;
using WaterNet.WV_Report.work;
using System.Collections;
using ChartFX.WinForms;
using Infragistics.Win.UltraWinGrid;
using EMFrame.log;

namespace WaterNet.WV_Report.form
{
    public partial class frmAccumulationHour : Form, WaterNet.WaterNetCore.IForminterface
    {
        private TableLayout tableLayout = null;
        private ChartManager chartManager = null;
        private UltraGridManager gridManager = null;
        private BlockComboboxControl blockControl = null;
        private ExcelManager excelManager = new ExcelManager();

        public frmAccumulationHour()
        {
            InitializeComponent();
            Load += new EventHandler(frmAccumulationHour_Load);
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        public void Open()
        {
        }

        private void frmAccumulationHour_Load(object sender, EventArgs e)
        {
            this.InitializeEvent();
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
        }

        private void InitializeForm()
        {
            this.tableLayout = new TableLayout(tableLayoutPanel1, chartPanel1, tablePanel1, chartVisibleBtn, tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 50;
            this.tableLayout.DefaultTableHeight = 50;

            this.flowKind.ValueMember = "CODE";
            this.flowKind.DisplayMember = "NAME";
            this.flowKind.DataSource = this.getFlowKinds();
            this.flowKind.SelectedIndex = 0;

            this.reservoirPanel.Location = new Point(740, 22);

            this.blockControl = new BlockComboboxControl(largeBlock, middleBlock, smallBlock);
            this.blockControl.AddDefaultOption(LOCATION_TYPE.MIDDLE_BLOCK, false);
            this.blockControl.AddDefaultOption(LOCATION_TYPE.SMALL_BLOCK, false);

            this.startDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 01:00");
            this.endDate.Value = DateTime.Now.ToString("yyyy-MM-dd 00:00");

            //메뉴권한
            object o = EMFrame.statics.AppStatic.USER_MENU["공급량수정ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }
        }

        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(0, "yyyy-mm-dd hh");
            this.chart1.Data.Clear();
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            UltraGridColumn column = null;
            column = this.ultraGrid1.DisplayLayout.Bands[0].Columns["VALUE"];
            column.CellClickAction = CellClickAction.Edit;
            column.CellActivation = Activation.AllowEdit;
        }

        private void InitializeValueList()
        {
            ValueList valueList = null;

            //대블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("TAG_GBN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("TAG_GBN");
                valueList.ValueListItems.Add("FRQ", "블록 유입");
                valueList.ValueListItems.Add("SPL_D", "분기점");
                valueList.ValueListItems.Add("SPL_I", "배수지 유입");
                valueList.ValueListItems.Add("SPL_O", "배수지 유출");
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["TAG_GBN"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["TAG_GBN"];
        }

        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(ExportExcel_EventHandler);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.flowKind.SelectedIndexChanged += new EventHandler(flowKind_SelectedIndexChanged);
            this.flowMeasurePoint.SelectedIndexChanged += new EventHandler(flowMeasurePoint_SelectedIndexChanged);
            this.ultraGrid1.AfterCellUpdate += new CellEventHandler(ultraGrid1_AfterCellUpdate);
            this.ultraGrid1.AfterExitEditMode += new EventHandler(ultraGrid1_AfterExitEditMode);
            this.ultraGrid1.KeyDown += new KeyEventHandler(ultraGrid1_KeyDown);
        }

        /// <summary>
        /// 엑셀파일 다운로드이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportExcel_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.ultraGrid1, "적산차수정", 0, 0);
                this.excelManager.Save("적산차수정", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.updateAccumulationHourFlows();
                    MessageBox.Show("정상적으로 처리되었습니다.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }      
            }
        }

        private void updateAccumulationHourFlows()
        {
            this.ultraGrid1.UpdateData();
            DataTable data = this.ultraGrid1.DataSource as DataTable;
            if (data == null)
            {
                return;
            }

            ReportWork.GetInstance().updateAccumulationHourFlows(data);
        }

        private Hashtable parameter = new Hashtable();
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.parameter["STARTDATE"] = ((DateTime)this.startDate.Value).ToString("yyyyMMddHH");
                this.parameter["ENDDATE"] = ((DateTime)this.endDate.Value).ToString("yyyyMMddHH");
                this.parameter["TAG_GBN"] = this.flowMeasurePoint.SelectedValue;
                if (this.flowKind.SelectedValue.ToString().IndexOf("BLOCK") != -1)
                {
                    if (this.middleBlock.SelectedIndex == 0)
                    {
                        this.parameter["LOC_CODE"] = this.largeBlock.SelectedValue;
                    }
                    else if (this.smallBlock.SelectedIndex == 0)
                    {
                        this.parameter["LOC_CODE"] = this.middleBlock.SelectedValue;
                    }

                    if (this.smallBlock.SelectedIndex != 0)
                    {
                        this.parameter["LOC_CODE"] = this.smallBlock.SelectedValue;
                    }
                }
                if (this.flowKind.SelectedValue.ToString().IndexOf("RESERVOIR") != -1)
                {
                    this.parameter["LOC_CODE"] = this.reservoir.SelectedValue;
                }

                this.getAccumulationHourFlows();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void getAccumulationHourFlows()
        {
            this.ultraGrid1.DataSource = ReportWork.GetInstance().getAccumulationHourFlows(parameter);
            this.InitializeChartSetting();
        }

        /// <summary>
        /// 차트 데이터 세팅 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            this.chartManager.AllClear(0);
            this.chart1.Data.Clear();

            DataTable table = this.ultraGrid1.DataSource as DataTable;

            if (table == null)
            {
                return;
            }

            this.chart1.Data.Series = 1;
            this.chart1.Data.Points = table.Rows.Count;

            this.chart1.AxesY[0].Title.Text = "유량(㎥/h)";

            this.chart1.Series[0].Gallery = Gallery.Area;
            this.chart1.Series[0].Text = "유입유량(㎥/h)";

            foreach (DataRow row in table.Rows)
            {
                int pointIndex = table.Rows.IndexOf(row);
                this.chart1.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["TIMESTAMP"]).ToString("yyyy-MM-dd HH");
                if (row["VALUE"] != DBNull.Value)
                {
                    this.chart1.Data[0, pointIndex] = Utils.ToDouble(row["VALUE"]);
                }
            }

            chart1.Series[0].Visible = true;
        }

        //공급구분이 변경되면 검색조건을 변경해준다
        //공급구분이 블록일경우 blockPanel을 활성화 배수지일경우 reservoirPanel을 활성화한다.
        private void flowKind_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.flowKind.SelectedValue.ToString() == "BLOCK")
            {
                this.blockPanel.Visible = true;
                this.reservoirPanel.Visible = false;
            }
            if (this.flowKind.SelectedValue.ToString() == "RESERVOIR")
            {
                this.blockPanel.Visible = false;
                this.reservoirPanel.Visible = true;
            }

            this.flowMeasurePoint.ValueMember = "CODE";
            this.flowMeasurePoint.DisplayMember = "NAME";
            this.flowMeasurePoint.DataSource = this.getFlowMeasurePoints(this.flowKind.SelectedValue.ToString());
            if (this.flowMeasurePoint.Items.Count != 0)
            {
                this.flowMeasurePoint.SelectedIndex = 0;
            }
            else
            {
                this.flowMeasurePoint.Text = "";
            }
        }

        private void flowMeasurePoint_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.flowKind.SelectedValue.ToString() == "RESERVOIR")
            {
                this.reservoir.ValueMember = "CODE";
                this.reservoir.DisplayMember = "NAME";
                this.reservoir.DataSource = this.getReservoirs(this.flowMeasurePoint.SelectedValue.ToString());
                if (this.reservoir.Items.Count != 0)
                {
                    this.reservoir.SelectedIndex = 0;
                }
                else
                {
                    this.reservoir.Text = "";
                }
            }
        }

        private bool canInitializeChart = false;
        private void ultraGrid1_AfterCellUpdate(object sender, CellEventArgs e)
        {
            this.canInitializeChart = true;
        }

        private void ultraGrid1_AfterExitEditMode(object sender, EventArgs e)
        {
            if (this.canInitializeChart)
            {
                this.InitializeChartSetting();
                this.canInitializeChart = false;
            }
        }

        private void ultraGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.V)
            {
                if (this.ultraGrid1.ActiveRow == null)
                {
                    MessageBox.Show("값을 붙여넣기 하시려면 먼저 시작 셀을 선택하셔야 합니다.");
                    return;
                }

                //클립보드의 엑셀 선택 영역을 가져온다..
                //엑셀이 아니라면 값은 null 이며 동작하지 않는다.
                string[,] range = WV_Common.excel.Clipboard.GetStringRange();
                if (range == null)
                {
                    return;
                }

                for (int i = 0; i < range.GetLength(0); i++)
                {
                    for (int j = 0; j < range.GetLength(1); j++)
                    {
                        double result = 0;
                        if (range[i, j] != null)
                        {
                            if (!Double.TryParse(range[i, j], out result))
                            {
                                MessageBox.Show("선택하신 영역내에 숫자가 아닌 값이 존재합니다.");
                                return;
                            }
                        }
                    }
                }

                int count = 0;
                for (int i = this.ultraGrid1.ActiveRow.Index; i < this.ultraGrid1.Rows.Count; i++)
                {
                    if (count <= range.GetLength(0) -1 && i <= this.ultraGrid1.Rows.Count - 1)
                    {
                        this.ultraGrid1.Rows[i].Cells["VALUE"].Value = range[count, 0];
                        count++;
                    }
                }

                this.canInitializeChart = false;
                this.ultraGrid1.PerformAction(UltraGridAction.ExitEditMode);
                this.InitializeChartSetting();
            }
        }

        private DataTable getFlowKinds()
        {
            DataTable data = new DataTable();
            data.Columns.Add("CODE");
            data.Columns.Add("NAME");
            data.Rows.Add("BLOCK", "블록");
            data.Rows.Add("RESERVOIR", "배수지");
            return data;
        }

        private DataTable getFlowMeasurePoints(string flowKind)
        {
            DataTable data = new DataTable();
            data.Columns.Add("CODE");
            data.Columns.Add("NAME");

            if (flowKind.IndexOf("BLOCK") != -1)
            {
                data.Rows.Add("FRQ", "블록 유입");
            }

            if (flowKind.IndexOf("RESERVOIR") != -1)
            {
                data.Rows.Add("SPL_D", "분기점");
                data.Rows.Add("SPL_I", "배수지 유입");
                data.Rows.Add("SPL_O", "배수지 유출");
            }

            return data;
        }

        private DataTable getReservoirs(string flowMeasurePoint)
        {
            return ReportWork.GetInstance().getReservoirs(flowMeasurePoint);
        }
    }
}
