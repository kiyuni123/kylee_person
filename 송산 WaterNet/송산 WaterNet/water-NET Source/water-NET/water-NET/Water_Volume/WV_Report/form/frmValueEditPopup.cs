﻿using System;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.util;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Report.work;

namespace WaterNet.WV_Report.form
{
    public partial class frmValueEditPopup : Form
    {
        private UltraGridManager gridManager = null;

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmValueEditPopup()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmValueEditPopup_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmValueEditPopup_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeValueList();
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid1);

            //값필드 수정가능
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["VALUE"].CellClickAction = CellClickAction.Edit;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["VALUE"].CellActivation = Activation.AllowEdit;

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.cancelBtn.Click += new EventHandler(cancelBtn_Click);
        }

        /// <summary>
        /// 수정버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            if (!this.isMultiUpdate && this.cell == null)
            {
                this.Close();
                return;
            }

            if (this.isMultiUpdate && this.cells == null)
            {
                this.Close();
                return;
            }

            this.UpdateValueEdit();
            this.Close();
        }

        /// <summary>
        /// 취소버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            //ValueList valueList = null;

            ////대블록
            //if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            //{
            //    valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
            //    Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            //}

            ////중블록
            //if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            //{
            //    valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
            //    Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            //}

            ////소블록
            //if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            //{
            //    valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
            //    Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            //}

            //this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            //this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            //this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
        }


        private UltraGridCell cell = null;
        private SelectedCellsCollection cells = null;
        private bool isMultiUpdate = false;

        public void SelectValueEdit(Hashtable parameter, UltraGridCell gridCell)
        {
            this.cell = gridCell;

            this.ultraGrid1.DataSource = ReportWork.GetInstance().SelectValueEdit(parameter);

            //태그구분에 따라 컬럼 변경
            if (parameter["TAG_GBN"].ToString() == "FRQ" || parameter["TAG_GBN"].ToString() == "FRQ_O" || 
                parameter["TAG_GBN"].ToString() == "SPL_D" || parameter["TAG_GBN"].ToString() == "SPL_O" || 
                parameter["TAG_GBN"].ToString() == "SPL_I")
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["DATEE"].Format = "HH:mm";
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["DATEE"].Header.Caption = "시간";

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["DATEE"].Hidden = true;
            }

            if (parameter["TAG_GBN"].ToString() == "MNF" || parameter["TAG_GBN"].ToString() == "YD")
            {
                if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.Exists("V_TIME"))
                {
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns["V_TIME"].Hidden = true;
                }
                if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.Exists("E_TIME"))
                {
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns["E_TIME"].Hidden = true;
                }
            }
        }

        public void SelectValueEdit(Hashtable parameter, SelectedCellsCollection gridCells)
        {
            this.isMultiUpdate = true;
            this.cells = gridCells;

            this.ultraGrid1.DataSource = ReportWork.GetInstance().SelectValueEdit(parameter);

            //태그구분에 따라 컬럼 변경
            if (parameter["TAG_GBN"].ToString() == "FRQ" || parameter["TAG_GBN"].ToString() == "FRQ_O" ||
                parameter["TAG_GBN"].ToString() == "SPL_D" || parameter["TAG_GBN"].ToString() == "SPL_O" ||
                parameter["TAG_GBN"].ToString() == "SPL_I")
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["DATEE"].Format = "HH:mm";
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["DATEE"].Header.Caption = "시간";

                this.ultraGrid1.DisplayLayout.Bands[0].Columns["DATEE"].Hidden = true;
            }
        }

        private void UpdateValueEdit()
        {
            //계값,
            if (this.ultraGrid1.Rows.Count == 0)
            {
                return;
            }

            double totalSum = 0;
            bool totalSum_n = true;

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                if (gridRow.Cells["VALUE"].Value != DBNull.Value)
                {
                    totalSum_n = false;
                }

                totalSum += Utils.ToDouble(gridRow.Cells["VALUE"].Value);

                Hashtable parameter = new Hashtable();
                parameter["LOC_CODE"] = gridRow.Cells["LOC_CODE"].Value;
                parameter["TAGNAME"] = gridRow.Cells["TAGNAME"].Value;
                parameter["TAG_GBN"] = gridRow.Cells["TAG_GBN"].Value;

                if (gridRow.Cells["TAG_GBN"].Value.ToString() == "MNF")
                {
                    parameter["DATEE"] = Convert.ToDateTime(gridRow.Cells["DATEE"].Value).ToString("yyyyMMdd");
                }

                    //시간 적산, 다중수정/ 단일수정
                else if (gridRow.Cells["TAG_GBN"].Value.ToString() == "FRQ" || gridRow.Cells["TAG_GBN"].Value.ToString() == "FRQ_O" ||
                    gridRow.Cells["TAG_GBN"].Value.ToString() == "SPL_D" || gridRow.Cells["TAG_GBN"].Value.ToString() == "SPL_O" ||
                    gridRow.Cells["TAG_GBN"].Value.ToString() == "SPL_I")
                {
                    parameter["DATEE"] = Convert.ToDateTime(gridRow.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
                    //20140512_승일 수정
                    //시단위공급량2 DB쿼리에서 날짜값을 가져오는 부분이 없어서 String 문자열 파싱하여
                    //parameter 인자값에 전달


                    DateTime v_cell = (DateTime)gridRow.Cells["V_TIME"].Value;
                    DateTime e_cell = (DateTime)gridRow.Cells["E_TIME"].Value;


                    //시작
                    long v_time = v_cell.Ticks;

                    //종료
                    long e_time = e_cell.Ticks;

                    if (this.isMultiUpdate)
                    {
                        /*
                        string v_cell = parameter["DATEE"].ToString().Substring(0, 8) + gridRow.Cells["V_TIME"].Value.ToString().Remove(2, 1);
                        string e_cell = string.Empty;
                        if (gridRow.Cells["E_TIME"].Value.ToString() == "24:00")
                        {
                            string date = parameter["DATEE"].ToString().Substring(0, 8);
                            DateTime dt = DateTime.ParseExact(date, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                            e_cell = dt.AddDays(1).ToString("yyyyMMdd") + "0000";
                        }
                        else
                        {
                            e_cell = parameter["DATEE"].ToString().Substring(0, 8) + gridRow.Cells["E_TIME"].Value.ToString().Remove(2, 1);
                        }
                        */

                        if (v_time == e_time)
                        {
                            parameter["V_TIME"] = v_cell;
                            parameter["E_TIME"] = v_cell;
                        }

                        if (v_time < e_time)
                        {
                            parameter["V_TIME"] = v_cell;
                            parameter["E_TIME"] = e_cell;
                        }
                        if (v_time > e_time && e_time != 0)
                        {
                            parameter["V_TIME"] = e_cell;
                            parameter["E_TIME"] = v_cell;
                        }
                        if (e_time == 0)
                        {
                            parameter["V_TIME"] = v_cell;
                            parameter["E_TIME"] = e_cell;
                        }
                    }
                    else if (!this.isMultiUpdate)
                    {
                        //parameter["V_TIME"] = Convert.ToDateTime(gridRow.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
                        //parameter["E_TIME"] = Convert.ToDateTime(gridRow.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");

                        parameter["V_TIME"] = v_cell;
                        parameter["E_TIME"] = e_cell;
                    }
                }
                else if (gridRow.Cells["TAG_GBN"].Value.ToString() == "YD")
                {
                    parameter["DATEE"] = Convert.ToDateTime(gridRow.Cells["DATEE"].Value).ToString("yyyyMMdd");
                }

                parameter["VALUE"] = gridRow.Cells["VALUE"].Value;

                //DB수정
                ReportWork.GetInstance().UpdateValueEdit(parameter);

            }

            if (this.isMultiUpdate)
            {
                foreach (UltraGridCell gridCell in this.cells)
                {
                    if (totalSum_n)
                    {
                        gridCell.Value = DBNull.Value;
                    }
                    else if (!totalSum_n)
                    {
                        gridCell.Value = totalSum;
                    }
                    //if (totalSum == 0)
                    //{
                    //    gridCell.Value = DBNull.Value;
                    //}
                    //else
                    //{
                    //    gridCell.Value = totalSum;
                    //}
                }
            }
            else if (!this.isMultiUpdate)
            {
                if (totalSum_n)
                {
                    this.cell.Value = DBNull.Value;
                }
                else if (!totalSum_n)
                {
                    this.cell.Value = totalSum;
                }
                //if (totalSum == 0)
                //{
                //    this.cell.Value = DBNull.Value;
                //}
                //else
                //{
                //    this.cell.Value = totalSum;
                //}
            }
        }
    }
}
