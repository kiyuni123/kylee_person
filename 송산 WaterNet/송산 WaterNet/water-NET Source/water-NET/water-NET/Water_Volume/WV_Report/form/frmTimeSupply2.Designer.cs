﻿namespace WaterNet.WV_Report.form
{
    partial class frmTimeSupply2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DATEE");
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("V_TIME");
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_SUM");
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK", 0, null, 76494311, 0, 0);
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK", 1, null, 76494311, 1, 0);
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK", 2, null, 76494311, 2, 0);
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH01", 3, null, 76494312, 0, 0);
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH02", 4, null, 76494312, 1, 0);
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH03", 5, null, 76494312, 2, 0);
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH04", 6, null, 76494312, 3, 0);
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH05", 7, null, 76494312, 4, 0);
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH06", 8, null, 76494312, 5, 0);
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH07", 9, null, 76494312, 6, 0);
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH08", 10, null, 76494312, 7, 0);
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH09", 11, null, 76494312, 8, 0);
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH10", 12, null, 76494312, 9, 0);
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn22 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH11", 13, null, 76494312, 10, 0);
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn23 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH12", 14, null, 76494312, 11, 0);
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn24 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH13", 15, null, 76494312, 12, 0);
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn25 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH14", 16, null, 76494312, 13, 0);
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn26 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH15", 17, null, 76494312, 14, 0);
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn27 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH16", 18, null, 76494312, 15, 0);
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn28 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH17", 19, null, 76494312, 16, 0);
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn29 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH18", 20, null, 76494312, 17, 0);
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn30 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH19", 21, null, 76494312, 18, 0);
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn31 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH20", 22, null, 76494312, 19, 0);
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn32 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH21", 23, null, 76494312, 20, 0);
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn33 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH22", 24, null, 76494312, 21, 0);
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn34 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH23", 25, null, 76494312, 22, 0);
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn35 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HH24", 26, null, 76494312, 23, 0);
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TAG_GBN", 27, null, 76494313, 0, 0);
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DAYSUM", 28, null, 76494326, 0, 0);
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("구분", 76494311);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup(76494312);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup2", 76494313);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup3", 76494326);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTimeSupply2));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.updateBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(10, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1232, 10);
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(10, 815);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1232, 10);
            this.pictureBox4.TabIndex = 39;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 825);
            this.pictureBox2.TabIndex = 37;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(1242, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 825);
            this.pictureBox3.TabIndex = 38;
            this.pictureBox3.TabStop = false;
            // 
            // ultraGrid1
            // 
            ultraGridColumn3.AutoSizeEdit = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn3.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.None;
            appearance38.BackColor = System.Drawing.SystemColors.Control;
            appearance38.TextHAlignAsString = "Center";
            appearance38.TextVAlignAsString = "Middle";
            ultraGridColumn3.CellAppearance = appearance38;
            ultraGridColumn3.Format = "HH24:mm";
            appearance39.TextHAlignAsString = "Center";
            appearance39.TextVAlignAsString = "Middle";
            ultraGridColumn3.Header.Appearance = appearance39;
            ultraGridColumn3.Header.Caption = "구 분";
            ultraGridColumn3.Header.Fixed = true;
            ultraGridColumn3.Header.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button;
            ultraGridColumn3.Header.VisiblePosition = 0;
            ultraGridColumn3.Hidden = true;
            ultraGridColumn3.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn3.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn3.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn3.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn3.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn3.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn3.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn3.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn3.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn5.CellActivation = Infragistics.Win.UltraWinGrid.Activation.Disabled;
            appearance40.BackColor = System.Drawing.SystemColors.Control;
            appearance40.TextHAlignAsString = "Center";
            appearance40.TextVAlignAsString = "Middle";
            ultraGridColumn5.CellAppearance = appearance40;
            ultraGridColumn5.Header.Caption = "구 분";
            ultraGridColumn5.Header.VisiblePosition = 1;
            ultraGridColumn5.Hidden = true;
            ultraGridColumn5.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn5.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn5.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(83, 0);
            ultraGridColumn5.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn5.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn6.AutoSizeEdit = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn6.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.None;
            ultraGridColumn6.CellActivation = Infragistics.Win.UltraWinGrid.Activation.Disabled;
            appearance41.BackColor = System.Drawing.SystemColors.Info;
            appearance41.TextHAlignAsString = "Right";
            appearance41.TextVAlignAsString = "Middle";
            ultraGridColumn6.CellAppearance = appearance41;
            ultraGridColumn6.Format = "###,###,###";
            appearance42.TextHAlignAsString = "Center";
            appearance42.TextVAlignAsString = "Middle";
            ultraGridColumn6.Header.Appearance = appearance42;
            ultraGridColumn6.Header.Caption = "총 계";
            ultraGridColumn6.Header.VisiblePosition = 2;
            ultraGridColumn6.Hidden = true;
            ultraGridColumn6.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn6.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn6.RowLayoutColumnInfo.OriginX = 1;
            ultraGridColumn6.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn6.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(85, 0);
            ultraGridColumn6.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn6.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn6.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn6.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn8.Header.Caption = "대블록";
            ultraGridColumn8.Header.Fixed = true;
            ultraGridColumn8.Header.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            ultraGridColumn8.Header.FixOnRight = Infragistics.Win.DefaultableBoolean.True;
            appearance43.TextHAlignAsString = "Left";
            appearance43.TextVAlignAsString = "Top";
            ultraGridColumn8.MergedCellAppearance = appearance43;
            ultraGridColumn8.NullText = "";
            ultraGridColumn8.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn8.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn8.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn8.RowLayoutColumnInfo.ParentGroupKey = "구분";
            ultraGridColumn8.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn8.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn8.Width = 100;
            appearance44.TextVAlignAsString = "Middle";
            ultraGridColumn9.CellAppearance = appearance44;
            ultraGridColumn9.Header.Caption = "중블록";
            ultraGridColumn9.Header.FixOnRight = Infragistics.Win.DefaultableBoolean.False;
            appearance45.TextHAlignAsString = "Left";
            appearance45.TextVAlignAsString = "Top";
            ultraGridColumn9.MergedCellAppearance = appearance45;
            ultraGridColumn9.NullText = "";
            ultraGridColumn9.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn9.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn9.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn9.RowLayoutColumnInfo.ParentGroupKey = "구분";
            ultraGridColumn9.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn9.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn9.Width = 100;
            appearance46.TextHAlignAsString = "Center";
            appearance46.TextVAlignAsString = "Middle";
            ultraGridColumn10.CellAppearance = appearance46;
            ultraGridColumn10.Header.Caption = "소블록";
            ultraGridColumn10.Header.FixOnRight = Infragistics.Win.DefaultableBoolean.False;
            appearance47.TextHAlignAsString = "Center";
            appearance47.TextVAlignAsString = "Middle";
            ultraGridColumn10.MergedCellAppearance = appearance47;
            ultraGridColumn10.NullText = "";
            ultraGridColumn10.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn10.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn10.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn10.RowLayoutColumnInfo.ParentGroupKey = "구분";
            ultraGridColumn10.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn10.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn10.Width = 100;
            appearance48.TextHAlignAsString = "Right";
            appearance48.TextVAlignAsString = "Middle";
            ultraGridColumn12.CellAppearance = appearance48;
            ultraGridColumn12.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            ultraGridColumn12.Format = "###,###,##0";
            ultraGridColumn12.Header.Caption = "01:00";
            ultraGridColumn12.NullText = "";
            ultraGridColumn12.RowLayoutColumnInfo.LabelSpan = 5;
            ultraGridColumn12.RowLayoutColumnInfo.OriginX = 1;
            ultraGridColumn12.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn12.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn12.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn12.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn12.Width = 80;
            appearance49.TextHAlignAsString = "Right";
            appearance49.TextVAlignAsString = "Middle";
            ultraGridColumn13.CellAppearance = appearance49;
            ultraGridColumn13.Format = "###,###,##0";
            ultraGridColumn13.Header.Caption = "02:00";
            ultraGridColumn13.NullText = "";
            ultraGridColumn13.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn13.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn13.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn13.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn13.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn13.Width = 80;
            appearance50.TextHAlignAsString = "Right";
            appearance50.TextVAlignAsString = "Middle";
            ultraGridColumn14.CellAppearance = appearance50;
            ultraGridColumn14.Format = "###,###,##0";
            ultraGridColumn14.Header.Caption = "03:00";
            ultraGridColumn14.NullText = "";
            ultraGridColumn14.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn14.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn14.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn14.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn14.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn14.Width = 80;
            appearance51.TextHAlignAsString = "Right";
            appearance51.TextVAlignAsString = "Middle";
            ultraGridColumn15.CellAppearance = appearance51;
            ultraGridColumn15.Format = "###,###,##0";
            ultraGridColumn15.Header.Caption = "04:00";
            ultraGridColumn15.NullText = "";
            ultraGridColumn15.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn15.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn15.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn15.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn15.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn15.Width = 80;
            appearance52.TextHAlignAsString = "Right";
            appearance52.TextVAlignAsString = "Middle";
            ultraGridColumn16.CellAppearance = appearance52;
            ultraGridColumn16.Format = "###,###,##0";
            appearance53.TextHAlignAsString = "Right";
            ultraGridColumn16.Header.Appearance = appearance53;
            ultraGridColumn16.Header.Caption = "05:00";
            ultraGridColumn16.NullText = "";
            ultraGridColumn16.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.Right;
            ultraGridColumn16.RowLayoutColumnInfo.OriginX = 1;
            ultraGridColumn16.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn16.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn16.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn16.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn16.Width = 80;
            appearance54.TextHAlignAsString = "Right";
            appearance54.TextVAlignAsString = "Middle";
            ultraGridColumn17.CellAppearance = appearance54;
            ultraGridColumn17.Format = "###,###,##0";
            ultraGridColumn17.Header.Caption = "06:00";
            ultraGridColumn17.NullText = "";
            ultraGridColumn17.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn17.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn17.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn17.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn17.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn17.Width = 80;
            appearance55.TextHAlignAsString = "Right";
            appearance55.TextVAlignAsString = "Middle";
            ultraGridColumn18.CellAppearance = appearance55;
            ultraGridColumn18.Format = "###,###,##0";
            ultraGridColumn18.Header.Caption = "07:00";
            ultraGridColumn18.NullText = "";
            ultraGridColumn18.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn18.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn18.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn18.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn18.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn18.Width = 80;
            appearance56.TextHAlignAsString = "Right";
            appearance56.TextVAlignAsString = "Middle";
            ultraGridColumn19.CellAppearance = appearance56;
            ultraGridColumn19.Format = "###,###,##0";
            ultraGridColumn19.Header.Caption = "08:00";
            ultraGridColumn19.NullText = "";
            ultraGridColumn19.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn19.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn19.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn19.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn19.Width = 80;
            appearance57.TextHAlignAsString = "Right";
            appearance57.TextVAlignAsString = "Middle";
            ultraGridColumn20.CellAppearance = appearance57;
            ultraGridColumn20.Format = "###,###,##0";
            ultraGridColumn20.Header.Caption = "09:00";
            ultraGridColumn20.NullText = "";
            ultraGridColumn20.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn20.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn20.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn20.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn20.Width = 80;
            appearance58.TextHAlignAsString = "Right";
            appearance58.TextVAlignAsString = "Middle";
            ultraGridColumn21.CellAppearance = appearance58;
            ultraGridColumn21.Format = "###,###,##0";
            ultraGridColumn21.Header.Caption = "10:00";
            ultraGridColumn21.NullText = "";
            ultraGridColumn21.RowLayoutColumnInfo.OriginX = 18;
            ultraGridColumn21.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn21.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn21.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn21.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn21.Width = 80;
            appearance59.TextHAlignAsString = "Right";
            appearance59.TextVAlignAsString = "Middle";
            ultraGridColumn22.CellAppearance = appearance59;
            ultraGridColumn22.Format = "###,###,##0";
            ultraGridColumn22.Header.Caption = "11:00";
            ultraGridColumn22.NullText = "";
            ultraGridColumn22.RowLayoutColumnInfo.OriginX = 20;
            ultraGridColumn22.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn22.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn22.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn22.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn22.Width = 80;
            appearance60.TextHAlignAsString = "Right";
            appearance60.TextVAlignAsString = "Middle";
            ultraGridColumn23.CellAppearance = appearance60;
            ultraGridColumn23.Format = "###,###,##0";
            ultraGridColumn23.Header.Caption = "12:00";
            ultraGridColumn23.NullText = "";
            ultraGridColumn23.RowLayoutColumnInfo.OriginX = 22;
            ultraGridColumn23.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn23.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn23.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn23.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn23.Width = 80;
            appearance61.TextHAlignAsString = "Right";
            appearance61.TextVAlignAsString = "Middle";
            ultraGridColumn24.CellAppearance = appearance61;
            ultraGridColumn24.Format = "###,###,##0";
            ultraGridColumn24.Header.Caption = "13:00";
            ultraGridColumn24.NullText = "";
            ultraGridColumn24.RowLayoutColumnInfo.OriginX = 24;
            ultraGridColumn24.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn24.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn24.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn24.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn24.Width = 80;
            appearance62.TextHAlignAsString = "Right";
            appearance62.TextVAlignAsString = "Middle";
            ultraGridColumn25.CellAppearance = appearance62;
            ultraGridColumn25.Format = "###,###,##0";
            ultraGridColumn25.Header.Caption = "14:00";
            ultraGridColumn25.NullText = "";
            ultraGridColumn25.RowLayoutColumnInfo.OriginX = 26;
            ultraGridColumn25.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn25.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn25.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn25.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn25.Width = 80;
            appearance63.TextHAlignAsString = "Right";
            appearance63.TextVAlignAsString = "Middle";
            ultraGridColumn26.CellAppearance = appearance63;
            ultraGridColumn26.Format = "###,###,##0";
            ultraGridColumn26.Header.Caption = "15:00";
            ultraGridColumn26.NullText = "";
            ultraGridColumn26.RowLayoutColumnInfo.OriginX = 28;
            ultraGridColumn26.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn26.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn26.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn26.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn26.Width = 80;
            appearance64.TextHAlignAsString = "Right";
            appearance64.TextVAlignAsString = "Middle";
            ultraGridColumn27.CellAppearance = appearance64;
            ultraGridColumn27.Format = "###,###,##0";
            ultraGridColumn27.Header.Caption = "16:00";
            ultraGridColumn27.NullText = "";
            ultraGridColumn27.RowLayoutColumnInfo.OriginX = 30;
            ultraGridColumn27.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn27.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn27.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn27.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn27.Width = 80;
            appearance65.TextHAlignAsString = "Right";
            appearance65.TextVAlignAsString = "Middle";
            ultraGridColumn28.CellAppearance = appearance65;
            ultraGridColumn28.Format = "###,###,##0";
            ultraGridColumn28.Header.Caption = "17:00";
            ultraGridColumn28.NullText = "";
            ultraGridColumn28.RowLayoutColumnInfo.OriginX = 32;
            ultraGridColumn28.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn28.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn28.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn28.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn28.Width = 80;
            appearance66.TextHAlignAsString = "Right";
            appearance66.TextVAlignAsString = "Middle";
            ultraGridColumn29.CellAppearance = appearance66;
            ultraGridColumn29.Format = "###,###,##0";
            ultraGridColumn29.Header.Caption = "18:00";
            ultraGridColumn29.NullText = "";
            ultraGridColumn29.RowLayoutColumnInfo.OriginX = 34;
            ultraGridColumn29.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn29.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn29.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn29.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn29.Width = 80;
            appearance67.TextHAlignAsString = "Right";
            appearance67.TextVAlignAsString = "Middle";
            ultraGridColumn30.CellAppearance = appearance67;
            ultraGridColumn30.Format = "###,###,##0";
            ultraGridColumn30.Header.Caption = "19:00";
            ultraGridColumn30.NullText = "";
            ultraGridColumn30.RowLayoutColumnInfo.OriginX = 36;
            ultraGridColumn30.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn30.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn30.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn30.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn30.Width = 80;
            appearance68.TextHAlignAsString = "Right";
            appearance68.TextVAlignAsString = "Middle";
            ultraGridColumn31.CellAppearance = appearance68;
            ultraGridColumn31.Format = "###,###,##0";
            ultraGridColumn31.Header.Caption = "20:00";
            ultraGridColumn31.NullText = "";
            ultraGridColumn31.RowLayoutColumnInfo.OriginX = 38;
            ultraGridColumn31.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn31.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn31.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn31.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn31.Width = 80;
            appearance69.TextHAlignAsString = "Right";
            appearance69.TextVAlignAsString = "Middle";
            ultraGridColumn32.CellAppearance = appearance69;
            ultraGridColumn32.Format = "###,###,##0";
            ultraGridColumn32.Header.Caption = "21:00";
            ultraGridColumn32.NullText = "";
            ultraGridColumn32.RowLayoutColumnInfo.OriginX = 40;
            ultraGridColumn32.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn32.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn32.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn32.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn32.Width = 80;
            appearance70.TextHAlignAsString = "Right";
            appearance70.TextVAlignAsString = "Middle";
            ultraGridColumn33.CellAppearance = appearance70;
            ultraGridColumn33.Format = "###,###,##0";
            ultraGridColumn33.Header.Caption = "22:00";
            ultraGridColumn33.NullText = "";
            ultraGridColumn33.RowLayoutColumnInfo.OriginX = 42;
            ultraGridColumn33.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn33.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn33.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn33.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn33.Width = 80;
            appearance71.TextHAlignAsString = "Right";
            appearance71.TextVAlignAsString = "Middle";
            ultraGridColumn34.CellAppearance = appearance71;
            ultraGridColumn34.Format = "###,###,##0";
            ultraGridColumn34.Header.Caption = "23:00";
            ultraGridColumn34.NullText = "";
            ultraGridColumn34.RowLayoutColumnInfo.OriginX = 44;
            ultraGridColumn34.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn34.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn34.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn34.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn34.Width = 80;
            appearance72.TextHAlignAsString = "Right";
            appearance72.TextVAlignAsString = "Middle";
            ultraGridColumn35.CellAppearance = appearance72;
            ultraGridColumn35.Format = "###,###,##0";
            ultraGridColumn35.Header.Caption = "24:00";
            ultraGridColumn35.NullText = "";
            ultraGridColumn35.RowLayoutColumnInfo.OriginX = 46;
            ultraGridColumn35.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn35.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn35.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn35.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn35.Width = 80;
            appearance73.TextHAlignAsString = "Center";
            appearance73.TextVAlignAsString = "Middle";
            ultraGridColumn1.CellAppearance = appearance73;
            ultraGridColumn1.Header.Caption = "구분";
            ultraGridColumn1.Header.Fixed = true;
            ultraGridColumn1.Header.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button;
            ultraGridColumn1.Header.FixOnRight = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn1.NullText = "";
            ultraGridColumn1.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn1.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn1.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 42);
            ultraGridColumn1.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn1.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn1.Width = 100;
            appearance74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            ultraGridColumn2.CellAppearance = appearance74;
            ultraGridColumn2.Format = "###,###,##0";
            ultraGridColumn2.Header.Caption = "일계";
            ultraGridColumn2.NullText = "";
            ultraGridColumn2.RowLayoutColumnInfo.OriginX = 56;
            ultraGridColumn2.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn2.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 42);
            ultraGridColumn2.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn2.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn2.Width = 80;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn3,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21,
            ultraGridColumn22,
            ultraGridColumn23,
            ultraGridColumn24,
            ultraGridColumn25,
            ultraGridColumn26,
            ultraGridColumn27,
            ultraGridColumn28,
            ultraGridColumn29,
            ultraGridColumn30,
            ultraGridColumn31,
            ultraGridColumn32,
            ultraGridColumn33,
            ultraGridColumn34,
            ultraGridColumn35,
            ultraGridColumn1,
            ultraGridColumn2});
            ultraGridGroup1.Header.Caption = "블록";
            ultraGridGroup1.Key = "구분";
            ultraGridGroup1.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup1.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 6;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup2.Header.Caption = "시간";
            ultraGridGroup2.Header.VisiblePosition = 2;
            ultraGridGroup2.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup2.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 8;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 48;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup3.Header.Caption = "";
            ultraGridGroup3.Header.VisiblePosition = 1;
            ultraGridGroup3.Hidden = true;
            ultraGridGroup3.Key = "NewGroup2";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.Header.Caption = "";
            ultraGridGroup4.Key = "NewGroup3";
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4});
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(10, 151);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(1232, 664);
            this.ultraGrid1.TabIndex = 42;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.updateBtn);
            this.panel8.Controls.Add(this.searchBtn);
            this.panel8.Controls.Add(this.excelBtn);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Controls.Add(this.panel2);
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.panel1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(10, 121);
            this.panel8.Margin = new System.Windows.Forms.Padding(0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1232, 30);
            this.panel8.TabIndex = 41;
            // 
            // updateBtn
            // 
            this.updateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.updateBtn.Location = new System.Drawing.Point(1146, 2);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(40, 25);
            this.updateBtn.TabIndex = 31;
            this.updateBtn.TabStop = false;
            this.updateBtn.Text = "저장";
            this.updateBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(1192, 2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 29;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(1100, 2);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 30;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(114, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 12);
            this.label2.TabIndex = 27;
            this.label2.Text = ": 결측";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Red;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(82, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(30, 15);
            this.panel2.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 12);
            this.label1.TabIndex = 25;
            this.label1.Text = ": 보정";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SkyBlue;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(0, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(30, 15);
            this.panel1.TabIndex = 24;
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1232, 111);
            this.searchBox1.TabIndex = 40;
            // 
            // frmTimeSupply2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1252, 825);
            this.Controls.Add(this.ultraGrid1);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Name = "frmTimeSupply2";
            this.Text = "시단위공급량2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private WaterNet.WV_Common.form.SearchBox searchBox1;

    }
}