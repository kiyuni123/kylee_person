﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WV_Common.util;
using WaterNet.WV_Report.work;
using WaterNet.WaterNetCore;
using WaterNet.WV_Report.cal;
using System.IO;
using WaterNet.WV_Common.enum1;
using System.Collections.Generic;
using EMFrame.log;
using System.Diagnostics;

namespace WaterNet.WV_Report.form
{
    public partial class frm31DayMNF : Form, WaterNet.WaterNetCore.IForminterface
    {
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frm31DayMNF()
        {
            InitializeComponent();
            this.Load += new EventHandler(frm31DayMNF_Load);
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        public void Open()
        {
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm31DayMNF_Load(object sender, EventArgs e)
        {
            //동진_2012.08.23
            object o = EMFrame.statics.AppStatic.USER_MENU["일야간최소유량ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }

            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeForm();

            //폼 로드 자동검색.
            this.searchBtn_Click(this.searchBtn, new EventArgs());
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);

            this.ultraGrid1.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            //구분 소팅 안되게
            if (this.ultraGrid1.DisplayLayout.Bands[0].Columns.IndexOf("DATEE") != -1)
            {
                UltraGridColumn column = this.ultraGrid1.DisplayLayout.Bands[0].Columns["DATEE"];
                column.SortIndicator = SortIndicator.Disabled;
            }
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);

            this.searchBox1.EndDateObject.ValueChanged += new EventHandler(EndDateObject_ValueChanged);
            this.EndDateObject_ValueChanged(null, null);

            this.ultraGrid1.DoubleClickHeader += new DoubleClickHeaderEventHandler(ultraGrid1_DoubleClickHeader);
            //셀수정이벤트,
            this.ultraGrid1.AfterCellUpdate += new CellEventHandler(ultraGrid1_AfterCellUpdate);
            this.ultraGrid1.AfterExitEditMode += new EventHandler(ultraGrid1_AfterExitEditMode);
            this.ultraGrid1.KeyDown += new KeyEventHandler(ultraGrid1_KeyDown);
        }

        /// <summary>
        /// 더블클릭 태그찾기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ultraGrid1_DoubleClickHeader(object sender, DoubleClickHeaderEventArgs e)
        {
            try
            {
                string loc_code = string.Empty;
                if (e.Header.Column == null && e.Header.Group != null)
                {
                    loc_code = e.Header.Group.Key;
                }
                else
                {
                    loc_code = e.Header.Column.Key;// e.Header.Column.Key;
                    //loc_gbn = e.Header.Column.Key.Substring(0, 1);
                }
                if (string.IsNullOrEmpty(loc_code) || loc_code.IndexOf("SUM") > -1) return;

                WV_Common.form.frmTagDescription form = new WaterNet.WV_Common.form.frmTagDescription(loc_code, "MNF");
                form.Show();
            }
            catch { }
        }

        private bool isUserCellEdit = false;
        private bool isUserCellUpdate = false;
        private void ultraGrid1_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (!isUserCellUpdate || 
                this.ultraGrid1.DataSource == null || 
                e.Cell.Column.CellAppearance.BackColor != Color.Empty)
            {
                return;
            }

            double mnf = 0;
            double filtering = Utils.ToDouble(e.Cell.Value);

            //2011.3.31
            bool mnf_n = true;
            bool filtering_n = true;

            //원데이터와 수정데이터를 비교
            DataSet dataSet = (DataSet)this.ultraGrid1.DataSource;

            foreach (DataRow dataRow in dataSet.Tables["SMALL_INFO"].Rows)
            {
                if (Convert.ToDateTime(dataRow["DATEE"]).ToString("yyyyMMdd") ==
                    Convert.ToDateTime(e.Cell.Row.Cells["DATEE"].Value).ToString("yyyyMMdd"))
                {
                    if (dataRow["SBLOCK"].ToString() == e.Cell.Column.Key)
                    {
                        //2011.3.31
                        if (dataRow["MNF"] != DBNull.Value)
                        {
                            mnf_n = false;
                        }
                        mnf = Utils.ToDouble(dataRow["MNF"]);
                    }
                }
            }

            //2011.3.31
            if (e.Cell.Value != DBNull.Value)
            {
                filtering_n = false;
            }

            //2011.3.31
            if (mnf_n && filtering_n)
            {
                e.Cell.ToolTipText = "[결측]";
                e.Cell.Appearance.BackColor = Color.Red;
            }
            else if (!mnf_n && !filtering_n && mnf == filtering)
            {
                e.Cell.ToolTipText = mnf.ToString();
                e.Cell.Appearance.BackColor = Color.Empty;
            }
            else
            {
                if (!mnf_n && filtering_n)
                {
                    e.Cell.ToolTipText = "[보정]\n원본값 : " + mnf.ToString() + " --> " + "보정값 : 결측";
                }
                else if (mnf_n && !filtering_n)
                {
                    e.Cell.ToolTipText = "[보정]\n원본값 : 결측" + " --> " + "보정값 : " + filtering.ToString();
                }
                else
                {
                    e.Cell.ToolTipText = "[보정]\n원본값 : " + mnf.ToString() + " --> " + "보정값 : " + filtering.ToString();
                }

                e.Cell.Appearance.BackColor = Color.SkyBlue;
            }

            double sumVal = 0;
            double totalVal = 0;
            string parentKey = e.Cell.Column.RowLayoutColumnInfo.ParentGroup.RowLayoutGroupInfo.ParentGroup.Key;

            foreach (UltraGridCell gridCell in e.Cell.Row.Cells)
            {
                if (gridCell.Column.CellAppearance.BackColor == Color.Empty)
                {
                    //소계
                    if (gridCell.Column.RowLayoutColumnInfo.ParentGroup.RowLayoutGroupInfo.ParentGroup.Key == parentKey)
                    {
                        sumVal += Utils.ToDouble(gridCell.Value);
                    }

                    //총계
                    totalVal += Utils.ToDouble(gridCell.Value);
                }
            }

            e.Cell.Row.Cells[parentKey + "_SUM"].Value = sumVal;
            e.Cell.Row.Cells["TOTAL_SUM"].Value = totalVal;

            //WaterNetCore.UserWorkHistoryLog.UPDATE(
            //    "일야간최소유량ToolStripMenuItem",
            //    e.Cell.Column.RowLayoutColumnInfo.ParentGroup.Header.Caption + " " + Convert.ToDateTime(e.Cell.Row.Cells["DATEE"].Value).ToString("yyyy-MM-dd") + "일 " + "야간최소유량 변경 (" + e.Cell.OriginalValue.ToString() + " => " + filtering.ToString() + ")"
            //);
        }

        private void ultraGrid1_AfterExitEditMode(object sender, EventArgs e)
        {
            this.isUserCellEdit = false;
        }

        private void ultraGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.V)
            {
                //선택된 셀이 없으면 동작하지 않는다.
                if (this.ultraGrid1.ActiveCell == null)
                {
                    return;
                }

                //클립보드의 엑셀 선택 영역을 가져온다..
                //엑셀이 아니라면 값은 null 이며 동작하지 않는다.
                string[,] range = WV_Common.excel.Clipboard.GetStringRange();
                if (range == null)
                {
                    return;
                }

                int start_row_idx = this.ultraGrid1.ActiveCell.Row.Index;
                int start_cell_idx = 0;
                int column_index = 0;
                Dictionary<int, string> columns = new Dictionary<int, string>();

                UltraGridColumn column = this.ultraGrid1.DisplayLayout.Bands[0].Columns[0];
                column = column.GetRelatedVisibleColumn(VisibleRelation.First);

                while (null != column)
                {
                    if (column != null)
                    {
                        columns[column_index] = column.Key;

                        if (this.ultraGrid1.ActiveCell.Column.Key == column.Key)
                        {
                            start_cell_idx = column_index;
                        }
                    }

                    column = column.GetRelatedVisibleColumn(VisibleRelation.Next);
                    column_index++;
                }

                int target_row_idx = 0;
                int target_cell_idx = 0;

                for (int i = 0; i < range.GetLength(0); i++)
                {
                    for (int j = 0; j < range.GetLength(1); j++)
                    {
                        double result = 0;
                        if (range[i, j] != null)
                        {
                            if (!Double.TryParse(range[i, j], out result))
                            {
                                MessageBox.Show("선택하신 영역내에 숫자가 아닌 값이 존재합니다.");
                                return;
                            }
                        }
                    }
                }

                this.ultraGrid1.PerformAction(UltraGridAction.ExitEditMode);

                for (int i = start_row_idx; i < this.ultraGrid1.Rows.Count; i++)
                {
                    target_cell_idx = 0;

                    if (target_row_idx > range.GetLength(0) - 1)
                    {
                        break;
                    }

                    for (int j = start_cell_idx; j < columns.Count; j++)
                    {
                        if (target_cell_idx > range.GetLength(1) - 1)
                        {
                            break;
                        }

                        if (this.ultraGrid1.Rows[i].Cells[columns[j]].Column.CellAppearance.BackColor == Color.Empty)
                        {
                            this.ultraGrid1.Rows[i].Cells[columns[j]].Value = range[target_row_idx, target_cell_idx];
                        }

                        target_cell_idx++;
                    }

                    target_row_idx++;
                }
            }
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalType = WaterNet.WV_Common.enum1.INTERVAL_TYPE.DATE;
            this.searchBox1.SmallBlockContainer.Visible = false;
            this.searchBox1.MiddleBlockObject.SelectedIndex = 0;
            this.searchBox1.SmallBlockObject.SelectedIndex = 0;

            //시작일자 수정불가,
            //this.searchBox1.StartDateObject.Enabled = false;
            this.searchBox1.StartDateObject.Value = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
            this.searchBox1.EndDateObject.Value = DateTime.Now.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 검색종료 일자를 기준으로 시작일자를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EndDateObject_ValueChanged(object sender, EventArgs e)
        {
            DateTime endDate = (DateTime)this.searchBox1.EndDateObject.Value;
            this.searchBox1.StartDateObject.Value = endDate.AddDays(-30).ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 엑셀버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelBtn_Click(object sender, EventArgs e)
        {
            //this.excelManager.Clear();

            //string resourceName = "DayMNF_" + EMFrame.statics.AppStatic.USER_SGCCD;
            //object obj = Properties.Resources.ResourceManager.GetObject(resourceName, Properties.Resources.Culture);
            //if (obj == null)
            //{
            //    MessageBox.Show("보고서 양식이 등록되어 있지 않습니다.");
            //    return;
            //}
            //this.excelManager.Load(new MemoryStream(((byte[])(obj))), WORKBOOK_TYPE.TEMPLATE);
            //this.excelManager.AddWorksheet(this.ultraGrid1, "temp");
            //this.excelManager.WriteContentToTemplete(0, 0, 6, 0);
            //string value = "조회 날자 " + ((DateTime)this.searchBox1.EndDateObject.Value).ToString("[yyyy/MM/dd]");
            //this.excelManager.WriteLine(0, 2, 0, value, WORKBOOK_TYPE.TEMPLATE);
            //this.excelManager.Open("31일야간최소유량", WORKBOOK_TYPE.TEMPLATE);
            Cursor cursor = this.Cursor;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ExpoterExcel();
            }
            catch (Exception e1)
            {
                Logger.Error(e1);
            }
            finally
            {
                this.Cursor = cursor;
            }
        }

        private void ExpoterExcel()
        {
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.WorkbookBeforeClose += new Microsoft.Office.Interop.Excel.AppEvents_WorkbookBeforeCloseEventHandler(xlApp_WorkbookBeforeClose);

            string midValue = searchBox1.MiddleBlockObject.Text;

            if (xlApp == null)
            {
                return;
            }

            Microsoft.Office.Interop.Excel.Workbooks workbooks = xlApp.Workbooks;
            Microsoft.Office.Interop.Excel.Workbook workbook = workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];
            Microsoft.Office.Interop.Excel.PageSetup print = worksheet.PageSetup;   //프린트 설정
            Microsoft.Office.Interop.Excel.Range range = xlApp.get_Range("B2", "AL2");  //병합
            Microsoft.Office.Interop.Excel.Range rangeCell;

            xlApp.ScreenUpdating = false;

            DataSet dataSet = this.ultraGrid1.DataSource as DataSet;

            //-----------엑셀 제어
            int columnCount = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count;
            int rowCount = this.ultraGrid1.Rows.Count;

            //틀 고정
            worksheet.Application.ActiveWindow.SplitRow = 6;
            worksheet.Application.ActiveWindow.SplitColumn = 1;
            worksheet.Application.ActiveWindow.FreezePanes = true;

            //타이틀 제목 병합
            rangeCell = worksheet.get_Range(worksheet.Cells[2, 2], worksheet.Cells[2, columnCount - 1]);
            rangeCell.Font.Size = 30;
            rangeCell.Font.Bold = true;
            
            rangeCell.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
            rangeCell.Font.Name = "돋음";
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데 정렬
            rangeCell.Merge(true);
            //구분 병합
            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, 1]);
            rangeCell.ColumnWidth = 9;
            rangeCell.Merge(false);

            // 전체 범위 설정
            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[rowCount + 14, columnCount]);
            rangeCell.Font.Name = "돋움";
            rangeCell.RowHeight = 27;
            // 전체 범위안쪽 테두리 설정
            rangeCell.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            rangeCell.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데정렬

            // 전체 범위 바깥쪽 테두리설정
            rangeCell.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin,
                Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic);

            //데이터 범위설정
            rangeCell = worksheet.get_Range(worksheet.Cells[7, 2], worksheet.Cells[rowCount + 14, columnCount]);
            rangeCell.NumberFormat = @"#,###,##0.00"; //숫자 범주
            rangeCell.ColumnWidth = 6.44; //컬럼 넓이
            rangeCell = worksheet.get_Range(worksheet.Cells[7, 1], worksheet.Cells[this.ultraGrid1.Rows.Count + 6, 1]);
            rangeCell.NumberFormat = @"mm월dd일"; //날짜 범주

            //데이터 오른쪽정렬
            rangeCell = worksheet.get_Range(worksheet.Cells[7, 2], worksheet.Cells[rowCount + 14, columnCount - 1]);
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlRight;

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, columnCount]);
            rangeCell.RowHeight = 13.5;   //구분 로우 높이
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 1], worksheet.Cells[4, columnCount]);
            rangeCell.RowHeight = 0;

            //프린트 여백
            print.LeftMargin = 0;
            print.RightMargin = 0;
            print.TopMargin = 0;
            print.BottomMargin = 0;

            print.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;   //프린트 가로로 출력
            print.Zoom = AutoSize;    //프린트 비율
            print.CenterVertically = false;  //세로 자동맞춤
            print.CenterHorizontally = true;    //가로 자동맞춤

            range.Cells[4, 0] = "구 분";
            range.Cells[this.ultraGrid1.Rows.Count + 6, 0] = "합계";
            range.Cells[this.ultraGrid1.Rows.Count + 6 + 1, 0] = "최대값";
            range.Cells[this.ultraGrid1.Rows.Count + 6 + 2, 0] = "최소값";
            range.Cells[this.ultraGrid1.Rows.Count + 6 + 3, 0] = "평균";
            //range.Cells[this.ultraGrid1.Rows.Count + 6 + 4, 0] = "4주평균";
            //range.Cells[this.ultraGrid1.Rows.Count + 6 + 5, 0] = "3주평균";
            //range.Cells[this.ultraGrid1.Rows.Count + 6 + 6, 0] = "2주평균";
            //range.Cells[this.ultraGrid1.Rows.Count + 6 + 7, 0] = "1주평균";

            range.Cells[2, 0] = "조회 날짜 " + ((DateTime)searchBox1.StartDateObject.Value).ToString("[yyyy/MM/dd]") + " ~ " + ((DateTime)searchBox1.EndDateObject.Value).ToString("[yyyy/MM/dd]");
            range.Cells[1, 1] = "31일간 야간최소유량 조회";
            //--------------------------    

            //중, 소블록 생성 및 병합
            DataTable dataTableMD = dataSet.Tables["MIDDLE_INFO"];
            int middle_count = 0;
            int startMerge = 1;
            foreach (DataRow row in dataTableMD.Rows)
            {
                int column_idx = dataTableMD.Rows.IndexOf(row) + 2;

                if (column_idx != 2 &&
                    row["MIDDLE_NAME"].ToString() != dataTableMD.Rows[dataTableMD.Rows.IndexOf(row) - 1]["MIDDLE_NAME"].ToString())
                {
                    startMerge++;
                    rangeCell = worksheet.get_Range(worksheet.Cells[5, startMerge], worksheet.Cells[5, column_idx + middle_count]);
                    rangeCell.Merge(true);
                    startMerge = column_idx + middle_count;
                    middle_count++;
                    worksheet.Cells[5, column_idx + middle_count] = row["MIDDLE_NAME"].ToString();
                }
                else if (column_idx == 2)
                {
                    worksheet.Cells[5, column_idx] = row["MIDDLE_NAME"].ToString();
                }
                worksheet.Cells[6, column_idx + middle_count] = row["SMALL_NAME"].ToString();

            }
            startMerge++;
            rangeCell = worksheet.get_Range(worksheet.Cells[5, startMerge], worksheet.Cells[5, columnCount - 1]);
            rangeCell.Merge(true);

            //합계,최대값,최소값,평균,
            int iii = 0;
            foreach (UltraGridGroup child in this.ultraGrid1.DisplayLayout.Bands[0].Groups)
            {
                iii++;
                string strChild = child.Header.Caption;

                if (strChild == "소계")
                {
                    continue;
                }
                else
                {
                    worksheet.Cells[6, iii] = child.Header.Caption;
                    try
                    {
                        worksheet.Cells[this.ultraGrid1.Rows.Count+ 6 + 1, iii] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_SUM"].Value.ToString(); //합계
                        worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 2, iii] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_MAX"].Value.ToString(); //최대값
                        worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 3, iii] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_MIN"].Value.ToString(); //최소값
                        worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 4, iii] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_AVERAGE"].Value.ToString(); //평균
                        //worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 5, iii] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_4WEEK"].Value.ToString(); //4주평균
                        //worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 6, iii] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_3WEEK"].Value.ToString(); //3주평균
                        //worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 7, iii] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_2WEEK"].Value.ToString(); //2주평균
                        //worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 8, iii] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_1WEEK"].Value.ToString(); //1주평균
                    }
                    catch (Exception ee)
                    {
                        worksheet.Cells[38, iii + 1] = null;
                    }
                }
            }
            

            //31일 야간 데이터 출력
            int row_index = 0;
            int column_index = 0;
            object columnValue = null;
            for (int rowIndex = row_index; rowIndex < this.ultraGrid1.Rows.Count; rowIndex++)
            {
                column_index = 0;
                UltraGridColumn columnIndex = this.ultraGrid1.DisplayLayout.Bands[0].Columns[0];
                columnIndex = columnIndex.GetRelatedVisibleColumn(VisibleRelation.First);

                while (null != columnIndex)
                {
                    if (columnIndex != null)
                    {
                        UltraGridRow row = this.ultraGrid1.DisplayLayout.Rows[rowIndex];
                        columnValue = row.GetExportValue(columnIndex);

                        range.Cells[rowIndex + 6, column_index] = columnValue;  //셀 삽입

                        if (columnIndex.Header.Caption.Contains("SUM"))
                        {
                            range.Cells[5, column_index] = "소계";

                            //소계 배경색
                            rangeCell = worksheet.get_Range(worksheet.Cells[7, column_index + 1], worksheet.Cells[this.ultraGrid1.Rows.Count + 6, column_index + 1]);
                            rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(204, 255, 255));
                        }
                        else if (columnIndex.Header.Caption.Contains("총 계"))
                        {
                            range.Cells[4, column_index] = "총 계";
                            //총계 병합
                            rangeCell = worksheet.get_Range(worksheet.Cells[5, column_index + 1], worksheet.Cells[6, column_index + 1]);
                            rangeCell.Merge(false);
                        }
                    }
                    columnIndex = columnIndex.GetRelatedVisibleColumn(VisibleRelation.Next);  //다음 컬럼으로 position이동
                    column_index++;
                }
             
                //중,소블록 컬럼범위, 배경색
                rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, columnCount]);
                rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(204, 255, 204));

                //총계 범위, 배경색
                //range.Cells[4, endMerge] = "총계";
                rangeCell = worksheet.get_Range(worksheet.Cells[7, columnCount], worksheet.Cells[this.ultraGrid1.Rows.Count + 6, columnCount]);
                rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(255, 255, 153));

                //합계 범위, 배경색
                rangeCell = worksheet.get_Range(worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 1, 1], worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 1, columnCount]);
                rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(255, 255, 153));
            }
            
            xlApp.ScreenUpdating = true;
            xlApp.Visible = true;
        }

        private void xlApp_WorkbookBeforeClose(Microsoft.Office.Interop.Excel.Workbook Wb, ref bool Cancel)
        {
            Process[] ExcelPros = Process.GetProcessesByName("EXCEL");

            for (int i = 0; i < ExcelPros.Length; i++)
            {
                Console.WriteLine(ExcelPros[i].MainWindowTitle);

                if (ExcelPros[i].MainWindowTitle == "")
                {
                    ExcelPros[i].Kill();
                }
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                Cursor currentCursor = this.Cursor;
                this.Cursor = Cursors.WaitCursor;

                try
                {
                    foreach (UltraGridRow row in this.ultraGrid1.Rows)
                    {
                        foreach (UltraGridCell cell in row.Cells)
                        {
                            if (cell.Appearance.BackColor == Color.SkyBlue)
                            {
                                Hashtable parameter = new Hashtable();
                                parameter["TAG_GBN"] = "MNF";
                                parameter["LOC_CODE"] = cell.Column.Key;
                                parameter["DATEE"] = Convert.ToDateTime(cell.Row.Cells["DATEE"].Value).ToString("yyyyMMdd");
                                parameter["VALUE"] = cell.Value;

                                ReportWork.GetInstance().UpdateValueEdit(parameter);

                                string work_desc = cell.Column.RowLayoutColumnInfo.ParentGroup.Header.Caption + " " + Convert.ToDateTime(cell.Row.Cells["DATEE"].Value).ToString("yyyy-MM-dd") + "일 야간최소유량 변경 (" + cell.ToolTipText.Replace("[보정]\n", "") + ")";

                                WaterNetCore.UserWorkHistoryLog.UPDATE(
                                    "일야간최소유량ToolStripMenuItem",
                                    work_desc
                                );
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    this.Cursor = currentCursor;
                }

                this.searchBtn.PerformClick();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
        }


        /// <summary>
        /// 검색버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectDayMNF(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 야간최소유량을 검색한다.
        /// </summary>
        /// <param name="dataSet"></param>
        private void SelectDayMNF(Hashtable parameter)
        {
            this.isUserCellUpdate = false;
            this.ultraGrid1.DataSource = ReportWork.GetInstance().SelectDayMNF(parameter);

            this.InitGrid();
            this.SetGridColumns();
            this.SetGridData();
            this.SetSummaryRows();
            this.isUserCellUpdate = true;
        }

        /// <summary>
        /// 그리드내 그룹 컬럼을 설정한다.
        /// </summary>
        private void SetGridColumns()
        {
            DataSet dataSet = (DataSet)this.ultraGrid1.DataSource;

            if (dataSet.Tables.IndexOf("MIDDLE_INFO") == -1 || dataSet.Tables["MIDDLE_INFO"].Rows.Count == 0)
            {
                return;
            }

            bool isFirst = true;

            foreach (DataRow dataRow in dataSet.Tables["MIDDLE_INFO"].Rows)
            {
                UltraGridGroup parent = null;
                UltraGridGroup group = null;

                //중블록 그룹 생성
                //해당 중블록 컬럼의 존재 유무 체크,
                if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(dataRow["MBLOCK"].ToString()) == -1)
                {
                    parent = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                    parent.Key = dataRow["MBLOCK"].ToString();
                    parent.Header.Caption = dataRow["MIDDLE_NAME"].ToString();
                    parent.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    parent.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    parent.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                    parent.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                    parent.RowLayoutGroupInfo.LabelSpan = 1;
                    parent.RowLayoutGroupInfo.SpanX = 1;
                    parent.RowLayoutGroupInfo.SpanY = 1;

                    if (isFirst)
                    {
                        parent.RowLayoutGroupInfo.OriginX = 1;
                        isFirst = false;
                    }
                }
                else if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(dataRow["MBLOCK"].ToString()) != -1)
                {
                    parent = this.ultraGrid1.DisplayLayout.Bands[0].Groups[dataRow["MBLOCK"].ToString()];
                }

                //소블록 그룹 생성
                if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(dataRow["SBLOCK"].ToString()) == -1)
                {
                    group = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                    group.Key = dataRow["SBLOCK"].ToString();
                    group.Header.Caption = dataRow["SMALL_NAME"].ToString();
                    group.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    group.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    group.RowLayoutGroupInfo.ParentGroup = parent;
                    group.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                    group.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                    group.RowLayoutGroupInfo.LabelSpan = 1;
                    group.RowLayoutGroupInfo.SpanX = 1;
                    group.RowLayoutGroupInfo.SpanY = 1;

                    UltraGridColumn column = this.GetReportColumn();
                    column.Key = dataRow["SBLOCK"].ToString();
                    column.Width = 70;
                    column.RowLayoutColumnInfo.ParentGroup = group;

                    object o = EMFrame.statics.AppStatic.USER_MENU["일야간최소유량ToolStripMenuItem"];

                    if (o.ToString().Equals("0"))
                    {
                        column.CellClickAction = CellClickAction.Edit;
                        column.CellActivation = Activation.AllowEdit;
                    }
                }
            }

            foreach (DataRow dataRow in dataSet.Tables["MIDDLE_INFO"].Rows)
            {
                UltraGridGroup parent = null;
                UltraGridGroup group = null;

                if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(dataRow["MBLOCK"].ToString()) != -1)
                {
                    parent = this.ultraGrid1.DisplayLayout.Bands[0].Groups[dataRow["MBLOCK"].ToString()];
                }

                //중블록 소계 그룹 생성
                //중블록 그룹 생성
                //해당 중블록 컬럼의 존재 유무 체크,
                if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(dataRow["MBLOCK"].ToString() + "_SUM") == -1)
                {
                    group = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                    group.Key = dataRow["MBLOCK"].ToString() + "_SUM";
                    group.Header.Caption = "소계";
                    group.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    group.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    group.RowLayoutGroupInfo.ParentGroup = parent;
                    group.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                    group.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                    group.RowLayoutGroupInfo.LabelSpan = 1;
                    group.RowLayoutGroupInfo.SpanX = 1;
                    group.RowLayoutGroupInfo.SpanY = 1;

                    UltraGridColumn column = this.GetReportColumn();
                    column.Key = dataRow["MBLOCK"].ToString() + "_SUM";
                    column.Width = 70;
                    column.RowLayoutColumnInfo.ParentGroup = group;
                    column.CellAppearance.BackColor = SystemColors.Info;

                    //211.3.31
                    column.Format = "###,###,###";
                }
            }
            this.ultraGrid1.DisplayLayout.Bands[0].Columns[1].RowLayoutColumnInfo.OriginX = 
                this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count;
        }

        /// <summary>
        /// 그리드내 데이터 컬럼을 설정한다.
        /// </summary>
        private void SetGridData()
        {
            DataSet dataSet = (DataSet)this.ultraGrid1.DataSource;

            if (dataSet.Tables.IndexOf("SMALL_INFO") == -1 || dataSet.Tables["SMALL_INFO"].Rows.Count == 0)
            {
                return;
            }

            foreach (DataRow dataRow in dataSet.Tables["SMALL_INFO"].Rows)
            {
                foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
                {
                    if (Convert.ToDateTime(dataRow["DATEE"]).ToString("yyyyMMdd") ==
                        Convert.ToDateTime(gridRow.Cells["DATEE"].Value).ToString("yyyyMMdd"))
                    {
                        //소계값인경우
                        if (dataRow["SBLOCK"].ToString() == dataRow["MBLOCK"].ToString() + "_SUM")
                        {
                            gridRow.Cells[dataRow["SBLOCK"].ToString()].Value = dataRow["MNF"];
                        }

                        //소계값이 아닌경우
                        else if (dataRow["SBLOCK"].ToString() != dataRow["MBLOCK"].ToString() + "_SUM")
                        {
                            double mnf = Utils.ToDouble(dataRow["MNF"]);
                            double filtering = Utils.ToDouble(dataRow["FILTERING"]);

                            bool mnf_n = true;
                            bool filtering_n = true;

                            if (dataRow["MNF"] != DBNull.Value)
                            {
                                mnf_n = false;
                            }

                            if (dataRow["FILTERING"] != DBNull.Value)
                            {
                                filtering_n = false;
                            }

                            //데이터누락(mnf값없음, filtering값없음)
                            //일반(mnf값있음. filtering값없음)
                            //보정(데이터누락 일반 아닌경우)
                            if (mnf_n && filtering_n)
                            {
                                gridRow.Cells[dataRow["SBLOCK"].ToString()].ToolTipText = "[결측]";
                                gridRow.Cells[dataRow["SBLOCK"].ToString()].Appearance.BackColor = Color.Red;
                            }
                            else if (!mnf_n && !filtering_n && mnf == filtering)
                            {
                                gridRow.Cells[dataRow["SBLOCK"].ToString()].ToolTipText = dataRow["MNF"].ToString();
                                gridRow.Cells[dataRow["SBLOCK"].ToString()].Value = dataRow["MNF"];
                            }
                            else
                            {
                                if (mnf_n && !filtering_n)
                                {
                                    gridRow.Cells[dataRow["SBLOCK"].ToString()].ToolTipText = "[보정]\n원본값 : 결측" + " --> " + "보정값 : " + filtering.ToString();
                                }
                                else if (!mnf_n && filtering_n)
                                {
                                    gridRow.Cells[dataRow["SBLOCK"].ToString()].ToolTipText = "[보정]\n원본값 : " + mnf.ToString() + " --> " + "보정값 : 결측";
                                }
                                else
                                {
                                    gridRow.Cells[dataRow["SBLOCK"].ToString()].ToolTipText = "[보정]\n원본값 : " + mnf.ToString() + " --> " + "보정값 : " + filtering.ToString();

                                }
                                gridRow.Cells[dataRow["SBLOCK"].ToString()].Value = dataRow["FILTERING"];
                                gridRow.Cells[dataRow["SBLOCK"].ToString()].Appearance.BackColor = Color.SkyBlue;
                            }
                        }
                    }
                }
            }

            //그리드를 돌며 소계인경우 특정색
            //그리드를 돌며 데이터가 없는경우 누락표시

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                foreach (UltraGridCell gridCell in gridRow.Cells)
                {
                    if (gridCell.Text == string.Empty && gridCell.Column.CellAppearance.BackColor == Color.Empty)
                    {
                        gridCell.Appearance.BackColor = Color.Red;
                        gridCell.ToolTipText = "[결측]";
                    }
                }
            }
        }

        private void SetSummaryRows()
        {
            foreach (UltraGridColumn gridColumn in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
            {
                SummarySettings summary = null;

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                    summary.DisplayFormat = "{0:###,##0.00}";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    summary.Key = gridColumn.Key + "_SUM";

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Maximum, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                    summary.DisplayFormat = "{0:###,##0.00}";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    summary.Key = gridColumn.Key + "_MAX";

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Minimum, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                    summary.DisplayFormat = "{0:###,##0.00}";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    summary.Key = gridColumn.Key + "_MIN";

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new MonthAverage(), gridColumn, SummaryPosition.UseSummaryPositionColumn, gridColumn);
                    summary.DisplayFormat = "{0:###,##0.00}";
                    summary.Appearance.BackColor = SystemColors.Control;
                    summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    summary.Key = gridColumn.Key + "_AVERAGE";

                    //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Week4Average(), gridColumn, SummaryPosition.UseSummaryPositionColumn, gridColumn);
                    //summary.DisplayFormat = "{0:###,##0.00}";
                    //summary.Appearance.BackColor = SystemColors.Control;
                    //summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    //summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    //summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    //summary.Key = gridColumn.Key + "_4WEEK";

                    //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Week3Average(), gridColumn, SummaryPosition.UseSummaryPositionColumn, gridColumn);
                    //summary.DisplayFormat = "{0:###,##0.00}";
                    //summary.Appearance.BackColor = SystemColors.Control;
                    //summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    //summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    //summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    //summary.Key = gridColumn.Key + "_3WEEK";

                    //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Week2Average(), gridColumn, SummaryPosition.UseSummaryPositionColumn, gridColumn);
                    //summary.DisplayFormat = "{0:###,##0.00}";
                    //summary.Appearance.BackColor = SystemColors.Control;
                    //summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    //summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    //summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    //summary.Key = gridColumn.Key + "_2WEEK";

                    //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Custom, new Week1Average(), gridColumn, SummaryPosition.UseSummaryPositionColumn, gridColumn);
                    //summary.DisplayFormat = "{0:###,##0.00}";
                    //summary.Appearance.BackColor = SystemColors.Control;
                    //summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                    //summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    //summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    //summary.Key = gridColumn.Key + "_1WEEK";

                if (gridColumn.CellAppearance.BackColor != Color.Empty)
                {
                    if (gridColumn.Index == 0)
                    {
                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_SUM"];
                        summary.DisplayFormat = "합계";
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MAX"];
                        summary.DisplayFormat = "최대값";
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MIN"];
                        summary.DisplayFormat = "최소값";
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_AVERAGE"];
                        summary.DisplayFormat = "평균";
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_4WEEK"];
                        //summary.DisplayFormat = "4주평균";
                        //summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        //summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_3WEEK"];
                        //summary.DisplayFormat = "3주평균";
                        //summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        //summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_2WEEK"];
                        //summary.DisplayFormat = "2주평균";
                        //summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        //summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_1WEEK"];
                        //summary.DisplayFormat = "1주평균";
                        //summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        //summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    }
                    else
                    {
                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_SUM"];
                        summary.DisplayFormat = " ";

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MAX"];
                        summary.DisplayFormat = " ";

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MIN"];
                        summary.DisplayFormat = " ";

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_AVERAGE"];
                        summary.DisplayFormat = " ";

                        //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_4WEEK"];
                        //summary.DisplayFormat = " ";

                        //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_3WEEK"];
                        //summary.DisplayFormat = " ";

                        //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_2WEEK"];
                        //summary.DisplayFormat = " ";

                        //summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_1WEEK"];
                        //summary.DisplayFormat = " ";
                    }
                }
            }
        }

        /// <summary>
        /// 동적으로 생성한 그룹을 삭제한다.
        /// </summary>
        private void InitGrid()
        {
            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Count; i > 0; i--)
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Groups.Remove(i - 1);
            }

            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count; i > 0; i--)
            {
                if (!this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].IsBound)
                {
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns.Remove(i - 1);
                }
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Clear();
        }


        private UltraGridColumn GetReportColumn()
        {
            UltraGridColumn gridColumn = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            gridColumn.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
            gridColumn.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            gridColumn.RowLayoutColumnInfo.LabelPosition = LabelPosition.None;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            gridColumn.RowLayoutColumnInfo.SpanX = 1;
            gridColumn.RowLayoutColumnInfo.SpanY = 1;
            gridColumn.RowLayoutColumnInfo.LabelSpan = 1;
            gridColumn.DataType = typeof(double);

            gridColumn.Format = "###,###,##0.00";
            gridColumn.DefaultCellValue = DBNull.Value;
            return gridColumn;
        }
    }   
}