﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WaterNetCore;
using System.Collections;
using WaterNet.WV_Common.util;
using WaterNet.WV_Report.work;
using System.IO;
using WaterNet.WV_Common.enum1;
using System.Collections.Generic;
using EMFrame.log;
using System.Diagnostics;

namespace WaterNet.WV_Report.form
{
    public partial class frmSystemDailyReport : Form, WaterNet.WaterNetCore.IForminterface
    {

        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        public frmSystemDailyReport()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmSystemDailyReport_Load);
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        public void Open()
        {

        }
        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSystemDailyReport_Load(object sender, EventArgs e)
        {
            //동진_2012.08.23
            object o = EMFrame.statics.AppStatic.USER_MENU["용수공급일보ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }

            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeForm();

            //폼 로드 자동검색.
            this.searchBtn_Click(this.searchBtn, new EventArgs());
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);

            this.ultraGrid1.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            //다중셀 드래그 선택가능하게.
            this.ultraGrid1.DisplayLayout.Override.SelectTypeCell = SelectType.ExtendedAutoDrag;
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);

            this.ultraGrid1.DoubleClickHeader += new DoubleClickHeaderEventHandler(ultraGrid1_DoubleClickHeader);

            //셀수정이벤트,
            //this.ultraGrid1.DoubleClickCell += new DoubleClickCellEventHandler(ultraGrid1_DoubleClickCell);
            this.ultraGrid1.AfterCellUpdate += new CellEventHandler(ultraGrid1_AfterCellUpdate);


            //셀 드레그를 좌우로 못하게,
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);

            //셀수정 Context
            this.ultraGrid1.MouseDown += new MouseEventHandler(ultraGrid1_MouseDown);

            //셀입력모드로 들어오기전에 태그 갯수를 조회해야한다.
            this.ultraGrid1.BeforeEnterEditMode += new CancelEventHandler(ultraGrid1_BeforeEnterEditMode);
            
            this.ultraGrid1.AfterExitEditMode += new EventHandler(ultraGrid1_AfterExitEditMode);

            this.ultraGrid1.KeyDown += new KeyEventHandler(ultraGrid1_KeyDown);
        }

        /// <summary>
        /// 더블클릭 태그찾기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ultraGrid1_DoubleClickHeader(object sender, DoubleClickHeaderEventArgs e)
        {
            try
            {
                string loc_code = string.Empty;
                string loc_gbn = string.Empty;
                if (e.Header.Column == null && e.Header.Group != null)
                {
                    string[] split = e.Header.Group.Key.Split('_');
                    loc_code = split[0];
                    if (split.Length == 3)
                    {
                        loc_gbn = split[1] + "_" + split[2];    
                    }
                    
                }
                else
                {
                    string[] split = e.Header.Column.Key.Split('_');
                    loc_code = split[0];
                    if (split.Length == 3)
                    {
                        loc_gbn = split[1] + "_" + split[2];
                    }
                }
                if (string.IsNullOrEmpty(loc_code) || loc_code.IndexOf("SUM") > -1) return;

                WV_Common.form.frmTagDescription form = new WaterNet.WV_Common.form.frmTagDescription(loc_code, string.IsNullOrEmpty(loc_gbn) ? "FRQ" : loc_gbn);
                form.Show();
            }
            catch { }
        }

        private MouseButtons pressedButton = MouseButtons.None;

        //셀수정 우클릭 메뉴 생성
        private void ultraGrid1_MouseDown(object sender, MouseEventArgs e)
        {
            this.pressedButton = e.Button;

            if (e.Button != MouseButtons.Right)
            {
                return;
            }

            if (this.ultraGrid1.Selected.Cells.Count <= 1)
            {
                return;
            }

            object o = EMFrame.statics.AppStatic.USER_MENU["용수공급일보ToolStripMenuItem"];

            if (!o.ToString().Equals("0"))
            {
                return;
            }

            if (this.ultraGrid1.Selected.Cells[0].Column.CellAppearance.BackColor != Color.Empty)
            {
                return;
            }

            ContextMenu contextMenu = new ContextMenu();
            contextMenu.MenuItems.Add("일괄수정", menuUtems1_Click);

            contextMenu.Show(this.ultraGrid1, new Point(e.X, e.Y));
        }

        //일괄수정일 경우 수정 팝업창을 생성한다.
        private void menuUtems1_Click(object sender, EventArgs e)
        {
            Hashtable parameter = new Hashtable();

            parameter["DATEE"] = Convert.ToDateTime(this.ultraGrid1.Selected.Cells[0].Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");

            string[] split = this.ultraGrid1.Selected.Cells[0].Column.Key.Split('_');

            parameter["LOC_CODE"] = split[0];
            parameter["TAG_GBN"] = split[1]+"_"+split[2];

            UltraGridCell v_cell = this.ultraGrid1.Selected.Cells[0].Row.Cells["V_TIME"];
            UltraGridCell e_cell = this.ultraGrid1.Selected.Cells[this.ultraGrid1.Selected.Cells.Count - 1].Row.Cells["V_TIME"];

            //시작
            //double v_time = Utils.ToDouble(v_cell.Value.ToString().Substring(0, 2));
            long v_time = ((DateTime)v_cell.Value).Ticks;

            //종료
            //double e_time = Utils.ToDouble(e_cell.Value.ToString().Substring(0, 2));
            long e_time = ((DateTime)e_cell.Value).Ticks;


            if (v_time == e_time)
            {
                //parameter["V_TIME"] = v_cell.Value.ToString();
                //parameter["E_TIME"] = v_cell.Value.ToString();
                parameter["V_TIME"] = (DateTime)v_cell.Value;
                parameter["E_TIME"] = (DateTime)v_cell.Value;

                parameter["STARTDATE"] = Convert.ToDateTime(v_cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
                parameter["ENDDATE"] = Convert.ToDateTime(v_cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
            }

            if (v_time < e_time)
            {
                //parameter["V_TIME"] = v_cell.Value.ToString();
                //parameter["E_TIME"] = e_cell.Value.ToString();
                parameter["V_TIME"] = (DateTime)v_cell.Value;
                parameter["E_TIME"] = (DateTime)e_cell.Value;

                parameter["STARTDATE"] = Convert.ToDateTime(v_cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
                parameter["ENDDATE"] = Convert.ToDateTime(e_cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
            }
            if (v_time > e_time)
            {
                //parameter["V_TIME"] = e_cell.Value.ToString();
                //parameter["E_TIME"] = v_cell.Value.ToString();
                parameter["V_TIME"] = (DateTime)e_cell.Value;
                parameter["E_TIME"] = (DateTime)v_cell.Value;

                parameter["STARTDATE"] = Convert.ToDateTime(e_cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
                parameter["ENDDATE"] = Convert.ToDateTime(v_cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
            }

            frmValueEditPopup form = new frmValueEditPopup();
            form.SelectValueEdit(parameter, this.ultraGrid1.Selected.Cells);
            form.ShowDialog();
        }

        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (e.Type.FullName != "Infragistics.Win.UltraWinGrid.UltraGridCell")
            {
                return;
            }

            //첫 선택이면 선택으로 넘긴다.
            if (this.ultraGrid1.Selected.Cells.Count <= 1)
            {
                return;
            }

            UltraGridCell firstCell = this.ultraGrid1.Selected.Cells[0];
            UltraGridCell eventCell = this.ultraGrid1.Selected.Cells[this.ultraGrid1.Selected.Cells.Count - 1];


            if (firstCell.Column.Index != eventCell.Column.Index)
            {
                eventCell.Selected = false;
                eventCell.Activated = false;
            }
            else if (firstCell.Column.Index == eventCell.Column.Index)
            {
                eventCell.Activated = false;
            }
        }

        //셀을 더블클릭하면..
        //태그를 찾아서 다이얼을 띄우고 날짜와 지역코드와 태그구분을 넘겨준다.
        private void ultraGrid1_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            if (this.pressedButton != MouseButtons.Right)
            {
                return;
            }

            if (e.Cell.Column.CellAppearance.BackColor != Color.Empty)
            {
                return;
            }

            object o = EMFrame.statics.AppStatic.USER_MENU["용수공급일보ToolStripMenuItem"];

            if (!o.ToString().Equals("0"))
            {
                return;
            }

            e.Cell.Activate();

            Hashtable parameter = new Hashtable();
            parameter["DATEE"] = Convert.ToDateTime(e.Cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");

            string[] split = e.Cell.Column.Key.Split('_');

            parameter["LOC_CODE"] = split[0];
            parameter["TAG_GBN"] = split[1] + "_" + split[2];

            parameter["V_TIME"] = e.Cell.Row.Cells["V_TIME"].Value.ToString();
            parameter["E_TIME"] = e.Cell.Row.Cells["V_TIME"].Value.ToString();

            frmValueEditPopup form = new frmValueEditPopup();
            form.SelectValueEdit(parameter, e.Cell);
            form.ShowDialog();
        }

        private bool isUserCellEdit = false;
        private bool isUserCellUpdate = false;
        private void ultraGrid1_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (!isUserCellUpdate ||
                this.ultraGrid1.DataSource == null ||
                e.Cell.Column.CellAppearance.BackColor != Color.Empty)
            {
                return;
            }

            double origin = 0;
            double flow = Utils.ToDouble(e.Cell.Value);

            //2011.3.31
            bool origin_n = true;
            bool flow_n = true;

            //원데이터와 수정데이터를 비교
            DataSet dataSet = (DataSet)this.ultraGrid1.DataSource;

            foreach (DataRow dataRow in dataSet.Tables["SMALL_INFO"].Rows)
            {
                if (Convert.ToDateTime(dataRow["DATEE"]).ToString("yyyyMMddHHmm") ==
                    Convert.ToDateTime(e.Cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm"))
                {
                    if (dataRow["SBLOCK"].ToString() == e.Cell.Column.Key)
                    {
                        //2011.3.31
                        if (dataRow["FLOW"] != DBNull.Value)
                        {
                            origin_n = false;
                        }
                        origin = Utils.ToDouble(dataRow["FLOW"]);
                    }
                }
            }

            //2011.3.31
            if (e.Cell.Value != DBNull.Value)
            {
                flow_n = false;
            }

            //2011.3.31
            if (flow_n && origin == 0 && flow == 0)
            {
                e.Cell.ToolTipText = "[결측]";
                e.Cell.Appearance.BackColor = Color.Red;
            }
            //else if (!origin_n && !flow_n && origin != 0 && flow != 0 && origin == flow)
            else if (!origin_n && !flow_n && origin == flow)
            {
                e.Cell.ToolTipText = origin.ToString();
                e.Cell.Appearance.BackColor = Color.Empty;
            }
            else
            {
                if (!origin_n && flow_n)
                {
                    e.Cell.ToolTipText = "[보정]\n원본값 : " + origin.ToString() + " --> " + "보정값 : 결측";
                }
                else if (origin_n && !flow_n)
                {
                    e.Cell.ToolTipText = "[보정]\n원본값 : 결측" + " --> " + "보정값 : " + flow.ToString();
                }
                else
                {
                    e.Cell.ToolTipText = "[보정]\n원본값 : " + origin.ToString() + " --> " + "보정값 : " + flow.ToString();
                }

                e.Cell.Appearance.BackColor = Color.SkyBlue;
            }

            double sumVal = 0;
            string parentKey = e.Cell.Column.RowLayoutColumnInfo.ParentGroup.RowLayoutGroupInfo.ParentGroup.Key;

            foreach (UltraGridCell gridCell in e.Cell.Row.Cells)
            {
                if (gridCell.Column.CellAppearance.BackColor == Color.Empty)
                {
                    //소계
                    if (gridCell.Column.RowLayoutColumnInfo.ParentGroup.RowLayoutGroupInfo.ParentGroup.Key == parentKey)
                    {
                        sumVal += Utils.ToDouble(gridCell.Value);
                    }

                }
            }

            //if (isUserCellEdit)
            //{
            //    Hashtable parameter = new Hashtable();

            //    string[] split = e.Cell.Column.Key.Split('_');

            //    parameter["LOC_CODE"] = split[0];
            //    parameter["TAG_GBN"] = split[1] + "_" + split[2];

            //    parameter["DATEE"] = Convert.ToDateTime(e.Cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
            //    parameter["V_TIME"] = Convert.ToDateTime(e.Cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
            //    parameter["E_TIME"] = Convert.ToDateTime(e.Cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
            //    parameter["VALUE"] = e.Cell.Value;

            //    ReportWork.GetInstance().UpdateValueEdit(parameter);

            //    WaterNetCore.UserWorkHistoryLog.UPDATE(
            //    "용수공급일보ToolStripMenuItem",
            //    e.Cell.Column.RowLayoutColumnInfo.ParentGroup.RowLayoutGroupInfo.ParentGroup.Header.Caption + " " +e.Cell.Column.RowLayoutColumnInfo.ParentGroup.Header.Caption + " " + Convert.ToDateTime(e.Cell.Row.Cells["DATEE"].Value).ToString("yyyy-MM-dd") + "일 " + Convert.ToDateTime(e.Cell.Row.Cells["DATEE"].Value).ToString("HH") + "시 공급량 변경 (" + e.Cell.OriginalValue.ToString() + " => " + flow.ToString() + ")"
            //);

            //    this.ultraGrid1.PerformAction(UltraGridAction.ExitEditMode);
            //}
        }

        //태그갯수를 파악해서 태그가 1개가 아니면 수정못하게한다.
        private void ultraGrid1_BeforeEnterEditMode(object sender, CancelEventArgs e)
        {
            Hashtable parameter = new Hashtable();

            string[] split = this.ultraGrid1.ActiveCell.Column.RowLayoutColumnInfo.ParentGroup.Key.Split('_');

            parameter["LOC_CODE"] = split[0];
            parameter["TAG_GBN"] = split[1] + "_" + split[2];

            //등록된 태그 갯수 조회.
            if (ReportWork.GetInstance().SelectTagCount(parameter).ToString() != "1")
            {
                MessageBox.Show("선택한 블록내에 다수의 관련 태그가 존재합니다.\n수정할 수 없습니다.");
                e.Cancel = true;
            }

            this.isUserCellEdit = true;
        }

        private void ultraGrid1_AfterExitEditMode(object sender, EventArgs e)
        {
            this.isUserCellEdit = false;
        }

        private void ultraGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.V)
            {
                //선택된 셀이 없으면 동작하지 않는다.
                if (this.ultraGrid1.ActiveCell == null)
                {
                    return;
                }

                //클립보드의 엑셀 선택 영역을 가져온다..
                //엑셀이 아니라면 값은 null 이며 동작하지 않는다.
                string[,] range = WV_Common.excel.Clipboard.GetStringRange();
                if (range == null)
                {
                    return;
                }

                int start_row_idx = this.ultraGrid1.ActiveCell.Row.Index;
                int start_cell_idx = 0;
                int column_index = 0;
                Dictionary<int, string> columns = new Dictionary<int, string>();

                UltraGridColumn column = this.ultraGrid1.DisplayLayout.Bands[0].Columns[0];
                column = column.GetRelatedVisibleColumn(VisibleRelation.First);

                while (null != column)
                {
                    if (column != null)
                    {
                        columns[column_index] = column.Key;

                        if (this.ultraGrid1.ActiveCell.Column.Key == column.Key)
                        {
                            start_cell_idx = column_index;
                        }
                    }

                    column = column.GetRelatedVisibleColumn(VisibleRelation.Next);
                    column_index++;
                }

                int target_row_idx = 0;
                int target_cell_idx = 0;

                for (int i = 0; i < range.GetLength(0); i++)
                {
                    for (int j = 0; j < range.GetLength(1); j++)
                    {
                        double result = 0;
                        if (range[i, j] != null)
                        {
                            if (!Double.TryParse(range[i, j], out result))
                            {
                                MessageBox.Show("선택하신 영역내에 숫자가 아닌 값이 존재합니다.");
                                return;
                            }
                        }
                    }
                }

                this.ultraGrid1.PerformAction(UltraGridAction.ExitEditMode);

                for (int i = start_row_idx; i < this.ultraGrid1.Rows.Count; i++)
                {
                    target_cell_idx = 0;

                    if (target_row_idx > range.GetLength(0) - 1)
                    {
                        break;
                    }

                    for (int j = start_cell_idx; j < columns.Count; j++)
                    {
                        if (target_cell_idx > range.GetLength(1) - 1)
                        {
                            break;
                        }

                        if (this.ultraGrid1.Rows[i].Cells[columns[j]].Column.CellAppearance.BackColor == Color.Empty)
                        {
                            this.ultraGrid1.Rows[i].Cells[columns[j]].Value = range[target_row_idx, target_cell_idx];
                        }

                        target_cell_idx++;
                    }

                    target_row_idx++;
                }
            }
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalType = WaterNet.WV_Common.enum1.INTERVAL_TYPE.DATE;
            this.searchBox1.IntervalText = "기준일자";

            this.searchBox1.MiddleBlockContainer.Visible = false;
            this.searchBox1.SmallBlockContainer.Visible = false;

            this.searchBox1.MiddleBlockObject.SelectedIndex = 0;
            this.searchBox1.SmallBlockObject.SelectedIndex = 0;


            //기준일자
            this.searchBox1.StartDateObject.Value = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd");
            this.searchBox1.EndDateObject.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            //this.searchBox1.DateObject.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 엑셀버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelBtn_Click(object sender, EventArgs e)
        {
            //this.excelManager.Clear();
            //string resourceName = "SystemDailyReport_" + EMFrame.statics.AppStatic.USER_SGCCD;
            //object obj = Properties.Resources.ResourceManager.GetObject(resourceName, Properties.Resources.Culture);
            //if (obj == null)
            //{
            //    MessageBox.Show("보고서 양식이 등록되어 있지 않습니다.");
            //    return;
            //}
            //this.excelManager.Load(new MemoryStream(((byte[])(obj))), WORKBOOK_TYPE.TEMPLATE);
            //this.excelManager.AddWorksheet(this.ultraGrid1, "배수지용수공급일지");
            //this.excelManager.WriteContentToTemplete(0, 0, 4, 0);
            //string value = "조회 날자 " + ((DateTime)this.searchBox1.DateObject.Value).ToString("[yyyy/MM/dd]");
            //this.excelManager.WriteLine(0, 3, 0, value, WORKBOOK_TYPE.TEMPLATE);
            //this.excelManager.Open("배수지용수공급일지", WORKBOOK_TYPE.TEMPLATE);
            Cursor cursor = this.Cursor;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ExpoterExcel();
            }
            catch (Exception e1)
            {
                Logger.Error(e1);
            }
            finally
            {
                this.Cursor = cursor;
            }
        }

        private void ExpoterExcel()
        {
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.WorkbookBeforeClose += new Microsoft.Office.Interop.Excel.AppEvents_WorkbookBeforeCloseEventHandler(xlApp_WorkbookBeforeClose);

            string midValue = searchBox1.MiddleBlockObject.Text;

            if (xlApp == null)
            {
                return;
            }

            Microsoft.Office.Interop.Excel.Workbooks workbooks = xlApp.Workbooks;
            Microsoft.Office.Interop.Excel.Workbook workbook = workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];
            Microsoft.Office.Interop.Excel.PageSetup print = worksheet.PageSetup;   //프린트 설정
            Microsoft.Office.Interop.Excel.Range range = xlApp.get_Range("B2", "AL2");  //병합
            Microsoft.Office.Interop.Excel.Range rangeCell;

            xlApp.ScreenUpdating = false;

            DataSet dataSet = this.ultraGrid1.DataSource as DataSet;

            //-----------엑셀 제어
            int columnCount = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count;
            int rowCount = this.ultraGrid1.Rows.Count;

            //틀 고정
            worksheet.Application.ActiveWindow.SplitRow = 6;
            worksheet.Application.ActiveWindow.SplitColumn = 1;
            worksheet.Application.ActiveWindow.FreezePanes = true;

            //타이틀 제목 병합
            rangeCell = worksheet.get_Range(worksheet.Cells[2, 2], worksheet.Cells[2, columnCount - 1]);
            rangeCell.Font.Size = 18;
            rangeCell.Font.Bold = true;
            rangeCell.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
            rangeCell.Font.Name = "돋음";
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데 정렬
            rangeCell.Merge(true);

            // 전체 범위 설정
            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[rowCount + 10, columnCount - 1]);
            rangeCell.Font.Size = 10;
            rangeCell.Font.Name = "굴림체";

            // 전체 범위 바깥쪽 테두리설정
            rangeCell.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin,
                Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic);

            // 전체 범위안쪽 테두리 설정
            rangeCell.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            rangeCell.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데정렬
            rangeCell.ColumnWidth = 4.78; //구분 넓이

            //데이터 범위설정
            rangeCell = worksheet.get_Range(worksheet.Cells[7, 2], worksheet.Cells[rowCount + 10, columnCount]);
            rangeCell.NumberFormat = @"#,###,##0"; //숫자 범주
            rangeCell = worksheet.get_Range(worksheet.Cells[30, 1], worksheet.Cells[rowCount + 10, 1]);
            rangeCell.NumberFormat = @"yyyy-mm-dd h:mm"; //날짜 범주

            //데이터 오른쪽정렬
            rangeCell = worksheet.get_Range(worksheet.Cells[7, 2], worksheet.Cells[rowCount + 10, columnCount - 1]);
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlRight;
            rangeCell.ColumnWidth = 4.33; //컬럼 넓이
            rangeCell.RowHeight = 15;   //로우 높이

            rangeCell = worksheet.get_Range(worksheet.Cells[6, 1], worksheet.Cells[6, columnCount]);
            rangeCell.RowHeight = 20.25;   //로우 높이

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[5, columnCount]);
            rangeCell.RowHeight = 31.50;   //로우 높이
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 1], worksheet.Cells[4, columnCount]);
            rangeCell.RowHeight = 0;

            //프린트 여백
            print.LeftMargin = 0;
            print.RightMargin = 0;
            print.TopMargin = 0;
            print.BottomMargin = 0;

            print.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;   //프린트 가로로 출력
            print.CenterVertically = false;  //세로 자동맞춤
            print.CenterHorizontally = true;    //가로 자동맞춤
            print.Zoom = AutoSize;

            range.Cells[3 + 1, 0] = "구 분";
            range.Cells[this.ultraGrid1.Rows.Count + 6, 0] = "합계";
            range.Cells[this.ultraGrid1.Rows.Count + 6 + 1, 0] = "최대값";
            range.Cells[this.ultraGrid1.Rows.Count + 6 + 2, 0] = "최소값";
            range.Cells[this.ultraGrid1.Rows.Count + 6 + 3, 0] = "평균";

            range.Cells[2, 0] = "조회 날짜 " + ((DateTime)searchBox1.DateObject.Value).ToString("[yyyy/MM/dd]");
            range.Cells[1, 1] = "배수지 용수 공급 일지";
            //-------------------------

            //중블록 컬럼
            DataTable dataTableMD = dataSet.Tables["MIDDLE_INFO"];

            for (int i = 0; i < dataTableMD.Rows.Count; i++)
            {
                DataRow dataRow = dataTableMD.Rows[i];

                if (i == 0)
                {
                    continue;
                }
                else if (i > 0)
                {
                    DataRow dataRowPrev = dataTableMD.Rows[i - 1];

                    if (dataRow["MIDDLE_NAME"].ToString() == dataRowPrev["MIDDLE_NAME"].ToString())
                    {
                        rangeCell = worksheet.get_Range(worksheet.Cells[5, i + 1], worksheet.Cells[5, i + 2]);
                        worksheet.Cells[5, i + 1] = dataRow["MIDDLE_NAME"].ToString();
                        rangeCell.Merge(true);
                    }
                    else
                    {
                        worksheet.Cells[5, i + 2] = dataRow["MIDDLE_NAME"].ToString();
                    }
                }
            }
            //소블록 컬럼
            DataTable dataTableSM = dataSet.Tables["SMALL_INFO"];

            for (int i = 0; i < dataTableMD.Rows.Count; i++)
            {
                DataRow dataRow = dataTableMD.Rows[i];

                worksheet.Cells[6, i + 2] = dataRow["SMALL_NAME"].ToString();
                rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, i + 2]);
                rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(204, 255, 204));
                //합계
                rangeCell = worksheet.get_Range(worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 1, 1], worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 1, i + 2]);
                rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(255, 255, 153));
            }

            //합계,최대값,최소값,평균,
            int ii = 0;
            foreach (UltraGridGroup child in this.ultraGrid1.DisplayLayout.Bands[0].Groups)
            {
                ii++;
                try
                {
                    worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 1, ii + 1] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_SUM"].Value.ToString(); //합계
                    worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 2, ii + 1] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_MAX"].Value.ToString(); //최대값
                    worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 3, ii + 1] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_MIN"].Value.ToString(); //최소값
                    worksheet.Cells[this.ultraGrid1.Rows.Count + 6 + 4, ii + 1] = this.ultraGrid1.Rows.SummaryValues[child.Key + "_AVERAGE"].Value.ToString(); //평균
                }
                catch (Exception ee)
                {
                    --ii;
                    worksheet.Cells[31, ii + 2] = null;
                }
            }

            //01시 ~ 24시까지 데이터 출력
            int row_index = 0;
            int column_index = 0;
            object columnValue = null;
            for (int rowIndex = row_index; rowIndex < this.ultraGrid1.Rows.Count; rowIndex++)
            {
                column_index = 0;
                UltraGridColumn columnIndex = this.ultraGrid1.DisplayLayout.Bands[0].Columns[0];
                columnIndex = columnIndex.GetRelatedVisibleColumn(VisibleRelation.First);

                while (null != columnIndex)
                {
                    if (columnIndex != null)
                    {
                        UltraGridRow row = this.ultraGrid1.DisplayLayout.Rows[rowIndex];
                        columnValue = row.GetExportValue(columnIndex);

                        range.Cells[rowIndex + 6, column_index] = columnValue;  //셀 삽입
                    }
                    columnIndex = columnIndex.GetRelatedVisibleColumn(VisibleRelation.Next);  //다음 컬럼으로 position이동
                    column_index++;
                }
            }
            //구분 병합
            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, 1]);
            rangeCell.Merge(false);

            xlApp.ScreenUpdating = true;
            xlApp.Visible = true;
        }

        private void xlApp_WorkbookBeforeClose(Microsoft.Office.Interop.Excel.Workbook Wb, ref bool Cancel)
        {
            Process[] ExcelPros = Process.GetProcessesByName("EXCEL");

            for (int i = 0; i < ExcelPros.Length; i++)
            {
                Console.WriteLine(ExcelPros[i].MainWindowTitle);

                if (ExcelPros[i].MainWindowTitle == "")
                {
                    ExcelPros[i].Kill();
                }
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                Cursor currentCursor = this.Cursor;
                this.Cursor = Cursors.WaitCursor;

                try
                {
                    foreach (UltraGridRow row in this.ultraGrid1.Rows)
                    {
                        foreach (UltraGridCell cell in row.Cells)
                        {
                            if (cell.Appearance.BackColor == Color.SkyBlue)
                            {
                                Hashtable parameter = new Hashtable();

                                string[] split = cell.Column.Key.Split('_');

                                parameter["LOC_CODE"] = split[0];
                                parameter["TAG_GBN"] = split[1] + "_" + split[2];

                                parameter["DATEE"] = Convert.ToDateTime(cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
                                //parameter["V_TIME"] = Convert.ToDateTime(cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
                                //parameter["E_TIME"] = Convert.ToDateTime(cell.Row.Cells["DATEE"].Value).ToString("yyyyMMddHHmm");
                                parameter["V_TIME"] = Convert.ToDateTime(cell.Row.Cells["DATEE"].Value);
                                parameter["E_TIME"] = Convert.ToDateTime(cell.Row.Cells["DATEE"].Value);
                                parameter["VALUE"] = cell.Value;

                                ReportWork.GetInstance().UpdateValueEdit(parameter);

                                string work_desc = cell.Column.RowLayoutColumnInfo.ParentGroup.Header.Caption + " " + Convert.ToDateTime(cell.Row.Cells["DATEE"].Value).ToString("yyyy-MM-dd") + "일 " + Convert.ToDateTime(cell.Row.Cells["DATEE"].Value).ToString("HH") + "시 공급량 변경 (" + cell.ToolTipText.Replace("[보정]\n", "") + ")";

                                WaterNetCore.UserWorkHistoryLog.UPDATE(
                                    "용수공급일보ToolStripMenuItem",
                                    work_desc
                                );
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    this.Cursor = currentCursor;
                }

                this.searchBtn.PerformClick();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
        }

        /// <summary>
        /// 검색버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectSystemTimeSupply(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 시단위 공급량을 검색한다.
        /// </summary>
        /// <param name="dataSet"></param>
        private void SelectSystemTimeSupply(Hashtable parameter)
        {
            this.isUserCellUpdate = false;
            this.ultraGrid1.DataSource = ReportWork.GetInstance().SelectSystemDailyReport(parameter);

            this.InitGrid();
            this.SetGridColumns();
            this.SetGridData();
            this.SetSummaryRows();
            this.isUserCellUpdate = true;
        }
        /// <summary>
        /// 그리드내 그룹 컬럼을 설정한다.
        /// </summary>
        private void SetGridColumns()
        {
            DataSet dataSet = (DataSet)this.ultraGrid1.DataSource;

            if (dataSet.Tables.IndexOf("MIDDLE_INFO") == -1 || dataSet.Tables["MIDDLE_INFO"].Rows.Count == 0)
            {
                return;
            }

            bool isFirst = true;

            foreach (DataRow dataRow in dataSet.Tables["MIDDLE_INFO"].Rows)
            {
                UltraGridGroup parent = null;
                UltraGridGroup group = null;

                //중블록 그룹 생성
                //해당 중블록 컬럼의 존재 유무 체크,
                if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(dataRow["MBLOCK"].ToString()) == -1)
                {
                    parent = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                    parent.Key = dataRow["MBLOCK"].ToString();
                    parent.Header.Caption = dataRow["MIDDLE_NAME"].ToString();
                    parent.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    parent.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    parent.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                    parent.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                    parent.RowLayoutGroupInfo.LabelSpan = 1;
                    parent.RowLayoutGroupInfo.SpanX = 1;
                    parent.RowLayoutGroupInfo.SpanY = 1;

                    if (isFirst)
                    {
                        parent.RowLayoutGroupInfo.OriginX = 1;
                        isFirst = false;
                    }
                }
                else if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(dataRow["MBLOCK"].ToString()) != -1)
                {
                    parent = this.ultraGrid1.DisplayLayout.Bands[0].Groups[dataRow["MBLOCK"].ToString()];
                }

                //소블록 그룹 생성
                if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(dataRow["SBLOCK"].ToString()) == -1)
                {
                    group = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();

                    //배수지와 분기를 소블록 코드에서 분류한다.
                    group.Key = dataRow["SBLOCK"].ToString();
                    group.Header.Caption = dataRow["SMALL_NAME"].ToString();
                    group.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    group.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    group.RowLayoutGroupInfo.ParentGroup = parent;
                    group.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                    group.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                    group.RowLayoutGroupInfo.LabelSpan = 1;
                    group.RowLayoutGroupInfo.SpanX = 1;
                    group.RowLayoutGroupInfo.SpanY = 1;

                    UltraGridColumn column = this.GetReportColumn();
                    column.Key = dataRow["SBLOCK"].ToString();
                    column.Width = 130;
                    column.RowLayoutColumnInfo.ParentGroup = group;

                    object o = EMFrame.statics.AppStatic.USER_MENU["용수공급일보ToolStripMenuItem"];

                    if (o.ToString().Equals("0") && dataRow["SBLOCK"].ToString() != "2_X")
                    {
                        column.CellClickAction = CellClickAction.Edit;
                        column.CellActivation = Activation.AllowEdit;
                    }

                    //if (EMFrame.statics.AppStatic.USER_RIGHT == "1" && dataRow["SBLOCK"].ToString() != "2_X")
                    //{
                    //    column.CellClickAction = CellClickAction.Edit;
                    //    column.CellActivation = Activation.AllowEdit;
                    //}
                }
            }

            foreach (DataRow dataRow in dataSet.Tables["MIDDLE_INFO"].Rows)
            {
                UltraGridGroup parent = null;

                if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(dataRow["MBLOCK"].ToString()) != -1)
                {
                    parent = this.ultraGrid1.DisplayLayout.Bands[0].Groups[dataRow["MBLOCK"].ToString()];
                }
            }
            this.ultraGrid1.DisplayLayout.Bands[0].Columns[2].RowLayoutColumnInfo.OriginX =
                this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count;
        }

        /// <summary>
        /// 그리드내 데이터 컬럼을 설정한다.
        /// </summary>
        private void SetGridData()
        {
            DataSet dataSet = (DataSet)this.ultraGrid1.DataSource;

            if (dataSet.Tables.IndexOf("SMALL_INFO") == -1 || dataSet.Tables["SMALL_INFO"].Rows.Count == 0)
            {
                return;
            }

            foreach (DataRow dataRow in dataSet.Tables["SMALL_INFO"].Rows)
            {
                foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
                {
                    if (Convert.ToDateTime(dataRow["DATEE"]).ToString("yyyyMMddHHmm") ==
                        Convert.ToDateTime(gridRow.Cells["DATEE"].Value).ToString("yyyyMMddHHmm"))
                    {
                        //소계값인경우
                        if (dataRow["SBLOCK"].ToString() == dataRow["MBLOCK"].ToString() + "_SUM")
                        {
                            if (gridRow.Cells.IndexOf(dataRow["SBLOCK"].ToString()) != -1)
                            {
                                gridRow.Cells[dataRow["SBLOCK"].ToString()].Value = dataRow["FLOW"];
                            }
                        }

                        //소계값이 아닌경우
                        else if (dataRow["SBLOCK"].ToString() != dataRow["MBLOCK"].ToString() + "_SUM")
                        {
                            double flow = Utils.ToDouble(dataRow["FLOW"]);

                            if (gridRow.Cells.IndexOf(dataRow["SBLOCK"].ToString()) != -1)
                            {
                                gridRow.Cells[dataRow["SBLOCK"].ToString()].ToolTipText = dataRow["FLOW"].ToString();
                                gridRow.Cells[dataRow["SBLOCK"].ToString()].Value = dataRow["FLOW"];
                            }
                        }
                    }
                }
            }

            //그리드를 돌며 소계인경우 특정색
            //그리드를 돌며 데이터가 없는경우 누락표시

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                foreach (UltraGridCell gridCell in gridRow.Cells)
                {
                    if (gridCell.Text == string.Empty && gridCell.Column.CellAppearance.BackColor == Color.Empty)
                    {
                        gridCell.Appearance.BackColor = Color.Red;
                        gridCell.ToolTipText = "[결측]";
                    }
                }
            }
        }

        private void SetSummaryRows()
        {
            foreach (UltraGridColumn gridColumn in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
            {
                SummarySettings summary = null;

                summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,###,##0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = gridColumn.Key + "_SUM";

                summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Maximum, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,###,##0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = gridColumn.Key + "_MAX";

                summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Minimum, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,###,##0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = gridColumn.Key + "_MIN";

                summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Average, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,###,##0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = gridColumn.Key + "_AVERAGE";

                if (gridColumn.CellAppearance.BackColor != Color.Empty)
                {
                    if (gridColumn.Index == 1)
                    {
                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_SUM"];
                        summary.DisplayFormat = "합계";
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MAX"];
                        summary.DisplayFormat = "최대값";
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MIN"];
                        summary.DisplayFormat = "최소값";
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_AVERAGE"];
                        summary.DisplayFormat = "평균";
                        summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    }
                    else if (gridColumn.Index == 0)
                    {

                    }
                    else
                    {
                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_SUM"];
                        summary.DisplayFormat = " ";

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MAX"];
                        summary.DisplayFormat = " ";

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MIN"];
                        summary.DisplayFormat = " ";

                        summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_AVERAGE"];
                        summary.DisplayFormat = " ";
                    }
                }
            }
        }

        /// <summary>
        /// 동적으로 생성한 그룹을 삭제한다.
        /// </summary>
        private void InitGrid()
        {
            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Count; i > 0; i--)
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Groups.Remove(i - 1);
            }

            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count; i > 0; i--)
            {
                if (!this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].IsBound)
                {
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns.Remove(i - 1);
                }
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Clear();
        }


        private UltraGridColumn GetReportColumn()
        {
            UltraGridColumn gridColumn = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            gridColumn.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
            gridColumn.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            gridColumn.RowLayoutColumnInfo.LabelPosition = LabelPosition.None;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            gridColumn.RowLayoutColumnInfo.SpanX = 1;
            gridColumn.RowLayoutColumnInfo.SpanY = 1;
            gridColumn.RowLayoutColumnInfo.LabelSpan = 1;
            gridColumn.DataType = typeof(double);

            //211.3.31
            gridColumn.Format = "###,###,##0";
            gridColumn.RowLayoutColumnInfo.PreferredCellSize = new Size(80, 20);
            return gridColumn;
        }
    }
}
