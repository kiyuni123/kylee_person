﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq; //승일 추가 - List 클래스 Max 속성 사용하기 위해 선언
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;
using WaterNet.WV_Report.work;
using System.Collections;
using System.IO;
using WaterNet.WV_Common.enum1;
using WaterNet.WaterNetCore;
using EMFrame.log;
using System.Resources;
using System.Reflection;
using System.Diagnostics;

namespace WaterNet.WV_Report.form
{
    public partial class frmDailyReport : Form, WaterNet.WaterNetCore.IForminterface
    {
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();
        public int SD_MaxValue = 0; //승일 추가(소블록의 최대 갯수_Excel, _Grid사용)

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmDailyReport()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmDailyReport_Load);
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        public void Open()
        {

        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDailyReport_Load(object sender, EventArgs e)
        {
            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeForm();
            //폼 로드 자동검색.
            this.searchBtn_Click(this.searchBtn, new EventArgs());

        }


        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);

            this.ultraGrid1.DisplayLayout.Bands[0].Override.SelectTypeRow = SelectType.None;
            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid1.DisplayLayout.Scrollbars = Scrollbars.None;
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.searchBox1.EndDateObject.ValueChanged += new EventHandler(EndDateObject_ValueChanged);

            this.ultraGrid1.DoubleClickHeader += new DoubleClickHeaderEventHandler(ultraGrid1_DoubleClickHeader);
        }

        void ultraGrid1_DoubleClickHeader(object sender, DoubleClickHeaderEventArgs e)
        {
            try
            {
                string loc_code = string.Empty;
                if (e.Header.Column == null && e.Header.Group != null)
                {
                    loc_code = e.Header.Group.Key;
                }
                else
                {
                    loc_code = e.Header.Column.Key;
                }
                if (string.IsNullOrEmpty(loc_code) || loc_code.IndexOf("SUM") > -1) return;
                Console.WriteLine(string.Format("{0}", loc_code));
                WV_Common.form.frmTagDescription form = new WaterNet.WV_Common.form.frmTagDescription(loc_code, "FRQ");
                form.Show();
            }
            catch { }
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalType = WaterNet.WV_Common.enum1.INTERVAL_TYPE.DATE;

            this.searchBox1.MiddleBlockContainer.Visible = false;
            this.searchBox1.SmallBlockContainer.Visible = false;

            this.searchBox1.MiddleBlockObject.SelectedIndex = 0;
            this.searchBox1.SmallBlockObject.SelectedIndex = 0;

            //시작일자 수정불가,
            this.searchBox1.StartDateObject.Enabled = false;

            this.panel1.AutoScroll = true;
        }

        /// <summary>
        /// 검색종료 일자를 기준으로 시작일자를 설정한다.
        /// 시작일자는 17일기준임
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EndDateObject_ValueChanged(object sender, EventArgs e)
        {
            DateTime endDate = (DateTime)this.searchBox1.EndDateObject.Value;

            string loc_code = this.searchBox1.LargeBlockObject.SelectedValue.ToString();

            DateTime startDate = DateTime.ParseExact(Utils.GetSuppliedStartdate(endDate.ToString("yyyyMMdd"), "yyyyMMdd", loc_code), "yyyyMMdd", null);
            this.searchBox1.StartDateObject.Value = startDate.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 엑셀버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelBtn_Click(object sender, EventArgs e)
        {
            //this.excelManager.Clear();
            //string resourceName = "DailyReport_" + EMFrame.statics.AppStatic.USER_SGCCD;
            //object obj = Properties.Resources.ResourceManager.GetObject(resourceName, Properties.Resources.Culture);
            //if (obj == null)
            //{
            //    MessageBox.Show("보고서 양식이 등록되어 있지 않습니다.");
            //    return;
            //}
            //this.excelManager.Load(new MemoryStream(((byte[])(obj))), WORKBOOK_TYPE.TEMPLATE);
            //this.excelManager.AddWorksheet(this.ultraGrid1, "temp");
            //this.excelManager.WriteContentToTemplete(0, 0, 8, 0);
            //string value = "조회 날자 ";
            //value += ((DateTime)this.searchBox1.StartDateObject.Value).ToString("[yyyy/MM/dd]");
            //value += " ~ ";
            //value += ((DateTime)this.searchBox1.EndDateObject.Value).ToString("[yyyy/MM/dd]");
            //TimeSpan dif = ((DateTime)this.searchBox1.EndDateObject.Value) - ((DateTime)this.searchBox1.StartDateObject.Value) ;
            //value += " " + (dif.Days+1).ToString() + "일간";
            //this.excelManager.WriteLine(0, 2, 0, value, WORKBOOK_TYPE.TEMPLATE);
            //this.excelManager.Open("관망운영일보", WORKBOOK_TYPE.TEMPLATE);
            Cursor cursor = this.Cursor;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ExpoterExcel();
            }
            catch (Exception e1)
            {
                Logger.Error(e1);
            }
            finally
            {
                this.Cursor = cursor;
            }
        }


        private void ExpoterExcel()
        {
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.WorkbookBeforeClose += new Microsoft.Office.Interop.Excel.AppEvents_WorkbookBeforeCloseEventHandler(xlApp_WorkbookBeforeClose);

            string midValue = searchBox1.MiddleBlockObject.Text;

            if (xlApp == null)
            {
                return;
            }

            Microsoft.Office.Interop.Excel.Workbooks workbooks = xlApp.Workbooks;
            Microsoft.Office.Interop.Excel.Workbook workbook = workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];
            Microsoft.Office.Interop.Excel.PageSetup print = worksheet.PageSetup;   //프린트 설정
            Microsoft.Office.Interop.Excel.Range range = xlApp.get_Range("B2", "AL2");  //병합
            Microsoft.Office.Interop.Excel.Range rangeCell;

            xlApp.ScreenUpdating = false;

            //---------------엑셀 제어
            DataSet dataSet = this.ultraGrid1.DataSource as DataSet;
            DataTable dataTableMD = dataSet.Tables["MIDDLE_INFO"];


            int columnCount = dataTableMD.Columns.Count + 1;    //17..시작이 0이 아닌 1부터 시작. +1해줌
            int rowCount = (dataTableMD.Rows.Count * 3) + 7;


            int blockColumnCount = dataTableMD.Rows.Count * 3;
            int blockRowCount = 5 + (dataTableMD.Rows.Count * 2);

            //결제란 -승일 수정 < 결재란 이미지 삭제 요청 >
            /*            
            //승일 수정 - 소블록의 길이가 중블록 보다 긴 경우 결재란 이미지를 소블록 마지막 위치 맨 위에 배치
            Image stamp = Properties.Resources.stamp;            
            if ((dataTableMD.Rows.Count == 1 && SD_MaxValue > 5) || (blockColumnCount + 2 < SD_MaxValue))
            {  
                Microsoft.Office.Interop.Excel.Range stamp_range =
                 worksheet.get_Range(worksheet.Cells[1, SD_MaxValue], worksheet.Cells[1, SD_MaxValue]);
                System.Windows.Forms.Clipboard.SetDataObject(stamp, true);
                worksheet.Paste(stamp_range, stamp);              
                
            }
            //승일 수정 - 엑셀저장 시 결재란 이미지가 Title을 가리는 현상방지
            else if (dataTableMD.Rows.Count == 1 && SD_MaxValue <= 5)
            {
                Microsoft.Office.Interop.Excel.Range stamp_range =
                 worksheet.get_Range(worksheet.Cells[1, blockColumnCount + 3], worksheet.Cells[1, blockColumnCount + 3]);
                System.Windows.Forms.Clipboard.SetDataObject(stamp, true);
                worksheet.Paste(stamp_range, stamp);    
            }
            //원본
            else
            {
                Microsoft.Office.Interop.Excel.Range stamp_range =
                 worksheet.get_Range(worksheet.Cells[1, blockColumnCount + 2], worksheet.Cells[1, blockColumnCount + 2]);
                System.Windows.Forms.Clipboard.SetDataObject(stamp, true);
                worksheet.Paste(stamp_range, stamp);
            }
            
            System.Windows.Forms.Clipboard.Clear();
            */

            //승일 수정 - 제목 병합 후 중블록, 소블록 길이에 따라 사이즈 변환
            if ((dataTableMD.Rows.Count * 3) + 2 < SD_MaxValue)
            {
                //타이틀 제목 병합
                rangeCell = worksheet.get_Range(worksheet.Cells[2, 1], worksheet.Cells[2, SD_MaxValue + 3]);
                rangeCell.Merge(true);
                rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데 정렬
                rangeCell.Font.Size = 30;
                rangeCell.Font.Bold = true;
                rangeCell.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                rangeCell.Font.Name = "돋음";
                rangeCell.RowHeight = 45;
            }
            else
            {
                rangeCell = worksheet.get_Range(worksheet.Cells[2, 1], worksheet.Cells[2, (dataTableMD.Rows.Count * 3) + 5]);
                rangeCell.Merge(true);
                rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데 정렬
                rangeCell.Font.Size = 30;
                rangeCell.Font.Bold = true;
                rangeCell.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                rangeCell.Font.Name = "돋음";
                rangeCell.RowHeight = 45;
            }

            // 전체 범위 설정            
            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[blockColumnCount + 11, blockColumnCount + 5]);
            rangeCell.Font.Name = "돋음";
            rangeCell.ColumnWidth = 10; //컬럼 넓이
            rangeCell.RowHeight = 18;

            //전체 범위 안쪽 테두리 설정
            rangeCell.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            rangeCell.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데정렬

            // 전체 범위 바깥쪽 테두리설정
            rangeCell.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin,
            Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic);


            //데이터 범위설정
            rangeCell = worksheet.get_Range(worksheet.Cells[7, 2], worksheet.Cells[rowCount + 4, columnCount]);
            rangeCell.NumberFormat = @"#,###,##0"; //숫자 범주 설정(정수표현)

            //데이터 오른쪽정렬
            rangeCell = worksheet.get_Range(worksheet.Cells[7, 4], worksheet.Cells[rowCount + 4, columnCount]);
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlRight;
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 1], worksheet.Cells[4, columnCount]);
            rangeCell.RowHeight = 0;

            //프린트 여백
            print.LeftMargin = 0;
            print.RightMargin = 0;
            print.TopMargin = 0;
            print.BottomMargin = 0;

            print.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;   //프린트 가로로 출력
            print.Zoom = AutoSize;//80;    //프린트 비율
            print.CenterVertically = false;  //세로 자동맞춤
            print.CenterHorizontally = true;    //가로 자동맞춤

            string value = "조회 날짜 " + ((DateTime)this.searchBox1.StartDateObject.Value).ToString("[yyyy/MM/dd]") + " ~ " +
                ((DateTime)this.searchBox1.EndDateObject.Value).ToString("[yyyy/MM/dd]");
            TimeSpan dif = ((DateTime)this.searchBox1.EndDateObject.Value) - ((DateTime)this.searchBox1.StartDateObject.Value);
            value += " " + (dif.Days + 1).ToString() + "일간";
            range.Cells[2, 0] = value;
            range.Cells[1, 0] = "유수율 종합 일보";

            //--------------------------

            if (dataSet == null || dataSet.Tables.IndexOf("MIDDLE_INFO") == -1)
            {
                return;
            }

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, 3]);
            rangeCell.Merge(true);
            rangeCell.Merge(false);
            rangeCell.Value2 = "구  분";

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 4], worksheet.Cells[6, 4]);
            rangeCell.Merge(false);
            rangeCell.Value2 = "정수장유출";

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 5], worksheet.Cells[6, 5]);
            rangeCell.Merge(false);
            rangeCell.Value2 = "공급량(합)";


            for (int i = 6; i < 10; i++)
            {
                if (i % 2 == 0)
                {
                    rangeCell = worksheet.get_Range(worksheet.Cells[i + 1, 1], worksheet.Cells[i + 2, 2]);
                    rangeCell.Merge(true);
                    rangeCell.Merge(false);
                    if (i == 6)
                    {
                        rangeCell.Value2 = "공급량";
                    }
                    else
                    {
                        rangeCell.Value2 = "누수량";
                    }
                }
                rangeCell = worksheet.get_Range(worksheet.Cells[i + 1, 3], worksheet.Cells[i + 2, 3]);
                if (i % 2 == 0)
                {
                    rangeCell.Value2 = "일  계";
                }
                else
                {
                    rangeCell.Value2 = "월누계";
                }
            }
            rangeCell = worksheet.get_Range(worksheet.Cells[11, 1], worksheet.Cells[11, 3]);
            rangeCell.Merge(true);
            rangeCell.Value2 = "유수율(%)";

            DataTable dataTableBG = dataSet.Tables["LARGE_INFO"];

            for (int i = 0; i < dataTableBG.Rows.Count; i++)
            {
                DataRow dataRow = dataTableBG.Rows[i];
                rangeCell = worksheet.get_Range(worksheet.Cells[7, 4], worksheet.Cells[7, 4]);
                rangeCell.Value2 = dataRow["jungsu_out_day_flow"].ToString();
                rangeCell = worksheet.get_Range(worksheet.Cells[8, 4], worksheet.Cells[8, 4]);
                rangeCell.Value2 = dataRow["jungsu_out_month_flow"].ToString();
                rangeCell = worksheet.get_Range(worksheet.Cells[9, 4], worksheet.Cells[9, 4]);
                rangeCell.Value2 = dataRow["jungsu_out_day_leakage"].ToString();
                rangeCell = worksheet.get_Range(worksheet.Cells[10, 4], worksheet.Cells[10, 4]);
                rangeCell.Value2 = dataRow["jungsu_out_month_leakage"].ToString();
                rangeCell = worksheet.get_Range(worksheet.Cells[11, 4], worksheet.Cells[11, 4]);
                rangeCell.Value2 = dataRow["jungsu_ratio"].ToString();

                rangeCell = worksheet.get_Range(worksheet.Cells[7, 5], worksheet.Cells[7, 5]);
                rangeCell.Value2 = dataRow["total_in_day_flow"].ToString();
                rangeCell = worksheet.get_Range(worksheet.Cells[8, 5], worksheet.Cells[8, 5]);
                rangeCell.Value2 = dataRow["total_in_month_flow"].ToString();
                rangeCell = worksheet.get_Range(worksheet.Cells[9, 5], worksheet.Cells[9, 5]);
                rangeCell.Value2 = dataRow["total_in_day_leakage"].ToString();
                rangeCell = worksheet.get_Range(worksheet.Cells[10, 4], worksheet.Cells[10, 4]);
                rangeCell.Value2 = dataRow["total_in_month_leakage"].ToString();
                rangeCell = worksheet.get_Range(worksheet.Cells[11, 4], worksheet.Cells[11, 4]);
                rangeCell.Value2 = dataRow["total_ratio"].ToString();
            }

            for (int i = 0; i < dataTableMD.Rows.Count; i++)
            {
                int originX = i * 3;
                DataRow dataRow = dataTableMD.Rows[i];

                //상위 중블록 컬럼
                rangeCell = worksheet.get_Range(worksheet.Cells[5, 6 + originX], worksheet.Cells[5, 8 + originX]);
                rangeCell.Merge(true);
                rangeCell.Value2 = dataRow["MIDDLE_NAME"].ToString();
                //왼쪽 중블록 컬럼
                rangeCell = worksheet.get_Range(worksheet.Cells[12 + originX, 1], worksheet.Cells[14 + originX, 1]);
                rangeCell.Merge(false);
                rangeCell.Value2 = dataRow["MIDDLE_NAME"].ToString();   //중블록
                rangeCell = worksheet.get_Range(worksheet.Cells[12 + originX, 2], worksheet.Cells[12 + originX, 3]);
                rangeCell.Merge(true);
                rangeCell.Value2 = "블록명";
                rangeCell = worksheet.get_Range(worksheet.Cells[13 + originX, 2], worksheet.Cells[14 + originX, 2]);
                rangeCell.Merge(false);
                rangeCell.Value2 = "공급량";
                rangeCell = worksheet.get_Range(worksheet.Cells[13 + originX, 3], worksheet.Cells[13 + originX, 3]);
                rangeCell.Value2 = "일  계";
                rangeCell = worksheet.get_Range(worksheet.Cells[14 + originX, 3], worksheet.Cells[14 + originX, 3]);
                rangeCell.Value2 = "월누계";
                rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[14 + originX, 3]);
                rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(204, 255, 204));

                //-------분기점
                rangeCell = worksheet.get_Range(worksheet.Cells[6, 6 + originX], worksheet.Cells[6, 6 + originX]);
                rangeCell.Value2 = "분기점";
                //공급량 일계
                rangeCell = worksheet.get_Range(worksheet.Cells[7, 6 + originX], worksheet.Cells[7, 6 + originX]);
                rangeCell.Value2 = dataRow["BRANCHOFF_DAY_FLOW"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //공급량 월누계
                rangeCell = worksheet.get_Range(worksheet.Cells[8, 6 + originX], worksheet.Cells[8, 6 + originX]);
                rangeCell.Value2 = dataRow["BRANCHOFF_MONTH_FLOW"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }
                //누수량 일계

                rangeCell = worksheet.get_Range(worksheet.Cells[9, 6 + originX], worksheet.Cells[9, 6 + originX]);
                rangeCell.Value2 = dataRow["BRANCHOFF_DAY_LEAKAGE"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //누수량 월누계
                rangeCell = worksheet.get_Range(worksheet.Cells[10, 6 + originX], worksheet.Cells[10, 6 + originX]);
                rangeCell.Value2 = dataRow["BRANCHOFF_MONTH_LEAKAGE"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //유수율
                rangeCell = worksheet.get_Range(worksheet.Cells[11, 6 + originX], worksheet.Cells[11, 6 + originX]);
                rangeCell.Value2 = dataRow["BRANCHOFF_RATIO"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //배수지유출
                rangeCell = worksheet.get_Range(worksheet.Cells[6, 7 + originX], worksheet.Cells[6, 7 + originX]);
                rangeCell.Value2 = "배수지유출";
                //공급량 일계
                rangeCell = worksheet.get_Range(worksheet.Cells[7, 7 + originX], worksheet.Cells[7, 7 + originX]);
                rangeCell.Value2 = dataRow["RESERVOIR_DAY_FLOW"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //공급량 월누계
                rangeCell = worksheet.get_Range(worksheet.Cells[8, 6 + originX + 1], worksheet.Cells[8, 6 + originX + 1]);
                rangeCell.Value2 = dataRow["RESERVOIR_MONTH_FLOW"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //누수량 일계
                rangeCell = worksheet.get_Range(worksheet.Cells[9, 6 + originX + 1], worksheet.Cells[9, 6 + originX + 1]);
                rangeCell.Value2 = dataRow["RESERVOIR_DAY_LEAKAGE"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //누수량 월누계
                rangeCell = worksheet.get_Range(worksheet.Cells[10, 6 + originX + 1], worksheet.Cells[10, 6 + originX + 1]);
                rangeCell.Value2 = dataRow["RESERVOIR_MONTH_LEAKAGE"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //유수율
                rangeCell = worksheet.get_Range(worksheet.Cells[11, 6 + originX + 1], worksheet.Cells[11, 6 + originX + 1]);
                rangeCell.Value2 = dataRow["RESERVOIR_RATIO"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //블록합
                rangeCell = worksheet.get_Range(worksheet.Cells[6, 6 + originX + 2], worksheet.Cells[6, 6 + originX + 2]);
                rangeCell.Value2 = "블록합"; ;
                //공급량 일계
                rangeCell = worksheet.get_Range(worksheet.Cells[7, 6 + originX + 2], worksheet.Cells[7, 6 + originX + 2]);
                rangeCell.Value2 = dataRow["BLOCK_DAY_FLOW"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //공급량 월누계
                rangeCell = worksheet.get_Range(worksheet.Cells[8, 6 + originX + 2], worksheet.Cells[8, 6 + originX + 2]);
                rangeCell.Value2 = dataRow["BLOCK_MONTH_FLOW"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //누수량 일계
                rangeCell = worksheet.get_Range(worksheet.Cells[9, 6 + originX + 2], worksheet.Cells[9, 6 + originX + 2]);
                rangeCell.Value2 = dataRow["BLOCK_DAY_LEAKAGE"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //누수량 월누계
                rangeCell = worksheet.get_Range(worksheet.Cells[10, 6 + originX + 2], worksheet.Cells[10, 6 + originX + 2]);
                rangeCell.Value2 = dataRow["BLOCK_MONTH_LEAKAGE"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //유수율
                rangeCell = worksheet.get_Range(worksheet.Cells[11, 6 + originX + 2], worksheet.Cells[11, 6 + originX + 2]);
                rangeCell.Value2 = dataRow["BLOCK_RATIO"].ToString();
                if (rangeCell != null)
                {
                    if (0 > Convert.ToInt32(rangeCell.Value2))
                    {
                        rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                }

                //상위 컬럼 배경색
                rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, 6 + originX + 2]);
                rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(204, 255, 204));
                //유수율 배경색
                rangeCell = worksheet.get_Range(worksheet.Cells[11, 2], worksheet.Cells[11, 6 + originX + 2]);
                rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(204, 255, 255));
                rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                rangeCell.NumberFormat = @"#,###,###.0"; //숫자 범주 설정(정수표현)
            }

            //원본임
            DataTable dataTableSM = dataSet.Tables["SMALL_INFO"];
            Dictionary<string, int> smallBlockCount = new Dictionary<string, int>();
            int rowEnter = 0;
            foreach (DataRow dataRow in dataTableSM.Rows)
            {
                string key = dataRow["MIDDLE_NAME"].ToString();

                if (smallBlockCount.ContainsKey(key))
                {
                    smallBlockCount[key]++;
                }
                else
                {
                    smallBlockCount[key] = 0;
                }

                foreach (DataRow middleRow in dataTableMD.Rows)
                {
                    if (key == middleRow["MIDDLE_NAME"].ToString())     //같은 계통이면
                    {
                        rowEnter = dataTableMD.Rows.IndexOf(middleRow) * 3;
                        break;
                    }
                }


                //승일 수정 - 중블록 길이가 길 경우 원본 적용
                if (smallBlockCount[key] + 3 < (dataTableMD.Rows.Count * 3) + 5)
                {
                    //소블록 블록명
                    rangeCell = worksheet.get_Range(worksheet.Cells[12 + rowEnter, 4 + smallBlockCount[key]], worksheet.Cells[12 + rowEnter, 4 + smallBlockCount[key]]);
                    rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(204, 255, 204));    //소블록 블록명 배경색
                    rangeCell.Value2 = dataRow["SMALL_NAME"].ToString();
                    rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;


                    //소블록 일계
                    rangeCell = worksheet.get_Range(worksheet.Cells[13 + rowEnter, 4 + smallBlockCount[key]], worksheet.Cells[13 + rowEnter, 4 + smallBlockCount[key]]);
                    rangeCell.Value2 = dataRow["DAY_FLOW"].ToString();

                    if (rangeCell != null)
                    {
                        if (0 > Convert.ToInt32(rangeCell.Value2))
                        {
                            rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                        }
                    }

                    //소블록 월계
                    rangeCell = worksheet.get_Range(worksheet.Cells[14 +
                        rowEnter, 4 + smallBlockCount[key]], worksheet.Cells[14 + rowEnter, 4 + smallBlockCount[key]]);

                    rangeCell.Value2 = dataRow["MONTH_FLOW"].ToString();

                    if (rangeCell != null)
                    {
                        if (0 > Convert.ToInt32(rangeCell.Value2))
                        {
                            rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                        }
                    }
                }
                //승일 수정 - 소블록 길이가 길 경우 동적으로 엑셀 칸 생성 
                else
                {

                    rangeCell = worksheet.get_Range(worksheet.Cells[12 + rowEnter, 4 + smallBlockCount[key]], worksheet.Cells[12 + rowEnter, 4 + smallBlockCount[key]]);
                    rangeCell.Interior.Color = ColorTranslator.ToOle(Color.FromArgb(204, 255, 204));    //소블록 블록명 배경색
                    rangeCell.Value2 = dataRow["SMALL_NAME"].ToString();
                    rangeCell.ColumnWidth = 10;
                    rangeCell.Font.Name = "돋음";
                    rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                    rangeCell.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    rangeCell.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                    rangeCell.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin,
                    Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic);


                    //소블록 일계
                    rangeCell = worksheet.get_Range(worksheet.Cells[13 + rowEnter, 4 + smallBlockCount[key]], worksheet.Cells[13 + rowEnter, 4 + smallBlockCount[key]]);
                    rangeCell.Value2 = dataRow["DAY_FLOW"].ToString();
                    rangeCell.ColumnWidth = 10;
                    rangeCell.Font.Name = "돋음";
                    if (rangeCell != null)
                    {
                        if (0 > Convert.ToInt32(rangeCell.Value2))
                        {
                            rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                        }
                    }

                    rangeCell.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    rangeCell.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                    rangeCell.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin,
                    Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic);


                    //소블록 월계
                    rangeCell = worksheet.get_Range(worksheet.Cells[14 + rowEnter, 4 + smallBlockCount[key]], worksheet.Cells[14 + rowEnter, 4 + smallBlockCount[key]]);
                    rangeCell.Value2 = dataRow["MONTH_FLOW"].ToString();
                    rangeCell.ColumnWidth = 10;
                    rangeCell.Font.Name = "돋음";
                    if (rangeCell != null)
                    {
                        if (0 > Convert.ToInt32(rangeCell.Value2))
                        {
                            rangeCell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                        }
                    }
                    rangeCell.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    rangeCell.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                    rangeCell.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin,
                    Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic);


                }

            }

            xlApp.ScreenUpdating = true;
            xlApp.Visible = true;
        }

        private void xlApp_WorkbookBeforeClose(Microsoft.Office.Interop.Excel.Workbook Wb, ref bool Cancel)
        {
            Process[] ExcelPros = Process.GetProcessesByName("EXCEL");

            for (int i = 0; i < ExcelPros.Length; i++)
            {
                Console.WriteLine(ExcelPros[i].MainWindowTitle);

                if (ExcelPros[i].MainWindowTitle == "")
                {
                    ExcelPros[i].Kill();
                }
            }
        }

        /// <summary>
        /// 검색버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            //this.SelectDailyReport( TEST());
            Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
            this.SelectDailyReport(parameter);
        }

        /// <summary>
        /// 관망운영일보를 검색한다.
        /// </summary>
        /// <param name="dataSet"></param>
        private void SelectDailyReport(Hashtable parameter)
        {
            this.InitGrid();
            this.ultraGrid1.DataSource = ReportWork.GetInstance().SelectDailyReport(parameter);
            //this.ultraGrid1.DataSource = this.TEST();

            DataSet dataSet = (DataSet)this.ultraGrid1.DataSource;
            this.AddReportGroup(dataSet);
            this.SetReportColumn(dataSet);
        }

        /// <summary>
        /// 동적으로 생성한 그룹을 삭제한다.
        /// </summary>
        private void InitGrid()
        {
            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Count; i > 0; i--)
            {
                if (this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup0" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup1" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup2" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup3" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup4" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup5" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup6" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup7" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup8" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups[i - 1].Key != "NewGroup9"
                    )
                {
                    this.ultraGrid1.DisplayLayout.Bands[0].Groups.Remove(i - 1);
                }
            }

            for (int i = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count; i > 0; i--)
            {
                if (this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "JUNGSU_OUT_DAY_FLOW" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "JUNGSU_OUT_MONTH_FLOW" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "JUNGSU_OUT_DAY_LEAKAGE" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "JUNGSU_OUT_MONTH_LEAKAGE" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "JUNGSU_RATIO" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "TOTAL_IN_DAY_FLOW" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "TOTAL_IN_MONTH_FLOW" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "TOTAL_IN_DAY_LEAKAGE" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "TOTAL_IN_MONTH_LEAKAGE" &&
                    this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].Key != "TOTAL_RATIO"
                    )
                {
                    if (!this.ultraGrid1.DisplayLayout.Bands[0].Columns[i - 1].IsBound)
                    {
                        this.ultraGrid1.DisplayLayout.Bands[0].Columns.Remove(i - 1);
                    }
                }
            }
        }

        //그룹을 동적으로 생성한다.
        private void AddReportGroup(DataSet dataSet)
        {

            if (dataSet == null || dataSet.Tables.IndexOf("MIDDLE_INFO") == -1)
            {
                return;
            }

            int width = 335;
            int height = 210;

            DataTable dataTable = dataSet.Tables["MIDDLE_INFO"];

            //승일 추가 - 소블록 최대 갯수 구하기 위함
            DataTable dataTableSD = dataSet.Tables["SMALL_INFO"];
            List<string> list = new List<string>();
            List<int> list_i = new List<int>();


            for (int i = 0; i < dataTableSD.Rows.Count; i++)
            {
                DataRow dr = dataTableSD.Rows[i];
                string name = dr["MIDDLE_NAME"].ToString();

                if (!list.Contains(name))
                {
                    list.Clear();
                }

                for (int j = 0; j < dataTable.Rows.Count; j++)
                {
                    DataRow dr2 = dataTable.Rows[j];
                    string MD_name = dr2["MIDDLE_NAME"].ToString();

                    if (name == MD_name)
                    {
                        list.Add(name);
                    }

                }

                list_i.Add(list.Count);
                SD_MaxValue = list_i.Max();
            }

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                int originX = i * 3;

                UltraGridGroup gridGroup = null;
                DataRow dataRow = dataTable.Rows[i];

                //승일 추가 - 컬럼 안에 소블록명이 노출되지 않기에 사용
                if (((dataTable.Rows.Count == 1) || (dataTable.Rows.Count == 2)) && dataTable.Rows.Count + 2 < SD_MaxValue)
                {
                    width += 600;
                    height += 90;
                }
                else
                {
                    width += 360;//승일 수정 - 그리드 안에 소블록명이 노출되지 않기에 사용
                    height += 90;
                }
                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.Key = dataRow["MIDDLE_NAME"].ToString();
                gridGroup.Header.Caption = dataRow["MIDDLE_NAME"].ToString();
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 3;
                gridGroup.RowLayoutGroupInfo.OriginX = originX + 5;
                gridGroup.RowLayoutGroupInfo.OriginY = 0;
                gridGroup.RowLayoutGroupInfo.SpanX = 3;
                gridGroup.RowLayoutGroupInfo.SpanY = 1;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(0, 30);

                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.RowLayoutGroupInfo.ParentGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups[dataRow["MIDDLE_NAME"].ToString()];
                gridGroup.Key = dataRow["MIDDLE_NAME"].ToString() + "분기점";
                gridGroup.Header.Caption = "분기점";
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 1;
                gridGroup.RowLayoutGroupInfo.OriginX = 0;
                gridGroup.RowLayoutGroupInfo.OriginY = 0;
                gridGroup.RowLayoutGroupInfo.SpanX = 1;
                gridGroup.RowLayoutGroupInfo.SpanY = 1;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(120, 30);//승일 수정 - 그리드 안에 내용이 노출되지 않기에 사용

                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.RowLayoutGroupInfo.ParentGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups[dataRow["MIDDLE_NAME"].ToString()];
                gridGroup.Key = dataRow["MIDDLE_NAME"].ToString() + "배수지유출";
                gridGroup.Header.Caption = "배수지유출";
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.Both;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 1;
                gridGroup.RowLayoutGroupInfo.OriginX = 1;
                gridGroup.RowLayoutGroupInfo.OriginY = 0;
                gridGroup.RowLayoutGroupInfo.SpanX = 1;
                gridGroup.RowLayoutGroupInfo.SpanY = 1;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(120, 30);//승일 수정 - 그리드 안에 내용이 노출되지 않기에 사용

                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.RowLayoutGroupInfo.ParentGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups[dataRow["MIDDLE_NAME"].ToString()];
                gridGroup.Key = dataRow["MIDDLE_NAME"].ToString() + "블록합";
                gridGroup.Header.Caption = "블록합";
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 1;
                gridGroup.RowLayoutGroupInfo.OriginX = 2;
                gridGroup.RowLayoutGroupInfo.OriginY = 0;
                gridGroup.RowLayoutGroupInfo.SpanX = 1;
                gridGroup.RowLayoutGroupInfo.SpanY = 1;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(120, 30);//승일 수정 - 그리드 안에 내용이 노출되지 않기에 사용

                //좌측하단 소블록
                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.Key = dataRow["MIDDLE_NAME"].ToString() + "_소블록";
                gridGroup.Header.Caption = dataRow["MIDDLE_NAME"].ToString();
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 3;
                gridGroup.RowLayoutGroupInfo.OriginX = 0;
                gridGroup.RowLayoutGroupInfo.OriginY = originX + 7;
                gridGroup.RowLayoutGroupInfo.SpanX = 0;
                gridGroup.RowLayoutGroupInfo.SpanY = 3;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(120, 30); //승일 수정 - 그리드 안에 내용이 노출되지 않기에 사용

                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.Header.Caption = "블록명";
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 2;
                gridGroup.RowLayoutGroupInfo.OriginX = 1;
                gridGroup.RowLayoutGroupInfo.OriginY = originX + 7;
                gridGroup.RowLayoutGroupInfo.SpanX = 2;
                gridGroup.RowLayoutGroupInfo.SpanY = 1;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(140, 30);

                UltraGridGroup blockGroup = null;
                blockGroup = gridGroup;
                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.RowLayoutGroupInfo.ParentGroup = blockGroup;
                gridGroup.Header.Caption = "공급량(㎥)";
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 2;
                gridGroup.RowLayoutGroupInfo.OriginX = 0;
                gridGroup.RowLayoutGroupInfo.OriginY = 0;
                gridGroup.RowLayoutGroupInfo.SpanX = 1;
                gridGroup.RowLayoutGroupInfo.SpanY = 2;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(80, 30);//승일 수정 - 그리드 안에 내용이 노출되지 않기에 사용

                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.RowLayoutGroupInfo.ParentGroup = blockGroup;
                gridGroup.Header.Caption = "일  계";
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 1;
                gridGroup.RowLayoutGroupInfo.OriginX = 1;
                gridGroup.RowLayoutGroupInfo.OriginY = 0;
                gridGroup.RowLayoutGroupInfo.SpanX = 1;
                gridGroup.RowLayoutGroupInfo.SpanY = 1;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(70, 30);

                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.RowLayoutGroupInfo.ParentGroup = blockGroup;
                gridGroup.Header.Caption = "월누계";
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 1;
                gridGroup.RowLayoutGroupInfo.OriginX = 1;
                gridGroup.RowLayoutGroupInfo.OriginY = 1;
                gridGroup.RowLayoutGroupInfo.SpanX = 1;
                gridGroup.RowLayoutGroupInfo.SpanY = 1;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(80, 30);//승일 수정 - 그리드 안에 내용이 노출되지 않기에 사용

            }
            this.ultraGrid1.Size = new Size(width, height);
            this.ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;//승일 추가 - 그리드내 자동 컬럼 크기 조절
            this.ultraGrid1.Refresh();



        }

        /// <summary>
        /// 컬럼을 동적으로 만들고 데이터를 설정한다.
        /// </summary>
        private void SetReportColumn(DataSet dataSet)
        {
            DataTable dataTable = null;
            UltraGridColumn gridColumn = null;

            //중블록(계통)
            dataTable = dataSet.Tables["MIDDLE_INFO"];

            foreach (DataRow dataRow in dataTable.Rows)
            {
                //계통명으로 그룹 시작 x값 찾기.
                UltraGridGroup gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups[dataRow["MIDDLE_NAME"].ToString()];

                int originX = gridGroup.RowLayoutGroupInfo.OriginX;

                //분기점...
                //공급량 일계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX;
                gridColumn.RowLayoutColumnInfo.OriginY = 2;
                gridColumn.Key = gridGroup.Key + "BRANCHOFF_DAY_FLOW";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BRANCHOFF_DAY_FLOW"];

                //공급량 월누계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX;
                gridColumn.RowLayoutColumnInfo.OriginY = 3;
                gridColumn.Key = gridGroup.Key + "BRANCHOFF_MONTH_FLOW";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BRANCHOFF_MONTH_FLOW"];

                //누수량 일계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX;
                gridColumn.RowLayoutColumnInfo.OriginY = 4;
                gridColumn.Key = gridGroup.Key + "BRANCHOFF_DAY_LEAKAGE";
                gridColumn.DataType = typeof(string);
                gridColumn.Format = "";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BRANCHOFF_DAY_LEAKAGE"];

                //누수량 월누계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX;
                gridColumn.RowLayoutColumnInfo.OriginY = 5;
                gridColumn.Key = gridGroup.Key + "BRANCHOFF_MONTH_LEAKAGE";
                gridColumn.DataType = typeof(string);
                gridColumn.Format = "";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BRANCHOFF_MONTH_LEAKAGE"];

                //유수율
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX;
                gridColumn.RowLayoutColumnInfo.OriginY = 6;
                gridColumn.Key = gridGroup.Key + "BRANCHOFF_RATIO";
                gridColumn.DataType = typeof(string);
                gridColumn.Format = "";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BRANCHOFF_RATIO"];

                //배수지유출
                //공급량 일계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 1;
                gridColumn.RowLayoutColumnInfo.OriginY = 2;
                gridColumn.Key = gridGroup.Key + "RESERVOIR_DAY_FLOW";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["RESERVOIR_DAY_FLOW"];

                //공급량 월누계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 1;
                gridColumn.RowLayoutColumnInfo.OriginY = 3;
                gridColumn.Key = gridGroup.Key + "RESERVOIR_MONTH_FLOW";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["RESERVOIR_MONTH_FLOW"];

                //누수량 일계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 1;
                gridColumn.RowLayoutColumnInfo.OriginY = 4;
                gridColumn.Key = gridGroup.Key + "RESERVOIR_DAY_LEAKAGE";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["RESERVOIR_DAY_LEAKAGE"];

                //누수량 월누계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 1;
                gridColumn.RowLayoutColumnInfo.OriginY = 5;
                gridColumn.Key = gridGroup.Key + "RESERVOIR_MONTH_LEAKAGE";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["RESERVOIR_MONTH_LEAKAGE"];

                //유수율
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 1;
                gridColumn.RowLayoutColumnInfo.OriginY = 6;
                gridColumn.Key = gridGroup.Key + "RESERVOIR_RATIO";
                gridColumn.Format = "###,###.0";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["RESERVOIR_RATIO"];

                //블록합
                //공급량 일계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 2;
                gridColumn.RowLayoutColumnInfo.OriginY = 2;
                gridColumn.Key = gridGroup.Key + "BLOCK_DAY_FLOW";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BLOCK_DAY_FLOW"];

                //공급량 월누계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 2;
                gridColumn.RowLayoutColumnInfo.OriginY = 3;
                gridColumn.Key = gridGroup.Key + "BLOCK_MONTH_FLOW";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BLOCK_MONTH_FLOW"];

                //누수량 일계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 2;
                gridColumn.RowLayoutColumnInfo.OriginY = 4;
                gridColumn.Key = gridGroup.Key + "BLOCK_DAY_LEAKAGE";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BLOCK_DAY_LEAKAGE"];

                //누수량 월누계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 2;
                gridColumn.RowLayoutColumnInfo.OriginY = 5;
                gridColumn.Key = gridGroup.Key + "BLOCK_MONTH_LEAKAGE";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BLOCK_MONTH_LEAKAGE"];

                //유수율
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = originX + 2;
                gridColumn.RowLayoutColumnInfo.OriginY = 6;
                gridColumn.Key = gridGroup.Key + "BLOCK_RATIO";
                gridColumn.Format = "###,###.0";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["BLOCK_RATIO"];
            }

            //소블록(계통)
            dataTable = dataSet.Tables["SMALL_INFO"];
            Dictionary<string, int> locationCount = new Dictionary<string, int>();

            foreach (DataRow dataRow in dataTable.Rows)
            {
                //계통명으로 그룹 시작 x값 찾기.
                string key = dataRow["MIDDLE_NAME"].ToString() + "_소블록";
                UltraGridGroup gridGroup = null;

                if (this.ultraGrid1.DisplayLayout.Bands[0].Groups.IndexOf(key) == -1)
                {
                    break;
                }
                else
                {
                    gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups[key];
                }

                if (locationCount.ContainsKey(key))
                {
                    locationCount[key]++;
                }
                else
                {
                    locationCount[key] = 0;
                }

                int originY = gridGroup.RowLayoutGroupInfo.OriginY;

                //소블록 블록명
                gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
                gridGroup.Header.Caption = dataRow["SMALL_NAME"].ToString();
                gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.Horizontal;
                gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
                gridGroup.RowLayoutGroupInfo.LabelSpan = 1;
                gridGroup.RowLayoutGroupInfo.OriginX = 3 + locationCount[key];
                gridGroup.RowLayoutGroupInfo.OriginY = originY;
                gridGroup.RowLayoutGroupInfo.SpanX = 1;
                gridGroup.RowLayoutGroupInfo.SpanY = 1;
                gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(80, 30);


                //소블록 일계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = 3 + locationCount[key];
                gridColumn.RowLayoutColumnInfo.OriginY = originY + 1;
                gridColumn.Key = dataRow["MIDDLE_NAME"].ToString() + dataRow["SMALL_NAME"].ToString() + "DAY_FLOW";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["DAY_FLOW"];

                //소블록 월계
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.OriginX = 3 + locationCount[key];
                gridColumn.RowLayoutColumnInfo.OriginY = originY + 2;
                gridColumn.Key = dataRow["MIDDLE_NAME"].ToString() + dataRow["SMALL_NAME"].ToString() + "MONTH_FLOW";
                this.ultraGrid1.Rows[0].Cells[gridColumn.Key].Value = dataRow["MONTH_FLOW"];


            }

        }

        private UltraGridColumn GetReportColumn()
        {
            UltraGridColumn gridColumn = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            //gridColumn.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            //gridColumn.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            gridColumn.RowLayoutColumnInfo.LabelPosition = LabelPosition.None;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            gridColumn.RowLayoutColumnInfo.SpanX = 1;
            gridColumn.RowLayoutColumnInfo.SpanY = 1;
            gridColumn.RowLayoutColumnInfo.LabelSpan = 1;
            gridColumn.DataType = typeof(double);
            gridColumn.Format = "###,###,###";
            gridColumn.Width = 85;
            return gridColumn;
        }
    }
}



//grid 정수장유출, 공급량(합) 크기 변경 120,120
//이유 컬럼 안에 내용이 노출되지 않기에 사용

