﻿using System;
using System.Data;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WaterNetCore;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.util;
using WaterNet.WV_Report.work;
using ChartFX.WinForms;
using EMFrame.log;

namespace WaterNet.WV_Report.form
{
    public partial class frmDailyLog : Form, WaterNet.WaterNetCore.IForminterface
    {
        private ChartManager chartManager = null;
        private UltraGridManager gridManager = null;

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmDailyLog()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmDailyLog_Load);
        }


        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }

        #endregion

        public void Open()
        {
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDailyLog_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================특이사항기록

            object o = EMFrame.statics.AppStatic.USER_MENU["특이사항기록ToolStripMenuItem"];
            //object o = EMFrame.statics.AppStatic.USER_MENU["관망운영일보ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.insertBtn.Enabled = false;
                this.updateBtn.Enabled = false;
                this.deleteBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
            this.InitializeForm();
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(0, "yyyy-MM-dd");
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);

            this.ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;

            UltraGridColumn gridColumn = null;
            gridColumn = this.ultraGrid1.DisplayLayout.Bands[0].Columns["DATEE"];
            gridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Date;

            gridColumn = this.ultraGrid1.DisplayLayout.Bands[0].Columns["CONTENT"];
            gridColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;

            FormManager.SetGridStyle_ColumeAllowEdit(this.ultraGrid1, "LBLOCK");
            FormManager.SetGridStyle_ColumeAllowEdit(this.ultraGrid1, "MBLOCK");
            FormManager.SetGridStyle_ColumeAllowEdit(this.ultraGrid1, "SBLOCK");
            FormManager.SetGridStyle_ColumeAllowEdit(this.ultraGrid1, "DATEE");
            FormManager.SetGridStyle_ColumeAllowEdit(this.ultraGrid1, "CONTENT");

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.Default;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //대블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.insertBtn.Click += new EventHandler(insertBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.deleteBtn.Click += new EventHandler(deleteBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalType = WaterNet.WV_Common.enum1.INTERVAL_TYPE.DATE;

            this.searchBox1.StartDateObject.Value = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
            this.searchBox1.EndDateObject.Value = DateTime.Now.ToString("yyyy-MM-dd");

            //tableLayoutPanel1.ColumnStyles[1].
        }

        /// <summary>
        /// 엑셀버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                FormManager.ExportToExcelFromUltraGrid(this.ultraGrid1, "특이사항기록");
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 추가버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void insertBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.DataSource == null)
            {
                this.gridManager.DefaultColumnsMapping2(this.ultraGrid1);
            }

            UltraGridRow gridRow = this.ultraGrid1.DisplayLayout.Bands[0].AddNew();

            if (this.searchBox1.Parameters.ContainsKey("LBLOCK"))
            {
                gridRow.Cells["LBLOCK"].Value = this.searchBox1.Parameters["LBLOCK"];
            }
            else if (!this.searchBox1.Parameters.ContainsKey("LBLOCK"))
            {
                gridRow.Cells["LBLOCK"].Value = this.searchBox1.LargeBlockObject.SelectedValue;
            }

            if (this.searchBox1.Parameters.ContainsKey("MBLOCK"))
            {
                gridRow.Cells["MBLOCK"].Value = this.searchBox1.Parameters["MBLOCK"];
            }
            else if (!this.searchBox1.Parameters.ContainsKey("MBLOCK"))
            {
                gridRow.Cells["MBLOCK"].Value = this.searchBox1.MiddleBlockObject.SelectedValue;
            }

            if (this.searchBox1.Parameters.ContainsKey("SBLOCK"))
            {
                gridRow.Cells["SBLOCK"].Value = this.searchBox1.Parameters["SBLOCK"];
            }
            else if (!this.searchBox1.Parameters.ContainsKey("SBLOCK"))
            {
                gridRow.Cells["SBLOCK"].Value = this.searchBox1.SmallBlockObject.SelectedValue;
            }
            
            gridRow.Cells["DATEE"].Value = DateTime.Now;
        }

        /// <summary>
        /// 저장버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.UpdateDailyLog();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 삭제버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.ultraGrid1.Selected.Rows.Count == 0)
                {
                    return;
                }

                DialogResult qe = MessageBox.Show("삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (qe == DialogResult.Yes)
                {
                    this.DeleteDailyLog();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 검색버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                parameter["LBLOCK"] = this.searchBox1.LargeBlockObject.SelectedValue;
                parameter["MBLOCK"] = this.searchBox1.MiddleBlockObject.SelectedValue;
                parameter["SBLOCK"] = this.searchBox1.SmallBlockObject.SelectedValue;
                this.SelectDailyLog(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 특이사항기록을 검색한다.
        /// </summary>
        /// <param name="dataSet"></param>
        private void SelectDailyLog(Hashtable parameter)
        {
            this.ultraGrid1.DataSource = ReportWork.GetInstance().SelectDailyLog(parameter);
            this.InitializeChartSetting();
        }

        /// <summary>
        /// 차트 데이터 세팅 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            this.chartManager.AllClear(0);

            DataSet dataSet = this.ultraGrid1.DataSource as DataSet;

            if (dataSet == null)
            {
                return;
            }

            if (dataSet.Tables.Count != 2)
            {
                return;
            }

            DataTable dataTable = dataSet.Tables[1];
            this.chart1.Data.Series = 4;
            this.chart1.Data.Points = dataTable.Rows.Count;

            foreach (DataRow row in dataTable.Rows)
            {
                int pointIndex = dataTable.Rows.IndexOf(row);
                this.chart1.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["DATEE"]).ToString("yyyy-MM-dd");
                this.chart1.Data[0, pointIndex] = Utils.ToDouble(row["FLOW"]);
                this.chart1.Data[1, pointIndex] = Utils.ToDouble(row["MNF"]);
                this.chart1.Data[2, pointIndex] = Utils.ToDouble(row["MINLEAK_COUNT"]);
                this.chart1.Data[3, pointIndex] = Utils.ToDouble(row["TAMLEAK_COUNT"]);
            }

            this.chart1.Series[0].Gallery = Gallery.Curve;
            this.chart1.Series[0].MarkerShape = MarkerShape.Diamond;
            this.chart1.Series[0].Text = "공급량(㎥/일)";

            this.chart1.Series[1].Gallery = Gallery.Curve;
            this.chart1.Series[1].MarkerShape = MarkerShape.Diamond;
            this.chart1.Series[1].Text = "야간최소유량(㎥/h)";

            this.chart1.Series[2].Gallery = Gallery.Scatter;
            this.chart1.Series[2].MarkerShape = MarkerShape.Rect;
            this.chart1.Series[2].Text = "누수복구(민원)";

            this.chart1.Series[3].Gallery = Gallery.Scatter;
            this.chart1.Series[3].MarkerShape = MarkerShape.Triangle;
            this.chart1.Series[3].Text = "누수복구(누수탐사)";

            this.chart1.AxesY.Add(new AxisY());
            this.chart1.AxesY.Add(new AxisY());

            this.chart1.Series[0].AxisY = this.chart1.AxesY[0];
            this.chart1.Series[1].AxisY = this.chart1.AxesY[1];
            this.chart1.Series[2].AxisY = this.chart1.AxesY[2];
            this.chart1.Series[3].AxisY = this.chart1.AxesY[2];

            this.chart1.Series[0].AxisY = this.chart1.Panes[0].Axes[0];
            this.chart1.Series[1].AxisY = this.chart1.Panes[1].Axes[0];

            this.chart1.Panes[1].Axes.Add(new AxisY());
            this.chart1.Series[2].AxisY = this.chart1.Panes[1].Axes[1];
            this.chart1.Series[3].AxisY = this.chart1.Panes[1].Axes[1];

            //this.chart1.Series[0].Pane = this.chart1.Panes[0];
            //this.chart1.Series[1].Pane = this.chart1.Panes[1];
            //this.chart1.Series[2].Pane = this.chart1.Panes[1];
            //this.chart1.Series[3].Pane = this.chart1.Panes[1];

            this.chart1.Panes[0].Axes[0].Title.Text = "공급량(㎥/일)";
            this.chart1.Panes[0].Axes[0].DataFormat.Format = AxisFormat.Number;
            this.chart1.Panes[0].Axes[0].DataFormat.CustomFormat = "###,###,###";

            this.chart1.Panes[1].Axes[0].Title.Text = "야간최소유량(㎥/h)";
            this.chart1.Panes[1].Axes[0].DataFormat.Format = AxisFormat.Number;
            this.chart1.Panes[1].Axes[0].DataFormat.CustomFormat = "###,###,###.0";

            this.chart1.Panes[1].Axes[1].Title.Text = "누수복구(건)";
            this.chart1.Panes[1].Axes[1].DataFormat.Format = AxisFormat.Number;
            this.chart1.Panes[1].Axes[1].DataFormat.CustomFormat = "###,###,###";



            chart1.Series[0].Visible = true;
            chart1.Series[1].Visible = true;
            chart1.Series[2].Visible = true;
            chart1.Series[3].Visible = true;
        }

        private void DeleteDailyLog()
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                return;
            }

            UltraGridRow gridRow = this.ultraGrid1.Selected.Rows[0];
            Hashtable parameter = Utils.ConverToHashtable(gridRow);
            ReportWork.GetInstance().DeleteDailyLog(parameter);
            gridRow.Delete(false);
            this.ultraGrid1.UpdateData();
        }


        private void UpdateDailyLog()
        {
            if (this.ultraGrid1.Rows.Count == 0)
            {
                return;
            }

            ReportWork.GetInstance().UpdateDailyLog(this.ultraGrid1.Rows);
            this.SelectDailyLog(this.searchBox1.Parameters);
        }
    }
}
