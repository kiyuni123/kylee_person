﻿namespace WaterNet.WV_Report.form
{
    partial class frmDailyReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNGSU_OUT_DAY_FLOW");
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNGSU_OUT_MONTH_FLOW");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNGSU_OUT_DAY_LEAKAGE");
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNGSU_OUT_MONTH_LEAKAGE");
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUNGSU_RATIO");
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_IN_DAY_FLOW");
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_IN_MONTH_FLOW");
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_IN_DAY_LEAKAGE");
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_IN_MONTH_LEAKAGE");
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_RATIO");
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 52808695);
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 52808696);
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup2", 52808697);
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup3", 52808698);
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup4", 52808699);
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup5", 52808700);
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup7 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup6", 52808701);
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup8 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup7", 52808702);
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup9 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup9", 52808703);
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup10 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup8", 52808704);
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDailyReport));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1252, 10);
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Location = new System.Drawing.Point(0, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 815);
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(1242, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 815);
            this.pictureBox3.TabIndex = 17;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(10, 815);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1232, 10);
            this.pictureBox4.TabIndex = 18;
            this.pictureBox4.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.searchBtn);
            this.panel8.Controls.Add(this.excelBtn);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(10, 121);
            this.panel8.Margin = new System.Windows.Forms.Padding(0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1232, 30);
            this.panel8.TabIndex = 21;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(1192, 2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 22;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(1146, 2);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 23;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // ultraGrid1
            // 
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance1;
            ultraGridBand1.CardSettings.AllowLabelSizing = false;
            ultraGridBand1.CardSettings.AllowSizing = false;
            ultraGridBand1.CardSettings.Style = Infragistics.Win.UltraWinGrid.CardStyle.Compressed;
            appearance2.TextHAlignAsString = "Right";
            appearance2.TextVAlignAsString = "Middle";
            ultraGridColumn12.CellAppearance = appearance2;
            ultraGridColumn12.Format = "###,###,###";
            ultraGridColumn12.Header.VisiblePosition = 0;
            ultraGridColumn12.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn12.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn12.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn12.RowLayoutColumnInfo.OriginX = 3;
            ultraGridColumn12.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn12.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 25);
            ultraGridColumn12.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(213, 0);
            ultraGridColumn12.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn12.RowLayoutColumnInfo.SpanY = 1;
            appearance3.TextHAlignAsString = "Right";
            appearance3.TextVAlignAsString = "Middle";
            ultraGridColumn13.CellAppearance = appearance3;
            ultraGridColumn13.Format = "###,###,###";
            ultraGridColumn13.Header.VisiblePosition = 1;
            ultraGridColumn13.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn13.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn13.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn13.RowLayoutColumnInfo.OriginX = 3;
            ultraGridColumn13.RowLayoutColumnInfo.OriginY = 3;
            ultraGridColumn13.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 26);
            ultraGridColumn13.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(213, 0);
            ultraGridColumn13.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn13.RowLayoutColumnInfo.SpanY = 1;
            appearance4.TextHAlignAsString = "Right";
            appearance4.TextVAlignAsString = "Middle";
            ultraGridColumn14.CellAppearance = appearance4;
            ultraGridColumn14.Format = "###,###,###";
            ultraGridColumn14.Header.VisiblePosition = 2;
            ultraGridColumn14.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn14.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn14.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn14.RowLayoutColumnInfo.OriginX = 3;
            ultraGridColumn14.RowLayoutColumnInfo.OriginY = 4;
            ultraGridColumn14.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 18);
            ultraGridColumn14.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(62, 0);
            ultraGridColumn14.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn14.RowLayoutColumnInfo.SpanY = 1;
            appearance5.TextHAlignAsString = "Right";
            appearance5.TextVAlignAsString = "Middle";
            ultraGridColumn15.CellAppearance = appearance5;
            ultraGridColumn15.Format = "###,###,###";
            ultraGridColumn15.Header.VisiblePosition = 3;
            ultraGridColumn15.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn15.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn15.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn15.RowLayoutColumnInfo.OriginX = 3;
            ultraGridColumn15.RowLayoutColumnInfo.OriginY = 5;
            ultraGridColumn15.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 18);
            ultraGridColumn15.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(213, 22);
            ultraGridColumn15.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn15.RowLayoutColumnInfo.SpanY = 1;
            appearance6.TextHAlignAsString = "Right";
            appearance6.TextVAlignAsString = "Middle";
            ultraGridColumn16.CellAppearance = appearance6;
            ultraGridColumn16.Format = "###,###,###";
            ultraGridColumn16.Header.VisiblePosition = 4;
            ultraGridColumn16.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn16.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn16.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn16.RowLayoutColumnInfo.OriginX = 3;
            ultraGridColumn16.RowLayoutColumnInfo.OriginY = 6;
            ultraGridColumn16.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 0);
            ultraGridColumn16.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn16.RowLayoutColumnInfo.SpanY = 1;
            appearance7.TextHAlignAsString = "Right";
            appearance7.TextVAlignAsString = "Middle";
            ultraGridColumn17.CellAppearance = appearance7;
            ultraGridColumn17.Format = "###,###,###";
            ultraGridColumn17.Header.VisiblePosition = 5;
            ultraGridColumn17.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn17.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn17.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn17.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn17.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn17.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(87, 25);
            ultraGridColumn17.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(237, 0);
            ultraGridColumn17.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn17.RowLayoutColumnInfo.SpanY = 1;
            appearance8.TextHAlignAsString = "Right";
            appearance8.TextVAlignAsString = "Middle";
            ultraGridColumn18.CellAppearance = appearance8;
            ultraGridColumn18.Format = "###,###,###";
            ultraGridColumn18.Header.VisiblePosition = 6;
            ultraGridColumn18.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn18.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn18.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn18.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn18.RowLayoutColumnInfo.OriginY = 3;
            ultraGridColumn18.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(87, 26);
            ultraGridColumn18.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(237, 0);
            ultraGridColumn18.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn18.RowLayoutColumnInfo.SpanY = 1;
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            ultraGridColumn19.CellAppearance = appearance9;
            ultraGridColumn19.Format = "###,###,###";
            ultraGridColumn19.Header.VisiblePosition = 7;
            ultraGridColumn19.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn19.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn19.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn19.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn19.RowLayoutColumnInfo.OriginY = 4;
            ultraGridColumn19.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(87, 18);
            ultraGridColumn19.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(190, 0);
            ultraGridColumn19.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn19.RowLayoutColumnInfo.SpanY = 1;
            appearance10.TextHAlignAsString = "Right";
            appearance10.TextVAlignAsString = "Middle";
            ultraGridColumn20.CellAppearance = appearance10;
            ultraGridColumn20.Format = "###,###,###";
            ultraGridColumn20.Header.VisiblePosition = 8;
            ultraGridColumn20.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn20.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn20.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn20.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn20.RowLayoutColumnInfo.OriginY = 5;
            ultraGridColumn20.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(87, 18);
            ultraGridColumn20.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(237, 22);
            ultraGridColumn20.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn20.RowLayoutColumnInfo.SpanY = 1;
            appearance11.TextHAlignAsString = "Right";
            appearance11.TextVAlignAsString = "Middle";
            ultraGridColumn21.CellAppearance = appearance11;
            ultraGridColumn21.Format = "###,###,###";
            ultraGridColumn21.Header.VisiblePosition = 9;
            ultraGridColumn21.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn21.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn21.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn21.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn21.RowLayoutColumnInfo.OriginY = 6;
            ultraGridColumn21.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(87, 26);
            ultraGridColumn21.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn21.RowLayoutColumnInfo.SpanY = 1;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21});
            appearance12.TextHAlignAsString = "Center";
            appearance12.TextVAlignAsString = "Middle";
            ultraGridGroup1.Header.Appearance = appearance12;
            ultraGridGroup1.Header.Caption = "구분";
            ultraGridGroup1.Key = "NewGroup0";
            ultraGridGroup1.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup1.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup1.RowLayoutGroupInfo.CellInsets.Right = 2;
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 2;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(175, 60);
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 3;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 2;
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            ultraGridGroup2.Header.Appearance = appearance13;
            ultraGridGroup2.Header.Caption = "월누계";
            ultraGridGroup2.Key = "NewGroup1";
            ultraGridGroup2.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup2.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 2;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup2.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(48, 30);
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 1;
            appearance14.TextHAlignAsString = "Center";
            appearance14.TextVAlignAsString = "Middle";
            ultraGridGroup3.Header.Appearance = appearance14;
            ultraGridGroup3.Header.Caption = "공급량(㎥)";
            ultraGridGroup3.Key = "NewGroup2";
            ultraGridGroup3.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup3.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 2;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 2;
            ultraGridGroup3.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(127, 60);
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 2;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 2;
            appearance15.TextHAlignAsString = "Center";
            appearance15.TextVAlignAsString = "Middle";
            ultraGridGroup4.Header.Appearance = appearance15;
            ultraGridGroup4.Header.Caption = "일  계";
            ultraGridGroup4.Key = "NewGroup3";
            ultraGridGroup4.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup4.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 2;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 2;
            ultraGridGroup4.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(48, 30);
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 1;
            appearance16.TextHAlignAsString = "Center";
            appearance16.TextVAlignAsString = "Middle";
            ultraGridGroup5.Header.Appearance = appearance16;
            ultraGridGroup5.Header.Caption = "누수량(㎥)";
            ultraGridGroup5.Key = "NewGroup4";
            ultraGridGroup5.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup5.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 2;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 4;
            ultraGridGroup5.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(127, 60);
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 2;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 2;
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            ultraGridGroup6.Header.Appearance = appearance17;
            ultraGridGroup6.Header.Caption = "월누계";
            ultraGridGroup6.Key = "NewGroup5";
            ultraGridGroup6.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup6.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 2;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 5;
            ultraGridGroup6.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(48, 30);
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 1;
            appearance18.TextHAlignAsString = "Center";
            appearance18.TextVAlignAsString = "Middle";
            ultraGridGroup7.Header.Appearance = appearance18;
            ultraGridGroup7.Header.Caption = "일  계";
            ultraGridGroup7.Key = "NewGroup6";
            ultraGridGroup7.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup7.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup7.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup7.RowLayoutGroupInfo.OriginX = 2;
            ultraGridGroup7.RowLayoutGroupInfo.OriginY = 4;
            ultraGridGroup7.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(48, 30);
            ultraGridGroup7.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup7.RowLayoutGroupInfo.SpanY = 1;
            appearance19.TextHAlignAsString = "Center";
            appearance19.TextVAlignAsString = "Middle";
            ultraGridGroup8.Header.Appearance = appearance19;
            ultraGridGroup8.Header.Caption = "유수율(%)";
            ultraGridGroup8.Key = "NewGroup7";
            ultraGridGroup8.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup8.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup8.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup8.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup8.RowLayoutGroupInfo.OriginY = 6;
            ultraGridGroup8.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(190, 30);
            ultraGridGroup8.RowLayoutGroupInfo.SpanX = 3;
            ultraGridGroup8.RowLayoutGroupInfo.SpanY = 1;
            appearance20.TextHAlignAsString = "Center";
            appearance20.TextVAlignAsString = "Middle";
            ultraGridGroup9.Header.Appearance = appearance20;
            ultraGridGroup9.Header.Caption = "공급량(합)";
            ultraGridGroup9.Key = "NewGroup9";
            ultraGridGroup9.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup9.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup9.RowLayoutGroupInfo.LabelSpan = 2;
            ultraGridGroup9.RowLayoutGroupInfo.OriginX = 4;
            ultraGridGroup9.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup9.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(70, 60);
            ultraGridGroup9.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup9.RowLayoutGroupInfo.SpanY = 2;
            appearance21.TextHAlignAsString = "Center";
            appearance21.TextVAlignAsString = "Middle";
            ultraGridGroup10.Header.Appearance = appearance21;
            ultraGridGroup10.Header.Caption = "정수장 유출";
            ultraGridGroup10.Key = "NewGroup8";
            ultraGridGroup10.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup10.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup10.RowLayoutGroupInfo.LabelSpan = 2;
            ultraGridGroup10.RowLayoutGroupInfo.OriginX = 3;
            ultraGridGroup10.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup10.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(90, 60);
            ultraGridGroup10.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup10.RowLayoutGroupInfo.SpanY = 2;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6,
            ultraGridGroup7,
            ultraGridGroup8,
            ultraGridGroup9,
            ultraGridGroup10});
            ultraGridBand1.MaxRows = 1;
            ultraGridBand1.MinRows = 1;
            ultraGridBand1.RowLayoutLabelStyle = Infragistics.Win.UltraWinGrid.RowLayoutLabelStyle.WithCellData;
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            appearance22.BackColor = System.Drawing.Color.White;
            appearance22.BackColor2 = System.Drawing.Color.White;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.ultraGrid1.DisplayLayout.SelectionOverlayColor = System.Drawing.Color.White;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(2, 2);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(389, 476);
            this.ultraGrid1.TabIndex = 22;
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1232, 111);
            this.searchBox1.TabIndex = 19;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ultraGrid1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(10, 151);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1232, 664);
            this.panel1.TabIndex = 23;
            // 
            // frmDailyReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1252, 825);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmDailyReport";
            this.Text = "관망운영일보";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private WaterNet.WV_Common.form.SearchBox searchBox1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Panel panel1;
    }
}