﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WaterNetCore;
using WaterNet.WV_Report.work;
using WaterNet.WV_Common.util;
using System.IO;
using WaterNet.WV_Common.enum1;
using System.Collections.Generic;
using EMFrame.log;
using System.Diagnostics;
using Infragistics.Win;

namespace WaterNet.WV_Report.form
{
    public partial class frmTimeSupply2 : Form, WaterNet.WaterNetCore.IForminterface
    {
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();
        private DataSet dataSet2 = new DataSet();

        public frmTimeSupply2()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmTimeSupply2_Load);
        }

        #region IForminterface 멤버

        public string FormID
        {
            get { return this.Text.ToString(); }
        }

        public string FormKey
        {
            get { return this.GetType().Namespace.ToString(); }
        }
        #endregion

        public void Open()
        {
        }

        private void frmTimeSupply2_Load(object sender, EventArgs e)
        {
            object o = EMFrame.statics.AppStatic.USER_MENU["시단위공급량2ToolStripMenuItem"];

            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }

            this.InitializeGrid();
            this.InitializeEvent();
            this.InitializeForm();

            //폼 로드 자동검색.
            this.searchBtn_Click(this.searchBtn, new EventArgs());
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetRowClick(this.ultraGrid1, false);

            this.ultraGrid1.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            //다중셀 드래그 선택가능하게.
            this.ultraGrid1.DisplayLayout.Override.SelectTypeCell = SelectType.ExtendedAutoDrag;

        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);

            //셀 수정 이벤트
            this.ultraGrid1.DoubleClickCell += new DoubleClickCellEventHandler(ultraGrid1_DoubleClickCell);
            this.ultraGrid1.AfterCellUpdate += new CellEventHandler(ultraGrid1_AfterCellUpdate);

            ////셀 드레그를 좌우로 못하게,
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);

            //셀수정 Context
            this.ultraGrid1.MouseDown += new MouseEventHandler(ultraGrid1_MouseDown);

            ////셀입력모드로 들어오기전에 태그 갯수를 조회해야한다.
            this.ultraGrid1.BeforeEnterEditMode += new CancelEventHandler(ultraGrid1_BeforeEnterEditMode);
            this.ultraGrid1.AfterExitEditMode += new EventHandler(ultraGrid1_AfterExitEditMode);
            this.ultraGrid1.KeyDown += new KeyEventHandler(ultraGrid1_KeyDown);


        }

        //블록 클릭시 태그정보 창 출력
        private void ultraGrid1_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            string loc_code = string.Empty;
            string tag_gbn = string.Empty;
            object ooo = e.Cell;

            switch (e.Cell.Column.Key)
            {
                case "LBLOCK":
                    if (e.Cell.Row.Cells["MBLOCK"].Value == DBNull.Value)
                        loc_code = e.Cell.Row.Cells["LOC_CODE"].Value.ToString();
                    else
                        loc_code = string.Empty;
                    if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "블록")
                        tag_gbn = "FRQ";
                    else if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "(배)유출")
                        tag_gbn = "SPL_O";
                    else if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "(배)유입")
                        tag_gbn = "SPL_I";
                    else if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "분기")
                        tag_gbn = "SPL_D";
                    break;

                case "MBLOCK":
                    if (e.Cell.Row.Cells["MBLOCK"].Value != DBNull.Value && e.Cell.Row.Cells["SBLOCK"].Value == DBNull.Value)
                        loc_code = e.Cell.Row.Cells["LOC_CODE"].Value.ToString();
                    else
                        loc_code = string.Empty;

                    if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "블록")
                        tag_gbn = "FRQ";
                    else if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "(배)유출")
                        tag_gbn = "SPL_O";
                    else if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "(배)유입")
                        tag_gbn = "SPL_I";
                    else if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "분기")
                        tag_gbn = "SPL_D";


                    break;

                case "SBLOCK":
                    if (e.Cell.Row.Cells["SBLOCK"].Value != DBNull.Value)
                        loc_code = e.Cell.Row.Cells["LOC_CODE"].Value.ToString();
                    else
                        loc_code = string.Empty;
                    if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "블록")
                        tag_gbn = "FRQ";
                    else if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "(배)유출")
                        tag_gbn = "SPL_O";
                    else if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "(배)유입")
                        tag_gbn = "SPL_I";
                    else if (e.Cell.Row.Cells["TAG_GBN"].Value.ToString() == "분기")
                        tag_gbn = "SPL_D";
                    break;
            }
            if (string.IsNullOrEmpty(loc_code) || loc_code.IndexOf("SUM") > -1) return;

            WV_Common.form.frmTagDescription form = new WaterNet.WV_Common.form.frmTagDescription(loc_code, tag_gbn);
            form.Show();
        }




        private MouseButtons pressedButton = MouseButtons.None;

        //셀수정 우클릭 메뉴 생성
        private void ultraGrid1_MouseDown(object sender, MouseEventArgs e)
        {
            this.pressedButton = e.Button;

            if (e.Button != MouseButtons.Right)
            {
                return;
            }

            if (this.ultraGrid1.Selected.Cells.Count <= 1)
            {
                return;
            }

            object o = EMFrame.statics.AppStatic.USER_MENU["시단위공급량ToolStripMenuItem"];

            if (!o.ToString().Equals("0"))
            {
                return;
            }

            if (this.ultraGrid1.Selected.Cells[0].Column.CellAppearance.BackColor != Color.Empty)
            {
                return;
            }

            ContextMenu contextMenu = new ContextMenu();
            contextMenu.MenuItems.Add("일괄수정", menuUtems1_Click);

            contextMenu.Show(this.ultraGrid1, new Point(e.X, e.Y));
        }

        //일괄수정일 경우 수정 팝업창을 생성한다.
        private void menuUtems1_Click(object sender, EventArgs e)
        {
            Hashtable parameter = new Hashtable();

            string tag_gbn = string.Empty;
            string tag_val = string.Empty;

            tag_gbn = this.ultraGrid1.Selected.Cells[0].Row.Cells["TAG_GBN"].Value.ToString();
            if (tag_gbn == "블록")
                tag_val = "FRQ";
            else if (tag_gbn == "(배)유출")
                tag_val = "SPL_O";
            else if (tag_gbn == "(배)유입")
                tag_val = "SPL_I";
            else
                tag_val = "SPL_D";

            parameter["TAG_GBN"] = tag_val;
            parameter["DATEE"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + this.ultraGrid1.Selected.Cells[0].Column.Header.Caption.ToString().Remove(2, 1);
            parameter["LOC_CODE"] = this.ultraGrid1.Selected.Cells[0].Row.Cells["LOC_CODE"].Value;


            string v_cell = this.ultraGrid1.Selected.Cells[0].Column.Header.Caption.ToString();
            string e_cell = this.ultraGrid1.Selected.Cells[this.ultraGrid1.Selected.Cells.Count - 1].Column.Header.Caption.ToString();

            //시작
            double v_time = Utils.ToDouble(v_cell.ToString().Substring(0, 2));

            //종료
            double e_time = Utils.ToDouble(e_cell.ToString().Substring(0, 2));

            if (v_time == e_time)
            {
                parameter["V_TIME"] = v_cell.ToString();
                parameter["E_TIME"] = v_cell.ToString();

                parameter["STARTDATE"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + v_cell.ToString().Remove(2, 1);
                parameter["ENDDATE"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + v_cell.ToString().Remove(2, 1);
            }

            if (v_time < e_time)
            {
                parameter["V_TIME"] = v_cell.ToString();
                parameter["E_TIME"] = e_cell.ToString();

                parameter["STARTDATE"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + v_cell.ToString().Remove(2, 1);
                parameter["ENDDATE"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + e_cell.ToString().Remove(2, 1);
            }
            if (v_time > e_time)
            {
                parameter["V_TIME"] = e_cell.ToString();
                parameter["E_TIME"] = v_cell.ToString();

                parameter["STARTDATE"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + e_cell.ToString().Remove(2, 1);
                parameter["ENDDATE"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + v_cell.ToString().Remove(2, 1);
            }

            frmValueEditPopup form = new frmValueEditPopup();
            form.SelectValueEdit(parameter, this.ultraGrid1.Selected.Cells);
            form.ShowDialog();
        }

        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (e.Type.FullName != "Infragistics.Win.UltraWinGrid.UltraGridCell")
            {
                return;
            }

            //첫 선택이면 선택으로 넘긴다.
            if (this.ultraGrid1.Selected.Cells.Count <= 1)
            {
                return;
            }

            if (this.ultraGrid1.Selected.Rows.Count != 0)
            { }

            UltraGridCell firstCell = this.ultraGrid1.Selected.Cells[0];
            UltraGridCell eventCell = this.ultraGrid1.Selected.Cells[this.ultraGrid1.Selected.Cells.Count - 1];



            //가로행 선택
            if (firstCell.Row.Index != eventCell.Row.Index)
            {
                eventCell.Selected = false;
                eventCell.Activated = false;
            }
            //세로행 선택 불가
            else if (firstCell.Row.Index == eventCell.Row.Index)
            {
                eventCell.Selected = true;
                eventCell.Activated = true;
            }
        }



        private bool isUserCellEdit = false;
        private bool isUserCellUpdate = false;
        private void ultraGrid1_AfterCellUpdate(object sender, CellEventArgs e)
        {


            if (this.ultraGrid1.DataSource == null || e.Cell.Column.CellAppearance.BackColor != Color.Empty)
            {
                return;
            }

            double origin = 0;
            double flow = Utils.ToDouble(e.Cell.Value);

            //비교하는거 수정 중 2014.04.21

            //문제점     : origin 변수에 자꾸 수정 후 값이 들어감,,
            //생각해낸점 : 디자이너에서 키값을 맞춰서 DB값을 가져오니 그리드 상에서 수정시 DataSet에 바로 업데이트 되는것 같음
            //             일단 값 수정하면 바로 DB내용도 바뀜;;

            //해결방안   : 쌩DB에서 원데이터 값을 가져올 수 있는 방안을 생각해내야함..
            //해결       : DataSet2 전역 변수로 설정 후 원본 값 비교

            string n = string.Empty;
            bool origin_n = true;
            bool flow_n = true;

            //컬럼 비교
            foreach (DataColumn dataColumn in dataSet2.Tables["BLOCK_INFO"].Columns)
            {
                if (dataColumn.ColumnName.ToString() == e.Cell.Column.ToString())
                {
                    n = dataColumn.ColumnName.ToString();
                    break;
                }
            }

            //LOC_CODE , TAG_GBN 비교
            foreach (DataRow dataRow in dataSet2.Tables["BLOCK_INFO"].Rows)
            {
                if (dataRow["LOC_CODE"].ToString() == e.Cell.Row.Cells["LOC_CODE"].Value.ToString())
                {
                    if (dataRow["TAG_GBN"].ToString() == e.Cell.Row.Cells["TAG_GBN"].Value.ToString())
                    {
                        if (dataRow[n] != DBNull.Value)
                        {
                            origin_n = false;
                        }
                        origin = Utils.ToDouble(dataRow[n]);
                    }
                }
            }

            //2011.3.31
            if (e.Cell.Value != DBNull.Value)
            {
                flow_n = false;
            }

            //2011.3.31
            if (flow_n && origin == 0 && flow == 0)
            {
                e.Cell.ToolTipText = "[결측]";
                e.Cell.Appearance.BackColor = Color.Red;
            }
            //else if (!origin_n && !flow_n && origin != 0 && flow != 0 && origin == flow)
            else if (!origin_n && !flow_n && origin == flow)
            {
                e.Cell.ToolTipText = origin.ToString();
                e.Cell.Appearance.BackColor = Color.Empty;
            }
            else
            {
                if (!origin_n && flow_n)
                {
                    e.Cell.ToolTipText = "[보정]\n원본값 : " + origin.ToString() + " --> " + "보정값 : 결측";
                }
                else if (origin_n && !flow_n)
                {
                    e.Cell.ToolTipText = "[보정]\n원본값 : 결측" + " --> " + "보정값 : " + flow.ToString();
                }
                else
                {
                    e.Cell.ToolTipText = "[보정]\n원본값 : " + origin.ToString() + " --> " + "보정값 : " + flow.ToString();
                }

                e.Cell.Appearance.BackColor = Color.SkyBlue;
            }

            // 그리드 상에서 값 수정 시 일계 값 적용
            double totalVal = 0;
            foreach (UltraGridCell gridCell in e.Cell.Row.Cells)
            {
                if ((gridCell.Column.Index != 0) && (gridCell.Column.Index != 1) && (gridCell.Column.Index != 2) && (gridCell.Column.Index != 3))
                {
                    if (gridCell.Column.CellAppearance.BackColor == Color.Empty)
                    {
                        totalVal += Utils.ToDouble(gridCell.Value);
                    }
                }
            }
            e.Cell.Row.Cells["DAYSUM"].Value = totalVal;

        }


        //태그갯수를 파악해서 태그가 1개가 아니면 수정못하게한다.
        private void ultraGrid1_BeforeEnterEditMode(object sender, CancelEventArgs e)
        {

            Hashtable parameter = new Hashtable();
            string tag_gbn = string.Empty;
            string tag_val = string.Empty;
            //선택된 셀의 tag_gbn 정보를 가져옴
            tag_gbn = this.ultraGrid1.ActiveRow.Cells["TAG_GBN"].Value.ToString();
            if (tag_gbn == "블록")
                tag_val = "FRQ";
            else if (tag_gbn == "(배)유출")
                tag_val = "SPL_O";
            else if (tag_gbn == "(배)유입")
                tag_val = "SPL_I";
            else
                tag_val = "SPL_D";

            parameter["TAG_GBN"] = tag_val;
            //선택된 셀의 loc_code 정보를 가져옴
            parameter["LOC_CODE"] = this.ultraGrid1.ActiveRow.Cells["LOC_CODE"].Value.ToString();

            //등록된 태그 갯수 조회.
            if (ReportWork.GetInstance().SelectTagCount(parameter).ToString() != "1")
            {
                MessageBox.Show("선택한 블록내에 다수의 관련 태그가 존재합니다.\n수정할 수 없습니다.");
                e.Cancel = true;
            }

            this.isUserCellEdit = true;
        }

        private void ultraGrid1_AfterExitEditMode(object sender, EventArgs e)
        {
            this.isUserCellEdit = false;
        }

        //...
        private void ultraGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.V)
            {
                //선택된 셀이 없으면 동작하지 않는다.
                if (this.ultraGrid1.ActiveCell == null)
                {
                    return;
                }

                //클립보드의 엑셀 선택 영역을 가져온다..
                //엑셀이 아니라면 값은 null 이며 동작하지 않는다.
                string[,] range = WV_Common.excel.Clipboard.GetStringRange();
                if (range == null)
                {
                    return;
                }

                int start_row_idx = this.ultraGrid1.ActiveCell.Row.Index;
                int start_cell_idx = 0;
                int column_index = 0;
                Dictionary<int, string> columns = new Dictionary<int, string>();

                UltraGridColumn column = this.ultraGrid1.DisplayLayout.Bands[0].Columns[0];
                column = column.GetRelatedVisibleColumn(VisibleRelation.First);

                while (null != column)
                {
                    if (column != null)
                    {
                        columns[column_index] = column.Key;

                        if (this.ultraGrid1.ActiveCell.Column.Key == column.Key)
                        {
                            start_cell_idx = column_index;
                        }
                    }

                    column = column.GetRelatedVisibleColumn(VisibleRelation.Next);
                    column_index++;
                }

                int target_row_idx = 0;
                int target_cell_idx = 0;

                for (int i = 0; i < range.GetLength(0); i++)
                {
                    for (int j = 0; j < range.GetLength(1); j++)
                    {
                        double result = 0;
                        if (range[i, j] != null)
                        {
                            if (!Double.TryParse(range[i, j], out result))
                            {
                                MessageBox.Show("선택하신 영역내에 숫자가 아닌 값이 존재합니다.");
                                return;
                            }
                        }
                    }
                }

                this.ultraGrid1.PerformAction(UltraGridAction.ExitEditMode);

                for (int i = start_row_idx; i < this.ultraGrid1.Rows.Count; i++)
                {
                    target_cell_idx = 0;

                    if (target_row_idx > range.GetLength(0) - 1)
                    {
                        break;
                    }

                    for (int j = start_cell_idx; j < columns.Count; j++)
                    {
                        if (target_cell_idx > range.GetLength(1) - 1)
                        {
                            break;
                        }

                        if (this.ultraGrid1.Rows[i].Cells[columns[j]].Column.CellAppearance.BackColor == Color.Empty)
                        {
                            this.ultraGrid1.Rows[i].Cells[columns[j]].Value = range[target_row_idx, target_cell_idx];
                        }

                        target_cell_idx++;
                    }

                    target_row_idx++;
                }
            }
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalType = WaterNet.WV_Common.enum1.INTERVAL_TYPE.SINGLE_DATE;
            this.searchBox1.IntervalText = "기준일자";

            this.searchBox1.SmallBlockContainer.Visible = false;
            this.searchBox1.CenterContainer.Visible = false;
            this.searchBox1.JungSuContainer.Visible = false;
            this.searchBox1.MiddleBlockContainer.Visible = false;

            //this.searchBox1.MiddleBlockObject.SelectedIndex = 0;
            this.searchBox1.SmallBlockObject.SelectedIndex = 0;

            //기준일자
            this.searchBox1.DateObject.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 엑셀버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelBtn_Click(object sender, EventArgs e)
        {
            //this.excelManager.Clear();
            //string resourceName = "TimeSupply_" + EMFrame.statics.AppStatic.USER_SGCCD;
            //object obj = Properties.Resources.ResourceManager.GetObject(resourceName, Properties.Resources.Culture);
            //if (obj == null)
            //{
            //    MessageBox.Show("보고서 양식이 등록되어 있지 않습니다.");
            //    return;
            //}
            //this.excelManager.Load(new MemoryStream(((byte[])(obj))), WORKBOOK_TYPE.TEMPLATE);
            //this.excelManager.AddWorksheet(this.ultraGrid1, "temp");
            //this.excelManager.WriteContentToTemplete(0, 0, 6, 0);
            //string value = "조회 날자 " + ((DateTime)this.searchBox1.DateObject.Value).ToString("[yyyy/MM/dd]");
            //this.excelManager.WriteLine(0, 2, 0, value, WORKBOOK_TYPE.TEMPLATE);
            //this.excelManager.Open("시단위공급량", WORKBOOK_TYPE.TEMPLATE);
            Cursor cursor = this.Cursor;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ExpoterExcel();
            }
            catch (Exception e1)
            {
                Logger.Error(e1);
            }
            finally
            {
                this.Cursor = cursor;
            }
        }

        private void ExpoterExcel()
        {
            DataSet dataSet = this.ultraGrid1.DataSource as DataSet;

            int tot = 0;
            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                if (gridRow.Cells["DAYSUM"].Value == DBNull.Value)
                {
                    continue;
                }
                tot += Convert.ToInt32(gridRow.Cells["DAYSUM"].Value);
            }
            if (tot == 0)
            {
                return;
            }

            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.WorkbookBeforeClose += new Microsoft.Office.Interop.Excel.AppEvents_WorkbookBeforeCloseEventHandler(xlApp_WorkbookBeforeClose);

            if (xlApp == null)
            {
                return;
            }

            Microsoft.Office.Interop.Excel.Workbooks workbooks = xlApp.Workbooks;
            Microsoft.Office.Interop.Excel.Workbook workbook = workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];
            Microsoft.Office.Interop.Excel.PageSetup print = worksheet.PageSetup;   //프린트 설정
            Microsoft.Office.Interop.Excel.Range range = xlApp.get_Range("B2", "AL2");  //병합
            Microsoft.Office.Interop.Excel.Range rangeCell;

            //-----------엑셀 제어
            int columnCount = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Count; //30 고정
            int rowCount = this.ultraGrid1.Rows.Count; //블록갯수에 따라 동적으로 변함

            //틀 고정
            worksheet.Application.ActiveWindow.SplitRow = 6;
            worksheet.Application.ActiveWindow.SplitColumn = 4;
            worksheet.Application.ActiveWindow.FreezePanes = true;

            //날짜 컬럼 사이즈
            rangeCell = worksheet.get_Range(worksheet.Cells[2, 1], worksheet.Cells[44, 1]);
            rangeCell.ColumnWidth = 8.11;

            //타이틀 제목 병합
            rangeCell = worksheet.get_Range(worksheet.Cells[2, 2], worksheet.Cells[2, columnCount - 1]);
            rangeCell.Font.Size = 30;
            rangeCell.Font.Bold = true;
            rangeCell.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
            rangeCell.Font.Name = "돋음";
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데 정렬
            rangeCell.Merge(true);

            //블록 병합
            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[5, 3]);
            rangeCell.Merge(false);

            //시간 병합
            rangeCell = worksheet.get_Range(worksheet.Cells[5, 4], worksheet.Cells[5, 28]);
            rangeCell.Merge(false);


            //시간데이터 설정
            rangeCell = worksheet.get_Range(worksheet.Cells[6, 4], worksheet.Cells[6, 29]); //24:00:00 표현 됫던거 수정
            rangeCell.NumberFormat = @"[h]:mm";

            //데이터 범위설정
            rangeCell = worksheet.get_Range(worksheet.Cells[7, 5], worksheet.Cells[rowCount + 10, columnCount]);
            rangeCell.NumberFormat = @"#,###,##0"; //숫자 범주


            //데이터 오른쪽정렬
            rangeCell = worksheet.get_Range(worksheet.Cells[7, 2], worksheet.Cells[rowCount + 10, columnCount - 1]);
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlRight;
            rangeCell.ColumnWidth = 6.44; //컬럼 넓이
            rangeCell.RowHeight = 25.5;

            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[6, columnCount]);
            rangeCell.RowHeight = 13.5;   //구분 로우 높이
            rangeCell = worksheet.get_Range(worksheet.Cells[4, 1], worksheet.Cells[4, columnCount]);
            rangeCell.RowHeight = 0;

            //프린트 여백
            print.LeftMargin = 0;
            print.RightMargin = 0;
            print.TopMargin = 0;
            print.BottomMargin = 0;

            print.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;   //프린트 가로로 출력
            print.Zoom = AutoSize;    //프린트 비율
            print.CenterVertically = false;  //세로 자동맞춤
            print.CenterHorizontally = true;    //가로 자동맞춤

            range.Cells[1, 1] = "일별 시단위 데이터 조회";
            range.Cells[2, 0] = "조회 날짜 " + ((DateTime)searchBox1.DateObject.Value).ToString("[yyyy/MM/dd]");

            range.Cells[4, 0] = "블 록";
            range.Cells[4, 3] = "시 간";

            range.Cells[5, 0] = "대블록";
            rangeCell.ColumnWidth = 9;
            range.Cells[5, 1] = "중블록";
            rangeCell.ColumnWidth = 9;
            range.Cells[5, 2] = "소블록";
            rangeCell.ColumnWidth = 9;
            range.Cells[5, 3] = "구 분";
            rangeCell.ColumnWidth = 9;

            range.Cells[5, 4] = "01:00"; range.Cells[5, 5] = "02:00";
            range.Cells[5, 6] = "03:00"; range.Cells[5, 7] = "04:00";
            range.Cells[5, 8] = "05:00"; range.Cells[5, 9] = "06:00";
            range.Cells[5, 10] = "07:00"; range.Cells[5, 11] = "08:00";
            range.Cells[5, 12] = "09:00"; range.Cells[5, 13] = "10:00";
            range.Cells[5, 14] = "11:00"; range.Cells[5, 15] = "12:00";
            range.Cells[5, 16] = "13:00"; range.Cells[5, 17] = "14:00";
            range.Cells[5, 18] = "15:00"; range.Cells[5, 19] = "16:00";
            range.Cells[5, 20] = "17:00"; range.Cells[5, 21] = "18:00";
            range.Cells[5, 22] = "19:00"; range.Cells[5, 23] = "20:00";
            range.Cells[5, 24] = "21:00"; range.Cells[5, 25] = "22:00";
            range.Cells[5, 26] = "23:00"; range.Cells[5, 27] = "24:00";
            range.Cells[5, 28] = "일 계";


            //대,중 블록 구분 후 병합 하기 위한 변수 선언
            int Lcount = 1;
            int Mcount = 0;
            int Scount = 1;
            int count = 1;

            string compare = "Compare";
            string compare2 = "Compare";
            string DS_Compare = "Compare";

            int loop = 1;
            int loop2 = 0;
            int temp = 1;
            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                //그리드 상에서 일계 값이 없다면 Hide 표시해주는 기능과 맞춰 주기 위해 continue 선언
                if (gridRow.Cells["DAYSUM"].Value.ToString() == "")
                {
                    continue;
                }
                else //그리드 상에서 다음 나오는 DAYSUM 값이이 같을 시엔 hide 시키려는 기능과 맞춰주기 위해 선언
                {
                    if (gridRow.Cells["DAYSUM"].Value.ToString() != DS_Compare)
                    {
                        DS_Compare = gridRow.Cells["DAYSUM"].Value.ToString();
                    }
                    else
                    {   //소블록 값 출력 위해 추가
                        if ((gridRow.Cells["MBLOCK"].Value.ToString() != DBNull.Value.ToString()) && (gridRow.Cells["SBLOCK"].Value.ToString() != DBNull.Value.ToString())
                        && (gridRow.Cells["DAYSUM"].Value.ToString() != DBNull.Value.ToString()))
                        {
                        }
                        else
                        {
                            continue;
                        }
                    }


                }

                //대블록
                if ((gridRow.Cells["LBLOCK"].Value.ToString() != ""))
                {
                    if (compare == gridRow.Cells["LBLOCK"].Value.ToString())
                    {
                        loop++;
                        rangeCell = worksheet.get_Range(worksheet.Cells[7, 1], worksheet.Cells[6 + loop, 1]);
                        rangeCell.Merge(false);
                    }
                    else
                    {
                        compare = gridRow.Cells["LBLOCK"].Value.ToString();
                    }
                    worksheet.Cells[6 + Lcount, 1] = gridRow.Cells["LBLOCK"].Value.ToString();
                    Lcount++;
                }


                //중블록 갯수 파악 후 병합
                if (gridRow.Cells["MBLOCK"].Value.ToString() == "")
                {
                    Mcount++;
                    rangeCell = worksheet.get_Range(worksheet.Cells[7, 2], worksheet.Cells[6 + Mcount, 2]);
                    rangeCell.Merge(false);
                }
                else if (gridRow.Cells["MBLOCK"].Value.ToString() != compare2)
                {
                    compare2 = gridRow.Cells["MBLOCK"].Value.ToString();
                    Mcount = loop2;
                    loop2++;
                }
                else if (gridRow.Cells["MBLOCK"].Value.ToString() == compare2)
                {
                    loop2++;
                    rangeCell = worksheet.get_Range(worksheet.Cells[7 + (Mcount + 1), 2], worksheet.Cells[6 + (loop2 + 1), 2]);
                    rangeCell.Merge(false);
                }
                // 병합 한 뒤 값 삽입 
                if ((gridRow.Cells["MBLOCK"].Value.ToString() == ""))
                {
                    temp++;
                }
                else
                {
                    worksheet.Cells[6 + temp, 2] = gridRow.Cells["MBLOCK"].Value.ToString();
                    temp++;
                }


                //소블록
                if ((gridRow.Cells["SBLOCK"].Value.ToString() == ""))
                {
                    Scount++;
                }
                else
                {
                    worksheet.Cells[6 + Scount, 3] = gridRow.Cells["SBLOCK"].Value.ToString();
                    Scount++;
                }
                //구분 , 시간별 데이터 값 설정
                worksheet.Cells[6 + count, 4] = gridRow.Cells["TAG_GBN"].Value.ToString();
                worksheet.Cells[6 + count, 5] = gridRow.Cells["HH01"].Value.ToString();
                worksheet.Cells[6 + count, 6] = gridRow.Cells["HH02"].Value.ToString();
                worksheet.Cells[6 + count, 7] = gridRow.Cells["HH03"].Value.ToString();
                worksheet.Cells[6 + count, 8] = gridRow.Cells["HH04"].Value.ToString();
                worksheet.Cells[6 + count, 9] = gridRow.Cells["HH05"].Value.ToString();
                worksheet.Cells[6 + count, 10] = gridRow.Cells["HH06"].Value.ToString();
                worksheet.Cells[6 + count, 11] = gridRow.Cells["HH07"].Value.ToString();
                worksheet.Cells[6 + count, 12] = gridRow.Cells["HH08"].Value.ToString();
                worksheet.Cells[6 + count, 13] = gridRow.Cells["HH09"].Value.ToString();
                worksheet.Cells[6 + count, 14] = gridRow.Cells["HH10"].Value.ToString();
                worksheet.Cells[6 + count, 15] = gridRow.Cells["HH11"].Value.ToString();
                worksheet.Cells[6 + count, 16] = gridRow.Cells["HH12"].Value.ToString();
                worksheet.Cells[6 + count, 17] = gridRow.Cells["HH13"].Value.ToString();
                worksheet.Cells[6 + count, 18] = gridRow.Cells["HH14"].Value.ToString();
                worksheet.Cells[6 + count, 19] = gridRow.Cells["HH15"].Value.ToString();
                worksheet.Cells[6 + count, 20] = gridRow.Cells["HH16"].Value.ToString();
                worksheet.Cells[6 + count, 21] = gridRow.Cells["HH17"].Value.ToString();
                worksheet.Cells[6 + count, 22] = gridRow.Cells["HH18"].Value.ToString();
                worksheet.Cells[6 + count, 23] = gridRow.Cells["HH19"].Value.ToString();
                worksheet.Cells[6 + count, 24] = gridRow.Cells["HH20"].Value.ToString();
                worksheet.Cells[6 + count, 25] = gridRow.Cells["HH21"].Value.ToString();
                worksheet.Cells[6 + count, 26] = gridRow.Cells["HH22"].Value.ToString();
                worksheet.Cells[6 + count, 27] = gridRow.Cells["HH23"].Value.ToString();
                worksheet.Cells[6 + count, 28] = gridRow.Cells["HH24"].Value.ToString();
                worksheet.Cells[6 + count, 29] = gridRow.Cells["DAYSUM"].Value.ToString();
                count++;

            }

            int iii = 0;
            foreach (UltraGridColumn gridColumn in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
            {
                if (gridColumn.Index == 5)
                {
                    worksheet.Cells[6 + count, 4] = "합  계";
                    worksheet.Cells[(6 + count) + 1, 4] = "최대값";
                    worksheet.Cells[(6 + count) + 2, 4] = "최소값";
                    worksheet.Cells[(6 + count) + 3, 4] = "평  균";

                    rangeCell = worksheet.get_Range(worksheet.Cells[6 + count, 1], worksheet.Cells[6 + count, 4]);
                    rangeCell.Merge(false);


                    rangeCell = worksheet.get_Range(worksheet.Cells[(6 + count) + 1, 1], worksheet.Cells[(6 + count) + 1, 4]);
                    rangeCell.Merge(false);


                    rangeCell = worksheet.get_Range(worksheet.Cells[(6 + count) + 2, 1], worksheet.Cells[(6 + count) + 2, 4]);
                    rangeCell.Merge(false);


                    rangeCell = worksheet.get_Range(worksheet.Cells[(6 + count) + 3, 1], worksheet.Cells[(6 + count) + 3, 4]);
                    rangeCell.Merge(false);

                }

                if (gridColumn.Index >= 6)
                {
                    iii++;
                    worksheet.Cells[6 + count, 4 + iii] = this.ultraGrid1.Rows.SummaryValues[gridColumn.Key + "_SUM"].Value.ToString();
                    worksheet.Cells[(6 + count) + 1, 4 + iii] = this.ultraGrid1.Rows.SummaryValues[gridColumn.Key + "_MAX"].Value.ToString();
                    worksheet.Cells[(6 + count) + 2, 4 + iii] = this.ultraGrid1.Rows.SummaryValues[gridColumn.Key + "_MIN"].Value.ToString();
                    worksheet.Cells[(6 + count) + 3, 4 + iii] = this.ultraGrid1.Rows.SummaryValues[gridColumn.Key + "_AVERAGE"].Value.ToString();
                }
            }

            // 전체 범위 설정
            rangeCell = worksheet.get_Range(worksheet.Cells[5, 1], worksheet.Cells[count + 9, columnCount - 1]); //맨윗 설정, 아래 설정
            rangeCell.Font.Name = "돋움";
            // 전체 범위안쪽 테두리 설정
            rangeCell.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            rangeCell.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
            rangeCell.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;  //가운데정렬

            // 전체 범위 바깥쪽 테두리설정
            rangeCell.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin,
                Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic);



            xlApp.Visible = true;
        }

        private void xlApp_WorkbookBeforeClose(Microsoft.Office.Interop.Excel.Workbook Wb, ref bool Cancel)
        {
            Process[] ExcelPros = Process.GetProcessesByName("EXCEL");

            for (int i = 0; i < ExcelPros.Length; i++)
            {
                Console.WriteLine(ExcelPros[i].MainWindowTitle);

                if (ExcelPros[i].MainWindowTitle == "")
                {
                    ExcelPros[i].Kill();
                }
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            DialogResult qe = MessageBox.Show("저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //태그구분을 위해 변수값 선언
            string tag_gbn = string.Empty;
            string tag_val = string.Empty;
            //값 저장 시 기존에 있던 work_desc라는 변수에 어떤 블록값을 변경 했는지
            //알려주기위해 사용
            string block_name = string.Empty;


            if (qe == DialogResult.Yes)
            {
                Cursor currentCursor = this.Cursor;
                this.Cursor = Cursors.WaitCursor;

                try
                {
                    foreach (UltraGridRow row in this.ultraGrid1.Rows)
                    {
                        foreach (UltraGridCell cell in row.Cells)
                        {
                            if (cell.Appearance.BackColor == Color.SkyBlue)
                            {
                                Hashtable parameter = new Hashtable();
                                tag_gbn = cell.Row.Cells["TAG_GBN"].Value.ToString();
                                if (tag_gbn == "블록")
                                    tag_val = "FRQ";
                                else if (tag_gbn == "(배)유출")
                                    tag_val = "SPL_O";
                                else if (tag_gbn == "(배)유입")
                                    tag_val = "SPL_I";
                                else
                                    tag_val = "SPL_D";

                                parameter["TAG_GBN"] = tag_val;
                                parameter["LOC_CODE"] = cell.Row.Cells["LOC_CODE"].Value.ToString();
                                
                                //24:00값 수정시 수정이 안되는 현상이 발생하여 수정
                                if (cell.Column.Header.Caption.ToString() == "24:00")
                                {
                                    parameter["DATEE"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).AddDays(1).ToString("yyyyMMdd") + "0000";
                                    parameter["V_TIME"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).AddDays(1).ToString("yyyyMMdd") + "0000";
                                    parameter["E_TIME"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).AddDays(1).ToString("yyyyMMdd") + "0000";
                                }
                                else
                                {
                                    parameter["DATEE"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + cell.Column.Header.Caption.ToString().Remove(2, 1);
                                    parameter["V_TIME"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + cell.Column.Header.Caption.ToString().Remove(2, 1);
                                    parameter["E_TIME"] = Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyyMMdd") + cell.Column.Header.Caption.ToString().Remove(2, 1);
                                }
                                parameter["VALUE"] = cell.Value;

                                if ((cell.Row.Cells["LBLOCK"].Value.ToString() != string.Empty) && (cell.Row.Cells["MBLOCK"].Value.ToString() == string.Empty) && (cell.Row.Cells["SBLOCK"].Value.ToString() == string.Empty))
                                {
                                    block_name = cell.Row.Cells["LBOCK"].Value.ToString();
                                }
                                else if ((cell.Row.Cells["LBLOCK"].Value.ToString() != string.Empty) && (cell.Row.Cells["MBLOCK"].Value.ToString() != string.Empty) && (cell.Row.Cells["SBLOCK"].Value.ToString() == string.Empty))
                                {
                                    block_name = cell.Row.Cells["MBOCK"].Value.ToString();
                                }
                                else if ((cell.Row.Cells["LBLOCK"].Value.ToString() != string.Empty) && (cell.Row.Cells["MBLOCK"].Value.ToString() != string.Empty) && (cell.Row.Cells["SBLOCK"].Value.ToString() != string.Empty))
                                {
                                    block_name  = cell.Row.Cells["SBLOCK"].Value.ToString();
                                }


                                ReportWork.GetInstance().UpdateValueEdit(parameter);
                                                                                              

                                string work_desc = block_name + " " + Convert.ToDateTime(this.searchBox1.DateObject.Value).ToString("yyyy-MM-dd") + "일 " + cell.Column.Header.Caption.ToString().Substring(0, 2) + "시 공급량 변경 (" + cell.ToolTipText.Replace("[보정]\n", "") + ")";
                                
                                
                                WaterNetCore.UserWorkHistoryLog.UPDATE(
                                    "시단위공급량ToolStripMenuItem",
                                    work_desc
                                );
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    this.Cursor = currentCursor;
                }

                this.searchBtn.PerformClick();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
        }

        /// <summary>
        /// 검색버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;


                this.SelectTimeSupply(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// 시단위 공급량을 검색한다.
        /// </summary>
        /// <param name="dataSet"></param>
        private void SelectTimeSupply(Hashtable parameter)
        {
            this.isUserCellUpdate = false;
            this.ultraGrid1.DataSource = ReportWork.GetInstance().SelectTimeSupply2(parameter);
            this.dataSet2 = ReportWork.GetInstance().SelectTimeSupply2(parameter);

            this.GridStyle();
            this.SetSummaryRows();
            this.isUserCellUpdate = true;
        }


        private void GridStyle()
        {
            //크기조절
            this.ultraGrid1.DisplayLayout.Override.DefaultRowHeight = 25;

            DataSet dataSet = (DataSet)this.ultraGrid1.DataSource;


            //울트라 그리드로 들어오는 값 병합시 사용
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].MergedCellEvaluationType = MergedCellEvaluationType.MergeSameText;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].MergedCellStyle = Infragistics.Win.UltraWinGrid.MergedCellStyle.Always;


            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].MergedCellEvaluationType = MergedCellEvaluationType.MergeSameText;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].MergedCellStyle = Infragistics.Win.UltraWinGrid.MergedCellStyle.Always;

            //울트라그리드 내 틀고정
            //01:00 ~ 24:00 까지의 데이터 확인시 해당 블록정보를 확인하기 위해 사용
            this.ultraGrid1.DisplayLayout.UseFixedHeaders = true;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].Group.Header.Fixed = true;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].Group.Header.Fixed = true;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].Group.Header.Fixed = true;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["TAG_GBN"].Group.Header.Fixed = true;

            //Null값 처리            
            int tot = 0;
            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                if (gridRow.Cells["DAYSUM"].Value == DBNull.Value)
                {
                    continue;
                }
                tot += Convert.ToInt32(gridRow.Cells["DAYSUM"].Value);
            }



            //데이터 가져와서 돌리다가
            //일계 값이 없으면
            //그 해당 열을 숨긴다..
            string DS_Compare = "compare";
            string Mblock_Compare = "Compare";
            int M_Value = 0;
            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                if (tot == 0)
                {

                }
                else
                {
                    //중블록 ~ 소블록 사이에 중복된 값 있으면 Hide
                    //if (gridRow.Cells["MBLOCK"].Value.ToString() == string.Empty)
                    //{
                    //}
                    //else if (gridRow.Cells["MBLOCK"].Value.ToString() != Mblock_Compare)
                    //{
                    //    Mblock_Compare = gridRow.Cells["MBLOCK"].Value.ToString();
                    //    M_Value = Convert.ToInt32(gridRow.Cells["DAYSUM"].Value);
                    //}
                    //else
                    //{
                    //    if (M_Value == Convert.ToInt32(gridRow.Cells["DAYSUM"].Value))
                    //    {
                    //        gridRow.Hidden = true;
                    //    }
                    //}

                    //다음에 나오는 블록값과 중복 시 Hidden
                    //if (gridRow.Cells["DAYSUM"].Value.ToString() != DS_Compare)
                    //{
                    //    DS_Compare = gridRow.Cells["DAYSUM"].Value.ToString();
                    //}
                    //else
                    //{
                    //    gridRow.Hidden = true;
                    //}

                    //이거 때문에 중블록 ~ 소블록 사이에 중복된 값 있으면 Hide 실행 안됨
                    //만일 Hide 시킬 일 있으면 이 부분 주석 처리 해야함.
                    if ((gridRow.Cells["MBLOCK"].Value.ToString() != DBNull.Value.ToString()) && (gridRow.Cells["SBLOCK"].Value.ToString() != DBNull.Value.ToString())
                        && (gridRow.Cells["DAYSUM"].Value.ToString() != DBNull.Value.ToString()))
                    {
                        gridRow.Hidden = false;
                    }

                    if (gridRow.Cells["DAYSUM"].Value.ToString() == DBNull.Value.ToString())
                    {
                        gridRow.Hidden = false; //NUll 값 나오게 하려면 풀어주면 됨
                    }
                }

                //그리드 상에 결측 표시
                foreach (UltraGridCell gridCell in gridRow.Cells)
                {   //20140512_일계값도 결측이라 표시되고 색상도 변하기에 막음
                    if ((gridCell.Column.Index != 1) && (gridCell.Column.Index != 2) && (gridCell.Column.Index != 3) && (gridCell.Column.Key != "DAYSUM"))
                    {//tot로 처리하여 결측 표시여부 선택
                        if (gridCell.Text == string.Empty && tot != 0)
                        {
                            gridCell.Appearance.BackColor = Color.Red;
                            gridCell.ToolTipText = "[결측]";
                        }
                    }
                }

            }

            //셀 클릭시 Edit
            for (int i = 6; i < 30; i++)
            {
                this.ultraGrid1.DisplayLayout.Bands[0].Columns[i].CellClickAction = CellClickAction.Edit;
                this.ultraGrid1.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
            }


        }


        private void SetSummaryRows()
        {

            foreach (UltraGridColumn gridColumn in this.ultraGrid1.DisplayLayout.Bands[0].Columns)
            {
                SummarySettings summary = null;

                summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,###,##0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = gridColumn.Key + "_SUM";

                summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Maximum, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,###,##0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = gridColumn.Key + "_MAX";

                summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Minimum, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,###,##0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = gridColumn.Key + "_MIN";

                summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Average, gridColumn, SummaryPosition.UseSummaryPositionColumn);
                summary.DisplayFormat = "{0:###,###,##0}";
                summary.Appearance.BackColor = SystemColors.Control;
                summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
                summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                summary.Key = gridColumn.Key + "_AVERAGE";


                if ((gridColumn.Index == 1) || (gridColumn.Index == 2) || (gridColumn.Key == "DAYSUM"))
                {
                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_SUM"];
                    summary.DisplayFormat = " ";

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MAX"];
                    summary.DisplayFormat = " ";

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MIN"];
                    summary.DisplayFormat = " ";

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_AVERAGE"];
                    summary.DisplayFormat = " ";
                }

                if (gridColumn.Index == 3)
                {
                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_SUM"];
                    summary.DisplayFormat = "합계";
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MAX"];
                    summary.DisplayFormat = "최대값";
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_MIN"];
                    summary.DisplayFormat = "최소값";
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries[gridColumn.Key + "_AVERAGE"];
                    summary.DisplayFormat = "평균";
                    summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                }
                else if (gridColumn.Index == 0)
                {

                }


            }
        }







    }
}
