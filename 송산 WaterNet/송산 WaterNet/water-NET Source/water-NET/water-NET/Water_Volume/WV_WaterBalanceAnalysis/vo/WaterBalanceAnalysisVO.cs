﻿using System;
using WaterNet.WV_Common.util;
using System.Collections;

namespace WaterNet.WV_WaterBalanceAnalysis.vo
{
    public class WaterBalanceAnalysisVO
    {
        private string loc_code = string.Empty;
        private int year = 0; 

        private double total_water_ratio_pub = 0;
        private double total_water_ratio_charge = 0;
        private double total_water_ratio_leaks = 0;
        private double total_water_supplied = 0;
        private double total_effect_revenue = 0;
        private double total_revenue = 0;
        private double total_water_charge = 0;
        private double total_water_exported = 0;
        private double total_revenue_etc = 0;
        private double total_non_revenue = 0;
        private double total_mtr_undreg = 0;
        private double total_water_undertake = 0;
        private double total_water_pub = 0;
        private double total_unauth_consump = 0;
        private double total_non_effect_revenue = 0;
        private double total_leaks = 0;
        private double total_leaks_est = 0;
        private double total_water_discount = 0;
        private double total_non_effect_revenue_etc = 0;
        private double m1_water_ratio_pub = 0;
        private double m1_water_ratio_charge = 0;
        private double m1_water_ratio_leaks = 0;
        private double m1_water_supplied = 100;
        private double m1_effect_revenue = 0;
        private double m1_revenue = 50;
        private double m1_water_charge = 0;
        private double m1_water_exported = 0;
        private double m1_revenue_etc = 0;
        private double m1_non_revenue = 0;
        private double m1_mtr_undreg = 0;
        private double m1_water_undertake = 0;
        private double m1_water_pub = 0;
        private double m1_unauth_consump = 0;
        private double m1_non_effect_revenue = 0;
        private double m1_leaks = 0;
        private double m1_leaks_est = 0;
        private double m1_water_discount = 0;
        private double m1_non_effect_revenue_etc = 0;
        private double m2_water_ratio_pub = 0;
        private double m2_water_ratio_charge = 0;
        private double m2_water_ratio_leaks = 0;
        private double m2_water_supplied = 0;
        private double m2_effect_revenue = 0;
        private double m2_revenue = 0;
        private double m2_water_charge = 0;
        private double m2_water_exported = 0;
        private double m2_revenue_etc = 0;
        private double m2_non_revenue = 0;
        private double m2_mtr_undreg = 0;
        private double m2_water_undertake = 0;
        private double m2_water_pub = 0;
        private double m2_unauth_consump = 0;
        private double m2_non_effect_revenue = 0;
        private double m2_leaks = 0;
        private double m2_leaks_est = 0;
        private double m2_water_discount = 0;
        private double m2_non_effect_revenue_etc = 0;
        private double m3_water_ratio_pub = 0;
        private double m3_water_ratio_charge = 0;
        private double m3_water_ratio_leaks = 0;
        private double m3_water_supplied = 0;
        private double m3_effect_revenue = 0;
        private double m3_revenue = 0;
        private double m3_water_charge = 0;
        private double m3_water_exported = 0;
        private double m3_revenue_etc = 0;
        private double m3_non_revenue = 0;
        private double m3_mtr_undreg = 0;
        private double m3_water_undertake = 0;
        private double m3_water_pub = 0;
        private double m3_unauth_consump = 0;
        private double m3_non_effect_revenue = 0;
        private double m3_leaks = 0;
        private double m3_leaks_est = 0;
        private double m3_water_discount = 0;
        private double m3_non_effect_revenue_etc = 0;
        private double m4_water_ratio_pub = 0;
        private double m4_water_ratio_charge = 0;
        private double m4_water_ratio_leaks = 0;
        private double m4_water_supplied = 0;
        private double m4_effect_revenue = 0;
        private double m4_revenue = 0;
        private double m4_water_charge = 0;
        private double m4_water_exported = 0;
        private double m4_revenue_etc = 0;
        private double m4_non_revenue = 0;
        private double m4_mtr_undreg = 0;
        private double m4_water_undertake = 0;
        private double m4_water_pub = 0;
        private double m4_unauth_consump = 0;
        private double m4_non_effect_revenue = 0;
        private double m4_leaks = 0;
        private double m4_leaks_est = 0;
        private double m4_water_discount = 0;
        private double m4_non_effect_revenue_etc = 0;
        private double m5_water_ratio_pub = 0;
        private double m5_water_ratio_charge = 0;
        private double m5_water_ratio_leaks = 0;
        private double m5_water_supplied = 0;
        private double m5_effect_revenue = 0;
        private double m5_revenue = 0;
        private double m5_water_charge = 0;
        private double m5_water_exported = 0;
        private double m5_revenue_etc = 0;
        private double m5_non_revenue = 0;
        private double m5_mtr_undreg = 0;
        private double m5_water_undertake = 0;
        private double m5_water_pub = 0;
        private double m5_unauth_consump = 0;
        private double m5_non_effect_revenue = 0;
        private double m5_leaks = 0;
        private double m5_leaks_est = 0;
        private double m5_water_discount = 0;
        private double m5_non_effect_revenue_etc = 0;
        private double m6_water_ratio_pub = 0;
        private double m6_water_ratio_charge = 0;
        private double m6_water_ratio_leaks = 0;
        private double m6_water_supplied = 0;
        private double m6_effect_revenue = 0;
        private double m6_revenue = 0;
        private double m6_water_charge = 0;
        private double m6_water_exported = 0;
        private double m6_revenue_etc = 0;
        private double m6_non_revenue = 0;
        private double m6_mtr_undreg = 0;
        private double m6_water_undertake = 0;
        private double m6_water_pub = 0;
        private double m6_unauth_consump = 0;
        private double m6_non_effect_revenue = 0;
        private double m6_leaks = 0;
        private double m6_leaks_est = 0;
        private double m6_water_discount = 0;
        private double m6_non_effect_revenue_etc = 0;
        private double m7_water_ratio_pub = 0;
        private double m7_water_ratio_charge = 0;
        private double m7_water_ratio_leaks = 0;
        private double m7_water_supplied = 0;
        private double m7_effect_revenue = 0;
        private double m7_revenue = 0;
        private double m7_water_charge = 0;
        private double m7_water_exported = 0;
        private double m7_revenue_etc = 0;
        private double m7_non_revenue = 0;
        private double m7_mtr_undreg = 0;
        private double m7_water_undertake = 0;
        private double m7_water_pub = 0;
        private double m7_unauth_consump = 0;
        private double m7_non_effect_revenue = 0;
        private double m7_leaks = 0;
        private double m7_leaks_est = 0;
        private double m7_water_discount = 0;
        private double m7_non_effect_revenue_etc = 0;
        private double m8_water_ratio_pub = 0;
        private double m8_water_ratio_charge = 0;
        private double m8_water_ratio_leaks = 0;
        private double m8_water_supplied = 0;
        private double m8_effect_revenue = 0;
        private double m8_revenue = 0;
        private double m8_water_charge = 0;
        private double m8_water_exported = 0;
        private double m8_revenue_etc = 0;
        private double m8_non_revenue = 0;
        private double m8_mtr_undreg = 0;
        private double m8_water_undertake = 0;
        private double m8_water_pub = 0;
        private double m8_unauth_consump = 0;
        private double m8_non_effect_revenue = 0;
        private double m8_leaks = 0;
        private double m8_leaks_est = 0;
        private double m8_water_discount = 0;
        private double m8_non_effect_revenue_etc = 0;
        private double m9_water_ratio_pub = 0;
        private double m9_water_ratio_charge = 0;
        private double m9_water_ratio_leaks = 0;
        private double m9_water_supplied = 0;
        private double m9_effect_revenue = 0;
        private double m9_revenue = 0;
        private double m9_water_charge = 0;
        private double m9_water_exported = 0;
        private double m9_revenue_etc = 0;
        private double m9_non_revenue = 0;
        private double m9_mtr_undreg = 0;
        private double m9_water_undertake = 0;
        private double m9_water_pub = 0;
        private double m9_unauth_consump = 0;
        private double m9_non_effect_revenue = 0;
        private double m9_leaks = 0;
        private double m9_leaks_est = 0;
        private double m9_water_discount = 0;
        private double m9_non_effect_revenue_etc = 0;
        private double m10_water_ratio_pub = 0;
        private double m10_water_ratio_charge = 0;
        private double m10_water_ratio_leaks = 0;
        private double m10_water_supplied = 0;
        private double m10_effect_revenue = 0;
        private double m10_revenue = 0;
        private double m10_water_charge = 0;
        private double m10_water_exported = 0;
        private double m10_revenue_etc = 0;
        private double m10_non_revenue = 0;
        private double m10_mtr_undreg = 0;
        private double m10_water_undertake = 0;
        private double m10_water_pub = 0;
        private double m10_unauth_consump = 0;
        private double m10_non_effect_revenue = 0;
        private double m10_leaks = 0;
        private double m10_leaks_est = 0;
        private double m10_water_discount = 0;
        private double m10_non_effect_revenue_etc = 0;
        private double m11_water_ratio_pub = 0;
        private double m11_water_ratio_charge = 0;
        private double m11_water_ratio_leaks = 0;
        private double m11_water_supplied = 0;
        private double m11_effect_revenue = 0;
        private double m11_revenue = 0;
        private double m11_water_charge = 0;
        private double m11_water_exported = 0;
        private double m11_revenue_etc = 0;
        private double m11_non_revenue = 0;
        private double m11_mtr_undreg = 0;
        private double m11_water_undertake = 0;
        private double m11_water_pub = 0;
        private double m11_unauth_consump = 0;
        private double m11_non_effect_revenue = 0;
        private double m11_leaks = 0;
        private double m11_leaks_est = 0;
        private double m11_water_discount = 0;
        private double m11_non_effect_revenue_etc = 0;
        private double m12_water_ratio_pub = 0;
        private double m12_water_ratio_charge = 0;
        private double m12_water_ratio_leaks = 0;
        private double m12_water_supplied = 0;
        private double m12_effect_revenue = 0;
        private double m12_revenue = 0;
        private double m12_water_charge = 0;
        private double m12_water_exported = 0;
        private double m12_revenue_etc = 0;
        private double m12_non_revenue = 0;
        private double m12_mtr_undreg = 0;
        private double m12_water_undertake = 0;
        private double m12_water_pub = 0;
        private double m12_unauth_consump = 0;
        private double m12_non_effect_revenue = 0;
        private double m12_leaks = 0;
        private double m12_leaks_est = 0;
        private double m12_water_discount = 0;
        private double m12_non_effect_revenue_etc = 0;

        public string LOC_CODE
        {
            get { return loc_code; }
            set { loc_code = value; }
        }

        public void SetYear(int year)
        {
            this.year = year;
        }

        public int GetYear()
        {
            return this.year;
        }

        public void SetData(Hashtable parameter)
        {
            int month = Convert.ToDateTime(parameter["YEAR_MON"]).Month;

            double water_supplied = Utils.ToDouble(parameter["WATER_SUPPLIED"]);
            double water_charge = Utils.ToDouble(parameter["WATER_CHARGE"]);
            double water_exported = Utils.ToDouble(parameter["WATER_EXPORTED"]);
            double revenue_etc = Utils.ToDouble(parameter["REVENUE_ETC"]);
            double mtr_undreg = Utils.ToDouble(parameter["MTR_UNDREG"]);
            double water_undertake = Utils.ToDouble(parameter["WATER_UNDERTAKE"]);
            double water_pub = Utils.ToDouble(parameter["WATER_PUB"]);
            double unauth_consump = Utils.ToDouble(parameter["UNAUTH_CONSUMP"]);
            double leaks = Utils.ToDouble(parameter["LEAKS"]);
            double leaks_est = Utils.ToDouble(parameter["LEAKS_EST"]);
            double water_discount = Utils.ToDouble(parameter["WATER_DISCOUNT"]);
            double non_effect_revenue_etc = Utils.ToDouble(parameter["NON_EFFECT_REVENUE_ETC"]);

            switch(month)
            {
                case 1:
                    {
                        m1_water_supplied = water_supplied;
                        m1_water_charge = water_charge;
                        m1_water_exported = water_exported;
                        m1_revenue_etc = revenue_etc;
                        m1_mtr_undreg = mtr_undreg;
                        m1_water_undertake = water_undertake;
                        m1_water_pub = water_pub;
                        m1_unauth_consump = unauth_consump;
                        m1_leaks = leaks;
                        m1_leaks_est = leaks_est;
                        m1_water_discount = water_discount;
                        m1_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 2:
                    {
                        m2_water_supplied = water_supplied;
                        m2_water_charge = water_charge;
                        m2_water_exported = water_exported;
                        m2_revenue_etc = revenue_etc;
                        m2_mtr_undreg = mtr_undreg;
                        m2_water_undertake = water_undertake;
                        m2_water_pub = water_pub;
                        m2_unauth_consump = unauth_consump;
                        m2_leaks = leaks;
                        m2_leaks_est = leaks_est;
                        m2_water_discount = water_discount;
                        m2_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 3:
                    {
                        m3_water_supplied = water_supplied;
                        m3_water_charge = water_charge;
                        m3_water_exported = water_exported;
                        m3_revenue_etc = revenue_etc;
                        m3_mtr_undreg = mtr_undreg;
                        m3_water_undertake = water_undertake;
                        m3_water_pub = water_pub;
                        m3_unauth_consump = unauth_consump;
                        m3_leaks = leaks;
                        m3_leaks_est = leaks_est;
                        m3_water_discount = water_discount;
                        m3_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 4:
                    {
                        m4_water_supplied = water_supplied;
                        m4_water_charge = water_charge;
                        m4_water_exported = water_exported;
                        m4_revenue_etc = revenue_etc;
                        m4_mtr_undreg = mtr_undreg;
                        m4_water_undertake = water_undertake;
                        m4_water_pub = water_pub;
                        m4_unauth_consump = unauth_consump;
                        m4_leaks = leaks;
                        m4_leaks_est = leaks_est;
                        m4_water_discount = water_discount;
                        m4_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 5:
                    {
                        m5_water_supplied = water_supplied;
                        m5_water_charge = water_charge;
                        m5_water_exported = water_exported;
                        m5_revenue_etc = revenue_etc;
                        m5_mtr_undreg = mtr_undreg;
                        m5_water_undertake = water_undertake;
                        m5_water_pub = water_pub;
                        m5_unauth_consump = unauth_consump;
                        m5_leaks = leaks;
                        m5_leaks_est = leaks_est;
                        m5_water_discount = water_discount;
                        m5_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 6:
                    {
                        m6_water_supplied = water_supplied;
                        m6_water_charge = water_charge;
                        m6_water_exported = water_exported;
                        m6_revenue_etc = revenue_etc;
                        m6_mtr_undreg = mtr_undreg;
                        m6_water_undertake = water_undertake;
                        m6_water_pub = water_pub;
                        m6_unauth_consump = unauth_consump;
                        m6_leaks = leaks;
                        m6_leaks_est = leaks_est;
                        m6_water_discount = water_discount;
                        m6_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 7:
                    {
                        m7_water_supplied = water_supplied;
                        m7_water_charge = water_charge;
                        m7_water_exported = water_exported;
                        m7_revenue_etc = revenue_etc;
                        m7_mtr_undreg = mtr_undreg;
                        m7_water_undertake = water_undertake;
                        m7_water_pub = water_pub;
                        m7_unauth_consump = unauth_consump;
                        m7_leaks = leaks;
                        m7_leaks_est = leaks_est;
                        m7_water_discount = water_discount;
                        m7_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 8:
                    {
                        m8_water_supplied = water_supplied;
                        m8_water_charge = water_charge;
                        m8_water_exported = water_exported;
                        m8_revenue_etc = revenue_etc;
                        m8_mtr_undreg = mtr_undreg;
                        m8_water_undertake = water_undertake;
                        m8_water_pub = water_pub;
                        m8_unauth_consump = unauth_consump;
                        m8_leaks = leaks;
                        m8_leaks_est = leaks_est;
                        m8_water_discount = water_discount;
                        m8_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 9:
                    {
                        m9_water_supplied = water_supplied;
                        m9_water_charge = water_charge;
                        m9_water_exported = water_exported;
                        m9_revenue_etc = revenue_etc;
                        m9_mtr_undreg = mtr_undreg;
                        m9_water_undertake = water_undertake;
                        m9_water_pub = water_pub;
                        m9_unauth_consump = unauth_consump;
                        m9_leaks = leaks;
                        m9_leaks_est = leaks_est;
                        m9_water_discount = water_discount;
                        m9_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 10:
                    {
                        m10_water_supplied = water_supplied;
                        m10_water_charge = water_charge;
                        m10_water_exported = water_exported;
                        m10_revenue_etc = revenue_etc;
                        m10_mtr_undreg = mtr_undreg;
                        m10_water_undertake = water_undertake;
                        m10_water_pub = water_pub;
                        m10_unauth_consump = unauth_consump;
                        m10_leaks = leaks;
                        m10_leaks_est = leaks_est;
                        m10_water_discount = water_discount;
                        m10_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 11:
                    {
                        m11_water_supplied = water_supplied;
                        m11_water_charge = water_charge;
                        m11_water_exported = water_exported;
                        m11_revenue_etc = revenue_etc;
                        m11_mtr_undreg = mtr_undreg;
                        m11_water_undertake = water_undertake;
                        m11_water_pub = water_pub;
                        m11_unauth_consump = unauth_consump;
                        m11_leaks = leaks;
                        m11_leaks_est = leaks_est;
                        m11_water_discount = water_discount;
                        m11_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
                case 12:
                    {
                        m12_water_supplied = water_supplied;
                        m12_water_charge = water_charge;
                        m12_water_exported = water_exported;
                        m12_revenue_etc = revenue_etc;
                        m12_mtr_undreg = mtr_undreg;
                        m12_water_undertake = water_undertake;
                        m12_water_pub = water_pub;
                        m12_unauth_consump = unauth_consump;
                        m12_leaks = leaks;
                        m12_leaks_est = leaks_est;
                        m12_water_discount = water_discount;
                        m12_non_effect_revenue_etc = non_effect_revenue_etc;
                        break;
                    }
            }
        }

        public Hashtable GetData(int month)
        {
            Hashtable parameter = new Hashtable();
            parameter["LOC_CODE"] = this.LOC_CODE;
            parameter["YEAR_MON"] = this.year.ToString() + month.ToString("00");

            switch (month)
            {
                case 1:
                    {
                        parameter["WATER_CHARGE"] = this.M1_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M1_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M1_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M1_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M1_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M1_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M1_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M1_LEAKS;
                        parameter["LEAKS_EST"] = this.M1_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M1_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M1_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 2:
                    {
                        parameter["WATER_CHARGE"] = this.M2_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M2_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M2_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M2_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M2_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M2_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M2_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M2_LEAKS;
                        parameter["LEAKS_EST"] = this.M2_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M2_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M2_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 3:
                    {
                        parameter["WATER_CHARGE"] = this.M3_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M3_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M3_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M3_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M3_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M3_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M3_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M3_LEAKS;
                        parameter["LEAKS_EST"] = this.M3_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M3_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M3_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 4:
                    {
                        parameter["WATER_CHARGE"] = this.M4_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M4_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M4_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M4_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M4_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M4_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M4_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M4_LEAKS;
                        parameter["LEAKS_EST"] = this.M4_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M4_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M4_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 5:
                    {
                        parameter["WATER_CHARGE"] = this.M5_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M5_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M5_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M5_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M5_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M5_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M5_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M5_LEAKS;
                        parameter["LEAKS_EST"] = this.M5_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M5_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M5_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 6:
                    {
                        parameter["WATER_CHARGE"] = this.M6_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M6_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M6_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M6_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M6_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M6_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M6_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M6_LEAKS;
                        parameter["LEAKS_EST"] = this.M6_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M6_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M6_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 7:
                    {
                        parameter["WATER_CHARGE"] = this.M7_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M7_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M7_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M7_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M7_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M7_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M7_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M7_LEAKS;
                        parameter["LEAKS_EST"] = this.M7_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M7_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M7_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 8:
                    {
                        parameter["WATER_CHARGE"] = this.M8_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M8_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M8_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M8_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M8_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M8_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M8_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M8_LEAKS;
                        parameter["LEAKS_EST"] = this.M8_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M8_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M8_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 9:
                    {
                        parameter["WATER_CHARGE"] = this.M9_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M9_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M9_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M9_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M9_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M9_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M9_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M9_LEAKS;
                        parameter["LEAKS_EST"] = this.M9_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M9_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M9_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 10:
                    {
                        parameter["WATER_CHARGE"] = this.M10_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M10_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M10_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M10_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M10_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M10_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M10_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M10_LEAKS;
                        parameter["LEAKS_EST"] = this.M10_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M10_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M10_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 11:
                    {
                        parameter["WATER_CHARGE"] = this.M11_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M11_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M11_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M11_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M11_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M11_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M11_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M11_LEAKS;
                        parameter["LEAKS_EST"] = this.M11_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M11_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M11_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
                case 12:
                    {
                        parameter["WATER_CHARGE"] = this.M12_WATER_CHARGE;
                        parameter["WATER_EXPORTED"] = this.M12_WATER_EXPORTED;
                        parameter["REVENUE_ETC"] = this.M12_REVENUE_ETC;
                        parameter["MTR_UNDREG"] = this.M12_MTR_UNDREG;
                        parameter["WATER_UNDERTAKE"] = this.M12_WATER_UNDERTAKE;
                        parameter["WATER_PUB"] = this.M12_WATER_PUB;
                        parameter["UNAUTH_CONSUMP"] = this.M12_UNAUTH_CONSUMP;
                        parameter["LEAKS"] = this.M12_LEAKS;
                        parameter["LEAKS_EST"] = this.M12_LEAKS_EST;
                        parameter["WATER_DISCOUNT"] = this.M12_WATER_DISCOUNT;
                        parameter["NON_EFFECT_REVENUE_ETC"] = this.M12_NON_EFFECT_REVENUE_ETC;
                        break;
                    }
            }

            return parameter;
        }

        public double TOTAL_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((TOTAL_REVENUE + TOTAL_WATER_PUB + TOTAL_NON_EFFECT_REVENUE_ETC) / TOTAL_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { total_water_ratio_pub = value; }
        }

        private double TOTAL_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (TOTAL_REVENUE / TOTAL_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { total_water_ratio_charge = value; }
        }

        public double TOTAL_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (TOTAL_LEAKS / TOTAL_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { total_water_ratio_leaks = value; }
        }

        //생산량, 고정값
        public double TOTAL_WATER_SUPPLIED
        {
            get 
            {
                double result = 0;
                result = M1_WATER_SUPPLIED
                    + M2_WATER_SUPPLIED
                    + M3_WATER_SUPPLIED
                    + M4_WATER_SUPPLIED
                    + M5_WATER_SUPPLIED
                    + M6_WATER_SUPPLIED
                    + M7_WATER_SUPPLIED
                    + M8_WATER_SUPPLIED
                    + M9_WATER_SUPPLIED
                    + M10_WATER_SUPPLIED
                    + M11_WATER_SUPPLIED
                    + M12_WATER_SUPPLIED;
                return result; 
            }
            set { total_water_supplied = value; }
        }

        //유효수량, 유수수량 + 무수수량
        public double TOTAL_EFFECT_REVENUE
        {
            get 
            {
                double result = 0;
                result = TOTAL_REVENUE + TOTAL_NON_REVENUE;
                return result; 
            }
            set { total_effect_revenue = value; }
        }

        //유수수량, 요금수량 + 분수량 + 기타유수수량
        public double TOTAL_REVENUE
        {
            get 
            {
                double result = 0;
                result = TOTAL_WATER_CHARGE + TOTAL_WATER_EXPORTED + TOTAL_REVENUE_ETC;
                return result; 
            }
            set { total_revenue = value; }
        }

        //요금수량
        public double TOTAL_WATER_CHARGE
        {
            get 
            {
                double result = 0;
                result = M1_WATER_CHARGE
                    + M2_WATER_CHARGE
                    + M3_WATER_CHARGE
                    + M4_WATER_CHARGE
                    + M5_WATER_CHARGE
                    + M6_WATER_CHARGE
                    + M7_WATER_CHARGE
                    + M8_WATER_CHARGE
                    + M9_WATER_CHARGE
                    + M10_WATER_CHARGE
                    + M11_WATER_CHARGE
                    + M12_WATER_CHARGE;
                return result; 
            }
            set { total_water_charge = value; }
        }

        //분수량
        public double TOTAL_WATER_EXPORTED
        {
            get 
            {
                double result = 0;
                result = M1_WATER_EXPORTED
                    + M2_WATER_EXPORTED
                    + M3_WATER_EXPORTED
                    + M4_WATER_EXPORTED
                    + M5_WATER_EXPORTED
                    + M6_WATER_EXPORTED
                    + M7_WATER_EXPORTED
                    + M8_WATER_EXPORTED
                    + M9_WATER_EXPORTED
                    + M10_WATER_EXPORTED
                    + M11_WATER_EXPORTED
                    + M12_WATER_EXPORTED;
                return result; 
            }
            set { total_water_exported = value; }
        }

        //기타유수수량
        public double TOTAL_REVENUE_ETC
        {
            get 
            {
                double result = 0;
                result = M1_REVENUE_ETC
                    + M2_REVENUE_ETC
                    + M3_REVENUE_ETC
                    + M4_REVENUE_ETC
                    + M5_REVENUE_ETC
                    + M6_REVENUE_ETC
                    + M7_REVENUE_ETC
                    + M8_REVENUE_ETC
                    + M9_REVENUE_ETC
                    + M10_REVENUE_ETC
                    + M11_REVENUE_ETC
                    + M12_REVENUE_ETC;
                return result; 
            }
            set { total_revenue_etc = value; }
        }

        //무수수량
        public double TOTAL_NON_REVENUE
        {
            get 
            {
                double result = 0;
                result = TOTAL_MTR_UNDREG + TOTAL_WATER_UNDERTAKE + TOTAL_WATER_PUB + TOTAL_UNAUTH_CONSUMP;
                return result; 
            }
            set { total_non_revenue = value; }
        }

        //계량기불감수량
        public double TOTAL_MTR_UNDREG
        {
            get 
            {
                double result = 0;
                result = M1_MTR_UNDREG
                    + M2_MTR_UNDREG
                    + M3_MTR_UNDREG
                    + M4_MTR_UNDREG
                    + M5_MTR_UNDREG
                    + M6_MTR_UNDREG
                    + M7_MTR_UNDREG
                    + M8_MTR_UNDREG
                    + M9_MTR_UNDREG
                    + M10_MTR_UNDREG
                    + M11_MTR_UNDREG
                    + M12_MTR_UNDREG;
                return result; 
            }
            set { total_mtr_undreg = value; }
        }

        //수도사업용수량
        public double TOTAL_WATER_UNDERTAKE
        {
            get 
            {
                double result = 0;
                result = M1_WATER_UNDERTAKE
                    + M2_WATER_UNDERTAKE
                    + M3_WATER_UNDERTAKE
                    + M4_WATER_UNDERTAKE
                    + M5_WATER_UNDERTAKE
                    + M6_WATER_UNDERTAKE
                    + M7_WATER_UNDERTAKE
                    + M8_WATER_UNDERTAKE
                    + M9_WATER_UNDERTAKE
                    + M10_WATER_UNDERTAKE
                    + M11_WATER_UNDERTAKE
                    + M12_WATER_UNDERTAKE;
                return result; 
            }
            set { total_water_undertake = value; }
        }

        //공공수량
        public double TOTAL_WATER_PUB
        {
            get 
            {
                double result = 0;
                result = M1_WATER_PUB
                    + M2_WATER_PUB
                    + M3_WATER_PUB
                    + M4_WATER_PUB
                    + M5_WATER_PUB
                    + M6_WATER_PUB
                    + M7_WATER_PUB
                    + M8_WATER_PUB
                    + M9_WATER_PUB
                    + M10_WATER_PUB
                    + M11_WATER_PUB
                    + M12_WATER_PUB;
                return result; 
            }
            set { total_water_pub = value; }
        }

        //부정사용량
        public double TOTAL_UNAUTH_CONSUMP
        {
            get 
            {
                double result = 0;
                result = M1_UNAUTH_CONSUMP
                    + M2_UNAUTH_CONSUMP
                    + M3_UNAUTH_CONSUMP
                    + M4_UNAUTH_CONSUMP
                    + M5_UNAUTH_CONSUMP
                    + M6_UNAUTH_CONSUMP
                    + M7_UNAUTH_CONSUMP
                    + M8_UNAUTH_CONSUMP
                    + M9_UNAUTH_CONSUMP
                    + M10_UNAUTH_CONSUMP
                    + M11_UNAUTH_CONSUMP
                    + M12_UNAUTH_CONSUMP;
                return result; 
            }
            set { total_unauth_consump = value; }
        }

        //무효수량, 누수량 + 감액조정수량 + 기타무효수량
        public double TOTAL_NON_EFFECT_REVENUE
        {
            get 
            {
                double result = 0;
                result = TOTAL_LEAKS + TOTAL_WATER_DISCOUNT + TOTAL_NON_EFFECT_REVENUE_ETC;
                return result; 
            }
            set { total_non_effect_revenue = value; }
        }

        //누수량
        public double TOTAL_LEAKS
        {
            get 
            {
                double result = 0;
                result = TOTAL_WATER_SUPPLIED - (TOTAL_EFFECT_REVENUE + TOTAL_WATER_DISCOUNT + TOTAL_NON_EFFECT_REVENUE_ETC);
                return result; 
            }
            set { total_leaks = value; }
        }

        //누수량(추정)
        public double TOTAL_LEAKS_EST
        {
            get 
            {
                double result = 0;
                result = M1_LEAKS_EST
                    + M2_LEAKS_EST
                    + M3_LEAKS_EST
                    + M4_LEAKS_EST
                    + M5_LEAKS_EST
                    + M6_LEAKS_EST
                    + M7_LEAKS_EST
                    + M8_LEAKS_EST
                    + M9_LEAKS_EST
                    + M10_LEAKS_EST
                    + M11_LEAKS_EST
                    + M12_LEAKS_EST;
                return result; 
            }
            set { total_leaks_est = value; }
        }

        //감액조정수량
        public double TOTAL_WATER_DISCOUNT
        {
            get 
            {
                double result = 0;
                result = M1_WATER_DISCOUNT
                    + M2_WATER_DISCOUNT
                    + M3_WATER_DISCOUNT
                    + M4_WATER_DISCOUNT
                    + M5_WATER_DISCOUNT
                    + M6_WATER_DISCOUNT
                    + M7_WATER_DISCOUNT
                    + M8_WATER_DISCOUNT
                    + M9_WATER_DISCOUNT
                    + M10_WATER_DISCOUNT
                    + M11_WATER_DISCOUNT
                    + M12_WATER_DISCOUNT;
                return result; 
            }
            set { total_water_discount = value; }
        }

        //기타무효수량
        public double TOTAL_NON_EFFECT_REVENUE_ETC
        {
            get 
            {
                double result = 0;
                result = M1_NON_EFFECT_REVENUE_ETC
                    + M2_NON_EFFECT_REVENUE_ETC
                    + M3_NON_EFFECT_REVENUE_ETC
                    + M4_NON_EFFECT_REVENUE_ETC
                    + M5_NON_EFFECT_REVENUE_ETC
                    + M6_NON_EFFECT_REVENUE_ETC
                    + M7_NON_EFFECT_REVENUE_ETC
                    + M8_NON_EFFECT_REVENUE_ETC
                    + M9_NON_EFFECT_REVENUE_ETC
                    + M10_NON_EFFECT_REVENUE_ETC
                    + M11_NON_EFFECT_REVENUE_ETC
                    + M12_NON_EFFECT_REVENUE_ETC;
                return result; 
            }
            set { total_non_effect_revenue_etc = value; }
        }


        /////////1월
        public double M1_WATER_RATIO_PUB
        {
            get 
            {
                double result = 0;
                result = ((M1_REVENUE + M1_WATER_PUB + M1_NON_EFFECT_REVENUE_ETC) / M1_WATER_SUPPLIED)*100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result; 
            }
            set { m1_water_ratio_pub = value; }
        }

        private double M1_WATER_RATIO_CHARGE
        {
            get 
            {
                double result = 0;
                result = (M1_REVENUE / M1_WATER_SUPPLIED)*100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result; 
            }
            set { m1_water_ratio_charge = value; }
        }

        public double M1_WATER_RATIO_LEAKS
        {
            get 
            {
                double result = 0;
                result = (M1_LEAKS / M1_WATER_SUPPLIED)*100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result; 
            }
            set { m1_water_ratio_leaks = value; }
        }

        public double M1_WATER_SUPPLIED
        {
            get { return m1_water_supplied; }
            set { m1_water_supplied = value; }
        }

        public double M1_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M1_REVENUE + M1_NON_REVENUE;
                return result;
            }
            set { m1_effect_revenue = value; }
        }

        public double M1_REVENUE
        {
            get
            {
                double result = 0;
                result = M1_WATER_CHARGE + M1_WATER_EXPORTED + M1_REVENUE_ETC;
                return result;
            }
            set { m1_revenue = value; }
        }

        public double M1_WATER_CHARGE
        {
            get { return m1_water_charge; }
            set { m1_water_charge = value; }
        }

        public double M1_WATER_EXPORTED
        {
            get { return m1_water_exported; }
            set { m1_water_exported = value; }
        }

        public double M1_REVENUE_ETC
        {
            get { return m1_revenue_etc; }
            set { m1_revenue_etc = value; }
        }

        public double M1_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M1_MTR_UNDREG + M1_WATER_UNDERTAKE + M1_WATER_PUB + M1_UNAUTH_CONSUMP;
                return result;
            }
            set { m1_non_revenue = value; }
        }

        public double M1_MTR_UNDREG
        {
            get { return m1_mtr_undreg; }
            set { m1_mtr_undreg = value; }
        }

        public double M1_WATER_UNDERTAKE
        {
            get { return m1_water_undertake; }
            set { m1_water_undertake = value; }
        }

        public double M1_WATER_PUB
        {
            get { return m1_water_pub; }
            set { m1_water_pub = value; }
        }

        public double M1_UNAUTH_CONSUMP
        {
            get { return m1_unauth_consump; }
            set { m1_unauth_consump = value; }
        }

        public double M1_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M1_LEAKS + M1_WATER_DISCOUNT + M1_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m1_non_effect_revenue = value; }
        }

        public double M1_LEAKS
        {
            get
            {
                double result = 0;
                result = M1_WATER_SUPPLIED - (M1_EFFECT_REVENUE + M1_WATER_DISCOUNT + M1_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m1_leaks = value; }
        }

        public double M1_LEAKS_EST
        {
            get { return m1_leaks_est; }
            set { m1_leaks_est = value; }
        }

        public double M1_WATER_DISCOUNT
        {
            get { return m1_water_discount; }
            set { m1_water_discount = value; }
        }

        public double M1_NON_EFFECT_REVENUE_ETC
        {
            get { return m1_non_effect_revenue_etc; }
            set { m1_non_effect_revenue_etc = value; }
        }

        /////////2월
        public double M2_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M2_REVENUE + M2_WATER_PUB + M2_NON_EFFECT_REVENUE_ETC) / M2_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m2_water_ratio_pub = value; }
        }

        private double M2_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M2_REVENUE / M2_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m2_water_ratio_charge = value; }
        }

        public double M2_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M2_LEAKS / M2_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m2_water_ratio_leaks = value; }
        }

        public double M2_WATER_SUPPLIED
        {
            get { return m2_water_supplied; }
            set { m2_water_supplied = value; }
        }

        public double M2_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M2_REVENUE + M2_NON_REVENUE;
                return result;
            }
            set { m2_effect_revenue = value; }
        }

        public double M2_REVENUE
        {
            get
            {
                double result = 0;
                result = M2_WATER_CHARGE + M2_WATER_EXPORTED + M2_REVENUE_ETC;
                return result;
            }
            set { m2_revenue = value; }
        }

        public double M2_WATER_CHARGE
        {
            get { return m2_water_charge; }
            set { m2_water_charge = value; }
        }

        public double M2_WATER_EXPORTED
        {
            get { return m2_water_exported; }
            set { m2_water_exported = value; }
        }

        public double M2_REVENUE_ETC
        {
            get { return m2_revenue_etc; }
            set { m2_revenue_etc = value; }
        }

        public double M2_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M2_MTR_UNDREG + M2_WATER_UNDERTAKE + M2_WATER_PUB + M2_UNAUTH_CONSUMP;
                return result;
            }
            set { m2_non_revenue = value; }
        }

        public double M2_MTR_UNDREG
        {
            get { return m2_mtr_undreg; }
            set { m2_mtr_undreg = value; }
        }

        public double M2_WATER_UNDERTAKE
        {
            get { return m2_water_undertake; }
            set { m2_water_undertake = value; }
        }

        public double M2_WATER_PUB
        {
            get { return m2_water_pub; }
            set { m2_water_pub = value; }
        }

        public double M2_UNAUTH_CONSUMP
        {
            get { return m2_unauth_consump; }
            set { m2_unauth_consump = value; }
        }

        public double M2_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M2_LEAKS + M2_WATER_DISCOUNT + M2_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m2_non_effect_revenue = value; }
        }

        public double M2_LEAKS
        {
            get
            {
                double result = 0;
                result = M2_WATER_SUPPLIED - (M2_EFFECT_REVENUE + M2_WATER_DISCOUNT + M2_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m2_leaks = value; }
        }

        public double M2_LEAKS_EST
        {
            get { return m2_leaks_est; }
            set { m2_leaks_est = value; }
        }

        public double M2_WATER_DISCOUNT
        {
            get { return m2_water_discount; }
            set { m2_water_discount = value; }
        }

        public double M2_NON_EFFECT_REVENUE_ETC
        {
            get { return m2_non_effect_revenue_etc; }
            set { m2_non_effect_revenue_etc = value; }
        }


        /////////3월
        public double M3_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M3_REVENUE + M3_WATER_PUB + M3_NON_EFFECT_REVENUE_ETC) / M3_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m3_water_ratio_pub = value; }
        }

        private double M3_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M3_REVENUE / M3_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m3_water_ratio_charge = value; }
        }

        public double M3_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M3_LEAKS / M3_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m3_water_ratio_leaks = value; }
        }

        public double M3_WATER_SUPPLIED
        {
            get { return m3_water_supplied; }
            set { m3_water_supplied = value; }
        }

        public double M3_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M3_REVENUE + M3_NON_REVENUE;
                return result;
            }
            set { m3_effect_revenue = value; }
        }

        public double M3_REVENUE
        {
            get
            {
                double result = 0;
                result = M3_WATER_CHARGE + M3_WATER_EXPORTED + M3_REVENUE_ETC;
                return result;
            }
            set { m3_revenue = value; }
        }

        public double M3_WATER_CHARGE
        {
            get { return m3_water_charge; }
            set { m3_water_charge = value; }
        }

        public double M3_WATER_EXPORTED
        {
            get { return m3_water_exported; }
            set { m3_water_exported = value; }
        }

        public double M3_REVENUE_ETC
        {
            get { return m3_revenue_etc; }
            set { m3_revenue_etc = value; }
        }

        public double M3_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M3_MTR_UNDREG + M3_WATER_UNDERTAKE + M3_WATER_PUB + M3_UNAUTH_CONSUMP;
                return result;
            }
            set { m3_non_revenue = value; }
        }

        public double M3_MTR_UNDREG
        {
            get { return m3_mtr_undreg; }
            set { m3_mtr_undreg = value; }
        }

        public double M3_WATER_UNDERTAKE
        {
            get { return m3_water_undertake; }
            set { m3_water_undertake = value; }
        }

        public double M3_WATER_PUB
        {
            get { return m3_water_pub; }
            set { m3_water_pub = value; }
        }

        public double M3_UNAUTH_CONSUMP
        {
            get { return m3_unauth_consump; }
            set { m3_unauth_consump = value; }
        }

        public double M3_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M3_LEAKS + M3_WATER_DISCOUNT + M3_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m3_non_effect_revenue = value; }
        }

        public double M3_LEAKS
        {
            get
            {
                double result = 0;
                result = M3_WATER_SUPPLIED - (M3_EFFECT_REVENUE + M3_WATER_DISCOUNT + M3_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m3_leaks = value; }
        }

        public double M3_LEAKS_EST
        {
            get { return m3_leaks_est; }
            set { m3_leaks_est = value; }
        }

        public double M3_WATER_DISCOUNT
        {
            get { return m3_water_discount; }
            set { m3_water_discount = value; }
        }

        public double M3_NON_EFFECT_REVENUE_ETC
        {
            get { return m3_non_effect_revenue_etc; }
            set { m3_non_effect_revenue_etc = value; }
        }


        /////////4월
        public double M4_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M4_REVENUE + M4_WATER_PUB + M4_NON_EFFECT_REVENUE_ETC) / M4_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m4_water_ratio_pub = value; }
        }

        private double M4_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M4_REVENUE / M4_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m4_water_ratio_charge = value; }
        }

        public double M4_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M4_LEAKS / M4_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m4_water_ratio_leaks = value; }
        }

        public double M4_WATER_SUPPLIED
        {
            get { return m4_water_supplied; }
            set { m4_water_supplied = value; }
        }

        public double M4_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M4_REVENUE + M4_NON_REVENUE;
                return result;
            }
            set { m4_effect_revenue = value; }
        }

        public double M4_REVENUE
        {
            get
            {
                double result = 0;
                result = M4_WATER_CHARGE + M4_WATER_EXPORTED + M4_REVENUE_ETC;
                return result;
            }
            set { m4_revenue = value; }
        }

        public double M4_WATER_CHARGE
        {
            get { return m4_water_charge; }
            set { m4_water_charge = value; }
        }

        public double M4_WATER_EXPORTED
        {
            get { return m4_water_exported; }
            set { m4_water_exported = value; }
        }

        public double M4_REVENUE_ETC
        {
            get { return m4_revenue_etc; }
            set { m4_revenue_etc = value; }
        }

        public double M4_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M4_MTR_UNDREG + M4_WATER_UNDERTAKE + M4_WATER_PUB + M4_UNAUTH_CONSUMP;
                return result;
            }
            set { m4_non_revenue = value; }
        }

        public double M4_MTR_UNDREG
        {
            get { return m4_mtr_undreg; }
            set { m4_mtr_undreg = value; }
        }

        public double M4_WATER_UNDERTAKE
        {
            get { return m4_water_undertake; }
            set { m4_water_undertake = value; }
        }

        public double M4_WATER_PUB
        {
            get { return m4_water_pub; }
            set { m4_water_pub = value; }
        }

        public double M4_UNAUTH_CONSUMP
        {
            get { return m4_unauth_consump; }
            set { m4_unauth_consump = value; }
        }

        public double M4_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M4_LEAKS + M4_WATER_DISCOUNT + M4_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m4_non_effect_revenue = value; }
        }

        public double M4_LEAKS
        {
            get
            {
                double result = 0;
                result = M4_WATER_SUPPLIED - (M4_EFFECT_REVENUE + M4_WATER_DISCOUNT + M4_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m4_leaks = value; }
        }

        public double M4_LEAKS_EST
        {
            get { return m4_leaks_est; }
            set { m4_leaks_est = value; }
        }

        public double M4_WATER_DISCOUNT
        {
            get { return m4_water_discount; }
            set { m4_water_discount = value; }
        }

        public double M4_NON_EFFECT_REVENUE_ETC
        {
            get { return m4_non_effect_revenue_etc; }
            set { m4_non_effect_revenue_etc = value; }
        }


        /////////5월
        public double M5_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M5_REVENUE + M5_WATER_PUB + M5_NON_EFFECT_REVENUE_ETC) / M5_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m5_water_ratio_pub = value; }
        }

        private double M5_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M5_REVENUE / M5_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m5_water_ratio_charge = value; }
        }

        public double M5_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M5_LEAKS / M5_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m5_water_ratio_leaks = value; }
        }

        public double M5_WATER_SUPPLIED
        {
            get { return m5_water_supplied; }
            set { m5_water_supplied = value; }
        }

        public double M5_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M5_REVENUE + M5_NON_REVENUE;
                return result;
            }
            set { m5_effect_revenue = value; }
        }

        public double M5_REVENUE
        {
            get
            {
                double result = 0;
                result = M5_WATER_CHARGE + M5_WATER_EXPORTED + M5_REVENUE_ETC;
                return result;
            }
            set { m5_revenue = value; }
        }

        public double M5_WATER_CHARGE
        {
            get { return m5_water_charge; }
            set { m5_water_charge = value; }
        }

        public double M5_WATER_EXPORTED
        {
            get { return m5_water_exported; }
            set { m5_water_exported = value; }
        }

        public double M5_REVENUE_ETC
        {
            get { return m5_revenue_etc; }
            set { m5_revenue_etc = value; }
        }

        public double M5_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M5_MTR_UNDREG + M5_WATER_UNDERTAKE + M5_WATER_PUB + M5_UNAUTH_CONSUMP;
                return result;
            }
            set { m5_non_revenue = value; }
        }

        public double M5_MTR_UNDREG
        {
            get { return m5_mtr_undreg; }
            set { m5_mtr_undreg = value; }
        }

        public double M5_WATER_UNDERTAKE
        {
            get { return m5_water_undertake; }
            set { m5_water_undertake = value; }
        }

        public double M5_WATER_PUB
        {
            get { return m5_water_pub; }
            set { m5_water_pub = value; }
        }

        public double M5_UNAUTH_CONSUMP
        {
            get { return m5_unauth_consump; }
            set { m5_unauth_consump = value; }
        }

        public double M5_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M5_LEAKS + M5_WATER_DISCOUNT + M5_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m5_non_effect_revenue = value; }
        }

        public double M5_LEAKS
        {
            get
            {
                double result = 0;
                result = M5_WATER_SUPPLIED - (M5_EFFECT_REVENUE + M5_WATER_DISCOUNT + M5_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m5_leaks = value; }
        }

        public double M5_LEAKS_EST
        {
            get { return m5_leaks_est; }
            set { m5_leaks_est = value; }
        }

        public double M5_WATER_DISCOUNT
        {
            get { return m5_water_discount; }
            set { m5_water_discount = value; }
        }

        public double M5_NON_EFFECT_REVENUE_ETC
        {
            get { return m5_non_effect_revenue_etc; }
            set { m5_non_effect_revenue_etc = value; }
        }


        /////////6월
        public double M6_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M6_REVENUE + M6_WATER_PUB + M6_NON_EFFECT_REVENUE_ETC) / M6_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m6_water_ratio_pub = value; }
        }

        private double M6_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M6_REVENUE / M6_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m6_water_ratio_charge = value; }
        }

        public double M6_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M6_LEAKS / M6_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m6_water_ratio_leaks = value; }
        }

        public double M6_WATER_SUPPLIED
        {
            get { return m6_water_supplied; }
            set { m6_water_supplied = value; }
        }

        public double M6_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M6_REVENUE + M6_NON_REVENUE;
                return result;
            }
            set { m6_effect_revenue = value; }
        }

        public double M6_REVENUE
        {
            get
            {
                double result = 0;
                result = M6_WATER_CHARGE + M6_WATER_EXPORTED + M6_REVENUE_ETC;
                return result;
            }
            set { m6_revenue = value; }
        }

        public double M6_WATER_CHARGE
        {
            get { return m6_water_charge; }
            set { m6_water_charge = value; }
        }

        public double M6_WATER_EXPORTED
        {
            get { return m6_water_exported; }
            set { m6_water_exported = value; }
        }

        public double M6_REVENUE_ETC
        {
            get { return m6_revenue_etc; }
            set { m6_revenue_etc = value; }
        }

        public double M6_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M6_MTR_UNDREG + M6_WATER_UNDERTAKE + M6_WATER_PUB + M6_UNAUTH_CONSUMP;
                return result;
            }
            set { m6_non_revenue = value; }
        }

        public double M6_MTR_UNDREG
        {
            get { return m6_mtr_undreg; }
            set { m6_mtr_undreg = value; }
        }

        public double M6_WATER_UNDERTAKE
        {
            get { return m6_water_undertake; }
            set { m6_water_undertake = value; }
        }

        public double M6_WATER_PUB
        {
            get { return m6_water_pub; }
            set { m6_water_pub = value; }
        }

        public double M6_UNAUTH_CONSUMP
        {
            get { return m6_unauth_consump; }
            set { m6_unauth_consump = value; }
        }

        public double M6_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M6_LEAKS + M6_WATER_DISCOUNT + M6_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m6_non_effect_revenue = value; }
        }

        public double M6_LEAKS
        {
            get
            {
                double result = 0;
                result = M6_WATER_SUPPLIED - (M6_EFFECT_REVENUE + M6_WATER_DISCOUNT + M6_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m6_leaks = value; }
        }

        public double M6_LEAKS_EST
        {
            get { return m6_leaks_est; }
            set { m6_leaks_est = value; }
        }

        public double M6_WATER_DISCOUNT
        {
            get { return m6_water_discount; }
            set { m6_water_discount = value; }
        }

        public double M6_NON_EFFECT_REVENUE_ETC
        {
            get { return m6_non_effect_revenue_etc; }
            set { m6_non_effect_revenue_etc = value; }
        }


        /////////7월
        public double M7_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M7_REVENUE + M7_WATER_PUB + M7_NON_EFFECT_REVENUE_ETC) / M7_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m7_water_ratio_pub = value; }
        }

        private double M7_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M7_REVENUE / M7_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m7_water_ratio_charge = value; }
        }

        public double M7_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M7_LEAKS / M7_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m7_water_ratio_leaks = value; }
        }

        public double M7_WATER_SUPPLIED
        {
            get { return m7_water_supplied; }
            set { m7_water_supplied = value; }
        }

        public double M7_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M7_REVENUE + M7_NON_REVENUE;
                return result;
            }
            set { m7_effect_revenue = value; }
        }

        public double M7_REVENUE
        {
            get
            {
                double result = 0;
                result = M7_WATER_CHARGE + M7_WATER_EXPORTED + M7_REVENUE_ETC;
                return result;
            }
            set { m7_revenue = value; }
        }

        public double M7_WATER_CHARGE
        {
            get { return m7_water_charge; }
            set { m7_water_charge = value; }
        }

        public double M7_WATER_EXPORTED
        {
            get { return m7_water_exported; }
            set { m7_water_exported = value; }
        }

        public double M7_REVENUE_ETC
        {
            get { return m7_revenue_etc; }
            set { m7_revenue_etc = value; }
        }

        public double M7_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M7_MTR_UNDREG + M7_WATER_UNDERTAKE + M7_WATER_PUB + M7_UNAUTH_CONSUMP;
                return result;
            }
            set { m7_non_revenue = value; }
        }

        public double M7_MTR_UNDREG
        {
            get { return m7_mtr_undreg; }
            set { m7_mtr_undreg = value; }
        }

        public double M7_WATER_UNDERTAKE
        {
            get { return m7_water_undertake; }
            set { m7_water_undertake = value; }
        }

        public double M7_WATER_PUB
        {
            get { return m7_water_pub; }
            set { m7_water_pub = value; }
        }

        public double M7_UNAUTH_CONSUMP
        {
            get { return m7_unauth_consump; }
            set { m7_unauth_consump = value; }
        }

        public double M7_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M7_LEAKS + M7_WATER_DISCOUNT + M7_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m7_non_effect_revenue = value; }
        }

        public double M7_LEAKS
        {
            get
            {
                double result = 0;
                result = M7_WATER_SUPPLIED - (M7_EFFECT_REVENUE + M7_WATER_DISCOUNT + M7_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m7_leaks = value; }
        }

        public double M7_LEAKS_EST
        {
            get { return m7_leaks_est; }
            set { m7_leaks_est = value; }
        }

        public double M7_WATER_DISCOUNT
        {
            get { return m7_water_discount; }
            set { m7_water_discount = value; }
        }

        public double M7_NON_EFFECT_REVENUE_ETC
        {
            get { return m7_non_effect_revenue_etc; }
            set { m7_non_effect_revenue_etc = value; }
        }


        /////////8월
        public double M8_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M8_REVENUE + M8_WATER_PUB + M8_NON_EFFECT_REVENUE_ETC) / M8_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m8_water_ratio_pub = value; }
        }

        private double M8_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M8_REVENUE / M8_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m8_water_ratio_charge = value; }
        }

        public double M8_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M8_LEAKS / M8_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m8_water_ratio_leaks = value; }
        }

        public double M8_WATER_SUPPLIED
        {
            get { return m8_water_supplied; }
            set { m8_water_supplied = value; }
        }

        public double M8_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M8_REVENUE + M8_NON_REVENUE;
                return result;
            }
            set { m8_effect_revenue = value; }
        }

        public double M8_REVENUE
        {
            get
            {
                double result = 0;
                result = M8_WATER_CHARGE + M8_WATER_EXPORTED + M8_REVENUE_ETC;
                return result;
            }
            set { m8_revenue = value; }
        }

        public double M8_WATER_CHARGE
        {
            get { return m8_water_charge; }
            set { m8_water_charge = value; }
        }

        public double M8_WATER_EXPORTED
        {
            get { return m8_water_exported; }
            set { m8_water_exported = value; }
        }

        public double M8_REVENUE_ETC
        {
            get { return m8_revenue_etc; }
            set { m8_revenue_etc = value; }
        }

        public double M8_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M8_MTR_UNDREG + M8_WATER_UNDERTAKE + M8_WATER_PUB + M8_UNAUTH_CONSUMP;
                return result;
            }
            set { m8_non_revenue = value; }
        }

        public double M8_MTR_UNDREG
        {
            get { return m8_mtr_undreg; }
            set { m8_mtr_undreg = value; }
        }

        public double M8_WATER_UNDERTAKE
        {
            get { return m8_water_undertake; }
            set { m8_water_undertake = value; }
        }

        public double M8_WATER_PUB
        {
            get { return m8_water_pub; }
            set { m8_water_pub = value; }
        }

        public double M8_UNAUTH_CONSUMP
        {
            get { return m8_unauth_consump; }
            set { m8_unauth_consump = value; }
        }

        public double M8_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M8_LEAKS + M8_WATER_DISCOUNT + M8_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m8_non_effect_revenue = value; }
        }

        public double M8_LEAKS
        {
            get
            {
                double result = 0;
                result = M8_WATER_SUPPLIED - (M8_EFFECT_REVENUE + M8_WATER_DISCOUNT + M8_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m8_leaks = value; }
        }

        public double M8_LEAKS_EST
        {
            get { return m8_leaks_est; }
            set { m8_leaks_est = value; }
        }

        public double M8_WATER_DISCOUNT
        {
            get { return m8_water_discount; }
            set { m8_water_discount = value; }
        }

        public double M8_NON_EFFECT_REVENUE_ETC
        {
            get { return m8_non_effect_revenue_etc; }
            set { m8_non_effect_revenue_etc = value; }
        }


        /////////9월
        public double M9_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M9_REVENUE + M9_WATER_PUB + M9_NON_EFFECT_REVENUE_ETC) / M9_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m9_water_ratio_pub = value; }
        }

        private double M9_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M9_REVENUE / M9_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m9_water_ratio_charge = value; }
        }

        public double M9_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M9_LEAKS / M9_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m9_water_ratio_leaks = value; }
        }

        public double M9_WATER_SUPPLIED
        {
            get { return m9_water_supplied; }
            set { m9_water_supplied = value; }
        }

        public double M9_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M9_REVENUE + M9_NON_REVENUE;
                return result;
            }
            set { m9_effect_revenue = value; }
        }

        public double M9_REVENUE
        {
            get
            {
                double result = 0;
                result = M9_WATER_CHARGE + M9_WATER_EXPORTED + M9_REVENUE_ETC;
                return result;
            }
            set { m9_revenue = value; }
        }

        public double M9_WATER_CHARGE
        {
            get { return m9_water_charge; }
            set { m9_water_charge = value; }
        }

        public double M9_WATER_EXPORTED
        {
            get { return m9_water_exported; }
            set { m9_water_exported = value; }
        }

        public double M9_REVENUE_ETC
        {
            get { return m9_revenue_etc; }
            set { m9_revenue_etc = value; }
        }

        public double M9_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M9_MTR_UNDREG + M9_WATER_UNDERTAKE + M9_WATER_PUB + M9_UNAUTH_CONSUMP;
                return result;
            }
            set { m9_non_revenue = value; }
        }

        public double M9_MTR_UNDREG
        {
            get { return m9_mtr_undreg; }
            set { m9_mtr_undreg = value; }
        }

        public double M9_WATER_UNDERTAKE
        {
            get { return m9_water_undertake; }
            set { m9_water_undertake = value; }
        }

        public double M9_WATER_PUB
        {
            get { return m9_water_pub; }
            set { m9_water_pub = value; }
        }

        public double M9_UNAUTH_CONSUMP
        {
            get { return m9_unauth_consump; }
            set { m9_unauth_consump = value; }
        }

        public double M9_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M9_LEAKS + M9_WATER_DISCOUNT + M9_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m9_non_effect_revenue = value; }
        }

        public double M9_LEAKS
        {
            get
            {
                double result = 0;
                result = M9_WATER_SUPPLIED - (M9_EFFECT_REVENUE + M9_WATER_DISCOUNT + M9_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m9_leaks = value; }
        }

        public double M9_LEAKS_EST
        {
            get { return m9_leaks_est; }
            set { m9_leaks_est = value; }
        }

        public double M9_WATER_DISCOUNT
        {
            get { return m9_water_discount; }
            set { m9_water_discount = value; }
        }

        public double M9_NON_EFFECT_REVENUE_ETC
        {
            get { return m9_non_effect_revenue_etc; }
            set { m9_non_effect_revenue_etc = value; }
        }


        /////////10월
        public double M10_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M10_REVENUE + M10_WATER_PUB + M10_NON_EFFECT_REVENUE_ETC) / M10_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m10_water_ratio_pub = value; }
        }

        private double M10_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M10_REVENUE / M10_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m10_water_ratio_charge = value; }
        }

        public double M10_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M10_LEAKS / M10_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m10_water_ratio_leaks = value; }
        }

        public double M10_WATER_SUPPLIED
        {
            get { return m10_water_supplied; }
            set { m10_water_supplied = value; }
        }

        public double M10_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M10_REVENUE + M10_NON_REVENUE;
                return result;
            }
            set { m10_effect_revenue = value; }
        }

        public double M10_REVENUE
        {
            get
            {
                double result = 0;
                result = M10_WATER_CHARGE + M10_WATER_EXPORTED + M10_REVENUE_ETC;
                return result;
            }
            set { m10_revenue = value; }
        }

        public double M10_WATER_CHARGE
        {
            get { return m10_water_charge; }
            set { m10_water_charge = value; }
        }

        public double M10_WATER_EXPORTED
        {
            get { return m10_water_exported; }
            set { m10_water_exported = value; }
        }

        public double M10_REVENUE_ETC
        {
            get { return m10_revenue_etc; }
            set { m10_revenue_etc = value; }
        }

        public double M10_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M10_MTR_UNDREG + M10_WATER_UNDERTAKE + M10_WATER_PUB + M10_UNAUTH_CONSUMP;
                return result;
            }
            set { m10_non_revenue = value; }
        }

        public double M10_MTR_UNDREG
        {
            get { return m10_mtr_undreg; }
            set { m10_mtr_undreg = value; }
        }

        public double M10_WATER_UNDERTAKE
        {
            get { return m10_water_undertake; }
            set { m10_water_undertake = value; }
        }

        public double M10_WATER_PUB
        {
            get { return m10_water_pub; }
            set { m10_water_pub = value; }
        }

        public double M10_UNAUTH_CONSUMP
        {
            get { return m10_unauth_consump; }
            set { m10_unauth_consump = value; }
        }

        public double M10_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M10_LEAKS + M10_WATER_DISCOUNT + M10_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m10_non_effect_revenue = value; }
        }

        public double M10_LEAKS
        {
            get
            {
                double result = 0;
                result = M10_WATER_SUPPLIED - (M10_EFFECT_REVENUE + M10_WATER_DISCOUNT + M10_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m10_leaks = value; }
        }

        public double M10_LEAKS_EST
        {
            get { return m10_leaks_est; }
            set { m10_leaks_est = value; }
        }

        public double M10_WATER_DISCOUNT
        {
            get { return m10_water_discount; }
            set { m10_water_discount = value; }
        }

        public double M10_NON_EFFECT_REVENUE_ETC
        {
            get { return m10_non_effect_revenue_etc; }
            set { m10_non_effect_revenue_etc = value; }
        }


        /////////11월
        public double M11_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M11_REVENUE + M11_WATER_PUB + M11_NON_EFFECT_REVENUE_ETC) / M11_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m11_water_ratio_pub = value; }
        }

        private double M11_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M11_REVENUE / M11_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m11_water_ratio_charge = value; }
        }

        public double M11_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M11_LEAKS / M11_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m11_water_ratio_leaks = value; }
        }

        public double M11_WATER_SUPPLIED
        {
            get { return m11_water_supplied; }
            set { m11_water_supplied = value; }
        }

        public double M11_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M11_REVENUE + M11_NON_REVENUE;
                return result;
            }
            set { m11_effect_revenue = value; }
        }

        public double M11_REVENUE
        {
            get
            {
                double result = 0;
                result = M11_WATER_CHARGE + M11_WATER_EXPORTED + M11_REVENUE_ETC;
                return result;
            }
            set { m11_revenue = value; }
        }

        public double M11_WATER_CHARGE
        {
            get { return m11_water_charge; }
            set { m11_water_charge = value; }
        }

        public double M11_WATER_EXPORTED
        {
            get { return m11_water_exported; }
            set { m11_water_exported = value; }
        }

        public double M11_REVENUE_ETC
        {
            get { return m11_revenue_etc; }
            set { m11_revenue_etc = value; }
        }

        public double M11_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M11_MTR_UNDREG + M11_WATER_UNDERTAKE + M11_WATER_PUB + M11_UNAUTH_CONSUMP;
                return result;
            }
            set { m11_non_revenue = value; }
        }

        public double M11_MTR_UNDREG
        {
            get { return m11_mtr_undreg; }
            set { m11_mtr_undreg = value; }
        }

        public double M11_WATER_UNDERTAKE
        {
            get { return m11_water_undertake; }
            set { m11_water_undertake = value; }
        }

        public double M11_WATER_PUB
        {
            get { return m11_water_pub; }
            set { m11_water_pub = value; }
        }

        public double M11_UNAUTH_CONSUMP
        {
            get { return m11_unauth_consump; }
            set { m11_unauth_consump = value; }
        }

        public double M11_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M11_LEAKS + M11_WATER_DISCOUNT + M11_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m11_non_effect_revenue = value; }
        }

        public double M11_LEAKS
        {
            get
            {
                double result = 0;
                result = M11_WATER_SUPPLIED - (M11_EFFECT_REVENUE + M11_WATER_DISCOUNT + M11_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m11_leaks = value; }
        }

        public double M11_LEAKS_EST
        {
            get { return m11_leaks_est; }
            set { m11_leaks_est = value; }
        }

        public double M11_WATER_DISCOUNT
        {
            get { return m11_water_discount; }
            set { m11_water_discount = value; }
        }

        public double M11_NON_EFFECT_REVENUE_ETC
        {
            get { return m11_non_effect_revenue_etc; }
            set { m11_non_effect_revenue_etc = value; }
        }


        /////////12월
        public double M12_WATER_RATIO_PUB
        {
            get
            {
                double result = 0;
                result = ((M12_REVENUE + M12_WATER_PUB + M12_NON_EFFECT_REVENUE_ETC) / M12_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m12_water_ratio_pub = value; }
        }

        private double M12_WATER_RATIO_CHARGE
        {
            get
            {
                double result = 0;
                result = (M12_REVENUE / M12_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m12_water_ratio_charge = value; }
        }

        public double M12_WATER_RATIO_LEAKS
        {
            get
            {
                double result = 0;
                result = (M12_LEAKS / M12_WATER_SUPPLIED) * 100;
                if (Double.IsNaN(result))
                {
                    result = 0;
                }
                return result;
            }
            set { m12_water_ratio_leaks = value; }
        }

        public double M12_WATER_SUPPLIED
        {
            get { return m12_water_supplied; }
            set { m12_water_supplied = value; }
        }

        public double M12_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M12_REVENUE + M12_NON_REVENUE;
                return result;
            }
            set { m12_effect_revenue = value; }
        }

        public double M12_REVENUE
        {
            get
            {
                double result = 0;
                result = M12_WATER_CHARGE + M12_WATER_EXPORTED + M12_REVENUE_ETC;
                return result;
            }
            set { m12_revenue = value; }
        }

        public double M12_WATER_CHARGE
        {
            get { return m12_water_charge; }
            set { m12_water_charge = value; }
        }

        public double M12_WATER_EXPORTED
        {
            get { return m12_water_exported; }
            set { m12_water_exported = value; }
        }

        public double M12_REVENUE_ETC
        {
            get { return m12_revenue_etc; }
            set { m12_revenue_etc = value; }
        }

        public double M12_NON_REVENUE
        {
            get
            {
                double result = 0;
                result = M12_MTR_UNDREG + M12_WATER_UNDERTAKE + M12_WATER_PUB + M12_UNAUTH_CONSUMP;
                return result;
            }
            set { m12_non_revenue = value; }
        }

        public double M12_MTR_UNDREG
        {
            get { return m12_mtr_undreg; }
            set { m12_mtr_undreg = value; }
        }

        public double M12_WATER_UNDERTAKE
        {
            get { return m12_water_undertake; }
            set { m12_water_undertake = value; }
        }

        public double M12_WATER_PUB
        {
            get { return m12_water_pub; }
            set { m12_water_pub = value; }
        }

        public double M12_UNAUTH_CONSUMP
        {
            get { return m12_unauth_consump; }
            set { m12_unauth_consump = value; }
        }

        public double M12_NON_EFFECT_REVENUE
        {
            get
            {
                double result = 0;
                result = M12_LEAKS + M12_WATER_DISCOUNT + M12_NON_EFFECT_REVENUE_ETC;
                return result;
            }
            set { m12_non_effect_revenue = value; }
        }

        public double M12_LEAKS
        {
            get
            {
                double result = 0;
                result = M12_WATER_SUPPLIED - (M12_EFFECT_REVENUE + M12_WATER_DISCOUNT + M12_NON_EFFECT_REVENUE_ETC);
                return result;
            }
            set { m12_leaks = value; }
        }

        public double M12_LEAKS_EST
        {
            get { return m12_leaks_est; }
            set { m12_leaks_est = value; }
        }

        public double M12_WATER_DISCOUNT
        {
            get { return m12_water_discount; }
            set { m12_water_discount = value; }
        }

        public double M12_NON_EFFECT_REVENUE_ETC
        {
            get { return m12_non_effect_revenue_etc; }
            set { m12_non_effect_revenue_etc = value; }
        }
    }
}