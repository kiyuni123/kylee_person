﻿using System;
using System.Collections.Generic;
using WaterNet.WV_WaterBalanceAnalysis.dao;
using WaterNet.WV_Common.work;
using System.Collections;
using System.Data;
using WaterNet.WV_WaterBalanceAnalysis.vo;
using WaterNet.WV_LeakageManage.work;
using WaterNet.WV_LeakageManage.vo;
using WaterNet.WV_Common.util;
using System.Windows.Forms;

namespace WaterNet.WV_WaterBalanceAnalysis.work
{
    public class WaterBalanceAnalysisWork : BaseWork
    {
        private static WaterBalanceAnalysisWork work = null;
        private WaterBalanceAnalysisDao dao = null;

        public static WaterBalanceAnalysisWork GetInstance()
        {
            if (work == null)
            {
                work = new WaterBalanceAnalysisWork();
            }
            return work;
        }

        private WaterBalanceAnalysisWork()
        {
            dao = WaterBalanceAnalysisDao.GetInstance();
        }

        public IList<WaterBalanceAnalysisVO> SelectWaterBalanceAnalysis(Hashtable parameter)
        {
            IList<WaterBalanceAnalysisVO> WaterBalanceAnalysisList = new List<WaterBalanceAnalysisVO>();
            WaterBalanceAnalysisVO WaterBalanceAnalysis = new WaterBalanceAnalysisVO();
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                //총괄수량수지분석 자료
                dao.SelectWaterBalanceAnalysis(base.DataBaseManager, result, "RESULT", parameter);

                //년도를 가지고 startDate와 endDate를 재설정.

                DateTime startdate = DateTime.ParseExact(Utils.GetSuppliedStartdate(parameter["YEAR"].ToString() + "01", "yyyyMM", parameter["LOC_CODE"].ToString()), "yyyyMMdd", null);

                parameter["STARTDATE"] = startdate.ToString("yyyyMMdd");
                parameter["ENDDATE"] = startdate.AddMonths(12).AddDays(-1).ToString("yyyyMMdd");

                //누수량 추정치
                IList<LeakageCalculationVO> leakage = LeakageManageWork.GetInstance().SelectLeakageManage(parameter);

                //총괄수량분석 자료를 돌며 년월이 같으면 누수량추정치에 값을 더한다,,
                foreach (DataRow dataRow in result.Tables["RESULT"].Rows)
                {
                    DateTime yearMon = Convert.ToDateTime(dataRow["YEAR_MON"]);
                    int idx = result.Tables["RESULT"].Rows.IndexOf(dataRow);

                    if (Utils.CanRevenueUpdate(yearMon.ToString("yyyyMM"), "yyyyMM", parameter["LOC_CODE"].ToString()))
                    {
                        foreach (LeakageCalculationVO lc in leakage)
                        {
                            if (lc.DATEE >= startdate.AddMonths(idx) && lc.DATEE <= startdate.AddMonths(idx + 1).AddDays(-1))
                            {
                                dataRow["LEAKS_EST"] = Utils.ToDouble(dataRow["LEAKS_EST"]) + lc.LEAKS_DAVG;
                            }
                        }
                    }

                    Hashtable resulthash = new Hashtable();
                    resulthash["YEAR_MON"] = dataRow["YEAR_MON"];
                    resulthash["WATER_SUPPLIED"] = dataRow["WATER_SUPPLIED"];
                    resulthash["WATER_CHARGE"] = dataRow["WATER_CHARGE"];
                    resulthash["WATER_EXPORTED"] = dataRow["WATER_EXPORTED"];
                    resulthash["REVENUE_ETC"] = dataRow["REVENUE_ETC"];
                    resulthash["MTR_UNDREG"] = dataRow["MTR_UNDREG"];
                    resulthash["WATER_UNDERTAKE"] = dataRow["WATER_UNDERTAKE"];
                    resulthash["WATER_PUB"] = dataRow["WATER_PUB"];
                    resulthash["UNAUTH_CONSUMP"] = dataRow["UNAUTH_CONSUMP"];
                    resulthash["LEAKS_EST"] = dataRow["LEAKS_EST"];
                    resulthash["WATER_DISCOUNT"] = dataRow["WATER_DISCOUNT"];
                    resulthash["NON_EFFECT_REVENUE_ETC"] = dataRow["NON_EFFECT_REVENUE_ETC"];

                    WaterBalanceAnalysis.SetData(resulthash);
                }

                WaterBalanceAnalysis.LOC_CODE = parameter["LOC_CODE"].ToString();
                WaterBalanceAnalysis.SetYear(Convert.ToInt32(parameter["YEAR"]));

                WaterBalanceAnalysisList.Add(WaterBalanceAnalysis);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return WaterBalanceAnalysisList;
        }

        public void UpdateWaterBalanceAnalysis(IList<WaterBalanceAnalysisVO> WaterBalanceAnalysisList)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (WaterBalanceAnalysisVO WaterBalanceAnalysis in WaterBalanceAnalysisList)
                {
                    for (int i = 1; i < 13; i++)
                    {
                        Hashtable parameter = WaterBalanceAnalysis.GetData(i);
                        dao.UpdateWaterBalanceAnalysis(base.DataBaseManager, parameter);
                    }
                }

                dao.UpdateLargeBlockWaterBalanceAnalysis(base.DataBaseManager);

                MessageBox.Show("정상적으로 처리되었습니다.");
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }
    }
}
