﻿using System.Text;
using WaterNet.WaterNetCore;
using WaterNet.WV_Common.dao;
using System.Data;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_WaterBalanceAnalysis.dao
{
    public class WaterBalanceAnalysisDao : BaseDao
    {
        private static WaterBalanceAnalysisDao dao = null;

        private WaterBalanceAnalysisDao() { }

        public static WaterBalanceAnalysisDao GetInstance()
        {
            if (dao == null)
            {
                dao = new WaterBalanceAnalysisDao();
            }
            return dao;
        }

        //총괄수량수지분석 조회
        public void SelectWaterBalanceAnalysis(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select to_date(mon.year_mon,'yyyymm') year_mon                                                       ");
            query.AppendLine("      ,to_number(wrr.water_supplied) water_supplied                                                  ");
            query.AppendLine("      ,to_number(wrr.revenue) water_charge                                                           ");
            query.AppendLine("      ,to_number(wto.water_exported) water_exported                                                  ");
            query.AppendLine("      ,to_number(wto.revenue_etc) revenue_etc                                                        ");
            query.AppendLine("      ,to_number(wto.mtr_undreg) mtr_undreg                                                          ");
            query.AppendLine("      ,to_number(wto.water_undertake) water_undertake                                                ");
            query.AppendLine("      ,to_number(wto.water_pub) water_pub                                                            ");
            query.AppendLine("      ,to_number(wto.unauth_consump) unauth_consump                                                  ");
            query.AppendLine("      ,to_number(wto.leaks) leaks                                                                    ");
            query.AppendLine("      ,0 leaks_est                                                                                   ");
            query.AppendLine("      ,to_number(wto.water_discount) water_discount                                                  ");
            query.AppendLine("      ,to_number(wto.non_effect_revenue_etc) non_effect_revenue_etc                                  ");
            query.AppendLine("  from                                                                                               ");
            query.AppendLine("      (                                                                                              ");
            query.AppendLine("      select year_mon                                                                                ");
            query.AppendLine("            ,water_supplied+nvl(add_water_supplied,0) water_supplied                                 ");
            query.AppendLine("            ,revenue+nvl(add_revenue,0) revenue                                                      ");
            query.AppendLine("        from wv_revenue_ratio                                                                        ");
            query.AppendLine("       where 1 = 1                                                                                   ");
            query.AppendLine("         and loc_code = :LOC_CODE                                                                    ");
            query.AppendLine("      ) wrr                                                                                          ");
            query.AppendLine("      ,(                                                                                             ");
            query.AppendLine("      select year_mon                                                                                ");
            query.AppendLine("            ,water_exported                                                                          ");
            query.AppendLine("            ,revenue_etc                                                                             ");
            query.AppendLine("            ,mtr_undreg                                                                              ");
            query.AppendLine("            ,water_undertake                                                                         ");
            query.AppendLine("            ,water_pub                                                                               ");
            query.AppendLine("            ,unauth_consump                                                                          ");
            query.AppendLine("            ,leaks                                                                                   ");
            query.AppendLine("            ,water_discount                                                                          ");
            query.AppendLine("            ,non_effect_revenue_etc                                                                  ");
            query.AppendLine("        from wv_total                                                                                ");
            query.AppendLine("       where 1 = 1                                                                                   ");
            query.AppendLine("         and loc_code = :LOC_CODE                                                                    ");
            query.AppendLine("      ) wto                                                                                          ");
            query.AppendLine("      ,(                                                                                             ");
            query.AppendLine("      select to_char(add_months(to_date(:YEAR||'01','yyyymm'), + rownum - 1),'yyyymm') year_mon      ");
            query.AppendLine("        from dual connect by rownum < 13                                                             ");
            query.AppendLine("      ) mon                                                                                          ");
            query.AppendLine(" where 1 = 1                                                                                         ");
            query.AppendLine("   and mon.year_mon = wrr.year_mon(+)                                                                ");
            query.AppendLine("   and mon.year_mon = wto.year_mon(+)                                                                ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("YEAR", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["LOC_CODE"];
            parameters[2].Value = parameter["YEAR"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //총괄수량수지분석 수정(중블록)
        public void UpdateWaterBalanceAnalysis(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine(" merge into wv_total wt                                                                                         ");
            query.AppendLine(" using (select :LOC_CODE loc_code, :YEAR_MON year_mon from dual) a											  ");
            query.AppendLine("    on (wt.loc_code = a.loc_code and wt.year_mon = a.year_mon)												  ");
            query.AppendLine("  when MATCHED then																							  ");
            query.AppendLine("       update set water_charge = :WATER_CHARGE																  ");
            query.AppendLine("                 ,water_exported = :WATER_EXPORTED															  ");
            query.AppendLine("                 ,revenue_etc = :REVENUE_ETC																	  ");
            query.AppendLine("                 ,mtr_undreg = :MTR_UNDREG																	  ");
            query.AppendLine("                 ,water_undertake = :WATER_UNDERTAKE															  ");
            query.AppendLine("                 ,water_pub = :WATER_PUB																		  ");
            query.AppendLine("                 ,unauth_consump = :UNAUTH_CONSUMP															  ");
            query.AppendLine("                 ,leaks = :LEAKS																				  ");
            query.AppendLine("                 ,water_discount = :WATER_DISCOUNT															  ");
            query.AppendLine("                 ,non_effect_revenue_etc = :NON_EFFECT_REVENUE_ETC									          ");
            query.AppendLine("  when not MATCHED then																						  ");
            query.AppendLine("       insert (loc_code ,year_mon ,water_charge ,water_exported ,revenue_etc ,mtr_undreg						  ");
            query.AppendLine("             ,water_undertake ,water_pub ,unauth_consump ,leaks ,water_discount ,non_effect_revenue_etc)	      ");
            query.AppendLine("       values (:LOC_CODE ,:YEAR_MON ,:WATER_CHARGE ,:WATER_EXPORTED ,:REVENUE_ETC ,:MTR_UNDREG				  ");
            query.AppendLine("             ,:WATER_UNDERTAKE ,:WATER_PUB ,:UNAUTH_CONSUMP ,:LEAKS ,:WATER_DISCOUNT ,:NON_EFFECT_REVENUE_ETC)  ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
	                ,new OracleParameter("WATER_CHARGE", OracleDbType.Varchar2)
                    ,new OracleParameter("WATER_EXPORTED", OracleDbType.Varchar2)
                    ,new OracleParameter("REVENUE_ETC", OracleDbType.Varchar2)
                    ,new OracleParameter("MTR_UNDREG", OracleDbType.Varchar2)
                    ,new OracleParameter("WATER_UNDERTAKE", OracleDbType.Varchar2)
                    ,new OracleParameter("WATER_PUB", OracleDbType.Varchar2)
                    ,new OracleParameter("UNAUTH_CONSUMP", OracleDbType.Varchar2)
                    ,new OracleParameter("LEAKS", OracleDbType.Varchar2)
                    ,new OracleParameter("WATER_DISCOUNT", OracleDbType.Varchar2)
                    ,new OracleParameter("NON_EFFECT_REVENUE_ETC", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
                    ,new OracleParameter("WATER_CHARGE", OracleDbType.Varchar2)
                    ,new OracleParameter("WATER_EXPORTED", OracleDbType.Varchar2)
                    ,new OracleParameter("REVENUE_ECT", OracleDbType.Varchar2)
                    ,new OracleParameter("MTR_UNDREG", OracleDbType.Varchar2)
                    ,new OracleParameter("WATER_UNDERTAKE", OracleDbType.Varchar2)
                    ,new OracleParameter("WATER_PUB", OracleDbType.Varchar2)
                    ,new OracleParameter("UNAUTH_CONSUMP", OracleDbType.Varchar2)
                    ,new OracleParameter("LEAKS", OracleDbType.Varchar2)
                    ,new OracleParameter("WATER_DISCOUNT", OracleDbType.Varchar2)
                    ,new OracleParameter("NON_EFFECT_REVENUE_ETC", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["YEAR_MON"];
            parameters[2].Value = parameter["WATER_CHARGE"];
            parameters[3].Value = parameter["WATER_EXPORTED"];
            parameters[4].Value = parameter["REVENUE_ETC"];
            parameters[5].Value = parameter["MTR_UNDREG"];
            parameters[6].Value = parameter["WATER_UNDERTAKE"];
            parameters[7].Value = parameter["WATER_PUB"];
            parameters[8].Value = parameter["UNAUTH_CONSUMP"];
            parameters[9].Value = parameter["LEAKS"];
            parameters[10].Value = parameter["WATER_DISCOUNT"];
            parameters[11].Value = parameter["NON_EFFECT_REVENUE_ETC"];
            parameters[12].Value = parameter["LOC_CODE"];
            parameters[13].Value = parameter["YEAR_MON"];
            parameters[14].Value = parameter["WATER_CHARGE"];
            parameters[15].Value = parameter["WATER_EXPORTED"];
            parameters[16].Value = parameter["REVENUE_ECT"];
            parameters[17].Value = parameter["MTR_UNDREG"];
            parameters[18].Value = parameter["WATER_UNDERTAKE"];
            parameters[19].Value = parameter["WATER_PUB"];
            parameters[20].Value = parameter["UNAUTH_CONSUMP"];
            parameters[21].Value = parameter["LEAKS"];
            parameters[22].Value = parameter["WATER_DISCOUNT"];
            parameters[23].Value = parameter["NON_EFFECT_REVENUE_ETC"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //총괄수량수지분석 수정(대블록)
        public void UpdateLargeBlockWaterBalanceAnalysis(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_total wt                                                                                              ");
            query.AppendLine("using(																											  ");
            query.AppendLine("      with loc as																									  ");
            query.AppendLine("      (																											  ");
            query.AppendLine("      select loc_code																								  ");
            query.AppendLine("            ,ploc_code																							  ");
            query.AppendLine("        from cm_location																							  ");
            query.AppendLine("       where 1 = 1																								  ");
            query.AppendLine("         and ftr_code = 'BZ002'																					  ");
            query.AppendLine("         and kt_gbn = '001'																						  ");
            query.AppendLine("       start with ftr_code = 'BZ001'																				  ");
            query.AppendLine("       connect by prior loc_code = ploc_code																		  ");
            query.AppendLine("      )																											  ");
            query.AppendLine("      select loc.ploc_code loc_code																				  ");
            query.AppendLine("            ,wt.year_mon																							  ");
            query.AppendLine("            ,sum(water_charge) water_charge																		  ");
            query.AppendLine("            ,sum(water_exported) water_exported																	  ");
            query.AppendLine("            ,sum(revenue_etc) revenue_etc																			  ");
            query.AppendLine("            ,sum(mtr_undreg) mtr_undreg																			  ");
            query.AppendLine("            ,sum(water_undertake) water_undertake																	  ");
            query.AppendLine("            ,sum(water_pub) water_pub																				  ");
            query.AppendLine("            ,sum(unauth_consump) unauth_consump																	  ");
            query.AppendLine("            ,sum(leaks) leaks																						  ");
            query.AppendLine("            ,sum(water_discount) water_discount																	  ");
            query.AppendLine("            ,sum(non_effect_revenue_etc) non_effect_revenue_etc													  ");
            query.AppendLine("        from loc																									  ");
            query.AppendLine("            ,wv_total wt																							  ");
            query.AppendLine("       where wt.loc_code = loc.loc_code																			  ");
            query.AppendLine("       group by																									  ");
            query.AppendLine("             loc.ploc_code																						  ");
            query.AppendLine("            ,wt.year_mon																							  ");
            query.AppendLine("      ) a																											  ");
            query.AppendLine("  on (wt.loc_code = a.loc_code and wt.year_mon = a.year_mon)														  ");
            query.AppendLine("when MATCHED then																									  ");
            query.AppendLine("     update set wt.water_charge = a.water_charge																	  ");
            query.AppendLine("               ,wt.water_exported = a.water_exported																  ");
            query.AppendLine("               ,wt.revenue_etc = a.revenue_etc																	  ");
            query.AppendLine("               ,wt.mtr_undreg = a.mtr_undreg																		  ");
            query.AppendLine("               ,wt.water_undertake = a.water_undertake															  ");
            query.AppendLine("               ,wt.water_pub = a.water_pub																		  ");
            query.AppendLine("               ,wt.unauth_consump = a.unauth_consump																  ");
            query.AppendLine("               ,wt.leaks = a.leaks																				  ");
            query.AppendLine("               ,wt.water_discount = a.water_discount																  ");
            query.AppendLine("               ,wt.non_effect_revenue_etc = a.non_effect_revenue_etc												  ");
            query.AppendLine(" when not MATCHED then																							  ");
            query.AppendLine("     insert (loc_code, year_mon, water_charge, water_exported, revenue_etc, mtr_undreg,							  ");
            query.AppendLine("             water_undertake, water_pub, unauth_consump, leaks, water_discount, non_effect_revenue_etc)			  ");
            query.AppendLine("     values (a.loc_code, a.year_mon, a.water_charge, a.water_exported, a.revenue_etc, a.mtr_undreg,				  ");
            query.AppendLine("             a.water_undertake, a.water_pub, a.unauth_consump, a.leaks, a.water_discount, a.non_effect_revenue_etc) ");

            manager.ExecuteScript(query.ToString(), null);
        }
    }
}
