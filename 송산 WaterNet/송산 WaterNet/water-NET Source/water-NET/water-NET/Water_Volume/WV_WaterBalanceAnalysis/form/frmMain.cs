﻿using System;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.form;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WV_WaterBalanceAnalysis.work;
using WaterNet.WV_WaterBalanceAnalysis.vo;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using EMFrame.log;

namespace WaterNet.WV_WaterBalanceAnalysis.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================총괄수량수지분석

            object o = EMFrame.statics.AppStatic.USER_MENU["총괄수량수지분석ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.MiddleBlockObject.DataSourceChanged += new EventHandler(MiddleBlockObject_DataSourceChanged);

            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalText = "검색년도";
            this.searchBox1.IntervalType = INTERVAL_TYPE.SINGLE_YEAR;

            this.searchBox1.SmallBlockContainer.Visible = false;
            this.searchBox1.SmallBlockObject.SelectedIndex = 0;
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid1);

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.InitializedGridSetting();
            this.gridManager.DefaultColumnsMapping(this.ultraGrid1);
            this.ultraGrid1.DisplayLayout.RowScrollRegions.Clear();
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.ultraGrid1.ClickCell += new ClickCellEventHandler(ultraGrid1_ClickCell);
        }

        //대블록일때 셀 수정 안된다는 메세지
        private void ultraGrid1_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (this.parameter == null || this.parameter["FTR_CODE"].ToString() != "BZ001")
            {
                return;
            }

            if (e.Cell.Column.Key.IndexOf("_WATER_EXPORTED") != -1 ||
                e.Cell.Column.Key.IndexOf("_REVENUE_ETC") != -1 ||
                e.Cell.Column.Key.IndexOf("_MTR_UNDREG") != -1 ||
                e.Cell.Column.Key.IndexOf("_WATER_UNDERTAKE") != -1 ||
                e.Cell.Column.Key.IndexOf("_WATER_PUB") != -1 ||
                e.Cell.Column.Key.IndexOf("_UNAUTH_CONSUMP") != -1 ||
                e.Cell.Column.Key.IndexOf("_WATER_DISCOUNT") != -1 ||
                e.Cell.Column.Key.IndexOf("_NON_EFFECT_REVENUE_ETC") != -1)
            {
                MessageBox.Show("대블록에서는 값을 수정할수 없습니다.\n중블록에서 수정후 저장해주세요.");
            }
        }

        //중블록에서 배수지 계통이 아닌경우 목록을 삭제한다.
        private void MiddleBlockObject_DataSourceChanged(object sender, EventArgs e)
        {
            DataTable dataTable = ((DataTable)this.searchBox1.MiddleBlockObject.DataSource);

            if (dataTable != null)
            {
                for (int i = dataTable.Rows.Count - 1; i >= 0; i--)
                {
                    if (dataTable.Rows[i]["KT_GBN"].ToString() != "001")
                    {
                        dataTable.Rows.Remove(dataTable.Rows[i]);
                    }
                }
            }
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.ultraGrid1, "총괄수량수지분석");
                this.excelManager.Save("총괄수량수지분석", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //저장버튼 클릭 이벤트 핸들러
        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.ultraGrid1.DataSource == null)
                {
                    return;
                }

                IList<WaterBalanceAnalysisVO> WaterBalanceAnalysisList = this.ultraGrid1.DataSource as IList<WaterBalanceAnalysisVO>;
                WaterBalanceAnalysisWork.GetInstance().UpdateWaterBalanceAnalysis(WaterBalanceAnalysisList);

                Hashtable parameter = this.searchBox1.Parameters;
                this.SelectWaterBalanceAnalysis(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private Hashtable parameter = null;
        //검색버튼 클릭 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.parameter = this.searchBox1.InitializeParameter().Parameters;

                if (!parameter.ContainsKey("YEAR"))
                {
                    parameter["YEAR"] = parameter["STARTDATE"];
                }

                if (parameter.ContainsKey("STARTDATE"))
                {
                    parameter.Remove("STARTDATE");
                }

                //DateTime startDay = new DateTime(Convert.ToInt32(parameter["YEAR"]), 1, 1);

                //parameter["STARTDATE"] = startDay.ToString("yyyyMMdd");
                //parameter["ENDDATE"] = startDay.AddMonths(12).AddDays(0 - startDay.Day).ToString("yyyyMMdd");

                this.SelectWaterBalanceAnalysis(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //총괄수량수지분석 조회
        private void SelectWaterBalanceAnalysis(Hashtable parameter)
        {
            //LOCATION_TYPE type = DataUtils.GetLocationType(parameter["FTR_CODE"].ToString());

            //if (parameter["FTR_CODE"].ToString() == "BZ001")
            //{
            //    MessageBox.Show("검색 대상 블록을 선택해야 합니다.");
            //    return;
            //}

            //GIS에서 블럭 포커스 이동
            this.mainMap.MoveToBlock(parameter);

            //그리드내 그룹컬럼 캡션 설정(검색조건에 따라, XX계통 XXXX년도 총괄수량수지분석);
            this.ultraGrid1.DisplayLayout.Bands[0].Groups["NewGroup0"].Header.Caption =
                parameter["LOC_NAME"].ToString() + " " + parameter["YEAR"].ToString() + "년도";

            this.ultraGrid1.DataSource = WaterBalanceAnalysisWork.GetInstance().SelectWaterBalanceAnalysis(parameter);


            //대블록인경우 컬럼 수정을 못해야 한다.
            bool isEdit = false;

            if (parameter["FTR_CODE"].ToString() != "BZ001")
            {
                isEdit = true;
            }

            this.GridColumnEditChange(isEdit);
        }

        //그리드내 그룹 및 컬럼을 설정
        private void InitializedGridSetting()
        {
            UltraGridColumn gridColumn = null;

            //12번반복,,
            for (int i = 1; i < 13; i++)
            {
                //첫번째 그룹은 그냥 생성,
                UltraGridGroup parentGroup = this.GetReportGroup();
                parentGroup.Header.Caption = i.ToString() + "월";
                parentGroup.Key = i.ToString() + "M";
                parentGroup.RowLayoutGroupInfo.LabelSpan = 2;
                parentGroup.RowLayoutGroupInfo.SpanX = 1;
                parentGroup.RowLayoutGroupInfo.SpanY = 2;


                //컬럼은 두번째 그룹을 부모 그룹으로 설정하고 위치는 Y + 1 로 증가시킴,
                //유수율(공공수량 반영)
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 0;
                gridColumn.CellAppearance.BackColor = Color.FromArgb(192, 255, 255);
                gridColumn.Format = "###,###.#";
                gridColumn.Key = "M" + i.ToString() + "_WATER_RATIO_PUB";

                //유수율(요금수량만 반영)
                //gridColumn = this.GetReportColumn();
                //gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                //gridColumn.RowLayoutColumnInfo.OriginX = 0;
                //gridColumn.RowLayoutColumnInfo.OriginY = 0;
                //gridColumn.Format = "###,###.#";
                //gridColumn.Key = "M" + i.ToString() + "_WATER_RATIO_CHARGE";

                //누수율
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 1;
                gridColumn.Format = "###,###.#";
                gridColumn.Key = "M" + i.ToString() + "_WATER_RATIO_LEAKS";
                gridColumn.CellAppearance.BackColor = SystemColors.Control;

                //생산량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 2;
                gridColumn.CellAppearance.BackColor = Color.FromArgb(255, 255, 192);
                gridColumn.Key = "M" + i.ToString() + "_WATER_SUPPLIED";

                //유효수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 3;
                gridColumn.CellAppearance.BackColor = Color.FromArgb(255, 224, 192);
                gridColumn.Key = "M" + i.ToString() + "_EFFECT_REVENUE";

                //유수수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 4;
                gridColumn.CellAppearance.BackColor = Color.FromArgb(192, 255, 192);
                gridColumn.Key = "M" + i.ToString() + "_REVENUE";

                //요금수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 5;
                gridColumn.Key = "M" + i.ToString() + "_WATER_CHARGE";
                gridColumn.CellAppearance.BackColor = SystemColors.Control;

                //분수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 6;
                gridColumn.Key = "M" + i.ToString() + "_WATER_EXPORTED";

                //기타유수수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 7;
                gridColumn.Key = "M" + i.ToString() + "_REVENUE_ETC";

                //무수수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 8;
                gridColumn.CellAppearance.BackColor = Color.FromArgb(192, 255, 192);
                gridColumn.Key = "M" + i.ToString() + "_NON_REVENUE";

                //계량기불감수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 9;
                gridColumn.Key = "M" + i.ToString() + "_MTR_UNDREG";

                //수도사업용수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 10;
                gridColumn.Key = "M" + i.ToString() + "_WATER_UNDERTAKE";

                //공공수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 11;
                gridColumn.Key = "M" + i.ToString() + "_WATER_PUB";

                //부정사용량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 12;
                gridColumn.Key = "M" + i.ToString() + "_UNAUTH_CONSUMP";

                //무효수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 13;
                gridColumn.CellAppearance.BackColor = Color.FromArgb(255, 224, 192);
                gridColumn.Key = "M" + i.ToString() + "_NON_EFFECT_REVENUE";

                //누수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 14;
                gridColumn.Key = "M" + i.ToString() + "_LEAKS";
                gridColumn.CellAppearance.BackColor = SystemColors.Control;

                //누수량(추정)
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 15;
                gridColumn.CellAppearance.BackColor = SystemColors.Control;
                gridColumn.Key = "M" + i.ToString() + "_LEAKS_EST";

                //감액조정수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 16;
                gridColumn.Key = "M" + i.ToString() + "_WATER_DISCOUNT";

                //기타무효수량
                gridColumn = this.GetReportColumn();
                gridColumn.RowLayoutColumnInfo.ParentGroup = parentGroup;
                gridColumn.RowLayoutColumnInfo.OriginX = 0;
                gridColumn.RowLayoutColumnInfo.OriginY = 17;
                gridColumn.Key = "M" + i.ToString() + "_NON_EFFECT_REVENUE_ETC";
            }
        }

        private void GridColumnEditChange(bool isEdit)
        {
            ColumnsCollection columns = this.ultraGrid1.DisplayLayout.Bands[0].Columns;

            //12번반복,,
            for (int i = 1; i < 13; i++)
            {
                if (isEdit)
                {
                    columns["M" + i.ToString() + "_WATER_EXPORTED"].CellActivation = Activation.AllowEdit;
                    columns["M" + i.ToString() + "_WATER_EXPORTED"].CellClickAction = CellClickAction.EditAndSelectText;
                    columns["M" + i.ToString() + "_REVENUE_ETC"].CellActivation = Activation.AllowEdit;
                    columns["M" + i.ToString() + "_REVENUE_ETC"].CellClickAction = CellClickAction.EditAndSelectText;
                    columns["M" + i.ToString() + "_MTR_UNDREG"].CellActivation = Activation.AllowEdit;
                    columns["M" + i.ToString() + "_MTR_UNDREG"].CellClickAction = CellClickAction.EditAndSelectText;
                    columns["M" + i.ToString() + "_WATER_UNDERTAKE"].CellActivation = Activation.AllowEdit;
                    columns["M" + i.ToString() + "_WATER_UNDERTAKE"].CellClickAction = CellClickAction.EditAndSelectText;
                    columns["M" + i.ToString() + "_WATER_PUB"].CellActivation = Activation.AllowEdit;
                    columns["M" + i.ToString() + "_WATER_PUB"].CellClickAction = CellClickAction.EditAndSelectText;
                    columns["M" + i.ToString() + "_UNAUTH_CONSUMP"].CellActivation = Activation.AllowEdit;
                    columns["M" + i.ToString() + "_UNAUTH_CONSUMP"].CellClickAction = CellClickAction.EditAndSelectText;
                    //columns["M" + i.ToString() + "_LEAKS"].CellActivation = Activation.AllowEdit;
                    //columns["M" + i.ToString() + "_LEAKS"].CellClickAction = CellClickAction.EditAndSelectText;
                    columns["M" + i.ToString() + "_WATER_DISCOUNT"].CellActivation = Activation.AllowEdit;
                    columns["M" + i.ToString() + "_WATER_DISCOUNT"].CellClickAction = CellClickAction.EditAndSelectText;
                    columns["M" + i.ToString() + "_NON_EFFECT_REVENUE_ETC"].CellActivation = Activation.AllowEdit;
                    columns["M" + i.ToString() + "_NON_EFFECT_REVENUE_ETC"].CellClickAction = CellClickAction.EditAndSelectText;
                }

                if (!isEdit)
                {
                    columns["M" + i.ToString() + "_WATER_EXPORTED"].CellActivation = Activation.ActivateOnly;
                    columns["M" + i.ToString() + "_WATER_EXPORTED"].CellClickAction = CellClickAction.CellSelect;
                    columns["M" + i.ToString() + "_REVENUE_ETC"].CellActivation = Activation.ActivateOnly;
                    columns["M" + i.ToString() + "_REVENUE_ETC"].CellClickAction = CellClickAction.CellSelect;
                    columns["M" + i.ToString() + "_MTR_UNDREG"].CellActivation = Activation.ActivateOnly;
                    columns["M" + i.ToString() + "_MTR_UNDREG"].CellClickAction = CellClickAction.CellSelect;
                    columns["M" + i.ToString() + "_WATER_UNDERTAKE"].CellActivation = Activation.ActivateOnly;
                    columns["M" + i.ToString() + "_WATER_UNDERTAKE"].CellClickAction = CellClickAction.CellSelect;
                    columns["M" + i.ToString() + "_WATER_PUB"].CellActivation = Activation.ActivateOnly;
                    columns["M" + i.ToString() + "_WATER_PUB"].CellClickAction = CellClickAction.CellSelect;
                    columns["M" + i.ToString() + "_UNAUTH_CONSUMP"].CellActivation = Activation.ActivateOnly;
                    columns["M" + i.ToString() + "_UNAUTH_CONSUMP"].CellClickAction = CellClickAction.CellSelect;
                    columns["M" + i.ToString() + "_LEAKS"].CellActivation = Activation.ActivateOnly;
                    columns["M" + i.ToString() + "_LEAKS"].CellClickAction = CellClickAction.CellSelect;
                    columns["M" + i.ToString() + "_WATER_DISCOUNT"].CellActivation = Activation.ActivateOnly;
                    columns["M" + i.ToString() + "_WATER_DISCOUNT"].CellClickAction = CellClickAction.CellSelect;
                    columns["M" + i.ToString() + "_NON_EFFECT_REVENUE_ETC"].CellActivation = Activation.ActivateOnly;
                    columns["M" + i.ToString() + "_NON_EFFECT_REVENUE_ETC"].CellClickAction = CellClickAction.CellSelect;
                }
            }
        }

        private UltraGridGroup GetReportGroup()
        {
            UltraGridGroup gridGroup = this.ultraGrid1.DisplayLayout.Bands[0].Groups.Add();
            gridGroup.Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            gridGroup.Header.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            gridGroup.RowLayoutGroupInfo.AllowCellSizing = RowLayoutSizing.None;
            gridGroup.RowLayoutGroupInfo.AllowLabelSizing = RowLayoutSizing.None;
            gridGroup.RowLayoutGroupInfo.LabelSpan = 1;
            gridGroup.RowLayoutGroupInfo.SpanX = 1;
            gridGroup.RowLayoutGroupInfo.SpanY = 1;
            gridGroup.RowLayoutGroupInfo.PreferredLabelSize = new Size(70, 30);
            return gridGroup;
        }

        private UltraGridColumn GetReportColumn()
        {
            UltraGridColumn gridColumn = this.ultraGrid1.DisplayLayout.Bands[0].Columns.Add();
            gridColumn.RowLayoutColumnInfo.AllowCellSizing = RowLayoutSizing.None;
            gridColumn.RowLayoutColumnInfo.AllowLabelSizing = RowLayoutSizing.None;
            gridColumn.RowLayoutColumnInfo.LabelPosition = LabelPosition.None;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
            gridColumn.RowLayoutColumnInfo.Column.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            gridColumn.RowLayoutColumnInfo.SpanX = 1;
            gridColumn.RowLayoutColumnInfo.SpanY = 1;
            gridColumn.RowLayoutColumnInfo.LabelSpan = 1;
            gridColumn.DataType = typeof(double);
            gridColumn.Format = "###,###,###";
            return gridColumn;
        }
    }
}