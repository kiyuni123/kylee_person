﻿namespace WaterNet.WV_WaterBalanceAnalysis.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("WaterBalanceAnalysisVO", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_WATER_RATIO_PUB");
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_WATER_RATIO_CHARGE");
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_WATER_RATIO_LEAKS");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_WATER_SUPPLIED");
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_EFFECT_REVENUE");
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_REVENUE");
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_WATER_CHARGE");
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_WATER_EXPORTED");
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_REVENUE_ETC");
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_NON_REVENUE");
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_MTR_UNDREG");
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_WATER_UNDERTAKE");
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_WATER_PUB");
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_UNAUTH_CONSUMP");
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_NON_EFFECT_REVENUE");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_LEAKS");
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn236 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_LEAKS_EST");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_WATER_DISCOUNT");
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TOTAL_NON_EFFECT_REVENUE_ETC");
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 19569000);
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 19569001);
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup2", 19569002);
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("WATER_RATIO_PUB", 19569003);
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("WATER_RATIO_LEAKS", 19569004);
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("WATER_SUPPLIED", 19569005);
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup7 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("EFFECT_REVENUE", 19569006);
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup8 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("REVENUE", 19569007);
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup9 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("WATER_CHARGE", 19569008);
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup10 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("WATER_EXPORTED", 19569009);
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup11 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("REVENUE_ECT", 19569010);
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup12 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NON_REVENUE", 19569011);
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup13 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("MTR_UNDREG", 19569012);
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup14 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("WATER_UNDERTAKE", 19569013);
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup15 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("WATER_PUB", 19569014);
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup16 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("UNAUTH_CONSUMP", 19569015);
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup17 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NON_EFFECT_REVENUE", 19569016);
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup18 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("LEAKS", 19569017);
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup19 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("LEAKS_EST", 19569018);
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup20 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("WATER_DISCOUNT", 19569019);
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup21 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NON_EFFECT_REVENUE_ETC", 19569020);
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.RowScrollRegion rowScrollRegion1 = new Infragistics.Win.UltraWinGrid.RowScrollRegion(423);
            Infragistics.Win.UltraWinGrid.RowScrollRegion rowScrollRegion2 = new Infragistics.Win.UltraWinGrid.RowScrollRegion(-7);
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.updateBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Location = new System.Drawing.Point(1321, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 628);
            this.pictureBox4.TabIndex = 28;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 628);
            this.pictureBox3.TabIndex = 27;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 638);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1331, 10);
            this.pictureBox2.TabIndex = 26;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1331, 10);
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.Controls.Add(this.updateBtn);
            this.panel8.Controls.Add(this.searchBtn);
            this.panel8.Controls.Add(this.excelBtn);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(10, 121);
            this.panel8.Margin = new System.Windows.Forms.Padding(0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1311, 30);
            this.panel8.TabIndex = 36;
            // 
            // updateBtn
            // 
            this.updateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.updateBtn.AutoSize = true;
            this.updateBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.updateBtn.Location = new System.Drawing.Point(1225, 2);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(40, 25);
            this.updateBtn.TabIndex = 27;
            this.updateBtn.TabStop = false;
            this.updateBtn.Text = "저장";
            this.updateBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(1271, 2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 22;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(1179, 2);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 23;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(10, 151);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(1311, 10);
            this.pictureBox5.TabIndex = 37;
            this.pictureBox5.TabStop = false;
            // 
            // ultraGrid1
            // 
            ultraGridBand1.AutoPreviewMaxLines = 1;
            ultraGridColumn11.Header.VisiblePosition = 0;
            ultraGridColumn11.Hidden = true;
            ultraGridColumn11.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn11.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn11.RowLayoutColumnInfo.OriginX = 3;
            ultraGridColumn11.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn11.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn11.RowLayoutColumnInfo.SpanY = 2;
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            appearance1.TextHAlignAsString = "Right";
            appearance1.TextVAlignAsString = "Middle";
            ultraGridColumn1.CellAppearance = appearance1;
            ultraGridColumn1.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn1.DefaultCellValue")));
            ultraGridColumn1.Format = "###,###.0";
            ultraGridColumn1.Header.VisiblePosition = 1;
            ultraGridColumn1.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn1.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn1.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn1.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn1.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn1.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn1.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn1.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 29);
            ultraGridColumn1.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn1.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn1.RowLayoutColumnInfo.SpanY = 1;
            appearance2.TextHAlignAsString = "Right";
            appearance2.TextVAlignAsString = "Middle";
            ultraGridColumn2.CellAppearance = appearance2;
            ultraGridColumn2.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn2.DefaultCellValue")));
            ultraGridColumn2.Format = "###,###.0";
            ultraGridColumn2.Header.VisiblePosition = 2;
            ultraGridColumn2.Hidden = true;
            ultraGridColumn2.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn2.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn2.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn2.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn2.RowLayoutColumnInfo.OriginY = 1;
            ultraGridColumn2.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn2.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn2.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 29);
            ultraGridColumn2.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn2.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn2.RowLayoutColumnInfo.SpanY = 1;
            appearance3.BackColor = System.Drawing.SystemColors.Control;
            appearance3.TextHAlignAsString = "Right";
            appearance3.TextVAlignAsString = "Middle";
            ultraGridColumn3.CellAppearance = appearance3;
            ultraGridColumn3.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn3.DefaultCellValue")));
            ultraGridColumn3.Format = "###,###.0";
            ultraGridColumn3.Header.VisiblePosition = 3;
            ultraGridColumn3.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn3.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn3.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn3.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn3.RowLayoutColumnInfo.OriginY = 1;
            ultraGridColumn3.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn3.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn3.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn3.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn3.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn3.RowLayoutColumnInfo.SpanY = 1;
            appearance4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            appearance4.TextHAlignAsString = "Right";
            appearance4.TextVAlignAsString = "Middle";
            ultraGridColumn4.CellAppearance = appearance4;
            ultraGridColumn4.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn4.DefaultCellValue")));
            ultraGridColumn4.Format = "###,###,###";
            ultraGridColumn4.Header.VisiblePosition = 4;
            ultraGridColumn4.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn4.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn4.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn4.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn4.RowLayoutColumnInfo.OriginY = 2;
            ultraGridColumn4.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn4.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn4.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn4.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn4.RowLayoutColumnInfo.SpanY = 1;
            appearance5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            appearance5.TextHAlignAsString = "Right";
            appearance5.TextVAlignAsString = "Middle";
            ultraGridColumn5.CellAppearance = appearance5;
            ultraGridColumn5.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn5.DefaultCellValue")));
            ultraGridColumn5.Format = "###,###,###";
            ultraGridColumn5.Header.VisiblePosition = 5;
            ultraGridColumn5.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn5.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn5.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn5.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn5.RowLayoutColumnInfo.OriginY = 3;
            ultraGridColumn5.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn5.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn5.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn5.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn5.RowLayoutColumnInfo.SpanY = 1;
            appearance6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            appearance6.TextHAlignAsString = "Right";
            appearance6.TextVAlignAsString = "Middle";
            ultraGridColumn6.CellAppearance = appearance6;
            ultraGridColumn6.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn6.DefaultCellValue")));
            ultraGridColumn6.Format = "###,###,###";
            ultraGridColumn6.Header.VisiblePosition = 6;
            ultraGridColumn6.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn6.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn6.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn6.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn6.RowLayoutColumnInfo.OriginY = 4;
            ultraGridColumn6.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn6.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn6.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn6.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn6.RowLayoutColumnInfo.SpanY = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Control;
            appearance7.TextHAlignAsString = "Right";
            appearance7.TextVAlignAsString = "Middle";
            ultraGridColumn7.CellAppearance = appearance7;
            ultraGridColumn7.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn7.DefaultCellValue")));
            ultraGridColumn7.Format = "###,###,###";
            ultraGridColumn7.Header.VisiblePosition = 7;
            ultraGridColumn7.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn7.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn7.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn7.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn7.RowLayoutColumnInfo.OriginY = 5;
            ultraGridColumn7.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn7.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn7.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn7.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn7.RowLayoutColumnInfo.SpanY = 1;
            appearance8.TextHAlignAsString = "Right";
            appearance8.TextVAlignAsString = "Middle";
            ultraGridColumn8.CellAppearance = appearance8;
            ultraGridColumn8.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn8.DefaultCellValue")));
            ultraGridColumn8.Format = "###,###,###";
            ultraGridColumn8.Header.VisiblePosition = 8;
            ultraGridColumn8.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn8.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn8.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn8.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn8.RowLayoutColumnInfo.OriginY = 6;
            ultraGridColumn8.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn8.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn8.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn8.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn8.RowLayoutColumnInfo.SpanY = 1;
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            ultraGridColumn20.CellAppearance = appearance9;
            ultraGridColumn20.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn20.DefaultCellValue")));
            ultraGridColumn20.Format = "###,###,###";
            ultraGridColumn20.Header.VisiblePosition = 9;
            ultraGridColumn20.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn20.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn20.RowLayoutColumnInfo.OriginY = 7;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn20.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 20);
            ultraGridColumn20.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn20.RowLayoutColumnInfo.SpanY = 1;
            appearance10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            appearance10.TextHAlignAsString = "Right";
            appearance10.TextVAlignAsString = "Middle";
            ultraGridColumn10.CellAppearance = appearance10;
            ultraGridColumn10.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn10.DefaultCellValue")));
            ultraGridColumn10.Format = "###,###,###";
            ultraGridColumn10.Header.VisiblePosition = 10;
            ultraGridColumn10.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn10.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn10.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn10.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn10.RowLayoutColumnInfo.OriginY = 8;
            ultraGridColumn10.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn10.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn10.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn10.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn10.RowLayoutColumnInfo.SpanY = 1;
            appearance11.TextHAlignAsString = "Right";
            appearance11.TextVAlignAsString = "Middle";
            ultraGridColumn12.CellAppearance = appearance11;
            ultraGridColumn12.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn12.DefaultCellValue")));
            ultraGridColumn12.Format = "###,###,###";
            ultraGridColumn12.Header.VisiblePosition = 11;
            ultraGridColumn12.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn12.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn12.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn12.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn12.RowLayoutColumnInfo.OriginY = 9;
            ultraGridColumn12.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn12.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn12.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn12.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn12.RowLayoutColumnInfo.SpanY = 1;
            appearance12.TextHAlignAsString = "Right";
            appearance12.TextVAlignAsString = "Middle";
            ultraGridColumn13.CellAppearance = appearance12;
            ultraGridColumn13.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn13.DefaultCellValue")));
            ultraGridColumn13.Format = "###,###,###";
            ultraGridColumn13.Header.VisiblePosition = 12;
            ultraGridColumn13.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn13.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn13.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn13.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn13.RowLayoutColumnInfo.OriginY = 10;
            ultraGridColumn13.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn13.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn13.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn13.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn13.RowLayoutColumnInfo.SpanY = 1;
            appearance13.TextHAlignAsString = "Right";
            appearance13.TextVAlignAsString = "Middle";
            ultraGridColumn14.CellAppearance = appearance13;
            ultraGridColumn14.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn14.DefaultCellValue")));
            ultraGridColumn14.Format = "###,###,###";
            ultraGridColumn14.Header.VisiblePosition = 13;
            ultraGridColumn14.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn14.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn14.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn14.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn14.RowLayoutColumnInfo.OriginY = 11;
            ultraGridColumn14.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn14.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn14.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn14.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn14.RowLayoutColumnInfo.SpanY = 1;
            appearance14.TextHAlignAsString = "Right";
            appearance14.TextVAlignAsString = "Middle";
            ultraGridColumn15.CellAppearance = appearance14;
            ultraGridColumn15.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn15.DefaultCellValue")));
            ultraGridColumn15.Format = "###,###,###";
            ultraGridColumn15.Header.VisiblePosition = 14;
            ultraGridColumn15.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn15.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn15.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn15.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn15.RowLayoutColumnInfo.OriginY = 12;
            ultraGridColumn15.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn15.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn15.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn15.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn15.RowLayoutColumnInfo.SpanY = 1;
            appearance15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            appearance15.TextHAlignAsString = "Right";
            appearance15.TextVAlignAsString = "Middle";
            ultraGridColumn16.CellAppearance = appearance15;
            ultraGridColumn16.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn16.DefaultCellValue")));
            ultraGridColumn16.Format = "###,###,###";
            ultraGridColumn16.Header.VisiblePosition = 15;
            ultraGridColumn16.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn16.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn16.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn16.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn16.RowLayoutColumnInfo.OriginY = 13;
            ultraGridColumn16.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn16.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn16.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn16.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn16.RowLayoutColumnInfo.SpanY = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Control;
            appearance16.TextHAlignAsString = "Right";
            appearance16.TextVAlignAsString = "Middle";
            ultraGridColumn17.CellAppearance = appearance16;
            ultraGridColumn17.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn17.DefaultCellValue")));
            ultraGridColumn17.Format = "###,###,###";
            ultraGridColumn17.Header.VisiblePosition = 16;
            ultraGridColumn17.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn17.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn17.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn17.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn17.RowLayoutColumnInfo.OriginY = 14;
            ultraGridColumn17.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn17.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn17.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn17.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn17.RowLayoutColumnInfo.SpanY = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Control;
            appearance17.TextHAlignAsString = "Right";
            appearance17.TextVAlignAsString = "Middle";
            ultraGridColumn236.CellAppearance = appearance17;
            ultraGridColumn236.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn236.DefaultCellValue")));
            ultraGridColumn236.Format = "###,###,###";
            ultraGridColumn236.Header.VisiblePosition = 17;
            ultraGridColumn236.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn236.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn236.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn236.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn236.RowLayoutColumnInfo.OriginY = 15;
            ultraGridColumn236.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn236.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn236.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn236.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn236.RowLayoutColumnInfo.SpanY = 1;
            appearance18.TextHAlignAsString = "Right";
            appearance18.TextVAlignAsString = "Middle";
            ultraGridColumn18.CellAppearance = appearance18;
            ultraGridColumn18.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn18.DefaultCellValue")));
            ultraGridColumn18.Format = "###,###,###";
            ultraGridColumn18.Header.VisiblePosition = 18;
            ultraGridColumn18.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn18.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn18.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn18.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn18.RowLayoutColumnInfo.OriginY = 16;
            ultraGridColumn18.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn18.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn18.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 18);
            ultraGridColumn18.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn18.RowLayoutColumnInfo.SpanY = 1;
            appearance19.TextHAlignAsString = "Right";
            appearance19.TextVAlignAsString = "Middle";
            ultraGridColumn19.CellAppearance = appearance19;
            ultraGridColumn19.DefaultCellValue = ((object)(resources.GetObject("ultraGridColumn19.DefaultCellValue")));
            ultraGridColumn19.Format = "###,###,###";
            ultraGridColumn19.Header.VisiblePosition = 19;
            ultraGridColumn19.RowLayoutColumnInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn19.RowLayoutColumnInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridColumn19.RowLayoutColumnInfo.LabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            ultraGridColumn19.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn19.RowLayoutColumnInfo.OriginY = 17;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn19.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(89, 20);
            ultraGridColumn19.RowLayoutColumnInfo.SpanX = 1;
            ultraGridColumn19.RowLayoutColumnInfo.SpanY = 1;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn11,
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8,
            ultraGridColumn20,
            ultraGridColumn10,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn236,
            ultraGridColumn18,
            ultraGridColumn19});
            appearance20.TextHAlignAsString = "Center";
            appearance20.TextVAlignAsString = "Middle";
            ultraGridGroup1.Header.Appearance = appearance20;
            ultraGridGroup1.Header.Caption = "배수지계통";
            ultraGridGroup1.Key = "NewGroup0";
            ultraGridGroup1.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup1.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(228, 20);
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 3;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 20;
            appearance21.TextHAlignAsString = "Center";
            appearance21.TextVAlignAsString = "Middle";
            ultraGridGroup2.Header.Appearance = appearance21;
            ultraGridGroup2.Header.Caption = "구분";
            ultraGridGroup2.Key = "NewGroup1";
            ultraGridGroup2.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup2.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.ParentGroupIndex = 0;
            ultraGridGroup2.RowLayoutGroupInfo.ParentGroupKey = "NewGroup0";
            ultraGridGroup2.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(139, 0);
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 19;
            appearance22.TextHAlignAsString = "Center";
            appearance22.TextVAlignAsString = "Middle";
            ultraGridGroup3.Header.Appearance = appearance22;
            ultraGridGroup3.Header.Caption = "계";
            ultraGridGroup3.Key = "NewGroup2";
            ultraGridGroup3.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup3.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.ParentGroupIndex = 0;
            ultraGridGroup3.RowLayoutGroupInfo.ParentGroupKey = "NewGroup0";
            ultraGridGroup3.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(89, 0);
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 2;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 19;
            appearance24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            appearance24.TextHAlignAsString = "Center";
            appearance24.TextVAlignAsString = "Middle";
            ultraGridGroup4.Header.Appearance = appearance24;
            ultraGridGroup4.Header.Caption = "유수율";
            ultraGridGroup4.Key = "WATER_RATIO_PUB";
            ultraGridGroup4.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup4.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup4.RowLayoutGroupInfo.ParentGroupIndex = 1;
            ultraGridGroup4.RowLayoutGroupInfo.ParentGroupKey = "NewGroup1";
            ultraGridGroup4.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(139, 29);
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 18;
            appearance25.TextHAlignAsString = "Center";
            appearance25.TextVAlignAsString = "Middle";
            ultraGridGroup5.Header.Appearance = appearance25;
            ultraGridGroup5.Header.Caption = "누수율";
            ultraGridGroup5.Key = "WATER_RATIO_LEAKS";
            ultraGridGroup5.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup5.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup5.RowLayoutGroupInfo.ParentGroupIndex = 3;
            ultraGridGroup5.RowLayoutGroupInfo.ParentGroupKey = "WATER_RATIO_PUB";
            ultraGridGroup5.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 17;
            appearance26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            appearance26.TextHAlignAsString = "Center";
            appearance26.TextVAlignAsString = "Middle";
            ultraGridGroup6.Header.Appearance = appearance26;
            ultraGridGroup6.Header.Caption = "생산량";
            ultraGridGroup6.Key = "WATER_SUPPLIED";
            ultraGridGroup6.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup6.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup6.RowLayoutGroupInfo.ParentGroupIndex = 4;
            ultraGridGroup6.RowLayoutGroupInfo.ParentGroupKey = "WATER_RATIO_LEAKS";
            ultraGridGroup6.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 16;
            appearance27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            appearance27.TextHAlignAsString = "Center";
            appearance27.TextVAlignAsString = "Middle";
            ultraGridGroup7.Header.Appearance = appearance27;
            ultraGridGroup7.Header.Caption = "유효수량";
            ultraGridGroup7.Key = "EFFECT_REVENUE";
            ultraGridGroup7.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup7.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup7.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup7.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup7.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup7.RowLayoutGroupInfo.ParentGroupIndex = 5;
            ultraGridGroup7.RowLayoutGroupInfo.ParentGroupKey = "WATER_SUPPLIED";
            ultraGridGroup7.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup7.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup7.RowLayoutGroupInfo.SpanY = 15;
            appearance28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            appearance28.TextHAlignAsString = "Center";
            appearance28.TextVAlignAsString = "Middle";
            ultraGridGroup8.Header.Appearance = appearance28;
            ultraGridGroup8.Header.Caption = "유수수량";
            ultraGridGroup8.Key = "REVENUE";
            ultraGridGroup8.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup8.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup8.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup8.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup8.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup8.RowLayoutGroupInfo.ParentGroupIndex = 6;
            ultraGridGroup8.RowLayoutGroupInfo.ParentGroupKey = "EFFECT_REVENUE";
            ultraGridGroup8.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup8.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup8.RowLayoutGroupInfo.SpanY = 14;
            appearance29.TextHAlignAsString = "Center";
            appearance29.TextVAlignAsString = "Middle";
            ultraGridGroup9.Header.Appearance = appearance29;
            ultraGridGroup9.Header.Caption = "요금수량";
            ultraGridGroup9.Key = "WATER_CHARGE";
            ultraGridGroup9.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup9.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup9.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup9.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup9.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup9.RowLayoutGroupInfo.ParentGroupIndex = 7;
            ultraGridGroup9.RowLayoutGroupInfo.ParentGroupKey = "REVENUE";
            ultraGridGroup9.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup9.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup9.RowLayoutGroupInfo.SpanY = 13;
            appearance30.TextHAlignAsString = "Center";
            appearance30.TextVAlignAsString = "Middle";
            ultraGridGroup10.Header.Appearance = appearance30;
            ultraGridGroup10.Header.Caption = "분수량";
            ultraGridGroup10.Key = "WATER_EXPORTED";
            ultraGridGroup10.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup10.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup10.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup10.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup10.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup10.RowLayoutGroupInfo.ParentGroupIndex = 8;
            ultraGridGroup10.RowLayoutGroupInfo.ParentGroupKey = "WATER_CHARGE";
            ultraGridGroup10.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup10.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup10.RowLayoutGroupInfo.SpanY = 12;
            appearance45.TextHAlignAsString = "Center";
            appearance45.TextVAlignAsString = "Middle";
            ultraGridGroup11.Header.Appearance = appearance45;
            ultraGridGroup11.Header.Caption = "기타유수수량";
            ultraGridGroup11.Key = "REVENUE_ECT";
            ultraGridGroup11.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup11.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup11.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup11.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup11.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup11.RowLayoutGroupInfo.ParentGroupIndex = 9;
            ultraGridGroup11.RowLayoutGroupInfo.ParentGroupKey = "WATER_EXPORTED";
            ultraGridGroup11.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup11.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup11.RowLayoutGroupInfo.SpanY = 11;
            appearance46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            appearance46.TextHAlignAsString = "Center";
            appearance46.TextVAlignAsString = "Middle";
            ultraGridGroup12.Header.Appearance = appearance46;
            ultraGridGroup12.Header.Caption = "무수수량";
            ultraGridGroup12.Key = "NON_REVENUE";
            ultraGridGroup12.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup12.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup12.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup12.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup12.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup12.RowLayoutGroupInfo.ParentGroupIndex = 10;
            ultraGridGroup12.RowLayoutGroupInfo.ParentGroupKey = "REVENUE_ECT";
            ultraGridGroup12.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup12.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup12.RowLayoutGroupInfo.SpanY = 10;
            appearance47.TextHAlignAsString = "Center";
            appearance47.TextVAlignAsString = "Middle";
            ultraGridGroup13.Header.Appearance = appearance47;
            ultraGridGroup13.Header.Caption = "계량기불감수량";
            ultraGridGroup13.Key = "MTR_UNDREG";
            ultraGridGroup13.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup13.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup13.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup13.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup13.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup13.RowLayoutGroupInfo.ParentGroupIndex = 11;
            ultraGridGroup13.RowLayoutGroupInfo.ParentGroupKey = "NON_REVENUE";
            ultraGridGroup13.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup13.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup13.RowLayoutGroupInfo.SpanY = 9;
            appearance48.TextHAlignAsString = "Center";
            appearance48.TextVAlignAsString = "Middle";
            ultraGridGroup14.Header.Appearance = appearance48;
            ultraGridGroup14.Header.Caption = "수도사업용수량";
            ultraGridGroup14.Key = "WATER_UNDERTAKE";
            ultraGridGroup14.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup14.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup14.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup14.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup14.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup14.RowLayoutGroupInfo.ParentGroupIndex = 12;
            ultraGridGroup14.RowLayoutGroupInfo.ParentGroupKey = "MTR_UNDREG";
            ultraGridGroup14.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup14.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup14.RowLayoutGroupInfo.SpanY = 8;
            appearance49.TextHAlignAsString = "Center";
            appearance49.TextVAlignAsString = "Middle";
            ultraGridGroup15.Header.Appearance = appearance49;
            ultraGridGroup15.Header.Caption = "공공수량";
            ultraGridGroup15.Key = "WATER_PUB";
            ultraGridGroup15.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup15.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup15.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup15.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup15.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup15.RowLayoutGroupInfo.ParentGroupIndex = 13;
            ultraGridGroup15.RowLayoutGroupInfo.ParentGroupKey = "WATER_UNDERTAKE";
            ultraGridGroup15.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup15.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup15.RowLayoutGroupInfo.SpanY = 7;
            appearance50.TextHAlignAsString = "Center";
            appearance50.TextVAlignAsString = "Middle";
            ultraGridGroup16.Header.Appearance = appearance50;
            ultraGridGroup16.Header.Caption = "부정사용량";
            ultraGridGroup16.Key = "UNAUTH_CONSUMP";
            ultraGridGroup16.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup16.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup16.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup16.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup16.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup16.RowLayoutGroupInfo.ParentGroupIndex = 14;
            ultraGridGroup16.RowLayoutGroupInfo.ParentGroupKey = "WATER_PUB";
            ultraGridGroup16.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup16.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup16.RowLayoutGroupInfo.SpanY = 6;
            appearance51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            appearance51.TextHAlignAsString = "Center";
            appearance51.TextVAlignAsString = "Middle";
            ultraGridGroup17.Header.Appearance = appearance51;
            ultraGridGroup17.Header.Caption = "무효수량";
            ultraGridGroup17.Key = "NON_EFFECT_REVENUE";
            ultraGridGroup17.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup17.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup17.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup17.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup17.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup17.RowLayoutGroupInfo.ParentGroupIndex = 15;
            ultraGridGroup17.RowLayoutGroupInfo.ParentGroupKey = "UNAUTH_CONSUMP";
            ultraGridGroup17.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup17.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup17.RowLayoutGroupInfo.SpanY = 5;
            appearance52.TextHAlignAsString = "Center";
            appearance52.TextVAlignAsString = "Middle";
            ultraGridGroup18.Header.Appearance = appearance52;
            ultraGridGroup18.Header.Caption = "누수량";
            ultraGridGroup18.Key = "LEAKS";
            ultraGridGroup18.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup18.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup18.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup18.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup18.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup18.RowLayoutGroupInfo.ParentGroupIndex = 16;
            ultraGridGroup18.RowLayoutGroupInfo.ParentGroupKey = "NON_EFFECT_REVENUE";
            ultraGridGroup18.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup18.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup18.RowLayoutGroupInfo.SpanY = 4;
            appearance53.TextHAlignAsString = "Center";
            appearance53.TextVAlignAsString = "Middle";
            ultraGridGroup19.Header.Appearance = appearance53;
            ultraGridGroup19.Header.Caption = "누수량(MNF분석결과)";
            ultraGridGroup19.Key = "LEAKS_EST";
            ultraGridGroup19.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup19.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup19.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup19.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup19.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup19.RowLayoutGroupInfo.ParentGroupIndex = 17;
            ultraGridGroup19.RowLayoutGroupInfo.ParentGroupKey = "LEAKS";
            ultraGridGroup19.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup19.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup19.RowLayoutGroupInfo.SpanY = 3;
            appearance54.TextHAlignAsString = "Center";
            appearance54.TextVAlignAsString = "Middle";
            ultraGridGroup20.Header.Appearance = appearance54;
            ultraGridGroup20.Header.Caption = "감액조정수량";
            ultraGridGroup20.Key = "WATER_DISCOUNT";
            ultraGridGroup20.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup20.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup20.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup20.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup20.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup20.RowLayoutGroupInfo.ParentGroupIndex = 18;
            ultraGridGroup20.RowLayoutGroupInfo.ParentGroupKey = "LEAKS_EST";
            ultraGridGroup20.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup20.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup20.RowLayoutGroupInfo.SpanY = 2;
            appearance55.TextHAlignAsString = "Center";
            appearance55.TextVAlignAsString = "Middle";
            ultraGridGroup21.Header.Appearance = appearance55;
            ultraGridGroup21.Header.Caption = "기타무효수량";
            ultraGridGroup21.Key = "NON_EFFECT_REVENUE_ETC";
            ultraGridGroup21.RowLayoutGroupInfo.AllowCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup21.RowLayoutGroupInfo.AllowLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;
            ultraGridGroup21.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup21.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup21.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup21.RowLayoutGroupInfo.ParentGroupIndex = 19;
            ultraGridGroup21.RowLayoutGroupInfo.ParentGroupKey = "WATER_DISCOUNT";
            ultraGridGroup21.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridGroup21.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup21.RowLayoutGroupInfo.SpanY = 1;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6,
            ultraGridGroup7,
            ultraGridGroup8,
            ultraGridGroup9,
            ultraGridGroup10,
            ultraGridGroup11,
            ultraGridGroup12,
            ultraGridGroup13,
            ultraGridGroup14,
            ultraGridGroup15,
            ultraGridGroup16,
            ultraGridGroup17,
            ultraGridGroup18,
            ultraGridGroup19,
            ultraGridGroup20,
            ultraGridGroup21});
            ultraGridBand1.MaxRows = 1;
            ultraGridBand1.RowLayoutLabelStyle = Infragistics.Win.UltraWinGrid.RowLayoutLabelStyle.WithCellData;
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid1.DisplayLayout.InterBandSpacing = 1;
            this.ultraGrid1.DisplayLayout.MaxBandDepth = 1;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            rowScrollRegion1.ScrollPosition = 0;
            rowScrollRegion2.ScrollPosition = 0;
            this.ultraGrid1.DisplayLayout.RowScrollRegions.Add(rowScrollRegion1);
            this.ultraGrid1.DisplayLayout.RowScrollRegions.Add(rowScrollRegion2);
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(10, 161);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(1311, 477);
            this.ultraGrid1.TabIndex = 48;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1311, 111);
            this.searchBox1.TabIndex = 35;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1331, 648);
            this.Controls.Add(this.ultraGrid1);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.MinimumSize = new System.Drawing.Size(600, 600);
            this.Name = "frmMain";
            this.Text = "총괄수량수지 분석";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private WaterNet.WV_Common.form.SearchBox searchBox1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button updateBtn;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
    }
}