﻿using System;
using WaterNet.WV_OperationManage.dao;
using WaterNet.WV_Common.work;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.dao;
using WaterNet.WV_LeakageManage.dao;
using System.Windows.Forms;
using WaterNet.WV_LeakageManage.vo;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_OperationManage.work
{
    public class OperationManageWork : BaseWork
    {
        private static OperationManageWork work = null;
        private OperationManageDao dao = null;

        public static OperationManageWork GetInstance()
        {
            if (work == null)
            {
                work = new OperationManageWork();
            }
            return work;
        }

        private OperationManageWork()
        {
            this.dao = OperationManageDao.GetInstance();
        }

        /// <summary>
        /// 집중관리대상구역조회(블록별 우선순위목록)
        /// 1. 블록 목록 조회
        /// 2. 블록별 감시인자 산정
        /// 3. 감시인자 기준 우선순위 선정
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public DataTable SelectLeakageRepairRanking(Hashtable parameter)
        {
            string dateTime = string.Empty;

            //결과테이블생성,
            DataTable rankingList = new DataTable();
            rankingList.Columns.Add("LOC_CODE", typeof(string));
            rankingList.Columns.Add("LBLOCK", typeof(string));
            rankingList.Columns.Add("MBLOCK", typeof(string));
            rankingList.Columns.Add("SBLOCK", typeof(string));
            rankingList.Columns.Add("RANK", typeof(int));
            rankingList.Columns.Add("REVENUE_RATIO", typeof(double));
            rankingList.Columns.Add("NON_REVENUE_GUPSUJUNSU", typeof(double));
            rankingList.Columns.Add("NON_REVENUE_PIPELEN", typeof(double));
            rankingList.Columns.Add("GUPSUJUN_LEAKS_DAY", typeof(double));
            rankingList.Columns.Add("PIPE_LENEN_LEAKS_DAY", typeof(double));
            rankingList.Columns.Add("EQUIP_BURST", typeof(double));
            rankingList.Columns.Add("GUPSUJUN_LEAKS_NT", typeof(double));
            rankingList.Columns.Add("PIPE_LENEN_LEAKS_NT", typeof(double));

            //유수율 분석 월이 안된다면 빈값을 돌려준다.
            if(!Utils.CanRevenueUpdate(parameter["YEAR_MON"].ToString(), "yyyyMM", parameter["LOC_CODE"].ToString()))
            {
                MessageBox.Show("조회 년월은 유수수량이 확정전입니다.\n조회 년월을 다시 설정해 주세요.");
                return rankingList;
            }

            try
            {
                ConnectionOpen();
                BeginTransaction();

                //모든블록목록
                DataTable blockList = BlockDao.GetInstance().SelectBlockListAll(base.DataBaseManager, "RESULT").Tables[0];
                if (blockList == null && blockList.Rows.Count == 0)
                {
                    return null;
                }

                //검색 블록 선정
                for (int i = blockList.Rows.Count-1; i >= 0; i--)
                {
                    DataRow row = blockList.Rows[i];

                    if (parameter["STANDARD"].ToString() == "MIDDLE")
                    {
                        if (row["KT_GBN"].ToString() != "001")
                        {
                            blockList.Rows.Remove(row);
                        }
                    }

                    if (parameter["STANDARD"].ToString() == "SMALL")
                    {
                        if(row["FTR_CODE"].ToString() != "BZ003")
                        {
                            blockList.Rows.Remove(row);
                        }
                    }
                }

                //블록설정
                foreach (DataRow dataRow in blockList.Rows)
                {
                    DataRow newRow = rankingList.NewRow();
                    newRow["LOC_CODE"] = dataRow["LOC_CODE"];
                    newRow["LBLOCK"] = dataRow["LBLOCK"];
                    newRow["MBLOCK"] = dataRow["MBLOCK"];
                    newRow["SBLOCK"] = dataRow["SBLOCK"];
                    rankingList.Rows.Add(newRow);
                }

                //블록별 감시인자 산정(보여줄떄는  중블록별/소블록별 보여줘야한다.)
                foreach (DataRow row in blockList.Rows)
                {
                    //대블록을 제외하고 중블록, 소블록인경우만 산정함
                    if (row["FTR_CODE"].ToString() != "BZ001")
                    {
                        //감시인자를 산정하기위한 파라미터 설정
                        if (parameter.ContainsKey("LOC_CODE"))
                        {
                            parameter.Remove("LOC_CODE");
                        }
                        parameter.Add("LOC_CODE", row["LOC_CODE"]);

                        //감시인자를 산정하고 결과 테이블에 담는다.
                        DataSet dataSet = new DataSet();

                        LeakageManageDao.GetInstance().InsertLeakageDefaultOption(DataBaseManager, parameter);
                        LeakageManageDao.GetInstance().SelectLeakageCalculation(base.DataBaseManager, dataSet, "RESULT", parameter);

                        foreach (DataRow leakageRow in dataSet.Tables["RESULT"].Rows)
                        {
                            LeakageCalculationVO leakageObj = new LeakageCalculationVO();

                            foreach (DataColumn column in dataSet.Tables["RESULT"].Columns)
                            {
                                if (leakageRow[column.ColumnName] == DBNull.Value && column.ColumnName != "SBLOCK")
                                {
                                    leakageRow[column.ColumnName] = 0;
                                }
                            }

                            leakageObj.LOC_CODE = Convert.ToString(leakageRow["LOC_CODE"]);
                            leakageObj.LBLOCK = Convert.ToString(leakageRow["LBLOCK"]);
                            leakageObj.MBLOCK = Convert.ToString(leakageRow["MBLOCK"]);
                            leakageObj.SBLOCK = Convert.ToString(leakageRow["SBLOCK"]);
                            leakageObj.DATEE = Convert.ToDateTime(leakageRow["DATEE"]);
                            leakageObj.M_AVERAGE = Convert.ToDouble(leakageRow["M_AVERAGE"]);
                            leakageObj.FILTERING = Convert.ToDouble(leakageRow["FILTERING"]);
                            leakageObj.PIP_LEN = Convert.ToDouble(leakageRow["PIP_LEN"]);
                            leakageObj.GUPSUJUNSU = Convert.ToInt32(leakageRow["GUPSUJUNSU"]);
                            leakageObj.GAJUNG_GAGUSU = Convert.ToInt32(leakageRow["GAJUNG_GAGUSU"]);
                            leakageObj.BGAJUNG_GAGUSU = Convert.ToInt32(leakageRow["BGAJUNG_GAGUSU"]);
                            leakageObj.SPECIAL_AMTUSE = Convert.ToDouble(leakageRow["SPECIAL_AMTUSE"]);
                            leakageObj.INFPNT_HPRES_DAVG = Convert.ToDouble(leakageRow["INFPNT_HPRES_DAVG"]);
                            leakageObj.MVAVG_MINTIME = Convert.ToString(leakageRow["MVAVG_MINTIME"]);
                            leakageObj.HH00_HPRES_IN = Convert.ToDouble(leakageRow["HH00_HPRES_IN"]);
                            leakageObj.HH01_HPRES_IN = Convert.ToDouble(leakageRow["HH01_HPRES_IN"]);
                            leakageObj.HH02_HPRES_IN = Convert.ToDouble(leakageRow["HH02_HPRES_IN"]);
                            leakageObj.HH03_HPRES_IN = Convert.ToDouble(leakageRow["HH03_HPRES_IN"]);
                            leakageObj.HH04_HPRES_IN = Convert.ToDouble(leakageRow["HH04_HPRES_IN"]);
                            leakageObj.HH05_HPRES_IN = Convert.ToDouble(leakageRow["HH05_HPRES_IN"]);
                            leakageObj.HH06_HPRES_IN = Convert.ToDouble(leakageRow["HH06_HPRES_IN"]);
                            leakageObj.HH07_HPRES_IN = Convert.ToDouble(leakageRow["HH07_HPRES_IN"]);
                            leakageObj.HH08_HPRES_IN = Convert.ToDouble(leakageRow["HH08_HPRES_IN"]);
                            leakageObj.HH09_HPRES_IN = Convert.ToDouble(leakageRow["HH09_HPRES_IN"]);
                            leakageObj.HH10_HPRES_IN = Convert.ToDouble(leakageRow["HH10_HPRES_IN"]);
                            leakageObj.HH11_HPRES_IN = Convert.ToDouble(leakageRow["HH11_HPRES_IN"]);
                            leakageObj.HH12_HPRES_IN = Convert.ToDouble(leakageRow["HH12_HPRES_IN"]);
                            leakageObj.HH13_HPRES_IN = Convert.ToDouble(leakageRow["HH13_HPRES_IN"]);
                            leakageObj.HH14_HPRES_IN = Convert.ToDouble(leakageRow["HH14_HPRES_IN"]);
                            leakageObj.HH15_HPRES_IN = Convert.ToDouble(leakageRow["HH15_HPRES_IN"]);
                            leakageObj.HH16_HPRES_IN = Convert.ToDouble(leakageRow["HH16_HPRES_IN"]);
                            leakageObj.HH17_HPRES_IN = Convert.ToDouble(leakageRow["HH17_HPRES_IN"]);
                            leakageObj.HH18_HPRES_IN = Convert.ToDouble(leakageRow["HH18_HPRES_IN"]);
                            leakageObj.HH19_HPRES_IN = Convert.ToDouble(leakageRow["HH19_HPRES_IN"]);
                            leakageObj.HH20_HPRES_IN = Convert.ToDouble(leakageRow["HH20_HPRES_IN"]);
                            leakageObj.HH21_HPRES_IN = Convert.ToDouble(leakageRow["HH21_HPRES_IN"]);
                            leakageObj.HH22_HPRES_IN = Convert.ToDouble(leakageRow["HH22_HPRES_IN"]);
                            leakageObj.HH23_HPRES_IN = Convert.ToDouble(leakageRow["HH23_HPRES_IN"]);
                            leakageObj.AVG_HPRES_PAVG = Convert.ToDouble(leakageRow["AVG_HPRES_PAVG"]);
                            leakageObj.HH00_HPRES_MID = Convert.ToDouble(leakageRow["HH00_HPRES_MID"]);
                            leakageObj.HH01_HPRES_MID = Convert.ToDouble(leakageRow["HH01_HPRES_MID"]);
                            leakageObj.HH02_HPRES_MID = Convert.ToDouble(leakageRow["HH02_HPRES_MID"]);
                            leakageObj.HH03_HPRES_MID = Convert.ToDouble(leakageRow["HH03_HPRES_MID"]);
                            leakageObj.HH04_HPRES_MID = Convert.ToDouble(leakageRow["HH04_HPRES_MID"]);
                            leakageObj.HH05_HPRES_MID = Convert.ToDouble(leakageRow["HH05_HPRES_MID"]);
                            leakageObj.HH06_HPRES_MID = Convert.ToDouble(leakageRow["HH06_HPRES_MID"]);
                            leakageObj.HH07_HPRES_MID = Convert.ToDouble(leakageRow["HH07_HPRES_MID"]);
                            leakageObj.HH08_HPRES_MID = Convert.ToDouble(leakageRow["HH08_HPRES_MID"]);
                            leakageObj.HH09_HPRES_MID = Convert.ToDouble(leakageRow["HH09_HPRES_MID"]);
                            leakageObj.HH10_HPRES_MID = Convert.ToDouble(leakageRow["HH10_HPRES_MID"]);
                            leakageObj.HH11_HPRES_MID = Convert.ToDouble(leakageRow["HH11_HPRES_MID"]);
                            leakageObj.HH12_HPRES_MID = Convert.ToDouble(leakageRow["HH12_HPRES_MID"]);
                            leakageObj.HH13_HPRES_MID = Convert.ToDouble(leakageRow["HH13_HPRES_MID"]);
                            leakageObj.HH14_HPRES_MID = Convert.ToDouble(leakageRow["HH14_HPRES_MID"]);
                            leakageObj.HH15_HPRES_MID = Convert.ToDouble(leakageRow["HH15_HPRES_MID"]);
                            leakageObj.HH16_HPRES_MID = Convert.ToDouble(leakageRow["HH16_HPRES_MID"]);
                            leakageObj.HH17_HPRES_MID = Convert.ToDouble(leakageRow["HH17_HPRES_MID"]);
                            leakageObj.HH18_HPRES_MID = Convert.ToDouble(leakageRow["HH18_HPRES_MID"]);
                            leakageObj.HH19_HPRES_MID = Convert.ToDouble(leakageRow["HH19_HPRES_MID"]);
                            leakageObj.HH20_HPRES_MID = Convert.ToDouble(leakageRow["HH20_HPRES_MID"]);
                            leakageObj.HH21_HPRES_MID = Convert.ToDouble(leakageRow["HH21_HPRES_MID"]);
                            leakageObj.HH22_HPRES_MID = Convert.ToDouble(leakageRow["HH22_HPRES_MID"]);
                            leakageObj.HH23_HPRES_MID = Convert.ToDouble(leakageRow["HH23_HPRES_MID"]);

                            leakageObj.AddLeakageOptionVO(
                                Convert.ToDouble(leakageRow["JUMAL_USED"]),
                                Convert.ToDouble(leakageRow["GAJUNG_USED"]),
                                Convert.ToDouble(leakageRow["BGAJUNG_USED"]),
                                Convert.ToInt32(leakageRow["SPECIAL_USED_PERCENT"]),
                                Convert.ToDouble(leakageRow["SPECIAL_USED"]),
                                Convert.ToDouble(leakageRow["N1"]),
                                Convert.ToDouble(leakageRow["LC"]),
                                Convert.ToDouble(leakageRow["ICF"])
                            );

                            foreach (DataRow rankRow in rankingList.Rows)
                            {
                                if (rankRow["LOC_CODE"].ToString() == leakageObj.LOC_CODE)
                                {
                                    rankRow["RANK"] = 0;
                                    rankRow["REVENUE_RATIO"] = 0;
                                    rankRow["NON_REVENUE_GUPSUJUNSU"] = 0;
                                    rankRow["NON_REVENUE_PIPELEN"] = 0;
                                    rankRow["GUPSUJUN_LEAKS_DAY"] = Utils.ToDouble(rankRow["GUPSUJUN_LEAKS_DAY"]) + leakageObj.GUPSUJUN_LEAKS_DAY;
                                    rankRow["PIPE_LENEN_LEAKS_DAY"] = Utils.ToDouble(rankRow["PIPE_LENEN_LEAKS_DAY"]) + leakageObj.PIPE_LENEN_LEAKS_DAY;
                                    rankRow["EQUIP_BURST"] = Utils.ToDouble(rankRow["EQUIP_BURST"]) + leakageObj.EQUIP_BURST;
                                    rankRow["GUPSUJUN_LEAKS_NT"] = Utils.ToDouble(rankRow["GUPSUJUN_LEAKS_NT"]) + leakageObj.GUPSUJUN_LEAKS_NT;
                                    rankRow["PIPE_LENEN_LEAKS_NT"] = Utils.ToDouble(rankRow["PIPE_LENEN_LEAKS_NT"]) + leakageObj.PIPE_LENEN_LEAKS_NT;
                                }
                            }

                            //전월유수율, 무수수량/급수전, 무수수량/배수관연장을 조회하고 결과에 담는다.
                            OperationManageDao.GetInstance().SelectNonRevenue(base.DataBaseManager, dataSet, "RATIO", parameter);

                            foreach (DataRow ratioRow in dataSet.Tables["RATIO"].Rows)
                            {
                                foreach (DataRow rankRow in rankingList.Rows)
                                {
                                    if (rankRow["LOC_CODE"].ToString() == ratioRow["LOC_CODE"].ToString())
                                    {
                                        rankRow["REVENUE_RATIO"] = Utils.ToDouble(ratioRow["REVENUE_RATIO"]);
                                        rankRow["NON_REVENUE_GUPSUJUNSU"] = Utils.ToDouble(ratioRow["NON_REVENUE_GUPSUJUNSU"]);
                                        rankRow["NON_REVENUE_PIPELEN"] = Utils.ToDouble(ratioRow["NON_REVENUE_PIPELEN"]);
                                    }
                                }
                            }
                        }
                    }
                }

                TimeSpan span = DateTime.ParseExact(parameter["ENDDATE"].ToString(), "yyyyMMdd", null)
                    - DateTime.ParseExact(parameter["STARTDATE"].ToString(), "yyyyMMdd", null);

                foreach (DataRow rankRow in rankingList.Rows)
                {
                    rankRow["GUPSUJUN_LEAKS_DAY"] = rankRow["GUPSUJUN_LEAKS_DAY"] == DBNull.Value ? 0 : Convert.ToDouble(rankRow["GUPSUJUN_LEAKS_DAY"]) / (span.Days + 1);
                    rankRow["PIPE_LENEN_LEAKS_DAY"] = rankRow["PIPE_LENEN_LEAKS_DAY"] == DBNull.Value ? 0 : Convert.ToDouble(rankRow["PIPE_LENEN_LEAKS_DAY"]) / (span.Days + 1);
                    rankRow["EQUIP_BURST"] = rankRow["EQUIP_BURST"] == DBNull.Value ? 0 : Convert.ToDouble(rankRow["EQUIP_BURST"]) / (span.Days + 1);
                    rankRow["GUPSUJUN_LEAKS_NT"] = rankRow["GUPSUJUN_LEAKS_NT"] == DBNull.Value ? 0 : Convert.ToDouble(rankRow["GUPSUJUN_LEAKS_NT"]) / (span.Days + 1);
                    rankRow["PIPE_LENEN_LEAKS_NT"] = rankRow["PIPE_LENEN_LEAKS_NT"] == DBNull.Value ? 0 : Convert.ToDouble(rankRow["PIPE_LENEN_LEAKS_NT"]) / (span.Days + 1);
                    rankRow["NON_REVENUE_GUPSUJUNSU"] = rankRow["NON_REVENUE_GUPSUJUNSU"] == DBNull.Value ? 0 : Convert.ToDouble(rankRow["NON_REVENUE_GUPSUJUNSU"]) / (span.Days + 1);
                    rankRow["NON_REVENUE_PIPELEN"] = rankRow["NON_REVENUE_PIPELEN"] == DBNull.Value ? 0 : Convert.ToDouble(rankRow["NON_REVENUE_PIPELEN"]) / (span.Days + 1);
                }

                //테스트추가
                //this.Add_TEST(rankingList);

                //블록별 감시 우선순위산정를 산정한다.
                //블럭별 컴럼별 순위를 정한다.
                //전체 순위합산으로 블록의 점수를 정한다.
                //1,2,3 등급으로 나눠서 우선순위를 산정한다.

                //순위를 산정하기위한 변수선언, (8개 항목 비교)
                //유수율을 제외하고 낮은값이 좋다.
                DataTable tempList = rankingList.Copy();
                double weightSum = 0;

                weightSum += Utils.ToDouble(parameter["REVENUE_RATIO"]);
                weightSum += Utils.ToDouble(parameter["NON_REVENUE_GUPSUJUNSU"]);
                weightSum += Utils.ToDouble(parameter["NON_REVENUE_PIPELEN"]);
                weightSum += Utils.ToDouble(parameter["GUPSUJUN_LEAKS_DAY"]);
                weightSum += Utils.ToDouble(parameter["PIPE_LENEN_LEAKS_DAY"]);
                weightSum += Utils.ToDouble(parameter["EQUIP_BURST"]);
                weightSum += Utils.ToDouble(parameter["GUPSUJUN_LEAKS_NT"]);
                weightSum += Utils.ToDouble(parameter["PIPE_LENEN_LEAKS_NT"]);

                foreach (DataColumn column in tempList.Columns)
                {
                    if (column.DataType == typeof(double))
                    {
                        foreach (DataRow row in tempList.Rows)
                        {
                            //첫 랭킹은 무조건 1로 한다.
                            double rank = 1.0;
                            Console.WriteLine(string.Format("{0},{1}", row["SBLOCK"], column.ColumnName));
                            double realValue = Utils.ToDouble(rankingList.Rows[tempList.Rows.IndexOf(row)][column.ColumnName]);
                            foreach (DataRow value in rankingList.Rows)
                            {
                                //현재가 아닌것과의 값만 비교하면 된다.
                                if (rankingList.Rows.IndexOf(value) != tempList.Rows.IndexOf(row))
                                {
                                    //유수율은 오름차순 순위, 나머지는 내림차순순위
                                    if (column.ColumnName == "REVENUE_RATIO")
                                    {
                                        if (Double.IsInfinity(realValue) || Double.IsNaN(realValue))
                                        {
                                            break;
                                        }

                                        //현재보다 큰게 있으면 나의 랭킹은 올라간다.
                                        //내가 값이상이면 무조건 랭킹을 올린다.
                                        else if (realValue > Utils.ToDouble(value[column.ColumnName]))
                                        {
                                            rank++;
                                        }
                                    }
                                    else
                                    {
                                        if (Double.IsInfinity(realValue) || Double.IsNaN(realValue))
                                        {
                                            break;
                                        }

                                        //나보다 작은게 있으면 나의 랭킹은 올라간다.
                                        //내가 값이상이면 무조건 랭킹을 올린다.
                                        else if (realValue < Utils.ToDouble(value[column.ColumnName]))
                                        {
                                            rank++;
                                        }
                                    }
                                }
                            }

                            row["RANK"] = Utils.ToDouble(row["RANK"]) + (rank * (Utils.ToDouble( parameter[column.ColumnName]) /weightSum));
                        }
                    }
                }

                foreach (DataRow row in rankingList.Rows)
                {
                    int rank = 1;
                    foreach (DataRow temp in tempList.Rows)
                    {
                        if (Utils.ToDouble(tempList.Rows[rankingList.Rows.IndexOf(row)]["RANK"]) > Utils.ToDouble(temp["RANK"]))
                        {
                            rank++;
                        }
                    }

                    row["RANK"] = rank;
                }

                //double[][] rank = new double[7][];
                //int[][] temp = new int[7][];

                //for (int i = 0; i < rank.Length; i++)
                //{
                //    rank[i] = new double[rankingList.Rows.Count];
                //    temp[i] = new int[rankingList.Rows.Count];
                //}

                //for (int i = 0; i < rankingList.Rows.Count; i++)
                //{
                //    rank[0][i] = Utils.ToDouble(rankingList.Rows[i]["NON_REVENUE_GUPSUJUNSU"]);
                //    rank[1][i] = Utils.ToDouble(rankingList.Rows[i]["NON_REVENUE_PIPELEN"]);
                //    rank[2][i] = Utils.ToDouble(rankingList.Rows[i]["GUPSUJUN_LEAKS_DAY"]);
                //    rank[3][i] = Utils.ToDouble(rankingList.Rows[i]["PIPE_LENEN_LEAKS_DAY"]);
                //    rank[4][i] = Utils.ToDouble(rankingList.Rows[i]["EQUIP_BURST"]);
                //    rank[5][i] = Utils.ToDouble(rankingList.Rows[i]["GUPSUJUN_LEAKS_NT"]);
                //    rank[6][i] = Utils.ToDouble(rankingList.Rows[i]["PIPE_LENEN_LEAKS_NT"]);

                //    temp[0][i] = 1;
                //    temp[1][i] = 1;
                //    temp[2][i] = 1;
                //    temp[3][i] = 1;
                //    temp[4][i] = 1;
                //    temp[5][i] = 1;
                //    temp[6][i] = 1;
                //}

                //for (int i = 0; i < rank.Length; i++)
                //{
                //    for (int j = 0; j < rank[i].Length; j++)
                //    {
                //        for (int k = 0; k < rank[i].Length; k++)
                //        {
                //            if (rank[i][j] < rank[i][k])
                //            {
                //                temp[i][j]++;
                //            }
                //        }
                //    }
                //}

                //int[] ranksum = new int[rankingList.Rows.Count];

                //for (int i = 0; i < temp[0].Length; i++)
                //{
                //    ranksum[i] += temp[0][i];
                //    ranksum[i] += temp[1][i];
                //    ranksum[i] += temp[2][i];
                //    ranksum[i] += temp[3][i];
                //    ranksum[i] += temp[4][i];
                //    ranksum[i] += temp[5][i];
                //    ranksum[i] += temp[6][i];
                //}

                //for (int i = 0; i < ranksum.Length; i++)
                //{
                //    temp[0][i] = 1;

                //    for (int j = 0; j < ranksum.Length; j++)
                //    {
                //        if (ranksum[i] > ranksum[j])
                //        {

                //            temp[0][i]++;
                //        }
                //    }
                //}

                //for (int i = 0; i < temp[0].Length; i++)
                //{
                //    rankingList.Rows[i]["RANK"] = temp[0][i];
                //}

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }

            return rankingList;
        }

        public void Add_TEST(DataTable table)
        {
            table.Rows.Clear();

            DataRow newrow = null;

            newrow = table.NewRow();
            newrow["LOC_CODE"] = "00";
            newrow["LBLOCK"] = "정읍시";
            newrow["MBLOCK"] = "마곡계통";
            newrow["SBLOCK"] = "테스트02";
            newrow["RANK"] = 0;
            newrow["REVENUE_RATIO"] = 98;
            newrow["NON_REVENUE_GUPSUJUNSU"] = 4.2;
            newrow["NON_REVENUE_PIPELEN"] = 0.42;
            newrow["GUPSUJUN_LEAKS_DAY"] = 4.2;
            newrow["PIPE_LENEN_LEAKS_DAY"] = 4.2;
            newrow["EQUIP_BURST"] = 8.2;
            newrow["GUPSUJUN_LEAKS_NT"] = 0.22;
            newrow["PIPE_LENEN_LEAKS_NT"] = 1.2;
            table.Rows.Add(newrow);

            newrow = table.NewRow();
            newrow["LOC_CODE"] = "00";
            newrow["LBLOCK"] = "정읍시";
            newrow["MBLOCK"] = "마곡계통";
            newrow["SBLOCK"] = "테스트03";
            newrow["RANK"] = 0;
            newrow["REVENUE_RATIO"] = 97;
            newrow["NON_REVENUE_GUPSUJUNSU"] = 4.3;
            newrow["NON_REVENUE_PIPELEN"] = 0.43;
            newrow["GUPSUJUN_LEAKS_DAY"] = 4.3;
            newrow["PIPE_LENEN_LEAKS_DAY"] = 4.3;
            newrow["EQUIP_BURST"] = 8.3;
            newrow["GUPSUJUN_LEAKS_NT"] = 0.23;
            newrow["PIPE_LENEN_LEAKS_NT"] = 1.5;
            table.Rows.Add(newrow);

            newrow = table.NewRow();
            newrow["LOC_CODE"] = "00";
            newrow["LBLOCK"] = "정읍시";
            newrow["MBLOCK"] = "마곡계통";
            newrow["SBLOCK"] = "테스트04";
            newrow["RANK"] = 0;
            newrow["REVENUE_RATIO"] = 95;
            newrow["NON_REVENUE_GUPSUJUNSU"] = 4.5;
            newrow["NON_REVENUE_PIPELEN"] = 0.45;
            newrow["GUPSUJUN_LEAKS_DAY"] = 4.5;
            newrow["PIPE_LENEN_LEAKS_DAY"] = 4.5;
            newrow["EQUIP_BURST"] = 8.5;
            newrow["GUPSUJUN_LEAKS_NT"] = 0.25;
            newrow["PIPE_LENEN_LEAKS_NT"] = 1.9;
            table.Rows.Add(newrow);

            newrow = table.NewRow();
            newrow["LOC_CODE"] = "00";
            newrow["LBLOCK"] = "정읍시";
            newrow["MBLOCK"] = "마곡계통";
            newrow["SBLOCK"] = "테스트05";
            newrow["RANK"] = 0;
            newrow["REVENUE_RATIO"] = 91;
            newrow["NON_REVENUE_GUPSUJUNSU"] = 4.1;
            newrow["NON_REVENUE_PIPELEN"] = 0.41;
            newrow["GUPSUJUN_LEAKS_DAY"] = 4.1;
            newrow["PIPE_LENEN_LEAKS_DAY"] = 4.1;
            newrow["EQUIP_BURST"] = 8.1;
            newrow["GUPSUJUN_LEAKS_NT"] = 0.21;
            newrow["PIPE_LENEN_LEAKS_NT"] = 1.5;
            table.Rows.Add(newrow);

            newrow = table.NewRow();
            newrow["LOC_CODE"] = "00";
            newrow["LBLOCK"] = "정읍시";
            newrow["MBLOCK"] = "마곡계통";
            newrow["SBLOCK"] = "테스트06";
            newrow["RANK"] = 0;
            newrow["REVENUE_RATIO"] = 99;
            newrow["NON_REVENUE_GUPSUJUNSU"] = 4.1;
            newrow["NON_REVENUE_PIPELEN"] = 0.42;
            newrow["GUPSUJUN_LEAKS_DAY"] = 4.1;
            newrow["PIPE_LENEN_LEAKS_DAY"] = 4.5;
            newrow["EQUIP_BURST"] = 9.1;
            newrow["GUPSUJUN_LEAKS_NT"] = 0.21;
            newrow["PIPE_LENEN_LEAKS_NT"] = 2.5;
            table.Rows.Add(newrow);
        }
    }
}
