﻿using System.Text;
using WaterNet.WV_Common.dao;
using WaterNet.WaterNetCore;
using System.Data;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_OperationManage.dao
{
    public class OperationManageDao : BaseDao
    {
        private static OperationManageDao dao = null;

        private OperationManageDao() { }

        public static OperationManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new OperationManageDao();
            }
            return dao;
        }

        //모든 블록을 조회한다.
        public void GetLocationList(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), null);
        }

        //유수율, 급수전무수수량, 배수관연장무수수량
        public void SelectNonRevenue(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select wrr.loc_code                                                                                             ");
            query.AppendLine("      ,round(to_number((wrr.revenue/decode(wrr.water_supplied,0,null,wrr.water_supplied))*100),2) revenue_ratio                                   ");
            query.AppendLine("      ,round(to_number((wrr.water_supplied-wrr.revenue)/wcd.gupsujunsu),2) non_revenue_gupsujunsu               ");
            query.AppendLine("      ,round(to_number((wrr.water_supplied-wrr.revenue)/wpl.pip_len),2) non_revenue_pipelen                     ");
            query.AppendLine("  from wv_revenue_ratio wrr                                                                                     ");
            query.AppendLine("      ,(                                                                                                        ");
            query.AppendLine("       select gupsujunsu gupsujunsu                                                                             ");
            query.AppendLine("         from wv_consumer_day                                                                                   ");
            query.AppendLine("        where 1 = 1                                                                                             ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                              ");
            query.AppendLine("          and datee = :ENDDATE                                                                                  ");
            query.AppendLine("      ) wcd                                                                                                     ");
            query.AppendLine("      ,(                                                                                                        ");
            query.AppendLine("       select pip_len/1000 pip_len                                                                              ");
            query.AppendLine("         from wv_pipe_lm_day                                                                                    ");
            query.AppendLine("        where 1 = 1                                                                                             ");
            query.AppendLine("          and loc_code = :LOC_CODE                                                                              ");
            query.AppendLine("          and datee = :ENDDATE                                                                                  ");
            query.AppendLine("          and saa_cde = 'SAA004'                                                                                ");
            query.AppendLine("      ) wpl                                                                                                     ");
            query.AppendLine(" where 1 = 1                                                                                                    ");
            query.AppendLine("   and wrr.loc_code = :LOC_CODE                                                                                 ");
            query.AppendLine("   and wrr.year_mon = :YEAR_MON                                                                                 ");

            IDataParameter[] parameters =  {
                    new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("YEAR_MON", OracleDbType.Varchar2)
            };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["ENDDATE"];
            parameters[2].Value = parameter["LOC_CODE"];
            parameters[3].Value = parameter["ENDDATE"];
            parameters[4].Value = parameter["LOC_CODE"];
            parameters[5].Value = parameter["YEAR_MON"];
            
            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }
    }
}
