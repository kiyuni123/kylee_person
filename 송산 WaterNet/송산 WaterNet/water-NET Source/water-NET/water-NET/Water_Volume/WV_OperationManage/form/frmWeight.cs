﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace WaterNet.WV_OperationManage.form
{
    public partial class frmWeight : Form
    {
        private HeaderBase weightItem = null;

        public frmWeight()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmWeight_Load);
        }

        private void frmWeight_Load(object sender, EventArgs e)
        {
            this.textBox1.Text = this.weightItem.Group.Header.Caption;
            this.button1.Click += new EventHandler(button1_Click);
            this.textBox1.KeyDown += new KeyEventHandler(frmWeight_KeyDown);
        }

        private void frmWeight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                double result = 0;
                if (Double.TryParse(this.textBox1.Text, out result))
                {
                    this.weightItem.Group.Header.Caption = this.textBox1.Text;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("숫자를 입력해야 합니다.");
                }
            }
        }

        public frmWeight(HeaderBase weightItem)
        {
            InitializeComponent();
            this.Load += new EventHandler(frmWeight_Load);
            this.weightItem = weightItem;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double result = 0;
            if (Double.TryParse(this.textBox1.Text, out result))
            {
                this.weightItem.Group.Header.Caption = this.textBox1.Text;
                this.Close();
            }
            else
            {
                MessageBox.Show("숫자를 입력해야 합니다.");
            }
        }
    }
}
