﻿namespace WaterNet.WV_OperationManage.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RANK");
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("REVENUE_RATIO");
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NON_REVENUE_GUPSUJUNSU");
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NON_REVENUE_PIPELEN");
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn22 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GUPSUJUN_LEAKS_DAY");
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn23 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIPE_LENEN_LEAKS_DAY");
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn24 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("EQUIP_BURST");
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn25 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GUPSUJUN_LEAKS_NT");
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn26 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIPE_LENEN_LEAKS_NT");
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 19284204);
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 19284205);
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup2", 19284206);
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup3", 19299493);
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("REVENUE_RATIO", 20657622);
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NON_REVENUE_GUPSUJUNSU", 20680803);
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup7 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NON_REVENUE_PIPELEN", 20718290);
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup8 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("GUPSUJUN_LEAKS_DAY", 20726668);
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup9 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("PIPE_LENEN_LEAKS_DAY", 20733017);
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup10 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("EQUIP_BURST", 20754717);
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup11 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("GUPSUJUN_LEAKS_NT", 20781003);
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup12 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("PIPE_LENEN_LEAKS_NT", 20808397);
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.excelBtn = new System.Windows.Forms.Button();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.smallCheck = new System.Windows.Forms.RadioButton();
            this.middleCheck = new System.Windows.Forms.RadioButton();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(10, 658);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1192, 10);
            this.pictureBox4.TabIndex = 45;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(1202, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 658);
            this.pictureBox3.TabIndex = 44;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Location = new System.Drawing.Point(0, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 658);
            this.pictureBox2.TabIndex = 43;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1212, 10);
            this.pictureBox1.TabIndex = 42;
            this.pictureBox1.TabStop = false;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(1106, 3);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 14;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // ultraGrid1
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance1;
            ultraGridColumn10.Header.Caption = "지역관리코드";
            ultraGridColumn10.Header.VisiblePosition = 0;
            ultraGridColumn10.Hidden = true;
            ultraGridColumn11.Header.Caption = "대블록";
            ultraGridColumn11.Header.VisiblePosition = 1;
            ultraGridColumn11.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn11.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn11.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(64, 0);
            ultraGridColumn11.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn11.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn11.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn12.Header.Caption = "중블록";
            ultraGridColumn12.Header.VisiblePosition = 2;
            ultraGridColumn12.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn12.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn12.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(62, 0);
            ultraGridColumn12.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn12.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn12.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn17.Header.Caption = "소블록";
            ultraGridColumn17.Header.VisiblePosition = 3;
            ultraGridColumn17.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn17.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn17.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(68, 0);
            ultraGridColumn17.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn17.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn17.RowLayoutColumnInfo.SpanY = 3;
            appearance2.TextHAlignAsString = "Right";
            ultraGridColumn18.CellAppearance = appearance2;
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            ultraGridColumn18.Header.Appearance = appearance3;
            ultraGridColumn18.Header.Caption = "종 합\r\n우선순위";
            ultraGridColumn18.Header.VisiblePosition = 12;
            ultraGridColumn18.NullText = "0";
            ultraGridColumn18.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn18.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn18.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(61, 0);
            ultraGridColumn18.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 64);
            ultraGridColumn18.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn18.RowLayoutColumnInfo.SpanY = 3;
            appearance4.TextHAlignAsString = "Right";
            ultraGridColumn19.CellAppearance = appearance4;
            ultraGridColumn19.Format = "###,##0.00";
            ultraGridColumn19.Header.Caption = "유수율(%)";
            ultraGridColumn19.Header.TextOrientation = new Infragistics.Win.TextOrientationInfo(0, Infragistics.Win.TextFlowDirection.Horizontal);
            ultraGridColumn19.Header.VisiblePosition = 4;
            ultraGridColumn19.NullText = "0";
            ultraGridColumn19.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn19.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn19.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn19.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(69, 0);
            ultraGridColumn19.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 26);
            ultraGridColumn19.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn19.RowLayoutColumnInfo.SpanY = 2;
            appearance5.TextHAlignAsString = "Right";
            ultraGridColumn20.CellAppearance = appearance5;
            ultraGridColumn20.Format = "###,##0.00";
            ultraGridColumn20.Header.Caption = "무수수량/급수전\r\n(㎥/일/전)";
            ultraGridColumn20.Header.VisiblePosition = 5;
            ultraGridColumn20.NullText = "0";
            ultraGridColumn20.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn20.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn20.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn20.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(105, 0);
            ultraGridColumn20.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn20.RowLayoutColumnInfo.SpanY = 2;
            appearance6.TextHAlignAsString = "Right";
            ultraGridColumn21.CellAppearance = appearance6;
            ultraGridColumn21.Format = "###,##0.00";
            ultraGridColumn21.Header.Caption = "무수수량/배수관연장\r\n(㎥/일/km)";
            ultraGridColumn21.Header.VisiblePosition = 6;
            ultraGridColumn21.NullText = "0";
            ultraGridColumn21.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn21.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn21.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn21.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn21.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(131, 0);
            ultraGridColumn21.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn21.RowLayoutColumnInfo.SpanY = 2;
            appearance7.TextHAlignAsString = "Right";
            ultraGridColumn22.CellAppearance = appearance7;
            ultraGridColumn22.Format = "###,##0.00";
            ultraGridColumn22.Header.Caption = "일누수량/급수전\r\n(㎥/일/전)";
            ultraGridColumn22.Header.VisiblePosition = 7;
            ultraGridColumn22.NullText = "0";
            ultraGridColumn22.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn22.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn22.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn22.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn22.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(108, 0);
            ultraGridColumn22.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn22.RowLayoutColumnInfo.SpanY = 2;
            appearance8.TextHAlignAsString = "Right";
            ultraGridColumn23.CellAppearance = appearance8;
            ultraGridColumn23.Format = "###,##0.00";
            ultraGridColumn23.Header.Caption = "일누수량/배수관연장\r\n(㎥/일/km)";
            ultraGridColumn23.Header.VisiblePosition = 8;
            ultraGridColumn23.NullText = "0";
            ultraGridColumn23.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn23.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn23.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn23.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn23.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(133, 0);
            ultraGridColumn23.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn23.RowLayoutColumnInfo.SpanY = 2;
            appearance9.TextHAlignAsString = "Right";
            ultraGridColumn24.CellAppearance = appearance9;
            ultraGridColumn24.Format = "###,##0.00";
            ultraGridColumn24.Header.Caption = "ESPB\r\n(일)";
            ultraGridColumn24.Header.VisiblePosition = 9;
            ultraGridColumn24.NullText = "0";
            ultraGridColumn24.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn24.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn24.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn24.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn24.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(71, 0);
            ultraGridColumn24.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn24.RowLayoutColumnInfo.SpanY = 2;
            appearance10.TextHAlignAsString = "Right";
            ultraGridColumn25.CellAppearance = appearance10;
            ultraGridColumn25.Format = "###,##0.00";
            ultraGridColumn25.Header.Caption = "야간파열누수량/급수전\r\n(㎥/h/전)";
            ultraGridColumn25.Header.VisiblePosition = 10;
            ultraGridColumn25.NullText = "0";
            ultraGridColumn25.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn25.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn25.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn25.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn25.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(143, 0);
            ultraGridColumn25.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn25.RowLayoutColumnInfo.SpanY = 2;
            appearance11.TextHAlignAsString = "Right";
            ultraGridColumn26.CellAppearance = appearance11;
            ultraGridColumn26.Format = "###,##0.00";
            ultraGridColumn26.Header.Caption = "야간파열누수량/배수관연장\r\n(㎥/h/km)";
            ultraGridColumn26.Header.VisiblePosition = 11;
            ultraGridColumn26.NullText = "0";
            ultraGridColumn26.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn26.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn26.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn26.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn26.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(165, 0);
            ultraGridColumn26.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn26.RowLayoutColumnInfo.SpanY = 2;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn12,
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21,
            ultraGridColumn22,
            ultraGridColumn23,
            ultraGridColumn24,
            ultraGridColumn25,
            ultraGridColumn26});
            appearance12.TextHAlignAsString = "Center";
            appearance12.TextVAlignAsString = "Middle";
            ultraGridGroup1.Header.Appearance = appearance12;
            ultraGridGroup1.Header.Caption = "월 NRW 기준";
            ultraGridGroup1.Key = "NewGroup0";
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 8;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 6;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 3;
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            ultraGridGroup2.Header.Appearance = appearance13;
            ultraGridGroup2.Header.Caption = "일누수량 기준";
            ultraGridGroup2.Key = "NewGroup1";
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 14;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 3;
            appearance14.TextHAlignAsString = "Center";
            appearance14.TextVAlignAsString = "Middle";
            ultraGridGroup3.Header.Appearance = appearance14;
            ultraGridGroup3.Header.Caption = "야간 파열누수량 기준";
            ultraGridGroup3.Key = "NewGroup2";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 18;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 6;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 3;
            appearance15.BackColor = System.Drawing.Color.DarkGray;
            appearance15.TextHAlignAsString = "Center";
            appearance15.TextVAlignAsString = "Middle";
            ultraGridGroup4.Header.Appearance = appearance15;
            ultraGridGroup4.Header.Caption = "가중치 설정";
            ultraGridGroup4.Header.TextOrientation = new Infragistics.Win.TextOrientationInfo(0, Infragistics.Win.TextFlowDirection.Horizontal);
            ultraGridGroup4.Header.ToolTipText = "가중치 설정";
            ultraGridGroup4.Key = "NewGroup3";
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 0;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup4.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 9;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 1;
            appearance16.BackColor = System.Drawing.Color.DarkGray;
            appearance16.TextHAlignAsString = "Right";
            appearance16.TextVAlignAsString = "Middle";
            ultraGridGroup5.Header.Appearance = appearance16;
            ultraGridGroup5.Header.Caption = "1";
            ultraGridGroup5.Header.ToolTipText = "항목을 더블클릭 하시면 수정 가능합니다.";
            ultraGridGroup5.Key = "REVENUE_RATIO";
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 9;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup5.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 1;
            appearance17.BackColor = System.Drawing.Color.DarkGray;
            appearance17.TextHAlignAsString = "Right";
            appearance17.TextVAlignAsString = "Middle";
            ultraGridGroup6.Header.Appearance = appearance17;
            ultraGridGroup6.Header.Caption = "1";
            ultraGridGroup6.Header.ToolTipText = "항목을 더블클릭 하시면 수정 가능합니다.";
            ultraGridGroup6.Key = "NON_REVENUE_GUPSUJUNSU";
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 11;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup6.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 1;
            appearance18.BackColor = System.Drawing.Color.DarkGray;
            appearance18.TextHAlignAsString = "Right";
            appearance18.TextVAlignAsString = "Middle";
            ultraGridGroup7.Header.Appearance = appearance18;
            ultraGridGroup7.Header.Caption = "1";
            ultraGridGroup7.Header.ToolTipText = "항목을 더블클릭 하시면 수정 가능합니다.";
            ultraGridGroup7.Key = "NON_REVENUE_PIPELEN";
            ultraGridGroup7.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup7.RowLayoutGroupInfo.OriginX = 13;
            ultraGridGroup7.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup7.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup7.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup7.RowLayoutGroupInfo.SpanY = 1;
            appearance19.BackColor = System.Drawing.Color.DarkGray;
            appearance19.TextHAlignAsString = "Right";
            appearance19.TextVAlignAsString = "Middle";
            ultraGridGroup8.Header.Appearance = appearance19;
            ultraGridGroup8.Header.Caption = "1";
            ultraGridGroup8.Header.ToolTipText = "항목을 더블클릭 하시면 수정 가능합니다.";
            ultraGridGroup8.Key = "GUPSUJUN_LEAKS_DAY";
            ultraGridGroup8.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup8.RowLayoutGroupInfo.OriginX = 15;
            ultraGridGroup8.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup8.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup8.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup8.RowLayoutGroupInfo.SpanY = 1;
            appearance20.BackColor = System.Drawing.Color.DarkGray;
            appearance20.TextHAlignAsString = "Right";
            appearance20.TextVAlignAsString = "Middle";
            ultraGridGroup9.Header.Appearance = appearance20;
            ultraGridGroup9.Header.Caption = "1";
            ultraGridGroup9.Header.ToolTipText = "항목을 더블클릭 하시면 수정 가능합니다.";
            ultraGridGroup9.Key = "PIPE_LENEN_LEAKS_DAY";
            ultraGridGroup9.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup9.RowLayoutGroupInfo.OriginX = 17;
            ultraGridGroup9.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup9.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup9.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup9.RowLayoutGroupInfo.SpanY = 1;
            appearance21.BackColor = System.Drawing.Color.DarkGray;
            appearance21.TextHAlignAsString = "Right";
            appearance21.TextVAlignAsString = "Middle";
            ultraGridGroup10.Header.Appearance = appearance21;
            ultraGridGroup10.Header.Caption = "1";
            ultraGridGroup10.Header.ToolTipText = "항목을 더블클릭 하시면 수정 가능합니다.";
            ultraGridGroup10.Key = "EQUIP_BURST";
            ultraGridGroup10.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup10.RowLayoutGroupInfo.OriginX = 19;
            ultraGridGroup10.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup10.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup10.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup10.RowLayoutGroupInfo.SpanY = 1;
            appearance22.BackColor = System.Drawing.Color.DarkGray;
            appearance22.TextHAlignAsString = "Right";
            appearance22.TextVAlignAsString = "Middle";
            ultraGridGroup11.Header.Appearance = appearance22;
            ultraGridGroup11.Header.Caption = "1";
            ultraGridGroup11.Header.ToolTipText = "항목을 더블클릭 하시면 수정 가능합니다.";
            ultraGridGroup11.Key = "GUPSUJUN_LEAKS_NT";
            ultraGridGroup11.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup11.RowLayoutGroupInfo.OriginX = 21;
            ultraGridGroup11.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup11.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup11.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup11.RowLayoutGroupInfo.SpanY = 1;
            appearance23.BackColor = System.Drawing.Color.DarkGray;
            appearance23.TextHAlignAsString = "Right";
            appearance23.TextVAlignAsString = "Middle";
            ultraGridGroup12.Header.Appearance = appearance23;
            ultraGridGroup12.Header.Caption = "1";
            ultraGridGroup12.Header.ToolTipText = "항목을 더블클릭 하시면 수정 가능합니다.";
            ultraGridGroup12.Key = "PIPE_LENEN_LEAKS_NT";
            ultraGridGroup12.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup12.RowLayoutGroupInfo.OriginX = 23;
            ultraGridGroup12.RowLayoutGroupInfo.OriginY = 3;
            ultraGridGroup12.RowLayoutGroupInfo.PreferredLabelSize = new System.Drawing.Size(0, 24);
            ultraGridGroup12.RowLayoutGroupInfo.SpanX = 1;
            ultraGridGroup12.RowLayoutGroupInfo.SpanY = 1;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6,
            ultraGridGroup7,
            ultraGridGroup8,
            ultraGridGroup9,
            ultraGridGroup10,
            ultraGridGroup11,
            ultraGridGroup12});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance24.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance24;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance25;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance26.BackColor2 = System.Drawing.SystemColors.Control;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance26;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance27;
            appearance28.BackColor = System.Drawing.SystemColors.Highlight;
            appearance28.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance28;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance29;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            appearance30.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance30;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance31.BackColor = System.Drawing.SystemColors.Control;
            appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance31.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance31;
            appearance32.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance32;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance33;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance34;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(10, 151);
            this.ultraGrid1.Margin = new System.Windows.Forms.Padding(0);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(1192, 507);
            this.ultraGrid1.TabIndex = 48;
            this.ultraGrid1.TabStop = false;
            this.ultraGrid1.Text = "ultraGrid4";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.Controls.Add(this.button1);
            this.panel5.Controls.Add(this.searchBtn);
            this.panel5.Controls.Add(this.smallCheck);
            this.panel5.Controls.Add(this.excelBtn);
            this.panel5.Controls.Add(this.middleCheck);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(10, 121);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1192, 30);
            this.panel5.TabIndex = 49;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(995, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 25);
            this.button1.TabIndex = 24;
            this.button1.Text = "블록별 기본설정";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(1152, 3);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 23;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // smallCheck
            // 
            this.smallCheck.AutoSize = true;
            this.smallCheck.Checked = true;
            this.smallCheck.Location = new System.Drawing.Point(138, 10);
            this.smallCheck.Name = "smallCheck";
            this.smallCheck.Size = new System.Drawing.Size(83, 16);
            this.smallCheck.TabIndex = 1;
            this.smallCheck.TabStop = true;
            this.smallCheck.Text = "소블록기준";
            this.smallCheck.UseVisualStyleBackColor = true;
            // 
            // middleCheck
            // 
            this.middleCheck.AutoSize = true;
            this.middleCheck.Location = new System.Drawing.Point(3, 10);
            this.middleCheck.Name = "middleCheck";
            this.middleCheck.Size = new System.Drawing.Size(129, 16);
            this.middleCheck.TabIndex = 0;
            this.middleCheck.Text = "중블록(배수지)기준";
            this.middleCheck.UseVisualStyleBackColor = true;
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1192, 111);
            this.searchBox1.TabIndex = 50;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1212, 668);
            this.Controls.Add(this.ultraGrid1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmMain";
            this.Text = "집중관리 블록 우선 순위 조회";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton smallCheck;
        private System.Windows.Forms.RadioButton middleCheck;
        private System.Windows.Forms.Button excelBtn;
        private WaterNet.WV_Common.form.SearchBox searchBox1;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button button1;
    }
}