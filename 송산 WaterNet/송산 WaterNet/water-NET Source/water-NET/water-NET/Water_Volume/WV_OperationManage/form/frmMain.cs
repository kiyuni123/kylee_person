﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using System.Collections;
using WaterNet.WV_OperationManage.work;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;
using Infragistics.Win;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.work;
using EMFrame.log;
using System.Data;

namespace WaterNet.WV_OperationManage.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalType = WaterNet.WV_Common.enum1.INTERVAL_TYPE.SINGLE_MONTH;
            this.searchBox1.MiddleBlockContainer.Visible = false;
            this.searchBox1.SmallBlockContainer.Visible = false;
            this.searchBox1.MiddleBlockObject.SelectedIndex = 0;
            this.searchBox1.SmallBlockObject.SelectedIndex = 0;

            //확정일자를 다음달 10일이라고 가정할 경우,
            //현재 4.5일 이라면.
            //3월을 선택하는게 아니라, 4월 10일 이전이므로 2월을 선택한다.

            int day = Convert.ToInt32(OptionWork.GetInstance().GetRevenueFix(this.searchBox1.LargeBlockObject.SelectedValue.ToString()));

            if (DateTime.Now.Day < day)
            {
                this.searchBox1.YearMonthObject.SelectedValue = DateTime.Now.AddMonths(-2).ToString("yyyy");
                this.searchBox1.MonthObject.SelectedValue = DateTime.Now.AddMonths(-2).ToString("MM");
            }
            else
            {
                this.searchBox1.YearMonthObject.SelectedValue = DateTime.Now.AddMonths(-1).ToString("yyyy");
                this.searchBox1.MonthObject.SelectedValue = DateTime.Now.AddMonths(-1).ToString("MM");
            }
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            }

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            }

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
            }
        }

        private void InitializeEvent()
        {
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.ultraGrid1.DoubleClickHeader += new DoubleClickHeaderEventHandler(ultraGrid1_DoubleClickHeader);

            this.button1.Click += new EventHandler(button1_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
        }

        private void ultraGrid1_DoubleClickHeader(object sender, DoubleClickHeaderEventArgs e)
        {
            if (e.Header.Group != null)
            {
                if (e.Header.Group.Key == "REVENUE_RATIO" ||
                    e.Header.Group.Key == "NON_REVENUE_GUPSUJUNSU" ||
                    e.Header.Group.Key == "NON_REVENUE_PIPELEN" ||
                    e.Header.Group.Key == "GUPSUJUN_LEAKS_DAY" ||
                    e.Header.Group.Key == "PIPE_LENEN_LEAKS_DAY" ||
                    e.Header.Group.Key == "EQUIP_BURST" ||
                    e.Header.Group.Key == "GUPSUJUN_LEAKS_NT" ||
                    e.Header.Group.Key == "PIPE_LENEN_LEAKS_NT")
                {
                    frmWeight weight = new frmWeight(e.Header);
                    weight.ShowDialog();
                    this.ultraGrid1.Refresh();
                }
            }
        }

        //순위에 따라서 로우 색을 다르게 준다. (1,2,3 등급선까지만 강조)
        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            if (e.Layout.Rows.Count == 0)
            {
                return;
            }

            foreach (UltraGridRow gridRow in e.Layout.Rows)
            {
                string rank = gridRow.Cells["RANK"].Value.ToString();

                if (rank == "1" || rank == "2" || rank == "3")
                {
                    gridRow.CellAppearance.BackColor = Color.RosyBrown;
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Utils.HashOwnedForm(this, typeof(WV_BlockManage.form.frmMain)))
            {
                return;
            }

            WV_BlockManage.form.frmMain oForm = new WaterNet.WV_BlockManage.form.frmMain();
            oForm.Owner = this;
            oForm.Show();
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                excelManager.AddWorksheet(this.gridManager.Items[0], "집중관리우선순위");
                excelManager.Save("집중관리우선순위", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        Hashtable parameter = null;
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.parameter = this.searchBox1.InitializeParameter().Parameters;
                if (this.middleCheck.Checked)
                {
                    this.parameter["STANDARD"] = "MIDDLE";
                }
                else if (this.smallCheck.Checked)
                {
                    this.parameter["STANDARD"] = "SMALL";
                }

                this.parameter["YEAR_MON"] = this.parameter["STARTDATE"];

                this.parameter["STARTDATE"] = Utils.GetSuppliedStartdate(this.parameter["YEAR_MON"].ToString(), "yyyyMM", this.parameter["LOC_CODE"].ToString());
                this.parameter["ENDDATE"] = Utils.GetSuppliedEnddate(this.parameter["YEAR_MON"].ToString(), "yyyyMM", this.parameter["LOC_CODE"].ToString());

                this.parameter["REVENUE_RATIO"] = this.ultraGrid1.DisplayLayout.Bands[0].Groups["REVENUE_RATIO"].Header.Caption;
                this.parameter["NON_REVENUE_GUPSUJUNSU"] = this.ultraGrid1.DisplayLayout.Bands[0].Groups["NON_REVENUE_GUPSUJUNSU"].Header.Caption;
                this.parameter["NON_REVENUE_PIPELEN"] = this.ultraGrid1.DisplayLayout.Bands[0].Groups["NON_REVENUE_PIPELEN"].Header.Caption;
                this.parameter["GUPSUJUN_LEAKS_DAY"] = this.ultraGrid1.DisplayLayout.Bands[0].Groups["GUPSUJUN_LEAKS_DAY"].Header.Caption;
                this.parameter["PIPE_LENEN_LEAKS_DAY"] = this.ultraGrid1.DisplayLayout.Bands[0].Groups["PIPE_LENEN_LEAKS_DAY"].Header.Caption;
                this.parameter["EQUIP_BURST"] = this.ultraGrid1.DisplayLayout.Bands[0].Groups["EQUIP_BURST"].Header.Caption;
                this.parameter["GUPSUJUN_LEAKS_NT"] = this.ultraGrid1.DisplayLayout.Bands[0].Groups["GUPSUJUN_LEAKS_NT"].Header.Caption;
                this.parameter["PIPE_LENEN_LEAKS_NT"] = this.ultraGrid1.DisplayLayout.Bands[0].Groups["PIPE_LENEN_LEAKS_NT"].Header.Caption;

                this.SelectOperationManage(this.parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SelectOperationManage(Hashtable parameter)
        {
            this.ultraGrid1.DataSource = OperationManageWork.GetInstance().SelectLeakageRepairRanking(parameter);
        }
    }
}