﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using WaterNet.WaterNetCore;
using WaterNet.WV_WaterMeterDataInformation.dao;
using WaterNet.WV_Common.work;

namespace WaterNet.WV_WaterMeterDataInformation.work
{
    public class WaterMeterDataInformationWork : BaseWork
    {
        private static WaterMeterDataInformationWork work = null;
        private WaterMeterDataInformationDao dao = null;

        private WaterMeterDataInformationWork()
        {
            dao = WaterMeterDataInformationDao.GetInstance();
        }

        public static WaterMeterDataInformationWork GetInstance()
        {
            if(work == null)
            {
                work = new WaterMeterDataInformationWork();
            }

            return work;
        }

        //TAG List 조회
        public DataTable SelectTagList(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectTagList(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //시간별 검침데이터 조회
        public DataTable SelectMeterDataByTime(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectMeterDataByTime(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

        //시간별 검침데이터 수정
        public void UpdateMeterData(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                dao.UpdateMeterData(base.DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        //일별 검침데이터 조회
        public DataTable SelectMeterDataByDay(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectMeterDataByDay(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }
        
        //월별 검침데이터 조회
        public DataTable SelectMeterDataByMonth(Hashtable conditions)
        {
            OracleDBManager dbManager = null;
            DataTable result = null;

            try
            {
                dbManager = new OracleDBManager();
                dbManager.ConnectionString = WaterNetCore.FunctionManager.GetConnectionString();
                dbManager.Open();

                result = dao.SelectMeterDataByMonth(dbManager, conditions);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dbManager.Close();
            }

            return result;
        }

    }
}
