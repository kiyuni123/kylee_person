﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WaterNet.WV_WaterMeterDataInformation.work;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using WaterNet.WV_Common.util;
using Infragistics.Win;
using ChartFX.WinForms;

namespace WaterNet.WV_WaterMeterDataInformation.form
{
    public partial class frmMain : Form
    {
        #region 1.전역번수

        private WaterMeterDataInformationWork work = null;

        #endregion

        #region 2.초기화

        public frmMain()
        {
            InitializeComponent();
            InitializeSetting();
            work = WaterMeterDataInformationWork.GetInstance();
        }

        #endregion

        #region 3.객체 이벤트

        //Form 최초 로딩 시 실행
        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectTagList();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMessageWithConsole(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //조회 버튼 클릭 시 실행
        private void butSelectTagList_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectTagList();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMessageWithConsole(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //TAG 설명 Textbox에서 Enter키 누를 시 실행
        private void texDescription_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    SelectTagList();
                }
                catch (Exception ex)
                {
                    Utils.ShowExceptionMessageWithConsole(ex);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        //TagList Grid Row 선택 시 실행
        private void griTagList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectDetailData();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMessageWithConsole(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //시간별 조회 버튼 클릭 시 실행
        private void butSelectTime_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectMeterDataByTime();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMessageWithConsole(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //시간별 저장 버튼 클릭 시 실행
        private void butupdate_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                UpdateMeterData();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMessageWithConsole(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //일별 조회 버튼 클릭 시 실행
        private void butSelectDay_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectMeterDataByDay();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMessageWithConsole(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //월별 조회 버튼 클릭 시 실행
        private void butSelectMonth_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectMeterDataByMonth();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMessageWithConsole(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //검침데이터 Tab 변경 시 실행
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SelectDetailData();
            }
            catch (Exception ex)
            {
                Utils.ShowExceptionMessageWithConsole(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region 4.상속구현

        #endregion

        #region 5.외부호출

        #endregion

        #region 6.내부사용

        //Tag List 조회
        private void SelectTagList()
        {
            Hashtable conditions = new Hashtable();
            if (!"".Equals(texDescription.Text.Trim())) conditions.Add("DESCRIPTION", texDescription.Text);

            griTagList.DataSource = work.SelectTagList(conditions);
            Utils.SelectFirstGridRow(griTagList);
        }

        //시간별 검침데이터 조회
        private void SelectMeterDataByTime()
        {
            InitChart();              //차트 초기화
            if (griTagList.Selected.Rows.Count == 0) return;

            Hashtable conditions = new Hashtable();
            conditions.Add("TAGNAME", griTagList.Selected.Rows[0].Cells["TAGNAME"].Value.ToString());
            conditions.Add("fromDateTime", ((DateTime)datFromDateTime.Value).ToString("yyyyMMdd") + (comFromTime.Text).PadLeft(2, '0'));
            conditions.Add("endDateTime", ((DateTime)datEndDateTime.Value).ToString("yyyyMMdd") + (comEndTime.Text).PadLeft(2, '0'));

            DataTable timeData = work.SelectMeterDataByTime(conditions);

            griMeterDataTime.DataSource = timeData;
            chaMeterData.DataSource = timeData;
        }

        //시간별 검침데이터 수정
        private void UpdateMeterData()
        {
            DialogResult qe = MessageBox.Show("저장하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                InitChart();
                if (griTagList.Selected.Rows.Count == 0 && griMeterDataTime.Rows.Count == 0) return;
                
                foreach (UltraGridRow row in this.griMeterDataTime.Rows)
                {
                    Hashtable parameter = new Hashtable();
                    parameter["TAGNAME"] = griTagList.Selected.Rows[0].Cells["TAGNAME"].Value;
                    parameter["TIMESTAMP"] = Convert.ToDateTime(row.Cells["TIMESTAMP"].Value).ToString("yyyyMMddHH");
                    parameter["VALUE"] = row.Cells["VALUE"].Value;

                    work.UpdateMeterData(parameter);
                }

                MessageBox.Show("저장이 완료되었습니다.");

                SelectMeterDataByDay();
            }

        }

        //일별 검침데이터 조회
        private void SelectMeterDataByDay()
        {
            InitChart();              //차트 초기화
            if (griTagList.Selected.Rows.Count == 0) return;

            Hashtable conditions = new Hashtable();
            conditions.Add("TAGNAME", griTagList.Selected.Rows[0].Cells["TAGNAME"].Value.ToString());
            conditions.Add("fromDateDay", ((DateTime)datFromDateDay.Value).ToString("yyyyMMdd"));
            conditions.Add("endDateDay", ((DateTime)datEndDateDay.Value).ToString("yyyyMMdd"));

            DataTable dayData = work.SelectMeterDataByDay(conditions);

            griMeterDataDay.DataSource = dayData;
            chaMeterData.DataSource = dayData;
        }

        

        //월별 검침데이터 조회
        private void SelectMeterDataByMonth()
        {
            InitChart();              //차트 초기화
            if (griTagList.Selected.Rows.Count == 0) return;

            Hashtable conditions = new Hashtable();
            conditions.Add("TAGNAME", griTagList.Selected.Rows[0].Cells["TAGNAME"].Value.ToString());
            conditions.Add("fromDateMonth", ((DateTime)datFromDateMonth.Value).ToString("yyyy-MM"));
            conditions.Add("endDateMonth", ((DateTime)datEndDateMonth.Value).ToString("yyyy-MM"));

            DataTable monthData = work.SelectMeterDataByMonth(conditions);

            griMeterDataMonth.DataSource = work.SelectMeterDataByMonth(conditions);
            chaMeterData.DataSource = work.SelectMeterDataByMonth(conditions);
        }

        //사용 Component(Grid, Combo, Chart...) 초기화
        private void InitializeSetting()
        {
            #region Grid 초기화

            //Tag List Grid
            UltraGridColumn tagListColumn;

            tagListColumn =griTagList.DisplayLayout.Bands[0].Columns.Add();
            tagListColumn.Key = "TAGNAME";
            tagListColumn.Header.Caption = "TAG";
            tagListColumn.CellActivation = Activation.NoEdit;
            tagListColumn.CellClickAction = CellClickAction.RowSelect;
            tagListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListColumn.CellAppearance.TextHAlign = HAlign.Center;
            tagListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListColumn.Width = 100;
            tagListColumn.Hidden = false;   //필드 보이기

            tagListColumn = griTagList.DisplayLayout.Bands[0].Columns.Add();
            tagListColumn.Key = "DESCRIPTION";
            tagListColumn.Header.Caption = "TAG 설명";
            tagListColumn.CellActivation = Activation.NoEdit;
            tagListColumn.CellClickAction = CellClickAction.RowSelect;
            tagListColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            tagListColumn.CellAppearance.TextHAlign = HAlign.Left;
            tagListColumn.CellAppearance.TextVAlign = VAlign.Middle;
            tagListColumn.Width = 500;
            tagListColumn.Hidden = false;   //필드 보이기

            //시간별 조회 Grid
            UltraGridColumn meterDataTimeColumn;

            meterDataTimeColumn = griMeterDataTime.DisplayLayout.Bands[0].Columns.Add();
            meterDataTimeColumn.Key = "TIMESTAMP";
            meterDataTimeColumn.Header.Caption = "계측일시";
            meterDataTimeColumn.CellActivation = Activation.NoEdit;
            meterDataTimeColumn.CellClickAction = CellClickAction.RowSelect;
            meterDataTimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            meterDataTimeColumn.CellAppearance.TextHAlign = HAlign.Center;
            meterDataTimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            meterDataTimeColumn.Width = 190;
            meterDataTimeColumn.Hidden = false;   //필드 보이기

            meterDataTimeColumn = griMeterDataTime.DisplayLayout.Bands[0].Columns.Add();
            meterDataTimeColumn.Key = "VALUE";
            meterDataTimeColumn.Header.Caption = "계측값";
            meterDataTimeColumn.CellActivation = Activation.AllowEdit;
            meterDataTimeColumn.CellClickAction = CellClickAction.EditAndSelectText;
            meterDataTimeColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            meterDataTimeColumn.CellAppearance.TextHAlign = HAlign.Right;
            meterDataTimeColumn.CellAppearance.TextVAlign = VAlign.Middle;
            meterDataTimeColumn.Format = "###,###,##0.##";
            meterDataTimeColumn.Width = 190;
            meterDataTimeColumn.Hidden = false;   //필드 보이기

            //일별 조회 Grid
            UltraGridColumn meterDataDayColumn;

            meterDataDayColumn = griMeterDataDay.DisplayLayout.Bands[0].Columns.Add();
            meterDataDayColumn.Key = "TIMESTAMP";
            meterDataDayColumn.Header.Caption = "계측일";
            meterDataDayColumn.CellActivation = Activation.NoEdit;
            meterDataDayColumn.CellClickAction = CellClickAction.RowSelect;
            meterDataDayColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            meterDataDayColumn.CellAppearance.TextHAlign = HAlign.Center;
            meterDataDayColumn.CellAppearance.TextVAlign = VAlign.Middle;
            meterDataDayColumn.Width = 190;
            meterDataDayColumn.Hidden = false;   //필드 보이기

            meterDataDayColumn = griMeterDataDay.DisplayLayout.Bands[0].Columns.Add();
            meterDataDayColumn.Key = "VALUE";
            meterDataDayColumn.Header.Caption = "계측값";
            meterDataDayColumn.CellActivation = Activation.NoEdit;
            meterDataDayColumn.CellClickAction = CellClickAction.RowSelect;
            meterDataDayColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            meterDataDayColumn.CellAppearance.TextHAlign = HAlign.Right;
            meterDataDayColumn.CellAppearance.TextVAlign = VAlign.Middle;
            meterDataDayColumn.Format = "###,###,##0.##";
            meterDataDayColumn.Width = 190;
            meterDataDayColumn.Hidden = false;   //필드 보이기

            //월별 조회 Grid
            UltraGridColumn meterDataMonthColumn;

            meterDataMonthColumn = griMeterDataMonth.DisplayLayout.Bands[0].Columns.Add();
            meterDataMonthColumn.Key = "TIMESTAMP";
            meterDataMonthColumn.Header.Caption = "계측월";
            meterDataMonthColumn.CellActivation = Activation.NoEdit;
            meterDataMonthColumn.CellClickAction = CellClickAction.RowSelect;
            meterDataMonthColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            meterDataMonthColumn.CellAppearance.TextHAlign = HAlign.Center;
            meterDataMonthColumn.CellAppearance.TextVAlign = VAlign.Middle;
            meterDataMonthColumn.Width = 190;
            meterDataMonthColumn.Hidden = false;   //필드 보이기

            meterDataMonthColumn = griMeterDataMonth.DisplayLayout.Bands[0].Columns.Add();
            meterDataMonthColumn.Key = "VALUE";
            meterDataMonthColumn.Header.Caption = "계측값";
            meterDataMonthColumn.CellActivation = Activation.NoEdit;
            meterDataMonthColumn.CellClickAction = CellClickAction.RowSelect;
            meterDataMonthColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            meterDataMonthColumn.CellAppearance.TextHAlign = HAlign.Right;
            meterDataMonthColumn.CellAppearance.TextVAlign = VAlign.Middle;
            meterDataMonthColumn.Format = "###,###,##0.##";
            meterDataMonthColumn.Width = 190;
            meterDataMonthColumn.Hidden = false;   //필드 보이기

            #endregion

            #region Combo 및 날짜관련 초기화
            
            //시간 Combobox 초기화
            DataTable timeTable = new DataTable();
            timeTable.Columns.Add("time");
            
            for(int i = 0; i < 24; i++)
            {
                DataRow row = timeTable.NewRow();
                row["time"] = i.ToString();
                timeTable.Rows.Add(row);
            }

            comFromTime.DisplayMember = "time";
            comFromTime.ValueMember = "time";
            comFromTime.DataSource = timeTable;

            comEndTime.DisplayMember = "time";
            comEndTime.ValueMember = "time";
            comEndTime.DataSource = timeTable;

            comFromTime.SelectedText = "0";
            comEndTime.SelectedText = "23";

            //시간별검색 날짜 초기화
            datFromDateTime.Value = DateTime.Now.ToString("yyyy-MM-dd");
            datEndDateTime.Value = DateTime.Now.ToString("yyyy-MM-dd");

            //일별검색 날짜 초기화
            datFromDateDay.Value = DateTime.Now.AddDays(-15).ToString("yyyy-MM-dd");
            datEndDateDay.Value = DateTime.Now.ToString("yyyy-MM-dd");

            //월별검색 날짜 초기화
            datFromDateMonth.Value = DateTime.Now.AddMonths(-3).ToString("yyyy-MM");
            datEndDateMonth.Value = DateTime.Now.ToString("yyyy-MM-dd");

            #endregion

            #region Chart 초기화

            //차트 초기화
            InitChart();

            chaMeterData.AxesX[0].LabelTrimming = StringTrimming.None;
            chaMeterData.AxesY[0].LabelTrimming = StringTrimming.None;

            //차트 Data Field 설정
            chaMeterData.DataSourceSettings.Fields.Add(new FieldMap("TIMESTAMP", FieldUsage.Label));
            chaMeterData.DataSourceSettings.Fields.Add(new FieldMap("VALUE", FieldUsage.Value));

            //Y축 설정
            chaMeterData.AxisY.Title.Text = "계측값";
            chaMeterData.AxisY.LabelsFormat.Decimals = 2;        //y축 값을 소수점 2째자리까지 설정

            #endregion
        }

        //차트 초기화 (재조회 등을 실행할때 매회 Chart를 초기화 시켜야 함)
        private void InitChart()
        {
            chaMeterData.Data.Clear();
            chaMeterData.Data.Series = 1;
            chaMeterData.Series[0].Color = Color.Crimson;
            chaMeterData.Series[0].MarkerShape = MarkerShape.Circle;
            chaMeterData.Series[0].MarkerSize = 0;
            chaMeterData.Series[0].Line.Width = 1;
            chaMeterData.AxesY[0].AutoScale = true;            
        }

        //하단 데이터 일괄조회 (Tab 선택상황에 따른 데이터 조회)
        private void SelectDetailData()
        {
            if (tabControl1.SelectedIndex == 0)
            {
                SelectMeterDataByTime();        //시간별 조회
            }
            else if (tabControl1.SelectedIndex == 1)
            {
                SelectMeterDataByDay();        //일별 조회
            }
            else if (tabControl1.SelectedIndex == 2)
            {
                SelectMeterDataByMonth();        //월별 조회
            }
        }
        
        #endregion

        
    }
}
