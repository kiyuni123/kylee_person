﻿namespace WaterNet.WV_WaterMeterDataInformation.form
{
    partial class frmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground3 = new ChartFX.WinForms.Adornments.SolidBackground();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picFrLeftM1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butSelectTagList = new System.Windows.Forms.Button();
            this.texDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.griTagList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.griMeterDataTime = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comEndTime = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.comFromTime = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.datEndDateTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.butSelectTime = new System.Windows.Forms.Button();
            this.datFromDateTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.griMeterDataDay = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.datEndDateDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.butSelectDay = new System.Windows.Forms.Button();
            this.datFromDateDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.griMeterDataMonth = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.datEndDateMonth = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.butSelectMonth = new System.Windows.Forms.Button();
            this.datFromDateMonth = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chaMeterData = new ChartFX.WinForms.Chart();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.ultraDateTimeEditor3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.button3 = new System.Windows.Forms.Button();
            this.ultraDateTimeEditor4 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraCombo3 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraCombo4 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label4 = new System.Windows.Forms.Label();
            this.butupdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griTagList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griMeterDataTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comFromTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datFromDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griMeterDataDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDateDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datFromDateDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griMeterDataMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDateMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datFromDateMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaMeterData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo4)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(984, 9);
            this.pictureBox2.TabIndex = 118;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 653);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(984, 9);
            this.pictureBox1.TabIndex = 119;
            this.pictureBox1.TabStop = false;
            // 
            // picFrLeftM1
            // 
            this.picFrLeftM1.BackColor = System.Drawing.SystemColors.Control;
            this.picFrLeftM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.picFrLeftM1.Location = new System.Drawing.Point(0, 9);
            this.picFrLeftM1.Name = "picFrLeftM1";
            this.picFrLeftM1.Size = new System.Drawing.Size(10, 644);
            this.picFrLeftM1.TabIndex = 121;
            this.picFrLeftM1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(974, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 644);
            this.pictureBox3.TabIndex = 122;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butSelectTagList);
            this.groupBox1.Controls.Add(this.texDescription);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(964, 57);
            this.groupBox1.TabIndex = 127;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "검색조건";
            // 
            // butSelectTagList
            // 
            this.butSelectTagList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectTagList.Location = new System.Drawing.Point(916, 20);
            this.butSelectTagList.Name = "butSelectTagList";
            this.butSelectTagList.Size = new System.Drawing.Size(42, 23);
            this.butSelectTagList.TabIndex = 167;
            this.butSelectTagList.Text = "조회";
            this.butSelectTagList.UseVisualStyleBackColor = true;
            this.butSelectTagList.Click += new System.EventHandler(this.butSelectTagList_Click);
            // 
            // texDescription
            // 
            this.texDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.texDescription.Location = new System.Drawing.Point(66, 20);
            this.texDescription.Name = "texDescription";
            this.texDescription.Size = new System.Drawing.Size(350, 21);
            this.texDescription.TabIndex = 135;
            this.texDescription.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.texDescription_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 12);
            this.label3.TabIndex = 134;
            this.label3.Text = "TAG 설명";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox4.Location = new System.Drawing.Point(10, 66);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(964, 9);
            this.pictureBox4.TabIndex = 128;
            this.pictureBox4.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.griTagList);
            this.groupBox2.Controls.Add(this.pictureBox8);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.pictureBox6);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(10, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(964, 238);
            this.groupBox2.TabIndex = 129;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "TAG정보";
            // 
            // griTagList
            // 
            this.griTagList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griTagList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griTagList.DisplayLayout.MaxColScrollRegions = 1;
            this.griTagList.DisplayLayout.MaxRowScrollRegions = 1;
            this.griTagList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griTagList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griTagList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griTagList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griTagList.DisplayLayout.Override.CellPadding = 0;
            this.griTagList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griTagList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griTagList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griTagList.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griTagList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.griTagList.DisplayLayout.Override.RowSelectorAppearance = appearance1;
            this.griTagList.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griTagList.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griTagList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griTagList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griTagList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griTagList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griTagList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griTagList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griTagList.Location = new System.Drawing.Point(13, 26);
            this.griTagList.Name = "griTagList";
            this.griTagList.Size = new System.Drawing.Size(938, 200);
            this.griTagList.TabIndex = 152;
            this.griTagList.Text = "ultraGrid1";
            this.griTagList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.griTagList_AfterSelectChange);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox8.Location = new System.Drawing.Point(951, 26);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(10, 200);
            this.pictureBox8.TabIndex = 123;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox7.Location = new System.Drawing.Point(3, 26);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 200);
            this.pictureBox7.TabIndex = 122;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox6.Location = new System.Drawing.Point(3, 226);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(958, 9);
            this.pictureBox6.TabIndex = 120;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(3, 17);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(958, 9);
            this.pictureBox5.TabIndex = 119;
            this.pictureBox5.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(10, 313);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(964, 10);
            this.splitter1.TabIndex = 130;
            this.splitter1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tabControl1);
            this.groupBox3.Controls.Add(this.pictureBox9);
            this.groupBox3.Controls.Add(this.pictureBox10);
            this.groupBox3.Controls.Add(this.pictureBox11);
            this.groupBox3.Controls.Add(this.pictureBox12);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox3.Location = new System.Drawing.Point(10, 323);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(463, 330);
            this.groupBox3.TabIndex = 131;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "검침데이터";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(13, 26);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(437, 292);
            this.tabControl1.TabIndex = 124;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.griMeterDataTime);
            this.tabPage1.Controls.Add(this.pictureBox29);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.pictureBox20);
            this.tabPage1.Controls.Add(this.pictureBox19);
            this.tabPage1.Controls.Add(this.pictureBox18);
            this.tabPage1.Controls.Add(this.pictureBox17);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(429, 266);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "시간별조회";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // griMeterDataTime
            // 
            this.griMeterDataTime.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMeterDataTime.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griMeterDataTime.DisplayLayout.MaxColScrollRegions = 1;
            this.griMeterDataTime.DisplayLayout.MaxRowScrollRegions = 1;
            this.griMeterDataTime.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griMeterDataTime.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMeterDataTime.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMeterDataTime.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griMeterDataTime.DisplayLayout.Override.CellPadding = 0;
            this.griMeterDataTime.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griMeterDataTime.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griMeterDataTime.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griMeterDataTime.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griMeterDataTime.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.griMeterDataTime.DisplayLayout.Override.RowSelectorAppearance = appearance3;
            this.griMeterDataTime.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griMeterDataTime.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griMeterDataTime.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griMeterDataTime.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griMeterDataTime.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griMeterDataTime.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griMeterDataTime.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griMeterDataTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griMeterDataTime.Location = new System.Drawing.Point(13, 55);
            this.griMeterDataTime.Name = "griMeterDataTime";
            this.griMeterDataTime.Size = new System.Drawing.Size(403, 199);
            this.griMeterDataTime.TabIndex = 171;
            this.griMeterDataTime.Text = "ultraGrid1";
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox29.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox29.Location = new System.Drawing.Point(13, 46);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(403, 9);
            this.pictureBox29.TabIndex = 170;
            this.pictureBox29.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butupdate);
            this.panel1.Controls.Add(this.comEndTime);
            this.panel1.Controls.Add(this.comFromTime);
            this.panel1.Controls.Add(this.datEndDateTime);
            this.panel1.Controls.Add(this.butSelectTime);
            this.panel1.Controls.Add(this.datFromDateTime);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(13, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(403, 34);
            this.panel1.TabIndex = 169;
            // 
            // comEndTime
            // 
            this.comEndTime.Location = new System.Drawing.Point(275, 7);
            this.comEndTime.Name = "comEndTime";
            this.comEndTime.Size = new System.Drawing.Size(39, 21);
            this.comEndTime.TabIndex = 170;
            // 
            // comFromTime
            // 
            this.comFromTime.Location = new System.Drawing.Point(113, 7);
            this.comFromTime.Name = "comFromTime";
            this.comFromTime.Size = new System.Drawing.Size(39, 21);
            this.comFromTime.TabIndex = 169;
            // 
            // datEndDateTime
            // 
            this.datEndDateTime.DateTime = new System.DateTime(2015, 4, 21, 0, 0, 0, 0);
            this.datEndDateTime.Location = new System.Drawing.Point(167, 7);
            this.datEndDateTime.Name = "datEndDateTime";
            this.datEndDateTime.Size = new System.Drawing.Size(106, 21);
            this.datEndDateTime.TabIndex = 1;
            this.datEndDateTime.Value = new System.DateTime(2015, 4, 21, 0, 0, 0, 0);
            // 
            // butSelectTime
            // 
            this.butSelectTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectTime.Location = new System.Drawing.Point(318, 6);
            this.butSelectTime.Name = "butSelectTime";
            this.butSelectTime.Size = new System.Drawing.Size(42, 23);
            this.butSelectTime.TabIndex = 168;
            this.butSelectTime.Text = "조회";
            this.butSelectTime.UseVisualStyleBackColor = true;
            this.butSelectTime.Click += new System.EventHandler(this.butSelectTime_Click);
            // 
            // datFromDateTime
            // 
            this.datFromDateTime.DateTime = new System.DateTime(2015, 4, 21, 0, 0, 0, 0);
            this.datFromDateTime.Location = new System.Drawing.Point(4, 7);
            this.datFromDateTime.Name = "datFromDateTime";
            this.datFromDateTime.Size = new System.Drawing.Size(106, 21);
            this.datFromDateTime.TabIndex = 0;
            this.datFromDateTime.Value = new System.DateTime(2015, 4, 21, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(153, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 12);
            this.label1.TabIndex = 153;
            this.label1.Text = "~";
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox20.Location = new System.Drawing.Point(13, 254);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(403, 9);
            this.pictureBox20.TabIndex = 126;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox19.Location = new System.Drawing.Point(13, 3);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(403, 9);
            this.pictureBox19.TabIndex = 125;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox18.Location = new System.Drawing.Point(416, 3);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(10, 260);
            this.pictureBox18.TabIndex = 124;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox17.Location = new System.Drawing.Point(3, 3);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(10, 260);
            this.pictureBox17.TabIndex = 123;
            this.pictureBox17.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.griMeterDataDay);
            this.tabPage2.Controls.Add(this.pictureBox30);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.pictureBox26);
            this.tabPage2.Controls.Add(this.pictureBox25);
            this.tabPage2.Controls.Add(this.pictureBox22);
            this.tabPage2.Controls.Add(this.pictureBox21);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(429, 266);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "일별조회";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // griMeterDataDay
            // 
            this.griMeterDataDay.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMeterDataDay.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griMeterDataDay.DisplayLayout.MaxColScrollRegions = 1;
            this.griMeterDataDay.DisplayLayout.MaxRowScrollRegions = 1;
            this.griMeterDataDay.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griMeterDataDay.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMeterDataDay.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMeterDataDay.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griMeterDataDay.DisplayLayout.Override.CellPadding = 0;
            this.griMeterDataDay.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griMeterDataDay.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griMeterDataDay.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griMeterDataDay.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griMeterDataDay.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.griMeterDataDay.DisplayLayout.Override.RowSelectorAppearance = appearance4;
            this.griMeterDataDay.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griMeterDataDay.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griMeterDataDay.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griMeterDataDay.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griMeterDataDay.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griMeterDataDay.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griMeterDataDay.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griMeterDataDay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griMeterDataDay.Location = new System.Drawing.Point(13, 55);
            this.griMeterDataDay.Name = "griMeterDataDay";
            this.griMeterDataDay.Size = new System.Drawing.Size(403, 199);
            this.griMeterDataDay.TabIndex = 173;
            this.griMeterDataDay.Text = "ultraGrid2";
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox30.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox30.Location = new System.Drawing.Point(13, 46);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(403, 9);
            this.pictureBox30.TabIndex = 172;
            this.pictureBox30.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.datEndDateDay);
            this.panel2.Controls.Add(this.butSelectDay);
            this.panel2.Controls.Add(this.datFromDateDay);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(13, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(403, 34);
            this.panel2.TabIndex = 170;
            // 
            // datEndDateDay
            // 
            this.datEndDateDay.Location = new System.Drawing.Point(122, 7);
            this.datEndDateDay.Name = "datEndDateDay";
            this.datEndDateDay.Size = new System.Drawing.Size(100, 21);
            this.datEndDateDay.TabIndex = 1;
            // 
            // butSelectDay
            // 
            this.butSelectDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectDay.Location = new System.Drawing.Point(358, 7);
            this.butSelectDay.Name = "butSelectDay";
            this.butSelectDay.Size = new System.Drawing.Size(42, 23);
            this.butSelectDay.TabIndex = 168;
            this.butSelectDay.Text = "조회";
            this.butSelectDay.UseVisualStyleBackColor = true;
            this.butSelectDay.Click += new System.EventHandler(this.butSelectDay_Click);
            // 
            // datFromDateDay
            // 
            this.datFromDateDay.Location = new System.Drawing.Point(4, 7);
            this.datFromDateDay.Name = "datFromDateDay";
            this.datFromDateDay.Size = new System.Drawing.Size(100, 21);
            this.datFromDateDay.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(105, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 12);
            this.label2.TabIndex = 153;
            this.label2.Text = "~";
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox26.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox26.Location = new System.Drawing.Point(13, 254);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(403, 9);
            this.pictureBox26.TabIndex = 127;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox25.Location = new System.Drawing.Point(13, 3);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(403, 9);
            this.pictureBox25.TabIndex = 126;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox22.Location = new System.Drawing.Point(416, 3);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(10, 260);
            this.pictureBox22.TabIndex = 125;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox21.Location = new System.Drawing.Point(3, 3);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(10, 260);
            this.pictureBox21.TabIndex = 124;
            this.pictureBox21.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.griMeterDataMonth);
            this.tabPage3.Controls.Add(this.pictureBox31);
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Controls.Add(this.pictureBox28);
            this.tabPage3.Controls.Add(this.pictureBox27);
            this.tabPage3.Controls.Add(this.pictureBox24);
            this.tabPage3.Controls.Add(this.pictureBox23);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(429, 266);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "월별조회";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // griMeterDataMonth
            // 
            this.griMeterDataMonth.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMeterDataMonth.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.griMeterDataMonth.DisplayLayout.MaxColScrollRegions = 1;
            this.griMeterDataMonth.DisplayLayout.MaxRowScrollRegions = 1;
            this.griMeterDataMonth.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.griMeterDataMonth.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMeterDataMonth.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            this.griMeterDataMonth.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.griMeterDataMonth.DisplayLayout.Override.CellPadding = 0;
            this.griMeterDataMonth.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
            this.griMeterDataMonth.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.griMeterDataMonth.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.griMeterDataMonth.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.griMeterDataMonth.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.Standard;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.griMeterDataMonth.DisplayLayout.Override.RowSelectorAppearance = appearance2;
            this.griMeterDataMonth.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.griMeterDataMonth.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;
            this.griMeterDataMonth.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.griMeterDataMonth.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.griMeterDataMonth.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.griMeterDataMonth.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.griMeterDataMonth.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.griMeterDataMonth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griMeterDataMonth.Location = new System.Drawing.Point(13, 55);
            this.griMeterDataMonth.Name = "griMeterDataMonth";
            this.griMeterDataMonth.Size = new System.Drawing.Size(403, 199);
            this.griMeterDataMonth.TabIndex = 174;
            this.griMeterDataMonth.Text = "ultraGrid3";
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox31.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox31.Location = new System.Drawing.Point(13, 46);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(403, 9);
            this.pictureBox31.TabIndex = 173;
            this.pictureBox31.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.datEndDateMonth);
            this.panel3.Controls.Add(this.butSelectMonth);
            this.panel3.Controls.Add(this.datFromDateMonth);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(13, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(403, 34);
            this.panel3.TabIndex = 171;
            // 
            // datEndDateMonth
            // 
            this.datEndDateMonth.Location = new System.Drawing.Point(108, 7);
            this.datEndDateMonth.MaskInput = "yyyy-mm";
            this.datEndDateMonth.Name = "datEndDateMonth";
            this.datEndDateMonth.Size = new System.Drawing.Size(80, 21);
            this.datEndDateMonth.TabIndex = 1;
            // 
            // butSelectMonth
            // 
            this.butSelectMonth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectMonth.Location = new System.Drawing.Point(358, 7);
            this.butSelectMonth.Name = "butSelectMonth";
            this.butSelectMonth.Size = new System.Drawing.Size(42, 23);
            this.butSelectMonth.TabIndex = 168;
            this.butSelectMonth.Text = "조회";
            this.butSelectMonth.UseVisualStyleBackColor = true;
            this.butSelectMonth.Click += new System.EventHandler(this.butSelectMonth_Click);
            // 
            // datFromDateMonth
            // 
            this.datFromDateMonth.DateTime = new System.DateTime(2015, 4, 8, 14, 0, 13, 0);
            this.datFromDateMonth.Location = new System.Drawing.Point(4, 7);
            this.datFromDateMonth.MaskInput = "yyyy-mm";
            this.datFromDateMonth.Name = "datFromDateMonth";
            this.datFromDateMonth.Size = new System.Drawing.Size(80, 21);
            this.datFromDateMonth.TabIndex = 0;
            this.datFromDateMonth.Value = new System.DateTime(2015, 4, 8, 14, 0, 13, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(88, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 12);
            this.label5.TabIndex = 153;
            this.label5.Text = "~";
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox28.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox28.Location = new System.Drawing.Point(13, 254);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(403, 9);
            this.pictureBox28.TabIndex = 127;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox27.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox27.Location = new System.Drawing.Point(13, 3);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(403, 9);
            this.pictureBox27.TabIndex = 126;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox24.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox24.Location = new System.Drawing.Point(416, 3);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(10, 260);
            this.pictureBox24.TabIndex = 125;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox23.Location = new System.Drawing.Point(3, 3);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(10, 260);
            this.pictureBox23.TabIndex = 124;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox9.Location = new System.Drawing.Point(450, 26);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(10, 292);
            this.pictureBox9.TabIndex = 123;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox10.Location = new System.Drawing.Point(3, 26);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(10, 292);
            this.pictureBox10.TabIndex = 122;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox11.Location = new System.Drawing.Point(3, 318);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(457, 9);
            this.pictureBox11.TabIndex = 120;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox12.Location = new System.Drawing.Point(3, 17);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(457, 9);
            this.pictureBox12.TabIndex = 119;
            this.pictureBox12.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(473, 323);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(10, 330);
            this.splitter2.TabIndex = 132;
            this.splitter2.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chaMeterData);
            this.groupBox4.Controls.Add(this.pictureBox13);
            this.groupBox4.Controls.Add(this.pictureBox14);
            this.groupBox4.Controls.Add(this.pictureBox15);
            this.groupBox4.Controls.Add(this.pictureBox16);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(483, 323);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(491, 330);
            this.groupBox4.TabIndex = 133;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "검침데이터Chart";
            // 
            // chaMeterData
            // 
            this.chaMeterData.AllSeries.Line.Width = ((short)(1));
            this.chaMeterData.AllSeries.MarkerShape = ChartFX.WinForms.MarkerShape.None;
            this.chaMeterData.AllSeries.MarkerSize = ((short)(2));
            solidBackground3.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chaMeterData.Background = solidBackground3;
            this.chaMeterData.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chaMeterData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chaMeterData.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chaMeterData.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chaMeterData.LegendBox.Visible = false;
            this.chaMeterData.Location = new System.Drawing.Point(13, 26);
            this.chaMeterData.Name = "chaMeterData";
            this.chaMeterData.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chaMeterData.Size = new System.Drawing.Size(465, 292);
            this.chaMeterData.TabIndex = 133;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox13.Location = new System.Drawing.Point(478, 26);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(10, 292);
            this.pictureBox13.TabIndex = 123;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox14.Location = new System.Drawing.Point(3, 26);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(10, 292);
            this.pictureBox14.TabIndex = 122;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox15.Location = new System.Drawing.Point(3, 318);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(485, 9);
            this.pictureBox15.TabIndex = 120;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox16.Location = new System.Drawing.Point(3, 17);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(485, 9);
            this.pictureBox16.TabIndex = 119;
            this.pictureBox16.TabStop = false;
            // 
            // ultraDateTimeEditor3
            // 
            this.ultraDateTimeEditor3.DateTime = new System.DateTime(2015, 3, 26, 0, 0, 0, 0);
            this.ultraDateTimeEditor3.Location = new System.Drawing.Point(186, 7);
            this.ultraDateTimeEditor3.Name = "ultraDateTimeEditor3";
            this.ultraDateTimeEditor3.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor3.TabIndex = 1;
            this.ultraDateTimeEditor3.Value = new System.DateTime(2015, 3, 26, 0, 0, 0, 0);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(358, 7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(42, 23);
            this.button3.TabIndex = 168;
            this.button3.Text = "조회";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // ultraDateTimeEditor4
            // 
            this.ultraDateTimeEditor4.DateTime = new System.DateTime(2015, 3, 26, 0, 0, 0, 0);
            this.ultraDateTimeEditor4.Location = new System.Drawing.Point(4, 7);
            this.ultraDateTimeEditor4.Name = "ultraDateTimeEditor4";
            this.ultraDateTimeEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor4.TabIndex = 0;
            this.ultraDateTimeEditor4.Value = new System.DateTime(2015, 3, 26, 0, 0, 0, 0);
            // 
            // ultraCombo3
            // 
            this.ultraCombo3.AutoSize = false;
            this.ultraCombo3.CheckedListSettings.CheckStateMember = "";
            this.ultraCombo3.Location = new System.Drawing.Point(289, 8);
            this.ultraCombo3.Name = "ultraCombo3";
            this.ultraCombo3.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.ultraCombo3.Size = new System.Drawing.Size(57, 21);
            this.ultraCombo3.TabIndex = 155;
            // 
            // ultraCombo4
            // 
            this.ultraCombo4.AutoSize = false;
            this.ultraCombo4.CheckedListSettings.CheckStateMember = "";
            this.ultraCombo4.Location = new System.Drawing.Point(107, 7);
            this.ultraCombo4.Name = "ultraCombo4";
            this.ultraCombo4.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.ultraCombo4.Size = new System.Drawing.Size(57, 21);
            this.ultraCombo4.TabIndex = 152;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(169, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 12);
            this.label4.TabIndex = 153;
            this.label4.Text = "~";
            // 
            // butupdate
            // 
            this.butupdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butupdate.Location = new System.Drawing.Point(361, 6);
            this.butupdate.Name = "butupdate";
            this.butupdate.Size = new System.Drawing.Size(42, 23);
            this.butupdate.TabIndex = 171;
            this.butupdate.Text = "저장";
            this.butupdate.UseVisualStyleBackColor = true;
            this.butupdate.Click += new System.EventHandler(this.butupdate_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 662);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFrLeftM1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.MinimumSize = new System.Drawing.Size(1000, 700);
            this.Name = "frmMain";
            this.Text = "수도계량기 검침데이터 조회";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFrLeftM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griTagList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griMeterDataTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comFromTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datFromDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griMeterDataDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDateDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datFromDateDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griMeterDataMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datEndDateMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datFromDateMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chaMeterData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picFrLeftM1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button butSelectTagList;
        private System.Windows.Forms.TextBox texDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private Infragistics.Win.UltraWinGrid.UltraGrid griTagList;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.Button butSelectTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datFromDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datEndDateTime;
        private System.Windows.Forms.PictureBox pictureBox29;
        private Infragistics.Win.UltraWinGrid.UltraGrid griMeterDataTime;
        private Infragistics.Win.UltraWinGrid.UltraGrid griMeterDataDay;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datEndDateDay;
        private System.Windows.Forms.Button butSelectDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datFromDateDay;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinGrid.UltraGrid griMeterDataMonth;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datEndDateMonth;
        private System.Windows.Forms.Button butSelectMonth;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datFromDateMonth;
        private System.Windows.Forms.Label label5;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor3;
        private System.Windows.Forms.Button button3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor4;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo3;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo4;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comFromTime;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comEndTime;
        private ChartFX.WinForms.Chart chaMeterData;
        private System.Windows.Forms.Button butupdate;
    }
}

