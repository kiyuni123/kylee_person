﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using WaterNet.WaterNetCore;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_WaterMeterDataInformation.dao
{
    public class WaterMeterDataInformationDao
    {
        private static WaterMeterDataInformationDao dao = null;

        private WaterMeterDataInformationDao()
        {
        }

        public static WaterMeterDataInformationDao GetInstance()
        {
            if(dao == null)
            {
                dao = new WaterMeterDataInformationDao();
            }

            return dao;
        }

        //TAG List 조회
        public DataTable SelectTagList(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("SELECT      TAGNAME							        ");
            queryString.AppendLine("            ,DESCRIPTION							");
            queryString.AppendLine("from        IF_IHTAGS							    ");
            queryString.AppendLine("where       USE_YN        = 'Y'						");
            queryString.AppendLine("and         MTR_YN        = 'Y'						");
            queryString.AppendLine("and         DESCRIPTION   like '%' || :1 || '%'     ");
            queryString.AppendLine("order by    DESCRIPTION							    ");

            string description = "";
            if (conditions["DESCRIPTION"] != null) description = (string)conditions["DESCRIPTION"];


            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
            };

            parameters[0].Value = description;

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //시간별 검침데이터 조회
        public DataTable SelectMeterDataByTime(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("SELECT        to_char(TIMESTAMP, 'yyyy-mm-dd hh24:mi')  as TIMESTAMP			");
            queryString.AppendLine("              ,to_number(VALUE)                        	as VALUE				");
            queryString.AppendLine("from          IF_ACCUMULATION_HOUR							                    ");
            queryString.AppendLine("where         TAGNAME       = :1							                    ");
            queryString.AppendLine("and           TIMESTAMP     between to_date(:2,'yyyymmddhh24mi') 	            ");
            queryString.AppendLine("                            and     to_date(:3,'yyyymmddhh24mi')                ");
            queryString.AppendLine("order by      TIMESTAMP     							                        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = conditions["TAGNAME"];
            parameters[1].Value = conditions["fromDateTime"] + "00";
            parameters[2].Value = conditions["endDateTime"] + "59";

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        public void UpdateMeterData(OracleDBManager dbmanager, Hashtable parameter)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("UPDATE IF_ACCUMULATION_HOUR SET VALUE = :VALUE  ");
            queryString.AppendLine("WHERE TAGNAME = :TAGNAME                        ");
            queryString.AppendLine("AND TO_CHAR(TIMESTAMP,'yyyyMMddHH24') =:TIMESTAMP                       ");

            IDataParameter[] parameters =  {
                    new OracleParameter("VALUE", OracleDbType.Varchar2)
                    ,new OracleParameter("TAGNAME", OracleDbType.Varchar2)
                    ,new OracleParameter("TIMESTAMP", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["VALUE"];
            parameters[1].Value = parameter["TAGNAME"];
            parameters[2].Value = parameter["TIMESTAMP"];

            dbmanager.ExecuteScript(queryString.ToString(), parameters); 

        }

        //일별 검침데이터 조회
        public DataTable SelectMeterDataByDay(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("SELECT        to_char(TIMESTAMP, 'yyyy-mm-dd')  as TIMESTAMP			        ");
            queryString.AppendLine("              ,sum(VALUE)                       as VALUE						");
            queryString.AppendLine("from          IF_ACCUMULATION_HOUR							                    ");
            queryString.AppendLine("where         TAGNAME       = :1							                    ");
            queryString.AppendLine("and           TIMESTAMP     between to_date(:2,'yyyymmddhh24mi') 	            ");
            queryString.AppendLine("                            and     to_date(:3,'yyyymmddhh24mi')                ");
            queryString.AppendLine("group by      to_char(TIMESTAMP, 'yyyy-mm-dd')                                  ");
            queryString.AppendLine("order by      TIMESTAMP     							                        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = conditions["TAGNAME"];
            parameters[1].Value = conditions["fromDateDay"] + "0000";
            parameters[2].Value = conditions["endDateDay"] + "2359";

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);
        }

        //월별 검침데이터 조회
        public DataTable SelectMeterDataByMonth(OracleDBManager dbManager, Hashtable conditions)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.AppendLine("SELECT        to_char(TIMESTAMP, 'yyyy-mm') as TIMESTAMP			            ");
            queryString.AppendLine("              ,sum(VALUE)                   as VALUE							");
            queryString.AppendLine("from          IF_ACCUMULATION_HOUR							                    ");
            queryString.AppendLine("where         TAGNAME       = :1							                    ");
            //queryString.AppendLine("and           TIMESTAMP     between to_date(:2,'yyyymm') 	                    ");
            //queryString.AppendLine("                            and     to_date(:3,'yyyymm')                        ");

            queryString.AppendLine("having        to_char(TIMESTAMP, 'yyyy-mm') between :2                          ");
            queryString.AppendLine("                                            and     :3                          ");


            queryString.AppendLine("group by      to_char(TIMESTAMP, 'yyyy-mm')                                     ");
            queryString.AppendLine("order by      TIMESTAMP     							                        ");

            IDataParameter[] parameters =  {
                 new OracleParameter("1", OracleDbType.Varchar2)
                 ,new OracleParameter("2", OracleDbType.Varchar2)
                 ,new OracleParameter("3", OracleDbType.Varchar2)
            };

            parameters[0].Value = conditions["TAGNAME"];
            parameters[1].Value = conditions["fromDateMonth"];
            parameters[2].Value = conditions["endDateMonth"];

            return dbManager.ExecuteScriptDataTable(queryString.ToString(), parameters);

        }
    }
}
