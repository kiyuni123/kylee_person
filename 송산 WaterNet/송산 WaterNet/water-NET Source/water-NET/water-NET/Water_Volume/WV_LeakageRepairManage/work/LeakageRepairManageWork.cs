﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterNet.WV_LeakageRepairManage.dao;
using WaterNet.WV_Common.work;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_LeakageRepairManage.work
{
    public class LeakageRepairManageWork : BaseWork
    {
        private static LeakageRepairManageWork work = null;
        private LeakageRepairManageDao dao = null;

        public static LeakageRepairManageWork GetInstance()
        {
            if (work == null)
            {
                work = new LeakageRepairManageWork();
            }
            return work;
        }

        private LeakageRepairManageWork()
        {
            dao = LeakageRepairManageDao.GetInstance();
        }

        public DataSet SelectLeakageRepair(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectLeakageRepair(base.DataBaseManager, result, "RESULT", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public void UpdateLeakageRepair(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.UpdateLeakageRepair(base.DataBaseManager, parameter);
                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                e = new Exception("데이터 업로드 오류가 발생했습니다.\n누수지점 파일을 확인후 다시 진행해주세요.");
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void UpdateLeakageRepair(RowsCollection rows)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();
                foreach (UltraGridRow gridRow in rows)
                {
                    Hashtable parameter = Utils.ConverToHashtable(gridRow);
                    parameter["MDFY_YN"] = "N";
                    parameter["LOC_CODE"] = parameter["SBLOCK"];
                    dao.UpdateLeakageRepair(base.DataBaseManager, parameter);
                }
                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                e = new Exception("데이터 업로드 오류가 발생했습니다.\n누수지점 파일을 확인후 다시 진행해주세요.");
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void UpdateLeakageRepairPoint(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.UpdateLeakageRepairPoint(base.DataBaseManager, parameter);
                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                e = new Exception("데이터 업로드 오류가 발생했습니다.\n누수지점 파일을 확인후 다시 진행해주세요.");
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public void DeleteLeakageRepair(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.DeleteLeakageRepair(base.DataBaseManager, parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            MessageBox.Show("정상적으로 처리되었습니다.");
        }

        public object GetLeakageRepairNo()
        {
            object value = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();
                value = dao.GetLeakageRepairNo(base.DataBaseManager);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return value;
        }

        public DataSet SelectLeakageMinWon(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectLeakageMinWon(base.DataBaseManager, result, "RESULT", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectLeakageReport(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                //dao.SelectLeakageRepair(base.DataBaseManager, result, "RESULT", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }



        public DataSet SelectLeakageTamSa(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectLeakageTamSa(base.DataBaseManager, result, "RESULT", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectLeakageTamSaRepair(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectLeakageTamSaRepair(base.DataBaseManager, result, "RESULT", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet SelectLeakageMinWonRepair(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();
                dao.SelectLeakageMinWonRepair(base.DataBaseManager, result, "RESULT", parameter);
                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet UpdateExcelImport(DataTable data)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (DataRow row in data.Rows)
                {
                    this.dao.UpdateExcelImport(DataBaseManager, Utils.ConverToHashtable(row));
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
