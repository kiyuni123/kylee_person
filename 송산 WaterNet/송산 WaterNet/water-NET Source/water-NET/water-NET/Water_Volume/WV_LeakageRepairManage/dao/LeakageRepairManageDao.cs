﻿using System.Text;
using WaterNet.WaterNetCore;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.dao;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_LeakageRepairManage.dao
{
    public class LeakageRepairManageDao : BaseDao
    {
        private static LeakageRepairManageDao dao = null;

        private LeakageRepairManageDao() { }

        public static LeakageRepairManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new LeakageRepairManageDao();
            }
            return dao;
        }

        //누수복구목록조회
        public void SelectLeakageRepair(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                        ");
            query.AppendLine("(                                                                                                                                  ");
            query.AppendLine("select c1.loc_code                                                                                                                 ");
            query.AppendLine("      ,c1.sgccd                                                                                                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                            ");
            query.AppendLine("      ,c1.ftr_idn                                                                                                                  ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                 ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code        ");
            query.AppendLine("      ,c1.ord                                                                                                                      ");
            query.AppendLine("  from                                                                                                                             ");
            query.AppendLine("      (                                                                                                                            ");
            query.AppendLine("       select sgccd                                                                                                                ");
            query.AppendLine("             ,loc_code                                                                                                             ");
            query.AppendLine("             ,ploc_code                                                                                                            ");
            query.AppendLine("             ,loc_name                                                                                                             ");
            query.AppendLine("             ,ftr_idn                                                                                                              ");
            query.AppendLine("             ,ftr_code                                                                                                             ");
            query.AppendLine("             ,kt_gbn                                                                                                               ");
            query.AppendLine("             ,rownum ord                                                                                                           ");
            query.AppendLine("         from cm_location                                                                                                          ");
            query.AppendLine("        where 1 = 1                                                                                                                ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                            ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                      ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                  ");
            query.AppendLine("      ) c1                                                                                                                         ");
            query.AppendLine("       ,cm_location c2                                                                                                             ");
            query.AppendLine("       ,cm_location c3                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                       ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            query.AppendLine(" order by c1.ord                                                                                                                   ");
            query.AppendLine(")                                                                                                                                  ");
            query.AppendLine("select loc.loc_code                                                                                                                ");
            query.AppendLine("      ,loc.sgccd                                                                                                                   ");
            query.AppendLine("      ,loc.lblock                                                                                                                  ");
            query.AppendLine("      ,loc.mblock                                                                                                                  ");
            query.AppendLine("      ,loc.sblock                                                                                                                  ");
            query.AppendLine("      ,loc.ftr_idn sftr_idn                                                                                                        ");
            query.AppendLine("      ,wlr.ftr_idn                                                                                                                 ");
            query.AppendLine("      ,wlr.mdfy_yn                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_idn                                                                                                                 ");
            query.AppendLine("      ,wlr.cano	                                                                                                                 ");
            query.AppendLine("      ,wlr.hjd_cde                                                                                                                 ");
            query.AppendLine("      ,to_char(to_date(wlr.lrs_ymd, 'yyyymmdd'),'yyyy-mm-dd') lrs_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.lek_ymd, 'yyyymmdd'),'yyyy-mm-dd') lek_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.res_ymd, 'yyyymmdd'),'yyyy-mm-dd') res_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.ree_ymd, 'yyyymmdd'),'yyyy-mm-dd') ree_ymd                                                              ");
            query.AppendLine("      ,wlr.lek_loc                                                                                                                 ");
            query.AppendLine("      ,wlr.pip_mop                                                                                                                 ");
            query.AppendLine("      ,wlr.pip_dip                                                                                                                 ");
            query.AppendLine("      ,wlr.lrs_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lep_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_exp                                                                                                                 ");
            query.AppendLine("      ,wlr.rep_exp                                                                                                                 ");
            query.AppendLine("      ,wlr.mat_des                                                                                                                 ");
            query.AppendLine("      ,wlr.rep_nam                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_vol                                                                                                                 ");
            query.AppendLine("      ,to_date(wlr.lrs_hms, 'hh24mi') lrs_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.lek_hms, 'hh24mi') lek_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.res_hms, 'hh24mi') res_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.ree_hms, 'hh24mi') ree_hms                                                                                      ");
            query.AppendLine("      ,wlr.wtp_per                                                                                                                 ");
            query.AppendLine("      ,wlr.x                                                                                                                       ");
            query.AppendLine("      ,wlr.y                                                                                                                       ");
            query.AppendLine("  from loc                                                                                                                         ");
            query.AppendLine("      ,wv_leak_restore wlr                                                                                                         ");
            query.AppendLine(" where wlr.loc_code = loc.loc_code                                                                                                 ");
            query.AppendLine("   and wlr.ree_ymd between :STARTDATE and :ENDDATE                                                                                 ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public void UpdateLeakageRepair(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("merge into wv_leak_restore wlr                                                                          ");
            query.AppendLine("using (select :LOC_CODE loc_code, :FTR_IDN ftr_idn, :MDFY_YN mdfy_yn from dual) a                        ");
            query.AppendLine("   on (wlr.loc_code = a.loc_code and wlr.ftr_idn = a.ftr_idn and wlr.mdfy_yn = a.mdfy_yn)                ");
            query.AppendLine(" when MATCHED then                                                                                       ");
            query.AppendLine("      update set lek_idn = :LEK_IDN                                                                      ");
            query.AppendLine("                ,cano = :CANO                                                                            ");
            query.AppendLine("                ,hjd_cde = :HJD_CDE                                                                      ");
            query.AppendLine("                ,lrs_ymd = :LRS_YMD                                                                      ");
            query.AppendLine("                ,lek_ymd = :LEK_YMD                                                                      ");
            query.AppendLine("                ,res_ymd = :RES_YMD                                                                      ");
            query.AppendLine("                ,ree_ymd = :REE_YMD                                                                      ");
            query.AppendLine("                ,lek_loc = :LEK_LOC                                                                      ");
            query.AppendLine("                ,pip_mop = :PIP_MOP                                                                      ");
            query.AppendLine("                ,pip_dip = :PIP_DIP                                                                      ");
            query.AppendLine("                ,lrs_cde = :LRS_CDE                                                                      ");
            query.AppendLine("                ,lep_cde = :LEP_CDE                                                                      ");
            query.AppendLine("                ,lek_exp = :LEK_EXP                                                                      ");
            query.AppendLine("                ,rep_exp = :REP_EXP                                                                      ");
            query.AppendLine("                ,mat_des = :MAT_DES                                                                      ");
            query.AppendLine("                ,rep_nam = :REP_NAM                                                                      ");
            query.AppendLine("                ,lek_cde = :LEK_CDE                                                                      ");
            query.AppendLine("                ,lek_vol = :LEK_VOL                                                                      ");
            query.AppendLine("                ,lrs_hms = :LRS_HMS                                                                      ");
            query.AppendLine("                ,lek_hms = :LEK_HMS                                                                      ");
            query.AppendLine("                ,res_hms = :RES_HMS                                                                      ");
            query.AppendLine("                ,ree_hms = :REE_HMS                                                                      ");
            query.AppendLine("                ,wtp_per = :WTP_PER                                                                      ");
            query.AppendLine(" when not MATCHED then                                                                                   ");
            query.AppendLine("      insert (loc_code, ftr_idn, mdfy_yn, lek_idn, cano, hjd_cde, lrs_ymd, lek_ymd, res_ymd,             ");
            query.AppendLine("              ree_ymd, lek_loc, pip_mop, pip_dip, lrs_cde, lep_cde, lek_exp, rep_exp, mat_des,           ");
            query.AppendLine("              rep_nam, lek_cde, lek_vol, lrs_hms, lek_hms, res_hms, ree_hms, wtp_per)                    ");
            query.AppendLine("      values (:LOC_CODE, :FTR_IDN, :MDFY_YN, :LEK_IDN, :CANO, :HJD_CDE, :LRS_YMD, :LEK_YMD, :RES_YMD,    ");
            query.AppendLine("              :REE_YMD, :LEK_LOC, :PIP_MOP, :PIP_DIP, :LRS_CDE, :LEP_CDE, :LEK_EXP, :REP_EXP, :MAT_DES,  ");
            query.AppendLine("              :REP_NAM, :LEK_CDE, :LEK_VOL, :LRS_HMS, :LEK_HMS, :RES_HMS, :REE_HMS, :WTP_PER)            ");

            IDataParameter[] parameters =  {
	                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
	                    ,new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
	                    ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)

                        ,new OracleParameter("LEK_IDN", OracleDbType.Varchar2)
                        ,new OracleParameter("CANO", OracleDbType.Varchar2)
                        ,new OracleParameter("HJD_CDE", OracleDbType.Varchar2)
                        ,new OracleParameter("LRS_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("RES_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("REE_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_LOC", OracleDbType.Varchar2)
                        ,new OracleParameter("PIP_MOP", OracleDbType.Varchar2)
                        ,new OracleParameter("PIP_DIP", OracleDbType.Varchar2)
                        ,new OracleParameter("LRS_CDE", OracleDbType.Varchar2)
                        ,new OracleParameter("LEP_CDE", OracleDbType.Varchar2)
	                    ,new OracleParameter("LEK_EXP", OracleDbType.Varchar2)
	                    ,new OracleParameter("REP_EXP", OracleDbType.Varchar2)
                        ,new OracleParameter("MAT_DES", OracleDbType.Varchar2)
                        ,new OracleParameter("REP_NAM", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_CDE", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_VOL", OracleDbType.Varchar2)
                        ,new OracleParameter("LRS_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("RES_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("REE_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("WTP_PER", OracleDbType.Varchar2)

	                    ,new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
	                    ,new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
	                    ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_IDN", OracleDbType.Varchar2)
                        ,new OracleParameter("CANO", OracleDbType.Varchar2)
                        ,new OracleParameter("HJD_CDE", OracleDbType.Varchar2)
                        ,new OracleParameter("LRS_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("RES_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("REE_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_LOC", OracleDbType.Varchar2)
                        ,new OracleParameter("PIP_MOP", OracleDbType.Varchar2)
                        ,new OracleParameter("PIP_DIP", OracleDbType.Varchar2)
                        ,new OracleParameter("LRS_CDE", OracleDbType.Varchar2)
                        ,new OracleParameter("LEP_CDE", OracleDbType.Varchar2)
	                    ,new OracleParameter("LEK_EXP", OracleDbType.Varchar2)
	                    ,new OracleParameter("REP_EXP", OracleDbType.Varchar2)
                        ,new OracleParameter("MAT_DES", OracleDbType.Varchar2)
                        ,new OracleParameter("REP_NAM", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_CDE", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_VOL", OracleDbType.Varchar2)
                        ,new OracleParameter("LRS_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("RES_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("REE_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("WTP_PER", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["FTR_IDN"];
            parameters[2].Value = parameter["MDFY_YN"];

            parameters[3].Value = parameter["LEK_IDN"];
            parameters[4].Value = parameter["CANO"];
            parameters[5].Value = parameter["HJD_CDE"];
            parameters[6].Value = parameter["LRS_YMD"].ToString().Replace("-", "");
            parameters[7].Value = parameter["LEK_YMD"].ToString().Replace("-", "");
            parameters[8].Value = parameter["RES_YMD"].ToString().Replace("-", "");
            parameters[9].Value = parameter["REE_YMD"].ToString().Replace("-", "");
            parameters[10].Value = parameter["LEK_LOC"];
            parameters[11].Value = parameter["PIP_MOP"];
            parameters[12].Value = parameter["PIP_DIP"];
            parameters[13].Value = parameter["LRS_CDE"];
            parameters[14].Value = parameter["LEP_CDE"];
            parameters[15].Value = parameter["LEK_EXP"];
            parameters[16].Value = parameter["REP_EXP"];
            parameters[17].Value = parameter["MAT_DES"];
            parameters[18].Value = parameter["REP_NAM"];
            parameters[19].Value = parameter["LEK_CDE"];
            parameters[20].Value = parameter["LEK_VOL"];
            parameters[21].Value = parameter["LRS_HMS"].ToString().Replace(":", "");
            parameters[22].Value = parameter["LEK_HMS"].ToString().Replace(":", "");
            parameters[23].Value = parameter["RES_HMS"].ToString().Replace(":", "");
            parameters[24].Value = parameter["REE_HMS"].ToString().Replace(":", "");
            parameters[25].Value = parameter["WTP_PER"];

            parameters[26].Value = parameter["LOC_CODE"];
            parameters[27].Value = parameter["FTR_IDN"];
            parameters[28].Value = parameter["MDFY_YN"];
            parameters[29].Value = parameter["LEK_IDN"];
            parameters[30].Value = parameter["CANO"];
            parameters[31].Value = parameter["HJD_CDE"];
            parameters[32].Value = parameter["LRS_YMD"].ToString().Replace("-", "");
            parameters[33].Value = parameter["LEK_YMD"].ToString().Replace("-", "");
            parameters[34].Value = parameter["RES_YMD"].ToString().Replace("-", "");
            parameters[35].Value = parameter["REE_YMD"].ToString().Replace("-", "");
            parameters[36].Value = parameter["LEK_LOC"];
            parameters[37].Value = parameter["PIP_MOP"];
            parameters[38].Value = parameter["PIP_DIP"];
            parameters[39].Value = parameter["LRS_CDE"];
            parameters[40].Value = parameter["LEP_CDE"];
            parameters[41].Value = parameter["LEK_EXP"];
            parameters[42].Value = parameter["REP_EXP"];
            parameters[43].Value = parameter["MAT_DES"];
            parameters[44].Value = parameter["REP_NAM"];
            parameters[45].Value = parameter["LEK_CDE"];
            parameters[46].Value = parameter["LEK_VOL"];
            parameters[47].Value = parameter["LRS_HMS"].ToString().Replace(":", "");
            parameters[48].Value = parameter["LEK_HMS"].ToString().Replace(":", "");
            parameters[49].Value = parameter["RES_HMS"].ToString().Replace(":", "");
            parameters[50].Value = parameter["REE_HMS"].ToString().Replace(":", "");
            parameters[51].Value = parameter["WTP_PER"];

            manager.ExecuteScript(query.ToString(), parameters);
        }


        public void UpdateLeakageRepairPoint(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("update wv_leak_restore a                                              ");
            query.AppendLine("   set a.x = :X														");
            query.AppendLine("      ,a.y = :Y														");
            query.AppendLine(" where a.ftr_idn = :FTR_IDN											");
            query.AppendLine("   and a.mdfy_yn = :MDFY_YN											");

            IDataParameter[] parameters =  {
	                     new OracleParameter("X", OracleDbType.Varchar2)
                        ,new OracleParameter("Y", OracleDbType.Varchar2)
                        ,new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                        ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["X"];
            parameters[1].Value = parameter["Y"];
            parameters[2].Value = parameter["FTR_IDN"];
            parameters[3].Value = parameter["MDFY_YN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }


        public void DeleteLeakageRepair(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete wv_leak_restore                                                 ");
            query.AppendLine(" where ftr_idn = :FTR_IDN                                              ");
            query.AppendLine("   and mdfy_yn = 'Y'                                                   ");

            IDataParameter[] parameters =  {
	                     new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
                    };

            parameters[0].Value = parameter["FTR_IDN"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //계량기 교체순번 조회
        public object GetLeakageRepairNo(OracleDBManager manager)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select wv_leak_restore_seq.nextval from dual");

            return manager.ExecuteScriptScalar(query.ToString(), null);
        }

        //누수민원조회
        public void SelectLeakageMinWon(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine(" select sgccd                                                                                                 ");
            query.AppendLine("       ,cano                                                                                                  ");
            query.AppendLine("       ,b.code_name                                                                                               ");
            //query.AppendLine("       ,to_date(caappldt,'yyyymmdd hh24miss') as caappldt                                                     ");
            query.AppendLine("       ,caappldt                                                                                              ");
            query.AppendLine("       ,canm                                                                                                  ");
            query.AppendLine("       ,caaddr                                                                                                ");
            //query.AppendLine("       ,catel                                                                                                 ");
            query.AppendLine("       ,replace(cacont,'개인정보 기입 금지' || chr(10),'') cacont                                                        ");
            query.AppendLine("       ,caprcsrslt                                                                                            ");
            query.AppendLine("   from wi_cainfo a, cm_code b                                                                                ");
            query.AppendLine("  where 1 = 1                                                                                                 ");
            query.AppendLine("    and calrgcd = '1900'                                                                                      ");
            query.AppendLine("    and b.pcode = '9004'                                                                                      ");
            query.AppendLine("    and a.camidcd = b.code                                                                                      ");
            query.AppendLine("    and caappldt between to_date(:STARTDATE||'000000','yyyymmddhh24miss') and to_date(:ENDDATE||'235959','yyyymmddhh24miss')                                        ");
            IDataParameter[] parameters =  {
                    new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["STARTDATE"];
            parameters[1].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }


        //누수탐사확인
        public void SelectLeakageTamSa(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                        ");
            query.AppendLine("(                                                                                                                                  ");
            query.AppendLine("select c1.loc_code                                                                                                                 ");
            query.AppendLine("      ,c1.sgccd                                                                                                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                            ");
            query.AppendLine("      ,c1.ftr_idn                                                                                                                  ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                 ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code        ");
            query.AppendLine("      ,c1.ord                                                                                                                      ");
            query.AppendLine("  from                                                                                                                             ");
            query.AppendLine("      (                                                                                                                            ");
            query.AppendLine("       select sgccd                                                                                                                ");
            query.AppendLine("             ,loc_code                                                                                                             ");
            query.AppendLine("             ,ploc_code                                                                                                            ");
            query.AppendLine("             ,loc_name                                                                                                             ");
            query.AppendLine("             ,ftr_idn                                                                                                              ");
            query.AppendLine("             ,ftr_code                                                                                                             ");
            query.AppendLine("             ,kt_gbn                                                                                                               ");
            query.AppendLine("             ,rownum ord                                                                                                           ");
            query.AppendLine("         from cm_location                                                                                                          ");
            query.AppendLine("        where 1 = 1                                                                                                                ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                            ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                      ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                  ");
            query.AppendLine("      ) c1                                                                                                                         ");
            query.AppendLine("       ,cm_location c2                                                                                                             ");
            query.AppendLine("       ,cm_location c3                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                       ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            query.AppendLine(" order by c1.ord                                                                                                                   ");
            query.AppendLine(")                                                                                                                                  ");
            query.AppendLine("select loc.loc_code                                                                                                                ");
            query.AppendLine("      ,loc.sgccd                                                                                                                   ");
            query.AppendLine("      ,loc.lblock                                                                                                                  ");
            query.AppendLine("      ,loc.mblock                                                                                                                  ");
            query.AppendLine("      ,loc.sblock                                                                                                                  ");
            query.AppendLine("      ,loc.ftr_idn sftr_idn                                                                                                        ");
            query.AppendLine("      ,wlr.ftr_idn                                                                                                                 ");
            query.AppendLine("      ,wlr.mdfy_yn                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_idn                                                                                                                 ");
            query.AppendLine("      ,wlr.cano	                                                                                                                 ");
            query.AppendLine("      ,wlr.hjd_cde                                                                                                                 ");
            query.AppendLine("      ,to_char(to_date(wlr.lrs_ymd, 'yyyymmdd'),'yyyy-mm-dd') lrs_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.lek_ymd, 'yyyymmdd'),'yyyy-mm-dd') lek_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.res_ymd, 'yyyymmdd'),'yyyy-mm-dd') res_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.ree_ymd, 'yyyymmdd'),'yyyy-mm-dd') ree_ymd                                                              ");
            query.AppendLine("      ,wlr.lek_loc                                                                                                                 ");
            query.AppendLine("      ,wlr.pip_mop                                                                                                                 ");
            query.AppendLine("      ,wlr.pip_dip                                                                                                                 ");
            query.AppendLine("      ,wlr.lrs_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lep_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_exp                                                                                                                 ");
            query.AppendLine("      ,wlr.rep_exp                                                                                                                 ");
            query.AppendLine("      ,wlr.mat_des                                                                                                                 ");
            query.AppendLine("      ,wlr.rep_nam                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_vol                                                                                                                 ");
            query.AppendLine("      ,to_date(wlr.lrs_hms, 'hh24mi') lrs_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.lek_hms, 'hh24mi') lek_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.res_hms, 'hh24mi') res_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.ree_hms, 'hh24mi') ree_hms                                                                                      ");
            query.AppendLine("      ,wlr.wtp_per                                                                                                                 ");
            query.AppendLine("      ,wlr.x                                                                                                                       ");
            query.AppendLine("      ,wlr.y                                                                                                                       ");
            query.AppendLine("  from loc                                                                                                                         ");
            query.AppendLine("      ,wv_leak_restore wlr                                                                                                         ");
            query.AppendLine(" where wlr.loc_code = loc.loc_code                                                                                                 ");
            query.AppendLine("   and wlr.lek_ymd between :STARTDATE and :ENDDATE                                                                                 ");
            query.AppendLine("   and wlr.lek_cde != 'BEG001'                                                                                                     ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //누수탐사복구
        public void SelectLeakageTamSaRepair(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                        ");
            query.AppendLine("(                                                                                                                                  ");
            query.AppendLine("select c1.loc_code                                                                                                                 ");
            query.AppendLine("      ,c1.sgccd                                                                                                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                            ");
            query.AppendLine("      ,c1.ftr_idn                                                                                                                  ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                 ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code        ");
            query.AppendLine("      ,c1.ord                                                                                                                      ");
            query.AppendLine("  from                                                                                                                             ");
            query.AppendLine("      (                                                                                                                            ");
            query.AppendLine("       select sgccd                                                                                                                ");
            query.AppendLine("             ,loc_code                                                                                                             ");
            query.AppendLine("             ,ploc_code                                                                                                            ");
            query.AppendLine("             ,loc_name                                                                                                             ");
            query.AppendLine("             ,ftr_idn                                                                                                              ");
            query.AppendLine("             ,ftr_code                                                                                                             ");
            query.AppendLine("             ,kt_gbn                                                                                                               ");
            query.AppendLine("             ,rownum ord                                                                                                           ");
            query.AppendLine("         from cm_location                                                                                                          ");
            query.AppendLine("        where 1 = 1                                                                                                                ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                            ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                      ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                  ");
            query.AppendLine("      ) c1                                                                                                                         ");
            query.AppendLine("       ,cm_location c2                                                                                                             ");
            query.AppendLine("       ,cm_location c3                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                       ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            query.AppendLine(" order by c1.ord                                                                                                                   ");
            query.AppendLine(")                                                                                                                                  ");
            query.AppendLine("select loc.loc_code                                                                                                                ");
            query.AppendLine("      ,loc.sgccd                                                                                                                   ");
            query.AppendLine("      ,loc.lblock                                                                                                                  ");
            query.AppendLine("      ,loc.mblock                                                                                                                  ");
            query.AppendLine("      ,loc.sblock                                                                                                                  ");
            query.AppendLine("      ,loc.ftr_idn sftr_idn                                                                                                        ");
            query.AppendLine("      ,wlr.ftr_idn                                                                                                                 ");
            query.AppendLine("      ,wlr.mdfy_yn                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_idn                                                                                                                 ");
            query.AppendLine("      ,wlr.cano	                                                                                                                 ");
            query.AppendLine("      ,wlr.hjd_cde                                                                                                                 ");
            query.AppendLine("      ,to_char(to_date(wlr.lrs_ymd, 'yyyymmdd'),'yyyy-mm-dd') lrs_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.lek_ymd, 'yyyymmdd'),'yyyy-mm-dd') lek_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.res_ymd, 'yyyymmdd'),'yyyy-mm-dd') res_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.ree_ymd, 'yyyymmdd'),'yyyy-mm-dd') ree_ymd                                                              ");
            query.AppendLine("      ,wlr.lek_loc                                                                                                                 ");
            query.AppendLine("      ,wlr.pip_mop                                                                                                                 ");
            query.AppendLine("      ,wlr.pip_dip                                                                                                                 ");
            query.AppendLine("      ,wlr.lrs_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lep_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_exp                                                                                                                 ");
            query.AppendLine("      ,wlr.rep_exp                                                                                                                 ");
            query.AppendLine("      ,wlr.mat_des                                                                                                                 ");
            query.AppendLine("      ,wlr.rep_nam                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_vol                                                                                                                 ");
            query.AppendLine("      ,to_date(wlr.lrs_hms, 'hh24mi') lrs_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.lek_hms, 'hh24mi') lek_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.res_hms, 'hh24mi') res_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.ree_hms, 'hh24mi') ree_hms                                                                                      ");
            query.AppendLine("      ,wlr.wtp_per                                                                                                                 ");
            query.AppendLine("      ,wlr.x                                                                                                                       ");
            query.AppendLine("      ,wlr.y                                                                                                                       ");
            query.AppendLine("  from loc                                                                                                                         ");
            query.AppendLine("      ,wv_leak_restore wlr                                                                                                         ");
            query.AppendLine(" where wlr.loc_code = loc.loc_code                                                                                                 ");
            query.AppendLine("   and wlr.ree_ymd between :STARTDATE and :ENDDATE                                                                                 ");
            query.AppendLine("   and wlr.lek_cde != 'BEG001'                                                                                                     ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //누수민원복구
        public void SelectLeakageMinWonRepair(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("with loc as                                                                                                                        ");
            query.AppendLine("(                                                                                                                                  ");
            query.AppendLine("select c1.loc_code                                                                                                                 ");
            query.AppendLine("      ,c1.sgccd                                                                                                                    ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))    ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock                                     ");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))           ");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock                                            ");
            query.AppendLine("      ,c1.ftr_idn                                                                                                                  ");
            query.AppendLine("      ,c1.ftr_code                                                                                                                 ");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code        ");
            query.AppendLine("      ,c1.ord                                                                                                                      ");
            query.AppendLine("  from                                                                                                                             ");
            query.AppendLine("      (                                                                                                                            ");
            query.AppendLine("       select sgccd                                                                                                                ");
            query.AppendLine("             ,loc_code                                                                                                             ");
            query.AppendLine("             ,ploc_code                                                                                                            ");
            query.AppendLine("             ,loc_name                                                                                                             ");
            query.AppendLine("             ,ftr_idn                                                                                                              ");
            query.AppendLine("             ,ftr_code                                                                                                             ");
            query.AppendLine("             ,kt_gbn                                                                                                               ");
            query.AppendLine("             ,rownum ord                                                                                                           ");
            query.AppendLine("         from cm_location                                                                                                          ");
            query.AppendLine("        where 1 = 1                                                                                                                ");
            query.AppendLine("        start with loc_code = :LOC_CODE                                                                                            ");
            query.AppendLine("        connect by prior loc_code = ploc_code                                                                                      ");
            query.AppendLine("        order SIBLINGS by ftr_idn                                                                                                  ");
            query.AppendLine("      ) c1                                                                                                                         ");
            query.AppendLine("       ,cm_location c2                                                                                                             ");
            query.AppendLine("       ,cm_location c3                                                                                                             ");
            query.AppendLine(" where 1 = 1                                                                                                                       ");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)                                                                                               ");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)                                                                                               ");
            query.AppendLine(" order by c1.ord                                                                                                                   ");
            query.AppendLine(")                                                                                                                                  ");
            query.AppendLine("select loc.loc_code                                                                                                                ");
            query.AppendLine("      ,loc.sgccd                                                                                                                   ");
            query.AppendLine("      ,loc.lblock                                                                                                                  ");
            query.AppendLine("      ,loc.mblock                                                                                                                  ");
            query.AppendLine("      ,loc.sblock                                                                                                                  ");
            query.AppendLine("      ,loc.ftr_idn sftr_idn                                                                                                        ");
            query.AppendLine("      ,wlr.ftr_idn                                                                                                                 ");
            query.AppendLine("      ,wlr.mdfy_yn                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_idn                                                                                                                 ");
            query.AppendLine("      ,wlr.cano	                                                                                                                 ");
            query.AppendLine("      ,wlr.hjd_cde                                                                                                                 ");
            query.AppendLine("      ,to_char(to_date(wlr.lrs_ymd, 'yyyymmdd'),'yyyy-mm-dd') lrs_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.lek_ymd, 'yyyymmdd'),'yyyy-mm-dd') lek_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.res_ymd, 'yyyymmdd'),'yyyy-mm-dd') res_ymd                                                              ");
            query.AppendLine("      ,to_char(to_date(wlr.ree_ymd, 'yyyymmdd'),'yyyy-mm-dd') ree_ymd                                                              ");
            query.AppendLine("      ,wlr.lek_loc                                                                                                                 ");
            query.AppendLine("      ,wlr.pip_mop                                                                                                                 ");
            query.AppendLine("      ,wlr.pip_dip                                                                                                                 ");
            query.AppendLine("      ,wlr.lrs_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lep_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_exp                                                                                                                 ");
            query.AppendLine("      ,wlr.rep_exp                                                                                                                 ");
            query.AppendLine("      ,wlr.mat_des                                                                                                                 ");
            query.AppendLine("      ,wlr.rep_nam                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_cde                                                                                                                 ");
            query.AppendLine("      ,wlr.lek_vol                                                                                                                 ");
            query.AppendLine("      ,to_date(wlr.lrs_hms, 'hh24mi') lrs_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.lek_hms, 'hh24mi') lek_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.res_hms, 'hh24mi') res_hms                                                                                      ");
            query.AppendLine("      ,to_date(wlr.ree_hms, 'hh24mi') ree_hms                                                                                      ");
            query.AppendLine("      ,wlr.wtp_per                                                                                                                 ");
            query.AppendLine("      ,wlr.x                                                                                                                       ");
            query.AppendLine("      ,wlr.y                                                                                                                       ");
            query.AppendLine("  from loc                                                                                                                         ");
            query.AppendLine("      ,wv_leak_restore wlr                                                                                                         ");
            query.AppendLine(" where wlr.loc_code = loc.loc_code                                                                                                 ");
            query.AppendLine("   and wlr.ree_ymd between :STARTDATE and :ENDDATE                                                                                 ");
            query.AppendLine("   and wlr.lek_cde = 'BEG001'                                                                                                     ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("STARTDATE", OracleDbType.Varchar2)
	                ,new OracleParameter("ENDDATE", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        public void UpdateExcelImport(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO WV_LEAK_RESTORE A ");
            query.AppendLine("USING ");
            query.AppendLine("( ");
            query.AppendLine("      SELECT :LOC_CODE LOC_CODE ");
            query.AppendLine("            ,:FTR_IDN FTR_IDN ");
            query.AppendLine("            ,:MDFY_YN MDFY_YN ");
            query.AppendLine("            ,:LEK_IDN LEK_IDN ");
            query.AppendLine("            ,:CANO CANO ");
            query.AppendLine("            ,:HJD_CDE HJD_CDE ");
            query.AppendLine("            ,:LRS_YMD LRS_YMD ");
            query.AppendLine("            ,:LEK_YMD LEK_YMD ");
            query.AppendLine("            ,:RES_YMD RES_YMD ");
            query.AppendLine("            ,:REE_YMD REE_YMD ");
            query.AppendLine("            ,:LEK_LOC LEK_LOC ");
            query.AppendLine("            ,:PIP_MOP PIP_MOP ");
            query.AppendLine("            ,:PIP_DIP PIP_DIP ");
            query.AppendLine("            ,:LRS_CDE LRS_CDE ");
            query.AppendLine("            ,:LEP_CDE LEP_CDE ");
            query.AppendLine("            ,:LEK_EXP LEK_EXP ");
            query.AppendLine("            ,:REP_EXP REP_EXP ");
            query.AppendLine("            ,:MAT_DES MAT_DES ");
            query.AppendLine("            ,:REP_NAM REP_NAM ");
            query.AppendLine("            ,:LEK_CDE LEK_CDE ");
            query.AppendLine("            ,:LEK_VOL LEK_VOL ");
            query.AppendLine("            ,:LRS_HMS LRS_HMS ");
            query.AppendLine("            ,:LEK_HMS LEK_HMS ");
            query.AppendLine("            ,:RES_HMS RES_HMS ");
            query.AppendLine("            ,:REE_HMS REE_HMS ");
            query.AppendLine("            ,:WTP_PER WTP_PER ");
            query.AppendLine("            ,:X X ");
            query.AppendLine("            ,:Y Y ");
            query.AppendLine("        FROM DUAL ");
            query.AppendLine(") B ");
            query.AppendLine("   ON ");
            query.AppendLine("( ");
            query.AppendLine("             A.LOC_CODE = B.LOC_CODE ");
            query.AppendLine("         AND A.FTR_IDN = B.FTR_IDN ");
            query.AppendLine("         AND A.MDFY_YN = B.MDFY_YN ");
            query.AppendLine(")  ");
            query.AppendLine("WHEN MATCHED THEN ");
            query.AppendLine("     UPDATE SET A.LEK_IDN = B.LEK_IDN ");
            query.AppendLine("               ,A.CANO = B.CANO ");
            query.AppendLine("               ,A.HJD_CDE = B.HJD_CDE ");
            query.AppendLine("               ,A.LRS_YMD = B.LRS_YMD ");
            query.AppendLine("               ,A.LEK_YMD = B.LEK_YMD ");
            query.AppendLine("               ,A.RES_YMD = B.RES_YMD ");
            query.AppendLine("               ,A.REE_YMD = B.REE_YMD ");
            query.AppendLine("               ,A.LEK_LOC = B.LEK_LOC ");
            query.AppendLine("               ,A.PIP_MOP = B.PIP_MOP ");
            query.AppendLine("               ,A.PIP_DIP = B.PIP_DIP ");
            query.AppendLine("               ,A.LRS_CDE = B.LRS_CDE ");
            query.AppendLine("               ,A.LEP_CDE = B.LEP_CDE ");
            query.AppendLine("               ,A.LEK_EXP = B.LEK_EXP ");
            query.AppendLine("               ,A.REP_EXP = B.REP_EXP ");
            query.AppendLine("               ,A.MAT_DES = B.MAT_DES ");
            query.AppendLine("               ,A.REP_NAM = B.REP_NAM ");
            query.AppendLine("               ,A.LEK_CDE = B.LEK_CDE ");
            query.AppendLine("               ,A.LEK_VOL = B.LEK_VOL ");
            query.AppendLine("               ,A.LRS_HMS = B.LRS_HMS ");
            query.AppendLine("               ,A.LEK_HMS = B.LEK_HMS ");
            query.AppendLine("               ,A.RES_HMS = B.RES_HMS ");
            query.AppendLine("               ,A.REE_HMS = B.REE_HMS ");
            query.AppendLine("               ,A.WTP_PER = B.WTP_PER ");
            query.AppendLine("               ,A.X = B.X ");
            query.AppendLine("               ,A.Y = B.Y ");
            query.AppendLine("WHEN NOT MATCHED THEN ");
            query.AppendLine("     INSERT (A.LOC_CODE, A.FTR_IDN, A.MDFY_YN, A.LEK_IDN, A.CANO, A.HJD_CDE, A.LRS_YMD, A.LEK_YMD, A.RES_YMD, A.REE_YMD, A.LEK_LOC, A.PIP_MOP, A.PIP_DIP, A.LRS_CDE, A.LEP_CDE, A.LEK_EXP, A.REP_EXP, A.MAT_DES, A.REP_NAM, A.LEK_CDE, A.LEK_VOL, A.LRS_HMS, A.LEK_HMS, A.RES_HMS, A.REE_HMS, A.WTP_PER, A.X, A.Y) ");
            query.AppendLine("     VALUES (B.LOC_CODE, B.FTR_IDN, B.MDFY_YN, B.LEK_IDN, B.CANO, B.HJD_CDE, B.LRS_YMD, B.LEK_YMD, B.RES_YMD, B.REE_YMD, B.LEK_LOC, B.PIP_MOP, B.PIP_DIP, B.LRS_CDE, B.LEP_CDE, B.LEK_EXP, B.REP_EXP, B.MAT_DES, B.REP_NAM, B.LEK_CDE, B.LEK_VOL, B.LRS_HMS, B.LEK_HMS, B.RES_HMS, B.REE_HMS, B.WTP_PER, B.X, B.Y) ");

            IDataParameter[] parameters =  {
	                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
	                    ,new OracleParameter("FTR_IDN", OracleDbType.Varchar2)
	                    ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)

                        ,new OracleParameter("LEK_IDN", OracleDbType.Varchar2)
                        ,new OracleParameter("CANO", OracleDbType.Varchar2)
                        ,new OracleParameter("HJD_CDE", OracleDbType.Varchar2)
                        ,new OracleParameter("LRS_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("RES_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("REE_YMD", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_LOC", OracleDbType.Varchar2)
                        ,new OracleParameter("PIP_MOP", OracleDbType.Varchar2)
                        ,new OracleParameter("PIP_DIP", OracleDbType.Varchar2)
                        ,new OracleParameter("LRS_CDE", OracleDbType.Varchar2)
                        ,new OracleParameter("LEP_CDE", OracleDbType.Varchar2)
	                    ,new OracleParameter("LEK_EXP", OracleDbType.Varchar2)
	                    ,new OracleParameter("REP_EXP", OracleDbType.Varchar2)
                        ,new OracleParameter("MAT_DES", OracleDbType.Varchar2)
                        ,new OracleParameter("REP_NAM", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_CDE", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_VOL", OracleDbType.Varchar2)
                        ,new OracleParameter("LRS_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("LEK_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("RES_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("REE_HMS", OracleDbType.Varchar2)
                        ,new OracleParameter("WTP_PER", OracleDbType.Varchar2)
                        ,new OracleParameter("X", OracleDbType.Varchar2)
                        ,new OracleParameter("Y", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["FTR_IDN"];
            parameters[2].Value = parameter["MDFY_YN"];

            parameters[3].Value = parameter["LEK_IDN"];
            parameters[4].Value = parameter["CANO"];
            parameters[5].Value = parameter["HJD_CDE"];
            parameters[6].Value = parameter["LRS_YMD"].ToString().Replace("-", "");
            parameters[7].Value = parameter["LEK_YMD"].ToString().Replace("-", "");
            parameters[8].Value = parameter["RES_YMD"].ToString().Replace("-", "");
            parameters[9].Value = parameter["REE_YMD"].ToString().Replace("-", "");
            parameters[10].Value = parameter["LEK_LOC"];
            parameters[11].Value = parameter["PIP_MOP"];
            parameters[12].Value = parameter["PIP_DIP"];
            parameters[13].Value = parameter["LRS_CDE"];
            parameters[14].Value = parameter["LEP_CDE"];
            parameters[15].Value = parameter["LEK_EXP"];
            parameters[16].Value = parameter["REP_EXP"];
            parameters[17].Value = parameter["MAT_DES"];
            parameters[18].Value = parameter["REP_NAM"];
            parameters[19].Value = parameter["LEK_CDE"];
            parameters[20].Value = parameter["LEK_VOL"];
            parameters[21].Value = parameter["LRS_HMS"].ToString().Replace(":", "");
            parameters[22].Value = parameter["LEK_HMS"].ToString().Replace(":", "");
            parameters[23].Value = parameter["RES_HMS"].ToString().Replace(":", "");
            parameters[24].Value = parameter["REE_HMS"].ToString().Replace(":", "");
            parameters[25].Value = parameter["WTP_PER"];

            parameters[26].Value = parameter["X"];
            parameters[27].Value = parameter["Y"];
            

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
