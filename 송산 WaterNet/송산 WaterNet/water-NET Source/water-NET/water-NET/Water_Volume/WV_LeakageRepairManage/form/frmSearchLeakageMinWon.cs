﻿using System;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.enum1;
using System.Collections;
using WaterNet.WV_LeakageRepairManage.work;
using EMFrame.log;

namespace WaterNet.WV_LeakageRepairManage.form
{
    public partial class frmSearchLeakageMinWon : Form
    {
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        public frmSearchLeakageMinWon()
        {
            InitializeComponent();
            Load += new EventHandler(frmSearchLeakageMinWon_Load);
        }

        private void frmSearchLeakageMinWon_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.IntervalType = WaterNet.WV_Common.enum1.INTERVAL_TYPE.DATE;
            this.searchBox1.LocationVisible = false;
            this.selectBtn.Enabled = false;
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
        }

        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("CAMIDCD"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("CAMIDCD");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, "2002");
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["CAMIDCD"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["CAMIDCD"];
        }

        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.selectBtn.Click += new EventHandler(selectBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);

            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid1.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.ultraGrid1, "누수민원정보");
                this.excelManager.Save("누수민원정보", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void selectBtn_Click(object sender, EventArgs e)
        {
            frmMain owner = this.Owner as frmMain;

            if (owner != null)
            {
                Hashtable parameter = null;

                if (this.ultraGrid1.Selected.Rows.Count == 0)
                {
                    return;
                }

                if (this.ultraGrid1.Selected.Rows.Count > 0)
                {
                    parameter = Utils.ConverToHashtable(this.ultraGrid1.Selected.Rows[0]);
                }

                owner.SetLeakageMinWon(parameter);
                
                this.Close();
            }
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectLeakageMinWon(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void SelectLeakageMinWon(Hashtable parameter)
        {
            this.ultraGrid1.DataSource = LeakageRepairManageWork.GetInstance().SelectLeakageMinWon(parameter).Tables[0];
        }

        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Selected = true;
            }
        }

        private void ultraGrid1_AfterSelectChange(object sender, Infragistics.Win.UltraWinGrid.AfterSelectChangeEventArgs e)
        {
            if (this.ultraGrid1.Selected.Rows.Count > 0)
            {
                this.selectBtn.Enabled = true;
            }
            else if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                this.selectBtn.Enabled = false;
            }
        }
    }
}
