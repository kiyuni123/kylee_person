﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.utils;
using Infragistics.Win;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.enum1;
using WaterNet.WaterNetCore;
using WaterNet.WV_LeakageRepairManage.work;
using System.Drawing;

namespace WaterNet.WV_LeakageRepairManage.form
{
    public partial class frmUpload : Form
    {

        private UltraGridManager gridManager = null;

        public frmUpload()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["누수지점관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.dataBtn.Enabled = false;
            }

            //===========================================================================

            //this.InitializeCode();
            //this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }



        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            FormManager.SetGridStyle_ColumeAllowEdit(this.ultraGrid1, 0, 24);

            //로우셀렉터 사용 안함
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LEK_CDE"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["PIP_MOP"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LRS_CDE"].CellClickAction = CellClickAction.CellSelect;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LEP_CDE"].CellClickAction = CellClickAction.CellSelect;
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            //
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LEK_CDE"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LEK_CDE");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2001");
            }

            //
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PIP_MOP"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PIP_MOP");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2014");
            }

            //
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LRS_CDE"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LRS_CDE");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2015");
            }

            //
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LEP_CDE"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LEP_CDE");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2016");
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LEK_CDE"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LEK_CDE"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["PIP_MOP"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["PIP_MOP"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LRS_CDE"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LRS_CDE"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LEP_CDE"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LEP_CDE"];
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            //파일선택
            this.fileBtn.Click += new EventHandler(fileBtn_Click);
            this.openFileDialog1.FileOk += new CancelEventHandler(openFileDialog1_FileOk);
            this.dataBtn.Click += new EventHandler(dataBtn_Click);
            this.closeBtn.Click += new EventHandler(closeBtn_Click);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.ultraGrid1.AfterCellUpdate += new CellEventHandler(ultraGrid1_AfterCellUpdate);
        }

        /// <summary>
        /// 파일선택 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileBtn_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
        }

        /// <summary>
        /// 파일을 선택한후에 이벤트 처리
        /// 1. 엑셀파일 여부 체크
        /// 2. 엑셀데이터를 데이터 테이블로 변환
        /// 3. 그리드 데이터 소스 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            int type = -1;

            try
            {
                type = LiAsExcelDB.ExcelFileType(this.openFileDialog1.FileName);
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
                return;
            }

            DataSet DS = null;

            // 0 : 97~2003,  1: 2007이상.
            if (type == 0 || type == 1)
            {
                DS = LiAsExcelDB.OpenExcel(this.openFileDialog1.FileName, true);
            }

            this.gridManager.ClearDataSource(this.ultraGrid1);

            ColumnsCollection columns = this.ultraGrid1.DisplayLayout.Bands[0].Columns;

            //unbound 컬럼 삭제
            for (int i = columns.Count - 1; i >= 0; i--)
            {
                if (!columns[i].IsBound)
                {
                    columns.RemoveAt(i);
                }
            }

            DataTable DT = DS.Tables[0];

            foreach (UltraGridColumn column in columns)
            {
                if (column.Index < DT.Columns.Count)
                {
                    DT.Columns[column.Index].ColumnName = column.Key;
                }
            }
            this.ultraGrid1.DataSource = DT;
        }

        /// <summary>
        /// 데이터이동 : 바로 DB에 데이터를 저장한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.Rows.Count == 0)
            {
                MessageBox.Show("등록된 데이터가 존재 하지 않습니다.");
                return;
            }

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                foreach (UltraGridCell gridCell in gridRow.Cells)
                {
                    if (gridCell.Appearance.BackColor == Color.Chocolate)
                    {
                        MessageBox.Show("오류 데이터가 존재합니다.\n확인후 계속 진행해 주세요.");
                        return;
                    }
                }
            }

            LeakageRepairManageWork.GetInstance().UpdateLeakageRepair(this.ultraGrid1.Rows);

            //foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            //{
            //    Hashtable parameter = Utils.ConverToHashtable(gridRow);
            //    parameter["MDFY_YN"] = "N";
            //    parameter["LOC_CODE"] = parameter["SBLOCK"];

            //    try
            //    {
            //        LeakageRepairManageWork.GetInstance().UpdateLeakageRepair(parameter);
            //    }
            //    catch (Exception e1)
            //    {
            //        MessageBox.Show(e1.Message);
            //        return;
            //    }
            //}
            //MessageBox.Show("정상적으로 처리되었습니다.");
            this.Close();
        }

        /// <summary>
        /// 창닫기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //파일을 불러왔을때 값체크 및 코드성 텍스트값을 코드값으로 변경한다..,,
        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ValueList SBLOCK = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
            ValueList LEK_CDE = this.ultraGrid1.DisplayLayout.ValueLists["LEK_CDE"];
            ValueList PIP_MOP = this.ultraGrid1.DisplayLayout.ValueLists["PIP_MOP"];
            ValueList LRS_CDE = this.ultraGrid1.DisplayLayout.ValueLists["LRS_CDE"];
            ValueList LEP_CDE = this.ultraGrid1.DisplayLayout.ValueLists["LEP_CDE"];

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                bool isSblock = false;
                bool isLekcde = false;
                bool isPipmop = false;
                bool isLrscde = false;
                bool isLepcde = false;

                foreach (ValueListItem item in SBLOCK.ValueListItems)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("SBLOCK") != -1)
                    {
                        if (gridRow.Cells["SBLOCK"].Value.ToString() == item.DisplayText)
                        {
                            gridRow.Cells["SBLOCK"].Value = item.DataValue;
                            isSblock = true;
                            break;
                        }
                    }
                }

                foreach (ValueListItem item in LEK_CDE.ValueListItems)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("LEK_CDE") != -1)
                    {
                        if (gridRow.Cells["LEK_CDE"].Value.ToString() == item.DisplayText)
                        {
                            gridRow.Cells["LEK_CDE"].Value = item.DataValue;
                            isLekcde = true;
                            break;
                        }
                    }
                }

                foreach (ValueListItem item in PIP_MOP.ValueListItems)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("PIP_MOP") != -1)
                    {
                        if (gridRow.Cells["PIP_MOP"].Value.ToString() == item.DisplayText)
                        {
                            gridRow.Cells["PIP_MOP"].Value = item.DataValue;
                            isPipmop = true;
                            break;
                        }
                    }
                }

                foreach (ValueListItem item in LRS_CDE.ValueListItems)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("LRS_CDE") != -1)
                    {
                        if (gridRow.Cells["LRS_CDE"].Value.ToString() == item.DisplayText)
                        {
                            gridRow.Cells["LRS_CDE"].Value = item.DataValue;
                            isLrscde = true;
                            break;
                        }
                    }
                }

                foreach (ValueListItem item in LEP_CDE.ValueListItems)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("LEP_CDE") != -1)
                    {
                        if (gridRow.Cells["LEP_CDE"].Value.ToString() == item.DisplayText)
                        {
                            gridRow.Cells["LEP_CDE"].Value = item.DataValue;
                            isLepcde = true;
                            break;
                        }
                    }
                }

                if (!isSblock)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("SBLOCK") != -1)
                    {
                        gridRow.Cells["SBLOCK"].Appearance.BackColor = Color.Chocolate;
                    }
                }

                if (!isLekcde)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("LEK_CDE") != -1)
                    {
                        gridRow.Cells["LEK_CDE"].Appearance.BackColor = Color.Chocolate;
                    }
                }

                if (!isPipmop)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("PIP_MOP") != -1)
                    {
                        gridRow.Cells["PIP_MOP"].Appearance.BackColor = Color.Chocolate;
                    }
                }

                if (!isLrscde)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("LRS_CDE") != -1)
                    {
                        gridRow.Cells["LRS_CDE"].Appearance.BackColor = Color.Chocolate;
                    }
                }

                if (!isLepcde)
                {
                    if (e.Layout.Bands[0].Columns.IndexOf("LEP_CDE") != -1)
                    {
                        gridRow.Cells["LEP_CDE"].Appearance.BackColor = Color.Chocolate;
                    }
                }
            }
        }

        private void ultraGrid1_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "SBLOCK" && e.Cell.Appearance.BackColor == Color.Chocolate)
            {
                e.Cell.Appearance.BackColor = Color.White;
            }
            else if (e.Cell.Column.Key == "LEK_CDE" && e.Cell.Appearance.BackColor == Color.Chocolate)
            {
                e.Cell.Appearance.BackColor = Color.White;
            }
            else if (e.Cell.Column.Key == "PIP_MOP" && e.Cell.Appearance.BackColor == Color.Chocolate)
            {
                e.Cell.Appearance.BackColor = Color.White;
            }
            else if (e.Cell.Column.Key == "LRS_CDE" && e.Cell.Appearance.BackColor == Color.Chocolate)
            {
                e.Cell.Appearance.BackColor = Color.White;
            }
            else if (e.Cell.Column.Key == "LEP_CDE" && e.Cell.Appearance.BackColor == Color.Chocolate)
            {
                e.Cell.Appearance.BackColor = Color.White;
            }
        }

        /// <summary>
        /// 기본설정 : 그리드의 빈값을 설정한다.
        /// </summary>
        private void UltraGridDefaultColumnsMapping(UltraGrid ultraGrid)
        {
            DataTable dataTable = new DataTable();

            foreach (UltraGridColumn gridColumn in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                dataTable.Columns.Add(gridColumn.Key, typeof(double));
            }

            DataRow dataRow = dataTable.NewRow();

            foreach (DataColumn dataColumn in dataTable.Columns)
            {
                dataRow[dataColumn.ColumnName] = DBNull.Value;
            }

            dataTable.Rows.Add(dataRow);

            ultraGrid.DataSource = dataTable;
        }
    }
}