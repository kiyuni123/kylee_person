﻿using System;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.form;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_Common.interface1;
using Infragistics.Win;
using WaterNet.WV_Common.util;
using System.Collections;
using WaterNet.WV_LeakageRepairManage.work;
using WaterNet.WV_Common.control;
using ESRI.ArcGIS.Geodatabase;
using WaterNet.WaterAOCore;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using EMFrame.log;
using WaterNet.WV_Common.excel;

namespace WaterNet.WV_LeakageRepairManage.form
{
    //insert row : 
    public partial class frmMain : Form, ExcelImport
    {
        private IMapControlWV mainMap = null;
        private UltraGridManager gridManager = null;
        private BlockComboboxControl detailblock = null;
        private ExcelManager excelManager = new ExcelManager();

        //누수지점선택여부
        private bool isLeakagePoint = false;

        //누수지점 레이어
        private ILayer pLayer = null;

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 데이터 확인용폼으로 사용될 경우
        /// </summary>
        public bool UsePlan
        {
            set
            {
                if (value == true)
                {
                    this.searchBox1.Visible = false;
                    this.panel1.Visible = false;
                    //this.searchBox1.InitializeParameter();

                    Hashtable parameter = this.searchBox1.Parameters;

                    if (parameter["GBN"].ToString() == "LEAKAGE_TAMSA")
                    {
                        this.SelectLeakageTamSa(this.searchBox1.Parameters);
                    }
                    if (parameter["GBN"].ToString() == "LEAKAGE_REPAIR")
                    {
                        this.SelectLeakageTamSaRepair(this.searchBox1.Parameters);
                    }
                    if (parameter["GBN"].ToString() == "LEAKAGE_MINWON")
                    {
                        this.SelectLeakageMinWonRepair(this.searchBox1.Parameters);
                    }
                }
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 수정가능여부 반환
        /// </summary>
        private bool DetailEnabled
        {
            get
            {
                bool enabled = false;

                UltraGridRow row = null;

                if (this.ultraGrid1.Selected.Rows.Count > 0)
                {
                    row = this.ultraGrid1.Selected.Rows[0];
                }

                if (row == null)
                {
                    enabled = true;
                }

                if (row != null)
                {
                    if (row.Cells["MDFY_YN"].Value.ToString() == "Y")
                    {
                        enabled = true;
                    }
                    else if (row.Cells["MDFY_YN"].Value.ToString() == "N")
                    {
                        enabled = false;
                    }
                }
                return enabled;
            }
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["누수지점관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.insertBtn.Enabled = false;
                this.updateBtn.Enabled = false;
                this.deleteBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 날짜, 콤보박스, 기타 제어객체를 설정한다.
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.Text = "검색조건";
            this.searchBox1.IntervalType = INTERVAL_TYPE.DATE;
            this.searchBox1.IntervalText = "검색기간";
            this.searchBox1.StartDateObject.Value = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
            this.searchBox1.EndDateObject.Value = DateTime.Now.ToString("yyyy-MM-dd");

            this.detailblock = new BlockComboboxControl(this.lblock, this.mblock, this.sblock);
            this.detailblock.AddDefaultOption(LOCATION_TYPE.All, true);

            //누수지점은 데이터가 있는 상태에서만 등록할수 있게해야됨.
            this.lekageBtn.Enabled = false;
        }

        /// <summary>
        /// 상세폼의 객체 활성/ 비활성 제어
        /// </summary>
        /// <param name="isEnabled"></param>
        private void SetEnabledDetailForm(bool isEnabled)
        {
            this.deleteBtn.Enabled = isEnabled;
            this.updateBtn.Enabled = isEnabled;
        }

        /// <summary>
        /// 그리드를 설정한다.
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            //로우셀렉터 설정
            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.ColumnChooserButton;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.Default;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            //대블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }

            //중블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }

            //소블록
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            //수정가능여부코드,
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MDFY_YN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MDFY_YN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2011");
            }

            //
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LEK_CDE"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LEK_CDE");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2001");
            }
            
            //
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("PIP_MOP"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("PIP_MOP");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2014");
            }

            //
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LRS_CDE"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LRS_CDE");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2015");
            }

            //
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LEP_CDE"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LEP_CDE");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2016");
            }
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MDFY_YN"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MDFY_YN"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LEK_CDE"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LEK_CDE"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["PIP_MOP"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["PIP_MOP"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LRS_CDE"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LRS_CDE"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LEP_CDE"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LEP_CDE"];

            Utils.SetValueList(this.lek_cde, VALUELIST_TYPE.CODE, "2001");
            Utils.SetValueList(this.pip_mop, VALUELIST_TYPE.CODE, "2014");
            Utils.SetValueList(this.lrs_cde, VALUELIST_TYPE.CODE, "2015");
            Utils.SetValueList(this.lep_cde, VALUELIST_TYPE.CODE, "2016");
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.importBtn.Click += new EventHandler(importBtn_Click);

            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.insertBtn.Click += new EventHandler(insertBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.deleteBtn.Click += new EventHandler(deleteBtn_Click);
            this.uploadBtn.Click += new EventHandler(uploadBtn_Click);
            this.leakageMinWonBtn.Click += new EventHandler(leakageMinWonBtn_Click);

            this.lekageBtn.Click += new EventHandler(lekageBtn_Click);

            //데이터 그리드 이벤트
            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);
            this.ultraGrid1.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid1_DoubleClickRow);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);

            this.Disposed += new EventHandler(frmMain_Disposed);
            this.mainMap.AxMap.OnMouseDown += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseDownEventHandler(AxMap_OnMouseDown);

        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            frmImport import = new frmImport();
            import.Items.Add(new ImportItem("LOC_CODE", "지역관리코드", typeof(string), true));
            import.Items.Add(new ImportItem("FTR_IDN", "관리번호", typeof(string), true));
            import.Items.Add(new ImportItem("MDFY_YN", "수정가능여부", typeof(string), true));
            import.Items.Add(new ImportItem("LEK_IDN", "누수적출관리번호", typeof(string), true));
            import.Items.Add(new ImportItem("CANO", "민원관리번호", typeof(string), false));
            import.Items.Add(new ImportItem("HJD_CDE", "행정읍면동", typeof(string), false));
            import.Items.Add(new ImportItem("LRS_YMD", "인지일자", typeof(string), false));
            import.Items.Add(new ImportItem("LEK_YMD", "현장확인일자", typeof(string), false));
            import.Items.Add(new ImportItem("RES_YMD", "복구시작일자", typeof(string), false));
            import.Items.Add(new ImportItem("REE_YMD", "복구종료일자", typeof(string), true));
            import.Items.Add(new ImportItem("LEK_LOC", "누수위치", typeof(string), false));
            import.Items.Add(new ImportItem("PIP_MOP", "관재질", typeof(string), false));
            import.Items.Add(new ImportItem("PIP_DIP", "관구경", typeof(string), false));
            import.Items.Add(new ImportItem("LRS_CDE", "누수원인", typeof(string), false));
            import.Items.Add(new ImportItem("LEP_CDE", "누수부위", typeof(string), false));
            import.Items.Add(new ImportItem("LEK_EXP", "누수현황", typeof(string), false));
            import.Items.Add(new ImportItem("REP_EXP", "누수복구내용", typeof(string), false));
            import.Items.Add(new ImportItem("MAT_DES", "소요자재내역", typeof(string), false));
            import.Items.Add(new ImportItem("REP_NAM", "누수복구자명", typeof(string), false));
            import.Items.Add(new ImportItem("LEK_CDE", "누수확인", typeof(string), false));
            import.Items.Add(new ImportItem("LEK_VOL", "누수량", typeof(string), false));
            import.Items.Add(new ImportItem("LRS_HMS", "인지시간", typeof(string), false));
            import.Items.Add(new ImportItem("LEK_HMS", "현장확인시간", typeof(string), false));
            import.Items.Add(new ImportItem("RES_HMS", "복구시작시간", typeof(string), false));
            import.Items.Add(new ImportItem("REE_HMS", "복구완료시간", typeof(string), false));
            import.Items.Add(new ImportItem("WTP_PER", "수압", typeof(string), false));
            import.Items.Add(new ImportItem("X", "GIS X좌표", typeof(string), false));
            import.Items.Add(new ImportItem("Y", "GIS Y좌표", typeof(string), false));
            import.UpdateExcel = this;
            import.ShowDialog();
        }

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //누수복구지점 조회
            this.SelectLeakageRepairPoint();
        }

        /// <summary>
        /// 엑셀버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.ultraGrid1, "누수복구관리");
                this.excelManager.Save("누수복구관리", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 검색버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectLeakageRepair(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 추가버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void insertBtn_Click(object sender, EventArgs e)
        {
            this.InsertLeakageRepair();
        }

        /// <summary>
        /// 저장버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            this.UpdateLeakageRepair();
        }

        /// <summary>
        /// 삭제버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                return;
            }

            DialogResult qe = MessageBox.Show("삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (qe == DialogResult.Yes)
            {
                this.DeleteLeakageRepair();
            }
        }

        /// <summary>
        /// 데이터업로드버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uploadBtn_Click(object sender, EventArgs e)
        {
            frmUpload form = new frmUpload();
            form.Owner = this;
            form.ShowDialog(this);
        }

        //누수민원 찾기
        private void leakageMinWonBtn_Click(object sender, EventArgs e)
        {
            frmSearchLeakageMinWon form = new frmSearchLeakageMinWon();
            form.Owner = this;
            form.ShowDialog();
        }

        /// <summary>
        /// 누수지점등록버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lekageBtn_Click(object sender, EventArgs e)
        {
            //DB에서 FTR_IDN을 조회한다.
            if (this.ftr_idn.Text == string.Empty)
            {
                DialogResult qe = MessageBox.Show("누수복구 관리번호가 존재하지 않습니다.\n 관리번호를 생성하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (qe == DialogResult.Yes)
                {
                    this.ftr_idn.Text = LeakageRepairManageWork.GetInstance().GetLeakageRepairNo().ToString();
                }
                else if (qe == DialogResult.No)
                {
                    MessageBox.Show("관리번호가 존재하지않으면 지점을 등록할수 없습니다.");
                    return;
                }
            }

            this.isLeakagePoint = true;
            this.mainMap.ToolActionCommand.Checked = true;
            this.mainMap.IMapControl3.CurrentTool = null;
            MessageBox.Show("맵을 클릭해서 누수지점을 등록해 주세요.\n누수지점은 상수관로 또는 급수관로를 선택하셔야 합니다.");
        }

        /// <summary>
        /// 활성화된 로우에 클릭 이벤트를 준다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Selected = true;
            }
        }

        /// <summary>
        /// 검색된 지점을 선택 상태로 만든다.
        /// </summary>
        private void SelectedWaterMeter()
        {
            //레이어를 가져온다.
            ILayer pLayer = this.getLayerInstance();

            //레이어가 없으면 넘어감,
            if (pLayer == null)
            {
                return;
            }

            IFeatureSelection selection = (IFeatureSelection)pLayer;
            selection.Clear();

            //검색내용이 없으면 넘어감,
            if (this.ultraGrid1.Rows.Count == 0)
            {
                this.mainMap.IMapControl3.ActiveView.Refresh();
                return;
            }

            foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
            {
                if (!gridRow.Hidden)
                {
                    string strWhere = "FTR_IDN = '" + gridRow.Cells["FTR_IDN"].Value.ToString() + "' AND MDFY_YN = '" + gridRow.Cells["FTR_IDN"].Value.ToString() + "'";
                    IFeature feature = ArcManager.GetFeature(pLayer, strWhere);
                    if (feature != null)
                    {
                        selection.Add(feature);
                    }
                }
            }
            this.mainMap.IMapControl3.MapScale = 1000;
            this.mainMap.IMapControl3.ActiveView.Refresh();
        }

        /// <summary>
        /// 그리드 로우 선택 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            this.SetEnabledDetailForm(this.DetailEnabled);
            this.detailblock.StopEvent = true;
            this.SelectLeakageRepairDetail();
        }

        private void ultraGrid1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            //레이어를 가져온다.
            ILayer pLayer = this.getLayerInstance();

            IFeatureSelection selection = (IFeatureSelection)pLayer;
            selection.Clear();

            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                this.mainMap.IMapControl3.ActiveView.Refresh();
                return;
            }

            UltraGridRow gridRow = this.ultraGrid1.Selected.Rows[0];

            string strWhere = "FTR_IDN = '" + gridRow.Cells["FTR_IDN"].Value.ToString() + "' AND MDFY_YN = '" + gridRow.Cells["MDFY_YN"].Value.ToString() + "'";

            IFeature feature = ArcManager.GetFeature(pLayer, strWhere);

            if (feature != null)
            {
                selection.Add(feature);
                this.mainMap.IMapControl3.MapScale = 1000;
                ArcManager.MoveCenterAt(this.mainMap.IMapControl3, feature.Shape);
            }
            else if (feature == null)
            {
                selection.Clear();
                this.mainMap.IMapControl3.ActiveView.Refresh();

                Hashtable parameter = new Hashtable();
                parameter["FTR_IDN"] = gridRow.Cells["SFTR_IDN"].Value;
                parameter["FTR_CODE"] = "BZ003";

                this.mainMap.MoveToBlock(parameter);

                MessageBox.Show("누수지점이 등록되지 않았습니다.\n누수지점등록 버튼을 클릭해서 지점을 등록해주세요.");
            }

        }

        /// <summary>
        /// 누구복구지점 검색
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectLeakageRepair(Hashtable parameter)
        {
            this.mainMap.MoveToBlock(parameter);
            this.ultraGrid1.DataSource = LeakageRepairManageWork.GetInstance().SelectLeakageRepair(parameter).Tables[0];
        }

        private void SelectLeakageTamSa(Hashtable parameter)
        {
            this.mainMap.MoveToBlock(parameter);
            this.ultraGrid1.DataSource = LeakageRepairManageWork.GetInstance().SelectLeakageTamSa(parameter).Tables[0];
        }

        private void SelectLeakageTamSaRepair(Hashtable parameter)
        {
            this.mainMap.MoveToBlock(parameter);
            this.ultraGrid1.DataSource = LeakageRepairManageWork.GetInstance().SelectLeakageTamSaRepair(parameter).Tables[0];
        }

        private void SelectLeakageMinWonRepair(Hashtable parameter)
        {
            this.mainMap.MoveToBlock(parameter);
            this.ultraGrid1.DataSource = LeakageRepairManageWork.GetInstance().SelectLeakageMinWonRepair(parameter).Tables[0];
        }

        private void SelectLeakageRepairPoint()
        {
            if (this.ultraGrid1.Rows.Count == 0)
            {
                return;
            }

            ILayer layer = ArcManager.GetMapLayer(this.mainMap.IMapControl3.Map, "누수지점");
            ArcManager.DeleteAllFeatures(layer);
            IWorkspaceEdit pWorkspaceEdit = ArcManager.getWorkspaceEdit(layer);

            if (pWorkspaceEdit == null)
            {
                return;
            }

            try
            {
                pWorkspaceEdit.StartEditing(true);
                pWorkspaceEdit.StartEditOperation();

                using (ESRI.ArcGIS.ADF.ComReleaser comReleaser = new ESRI.ArcGIS.ADF.ComReleaser())
                {
                    IFeatureBuffer pFeatureBuffer = ((IFeatureLayer)layer).FeatureClass.CreateFeatureBuffer();
                    IFeatureCursor pFeatureCursor = ((IFeatureLayer)layer).FeatureClass.Insert(true);
                    IFeature pFeaturebuffer = pFeatureBuffer as IFeature;
                    comReleaser.ManageLifetime(pFeatureCursor);

                    foreach (UltraGridRow item in this.ultraGrid1.Rows)
                    {
                        if (item.Cells["X"].Value != DBNull.Value && item.Cells["Y"].Value != DBNull.Value)
                        {
                            IPoint point = new PointClass();
                            point.PutCoords(Convert.ToDouble(item.Cells["X"].Value), Convert.ToDouble(item.Cells["Y"].Value));
                            pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("FTR_IDN"), item.Cells["FTR_IDN"].Value);
                            pFeaturebuffer.set_Value(pFeaturebuffer.Fields.FindField("MDFY_YN"), item.Cells["MDFY_YN"].Value);
                            pFeaturebuffer.Shape = point as IGeometry;

                            pFeatureCursor.InsertFeature(pFeatureBuffer);
                        }
                    }

                    pFeatureCursor.Flush();
                }

                pWorkspaceEdit.StopEditOperation();
                pWorkspaceEdit.StopEditing(true);
            }
            catch (Exception ex)
            {
                pWorkspaceEdit.AbortEditOperation();
                pWorkspaceEdit.StopEditing(false);

                throw ex;
            }
        }

        /// <summary>
        /// 누구복구지점 상세 검색
        /// </summary>
        /// <param name="parameter"></param>
        private void SelectLeakageRepairDetail()
        {
            Utils.ClearControls(this.groupBox2);

            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                return;
            }


            //누수지점은 데이터가 있는 상태에서만 등록할수 있게해야됨.
            this.lekageBtn.Enabled = true;

            UltraGridRow row = this.ultraGrid1.Selected.Rows[0];
            Hashtable parameter = Utils.ConverToHashtable(row);

            Utils.SetControlDataSetting(parameter, this.groupBox2);

            if (row.Cells["MDFY_YN"].Value.ToString() == "Y")
            {
                this.detailblock.StopEvent = false;
            }
            else if (row.Cells["MDFY_YN"].Value.ToString() == "N")
            {
                this.detailblock.StopEvent = true;
            }
        }

        /// <summary>
        /// 누수복구지점 저장 : 추가
        /// </summary>
        private void InsertLeakageRepair()
        {
            this.ultraGrid1.Selected.Rows.Clear();
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Activated = false;
            }

            this.lekageBtn.Enabled = false;

            Utils.ClearControls(this.groupBox2);
            this.SetEnabledDetailForm(this.DetailEnabled);
            this.detailblock.StopEvent = false;

            if (this.ultraGrid1.Rows.Count > 0)
            {
                this.ultraGrid1.ActiveRowScrollRegion.ScrollRowIntoView(this.ultraGrid1.Rows[0]);
            }

            //DB에서 FTR_IDN을 조회한다.
            if (this.ftr_idn.Text == string.Empty)
            {
                this.ftr_idn.Text = LeakageRepairManageWork.GetInstance().GetLeakageRepairNo().ToString();
            }
        }

        /// <summary>
        /// 누수복구지점 저장 : 수정 및 저장
        /// </summary>
        private void UpdateLeakageRepair()
        {
            if (this.sblock.SelectedIndex == 0)
            {
                MessageBox.Show("소블록을 선택해야 합니다.");
                return;
            }

            if (this.ftr_idn.Text == string.Empty)
            {
                MessageBox.Show("누수복구 관리번호가 존재하지 않습니다.");
                return;
            }

            Hashtable parameter = Utils.ConverToHashtable(this.groupBox2);

            if (!parameter.ContainsKey("LOC_CODE"))
            {
                parameter["LOC_CODE"] = parameter["SBLOCK"];
            }

            if (!parameter.ContainsKey("MDFY_YN"))
            {
                parameter["MDFY_YN"] = "Y";
            }

            LeakageRepairManageWork.GetInstance().UpdateLeakageRepair(parameter);
            this.SelectLeakageRepair(this.searchBox1.Parameters);
        }

        /// <summary>
        /// 누수복구지점삭제
        /// </summary>
        private void DeleteLeakageRepair()
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                return;
            }

            UltraGridRow gridRow = this.ultraGrid1.Selected.Rows[0];
            Hashtable parameter = Utils.ConverToHashtable(gridRow);
            LeakageRepairManageWork.GetInstance().DeleteLeakageRepair(parameter);
            this.SelectLeakageRepair(this.searchBox1.Parameters);
        }

        /// <summary>
        /// 창이 닫혔을때 이벤트 삭제.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Disposed(object sender, EventArgs e)
        {
            this.mainMap.AxMap.OnMouseDown -= AxMap_OnMouseDown;
        }

        /// <summary>
        /// 맵을 클릭했을때 지점 선택관련.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AxMap_OnMouseDown(object sender, ESRI.ArcGIS.Controls.IMapControlEvents2_OnMouseDownEvent e)
        {
            //ftr_idn은 입력이 불가이다, (추가 버튼을 눌렀을때 DB조회후에 FTR_IDN을 가져와야 한다.)
            //mdfy_yn은 선택된 로우가 있다면 선택로우의 값을 사용하고, 없다면 Y 값을 사용한다.

            string ftr_idn = this.ftr_idn.Text;
            string mdfy_yn = string.Empty;

            if (this.ultraGrid1.Selected.Rows.Count > 0)
            {
                mdfy_yn = this.ultraGrid1.Selected.Rows[0].Cells["MDFY_YN"].Value.ToString();
            }
            else if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                mdfy_yn = "Y";
            }

            if (e.button == 1 && this.mainMap.ToolActionCommand.Checked && this.mainMap.IMapControl3.CurrentTool == null && this.isLeakagePoint)
            {
                ILayer pLayer = this.getLayerInstance();

                ESRI.ArcGIS.Geometry.IPoint pPoint = new ESRI.ArcGIS.Geometry.PointClass();
                pPoint.X = e.mapX;
                pPoint.Y = e.mapY;
                pPoint.Z = 0;

                //누수지점을 등록하기전에 관로와 선택한 지점이 중복되는지 확인해야한다.
                ILayer pipeLayer = ArcManager.GetMapLayer(this.mainMap.IMapControl3, "상수관로");
                ILayer splyLayer = ArcManager.GetMapLayer(this.mainMap.IMapControl3, "급수관로");


                if (pipeLayer == null)
                {
                    MessageBox.Show("상수관로 레이어가 존재하지 않습니다.\n작업을 중단합니다.");
                    return;
                }

                if (splyLayer == null)
                {
                    MessageBox.Show("급수관로 레이어가 존재하지 않습니다.\n작업을 중단합니다.");
                    return;
                }

                IFeatureCursor pipeCursor =
                    ArcManager.GetSpatialCursor(pipeLayer, pPoint, 0.5, esriSpatialRelEnum.esriSpatialRelIntersects, null);

                IFeature pipeFeature = pipeCursor.NextFeature();

                IFeatureCursor splyCursor =
                    ArcManager.GetSpatialCursor(pipeLayer, pPoint, 0.5, esriSpatialRelEnum.esriSpatialRelIntersects, null);

                IFeature splyFeature = splyCursor.NextFeature();

                if (pipeFeature == null && splyFeature == null)
                {
                    MessageBox.Show("누수지점은 상수관로 또는 급수관로를 선택하셔야 합니다.");
                    return;
                }

                string strWhere = "FTR_IDN = '" + ftr_idn + "' AND MDFY_YN = '" + mdfy_yn + "'";

                //기존 등록지점 존재 유무 확인
                bool hasLeakagePoint = false;
                IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
                IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, strWhere) as IFeatureCursor;
                IFeature pFeature = pCursor.NextFeature();
                if (pFeature != null)
                {
                    hasLeakagePoint = true;
                }

                Hashtable parameter = new Hashtable();
                parameter["FTR_IDN"] = ftr_idn;
                parameter["MDFY_YN"] = mdfy_yn;
                parameter["X"] = e.mapX;
                parameter["Y"] = e.mapY;

                //기존 선택 지점이 존재 한다면 기존 지점을 삭제하고 다시 등록할건지 확인함.,
                if (hasLeakagePoint)
                {
                    DialogResult qe = MessageBox.Show("기존에 등록된 누수지점이 존재합니다.\n누수지점을 다시 등록하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (qe == DialogResult.No)
                    {
                        this.isLeakagePoint = false;
                        return;
                    }

                    this.RemovePointOnLayer(ftr_idn, mdfy_yn);
                }

                this.UpdateLeakageRepairPoint(parameter);
                this.isLeakagePoint = false;
            }
        }

        private void UpdateLeakageRepairPoint(Hashtable parameter)
        {
            LeakageRepairManageWork.GetInstance().UpdateLeakageRepairPoint(parameter);
            this.SelectLeakageRepair(this.searchBox1.InitializeParameter().Parameters);
        }

        /// <summary>
        /// Layer에 Point를 저장한다.
        /// </summary>
        private void SaveLeakagePointToLayer(ESRI.ArcGIS.Geometry.IGeometry oIGeometry, string strFTR_IDN, string strMDFY_YN)
        {
            ILayer pLayer = this.getLayerInstance();

            IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
            IFeature pFeature = pFeatureClass.CreateFeature();

            //Shpae에 지점 등록
            pFeature.Shape = oIGeometry;

            //등록된 지점에 밸류 등록
            ArcManager.SetValue(pFeature, "FTR_IDN", (object)strFTR_IDN);
            ArcManager.SetValue(pFeature, "MDFY_YN", (object)strMDFY_YN);

            //Shape 저장
            pFeature.Store();

            //GIS Map Refresh
            ArcManager.PartialRefresh(this.mainMap.IMapControl3.Map, esriViewDrawPhase.esriViewGeography);
        }

        /// <summary>
        /// Layer에서 Point를 Remove한다.
        /// </summary>
        private void RemovePointOnLayer(string strFTR_IDN, string strMDFY_YN)
        {
            ILayer pLayer = this.getLayerInstance();

            string strWhere = "FTR_IDN = '" + strFTR_IDN + "' AND MDFY_YN = '" + strMDFY_YN + "'";

            IFeatureClass pFeatureClass = ((IFeatureLayer)pLayer).FeatureClass;
            IFeatureCursor pCursor = WaterAOCore.ArcManager.GetCursor(pFeatureClass, strWhere) as IFeatureCursor;

            IFeature pFeature = pCursor.NextFeature();

            while (pFeature != null)
            {
                pFeature.Delete();
                pFeature = pCursor.NextFeature();
            }

            this.BringToFront();
        }

        /// <summary>
        /// 누수지점 레이어를 반환
        /// </summary>
        /// <returns></returns>
        private ILayer getLayerInstance()
        {
            if (this.pLayer == null)
            {
                this.pLayer = ArcManager.GetMapLayer(this.mainMap.IMapControl3, "누수지점");
            }
            return this.pLayer;
        }

        /// <summary>
        /// 민원번호 설정
        /// </summary>
        /// <param name="parameter"></param>
        public void SetLeakageMinWon(Hashtable parameter)
        {
            this.cano.Text = parameter["CANO"].ToString();
        }

        #region ExcelImport 멤버

        public void update(System.Data.DataTable data)
        {
            LeakageRepairManageWork.GetInstance().UpdateExcelImport(data);
        }

        #endregion
    }
}
