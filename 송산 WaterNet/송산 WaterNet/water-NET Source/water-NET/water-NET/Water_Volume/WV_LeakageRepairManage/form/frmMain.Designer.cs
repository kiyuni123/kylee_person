﻿namespace WaterNet.WV_LeakageRepairManage.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn34 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SGCCD");
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn35 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn36 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn37 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn38 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MDFY_YN");
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn39 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FTR_IDN");
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn40 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEK_IDN");
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn41 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CANO");
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn42 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HJD_CDE");
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn43 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LRS_YMD");
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn44 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEK_YMD");
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn45 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RES_YMD");
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn46 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("REE_YMD");
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn47 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEK_LOC");
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn48 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIP_MOP");
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn49 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIP_DIP");
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn50 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LRS_CDE");
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn51 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEP_CDE");
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn52 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEK_EXP");
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn53 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("REP_EXP");
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn54 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MAT_DES");
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn55 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("REP_NAM");
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn56 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEK_CDE");
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn57 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEK_VOL");
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn58 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LRS_HMS");
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn59 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEK_HMS");
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn60 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("RES_HMS");
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn61 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("REE_HMS");
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn62 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("WTP_PER");
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn63 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("X");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn64 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Y");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SFTR_IDN");
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uploadBtn = new System.Windows.Forms.Button();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.insertBtn = new System.Windows.Forms.Button();
            this.updateBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lekageBtn = new System.Windows.Forms.Button();
            this.lek_loc = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.pip_dip = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lek_hms = new System.Windows.Forms.TextBox();
            this.ree_hms = new System.Windows.Forms.TextBox();
            this.res_hms = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lrs_hms = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.wtp_per = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.lek_vol = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.lek_cde = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rep_nam = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.mat_des = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.rep_exp = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.lek_exp = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lep_cde = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lrs_cde = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.pip_mop = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ree_ymd = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label11 = new System.Windows.Forms.Label();
            this.res_ymd = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label10 = new System.Windows.Forms.Label();
            this.lek_ymd = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label9 = new System.Windows.Forms.Label();
            this.lrs_ymd = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label5 = new System.Windows.Forms.Label();
            this.leakageMinWonBtn = new System.Windows.Forms.Button();
            this.cano = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lek_idn = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.sblock = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mblock = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblock = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.ftr_idn = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            this.importBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ree_ymd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.res_ymd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lek_ymd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lrs_ymd)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Location = new System.Drawing.Point(1062, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 548);
            this.pictureBox4.TabIndex = 21;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 548);
            this.pictureBox3.TabIndex = 20;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 558);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1072, 10);
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1072, 10);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.importBtn);
            this.panel1.Controls.Add(this.uploadBtn);
            this.panel1.Controls.Add(this.deleteBtn);
            this.panel1.Controls.Add(this.insertBtn);
            this.panel1.Controls.Add(this.updateBtn);
            this.panel1.Controls.Add(this.searchBtn);
            this.panel1.Controls.Add(this.excelBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 121);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1052, 30);
            this.panel1.TabIndex = 48;
            // 
            // uploadBtn
            // 
            this.uploadBtn.Location = new System.Drawing.Point(663, 3);
            this.uploadBtn.Name = "uploadBtn";
            this.uploadBtn.Size = new System.Drawing.Size(90, 23);
            this.uploadBtn.TabIndex = 52;
            this.uploadBtn.Text = "데이터업로드";
            this.uploadBtn.UseVisualStyleBackColor = true;
            this.uploadBtn.Visible = false;
            // 
            // deleteBtn
            // 
            this.deleteBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.deleteBtn.AutoSize = true;
            this.deleteBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.deleteBtn.Location = new System.Drawing.Point(966, 2);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(40, 25);
            this.deleteBtn.TabIndex = 51;
            this.deleteBtn.TabStop = false;
            this.deleteBtn.Text = "삭제";
            this.deleteBtn.UseVisualStyleBackColor = true;
            // 
            // insertBtn
            // 
            this.insertBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.insertBtn.AutoSize = true;
            this.insertBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.insertBtn.Location = new System.Drawing.Point(874, 2);
            this.insertBtn.Name = "insertBtn";
            this.insertBtn.Size = new System.Drawing.Size(40, 25);
            this.insertBtn.TabIndex = 50;
            this.insertBtn.TabStop = false;
            this.insertBtn.Text = "추가";
            this.insertBtn.UseVisualStyleBackColor = true;
            // 
            // updateBtn
            // 
            this.updateBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.updateBtn.AutoSize = true;
            this.updateBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.updateBtn.Location = new System.Drawing.Point(920, 2);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(40, 25);
            this.updateBtn.TabIndex = 48;
            this.updateBtn.TabStop = false;
            this.updateBtn.Text = "저장";
            this.updateBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(1012, 2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 30;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(828, 2);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 31;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ultraGrid1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 151);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1052, 407);
            this.tableLayoutPanel1.TabIndex = 51;
            // 
            // ultraGrid1
            // 
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance32;
            appearance33.TextVAlignAsString = "Middle";
            ultraGridColumn7.CellAppearance = appearance33;
            ultraGridColumn7.Header.Caption = "지역관리코드";
            ultraGridColumn7.Header.VisiblePosition = 0;
            ultraGridColumn7.Hidden = true;
            appearance34.TextVAlignAsString = "Middle";
            ultraGridColumn34.CellAppearance = appearance34;
            ultraGridColumn34.Header.Caption = "센터";
            ultraGridColumn34.Header.VisiblePosition = 1;
            ultraGridColumn34.Hidden = true;
            appearance35.TextVAlignAsString = "Middle";
            ultraGridColumn35.CellAppearance = appearance35;
            ultraGridColumn35.Header.Caption = "대블록";
            ultraGridColumn35.Header.VisiblePosition = 2;
            ultraGridColumn35.Width = 60;
            appearance36.TextVAlignAsString = "Middle";
            ultraGridColumn36.CellAppearance = appearance36;
            ultraGridColumn36.Header.Caption = "중블록";
            ultraGridColumn36.Header.VisiblePosition = 3;
            ultraGridColumn36.Width = 60;
            appearance37.TextVAlignAsString = "Middle";
            ultraGridColumn37.CellAppearance = appearance37;
            ultraGridColumn37.Header.Caption = "소블록";
            ultraGridColumn37.Header.VisiblePosition = 4;
            ultraGridColumn37.Width = 60;
            appearance38.TextHAlignAsString = "Center";
            appearance38.TextVAlignAsString = "Middle";
            ultraGridColumn38.CellAppearance = appearance38;
            ultraGridColumn38.Header.Caption = "수정가능여부";
            ultraGridColumn38.Header.VisiblePosition = 6;
            ultraGridColumn38.Width = 87;
            appearance39.TextHAlignAsString = "Left";
            appearance39.TextVAlignAsString = "Middle";
            ultraGridColumn39.CellAppearance = appearance39;
            ultraGridColumn39.Header.Caption = "관리번호";
            ultraGridColumn39.Header.VisiblePosition = 5;
            ultraGridColumn39.Width = 95;
            appearance40.TextHAlignAsString = "Left";
            appearance40.TextVAlignAsString = "Middle";
            ultraGridColumn40.CellAppearance = appearance40;
            ultraGridColumn40.Header.Caption = "누수적출관리번호";
            ultraGridColumn40.Header.VisiblePosition = 7;
            ultraGridColumn40.Width = 109;
            appearance41.TextHAlignAsString = "Left";
            appearance41.TextVAlignAsString = "Middle";
            ultraGridColumn41.CellAppearance = appearance41;
            ultraGridColumn41.Header.Caption = "민원관리번호";
            ultraGridColumn41.Header.VisiblePosition = 8;
            appearance42.TextHAlignAsString = "Center";
            appearance42.TextVAlignAsString = "Middle";
            ultraGridColumn42.CellAppearance = appearance42;
            ultraGridColumn42.Header.Caption = "행정구역";
            ultraGridColumn42.Header.VisiblePosition = 9;
            ultraGridColumn42.Hidden = true;
            appearance43.TextHAlignAsString = "Center";
            appearance43.TextVAlignAsString = "Middle";
            ultraGridColumn43.CellAppearance = appearance43;
            ultraGridColumn43.Format = "yyyy-MM-dd";
            ultraGridColumn43.Header.Caption = "인지일자";
            ultraGridColumn43.Header.VisiblePosition = 10;
            ultraGridColumn43.Hidden = true;
            appearance44.TextHAlignAsString = "Center";
            appearance44.TextVAlignAsString = "Middle";
            ultraGridColumn44.CellAppearance = appearance44;
            ultraGridColumn44.Format = "yyyy-MM-dd";
            ultraGridColumn44.Header.Caption = "현장확인일자";
            ultraGridColumn44.Header.VisiblePosition = 11;
            ultraGridColumn44.Hidden = true;
            appearance45.TextHAlignAsString = "Center";
            appearance45.TextVAlignAsString = "Middle";
            ultraGridColumn45.CellAppearance = appearance45;
            ultraGridColumn45.Format = "yyyy-MM-dd";
            ultraGridColumn45.Header.Caption = "복구시작일";
            ultraGridColumn45.Header.VisiblePosition = 12;
            ultraGridColumn45.Hidden = true;
            appearance46.TextHAlignAsString = "Center";
            appearance46.TextVAlignAsString = "Middle";
            ultraGridColumn46.CellAppearance = appearance46;
            ultraGridColumn46.Format = "yyyy-MM-dd";
            ultraGridColumn46.Header.Caption = "복구종료일";
            ultraGridColumn46.Header.VisiblePosition = 13;
            ultraGridColumn46.Width = 84;
            appearance47.TextHAlignAsString = "Left";
            appearance47.TextVAlignAsString = "Middle";
            ultraGridColumn47.CellAppearance = appearance47;
            ultraGridColumn47.Header.Caption = "누수위치";
            ultraGridColumn47.Header.VisiblePosition = 14;
            appearance48.TextHAlignAsString = "Left";
            appearance48.TextVAlignAsString = "Middle";
            ultraGridColumn48.CellAppearance = appearance48;
            ultraGridColumn48.Header.Caption = "관재질";
            ultraGridColumn48.Header.VisiblePosition = 15;
            ultraGridColumn48.Width = 85;
            appearance49.TextHAlignAsString = "Center";
            appearance49.TextVAlignAsString = "Middle";
            ultraGridColumn49.CellAppearance = appearance49;
            ultraGridColumn49.Header.Caption = "관구경";
            ultraGridColumn49.Header.VisiblePosition = 16;
            ultraGridColumn49.Width = 65;
            appearance50.TextHAlignAsString = "Left";
            appearance50.TextVAlignAsString = "Middle";
            ultraGridColumn50.CellAppearance = appearance50;
            ultraGridColumn50.Header.Caption = "누수원인";
            ultraGridColumn50.Header.VisiblePosition = 17;
            ultraGridColumn50.Hidden = true;
            appearance51.TextHAlignAsString = "Left";
            appearance51.TextVAlignAsString = "Middle";
            ultraGridColumn51.CellAppearance = appearance51;
            ultraGridColumn51.Header.Caption = "누수부위";
            ultraGridColumn51.Header.VisiblePosition = 18;
            ultraGridColumn51.Hidden = true;
            appearance52.TextHAlignAsString = "Left";
            appearance52.TextVAlignAsString = "Middle";
            ultraGridColumn52.CellAppearance = appearance52;
            ultraGridColumn52.Header.Caption = "누수현황";
            ultraGridColumn52.Header.VisiblePosition = 19;
            ultraGridColumn52.Hidden = true;
            appearance53.TextHAlignAsString = "Left";
            appearance53.TextVAlignAsString = "Middle";
            ultraGridColumn53.CellAppearance = appearance53;
            ultraGridColumn53.Header.Caption = "누수복구내용";
            ultraGridColumn53.Header.VisiblePosition = 20;
            appearance54.TextHAlignAsString = "Left";
            appearance54.TextVAlignAsString = "Middle";
            ultraGridColumn54.CellAppearance = appearance54;
            ultraGridColumn54.Header.Caption = "소요자재내역";
            ultraGridColumn54.Header.VisiblePosition = 21;
            ultraGridColumn54.Hidden = true;
            appearance55.TextHAlignAsString = "Left";
            appearance55.TextVAlignAsString = "Middle";
            ultraGridColumn55.CellAppearance = appearance55;
            ultraGridColumn55.Header.Caption = "누수복구자명";
            ultraGridColumn55.Header.VisiblePosition = 22;
            ultraGridColumn55.Hidden = true;
            appearance56.TextHAlignAsString = "Left";
            appearance56.TextVAlignAsString = "Middle";
            ultraGridColumn56.CellAppearance = appearance56;
            ultraGridColumn56.Header.Caption = "누수확인";
            ultraGridColumn56.Header.VisiblePosition = 23;
            ultraGridColumn56.Hidden = true;
            appearance57.TextHAlignAsString = "Right";
            appearance57.TextVAlignAsString = "Middle";
            ultraGridColumn57.CellAppearance = appearance57;
            ultraGridColumn57.Header.Caption = "누수량";
            ultraGridColumn57.Header.VisiblePosition = 24;
            ultraGridColumn57.Hidden = true;
            appearance58.TextHAlignAsString = "Center";
            appearance58.TextVAlignAsString = "Middle";
            ultraGridColumn58.CellAppearance = appearance58;
            ultraGridColumn58.Format = "hh:mm";
            ultraGridColumn58.Header.Caption = "인지시간";
            ultraGridColumn58.Header.VisiblePosition = 25;
            ultraGridColumn58.Hidden = true;
            appearance59.TextHAlignAsString = "Center";
            appearance59.TextVAlignAsString = "Middle";
            ultraGridColumn59.CellAppearance = appearance59;
            ultraGridColumn59.Format = "hh:mm";
            ultraGridColumn59.Header.Caption = "현장확인시간";
            ultraGridColumn59.Header.VisiblePosition = 26;
            ultraGridColumn59.Hidden = true;
            appearance60.TextHAlignAsString = "Center";
            appearance60.TextVAlignAsString = "Middle";
            ultraGridColumn60.CellAppearance = appearance60;
            ultraGridColumn60.Format = "hh:mm";
            ultraGridColumn60.Header.Caption = "복구시작시간";
            ultraGridColumn60.Header.VisiblePosition = 27;
            ultraGridColumn60.Hidden = true;
            appearance61.TextHAlignAsString = "Center";
            appearance61.TextVAlignAsString = "Middle";
            ultraGridColumn61.CellAppearance = appearance61;
            ultraGridColumn61.Format = "hh:mm";
            ultraGridColumn61.Header.Caption = "복구완료시간";
            ultraGridColumn61.Header.VisiblePosition = 28;
            ultraGridColumn61.Hidden = true;
            appearance62.TextHAlignAsString = "Right";
            appearance62.TextVAlignAsString = "Middle";
            ultraGridColumn62.CellAppearance = appearance62;
            ultraGridColumn62.Header.Caption = "수압";
            ultraGridColumn62.Header.VisiblePosition = 29;
            ultraGridColumn62.Hidden = true;
            ultraGridColumn63.Header.VisiblePosition = 30;
            ultraGridColumn63.Hidden = true;
            ultraGridColumn64.Header.VisiblePosition = 31;
            ultraGridColumn64.Hidden = true;
            ultraGridColumn1.Header.VisiblePosition = 32;
            ultraGridColumn1.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn7,
            ultraGridColumn34,
            ultraGridColumn35,
            ultraGridColumn36,
            ultraGridColumn37,
            ultraGridColumn38,
            ultraGridColumn39,
            ultraGridColumn40,
            ultraGridColumn41,
            ultraGridColumn42,
            ultraGridColumn43,
            ultraGridColumn44,
            ultraGridColumn45,
            ultraGridColumn46,
            ultraGridColumn47,
            ultraGridColumn48,
            ultraGridColumn49,
            ultraGridColumn50,
            ultraGridColumn51,
            ultraGridColumn52,
            ultraGridColumn53,
            ultraGridColumn54,
            ultraGridColumn55,
            ultraGridColumn56,
            ultraGridColumn57,
            ultraGridColumn58,
            ultraGridColumn59,
            ultraGridColumn60,
            ultraGridColumn61,
            ultraGridColumn62,
            ultraGridColumn63,
            ultraGridColumn64,
            ultraGridColumn1});
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance63.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance63.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance63.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance63.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance63;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance64;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance65.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance65.BackColor2 = System.Drawing.SystemColors.Control;
            appearance65.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance65.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance65;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance66.BackColor = System.Drawing.SystemColors.Window;
            appearance66.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance66;
            appearance67.BackColor = System.Drawing.SystemColors.Highlight;
            appearance67.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance67;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance68.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance68;
            appearance69.BorderColor = System.Drawing.Color.Silver;
            appearance69.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance69;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance70.BackColor = System.Drawing.SystemColors.Control;
            appearance70.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance70.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance70.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance70.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance70;
            appearance71.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance71;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance72.BackColor = System.Drawing.SystemColors.Window;
            appearance72.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance72;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance73.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance73;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(3, 3);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(1046, 156);
            this.ultraGrid1.TabIndex = 48;
            this.ultraGrid1.TabStop = false;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 165);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1046, 239);
            this.panel3.TabIndex = 49;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lekageBtn);
            this.groupBox2.Controls.Add(this.lek_loc);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.pip_dip);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.lek_hms);
            this.groupBox2.Controls.Add(this.ree_hms);
            this.groupBox2.Controls.Add(this.res_hms);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.lrs_hms);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.wtp_per);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.lek_vol);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.lek_cde);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.rep_nam);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.mat_des);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.rep_exp);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.lek_exp);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.lep_cde);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.lrs_cde);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.pip_mop);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.ree_ymd);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.res_ymd);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.lek_ymd);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lrs_ymd);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.leakageMinWonBtn);
            this.groupBox2.Controls.Add(this.cano);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lek_idn);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.sblock);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.mblock);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lblock);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.ftr_idn);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1046, 239);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "누수복구 정보";
            // 
            // lekageBtn
            // 
            this.lekageBtn.Location = new System.Drawing.Point(895, 17);
            this.lekageBtn.Name = "lekageBtn";
            this.lekageBtn.Size = new System.Drawing.Size(90, 23);
            this.lekageBtn.TabIndex = 53;
            this.lekageBtn.Text = "누수지점등록";
            this.lekageBtn.UseVisualStyleBackColor = true;
            // 
            // lek_loc
            // 
            this.lek_loc.Location = new System.Drawing.Point(612, 175);
            this.lek_loc.Name = "lek_loc";
            this.lek_loc.Size = new System.Drawing.Size(373, 21);
            this.lek_loc.TabIndex = 99;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(552, 178);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 98;
            this.label27.Text = "누수위치";
            // 
            // pip_dip
            // 
            this.pip_dip.Location = new System.Drawing.Point(362, 114);
            this.pip_dip.Name = "pip_dip";
            this.pip_dip.Size = new System.Drawing.Size(121, 21);
            this.pip_dip.TabIndex = 97;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(780, 244);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 12);
            this.label22.TabIndex = 94;
            this.label22.Text = "복구완료시간";
            // 
            // lek_hms
            // 
            this.lek_hms.Location = new System.Drawing.Point(362, 238);
            this.lek_hms.Name = "lek_hms";
            this.lek_hms.Size = new System.Drawing.Size(122, 21);
            this.lek_hms.TabIndex = 93;
            // 
            // ree_hms
            // 
            this.ree_hms.Location = new System.Drawing.Point(863, 241);
            this.ree_hms.Name = "ree_hms";
            this.ree_hms.Size = new System.Drawing.Size(122, 21);
            this.ree_hms.TabIndex = 91;
            // 
            // res_hms
            // 
            this.res_hms.Location = new System.Drawing.Point(611, 241);
            this.res_hms.Name = "res_hms";
            this.res_hms.Size = new System.Drawing.Size(122, 21);
            this.res_hms.TabIndex = 89;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(528, 244);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 12);
            this.label23.TabIndex = 90;
            this.label23.Text = "복구시작시간";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(280, 244);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(77, 12);
            this.label24.TabIndex = 87;
            this.label24.Text = "현장확인시간";
            // 
            // lrs_hms
            // 
            this.lrs_hms.Location = new System.Drawing.Point(101, 238);
            this.lrs_hms.Name = "lrs_hms";
            this.lrs_hms.Size = new System.Drawing.Size(122, 21);
            this.lrs_hms.TabIndex = 85;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(42, 241);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 86;
            this.label25.Text = "인지시간";
            // 
            // wtp_per
            // 
            this.wtp_per.Location = new System.Drawing.Point(863, 210);
            this.wtp_per.Name = "wtp_per";
            this.wtp_per.Size = new System.Drawing.Size(122, 21);
            this.wtp_per.TabIndex = 83;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(829, 213);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 12);
            this.label21.TabIndex = 84;
            this.label21.Text = "수압";
            // 
            // lek_vol
            // 
            this.lek_vol.Location = new System.Drawing.Point(611, 210);
            this.lek_vol.Name = "lek_vol";
            this.lek_vol.Size = new System.Drawing.Size(122, 21);
            this.lek_vol.TabIndex = 81;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(564, 213);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 82;
            this.label20.Text = "누수량";
            // 
            // lek_cde
            // 
            this.lek_cde.FormattingEnabled = true;
            this.lek_cde.Location = new System.Drawing.Point(363, 210);
            this.lek_cde.Name = "lek_cde";
            this.lek_cde.Size = new System.Drawing.Size(121, 20);
            this.lek_cde.TabIndex = 80;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(304, 213);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 79;
            this.label7.Text = "누수확인";
            // 
            // rep_nam
            // 
            this.rep_nam.Location = new System.Drawing.Point(101, 207);
            this.rep_nam.Name = "rep_nam";
            this.rep_nam.Size = new System.Drawing.Size(122, 21);
            this.rep_nam.TabIndex = 77;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 78;
            this.label6.Text = "누수복구자명";
            // 
            // mat_des
            // 
            this.mat_des.Location = new System.Drawing.Point(101, 175);
            this.mat_des.Name = "mat_des";
            this.mat_des.Size = new System.Drawing.Size(383, 21);
            this.mat_des.TabIndex = 76;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(20, 178);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 12);
            this.label19.TabIndex = 75;
            this.label19.Text = "소요자재내역";
            // 
            // rep_exp
            // 
            this.rep_exp.Location = new System.Drawing.Point(612, 144);
            this.rep_exp.Name = "rep_exp";
            this.rep_exp.Size = new System.Drawing.Size(373, 21);
            this.rep_exp.TabIndex = 74;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(529, 147);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 12);
            this.label18.TabIndex = 73;
            this.label18.Text = "누수복구내용";
            // 
            // lek_exp
            // 
            this.lek_exp.Location = new System.Drawing.Point(101, 144);
            this.lek_exp.Name = "lek_exp";
            this.lek_exp.Size = new System.Drawing.Size(383, 21);
            this.lek_exp.TabIndex = 71;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(44, 147);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 72;
            this.label16.Text = "누수현황";
            // 
            // lep_cde
            // 
            this.lep_cde.FormattingEnabled = true;
            this.lep_cde.Location = new System.Drawing.Point(864, 114);
            this.lep_cde.Name = "lep_cde";
            this.lep_cde.Size = new System.Drawing.Size(121, 20);
            this.lep_cde.TabIndex = 70;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(805, 117);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 69;
            this.label15.Text = "누수부위";
            // 
            // lrs_cde
            // 
            this.lrs_cde.FormattingEnabled = true;
            this.lrs_cde.Location = new System.Drawing.Point(612, 113);
            this.lrs_cde.Name = "lrs_cde";
            this.lrs_cde.Size = new System.Drawing.Size(121, 20);
            this.lrs_cde.TabIndex = 68;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(553, 116);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 67;
            this.label12.Text = "누수원인";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(316, 117);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 65;
            this.label13.Text = "관구경";
            // 
            // pip_mop
            // 
            this.pip_mop.FormattingEnabled = true;
            this.pip_mop.Location = new System.Drawing.Point(101, 114);
            this.pip_mop.Name = "pip_mop";
            this.pip_mop.Size = new System.Drawing.Size(121, 20);
            this.pip_mop.TabIndex = 64;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(54, 116);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 63;
            this.label14.Text = "관재질";
            // 
            // ree_ymd
            // 
            this.ree_ymd.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.ree_ymd.Location = new System.Drawing.Point(864, 82);
            this.ree_ymd.Name = "ree_ymd";
            this.ree_ymd.Size = new System.Drawing.Size(121, 21);
            this.ree_ymd.TabIndex = 62;
            this.ree_ymd.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(781, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 12);
            this.label11.TabIndex = 61;
            this.label11.Text = "복구종료일자";
            // 
            // res_ymd
            // 
            this.res_ymd.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.res_ymd.Location = new System.Drawing.Point(612, 82);
            this.res_ymd.Name = "res_ymd";
            this.res_ymd.Size = new System.Drawing.Size(121, 21);
            this.res_ymd.TabIndex = 60;
            this.res_ymd.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(529, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 59;
            this.label10.Text = "복구시작일자";
            // 
            // lek_ymd
            // 
            this.lek_ymd.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.lek_ymd.Location = new System.Drawing.Point(363, 82);
            this.lek_ymd.Name = "lek_ymd";
            this.lek_ymd.Size = new System.Drawing.Size(121, 21);
            this.lek_ymd.TabIndex = 58;
            this.lek_ymd.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(280, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 57;
            this.label9.Text = "현장확인일자";
            // 
            // lrs_ymd
            // 
            this.lrs_ymd.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.lrs_ymd.Location = new System.Drawing.Point(102, 82);
            this.lrs_ymd.Name = "lrs_ymd";
            this.lrs_ymd.Size = new System.Drawing.Size(121, 21);
            this.lrs_ymd.TabIndex = 56;
            this.lrs_ymd.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 55;
            this.label5.Text = "인지일자";
            // 
            // leakageMinWonBtn
            // 
            this.leakageMinWonBtn.Location = new System.Drawing.Point(708, 48);
            this.leakageMinWonBtn.Name = "leakageMinWonBtn";
            this.leakageMinWonBtn.Size = new System.Drawing.Size(37, 23);
            this.leakageMinWonBtn.TabIndex = 54;
            this.leakageMinWonBtn.Text = "찾기";
            this.leakageMinWonBtn.UseVisualStyleBackColor = true;
            // 
            // cano
            // 
            this.cano.Enabled = false;
            this.cano.Location = new System.Drawing.Point(612, 50);
            this.cano.Name = "cano";
            this.cano.Size = new System.Drawing.Size(90, 21);
            this.cano.TabIndex = 52;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(529, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 53;
            this.label4.Text = "민원관리번호";
            // 
            // lek_idn
            // 
            this.lek_idn.Enabled = false;
            this.lek_idn.Location = new System.Drawing.Point(363, 50);
            this.lek_idn.Name = "lek_idn";
            this.lek_idn.Size = new System.Drawing.Size(121, 21);
            this.lek_idn.TabIndex = 49;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(256, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 12);
            this.label3.TabIndex = 50;
            this.label3.Text = "누수적출관리번호";
            // 
            // sblock
            // 
            this.sblock.FormattingEnabled = true;
            this.sblock.Location = new System.Drawing.Point(612, 19);
            this.sblock.Name = "sblock";
            this.sblock.Size = new System.Drawing.Size(121, 20);
            this.sblock.TabIndex = 48;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(565, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 47;
            this.label1.Text = "소블록";
            // 
            // mblock
            // 
            this.mblock.FormattingEnabled = true;
            this.mblock.Location = new System.Drawing.Point(363, 20);
            this.mblock.Name = "mblock";
            this.mblock.Size = new System.Drawing.Size(121, 20);
            this.mblock.TabIndex = 46;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(316, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 45;
            this.label2.Text = "중블록";
            // 
            // lblock
            // 
            this.lblock.FormattingEnabled = true;
            this.lblock.Location = new System.Drawing.Point(101, 20);
            this.lblock.Name = "lblock";
            this.lblock.Size = new System.Drawing.Size(121, 20);
            this.lblock.TabIndex = 44;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(54, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 12);
            this.label17.TabIndex = 43;
            this.label17.Text = "대블록";
            // 
            // ftr_idn
            // 
            this.ftr_idn.Enabled = false;
            this.ftr_idn.Location = new System.Drawing.Point(101, 50);
            this.ftr_idn.Name = "ftr_idn";
            this.ftr_idn.Size = new System.Drawing.Size(122, 21);
            this.ftr_idn.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(42, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 25;
            this.label8.Text = "관리번호";
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1052, 111);
            this.searchBox1.TabIndex = 28;
            // 
            // importBtn
            // 
            this.importBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.importBtn.Location = new System.Drawing.Point(759, 2);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(63, 25);
            this.importBtn.TabIndex = 54;
            this.importBtn.TabStop = false;
            this.importBtn.Text = "Import";
            this.importBtn.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 568);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.MinimumSize = new System.Drawing.Size(600, 600);
            this.Name = "frmMain";
            this.Text = "누수지점 관리";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ree_ymd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.res_ymd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lek_ymd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lrs_ymd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private WaterNet.WV_Common.form.SearchBox searchBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button deleteBtn;
        private System.Windows.Forms.Button insertBtn;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox ftr_idn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox sblock;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox mblock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox lblock;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button uploadBtn;
        private System.Windows.Forms.TextBox lek_idn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button leakageMinWonBtn;
        private System.Windows.Forms.TextBox cano;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor res_ymd;
        private System.Windows.Forms.Label label10;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor lek_ymd;
        private System.Windows.Forms.Label label9;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor lrs_ymd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox mat_des;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox rep_exp;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox lek_exp;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox lep_cde;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox lrs_cde;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox pip_mop;
        private System.Windows.Forms.Label label14;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ree_ymd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox lek_hms;
        private System.Windows.Forms.TextBox ree_hms;
        private System.Windows.Forms.TextBox res_hms;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox lrs_hms;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox wtp_per;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox lek_vol;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox lek_cde;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox rep_nam;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox pip_dip;
        private System.Windows.Forms.TextBox lek_loc;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button lekageBtn;
        private System.Windows.Forms.Button importBtn;
    }
}