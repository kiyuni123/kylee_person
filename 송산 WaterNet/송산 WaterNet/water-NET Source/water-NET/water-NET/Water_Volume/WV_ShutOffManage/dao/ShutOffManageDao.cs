﻿using System.Text;
using WaterNet.WV_Common.dao;
using WaterNet.WaterNetCore;
using System.Data;
using System.Collections;
using Oracle.DataAccess.Client;

namespace WaterNet.WV_ShutOffManage.dao
{
    public class ShutOffManageDao: BaseDao
    {
        private static ShutOffManageDao dao = null;

        private ShutOffManageDao() { }

        public static ShutOffManageDao GetInstance()
        {
            if (dao == null)
            {
                dao = new ShutOffManageDao();
            }
            return dao;
        }

        //기타단수작업 목록조회
        public void SelectShutOffManage(OracleDBManager manager, DataSet dataSet, string dataMember, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("with loc as                                                                                                                                                               ");
            query.AppendLine("(																																											");
            query.AppendLine("select c1.loc_code																																						");
            query.AppendLine("      ,c1.sgccd																																							");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code)))											");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c2.loc_code), c3.loc_code)) lblock																			");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))													");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, c1.loc_code), c2.loc_code)) mblock																			");
            query.AppendLine("      ,decode(c1.ploc_code, null, decode(c2.loc_code, null, decode(c2.ploc_code, null, decode(c3.loc_code, null, null)))													");
            query.AppendLine("      ,decode(c2.ploc_code, null, decode(c3.loc_code, null, null), c1.loc_code)) sblock																					");
            query.AppendLine("      ,c1.ftr_code																																						");
            query.AppendLine("      ,decode(c1.kt_gbn, '002', (select loc_code from cm_location where ploc_code = c1.loc_code), c1.loc_code) tag_loc_code                								");
            query.AppendLine("      ,decode(c1.ftr_code, 'BZ002', (select loc_gbn from cm_location where loc_name = c1.rel_loc_name)) rel_loc_gbn														");
            query.AppendLine("      ,c1.rel_loc_name																																					");
            query.AppendLine("      ,c1.ord																																								");
            query.AppendLine("  from																																									");
            query.AppendLine("      (																																									");
            query.AppendLine("       select sgccd																																						");
            query.AppendLine("             ,loc_code																																					");
            query.AppendLine("             ,ploc_code																																					");
            query.AppendLine("             ,loc_name																																					");
            query.AppendLine("             ,ftr_idn																																						");
            query.AppendLine("             ,ftr_code																																					");
            query.AppendLine("             ,rel_loc_name																																				");
            query.AppendLine("             ,kt_gbn																																				        ");
            query.AppendLine("             ,rownum ord																																					");
            query.AppendLine("         from cm_location																																					");
            query.AppendLine("        where 1 = 1																																						");
            query.AppendLine("        start with loc_code = :0																																			");
            query.AppendLine("        connect by prior loc_code = ploc_code																																");
            query.AppendLine("        order SIBLINGS by ftr_idn																																");
            query.AppendLine("      ) c1																																								");
            query.AppendLine("       ,cm_location c2																																					");
            query.AppendLine("       ,cm_location c3																																					");
            query.AppendLine(" where 1 = 1																																								");
            query.AppendLine("   and c1.ploc_code = c2.loc_code(+)																																		");
            query.AppendLine("   and c2.ploc_code = c3.loc_code(+)																																		");
            query.AppendLine(" order by c1.ord																																							");
            query.AppendLine(")																																											");
            query.AppendLine("select loc.loc_code																																						");
            query.AppendLine("      ,loc.sgccd																																							");
            query.AppendLine("      ,loc.lblock																																							");
            query.AppendLine("      ,loc.mblock																																							");
            query.AppendLine("      ,loc.sblock																																							");
            query.AppendLine("      ,to_date(wec.datee,'yyyymmdd') datee																									  						    ");
            query.AppendLine("      ,wec.work_num																																						");
            query.AppendLine("      ,wec.mdfy_yn																																						");
            query.AppendLine("      ,wec.work_volume																																					");
            query.AppendLine("      ,wec.work_loc																																						");
            query.AppendLine("      ,wec.work_con																																						");
            query.AppendLine("  from loc																																								");
            query.AppendLine("      ,wv_etc_cutoff wec																																					");
            query.AppendLine(" where 1 = 1																																								");
            query.AppendLine("   and wec.loc_code = loc.loc_code																																		");
            query.AppendLine("   and wec.datee between :1 and :2																																		");

            IDataParameter[] parameters =  {
	                 new OracleParameter("0", OracleDbType.Varchar2)
                    ,new OracleParameter("1", OracleDbType.Varchar2)
	                ,new OracleParameter("2", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["STARTDATE"];
            parameters[2].Value = parameter["ENDDATE"];

            manager.FillDataSetScript(dataSet, dataMember, query.ToString(), parameters);
        }

        //기타단수작업 삭제
        public void DeleteShutOffManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("delete wv_etc_cutoff                                                             ");
            query.AppendLine(" where 1 = 1                                                                     ");
            query.AppendLine("   and loc_code = :0                                                             ");
            query.AppendLine("   and datee = :1                                                                ");
            query.AppendLine("   and work_num = :2                                                             ");
            query.AppendLine("   and mdfy_yn = 'Y'                                                             ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("0", OracleDbType.Varchar2)
                    ,new OracleParameter("1", OracleDbType.Varchar2)
                    ,new OracleParameter("2", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["WORK_NUM"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //기타단수작업 등록
        public void UpdateShutOffManage(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            //query.AppendLine("insert into wv_etc_cutoff                                                        ");
            //query.AppendLine("(loc_code, datee, mdfy_yn, work_num, work_volume, work_loc, work_con)			   ");
            //query.AppendLine("values																		   ");
            //query.AppendLine("(:0, :1, 'Y', :2, :3, :4, :5)													   ");


            //IDataParameter[] parameters =  {
            //         new OracleParameter("0", OracleDbType.Varchar2)
            //        ,new OracleParameter("1", OracleDbType.Varchar2)
            //        ,new OracleParameter("2", OracleDbType.Varchar2)
            //        ,new OracleParameter("3", OracleDbType.Varchar2)
            //        ,new OracleParameter("4", OracleDbType.Varchar2)
            //        ,new OracleParameter("5", OracleDbType.Varchar2)
            //    };

            //parameters[0].Value = parameter["SBLOCK"];
            //parameters[1].Value = parameter["DATEE"];
            //parameters[2].Value = parameter["WORK_NUM"];
            //parameters[3].Value = parameter["WORK_VOLUME"];
            //parameters[4].Value = parameter["WORK_LOC"];
            //parameters[5].Value = parameter["WORK_CON"];

            query.AppendLine("merge into wv_etc_cutoff a                                                                                   ");
            query.AppendLine("using (select :LOC_CODE loc_code, :DATEE datee, :WORK_NUM work_num,                                          ");
            query.AppendLine("              :WORK_VOLUME work_volume, 'Y' work_mdfy_yn, :WORK_LOC work_loc, :WORK_CON work_con from dual) b");
            query.AppendLine("   on (a.loc_code = b.loc_code and a.datee = b.datee and a.work_num = b.work_num and mdfy_yn = 'Y')          ");
            query.AppendLine("when matched then                                                                                            ");
            query.AppendLine("     update set a.work_volume = b.work_volume                                                                ");
            query.AppendLine("               ,a.work_loc = b.work_loc                                                                      ");
            query.AppendLine("               ,a.work_con = b.work_con                                                                      ");
            query.AppendLine("when not matched then                                                                                        ");
            query.AppendLine("     insert (a.loc_code, a.datee, a.work_num, a.work_volume, a.mdfy_yn, a.work_loc, a.work_con)              ");
            query.AppendLine("     values (b.loc_code, b.datee, b.work_num, b.work_volume, 'Y', b.work_loc, b.work_con)                    ");

            IDataParameter[] parameters =  {
                     new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
                    ,new OracleParameter("WORK_NUM", OracleDbType.Varchar2)
                    ,new OracleParameter("WORK_VOLUME", OracleDbType.Varchar2)
                    ,new OracleParameter("WORK_LOC", OracleDbType.Varchar2)
                    ,new OracleParameter("WORK_CON", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["WORK_NUM"];
            parameters[3].Value = parameter["WORK_VOLUME"];
            parameters[4].Value = parameter["WORK_LOC"];
            parameters[5].Value = parameter["WORK_CON"];

            manager.ExecuteScript(query.ToString(), parameters);
        }

        //기타단수작업 작업번호 조회
        public object GetShutOffNo(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("select nvl(max(to_number(work_num)),0) + 1                                       ");
            query.AppendLine("  from wv_etc_cutoff                                                             ");
            query.AppendLine(" where 1 = 1                                                                     ");
            query.AppendLine("   and loc_code = :0                                                             ");
            query.AppendLine("   and datee = :1                                                                ");
            query.AppendLine("   and mdfy_yn = 'Y'                                                             ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("0", OracleDbType.Varchar2)
                    ,new OracleParameter("1", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];

            return manager.ExecuteScriptScalar(query.ToString(), parameters);
        }

        public void UpdateExcelImport(OracleDBManager manager, Hashtable parameter)
        {
            StringBuilder query = new StringBuilder();

            query.AppendLine("MERGE INTO WV_ETC_CUTOFF A ");
            query.AppendLine("USING ");
            query.AppendLine("( ");
            query.AppendLine("      SELECT :LOC_CODE LOC_CODE ");
            query.AppendLine("            ,:DATEE DATEE ");
            query.AppendLine("            ,:WORK_NUM WORK_NUM ");
            query.AppendLine("            ,:MDFY_YN MDFY_YN ");
            query.AppendLine("            ,:WORK_VOLUME WORK_VOLUME ");
            query.AppendLine("            ,:WORK_LOC WORK_LOC ");
            query.AppendLine("            ,:WORK_CON WORK_CON ");
            query.AppendLine("        FROM DUAL ");
            query.AppendLine(") B ");
            query.AppendLine("   ON ");
            query.AppendLine("( ");
            query.AppendLine("             A.LOC_CODE = B.LOC_CODE ");
            query.AppendLine("         AND A.DATEE = B.DATEE ");
            query.AppendLine("         AND A.WORK_NUM = B.WORK_NUM ");
            query.AppendLine("         AND A.MDFY_YN = B.MDFY_YN ");
            query.AppendLine(")  ");
            query.AppendLine("WHEN MATCHED THEN ");
            query.AppendLine("     UPDATE SET A.WORK_VOLUME = B.WORK_VOLUME ");
            query.AppendLine("               ,A.WORK_LOC = B.WORK_LOC ");
            query.AppendLine("               ,A.WORK_CON = B.WORK_CON ");
            query.AppendLine("WHEN NOT MATCHED THEN ");
            query.AppendLine("     INSERT (A.LOC_CODE, A.DATEE, A.WORK_NUM, A.MDFY_YN, A.WORK_VOLUME, A.WORK_LOC, A.WORK_CON) ");
            query.AppendLine("     VALUES (B.LOC_CODE, B.DATEE, B.WORK_NUM, B.MDFY_YN, B.WORK_VOLUME, B.WORK_LOC, B.WORK_CON) ");

            IDataParameter[] parameters =  {
	                 new OracleParameter("LOC_CODE", OracleDbType.Varchar2)
                    ,new OracleParameter("DATEE", OracleDbType.Varchar2)
	                ,new OracleParameter("WORK_NUM", OracleDbType.Varchar2)
                    ,new OracleParameter("MDFY_YN", OracleDbType.Varchar2)
                    ,new OracleParameter("WORK_VOLUME", OracleDbType.Varchar2)
                    ,new OracleParameter("WORK_LOC", OracleDbType.Varchar2)
                    ,new OracleParameter("WORK_CON", OracleDbType.Varchar2)
                };

            parameters[0].Value = parameter["LOC_CODE"];
            parameters[1].Value = parameter["DATEE"];
            parameters[2].Value = parameter["WORK_NUM"];
            parameters[3].Value = parameter["MDFY_YN"];
            parameters[4].Value = parameter["WORK_VOLUME"];
            parameters[5].Value = parameter["WORK_LOC"];
            parameters[6].Value = parameter["WORK_CON"];

            manager.ExecuteScript(query.ToString(), parameters);
        }
    }
}
