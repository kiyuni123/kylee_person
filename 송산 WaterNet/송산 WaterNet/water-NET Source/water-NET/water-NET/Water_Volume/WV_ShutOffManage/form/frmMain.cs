﻿using System;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.control;
using WaterNet.WV_Common.form;
using WaterNet.WV_Common.enum1;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using WaterNet.WV_Common.util;
using System.Collections;
using WaterNet.WV_ShutOffManage.work;
using EMFrame.log;
using WaterNet.WV_Common.excel;

namespace WaterNet.WV_ShutOffManage.form
{
    public partial class frmMain : Form, ExcelImport
    {
        private IMapControlWV mainMap = null;
        private UltraGridManager gridManager = null;
        private BlockComboboxControl detailblock = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 데이터 확인용폼으로 사용될 경우
        /// </summary>
        public bool UsePlan
        {
            set
            {
                if (value == true)
                {
                    this.searchBox1.Visible = false;
                    this.panel1.Visible = false;
                    this.searchBox1.InitializeParameter();
                    this.SelectShutOffManage(this.searchBox1.Parameters);
                }
            }
        }

        /// <summary>
        /// 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼로드 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)       
            //=========================================================단수작업 관리

            object o = EMFrame.statics.AppStatic.USER_MENU["단수작업관리ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.insertBtn.Enabled = false;
                this.updateBtn.Enabled = false;
                this.deleteBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 폼의 수정가능 여부
        /// </summary>
        private bool DetailEnabled
        {
            get
            {
                bool enabled = false;

                UltraGridRow row = null;

                if (this.ultraGrid1.Selected.Rows.Count > 0)
                {
                    row = this.ultraGrid1.Selected.Rows[0];
                }

                if (row == null)
                {
                    enabled = true;
                }

                if (row != null)
                {
                    if (row.Cells["MDFY_YN"].Value.ToString() == "Y")
                    {
                        enabled = true;
                    }
                    else if (row.Cells["MDFY_YN"].Value.ToString() == "N")
                    {
                        enabled = false;
                    }
                }
                return enabled;
            }
        }

        /// <summary>
        /// 폼 초기화 설정
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.searchBox1.Text = "검색조건";
            this.searchBox1.IntervalType = INTERVAL_TYPE.DATE;
            this.searchBox1.IntervalText = "검색기간";
            this.searchBox1.StartDateObject.Value = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
            this.searchBox1.EndDateObject.Value = DateTime.Now.ToString("yyyy-MM-dd");

            this.detailblock = new BlockComboboxControl(this.lblock, this.mblock, this.sblock);
            this.detailblock.AddDefaultOption(LOCATION_TYPE.All, false);

            this.detailblock.StopEvent = true;
        }

        /// <summary>
        /// 그리드 초기화 설정
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.Default;
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            //수정가능여부코드,
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MDFY_YN"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MDFY_YN");
                Utils.SetValueList(valueList, VALUELIST_TYPE.CODE, "2011");
            }
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MDFY_YN"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MDFY_YN"];
        }

        /// <summary>
        /// 이벤트를 설정한다.
        /// </summary>
        private void InitializeEvent()
        {
            this.importBtn.Click += new EventHandler(importBtn_Click);

            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.insertBtn.Click += new EventHandler(insertBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);
            this.deleteBtn.Click += new EventHandler(deleteBtn_Click);

            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            frmImport import = new frmImport();
            import.Items.Add(new ImportItem("LOC_CODE", "지역관리번호", typeof(string), true));
            import.Items.Add(new ImportItem("DATEE", "작업일자", typeof(DateTime), true));
            import.Items.Add(new ImportItem("WORK_NUM", "작업번호", typeof(double), true));
            import.Items.Add(new ImportItem("MDFY_YN", "수정가능여부", typeof(string), true));
            import.Items.Add(new ImportItem("WORK_VOLUME", "수도사업용수량", typeof(double), false));
            import.Items.Add(new ImportItem("WORK_LOC", "작업위치", typeof(string), false));
            import.Items.Add(new ImportItem("WORK_CON", "작업내용", typeof(string), false));
            import.UpdateExcel = this;
            import.ShowDialog();
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.ultraGrid1, "단수작업정보");
                this.excelManager.Save("단수작업정보", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 추가버튼클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void insertBtn_Click(object sender, EventArgs e)
        {
            this.InsertShutOffManageDetail();
        }

        /// <summary>
        /// 데이터 추가 상태로 폼 변환
        /// </summary>
        private void InsertShutOffManageDetail()
        {
            this.ultraGrid1.Selected.Rows.Clear();
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Activated = false;
            }

            Utils.ClearControls(this.groupBox1);
            this.SetEnabledDetailForm(this.DetailEnabled);

            if (this.ultraGrid1.Rows.Count > 0)
            {
                this.ultraGrid1.ActiveRowScrollRegion.ScrollRowIntoView(this.ultraGrid1.Rows[0]);
            }
        }

        //검색버튼 클릭 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Hashtable parameter = this.searchBox1.InitializeParameter().Parameters;
                this.SelectShutOffManage(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //기타단수작업 조회.
        private void SelectShutOffManage(Hashtable parameter)
        {
            //LOCATION_TYPE type = DataUtils.GetLocationType(parameter["FTR_CODE"].ToString());

            //if (parameter["FTR_CODE"].ToString() == "BZ001")
            //{
            //    MessageBox.Show("검색 대상 블록을 선택해야 합니다.");
            //    return;
            //}

            //GIS에서 블럭 포커스 이동
            this.mainMap.MoveToBlock(parameter);
            this.ultraGrid1.DataSource = ShutOffManageWork.GetInstance().SelectShutOffManage(parameter).Tables[0];
        }

        /// <summary>
        /// 수정버튼클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (!this.ValidateDetailForm())
                {
                    return;
                }
                Hashtable parameter = Utils.ConverToHashtable(this.groupBox1);

                //수정인경우.
                if (this.ultraGrid1.ActiveRow != null)
                {
                    parameter["LOC_CODE"] = this.ultraGrid1.Selected.Rows[0].Cells["LOC_CODE"].Value.ToString();
                    parameter["WORK_NUM"] = this.ultraGrid1.Selected.Rows[0].Cells["WORK_NUM"].Value.ToString();
                }

                //등록인경우
                else
                {
                    parameter["LOC_CODE"] = parameter["SBLOCK"];
                    parameter["WORK_NUM"] = ShutOffManageWork.GetInstance().GetShutOffNo(parameter);
                }

                //if (this.ultraGrid1.Selected.Rows.Count > 0)
                //{
                //    if (parameter.ContainsKey("WORK_NUM"))
                //    {
                //        parameter.Remove("WORK_NUM");
                //    }

                //    if (parameter.ContainsKey("LOC_CODE"))
                //    {
                //        parameter.Remove("LOC_CODE");
                //    }
                //    parameter["LOC_CODE"] = this.ultraGrid1.Selected.Rows[0].Cells["LOC_CODE"].Value.ToString();
                //    parameter["WORK_NUM"] = this.ultraGrid1.Selected.Rows[0].Cells["WORK_NUM"].Value.ToString();
                //}

                this.UpdateShutOffManage(parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void UpdateShutOffManage(Hashtable parameter)
        {
            ShutOffManageWork.GetInstance().UpdateShutOffManage(parameter);
            this.SelectShutOffManage(this.searchBox1.Parameters);
            foreach (UltraGridRow row in this.ultraGrid1.Rows)
            {
                if (row.Cells["LOC_CODE"].Value.ToString() == parameter["LOC_CODE"].ToString() &&
                    Convert.ToDateTime(row.Cells["DATEE"].Value).ToString("yyyyMMdd") == parameter["DATEE"].ToString() &&
                    row.Cells["MDFY_YN"].Value.ToString() == "Y" &&
                    row.Cells["WORK_NUM"].Value.ToString() == parameter["WORK_NUM"].ToString())
                {
                    if (this.ultraGrid1.ActiveRow != null)
                    {
                        this.ultraGrid1.ActiveRow.Activated = false;
                    }
                    this.ultraGrid1.ActiveRow = row;
                    this.ultraGrid1.ActiveRowScrollRegion.ScrollRowIntoView(row);
                }
            }
        }

        /// <summary>
        /// 삭제버튼클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.ultraGrid1.Selected.Rows.Count == 0)
                {
                    return;
                }

                DialogResult qe = MessageBox.Show("삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (qe == DialogResult.Yes)
                {
                    this.DeleteShutOffManage(Utils.ConverToHashtable(this.ultraGrid1.Selected.Rows[0]));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void DeleteShutOffManage(Hashtable parameter)
        {
            ShutOffManageWork.GetInstance().DeleteShutOffManage(parameter);
            this.SelectShutOffManage(this.searchBox1.Parameters);
        }

        /// <summary>
        /// 그리드 로우 활성 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Selected = true;
            }
        }

        /// <summary>
        /// 그리드 로우 선택 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                return;
            }

            UltraGridRow row = this.ultraGrid1.Selected.Rows[0];

            this.SetEnabledDetailForm(this.DetailEnabled);

            //수정가능이면.....
            if (row.Cells["MDFY_YN"].Value.ToString() == "Y")
            {
                this.detailblock.StopEvent = false;
            }
            else if (row.Cells["MDFY_YN"].Value.ToString() == "N")
            {
                this.detailblock.StopEvent = true;
            }

            this.lblock.Enabled = false;
            this.mblock.Enabled = false;
            this.sblock.Enabled = false;
            this.datee.Enabled = false;

            this.lblock.SelectedValue = row.Cells["LBLOCK"].Value;
            this.mblock.SelectedValue = row.Cells["MBLOCK"].Value;
            this.sblock.SelectedValue = row.Cells["SBLOCK"].Value;
            this.datee.Value = row.Cells["DATEE"].Value;
            this.work_volume.Text = row.Cells["WORK_VOLUME"].Value.ToString();
            this.work_con.Text = row.Cells["WORK_CON"].Value.ToString();
            this.work_loc.Text = row.Cells["WORK_LOC"].Value.ToString();
        }

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            Utils.ClearControls(this.groupBox1);
        }

        private bool ValidateDetailForm()
        {
            bool isValudate = false;

            if (this.sblock.SelectedIndex == 0)
            {
                MessageBox.Show("블록을 선택해야 합니다.");
                this.sblock.Select();
                return isValudate;
            }

            try
            {
                Convert.ToDouble(this.work_volume.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show("사용량은 숫자 형식으로 입력하셔야 합니다.");
                this.work_volume.Select();
                return isValudate;
            }

            isValudate = true;

            return isValudate;
        }

        ///<summary>
        ///상세폼의 객체 활성/ 비활성 제어
        ///</summary>
        ///<param name="isEnabled"></param>
        private void SetEnabledDetailForm(bool isEnabled)
        {
            this.lblock.Enabled = isEnabled;
            this.mblock.Enabled = isEnabled;
            this.sblock.Enabled = isEnabled;
            this.datee.Enabled = isEnabled;
            this.work_con.Enabled = isEnabled;
            this.work_loc.Enabled = isEnabled;
            this.work_volume.Enabled = isEnabled;

            this.deleteBtn.Enabled = isEnabled;
            this.updateBtn.Enabled = isEnabled;
        }

        #region ExcelImport 멤버

        public void update(System.Data.DataTable data)
        {
            ShutOffManageWork.GetInstance().UpdateExcelImport(data);
        }

        #endregion
    }
}
