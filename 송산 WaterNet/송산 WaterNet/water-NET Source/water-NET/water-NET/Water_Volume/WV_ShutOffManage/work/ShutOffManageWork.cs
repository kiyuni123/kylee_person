﻿using System;
using WaterNet.WV_ShutOffManage.dao;
using System.Data;
using System.Collections;
using WaterNet.WV_Common.work;
using System.Windows.Forms;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_ShutOffManage.work
{
    public class ShutOffManageWork: BaseWork
    {
        private static ShutOffManageWork work = null;
        private ShutOffManageDao dao = null;

        public static ShutOffManageWork GetInstance()
        {
            if (work == null)
            {
                work = new ShutOffManageWork();
            }
            return work;
        }

        private ShutOffManageWork()
        {
            dao = ShutOffManageDao.GetInstance();
        }

        public DataSet SelectShutOffManage(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                this.dao.SelectShutOffManage(DataBaseManager, result, "RESULT", parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet DeleteShutOffManage(Hashtable parameter)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                this.dao.DeleteShutOffManage(DataBaseManager, parameter);

                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public void UpdateShutOffManage(Hashtable parameter)
        {
            try
            {
                ConnectionOpen();
                BeginTransaction();

                //this.dao.DeleteShutOffManage(DataBaseManager, parameter);
                this.dao.UpdateShutOffManage(DataBaseManager, parameter);

                CommitTransaction();
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
        }

        public object GetShutOffNo(Hashtable parameter)
        {
            object result = null;

            try
            {
                ConnectionOpen();
                BeginTransaction();

                result = dao.GetShutOffNo(DataBaseManager, parameter);

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }

        public DataSet UpdateExcelImport(DataTable data)
        {
            DataSet result = new DataSet();
            try
            {
                ConnectionOpen();
                BeginTransaction();

                foreach (DataRow row in data.Rows)
                {
                    this.dao.UpdateExcelImport(DataBaseManager, Utils.ConverToHashtable(row));
                }

                CommitTransaction();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseTransaction();
                ConnectionClose();
            }
            return result;
        }
    }
}
