﻿using System;
using System.Collections.Generic;

namespace WaterNet.WV_LeakageManage.vo
{
    public class LeakageCalculationVO
    {
        private IList<LeakageOptionVO> leakageOptionVO = new List<LeakageOptionVO>();

        public void AddLeakageOptionVO(double jumal_used, double gajung_used,
            double bgajung_used, int special_used_percent, double special_used,
            double n1, double lc, double icf)
        {
            leakageOptionVO.Add(new LeakageOptionVO(jumal_used, gajung_used, bgajung_used, 
                special_used_percent, special_used, n1, lc, icf));
        }

        //public void AddLeakageOptionVO(string tm_ins_yn, double hpres_diff, double jumal_used, double gajung_used,
        //    double bgajung_used, string special_usedtm_yn, int special_used_percent, double special_used,
        //    double n1, double lc, double icf)
        //{
        //    leakageOptionVO.Add(new LeakageOptionVO(tm_ins_yn, hpres_diff, jumal_used, gajung_used, bgajung_used,
        //        special_usedtm_yn, special_used_percent, special_used, n1, lc, icf));
        //}

        //--------- 데이터값
        private string loc_code;               //지역코드
        private string sgccd;                  //센터코드
        private string lblock;                 //대블록
        private string mblock;                 //중블록
        private string sblock;                 //소블록
        private DateTime datee;                //일자
        private double m_average;              //이동평균 야간최소유량
        private double filtering;              //필터링 야간최소유량
        private double pip_len;                //배수관연장
        private int gupsujunsu;                //급수전수
        private int gajung_gagusu;             //가정가구수
        private int bgajung_gagusu;            //비가정가구수
        //private double gajung_amtuse_nt;       //가정야간사용량
        //private double bgajung_amtuse_nt;      //비가정야간사용량
        private double special_amtuse;         //특별사용량
        //private double special_amtuse_nt;      //특별야간사용량
        //private double jumal_amtuse;           //주말사용량
        //private double amtuse_nt;              //야간사용량
        //private double leaks_nt;               //야간누수량
        private double infpnt_hpres_davg;      //유입지점일평균수압
        //private double infpnt_hpres_pavg;      //유입지점야간최소유량시간대평균수압
        //private double avg_hpres_davg;         //평균수압지점일평균수압
        private double avg_hpres_pavg;         //평균수압지점야간최소유량시간대평균수압
        private string mvavg_mintime;          //야간최소유량시간대

        //private double ndf;                    //NDF
        //private double leaks_davg;             //일평균누수량
        //private double besu_lenen_leaks;       //배수관연장/누수량
        //private double gupsujun_leaks;         //급수전/누수량
        //private double okne_lenen_leaks;       //옥내관연장/누수량
        //private double nt_back_leaks;          //야간배경누수량
        //private double nt_burst_leaks;         //야간파열누수량
        //private double gupsujun_leaks_day;     //급수전/일누수량
        //private double pipe_lenen_leaks_day;   //배수관연장/일누수량
        //private double equip_burst;            //등가급수관파열
        //private double gupsujun_leaks_nt;      //급수전/야간파열누수량
        //private double pipe_lenen_leaks_nt;    //배수관연장/야간파열누수량

        private double hh00_hpres_in;          //00~01 유입지점 평균수압
        private double hh01_hpres_in;          //01~02 유입지점 평균수압
        private double hh02_hpres_in;          //02~03 유입지점 평균수압
        private double hh03_hpres_in;          //03~04 유입지점 평균수압
        private double hh04_hpres_in;          //04~05 유입지점 평균수압
        private double hh05_hpres_in;          //05~06 유입지점 평균수압
        private double hh06_hpres_in;          //06~07 유입지점 평균수압
        private double hh07_hpres_in;          //07~08 유입지점 평균수압
        private double hh08_hpres_in;          //08~09 유입지점 평균수압
        private double hh09_hpres_in;          //09~10 유입지점 평균수압
        private double hh10_hpres_in;          //10~11 유입지점 평균수압
        private double hh11_hpres_in;          //11~12 유입지점 평균수압
        private double hh12_hpres_in;          //12~13 유입지점 평균수압
        private double hh13_hpres_in;          //13~14 유입지점 평균수압
        private double hh14_hpres_in;          //14~15 유입지점 평균수압
        private double hh15_hpres_in;          //15~16 유입지점 평균수압
        private double hh16_hpres_in;          //16~17 유입지점 평균수압
        private double hh17_hpres_in;          //17~18 유입지점 평균수압
        private double hh18_hpres_in;          //18~19 유입지점 평균수압
        private double hh19_hpres_in;          //19~20 유입지점 평균수압
        private double hh20_hpres_in;          //20~21 유입지점 평균수압
        private double hh21_hpres_in;          //21~22 유입지점 평균수압
        private double hh22_hpres_in;          //22~23 유입지점 평균수압
        private double hh23_hpres_in;          //23~24 유입지점 평균수압

        private double hh00_hpres_mid;         //00~01 평균수압지점 평균수압
        private double hh01_hpres_mid;         //01~02 평균수압지점 평균수압
        private double hh02_hpres_mid;         //02~03 평균수압지점 평균수압
        private double hh03_hpres_mid;         //03~04 평균수압지점 평균수압
        private double hh04_hpres_mid;         //04~05 평균수압지점 평균수압
        private double hh05_hpres_mid;         //05~06 평균수압지점 평균수압
        private double hh06_hpres_mid;         //06~07 평균수압지점 평균수압
        private double hh07_hpres_mid;         //07~08 평균수압지점 평균수압
        private double hh08_hpres_mid;         //08~09 평균수압지점 평균수압
        private double hh09_hpres_mid;         //09~10 평균수압지점 평균수압
        private double hh10_hpres_mid;         //10~11 평균수압지점 평균수압
        private double hh11_hpres_mid;         //11~12 평균수압지점 평균수압
        private double hh12_hpres_mid;         //12~13 평균수압지점 평균수압
        private double hh13_hpres_mid;         //13~14 평균수압지점 평균수압
        private double hh14_hpres_mid;         //14~15 평균수압지점 평균수압
        private double hh15_hpres_mid;         //15~16 평균수압지점 평균수압
        private double hh16_hpres_mid;         //16~17 평균수압지점 평균수압
        private double hh17_hpres_mid;         //17~18 평균수압지점 평균수압
        private double hh18_hpres_mid;         //18~19 평균수압지점 평균수압
        private double hh19_hpres_mid;         //19~20 평균수압지점 평균수압
        private double hh20_hpres_mid;         //20~21 평균수압지점 평균수압
        private double hh21_hpres_mid;         //21~22 평균수압지점 평균수압
        private double hh22_hpres_mid;         //22~23 평균수압지점 평균수압
        private double hh23_hpres_mid;         //23~24 평균수압지점 평균수압


        private double day_supplied; //일공급량
        //private double non_revenue_gupsujunsu;
        //private double non_revenue_pipelen;

        /// <summary>
        /// 옵션
        /// </summary>
        public IList<LeakageOptionVO> LeakageOptionVO
        {
            get
            {
                return this.leakageOptionVO;
            }
        }

        private LeakageOptionVO OPTION
        {
            get
            {
                return this.leakageOptionVO[0];
            }
        }

        /// <summary>
        /// 지역코드
        /// </summary>
        public string LOC_CODE
        {
            set
            {
                this.loc_code = value;
            }
            get
            {
                return this.loc_code;
            }
        }

        /// <summary>
        /// 센터코드
        /// </summary>
        public string SGCCD
        {
            set
            {
                this.sgccd = value;
            }
            get
            {
                return this.sgccd;
            }
        }

        /// <summary>
        /// 대블록
        /// </summary>
        public string LBLOCK
        {
            set
            {
                this.lblock = value;
            }
            get
            {
                return this.lblock;
            }
        }

        /// <summary>
        /// 중블록
        /// </summary>
        public string MBLOCK
        {
            set
            {
                this.mblock = value;
            }
            get
            {
                return this.mblock;
            }
        }

        /// <summary>
        /// 소블록
        /// </summary>
        public string SBLOCK
        {
            set
            {
                this.sblock = value;
            }
            get
            {
                return this.sblock;
            }
        }

        /// <summary>
        /// 일자
        /// </summary>
        public DateTime DATEE
        {
            set
            {
                this.datee = value;
            }
            get
            {
                return this.datee;
            }
        }

        /// <summary>
        /// 이동평균 야간최소유량
        /// </summary>
        public double M_AVERAGE
        {
            set
            {
                this.m_average = value;
            }
            get
            {
                return this.m_average;
            }
        }

        /// <summary>
        /// 필터링 야간최소유량
        /// </summary>
        public double FILTERING
        {
            set
            {
                this.filtering = value;
            }
            get
            {
                //double filtering = 0;
                //if (this.OPTION.JUMAL_USED != 0)
                //{
                //    if (this.DATEE.DayOfWeek == DayOfWeek.Saturday || this.DATEE.DayOfWeek == DayOfWeek.Sunday)
                //    {
                //        filtering = (this.filtering / 100) * this.OPTION.JUMAL_USED;
                //    }
                //}

                //return this.filtering + filtering;
                return this.filtering;
            }
        }

        /// <summary>
        /// 배수관연장
        /// </summary>
        public double PIP_LEN
        {
            set
            {
                this.pip_len = value;
            }
            get
            {
                return this.pip_len;
            }
        }

        /// <summary>
        /// 수용가 급수전수
        /// </summary>
        public int GUPSUJUNSU
        {
            set
            {
                this.gupsujunsu = value;
            }
            get
            {
                return this.gupsujunsu;
            }
        }

        /// <summary>
        /// 수용가 가정가구수
        /// </summary>
        public int GAJUNG_GAGUSU
        {
            set
            {
                this.gajung_gagusu = value;
            }
            get
            {
                return this.gajung_gagusu;
            }
        }
        
        /// <summary>
        /// 수용가 비가정가구수
        /// </summary>
        public int BGAJUNG_GAGUSU
        {
            set
            {
                this.bgajung_gagusu = value;
            }
            get
            {
                return this.bgajung_gagusu;
            }
        }

        /// <summary>
        /// 수용가 가정야간사용량
        /// 가정가구수 * 가정야간사용
        /// </summary>
        public double GAJUNG_AMTUSE_NT
        {
            get
            {
                double gajung_amtuse_nt = this.gajung_gagusu * this.OPTION.GAJUNG_USED;
                return gajung_amtuse_nt;
            }
        }

        /// <summary>
        /// 수용가 비가정야간사용량
        /// 비가정가구수 * 비가정야간사용
        /// </summary>
        public double BGAJUNG_AMTUSE_NT
        {
            get
            {
                double bgajung_amtuse_nt = this.bgajung_gagusu * this.OPTION.BGAJUNG_USED;
                return bgajung_amtuse_nt;
            }
        }

        /// <summary>
        /// 수용가 특별사용량
        /// (대수용가 사용량 / 해당월의 일수) * 1000
        /// </summary>
        public double SPECIAL_AMTUSE
        {
            set
            {
                this.special_amtuse = value;
            }
            get
            {
                return this.special_amtuse;
            }

        }

        /// <summary>
        /// 수용가 특별야간사용량
        /// 대수용가에 TM 이있는경우 사용자 직접 입력,
        /// TM이 없는 경우 % 설정으로 계산
        /// (특별사용량 / 100) * 설정%
        /// </summary>
        public double SPECIAL_AMTUSE_NT
        {
            get
            {
                double special_amtuse_nt = 0;

                if (this.OPTION.SPECIAL_USED_PERCENT == 99)
                {
                    special_amtuse_nt = this.OPTION.SPECIAL_USED;
                }
                else if (OPTION.SPECIAL_USED_PERCENT != 99)
                {
                    special_amtuse_nt = (this.SPECIAL_AMTUSE / 100) * this.OPTION.SPECIAL_USED_PERCENT;
                }
                return special_amtuse_nt;
            }
        }

        /// <summary>
        /// 주말사용량
        /// 토요일이나 일요일인 경우 
        /// (가정야간사용량 * 주말사용) + (비가정야간사용량 * 주말사용) + (특별야간사용량 * 주말사용)
        /// </summary>
        public double JUMAL_AMTUSE
        {
            get
            {
                double jumal_amtuse = 0;

                if (this.OPTION.JUMAL_USED == 0)
                {
                    return jumal_amtuse;
                }
                else if (this.OPTION.JUMAL_USED != 0)
                {
                    if (this.DATEE.DayOfWeek == DayOfWeek.Saturday || this.DATEE.DayOfWeek == DayOfWeek.Sunday)
                    {
                        jumal_amtuse = ((this.GAJUNG_AMTUSE_NT / 100) * this.OPTION.JUMAL_USED) + ((this.BGAJUNG_AMTUSE_NT / 100) * this.OPTION.JUMAL_USED)
                            + ((this.SPECIAL_AMTUSE_NT / 100) * this.OPTION.JUMAL_USED);
                    }
                }
                return jumal_amtuse;
            }
        }

        /// <summary>
        /// 야간사용량
        /// 가정야간사용량 + 비가정야간사용량 + 특별야간사용량 + 주말야간사용량
        /// </summary>
        public double AMTUSE_NT
        {
            get
            {
                double amtuse_nt = 0;

                if ((this.GAJUNG_AMTUSE_NT + this.BGAJUNG_AMTUSE_NT + this.SPECIAL_AMTUSE_NT + this.JUMAL_AMTUSE) != 0)
                {
                    amtuse_nt = (this.GAJUNG_AMTUSE_NT + this.BGAJUNG_AMTUSE_NT + this.SPECIAL_AMTUSE_NT + this.JUMAL_AMTUSE) / 1000;
                }
                
                return amtuse_nt;
            }
        }

        /// <summary>
        /// 야간누수량
        /// 필터링 야간최소유량 - 야간사용량
        /// </summary>
        public double LEAKS_NT
        {
            get
            {
                double leaks_nt = this.FILTERING - this.AMTUSE_NT;
                return leaks_nt;
            }
        }

        /// <summary>
        /// 유입지점일평균수압
        /// </summary>
        public double INFPNT_HPRES_DAVG
        {
            set
            {
                this.infpnt_hpres_davg = value;
            }
            get
            {
                return this.infpnt_hpres_davg;
            }
        }

        /// <summary>
        /// 유입지점야간최소유량시간대평균수압
        /// </summary>
        public double INFPNT_HPRES_PAVG
        {
            get
            {
                double infpnt_hpres_pavg = 0;
                
                if (this.mvavg_mintime == "00")
                {
                    infpnt_hpres_pavg = this.hh00_hpres_in;
                }
                else if (this.mvavg_mintime == "01")
                {
                    infpnt_hpres_pavg = this.hh01_hpres_in;
                }
                else if (this.mvavg_mintime == "02")
                {
                    infpnt_hpres_pavg = this.hh02_hpres_in;
                }
                else if (this.mvavg_mintime == "03")
                {
                    infpnt_hpres_pavg = this.hh03_hpres_in;
                }
                else if (this.mvavg_mintime == "04")
                {
                    infpnt_hpres_pavg = this.hh04_hpres_in;
                }
                else if (this.mvavg_mintime == "05")
                {
                    infpnt_hpres_pavg = this.hh05_hpres_in;
                }
                else if (this.mvavg_mintime == "06")
                {
                    infpnt_hpres_pavg = this.hh06_hpres_in;
                }
                else if (this.mvavg_mintime == "07")
                {
                    infpnt_hpres_pavg = this.hh07_hpres_in;
                }
                return infpnt_hpres_pavg;
            }
        }

        /// <summary>
        /// 평균수압지점일평균수압
        /// </summary>
        public double AVG_HPRES_DAVG
        {
            get
            {
                return this.avg_hpres_pavg;
            }
        }

        /// <summary>
        /// 평균수압지점야간최소유량시간대수압
        /// </summary>
        public double AVG_HPRES_PAVG
        {
            set
            {
                this.avg_hpres_pavg = value;
            }
            get
            {
                double avg_hpres_pavg = 0;

                if (this.mvavg_mintime == "00")
                {
                    avg_hpres_pavg = this.hh00_hpres_mid;
                }
                else if (this.mvavg_mintime == "01")
                {
                    avg_hpres_pavg = this.hh01_hpres_mid;
                }
                else if (this.mvavg_mintime == "02")
                {
                    avg_hpres_pavg = this.hh02_hpres_mid;
                }
                else if (this.mvavg_mintime == "03")
                {
                    avg_hpres_pavg = this.hh03_hpres_mid;
                }
                else if (this.mvavg_mintime == "04")
                {
                    avg_hpres_pavg = this.hh04_hpres_mid;
                }
                else if (this.mvavg_mintime == "05")
                {
                    avg_hpres_pavg = this.hh05_hpres_mid;
                }
                else if (this.mvavg_mintime == "06")
                {
                    avg_hpres_pavg = this.hh06_hpres_mid;
                }
                else if (this.mvavg_mintime == "07")
                {
                    avg_hpres_pavg = this.hh07_hpres_mid;
                }

                return avg_hpres_pavg;
            }
        }

        /// <summary>
        /// 야간최소유량시간대
        /// </summary>
        public string MVAVG_MINTIME
        {
            set
            {
                this.mvavg_mintime = value;
            }
        }

        /// <summary>
        /// NDF
        /// </summary>
        public double NDF
        {
            get
            {
                double ndf = 0;

                if (this.AVG_HPRES_PAVG != 0)
                {
                    ndf =
                       Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh01_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh02_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh03_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh04_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh05_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh06_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh07_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh08_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh09_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh10_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh11_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh12_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh13_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh14_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh15_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh16_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh17_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh18_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh19_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh20_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh21_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh22_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
                       Math.Pow((this.hh23_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1);
                }

                return ndf;
            }
        }

        //public double NDF
        //{
        //    get
        //    {
        //        double ndf = 0;

        //        if (this.OPTION.N1 == 1.0 && this.OPTION.TM_INS_YN == "Y")
        //        {
        //            ndf = (this.AVG_HPRES_PAVG / this.AVG_HPRES_PAVG) * 24;
        //        }
        //        else if (this.OPTION.N1 != 1.0 && this.OPTION.TM_INS_YN == "Y")
        //        {
        //            ndf =
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_mid / this.AVG_HPRES_PAVG), this.OPTION.N1);
        //        }
        //        else if (this.OPTION.N1 == 1.0 && this.OPTION.TM_INS_YN == "N")
        //        {
        //            ndf = (this.AVG_HPRES_DAVG / this.AVG_HPRES_PAVG) * 24;
        //        }
        //        else if (this.OPTION.N1 != 1.0 && this.OPTION.TM_INS_YN == "N")
        //        {
        //            ndf =
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1) +
        //               Math.Pow((this.hh00_hpres_in / this.AVG_HPRES_PAVG), this.OPTION.N1);
        //        }
        //        return ndf;
        //    }
        //}


        /// <summary>
        /// 일평균누수량
        /// </summary>
        public double LEAKS_DAVG
        {
            get
            {
                double leaks_davg = 0;
                leaks_davg = this.LEAKS_NT * this.NDF;
                return leaks_davg;
            }
        }

        /// <summary>
        /// 배수관연장 * 0.02
        /// </summary>
        public double BESU_LENEN_LEAKS
        {
            get
            {
                double besu_lenen_leaks = 0;
                besu_lenen_leaks = this.PIP_LEN * 0.02;
                return besu_lenen_leaks;
            }
        }

        /// <summary>
        /// 급수전 * 1.25
        /// </summary>
        public double GUPSUJUN_LEAKS
        {
            get
            {
                double gupsujun_leaks = 0;
                gupsujun_leaks = this.GUPSUJUNSU * 1.25;
                return gupsujun_leaks;
            }
        }

        /// <summary>
        /// 옥내관연장 * 0.033
        /// </summary>
        public double OKNE_LENEN_LEAKS
        {
            get
            {
                double okne_lenen_leaks = 0;
                okne_lenen_leaks = this.GUPSUJUNSU * this.OPTION.LC * 0.033;
                return okne_lenen_leaks;
            }
        }

        /// <summary>
        /// 야간배경누수량
        /// </summary>
        public double NT_BACK_LEAKS
        {
            get
            {
                double nt_back_leaks = 0;
                nt_back_leaks = (this.OPTION.ICF * (this.BESU_LENEN_LEAKS + this.GUPSUJUN_LEAKS + this.OKNE_LENEN_LEAKS)
                    * Math.Pow((this.AVG_HPRES_PAVG / 5.0), 1.5))/1000;
                return nt_back_leaks;
            }
        }

        /// <summary>
        /// 야간파열누수량
        /// </summary>
        public double NT_BURST_LEAKS
        {
            get
            {
                double nt_burst_leaks = 0;
                nt_burst_leaks = this.LEAKS_NT - this.NT_BACK_LEAKS;
                return nt_burst_leaks;
            }
        }

        /// <summary>
        /// 급수전/일누수량
        /// </summary>
        public double GUPSUJUN_LEAKS_DAY
        {
            get
            {
                double gupsujun_leaks_day = 0;
                gupsujun_leaks_day = this.LEAKS_DAVG / this.GUPSUJUNSU;
                return gupsujun_leaks_day;
            }
        }

        /// <summary>
        /// 배수관연장/일누수량
        /// </summary>
        public double PIPE_LENEN_LEAKS_DAY
        {
            get
            {
                double pipe_lenen_leaks_day = 0;
                pipe_lenen_leaks_day = this.LEAKS_DAVG / (this.PIP_LEN / 1000);
                return pipe_lenen_leaks_day;
            }
        }

        /// <summary>
        /// 등가급수관파열(ESPB)
        /// </summary>
        public double EQUIP_BURST
        {
            get
            {
                double equip_burst = 0;
                //equip_burst = this.NT_BURST_LEAKS / Math.Pow(1.6 * (this.AVG_HPRES_PAVG / 5), 0.5);
                equip_burst = this.NT_BURST_LEAKS / (1.6 * Math.Pow((this.AVG_HPRES_PAVG / 5), 0.5));
                return equip_burst;
            }
        }

        /// <summary>
        /// 급수전/야간파열누수량
        /// </summary>
        public double GUPSUJUN_LEAKS_NT
        {
            get
            {
                double gupsujun_leaks_nt = 0;
                gupsujun_leaks_nt = this.NT_BURST_LEAKS / this.GUPSUJUNSU;
                return gupsujun_leaks_nt;
            }
        }

        /// <summary>
        /// 배수관연장/야간파열누수량
        /// </summary>
        public double PIPE_LENEN_LEAKS_NT
        {
            get
            {
                double pipe_lenen_leaks_nt = 0;
                pipe_lenen_leaks_nt = this.NT_BURST_LEAKS / (this.PIP_LEN / 1000);
                return pipe_lenen_leaks_nt;
            }
        }

        /// <summary>
        /// 일공급량
        /// </summary>
        public double DAY_SUPPLIED
        {
            set
            {
                this.day_supplied = value;
            }
            get
            {
                return this.day_supplied;
            }
        }

        ///// <summary>
        ///// 급수전/무수수량
        ///// </summary>
        //public double NON_REVENUE_GUPSUJUNSU
        //{
        //    set
        //    {
        //        this.non_revenue_gupsujunsu = value;
        //    }
        //    get
        //    {
        //        return this.non_revenue_gupsujunsu;
        //    }
        //}

        ///// <summary>
        ///// 관로연장/무수수량
        ///// </summary>
        //public double NON_REVENUE_PIPELEN
        //{
        //    set
        //    {
        //        this.non_revenue_pipelen = value;
        //    }
        //    get
        //    {
        //        return this.non_revenue_pipelen;
        //    }
        //}

        /// <summary>
        /// set only - 00~01 유입지점평균수압
        /// </summary>
        public double HH00_HPRES_IN
        {
            set
            {
                this.hh00_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 01~02 유입지점평균수압
        /// </summary>
        public double HH01_HPRES_IN
        {
            set
            {
                this.hh01_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 02~03 유입지점평균수압
        /// </summary>
        public double HH02_HPRES_IN
        {
            set
            {
                this.hh02_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 03~04 유입지점평균수압
        /// </summary>
        public double HH03_HPRES_IN
        {
            set
            {
                this.hh03_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 04~05 유입지점평균수압
        /// </summary>
        public double HH04_HPRES_IN
        {
            set
            {
                this.hh04_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 05~06 유입지점평균수압
        /// </summary>
        public double HH05_HPRES_IN
        {
            set
            {
                this.hh05_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 06~07 유입지점평균수압
        /// </summary>
        public double HH06_HPRES_IN
        {
            set
            {
                this.hh06_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 07~08 유입지점평균수압
        /// </summary>
        public double HH07_HPRES_IN
        {
            set
            {
                this.hh07_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 08~09 유입지점평균수압
        /// </summary>
        public double HH08_HPRES_IN
        {
            set
            {
                this.hh08_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 09~10 유입지점평균수압
        /// </summary>
        public double HH09_HPRES_IN
        {
            set
            {
                this.hh09_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 10~11 유입지점평균수압
        /// </summary>
        public double HH10_HPRES_IN
        {
            set
            {
                this.hh10_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 11~12 유입지점평균수압
        /// </summary>
        public double HH11_HPRES_IN
        {
            set
            {
                this.hh11_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 12~13 유입지점평균수압
        /// </summary>
        public double HH12_HPRES_IN
        {
            set
            {
                this.hh12_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 13~14 유입지점평균수압
        /// </summary>
        public double HH13_HPRES_IN
        {
            set
            {
                this.hh13_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 14~15 유입지점평균수압
        /// </summary>
        public double HH14_HPRES_IN
        {
            set
            {
                this.hh14_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 15~16 유입지점평균수압
        /// </summary>
        public double HH15_HPRES_IN
        {
            set
            {
                this.hh15_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 16~17 유입지점평균수압
        /// </summary>
        public double HH16_HPRES_IN
        {
            set
            {
                this.hh16_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 17~18 유입지점평균수압
        /// </summary>
        public double HH17_HPRES_IN
        {
            set
            {
                this.hh17_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 18~19 유입지점평균수압
        /// </summary>
        public double HH18_HPRES_IN
        {
            set
            {
                this.hh18_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 19~20 유입지점평균수압
        /// </summary>
        public double HH19_HPRES_IN
        {
            set
            {
                this.hh19_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 20~21 유입지점평균수압
        /// </summary>
        public double HH20_HPRES_IN
        {
            set
            {
                this.hh20_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 21~22 유입지점평균수압
        /// </summary>
        public double HH21_HPRES_IN
        {
            set
            {
                this.hh21_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 22~23 유입지점평균수압
        /// </summary>
        public double HH22_HPRES_IN
        {
            set
            {
                this.hh22_hpres_in = value;
            }
        }

        /// <summary>
        /// 23~24 평균수압지점 유입지점평균수압
        /// </summary>
        public double HH23_HPRES_IN
        {
            set
            {
                this.hh23_hpres_in = value;
            }
        }

        /// <summary>
        /// set only - 00~01 평균수압지점지점평균수압
        /// </summary>
        public double HH00_HPRES_MID
        {
            set
            {
                this.hh00_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 01~02 평균수압지점지점평균수압
        /// </summary>
        public double HH01_HPRES_MID
        {
            set
            {
                this.hh01_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 02~03 평균수압지점지점평균수압
        /// </summary>
        public double HH02_HPRES_MID
        {
            set
            {
                this.hh02_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 03~04 평균수압지점지점평균수압
        /// </summary>
        public double HH03_HPRES_MID
        {
            set
            {
                this.hh03_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 04~05 평균수압지점지점평균수압
        /// </summary>
        public double HH04_HPRES_MID
        {
            set
            {
                this.hh04_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 05~06 평균수압지점지점평균수압
        /// </summary>
        public double HH05_HPRES_MID
        {
            set
            {
                this.hh05_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 06~07 평균수압지점지점평균수압
        /// </summary>
        public double HH06_HPRES_MID
        {
            set
            {
                this.hh06_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 07~08 평균수압지점지점평균수압
        /// </summary>
        public double HH07_HPRES_MID
        {
            set
            {
                this.hh07_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 08~09 평균수압지점지점평균수압
        /// </summary>
        public double HH08_HPRES_MID
        {
            set
            {
                this.hh08_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 09~10 평균수압지점지점평균수압
        /// </summary>
        public double HH09_HPRES_MID
        {
            set
            {
                this.hh09_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 10~11 평균수압지점지점평균수압
        /// </summary>
        public double HH10_HPRES_MID
        {
            set
            {
                this.hh10_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 11~12 평균수압지점지점평균수압
        /// </summary>
        public double HH11_HPRES_MID
        {
            set
            {
                this.hh11_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 12~13 평균수압지점지점평균수압
        /// </summary>
        public double HH12_HPRES_MID
        {
            set
            {
                this.hh12_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 13~14 평균수압지점지점평균수압
        /// </summary>
        public double HH13_HPRES_MID
        {
            set
            {
                this.hh13_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 14~15 평균수압지점지점평균수압
        /// </summary>
        public double HH14_HPRES_MID
        {
            set
            {
                this.hh14_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 15~16 평균수압지점지점평균수압
        /// </summary>
        public double HH15_HPRES_MID
        {
            set
            {
                this.hh15_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 16~17 평균수압지점지점평균수압
        /// </summary>
        public double HH16_HPRES_MID
        {
            set
            {
                this.hh16_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 17~18 평균수압지점지점평균수압
        /// </summary>
        public double HH17_HPRES_MID
        {
            set
            {
                this.hh17_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 18~19 평균수압지점지점평균수압
        /// </summary>
        public double HH18_HPRES_MID
        {
            set
            {
                this.hh18_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 19~20 평균수압지점지점평균수압
        /// </summary>
        public double HH19_HPRES_MID
        {
            set
            {
                this.hh19_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 20~21 평균수압지점지점평균수압
        /// </summary>
        public double HH20_HPRES_MID
        {
            set
            {
                this.hh20_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 21~22 평균수압지점지점평균수압
        /// </summary>
        public double HH21_HPRES_MID
        {
            set
            {
                this.hh21_hpres_mid = value;
            }
        }

        /// <summary>
        /// set only - 22~23 평균수압지점지점평균수압
        /// </summary>
        public double HH22_HPRES_MID
        {
            set
            {
                this.hh22_hpres_mid = value;
            }
        }

        /// <summary>
        /// 23~24 평균수압지점 평균수압지점지점평균수압
        /// </summary>
        public double HH23_HPRES_MID
        {
            set
            {
                this.hh23_hpres_mid = value;
            }
        }
    }
}
