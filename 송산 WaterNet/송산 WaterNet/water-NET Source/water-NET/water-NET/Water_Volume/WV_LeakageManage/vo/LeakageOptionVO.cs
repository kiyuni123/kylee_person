﻿using System;
using System.Windows.Forms;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_LeakageManage.vo
{
    public class LeakageOptionVO
    {
        public LeakageOptionVO() { }

        public LeakageOptionVO(double jumal_used, double gajung_used,
            double bgajung_used, int special_used_percent, double special_used,
            double n1, double lc, double icf)
        {
            this.JUMAL_USED = jumal_used;
            this.GAJUNG_USED = gajung_used;
            this.BGAJUNG_USED = bgajung_used;
            this.SPECIAL_USED_PERCENT = special_used_percent;
            this.SPECIAL_USED = special_used;
            this.N1 = n1;
            this.LC = lc;
            this.ICF = icf;
        }

        //public LeakageOptionVO(string tm_ins_yn, double hpres_diff, double jumal_used, double gajung_used,
        //    double bgajung_used, string special_usedtm_yn, int special_used_percent, double special_used,
        //    double n1, double lc, double icf)
        //{
        //    this.TM_INS_YN = tm_ins_yn;
        //    this.HPRES_DIFF = hpres_diff;
        //    this.JUMAL_USED = jumal_used;
        //    this.GAJUNG_USED = gajung_used;
        //    this.BGAJUNG_USED = bgajung_used;
        //    this.SPECIAL_USEDTM_YN = special_usedtm_yn;
        //    this.SPECIAL_USED_PERCENT = special_used_percent;
        //    this.SPECIAL_USED = special_used;
        //    this.N1 = n1;
        //    this.LC = lc;
        //    this.ICF = icf;
        //}

        //private string tm_ins_yn;           //TM사용여부
        //private double hpres_diff;          //수압차
        private double jumal_used;          //주말사용
        private double gajung_used;         //가정사용
        private double bgajung_used;        //비가정사용
        //private string special_usedtm_yn;   //특별야간입력여부
        private int special_used_percent;   //특별야간(퍼센트)
        private double special_used;        //특별야간(입력)
        private double n1;                  //N1
        private double lc;                  //LC
        private double icf;                 //ICF

        /// <summary>
        /// TM설치여부
        /// </summary>
        //public string TM_INS_YN
        //{
        //    set
        //    {
        //        this.tm_ins_yn = value;
        //    }
        //    get
        //    {
        //        return this.tm_ins_yn;
        //    }
        //}

        ///// <summary>
        ///// 수압제어유형
        ///// </summary>
        //public string HPRES_KIND
        //{
        //    set
        //    {
        //        this.hpres_kind = value;
        //    }
        //    get
        //    {
        //        return this.hpres_kind;
        //    }
        //}

        ///// <summary>
        ///// 주간시작시간
        ///// </summary>
        //public string THOUR_START
        //{
        //    set
        //    {
        //        this.thour_start = value;
        //    }
        //    get
        //    {
        //        return this.thour_start;
        //    }
        //}

        ///// <summary>
        ///// 주간종료시간
        ///// </summary>
        //public string THOUR_END
        //{
        //    set
        //    {
        //        this.thour_end = value;
        //    }
        //    get
        //    {
        //        return this.thour_end;
        //    }
        //}

        ///// <summary>
        ///// 야간시간시작
        ///// </summary>
        //public string NHOUR_START
        //{
        //    set
        //    {
        //        this.nhour_start = value;
        //    }
        //    get
        //    {
        //        return this.nhour_start;
        //    }
        //}

        ///// <summary>
        ///// 야간종료시간
        ///// </summary>
        //public string NHOUR_END
        //{
        //    set
        //    {
        //        this.nhour_end = value;
        //    }
        //    get
        //    {
        //        return this.nhour_end;
        //    }
        //}

        /// <summary>
        /// 유입지점과 평균수압지점 수압차
        /// </summary>
        //public double HPRES_DIFF
        //{
        //    set
        //    {
        //        this.hpres_diff = value;
        //    }
        //    get
        //    {
        //        return this.hpres_diff;
        //    }
        //}

        /// <summary>
        /// 주말사용
        /// </summary>
        public double JUMAL_USED
        {
            set
            {
                this.jumal_used = value;
            }
            get
            {
                return this.jumal_used;
            }
        }

        /// <summary>
        /// 가정사용
        /// </summary>
        public double GAJUNG_USED
        {
            set
            {
                this.gajung_used = value;
            }
            get
            {
                return this.gajung_used;
            }
        }

        /// <summary>
        /// 비가정사용
        /// </summary>
        public double BGAJUNG_USED
        {
            set
            {
                this.bgajung_used = value;
            }
            get
            {
                return this.bgajung_used;
            }
        }

        /// <summary>
        /// 특별사용량 직접 입력여부
        /// </summary>
        //public string SPECIAL_USEDTM_YN
        //{
        //    set
        //    {
        //        if (this.special_used_percent == 99)
        //        {
        //            this.special_usedtm_yn = "Y";
        //        }
        //        else
        //        {
        //            this.special_usedtm_yn = "N";
        //        }
        //        //this.special_usedtm_yn = value;
        //    }
        //    get
        //    {
        //        return this.special_usedtm_yn;
        //    }
        //}

        /// <summary>
        /// 특별사용량%
        /// </summary>
        public int SPECIAL_USED_PERCENT
        {
            set
            {
                this.special_used_percent = value;
            }
            get
            {
                return this.special_used_percent;
            }
        }

        /// <summary>
        /// 특별사용량 입력값
        /// </summary>
        public double SPECIAL_USED
        {
            set
            {
                this.special_used = value;
            }
            get
            {
                return this.special_used;
            }
        }

        /// <summary>
        /// N1지수
        /// </summary>
        public double N1
        {
            set
            {
                this.n1 = value;
            }
            get
            {
                return this.n1;
            }
        }

        /// <summary>
        /// 평균옥내관연장
        /// </summary>
        public double LC
        {
            set
            {
                this.lc = value;
            }
            get
            {
                return this.lc;
            }
        }

        /// <summary>
        /// 시설상태지수
        /// </summary>
        public double ICF
        {
            set
            {
                this.icf = value;
            }
            get
            {
                return this.icf;
            }
        }


        /// <summary>
        /// UI에서 들어오는 값을 체크하고 등록한다.
        /// </summary>
        /// <param name="TM_INS_YN"></param>
        /// <param name="HPRES_DIFF"></param>
        /// <param name="JUMAL_USED"></param>
        /// <param name="GAJUNG_USED"></param>
        /// <param name="BGAJUNG_USED"></param>
        /// <param name="SPECIAL_USED_PERCENT"></param>
        /// <param name="SPECIAL_USED"></param>
        /// <param name="N1"></param>
        /// <param name="LC"></param>
        /// <param name="ICF"></param>
        public void SetValue(TextBox JUMAL_USED, TextBox GAJUNG_USED, TextBox BGAJUNG_USED,
            ComboBox SPECIAL_USED_PERCENT, TextBox SPECIAL_USED, TextBox N1, TextBox LC, TextBox ICF)
        {
            this.jumal_used = ControlUtils.GetDoubleValue(JUMAL_USED);
            this.gajung_used = ControlUtils.GetDoubleValue(GAJUNG_USED);
            this.bgajung_used = ControlUtils.GetDoubleValue(BGAJUNG_USED);
            this.special_used_percent = Convert.ToInt32(SPECIAL_USED_PERCENT.SelectedValue);
            this.special_used = ControlUtils.GetDoubleValue(SPECIAL_USED);
            this.n1 = ControlUtils.GetDoubleValue(N1);
            this.lc = ControlUtils.GetDoubleValue(LC);
            this.icf = ControlUtils.GetDoubleValue(ICF);
        }

        //public void SetValue(ComboBox TM_INS_YN, TextBox HPRES_DIFF, TextBox JUMAL_USED, TextBox GAJUNG_USED, TextBox BGAJUNG_USED,
        //    ComboBox SPECIAL_USED_PERCENT, TextBox SPECIAL_USED, TextBox N1, TextBox LC, TextBox ICF)
        //{
        //    this.tm_ins_yn = TM_INS_YN.SelectedValue.ToString();
        //    this.hpres_diff = ControlUtils.GetDoubleValue(HPRES_DIFF);
        //    this.jumal_used = ControlUtils.GetDoubleValue(JUMAL_USED);
        //    this.gajung_used = ControlUtils.GetDoubleValue(GAJUNG_USED);
        //    this.bgajung_used = ControlUtils.GetDoubleValue(BGAJUNG_USED);
        //    this.special_used_percent = Convert.ToInt32(SPECIAL_USED_PERCENT.SelectedValue);
        //    this.special_used = ControlUtils.GetDoubleValue(SPECIAL_USED);
        //    this.n1 = ControlUtils.GetDoubleValue(N1);
        //    this.lc = ControlUtils.GetDoubleValue(LC);
        //    this.icf = ControlUtils.GetDoubleValue(ICF);
        //}

        /// <summary>
        /// UI객체에 값을 세팅한다.
        /// </summary>
        /// <param name="TM_INS_YN"></param>
        /// <param name="HPRES_DIFF"></param>
        /// <param name="JUMAL_USED"></param>
        /// <param name="GAJUNG_USED"></param>
        /// <param name="BGAJUNG_USED"></param>
        /// <param name="SPECIAL_USED_PERCENT"></param>
        /// <param name="SPECIAL_USED"></param>
        /// <param name="N1"></param>
        /// <param name="LC"></param>
        /// <param name="ICF"></param>
        public void GetValue(TextBox JUMAL_USED, TextBox GAJUNG_USED, TextBox BGAJUNG_USED,
            ComboBox SPECIAL_USED_PERCENT, TextBox SPECIAL_USED, TextBox N1, TextBox LC, TextBox ICF)
        {
            GAJUNG_USED.Text = this.GAJUNG_USED.ToString();
            BGAJUNG_USED.Text = this.BGAJUNG_USED.ToString();

            if (this.SPECIAL_USED_PERCENT != 99)
            {
                ComboBoxUtils.SelectItemByValueMember(SPECIAL_USED_PERCENT, this.SPECIAL_USED_PERCENT.ToString());
                ControlUtils.SetEnabled(SPECIAL_USED, false);
            }
            else
            {
                SPECIAL_USED.Text = this.SPECIAL_USED.ToString();
                ControlUtils.SetEnabled(SPECIAL_USED, true);
            }

            JUMAL_USED.Text = this.JUMAL_USED.ToString();

            LC.Text = this.LC.ToString();
            ICF.Text = this.ICF.ToString();
            N1.Text = this.N1.ToString();

        }

        //public void GetValue(ComboBox TM_INS_YN, TextBox HPRES_DIFF, TextBox JUMAL_USED, TextBox GAJUNG_USED, TextBox BGAJUNG_USED,
        //    ComboBox SPECIAL_USED_PERCENT, TextBox SPECIAL_USED, TextBox N1, TextBox LC, TextBox ICF)
        //{
        //    GAJUNG_USED.Text = this.GAJUNG_USED.ToString();
        //    BGAJUNG_USED.Text = this.BGAJUNG_USED.ToString();

        //    if (this.SPECIAL_USED_PERCENT != 99)
        //    {
        //        ComboBoxUtils.SelectItemByValueMember(SPECIAL_USED_PERCENT, this.SPECIAL_USED_PERCENT.ToString());
        //        ControlUtils.SetEnabled(SPECIAL_USED, false);
        //    }
        //    else
        //    {
        //        SPECIAL_USED.Text = this.SPECIAL_USED.ToString();
        //        ControlUtils.SetEnabled(SPECIAL_USED, true);
        //    }

        //    JUMAL_USED.Text = this.JUMAL_USED.ToString();

        //    if (this.TM_INS_YN.ToString() == "Y")
        //    {
        //        ComboBoxUtils.SelectItemByValueMember(TM_INS_YN, this.TM_INS_YN.ToString());
        //        HPRES_DIFF.Text = this.HPRES_DIFF.ToString();
        //        ControlUtils.SetEnabled(HPRES_DIFF, false);
        //    }
        //    else
        //    {
        //        ComboBoxUtils.SelectItemByValueMember(TM_INS_YN, this.TM_INS_YN.ToString());
        //        HPRES_DIFF.Text = this.HPRES_DIFF.ToString();
        //        ControlUtils.SetEnabled(HPRES_DIFF, true);
        //    }

        //    LC.Text = this.LC.ToString();
        //    ICF.Text = this.ICF.ToString();
        //    N1.Text = this.N1.ToString();

        //}

        public void ValueCopy(LeakageOptionVO changeValue)
        {
            this.jumal_used = changeValue.JUMAL_USED;
            this.gajung_used = changeValue.GAJUNG_USED;
            this.bgajung_used = changeValue.BGAJUNG_USED;
            //this.special_usedtm_yn = changeValue.SPECIAL_USEDTM_YN;
            this.special_used_percent = changeValue.SPECIAL_USED_PERCENT;
            this.special_used = changeValue.SPECIAL_USED;
            this.n1 = changeValue.N1;
            this.lc = changeValue.LC;
            this.icf = changeValue.ICF;
        }

        //public void ValueCopy(LeakageOptionVO changeValue)
        //{
        //    this.tm_ins_yn = changeValue.TM_INS_YN;
        //    this.hpres_diff = changeValue.HPRES_DIFF;
        //    this.jumal_used = changeValue.JUMAL_USED;
        //    this.gajung_used = changeValue.GAJUNG_USED;
        //    this.bgajung_used = changeValue.BGAJUNG_USED;
        //    this.special_usedtm_yn = changeValue.SPECIAL_USEDTM_YN;
        //    this.special_used_percent = changeValue.SPECIAL_USED_PERCENT;
        //    this.special_used = changeValue.SPECIAL_USED;
        //    this.n1 = changeValue.N1;
        //    this.lc = changeValue.LC;
        //    this.icf = changeValue.ICF;
        //}
    }
}
