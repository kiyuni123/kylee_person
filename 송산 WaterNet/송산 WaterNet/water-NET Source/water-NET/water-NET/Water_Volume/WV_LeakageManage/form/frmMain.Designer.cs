﻿namespace WaterNet.WV_LeakageManage.form
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("LeakageCalculationVO", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn61 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SGCCD");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn62 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn63 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn64 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn65 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DATEE");
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn66 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("M_AVERAGE");
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn67 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FILTERING");
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn68 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIP_LEN");
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn69 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GUPSUJUNSU");
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn70 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GAJUNG_GAGUSU");
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn71 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BGAJUNG_GAGUSU");
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn72 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GAJUNG_AMTUSE_NT");
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn73 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BGAJUNG_AMTUSE_NT");
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn74 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPECIAL_AMTUSE");
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn75 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPECIAL_AMTUSE_NT");
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn76 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUMAL_AMTUSE");
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn77 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AMTUSE_NT");
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn78 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEAKS_NT");
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn79 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("INFPNT_HPRES_DAVG");
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn80 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("INFPNT_HPRES_PAVG");
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn81 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AVG_HPRES_DAVG");
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn82 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AVG_HPRES_PAVG");
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn83 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NDF");
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn84 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LEAKS_DAVG");
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn87 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BESU_LENEN_LEAKS");
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn88 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GUPSUJUN_LEAKS");
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn89 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OKNE_LENEN_LEAKS");
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn90 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NT_BACK_LEAKS");
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn91 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NT_BURST_LEAKS");
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn92 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GUPSUJUN_LEAKS_DAY");
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn93 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIPE_LENEN_LEAKS_DAY");
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn94 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("EQUIP_BURST");
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn95 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GUPSUJUN_LEAKS_NT");
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn96 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PIPE_LENEN_LEAKS_NT");
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DAY_SUPPLIED");
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LeakageOptionVO");
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("LeakageOptionVO", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TM_INS_YN");
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("HPRES_DIFF");
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("JUMAL_USED");
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GAJUNG_USED");
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BGAJUNG_USED");
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPECIAL_USEDTM_YN");
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPECIAL_USED_PERCENT");
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SPECIAL_USED");
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("N1");
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LC");
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ICF");
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            ChartFX.WinForms.Adornments.SolidBackground solidBackground1 = new ChartFX.WinForms.Adornments.SolidBackground();
            ChartFX.WinForms.SeriesAttributes seriesAttributes1 = new ChartFX.WinForms.SeriesAttributes();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.settingBtn = new System.Windows.Forms.Button();
            this.leakageCompareBtn = new System.Windows.Forms.Button();
            this.updateBtn = new System.Windows.Forms.Button();
            this.chartVisibleBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.tableVisibleBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.chartPanel1 = new System.Windows.Forms.Panel();
            this.chart1 = new ChartFX.WinForms.Chart();
            this.leakageMaxSetter = new Infragistics.Win.UltraWinEditors.UltraTrackBar();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.panel10 = new System.Windows.Forms.Panel();
            this.fFlowCheck = new System.Windows.Forms.CheckBox();
            this.mFlowCheck = new System.Windows.Forms.CheckBox();
            this.chartChange = new System.Windows.Forms.ComboBox();
            this.searchBox1 = new WaterNet.WV_Common.form.SearchBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tablePanel1.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.panel3.SuspendLayout();
            this.chartPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leakageMaxSetter)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1042, 10);
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox4.Location = new System.Drawing.Point(1032, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 598);
            this.pictureBox4.TabIndex = 42;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Location = new System.Drawing.Point(0, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 598);
            this.pictureBox3.TabIndex = 41;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 608);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1042, 10);
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.settingBtn);
            this.panel1.Controls.Add(this.leakageCompareBtn);
            this.panel1.Controls.Add(this.updateBtn);
            this.panel1.Controls.Add(this.chartVisibleBtn);
            this.panel1.Controls.Add(this.searchBtn);
            this.panel1.Controls.Add(this.excelBtn);
            this.panel1.Controls.Add(this.tableVisibleBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 121);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1022, 30);
            this.panel1.TabIndex = 46;
            // 
            // settingBtn
            // 
            this.settingBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.settingBtn.AutoSize = true;
            this.settingBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.settingBtn.Location = new System.Drawing.Point(834, 2);
            this.settingBtn.Name = "settingBtn";
            this.settingBtn.Size = new System.Drawing.Size(96, 25);
            this.settingBtn.TabIndex = 50;
            this.settingBtn.TabStop = false;
            this.settingBtn.Text = "누수량산정 설정";
            this.settingBtn.UseVisualStyleBackColor = true;
            // 
            // leakageCompareBtn
            // 
            this.leakageCompareBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.leakageCompareBtn.AutoSize = true;
            this.leakageCompareBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.leakageCompareBtn.Location = new System.Drawing.Point(705, 2);
            this.leakageCompareBtn.Name = "leakageCompareBtn";
            this.leakageCompareBtn.Size = new System.Drawing.Size(123, 25);
            this.leakageCompareBtn.TabIndex = 49;
            this.leakageCompareBtn.TabStop = false;
            this.leakageCompareBtn.Text = "무수수량/누수량 비교";
            this.leakageCompareBtn.UseVisualStyleBackColor = true;
            // 
            // updateBtn
            // 
            this.updateBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.updateBtn.AutoSize = true;
            this.updateBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.updateBtn.Location = new System.Drawing.Point(936, 2);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(40, 25);
            this.updateBtn.TabIndex = 48;
            this.updateBtn.TabStop = false;
            this.updateBtn.Text = "저장";
            this.updateBtn.UseVisualStyleBackColor = true;
            // 
            // chartVisibleBtn
            // 
            this.chartVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chartVisibleBtn.Location = new System.Drawing.Point(547, 2);
            this.chartVisibleBtn.Name = "chartVisibleBtn";
            this.chartVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.chartVisibleBtn.TabIndex = 33;
            this.chartVisibleBtn.TabStop = false;
            this.chartVisibleBtn.Text = "그래프";
            this.chartVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.AutoSize = true;
            this.searchBtn.Font = new System.Drawing.Font("굴림", 8F);
            this.searchBtn.Location = new System.Drawing.Point(982, 3);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 25);
            this.searchBtn.TabIndex = 30;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(659, 2);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 25);
            this.excelBtn.TabIndex = 31;
            this.excelBtn.TabStop = false;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // tableVisibleBtn
            // 
            this.tableVisibleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableVisibleBtn.Location = new System.Drawing.Point(603, 2);
            this.tableVisibleBtn.Name = "tableVisibleBtn";
            this.tableVisibleBtn.Size = new System.Drawing.Size(50, 25);
            this.tableVisibleBtn.TabIndex = 32;
            this.tableVisibleBtn.TabStop = false;
            this.tableVisibleBtn.Text = "테이블";
            this.tableVisibleBtn.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tablePanel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.chartPanel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 151);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1022, 457);
            this.tableLayoutPanel1.TabIndex = 47;
            // 
            // tablePanel1
            // 
            this.tablePanel1.BackColor = System.Drawing.Color.White;
            this.tablePanel1.Controls.Add(this.tableLayoutPanel11);
            this.tablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel1.Location = new System.Drawing.Point(0, 192);
            this.tablePanel1.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.tablePanel1.Name = "tablePanel1";
            this.tablePanel1.Size = new System.Drawing.Size(1022, 265);
            this.tablePanel1.TabIndex = 1;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.panel17, 0, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 0F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1022, 265);
            this.tableLayoutPanel11.TabIndex = 1;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.ultraGrid1);
            this.panel17.Controls.Add(this.panel3);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(0, 10);
            this.panel17.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(1022, 255);
            this.panel17.TabIndex = 1;
            // 
            // ultraGrid1
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance13;
            ultraGridBand1.ColHeaderLines = 2;
            appearance14.TextHAlignAsString = "Center";
            ultraGridColumn61.Header.Appearance = appearance14;
            ultraGridColumn61.Header.Caption = "지역관리번호";
            ultraGridColumn61.Header.VisiblePosition = 0;
            ultraGridColumn61.Hidden = true;
            ultraGridColumn13.Header.Caption = "센터";
            ultraGridColumn13.Header.VisiblePosition = 1;
            ultraGridColumn13.Hidden = true;
            appearance15.TextHAlignAsString = "Center";
            ultraGridColumn62.Header.Appearance = appearance15;
            ultraGridColumn62.Header.Caption = "대블록";
            ultraGridColumn62.Header.VisiblePosition = 2;
            ultraGridColumn62.Width = 60;
            appearance16.TextHAlignAsString = "Center";
            ultraGridColumn63.Header.Appearance = appearance16;
            ultraGridColumn63.Header.Caption = "중블록";
            ultraGridColumn63.Header.VisiblePosition = 3;
            ultraGridColumn63.Width = 60;
            appearance17.TextHAlignAsString = "Center";
            ultraGridColumn64.Header.Appearance = appearance17;
            ultraGridColumn64.Header.Caption = "소블록";
            ultraGridColumn64.Header.VisiblePosition = 4;
            ultraGridColumn64.Width = 60;
            appearance18.TextHAlignAsString = "Center";
            ultraGridColumn65.CellAppearance = appearance18;
            appearance19.TextHAlignAsString = "Center";
            ultraGridColumn65.Header.Appearance = appearance19;
            ultraGridColumn65.Header.Caption = "일자";
            ultraGridColumn65.Header.VisiblePosition = 5;
            ultraGridColumn65.Width = 80;
            appearance20.TextHAlignAsString = "Right";
            ultraGridColumn66.CellAppearance = appearance20;
            ultraGridColumn66.Format = "###,##0.00";
            appearance21.TextHAlignAsString = "Center";
            ultraGridColumn66.Header.Appearance = appearance21;
            ultraGridColumn66.Header.Caption = "이동평균 야간최소유량\r\n(㎥/h)";
            ultraGridColumn66.Header.TextOrientation = new Infragistics.Win.TextOrientationInfo(0, Infragistics.Win.TextFlowDirection.Horizontal);
            ultraGridColumn66.Header.VisiblePosition = 6;
            ultraGridColumn66.Hidden = true;
            ultraGridColumn66.Width = 134;
            appearance22.TextHAlignAsString = "Right";
            ultraGridColumn67.CellAppearance = appearance22;
            ultraGridColumn67.Format = "###,##0.00";
            appearance23.TextHAlignAsString = "Center";
            ultraGridColumn67.Header.Appearance = appearance23;
            ultraGridColumn67.Header.Caption = "보정 야간최소유량\r\n(㎥/h)";
            ultraGridColumn67.Header.VisiblePosition = 7;
            ultraGridColumn67.Width = 123;
            appearance24.TextHAlignAsString = "Right";
            ultraGridColumn68.CellAppearance = appearance24;
            ultraGridColumn68.Format = "###,###,###";
            appearance25.TextHAlignAsString = "Center";
            ultraGridColumn68.Header.Appearance = appearance25;
            ultraGridColumn68.Header.Caption = "배수관연장\r\n(m)";
            ultraGridColumn68.Header.VisiblePosition = 9;
            ultraGridColumn68.Width = 134;
            appearance26.TextHAlignAsString = "Right";
            ultraGridColumn69.CellAppearance = appearance26;
            appearance27.TextHAlignAsString = "Center";
            ultraGridColumn69.Header.Appearance = appearance27;
            ultraGridColumn69.Header.Caption = "급수전수\r\n(전수)";
            ultraGridColumn69.Header.VisiblePosition = 10;
            ultraGridColumn69.Width = 83;
            appearance28.TextHAlignAsString = "Right";
            ultraGridColumn70.CellAppearance = appearance28;
            appearance29.TextHAlignAsString = "Center";
            ultraGridColumn70.Header.Appearance = appearance29;
            ultraGridColumn70.Header.Caption = "가정가구수\r\n(가구수)";
            ultraGridColumn70.Header.VisiblePosition = 11;
            ultraGridColumn70.Width = 76;
            appearance30.TextHAlignAsString = "Right";
            ultraGridColumn71.CellAppearance = appearance30;
            appearance31.TextHAlignAsString = "Center";
            ultraGridColumn71.Header.Appearance = appearance31;
            ultraGridColumn71.Header.Caption = "비가정가구수\r\n(가구수)";
            ultraGridColumn71.Header.VisiblePosition = 12;
            ultraGridColumn71.Width = 90;
            appearance32.BackColor = System.Drawing.SystemColors.Info;
            appearance32.TextHAlignAsString = "Right";
            ultraGridColumn72.CellAppearance = appearance32;
            ultraGridColumn72.Format = "###,##0.00";
            appearance33.TextHAlignAsString = "Center";
            ultraGridColumn72.Header.Appearance = appearance33;
            ultraGridColumn72.Header.Caption = "가정야간사용량\r\n(L/시간)";
            ultraGridColumn72.Header.VisiblePosition = 13;
            ultraGridColumn72.Width = 98;
            appearance34.BackColor = System.Drawing.SystemColors.Info;
            appearance34.TextHAlignAsString = "Right";
            ultraGridColumn73.CellAppearance = appearance34;
            ultraGridColumn73.Format = "###,##0.00";
            appearance35.TextHAlignAsString = "Center";
            ultraGridColumn73.Header.Appearance = appearance35;
            ultraGridColumn73.Header.Caption = "비가정야간사용량\r\n(L/시간)";
            ultraGridColumn73.Header.VisiblePosition = 14;
            ultraGridColumn73.Width = 113;
            appearance36.TextHAlignAsString = "Right";
            ultraGridColumn74.CellAppearance = appearance36;
            ultraGridColumn74.Format = "###,##0.00";
            appearance37.TextHAlignAsString = "Center";
            ultraGridColumn74.Header.Appearance = appearance37;
            ultraGridColumn74.Header.Caption = "특별사용량\r\n(L/시간)";
            ultraGridColumn74.Header.VisiblePosition = 15;
            ultraGridColumn74.Width = 113;
            appearance38.BackColor = System.Drawing.SystemColors.Info;
            appearance38.TextHAlignAsString = "Right";
            ultraGridColumn75.CellAppearance = appearance38;
            ultraGridColumn75.Format = "###,##0.00";
            appearance39.TextHAlignAsString = "Center";
            ultraGridColumn75.Header.Appearance = appearance39;
            ultraGridColumn75.Header.Caption = "특별야간사용량\r\n(L/시간)";
            ultraGridColumn75.Header.VisiblePosition = 16;
            ultraGridColumn75.Width = 99;
            appearance40.BackColor = System.Drawing.SystemColors.Info;
            appearance40.TextHAlignAsString = "Right";
            ultraGridColumn76.CellAppearance = appearance40;
            ultraGridColumn76.Format = "###,##0.00";
            appearance41.TextHAlignAsString = "Center";
            ultraGridColumn76.Header.Appearance = appearance41;
            ultraGridColumn76.Header.Caption = "주말야간사용량\r\n(L/시간)";
            ultraGridColumn76.Header.VisiblePosition = 17;
            ultraGridColumn76.Width = 99;
            appearance42.BackColor = System.Drawing.SystemColors.Info;
            appearance42.TextHAlignAsString = "Right";
            ultraGridColumn77.CellAppearance = appearance42;
            ultraGridColumn77.Format = "###,##0.00";
            appearance43.TextHAlignAsString = "Center";
            ultraGridColumn77.Header.Appearance = appearance43;
            ultraGridColumn77.Header.Caption = "야간사용량\r\n(㎥/h)";
            ultraGridColumn77.Header.VisiblePosition = 18;
            ultraGridColumn77.Width = 98;
            appearance44.BackColor = System.Drawing.SystemColors.Info;
            appearance44.TextHAlignAsString = "Right";
            ultraGridColumn78.CellAppearance = appearance44;
            ultraGridColumn78.Format = "###,##0.00";
            appearance45.TextHAlignAsString = "Center";
            ultraGridColumn78.Header.Appearance = appearance45;
            ultraGridColumn78.Header.Caption = "야간누수량\r\n(㎥/h)";
            ultraGridColumn78.Header.VisiblePosition = 19;
            ultraGridColumn78.Width = 80;
            appearance46.TextHAlignAsString = "Right";
            ultraGridColumn79.CellAppearance = appearance46;
            ultraGridColumn79.Format = "###,##0.00";
            appearance47.TextHAlignAsString = "Center";
            ultraGridColumn79.Header.Appearance = appearance47;
            ultraGridColumn79.Header.Caption = "유입지점일평균수압\r\n(kgf/㎠)";
            ultraGridColumn79.Header.VisiblePosition = 20;
            ultraGridColumn79.Width = 123;
            appearance48.TextHAlignAsString = "Right";
            ultraGridColumn80.CellAppearance = appearance48;
            ultraGridColumn80.Format = "###,##0.00";
            appearance49.TextHAlignAsString = "Center";
            ultraGridColumn80.Header.Appearance = appearance49;
            ultraGridColumn80.Header.Caption = "유입지점야간최소유량\r\n시간대 평균수압 (kgf/㎠)";
            ultraGridColumn80.Header.VisiblePosition = 21;
            ultraGridColumn80.Width = 167;
            appearance50.BackColor = System.Drawing.SystemColors.Info;
            appearance50.TextHAlignAsString = "Right";
            ultraGridColumn81.CellAppearance = appearance50;
            ultraGridColumn81.Format = "###,##0.00";
            appearance51.TextHAlignAsString = "Center";
            ultraGridColumn81.Header.Appearance = appearance51;
            ultraGridColumn81.Header.Caption = "평균수압지점일평균수압\r\n(kgf/㎠)";
            ultraGridColumn81.Header.VisiblePosition = 22;
            ultraGridColumn81.Width = 134;
            appearance52.BackColor = System.Drawing.SystemColors.Info;
            appearance52.TextHAlignAsString = "Right";
            ultraGridColumn82.CellAppearance = appearance52;
            ultraGridColumn82.Format = "###,##0.00";
            appearance53.TextHAlignAsString = "Center";
            ultraGridColumn82.Header.Appearance = appearance53;
            ultraGridColumn82.Header.Caption = "평균수압지점야간최소유량\r\n시간대 평균수압 (kgf/㎠)";
            ultraGridColumn82.Header.VisiblePosition = 23;
            ultraGridColumn82.Width = 168;
            appearance54.BackColor = System.Drawing.SystemColors.Info;
            appearance54.TextHAlignAsString = "Right";
            ultraGridColumn83.CellAppearance = appearance54;
            ultraGridColumn83.Format = "###,##0.00";
            appearance55.TextHAlignAsString = "Center";
            ultraGridColumn83.Header.Appearance = appearance55;
            ultraGridColumn83.Header.VisiblePosition = 24;
            ultraGridColumn83.Width = 69;
            appearance56.BackColor = System.Drawing.SystemColors.Info;
            appearance56.TextHAlignAsString = "Right";
            ultraGridColumn84.CellAppearance = appearance56;
            ultraGridColumn84.Format = "###,##0.00";
            appearance57.TextHAlignAsString = "Center";
            ultraGridColumn84.Header.Appearance = appearance57;
            ultraGridColumn84.Header.Caption = "일평균누수량\r\n(㎥/일)";
            ultraGridColumn84.Header.VisiblePosition = 25;
            ultraGridColumn84.Width = 89;
            appearance58.BackColor = System.Drawing.SystemColors.Info;
            appearance58.TextHAlignAsString = "Right";
            ultraGridColumn87.CellAppearance = appearance58;
            ultraGridColumn87.Format = "###,##0.00";
            appearance59.TextHAlignAsString = "Center";
            ultraGridColumn87.Header.Appearance = appearance59;
            ultraGridColumn87.Header.Caption = "배수관연장\r\n(*0.02)";
            ultraGridColumn87.Header.VisiblePosition = 26;
            ultraGridColumn87.Width = 79;
            appearance60.BackColor = System.Drawing.SystemColors.Info;
            appearance60.TextHAlignAsString = "Right";
            ultraGridColumn88.CellAppearance = appearance60;
            ultraGridColumn88.Format = "###,##0.00";
            appearance61.TextHAlignAsString = "Center";
            ultraGridColumn88.Header.Appearance = appearance61;
            ultraGridColumn88.Header.Caption = "급수전\r\n(*.1.25)";
            ultraGridColumn88.Header.VisiblePosition = 27;
            ultraGridColumn88.Width = 72;
            appearance62.BackColor = System.Drawing.SystemColors.Info;
            appearance62.TextHAlignAsString = "Right";
            ultraGridColumn89.CellAppearance = appearance62;
            ultraGridColumn89.Format = "###,##0.00";
            appearance63.TextHAlignAsString = "Center";
            ultraGridColumn89.Header.Appearance = appearance63;
            ultraGridColumn89.Header.Caption = "옥내관연장\r\n(*0.033)";
            ultraGridColumn89.Header.VisiblePosition = 28;
            ultraGridColumn89.Width = 82;
            appearance64.BackColor = System.Drawing.SystemColors.Info;
            appearance64.TextHAlignAsString = "Right";
            ultraGridColumn90.CellAppearance = appearance64;
            ultraGridColumn90.Format = "###,##0.00";
            appearance65.TextHAlignAsString = "Center";
            ultraGridColumn90.Header.Appearance = appearance65;
            ultraGridColumn90.Header.Caption = "야간배경누수량\r\n(㎥/h)";
            ultraGridColumn90.Header.VisiblePosition = 29;
            ultraGridColumn90.Width = 103;
            appearance66.BackColor = System.Drawing.SystemColors.Info;
            appearance66.TextHAlignAsString = "Right";
            ultraGridColumn91.CellAppearance = appearance66;
            ultraGridColumn91.Format = "###,##0.00";
            appearance67.TextHAlignAsString = "Center";
            ultraGridColumn91.Header.Appearance = appearance67;
            ultraGridColumn91.Header.Caption = "야간파열누수량\r\n(㎥/h)";
            ultraGridColumn91.Header.VisiblePosition = 30;
            ultraGridColumn91.Width = 103;
            appearance68.BackColor = System.Drawing.SystemColors.Info;
            appearance68.TextHAlignAsString = "Right";
            ultraGridColumn92.CellAppearance = appearance68;
            ultraGridColumn92.Format = "###,##0.00";
            appearance69.TextHAlignAsString = "Center";
            ultraGridColumn92.Header.Appearance = appearance69;
            ultraGridColumn92.Header.Caption = "일누수량/급수전\r\n(㎥/전/일)";
            ultraGridColumn92.Header.VisiblePosition = 31;
            ultraGridColumn92.Width = 105;
            appearance70.BackColor = System.Drawing.SystemColors.Info;
            appearance70.TextHAlignAsString = "Right";
            ultraGridColumn93.CellAppearance = appearance70;
            ultraGridColumn93.Format = "###,##0.00";
            appearance71.TextHAlignAsString = "Center";
            ultraGridColumn93.Header.Appearance = appearance71;
            ultraGridColumn93.Header.Caption = "일누수량/배수관연장\r\n(㎥/km/일)";
            ultraGridColumn93.Header.VisiblePosition = 32;
            ultraGridColumn93.Width = 128;
            appearance72.BackColor = System.Drawing.SystemColors.Info;
            appearance72.TextHAlignAsString = "Right";
            ultraGridColumn94.CellAppearance = appearance72;
            ultraGridColumn94.Format = "###,##0.00";
            appearance73.TextHAlignAsString = "Center";
            ultraGridColumn94.Header.Appearance = appearance73;
            ultraGridColumn94.Header.Caption = "등가급수관파열";
            ultraGridColumn94.Header.VisiblePosition = 33;
            ultraGridColumn94.Width = 99;
            appearance74.BackColor = System.Drawing.SystemColors.Info;
            appearance74.TextHAlignAsString = "Right";
            ultraGridColumn95.CellAppearance = appearance74;
            ultraGridColumn95.Format = "###,##0.000";
            appearance75.TextHAlignAsString = "Center";
            ultraGridColumn95.Header.Appearance = appearance75;
            ultraGridColumn95.Header.Caption = "야간파열누수량/급수전\r\n(㎥/전/시간)";
            ultraGridColumn95.Header.VisiblePosition = 34;
            ultraGridColumn95.Width = 140;
            appearance76.BackColor = System.Drawing.SystemColors.Info;
            appearance76.TextHAlignAsString = "Right";
            ultraGridColumn96.CellAppearance = appearance76;
            ultraGridColumn96.Format = "###,##0.000";
            appearance77.TextHAlignAsString = "Center";
            ultraGridColumn96.Header.Appearance = appearance77;
            ultraGridColumn96.Header.Caption = "야간파열누수량/배수관연장\r\n(㎥/km/시간)";
            ultraGridColumn96.Header.VisiblePosition = 35;
            ultraGridColumn96.Width = 154;
            appearance78.TextHAlignAsString = "Right";
            ultraGridColumn17.CellAppearance = appearance78;
            ultraGridColumn17.Format = "###,###";
            appearance79.TextHAlignAsString = "Center";
            ultraGridColumn17.Header.Appearance = appearance79;
            ultraGridColumn17.Header.Caption = "공급량";
            ultraGridColumn17.Header.VisiblePosition = 8;
            ultraGridColumn17.Hidden = true;
            ultraGridColumn1.Header.VisiblePosition = 36;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn61,
            ultraGridColumn13,
            ultraGridColumn62,
            ultraGridColumn63,
            ultraGridColumn64,
            ultraGridColumn65,
            ultraGridColumn66,
            ultraGridColumn67,
            ultraGridColumn68,
            ultraGridColumn69,
            ultraGridColumn70,
            ultraGridColumn71,
            ultraGridColumn72,
            ultraGridColumn73,
            ultraGridColumn74,
            ultraGridColumn75,
            ultraGridColumn76,
            ultraGridColumn77,
            ultraGridColumn78,
            ultraGridColumn79,
            ultraGridColumn80,
            ultraGridColumn81,
            ultraGridColumn82,
            ultraGridColumn83,
            ultraGridColumn84,
            ultraGridColumn87,
            ultraGridColumn88,
            ultraGridColumn89,
            ultraGridColumn90,
            ultraGridColumn91,
            ultraGridColumn92,
            ultraGridColumn93,
            ultraGridColumn94,
            ultraGridColumn95,
            ultraGridColumn96,
            ultraGridColumn17,
            ultraGridColumn1});
            appearance80.TextHAlignAsString = "Center";
            ultraGridColumn2.CellAppearance = appearance80;
            ultraGridColumn2.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            ultraGridColumn2.ColSpan = ((short)(3));
            appearance81.TextHAlignAsString = "Center";
            ultraGridColumn2.Header.Appearance = appearance81;
            ultraGridColumn2.Header.Caption = "평균수압지점 수압계 사용여부";
            ultraGridColumn2.Header.VisiblePosition = 0;
            ultraGridColumn2.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            ultraGridColumn2.Width = 161;
            appearance82.TextHAlignAsString = "Right";
            ultraGridColumn3.CellAppearance = appearance82;
            ultraGridColumn3.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            ultraGridColumn3.ColSpan = ((short)(3));
            appearance83.TextHAlignAsString = "Center";
            ultraGridColumn3.Header.Appearance = appearance83;
            ultraGridColumn3.Header.Caption = "유입지점과 평균수압지점 수압차";
            ultraGridColumn3.Header.VisiblePosition = 1;
            ultraGridColumn3.Width = 337;
            appearance84.TextHAlignAsString = "Right";
            ultraGridColumn4.CellAppearance = appearance84;
            ultraGridColumn4.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            appearance85.TextHAlignAsString = "Center";
            ultraGridColumn4.Header.Appearance = appearance85;
            ultraGridColumn4.Header.Caption = "주말야간사용";
            ultraGridColumn4.Header.VisiblePosition = 2;
            ultraGridColumn4.Width = 83;
            appearance86.TextHAlignAsString = "Right";
            ultraGridColumn5.CellAppearance = appearance86;
            ultraGridColumn5.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            appearance87.TextHAlignAsString = "Center";
            ultraGridColumn5.Header.Appearance = appearance87;
            ultraGridColumn5.Header.Caption = "가정야간사용";
            ultraGridColumn5.Header.VisiblePosition = 3;
            ultraGridColumn5.Width = 76;
            appearance88.TextHAlignAsString = "Right";
            ultraGridColumn6.CellAppearance = appearance88;
            ultraGridColumn6.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            appearance89.TextHAlignAsString = "Center";
            ultraGridColumn6.Header.Appearance = appearance89;
            ultraGridColumn6.Header.Caption = "비가정야간사용";
            ultraGridColumn6.Header.VisiblePosition = 4;
            ultraGridColumn6.Width = 90;
            appearance90.TextHAlignAsString = "Center";
            ultraGridColumn7.CellAppearance = appearance90;
            ultraGridColumn7.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            appearance91.TextHAlignAsString = "Center";
            ultraGridColumn7.Header.Appearance = appearance91;
            ultraGridColumn7.Header.Caption = "특별야간사용입력여부";
            ultraGridColumn7.Header.VisiblePosition = 5;
            ultraGridColumn7.Width = 98;
            appearance92.TextHAlignAsString = "Right";
            ultraGridColumn8.CellAppearance = appearance92;
            ultraGridColumn8.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            appearance93.TextHAlignAsString = "Center";
            ultraGridColumn8.Header.Appearance = appearance93;
            ultraGridColumn8.Header.Caption = "특별야간사용(%)";
            ultraGridColumn8.Header.VisiblePosition = 6;
            ultraGridColumn8.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            ultraGridColumn8.Width = 113;
            appearance94.TextHAlignAsString = "Right";
            ultraGridColumn9.CellAppearance = appearance94;
            ultraGridColumn9.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            appearance95.TextHAlignAsString = "Center";
            ultraGridColumn9.Header.Appearance = appearance95;
            ultraGridColumn9.Header.Caption = "특별야간사용";
            ultraGridColumn9.Header.VisiblePosition = 7;
            ultraGridColumn9.Width = 113;
            appearance96.TextHAlignAsString = "Right";
            ultraGridColumn10.CellAppearance = appearance96;
            ultraGridColumn10.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            appearance97.TextHAlignAsString = "Center";
            ultraGridColumn10.Header.Appearance = appearance97;
            ultraGridColumn10.Header.VisiblePosition = 8;
            ultraGridColumn10.Width = 99;
            appearance98.TextHAlignAsString = "Right";
            ultraGridColumn11.CellAppearance = appearance98;
            ultraGridColumn11.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            appearance99.TextHAlignAsString = "Center";
            ultraGridColumn11.Header.Appearance = appearance99;
            ultraGridColumn11.Header.VisiblePosition = 9;
            ultraGridColumn11.Width = 99;
            appearance100.TextHAlignAsString = "Right";
            ultraGridColumn12.CellAppearance = appearance100;
            ultraGridColumn12.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            appearance101.TextHAlignAsString = "Center";
            ultraGridColumn12.Header.Appearance = appearance101;
            ultraGridColumn12.Header.VisiblePosition = 10;
            ultraGridColumn12.Width = 98;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn12});
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance102.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance102.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance102.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance102.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance102;
            appearance103.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance103;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance104.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance104.BackColor2 = System.Drawing.SystemColors.Control;
            appearance104.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance104.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance104;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance105.BackColor = System.Drawing.SystemColors.Window;
            appearance105.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance105;
            appearance106.BackColor = System.Drawing.SystemColors.Highlight;
            appearance106.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance106;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance107.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance107;
            appearance108.BorderColor = System.Drawing.Color.Silver;
            appearance108.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance108;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance109.BackColor = System.Drawing.SystemColors.Control;
            appearance109.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance109.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance109.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance109.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance109;
            appearance110.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance110;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance111.BackColor = System.Drawing.SystemColors.Window;
            appearance111.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance111;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance112.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance112;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ultraGrid1.Location = new System.Drawing.Point(0, 20);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(1022, 235);
            this.ultraGrid1.TabIndex = 12;
            this.ultraGrid1.TabStop = false;
            this.ultraGrid1.Text = "ultraGrid2";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.checkBox1);
            this.panel3.Controls.Add(this.checkBox4);
            this.panel3.Controls.Add(this.checkBox3);
            this.panel3.Controls.Add(this.checkBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1022, 20);
            this.panel3.TabIndex = 5;
            this.panel3.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(0, 0);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(120, 16);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "누수감시산정인자";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Checked = true;
            this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox4.Location = new System.Drawing.Point(316, 0);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(60, 16);
            this.checkBox4.TabIndex = 2;
            this.checkBox4.Text = "누수량";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(226, 0);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(84, 16);
            this.checkBox3.TabIndex = 1;
            this.checkBox3.Text = "일일공급량";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Location = new System.Drawing.Point(124, 0);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(96, 16);
            this.checkBox2.TabIndex = 0;
            this.checkBox2.Text = "야간최소유량";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // chartPanel1
            // 
            this.chartPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.chartPanel1.Controls.Add(this.chart1);
            this.chartPanel1.Controls.Add(this.leakageMaxSetter);
            this.chartPanel1.Controls.Add(this.panel9);
            this.chartPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel1.Location = new System.Drawing.Point(0, 0);
            this.chartPanel1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.chartPanel1.Name = "chartPanel1";
            this.chartPanel1.Size = new System.Drawing.Size(1022, 177);
            this.chartPanel1.TabIndex = 0;
            // 
            // chart1
            // 
            this.chart1.AllSeries.Gallery = ChartFX.WinForms.Gallery.Curve;
            this.chart1.AllSeries.Line.Width = ((short)(1));
            this.chart1.AxisY.Font = new System.Drawing.Font("굴림", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chart1.AxisY.Title.Text = "";
            solidBackground1.AssemblyName = "ChartFX.WinForms.Adornments";
            this.chart1.Background = solidBackground1;
            this.chart1.Border = new ChartFX.WinForms.Adornments.SimpleBorder(ChartFX.WinForms.Adornments.SimpleBorderType.Color, System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(125)))), ((int)(((byte)(138))))));
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.LegendBox.BackColor = System.Drawing.Color.Transparent;
            this.chart1.LegendBox.Border = ChartFX.WinForms.DockBorder.External;
            this.chart1.LegendBox.ContentLayout = ChartFX.WinForms.ContentLayout.Near;
            this.chart1.LegendBox.Dock = ChartFX.WinForms.DockArea.Bottom;
            this.chart1.LegendBox.PlotAreaOnly = false;
            this.chart1.Location = new System.Drawing.Point(0, 27);
            this.chart1.MainPane.Title.Text = "";
            this.chart1.Name = "chart1";
            this.chart1.RandomData.Series = 1;
            seriesAttributes1.Visible = false;
            this.chart1.Series.AddRange(new ChartFX.WinForms.SeriesAttributes[] {
            seriesAttributes1});
            this.chart1.Size = new System.Drawing.Size(993, 150);
            this.chart1.TabIndex = 23;
            this.chart1.TabStop = false;
            // 
            // leakageMaxSetter
            // 
            this.leakageMaxSetter.Dock = System.Windows.Forms.DockStyle.Right;
            this.leakageMaxSetter.Location = new System.Drawing.Point(993, 27);
            this.leakageMaxSetter.Name = "leakageMaxSetter";
            this.leakageMaxSetter.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.leakageMaxSetter.Size = new System.Drawing.Size(29, 150);
            this.leakageMaxSetter.TabIndex = 24;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label1);
            this.panel9.Controls.Add(this.radioButton2);
            this.panel9.Controls.Add(this.radioButton1);
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.chartChange);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1022, 27);
            this.panel9.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(684, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "차트기준변경 : ";
            // 
            // radioButton2
            // 
            this.radioButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(889, 5);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(131, 16);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.Text = "야간파열누수량기준";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(776, 5);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(107, 16);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "야간사용량기준";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.fFlowCheck);
            this.panel10.Controls.Add(this.mFlowCheck);
            this.panel10.Location = new System.Drawing.Point(127, 1);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(279, 24);
            this.panel10.TabIndex = 1;
            // 
            // fFlowCheck
            // 
            this.fFlowCheck.AutoSize = true;
            this.fFlowCheck.Checked = true;
            this.fFlowCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fFlowCheck.Location = new System.Drawing.Point(153, 5);
            this.fFlowCheck.Name = "fFlowCheck";
            this.fFlowCheck.Size = new System.Drawing.Size(120, 16);
            this.fFlowCheck.TabIndex = 1;
            this.fFlowCheck.Text = "보정야간최소유량";
            this.fFlowCheck.UseVisualStyleBackColor = true;
            // 
            // mFlowCheck
            // 
            this.mFlowCheck.AutoSize = true;
            this.mFlowCheck.Location = new System.Drawing.Point(4, 5);
            this.mFlowCheck.Name = "mFlowCheck";
            this.mFlowCheck.Size = new System.Drawing.Size(144, 16);
            this.mFlowCheck.TabIndex = 0;
            this.mFlowCheck.Text = "이동평균야간최소유량";
            this.mFlowCheck.UseVisualStyleBackColor = true;
            // 
            // chartChange
            // 
            this.chartChange.FormattingEnabled = true;
            this.chartChange.Location = new System.Drawing.Point(0, 4);
            this.chartChange.Name = "chartChange";
            this.chartChange.Size = new System.Drawing.Size(121, 20);
            this.chartChange.TabIndex = 0;
            // 
            // searchBox1
            // 
            this.searchBox1.AutoSize = true;
            this.searchBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox1.Location = new System.Drawing.Point(10, 10);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.Parameters = ((System.Collections.Hashtable)(resources.GetObject("searchBox1.Parameters")));
            this.searchBox1.Size = new System.Drawing.Size(1022, 111);
            this.searchBox1.TabIndex = 45;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 618);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmMain";
            this.Text = "누수량 산정 결과 조회";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tablePanel1.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.chartPanel1.ResumeLayout(false);
            this.chartPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leakageMaxSetter)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private WaterNet.WV_Common.form.SearchBox searchBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button chartVisibleBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.Button tableVisibleBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel chartPanel1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.CheckBox fFlowCheck;
        private System.Windows.Forms.CheckBox mFlowCheck;
        private System.Windows.Forms.ComboBox chartChange;
        private System.Windows.Forms.Panel tablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Panel panel17;
        private ChartFX.WinForms.Chart chart1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.Button settingBtn;
        private System.Windows.Forms.Button leakageCompareBtn;
        private System.Windows.Forms.Button updateBtn;
        private Infragistics.Win.UltraWinEditors.UltraTrackBar leakageMaxSetter;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label1;
    }
}