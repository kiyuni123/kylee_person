﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ChartFX.WinForms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WaterNetCore;
using WaterNet.WV_Common.form;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.manager;
using WaterNet.WV_Common.util;
using WaterNet.WV_LeakageManage.vo;
using WaterNet.WV_LeakageManage.work;
using WaterNet.WV_Common.enum1;
using Infragistics.Win.UltraWinGrid.ExcelExport;
using EMFrame.log;

namespace WaterNet.WV_LeakageManage.form
{
    public partial class frmMain : Form
    {
        private IMapControlWV mainMap = null;
        private TableLayout tableLayout = null;
        private ChartManager chartManager = null;
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        /// <summary>
        /// 검색박스 반환 객체
        /// </summary>
        public SearchBox SearchBox
        {
            get
            {
                return this.searchBox1;
            }
        }

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        /// <summary>
        /// 기본 생성자
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmMain_Load);
        }

        /// <summary>
        /// 폼의 로드가 완료되면 폼의 하위객체, 차트, 그리드, 이벤트를 설정한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================

            object o = EMFrame.statics.AppStatic.USER_MENU["누수량산정결과조회ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
            }

            //===========================================================================

            this.InitializeCode();
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
        }

        /// <summary>
        /// 코드데이터 초기화 설정
        /// </summary>
        private void InitializeCode()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("select code cd, code_name cdnm from cm_code where pcode = '2003' order by to_number(orderby)");
            FormManager.SetComboBoxEX(chartChange, sql.ToString(), false);
        }

        /// <summary>
        /// 폼 초기화 설정
        /// </summary>
        private void InitializeForm()
        {
            this.searchBox1.InitializeSearchBox();
            this.tableLayout = new TableLayout(tableLayoutPanel1, chartPanel1, tablePanel1, chartVisibleBtn, tableVisibleBtn);
            this.tableLayout.DefaultChartHeight = 40;
            this.tableLayout.DefaultTableHeight = 60;

            this.searchBox1.Text = "검색조건";
            this.searchBox1.IntervalType = INTERVAL_TYPE.DATE;
            this.searchBox1.IntervalText = "검색기간";

            //검색기간을 기본 3개월로 설정한다.
            this.searchBox1.StartDateObject.Value = ((DateTime)this.searchBox1.EndDateObject.Value).AddMonths(-3).ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(0, "yyyy-MM-dd");

            this.chart1.Commands[CommandId.BringSeriesToFront].Enabled = false;
            this.chart1.Commands[CommandId.SendSeriesToBack].Enabled = false;
        }

        /// <summary>
        /// 그리드 초기화 설정
        /// </summary>
        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.HeaderPlacement = HeaderPlacement.Default;
            this.ultraGrid1.DisplayLayout.ViewStyle = ViewStyle.MultiBand;
            
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SPECIAL_USED_PERCENT"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SPECIAL_USED_PERCENT");
                valueList.ValueListItems.Add("0", "0");
                valueList.ValueListItems.Add("1", "1");
                valueList.ValueListItems.Add("2", "2");
                valueList.ValueListItems.Add("3", "3");
                valueList.ValueListItems.Add("4", "4");
                valueList.ValueListItems.Add("5", "5");
                valueList.ValueListItems.Add("6", "6");
                valueList.ValueListItems.Add("7", "7");
                valueList.ValueListItems.Add("8", "8");
                valueList.ValueListItems.Add("9", "9");
                valueList.ValueListItems.Add("10", "10");
            }
            this.ultraGrid1.DisplayLayout.Bands[1].Columns["SPECIAL_USED_PERCENT"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SPECIAL_USED_PERCENT"];
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
        }

        /// <summary>
        /// 이벤트 초기화 설정
        /// </summary>
        private void InitializeEvent()
        {
            //동진 수정_2012.6.12
            this.ultraGrid1.BeforeColumnChooserDisplayed += new BeforeColumnChooserDisplayedEventHandler(ultraGrid1_BeforeColumnChooserDisplayed);  //동진_6.12
            //=======================

            this.excelManager.Exporter.OutliningStyle = OutliningStyle.None;
            this.excelManager.Exporter.BandSpacing = 0;
            this.excelManager.Exporter.RowExporting += new RowExportingEventHandler(Exporter_RowExporting);
            this.excelManager.Exporter.HeaderRowExporting += new HeaderRowExportingEventHandler(Exporter_HeaderRowExporting);
            this.excelBtn.Click += new EventHandler(ExportExcel_EventHandler);

            this.searchBtn.Click += new EventHandler(SelectLeakageManageRanking_EventHandler);

            this.chartChange.SelectedIndexChanged += new EventHandler(chartChange_EventHandler);
            this.mFlowCheck.CheckedChanged += new EventHandler(chartChange_EventHandler);
            this.fFlowCheck.CheckedChanged += new EventHandler(chartChange_EventHandler);
            this.radioButton1.CheckedChanged += new EventHandler(chartChange_EventHandler);
            this.radioButton2.CheckedChanged += new EventHandler(chartChange_EventHandler);

            this.leakageMaxSetter.ValueChanged += new EventHandler(leakageMaxSetter_ValueChanged);

            this.updateBtn.Click += new EventHandler(UpdateLeakageManageOption_EventHandler);

            this.settingBtn.Click += new EventHandler(settingBtn_Click);
            this.leakageCompareBtn.Click += new EventHandler(leakageCompareBtn_Click);

            this.checkBox1.CheckedChanged += new EventHandler(GridColumnsVisible_Changed);
            this.checkBox2.CheckedChanged += new EventHandler(GridColumnsVisible_Changed);
            this.checkBox3.CheckedChanged += new EventHandler(GridColumnsVisible_Changed);
            this.checkBox4.CheckedChanged += new EventHandler(GridColumnsVisible_Changed);
        }

        //=========================================================
        //
        //                    동진 수정_2012.6.12
        //                     ColumnChooser 제어
        //=========================================================
        void ultraGrid1_BeforeColumnChooserDisplayed(object sender, BeforeColumnChooserDisplayedEventArgs e)
        {
            e.Dialog.ColumnChooserControl.DisplayLayout.Override.Reset();
            e.Dialog.ColumnChooserControl.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;  //원클릭( 원래는 더블클릭이였음)
            e.Dialog.ColumnChooserControl.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
        
        }
        //=========================================================================

        private void Exporter_HeaderRowExporting(object sender, HeaderRowExportingEventArgs e)
        {
            if (e.CurrentOutlineLevel != 0)
            {
                e.Cancel = true;
            }
        }

        private void Exporter_RowExporting(object sender, RowExportingEventArgs e)
        {
            if (e.CurrentOutlineLevel != 0)
            {
                e.Cancel = true;
            }
        }



        /// <summary>
        /// 엑셀파일 다운로드이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportExcel_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                this.excelManager.AddWorksheet(this.chart1, "누수량산정결과", 0, 0, 9, 20);
                this.excelManager.AddWorksheet(this.ultraGrid1, "누수량산정결과", 10, 0);
                this.excelManager.Save("누수량산정결과", true);
                //this.excelManager.Open("누수량산정결과",WORKBOOK_TYPE.CONTENT);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 로우활성화 이벤트 핸들러
        /// 블록별 우선순위에서 로우를 클릭했을때..
        /// 선택한 블록을 선택상태로 지정해준다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            grid.Selected.Rows.Clear();
            grid.ActiveRow.Selected = true;
        }

        /// <summary>
        /// 차트변경 이벤트 핸들러
        /// 야간최소유량기준, 누수량기준, ESPB기준등으로 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chartChange_EventHandler(object sender, EventArgs e)
        {
            if (sender.Equals(this.chartChange))
            {
                this.InitializeChartSetting();
            }
            else
            {
                this.ChangeChartSeries();
            }
            //this.ChangeChartSeries();
            //this.InitializeChartSetting();
        }

        /// <summary>
        /// 검색버튼 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectLeakageManageRanking_EventHandler(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.SelectLeakageManage(this.searchBox1.InitializeParameter().Parameters);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        /// <summary>
        /// 누수관리 검색
        /// </summary>
        /// <param name="parameter"></param>
        public void SelectLeakageManage(Hashtable parameter)
        {
            if (parameter["FTR_CODE"].ToString() == "BZ001")
            {
                MessageBox.Show("검색 대상 블록을 선택해야 합니다.");
                return;
            }
            this.mainMap.MoveToBlock(parameter);
            this.maxLeakgeValue = LeakageManageWork.GetInstance().SelectLeakageMaxValue(parameter);
            this.ultraGrid1.DataSource = LeakageManageWork.GetInstance().SelectLeakageManage(parameter);
            this.InitializeChartSetting();
        }

        //series 0 : 이동평균야간최소유량
        //series 1 : 필터링야간최소유량
        //series 2 : 야간파열누수량
        //series 3 : 야간배경누수량
        //series 4 : 야간사용량
        //series 5 : 일누수/급수전수
        //series 6 : 일누수/배수관연장
        //series 7 : 등가급수관파열

        private double maxFlowValue = 0;
        private double maxLeakgeValue = 0;

        private SeriesAttributes f0;
        private SeriesAttributes f1;
        private SeriesAttributes f2;
        private SeriesAttributes f3;
        private SeriesAttributes f4;

        /// <summary>
        /// 검색 데이터 변경시 차트 세팅/ 데이터 초기화 
        /// </summary>
        private void InitializeChartSetting()
        {

            this.maxFlowValue = 0;

            IList<LeakageCalculationVO> leakageList = (IList<LeakageCalculationVO>)this.ultraGrid1.DataSource;

            Chart chart = this.chartManager.Items[0];
            this.chartManager.AllClear(chart);
            chart.Data.Clear();
            //chart.Data.Series = 8;

            if (this.chartChange.SelectedValue.ToString() == "1001")
            {
                chart.Data.Series = 5;
                chart.Data.Points = leakageList.Count;  
                foreach (LeakageCalculationVO leakageVO in leakageList)
                {
                    int pointIndex = leakageList.IndexOf(leakageVO);
                    chart.AxisX.Labels[pointIndex] = leakageVO.DATEE.ToShortDateString();

                    chart.Data[0, pointIndex] = Utils.ToDouble(leakageVO.M_AVERAGE);
                    chart.Data[1, pointIndex] = Utils.ToDouble(leakageVO.FILTERING);
                    chart.Data[2, pointIndex] = Utils.ToDouble(leakageVO.AMTUSE_NT);
                    chart.Data[3, pointIndex] = Utils.ToDouble(leakageVO.NT_BACK_LEAKS);
                    chart.Data[4, pointIndex] = Utils.ToDouble(leakageVO.NT_BURST_LEAKS);

                    //max값을 보존.
                    if (this.maxFlowValue < leakageVO.FILTERING)
                    {
                        this.maxFlowValue = leakageVO.FILTERING;
                    }
                }
                chart.Series[0].Gallery = Gallery.Lines;
                chart.Series[0].Line.Width = 1;
                chart.Series[0].Stacked = true;
                chart.Series[0].MarkerSize = 0;

                chart.Series[1].Gallery = Gallery.Lines;
                chart.Series[1].Line.Width = 1;
                chart.Series[1].Stacked = true;
                chart.Series[1].MarkerSize = 0;

                chart.Series[2].Gallery = Gallery.Area;
                chart.Series[2].Stacked = true;
                chart.Series[2].MarkerSize = 1;

                chart.Series[3].Gallery = Gallery.Area;
                chart.Series[3].Stacked = true;
                chart.Series[3].MarkerSize = 1;

                chart.Series[4].Gallery = Gallery.Area;
                chart.Series[4].Stacked = true;
                chart.Series[4].MarkerSize = 1;

                chart.Series[0].Text = "이동평균야간최소유량(㎥/h)";
                chart.Series[1].Text = "보정야간최소유량(㎥/h)";

                chart.Series[2].Text = "야간사용량(㎥/h)";
                chart.Series[3].Text = "야간배경누수량(㎥/h)";
                chart.Series[4].Text = "야간파열누수량(㎥/h)";


                chart.AxisY.Title.Text = "유량(㎥/h)";

                chart.Series[0].Color = Color.FromArgb(38, 100, 193);
                chart.Series[1].Color = Color.FromArgb(199, 56, 0);
                chart.Series[2].Color = Color.FromArgb(70, 177, 194);
                chart.Series[3].Color = Color.FromArgb(118, 200, 45);
                chart.Series[4].Color = Color.FromArgb(236, 179, 70);

                f0 = chart.Series[0];
                f1 = chart.Series[1];
                f2 = chart.Series[2];
                f3 = chart.Series[3];
                f4 = chart.Series[4];

                this.chart1.Series[0].AxisY.Max = double.MaxValue;

                this.leakageMaxSetter.MaxValue = Convert.ToInt32( this.maxLeakgeValue + 5);
                this.leakageMaxSetter.Value = Convert.ToInt32( this.maxLeakgeValue );
            }
            if (this.chartChange.SelectedValue.ToString() == "1002")
            {
                chart.Data.Series = 2;
                chart.Data.Points = leakageList.Count;
                foreach (LeakageCalculationVO leakageVO in leakageList)
                {
                    int pointIndex = leakageList.IndexOf(leakageVO);
                    chart.AxisX.Labels[pointIndex] = leakageVO.DATEE.ToShortDateString();

                    chart.Data[0, pointIndex] = Utils.ToDouble(leakageVO.GUPSUJUN_LEAKS_DAY);
                    chart.Data[1, pointIndex] = Utils.ToDouble(leakageVO.PIPE_LENEN_LEAKS_DAY);

                }
                chart.AxesY.Add(new AxisY());

                chart.Series[0].Gallery = Gallery.Curve;

                chart.Series[0].Line.Width = 1;
                chart.Series[0].MarkerSize = 1;

                chart.Series[1].Gallery = Gallery.Curve;
                chart.Series[1].Line.Width = 1;
                chart.Series[1].MarkerSize = 1;
                chart.Series[1].AxisY = chart1.AxesY[1];

                chart.Series[0].Text = "일누수량/급수전(㎥/전/일)";
                chart.Series[1].Text = "일누수량/배수관연장(㎥/km/일)";

                chart.AxisY.Title.Text = "일누수량/급수전(㎥/전/일)";
                chart.AxisY2.Title.Text = "일누수량/배수관연장(㎥/km/일)";

                chart.AxesY[0].LabelsFormat.Format = AxisFormat.Number;
                chart.AxesY[0].LabelsFormat.CustomFormat = "####,####.00";

                this.chart1.Series[0].AxisY.Max = double.MaxValue;
            }
            if (this.chartChange.SelectedValue.ToString() == "1003")
            {
                chart.Data.Series = 1;
                chart.Data.Points = leakageList.Count;
                foreach (LeakageCalculationVO leakageVO in leakageList)
                {
                    int pointIndex = leakageList.IndexOf(leakageVO);
                    chart.AxisX.Labels[pointIndex] = leakageVO.DATEE.ToShortDateString();

                    chart.Data[0, pointIndex] = Utils.ToDouble(leakageVO.EQUIP_BURST);

                }
                chart.Series[0].Gallery = Gallery.Bar;
                chart.Series[0].Line.Width = 1;
                chart.Series[0].MarkerSize = 1;

                chart.Series[0].Text = "등가급수관파열";
                chart.AxisY.Title.Text = "등가급수관파열";

                this.chart1.Series[0].AxisY.Max = double.MaxValue;
            }

            chart.Refresh();
            this.ChangeChartSeries();
        }

        /// <summary>
        /// 차트 시리즈 변경
        /// </summary>
        private void ChangeChartSeries()
        {
            Chart chart = this.chartManager.Items[0];

            this.leakageMaxSetter.Enabled = false;

            if (this.chartChange.SelectedValue.ToString() == "1001")
            {
                //chart.AllSeries.Stacked = Stacked.Normal;

                //chart.Series[2].Stacked = true;
                //chart.Series[3].Stacked = true;
                //chart.Series[4].Stacked = true;

                leakageMaxSetter.Enabled = true;

                if (mFlowCheck.Checked)
                {
                    chart.Series[0].Visible = true;
                }
                else
                {
                    chart.Series[0].Visible = false;
                }

                if (fFlowCheck.Checked)
                {
                    chart.Series[1].Visible = true;
                }
                else
                {
                    chart.Series[1].Visible = false;
                }

                chart.Series[2].Visible = true;
                chart.Series[3].Visible = true;
                chart.Series[4].Visible = true;

                if (chart.Series[0].AxisY.Sections.Count > 0)
                {
                    chart.Series[0].AxisY.Sections[0].Visible = true;
                }

                //파열누수량기준
                if (radioButton2.Checked)
                {
                    chart.Series.RemoveAt(0);
                    chart.Series.RemoveAt(0);
                    chart.Series.RemoveAt(0);
                    chart.Series.RemoveAt(0);
                    chart.Series.RemoveAt(0);
                    chart.Series.Insert(0, f0);
                    chart.Series.Insert(1, f1);
                    chart.Series.Insert(2, f4);
                    chart.Series.Insert(3, f2);
                    chart.Series.Insert(4, f3);
                }
                //야간사용량기준
                else if (radioButton1.Checked)
                {
                    chart.Series.RemoveAt(0);
                    chart.Series.RemoveAt(0);
                    chart.Series.RemoveAt(0);
                    chart.Series.RemoveAt(0);
                    chart.Series.RemoveAt(0);
                    chart.Series.Insert(0, f0);
                    chart.Series.Insert(1, f1);
                    chart.Series.Insert(2, f2);
                    chart.Series.Insert(3, f3);
                    chart.Series.Insert(4, f4);
                }
            }
            else if (this.chartChange.SelectedValue.ToString() == "1002")
            {
                if (chart.Series[0].AxisY.Sections.Count > 0)
                {
                    chart.Series[0].AxisY.Sections[0].Visible = false;
                }
            }
            else if (this.chartChange.SelectedValue.ToString() == "1003")
            {
                if (chart.Series[0].AxisY.Sections.Count > 0)
                {
                    chart.Series[0].AxisY.Sections[0].Visible = false;
                }
            }

            foreach (AxisY ax in chart.AxesY)
            {
                ax.Min = 0;
            }
        }

        ///// <summary>
        ///// 검색 데이터 변경시 차트 세팅/ 데이터 초기화 
        ///// </summary>
        //private void InitializeChartSetting()
        //{
        //    this.maxFlowValue = 0;

        //    IList<LeakageCalculationVO> leakageList = (IList<LeakageCalculationVO>)this.ultraGrid1.DataSource;

        //    Chart chart = this.chartManager.Items[0];
            
        //    chart.Data.Series = 8;
        //    chart.Data.Points = leakageList.Count;

        //    foreach (LeakageCalculationVO leakageVO in leakageList)
        //    {
        //        int pointIndex = leakageList.IndexOf(leakageVO);
        //        chart.AxisX.Labels[pointIndex] = leakageVO.DATEE.ToShortDateString();

        //        chart.Data[0, pointIndex] = Utils.ToDouble(leakageVO.M_AVERAGE);
        //        chart.Data[1, pointIndex] = Utils.ToDouble(leakageVO.FILTERING);
        //        chart.Data[2, pointIndex] = Utils.ToDouble(leakageVO.AMTUSE_NT);
        //        chart.Data[3, pointIndex] = Utils.ToDouble(leakageVO.NT_BACK_LEAKS);
        //        chart.Data[4, pointIndex] = Utils.ToDouble(leakageVO.NT_BURST_LEAKS);

        //        //chart.Data[2, pointIndex] = Utils.ToDouble(leakageVO.FILTERING - leakageVO.LEAKS_NT);
        //        //chart.Data[3, pointIndex] = Utils.ToDouble(leakageVO.NT_BACK_LEAKS + leakageVO.FILTERING - leakageVO.LEAKS_NT);
        //        //chart.Data[4, pointIndex] = Utils.ToDouble(leakageVO.NT_BURST_LEAKS + leakageVO.NT_BACK_LEAKS + leakageVO.FILTERING - leakageVO.LEAKS_NT);
                
        //        chart.Data[5, pointIndex] = Utils.ToDouble(leakageVO.GUPSUJUN_LEAKS_DAY);
        //        chart.Data[6, pointIndex] = Utils.ToDouble(leakageVO.PIPE_LENEN_LEAKS_DAY);
        //        chart.Data[7, pointIndex] = Utils.ToDouble(leakageVO.EQUIP_BURST);

        //        //max값을 보존.
        //        if (this.maxFlowValue < leakageVO.FILTERING)
        //        {
        //            this.maxFlowValue = leakageVO.FILTERING;
        //        }
        //    }

        //    chart.Series[0].Gallery = Gallery.Lines;
        //    chart.Series[0].Line.Width = 1;
        //    chart.Series[0].MarkerSize = 0;

        //    chart.Series[1].Gallery = Gallery.Lines;
        //    chart.Series[1].Line.Width = 1;
        //    chart.Series[1].MarkerSize = 0;

        //    chart.Series[2].Gallery = Gallery.Area;
        //    chart.Series[2].MarkerSize = 1;

        //    chart.Series[3].Gallery = Gallery.Area;
        //    chart.Series[3].MarkerSize = 1;

        //    chart.Series[4].Gallery = Gallery.Area;
        //    chart.Series[4].MarkerSize = 1;

        //    chart.AxesY.Add(new AxisY());

        //    chart.Series[5].Gallery = Gallery.Curve;
            
        //    chart.Series[5].Line.Width = 1;
        //    chart.Series[5].MarkerSize = 1;

        //    chart.Series[6].Gallery = Gallery.Curve;
        //    chart.Series[6].Line.Width = 1;
        //    chart.Series[6].MarkerSize = 1;
        //    chart.Series[6].AxisY = chart1.AxesY[1];

        //    chart.Series[7].Gallery = Gallery.Bar;
        //    chart.Series[7].Line.Width = 1;
        //    chart.Series[7].MarkerSize = 1;

        //    chart.Series[0].Text = "이동평균야간최소유량(㎥/h)";
        //    chart.Series[1].Text = "보정야간최소유량(㎥/h)";

        //    chart.Series[2].Text = "야간사용량(㎥/h)";
        //    chart.Series[3].Text = "야간배경누수량(㎥/h)";
        //    chart.Series[4].Text = "야간파열누수량(㎥/h)";

        //    chart.Series[5].Text = "일누수량/급수전(㎥/전/일)";
        //    chart.Series[6].Text = "일누수량/배수관연장(㎥/km/일)";
        //    chart.Series[7].Text = "등가급수관파열";


        //    chart.Series[0].Color = Color.FromArgb(38, 100, 193);
        //    chart.Series[1].Color = Color.FromArgb(199, 56, 0);
        //    chart.Series[2].Color = Color.FromArgb(70, 177, 194);
        //    chart.Series[3].Color = Color.FromArgb(118, 200, 45);
        //    chart.Series[4].Color = Color.FromArgb(236, 179, 70);

        //    f1 = chart.Series[2];
        //    f2 = chart.Series[3];
        //    f3 = chart.Series[4];

        //    this.leakageMaxSetter.MaxValue = this.maxLeakgeValue + 5;
        //    this.leakageMaxSetter.Value = this.maxLeakgeValue;

        //    chart.Refresh();
        //    this.ChangeChartSeries();
        //}

        ///// <summary>
        ///// 차트 시리즈 변경
        ///// </summary>
        //private void ChangeChartSeries()
        //{
        //    Chart chart = this.chartManager.Items[0];

        //    foreach (SeriesAttributes series in chart.Series)
        //    {
        //        series.Visible = false;
        //    }

        //    this.leakageMaxSetter.Enabled = false;

        //    if (this.chartChange.SelectedValue.ToString() == "1001")
        //    {
        //        //chart.AllSeries.Stacked = Stacked.Normal;

        //        chart.Series[2].Stacked = true;
        //        chart.Series[3].Stacked = true;
        //        chart.Series[4].Stacked = true;

        //        chart.AxisY.Title.Text = "유량(㎥/h)";
        //        leakageMaxSetter.Enabled = true;

        //        if (mFlowCheck.Checked)
        //        {
        //            chart.Series[0].Visible = true;
        //        }

        //        if (fFlowCheck.Checked)
        //        {
        //            chart.Series[1].Visible = true;
        //        }

        //        chart.Series[2].Visible = true;
        //        chart.Series[3].Visible = true;
        //        chart.Series[4].Visible = true;

        //        if (chart.Series[0].AxisY.Sections.Count > 0)
        //        {
        //            chart.Series[0].AxisY.Sections[0].Visible = true;
        //        }

        //        //파열누수량기준
        //        if (radioButton2.Checked)
        //        {
        //            chart.Series.RemoveAt(2);
        //            chart.Series.RemoveAt(2);
        //            chart.Series.RemoveAt(2);
        //            chart.Series.Insert(2, f3);
        //            chart.Series.Insert(3, f1);
        //            chart.Series.Insert(4, f2);
        //        }
        //        //야간사용량기준
        //        else if (radioButton1.Checked)
        //        {
        //            chart.Series.RemoveAt(2);
        //            chart.Series.RemoveAt(2);
        //            chart.Series.RemoveAt(2);
        //            chart.Series.Insert(2, f1);
        //            chart.Series.Insert(3, f2);
        //            chart.Series.Insert(4, f3);
        //        }

        //        chart.AxesY[1].Visible = false;
        //    }
        //    else if (this.chartChange.SelectedValue.ToString() == "1002")
        //    {
        //        chart.AxisY.Title.Text = "일누수량/급수전(㎥/전/일)";
        //        chart.AxisY2.Title.Text = "일누수량/배수관연장(㎥/km/일)";
        //        chart.Series[5].Visible = true;
        //        chart.Series[6].Visible = true;

        //        if (chart.Series[0].AxisY.Sections.Count > 0)
        //        {
        //            chart.Series[0].AxisY.Sections[0].Visible = false;
        //        }
        //        chart.AxesY[1].Visible = true;
        //    }
        //    else if (this.chartChange.SelectedValue.ToString() == "1003")
        //    {
        //        chart.AxisY.Title.Text = "등가급수관파열";
        //        chart.Series[7].Visible = true;

        //        if (chart.Series[0].AxisY.Sections.Count > 0)
        //        {
        //            chart.Series[0].AxisY.Sections[0].Visible = false;
        //        }
        //        chart.AxesY[1].Visible = false; ;
        //    }

        //    foreach (AxisY ax in chart.AxesY)
        //    {
        //        ax.Min = 0;
        //    }
        //}

        /// <summary>
        /// 누수감시 한계치 설정값 변경 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void leakageMaxSetter_ValueChanged(object sender, EventArgs e)
        {
            this.chart1.Series[0].AxisY.Sections.Clear();
            this.chart1.Series[0].AxisY.Sections.Add(new AxisSection(this.leakageMaxSetter.Value, this.leakageMaxSetter.Value + 1, Color.Red));

            if (this.leakageMaxSetter.Value == this.leakageMaxSetter.MaxValue)
            {
                this.leakageMaxSetter.MaxValue += 5;
            }

            if (this.leakageMaxSetter.Value < Convert.ToInt32(this.maxLeakgeValue))
            {
                this.leakageMaxSetter.MaxValue = Convert.ToInt32(this.maxLeakgeValue);
            }

            if (this.maxFlowValue < this.leakageMaxSetter.Value)
            {
                this.chart1.Series[0].AxisY.Max = this.leakageMaxSetter.Value + 5;
            }
            else
            {
                this.chart1.Series[0].AxisY.Max = this.maxFlowValue;
            }

            if (chartChange.SelectedIndex == 0)
            {
                this.chart1.Series[0].AxisY.Sections[0].Visible = true;
            }
        }

        /// <summary>
        /// 현재 선택된 블럭의 지역코드,
        /// </summary>
        private string currentDetailBlockLocationCode = string.Empty;

        public string CurrentDetailBlockLocationCode
        {
            get
            {
                return this.currentDetailBlockLocationCode;
            }
        }

        /// <summary>
        /// 누수량관리 옵션설정값 변경
        /// </summary>
        /// <param name="changeValue"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public void SetLeakageDefaulutOption(LeakageOptionVO changeValue, DateTime startDate, DateTime endDate)
        {
            if (this.ultraGrid1.DataSource == null)
            {
                return;
            }

            IList<LeakageCalculationVO> leakageList = (IList<LeakageCalculationVO>)this.ultraGrid1.DataSource;
            foreach (LeakageCalculationVO leakageVO in leakageList)
            {
                if (leakageVO.DATEE >= startDate && leakageVO.DATEE <= endDate)
                {
                    leakageVO.LeakageOptionVO[0].ValueCopy(changeValue);

                    //주말여부체크
                    if (leakageVO.DATEE.DayOfWeek != DayOfWeek.Sunday && leakageVO.DATEE.DayOfWeek != DayOfWeek.Saturday)
                    {
                        leakageVO.LeakageOptionVO[0].JUMAL_USED = 0;
                    }
                }
            }
            this.ultraGrid1.Refresh();
            this.InitializeChartSetting();
        }

        /// <summary>
        /// 누수량관리 옵션설정값 저장
        /// </summary>
        private void UpdateLeakageManageOption_EventHandler(object sender, EventArgs e)
        {
            if (this.ultraGrid1.Rows.Count == 0 || this.ultraGrid1.DataSource == null)
            {
                return;
            }

            //Hashtable parameter = new Hashtable();
            //parameter["LOC_CODE"] = this.currentDetailBlockLocationCode;

            Hashtable parameter = this.searchBox1.Parameters;
            parameter["MAX_LEAKAGE"] = this.leakageMaxSetter.Value;

            LeakageManageWork.GetInstance().UpdateLeakageMaxValue(parameter);
            LeakageManageWork.GetInstance().UpdateLeakageManageOption(this.ultraGrid1.DataSource);
        }

        /// <summary>
        /// 옵션설정 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void settingBtn_Click(object sender, EventArgs e)
        {
            Hashtable parameter = this.searchBox1.Parameters;

            if (parameter == null)
            {
                return;
            }

            if (this.ultraGrid1.Rows.Count == 0 || this.ultraGrid1.DataSource == null)
            {
                return;
            }

            frmOptionSetting oForm = new frmOptionSetting();
            oForm.Owner = this;
            oForm.Show();
        }

        /// <summary>
        /// 년간누수량비교 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void leakageCompareBtn_Click(object sender, EventArgs e)
        {
            Hashtable parameter = this.searchBox1.Parameters;

            if (parameter == null)
            {
                return;
            }

            if (this.ultraGrid1.Rows.Count == 0 || this.ultraGrid1.DataSource == null)
            {
                return;
            }

            frmLeakageCompare oForm = new frmLeakageCompare();
            oForm.Owner = this;
            oForm.Show();
        }

        /// <summary>
        /// 그리드 컬럼 숨김/ 보임
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridColumnsVisible_Changed(object sender, EventArgs e)
        {
            ColumnsCollection columns = this.ultraGrid1.DisplayLayout.Bands[0].Columns;

            if (checkBox1.Checked)
            {
                columns["M_AVERAGE"].Hidden = false;
                columns["FILTERING"].Hidden = false;
                columns["PIP_LEN"].Hidden = false;
                columns["GUPSUJUNSU"].Hidden = false;
                columns["GAJUNG_GAGUSU"].Hidden = false;
                columns["BGAJUNG_GAGUSU"].Hidden = false;
                columns["GAJUNG_AMTUSE_NT"].Hidden = false;
                columns["BGAJUNG_AMTUSE_NT"].Hidden = false;
                columns["SPECIAL_AMTUSE"].Hidden = false;
                columns["SPECIAL_AMTUSE_NT"].Hidden = false;
                columns["JUMAL_AMTUSE"].Hidden = false;
                columns["AMTUSE_NT"].Hidden = false;
                columns["LEAKS_NT"].Hidden = false;
                columns["INFPNT_HPRES_DAVG"].Hidden = false;
                columns["INFPNT_HPRES_PAVG"].Hidden = false;
                columns["AVG_HPRES_DAVG"].Hidden = false;
                columns["AVG_HPRES_PAVG"].Hidden = false;
                columns["NDF"].Hidden = false;
                columns["LEAKS_DAVG"].Hidden = false;
                columns["BESU_LENEN_LEAKS"].Hidden = false;
                columns["GUPSUJUN_LEAKS"].Hidden = false;
                columns["OKNE_LENEN_LEAKS"].Hidden = false;
                columns["NT_BACK_LEAKS"].Hidden = false;
                columns["NT_BURST_LEAKS"].Hidden = false;
                columns["GUPSUJUN_LEAKS_DAY"].Hidden = false;
                columns["PIPE_LENEN_LEAKS_DAY"].Hidden = false;
                columns["M_AVERAGE"].Hidden = false;
                columns["FILTERING"].Hidden = false;
            }
            else if (!checkBox1.Checked)
            {
                columns["M_AVERAGE"].Hidden = true;
                columns["FILTERING"].Hidden = true;
                columns["PIP_LEN"].Hidden = true;
                columns["GUPSUJUNSU"].Hidden = true;
                columns["GAJUNG_GAGUSU"].Hidden = true;
                columns["BGAJUNG_GAGUSU"].Hidden = true;
                columns["GAJUNG_AMTUSE_NT"].Hidden = true;
                columns["BGAJUNG_AMTUSE_NT"].Hidden = true;
                columns["SPECIAL_AMTUSE"].Hidden = true;
                columns["SPECIAL_AMTUSE_NT"].Hidden = true;
                columns["JUMAL_AMTUSE"].Hidden = true;
                columns["AMTUSE_NT"].Hidden = true;
                columns["LEAKS_NT"].Hidden = true;
                columns["INFPNT_HPRES_DAVG"].Hidden = true;
                columns["INFPNT_HPRES_PAVG"].Hidden = true;
                columns["AVG_HPRES_DAVG"].Hidden = true;
                columns["AVG_HPRES_PAVG"].Hidden = true;
                columns["NDF"].Hidden = true;
                columns["LEAKS_DAVG"].Hidden = true;
                columns["BESU_LENEN_LEAKS"].Hidden = true;
                columns["GUPSUJUN_LEAKS"].Hidden = true;
                columns["OKNE_LENEN_LEAKS"].Hidden = true;
                columns["NT_BACK_LEAKS"].Hidden = true;
                columns["NT_BURST_LEAKS"].Hidden = true;
                columns["GUPSUJUN_LEAKS_DAY"].Hidden = true;
                columns["PIPE_LENEN_LEAKS_DAY"].Hidden = true;
            }

            if (this.checkBox2.Checked)
            {
                columns["M_AVERAGE"].Hidden = false;
                columns["FILTERING"].Hidden = false;
            }
            else if (!this.checkBox2.Checked)
            {
                columns["M_AVERAGE"].Hidden = true;
                columns["FILTERING"].Hidden = true;
            }

            if (this.checkBox3.Checked)
            {
                columns["DAY_SUPPLIED"].Hidden = false;
            }
            else if (!this.checkBox3.Checked)
            {
                columns["DAY_SUPPLIED"].Hidden = true;
            }

            if (this.checkBox4.Checked)
            {
                columns["EQUIP_BURST"].Hidden = false;
                columns["GUPSUJUN_LEAKS_NT"].Hidden = false;
                columns["PIPE_LENEN_LEAKS_NT"].Hidden = false;
            }
            else if (!this.checkBox4.Checked)
            {
                columns["EQUIP_BURST"].Hidden = true;
                columns["GUPSUJUN_LEAKS_NT"].Hidden = true;
                columns["PIPE_LENEN_LEAKS_NT"].Hidden = true;
            }
        }
    }
}
