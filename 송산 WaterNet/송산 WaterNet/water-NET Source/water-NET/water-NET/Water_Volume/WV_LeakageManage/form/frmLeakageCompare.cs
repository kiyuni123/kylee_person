﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using WaterNet.WV_LeakageManage.work;
using WaterNet.WV_Common.manager;
using ChartFX.WinForms;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.enum1;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace WaterNet.WV_LeakageManage.form
{
    public partial class frmLeakageCompare : Form
    {
        private UltraGridManager gridManager = null;
        private ChartManager chartManager1 = null;

        public frmLeakageCompare()
        {
            InitializeComponent();
            Load += new EventHandler(frmLeakageCompare_Load);
        }

        private void frmLeakageCompare_Load(object sender, EventArgs e)
        {
            this.InitializeCode();
            this.InitializeForm();
            this.InitializeChart();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeEvent();
            this.SelectLeakageCompare();
        }

        private void InitializeCode()
        {
        }

        private void InitializeForm()
        {
        }

        private void InitializeChart()
        {
            this.chartManager1 = new ChartManager();
            this.chartManager1.Add(this.chart1);
            this.chartManager1.SetAxisXFormat(0, "yyyy-MM");
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);

            this.ultraGrid1.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid1.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.TopFixed;

            this.SetSummaryRows();

            //동진 수정_2012.6.27
            this.ultraGrid1.BeforeColumnChooserDisplayed += new BeforeColumnChooserDisplayedEventHandler(ultraGrid1_BeforeColumnChooserDisplayed);  //동진_6.12
            //=======================
        }

        //=========================================================
        //
        //                    동진 수정_2012.6.27
        //                     ColumnChooser 셀 높이
        //=========================================================
        void ultraGrid1_BeforeColumnChooserDisplayed(object sender, BeforeColumnChooserDisplayedEventArgs e)
        {
            e.Dialog.ColumnChooserControl.DisplayLayout.Override.Reset();
            e.Dialog.ColumnChooserControl.DisplayLayout.Override.CellClickAction = CellClickAction.CellSelect;  //원클릭( 원래는 더블클릭이였음)
            e.Dialog.ColumnChooserControl.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

        }
        //=========================================================================

        private void SetSummaryRows()
        {
            this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Clear();
            SummarySettings summary = null;

            ColumnsCollection columns = this.ultraGrid1.DisplayLayout.Bands[0].Columns;

            summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, columns["WATER_SUPPLIED"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,###}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Key = "WATER_SUPPLIED";

            summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, columns["REVENUE"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,###}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Key = "REVENUE";

            summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, columns["NON_REVENUE"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,###}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Key = "NON_REVENUE";

            summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, columns["LEAKS"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,###}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Key = "LEAKS";

            summary = this.ultraGrid1.DisplayLayout.Bands[0].Summaries.Add(SummaryType.Sum, columns["LEAKS_DAVG"], SummaryPosition.UseSummaryPositionColumn);
            summary.DisplayFormat = "{0:###,###,###}";
            summary.Appearance.BackColor = SystemColors.Control;
            summary.ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False;
            summary.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            summary.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            summary.Key = "LEAKS_DAVG";
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
            }
            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
            }

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
        }

        private void InitializeEvent()
        {
        }

        private void SelectLeakageCompare()
        {
            Hashtable parameter = ((frmMain)this.Owner).SearchBox.Parameters;
            this.ultraGrid1.DataSource = LeakageManageWork.GetInstance().SelectLeakageCompare(parameter).Tables[0];

            //if (parameter["FTR_CODE"].ToString() == "BZ002")
            //{
            //    this.ultraGrid1.DisplayLayout.Bands[0].Columns["Leaks"].Hidden = false;
            //}
            //else if(parameter["FTR_CODE"].ToString() == "BZ003")
            //{
            //    this.ultraGrid1.DisplayLayout.Bands[0].Columns["Leaks"].Hidden = true;
            //}
            this.InitializeChartSetting();
        }

        private void InitializeChartSetting()
        {
            Chart chart = this.chartManager1.Items[0];

            DataTable compareList = (DataTable)this.ultraGrid1.DataSource;

            chart.Data.Series = 5;
            chart.Data.Points = compareList.Rows.Count;

            foreach (DataRow row in compareList.Rows)
            {
                int pointIndex = compareList.Rows.IndexOf(row);
                chart.AxisX.Labels[pointIndex] = Convert.ToDateTime(row["YEAR_MON"]).ToString("yyyy-MM");
                chart.Data[0, pointIndex] = Utils.ToDouble(row["WATER_SUPPLIED"]);
                chart.Data[1, pointIndex] = Utils.ToDouble(row["REVENUE"]);
                chart.Data[2, pointIndex] = Utils.ToDouble(row["NON_REVENUE"]);
                //chart.Data[3, pointIndex] = Utils.ToDouble(row["LEAKS"] == DBNull.Value ? 0 : row["LEAKS"]);
                chart.Data[3, pointIndex] = Utils.ToDouble(row["LEAKS"]);
                chart.Data[4, pointIndex] = Utils.ToDouble(row["LEAKS_DAVG"]);
            }

            this.chartManager1.AllShowSeries(chart);

            Hashtable parameter = ((frmMain)this.Owner).SearchBox.Parameters;

            //if (parameter["FTR_CODE"].ToString() == "BZ001")
            //{
            //    chart.Series[3].Visible = true;
            //}
            //else
            //{
            //    chart.Series[3].Visible = false;
            //}

            chart.Series[0].Gallery = Gallery.Bar;

            chart.Series[1].MarkerSize = 2;
            chart.Series[1].Line.Width = 3;
            chart.Series[1].Gallery = Gallery.Lines;
            chart.Series[1].MarkerShape = MarkerShape.Triangle;

            chart.Series[2].MarkerSize = 2;
            chart.Series[2].Line.Width = 3;
            chart.Series[2].Gallery = Gallery.Lines;
            chart.Series[2].MarkerShape = MarkerShape.Triangle;

            chart.Series[3].MarkerSize = 2;
            chart.Series[3].Line.Width = 3;
            chart.Series[3].Gallery = Gallery.Lines;
            chart.Series[3].MarkerShape = MarkerShape.Triangle;

            chart.Series[4].MarkerSize = 2;
            chart.Series[4].Line.Width = 3;
            chart.Series[4].Gallery = Gallery.Lines;
            chart.Series[4].MarkerShape = MarkerShape.Triangle;

            chart.Series[0].Text = "급수량(㎥/월)";
            chart.Series[1].Text = "유수수량(㎥/월)";
            chart.Series[2].Text = "무수수량(㎥/월)";
            chart.Series[3].Text = "총괄수량수지분석 누수량(㎥/월)";
            chart.Series[4].Text = "야간최소유량분석 누수량(㎥/월)";
            chart.AxisY.Title.Text = "유량(㎥/h)";

            chart.AxisY.LabelsFormat.Format = AxisFormat.Number;
            chart.AxisY.LabelsFormat.CustomFormat = "###,###,###";
            chart.AxisY.DataFormat.Format = AxisFormat.Number;
            chart.AxisY.DataFormat.CustomFormat = "###,###,###";

            chart.Series[0].SendToBack();
        }
    }
}
