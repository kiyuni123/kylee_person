﻿namespace WaterNet.WV_LeakageManage.form
{
    partial class frmOptionSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Description = new System.Windows.Forms.RichTextBox();
            this.updateBtn = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.closeBtn = new System.Windows.Forms.Button();
            this.endDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.startDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.N1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ICF = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TM_INS_YN = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.SPECIAL_USED = new System.Windows.Forms.TextBox();
            this.SPECIAL_USED_PERCENT = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LC = new System.Windows.Forms.TextBox();
            this.HPRES_DIFF = new System.Windows.Forms.TextBox();
            this.JUMAL_USED = new System.Windows.Forms.TextBox();
            this.BGAJUNG_USED = new System.Windows.Forms.TextBox();
            this.GAJUNG_USED = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.endDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(10, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(472, 10);
            this.pictureBox1.TabIndex = 52;
            this.pictureBox1.TabStop = false;
            // 
            // Description
            // 
            this.Description.BackColor = System.Drawing.SystemColors.Info;
            this.Description.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Description.Location = new System.Drawing.Point(10, 253);
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Size = new System.Drawing.Size(472, 137);
            this.Description.TabIndex = 56;
            this.Description.Text = "";
            // 
            // updateBtn
            // 
            this.updateBtn.Location = new System.Drawing.Point(365, 6);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(50, 25);
            this.updateBtn.TabIndex = 43;
            this.updateBtn.Text = "적용";
            this.updateBtn.UseVisualStyleBackColor = true;
            this.updateBtn.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(482, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 400);
            this.pictureBox3.TabIndex = 54;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.updateBtn);
            this.panel2.Controls.Add(this.closeBtn);
            this.panel2.Controls.Add(this.endDate);
            this.panel2.Controls.Add(this.startDate);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(10, 10);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(472, 38);
            this.panel2.TabIndex = 57;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(27, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 26;
            this.label18.Text = "적용기간";
            // 
            // closeBtn
            // 
            this.closeBtn.Location = new System.Drawing.Point(421, 6);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(50, 25);
            this.closeBtn.TabIndex = 44;
            this.closeBtn.Text = "닫기";
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // endDate
            // 
            this.endDate.Location = new System.Drawing.Point(215, 11);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(100, 21);
            this.endDate.TabIndex = 2;
            // 
            // startDate
            // 
            this.startDate.Location = new System.Drawing.Point(92, 11);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(100, 21);
            this.startDate.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(198, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "-";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-82, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "적용기간";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(482, 10);
            this.pictureBox5.TabIndex = 58;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox6.Location = new System.Drawing.Point(10, 390);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(472, 10);
            this.pictureBox6.TabIndex = 59;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Location = new System.Drawing.Point(0, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 390);
            this.pictureBox2.TabIndex = 53;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox7.Location = new System.Drawing.Point(10, 243);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(472, 10);
            this.pictureBox7.TabIndex = 60;
            this.pictureBox7.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.N1);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.ICF);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.TM_INS_YN);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.SPECIAL_USED);
            this.groupBox1.Controls.Add(this.SPECIAL_USED_PERCENT);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.LC);
            this.groupBox1.Controls.Add(this.HPRES_DIFF);
            this.groupBox1.Controls.Add(this.JUMAL_USED);
            this.groupBox1.Controls.Add(this.BGAJUNG_USED);
            this.groupBox1.Controls.Add(this.GAJUNG_USED);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(10, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(472, 185);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "항목설정";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(217, 117);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 12);
            this.label17.TabIndex = 25;
            this.label17.Text = "%";
            // 
            // N1
            // 
            this.N1.Location = new System.Drawing.Point(135, 146);
            this.N1.Name = "N1";
            this.N1.Size = new System.Drawing.Size(81, 21);
            this.N1.TabIndex = 24;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(85, 149);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 12);
            this.label16.TabIndex = 23;
            this.label16.Text = "N1지수";
            // 
            // ICF
            // 
            this.ICF.Location = new System.Drawing.Point(180, 280);
            this.ICF.Name = "ICF";
            this.ICF.Size = new System.Drawing.Size(178, 21);
            this.ICF.TabIndex = 22;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(150, 283);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(24, 12);
            this.label15.TabIndex = 21;
            this.label15.Text = "ICF";
            // 
            // TM_INS_YN
            // 
            this.TM_INS_YN.FormattingEnabled = true;
            this.TM_INS_YN.Location = new System.Drawing.Point(182, 320);
            this.TM_INS_YN.Name = "TM_INS_YN";
            this.TM_INS_YN.Size = new System.Drawing.Size(80, 20);
            this.TM_INS_YN.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(103, 323);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 12);
            this.label14.TabIndex = 19;
            this.label14.Text = "TM사용여부";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(217, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 12);
            this.label13.TabIndex = 18;
            this.label13.Text = "%";
            // 
            // SPECIAL_USED
            // 
            this.SPECIAL_USED.Location = new System.Drawing.Point(235, 82);
            this.SPECIAL_USED.Name = "SPECIAL_USED";
            this.SPECIAL_USED.Size = new System.Drawing.Size(79, 21);
            this.SPECIAL_USED.TabIndex = 17;
            // 
            // SPECIAL_USED_PERCENT
            // 
            this.SPECIAL_USED_PERCENT.FormattingEnabled = true;
            this.SPECIAL_USED_PERCENT.Location = new System.Drawing.Point(136, 82);
            this.SPECIAL_USED_PERCENT.Name = "SPECIAL_USED_PERCENT";
            this.SPECIAL_USED_PERCENT.Size = new System.Drawing.Size(80, 20);
            this.SPECIAL_USED_PERCENT.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(363, 211);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 12);
            this.label12.TabIndex = 15;
            this.label12.Text = "kgf/㎠";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(320, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 14;
            this.label11.Text = "L/hr";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(319, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 12);
            this.label10.TabIndex = 13;
            this.label10.Text = "L/prop/hr";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(319, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "L/prop/hr";
            // 
            // LC
            // 
            this.LC.Location = new System.Drawing.Point(180, 244);
            this.LC.Name = "LC";
            this.LC.Size = new System.Drawing.Size(178, 21);
            this.LC.TabIndex = 11;
            // 
            // HPRES_DIFF
            // 
            this.HPRES_DIFF.Location = new System.Drawing.Point(180, 208);
            this.HPRES_DIFF.Name = "HPRES_DIFF";
            this.HPRES_DIFF.Size = new System.Drawing.Size(178, 21);
            this.HPRES_DIFF.TabIndex = 10;
            // 
            // JUMAL_USED
            // 
            this.JUMAL_USED.Location = new System.Drawing.Point(136, 113);
            this.JUMAL_USED.Name = "JUMAL_USED";
            this.JUMAL_USED.Size = new System.Drawing.Size(80, 21);
            this.JUMAL_USED.TabIndex = 9;
            // 
            // BGAJUNG_USED
            // 
            this.BGAJUNG_USED.Location = new System.Drawing.Point(136, 49);
            this.BGAJUNG_USED.Name = "BGAJUNG_USED";
            this.BGAJUNG_USED.Size = new System.Drawing.Size(178, 21);
            this.BGAJUNG_USED.TabIndex = 7;
            // 
            // GAJUNG_USED
            // 
            this.GAJUNG_USED.Location = new System.Drawing.Point(136, 20);
            this.GAJUNG_USED.Name = "GAJUNG_USED";
            this.GAJUNG_USED.Size = new System.Drawing.Size(178, 21);
            this.GAJUNG_USED.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(153, 247);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 12);
            this.label8.TabIndex = 5;
            this.label8.Text = "LC";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 211);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "유입/평균수압지점 수압차";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(52, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "주말야간사용";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "특별야간사용";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "비가정야간사용";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "가정야간사용";
            // 
            // frmOptionSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 400);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Description);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox3);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 432);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(500, 432);
            this.Name = "frmOptionSetting";
            this.Text = "항목설정";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.endDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RichTextBox Description;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button closeBtn;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor endDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor startDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox SPECIAL_USED;
        private System.Windows.Forms.ComboBox SPECIAL_USED_PERCENT;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox LC;
        private System.Windows.Forms.TextBox HPRES_DIFF;
        private System.Windows.Forms.TextBox JUMAL_USED;
        private System.Windows.Forms.TextBox BGAJUNG_USED;
        private System.Windows.Forms.TextBox GAJUNG_USED;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox TM_INS_YN;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox N1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox ICF;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
    }
}