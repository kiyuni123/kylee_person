﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using Infragistics.Win;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.enum1;
using WaterNet.WV_LeakageManage.work;
using ChartFX.WinForms;
using WaterNet.WaterNETMonitoring.Control;
using EMFrame.log;

namespace WaterNet.WV_LeakageManage.form
{
    public partial class popLeakageResult : Form
    {
        private ChartManager chartManager = null;
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();

        public popLeakageResult()
        {
            InitializeComponent();
            this.Load += new EventHandler(popLeakageResult_Load);
        }

        private void popLeakageResult_Load(object sender, EventArgs e)
        {
            //=========================================================
            //
            //                    동진 수정_2012.6.07
            //                      권한박탈(조회만 가능)      
            //=========================================================일별 공급유량/야간최소유량 

            object o = EMFrame.statics.AppStatic.USER_MENU["누수감시ToolStripMenuItem"];
            if (o != null && (Convert.ToString(o).Equals("1") ? true : false))
            {
                this.updateBtn.Enabled = false;
                
            }

            //===========================================================================

            this.InitializeChart();
            this.InitializeEvent();
            this.InitializeGrid();
            this.InitializeValueList();
            this.InitializeForm();
        }

        /// <summary>
        /// 차트를 설정한다.
        /// </summary>
        private void InitializeChart()
        {
            this.chartManager = new ChartManager();
            this.chartManager.Add(this.chart1);
            this.chartManager.SetAxisXFormat(0, "yyyy-MM-dd");
        }

        private void InitializeEvent()
        {
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.moniteringBtn.Click += new EventHandler(moniteringBtn_Click);
            this.updateBtn.Click += new EventHandler(updateBtn_Click);

            this.checkBox1.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox2.CheckedChanged += new EventHandler(checkBox_CheckedChanged);
            this.checkBox3.CheckedChanged += new EventHandler(checkBox_CheckedChanged);

            this.ultraGrid1.AfterCellUpdate += new CellEventHandler(ultraGrid1_CellChange);
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                excelManager.AddWorksheet(this.ultraGrid1, "누수감시데이터");
                excelManager.AddWorksheet(this.chart1, "누수감시데이터", 3, 0, 30, 6);
                excelManager.AddWorksheet(this.ultraGrid2, "누수감시데이터", 3, 6);
                excelManager.Save("누수감시데이터", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void moniteringBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //로직재실행 데이터가 없으면 넘어간다.
                if (this.ultraGrid1.Rows.Count == 0)
                {
                    return;
                }

                string loc_code = this.ultraGrid1.Rows[0].Cells["LOC_CODE"].Value.ToString();
                DateTime day = Convert.ToDateTime(this.ultraGrid1.Rows[0].Cells["DATEE"].Value);

                Monitering monitering = new Monitering();
                monitering.Add(loc_code);
                monitering.Start(day, WaterNet.WaterNETMonitoring.enum1.MONITOR_TYPE.LEAKAGE);

                //감시완료후 부모 재검색, 부모창에서 맵 랜더링 변환 호출
                frmLeakageResult owner = (frmLeakageResult)this.Owner;
                owner.SelectLeakageResult(owner.Parameter);

                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            Hashtable parameter = Utils.ConverToHashtable(this.ultraGrid1.Rows[0]);
            LeakageManageWork.GetInstance().UpdateLeakageMaxValue(parameter);
            MessageBox.Show("정상적으로 처리되었습니다.");
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            this.ChangeChartSeries();
        }

        private void ultraGrid1_CellChange(object sender, CellEventArgs e)
        {
            this.maxLeakgeValue = Utils.ToDouble(e.Cell.Value);
            this.SetMaxLeakageSection();
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);

            this.gridManager.SetRowClick(this.ultraGrid1, false);
            this.gridManager.SetRowClick(this.ultraGrid2, false);

            this.gridManager.SetCellClick(this.ultraGrid1);
            this.gridManager.SetCellClick(this.ultraGrid2);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MAX_LEAKAGE"].CellActivation = Activation.AllowEdit;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MAX_LEAKAGE"].CellClickAction = CellClickAction.Edit;


            //this.gridManager.DefaultColumnsMapping(this.ultraGrid1);
            //this.gridManager.DefaultColumnsMapping(this.ultraGrid2);
        }



        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            }

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            }

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
            }
        }

        private void InitializeForm()
        {
            this.SelectLeakageResult(this.parameter);
        }


        private Hashtable parameter = null;

        public Hashtable Parameter
        {
            set
            {
                this.parameter = value;
            }
        }

        public void SelectLeakageResult(Hashtable parameter)
        {
            //최대값가져오고,
            this.maxLeakgeValue = LeakageManageWork.GetInstance().SelectLeakageMaxValue(parameter);

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("LOC_CODE", typeof(string));
            dataTable.Columns.Add("SGCCD", typeof(string));
            dataTable.Columns.Add("LBLOCK", typeof(string));
            dataTable.Columns.Add("MBLOCK", typeof(string));
            dataTable.Columns.Add("SBLOCK", typeof(string));
            dataTable.Columns.Add("LOC_NAME", typeof(string));
            dataTable.Columns.Add("FTR_CODE", typeof(string));
            dataTable.Columns.Add("FTR_IDN",typeof(string));
            dataTable.Columns.Add("DATEE", typeof(DateTime));
            dataTable.Columns.Add("MONITOR_RESULT", typeof(string));
            dataTable.Columns.Add("MAX_LEAKAGE", typeof(double));

            DataRow dataRow = dataTable.NewRow();
            dataRow["LOC_CODE"] = parameter["LOC_CODE"];
            dataRow["SGCCD"] = parameter["SGCCD"];
            dataRow["LBLOCK"] = parameter["LBLOCK"];
            dataRow["MBLOCK"] = parameter["MBLOCK"];
            dataRow["SBLOCK"] = parameter["SBLOCK"];
            dataRow["LOC_NAME"] = parameter["LOC_NAME"];
            dataRow["FTR_CODE"] = parameter["FTR_CODE"];
            dataRow["FTR_IDN"] = parameter["FTR_IDN"];
            dataRow["DATEE"] = DateTime.ParseExact(parameter["DATEE"].ToString(), "yyyyMMdd", null);
            dataRow["MONITOR_RESULT"] = parameter["MONITOR_RESULT"];
            dataRow["MAX_LEAKAGE"] = this.maxLeakgeValue;
            dataTable.Rows.Add(dataRow);

            this.ultraGrid1.DataSource = dataTable;

            this.SetLeakageResult();
            this.SelectLeakageResultData(parameter);
        }

        private void SetLeakageResult()
        {
            if (this.ultraGrid1.Rows.Count == 0)
            {
                return;
            }

            if (this.ultraGrid1.Rows[0].Cells["MONITOR_RESULT"].Text == "정상")
            {
                this.ultraGrid1.Rows[0].Cells["MONITOR_RESULT"].Appearance.ForeColor = Color.FromArgb(30, 144, 255);
            }

            if (this.ultraGrid1.Rows[0].Cells["MONITOR_RESULT"].Text == "누수")
            {
                this.ultraGrid1.Rows[0].Cells["MONITOR_RESULT"].Appearance.ForeColor = Color.FromArgb(255, 0, 0);
            }

            if (this.ultraGrid1.Rows[0].Cells["MONITOR_RESULT"].Text == "데이터 확인")
            {
                this.ultraGrid1.Rows[0].Cells["MONITOR_RESULT"].Appearance.ForeColor = Color.FromArgb(255, 165, 0);
            }

            this.ultraGrid1.Rows[0].Cells["MONITOR_RESULT"].Appearance.FontData.Bold = DefaultableBoolean.True;
        }

        private void SelectLeakageResultData(Hashtable parameter)
        {
            this.ultraGrid2.DataSource = LeakageManageWork.GetInstance().SelectLeakageResultData(parameter);
            this.InitializeChartSetting();
        }

        private double maxFlowValue = 0;
        private double maxLeakgeValue = 0;

        /// <summary>
        /// 차트 데이터 세팅 초기화
        /// </summary>
        private void InitializeChartSetting()
        {
            this.maxFlowValue = 0;

            DataTable dataTable = this.ultraGrid2.DataSource as DataTable;

            Chart chart = this.chartManager.Items[0];

            if (dataTable == null)
            {
                return;
            }

            chart.Data.Series = 2;
            chart.Data.Points = dataTable.Rows.Count;
            this.chart1.Data.Clear();

            int k = 0;

            for (int i = dataTable.Rows.Count - 1; i >= 0; i--)
            {
                chart.AxisX.Labels[k] = Convert.ToDateTime(dataTable.Rows[i]["DATEE"]).ToString("yyyy-MM-dd");

                if (dataTable.Rows[i]["FLOW"] != DBNull.Value)
                {
                    chart.Data[0, k] = Utils.ToDouble(dataTable.Rows[i]["FLOW"]);
                }

                chart.Data[1, k] = Utils.ToDouble(dataTable.Rows[i]["MNF"]);

                //if (dataTable.Rows[i]["MNF"] != DBNull.Value)
                //{
                //    chart.Data[1, k] = Utils.ToDouble(dataTable.Rows[i]["MNF"]);
                //}

                if (this.maxFlowValue < Utils.ToDouble(dataTable.Rows[i]["FLOW"]))
                {
                    this.maxFlowValue = Utils.ToDouble(dataTable.Rows[i]["FLOW"]);
                }

                //if (this.maxFlowValue < Utils.ToDouble(dataTable.Rows[i]["MNF"]))
                //{
                //    this.maxFlowValue = Utils.ToDouble(dataTable.Rows[i]["MNF"]);
                //}
                k++;
            }
            if (maxFlowValue > 0)
                this.maxFlowValue = (chart.Series[0].AxisY.Max / 100) * 10;
            else
                this.maxFlowValue = chart.Series[0].AxisY.Max + 5;
            chart.Series[0].Gallery = Gallery.Curve;
            chart.Series[0].MarkerShape = MarkerShape.Diamond;
            chart.Series[0].Text = "공급량(㎥/일)";

            chart.Series[1].Gallery = Gallery.Curve;
            chart.Series[1].MarkerShape = MarkerShape.Diamond;
            chart.Series[1].Text = "야간최소유량(㎥/h)";

            chart.AxesY.Add(new AxisY());

            chart.Series[1].AxisY = chart.AxesY[1];

            this.chart1.AxesY[0].Title.Text = "공급량(㎥/일)";
            this.chart1.AxesY[0].DataFormat.Format = AxisFormat.Number;
            this.chart1.AxesY[0].DataFormat.CustomFormat = "###,###,###";

            this.chart1.AxesY[1].Title.Text = "보정 야간최소유량(㎥/h)";
            this.chart1.AxesY[1].DataFormat.Format = AxisFormat.Number;
            this.chart1.AxesY[1].DataFormat.CustomFormat = "###,###,###.00";

            this.chart1.AxesY[0].LabelsFormat.Format = AxisFormat.Number;
            this.chart1.AxesY[0].LabelsFormat.CustomFormat = "###,###,###";

            this.chart1.AxesY[1].LabelsFormat.Format = AxisFormat.Number;
            this.chart1.AxesY[1].LabelsFormat.CustomFormat = "###,###,###.00";
            this.chart1.AxesY[1].Grids.Major.Visible = false;

            chart.Refresh();
            this.ChangeChartSeries();
            this.SetMaxLeakageSection();
        }

        /// <summary>
        /// 차트 시리즈 변경
        /// </summary>
        private void ChangeChartSeries()
        {
            Chart chart = this.chartManager.Items[0];

            foreach (SeriesAttributes series in chart.Series)
            {
                series.Visible = false;
                foreach (AxisSection section in series.AxisY.Sections)
                {
                    section.Visible = false;
                }
            }

            if (this.checkBox1.Checked)
            {
                chart.Series[0].Visible = true;
            }

            if (this.checkBox2.Checked)
            {
                chart.Series[1].Visible = true;
            }

            if (this.checkBox3.Checked)
            {
                if (chart.Series[1].AxisY.Sections.Count > 0)
                {
                    chart.Series[1].AxisY.Sections[0].Visible = true;
                }
            }
        }

        private void SetMaxLeakageSection()
        {
            this.chart1.Series[1].AxisY.Min = 0;
            this.chart1.Series[1].AxisY.Sections.Clear();
            double num = 0;
            //20140512_승일 수정(ChartFx 표현방법 수정)    
                    
            if (maxLeakgeValue > 100)
            {
                num = 0.5;
            }
            else if (maxLeakgeValue <= 100 && maxLeakgeValue >= 50)
            {
                num = 0.1;
            }
            else
            {
                num = 0.01;
            }
            
            this.chart1.Series[1].AxisY.Sections.Add(new AxisSection(this.maxLeakgeValue, this.maxLeakgeValue + num, Color.Red));
            this.chart1.Series[1].AxisY.Sections[0].Text = "누수한계치 : " + this.maxLeakgeValue.ToString() + "(㎥/h)";

            if (this.maxLeakgeValue >= this.maxFlowValue)
            {
                this.chart1.Series[1].AxisY.Max = this.maxLeakgeValue + 5;
            }
            else
            {
                this.chart1.Series[1].AxisY.Max = this.maxFlowValue;
            }

            this.chart1.Series[1].AxisY.Sections[0].Visible = this.checkBox3.Checked;
        }
    }
}
