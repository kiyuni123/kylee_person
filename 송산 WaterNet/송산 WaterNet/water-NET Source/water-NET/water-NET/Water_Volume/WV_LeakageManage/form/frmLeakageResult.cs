﻿using System;
using System.Data;
using System.Windows.Forms;
using WaterNet.WV_Common.interface1;
using WaterNet.WV_Common.manager;
using Infragistics.Win.UltraWinGrid;
using WaterNet.WV_Common.util;
using WaterNet.WV_Common.enum1;
using Infragistics.Win;
using System.Collections;
using WaterNet.WV_LeakageManage.work;
using WaterNet.WaterNETMonitoring.Control;
using EMFrame.log;

namespace WaterNet.WV_LeakageManage.form
{
    public partial class frmLeakageResult : Form
    {
        private IMapControlWV mainMap = null;
        private UltraGridManager gridManager = null;
        private ExcelManager excelManager = new ExcelManager();
        private string selectedBlock = string.Empty;

        /// <summary>
        /// MainMap 을 제어하기 위한 인스턴스 변수
        /// </summary>
        public IMapControlWV MainMap
        {
            set
            {
                this.mainMap = value;
            }
        }

        //맵에서 바로 블록 선택후 메뉴호출시에 선택된 블록의 FTR_IDN을 세팅해준다.
        public string SelectedBlock
        {
            set
            {
                this.selectedBlock = value;
            }
        }

        public frmLeakageResult()
        {
            InitializeComponent();
            Load += new EventHandler(frmLeakageResult_Load);
        }

        private void frmLeakageResult_Load(object sender, EventArgs e)
        {
            this.InitializeEvent();
            this.InitializeForm();
            this.InitializeGrid();
            this.InitializeValueList();
        }

        private void InitializeForm()
        {
            this.datee.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            this.SelectLeakageResult(this.Parameter);
        }

        private void InitializeGrid()
        {
            this.gridManager = new UltraGridManager();
            this.gridManager.Add(this.ultraGrid1);
            this.gridManager.Add(this.ultraGrid2);

            this.gridManager.SetRowClick(this.ultraGrid1, true);

            this.ultraGrid1.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid1.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.RowsAndCells;

            this.ultraGrid2.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.None;
            this.ultraGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid2.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.RowsAndCells;

            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MONITOR_ALARM"].CellActivation = Activation.AllowEdit;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MONITOR_ALARM"].CellClickAction = CellClickAction.Edit;
            this.ultraGrid1.DisplayLayout.Bands[0].Columns["MONITOR_ALARM"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
        }

        /// <summary>
        /// 그리드내 ValueList를 추가
        /// </summary>
        private void InitializeValueList()
        {
            ValueList valueList = null;

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("LBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("LBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.LARGE_BLOCK, null);
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["LBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["LBLOCK"];
            }

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("MBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("MBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.MIDDLE_BLOCK, null);
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["MBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["MBLOCK"];
            }

            if (!this.ultraGrid1.DisplayLayout.ValueLists.Exists("SBLOCK"))
            {
                valueList = this.ultraGrid1.DisplayLayout.ValueLists.Add("SBLOCK");
                Utils.SetValueList(valueList, VALUELIST_TYPE.SMALL_BLOCK, null);
                this.ultraGrid1.DisplayLayout.Bands[0].Columns["SBLOCK"].ValueList = this.ultraGrid1.DisplayLayout.ValueLists["SBLOCK"];
            }
        }

        private void InitializeEvent()
        {
            this.Disposed += new EventHandler(frmLeakageResult_Disposed);
            this.ultraGrid1.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid1_InitializeLayout);
            this.ultraGrid2.InitializeLayout += new InitializeLayoutEventHandler(ultraGrid2_InitializeLayout);
            this.ultraGrid1.AfterRowActivate += new EventHandler(ultraGrid1_AfterRowActivate);
            this.ultraGrid1.AfterSelectChange += new AfterSelectChangeEventHandler(ultraGrid1_AfterSelectChange);

            //더블클릭 맵이동 수정
            this.ultraGrid1.DoubleClickRow += new DoubleClickRowEventHandler(ultraGrid1_DoubleClickRow);
            this.ultraGrid1.DoubleClickCell += new DoubleClickCellEventHandler(ultraGrid1_DoubleClickCell);

            this.datee.ValueChanged += new EventHandler(datee_ValueChanged);

            this.ultraGrid1.CellChange += new CellEventHandler(ultraGrid1_CellChange);

            this.moniteringBtn.Click += new EventHandler(moniteringBtn_Click);
            this.dataBtn.Click += new EventHandler(dataBtn_Click);
            this.excelBtn.Click += new EventHandler(excelBtn_Click);
            this.searchBtn.Click += new EventHandler(searchBtn_Click);
        }

        //폼이 닫힐때 누수감시기준일자를 최근일자로 변경한다.
        private void frmLeakageResult_Disposed(object sender, EventArgs e)
        {
            Hashtable parameter = new Hashtable();
            parameter["STARTDATE"] = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
            parameter["ENDDATE"] = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
            this.mainMap.SetLeakageMoniteringRenderer(parameter);
        }

        private void moniteringBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Monitering monitering = new Monitering();
                monitering.AddAll();
                monitering.Start(Convert.ToDateTime(this.datee.Value), WaterNet.WaterNETMonitoring.enum1.MONITOR_TYPE.LEAKAGE);

                this.SelectLeakageResult(this.Parameter);
                MessageBox.Show("정상적으로 처리되었습니다.");
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        //검색버튼 이벤트 핸들러
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.SelectLeakageResult(this.Parameter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.excelManager.Clear();
                excelManager.AddWorksheet(this.ultraGrid1, "누수감시결과");
                excelManager.AddWorksheet(this.ultraGrid2, "누수감시결과", 0, 6);
                excelManager.Save("누수감시결과", true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void ultraGrid1_CellChange(object sender, CellEventArgs e)
        {
            this.ultraGrid1.UpdateData();
            Hashtable parameter = Utils.ConverToHashtable(e.Cell.Row);
            this.UpdateLeakageResultAlarm(parameter);
            parameter["STARTDATE"] = parameter["DATEE"];
            this.mainMap.SetLeakageMoniteringRenderer(parameter);
        }

        private void datee_ValueChanged(object sender, EventArgs e)
        {
            this.SelectLeakageResult(this.Parameter);
        }

        int count = 0;

        private void ultraGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            count++;

            foreach (UltraGridRow row in e.Layout.Rows)
            {
                //누수아님
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "F")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("정상", false);

                    //체크박스 수정불가능하게,
                    row.Cells["MONITOR_ALARM"].Activation = Activation.Disabled;
                }

                //누수임
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "T")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("누수", false);
                }

                //데이터확인
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "N")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("데이터 확인", false);
                }

                //2012-03-02 추가
                //확인필요
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "A")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("관심", false);
                }
            }

            //맵상에서 블록이 선택된 경우라면 그리드에서 해당블록을 선택해준다.
            if (this.selectedBlock != string.Empty && this.count == 2)
            {
                foreach (UltraGridRow gridRow in this.ultraGrid1.Rows)
                {
                    if (gridRow.Cells["FTR_IDN"].Value.ToString() == this.selectedBlock)
                    {
                        gridRow.Activate();
                        if (this.ultraGrid1.Selected.Rows != null)
                        {
                            this.ultraGrid1.Selected.Rows.Clear();
                            this.ultraGrid1.Selected.Rows.Add(gridRow);
                        }
                        break;
                    }
                }
            }
            else if (this.selectedBlock == string.Empty && this.count == 2)
            {
                if (this.ultraGrid1.Rows.Count > 0)
                {
                    this.ultraGrid1.Rows[0].Activate();
                }
            }
            if (this.ultraGrid1.Rows.Count != 0 && this.count == 2)
            {
                this.selectedBlock = string.Empty;
            }
        }

        private void ultraGrid2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            foreach (UltraGridRow row in e.Layout.Rows)
            {
                //누수아님
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "F")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("정상", false);
                }

                //누수임
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "T")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("누수", false);
                }

                //데이터확인
                if (row.Cells["MONITOR_RESULT"].Value.ToString() == "N")
                {
                    row.Cells["MONITOR_RESULT"].SetValue("데이터 확인", false);
                }
            }
        }

        public void SelectLeakageResult(Hashtable parameter)
        {
            DataTable table = LeakageManageWork.GetInstance().SelectLeakageResult(parameter);
            table.ColumnChanging += new DataColumnChangeEventHandler(this.OnTableCellValueChanging);
            this.ultraGrid1.DataSource = table;
            this.mainMap.SetLeakageMoniteringRenderer(parameter);
        }

        private void OnTableCellValueChanging(object sender, DataColumnChangeEventArgs e)
        {
            this.ValidateDataRowCell(e.Row, e.Column, e.ProposedValue);
        }

        private void ValidateDataRowCell(DataRow row, DataColumn column, object value)
        {
            if (column.ColumnName == "MONITOR_RESULT")
            {
                row.SetColumnError(column, string.Empty);

                if (row[column.ColumnName].ToString() == "T")
                {
                    row.SetColumnError(column, "누수");
                }
                else if (row[column.ColumnName].ToString() == "N")
                {
                    row.SetColumnError(column, "데이터 확인");
                }
                //else if (row[column.ColumnName].ToString() == "A")
                //{
                //    row.SetColumnError(column, "확인필요");
                //}
            }
        }

        /// <summary>
        /// 활성화된 로우에 클릭 이벤트를 준다. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            if (this.ultraGrid1.ActiveRow != null)
            {
                this.ultraGrid1.ActiveRow.Selected = true;
            }
        }

        private int Is_First_Call = 0;

        /// <summary>
        /// 그리드 로우 선택 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (this.ultraGrid1.Selected.Rows.Count == 0)
            {
                return;
            }

            Hashtable parameter = Utils.ConverToHashtable(this.ultraGrid1.Selected.Rows[0]);
            parameter["STARTDATE"] = Convert.ToDateTime(this.datee.Value).ToString("yyyyMMdd");
            parameter["ENDDATE"] = Convert.ToDateTime(this.datee.Value).ToString("yyyyMMdd");

            this.SelectLeakageResultDetail(parameter);
        }

        private void ultraGrid1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            Hashtable parameter = Utils.ConverToHashtable(e.Row);
            parameter["STARTDATE"] = Convert.ToDateTime(this.datee.Value).ToString("yyyyMMdd");
            parameter["ENDDATE"] = Convert.ToDateTime(this.datee.Value).ToString("yyyyMMdd");
            this.mainMap.MoveToBlock(parameter);

            if (this.IsdataPop)
            {
                popLeakageResult oForm = new popLeakageResult();
                oForm.Parameter = parameter;
                oForm.Owner = this;
                oForm.ShowDialog();
            }

            this.IsdataPop = false;
        }

        private bool IsdataPop = false;
        private void ultraGrid1_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            if (e.Cell == null || e.Cell.Row == null)
            {
                return;
            }

            if (e.Cell.Column.Key == "MONITOR_RESULT")
            {
                this.IsdataPop = true;
            }
        }

        private void SelectLeakageResultDetail(Hashtable parameter)
        {
            DataTable table = LeakageManageWork.GetInstance().SelectLeakageResultDetail(parameter);
            table.ColumnChanging += new DataColumnChangeEventHandler(this.OnTableCellValueChanging);
            this.ultraGrid2.DataSource = table;
        }

        private void UpdateLeakageResultAlarm(Hashtable parameter)
        {
            LeakageManageWork.GetInstance().UpdateLeakageResultAlarm(parameter);
        }

        //공급량/ 야간최소유량 확인 팝업 호출
        private void dataBtn_Click(object sender, EventArgs e)
        {
            //그리드1의 선택 로우가 있어야한다.
            if (this.ultraGrid1.ActiveRow == null)
            {
                MessageBox.Show("누수감시결과 데이터를 선택해야 합니다.");
                return;
            }

            popLeakageResult oForm = new popLeakageResult();
            oForm.Parameter = Utils.ConverToHashtable(this.ultraGrid1.ActiveRow);            
            oForm.Show();
        }

        public Hashtable Parameter
        {
            get
            {
                Hashtable parameter = new Hashtable();
                parameter["STARTDATE"] = Convert.ToDateTime(this.datee.Value).ToString("yyyyMMdd");
                parameter["ENDDATE"] = Convert.ToDateTime(this.datee.Value).ToString("yyyyMMdd");
                return parameter;
            }
        }
    }
}
