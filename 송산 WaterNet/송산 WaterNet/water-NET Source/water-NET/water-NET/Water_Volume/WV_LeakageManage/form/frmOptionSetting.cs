﻿using System;
using System.Text;
using System.Windows.Forms;
using WaterNet.WaterNetCore;
using System.Collections;
using WaterNet.WV_LeakageManage.work;
using WaterNet.WV_LeakageManage.vo;
using WaterNet.WV_Common.util;

namespace WaterNet.WV_LeakageManage.form
{
    public partial class frmOptionSetting : Form
    {
        /// <summary>
        /// 생성자 초기 설정
        /// </summary>
        public frmOptionSetting()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmOptionSetting_Load);
        }

        /// <summary>
        /// 폼로드 완료 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmOptionSetting_Load(object sender, EventArgs e)
        {
            this.InitializeCode();
            this.InitializeForm();
            this.InitializeEvent();
        }

        /// <summary>
        /// 코드데이터 초기화 설정
        /// </summary>
        private void InitializeCode()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("select code cd, code_name cdnm from cm_code where pcode = '2004' order by to_number(orderby)");
            FormManager.SetComboBoxEX(this.SPECIAL_USED_PERCENT, sql.ToString(), false);

            sql = new StringBuilder();
            sql.AppendLine("select code cd, code_name cdnm from cm_code where pcode = '2006' order by to_number(orderby)");
            FormManager.SetComboBoxEX(this.TM_INS_YN, sql.ToString(), false);
        }

        /// <summary>
        /// 폼 초기화 설정
        /// </summary>
        private void InitializeForm()
        {
            startDate.MaskInput = "yyyy-mm-dd";
            startDate.PromptChar = ' ';
            endDate.MaskInput = "yyyy-mm-dd";
            endDate.PromptChar = ' ';

            this.SetBlockDefaultOption();
            this.Description.Select();
        }

        /// <summary>
        /// 이벤트 초기화 설정
        /// </summary>
        private void InitializeEvent()
        {
            this.SPECIAL_USED_PERCENT.SelectedIndexChanged += new EventHandler(SPECIAL_USED_PERCENT_SelectedIndexChanged);
            this.TM_INS_YN.SelectedIndexChanged += new EventHandler(TM_INS_YN_SelectedIndexChanged);

            this.GAJUNG_USED.Click += new EventHandler(GAJUNG_USED_Click);
            this.BGAJUNG_USED.Click += new EventHandler(BGAJUNG_USED_Click);
            this.SPECIAL_USED_PERCENT.Click += new EventHandler(SPECIAL_USED_PERCENT_Click);
            this.SPECIAL_USED.Click += new EventHandler(SPECIAL_USED_Click);
            this.JUMAL_USED.Click += new EventHandler(JUMAL_USED_Click);
            this.N1.Click += new EventHandler(N1_Click);
        }

        private void N1_Click(object sender, EventArgs e)
        {
            this.Description.Text = "  N1(누수지수) : FAVAD공식(누수와 수압과의 관계식) 중 누수지수를 말함 \n";
            this.Description.Text += "  (보통 1.0을 적용) \n";
        }

        private void JUMAL_USED_Click(object sender, EventArgs e)
        {
            this.Description.Text = "  주말 야간사용량 (Weekend night use) : \n";
            this.Description.Text += "  요일이 주말일 경우 야간사용량 가중치를 위해 설정 \n";
            this.Description.Text += "  주말사용량 (l/hr) =  \n";
            this.Description.Text += "   (가정야간사용량 + 비가정야간사용량 + 특별야간사용량) X (설정값%) 적용\n";
            this.Description.Text += "  \n";
        }

        private void SPECIAL_USED_Click(object sender, EventArgs e)
        {
            this.Description.Text = "  특별 야간사용량 (Exceptional night use) : \n";
            this.Description.Text += "  특별 야간사용 수용가의 야간사용량을 직접 측정하여 산출(l/hr)  \n";
            this.Description.Text += "  측정불가시, 대수용가의 월사용량 X 0 ~ 10% 적용  \n";
            this.Description.Text += "  \n";
            this.Description.Text += "  *특별 야간사용량 : 야간최소유량이 측정되는 시간동안 대수용가의 계량된 사용량 \n";
            this.Description.Text += "  *대수용가 선정기준 : 수용가 중 야간사용량이 500 l/hr 이상 수용가 \n";
            this.Description.Text += "  (측정불가시, 월사용량이 360 ㎥ 이상 수용가) \n";
            this.Description.Text += "  *단계시험 등을 통해 실제 누수량을 알고 있을시, 산정된 누수량과 비교하여 \n";
            this.Description.Text += "   큰 차이를 나타낼 경우 예외적 야간사용량을 조정 \n";
            this.Description.Text += "  - 가구당 예외적야간사용량 = 월사용량/일수/24 X 0 ~ 10% \n";
        }

        private void SPECIAL_USED_PERCENT_Click(object sender, EventArgs e)
        {
            this.Description.Text = "  특별 야간사용량 (Exceptional night use) : \n";
            this.Description.Text += "  특별 야간사용 수용가의 야간사용량을 직접 측정하여 산출(l/hr)  \n";
            this.Description.Text += "  측정불가시, 대수용가의 월사용량 X 0 ~ 10% 적용  \n";
            this.Description.Text += "  \n";
            this.Description.Text += "  *특별 야간사용량 : 야간최소유량이 측정되는 시간동안 대수용가의 계량된 사용량 \n";
            this.Description.Text += "  *대수용가 선정기준 : 수용가 중 야간사용량이 500 l/hr 이상 수용가 \n";
            this.Description.Text += "  (측정불가시, 월사용량이 360 ㎥ 이상 수용가) \n";
            this.Description.Text += "  *단계시험 등을 통해 실제 누수량을 알고 있을시, 산정된 누수량과 비교하여 \n";
            this.Description.Text += "   큰 차이를 나타낼 경우 예외적 야간사용량을 조정 \n";
            this.Description.Text += "  - 가구당 예외적야간사용량 = 월사용량/일수/24 X 0 ~ 10% \n";
        }

        private void BGAJUNG_USED_Click(object sender, EventArgs e)
        {
            this.Description.Text = "  비가정 야간사용량 (Non-household night use) : \n";
            this.Description.Text += "  비가정간사용량(l/hr) = 비가구수 X 가구당 비가정야간사용량  \n";
            this.Description.Text += "  \n";
            this.Description.Text += "  *비가구수 : 수용가 중 비가정(일반등)으로 분류된 수용가의 수 \n";
            this.Description.Text += "  (비가정용임에도 불구하고 예외적야간사용수용가인 경우에는 제외) \n";
            this.Description.Text += "  *가구당 비가정야간사용량 : 측정불가시 8 l/hr \n";
        }

        private void GAJUNG_USED_Click(object sender, EventArgs e)
        {
            this.Description.Text = "  가정 야간사용량 (Household night use) : \n";
            this.Description.Text += "  가정간사용량(l/hr) = 가정가구수 X 가구당 가정야간사용량  \n";
            this.Description.Text += "  (또는) = 인구수 X 야간화장실사용량  \n";
            this.Description.Text += "  \n";
            this.Description.Text += "  *가정가구수 : 수용가 중 가정용으로 분류된 수용가의 가구수 \n";
            this.Description.Text += "  (가정용임에도 불구하고 예외적야간사용수용가인 경우에는 제외) \n";
            this.Description.Text += "  *가구당 가정야간사용량 : 측정불가시 1.7 l/hr \n";
            this.Description.Text += "  *야간화장실사용량 : 측정불가시 0.6 l/hr \n";
        }


        /// <summary>
        /// 콤보박스선택변경이벤트(TM사용여부)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TM_INS_YN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.TM_INS_YN.SelectedValue.ToString() == "Y")
            {
                ControlUtils.SetEnabled(this.HPRES_DIFF, false);
            }
            else
            {
                ControlUtils.SetEnabled(this.HPRES_DIFF, true);
            }
        }

        /// <summary>
        /// 콤보박스선택변경이벤트(대수용가사용량)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SPECIAL_USED_PERCENT_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SPECIAL_USED_PERCENT.SelectedValue.ToString() == "99")
            {
                ControlUtils.SetEnabled(this.SPECIAL_USED, true);
            }
            else
            {
                ControlUtils.SetEnabled(this.SPECIAL_USED, false);
            }
        }

        /// <summary>
        /// 블록별 기본값 설정
        /// </summary>
        /// <param name="parameter"></param>
        private void SetBlockDefaultOption()
        {
            //상위 폼에서 검색된 기간과 선택된 블록의 정보를 가져온다.
            Hashtable parameter = ((frmMain)Owner).SearchBox.Parameters as Hashtable;
            //parameter["LOC_CODE"] = ((frmMain)Owner).CurrentDetailBlockLocationCode;

            //폼내에 값을 세팅한다.
            this.startDate.Value = DataUtils.StringToFormatString(parameter["STARTDATE"].ToString(), "yyyy-MM-dd");
            this.endDate.Value = DataUtils.StringToFormatString(parameter["ENDDATE"].ToString(), "yyyy-MM-dd");

            //초기 블록의 설정값을 조회한다.
            LeakageOptionVO  defaultOption = this.SelectBlockDefaultOption(parameter);

            defaultOption.GetValue(this.JUMAL_USED, this.GAJUNG_USED, this.BGAJUNG_USED,
                this.SPECIAL_USED_PERCENT, this.SPECIAL_USED, this.N1, this.LC, this.ICF);
        }

        /// <summary>
        /// 누수관리 초기설정값 조회
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private LeakageOptionVO SelectBlockDefaultOption(Hashtable parameter)
        {
            return LeakageManageWork.GetInstance().SelectBlockDefaultOption(parameter);
        }

        /// <summary>
        /// 적용버튼 클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            if (this.startDate.Value == null || this.endDate.Value == null)
            {
                return;
            }

            //수정된 값들의 포멧을 체크한다,
            LeakageOptionVO defaultOption = new LeakageOptionVO();
            defaultOption.SetValue(this.JUMAL_USED, this.GAJUNG_USED, this.BGAJUNG_USED, 
                this.SPECIAL_USED_PERCENT,this.SPECIAL_USED, this.N1, this.LC, this.ICF);

            ((frmMain)Owner).SetLeakageDefaulutOption(defaultOption, (DateTime)this.startDate.Value, (DateTime)this.endDate.Value);

            MessageBox.Show("항목 적용이 완료 되었습니다.\n상위 메뉴에서 저장을 선택해야 설정값이 DB에 저장됩니다.");
        }

        //private void updateBtn_Click(object sender, EventArgs e)
        //{
        //    //수정된 값들의 포멧을 체크한다,
        //    LeakageOptionVO defaultOption = new LeakageOptionVO();
        //    defaultOption.SetValue(this.TM_INS_YN, this.HPRES_DIFF, this.JUMAL_USED, this.GAJUNG_USED, this.BGAJUNG_USED,
        //        this.SPECIAL_USED_PERCENT, this.SPECIAL_USED, this.N1, this.LC, this.ICF);

        //    ((frmMain)Owner).SetLeakageDefaulutOption(defaultOption, (DateTime)this.startDate.Value, (DateTime)this.endDate.Value);
        //}

        /// <summary>
        /// 닫기버튼 클릭 이벤트 핸들러
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}