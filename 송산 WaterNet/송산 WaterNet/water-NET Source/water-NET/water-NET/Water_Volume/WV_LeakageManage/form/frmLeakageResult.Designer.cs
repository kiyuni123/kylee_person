﻿namespace WaterNet.WV_LeakageManage.form
{
    partial class frmLeakageResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SGCCD");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SBLOCK");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_NAME");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FTR_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("FTR_IDN");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DATEE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MONITOR_RESULT");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MONITOR_ALARM");
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("LOC_CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DATEE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CODE");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CODE_NAME");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MONITOR_COUNT_MAX");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MONITOR_COUNT");
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("MONITOR_RESULT");
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.moniteringBtn = new System.Windows.Forms.Button();
            this.DateToDate = new System.Windows.Forms.Panel();
            this.datee = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.DateToDateIntervalText = new System.Windows.Forms.Label();
            this.dataBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.excelBtn = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.groupBox1.SuspendLayout();
            this.DateToDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.moniteringBtn);
            this.groupBox1.Controls.Add(this.DateToDate);
            this.groupBox1.Controls.Add(this.dataBtn);
            this.groupBox1.Controls.Add(this.searchBtn);
            this.groupBox1.Controls.Add(this.excelBtn);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1152, 53);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "기본설정";
            // 
            // moniteringBtn
            // 
            this.moniteringBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.moniteringBtn.Location = new System.Drawing.Point(846, 17);
            this.moniteringBtn.Name = "moniteringBtn";
            this.moniteringBtn.Size = new System.Drawing.Size(127, 23);
            this.moniteringBtn.TabIndex = 60;
            this.moniteringBtn.Text = "누수판단로직 재실행";
            this.moniteringBtn.UseVisualStyleBackColor = true;
            // 
            // DateToDate
            // 
            this.DateToDate.Controls.Add(this.datee);
            this.DateToDate.Controls.Add(this.DateToDateIntervalText);
            this.DateToDate.Location = new System.Drawing.Point(6, 18);
            this.DateToDate.Name = "DateToDate";
            this.DateToDate.Size = new System.Drawing.Size(150, 22);
            this.DateToDate.TabIndex = 13;
            // 
            // datee
            // 
            this.datee.DateTime = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            this.datee.Location = new System.Drawing.Point(47, 0);
            this.datee.Name = "datee";
            this.datee.Size = new System.Drawing.Size(100, 21);
            this.datee.TabIndex = 8;
            this.datee.Value = new System.DateTime(2010, 10, 20, 0, 0, 0, 0);
            // 
            // DateToDateIntervalText
            // 
            this.DateToDateIntervalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.DateToDateIntervalText.AutoSize = true;
            this.DateToDateIntervalText.Location = new System.Drawing.Point(3, 4);
            this.DateToDateIntervalText.Margin = new System.Windows.Forms.Padding(0);
            this.DateToDateIntervalText.Name = "DateToDateIntervalText";
            this.DateToDateIntervalText.Size = new System.Drawing.Size(41, 12);
            this.DateToDateIntervalText.TabIndex = 1;
            this.DateToDateIntervalText.Text = "기준일";
            // 
            // dataBtn
            // 
            this.dataBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dataBtn.Location = new System.Drawing.Point(1025, 17);
            this.dataBtn.Name = "dataBtn";
            this.dataBtn.Size = new System.Drawing.Size(75, 23);
            this.dataBtn.TabIndex = 14;
            this.dataBtn.Text = "데이터확인";
            this.dataBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.Location = new System.Drawing.Point(1106, 17);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(40, 23);
            this.searchBtn.TabIndex = 15;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // excelBtn
            // 
            this.excelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelBtn.Location = new System.Drawing.Point(979, 17);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(40, 23);
            this.excelBtn.TabIndex = 12;
            this.excelBtn.Text = "엑셀";
            this.excelBtn.UseVisualStyleBackColor = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(10, 658);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1152, 10);
            this.pictureBox4.TabIndex = 53;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox3.Location = new System.Drawing.Point(1162, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 658);
            this.pictureBox3.TabIndex = 52;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Location = new System.Drawing.Point(0, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 658);
            this.pictureBox2.TabIndex = 51;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1172, 10);
            this.pictureBox1.TabIndex = 50;
            this.pictureBox1.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(515, 63);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 595);
            this.panel5.TabIndex = 56;
            // 
            // ultraGrid1
            // 
            ultraGridColumn6.Header.Caption = "지역관리번호";
            ultraGridColumn6.Header.VisiblePosition = 0;
            ultraGridColumn6.Hidden = true;
            ultraGridColumn7.Header.Caption = "센터";
            ultraGridColumn7.Header.VisiblePosition = 1;
            ultraGridColumn7.Hidden = true;
            ultraGridColumn8.Header.Caption = "대블록";
            ultraGridColumn8.Header.VisiblePosition = 3;
            ultraGridColumn8.Width = 60;
            ultraGridColumn9.Header.Caption = "중블록";
            ultraGridColumn9.Header.VisiblePosition = 4;
            ultraGridColumn9.Width = 60;
            ultraGridColumn11.Header.Caption = "소블록";
            ultraGridColumn11.Header.VisiblePosition = 5;
            ultraGridColumn11.Width = 60;
            ultraGridColumn13.Header.Caption = "블록명";
            ultraGridColumn13.Header.VisiblePosition = 6;
            ultraGridColumn13.Hidden = true;
            ultraGridColumn14.Header.Caption = "블록구분";
            ultraGridColumn14.Header.VisiblePosition = 7;
            ultraGridColumn14.Hidden = true;
            ultraGridColumn15.Header.Caption = "블록관리번호";
            ultraGridColumn15.Header.VisiblePosition = 8;
            ultraGridColumn15.Hidden = true;
            ultraGridColumn16.Header.Caption = "감시일자";
            ultraGridColumn16.Header.VisiblePosition = 9;
            ultraGridColumn16.Width = 84;
            ultraGridColumn17.Header.Caption = "누수판단결과";
            ultraGridColumn17.Header.VisiblePosition = 10;
            ultraGridColumn17.Width = 133;
            ultraGridColumn1.Header.Caption = "누수알람";
            ultraGridColumn1.Header.VisiblePosition = 2;
            ultraGridColumn1.Width = 80;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn11,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn1});
            this.ultraGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraGrid1.Location = new System.Drawing.Point(10, 63);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(505, 595);
            this.ultraGrid1.TabIndex = 58;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // ultraGrid2
            // 
            ultraGridColumn3.Header.Caption = "지역관리번호";
            ultraGridColumn3.Header.VisiblePosition = 0;
            ultraGridColumn3.Hidden = true;
            ultraGridColumn4.Header.Caption = "감시일자";
            ultraGridColumn4.Header.VisiblePosition = 1;
            ultraGridColumn4.Hidden = true;
            ultraGridColumn5.Header.Caption = "누수알람코드";
            ultraGridColumn5.Header.VisiblePosition = 2;
            ultraGridColumn5.Hidden = true;
            ultraGridColumn18.Header.Caption = "누수알람명";
            ultraGridColumn18.Header.VisiblePosition = 3;
            ultraGridColumn18.Width = 238;
            appearance3.TextHAlignAsString = "Center";
            ultraGridColumn19.CellAppearance = appearance3;
            ultraGridColumn19.Header.Caption = "누수판단카운트";
            ultraGridColumn19.Header.VisiblePosition = 4;
            appearance4.TextHAlignAsString = "Center";
            ultraGridColumn20.CellAppearance = appearance4;
            ultraGridColumn20.Header.Caption = "현재누수카운트";
            ultraGridColumn20.Header.VisiblePosition = 5;
            ultraGridColumn20.Width = 119;
            ultraGridColumn21.Header.Caption = "누수판단결과";
            ultraGridColumn21.Header.VisiblePosition = 6;
            ultraGridColumn21.Width = 142;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21});
            this.ultraGrid2.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.ultraGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid2.Location = new System.Drawing.Point(525, 63);
            this.ultraGrid2.Name = "ultraGrid2";
            this.ultraGrid2.Size = new System.Drawing.Size(637, 595);
            this.ultraGrid2.TabIndex = 59;
            this.ultraGrid2.Text = "ultraGrid2";
            // 
            // frmLeakageResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 668);
            this.Controls.Add(this.ultraGrid2);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.ultraGrid1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmLeakageResult";
            this.Text = "자동 누수감시 결과";
            this.groupBox1.ResumeLayout(false);
            this.DateToDate.ResumeLayout(false);
            this.DateToDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel DateToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor datee;
        private System.Windows.Forms.Label DateToDateIntervalText;
        private System.Windows.Forms.Button excelBtn;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button dataBtn;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid2;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button moniteringBtn;
    }
}